﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using TSU.Views.Message;
using TSU.Common.Guided;
using TSU.Common.Instruments.Reflector;
using TSU.Connectivity_Tests;
using TSU.Views;
using D = TSU.Common.Dependencies;
using E = TSU.Common.Elements;
using Ex = TSU.Tools.Exploration;
using I = TSU.Common.Instruments;
using O = TSU.Common.Operations;
using P = TSU.Tsunami2.Preferences;
using R = TSU.Properties.Resources;
using Z = TSU.Common.Zones;

namespace TSU.Functionalities
{
    public abstract class Builder
    {
        public int id;
        public string nameAndDescription;
        public System.Drawing.Color color;
        public System.Drawing.Image image;
        private BigButton bigButton;
        internal Module parentModule;
        internal Module Module;

        public Builder(Module parentModule, string nameAndDescription)
        {
            this.nameAndDescription = nameAndDescription;
            this.parentModule = parentModule;
        }

        public virtual Module Build()
        {
            return null;
        }

        internal void Start()
        {
            if (Module != null) // If already created (and sotre in this varaible because one instance was wanted) just show it
            {
                Module.View.UpdateView();
                Module.View.BringToFront();
            }
            else // create a new module and add it ti Tsunami
            {
                bool hasUnfinishedModules = Tsunami2.Properties.MeasurementModules
                    .Any(module => module._Name == "Element alignment");

                if (hasUnfinishedModules && this.bigButton.Name == "Element alignment")
                {
                    throw new TsuException("Not allowed;2 Guided element modules are not allowed in the same .tsu file.");
                }
                else
                {
                    try
                    {
                        HourGlass.Set();
                        this.parentModule.Add(this.Build());
                    }
                    catch (System.Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        HourGlass.Remove();
                    }
                }
            }
        }

        public virtual BigButton BuildButton()
        {
            if (bigButton == null)
            {
                bigButton = new BigButton(this.nameAndDescription, this.image, this.Start, this.color);
                //BigButtonMenuItem i = new BigButtonMenuItem(b);
                bigButton.BigButtonClicked += OnBigButtonClick;
            }
            else
            {
                this.color = P.Theme.Colors.Object;
                bigButton.SetColors(this.color);
            }
            return bigButton;
        }

        private void OnBigButtonClick(object source, TsuObjectEventArgs e)
        {
            // Tsunami2.Preferences.Values.AddButtonToUsedList(source as BigButton);
        }
    }
}

#region Advanced Modules

namespace TSU.Functionalities.AdvancedModules
{
    class Theodolite : Builder
    {
        public Theodolite(Module parentModule)
            : base(parentModule, R.T_MNU_THEODOLITE)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Theodolite;
        }

        public override Module Build()
        {
            return new Polar.Module(parentModule, this.nameAndDescription);
        }
    }
    class Ecarto : Builder
    {
        public Ecarto(Module parentModule)
            : base(parentModule, R.T_MNU_ECARTO)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Line;
        }
        public override Module Build()
        {
            Line.Module m = new Line.Module(parentModule, this.nameAndDescription);

            return m;
        }
    }
    class Nivellement : Builder
    {
        public Nivellement(Module parentModule)
            : base(parentModule, R.T_MNU_LEVELING)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Level;
        }
        public override Module Build()
        {
            return new Level.Module(parentModule, this.nameAndDescription);
        }
    }
    class Tilt : Builder
    {
        public Tilt(Module parentModule)
            : base(parentModule, R.StringTilt_TiltModule)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Tilt;
        }

        public override Module Build()
        {
            return new TSU.Tilt.Module(parentModule, this.nameAndDescription);
        }
    }
    class LengthSetting : Builder
    {
        public LengthSetting(Module parentModule)
            : base(parentModule, R.StringLength_LengthModule)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.LengthSetting;
        }

        public override Module Build()
        {
            return new Length.Module(parentModule, this.nameAndDescription);
        }
    }
    //class OpenExisting : Builder
    //{
    //    public OpenExisting(Module parentModule)
    //        : base(parentModule,R.T_OPENFINALMODULE)
    //    {
    //        this.color =TSU.Tsunami2.TsunamiPreferences.Theme._ColorsColor3;
    //        this.image =R.Open;
    //    }

    //    public override Module Build()
    //    {
    //        ModuleView v = new ModuleView();
    //        string fileName = v.GetFileNameToOpen(TSU.Tsunami2.TsunamiPreferences.Values.PathOfTheDay,
    //            "", "TSU (*.TSU)|*.TSU|Tsunami (*.*)|*.*", R.T_BROWSE_XML);
    //        if (fileName != "")
    //        {
    //            object o = new Module();
    //            o = Export.TSU.OpenFile(ref o, fileName);
    //            Universal.FinalModule m = o as Universal.FinalModule;
    //            m.ParentModule = Tsunami2.Properties;
    //            m.ReCreateWhatIsNotSerialized();
    //            return m; ;
    //        }
    //        return null;
    //    }
    //}
}

#endregion

#region Managers Modules

namespace TSU.Functionalities.ManagementModules
{
    class OpenModule : Builder
    {
        public OpenModule(Module parentModule)
            : base(parentModule, R.T408)
        {
            this.id = 1;
            this.color = P.Theme.Colors.Object;
            this.image = R.Module;
        }

        public override Module Build()
        {
            Ex.Explorator ex = Ex.Explorator.Instance(parentModule);
            ex._TsuView.ShowOnTop();
            //ex.UpdateView();
            Module = ex; // store because we want only one instance of this module
            return ex;
        }
    }
    class AllElement : Builder
    {
        public AllElement(Module parentModule)
            : base(parentModule, R.T410)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Element_Aimant;
        }

        public override Module Build()
        {
            E.Manager.Module e = new E.Manager.Module(parentModule);
            e.MultiSelection = true;
            // This to navigate the all element of the Tsunami instance
            e.isMainManager = true;
            e.ShowAllElementIncludedInParentModule();

            Module = e; // store because we want only one instance of this module
            return e;
        }
    }
    class Element : Builder
    {
        public Element(Module parentModule)
            : base(parentModule, R.T_MNU_ELEMENT)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Element_Aimant;
        }

        public override Module Build()
        {
            E.Manager.Module e = new E.Manager.Module(parentModule);
            e.MultiSelection = false;
            (e._TsuView as E.Manager.View).CheckBoxesVisible = false;
            // to open new theoritcal file
            e.isMainManager = false;

            Module = e; // store because we want only one instance of this module
            return e;
        }
    }

    class Elements : Builder
    {
        public Elements(Module parentModule)
            : base(parentModule, R.T_MNU_ELEMENT)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Element_Aimant;
        }

        public override Module Build()
        {
            E.Manager.Module e = new E.Manager.Module(parentModule);
            e.SetSelectableToAllPoints();
            e.MultiSelection = true;
            (e._TsuView as E.Manager.View).CheckBoxesVisible = true;
            // to open new theoritcal file
            e.isMainManager = false;
            e.View.ElementShownInListType = E.Manager.View.ElementShownInListTypes.Points;
            e.View.UpdateView();

            Module = e; // store because we want only one instance of this module
            return e;
        }
    }
    class Measurement : Builder
    {
        public Measurement(Module parentModule)
            : base(parentModule, R.T_MNU_Meas)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Measurement;
        }

        public override Module Build()
        {
            Common.Measures.Manager e = new Common.Measures.Manager(parentModule);
            e.MultiSelection = true;
            (e._TsuView as Common.Measures.View).CheckBoxesVisible = true;
            e.isMainManager = true;
            e._TsuView.UpdateView();

            Module = e; // store because we want only one instance of this module
            return e;
        }
    }

    public class BeamOffsetComputation : Builder
    {
        public BeamOffsetComputation(Module parentModule)
            : base(parentModule, "Beam Offset Computation")
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.BOC;
        }

        public override Module Build()
        {
            Common.Compute.Compensations.Manager.Module e = new Common.Compute.Compensations.Manager.Module(parentModule);
            e.isMainManager = true;
            e.IsBeamOffsetsFrameCompensation = true;
            e._TsuView.UpdateView();
            Module = e; // store because we want only one instance of this module
            return e;
        }
    }
    public class Dependencies : Builder
    {
        public Dependencies(Module parentModule)
            : base(parentModule, R.T_MNU_Depenencies)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Susoft;
        }

        public override Module Build()
        {
            D.Manager.Module e = new D.Manager.Module(parentModule);
            e.MultiSelection = true;
            e.isMainManager = true;
            e._TsuView.UpdateView();
            Module = e; // store because we want only one instance of this module
            return e;
        }
    }
    class Instrument : Builder
    {
        public Instrument(Module parentModule)
            : base(parentModule, string.Format(R.T_MNU_INST, P.Values.Paths.Instruments))
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Instrument;
        }

        public override Module Build()
        {
            I.Manager.Module m = new I.Manager.Module(null);
            m.AllElements = m.GetByClassesAndTypes(new List<I.InstrumentClasses>
            {
                I.InstrumentClasses.CHAINE,
                I.InstrumentClasses.DISTINVAR,
                I.InstrumentClasses.ECARTOMETRE,
                I.InstrumentClasses.EDM,
                I.InstrumentClasses.GABARIT,
                I.InstrumentClasses.GYROSCOPE,
                I.InstrumentClasses.JAUGE_A_TILT,
                I.InstrumentClasses.INTERFACE,
                I.InstrumentClasses.LASER_TRACKER,
                I.InstrumentClasses.MEKOMETRE,
                I.InstrumentClasses.MIRE,
                I.InstrumentClasses.NIVEAU,
                I.InstrumentClasses.PRISME,
                I.InstrumentClasses.TACHEOMETRE,
                I.InstrumentClasses.THEODOLITE,
                I.InstrumentClasses.TILTMETRE,
                I.InstrumentClasses.TRIPOD,
                I.InstrumentClasses.FIL
            });
            m._TsuView.UpdateView();
            Module = m; // store because we want only one instance of this module
            return m;
        }
    }
    class Operation : Builder
    {
        public Operation(Module parentModule)
            : base(parentModule,
                  string.Format(R.T_MNU_OPERATION,
                     P.Values.Paths.Operations))
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Operation;
        }

        public override Module Build()
        {
            O.Manager m = new O.Manager(null);
            m._TsuView.UpdateView();
            Module = m; // store because we want only one instance of this module
            return m;
        }
    }
    class Zone : Builder
    {
        public Zone(Module parentModule)
            : base(parentModule,
                  string.Format(R.T_MNU_ZONE,
                     P.Values.Paths.Zones))
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Element_Zone;
        }

        public override Module Build()
        {
            Z.Manager m = new Z.Manager(Tsunami2.Properties);
            m.SelectionChanged += Tsunami2.Properties.On_ZoneChanged;
            m._TsuView.UpdateView();
            Module = m; // store because we want only one instance of this module
            return m;
        }
    }

}
#endregion

#region Guided Modules

namespace TSU.Functionalities.GuidedModules
{
    class Ecartometry : Builder
    {

        public Ecartometry(Module parentModule)
            : base(parentModule, R.StringGuidedLine_MainButton)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Line1;
        }

        public override Module Build()
        {
            Common.Guided.Group.Module guidedModuleGroup = new Common.Guided.Group.Module(this.parentModule, R.StringGuidedLine_GroupWireName);
            (guidedModuleGroup.View as Common.Guided.Group.View).buttons.add.BigButtonClicked += delegate
            {
                guidedModuleGroup.AddNewEcartometryModule();
            };
            guidedModuleGroup.AddNewEcartometryModule();
            return guidedModuleGroup;
        }
    }
    class Survey1Face : Builder
    {
        public Survey1Face(Module parentModule)
            : base(parentModule, R.T_GM_S1F)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.SurveyOneFace;
        }

        public override Module Build()
        {
            return Polar.GuidedModules.Survey1Face.Get(this.parentModule, this.nameAndDescription);
        }
    }
    class Survey2Face : Builder
    {
        public Survey2Face(Module parentModule)
            : base(parentModule, R.T_GM_S2F)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Survey2Face;
        }

        public override Module Build()
        {
            return Polar.GuidedModules.Survey2Face.Get(this.parentModule, this.nameAndDescription);
        }
    }

    class Survey2FaceArray : Builder
    {
        public Survey2FaceArray(Module parentModule)
            : base(parentModule, R.T_GM_S2F + "_Array")
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Survey2Face;
        }

        public override Module Build()
        {
            return Polar.GuidedModules.Survey2FaceArray.Get(this.parentModule, this.nameAndDescription);
        }
    }

    class EquiDistance : Builder
    {
        public EquiDistance(Module parentModule)
            : base(parentModule, $"{R.T_EQUIDISTANCE};{R.T_EQUIDISTANCE_SPS}")

        {
            this.color = P.Theme.Colors.Object;
            this.image = R.EquiDistance;
        }

        public override Module Build()
        {
            return Length.GuidedModules.EquiDistance.Get(this.parentModule, this.nameAndDescription);
        }
    }
    class Alignment : Builder
    {
        public override Module Build()
        {
            Common.Guided.Group.Module guidedModuleGroup = new Common.Guided.Group.Module(this.parentModule, R.StringAlignment_MainButton);
            Decore.ShowRaisedEvent(guidedModuleGroup);

            //Hide the add new station button
            (guidedModuleGroup.View as Common.Guided.Group.View).buttons.add.Available = false;
            guidedModuleGroup.AddNewAlignmentAdminModule();
            guidedModuleGroup.AddNewAlignmentTiltModule();
            guidedModuleGroup.AddNewAlignmentLevellingModule();
            guidedModuleGroup.AddNewAlignmentEcartometryModule();
            guidedModuleGroup.AddNewAlignmentTheodoliteModule(guidedModuleGroup);
            guidedModuleGroup.AddNewAlignmentLengthSettingModule();// this will subscribe teh group module to the stationmodule evetn:measureThreated
            guidedModuleGroup.AddNewAlignmentLevellingCheckModule();
            foreach (Common.Guided.Module item in guidedModuleGroup.SubGuidedModules)
            {
                if (guidedModuleGroup.SubGuidedModules.FindIndex(x => item._Name == x._Name) != 0)
                {
                    item.View.Visible = false;
                }
            }
            guidedModuleGroup.SetActiveGuidedModule(guidedModuleGroup.SubGuidedModules[0]);
            return guidedModuleGroup;
        }

        public Alignment(Module parentModule)
            : base(parentModule, R.StringAlignment_MainButton)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Alignment;
        }

        //public Alignment(Module parentModule)
        //    : base(parentModule,R.T426)
        //{
        //    this.color = Tsunami2.Preferences.Theme.Color.ButtonNormal;
        //    this.image =R.Alignment;
        //}

        //public override Module Build()
        //{
        //    return G.Alignment.Get(this.parentModule, this.nameAndDescription);
        //}

    }
    class PreAlignment : Builder
    {
        public PreAlignment(Module parentModule)
            : base(parentModule, R.T428)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.PreAlignment;
        }

        public override Module Build()
        {
            return Polar.GuidedModules.PreAlignment.Get(this.parentModule, this.nameAndDescription);
        }
    }

    class Implantation : Builder
    {
        public Implantation(Module parentModule)
            : base(parentModule, R.T430)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.I_Implantation;
        }

        public override Module Build()
        {

            return Polar.GuidedModules.Implantation.Get(this.parentModule, this.nameAndDescription);
        }
    }

    class PoinrtLancé : Builder
    {
        public PoinrtLancé(Module parentModule)
            : base(parentModule, R.T_B_POINTLANCE)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.I_PointLancé;
        }

        public override Module Build()
        {
            return Polar.GuidedModules.PointLancé.Get(this.parentModule, this.nameAndDescription);
        }
    }

    class Cheminement : Builder
    {
        public override Module Build()
        {
            Common.Guided.Group.Module guidedModuleGroup = new Common.Guided.Group.Module(this.parentModule, R.StringCheminement_GroupLevelStationName);
            (guidedModuleGroup.View as Common.Guided.Group.View).buttons.add.BigButtonClicked += delegate
            {
                guidedModuleGroup.AddNewLevellingModule();
            };
            guidedModuleGroup.AddNewLevellingModule();
            return guidedModuleGroup;
        }

        public Cheminement(Module parentModule)
            : base(parentModule, R.StringCheminement_MainButton)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Cheminement;
        }
    }
    class TourDHorizon : Builder
    {
        public TourDHorizon(Module parentModule)
            : base(parentModule, R.T_GM_TdH)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.TourDHorizon;
        }

        public override Module Build()
        {
            return Polar.GuidedModules.TourDHorizon.Get(this.parentModule, this.nameAndDescription);
        }
    }
    class SurveyTilt : Builder
    {
        public SurveyTilt(Module parentModule)
            : base(parentModule, R.StringGuidedTilt_MainButton)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Tilt;
        }

        public override Module Build()
        {
            return Tilt.GuidedModules.Survey.Get(this.parentModule, this.nameAndDescription);
        }
    }
    //class OpenExistingGuided : Builder
    //{
    //    public OpenExistingGuided(Module parentModule)
    //        : base(parentModule,R.T_OpenGuidedModule)
    //    {
    //        this.color =TSU.Tsunami2.TsunamiPreferences.Theme._ColorsColor3;
    //        this.image =R.Open;
    //    }

    //    public override Module Build()
    //    {
    //        ModuleView v = new ModuleView();
    //        string fileName = v.GetFileNameToOpen(TSU.Tsunami2.TsunamiPreferences.Values.PathOfTheDay,
    //            "", "TSU (*.TSU)|*.TSU|Tsunami (*.*)|*.*", R.T_BROWSE_XML);
    //        if (fileName != "")
    //        {
    //            object o = new Module();
    //            o = Export.TSU.OpenFile(ref o, fileName);
    //            Module m = o as Module;
    //            m.ParentModule = Tsunami2.Properties;
    //            m.ReCreateWhatIsNotSerialized();
    //            return m; ;
    //        }
    //        return null;
    //    }
    //}

}
#endregion

#region Test Modules

namespace TSU.Functionalities.TestModules
{
    class Dll : Builder
    {
        public Dll(Module parentModule)
            : base(parentModule, R.T434)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.TSU1;
        }

        public override Module Build()
        {
            Manager m = new DllTestModule(parentModule, this.nameAndDescription);
            m._Name = "TestDll";
            m._TsuView = new TestDllView();
            Module = m; // store because we want only one instance of this module
            return m;
        }
    }

    class Instrument : Builder
    {
        public Instrument(Module parentModule)
            : base(parentModule, R.T435)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Instruments;
        }

        public override Module Build()
        {
            Manager m = new InstrumentTestModule(parentModule, this.nameAndDescription);
            m._TsuView = new TestInstrument();
            Module = m; // store because we want only one instance of this module
            return m;
        }
    }
    class Test : Builder
    {
        public Test(Module parentModule)
            : base(parentModule, string.Format("{0};{1}", R.T_TEST, R.T_TEST))
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.MessageTsu;
        }

        public override Module Build()
        {
            Manager m = new TestModule(parentModule, this.nameAndDescription);
            m._TsuView = new FormTestMessage();
            Module = m; // store because we want only one instance of this module
            return m;
        }
    }

    class AM_At40 : Builder
    {
        public AM_At40(Module parentModule)
            : base(parentModule, "Advanced free station 392052")
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Nami;
        }

        public override Module Build()
        {
            Polar.Module m = new Polar.Module(parentModule, this.nameAndDescription);
            Polar.Station.Module stm = new Polar.Station.Module(m);
            Polar.Station st = stm.StationTheodolite;
            m.Add(stm);
            m.SetActiveStationModule(stm);
            I.Instrument i = Preferences.Preferences.Instance.Instruments.Find(j => j._SerialNumber == "392052");
            stm.OnInstrumentChanged(i as I.Sensor);
            stm.ChangeInstrumentHeight(0);
            st.Parameters2.Setups.InitialValues.StationPoint = new E.Point("STL.0001");
            st.Parameters2.Setups.InitialValues.IsPositionKnown = false;
            st.Parameters2._Team = "PSPS";
            st.Parameters2.Setups.InitialValues.VerticalisationState = Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation.Verticalised;
            st.Parameters2.DefaultMeasure.Distance.Reflector = Preferences.Preferences.Instance.Instruments.Find(x => x._Name == "RFI0.5_2884") as Reflector;
            st.Parameters2.DefaultMeasure._Point._Name = "PM001";
            stm.ChangeNumberOfMeasurement(1, st.Parameters2.DefaultMeasure);


            string path = P.Values.Paths.Temporary + "res.res";
            string content =
    @"% Point List in IdXYZ Format created by Tsunami
            Point.001     0.000007     2.122871     0.879855
            Point.002     0.905022     1.877979     0.875809
            Point.003     2.403806    -1.276692     0.844213
            Point.004    -0.659031    -1.173726     0.245880
            Point.005    -1.034864    -1.010230     0.264591";

            File.WriteAllText(path, content);
            m._ElementManager.zone = Preferences.Preferences.Instance.Zones[0];
            m._ElementManager.AddElementsFromFile(new FileInfo(path), E.Coordinates.CoordinateSystemsTsunamiTypes.Unknown, false);
            m._TsuView.UpdateView();
            stm.UpdateView();
            m.UpdateView();
            stm.ShowInParentModule();
            Module = m; // store because we want only one instance of this module
            return m;
        }
    }

    class GM_At40 : Builder
    {
        public GM_At40(Module parentModule)
            : base(parentModule, "GM_S1F_")
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Nami;
        }

        public override Module Build()
        {
            Common.Guided.Module gm = Polar.GuidedModules.Survey1Face.Get(this.parentModule, this.nameAndDescription);

            // load element from 928-r008
            string path = Preferences.Preferences.Instance.Paths.Temporary + "res.res";
            string content =
    @"% Point List in IdXYZ Format created by Tsunami
            Point.001     0.000007     2.122871     0.879855
            Point.002     0.905022     1.877979     0.875809
            Point.003     2.403806    -1.276692     0.844213
            Point.004    -0.659031    -1.173726     0.245880
            Point.005    -1.034864    -1.010230     0.264591";

            File.WriteAllText(path, content);
            gm._ElementManager.zone = Preferences.Preferences.Instance.Zones[0];
            gm._ElementManager.AddElementsFromFile(new FileInfo(path), E.Coordinates.CoordinateSystemsTsunamiTypes.SU, false);

            Polar.Station.Module stm = gm._ActiveStationModule as Polar.Station.Module;
            Polar.Station st = stm.StationTheodolite;

            //TSU.Common.Instruments.Instrument i = Preferences.Preferences.Instance.GeodeInstruments.Find(j => j._SerialNumber == "392052");
            //stm.OnInstrumentChanged(i as Sensor);
            // stm._InstrumentManager.SelectedInstrumentModule.Connect(); make problem with reflector as ther is no time to receive targets

            stm.ChangeInstrumentHeight(0);
            st.Parameters2._StationPoint = new E.Point("STL.0001");
            st.Parameters2.Setups.InitialValues.IsPositionKnown = false;
            st.Parameters2._Team = "PSPS";
            st.Parameters2.Setups.InitialValues.VerticalisationState = Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation.Verticalised;
            st.Parameters2.DefaultMeasure.Distance.Reflector = Preferences.Preferences.Instance.Instruments.Find(x => x._Name == "RFI0.5_2884") as Reflector;
            st.Parameters2.DefaultMeasure._Point._Name = "PM001";
            stm.ChangeNumberOfMeasurement(1, st.Parameters2.DefaultMeasure);

            //stm.UpdateView();
            gm.currentIndex = 2;
            gm.UpdateView();
            Module = gm; // store because we want only one instance of this module
            return gm;
        }
    }

    class DnA3000 : Builder
    {
        public DnA3000(Module parentModule)
            : base(parentModule, R.T439)
        {
            this.color = P.Theme.Colors.Object;

            this.image = R.Nami;
        }

        public override Module Build()
        {
            Level.Module m = new Level.Module(parentModule, R.T441);
            Level.Station.Module stm = new Level.Station.Module(m);
            m.Add(stm);
            stm._WorkingStationLevel._Parameters._Instrument = (Preferences.Preferences.Instance.Instruments.Find(j => j._SerialNumber == "331085") as I.Level);

            string path = Preferences.Preferences.Instance.Paths.Temporary + "res.res";
            string content =
    @"ST20                 16.00137      6.07501     -1.87510
    PJ03   25.216900   4.516700   -1.422500
    PMW01   9.939100   6.436600   -2.401000
    TEMP1   13.181100   7.364600   -1.142700
    TRIJU   23.042500   3.916900   -1.988600";

            File.WriteAllText(path, content);
            m._ElementManager.AddElementsFromFile(new FileInfo(path), E.Coordinates.CoordinateSystemsTsunamiTypes.SU, false);

            content =
    @" TT41      ;BTV.412426.S                    ;  821.53150; 3080.22616; 4216.55683;2380.52769;380.96736;  .350; -.01772; -.056608; 98.2938; 1;O;23-MAR-2016;                    ;;
    TT41      ;BTV.412442.E                    ;  829.26890; 3087.95250; 4216.76271;2380.16179;380.60281; 7.690; -.01772; -.056608; 98.2938; 8;A;23-MAR-2016;                    ;;
    TT41      ;BTV.412442.S                    ;  829.65290; 3088.33574; 4216.77298;2380.14007;380.58115;  .448; -.01772; -.056608; 98.2938; 8;O;23-MAR-2016;                    ;;
    TT41      ;LASER.412443.E                  ;  829.78810; 3088.46232; 4216.77893;2379.98605;380.42716;  .123; -.01772; -.056608; 98.2938;24; ;23-MAR-2016;                    ;;
    TT41      ;LASER.412443.S                  ;  830.07370; 3088.74736; 4216.78657;2379.96989;380.41105;  .246; -.01772; -.056608; 98.2938;24; ;23-MAR-2016;                    ;;
    TCC4      ;NIV.1.                          ;99999.00000; 3082.76349; 4222.18976;    .00000;380.05740;      ;        ;         ;        ; 8;O;23-MAR-2016;                    ;;
    TCC4      ;NID.8.                          ;99999.00000; 3080.16305; 4213.96310;    .00000;381.57889;      ;        ;         ;        ;24;O;23-MAR-2016;                    ;;
    ";
            path = P.Values.Paths.Temporary + "dat.dat";
            File.WriteAllText(path, content);
            m._ElementManager.AddElementsFromFile(new FileInfo(path), E.Coordinates.CoordinateSystemsTsunamiTypes.CCS, false);
            m._TsuView.UpdateView();
            stm.ShowInParentModule();
            Module = m; // store because we want only one instance of this module
            return m;
        }
    }
    class Chaba : Builder
    {
        public Chaba(Module parentModule)
            : base(parentModule, R.T442)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Nami;
        }

        public override Module Build()
        {
            return new E.Manager.Module(null);
        }
    }
    class Pascal : Builder
    {
        public Pascal(Module parentModule)
            : base(parentModule, R.T444)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Nami;
        }

        public override Module Build()
        {
            E.Manager.Module m = new E.Manager.Module();
            m.MultiSelection = true;
            m.AddElementsFromFile(new FileInfo(@"C:\data\Theoretical Files\LHCb SU.xml"), E.Coordinates.CoordinateSystemsTsunamiTypes.Unknown, false);
            return m;
        }
    }
    class Message : Builder
    {
        public Message(Module parentModule)
            : base(parentModule, string.Format("{0};{1}", R.T_MESSAGE, R.T_TEST_MESSAGE_EXAMPLE)
)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Nami;
        }

        public override Module Build()
        {
            Manager m = new MessageTestModule(parentModule, this.nameAndDescription);
            return m;
        }
    }
    class ElementManager : Builder
    {
        public ElementManager(Module parentModule)
            : base(parentModule, string.Format("{0};{1}", R.T_ELEMENTMANAGER, R.T_ELEMENTMANAGER_EXAMPLE)
)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Nami;
        }

        public override Module Build()
        {
            Module m = new E.Manager.Module(parentModule);
            //            string path = @"c:\temp\temp.dat";
            //            File.WriteAllText(path,
            //            @"TT41      ;QTLD.412100.E                   ;  744.34425; 3003.20310; 4214.49562;2385.11806;385.54483; 2.273; -.01727; -.056608; 98.8033; 1;A;24-AUG-2015;                    ;;
            //TT41      ;QTLD.412100.S                   ;  747.14425; 3005.99812; 4214.54816;2384.95965;385.38686; 2.990; -.01727; -.056608; 98.8033; 1;O;24-AUG-2015;                    ;;
            //TT41      ;QTLD.412108.E                   ;  747.89725; 3006.74978; 4214.56229;2384.91705;385.34438;  .563; -.01727; -.056608; 98.8033; 1;A;24-AUG-2015;                    ;;
            //TT41      ;QTLD.412108.S                   ;  750.69725; 3009.54480; 4214.61484;2384.75863;385.18641; 2.990; -.01727; -.056608; 98.8033; 1;O;24-AUG-2015;                    ;;
            //TT41      ;MBG.412115.E                    ;  753.09325; 3011.92347; 4214.67660;2384.39849;384.82666;  .896; -.00033; -.056608; 98.5485; 1;A;24-AUG-2015;                    ;;
            //TT41      ;MBG.412115.S                    ;  756.58325; 3015.40697; 4214.75604;2384.20104;384.62977; 6.300; -.00033; -.056608; 98.5485; 1;O;24-AUG-2015;                    ;;
            //TT41      ;MBHFD.412133.E                  ;  760.71825; 3019.54045; 4214.85175;2384.07686;384.50626; 2.406; -.01775; -.056608; 98.2561; 1;A;24-AUG-2015;                    ;;
            //TT41      ;MBHFD.412133.S                  ;  761.97025; 3020.78998; 4214.88599;2384.00603;384.43564; 1.900; -.01775; -.056608; 98.2561; 1;O;24-AUG-2015;                    ;;
            //TT41      ;MBHFD.412141.E                  ;  764.71825; 3023.53251; 4214.96276;2383.85062;384.28068; 2.100; -.01782; -.056588; 98.1807; 1;A;24-AUG-2015;                    ;;
            //TT41      ;MBHFD.412141.S                  ;  765.97025; 3024.78199; 4214.99848;2383.77981;384.21008; 1.900; -.01782; -.056588; 98.1807; 1;O;24-AUG-2015;                    ;;
            //TT41      ;MDSH.412147.E                   ;  767.49250; 3026.29517; 4215.04442;2383.58893;384.01945; 1.108; -.01785; -.056567; 98.1430; 1;A;24-AUG-2015;                    ;;
            //TT41      ;MDSH.412147.S                   ;  768.01250; 3026.81412; 4215.05957;2383.55953;383.99014;  .700; -.01785; -.056567; 98.1430; 1;O;24-AUG-2015;                    ;;
            //TT41      ;QTLF.412200.E                   ;  768.74550; 3027.55818; 4215.07741;2383.73770;384.16843;  .548; -.01785; -.056567; 98.1430; 1;A;24-AUG-2015;                    ;;
            //TT41      ;QTLF.412200.S                   ;  771.54550; 3030.35251; 4215.15895;2383.57941;384.01060; 2.990; -.01785; -.056567; 98.1430; 1;O;24-AUG-2015;                    ;;
            //TT41      ;QTLF.412208.E                   ;  772.29850; 3031.10399; 4215.18087;2383.53683;383.96815;  .563; -.01785; -.056567; 98.1430; 1;A;24-AUG-2015;                    ;;
            //TT41      ;QTLF.412208.S                   ;  775.09850; 3033.89832; 4215.26241;2383.37853;383.81032; 2.990; -.01785; -.056567; 98.1430; 1;O;24-AUG-2015;                    ;;
            //TT41      ;QTLF.412215.E                   ;  775.85150; 3034.64980; 4215.28433;2383.33597;383.76788;  .563; -.01785; -.056567; 98.1430; 1;A;24-AUG-2015;                    ;;
            //TT41      ;QTLF.412215.S                   ;  778.65150; 3037.44413; 4215.36587;2383.17765;383.61003; 2.990; -.01785; -.056567; 98.1430; 1;O;24-AUG-2015;                    ;;
            //TT41      ;MDSV.412223.E                   ;  780.20250; 3038.98725; 4215.41235;2383.00701;383.43965; 1.366; -.01785; -.056567; 98.1430; 1;A;24-AUG-2015;                    ;;
            //TT41      ;MDSV.412223.S                   ;  780.72250; 3039.50620; 4215.42749;2382.97761;383.41034;  .700; -.01785; -.056567; 98.1430; 1;O;24-AUG-2015;                    ;;
            //TT41      ;QTSD.412300.E                   ;  781.45550; 3040.24245; 4215.44751;2383.01912;383.45197;  .548; -.01785; -.056567; 98.1430; 1;A;24-AUG-2015;                    ;;
            //TT41      ;QTSD.412300.S                   ;  782.75550; 3041.53982; 4215.48537;2382.94563;383.37870; 1.490; -.01785; -.056567; 98.1430; 1;O;24-AUG-2015;                    ;;
            //TT41      ;QTLD.412305.E                   ;  783.56550; 3042.34818; 4215.50896;2382.89984;383.33304;  .620; -.01785; -.056567; 98.1430; 1;A;24-AUG-2015;                    ;;
            //TT41      ;QTLD.412305.S                   ;  786.36550; 3045.14251; 4215.59049;2382.74154;383.17521; 2.990; -.01785; -.056567; 98.1430; 1;O;24-AUG-2015;                    ;;
            //TT41      ;MBHFD.412324.E                  ;  793.61970; 3052.37548; 4215.80350;2382.21661;382.65151; 6.835; -.01782; -.056567; 98.1807; 1;A;24-AUG-2015;                    ;;
            //TT41      ;MBHFD.412324.S                  ;  794.87170; 3053.62496; 4215.83922;2382.14581;382.58092; 1.900; -.01782; -.056567; 98.1807; 1;O;24-AUG-2015;                    ;;
            //TT41      ;MBHFD.412330.E                  ;  796.37650; 3055.12676; 4215.88126;2382.06070;382.49607;  .857; -.01775; -.056588; 98.2561; 1;A;24-AUG-2015;                    ;;
            //TT41      ;MBHFD.412330.S                  ;  797.62850; 3056.37629; 4215.91549;2381.98987;382.42545; 1.900; -.01775; -.056588; 98.2561; 1;O;24-AUG-2015;                    ;;
            //TT41      ;MDSV.412335.E                   ;  798.73650; 3057.48394; 4215.94468;2381.95903;382.39480;  .694; -.01772; -.056608; 98.2938; 1;A;24-AUG-2015;                    ;;
            //TT41      ;MDSV.412335.S                   ;  799.25650; 3058.00292; 4215.95860;2381.92961;382.36547;  .700; -.01772; -.056608; 98.2938; 1;O;24-AUG-2015;                    ;;
            //TT41      ;MDSH.412338.E                   ;  800.04150; 3058.77857; 4215.98178;2381.74854;382.18453;  .605; -.01772; -.056608; 98.2938; 1;A;24-AUG-2015;                    ;;
            //TT41      ;MDSH.412338.S                   ;  800.56150; 3059.29755; 4215.99569;2381.71911;382.15519;  .700; -.01772; -.056608; 98.2938; 1;O;24-AUG-2015;                    ;;
            //");
            //            m.AddFromFile2(new FileInfo(path), E.Coordinates.CoordinateSystemsTsunamiTypes.CCS, true);
            //            E.Composites.CompositeElement c = m.AllElements[m.AllElements.Count - 1] as E.Composites.CompositeElement;
            //            E.Point p = new E.Point("test1.1");
            //            m.AddElement(m, p);
            //            m.AddElement(m, new E.Point("test1.2"));
            //            m.AddElement(m.ParentModule, new E.Point("test2.1"));
            //            m.View.MultiSelection = true;
            //            m.SetSelectableListToAllAlesages();
            //            m.View.UpdateView();
            //            //m.ElementModuleView.SetSelectableList(new List<TsuObject>() { m.Elements[1], m.Elements[2] });
            //            //m.ElementModuleView.SetPreSelectedList(new List<TsuObject>() { p });
            return m;
        }
    }
    class ClientTCP : Builder
    {
        public ClientTCP(Module parentModule)
            : base(parentModule, R.T446)
        {
            this.color = P.Theme.Colors.Attention;
            this.image = R.Nami;
        }

        public override Module Build()
        {
            Manager m = new TcpTestModule(parentModule, this.nameAndDescription);
            m._TsuView = new TSU.Connectivity_Tests.Client();
            return m;
        }
    }
    class ServerTCP : Builder
    {
        public ServerTCP(Module parentModule)
            : base(parentModule, R.T448)
        {
            this.color = P.Theme.Colors.Attention;
            this.image = R.Nami;
        }

        public override Module Build()
        {
            Manager m = new ServerTestModule(parentModule, this.nameAndDescription);
            return m;
        }
    }
}
#endregion

#region Settings Modules

namespace TSU.Functionalities.PreferencesModules
{
    class GUISettings : Builder
    {
        public GUISettings(Module parentModule)
            : base(parentModule, R.T_Settings)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.I_Settings;
        }

        public override Module Build()
        {
            if (P.Values.View == null)
            {
                P.Values.CreateViewAutmatically = true;
                P.Values.CreateView();
            }
            return P.Values;
        }
    }
    class Explorer : Builder
    {
        public Explorer(Module parentModule)
            : base(parentModule, R.T_B_Explore)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Module;
        }
        public override Module Build()
        {
            Ex.Explorator ex = Ex.Explorator.Instance(Tsunami2.Properties);
            ex._TsuView.ShowOnTop();
            return ex;
        }
    }
}

#endregion

#region Smart Modules

namespace TSU.Functionalities.SmartModules
{
    class Roll : Builder
    {
        public Roll(Module parentModule)
            : base(parentModule, R.StringSmart_Roll)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Tilt;
        }

        public override Module Build()
        {
            return new Tilt.Smart.Module(parentModule, this.nameAndDescription);
        }
    }

    class Levelling : Builder
    {
        public Levelling(Module parentModule)
            : base(parentModule, R.StringSmart_Level)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Level;
        }

        public override Module Build()
        {
            return new Level.Smart.Module(parentModule, this.nameAndDescription);
        }
    }

    class Wire : Builder
    {
        public Wire(Module parentModule)
            : base(parentModule, R.StringSmart_SmartWire)
        {
            this.color = P.Theme.Colors.Object;
            this.image = R.Line;
        }

        public override Module Build()
        {
            return new Line.Smart.Module(parentModule, this.nameAndDescription);
        }
    }
}

#endregion
