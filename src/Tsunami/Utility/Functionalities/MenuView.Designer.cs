﻿namespace TSU.Functionalities
{
    partial class MenuView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // _PanelBottom
            // 
            this._PanelBottom.AllowDrop = true;
            this._PanelBottom.AutoSize = true;
            this._PanelBottom.Cursor = System.Windows.Forms.Cursors.Hand;
            this._PanelBottom.Location = new System.Drawing.Point(5, 84);
            this._PanelBottom.Size = new System.Drawing.Size(738, 707);
            // 
            // _PanelTop
            // 
            this._PanelTop.Size = new System.Drawing.Size(738, 79);
            // 
            // buttonForFocus
            // 
            this.buttonForFocusWithTabIndex0.Location = new System.Drawing.Point(375, -20);
            // 
            // MenuView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 796);
            this.MinimumSize = new System.Drawing.Size(200, 187);
            this.Name = "MenuView";
            this.Text = "MenuView";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MenuView_FormClosing);
            this.Load += new System.EventHandler(this.MenuView_Load);
            this.Controls.SetChildIndex(this.buttonForFocusWithTabIndex0, 0);
            this.Controls.SetChildIndex(this._PanelTop, 0);
            this.Controls.SetChildIndex(this._PanelBottom, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}