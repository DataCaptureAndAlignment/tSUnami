﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml.Serialization;
using TSU.Common;
using TSU.Common.Elements;
using TSU.Connectivity_Tests;
using TSU.Views.Message;
using R = TSU.Properties.Resources;

namespace TSU.Functionalities
{
    // To add something in the menu:
    // Create a new class in TSU.Module.Menu.Builders and add an instance to one of the builder list in one of those  CreateAdvancedBuilders, CreateTestBuilders(), ....

    public class Menu : Manager
    {
        #region Fields
        [XmlIgnore]
        public new MenuView View
        {
            get
            {
                return this._TsuView as MenuView;
            }
            set
            {
                this._TsuView = value;
            }
        }

        [XmlIgnore]
        public Tsunami Tsunami
        {
            get
            {
                return this.ParentModule as Tsunami;
            }
        }

        internal List<Builder> advancedBuilders;
        internal List<Builder> guidedBuilders;
        internal List<Builder> smartBuilders;
        internal List<Builder> guidedTheodoliteBuilders;

        internal List<Builder> managementBuilders;
        internal List<Builder> testBuilders;
        internal List<Builder> settingsBuilders;

        internal Module _activeMeasurementModule { get; set; }

        #endregion

        #region Construction

        public Menu() : base()
        {

        }
        public Menu(Module module)
            : base(module, R.T449)
        {

        }

        /// <summary>
        /// Normally called from the Module.Module constructor
        /// </summary>
        public override void Initialize()
        {
            this.CreateManagementBuilders();
            this.CreateAdvancedBuilders();
            this.CreateGuidedBuilders();
            this.CreateSmartBuilders();
            this.CreateManagementBuilders();
            this.CreateTestBuilders();
            this.CreateSettingsBuilders();
            base.Initialize();
        }

        private void CreateSettingsBuilders()
        {
            this.settingsBuilders = new List<Builder>
            {
                new PreferencesModules.GUISettings(ParentModule),
                new PreferencesModules.Explorer(ParentModule)
            };
        }

        private void CreateAdvancedBuilders()
        {
            this.advancedBuilders = new List<Builder>
            {
                new AdvancedModules.Theodolite(ParentModule),
                new AdvancedModules.Tilt(ParentModule),
                new AdvancedModules.Nivellement(ParentModule),
                new AdvancedModules.Ecarto(ParentModule),
                new AdvancedModules.LengthSetting(ParentModule)
            };
            //new Func.AdvancedModules.OpenExisting(ParentModule),;
        }
        private void CreateGuidedBuilders()
        {
            //this.guidedTheodoliteBuilders = new List<Builder>
            //{
            //    new GuidedModules.Implantation(ParentModule),
            //    new GuidedModules.PoinrtLancé(ParentModule),
            //    new GuidedModules.TourDHorizon(ParentModule),
            //    new GuidedModules.Survey2Face(ParentModule),
            //    new GuidedModules.Survey1Face(ParentModule)
            //};

            this.guidedBuilders = new List<Builder>
            {
                //new GuidedModules.SurveyTilt(ParentModule),
                //new GuidedModules.Cheminement(ParentModule),
                //new GuidedModules.Ecartometry(ParentModule),
                new GuidedModules.Alignment(ParentModule)
            };
        }

        private void CreateSmartBuilders()
        {
            this.smartBuilders = new List<Builder>
            {
                new SmartModules.Roll(ParentModule),
                new SmartModules.Levelling(ParentModule),
                new SmartModules.Wire(ParentModule)
            };
        }

        private void CreateManagementBuilders()
        {
            this.managementBuilders = new List<Builder>
            {
                new ManagementModules.Elements(ParentModule),
                new ManagementModules.Instrument(ParentModule),
                new ManagementModules.Measurement(ParentModule),
                new ManagementModules.Operation(ParentModule),
                new ManagementModules.Zone(ParentModule),
                new ManagementModules.Dependencies(ParentModule),
                new ManagementModules.BeamOffsetComputation(ParentModule)
            };
            //new Func.ManagementModules.AllElement(ParentModule),
            //new Func.ManagementModules.Element(ParentModule),
            //new Func.ManagementModules.OpenModule(ParentModule),
        }
        private void CreateTestBuilders()
        {
            this.testBuilders = new List<Builder>
            {
                new TestModules.AM_At40(ParentModule),
                new TestModules.GM_At40(ParentModule),
                new TestModules.Chaba(ParentModule),
                new TestModules.Dll(ParentModule),
                new TestModules.DnA3000(ParentModule),
                new GuidedModules.Survey2FaceArray(ParentModule),
                new GuidedModules.EquiDistance(ParentModule),
                new GuidedModules.PreAlignment(ParentModule),
                new TestModules.Test(ParentModule),
                new TestModules.Instrument(ParentModule),
                new TestModules.ElementManager(ParentModule)
            };
            //new Func.TestModules.Pascal(ParentModule),
            //new Func.TestModules.ClientTCP(ParentModule),
            //new Func.TestModules.ServerTCP(ParentModule),
            //new Func.TestModules.Message(ParentModule),
        }
        #endregion

        #region General
        internal void NotImplementedModule()
        {
            Manager m = new TestModule(ParentModule, R.T021);
            ParentModule.Add(m);
        }

        internal void RefreshOpenedAndFinishedModule()
        {
            View.AddOpenedAndFinishedModuleButton();
        }

        enum OpeningAction
        {
            KeepCurrent, Mix, CloseAndOpen, JustOpen, Unknown
        }

        public void OpenExistingModules(string fileName)
        {
            fileName = this.Tsunami.MakeCopyIfThisIsATemplate(fileName);

            try
            {

                // check if it a zip containing a TSU, coming from a bug export from Tsunami
                {
                    FileInfo fi = new FileInfo(fileName);
                    if (fi.Extension.ToUpper() == ".ZIP")
                    {
                        string TsuPath = ExtractTSUFromZip(fi);

                        if (TsuPath != "")
                            OpenExistingModules(TsuPath);
                        else
                            Tsunami.TsuLoaded.Set();

                        return;
                    }
                }

                bool saveSomeMemory = false;
                if (new FileInfo(fileName).Length > 500000)
                    saveSomeMemory = true;


                if (fileName != "")
                {
                    OpeningAction action = OpeningAction.KeepCurrent;

                    // OpenFile
                    object obj = null;
                    BroadcastMessage("Reading the *.TSU file");
                    var openFileThread = new Thread(() =>
                    {
                        try
                        {
                            //this.Tsunami.View.ShowWaitingForm(this.Tsunami, "Opening an existing module", 10*1000);
                            InvokeOnApplicationDispatcher(() =>
                            {
                                this.Tsunami.View.WaitingForm.Show(this, this.Tsunami.View, "Opening an existing module",1000);
                                HourGlass.Set();
                            });

                            obj = IO.Tsu.OpenFile(fileName);

                            if (obj == null)
                                return;
                            Tsunami openedTsunami = obj as Tsunami;
                            Tsunami currentTsunami = Tsunami2.Properties;

                            WhatToDoWithJustOpenedTsunami(saveSomeMemory, ref action, obj, openedTsunami, currentTsunami);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception($"{R.T_TSU_OPENING_FAILED}", ex);
                        }
                        finally
                        {
                            InvokeOnApplicationDispatcher(() =>
                            {
                                HourGlass.Remove();
                            this.Tsunami.View.HideWaitingForm();
                            Tsunami.TsuLoaded.Set();
                            });
                        }
                    });
                    openFileThread.Start();
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_TSU_OPENING_FAILED}", ex);
            }
        }

        private void WhatToDoWithJustOpenedTsunami(bool saveSomeMemory, ref OpeningAction action, object obj, Tsunami openedTsunami, Tsunami currentTsunami)
        {
            if (openedTsunami != null) // this is a full Tsunami file woth several measuremet module
            {
                bool modulesAreOpened = currentTsunami.MeasurementModules.Count > 0;
                bool incompatibleLanguage = currentTsunami.Language != openedTsunami.Language;


                //check language compatibility
                if (incompatibleLanguage)
                {
                    if (modulesAreOpened)
                    {
                        throw new Exception($"{R.T_SORRY};" +
                            $"{R.T_NO_OPENING_AVAILABLE_YET}, {R.T_NO_LANGUAGE_SWITH}");
                    }
                    else
                    {
                        string titleAndMessage = $"{R.T_SORRY};{R.T_LANGUAGE_CHANGE} {openedTsunami.Language}";
                        new MessageInput(MessageType.Warning, titleAndMessage).Show();
                        Tsunami2.Preferences.Values.SwitchLanguage(openedTsunami.Language, saveInPreferences: false);
                    }
                }

                if (modulesAreOpened)
                {
                    string close = "Close";
                    string add = "Add";


                    bool zoneCompatible = true;
                    { // check zone compatibility
                        if (openedTsunami.Zone != null)
                            if (currentTsunami.Zone != null)
                                zoneCompatible = openedTsunami.Zone._Name == currentTsunami.Zone._Name;
                    }

                    bool hourGlassWasOn = HourGlass.IsSet;
                    HourGlass.Remove();
                    this.Tsunami.View.WaitingForm.Suspend();
                    if (!zoneCompatible)
                    {
                        string titleAndMessage = string.Format("{0};{1}", R.T_ZONE_CONFLICT, R.T_THE_INCOMING_MODULES_HAVE_A_DIFFERENT_ZONE_THAT_THE_EXISTING_ONE_DO_YOU_WANT_TO_SAVE_AND_CLOSE_THE_EXISTING_ONE);
                        MessageInput mi = new MessageInput(MessageType.ImportantChoice, titleAndMessage)
                        {
                            ButtonTexts = new List<string> { close, R.T_CANCEL },
                        };
                        if (mi.Show().TextOfButtonClicked == close)
                            action = OpeningAction.CloseAndOpen;
                        else
                            action = OpeningAction.KeepCurrent;
                    }
                    else
                    {
                        string titleAndMessage = string.Format("{0};{1}", R.T_MODULES_ARE_ALREADY_OPENED, R.T_DO_YOU_WANT_TO_SAVE_CHANGES_AND_CLOSE_THEM_OR_DO_YOU_WANT_TO_ADD_THE_INCOMING_MODULES_WITHIN_THE_SAME_FILE);
                        MessageInput mi = new MessageInput(MessageType.ImportantChoice, titleAndMessage)
                        {
                            ButtonTexts = new List<string> { R.T_CANCEL, close, add },
                        };
                        string r = mi.Show().TextOfButtonClicked;
                        if (r == R.T_CANCEL) return;
                        if (r == close)
                            action = OpeningAction.CloseAndOpen;
                        else
                            action = OpeningAction.Mix;
                    }

                    this.Tsunami.View.WaitingForm.Resume();
                    if (hourGlassWasOn)
                        HourGlass.Set();
                }
                else
                    action = OpeningAction.JustOpen;

                switch (action)
                {
                    case OpeningAction.JustOpen:
                        InvokeOnApplicationDispatcher(() =>
                        {
                            BroadcastMessage(R.T_LOADING_THE_MODULES);
                            Tsunami.Load(openedTsunami, saveSomeMemory);
                        });
                        break;
                    case OpeningAction.Mix:
                        foreach (var item in openedTsunami.Points)
                            currentTsunami.Points.AddIfDoNotExistWithSameGUID(item);

                        foreach (var item in openedTsunami.HiddenPoints)
                            currentTsunami.HiddenPoints.AddIfDoNotExistWithSameGUID(item);


                        Polar.Module moduleTheo1 = currentTsunami.MeasurementModules.Find(x => x is Polar.Module) as Polar.Module;
                        bool thereIsAPolarModule = (moduleTheo1 != null);
                        bool thereWasSomeMergeing = false; // if tru we will need to restart the moduleTheo1.

                        foreach (Module item in openedTsunami.MeasurementModules)
                        {
                            InvokeOnApplicationDispatcher(() =>{ this.Tsunami.View.WaitingForm.BeginAstep(item.NameAndDescription); });
                           
                            if (item is Polar.Module atm)
                            {
                                bool mergePolarModule = false;
                                if (thereIsAPolarModule)
                                {
                                    string titleAndMessage = $"{R.T_MERGE_THE_POLAR_STATIONS_TOGETHER}?;{R.T_MERGE_THE_POLAR_STATIONS_TOGETHER}? ";
                                    MessageInput mi = new MessageInput(MessageType.ImportantChoice, titleAndMessage)
                                    {
                                        ButtonTexts = new List<string> { R.T_YES, R.T_NO },
                                    };
                                    mergePolarModule = mi.Show().TextOfButtonClicked == R.T_YES;
                                }

                                if (mergePolarModule)
                                {
                                    foreach (Polar.Station.Module stm in atm.StationModules)
                                    {
                                        moduleTheo1.StationModules.Add(stm);
                                        stm.ParentModule = moduleTheo1;
                                        // stm.ReCreateWhatIsNotSerialized();
                                    }
                                    thereWasSomeMergeing = true;
                                    continue;
                                }
                            }
                            InvokeOnApplicationDispatcher(() =>{Tsunami.RestartModule(item); });
                        }
                        if (thereWasSomeMergeing)
                        {
                            //RestartModule(moduleTheo1, saveSomeMemory: true, addToTsunami: false);
                            new MessageInput(MessageType.Important, R.T_CONTINUE_TO_ADD_TSU_FILES_OR_SAVE_AND_RESTART_THE_SEE_EFFECT_OF_THE_MERG).Show();
                        }

                        break;
                    case OpeningAction.CloseAndOpen:
                        // Close
                        SaveAndCloseAndOpen(openedTsunami);
                        break;
                    case OpeningAction.Unknown:
                    case OpeningAction.KeepCurrent:
                    default:
                        InvokeOnApplicationDispatcher(() =>
                        {
                            openedTsunami.Dispose();
                        });
                        
                        return;
                }


            }
            else // this is a single module
            {
                Tsunami.RestartModule(obj as Module);
            }
        }

        /// <summary>
        /// this check fot TSUT extention, if so the template is copied with default name in the folderof the day
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string MakeCopyIfThisIsATemplate(string fileName)
        {
            FileInfo fi = new FileInfo(fileName);
            if (fi.Extension.ToUpper() == ".TSUT")
            {
                string proposedPath = Tsunami.GetProposedPath();
                File.Copy(fileName, proposedPath);
                fileName = proposedPath;
            }
            return fileName;
        }


        private static string ExtractTSUFromZip(FileInfo fi)
        {
            string tempPath = Tsunami2.Preferences.Values.Paths.Temporary;
            // extract 
            using (Ionic.Zip.ZipFile zip1 = Ionic.Zip.ZipFile.Read(fi.FullName))
            {
                // here, we extract every entry, but we could extract conditionally
                // based on entry name, size, date, checkbox status, etc.  
                foreach (Ionic.Zip.ZipEntry e in zip1)
                {
                    if (e.FileName.ToUpper().EndsWith(".TSU"))
                    {
                        e.Extract(tempPath, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);

                        return tempPath + e.FileName;
                    }
                }
            }
            return "";


        }

        public void Save()
        {
            Tsunami currentTsunami = Tsunami2.Properties;

            if (currentTsunami.MeasurementModules.Count > 0)
                currentTsunami.Save("", "manual saving");

            else
            {
                string titleAndMessage = $"{R.T_NOTHING_TO_SAVE};{R.T_YOU_NEED_A_MEASUREMENT_MODULE_OPENED_TO_BE_ABLE_TO_SAVE}";
                new MessageInput(MessageType.GoodNews, titleAndMessage).Show();
            }
        }

        public void New()
        {
            Tsunami currentTsunami = Tsunami2.Properties;

            if (currentTsunami.MeasurementModules.Count > 0)
                SaveAndCloseEverything();
            else
            {
                string titleAndMessage = $"{R.T_NOTHING_TO_DO};{R.T_YOU_ARE_ALREADY_IN_A_FRESH_TSUNAMI_INSTANCE}";
                new MessageInput(MessageType.GoodNews, titleAndMessage).Show();
            }
        }

        public void SaveAndCloseEverything()
        {
            Common.Tsunami currentTsunami = Tsunami2.Properties;
            currentTsunami.SavingFinished += CleanInstance_After_saving;
            currentTsunami.Save(currentTsunami.PathOfTheSavedFile, "on quitting");
        }

        Tsunami waitingTsunami;

        private void SaveAndCloseAndOpen(Tsunami openedTsunami)
        {
            waitingTsunami = openedTsunami;
            Tsunami currentTsunami = Tsunami2.Properties;
            InvokeOnApplicationDispatcher(() => { BroadcastMessage($"Saving {currentTsunami.PathOfTheSavedFile}"); }); 
            //currentTsunami.AfterSaving += OpenWaitingInstance;
            Debug.WriteInConsole("Beginning Save of currentTsunami.");
            currentTsunami.Save(currentTsunami.PathOfTheSavedFile, "on closing");
            OpenWaitingInstance(currentTsunami, null);
            Debug.WriteInConsole("Returned from Save of currentTsunami.");
        }

        private void OpenWaitingInstance(object sender, EventArgs e)
        {
            Debug.WriteInConsole("Finished Save of currentTsunami.");

            Tsunami currentTsunami = sender as Tsunami;

            Debug.WriteInConsole("Beginning CleanInstance of currentTsunami.");
            InvokeOnApplicationDispatcher(() => { CleanInstance(currentTsunami); }); 
            Debug.WriteInConsole("Finished CleanInstance of currentTsunami.");

            Debug.WriteInConsole("Beginning Load of waitingTsunami.");
            InvokeOnApplicationDispatcher(() => { Tsunami.Load(waitingTsunami); }); 
            Debug.WriteInConsole("Finished Load of waitingTsunami.");

            currentTsunami.SavingFinished -= OpenWaitingInstance;
        }

        private void CleanInstance_After_saving(object sender, EventArgs e)
        {
            Tsunami currentTsunami = sender as Tsunami;
            CleanInstance(currentTsunami);
            currentTsunami.SavingFinished -= CleanInstance_After_saving;
        }

        private void CleanInstance(Tsunami t)
        {
            t.PathOfTheSavedFile = "";
            t.Zone = null;
            t.Elements.Clear();

            foreach (Module item in t.MeasurementModules)
            {
                t.Remove(item);
                item.Dispose();
            }
            t.MeasurementModules.Clear();

            // remove event from instrument module
            foreach (var item in Tsunami2.Preferences.Values.InstrumentModules)
            {
                item.CleanSubscriber();
            }
        }

        internal void Quit()
        {
            Tsunami2.Properties.Quit();
        }

        internal void ShowSplashScreen()
        {
            (this.ParentModule as Tsunami).View.SplashScreenView.Show();
        }

        /// <summary>
        /// Recherche un module non-fini, d'abord dans les modules suivants et ensuite dans les modules précédent pour le mettre comme module actif.
        /// </summary>
        /// <param name="module"></param>
        internal void SetAnotherModuleAsActive(Module module)
        {
            int actualIndex = Tsunami2.Properties.MeasurementModules.FindIndex(x => x._Name == module._Name && x.CreatedOn == module.CreatedOn);
            for (int i = actualIndex + 1; i <= Tsunami2.Properties.MeasurementModules.Count - 1; i++)
            {
                if (!Tsunami2.Properties.MeasurementModules[i].Finished)
                {
                    this._activeMeasurementModule = Tsunami2.Properties.MeasurementModules[i];
                    (this._activeMeasurementModule as FinalModule).SetInstrumentInActiveStationModule();
                    return;
                }
            }
            for (int i = actualIndex - 1; i >= 0; i--)
            {
                if (!Tsunami2.Properties.MeasurementModules[i].Finished)
                {
                    this._activeMeasurementModule = Tsunami2.Properties.MeasurementModules[i];
                    (this._activeMeasurementModule as FinalModule).SetInstrumentInActiveStationModule();
                    return;
                }
            }
            this._activeMeasurementModule = null;

        }
        #endregion

    }
}
