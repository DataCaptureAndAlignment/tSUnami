﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using TSU.Views;
using M = TSU;
using R = TSU.Properties.Resources;


namespace TSU.Functionalities
{
    public partial class MenuView : ModuleView
    {
        private Buttons buttons;
        internal List<BigButton> buttonOpenedModules;
        internal List<BigButton> buttonFinishedModules;
        internal new Menu Module
        {
            get
            {
                return this._Module as Menu;
            }
        }

        public MenuView(M.IModule module) : base(module)
        {
            InitializeComponent();
            this.ApplyThemeColors();
            this.AdaptTopPanelToButtonHeight();
            this.Width = TSU.Tsunami2.Preferences.Theme.ButtonHeight * 5;
            this.Resizable = true;
            this._Name = string.Format("View of '{0}'", module._Name);
            this.FormClosing += new FormClosingEventHandler(OnClosing);
            this.ShowDockedLeft();
            this.CreateAllButtons();
            this.MinimumSize = new Size(TSU.Tsunami2.Preferences.Theme.ButtonHeight + 10, 0); // not less width than the button height
            //this.UpdateView(); not neede because it will be update when added to a parent module?
            
            this._PanelBottom.Click += delegate { ShowRecentModulePopUp(); };
        }


        private void ApplyThemeColors()
        {
            this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
        }
        public void CreateAllButtons()
        {
            buttons = new Buttons(this);
            //Button to Add
            this.CreateTopButton();
            //OpenMduleButton
            this.buttonOpenedModules = new List<BigButton>();
            this.buttonFinishedModules = new List<BigButton>();
            //ModuleButtons
            CreateBuildersButtons(this.Module.guidedBuilders);
            //CreateBuildersButtons(this.Module.guidedTheodoliteBuilders);
            //CreateBuildersButtons(this.Module.guidedElementsAlignmentBuilders);
            CreateBuildersButtons(this.Module.smartBuilders);
            CreateBuildersButtons(this.Module.advancedBuilders);
            CreateBuildersButtons(this.Module.managementBuilders);
            CreateBuildersButtons(this.Module.testBuilders);
            CreateBuildersButtons(this.Module.settingsBuilders);
            //buttonOpenModules.Add(NewBigButton(R.StringMenu_FinishedModule,R.MessageTsu_OK,this.ShowfinishedModuleStrip,TsunamiPreferences.Theme._ColorsColor3));
            AddOpenedAndFinishedModuleButton();

        }

        private void CreateBuildersButtons(List<Builder> list)
        {
            foreach (var item in list)
            {
                BigButton b = item.BuildButton();
                M.Tsunami2.Preferences.Values.GuiPrefs.Buttons.Add(b);
                b.BigButtonClicked += OnButtonCLick;
            }
        }
        private void OnButtonCLick(object source, TsuObjectEventArgs e)
        {
            M.Tsunami2.Preferences.Values.AddButtonToUsedList(source as BigButton);

        }


        internal void ShowRecentModulePopUp()
        {
            if (!((TSU.Tsunami2.Preferences.Tsunami.View._Strategy is TSU.Common.TsunamiView.Strategy.Windows)
                || (TSU.Tsunami2.Preferences.Tsunami.View._Strategy is TSU.Common.TsunamiView.Strategy.Tab)))
            {
                this.ShowDockedFill();
                this.ShowInMessageTsu(R.T460, R.T_OK, null);
            }

            List<Control> cm = new List<Control>();


            cm.Add(new Label() { Text = $"{R.T_RECENT_MODULES}:", ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground, Dock = DockStyle.Top });
            foreach (var item in M.Tsunami2.Preferences.Values.GuiPrefs.LastUsedButtons)
            {
                var copy = item.GetClone();
                cm.Add(copy);
            }

            // add shortcut to last *.tsu
            int count = TSU.Tsunami2.Preferences.Values.GuiPrefs.RecentPaths.Count;
            if (count > 0)
            {
                string path = TSU.Tsunami2.Preferences.Values.GuiPrefs.RecentPaths[count - 1];
                cm.Add(new BigButton(
                    $"{R.T_LAST_USED} '*.tsu';{path}",
                    R.Update, () => { TSU.Tsunami2.Preferences.Tsunami.Menu.OpenExistingModules(path); }, null, false));
            }

            this.ShowPopUpSubMenu(cm, "recent ");



        }

        //  Deal with Main button
        private void CreateTopButton()
        {
            this._PanelTop.Controls.Clear();
            //main.Show();
            this._PanelTop.Controls.Add(buttons.main);
        }

        private void ShowGlobalMenuStrip()
        {
            this.contextButtons.Clear();
            this.contextButtons.Add(buttons.New);
            this.contextButtons.Add(buttons.OpenExisting);
            this.contextButtons.Add(buttons.OpenTemplate);
            this.contextButtons.Add(buttons.Recent);

            this.contextButtons.Add(buttons.SaveMenu);

            this.contextButtons.Add(this.Module.settingsBuilders[0].BuildButton()); // this is the 'setting...'

            this.contextButtons.Add(buttons.Gadgets);
            this.contextButtons.Add(buttons.help);
            this.contextButtons.Add(buttons.about);

            this.contextButtons.Add(buttons.Restart);
            this.contextButtons.Add(buttons.quit);

            this.ShowPopUpMenu(contextButtons);
        }

        public BigButton GetExploreButtons()
        {
            BigButton b = this.Module.settingsBuilders[1].BuildButton();
            return b;
        }

        private void ShowfinishedModuleStrip()
        {
            this.contextButtons.Clear();
            foreach (BigButton button in this.buttonFinishedModules)
            {
                this.contextButtons.Add(button);
            }
            this.ShowPopUpMenu(contextButtons);
        }
        private Image GetGuidedModuleImage(ENUM.GuidedModuleType type)
        {
            switch (type)
            {
                case ENUM.GuidedModuleType.Alignment:
                    return R.Alignment;
                case ENUM.GuidedModuleType.Cheminement:
                    return R.Cheminement;
                case ENUM.GuidedModuleType.Ecartometry:
                    return R.Line;
                case ENUM.GuidedModuleType.Implantation:
                    return R.I_Implantation;
                case ENUM.GuidedModuleType.PointLance:
                    return R.I_PointLancé;
                case ENUM.GuidedModuleType.PreAlignment:
                    return R.PreAlignment;
                case ENUM.GuidedModuleType.Survey1Face:
                    return R.SurveyOneFace;
                case ENUM.GuidedModuleType.Survey2Face:
                    return R.Survey2Face;
                case ENUM.GuidedModuleType.SurveyTilt:
                    return R.Tilt;
                case ENUM.GuidedModuleType.TourDHorizon:
                    return R.TourDHorizon;
                case ENUM.GuidedModuleType.Example:
                case ENUM.GuidedModuleType.AlignmentEcartometry:
                case ENUM.GuidedModuleType.AlignmentImplantation:
                case ENUM.GuidedModuleType.AlignmentLevelling:
                case ENUM.GuidedModuleType.AlignmentLevellingCheck:
                case ENUM.GuidedModuleType.AlignmentManager:
                case ENUM.GuidedModuleType.AlignmentTilt:
                case ENUM.GuidedModuleType.NoType:
                default:
                    return null;
            }
        }
        /// <summary>
        /// Crèes tous les boutons des modules ouverts et les ajoutes aux listes modules ouverts ou fermé
        /// </summary>
        internal void AddOpenedAndFinishedModuleButton()
        {
            buttonFinishedModules.Clear();
            buttonOpenedModules.Clear();
            if (Tsunami2.Properties != null)
            {
                foreach (var item in Tsunami2.Properties.childModules)
                {
                    if (item is Common.FinalModule)
                        AddOpenedAndFinishedModuleButton(item, false);
                }
                this.Module.Tsunami?.View?.Clean();

                this.UpdateView();
            }
        }
        /// <summary>
        /// Crèe un bouton et l'ajoute à la liste des modules finis ou ouverts
        /// </summary>
        /// <param name="module"></param>
        internal void AddOpenedAndFinishedModuleButton(M.Module module, bool updateView = true)
        {
            if (module is Common.FinalModule)
            {
                System.Drawing.Image image = null;
                if (module.View != null)
                {
                    if (module is Common.Guided.Module)
                        module.View.Image = GetGuidedModuleImage((module as Common.Guided.Module).guideModuleType);

                    if (module is Common.Guided.Group.Module)
                        module.View.Image = GetGuidedModuleImage((module as Common.Guided.Group.Module).Type);
                    image = module.View.Image;
                }
                string type = module is Common.Guided.Module || module is Common.Guided.Group.Module ? "GM" : "AM";
                string createdOn = module.CreatedOn.ToString("G", CultureInfo.CreateSpecificCulture("nl-BE"));
                BigButton b = NewBigButton(
                    $"{type}_{module._Name};{R.StringBigButton_CreatedOn}{createdOn}",
                    image,
                    module._TsuView.ShowActiveModule,
                    M.Tsunami2.Preferences.Theme.Colors.Object);
                if (module.Finished)
                    buttonFinishedModules.Add(b);
                else
                    buttonOpenedModules.Add(b);
                if (updateView) this.UpdateView();
            }
        }

        BigButton jiraBug;
        BigButton jiraImp;
        BigButton logHtml;
        BigButton logTsu;

        public override void UpdateView()
        {

            base.UpdateView();

            //CreateTopButton();

            this._PanelBottom.Controls.Clear();

            bool alwaysShowJiraButtons = true;

            if (alwaysShowJiraButtons || Tsunami2.Properties.IsBeta || Tsunami2.Properties.IsAlpha)
            {
                if (jiraBug == null) jiraBug = Tsunami2.Properties.Gadgets.View.buttons.jiraBug.GetClone();
                if (jiraImp == null) jiraImp = Tsunami2.Properties.Gadgets.View.buttons.jiraImprovement.GetClone();
                this._PanelBottom.Controls.Add(Views.TsuView.CreateLayoutForButtons(new List<Control>(){
                   jiraBug,jiraImp}, DockStyle.Bottom, null));
            }


            if (Tsunami2.Preferences.Values.GuiPrefs.ShowLogButtons.IsTrue)
            {
                if (logTsu == null) logTsu = Tsunami2.Properties.Gadgets.View.buttons.log.GetClone();
                if (logHtml == null) logHtml = Tsunami2.Properties.Gadgets.View.buttons.logHtml.GetClone();
                this._PanelBottom.Controls.Add(Views.TsuView.CreateLayoutForButtons(new List<Control>(){
                   logTsu,
                   logHtml}, DockStyle.Bottom, null));
            }

            foreach (BigButton item in buttonOpenedModules)
                this._PanelBottom.Controls.Add(item);

            if (Preferences.Preferences.Instance.GuiPrefs.UserType == Preferences.Preferences.UserTypes.Super) // coreespond to user 
                this._PanelBottom.Controls.Add(buttons.test);

            if (Preferences.Preferences.Instance.GuiPrefs.UserType > 0) // correspond to user and advanced but not guided
            {
                this._PanelBottom.Controls.Add(buttons.manager);
                this._PanelBottom.Controls.Add(buttons.advanced);
            }
            this._PanelBottom.Controls.Add(buttons.guided);
            this._PanelBottom.Controls.Add(buttons.smart);

            if (this.buttonFinishedModules.Count > 0)
                this._PanelBottom.Controls.Add(buttons.finishedModule);

            


            if (this.Module._activeMeasurementModule != null)
            {
                string activeModuleName = "";
                string activeModuleDescription = "";
                if (this.Module._activeMeasurementModule is Common.Guided.Module || this.Module._activeMeasurementModule is Common.Guided.Group.Module)
                {
                    activeModuleName = "GM_" + this.Module._activeMeasurementModule._Name;
                    activeModuleDescription = R.StringBigButton_CreatedOn + this.Module._activeMeasurementModule.CreatedOn.ToString("G", CultureInfo.CreateSpecificCulture("nl-BE"));

                }
                else
                {
                    activeModuleName = "AM_" + this.Module._activeMeasurementModule._Name;
                    activeModuleDescription = R.StringBigButton_CreatedOn + this.Module._activeMeasurementModule.CreatedOn.ToString("G", CultureInfo.CreateSpecificCulture("nl-BE"));
                }
                BigButton buttonToHighlight = this.buttonOpenedModules.Find(x => x.Title == activeModuleName && x.Description == activeModuleDescription);
                if (buttonToHighlight != null) buttonToHighlight.HighLightWithColor();
            }

            infoLabel = new Label() { Dock = DockStyle.Bottom, Visible = true, ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore };
            //this._PanelBottom.Controls.Add(infoLabel);

            int forcedTabIndex = this._PanelBottom.Controls.Count;
            foreach (Control item in this._PanelBottom.Controls)
            {
                item.TabIndex = forcedTabIndex;
                forcedTabIndex--;
            }
        }
        public Label infoLabel;

        public override void SelectNextButton(BigButton current, bool forward)
        {
            Control actual = this.ActiveControl;
            List<BigButton> allButtons = BigButton.GetButtonsFrom(this);

            for (int i = 0; i < allButtons.Count; i++)
            {
                if (allButtons[i] == actual)
                {
                    if (forward)
                    {
                        if (i == 0)
                            i = allButtons.Count - 1;
                        else
                            i--;
                    }
                    else
                    {
                        if (i == allButtons.Count - 1)
                            i = 0;
                        else
                            i++;
                    }
                    allButtons[i].Focus();
                    break;
                }
            }
            infoLabel.Text = this.ActiveControl.ToString();
        }

        public override void SelectOtherView()
        {
            Tsunami2.Properties.View.ActiveControl = Tsunami2.Properties.View.ActiveView;
        }

        private int actualTop = 5;
        private int actualLeft = 5;
        private int verticalSpacing = 4;

        private void ShowSmartMenuStrip()
        {
            this.contextButtons.Clear();
            foreach (var item in this.Module.smartBuilders) this.contextButtons.Add(item.BuildButton());
            this.ShowPopUpMenu(contextButtons);
        }

        private void ShowGuidedMenuStrip()
        {
            this.contextButtons.Clear();
            foreach (var item in this.Module.guidedBuilders)
                this.contextButtons.Add(item.BuildButton());

            //// Theodolite guided button
            //BigButton b = new BigButton(R.T_TheodoliteGuideds, R.Theodolite, TSU.Tsunami2.Preferences.Theme.Colors.Highlight, true);
            //b.BigButtonClicked += delegate
            //{
            //    List<Control> lTGB = new List<Control>();
            //    foreach (var item in this.Module.guidedTheodoliteBuilders)
            //        lTGB.Add(item.BuildButton());
            //    this.ShowPopUpSubMenu(lTGB, "guided ");
            //};
            //b.HasSubButtons = true;
            //this.contextButtons.Add(b);

            //// Element alignmetn modules
            //BigButton c = new BigButton(
            //    "Element alignment;Guided Modules trying to help you the process of an magnet, collimateur, ... alignment", 
            //    R.Element_Aimant,TSU.Tsunami2.TsunamiPreferences.Theme._Colors.Color.Other2, true);
            //c.BigButtonClicked += delegate
            //{
            //    List<Control> lTGB = new List<Control>();
            //    foreach (var item in this.Module.guidedElementsAlignmentBuilders)
            //        lTGB.Add(item.BuildButton());
            //    this.PopUpMenu.ShowSubMenu(lTGB);
            //};
            //c.IsEndContextMenuItem = false;
            //c.HasSubButton = true;
            //this.contextButtons.Add(c);
            this.contextButtons.Reverse();
            this.ShowPopUpMenu(contextButtons);
        }

        private void ShowAdvancedMenuStrip()
        {
            this.contextButtons.Clear();
            foreach (var item in this.Module.advancedBuilders) this.contextButtons.Add(item.BuildButton());
            this.ShowPopUpMenu(contextButtons);
        }
        private void ShowManagerMenuStrip()
        {
            this.contextButtons.Clear();
            foreach (var item in this.Module.managementBuilders) this.contextButtons.Add(item.BuildButton());
            this.ShowPopUpMenu(contextButtons);
        }
        private void ShowTestMenuStrip()
        {
            this.contextButtons.Clear();
            foreach (var item in this.Module.testBuilders) this.contextButtons.Add(item.BuildButton());
            this.ShowPopUpMenu(contextButtons);
        }


        public BigButton NewBigButton(string titleAndDescription, Image image, Action action, Color? color = null)
        {
            //color = (color == null) ? Tsunami2.Preferences.Theme._Colors.ButtonNormal : color;
            BigButton bigButton = new BigButton(titleAndDescription, image, action, color);
            bigButton.Top = actualTop;
            bigButton.Left = actualLeft;
            actualTop += bigButton.Height + verticalSpacing;
            return bigButton;
        }
        private void OnClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }
        private void MenuView_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }

        class Buttons
        {
            private MenuView _view;

            public BigButton main;

            public BigButton advanced;
            public BigButton manager;
            public BigButton guided;
            public BigButton smart;
            public BigButton test;

            public BigButton New;
            public BigButton OpenExisting;
            public BigButton OpenTemplate;
            public BigButton SaveMenu;
            public BigButton Save;
            public BigButton SaveAs;
            public BigButton SaveAsTemplate;
            public BigButton Recent;
            public BigButton Restart;

            public BigButton Export4Geode;
            public BigButton Export4LGC;


            public BigButton finishedModule;

            public BigButton Export;

            // extra
            public BigButton Gadgets;

            public BigButton help;

            public BigButton about;

            public BigButton quit;

            public Buttons(MenuView view)
            {
                _view = view;

                // main
                main = new BigButton(
                    string.Format("{0};{1}", Globals.AppName, R.T_B_Main),
                   Globals.AppImage, _view.ShowGlobalMenuStrip, M.Tsunami2.Preferences.Theme.Colors.Action //TsunamiPreferences.Values.MainColor
                    , true);

                New = new BigButton(
                  $"{R.T_NEW};{R.T_CLEAR_THE_OPENED_MODULE_AND_START_FROM_A_EMPTY_TSUNAMI}",
                  R.Element_File, () => { _view.Module.New(); }, TSU.Tsunami2.Preferences.Theme.Colors.Object, false);
                // sub
                OpenExisting = new BigButton(R.T_OPEN,
                   R.Open, () => { _view.OpenExistingModule(); }, TSU.Tsunami2.Preferences.Theme.Colors.Object, false);

                OpenTemplate = new BigButton(R.T_OPEN_TEMPLATE,
                  R.Open, () => { _view.OpenExistingTemplate(); }, TSU.Tsunami2.Preferences.Theme.Colors.Object, false);

                SaveMenu = new BigButton(
                  $"{R.T_SAVE}...;{R.T_SAVE_THE_ACTUAL_OPENED_MODULES_INTO_A}",
                  R.Save, () => { _view.OpenSaveModule(); ; }, TSU.Tsunami2.Preferences.Theme.Colors.Object, hasSubButtons: true);
                Save = new BigButton(
                   $"{R.T_SAVE};{R.T_SAVE_THE_ACTUAL_OPENED_MODULES_INTO_A}",
                   R.Save, () => { _view.Module.Save(); }, TSU.Tsunami2.Preferences.Theme.Colors.Object, false);
                SaveAs = new BigButton(
                   R.T_SaveModule,
                   R.Save, _view.SaveAs, TSU.Tsunami2.Preferences.Theme.Colors.Object, false);
                SaveAsTemplate = new BigButton(
                   R.T_SaveModule.Replace("As;", "As template;"),
                   R.Save, _view.SaveAsTemplate, TSU.Tsunami2.Preferences.Theme.Colors.Object, false);

                Recent = new BigButton(
                   $"{R.T_RECENT};{R.T_RECENT_DETAILS}",
                   R.OpenRecent, () => { _view.OpenRecentModule(); }, TSU.Tsunami2.Preferences.Theme.Colors.Object, hasSubButtons: true);

                Restart = new BigButton(
                   $"{R.T_RESTART};{R.T_RESTART_DETAILS}",
                   R.Update, _view.Restart, TSU.Tsunami2.Preferences.Theme.Colors.Object, false);



                Export = new BigButton(
                   R.T_B_MeasureExport,
                   R.Export, () =>
                   {
                       (Export.ParentForm as TsuPopUpMenu).ShowSubMenu(new List<Control> { Export4Geode, Export4LGC }, "Export");
                   }
                   , TSU.Tsunami2.Preferences.Theme.Colors.Highlight, true);

                Export4Geode = new BigButton(
                   R.T_B_MeasureExport2Geode,
                   R.Geode, _view.Export4Geode, TSU.Tsunami2.Preferences.Theme.Colors.Object, false);
                Export4LGC = new BigButton(
                   R.T_B_MeasureExportLGC2,
                   R.Lgc, _view.Export4LGC, TSU.Tsunami2.Preferences.Theme.Colors.Object, false);


                advanced = new BigButton(
                   R.T_B_AddAdvancedModule,
                   R.AdvancedModule, _view.ShowAdvancedMenuStrip, TSU.Tsunami2.Preferences.Theme.Colors.Choice, hasSubButtons: true);
                manager = new BigButton(
                   R.T_B_AddManagerModule,
                   R.ManagerModule, _view.ShowManagerMenuStrip, TSU.Tsunami2.Preferences.Theme.Colors.Choice, hasSubButtons: true);
                guided = new BigButton(
                   R.T_B_AddGuidedModule,
                   R.GuidedModule, _view.ShowGuidedMenuStrip, TSU.Tsunami2.Preferences.Theme.Colors.Choice, hasSubButtons: true);
                smart = new BigButton(
                   R.T_B_AddSmartModule,
                   R.SmartModule, _view.ShowSmartMenuStrip, TSU.Tsunami2.Preferences.Theme.Colors.Choice, hasSubButtons: true);
                string sTest = R.T_B_AddTestModule;
                test = new BigButton(
                   R.T_B_AddTestModule,
                   R.ModuleTest, _view.ShowTestMenuStrip, TSU.Tsunami2.Preferences.Theme.Colors.Choice, hasSubButtons: true);

                finishedModule = new BigButton(
                    R.StringMenu_FinishedModule,
                    R.MessageTsu_OK, _view.ShowfinishedModuleStrip, TSU.Tsunami2.Preferences.Theme.Colors.Highlight, true);
                finishedModule.Dock = DockStyle.Bottom;

                //Links = new BigButton(
                //   $"{R.T_LINKS};{R.T_HELP_JIRA_DATA_PREFERENCES}",
                //   R.Add, ()=>
                //   {
                //       (Export.ParentForm as TsuPopUpMenu).ShowSubMenu(new List<Control> { help, jira, jiraImprovement, jiraBug});
                //   },
                //  TSU.Tsunami2.TsunamiPreferences.Theme._Colors.Color.Other2,hasSubButtons:true);

                // Extra Tools
                Gadgets = new BigButton(
                   $"{R.T_SMALL_TOOLS};{R.T_EXTRA_DETAILS}",
                   R.Gadget, () =>
                   {
                       Tsunami2.Properties.Add(Tsunami2.Properties.Gadgets);
                   },
                   null, hasSubButtons: false);

                help = new BigButton(
                   R.T_HELP,
                   R.StatusQuestionnable, TSU.Common.Tsunami.ShowHelp);

                quit = new BigButton(
                   R.T_B_Quit,
                   R.MessageTsu_Problem, _view.Module.Quit, TSU.Tsunami2.Preferences.Theme.Colors.Bad);
            }
        }

        private void SaveAsTemplate()
        {
            Tsunami2.Properties.SaveAsTemplate();
        }

        private void OpenSaveModule()
        {
            List<Control> cm = new List<Control>
            {
                buttons.Save,
                buttons.SaveAs,
                buttons.SaveAsTemplate
            };

            this.ShowPopUpSubMenu(cm, "Save Menu");
        }

        private void OpenRecentModule()
        {
            List<Control> cm = new List<Control>();

            foreach (var item in Enumerable.Reverse(M.Tsunami2.Preferences.Values.GuiPrefs.RecentPaths))
            {
                if (System.IO.File.Exists(item))
                {
                    var info = new FileInfo(item);
                    var button = new BigButton($"{info.Name};{item}", R.Element_File, () => OpenExistingModule(item));
                    if (info.Extension.ToUpper() == ".TSUT")
                        button.SetColors(Tsunami2.Preferences.Theme.Colors.Action);
                    cm.Add(button);
                }
            }

            this.ShowPopUpSubMenu(cm, "Recent ");
        }



        private void Restart()
        {
            this.Module.Tsunami.Restart();
        }



        private void Export4Geode()
        {
            Tsunami2.Properties.Export(M.IO.Types.Geode, "");
        }

        private void Export4LGC()
        {
            string fileName = Tsunami2.Properties.View.GetSavingName(M.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay,
                System.IO.Path.GetFileNameWithoutExtension(Tsunami2.Properties.PathOfTheSavedFile), "INP (*.inp)|*.inp|LGC (*.lgc)|*.lgc");
            if (fileName != "")
                Tsunami2.Properties.Export(M.IO.Types.LGC2, fileName);
        }

        private void SaveAs()
        {
            Tsunami2.Properties.SaveAs();
        }

        internal void OpenExistingTemplate(string fileName = "")
        {
            string folder = "";
            if (fileName == "")
                folder = Tsunami2.Preferences.Values.Paths.Templates;
            OpenExistingModule(fileName, "TSU template (*.tsut)|*.tsut|Existing TSU (*.tsu)|*.tsu|Other (*.*)|*.*", folder: folder);
        }

        internal void OpenExistingModule(string fileName = "", string defaultFilter = "Existing TSU (*.tsu)|*.tsu|TSU template (*.tsut)|*.tsut|Other (*.*)|*.*", string folder = "")
        {
            try
            {
                if (folder == "")
                    folder = M.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay;
                if (fileName == "")
                    fileName = M.TsuPath.GetFileNameToOpen(this,  folder, "", defaultFilter, R.T_BROWSE_XML);
                if (fileName == "") return;

                // add to last position in recent list
                {
                    M.Tsunami2.Preferences.Values.GuiPrefs.RecentPaths.Remove(fileName);
                    M.Tsunami2.Preferences.Values.GuiPrefs.RecentPaths.Add(fileName);
                }

                TSU.Views.Message.RaisedEventMessage message = new TSU.Views.Message.RaisedEventMessage();
                message.ParentView = Tsunami2.Properties.View;
                message.coveringMessage = true;
                Tsunami2.Properties.SubModuleAdding += message.OnSubModuleAdding;
                //message.Show();
                message.SetTitleAndMessage($"{R.T_OPENING_EXISTING_MODULES};{R.T_STARTING}", "G", out System.Drawing.Size dummy);
                message.Refresh();


                this.Module.OpenExistingModules(fileName);
                Tsunami2.Properties.SubModuleAdding -= message.OnSubModuleAdding;
                message.Hide();
                message.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception($"Error Opening '{fileName}'", ex);
            }
        }

        private void MenuView_Load(object sender, EventArgs e)
        {
            // using reflection to allow double buffereing to sow reduce the flickering
            typeof(Panel).InvokeMember("DoubleBuffered",
            BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
            null, _PanelBottom, new object[] { true });
        }


        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);
        }
    }
}
