﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Drawing;
using R = TSU.Properties.Resources;
using TSU.ENUM;
using TSU.Common.Elements;
using D = TSU.Common.Instruments.Device;
using TSU.Common.Instruments;
using TSU.Common.Zones;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using System.Xml;
using System.Threading;
using TSU;
using AD = TSU.Common.Dependencies;
using P = TSU.Preferences;
using System.Xml.Linq;
using TSU.Common;

namespace TSU.Splash
{
    public class Module: TSU.Module
    {

        public new ScreenView View
        {
            get
            {
                return this._TsuView as ScreenView;
            }
            set
            {
                this._TsuView = value;
            }
        }
        #region Singleton Tools
        private static Module instance;

        public Module():base()
        {

        }

        public Module(TSU.Module parentModule)
            : base(parentModule)
        {
            

        }

        public static Module Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Module(null);


                }
                return instance;
            }
        }
        #endregion



        #region Fields
        
        internal Common.Tsunami.VersionTypes VersionType = Common.Tsunami.VersionTypes.Normal;
        internal string version;
        internal string date;
        #endregion

        #region Initialisation

        public override void Initialize()
        {
            this._Name =R.T468;
            var lnkPath = Tsunami2.Preferences.Values.Paths.Links;
            if (Directory.Exists(lnkPath))
                Directory.Delete(lnkPath, recursive:true);
            base.Initialize();
        }

        #endregion

        internal void ReadVersionFile()
        {
            XDocument doc = XDocument.Load(Tsunami2.Preferences.Values.Paths.Dependencies);
            var version = doc.Root.Attribute("V").Value;
            var date = doc.Root.Attribute("StringDate").Value;
            this.version = version;
            this.date = date;

            if (version.ToUpper().Contains("BETA"))
                this.VersionType = Common.Tsunami.VersionTypes.Beta;
            else
            {
                if (version.ToUpper().Contains("ALPHA"))
                    this.VersionType = Common.Tsunami.VersionTypes.Alpha;
                else
                {
                    if (version.ToUpper().Contains("GAMMA"))
                        this.VersionType = Common.Tsunami.VersionTypes.Gamma;
                    else
                        this.VersionType = Common.Tsunami.VersionTypes.Normal;
                }

            }

        }

        private void CleanTemporaryDirectories(int numberOfFileToKeepInEachFolder)
        {
            string temporaryFolder = P.Preferences.Instance.Paths.Temporary;

            if (!File.Exists(temporaryFolder)) Directory.CreateDirectory(temporaryFolder);

            foreach (string dirName in Directory.GetDirectories(temporaryFolder))
            {
                int totalNumberOfFile = Directory.GetFiles(dirName).Count();
                FileInfo[] filesToDelete = Directory.GetFiles(dirName)
                                            .Select(x => new FileInfo(x))
                                            .OrderBy(x => x.LastAccessTime)
                                            .Take(totalNumberOfFile - numberOfFileToKeepInEachFolder)
                                            .ToArray();

                foreach (var file in filesToDelete)
                {
                    file.Delete();
                }
            }

        }

        internal void UpdateVersion()
        {
            if (this.ParentModule is Tsunami t)
            {
                t.Date = this.date;
                t.Version = this.version;
            }
        }
    }
}
