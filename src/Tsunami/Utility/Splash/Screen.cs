﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TSU.Common;
using TSU.Views;
using TSU.Views.Message;
using AD = TSU.Common.Dependencies;
using D = TSU.Functionalities.ManagementModules;
using M = TSU;
using P = TSU.Preferences;
using R = TSU.Properties.Resources;
using System.Runtime.InteropServices;
using GeoComClient;


namespace TSU.Splash
{

    public partial class ScreenView : TsuView
    {

        #region Fields
        public const FormWindowState LAUCHING_STATE_OF_MAIN_WINDOW = FormWindowState.Maximized;
        [DllImport("user32.dll")]
        private static extern int GetDpiForSystem();
        private Logs.LogView logView;

        internal Common.Tsunami tsunami
        {
            get
            {
                return this.Module.ParentModule as Tsunami;
            }
        }
        bool busy;

        bool stoppedByAUserClick;
        public bool problemFound;
        bool problemFoundIndependencies;
        bool problemFoundInInitialisation;
        bool LoadingPrefFinished;
        bool LoadingDepFinished;

        private Module Module
        {
            get
            {
                return this._Module as Module;
            }
            set
            {
                this._Module = value;
            }
        }

        #endregion

        #region Construction

        public ScreenView(M.Module parentModule) : base(parentModule)
        {
            // The SOFTWARE
            Globals.AppName = "The SOFTWARE";
            Globals.AppNameDescription = "Formerly known as Tsunami";
            Globals.AppColor = Color.FromArgb(255, 255, 192, 0);
            Globals.AppImage = R.Tsunami;

            // C4ALM
            Globals.AppName = "C4ALM";
            Globals.AppNameDescription = "Le calme après la tempête, Compagnion 4 Alignments and Measurements";
            Globals.AppColor = Color.FromArgb(255, 100, 255, 200);
            Globals.AppImage = R.calme;

            // TSUNAMI
            Globals.AppName = "TSUNAMI";
            Globals.AppNameDescription = R.T458;
            Globals.AppColor = Color.FromArgb(255, 255, 192, 0);
            Globals.AppImage = R.Tsunami;

            // Load file paths, create diretories and log files
            M.Tsunami2.Preferences.Values.LoadFilePaths();

            // Read Tsunami version
            this.Module.ReadVersionFile();

            InitializeComponent();
            this.SuspendLayout();

            this.VersionLabel.Text = $"{Globals.AppName} {this.Module.version}";
            this.VersionLabel.ForeColor = Color.Black;

            switch (this.Module.VersionType)
            {
                case Common.Tsunami.VersionTypes.Beta:
                    this.Switch2Tiramisu();
                    break;
                case Common.Tsunami.VersionTypes.Alpha:
                    this.Switch2Alpha();
                    break;
                default:
                case Common.Tsunami.VersionTypes.Gamma:
                    this.Switch2Gamma();
                    break;
                case Common.Tsunami.VersionTypes.Normal:
                    break;
            }




            // Init
            SetColor(Globals.AppColor);

            LoadingPrefFinished = false;
            this.AllowMovement(this.pictureBox1);
            this.AllowMovement(this.splitContainer1.Panel1);
            this.AllowMovement(this);

            stoppedByAUserClick = false;
            this._Module._TsuView = this;

            this.pictureBox1.Image = Globals.AppImage;

            // Setup Log
            this.SetupLog();

            if (Debug.Debugging || Globals.IsBeta)
                Debug.StartTimerToLogRessourcesState();

            this.Shown += delegate
            {
                // Timer
                loadingTimer = new Timer() { Interval = 10, Enabled = true };
                loadingTimer.Tick += new EventHandler(LoadingTimer_Tick);
            };


            this.ResumeLayout(true);

        }

        void SetColor(Color color)
        {
            this.BackColor = color;
            this.splitContainer1.BackColor = color;
            this.splitContainer1.Panel1.BackColor = color;
            this.splitContainer1.Panel2.BackColor = color;
            this.pictureBox1.BackColor = color;
            this.VersionLabel.BackColor = color;
        }

        internal void Switch2Tiramisu()
        {
            // Tiramisu
            Globals.AppColor = Color.LightGoldenrodYellow;
            Globals.AppName = "TIRAMISU";
            Globals.AppImage = R.Tiramisu;
            Globals.IsBeta = true;
            this.VersionLabel.ForeColor = Color.Orange;
        }

        internal void Switch2Alpha()
        {
            // Alpha
            Globals.AppColor = Color.FromArgb(50, 50, 50);
            Globals.AppName = "T5UN4M1";
            Globals.IsBeta = true;
            Globals.AppImage = R.Tsunami_Alpha;
            this.VersionLabel.ForeColor = Color.Red;
        }

        internal void Switch2Gamma()
        {
            // Alpha
            Globals.AppColor = Color.FromArgb(100, 50, 50);
            Globals.AppName = "TIRAMISU second baking";
            Globals.IsBeta = true;
            Globals.AppImage = R.Tiramisu;
            this.VersionLabel.ForeColor = Color.Brown;
        }

        private void Exit()
        {
            this.tsunami.Quit(true);
        }

        private void SetupLog()
        {
            logView = new Logs.LogView();

            P.GUI.Theme.ThemeColors colors = Tsunami2.Preferences.Theme.Colors;

            logView.SetColors(beginningOf: colors.NormalFore,
                              logBack: Globals.AppColor,
                              dateAndTime: colors.NormalFore,
                              success: Color.Green,
                              finishOf: Color.Green,
                              payAttentionTo: Color.OrangeRed,
                              fYI: Color.Gray);
            logView.BringToFront();
            logView.Visible = true;

            this.splitContainer1.Panel2.Controls.Add(logView);
        }

        bool readyToAdapt = false;
        private int countOfAdaptation = 5;

        private void AdaptSize()
        {
            if (!readyToAdapt) return;
            countOfAdaptation++;
            if (countOfAdaptation > 3)
            {
                Size preferedSize = this.logView.GetPreferedSize;
                int preferedWidth = preferedSize.Width + 10;
                int preferedHeight = preferedSize.Height + this.splitContainer1.SplitterDistance + 60;

                if (this.Width <= preferedWidth)
                {
                    this.Width = preferedWidth;
                }

                if (this.Height <= preferedHeight)
                {
                    this.Height = preferedHeight;
                }

                countOfAdaptation = 0;

                Rectangle r = System.Windows.Forms.Screen.FromControl(this).Bounds;
                this.Top = (r.Height - this.Height) / 2;
                this.Left = (r.Width - this.Width) / 2;
            }
        }

        #endregion

        #region Actions

        private void TryAction(Action action, string SuccessDescripton = null, bool useNumber = false)
        {
            if (SuccessDescripton == null)
                SuccessDescripton = R.T484;
            busy = true;
            try
            {
                action();
                if (useNumber)
                    SuccessDescripton = tsunami.tempNumberOfLoadedItem + " " + SuccessDescripton;
                Logs.Log.AddEntryAsSuccess(null, SuccessDescripton);
            }
            catch (WarningException ex)
            {
                busy = false;
                stoppedByAUserClick = true;
                Logs.Log.AddEntryAsPayAttentionOf(null, ex.Message);
            }
            catch (Exception ex)
            {
                busy = false;
                problemFound = true;
                stoppedByAUserClick = true;
                Logs.Log.AddEntryAsError(null, ex.Message);
            }
        }

        private void SplashScreen_Click(object sender, EventArgs e)
        {
            stoppedByAUserClick = true;
            if (LoadingPrefFinished && LoadingDepFinished && !problemFound)
            {
                if (tsunami.Menu != null) // this is not the first showing of the spllash screen
                    CloseSplashScreen();
                else
                {
                    Continue(this, null);
                }

            }
        }

        private void SetupDependencieManagerAndContinue(object source, TsuObjectEventArgs e)
        {
            CloseSplashScreen();
            StartTsunami();

            // Start dependencie manager
            D.Dependencies dep = this.tsunami.Menu.managementBuilders.OfType<D.Dependencies>().FirstOrDefault();
            if (dep != null)
            {
                dep.Start();
                new MessageInput(MessageType.GoodNews, R.T_M_DEP_START)
                {
                    ButtonTexts = new List<string> { R.T_THANKS }
                }.Show();
            }
        }

        public void Continue(object sender, EventArgs e)
        {
            if (Debug.Debugging)
            {
                tsunami.Test_with_preferences();
                if (AcceptBetaButton != null) // just to avoid double menu attempt if we click the beta button in debug mode
                    AcceptBetaButton.Enabled = false;
            }

            if (!problemFound && !stoppedByAUserClick)
                KeepSplashScreenAbitLonger();
            else
                CloseSplashScreen();

            StartTsunami();
        }


        private void StartTsunami()
        {
            P.Preferences.SwitchUserBaseOnBetaOrNot();

            Tools.Access.IsAdministrator();

            tsunami.View.WindowState = LAUCHING_STATE_OF_MAIN_WINDOW;
            tsunami.View.UpdateTextTitle();

            // menu
            tsunami.CreateMenu();

            // No longer doing this here, because we need the log to start sooner
            // CreateFolders();

            tsunami.View.Show();

            tsunami.View.SplashScreenView = this;
            M.Tsunami2.Preferences.Values.SwitchLanguage(M.Tsunami2.Preferences.Values.GuiPrefs.Language);

            tsunami._TsuView.UpdateView();

            TSU.Debug.TestAfterStart();

            TSU.Tools.Arguments.Handle();

        }

        private void KeepSplashScreenAbitLonger()
        {
            this.TopMost = true;
            Timer t = new Timer()
            {
                Enabled = true,

            };

            if (!stoppedByAUserClick)
                t.Interval = 800;
            else
                t.Interval = 1;

            t.Tick += delegate
            {
                t.Stop();
                CloseSplashScreen();
                if (t != null)
                {
                    t.Stop();
                    t.Dispose();
                    t = null;
                }
            };
        }




        private void CloseSplashScreen()
        {
            this.loadingTimer.Stop();
            this.dependencyTimer.Stop();
            this.Hide();

            var prefs = Tsunami2.Preferences.Values.GuiPrefs;

            if (tsunami.IsBeta)
                prefs.MakeVideoFromScreenshots = new TsuBool(true);

            if (!Debug.IsRunningInATest)
            {
                var r = new MessageInput(MessageType.Critical,
                    "Attention please;" +
                    "A video of your activity during the use of Tsunami will be recorded.\r\n" +
                    "The video includes interactions with other software.\r\n" +
                    "The video stays locally on your computer.\r\n" +
                    "The video is for debugging purpose. It can be usefull to link it to Jira tickets.\r\n"
                    )
                {
                    ButtonTexts = new List<string> { R.T_OK, "STOP IT!" },
                    AsNotification = true

                }.Show();

                if (r.TextOfButtonClicked != R.T_OK)
                {
                    prefs.MakeVideoFromScreenshots.Set(false);
                    prefs.MakeScreenshotAtEveryMouseClick.Set(false);
                }
                else
                {
                    prefs.MakeVideoFromScreenshots.Set(true);
                    prefs.MakeScreenshotAtEveryMouseClick.Set(true);
                }
            }

            if (prefs.MakeScreenshotAtEveryMouseClick.IsTrue || prefs.MakeVideoFromScreenshots.IsTrue)
                TSU.Debug.MouseAndKeyboardMonitoring_Subscribe();
        }

        private void WarnSfbUsers()
        {
            if (System.Diagnostics.Debugger.IsAttached) return;
            bool sfbInstall = false;
            if (System.IO.Directory.Exists(@"C:\Program Files (x86)\SuSoft\SFB\9.01.04")) sfbInstall = true;
            if (System.IO.Directory.Exists(@"C:\Program Files\SuSoft\SFB\9.01.04")) sfbInstall = true;

            if (sfbInstall)
            {
                string doIt = R.T_INSTALL_SFB;
                string titleAndMessage = $"{R.T_YOU_HAVE_SFB_INSTALLED_ON_YOUR_COMPUTER};" +
                                         $"{R.T_WHEN_YOU_INSTALL_CARNET4000}\r\n" +
                                         $"{R.T_IT_COULD_MAKE_CONFLICT}\r\n" +
                                         $"{R.T_DO_YOU_WANT_SFB}.\r\n\r\n" +
                                         $"If you experience some strange unexpected resolution change during the use of Tsunami, it is probably because of your Windows resolution setting set to '125%'.\r\n" +
                                         $"Unfortunately you have to use 100% ou 150% to avoid this problem for the moment. Sorry about that.";
                MessageInput mi = new MessageInput(MessageType.Critical, titleAndMessage)
                {
                    ButtonTexts = new List<string> { doIt, R.T_CONTINUE_WITH_TSUNAMI }
                };
                string r = mi.Show().TextOfButtonClicked;

                if (r == doIt)
                {
                    var _assembly = System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase;
                    var _path = System.IO.Path.GetDirectoryName(_assembly);
                    Shell.Run(_path + "\\setupSFB9.01.04.exe");
                    this.Exit();
                }
            }
        }

        #region Preferences Loading

        /// <summary>
        /// Enumeration of all steps, in the order we want them to execute.
        /// Don't add anything after Done as it won't be executed
        /// </summary>
        private enum LoadingTimerStep
        {
            LoadGuiPref,
            UpdateVersion,
            CheckDecimalSeparator,
            T486,
            T487,
            LoadZones,
            LoadTolerances,
            LoadInstrumentTolerances,
            LoadInstrumentsTcpIpParameters,
            LoadOperationNumber,
            LoadInstrumentList,
            LoadCalibrationCoefficientSetsForDistanceCorrections,
            DLLCheckPresence,
            LoadInstrumentPrecisions,
            LoadSocketCodes,
            WindowsScalingCheck,
            CleanTemporaryDirectories,
            T498,
            GetDependenciesFromXml,
            AddButton,
            Done,
        }

        private Timer loadingTimer;
        private LoadingTimerStep loadingTimerStep;

        async void LoadingTimer_Tick(object sender, EventArgs e)
        {
            if (busy) return;

            this.AdaptSize();

            switch (loadingTimerStep)
            {
                case LoadingTimerStep.LoadGuiPref:
                    try
                    {
                        busy = true;
                        TSU.Preferences.GUI.Global.GuiPrefFileExist.Reset();
                        string filePath = Tsunami2.Preferences.Values.Paths.GuiPreferences;
                        TSU.Preferences.GUI.Global.GuiPrefFileExist.WaitOne(20000);
                        M.Tsunami2.Preferences.Values.LoadGuiPref(filePath);
                        busy = false;
                        Logs.Log.AddEntryAsSuccess(this.Module, $"{R.T_LOAD_GUIPREF} from {MessageTsu.GetLink(M.Tsunami2.Preferences.Values.Paths.GuiPreferences)}");
                    }
                    catch (Exception ex)
                    {
                        Logs.Log.AddEntryAsError(this.Module, $"{R.T_LOAD_GUIPREF} from {MessageTsu.GetLink(M.Tsunami2.Preferences.Values.Paths.GuiPreferences)}");
                        busy = false;
                    }
                    break;

                case LoadingTimerStep.UpdateVersion:
                    TryAction(
                        this.Module.UpdateVersion,
                        $"{R.T485} {Globals.AppName} version {this.Module.version} from {this.Module.date}"
                    );
                    busy = false;
                    break;

                case LoadingTimerStep.CheckDecimalSeparator:
                    TryAction(
                        M.Tsunami2.Preferences.Values.CheckDecimalSeparator,
                        ""
                    );
                    busy = false;
                    break;

                case LoadingTimerStep.T486:
                    Logs.Log.AddEntryAsFinishOf(null, R.T486);
                    readyToAdapt = true;
                    break;

                case LoadingTimerStep.T487:
                    Logs.Log.AddEntryAsBeginningOf(null, R.T487);
                    break;

                case LoadingTimerStep.LoadZones:
                    TryAction(
                        M.Tsunami2.Preferences.Values.LoadZones,
                        $"{R.T490} from {MessageTsu.GetLink(M.Tsunami2.Preferences.Values.Paths.Zones)}",
                        useNumber: true
                    );
                    busy = false;
                    break;

                case LoadingTimerStep.LoadInstrumentTolerances:
                    TryAction(
                        M.Tsunami2.Preferences.Values.LoadInstrumentsTolerances,
                        $"{R.T_LOADING_TOL} from {MessageTsu.GetLink(M.Tsunami2.Preferences.Values.Paths.InstrumentTolerances)}",
                        useNumber: true
                    );
                    busy = false;
                    break;

                case LoadingTimerStep.LoadTolerances:
                    TryAction(
                        M.Tsunami2.Preferences.Values.LoadTolerances,
                        $"{R.T_LOADING_TOL} from {MessageTsu.GetLink(M.Tsunami2.Preferences.Values.Paths.Tolerances)}",
                        useNumber: true
                    );
                    busy = false;
                    break;

                case LoadingTimerStep.LoadInstrumentsTcpIpParameters:
                    TryAction(
                        M.Tsunami2.Preferences.Values.LoadInstrumentsTcpIpParameters,
                        $"{R.T493} from {MessageTsu.GetLink(M.Tsunami2.Preferences.Values.Paths.At40xIp)}",
                        useNumber: true
                    );
                    busy = false;
                    break;

                case LoadingTimerStep.LoadOperationNumber:
                    TryAction(
                        M.Tsunami2.Preferences.Values.LoadOperationNumber,
                        $"{R.T494} from {MessageTsu.GetLink(M.Tsunami2.Preferences.Values.Paths.Operations)}",
                        useNumber: true
                    );
                    busy = false;
                    break;

                case LoadingTimerStep.LoadInstrumentList:
                    TryAction(
                        M.Tsunami2.Preferences.Values.LoadInstrumentList,
                        $"{R.T495} from {MessageTsu.GetLink(M.Tsunami2.Preferences.Values.Paths.Instruments)}",
                        useNumber: true
                    );
                    busy = false;
                    break;

                case LoadingTimerStep.LoadCalibrationCoefficientSetsForDistanceCorrections:
                    TryAction(
                        () => { M.Tsunami2.Preferences.Values.LoadCalibrationCoefficientSetsForDistanceCorrections(); },
                        $"{R.T496} from {MessageTsu.GetLink(M.Tsunami2.Preferences.Values.Paths.Etalonnages)}",
                        useNumber: true
                    );
                    busy = false;
                    break;

                case LoadingTimerStep.DLLCheckPresence:
                    TryAction(
                        M.Tsunami2.Preferences.Values.DLLCheckPresence,
                        R.T_DLL_PRESENCE_CHECKED
                    );
                    busy = false;
                    break;

                case LoadingTimerStep.LoadInstrumentPrecisions:
                    TryAction(
                        M.Tsunami2.Preferences.Values.LoadInstrumentPrecisions,
                        $"{R.T_LS_Precisions} from {MessageTsu.GetLink(M.Tsunami2.Preferences.Values.Paths.InstrumentSlashTargetCoupleSigmas)}",
                        useNumber: true
                    );
                    busy = false;
                    break;

                case LoadingTimerStep.LoadSocketCodes:
                    TryAction(
                        M.Tsunami2.Preferences.Values.LoadSocketCodes,
                        $"{R.T_LS_SocketCodes} from {MessageTsu.GetLink(M.Tsunami2.Preferences.Values.Paths.SocketCodes)}",
                        useNumber: true
                    );
                    busy = false;
                    break;

                case LoadingTimerStep.WindowsScalingCheck:
                    TryAction(
                        checkWindowsScaling,
                        R.T_WINDOWS_SCALE_CORRECT,
                        useNumber: false
                        );
                    busy = false;
                    break;

                case LoadingTimerStep.CleanTemporaryDirectories:
                    TryAction(
                        M.Tsunami2.Preferences.Values.CleanTemporaryDirectories,
                        R.T489
                    );
                    busy = false;
                    break;

                case LoadingTimerStep.T498:
                    Logs.Log.AddEntryAsBeginningOf(null, R.T498);
                    break;

                case LoadingTimerStep.GetDependenciesFromXml:
                    TryAction(
                        GetDependenciesFromXml,
                        $"{R.T_READING_TSUNAMI_DEPENDENCIES_XML_FILE} from {MessageTsu.GetLink(M.Tsunami2.Preferences.Values.Paths.Dependencies)}"
                    );
                    break;

                case LoadingTimerStep.AddButton:
                    if (problemFound)
                    {
                        SetColor(Color.Red);
                        AddSolutionButton();
                        problemFoundInInitialisation = true;
                    }
                    else
                    {
                        if (this.Module.VersionType != Common.Tsunami.VersionTypes.Normal)
                        {
                            AddAcceptBetaButton();
                        }
                    }
                    break;

                default:
                    this.LoadingPrefFinished = true;
                    loadingTimer.Stop();
                    CheckForProblems();
                    break;
            }

            loadingTimerStep++;

            this.UpdateView();
        }

        private void checkWindowsScaling()
        {
            int dpi = GetDpiForSystem();

            // 96 DPI is standard for 100% scaling.
            float scaleFactor = dpi / 96.0f;

            if (scaleFactor != 1.0f)
            {
                throw new WarningException(R.T_WINDOWS_SCALE_INCORRECT);
            }
        }
        private void CheckForProblems()
        {
            if (!this.problemFound && !this.problemFoundIndependencies && !this.problemFoundInInitialisation && !stoppedByAUserClick || TSU.Tools.Arguments.ContainsMacro)
            {
                if (Debug.Debugging || tsunami.IsRunningForFunctionalTest)
                {
                    Continue(this, null);
                }
                else if (this.Module.VersionType == Common.Tsunami.VersionTypes.Normal)
                {
                    Continue(this, null);
                }
            }
            else
            {
                this.labelClose.Visible = true; 
            }
            
        }

        private void SplashScreen_ResizeBegin(object sender, EventArgs e)
        {
            this.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.pictureBox1.SuspendLayout();
        }

        private void SplashScreen_ResizeEnd(object sender, EventArgs e)
        {
            this.pictureBox1.ResumeLayout(true);
            this.splitContainer1.Panel1.ResumeLayout(true);
            this.splitContainer1.ResumeLayout(true);
            this.ResumeLayout(true);
        }

        BigButton AcceptBetaButton;
        private void AddAcceptBetaButton()
        {
            string text;
            Bitmap bitmap = R.Tsunami;
            switch (this.Module.VersionType)
            {
                case Common.Tsunami.VersionTypes.Beta:
                case Common.Tsunami.VersionTypes.Alpha:
                case Common.Tsunami.VersionTypes.Gamma:
                    text = $"{this.Module.VersionType} VERSION; {R.T_I_UNDERSTAND} {R.T_THAT} {Globals.AppName} {R.T_IS} {this.Module.VersionType} VERSION {R.T_OF} {Globals.AppName} {R.T_AND} {R.T_SHOULD_BE_USE_FOR_TEST_PURPOSE_ONLY}";
                    break;
                case Common.Tsunami.VersionTypes.Normal:
                default:
                    text = "";
                    break;
            }

            AcceptBetaButton = new BigButton(text,
                bitmap, Continue, null, TSU.Tsunami2.Preferences.Theme.Colors.Bad, false);
            AcceptBetaButton.AddAction(() => { AcceptBetaButton.Enabled = false; });
            this.Height += AcceptBetaButton.Height + 5;
            AcceptBetaButton.Dock = DockStyle.Bottom;
            this.Controls.Add(AcceptBetaButton);
        }


        private void AddSolutionButton()
        {
            BigButton b = new BigButton(R.T_B_SC_ACTION,
                R.Tsunami, delegate { ShowPopUpSubMenu(GetSolutionButtons(), "splash "); }, null, TSU.Tsunami2.Preferences.Theme.Colors.Bad, true);
            this.Height += b.Height + 5;
            b.Dock = DockStyle.Bottom;
            this.Controls.Add(b);
            b.BringToFront();
            this.Height += b.Height;
            this.logView.BringToFront();
        }


        public List<Control> GetSolutionButtons()
        {
            List<Control> controls = new List<Control>();

            if (problemFoundIndependencies)
            {
                BigButton i = new BigButton(
                      R.T_B_Splash_InstalDep,
                      R.Susoft,
                       SetupDependencieManagerAndContinue, null, TSU.Tsunami2.Preferences.Theme.Colors.Good);
                i.Width += 100;
                controls.Add(i);
            }

            if (problemFound)
            {
                BigButton restart = new BigButton(
                    "Restart",
                    R.Update,
                     () =>
                     {
                         this.tsunami.Restart();
                     }, TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                restart.Width += 100;
                controls.Add(restart);
            }

            if (problemFound)
            {
                BigButton e = new BigButton(
                    R.T_B_Quit,
                    R.StatusBad,
                     Exit, TSU.Tsunami2.Preferences.Theme.Colors.Bad);
                e.Width += 100;
                controls.Add(e);
            }

            if (problemFound)
            {
                string text = R.T_B_Splash_Err_Cont;
                Bitmap bitmap = R.Tsunami;
                switch (this.Module.VersionType)
                {
                    case Common.Tsunami.VersionTypes.Beta:
                    case Common.Tsunami.VersionTypes.Alpha:
                    case Common.Tsunami.VersionTypes.Gamma:
                        text += $" {R.T_I_UNDERSTAND} {R.T_THAT} {Globals.AppName} {R.T_IS} {this.Module.VersionType} VERSION {R.T_OF} {Globals.AppName} {R.T_AND} {R.T_SHOULD_BE_USE_FOR_TEST_PURPOSE_ONLY}";
                        break;
                    case Common.Tsunami.VersionTypes.Normal:
                    default:
                        break;
                }
                BigButton c = new BigButton(
                    text,
                    bitmap,
                     Continue, null, TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                c.Width += 100;
                controls.Add(c);
            }

            if (problemFound)
            {
                string text = "Open preferences folders;To help you add some missing files";
                BigButton c = new BigButton(
                    text,
                    R.Element_File,
                     () =>
                     {
                         System.Diagnostics.Process.Start("explorer.exe", M.Tsunami2.Preferences.Values.Paths.PersonnalPreferencesPath);
                     }, TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                c.Width += 100;
                controls.Add(c);
            }
            return controls;
        }

        #endregion

        #region Dependencies

        private int dependencyIndex;
        private Timer dependencyTimer;
        private int dependencyTimerCounter;

        private void GetDependenciesFromXml()
        {
            // say that an action is going on, will be stop in the timer 2
            busy = true;

            // Setup timer 2
            dependencyTimer = new Timer() { Interval = 20, Enabled = false };
            dependencyTimer.Tick += new EventHandler(dependencyTimer_TickXml);
            dependencyIndex = 0;
            M.Tsunami2.Preferences.Values.LoadDependenciesFromXml();
            dependencyTimerCounter = 0;
            dependencyTimer.Start();
        }

        private void dependencyTimer_TickXml(object sender, EventArgs e)
        {
            int countTemp = 0;
            foreach (AD.Application item in P.Preferences.Dependencies.Applications)
            {
                if (countTemp == dependencyIndex)
                {
                    if (item.IsInstalled)
                        Logs.Log.AddEntryAsSuccess(null, item.ToString() + " " + R.T347);
                    else
                    {
                        if (item.Vital)
                        {
                            Logs.Log.AddEntryAsError(null, item.ToString() + " " + R.T499);
                            problemFound = true;
                            problemFoundIndependencies = true;
                        }
                        else
                        {
                            Logs.Log.AddEntryAsPayAttentionOf(null, $"{item} {R.T499} but not vital, you will not be able to use some functionnalities");

                        }
                    }
                    this.AdaptSize();
                }
                countTemp++;
            }
            dependencyIndex++;

            if (dependencyIndex == P.Preferences.Dependencies.Applications.Count)
            {
                dependencyTimer.Stop();
                LoadingDepFinished = true;
                busy = false;
            }

            dependencyTimerCounter++;
            if (dependencyTimerCounter > 100)
            {
                Logs.Log.AddEntryAsError(null, R.EM_Splash_TooLong);
                dependencyTimer.Stop();
                problemFound = true;
                problemFoundIndependencies = true;
                LoadingDepFinished = true;
                busy = false;
            }
        }

        #endregion

        private void SplashScreen_Load(object sender, EventArgs e)
        {
            this.AdaptSize();
        }

        #endregion

        private void VersionLabel_Click(object sender, EventArgs e)
        {

        }

        private void labelClose_Click(object sender, EventArgs e)
        {
            CloseSplashScreen();
        }
    }
}
