﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TSU.Views;
using TSU.Views.Message;
using M = TSU;
using R = TSU.Properties.Resources;

namespace TSU.Logs
{
    public partial class LogView : Control
    {
        public Size GetPreferedSize
        {
            get
            {
                var b = richTextBox;
                Size a = TextRenderer.MeasureText(b.Text, b.Font);
                return a;
            }
        }

        public M.Module WantedModule { get; set; }

        public LogView() : base()
        {
            DoubleBuffered = true;

            InitializeComponent();

            Log.Subscribe(this);

            richTextBox.BackColor = colors.Color4LogBox;
            richTextBox.LinkClicked += OnLinkClicked;
            richTextBox.ScrollBars = RichTextBoxScrollBars.Vertical;
            richTextBox.WordWrap = true;

            Dock = DockStyle.Fill;
        }

        private void OnLinkClicked(object sender, LinkClickedEventArgs e)
        {
            MessageTsu.OpenLink(e.LinkText);
        }

        internal void Append(IEnumerable<LogEntry> logEntries)
        {
            if (!IsHandleCreated || IsDisposed || Disposing)
                return;

            if (InvokeRequired)
                BeginInvoke(new Action<IEnumerable<LogEntry>, bool>(AppendFromMainThread), logEntries, false);
            else
                AppendFromMainThread(logEntries, false);
        }

        private void AppendFromMainThread(IEnumerable<LogEntry> logEntries, bool clear)
        {
            richTextBox.BeginUpdate();

            if (clear)
                richTextBox.Clear();

            foreach (LogEntry logEntry in logEntries)
                Append(logEntry);

            try
            {
                richTextBox.SelectionStart = richTextBox.Text.Length;
                richTextBox.ScrollToBottom();
            }
            catch (Exception)
            {
                TSU.Debug.WriteInConsole("ScrollToBottom failed");
            }

            richTextBox.EndUpdate();
        }

        private void Append(LogEntry logEntry)
        {
            // Put the caret at the end of the text
            richTextBox.SelectionStart = richTextBox.Text.Length;

            // Define the color for the new text
            richTextBox.SelectionColor = richTextBox.ForeColor;
            switch (logEntry.type)
            {
                case LogEntry.LogEntryTypes.Success:
                    richTextBox.SelectionColor = colors.Color4Success;
                    break;
                case LogEntry.LogEntryTypes.BeginningOf:
                    richTextBox.SelectionColor = colors.Color4BeginningOf;
                    break;
                case LogEntry.LogEntryTypes.Error:
                    richTextBox.SelectionColor = colors.Color4Error;
                    break;
                case LogEntry.LogEntryTypes.FinishingOf:
                    richTextBox.SelectionColor = colors.Color4FinishOf;
                    break;
                case LogEntry.LogEntryTypes.PayAttention:
                    richTextBox.SelectionColor = colors.Color4PayAttentionTo;
                    break;
                case LogEntry.LogEntryTypes.FYI:
                    richTextBox.SelectionColor = colors.Color4FYI;
                    break;
                default:
                    break;
            }

            // Add the new text
            string untreatedText = logEntry.ToFullLogEntry();
            if (untreatedText.Contains('$'))
            {
                MessageTsu.ThreatStringColorForRTB(untreatedText, richTextBox, richTextBox.SelectionColor);
            }
            else
            {
                richTextBox.SelectedText = untreatedText;
            }
        }

        private class LogColors
        {
            public Color Color4BeginningOf { get; set; } = Tsunami2.Preferences.Theme.Colors.LightForeground;
            public Color Color4FinishOf { get; set; } = Tsunami2.Preferences.Theme.Colors.Good;
            public Color Color4Success { get; set; } = Tsunami2.Preferences.Theme.Colors.Good;
            public Color Color4Error { get; set; } = Tsunami2.Preferences.Theme.Colors.Bad;
            public Color Color4FYI { get; set; } = Tsunami2.Preferences.Theme.Colors.LightForeground;
            public Color Color4PayAttentionTo { get; set; } = Tsunami2.Preferences.Theme.Colors.Attention;
            public Color Color4DateAndTime { get; set; } = Tsunami2.Preferences.Theme.Colors.Action;
            public Color Color4LogBox { get; set; } = Tsunami2.Preferences.Theme.Colors.DarkBackground;

            public void SetColors(
                Color? beginningOf = null,
                Color? logBack = null,
                Color? dateAndTime = null,
                Color? error = null,
                Color? fYI = null,
                Color? payAttentionTo = null,
                Color? success = null,
                Color? finishOf = null)
            {
                Color4LogBox = logBack ?? Color4LogBox; // if value is null then set to Tsunami2.Preferences.Theme._Colors.Foreground
                Color4BeginningOf = beginningOf ?? Color4BeginningOf;
                Color4FinishOf = finishOf ?? Color4FinishOf;
                Color4Success = success ?? Color4Success;
                Color4Error = error ?? Color4Error;
                Color4FYI = fYI ?? Color4FYI;
                Color4PayAttentionTo = payAttentionTo ?? Color4PayAttentionTo;
                Color4DateAndTime = dateAndTime ?? Color4DateAndTime;
            }
        }

        private readonly LogColors colors = new LogColors();

        public void SetColors(
            Color? beginningOf = null, Color? logBack = null, Color? dateAndTime = null,
            Color? error = null, Color? fYI = null, Color? payAttentionTo = null, Color? success = null, Color? finishOf = null)
        {
            colors.SetColors(beginningOf, logBack, dateAndTime, error, fYI, payAttentionTo, success, finishOf);

            BackColor = colors.Color4LogBox;
            richTextBox.BackColor = colors.Color4LogBox;
        }

        public void UpdateView()
        {
            var stopwatch = Stopwatch.StartNew();
            TSU.Debug.WriteInConsole($"UpdatingLogView...");

            // Apply the filter
            bool Keep(LogEntry e) => WantedModule == null || e.sender == WantedModule;
            IEnumerable<LogEntry> shortList = Log.AllEntries.Where(Keep).ToList();

            //// Keep only the last entries to speed up the process
            //const int NUMBER_OF_ENTRIES_TO_UPDATE = 100;
            ////// Reverse the sequence, to take the last ones
            //shortList = shortList.Reverse().Take(NUMBER_OF_ENTRIES_TO_UPDATE);
            ////// Reverse agin, to have the original order
            //shortList = shortList.Reverse();

            AppendFromMainThread(shortList, true);

            stopwatch.Stop();

            TSU.Debug.WriteInConsole($"...Log View Updated *********************************************** in {stopwatch.Elapsed}");

        }

        public void Copy(object sender, EventArgs e)
        {
            var b = richTextBox;
            if (b.SelectedText == "")
                Clipboard.SetText(b.Text);
            else
                Clipboard.SetText(b.SelectedText);
        }

        private void Box_DoubleClick(object sender, EventArgs e)
        {
            List<Control> listOfControls = new List<Control>
            {
                new BigButton(R.T_B_OpenHtmlLog, R.Log,OpenHtmlLog, Tsunami2.Preferences.Theme.Colors.Action),
                new BigButton(R.T_B_ShowAllLog, R.Log,ShowAllEntries, Tsunami2.Preferences.Theme.Colors.Action),
                new BigButton(R.T_B_CopyString, R.Copy, Copy, null, Tsunami2.Preferences.Theme.Colors.Action)
            };

            // Clean the list to show all modules
            WantedModule = null;// to show from all modules

            List<M.Module> modulesHavingEntriesInTheLog = new List<M.Module>();

            // create the list of all available modules
            foreach (LogEntry item in Log.AllEntries)
            {
                if (item.sender != null && !modulesHavingEntriesInTheLog.Contains(item.sender))
                {
                    modulesHavingEntriesInTheLog.Add(item.sender);

                    // create button that will show only the selected module
                    listOfControls.Add(new BigButton(
                        item.sender._Name + ";" + R.buttonShowLogFrom + " " + item.sender._Name,
                        R.Log,
                        () =>
                        {
                            WantedModule = item.sender;
                            UpdateView();
                        }));
                }
            }

            TsuPopUpMenu tsuPopUp = new TsuPopUpMenu(this, "LogView PopupMenu");
            tsuPopUp.ShowPopUpMenu(listOfControls, "Log");
        }

        private void ShowAllEntries()
        {
            WantedModule = null; // to show from all modules
            UpdateView();
        }

        private static void OpenHtmlLog()
        {
            Shell.Run(Log.GetHtmlLogPath());
        }

        internal void ShowDockedFill()
        {
            Dock = DockStyle.Fill;
            Visible = true;
            Show();
        }
    }
}
