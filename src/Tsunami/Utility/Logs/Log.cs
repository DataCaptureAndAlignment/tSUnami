﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using static TSU.Logs.LogEntry;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;

namespace TSU.Logs
{
    public static class Log
    {
        #region Fields & Props

        private static readonly object lockObj = new object();

        private static DiskFile diskFile = null;

        private static readonly List<LogView> observingViews = new List<LogView>();

        private static LogEntry previousLogEntry = null;

        internal static readonly List<LogEntry> AllEntries = new List<LogEntry>();

        #endregion

        public static new string ToString()
        {
            string message = "";
            foreach (var item in AllEntries)
            {
                message += item.ToString() + "\r\n";
            }
            return message;
        }

        public static void StartHtmlLog(string path)
        {
            lock (lockObj)
            {
                diskFile = new DiskFile(path);

                // Add all existing entries
                foreach (var item in AllEntries)
                    diskFile.Append(item);
            }
        }

        public static string GetHtmlLogPath()
        {
            return diskFile?.pathToSave ?? string.Empty;
        }

        public static void Subscribe(LogView lv)
        {
            void SubscribeCallback(object state)
            {
                lock (lockObj)
                {
                    observingViews.Add(lv);
                }
            }
            ThreadPool.QueueUserWorkItem(SubscribeCallback);
        }

        private static void NotifyUsersStartWorkerThread(Module sender, string logEntry, LogEntryTypes type)
        {
            try
            {
                lock (lockObj)
                {
                    if (logEntry.Contains("\r\n")) //ligne de log sur plusieurs lignes
                    {
                        logEntry = "\r\n" + logEntry;
                        logEntry = logEntry.Replace("\r\n", "\r\n\t\t");
                    }

                    LogEntry e = new LogEntry(sender, logEntry + Environment.NewLine, type);
                    Debug.WriteInConsole($"TSU.LOGGING: {logEntry}");

                    if (previousLogEntry == null || logEntry != previousLogEntry.entry)
                    {
                        // New Entry : add to the list 
                        AllEntries.Add(e);
                    }
                    else
                    {
                        // Same Entry

                        // Don't add to the list, so an update will remove the copies

                        // Update the existing. It is a reference so it will change the element of list.
                        previousLogEntry.number++;
                        previousLogEntry.date = e.date;

                        // Append the updated element, because refreshing the existing would be slow and have side effects
                        e = previousLogEntry;
                    }

                    previousLogEntry = e;

                    // Update the DiskFile
                    diskFile?.Append(e);

                    ThreadPool.QueueUserWorkItem(NotifyUsersCallback, e);
                }

                void NotifyUsersCallback(object e)
                {
                    // Append to the LogViews; if we are not in the main thread they will call Invoke internally
                    List<LogEntry> logEntries = new List<LogEntry>() { (LogEntry)e };
                    foreach (LogView lv in observingViews)
                        lv?.Append(logEntries);
                }

            }
            catch (Exception ex)
            {
                Debug.WriteInConsole($"/!\\  /!\\  /!\\  /!\\Cannot Notify The Log Entry {ex.Message}");
            }
        }

        // Notifications
        public static void AddEntry(Module sender, string message, LogEntryTypes type)
        {
            NotifyUsersStartWorkerThread(sender, message, type);
        }

        public static void AddEntryAsSuccess(Module sender, string message)
        {
            NotifyUsersStartWorkerThread(sender, message, LogEntry.LogEntryTypes.Success);
        }

        public static void AddEntryAsBeginningOf(Module sender, string message)
        {
            NotifyUsersStartWorkerThread(sender, message + " ...", LogEntry.LogEntryTypes.BeginningOf);
        }

        public static void AddEntryAsError(Module sender, string message)
        {
            NotifyUsersStartWorkerThread(sender, message + " !!!", LogEntry.LogEntryTypes.Error);
        }

        public static void AddEntryAsFinishOf(Module sender, string message)
        {
            NotifyUsersStartWorkerThread(sender, "... " + message, LogEntry.LogEntryTypes.FinishingOf);
        }

        public static void AddEntryAsFYI(Module sender, string message)
        {
            NotifyUsersStartWorkerThread(sender, "(" + message + ")", LogEntry.LogEntryTypes.FYI);
        }

        public static void AddEntryAsPayAttentionOf(Module sender, string message)
        {
            NotifyUsersStartWorkerThread(sender, @"/!\ " + message + @" /!\", LogEntry.LogEntryTypes.PayAttention);
        }

        public static void AddEntryAsResult(Module sender, Result result, bool skipSuccesMessage = false)
        {
            if (result.Success)
            {
                if (!skipSuccesMessage)
                    NotifyUsersStartWorkerThread(sender, result.AccessToken?.ToString() ?? "OK", LogEntry.LogEntryTypes.Success);
            }
            else
            {
                NotifyUsersStartWorkerThread(sender, result.ErrorMessage + " !!!", LogEntry.LogEntryTypes.Error);
            }
        }

        private class DiskFile
        {
            public string pathToSave;

            public DiskFile()
            {
            }

            public DiskFile(string path)
            {
                pathToSave = path;
            }

            void WriteHtmlHeader(FileStream logOnDisk)
            {
                byte[] info = new UTF8Encoding(true).GetBytes(
                    @"<!DOCTYPE html>
                    <html>
                    <head>
                    <style>
                    #para1 {
                        color: black;
                        vertical-align: top
                        }
                    td {
                        text-align: left;
                        min-width: 180px;
                        max-width: 40p;
                        vertical-align: top;
                    }

                    tr:nth-child(even){background-color: #f2f2f2;}
                    tr:hover {background-color: #ddd;}
                    </style>
                    </head>
                    <body>

                    <table>
                    <tr> <th >Date and time</th><th>Message</th><th>Sender</th></tr>"
                );
                logOnDisk.Write(info, 0, info.Length);
            }

            string missedLogs = "";

            public void Append(LogEntry value)
            {
                string entry = "";
                try
                {
                    using (FileStream logOnDisk = new FileStream(pathToSave, FileMode.Append)) // create if null
                    {
                        if (logOnDisk.Position == 0)
                            WriteHtmlHeader(logOnDisk);
                        if (missedLogs != "") entry += missedLogs;
                        entry += value.ToFullLogEntryInHtml() + "\r\n";
                        byte[] info = new UTF8Encoding(true).GetBytes(entry);
                        logOnDisk.Write(info, 0, info.Length);
                        logOnDisk.Flush();
                    }
                    missedLogs = "";
                }
                catch (Exception ex)
                {
                    missedLogs += ex.Message + $"this message was not written on the first attempt: {entry}\r\n";
                }
            }
        }

        internal static void OnStatusChanged(object sender, I.StatusEventArgs e)
        {
            AddEntryAsFYI(sender as Module, string.Format("Status changed to {0}", e.StatusAsText));
        }

        internal static void StartingMeasurement(object sender, M.MeasurementEventArgs e)
        {
            AddEntryAsBeginningOf(sender as Module, string.Format("Measure of {0} begins", e.BeingMeasured));
        }

        internal static void OnMeasureAvailable(object sender, M.MeasurementEventArgs e)
        {
            AddEntryAsFYI(sender as Module, string.Format("Result of {0} measurement is available", e.JustMeasured));
        }

        internal static void OnMeasurementFailed(object sender, M.MeasurementEventArgs e)
        {
            if (e.JustMeasured != null)
            {
                if (e.JustMeasured._Status is M.States.Cancel)
                    AddEntryAsFYI(sender as Module, string.Format("Measure Cancelled {0}", e.BeingMeasured));
                else
                    AddEntryAsError(sender as Module, string.Format("Measure failed {0}", e.JustMeasured));
            }
            else
            {
                AddEntryAsError(sender as Module, string.Format("Measure failed {0}", e.JustMeasured));
            }
        }

        internal static void OnMeasurementFinished(object sender, M.MeasurementEventArgs e)
        {
            AddEntryAsFinishOf(sender as Module, string.Format("Finished to measure {0}", e.JustMeasured));
        }

        internal static void OnInstrumentChangingFace(object sender, M.MeasurementEventArgs e)
        {
            AddEntryAsFYI(sender as Module, "Changing Face");
        }

        internal static void OnSubMeasurementStarted(object sender, M.MeasurementEventArgs e)
        {
            AddEntryAsSuccess(sender as Module, string.Format("Sub measure of {0} started", e.BeingMeasured._PointName));
        }

        internal static void OnInstrumentLocking(object sender, M.MeasurementEventArgs e)
        {
            AddEntryAsFYI(sender as Module, "Locking");
        }

        internal static void OnInstrumentAcquiring(object sender, M.MeasurementEventArgs e)
        {
            AddEntryAsFYI(sender as Module, "Acquiring");
        }

        internal static void OnInstrumentMoving(object sender, M.MeasurementEventArgs e)
        {
            AddEntryAsFYI(sender as Module, "Moving");
        }

        internal static void OnStartingMeasureCorrection(object sender, M.MeasurementEventArgs e)
        {
            if (e.BeingMeasured != null)
            {
                if (e.BeingMeasured._Status.Type != M.States.Types.Cancel)
                    AddEntryAsSuccess(sender as Module, string.Format("Raw Sub measure started: {0}", e.BeingMeasured));
                else
                    AddEntryAsFYI(sender as Module, string.Format("Raw Sub measure started: {0}", e.BeingMeasured));
            }

        }

        internal static void OnMeasureCorrected(object sender, M.MeasurementEventArgs e)
        {
            if (e.BeingMeasured != null)
            {
                if (e.BeingMeasured._Status.Type != M.States.Types.Cancel)
                    AddEntryAsSuccess(sender as Module, string.Format("Sub measure Corrected: {0}", e.BeingMeasured));
                else
                    AddEntryAsFYI(sender as Module, string.Format("Sub measure Corrected: {0}", e.BeingMeasured));
            }

        }

        internal static void AddEntry(Module m, Exception ex)
        {
            AddEntryAsPayAttentionOf(m, ex.Message);
            if (ex.InnerException != null)
                AddEntry(m, ex.InnerException);
        }

        public static List<LogEntry> GetAllEntries()
        {
            return new List<LogEntry>(AllEntries);
        }
    }
}
