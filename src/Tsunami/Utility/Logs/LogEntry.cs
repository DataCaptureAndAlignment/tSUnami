﻿using System;
using System.Xml.Serialization;

namespace TSU.Logs
{
    public class LogEntry
    {
        public enum LogEntryTypes
        {
            Success,
            BeginningOf,
            Error,
            FinishingOf,
            PayAttention,
            FYI
        }

        public string date;

        public string entry;

        public int number = 0;

        public LogEntryTypes type;

        [XmlIgnore]
        public Module sender;

        public LogEntry()
        {

        }

        public LogEntry(Module sender, string entry, LogEntryTypes type)
        {
            date = DateTime.Now.ToString("yyyy-MM-dd, h:mm:ss tt");
            this.type = type;
            this.entry = entry;
            this.sender = sender;
        }

        public override string ToString()
        {
            return $"{entry}";
        }

        internal string ToFullLogEntry()
        {
            string header = date;
            if (sender != null && sender._Name != "")
                header += ", " + sender._Name;
            header += ": ";

            string[] lines = entry.Split(new[] { "\r\n", "\n" }, StringSplitOptions.None);

            string text = "";
            foreach (string line in lines)
            {
                if (!string.IsNullOrWhiteSpace(line)) // Only add header if the line is not empty
                {
                    text += header + line + "\r\n";
                }
            }

            if (number > 0)
            {
                string sNumber = "(" + number.ToString() + ")";
                text = text.Insert(text.Length - 2, sNumber);
            }

            return text;
        }

        internal string ToFullLogEntryInHtml()
        {
            string text = "";

            // new row
            text += "<tr>";

            // date
            text += $"<td>{date}</td>";

            // entry
            text += $"<td>";

            // color
            switch (type)
            {
                case LogEntryTypes.Success:
                    text += "<font color = green>";
                    break;
                case LogEntryTypes.BeginningOf:
                    text += "<font color = blue>";
                    break;
                case LogEntryTypes.Error:
                    text += "<font color = red>";
                    break;
                case LogEntryTypes.FinishingOf:
                    text += "<font color = green>";
                    break;
                case LogEntryTypes.PayAttention:
                    text += "<font color = orange>";
                    break;
                case LogEntryTypes.FYI:
                    text += "<font color = gray>";
                    break;
                default:
                    break;
            }
            text += $"{entry}</font>";

            // show several in row with the number in ()
            if (number > 0)
                text += $"({number})";

            text += "</td>";

            // sender
            string senderName = sender?._Name ?? "-";

            text += $"<td>{senderName}</td>";

            text += "</tr>";

            return text;
        }
    }
}
