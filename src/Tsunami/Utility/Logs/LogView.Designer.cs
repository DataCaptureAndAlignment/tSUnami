﻿using System;
using TSU.Common;

namespace TSU.Logs
{
    partial class LogView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox = new TSU.Views.ExtendedRichTextBox();
            this.SuspendLayout();
            // 
            // box
            // 
            this.richTextBox.BackColor = System.Drawing.Color.IndianRed;
            this.richTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox.Location = new System.Drawing.Point(2, 2);
            this.richTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.ReadOnly = true;
            this.richTextBox.Size = new System.Drawing.Size(329, 96);
            this.richTextBox.TabIndex = 12;
            this.richTextBox.Text = "";
            this.richTextBox.WordWrap = false;
            this.richTextBox.DoubleClick += new System.EventHandler(this.Box_DoubleClick);
            this.BackColor = System.Drawing.Color.Fuchsia;
            this.ClientSize = new System.Drawing.Size(333, 100);
            this.Controls.Add(this.richTextBox);
            this.MinimumSize = new System.Drawing.Size(300, 100);
            this.ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.Name = "LogView";
            this.Padding = new System.Windows.Forms.Padding(2);
            this.ResumeLayout(false);
        }

        #endregion

        public TSU.Views.ExtendedRichTextBox richTextBox;
    }
}
