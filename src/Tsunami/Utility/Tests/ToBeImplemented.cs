﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSU.Views;
using M = TSU;


namespace TSU.Connectivity_Tests
{
    public partial class ToBeImplemented : TsuView
    {
        public ToBeImplemented(M.Module parentmodule, string todo = "Not Sure yet what this will do...")
            : base(parentmodule)
        {

            InitializeComponent();
            this.ApplyThemeColors();
            Todo.Text = todo;
        }
        private void ApplyThemeColors()
        {
            this.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
        }
    }
}
