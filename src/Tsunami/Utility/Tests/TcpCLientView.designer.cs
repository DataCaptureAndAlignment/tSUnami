﻿using TSU.Common;

namespace TSU.Connectivity_Tests
{
    partial class Client
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.StartButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ResultLabel = new System.Windows.Forms.Label();
            this.LabelRec = new System.Windows.Forms.Label();
            this.TransmissionLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.label1.Location = new System.Drawing.Point(292, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(214, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "I am the client, I want to communicate with ip:";
            // 
            // StartButton
            // 
            this.StartButton.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.StartButton.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.StartButton.Location = new System.Drawing.Point(296, 251);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(150, 52);
            this.StartButton.TabIndex = 14;
            this.StartButton.Text = "START";
            this.StartButton.UseMnemonic = false;
            this.StartButton.UseVisualStyleBackColor = false;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.CloseButton.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.CloseButton.Location = new System.Drawing.Point(452, 251);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(146, 51);
            this.CloseButton.TabIndex = 15;
            this.CloseButton.Text = "CLOSE";
            this.CloseButton.UseVisualStyleBackColor = false;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.pictureBox1.Location = new System.Drawing.Point(295, 423);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(296, 10);
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // ResultLabel
            // 
            this.ResultLabel.AutoSize = true;
            this.ResultLabel.ForeColor = Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.ResultLabel.Location = new System.Drawing.Point(292, 373);
            this.ResultLabel.Name = "ResultLabel";
            this.ResultLabel.Size = new System.Drawing.Size(56, 13);
            this.ResultLabel.TabIndex = 12;
            this.ResultLabel.Text = "DEFAULT";
            // 
            // LabelRec
            // 
            this.LabelRec.AutoSize = true;
            this.LabelRec.ForeColor = Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.LabelRec.Location = new System.Drawing.Point(292, 396);
            this.LabelRec.Name = "LabelRec";
            this.LabelRec.Size = new System.Drawing.Size(105, 13);
            this.LabelRec.TabIndex = 9;
            this.LabelRec.Text = "RECEIVED VIA TCP";
            // 
            // TransmissionLabel
            // 
            this.TransmissionLabel.AutoSize = true;
            this.TransmissionLabel.ForeColor = Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.TransmissionLabel.Location = new System.Drawing.Point(509, 202);
            this.TransmissionLabel.Name = "TransmissionLabel";
            this.TransmissionLabel.Size = new System.Drawing.Size(56, 13);
            this.TransmissionLabel.TabIndex = 13;
            this.TransmissionLabel.Text = "DEFAULT";
            // 
            // button1
            // 
            this.button1.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.button1.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button1.Location = new System.Drawing.Point(295, 308);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(303, 62);
            this.button1.TabIndex = 10;
            this.button1.Text = "PULL DATA FROM SERVER";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.MouseCaptureChanged += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.label2.Location = new System.Drawing.Point(460, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "on port:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(512, 124);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 18;
            this.textBox1.Text = "137.138.119.250";
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(512, 150);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 19;
            this.textBox2.Text = "11000";
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.label4.Location = new System.Drawing.Point(449, 178);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "And Send:";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(512, 175);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 22;
            this.textBox3.Text = "Data Request";
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.label3.Location = new System.Drawing.Point(480, 202);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "I\'m:";
            // 
            // Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.ClientSize = new System.Drawing.Size(891, 631);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.ResultLabel);
            this.Controls.Add(this.LabelRec);
            this.Controls.Add(this.TransmissionLabel);
            this.Controls.Add(this.button1);
            this.Name = "Client";
            this.Text = "CLIENT";
            this.Load += new System.EventHandler(this.CLIENT_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label ResultLabel;
        private System.Windows.Forms.Label LabelRec;
        private System.Windows.Forms.Label TransmissionLabel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label3;


    }
}

