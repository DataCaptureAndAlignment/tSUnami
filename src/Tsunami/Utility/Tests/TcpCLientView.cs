﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSU.IO.TCP;
using TSU.Views;


namespace TSU.Connectivity_Tests
{
    public partial class Client : TsuView
    {
        public AsynchronousClient AsynchronousClient;
        public Client()
        {
            InitializeComponent();
            this.ApplyThemeColors();
        }

        private void ApplyThemeColors()
        {
            this.label1.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.StartButton.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.StartButton.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.CloseButton.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.CloseButton.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.pictureBox1.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.ResultLabel.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.LabelRec.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.TransmissionLabel.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.button1.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button1.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.label2.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.label4.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.label3.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string response = AsynchronousClient.SendThroughTCPSocket("DataRequest");
            if (response != "")
            {
                //Manage the response
                //TSU.Debug.WriteInConsole("The server did respond to the last request.\n Response : " + response);
                Data.lastResponse = response;
                response = "";
            }
            else
            {
                TSU.Debug.WriteInConsole("TSU.TCPIP "+"Null response from server");
            }

            //set back to listening state 
           //AsynchronousClient.BackToSocketListening();
         }    


        private void StartButton_Click(object sender, EventArgs e)
        {

            Thread tcpClientThread = new Thread(delegate()
            {
                //start client
                AsynchronousClient.StartClient();

            });
            tcpClientThread.IsBackground = true;
            tcpClientThread.Start(); 
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Thread tcpClientThread = new Thread(delegate()
            {
                //stop client
                AsynchronousClient.StopClient();

            });
            tcpClientThread.IsBackground = true;
            tcpClientThread.Start(); 
        }

        private void CLIENT_Load(object sender, EventArgs e)
        {
            this.AsynchronousClient = new TSU.IO.TCP.AsynchronousClient();

            textBox1_TextChanged(this, null);
            textBox2_TextChanged(this, null);
            textBox3_TextChanged(this, null);

            // To update the first time.
            UpdateTransmissionLabel();
            UpdateResultLabel();
            //update every 0.3 sec
            var timer = new System.Timers.Timer { Interval = 300 };
            timer.Elapsed += (o, args) =>
            {
                UpdateTransmissionLabel();
                UpdateResultLabel();
            };
            timer.Start();
        }

        private void UpdateTransmissionLabel()
        {
            if (this.TransmissionLabel.InvokeRequired)
            {
                this.TransmissionLabel.BeginInvoke((MethodInvoker)delegate()
                {
                    this.TransmissionLabel.Text = AsynchronousClient.currentState.ToString(); ;
                });
            }
            else
            {
                this.TransmissionLabel.Text = AsynchronousClient.currentState.ToString(); ;
            }
        }

        private void UpdateResultLabel()
        {
            if (this.ResultLabel.InvokeRequired)
            {
                this.ResultLabel.BeginInvoke((MethodInvoker)delegate()
                {
                    this.ResultLabel.Text = Data.lastResponse; ;
                });
            }
            else
            {
                this.ResultLabel.Text = Data.lastResponse; ;
            }
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                AsynchronousClient.ipAddress = System.Net.IPAddress.Parse(textBox1.Text);
                StartButton.Text = "Start connection with " + AsynchronousClient.ipAddress.ToString() + ":" + AsynchronousClient.port.ToString();
                CloseButton.Text = "Stop connection with " + AsynchronousClient.ipAddress.ToString() + ":" + AsynchronousClient.port.ToString();
            }
            catch
            {

            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            try
            {
            AsynchronousClient.port = int.Parse(textBox2.Text);
            StartButton.Text = "Start connection with " + AsynchronousClient.ipAddress.ToString() + ":" + AsynchronousClient.port.ToString();
            CloseButton.Text = "Stop connection with " + AsynchronousClient.ipAddress.ToString() + ":" + AsynchronousClient.port.ToString();
            }
            catch
            {

            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            AsynchronousClient.nextMessage = textBox3.Text;
        }

    }
}
