﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSU.Views;

using D = TSU.Common.Instruments.Device;
using TSU.Common.Measures;
using TSU.Common.Elements;


namespace TSU.Connectivity_Tests
{
    public partial class TestInstrument : TsuView
    {

        private D.TC2002.Module tc2002;
        private D.TC2002.View tc2002Form;

        private D.AT40x.Module at40x;

        private D.DNA03.Module dna03;
        private D.DNA03.View dna03Form;

        private D.TDA5005.Module tda5005;
        private D.TDA5005.View tda5005View;

        private D.TCRA1100.Module tcra1100;
        private D.TCRA1100.View tcra1100View;

        public TestInstrument()
            : base(null)
        {
            InitializeComponent();
            this.ApplyThemeColors();
        }
        private void ApplyThemeColors()
        {

        }
        private void button2_Click(object sender, EventArgs e)
        {
            tc2002 = new D.TC2002.Module(Tsunami2.Properties, TSU.Tsunami2.Preferences.Values.Instruments.Find(x=> x._SerialNumber=="437566"));
            tc2002Form = new D.TC2002.View(tc2002);
            tc2002Form.ShowDockedTop();
            this.panel1.Controls.Add(tc2002Form);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            at40x = new D.AT40x.Module(null, null);
            at40x._TsuView.ShowDockedTop();
            this.panel1.Controls.Add(at40x._TsuView);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            dna03 = new D.DNA03.Module(null, null);
            dna03Form = new D.DNA03.View(dna03);
            dna03Form.ShowDockedTop();
            this.panel1.Controls.Add(dna03Form);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            tda5005 = new D.TDA5005.Module(Tsunami2.Properties, TSU.Tsunami2.Preferences.Values.Instruments.Find(x=> x._SerialNumber == "438720"));
            tda5005View = new D.TDA5005.View(tda5005);
            tda5005View.ShowDockedTop();
            this.panel1.Controls.Add(tda5005View);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tcra1100 = new D.TCRA1100.Module(null, null);
            tcra1100View = new D.TCRA1100.View(tcra1100);
            tcra1100View.ShowDockedTop();
            this.panel1.Controls.Add(tcra1100View);
        }

        private  void button3_Click(object sender, EventArgs e)
        {
            D.Manual.Theodolite.Instrument theo = new D.Manual.Theodolite.Instrument();
            D.Manual.Theodolite.ManualTheodoliteView m = new D.Manual.Theodolite.ManualTheodoliteView();
            m.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            m.ShowInMessageTsu($"{R.T_ENTER_A_VALUE};{R.T_PLEASE_ENTER_THE_READING_FROM_YOUR_INSTRUMENT}", R.T_CANCEL, null, null);
            //m.ShowDockedTop();
            //this.panel1.Controls.Add(m);
        }
    }
}
