using System;
using M = TSU;
using R = TSU.Properties.Resources;


namespace TSU.Connectivity_Tests
{
    public class TestModule : M.Manager
    {
        




        public TestModule(M.Module module, string nameAndDescription)
            : base(module, nameAndDescription)
        {
        }
    }

    public class DllTestModule : TestModule
    {
        public DllTestModule(M.Module module, string nameAndDescription)
            : base(module, nameAndDescription)
        {
        }
    }

     public class InstrumentTestModule : TestModule
    {

         public InstrumentTestModule(M.Module module, string nameAndDescription)
             : base(module, nameAndDescription)
        {
        }
    }

     public class TcpTestModule : TestModule
    {

         public TcpTestModule(M.Module module, string nameAndDescription)
             : base(module, nameAndDescription)
        {
        }
    }

     public class ServerTestModule : TestModule
    {

         public ServerTestModule(M.Module module, string nameAndDescription)
             : base(module, nameAndDescription)
        {
        }
    }
     public class MessageTestModule : TestModule
     {

         public MessageTestModule(M.Module module, string nameAndDescription)
             : base(module, nameAndDescription)
         {
         }
     }

}
