﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using TSU.Common.Measures;
using TSU.Views;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using R = TSU.Properties.Resources;


namespace TSU.Connectivity_Tests
{
    public partial class FormTestMessage : TsuView
    {
        public FormTestMessage()
            : base(null)
        {
            InitializeComponent();
            this.ApplyThemeColors();
            //this._strategy = new ViewStrategy(this, ViewStrategy.ViewStrategyType.List);
            this.contextMenuStrip.Items.Add("test");
            this.contextMenuStrip.Items.Add("test2");
            this.contextMenuStrip.Opening += new CancelEventHandler(menuOpening);
            this.ResumeLayout(false);
            this.PerformLayout();
        }
        private void ApplyThemeColors()
        {
            this.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
        }
        private void menuOpening(object sender, CancelEventArgs e)
        {
            e.Cancel = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageInput mi = new MessageInput(MessageType.Choice, "Une Question pour vous!;Comment\r\nComment\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n  \r\n Comment  \r\n Comment \r\n Comment \r\n Comment  \r\n Comment \r\n Comment  \r\n Comment \r\n Comment  \r\n Comment \r\n Comment  \r\n Comment \r\n Comment que c'est? \r\n ")
            {
                ButtonTexts = new List<string> { "Je ne sais pas", "Ca va", "Ca va PAS" }
            };
            button1.Text = mi.Show().TextOfButtonClicked;
            new MessageInput(MessageType.GoodNews, "Flag is now:").Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageInput mi = new MessageInput(MessageType.GoodNews, "Bravo!;Tout c'est bien passé, félicitation \r\nbla \r\nblablbal \r\nkkkfdfd \r\n vdfgdficitation \r\n bla \r\n blablbal \r\n kkkfdfd \r\n vdfgdf")
            {
                ButtonTexts = new List<string> { R.T_MERCI }
            };
            button2.Text = mi.Show().TextOfButtonClicked;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageInput mi = new MessageInput(MessageType.Warning, R.T_POINT_EXIST)
            {
                ButtonTexts = new List<string> { R.T_I_KNOW, R.T_CANCEL }
            };
            button3.Text = mi.Show().TextOfButtonClicked;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string longmessage = "";

            for (int i = 0; i < 100; i++)
            {
                longmessage += "\r\n" + $"{R.T_VOUS_AVEZ_DÉBULLEZ}" + $"{R.T_VOUS_AVEZ_DÉBULLEZ}"
                + $"{R.T_VOUS_AVEZ_DÉBULLEZ}" + $"{R.T_VOUS_AVEZ_DÉBULLEZ}";
            }

            string titleAndMessage = $"{R.T_ATTENTION};" + longmessage;
            MessageInput mi = new MessageInput(MessageType.Critical, titleAndMessage)
            {
                ButtonTexts = new List<string> { R.T_OK + "!" }
            };
            button4.Text = mi.Show().TextOfButtonClicked;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            button5.Text = this.ShowMessageOfBug(
                $"{R.T_CATASTROPHE};{R.T_TSUNAMI_BUGS_VOUS_FERIEZ_MIEUX_DÊTRE_TRÈS_PRUDENT}", new Exception(),
                R.T_DACCORD_JAI_BIEN_COMPRIS_LE_MESSAGE);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string titleAndMessage = $"{R.T_IP_NEEDED};{R.T_ENTER_THE_IP_ADRESS}";
            List<string> buttonTexts = new List<string> { R.T_VALIDATE, "192.168.250.1" };

            MessageTsu.ShowMessageWithTextBox(titleAndMessage, buttonTexts, out string buttonClicked, out string textInput);

            if (buttonClicked == "192.168.250.1")
                button6.Text = buttonClicked;
            else
            {
                button6.Text = textInput;
            }
        }


        private void FormTestMessage_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("clik!");
            this.contextMenuStrip.TopLevel = true;
            this.contextMenuStrip.Show(Cursor.Position.X, Cursor.Position.Y);
            this.contextMenuStrip.Update();
            this.contextMenuStrip.Focus();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Logs.Log.AddEntryAsBeginningOf(null, "test");
            Logs.LogView lv = new Logs.LogView();

            //Set colors
            Preferences.GUI.Theme.ThemeColors colors = Tsunami2.Preferences.Theme.Colors;
            lv.SetColors(beginningOf: colors.LightForeground,
                         logBack: colors.DarkBackground,
                         dateAndTime: colors.LightForeground,
                         error: colors.Bad,
                         fYI: colors.LightForeground,
                         payAttentionTo: colors.Attention,
                         success: colors.Good,
                         finishOf: colors.LightForeground);

            // show all entries
            lv.WantedModule = null;
            lv.UpdateView();
            lv.Height = 200;
            lv.Dock = DockStyle.Fill;

            ShowMessageWithFillControl(control: lv,
                titleAndMessage: string.Format("{0};{1}", R.T_LOG,
                    R.T_ALL_LOG_ENTRIES_DOUBLE_CLICK_THE_TEXT_TO_REDUCE_THE_ENTRIES_BY_SELECTING_THE_SENDERS),
                buttonTexts: new List<string> { R.T_CLOSE }, waitForExit: false);
        }

        public System.Diagnostics.Process process;


        private void button9_Click(object sender, EventArgs e)
        {
            Shell.RunInDialogView(@"c:\Data\Tsunami\temp\LGC2\a.inp");
        }

        private void button10_Click(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            Shell.RunInDialogView(@"C:\windows\system32\notepad.exe");
        }

        private void button12_Click(object sender, EventArgs e)
        {
            List<string> l = new List<string> { @"C:\windows\notepad.exe", @"c:\Data\Tsunami\temp\test.txt" };
            Shell.RunWithWaitingMessage(this, l);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            Shell.RunInDialogView(@"\\cern.ch\dfs\Support\SurveyingEng\Computing\Software\Install\SUSOFT\SFB\9.01.04\setup.exe", "", "", "", true);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            TsuView t = new TsuView();

            WebBrowser w = new WebBrowser();
            w.Navigate("https://readthedocs.web.cern.ch/pages/viewpage.action?pageId=22152983");
            w.Dock = DockStyle.Fill;
            t.Controls.Add(w);
            t.Resizable = true;
            t.ShowDialog();
        }


        private void button16_Click(object sender, EventArgs e)
        {
            if (panel1.Visible)
                panel1.Hide();
            else
                panel1.Show();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            E.Manager.Module t = new E.Manager.Module();

            t.View.ShowInMessageTsu("test", R.T_OK, delegate { });
        }

        private void button18_Click(object sender, EventArgs e)
        {
            this.WaitingForm = new Views.Message.ProgressMessage(null, "titre", "debut", 5, 5000);


            WaitingForm.Show();

            WaitingForm.BeginAstep("task1");
            BackgroundWorker w = new BackgroundWorker();
            w.DoWork += delegate
            {
                System.Threading.Thread.Sleep(1000);
            };
            w.RunWorkerCompleted += delegate
            {
                WaitingForm.BeginAstep("task2");
            };

            w.RunWorkerAsync();

            w = new BackgroundWorker();
            w.DoWork += delegate
            {
                System.Threading.Thread.Sleep(2000);
            };
            w.RunWorkerCompleted += delegate
            {
                WaitingForm.BeginAstep("task3");
            };
            w.RunWorkerAsync();

            w = new BackgroundWorker();
            w.DoWork += delegate
            {
                System.Threading.Thread.Sleep(3000);
            };
            w.RunWorkerAsync();
            w.RunWorkerCompleted += delegate
            {
                WaitingForm.Stop();
            };

        }


        private void button20_Click(object sender, EventArgs e)
        {
            string titleAndMessage = $"{R.T_NAME_IT};{R.T_PLEASE_CHOOSE_MAGNET_IN_THE_LIST}?";
            MessageInput mi = new MessageInput(MessageType.ImportantChoice, titleAndMessage)
            {
                ButtonTexts = new List<string> { R.T_VALIDATE, R.T_CANCEL },
                TextOfButtonToBeLocked = R.T_CANCEL
            };
            string temp = mi.Show().TextOfButtonClicked;
        }

        private void BtnTestShowControlClick(object sender, EventArgs e)
        {
            MeasureOfOffset measureOfOffset = new MeasureOfOffset();

            bool ret = Line.Smart.MeasureInput.MeasureInput.Show(ref measureOfOffset);

            StringBuilder titleAndMessage = new StringBuilder();

            titleAndMessage.Append("Result;");
            titleAndMessage.AppendLine($"return={ret}");
            titleAndMessage.AppendLine($"_RawReading={measureOfOffset._RawReading}");
            titleAndMessage.Append($"_StandardDeviation={measureOfOffset._StandardDeviation}");

            for (int i = 0; i < measureOfOffset.SubReadings.Count; i++)
            {
                MeasureOfOffset.SubReading sr = measureOfOffset.SubReadings[i];
                titleAndMessage.AppendLine();
                titleAndMessage.Append($"SubReadings[{i}]:");
                titleAndMessage.Append($"value={sr.Reading}");
                titleAndMessage.Append($",use={sr.Use}");
            }

            new MessageInput(MessageType.FYI, titleAndMessage.ToString()).Show();
        }
    }
}
