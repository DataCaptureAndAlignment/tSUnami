﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using TSU.Views;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSU.IO.TCP;


namespace TSU.Connectivity_Tests
{
    public partial class Server : TsuView
    {
        public AsynchronousServer AsynchronousServer;
        public Server()
        {
            InitializeComponent();
            this.ApplyThemeColors();
        }
        private void ApplyThemeColors()
        {
            this.label4.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.label2.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.label1.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.StartButton.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.StartButton.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.CloseButton.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.CloseButton.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.LabelFile.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.TransmissionLabel.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button1.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button1.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            this.AsynchronousServer = new TSU.IO.TCP.AsynchronousServer();

            textBox1_TextChanged(this, null);
            textBox2_TextChanged(this, null);
            textBox3_TextChanged(this, null);

            // To update the first time.
            UpdateTransmissionLabel();
            UpdateFileLabel();
            //update every 0.3 sec
            var timer = new System.Timers.Timer { Interval = 300 };
            timer.Elapsed += (o, args) =>
            {
                UpdateTransmissionLabel();
                UpdateFileLabel();
            };
            timer.Start();
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            //init a new communication thread which will run aside
            Thread tcpServerThread = new Thread(delegate()
            {
                //socket opening
                AsynchronousServer.StartListening();
            });
            tcpServerThread.IsBackground = true;
            tcpServerThread.Start();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            //init a new communication thread which will run aside
            Thread tcpServerThread = new Thread(delegate()
            {
                //socket closing
                AsynchronousServer.StopServer();
            });
            tcpServerThread.IsBackground = true;
            tcpServerThread.Start();

        }

        private void SendButton_Click(object sender, EventArgs e)
        {
            //init a new communication thread which will run aside
            Thread tcpServerThread = new Thread(delegate()
            {
                //send data
                AsynchronousServer.SendThroughTCPSocket("ShouldRequest");
            });
            tcpServerThread.IsBackground = true;
            tcpServerThread.Start();
        }

        private void UpdateTransmissionLabel()
        {
            if (this.TransmissionLabel.InvokeRequired)
            {
                this.TransmissionLabel.BeginInvoke((MethodInvoker)delegate() {
                    this.TransmissionLabel.Text = AsynchronousServer.currentState.ToString(); ;
                });
            }
            else
            {
                this.TransmissionLabel.Text = AsynchronousServer.currentState.ToString(); ;
            }
        }

        private void UpdateFileLabel()
        {
            if (this.LabelFile.InvokeRequired)
            {
                this.LabelFile.BeginInvoke((MethodInvoker)delegate()
                {
                    this.LabelFile.Text = Data.xmlToSend; ;
                });
            }
            else
            {
                this.LabelFile.Text = Data.xmlToSend; ;
            }
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                AsynchronousServer.ipAddress = System.Net.IPAddress.Parse(textBox1.Text);
                StartButton.Text = "Start connection with " + AsynchronousServer.ipAddress.ToString() + ":" + AsynchronousServer.port.ToString();
                CloseButton.Text = "Stop connection with " + AsynchronousServer.ipAddress.ToString() + ":" + AsynchronousServer.port.ToString();
            }
            catch
            {

            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                AsynchronousServer.port = int.Parse(textBox2.Text);
                StartButton.Text = "Start connection with " + AsynchronousServer.ipAddress.ToString() + ":" + AsynchronousServer.port.ToString();
                CloseButton.Text = "Stop connection with " + AsynchronousServer.ipAddress.ToString() + ":" + AsynchronousServer.port.ToString();
            }
            catch
            {

            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            AsynchronousServer.nextMessage = textBox3.Text;
            Data.xmlToSend = textBox3.Text;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
