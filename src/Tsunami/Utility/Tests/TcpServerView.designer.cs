﻿using TSU.Common;

namespace TSU.Connectivity_Tests
{
    partial class Server
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.StartButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.LabelFile = new System.Windows.Forms.Label();
            this.TransmissionLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(227, 50);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 36;
            this.textBox3.Text = "Time";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.label4.Location = new System.Drawing.Point(164, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 35;
            this.label4.Text = "And Send:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(227, 25);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 34;
            this.textBox2.Text = "11000";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(227, -1);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 33;
            this.textBox1.Text = "137.138.119.250";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.label2.Location = new System.Drawing.Point(175, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "on port:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.label1.Location = new System.Drawing.Point(7, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(220, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "I\'m the Server, i want to communicate with ip:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // StartButton
            // 
            this.StartButton.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.StartButton.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.StartButton.Location = new System.Drawing.Point(11, 126);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(150, 52);
            this.StartButton.TabIndex = 29;
            this.StartButton.Text = "START";
            this.StartButton.UseMnemonic = false;
            this.StartButton.UseVisualStyleBackColor = false;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.CloseButton.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.CloseButton.Location = new System.Drawing.Point(167, 126);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(146, 51);
            this.CloseButton.TabIndex = 30;
            this.CloseButton.Text = "CLOSE";
            this.CloseButton.UseVisualStyleBackColor = false;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // LabelFile
            // 
            this.LabelFile.AutoSize = true;
            this.LabelFile.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.LabelFile.Location = new System.Drawing.Point(7, 248);
            this.LabelFile.Name = "LabelFile";
            this.LabelFile.Size = new System.Drawing.Size(56, 13);
            this.LabelFile.TabIndex = 27;
            this.LabelFile.Text = "DEFAULT";
            // 
            // TransmissionLabel
            // 
            this.TransmissionLabel.AutoSize = true;
            this.TransmissionLabel.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.TransmissionLabel.Location = new System.Drawing.Point(224, 77);
            this.TransmissionLabel.Name = "TransmissionLabel";
            this.TransmissionLabel.Size = new System.Drawing.Size(56, 13);
            this.TransmissionLabel.TabIndex = 28;
            this.TransmissionLabel.Text = "DEFAULT";
            // 
            // button1
            // 
            this.button1.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.button1.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button1.Location = new System.Drawing.Point(10, 183);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(303, 62);
            this.button1.TabIndex = 25;
            this.button1.Text = "Send To Client";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.SendButton_Click);
            // 
            // SERVER
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.ClientSize = new System.Drawing.Size(363, 280);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.LabelFile);
            this.Controls.Add(this.TransmissionLabel);
            this.Controls.Add(this.button1);
            this.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.Name = "SERVER";
            this.Text = "SERVER";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Label LabelFile;
        private System.Windows.Forms.Label TransmissionLabel;
        private System.Windows.Forms.Button button1;

    }
}

