﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Windows.Forms;
using TSU.Common.Instruments.Adapter;
using TSU.Views;
using P = TSU.Tsunami2.Preferences;
using R = TSU.Properties.Resources;

namespace TSU.Connectivity_Tests
{
    public class TestDllView : TsuView
    {
        public TestDllView()
            : base(null)
        {
            InitializeComponent();
            this.ApplyThemeColors();
            //this.BackgroundImage = Image.FromFile("C:\\$PS\\My codes\\SVN-tSUnami\\src\\Graphic\\tsunami_32x32.ico");
            //this.Icon = new Icon("C:\\$PS\\My codes\\SVN-tSUnami\\src\\Graphic\\tsunami_32x32.ico");
            //this.Text = "tSUnami dependencies test";

            //Creation bouton + evenement clic
            Button btnGEOCOM105 = new Button();
            btnGEOCOM105.Text = "GCom105";
            btnGEOCOM105.Left = 20;
            btnGEOCOM105.Top = 170;
            btnGEOCOM105.Refresh();
            this.Controls.Add(btnGEOCOM105);
            btnGEOCOM105.Click += new EventHandler(btnGEOCOM105_clic);

            //Creation bouton + evenement clic
            Button btnGEOCOM32 = new Button();
            btnGEOCOM32.Text = "GeoCom32";
            btnGEOCOM32.Left = 20;
            btnGEOCOM32.Top = 200;
            btnGEOCOM32.Refresh();
            this.Controls.Add(btnGEOCOM32);
            btnGEOCOM32.Click += new EventHandler(btnGEOCOM32_clic);

            //Creation bouton + evenement clic
            Button btnDNA03 = new Button();
            btnDNA03.Text = "DNA 03";
            btnDNA03.Left = 20;
            btnDNA03.Top = 230;
            btnDNA03.Refresh();
            this.Controls.Add(btnDNA03);
            btnDNA03.Click += new EventHandler(btnDNA03_clic);

            panel1.Controls.Add(new BigButton(
                $"{R.T_ADD_AN_ADVANCED_MODULE};{R.T_ADD_ONE_OF_THE_FOLLOWING_MODULE_WHICH_WILL_GIVE_YOU_COMPLETE_FREEDOM_OF_USE_THEODOLITE_LEVELLING_TILT_OR_OFFSET_MODULE_OR_OBJECT_MANAGERS}",
               R.AdvancedModule, test, P.Theme.Colors.Highlight));
        }
        private void ApplyThemeColors()
        {

        }
        public void test()
        {

        }



        //Connection DNA03
        public bool DNA_StartConnect(ref SerialPort port) //initialise la connection série et ouvre le port
        {
            try
            {
                port.BaudRate = 9600;
                port.Parity = Parity.None;
                port.StopBits = StopBits.One;
                port.DataBits = 8;
                port.Open();
                return port.IsOpen;
            }
            catch (IOException)
            {
                return false;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        //initialise les paramètres
        public bool DNA_Init(ref SerialPort port) //initialise les paramètres du DNA
        {
            try
            {
                Thread.Sleep(50);
                //DNA ON
                port.Write("a" + "\r\n");
                Thread.Sleep(10000);
                //DNA CLEAR
                port.Write("c" + "\r\n");
                Thread.Sleep(50);
                //PURGE PORT
                DNA_Purge(ref port);
                //Temps avant erreur de timeout lors de lecture et écriture sur port série
                port.ReadTimeout = 10000;
                port.WriteTimeout = 100;
                port.Write("SET/30/1\r\n");//Met le BIP en Medium
                Thread.Sleep(50);
                DNA_Purge(ref port);
                port.Write("SET/41/0\r\n");//UNITES METRES
                Thread.Sleep(50);
                DNA_Purge(ref port);
                port.Write("SET/51/5/r/n");//5 DECIMALES
                Thread.Sleep(50);//PROTOCOL ON 
                DNA_Purge(ref port);
                port.Write("SET/75/1\r\n");
                Thread.Sleep(50);//Correction earth curvature off
                DNA_Purge(ref port);
                port.Write("SET/125/0\r\n");
                Thread.Sleep(50);//SORTIE RS232
                DNA_Purge(ref port);
                port.Write("SET/76/1\r\n");//Mire MODE NORMAL
                Thread.Sleep(50);
                DNA_Purge(ref port);
                port.Write("SET/127/0\r\n");
                Thread.Sleep(50);//GSI8
                DNA_Purge(ref port);
                port.Write("SET/137/0\r\n"); //GSI8
                Thread.Sleep(50);
                DNA_Purge(ref port);
                port.Write("SET/95/0\r\n"); //AUTO OFF OFF
                Thread.Sleep(50);
                DNA_Purge(ref port);
                return true;
                // Gestion erreur
            }
            //    catch (IOException ex)
            //{
            //    MessageBox.Show(The port is in an invalid state.- or -An attempt to set the state of the underlying port failed.");
            //    return false;
            //}

            //catch (InvalidDataException ex)
            //{
            //    MessageBox.Show(The stream is closed. This can occur because the Open method has not been called or the Close method has been called.");
            //    return false;
            //}

            //catch (TimeoutException ex)
            //    {
            //    MessageBox.Show(The WriteLine method could not write to the stream.");
            //    return false;
            //    }
            //catch (InvalidOperationException ex)
            //    {
            //    MessageBox.Show(The specified port is not open.");
            //    return false;
            //    }
            //     catch (ArgumentNullException ex)
            //    {
            //    MessageBox.Show(The str parameter is null.");
            //    return false;
            //    }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        //controle de la charge de la batterie
        public bool DNA_Batterie(ref SerialPort port, ref int iLvlBat) //donne la charge batterie
        {
            try
            {
                string sBat = string.Empty;
                DNA_Purge(ref port);
                port.Write("CONF/90\r\n"); //BATTERY level
                Thread.Sleep(50);
                sBat = port.ReadLine();
                DNA_Purge(ref port);
                int iFound = sBat.IndexOf("/");
                sBat = sBat.Substring(iFound + 1, 4); //lit le substring après le / contenant le niveau bat
                iLvlBat = Convert.ToInt32(sBat);
                iLvlBat = iLvlBat * 10; // convertit en %
                return true;
            }
            //catch (IOException ex)
            //{
            //    MessageBox.Show(The port is in an invalid state.- or -An attempt to set the state of the underlying port failed.");
            //    return false;
            //}

            //catch (InvalidDataException ex)
            //{
            //    MessageBox.Show(The stream is closed. This can occur because the Open method has not been called or the Close method has been called.");
            //    return false;
            //}

            //catch (TimeoutException ex)
            //    {
            //    MessageBox.Show(The WriteLine method could not write to the stream.");
            //    return false;
            //    }
            //catch (InvalidOperationException ex)
            //    {
            //    MessageBox.Show(The specified port is not open.");
            //    return false;
            //    }
            //     catch (ArgumentNullException ex)
            //    {
            //    MessageBox.Show(The str parameter is null.");
            //    return false;
            //    }
            //}  
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        public bool DNA_CloseConnect(ref SerialPort port) //ferme la connection au DNA et éteint le DNA
        {
            try
            {
                port.Write("b\r\n");
                //Thread.Sleep(5000);
                port.Close();
                return !port.IsOpen;
            }
            //catch (IOException ex)
            //{
            //    MessageBox.Show ( "Th
            //}

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        public bool DNA_Purge(ref SerialPort port) //Purge le buffer entrée et sortie
        {
            try
            {
                port.DiscardOutBuffer();
                port.DiscardInBuffer();
                return true;
            }
            //catch (IOException ex)
            //{
            //    MessageBox.Show(The port is in an invalid state.- or -An attempt to set the state of the underlying port failed.");
            //    return false;
            //}

            //catch (InvalidDataException ex)
            //{
            //    MessageBox.Show("The stream is closed. This can occur because the Open method has not been called or the Close method has been called.");
            //    return false;
            //}
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        public bool DNA_StaticMeasure(ref SerialPort port) //ref string[] hauteur
        {
            try
            {
                string[] arrayMesBrute = new string[3];
                for (int i = 0; i < 3; i++)
                {
                    port.Write("GET/M/WI32/WI330" + "\r\n"); //demande une mesure hauteur et distance
                    arrayMesBrute[i] = port.ReadLine();
                    DNA_Purge(ref port);
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        public bool DNA_ContinuousMeasure(ref SerialPort port) //ref string[] hauteur
        {
            try
            {
                string[] arrayMesBrute = new string[10];
                port.Write("GET/C/WI32/WI330" + "\r\n"); //demande une mesure hauteur et distance
                for (int i = 0; i < 3; i++)
                {
                    arrayMesBrute[i] = port.ReadLine();
                    //DNA_Purge(ref port);
                }
                port.Write("c+\r\n");
                string buf;
                buf = Convert.ToString(port.ReadExisting());
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        public List<string> GetAllPorts()
        {
            List<String> allPorts = new List<String>();
            foreach (String portName in SerialPort.GetPortNames())
            {
                allPorts.Add(portName);
            }
            return allPorts;
        }
        // test GeoCom32.dll
        #region GeoCom32 Tests
        // test GeoCom32.dll

        private void btnGEOCOM32_clic(object sender, EventArgs e)
        {
            int iRetcode32 = 0;

            // initialize Geocom
            iRetcode32 = GeoCom32_dll.VB_COM_Init();
            MessageBox.Show("VB_COM_Init iRetcode = " + iRetcode32.ToString());
            int iPort32 = (int)GeoCom32_dll.COM_PORT.COM_1;
            int iRate32 = (int)GeoCom32_dll.COM_BAUD_RATE.COM_BAUD_19200;
            MessageBox.Show("COMPORT = " + iPort32.ToString());
            iRetcode32 = GeoCom32_dll.VB_COM_OpenConnection(iPort32, iRate32, 2);
            MessageBox.Show("VB_COM_OpenConnection iRetcode = " + iRetcode32.ToString());

            //Test port com
            iRetcode32 = GeoCom32_dll.VB_COM_NullProc();
            MessageBox.Show("VB_COM_NullProc iRetcode = " + iRetcode32.ToString());

            //Mise OFF et ON ATR
            int iSetATROnOff32 = (int)GeoCom32_dll.ON_OFF_TYPE.OFF;
            int iStatusATR32 = (int)GeoCom32_dll.ON_OFF_TYPE.ON;
            GeoCom32_dll.VB_AUT_SetATRStatus(ref iSetATROnOff32);
            GeoCom32_dll.VB_AUT_GetATRStatus(ref iStatusATR32);
            MessageBox.Show("ATRStatus = " + Enum.GetName(typeof(GeoCom32_dll.ON_OFF_TYPE), iStatusATR32));
            iSetATROnOff32 = (int)GeoCom32_dll.ON_OFF_TYPE.ON;
            GeoCom32_dll.VB_AUT_SetATRStatus(ref iSetATROnOff32);
            GeoCom32_dll.VB_AUT_GetATRStatus(ref iStatusATR32);
            MessageBox.Show("ATRStatus = " + Enum.GetName(typeof(GeoCom32_dll.ON_OFF_TYPE), iStatusATR32));

            // Mesure ATR +dist
            double dDSrchHz32 = 0.016;
            double dDSrchV32 = 0.016;
            int iDummy32 = (int)GeoCom32_dll.BOOLE.FALSE;
            GeoCom32_dll.VB_AUT_FineAdjust3(ref dDSrchHz32, ref dDSrchV32, ref iDummy32);
            MessageBox.Show("Ajustement ATR fini");
            int iMeasureCommand32 = (int)GeoCom32_dll.TMC_MEASURE_PRG.TMC_DEF_DIST;
            int iInclineCommand32 = (int)GeoCom32_dll.TMC_INCLINE_PRG.TMC_AUTO_INC;
            GeoCom32_dll.VB_TMC_DoMeasure(ref iMeasureCommand32, ref iInclineCommand32);
            MessageBox.Show("Mesure en cours...");
            int iWaitTime32 = 5000;
            GeoCom32_dll.TMC_HZ_V_ANG SAngle_Hz_V32 = new GeoCom32_dll.TMC_HZ_V_ANG(0, 0);
            double dSlopeDistance32 = 0;
            GeoCom32_dll.VB_TMC_GetSimpleMea(ref iWaitTime32, ref SAngle_Hz_V32, ref dSlopeDistance32, ref iInclineCommand32);
            MessageBox.Show(string.Format("HZ =  {0}  V=  {1}  Distance =  {3}", SAngle_Hz_V32.dHz.ToString(), SAngle_Hz_V32.dV.ToString(), dSlopeDistance32.ToString()));

            // Changemenent face et mesure ATR + dist
            int iPosMode32 = (int)GeoCom32_dll.AUT_POSMODE.AUT_PRECISE;
            int iATRMode32 = (int)GeoCom32_dll.AUT_ATRMODE.AUT_POSITION;
            GeoCom32_dll.VB_AUT_ChangeFace4(ref iPosMode32, ref iATRMode32, ref iDummy32);
            MessageBox.Show("Changement face fini");
            GeoCom32_dll.VB_AUT_FineAdjust3(ref dDSrchHz32, ref dDSrchV32, ref iDummy32);
            MessageBox.Show("Ajustement ATR fini");
            GeoCom32_dll.VB_TMC_DoMeasure(ref iMeasureCommand32, ref iInclineCommand32);
            MessageBox.Show("Mesure en cours...");
            GeoCom32_dll.VB_TMC_GetSimpleMea(ref iWaitTime32, ref SAngle_Hz_V32, ref dSlopeDistance32, ref iInclineCommand32);
            MessageBox.Show(string.Format("HZ =  {0}  V=  {1}  Distance =  {3}", SAngle_Hz_V32.dHz.ToString(), SAngle_Hz_V32.dV.ToString(), dSlopeDistance32.ToString()));

            // Arret du TPS
            int iOffMode32 = (int)GeoCom32_dll.COM_TPS_STOP_MODE.COM_TPS_SHUT_DOWN;
            GeoCom32_dll.VB_COM_SwitchOffTPS(ref iOffMode32);
            // Fermeture Connection
            iRetcode32 = GeoCom32_dll.VB_COM_CloseConnection();
            MessageBox.Show("VB_COM_CloseConnection iRetcode = " + iRetcode32.ToString());
            iRetcode32 = GeoCom32_dll.VB_COM_End();
            MessageBox.Show("VB_COM_End iRetcode = " + iRetcode32.ToString());
        }
        #endregion
        // Test Gcom105.dll
        #region GeoCom105 Tests
        // Test Gcom105.dll
        private void btnGEOCOM105_clic(object sender, EventArgs e)
        {
            int iRetcode105 = 0;
            // initialize Geocom
            iRetcode105 = GeoCom105_dll.VB_COM_Init();
            MessageBox.Show("VB_COM_Init iRetcode = " + iRetcode105.ToString());
            int iPort105 = (int)GeoCom105_dll.COM_PORT.COM_3;
            int iRate105 = (int)GeoCom105_dll.COM_BAUD_RATE.COM_BAUD_19200;
            MessageBox.Show("COMPORT = " + iPort105.ToString());
            iRetcode105 = GeoCom105_dll.VB_COM_OpenConnection(iPort105, ref iRate105, 2);
            MessageBox.Show("VB_COM_OpenConnection iRetcode = " + iRetcode105.ToString());

            //Test port com
            iRetcode105 = GeoCom105_dll.VB_COM_NullProc();
            MessageBox.Show("VB_COM_NullProc iRetcode = " + iRetcode105.ToString());

            //Mise OFF et ON ATR
            int iSetATROnOff105 = (int)GeoCom105_dll.ON_OFF_TYPE.OFF;
            int iStatusATR105 = (int)GeoCom105_dll.ON_OFF_TYPE.ON;
            GeoCom105_dll.VB_AUT_SetATRStatus(ref iSetATROnOff105);
            GeoCom105_dll.VB_AUT_GetATRStatus(ref iStatusATR105);
            MessageBox.Show("ATRStatus = " + Enum.GetName(typeof(GeoCom105_dll.ON_OFF_TYPE), iStatusATR105));
            iSetATROnOff105 = (int)GeoCom105_dll.ON_OFF_TYPE.ON;
            GeoCom105_dll.VB_AUT_SetATRStatus(ref iSetATROnOff105);
            GeoCom105_dll.VB_AUT_GetATRStatus(ref iStatusATR105);
            MessageBox.Show("ATRStatus = " + Enum.GetName(typeof(GeoCom105_dll.ON_OFF_TYPE), iStatusATR105));

            // Mesure ATR +dist
            double dDSrchHz105 = 0.016;
            double dDSrchV105 = 0.016;
            int iDummy105 = (int)GeoCom105_dll.BOOLE.FALSE;
            GeoCom105_dll.VB_AUT_FineAdjust3(ref dDSrchHz105, ref dDSrchV105, ref iDummy105);
            MessageBox.Show("Ajustement ATR fini");
            int iMeasureCommand105 = (int)GeoCom105_dll.TMC_MEASURE_PRG.TMC_DEF_DIST;
            int iInclineCommand105 = (int)GeoCom105_dll.TMC_INCLINE_PRG.TMC_AUTO_INC;
            GeoCom105_dll.VB_TMC_DoMeasure(ref iMeasureCommand105, ref iInclineCommand105);
            MessageBox.Show("Mesure en cours...");
            int iWaitTime105 = 5000;
            GeoCom105_dll.TMC_HZ_V_ANG SAngle_Hz_V105 = new GeoCom105_dll.TMC_HZ_V_ANG(0, 0);
            double dSlopeDistance105 = 0;
            GeoCom105_dll.VB_TMC_GetSimpleMea(ref iWaitTime105, ref SAngle_Hz_V105, ref dSlopeDistance105, ref iInclineCommand105);
            MessageBox.Show(string.Format("HZ = {0} V= {1} Distance = {3}", SAngle_Hz_V105.dHz.ToString(), SAngle_Hz_V105.dV.ToString(), dSlopeDistance105.ToString()));

            // Changemenent face et mesure ATR + dist
            int iPosMode105 = (int)GeoCom105_dll.AUT_POSMODE.AUT_PRECISE;
            int iATRMode105 = (int)GeoCom105_dll.AUT_ATRMODE.AUT_POSITION;
            GeoCom105_dll.VB_AUT_ChangeFace4(ref iPosMode105, ref iATRMode105, ref iDummy105);
            MessageBox.Show("Changement face fini");
            GeoCom105_dll.VB_AUT_FineAdjust3(ref dDSrchHz105, ref dDSrchV105, ref iDummy105);
            MessageBox.Show("Ajustement ATR fini");
            GeoCom105_dll.VB_TMC_DoMeasure(ref iMeasureCommand105, ref iInclineCommand105);
            MessageBox.Show("Mesure en cours...");
            GeoCom105_dll.VB_TMC_GetSimpleMea(ref iWaitTime105, ref SAngle_Hz_V105, ref dSlopeDistance105, ref iInclineCommand105);
            MessageBox.Show(string.Format("HZ = {0}  V = {1}  Distance = {3}", SAngle_Hz_V105.dHz.ToString(), SAngle_Hz_V105.dV.ToString(), dSlopeDistance105.ToString()));

            // Mesure sans réflecteur
            int iOnOffLaser = (int)GeoCom105_dll.ON_OFF_TYPE.ON;
            int iSetMeasProg105 = (int)GeoCom105_dll.BAP_USER_MEASPRG.SINGLE_RLESS_VISIBLE;
            iSetATROnOff105 = (int)GeoCom105_dll.ON_OFF_TYPE.OFF;
            double dHzGoPos = SAngle_Hz_V105.dHz - 0.09; //nouvelle position Hz désirée
            double dVGoPos = SAngle_Hz_V105.dV - 0.01; //nouvelle position V désirée
            int bDummy = (int)GeoCom105_dll.BOOLE.FALSE;
            GeoCom105_dll.VB_AUT_SetATRStatus(ref iSetATROnOff105);
            GeoCom105_dll.VB_EDM_Laserpointer(ref iOnOffLaser);
            GeoCom105_dll.VB_AUT_MakePositioning4(ref dHzGoPos, ref dVGoPos, ref iPosMode105, ref iATRMode105, ref bDummy);
            MessageBox.Show("Vérifier mise en marche laser");
            GeoCom105_dll.VB_BAP_SetMeasPrg(ref iSetMeasProg105);
            iMeasureCommand105 = (int)GeoCom105_dll.TMC_MEASURE_PRG.TMC_DEF_DIST;
            GeoCom105_dll.VB_TMC_DoMeasure(ref iMeasureCommand105, ref iInclineCommand105);
            MessageBox.Show("Mesure en cours...");
            GeoCom105_dll.VB_TMC_GetSimpleMea(ref iWaitTime105, ref SAngle_Hz_V105, ref dSlopeDistance105, ref iInclineCommand105);
            MessageBox.Show(string.Format("HZ =  {0}  V=  {1}  Distance =  {3}", SAngle_Hz_V105.dHz.ToString(), SAngle_Hz_V105.dV.ToString(), dSlopeDistance105.ToString()));
            dHzGoPos = SAngle_Hz_V105.dHz + 0.09; //retourne position Hz départ
            dVGoPos = SAngle_Hz_V105.dV + 0.01; //retourne position V départ
            GeoCom105_dll.VB_AUT_MakePositioning4(ref dHzGoPos, ref dVGoPos, ref iPosMode105, ref iATRMode105, ref bDummy);
            // Arret du TPS
            int iOffMode105 = (int)GeoCom105_dll.COM_TPS_STOP_MODE.COM_TPS_SHUT_DOWN;
            GeoCom105_dll.VB_COM_SwitchOffTPS(ref iOffMode105);
            // Fermeture Connection
            iRetcode105 = GeoCom105_dll.VB_COM_CloseConnection();
            MessageBox.Show("VB_COM_CloseConnection iRetcode = " + iRetcode105.ToString());
            iRetcode105 = GeoCom105_dll.VB_COM_End();
            MessageBox.Show("VB_COM_End iRetcode = " + iRetcode105.ToString());
        }
        #endregion
        #region Test DNA03
        //Test DNA 03
        private void btnDNA03_clic(object sender, EventArgs e)
        {
            //initialisation des variables communications DNA
            //mMax = 3;
            ////numMes = 0
            //Array.Clear(ArrayMes, 0, 3);
            //Array.Clear(ArrayDh, 0, 3);
            //mError = false;
            //mMeasuring = false;
            //mBat = "";
            //mBeep = "";
            //mUnit = "";
            //mDecimal = "";
            //mProtocol = "";
            //mCorrEarthCurv = "";
            //mRS232 = "";
            //mModMire = "";
            //mGSI = "";
            //mBaudrate = "";
            //mParity = "";
            //mTerminator = "";
            bool OK = false;
            SerialPort DNAPort = new SerialPort();
            DNAPort.PortName = "COM3";
            OK = DNA_StartConnect(ref DNAPort);
            MessageBox.Show("Connection " + Convert.ToString(OK));
            OK = DNA_Init(ref DNAPort);
            MessageBox.Show("Initialisation " + Convert.ToString(OK));
            int NivBat = 0;
            OK = DNA_Batterie(ref DNAPort, ref NivBat);
            MessageBox.Show("Niveau Batterie " + NivBat.ToString() + " %");
            string[] lecture = new string[3];
            DNA_ContinuousMeasure(ref DNAPort);
            OK = DNA_CloseConnect(ref DNAPort);
            MessageBox.Show("Fermeture " + Convert.ToString(OK));
        }
        #endregion

        private void InitializeComponent()
        {
            this.label1 = new Label();
            this.button1 = new Button();
            this.panel1 = new Panel();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new Size(107, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "TestdesDllExternes";
            // 
            // button1
            // 
            this.button1.Location = new Point(137, 12);
            this.button1.Name = "button1";
            this.button1.Size = new Size(93, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Advanced";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Location = new Point(448, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new Size(395, 88);
            this.panel1.TabIndex = 2;
            // 
            // TestDllView
            // 
            this.BackColor = P.Theme.Colors.Object;
            this.ClientSize = new Size(955, 369);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Name = "TestDllView";
            this.Load += new EventHandler(this.frmTsunamiTests_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private Label label1;
        private Button button1;
        private Panel panel1;

        private void frmTsunamiTests_Load(object sender, EventArgs e)
        {

        }
    }
}
