using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;
using GeoComClient;
using MM = TSU;
using TSU.Views;
using M = TSU;
using TSU.Logs;
using TSU.Common;
using TSU.Tools.Exploration;
using TSU.Views.Message;

namespace TSU.Preferences
{
    public partial class View : ModuleView
    {
        internal new Preferences Module
        {
            get
            {
                return this._Module as Preferences;
            }
        }

        Buttons buttons;
        BigButton languagesButton;
        BigButton usersButton;
        BigButton viewTypesButton;
        BigButton precisionsButton;
        BigButton rallongeButton;
        BigButton tolerancesButton;
        BigButton instrumentTolerancesButton;
        BigButton themeColorButton;
        BigButton daltonianButton;
        BigButton CommentedLinesButton;
        BigButton buttonAskTSULocation;

        private static readonly object padlock = new object();
        private static View instance;
        private bool buttonVisualChanged = true;
        public static View Instance(M.IModule module)
        {
            lock (padlock) // to be threat safe, no way to create 2 at the same time
            {
                if (instance == null)
                    instance = new View(module);
                return instance;
            }
        }

        private View(M.IModule module) : base(module)
        {
            this.Image = R.I_Settings;
            buttons = new Buttons(this);
            InitializeMenu();

            this.AdaptTopPanelToButtonHeight();
            this._PanelBottom.Padding = new Padding(10);
            viewTypesButton = (buttons.windowsStrategyTSU);
            this.UpdateView();
        }

        private void InitializeMenu()
        {
            //Creation of buttons
            buttons = new Buttons(this);

            //Main Button
            BigButton bigbuttonTop = new BigButton(
            //    string.Format("{0} {1} '{2}",R.T_GUI_PREF, "Preferences are saved in",TSU.Tsunami2.TsunamiPreferences.Values.Paths.GuiPreferences),
            $"{R.T_GUI_PREF} {R.T_PREFERENCES_ARE_SAVED_IN} '{TSU.Tsunami2.Preferences.Values.Paths.GuiPreferences}",
                R.I_Settings,
                this.ShowContextMenuGlobal, null, true);

            this._PanelTop.Controls.Clear();
            this._PanelTop.Controls.Add(bigbuttonTop);

            this.SetContextMenuToGlobal();

        }

        private void SetContextMenuToGlobal()
        {
            contextButtons.Clear();
            contextButtons.Add(buttons.close);
            //   contextButtons.Add(new BigButton("Restart Tsunami;To apply GUI modifications that require a restart of the application", R.Update, ()=>
            contextButtons.Add(new BigButton($"{R.T_RESTART_TSUNAMI};{R.T_TO_APPLY_GUI_MODIFICATIONS_THAT_REQUIRE_A_RESTART_OF_THE_APPLICATION}", R.Update, () =>
            {
                Application.Restart();
                Environment.Exit(0);
            }, null, false));

        }

        private void ShowContextMenuGlobal()
        {
            this.SetContextMenuToGlobal();
            this.ShowPopUpMenu(contextButtons);
        }

        public override void UpdateView()
        {
            //switch (TSU.Tsunami2.TsunamiPreferences.Values.GuiPrefs.UnitForExtension)
            //{
            //    case Preferences.DistanceUnit.m: rallongeButton = buttons.rallongeM; break;
            //    case Preferences.DistanceUnit.mm: 
            //    default: rallongeButton = buttons.rallongeMM; break;
            //}

            switch (TSU.Tsunami2.Preferences.Values.GuiPrefs.UserType)
            {
                case Preferences.UserTypes.Advanced: usersButton = buttons.advancedUser; break;
                case Preferences.UserTypes.Super: usersButton = buttons.superUser; break;
                case Preferences.UserTypes.Guided:
                default: usersButton = buttons.guidedUser; break;
            }

            switch (TSU.Tsunami2.Preferences.Values.GuiPrefs.Language)
            {
                case Preferences.SupportedLanguages.fran�ais: languagesButton = (buttons.LanguageFrancais); break;
                case Preferences.SupportedLanguages.russian: languagesButton = (buttons.LanguageRussian); break;
                case Preferences.SupportedLanguages.english:
                default: languagesButton = (buttons.LanguageEnglish); break;
            }

            switch (TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles)
            {
                case 2: precisionsButton = buttons.precisionMillimetric; break;
                case 3: precisionsButton = buttons.precisionDeciMillimetric; break;
                case 5: precisionsButton = buttons.precisionMicrometric; break;
                default: precisionsButton = buttons.precisionCentiMillimetric; break;
            }

            tolerancesButton = buttons.tolerances;
            instrumentTolerancesButton = buttons.instrumentTolerances;

            switch (TSU.Tsunami2.Preferences.Values.GuiPrefs.Theme.ThemeName)
            {
                case ENUM.ThemeColorName.Normal:
                    themeColorButton = buttons.ColorThemeNormal;
                    break;
                case ENUM.ThemeColorName.Sick:
                    themeColorButton = buttons.ColorThemeSick;
                    break;
                case ENUM.ThemeColorName.Pink:
                    themeColorButton = buttons.ColorThemePink;
                    break;
                case ENUM.ThemeColorName.RainBow:
                    themeColorButton = buttons.ColorThemeRainbow;
                    break;
                case ENUM.ThemeColorName.Dark:
                    themeColorButton = buttons.ColorThemeDark;
                    break;
                case ENUM.ThemeColorName.Light:
                    themeColorButton = buttons.ColorThemeLight;
                    break;
                case ENUM.ThemeColorName.Gray:
                    themeColorButton = buttons.ColorThemeGray;
                    break;
                default:
                    themeColorButton = buttons.ColorThemeNormal;
                    break;
            }
            if (TSU.Tsunami2.Preferences.Values.GuiPrefs.daltonien.IsTrue)
            {
                daltonianButton = buttons.ColorBlind;
            }
            else
                daltonianButton = buttons.NotColorBlind;

            if (TSU.Tsunami2.Preferences.Values.GuiPrefs.ShowCommentedLineInGeodeExport.IsTrue)
            {
                CommentedLinesButton = buttons.ShowComment;
            }
            else
                CommentedLinesButton = buttons.DontShowComment;

            M.Tsunami2.Preferences.Values.SaveGuiPrefs();

            int PADDING = 5;
            if (buttonVisualChanged)
            {
                var theme = TSU.Tsunami2.Preferences.Theme;
                Action<List<Control>> add = (controls) => { AddControlsLineInControl(this._PanelBottom, controls, DockStyle.Top); };

                Func<string, Label> label = (text) => { return new Label() {
                    Text = text,
                    Font = theme.Fonts.Normal,
                    ForeColor = theme.Colors.LightForeground,
                    AutoSize = true
                }; };

                //if (Tsunami2.Properties.Gadgets == null)
                //{
                //    var gadjet = new TSU.Tools.Gadgets.Module(Tsunami2.Properties);
                //    Tsunami2.Properties.Gadgets = gadjet;
                //}

                this.SuspendLayout();
                this._PanelBottom.Controls.Clear();

                add(new List<Control>() { label(R.T_GUI_PREF) });
                add(new List<Control>() { usersButton, languagesButton });
                add(new List<Control>() { viewTypesButton, precisionsButton });
                add(new List<Control>() { themeColorButton, daltonianButton });
                add(new List<Control>() { buttons.IncreaseTextSize, buttons.ReduceTextSize });
                add(new List<Control>() { instrumentTolerancesButton, tolerancesButton });
                add(new List<Control>() { buttons.buttonAskTSULocation, CommentedLinesButton });
                add(new List<Control>() { label($"{R.T_OTHER_SETTINGS}:") });
                add(new List<Control>() { Tsunami2.Properties.Menu.View.GetExploreButtons() });
                add(new List<Control>() { Tsunami2.Properties.Gadgets.View.buttons.commonPreferences, Tsunami2.Properties.Gadgets.View.buttons.personalPreferences });

                Tsunami2.Properties.Menu.UpdateView();
            }
            base.UpdateView();

            this.ResumeLayout();
        }

        private void ChangeWindowsViewStrategy2Tab()
        {
            (this.Module.ParentModule as Common.Tsunami).View.ChangeStrategy(TsunamiView.TsunamiViewsStrategies.Tab);
            viewTypesButton = buttons.windowsStrategyTAB;
            this.UpdateView();
        }

        private void ChangeWindowsViewStrategy2TSU()
        {
            (this.Module.ParentModule as Common.Tsunami).View.ChangeStrategy(TsunamiView.TsunamiViewsStrategies.Windows);
            viewTypesButton = buttons.windowsStrategyTSU;
            //Bring the Gui preferences back to the front,
            //because reloading the Windows Strategy may put another module in front instead
            this.BringToFront();
            this.UpdateView();
        }

        #region Deal With Access
        internal void ShowGuidedButtons()
        {
            TSU.Tsunami2.Preferences.Values.GuiPrefs.UserType = Preferences.UserTypes.Guided;
            usersButton = buttons.guidedUser;
            this.UpdateView();
        }
        internal void ShowAdvancedButtons()
        {
            string titleAndMessage1 = $"{R.T_PASSWORD};{R.T_PLEASE_ENTER_THE_PASSWORD_THAT_ALLOWS_YOU_TO_USE_ADVANCED_MODULES}";
            List<string> buttonTexts = new List<string> { R.T_OK, R.T_CANCEL };

            MessageTsu.ShowMessageWithTextBox(titleAndMessage1, buttonTexts, out string buttonClicked, out string textInput, R.T_ADVANCED, true);

            if (buttonClicked == R.T_CANCEL)
                return;

            if (textInput != R.T_ADVANCED)
            {
                string titleAndMessage = $"{R.T_WRONG_PASSWORD};{R.T_ACCES_DENIED}";
                new MessageInput(MessageType.Critical, titleAndMessage).Show();
                return;
            }

            TSU.Tsunami2.Preferences.Values.GuiPrefs.UserType = Preferences.UserTypes.Advanced;
            usersButton = buttons.advancedUser;
            this.UpdateView();
        }

        internal void ShowAllButtons()
        {
            TSU.Tsunami2.Preferences.Values.GuiPrefs.UserType = Preferences.UserTypes.Super;
            usersButton = buttons.superUser;
            this.UpdateView();
        }

        #endregion

        #region Languages

        internal void SwitchToFrench()
        {
            if (CheckIfPossible() == false)
                return;
            M.Tsunami2.Preferences.Values.SwitchLanguage(Preferences.SupportedLanguages.fran�ais);
            languagesButton = buttons.LanguageFrancais;
            this.UpdateView();
        }

        private bool CheckIfPossible()
        {
            if (TSU.Tsunami2.Preferences.Tsunami.MeasurementModules.Count > 0)
            {
                string titleAndMessage = $"{R.T_SORRY};{R.T_NO_LANGUAGE_SWITH}";
                new MessageInput(MessageType.Warning, titleAndMessage).Show();
                return false;
            }
            return true;
        }

        internal void SwitchToEnglish()
        {
            if (CheckIfPossible() == false)
                return;
            M.Tsunami2.Preferences.Values.SwitchLanguage(Preferences.SupportedLanguages.english);
            languagesButton = buttons.LanguageEnglish;
            this.UpdateView();
        }

        internal void SwitchToRussian()
        {
            if (CheckIfPossible() == false)
                return;
            M.Tsunami2.Preferences.Values.SwitchLanguage(Preferences.SupportedLanguages.russian);
            languagesButton = buttons.LanguageRussian;
            buttonVisualChanged = true;
            this.UpdateView();
        }

        #endregion

        #region precision

        private void SetPrecisionToMilli()
        {
            this.Module.GuiPrefs.NumberOfDecimalForAngles = 2;
            this.Module.GuiPrefs.NumberOfDecimalForDistances = 3;
            precisionsButton = buttons.precisionMillimetric;
            this.UpdateView();
        }


        private void SetPrecisionToDeciMilli()
        {
            this.Module.GuiPrefs.NumberOfDecimalForAngles = 3;
            this.Module.GuiPrefs.NumberOfDecimalForDistances = 4;
            precisionsButton = buttons.precisionDeciMillimetric;
            this.UpdateView();
        }

        private void SetPrecisionToCentiMilli()
        {
            this.Module.GuiPrefs.NumberOfDecimalForAngles = 4;
            this.Module.GuiPrefs.NumberOfDecimalForDistances = 5;
            precisionsButton = buttons.precisionCentiMillimetric;
            this.UpdateView();
        }

        private void SetPrecisionToMicro()
        {
            this.Module.GuiPrefs.NumberOfDecimalForAngles = 5;
            this.Module.GuiPrefs.NumberOfDecimalForDistances = 6;
            precisionsButton = buttons.precisionMicrometric;
            this.UpdateView();
        }

        #endregion

        #region tolerances in mm



        private void OpenInstrumentToleranceFile()
        {
            var path = Tsunami2.Preferences.Values.Paths;
            TSU.Shell.RunWithWaitingMessage(this, new List<string>() { path.InstrumentTolerances });
            this.Module.LoadInstrumentsTolerances();
        }

        private void OpenToleranceFile()
        {
            var path = Tsunami2.Preferences.Values.Paths;
            TSU.Shell.RunWithWaitingMessage(this, new List<string>() { path.Tolerances });
            this.Module.LoadTolerances();
        }


        #endregion

        #region color theme


        private void SetColorToSickTheme()
        {
            this.Module.SetThemeToSick();
            buttons = new Buttons(this);
            InitializeMenu();
            themeColorButton = buttons.ColorThemeSick;
            SetNewThemeColors();
            buttonVisualChanged = true;
            this.UpdateView();
        }



        private void SetColorToNormalTheme()
        {
            this.Module.SetThemeToNormal();
            buttons = new Buttons(this);
            InitializeMenu();
            themeColorButton = buttons.ColorThemeSick;
            SetNewThemeColors();
            buttonVisualChanged = true;
            this.UpdateView();
        }
        private void SetColorToLightTheme()
        {
            this.Module.SetThemeToLight();
            buttons = new Buttons(this);
            InitializeMenu();
            themeColorButton = buttons.ColorThemeLight;
            SetNewThemeColors();
            buttonVisualChanged = true;
            this.UpdateView();
        }

        private void SetColorToDarkTheme()
        {
            this.Module.SetThemeToDark();
            buttons = new Buttons(this);
            InitializeMenu();
            themeColorButton = buttons.ColorThemeDark;
            SetNewThemeColors();
            buttonVisualChanged = true;
            this.UpdateView();
        }

        private void SetColorToPinkTheme()
        {
            this.Module.SetThemeToPink();
            buttons = new Buttons(this);
            InitializeMenu();
            themeColorButton = buttons.ColorThemePink;
            SetNewThemeColors();
            buttonVisualChanged = true;
            this.UpdateView();
        }
        private void SetColorToRainbowTheme()
        {
            this.Module.SetThemeToRainbow();
            buttons = new Buttons(this);
            InitializeMenu();
            themeColorButton = buttons.ColorThemeRainbow;
            SetNewThemeColors();
            buttonVisualChanged = true;
            this.UpdateView();
        }
        private void SetColorToGrayTheme()
        {
            this.Module.SetThemeToGray();
            buttons = new Buttons(this);
            InitializeMenu();
            themeColorButton = buttons.ColorThemeGray;
            SetNewThemeColors();
            Tsunami2.Properties.Menu.View.CreateAllButtons();
            buttonVisualChanged = true;
            this.UpdateView();
        }
        private void SetNewThemeColors()
        {
            this.Module.tsunami.View.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.Module.tsunami.Menu.View.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.Module.tsunami.Menu.View._PanelBottom.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.Module.tsunami.Menu.View._PanelTop.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.Module.tsunami.View._PanelBottom.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.Module.tsunami.View._PanelTop.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this._PanelBottom.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this._PanelTop.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            viewTypesButton.SetColors(M.Tsunami2.Preferences.Theme.Colors.Object);
            Tsunami2.Properties.Menu.View.CreateAllButtons();

        }
        private void SetToColorBlind()
        {
            this.Module.GuiPrefs.daltonien.IsTrue = true;
            precisionsButton = buttons.ColorBlind;
            this.UpdateView();
        }

        private void SetToNotColorBlind()
        {
            this.Module.GuiPrefs.daltonien.IsTrue = false;
            precisionsButton = buttons.NotColorBlind;
            this.UpdateView();
        }

        #endregion

        class Buttons
        {
            private View view;

            public BigButton precisionMillimetric;
            public BigButton precisionDeciMillimetric;
            public BigButton precisionCentiMillimetric;
            public BigButton precisionMicrometric;

            public BigButton tolerances;
            public BigButton instrumentTolerances;

            public BigButton guidedUser;
            public BigButton advancedUser;
            public BigButton superUser;

            public BigButton windowsStrategyTSU;
            public BigButton windowsStrategyTAB;

            public BigButton close;

            public BigButton LanguageFrancais;
            public BigButton LanguageRussian;
            public BigButton LanguageEnglish;

            public BigButton ColorThemeNormal;
            public BigButton ColorThemeDark;
            public BigButton ColorThemeLight;
            public BigButton ColorThemeSick;
            public BigButton ColorThemePink;
            public BigButton ColorThemeRainbow;
            public BigButton ColorThemeGray;
            public BigButton IncreaseTextSize;
            public BigButton ReduceTextSize;


            public BigButton ColorBlind;
            public BigButton NotColorBlind;


            public BigButton buttonAskTSULocation;
            public BigButton ShowComment;
            public BigButton DontShowComment;

            public BigButton More;

            public Buttons(View view)
            {
                this.view = view;


                // Color
                buttonAskTSULocation = new BigButton(
                   ((this.view.Module.GuiPrefs.AskForTsuFileNameAndLocation.IsTrue) ?
                   R.T_ASK_FOR_TSU_LOC :
                   R.T_DONT_ASK_FOR_TSU_LOC) + ';' + R.T_Click_to_change,
                   R.Element_File, () =>
                   {
                       this.view.Module.GuiPrefs.AskForTsuFileNameAndLocation.IsTrue = !this.view.Module.GuiPrefs.AskForTsuFileNameAndLocation.IsTrue;
                       this.view.buttonAskTSULocation.SetAttributes(
                           ((this.view.Module.GuiPrefs.AskForTsuFileNameAndLocation.IsTrue) ?
                           R.T_ASK_FOR_TSU_LOC :
                           R.T_DONT_ASK_FOR_TSU_LOC) + ';' + R.T_Click_to_change, null, null);

                   }, null, false);


                // Color
                ColorThemeNormal = new BigButton(
                   R.T_NormalTheme,
                   R.I_Normal, this.view.SetColorToDarkTheme);
                ColorThemeDark = new BigButton(
                   R.T_Dark,
                  R.I_Dark, this.view.SetColorToLightTheme);
                ColorThemeLight = new BigButton(
                   R.T_Light,
                  R.I_Light, this.view.SetColorToSickTheme);
                ColorThemeSick = new BigButton(
                   R.T_Sick,
                  R.I_Sick, this.view.SetColorToPinkTheme);
                ColorThemePink = new BigButton(
                    R.T_PinkTheme,
                    R.I_PinkKawaii, this.view.SetColorToRainbowTheme);
                ColorThemeRainbow = new BigButton(
                    R.T_RainbowTheme,
                    R.I_Rainbow, this.view.SetColorToGrayTheme);
                ColorThemeGray = new BigButton(
                    R.T_GrayTheme,
                    R.I_GrayCloud, this.view.SetColorToNormalTheme);
                // user types
                guidedUser = new BigButton(
                  R.T_B_GuidedUser,
                  R.Guided, this.view.ShowAdvancedButtons);
                advancedUser = new BigButton(
                   R.T_BAdvancedUser,
                  R.Advanced, this.view.ShowAllButtons);
                superUser = new BigButton(
                   R.buttonSuperUser,
                  R.Super, this.view.ShowGuidedButtons);

                // rallonges
                //rallongeMM = new BigButton(
                //  R.T_B_RallongeMM,
                //  R.Rallonge, this.view.SetRallongeM);
                //rallongeM = new BigButton(
                //   R.T_B_RallongeM,
                //  R.Rallonge, this.view.SetRallongeMM);

                // Views
                windowsStrategyTSU = new BigButton(
                    R.T_buttonWindowsStrategyTSU,
                    R.Tsunami,
                    this.view.ChangeWindowsViewStrategy2Tab);
                windowsStrategyTAB = new BigButton(
                    R.T_buttonWindowsStrategyTAB,
                    R.View,
                    this.view.ChangeWindowsViewStrategy2TSU);

                close = new BigButton(
                  R.T_B_Close,
                  R.MessageTsu_Problem, this.view.CloseView, TSU.Tsunami2.Preferences.Theme.Colors.Bad);

                // Languages
                LanguageFrancais = new BigButton(string.Format("{0};{1}", R.T_FRANCAIS, R.T_TRADUCTION_PAR_GOOGLE_TRANSLATOR)
,
                   R.French, this.view.SwitchToRussian);
                LanguageRussian = new BigButton(string.Format("{0};{1}", R.T_RUSSIAN, R.T_NOT_YET_AVAILABLE)
,
                   R.Russian, this.view.SwitchToEnglish);
                LanguageEnglish = new BigButton(string.Format("{0};{1}", R.T_ENGLISH, R.T_ENGLISH)
,
                   R.English, this.view.SwitchToFrench);

                //Precision
                precisionMillimetric = new BigButton(
                    string.Format(R.T_PRECISIONS_MILLI, M.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles.ToString(), M.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances.ToString()),
                   R.Milli, this.view.SetPrecisionToDeciMilli, null, false);

                precisionDeciMillimetric = new BigButton(
                    string.Format(R.T_PRECISIONS_DECIMILLI, M.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles.ToString(), M.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances.ToString()),
                   R.DeciMilli, this.view.SetPrecisionToCentiMilli, null, false);

                precisionCentiMillimetric = new BigButton(
                    string.Format(R.T_PRECISIONS_CENTIMILLI, M.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles.ToString(), M.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances.ToString()),
                   R.CentiMilli, this.view.SetPrecisionToMicro, null, false);

                precisionMicrometric = new BigButton(
                    string.Format(R.T_PRECISIONS_MICRO, M.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles.ToString(), M.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances.ToString()),
                   R.Micro, this.view.SetPrecisionToMilli, null, false);

                // tolerance in mm
                instrumentTolerances = new BigButton(
                    string.Format(R.T_CHANGE_INSTRUMENT_TOLERANCES),
                   R.Tolerances, this.view.OpenInstrumentToleranceFile);


                tolerances = new BigButton(
                    string.Format(R.T_CHANGE_TOLERANCES),
                   R.Tolerances, this.view.OpenToleranceFile);


                // daltonien
                ColorBlind = new BigButton(
                    string.Format(R.T_COLORBLIND),
                   R.I_ColorBlind, this.view.SetToNotColorBlind);

                NotColorBlind = new BigButton(
                    string.Format(R.T_NOTCOLORBLIND),
                   R.I_ColorBlindNot, this.view.SetToColorBlind);

                // text size
                IncreaseTextSize = new BigButton(
                    string.Format($"{R.T_TEXT_SIZE_PLUS}; {R.T_INCREASE_THE_SIZE_OF_THE_TEXTS}"),
                   R.Comment, this.view.IncreaseTextSize);

                ReduceTextSize = new BigButton(
                    string.Format($"{R.T_TEXT_SIZE_MINUS};{R.T_REDUCE_THE_SIZE_OF_THE_TEXTS}"),
                   R.Comment, this.view.ReduceTextSize);

                // daltonien
                ShowComment = new BigButton(
                    string.Format($"{R.T_SHOW_COMMENTED_LINES};{R.T_THE_BAD_MEASURES_TEMPORARY_MEASURE_ETC_WILL_BE_EXPORTED_IN_THE_FILES_WITH_A_AT_THE_BEGINNING_OF_THE_LINE}"),
                   R.Comment, this.view.SetDontShowComment);

                DontShowComment = new BigButton(
                    string.Format($"{R.T_DONT_SHOW_COMMENTED_LINES};{R.T_THE_BAD_MEASURES_TEMPORARY_MEASURE_ETC_WILL_NOT_BE_EXPORTED_IN_THE_FILES}"),
                   R.Comment, this.view.SetShowComment);

                // Extra
                More = new BigButton(
                    string.Format($"{R.BT_More}"),
                   R.Add, this.view.ShowMorePreferences);
            }
        }

        private void ShowMorePreferences()
        {
            Explorator ex = Explorator.Instance(Tsunami2.Properties);
            ex._TsuView.ShowOnTop();
            Tsunami2.Properties.Add(ex);

        }

        private void ReduceTextSize()
        {
            ChangeTextSize(.9);

        }
        private void IncreaseTextSize()
        {
            ChangeTextSize(1.1);
        }

        private void ChangeTextSize(double factor)
        {
            var theme = this.Module.GuiPrefs.Theme;
            var fonts = theme.Fonts;
            //if (factor > 0 && fonts.LargeTextSize > 30) return;
            //if (factor < 0 && fonts.LargeTextSize < 4) return;
            fonts.NormalTextSize = (int)(fonts.NormalTextSize * factor);
            fonts.LargeTextSize = (int)(fonts.LargeTextSize * factor);
            fonts.TinyTextSize = (fonts.TinyTextSize < 10 && factor > 1) ? (int)(fonts.TinyTextSize + 1): (int) (fonts.TinyTextSize * factor);
            theme.ButtonHeight = (int)(theme.ButtonHeight * factor);
            this.RecreateButtons();

            // change at least the font size for the 'text size' button for theuser to see the result after restarting
            Font newTitleFont = new Font(fonts.Normal.FontFamily, fonts.NormalTextSize);
            Font newDescriptionFont = new Font(fonts.Normal.FontFamily, fonts.TinyTextSize);
            this.buttons.IncreaseTextSize.SetTextSize(newTitleFont, newDescriptionFont);
            this.buttons.ReduceTextSize.SetTextSize(newTitleFont, newDescriptionFont);

            this.UpdateView();
        }

        private void RecreateButtons()
        {
            this.buttons = new Buttons(this);
        }

        private void SetDontShowComment()
        {
            this.Module.GuiPrefs.ShowCommentedLineInGeodeExport.Set(false);
            CommentedLinesButton = buttons.DontShowComment;
            this.UpdateView();
        }

        private void SetShowComment()
        {
            this.Module.GuiPrefs.ShowCommentedLineInGeodeExport.Set(true);
            CommentedLinesButton = buttons.ShowComment;
            this.UpdateView();
        }

        private void CloseView()
        {
            Tsunami2.Properties.Remove(this.Module);
        }
    }
}
