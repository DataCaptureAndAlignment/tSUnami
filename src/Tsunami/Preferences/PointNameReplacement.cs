﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSU.Preferences
{
    public class PointNameReplacement
    {
        public List<string> separators { get; set; } = new List<string>() { "-", "_", "." };
        public List<List<string>> parts { get; set; } = new List<List<string>>();

        public PointNameReplacement()
        {
            parts.Add(new List<string>() { "U", "D" });
            parts.Add(new List<string>() { "R", "L" });
            parts.Add(new List<string>() { "A", "C" });
            parts.Add(new List<string>() { "J", "S" });
            parts.Add(new List<string>() { "T", "B" });
        }
    }
}
