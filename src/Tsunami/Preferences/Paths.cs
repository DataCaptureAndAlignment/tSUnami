﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management.Instrumentation;
using System.Threading;
using TSU.Common.Compute.Compensations.BeamOffsets;
using TSU.Logs;
using TSU.Views.Message;
using R = TSU.Properties.Resources;

namespace TSU.Preferences
{
    public class Paths
    {
        private enum FilePathsKeys
        {
            TheodoliteCollimations,
            Etalonnages,
            Operations,
            Instruments,
            Data,
            FichierTheo,
            Dependencies,
            Addins,
            At40xIps,
            SSID_and_IP,
            ZoneRotations,
            Zones,
            InstrumentSlashTargetCoupleSigmas,
            Temporary,
            Preferences,
            Preferences_sync,
            SocketCodes,
            SocketCodesPairedWithKeyWords,
            GUIPreferences,
            Measures,
            Backup,
            Tolerances,
            InstrumentTolerances,
            Templates,
            Links
        }

        readonly Preferences preferences;

        public Paths()
        {
        }
        public Paths(Preferences preferences)
        {
            this.preferences = preferences;
        }

        public string MeasureOfTheDay
        {
            get
            {
                string _default = GetPath(FilePathsKeys.Measures) + string.Format("{0:yyyyMMdd}", DateTime.Now) + "\\";

                if (Tsunami2.Properties == null) return _default;

                if (Tsunami2.Properties.PathOfTheSavedFile == "") return _default;

                return Path.GetDirectoryName(Tsunami2.Properties.PathOfTheSavedFile) + "\\";
            }
        }

        public string PersonnalPreferencesPath
        {
            get
            {
                return GetPath(FilePathsKeys.Preferences);
            }
        }

        public string CommonPreferencesPath
        {
            get
            {
                string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                if (baseDirectory.Last() != '\\')
                    baseDirectory += '\\';
                return baseDirectory + @"Preferences\";
            }
        }
        public string SyncPreferencesPath
        {
            get
            {
                return GetPath(FilePathsKeys.Preferences_sync);
            }
        }

        public List<string> LocalHelp
        {
            get
            {
                string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                if (baseDirectory.Last() != '\\')
                    baseDirectory += '\\';
                return Directory.GetFiles(baseDirectory + @"Resources\HELP\", "*.*",
                                         SearchOption.TopDirectoryOnly).ToList();
            }
        }

        public string Dependencies
        {
            get
            {
                string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                if (baseDirectory.Last() != '\\')
                    baseDirectory += '\\';
                return baseDirectory + @"Preferences\" + GetPath(FilePathsKeys.Dependencies);
            }
        }


        public string Temporary
        {
            get
            {
                TSU.Debug.WriteInConsole(FilePathsKeys.Temporary);
                return GetPath(FilePathsKeys.Temporary);
            }
        }

        public string Data
        {
            get
            {
                return GetPath(FilePathsKeys.Data);
            }
        }

        public  string forcedMeasureFolder = "";
        public string Measures
        {
            get
            {
                if (forcedMeasureFolder == "")
                    return GetPath(FilePathsKeys.Measures);
                else
                    return forcedMeasureFolder;
            }
        }

        public string Templates
        {
            get
            {
                return GetPath(FilePathsKeys.Templates);
            }
        }

        public string Links
        {
            get
            {
                string path = @"c:\data\tsunami\lnk\"; // we want this one to be short event if PATHS is not initialiszed
                if (!System.IO.File.Exists(path))
                    System.IO.Directory.CreateDirectory(path);
                return path;
            }
        }

        public string PersonalPreferencesPath
        {
            get
            {
                string path = @"C:\data\tsunami\Preferences\";
                if (!System.IO.Directory.Exists(path))
                    System.IO.Directory.CreateDirectory(path);
                return path + "local_chantier.dat";
            }
        }
        

        private string latestFichierTheoPath = "";

        public string FichierTheo
        {
            get
            {
                if (latestFichierTheoPath == "")
                    return GetPath(FilePathsKeys.FichierTheo);
                else
                    return latestFichierTheoPath;
            }

            set
            {
                latestFichierTheoPath = value;
            }
        }

        public string Backups
        {
            get
            {
                return GetPath(FilePathsKeys.Backup);
            }
        }

        #region overridable preferences

        public string Zones
        {
            get
            {
                return GetTheRightPreferencesPath(GetPath(FilePathsKeys.Zones),
                    forceLocalExistance: false,
                    proposeToCreateLocalFromCommon: false,
                    copyFromCommon: false);
            }
        }

        public string Operations
        {
            get
            {
                return GetTheRightPreferencesPath(GetPath(FilePathsKeys.Operations),
                    forceLocalExistance: false,
                    proposeToCreateLocalFromCommon: false,
                    copyFromCommon: false);
            }
        }

        public string SocketCodes
        {
            get
            {
                return GetTheRightPreferencesPath(GetPath(FilePathsKeys.SocketCodes),
                    forceLocalExistance: false,
                    proposeToCreateLocalFromCommon: false,
                    copyFromCommon: false);
            }
        }

        public string SocketCodesPairedWithKeyWords
        {
            get
            {
                return GetTheRightPreferencesPath(GetPath(FilePathsKeys.SocketCodesPairedWithKeyWords),
                    forceLocalExistance: false,
                    proposeToCreateLocalFromCommon: false,
                    copyFromCommon: false);
            }
        }


        public string Etalonnages
        {
            get
            {
                return GetTheRightPreferencesPath(GetPath(FilePathsKeys.Etalonnages),
                    forceLocalExistance: true,
                    proposeToCreateLocalFromCommon: false,
                    copyFromCommon: false);
            }
        }

        public string InstrumentSlashTargetCoupleSigmas
        {
            get
            {
                return GetTheRightPreferencesPath(GetPath(FilePathsKeys.InstrumentSlashTargetCoupleSigmas),
                    forceLocalExistance: false,
                    proposeToCreateLocalFromCommon: false,
                    copyFromCommon: false);
            }
        }

        public string InstrumentTolerances
        {
            get
            {
                return GetTheRightPreferencesPath(GetPath(FilePathsKeys.InstrumentTolerances),
                    forceLocalExistance: false,
                    proposeToCreateLocalFromCommon: false,
                    copyFromCommon: false);
            }
        }

        public string Tolerances
        {
            get
            {
                return GetTheRightPreferencesPath(GetPath(FilePathsKeys.Tolerances),
                    forceLocalExistance: false,
                    proposeToCreateLocalFromCommon: false,
                    copyFromCommon: false);
            }
        }

        public string Instruments
        {
            get
            {
                return GetTheRightPreferencesPath(GetPath(FilePathsKeys.Instruments),
                    forceLocalExistance: false,
                    proposeToCreateLocalFromCommon: false,
                    copyFromCommon: false);
            }
        }

        public string GuiPreferences
        {
            get
            {
                var GUIforV2 = GetPath(FilePathsKeys.GUIPreferences);

                try
                {
                    GetTheRightPreferencesPath(GUIforV2,
                               forceLocalExistance: true,
                               proposeToCreateLocalFromCommon: false,
                               copyFromCommon: true,
                               out string path);

                    if (!File.Exists(path))
                    {
                        Tsunami2.Preferences.Values.GuiPrefs = new GUI.Global();
                        TSU.IO.Xml.CreateFile(Tsunami2.Preferences.Values.GuiPrefs, path);
                        TSU.Preferences.GUI.Global.GuiPrefFileExist.Set();
                    }
                    else
                        TSU.Preferences.GUI.Global.GuiPrefFileExist.Set();
                    return path;
                }
                catch (FileNotFoundException)
                {
                    return "";
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        public string At40xIp
        {
            get
            {
                return GetTheRightPreferencesPath(relativePreferencePath: GetPath(FilePathsKeys.At40xIps),
                    forceLocalExistance: true,
                    proposeToCreateLocalFromCommon: true,
                    copyFromCommon: true);
            }
        }

        public string SSID_and_IP
        {
            get
            {
                return GetTheRightPreferencesPath(relativePreferencePath: GetPath(FilePathsKeys.SSID_and_IP),
                    forceLocalExistance: true,
                    proposeToCreateLocalFromCommon: true,
                    copyFromCommon: true);
            }
        }


        /// <summary>
        /// there is 3 preferences path to choose from:
        /// 1. the Preferences_sync folder where ASG is synchornising automaticaly their files from geode usually in c:data/tsu/preferences_sync
        /// 2. the personnal preference folder usually in c:data/tsu/preferences
        /// 3. if the othere do not exist the preferences from the currrent version in program files
        /// </summary>
        /// <param name="relativePreferencePath"></param>
        /// <param name="forceLocalExistance"></param>
        /// <returns></returns>
        private void GetTheRightPreferencesPath(string relativePreferencePath, bool forceLocalExistance, bool proposeToCreateLocalFromCommon, bool copyFromCommon, out string foundPath)
        {
            string syncPath = this.SyncPreferencesPath + relativePreferencePath;
            string localPath = this.PersonnalPreferencesPath + relativePreferencePath;
            string commonPath = this.CommonPreferencesPath + relativePreferencePath;

            bool syncExist = File.Exists(syncPath);
            bool localExist = File.Exists(localPath);
            bool commonExist = File.Exists(commonPath);

            if (syncExist)
            {
                foundPath = syncPath;
                return;
            }

            if (localExist)
            {
                foundPath = localPath;
                return;
            }

            if (forceLocalExistance && copyFromCommon)
            {
                foundPath = localPath;
                if (File.Exists(commonPath))
                    File.Copy(commonPath, localPath);
                return;
            }

            if (forceLocalExistance && !proposeToCreateLocalFromCommon)
            {
                foundPath = localPath;// files that JFF really want user to put themself on their computer, only etalonnage?
                return;
            }

            if (forceLocalExistance && commonExist)
            {
                string copy = "Copy";
                string titleAndMessage = $"You don't have a local version of {relativePreferencePath};You must have this file locally, Do you want to copy the one from the installation or to add it yourself?";
                MessageInput mi = new MessageInput(MessageType.Warning, titleAndMessage)
                {
                    ButtonTexts = new List<string> { copy, R.T_Quit }
                };
                string respond = mi.Show().TextOfButtonClicked;
                if (respond == copy)
                {
                    foundPath = localPath;
                    File.Copy(commonPath, localPath);
                    return;
                }
                else
                    Tsunami2.Preferences.Tsunami.Quit();
            }
            foundPath = commonPath;
        }

        private string GetTheRightPreferencesPath(string relativePreferencePath, bool forceLocalExistance, bool proposeToCreateLocalFromCommon, bool copyFromCommon)
        {
            GetTheRightPreferencesPath(relativePreferencePath, forceLocalExistance, proposeToCreateLocalFromCommon, copyFromCommon, out string foundPath);
            return foundPath;
        }


        #endregion


        #region Forced overriding

        //public string Operations
        //{
        //    get
        //    {
        //        string overridedPath = GetPath(FilePathsKeys.Preferences) + GetPath(FilePathsKeys.Operations);
        //        if (!File.Exists(overridedPath))
        //            File.Copy(AppDomain.CurrentDomain.BaseDirectory + @"Preferences\" + GetPath(FilePathsKeys.Operations), overridedPath);
        //        return overridedPath;
        //    }
        //}




        #endregion

        public string FileToOpen;

        private Dictionary<string, string> FilePaths;

        /// <summary>
        /// this return the path itself if he is absolute, if it is relativ, it will give the path from c:\data\tsunami\[relativePath] if it exist or the tsunami\1.00.20\[relativePath]
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetPath(FilePathsKeys key)
        {
            string path;
            if (FilePaths == null) // from tests projects
                path = Environment.CurrentDirectory;
            else
                path = FilePaths[key.ToString()];
            return path;
        }

        private void CreateMeasureFolderOfTheDay()
        {
            if (!Directory.Exists(MeasureOfTheDay)) Directory.CreateDirectory(MeasureOfTheDay.TrimEnd('\\'));
        }

        public void LoadFilePaths()
        {
            try
            {
                string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                if (baseDirectory.Last() != '\\')
                    baseDirectory += '\\';
                this.preferences.FillDictionnaryFromXmlFile(ref FilePaths, baseDirectory + @"Preferences\FilePaths.xml", "File paths"); // need to be first
                                                                                                                                        //this.CorrectRessourcesFilePaths();

                // if debugging copy prefs to data folder
                //if (System.Diagnostics.Debugger.IsAttached)
                //{
                //    string baseDir = AppDomain.CurrentDomain.BaseDirectory;
                //    string debugDir = Directory.GetParent(baseDir).FullName;
                //    string binDir = Directory.GetParent(debugDir).FullName;
                //    string srcDir = Directory.GetParent(binDir).FullName;

                //    string SourcePath = srcDir + @"\Preferences";
                //    string DestinationPath = Preferences;

                //    foreach (string dirPath in Directory.GetDirectories(SourcePath, "*",
                //        SearchOption.AllDirectories))
                //        Directory.CreateDirectory(dirPath.Replace(SourcePath, DestinationPath));

                //    //Copy all the files & Replaces any files with the same name
                //    foreach (string newPath in Directory.GetFiles(SourcePath, "*.*",
                //        SearchOption.AllDirectories))
                //        File.Copy(newPath, newPath.Replace(SourcePath, DestinationPath), true);

                //}
                this.CreateOverridableFolderPaths();
                this.CreateMeasureFolderOfTheDay();
                this.CreateFolders();
            }
            catch (Exception ex)
            {
                throw new Exception(R.T469 + ex.Message, ex);
            }
        }

        private void CreateFolders()
        {
            // Create folders and logfile
            string dataPath = preferences.Paths.Data;
            if (!Directory.Exists(dataPath)) Directory.CreateDirectory(dataPath.TrimEnd('\\'));

            string MeasuresPath = preferences.Paths.Measures;
            if (!Directory.Exists(MeasuresPath)) Directory.CreateDirectory(MeasuresPath.TrimEnd('\\'));

            string TheoFilePath = preferences.Paths.FichierTheo;
            if (!Directory.Exists(TheoFilePath)) Directory.CreateDirectory(TheoFilePath.TrimEnd('\\'));

            string TempPath = preferences.Paths.Temporary;
            if (!Directory.Exists(TempPath)) Directory.CreateDirectory(TempPath.TrimEnd('\\'));

            string BackupPath = preferences.Paths.Backups;
            if (!Directory.Exists(BackupPath)) Directory.CreateDirectory(BackupPath.TrimEnd('\\'));

            string todayPath = preferences.Paths.MeasureOfTheDay;
            if (!Directory.Exists(todayPath)) Directory.CreateDirectory(todayPath.TrimEnd('\\'));

            string logFolderPath = $"{todayPath}Logs\\";
            if (!Directory.Exists(logFolderPath)) Directory.CreateDirectory(logFolderPath.TrimEnd('\\'));

            string logFullPath = $"{logFolderPath}{Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now)}, Log.html";
            Logs.Log.StartHtmlLog(logFullPath);
        }

        private void CreateOverridableFolderPaths()

        {
            string pref = this.GetPath(FilePathsKeys.Preferences);
            if (!Directory.Exists(pref)) Directory.CreateDirectory(pref);

            string inst = pref + "Instruments and Calibrations";
            if (!Directory.Exists(inst)) Directory.CreateDirectory(inst);
        }

        //private void CorrectRessourcesFilePaths()
        //{
        //    // add the ressources path to the relative path
        //    foreach (string key in FilePaths.Keys.ToList())
        //    {
        //        string preferencePathToChoose = "";
        //        if (!(FilePaths[key].Contains(':')))
        //        {
        //            bool overridePath = 
        //            FilePaths[key] = RessourcesPath + FilePaths[key];

        //        }
        //    }

        //}
    }

}
