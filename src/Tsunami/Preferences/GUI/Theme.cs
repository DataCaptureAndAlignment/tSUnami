﻿using System;
using System.Windows.Forms;
using System.Xml.Serialization;
using D = System.Drawing;
using R = TSU.Properties.Resources;
using P = TSU.Tsunami2.Preferences;

namespace TSU.Preferences.GUI
{
    public class Theme
    {
        public class ThemeColors
        {
            [XmlIgnore]
            public D.Image Logo = R.Tsunami;

            public bool FullColorisation { get; set; } = true;
            private string good { get; set; } = D.KnownColor.Lime.ToString();
            private string attention { get; set; } = D.KnownColor.Orange.ToString();
            private string bad { get; set; } = D.KnownColor.Red.ToString();
            private string grayout { get; set; } = D.KnownColor.LightGray.ToString();
            private string lightBackground { get; set; } = D.KnownColor.Snow.ToString();
            private string darkForeground { get; set; } = "#303030"; //DarkGray
            private string darkBackground { get; set; } = "#303030"; //DarkGray
            private string lightForeground { get; set; } = D.KnownColor.Snow.ToString();
            private string action { get; set; } = "#303030"; //"#64FFC8"; // System.Drawing.KnownColor.Gold.ToString();
            private string highlight { get; set; } = D.KnownColor.LightBlue.ToString();
            private string choice { get; set; } = D.KnownColor.DeepSkyBlue.ToString();
            private string normal { get; set; } = D.KnownColor.LightGray.ToString();
            private string transparent { get; set; } = "Transparent";
            private string mainBackground { get; set; } = "#303030"; //DarkGray
            private string textHighLightFore { get; set; } = D.KnownColor.Snow.ToString();
            private string textHighLightBack { get; set; } = D.KnownColor.DeepSkyBlue.ToString();

            internal D.Color GetColorFromString(string colorName)
            {
                D.Color c;

                try
                {
                    if (colorName.Contains("TSU-A"))
                    {
                        string[] s = colorName.Split('A');
                        s = s[1].Split('B');
                        byte a = byte.Parse(s[0]);
                        s = s[1].Split('G');
                        byte b = byte.Parse(s[0]);
                        s = s[1].Split('R');
                        byte g = byte.Parse(s[0]);
                        byte r = byte.Parse(s[1]);
                        c = D.Color.FromArgb(a, r, g, b);
                    }
                    else
                    {
                        c = D.ColorTranslator.FromHtml(colorName);
                    }
                }
                catch
                {
                    Random rnd = new Random();
                    c = D.Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
                }
                return c;
            }

            internal string GetStringFromColor(D.Color color)
            {
                string s;

                try
                {
                    s = D.ColorTranslator.ToHtml(color);
                }
                catch
                {
                    s = $"TSU-A{color.A}B{color.B}G{color.G}R{color.R}";
                }
                return s;
            }

            // those are ignoer because we cannot serialisaed a colro but only its html name
            [XmlIgnore]
            public D.Color Good
            {
                get
                {
                    return GetColorFromString(good);
                }
                set
                {
                    good = GetStringFromColor(value);
                }
            }
            [XmlIgnore]
            public D.Color Attention
            {
                get
                {
                    return GetColorFromString(attention);
                }
                set
                {
                    attention = GetStringFromColor(value);
                }
            }
            [XmlIgnore]
            public D.Color Bad
            {
                get
                {
                    return GetColorFromString(bad);
                }
                set
                {
                    bad = GetStringFromColor(value);
                }
            }

            [XmlIgnore]
            public D.Color Grayout
            {
                get
                {
                    return GetColorFromString(grayout);
                }
                set
                {
                    grayout = GetStringFromColor(value);
                }
            }
            [XmlIgnore]
            public D.Color LightBackground
            {
                get
                {
                    return GetColorFromString(lightBackground);
                }
                set
                {
                    lightBackground = GetStringFromColor(value);
                }
            }
            [XmlIgnore]
            public D.Color Background
            {
                get
                {
                    return GetColorFromString(mainBackground);
                }
                set
                {
                    mainBackground = GetStringFromColor(value);
                }
            }
            [XmlIgnore]
            public D.Color NormalFore
            {
                get
                {
                    return GetColorFromString(darkForeground);
                }
                set
                {
                    darkForeground = GetStringFromColor(value);
                }
            }

            [XmlIgnore]
            public D.Color TextOnDarkBackGround
            {
                get
                {
                    return GetColorFromString(lightForeground);
                }
                set
                {
                    lightForeground = GetStringFromColor(value);
                }
            }
            [XmlIgnore]
            public D.Color Action
            {
                get
                {
                    return GetColorFromString(action);
                }
                set
                {
                    action = GetStringFromColor(value);
                }
            }

            [XmlIgnore]
            public D.Color Choice
            {
                get
                {
                    return GetColorFromString(choice);
                }
                set
                {
                    choice = GetStringFromColor(value);
                }
            }

            [XmlIgnore]
            public D.Color Highlight
            {
                get
                {
                    return GetColorFromString(highlight);
                }
                set
                {
                    highlight = GetStringFromColor(value);
                }
            }
            [XmlIgnore]
            public D.Color Object
            {
                get
                {
                    return GetColorFromString(normal);
                }
                set
                {
                    normal = GetStringFromColor(value);
                }
            }
            [XmlIgnore]
            public D.Color Transparent
            {
                get
                {
                    return GetColorFromString(transparent);
                }
                set
                {
                    transparent = GetStringFromColor(value);
                }
            }
            [XmlIgnore]
            public D.Color LightForeground
            {
                get
                {
                    return GetColorFromString(lightForeground);
                }
                set
                {
                    lightForeground = GetStringFromColor(value);
                }
            }
            [XmlIgnore]
            public D.Color DarkBackground
            {
                get
                {
                    return GetColorFromString(darkBackground);
                }
                set
                {
                    darkBackground = GetStringFromColor(value);
                }
            }
            [XmlIgnore]
            public D.Color TextHighLightFore
            {
                get
                {
                    return GetColorFromString(textHighLightFore);
                }
                set
                {
                    textHighLightFore = GetStringFromColor(value);
                }
            }
            [XmlIgnore]
            public D.Color TextHighLightBack
            {
                get
                {
                    return GetColorFromString(textHighLightBack);
                }
                set
                {
                    textHighLightBack = GetStringFromColor(value);
                }
            }


            public D.Color GetVariation(D.Color initial, int variation)
            {
                int realVariation = variation * 20;
                int r = initial.R;
                int g = initial.G;
                int b = initial.B;

                r = r > 128 ? r - realVariation : r + realVariation;
                g = g > 128 ? g - realVariation : g + realVariation;
                b = b > 128 ? b - realVariation : b + realVariation;

                return D.Color.FromArgb(initial.A, r, g, b);
            }

            internal D.Color BestForeBasedOnBackground(D.Color background)
            {
                var red = background.R;
                var green = background.G;
                var blue = background.B;
                if (red * 0.299 + green * 0.587 + blue * 0.114 > 186)
                    return D.Color.Black;
                else
                    return this.LightForeground;

            }
        }

        public ThemeColors Colors { get; set; }

        public class ThemeFonts
        {
            public enum Types
            {
                Normal, Large, Tiny, NormalBold
            }

            [XmlIgnore]
            public Types Type { get; set; }
            public string fontNameNice { get; set; } = "Microsoft Sans Serif";
            public string fontNameMonospaced { get; set; } = "Consolas";
            public int LargeTextSize { get; set; } = 24;
            public int NormalTextSize { get; set; } = 16;
            public int TinyTextSize { get; set; } = 12;

            public void ResetFonts()
            {
                this.large = null;
                this.tiny = null;
                this.normal = null;
            }

            private D.Font large = null;
            [XmlIgnore]
            public D.Font Large
            {
                get
                {
                    if (large == null)
                        large = new D.Font(
                            fontNameNice,
                            LargeTextSize,
                            D.FontStyle.Regular,
                            D.GraphicsUnit.Point,
                            0);

                    return large;
                }
            }

            private D.Font normal = null;

            [XmlIgnore]
            public D.Font Normal
            {
                get
                {
                    if (normal == null)
                        normal = new D.Font(
                            fontNameNice,
                            NormalTextSize,
                            D.FontStyle.Regular,
                            D.GraphicsUnit.Point,
                            0);

                    return normal;
                }
            }

            private D.Font normalBold = null;

            [XmlIgnore]
            public D.Font NormalBold
            {
                get
                {
                    if (normalBold == null)
                        normalBold = new D.Font(
                            fontNameNice,
                            NormalTextSize,
                            D.FontStyle.Bold,
                            D.GraphicsUnit.Point,
                            0);

                    return normalBold;
                }
            }

            private D.Font normalItalic = null;

            [XmlIgnore]
            public D.Font NormalItalic
            {
                get
                {
                    if (normalItalic == null)
                        normalItalic = new D.Font(
                            fontNameNice,
                            NormalTextSize,
                            D.FontStyle.Italic,
                            D.GraphicsUnit.Point,
                            0);

                    return normalItalic;
                }
            }

            private D.Font tiny = null;
            [XmlIgnore]
            public D.Font Tiny
            {
                get
                {
                    if (tiny == null)
                        tiny = new D.Font(
                            fontNameNice,
                            TinyTextSize,
                            D.FontStyle.Regular,
                            D.GraphicsUnit.Point,
                            0);

                    return tiny;
                }
            }


            private D.Font monospaceFont = null;
            [XmlIgnore]
            public D.Font MonospaceFont
            {
                get
                {
                    if (monospaceFont == null)
                        monospaceFont = new D.Font(
                            fontNameMonospaced,
                            NormalTextSize,
                            D.FontStyle.Regular,
                            D.GraphicsUnit.Point,
                            0);

                    return monospaceFont;
                }
            }

            internal D.Font GetByType(Types type)
            {
                switch (type)
                {
                    case Types.NormalBold:
                        return NormalBold;
                    case Types.Normal:
                        return Normal;
                    case Types.Large:
                        return Large;
                    case Types.Tiny:
                        return Tiny;
                    default:
                        return Normal;
                }
            }
        }

        public ThemeFonts Fonts { get; set; }

        public int ButtonHeight { get; set; } = 77;



        public ENUM.ThemeColorName ThemeName { get; set; } = ENUM.ThemeColorName.Normal;

        /// <summary>
        /// when set to tru the guided steps are text mostly
        /// </summary>
        public bool Verbose { get; set; } = false;

        public Theme()
        {
            Colors = new ThemeColors();
            ThemeName = ENUM.ThemeColorName.Normal;
            Fonts = new ThemeFonts();
        }

        public void ApplyTo(SplitContainer splitContainer)
        {
            splitContainer.TabStop = false;
            splitContainer.SplitterWidth = 20;
            splitContainer.BackColor = D.Color.Black;
        }
        public void ApplyTo(DataGridView dgv)
        {
            dgv.ReadOnly = true;

            dgv.AllowUserToResizeColumns = true;
            dgv.AllowUserToResizeRows = false;
            dgv.MultiSelect = true;
            dgv.ShowCellErrors = false;
            dgv.ShowEditingIcon = false;
            dgv.EnableHeadersVisualStyles = false;
            dgv.ShowRowErrors = false;
            dgv.AllowUserToAddRows = false;
            dgv.ScrollBars = ScrollBars.Both;
            dgv.AllowUserToDeleteRows = false;
            dgv.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.BackgroundColor = P.Theme.Colors.DarkBackground;
            dgv.BorderStyle = BorderStyle.None;
            dgv.Font = P.Theme.Fonts.Normal;
            dgv.ForeColor = P.Theme.Colors.LightForeground;

            // default
            dgv.DefaultCellStyle.Font = P.Theme.Fonts.Normal;
            dgv.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgv.DefaultCellStyle.BackColor = P.Theme.Colors.DarkBackground;
            dgv.DefaultCellStyle.ForeColor = P.Theme.Colors.LightForeground;
            dgv.DefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            dgv.DefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            dgv.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            // Column header
            dgv.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgv.ColumnHeadersDefaultCellStyle.BackColor = P.Theme.Colors.DarkBackground;
            dgv.ColumnHeadersDefaultCellStyle.ForeColor = P.Theme.Colors.LightForeground;
            dgv.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;

            dgv.ColumnHeadersDefaultCellStyle.Font = P.Theme.Fonts.Normal;
            dgv.ColumnHeadersDefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            dgv.ColumnHeadersDefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            dgv.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.True;

            // Row header
            dgv.RowHeadersDefaultCellStyle.Font = P.Theme.Fonts.Normal;
            dgv.RowHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv.RowHeadersDefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            dgv.RowHeadersDefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            dgv.RowHeadersDefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            dgv.RowHeadersDefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            dgv.RowHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dgv.RowHeadersVisible = false;
            dgv.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;

            // Position
            dgv.GridColor = P.Theme.Colors.Object;
            dgv.Location = new D.Point(0, 0);
        }
    }
}
