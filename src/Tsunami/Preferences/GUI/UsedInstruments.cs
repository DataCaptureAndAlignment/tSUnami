﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSU.Preferences.GUI
{
    [Serializable]
    public class UsedInstruments : IEquatable<UsedInstruments>
    {
        public string Name { get; set; }
        public string Type { get; set; }

        public bool Equals(UsedInstruments other)
        {
            bool sameName = Name == other.Name;
            bool sameType = Type == other.Type;
            return sameName && sameType;
        }
    }
}
