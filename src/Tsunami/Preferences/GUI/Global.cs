﻿using System;
using System.Collections.Generic;
using D = System.Drawing;
using System.Linq;
using System.Xml.Serialization;
using I = TSU.Common.Instruments;
using System.Windows;

using R = TSU.Properties.Resources;
using V = TSU.Common.Strategies.Views;
using TSU.Polar.GuidedModules.Steps;
using TSU.Common;
using TSU.Tools;
using TSU.Views.Message;
using static TSU.Polar.Station.PinTableView;
using System.Windows.Forms;
using System.Threading;

namespace TSU.Preferences.GUI
{
    public class Global : TsuObject
    {
        internal static ManualResetEvent GuiPrefFileExist = new ManualResetEvent(true);
        public List<string> RecentPaths { get; set; } = new List<string>();

        private const int MIN_MAX_NUMBER_OF_POINT_FILE_BEFORE_WARNING = 50;
        private int maxNumberOfPointsPerFileBeforeWarning = MIN_MAX_NUMBER_OF_POINT_FILE_BEFORE_WARNING;
        public int MaxNumberOfPointsPerFileBeforeWarning
        {
            get { return Math.Max(maxNumberOfPointsPerFileBeforeWarning, MIN_MAX_NUMBER_OF_POINT_FILE_BEFORE_WARNING); }
            set { maxNumberOfPointsPerFileBeforeWarning = Math.Max(value, MIN_MAX_NUMBER_OF_POINT_FILE_BEFORE_WARNING); }
        }

        public void AddToRecentPaths(string newPath)
        {
            if (newPath == "") return;
            if (!System.IO.File.Exists(newPath)) return;

            RecentPaths.Remove(newPath);
            RecentPaths.Add(newPath);

            if (RecentPaths.Count > 10)
            {
                RecentPaths.RemoveAt(0);
            }

            // save pref
            TSU.Tsunami2.Preferences.Values.SaveGuiPrefs();
        }


        public TsuBool AddPointsToTheEndOfTheNextPointsList { get; set; } = new TsuBool(true);

        public int SplitterWidth { get; set; } = 50;

        Polar.Station.PinTableView.SplitContainerPosition polarStationOrientation;
        public Polar.Station.PinTableView.SplitContainerPosition PinnedPointsViewPosition
        {
            get
            {
                return polarStationOrientation;
            }
            set
            {
                polarStationOrientation = value;
                TSU.Tsunami2.Preferences.Values.SaveGuiPrefs();
            }
        }

        public Theme Theme { get; set; }


        public int i { get; set; } = 0;

        public TsuBool AskForTsuFileNameAndLocation { get; set; } = new TsuBool(false);

        public V.Types InstrumentViewType { get; set; } = V.Types.Tree;
        public V.Types ReflectorViewType { get; set; } = V.Types.List;
        public V.Types OperationViewType { get; set; } = V.Types.Tree;
        public V.Types ElementViewType { get; set; } = V.Types.List;
        public V.Types ZoneViewType { get; set; } = V.Types.Tree;
        public V.Types MeasureViewType { get; set; } = V.Types.Tree;


        private Preferences.FileOpeningMethods fileOpeningMethod = Preferences.FileOpeningMethods.NotDefined;
        public Preferences.FileOpeningMethods FileOpeningMethod
        {
            get
            {
                if (fileOpeningMethod == Preferences.FileOpeningMethods.NotDefined)
                    fileOpeningMethod = Preferences.FileOpeningMethods.WindowsDefault; // now that we have survey pad, not much file are open with other software so les windiws decide:

                if (fileOpeningMethod != Preferences.FileOpeningMethods.NotDefined)
                    return fileOpeningMethod;

                // ask
                string message = $"{R.T_WHICH_PROGRAM_TO_OPEN_TXT_FILES}?; {R.T_DO_YOU_WANT_TO_USE_NOTEPAD}";
                string np = "NotePad";
                string NPPP = $"NP++\r\n({R.T_MUST_BE_INSTALLED})";
                string wDefault = "Windows\r\nDefault";
                MessageInput mi = new MessageInput(MessageType.Choice, message)
                {
                    ButtonTexts = new List<string> { np, NPPP, wDefault }
                };
                string response = mi.Show().TextOfButtonClicked;
                if (response == np)
                    fileOpeningMethod = Preferences.FileOpeningMethods.NotePad;
                if (response == NPPP)
                    fileOpeningMethod = Preferences.FileOpeningMethods.NotePPP;
                if (response == wDefault)
                    fileOpeningMethod = Preferences.FileOpeningMethods.WindowsDefault;

                return fileOpeningMethod;
            }
            set
            {
                fileOpeningMethod = value;
            }
        }


        public Global()
        {
            if (Buttons == null) Buttons = new List<Views.BigButton>();
            if (lastUsedButtons == null) lastUsedButtons = new List<Views.BigButton>();
            if (LastUsedButtonsText == null) LastUsedButtonsText = new List<string>();
            if (Theme == null) Theme = new Theme();
        }

        public string NotePPPPath { get; set; } = @"C:\Program Files (x86)\Notepad++\notepad++.exe";

        [XmlIgnore]
        public List<Views.BigButton> Buttons;

        private List<Views.BigButton> lastUsedButtons;


        [XmlIgnore]
        public List<Views.BigButton> LastUsedButtons
        {
            get
            {
                if (lastUsedButtons.Count == 0 && Buttons.Count > 0)
                {

                    foreach (string item in LastUsedButtonsText)
                    {
                        Views.BigButton b = Buttons.Find(x => x.GetHashCode().ToString() == item);
                        if (b != null)
                            lastUsedButtons.Add(b);
                    }
                }

                return lastUsedButtons;
            }
            set
            {
                lastUsedButtons = value;
            }

        }

        public List<string> LastUsedButtonsText { get; set; }


        Preferences.UserTypes userType = Preferences.UserTypes.Advanced;
        public Preferences.UserTypes UserType
        {
            get
            {
                return userType;
            }
            set
            {
                userType = value;
            }
        }

        Preferences.SupportedLanguages language = Preferences.SupportedLanguages.english;
        public Preferences.SupportedLanguages Language
        {
            get
            {
                return language;
            }
            set
            {
                language = value;
            }
        }

        public StakeOut.DisplayTypes DisplayTypeToShow_obsolete = StakeOut.DisplayTypes.Displacement;



        public Preferences.DistanceUnit UnitForExtension { get; set; } = Preferences.DistanceUnit.mm;

        [XmlIgnore]
        public int NumberOfDecimalForAngles { get; set; } = 4;
        [XmlIgnore]
        public int NumberOfDecimalForDistances { get; set; } = 5;
        public string LastTeamUsed { get; set; } = R.String_UnknownTeam;
        public double ReferenceEcartoTemperature { get; set; } = 20;
        public double ReferenceLevellingStaffTemperature { get; set; } = 20;
        public double DilatationCoeffEcartoTemperature { get; set; } = 0.000023;
        public double DilatationCoeffInvarStaffemperature { get; set; } = 0.0000015;

        public List<I.StaffType> staffTypeForTemperatureCorrection { get; set; } = new List<I.StaffType>
        {
            I.StaffType.LCB120,
            I.StaffType.LCB190,
            I.StaffType.LCB90,
            I.StaffType.LCBL110,
            I.StaffType.LCBL190,
            I.StaffType.G10,
            I.StaffType.G16,
            I.StaffType.G182,
            I.StaffType.G20,
            I.StaffType.G26,
            I.StaffType.G260,
            I.StaffType.G30,
            I.StaffType.G40,
            I.StaffType.G50,
            I.StaffType.G56,
            I.StaffType.G60,
            I.StaffType.G80,
            I.StaffType.GCB70
        };

        public double ToleranceLevelBlueElevationMM { get; set; } = 0.1;


        public TsuBool LogAT40xMeasurementSpeed { get; set; } = new TsuBool(false);

        public TsuBool HideMachineParameters { get; set; } = new TsuBool(false);
        public TsuBool HideUnusedCalaPoint { get; set; } = new TsuBool(false);

        public TsuBool ShowCommentedLineInGeodeExport { get; set; } = new TsuBool(true);

        public TsuBool daltonien { get; set; } = new TsuBool(false);

        public TsuBool ShowCodeProposition { get; set; } = new TsuBool(true);

        public TsuBool DontEverShowTheDeclarationStepOfGuidedModule { get; set; } = new TsuBool(false);

        public TsuBool UseDotsInNamesAndCodesForThepoints { get; set; } = new TsuBool(true);

        public TsuBool ExportBadPolarStationsAsCommentedLines { get; set; } = new TsuBool(true);

        public TsuBool ShowLogButtons { get; set; } = new TsuBool(true);

        public TsuBool MakeScreenshotAtEveryMouseClick = new TsuBool(true);

        public TsuBool MakeVideoFromScreenshots = new TsuBool(true);

        public TsuBool ShowOneButtonSubMenu = new TsuBool(true);

        public TsuBool MoveBetweenSameFaceMeasurements = new TsuBool(true);

        public TsuBool AllowExportOfPolarStationsWithoutClosure { get; set; } = new TsuBool(false);

        public TsuBool ShowDescriptionWhenHooveringAListView { get; set; } = new TsuBool(false);
        public TsuBool NotificationLeavingOnTheLeftSide { get; set; } = new TsuBool(true);
        public TsuBool BocMeasuresAreNeverExpired { get; set; } = new TsuBool(false);

        public TsuBool BOC_from_ADMIN_step_mixes_all_observations { get; set; } = new TsuBool(true);
        public TsuBool BOC_Run_on_measure_update { get; set; } = new TsuBool(false);

        public TsuBool MoveOneEdgeOnlyWhenResizingWindows { get; set; } = new TsuBool(true);

        private readonly List<UsedInstruments> lastInstrumenTSUsed = new List<UsedInstruments>();

        public List<UsedInstruments> LastInstrumenTSUsed
        {
            get
            {
                return lastInstrumenTSUsed;
            }
        }
        internal void AddLastTeamtUsed(string team)
        {
            if (team.ToUpper() != R.String_UnknownTeam && team.ToUpper() != LastTeamUsed)
            {
                LastTeamUsed = team.ToUpper();
                TSU.Tsunami2.Preferences.Values.SaveGuiPrefs();
            }
        }

        public int OperationNumber { get; set; }
        [XmlIgnore]
        public Common.Operations.Operation LastOpUsed
        {
            get
            {
                return Common.Operations.Manager.GetOperationbyNumber(OperationNumber);
            }
            set
            {
                OperationNumber = value.value;

                TSU.Tsunami2.Preferences.Values.SaveGuiPrefs();
            }
        }




        internal void AddLastInstrumentUsed(I.Instrument i)
        {
            string type = i.GetType().ToString();
            UsedInstruments u = new UsedInstruments() { Name = i._Name, Type = type };

            // remove instrument of this type if existing so the new one is for sure at the end of the list
            lastInstrumenTSUsed.Remove(u);

            lastInstrumenTSUsed.Add(u);

            TSU.Tsunami2.Preferences.Values.SaveGuiPrefs();
        }

        internal I.Instrument GetstInstrumentUsedOfType(I.InstrumentClasses type)
        {
            if (lastInstrumenTSUsed == null) return null;

            // search for this type
            string name = "";
            foreach (var item in lastInstrumenTSUsed)
            {
                if (item.Type == type.ToString())
                {
                    name = item.Name;
                    break;
                }
            }

            if (name == "") return null;

            // search for this instrument and return it if found
            foreach (var item in TSU.Tsunami2.Preferences.Values.Instruments)
            {
                if (item._Name == name)
                {
                    return item;
                }
            }
            return null;
        }

        public Tools.Automation Automation { get; set; } = new Automation();

        public Macro.CustomSet Macros { get; set; } = new Macro.CustomSet();
        public int InstrumentSplitDistance { get; set; } = 435;
        public int PintableSplitDistance { get; set; } = 306;
        public Orientation InstrumentSplitOrientation { get; set; }
        public int VideoBitRateKb { get; set; } = 250;

    }
}
