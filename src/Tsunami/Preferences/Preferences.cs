﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Serialization;
using TSU.Common.Compute.Transformation;
using TSU.Common.Elements;
using TSU.Common.Instruments;
using TSU.Common.Instruments.Reflector;
using TSU.Common.Operations;
using TSU.Common.Zones;
using TSU.IO;
using TSU.Preferences.GUI;
using TSU.Views;
using TSU.Views.Message;
using AD = TSU.Common.Dependencies;
using M = TSU;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Preferences
{

    public class Preferences : Module
    {
        #region Singleton Tools
        private static Preferences instance;

        private Preferences() : base()
        {
            CreateTimerToKillBlockerProcesses();
        }

        private Preferences(Module parentModule)
            : base(parentModule, R.T_GUI_PREF)
        {
            CreateTimerToKillBlockerProcesses();
        }
        public static Preferences Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Preferences(null);
                }
                return instance;
            }
        }

        #endregion

        #region Enums
        public enum UserTypes
        {
            Guided = 0,
            Advanced = 1,
            Super = 2,
        }

        public enum DistanceUnit
        {
            mm = 0,
            m = 1
        }

        public enum SupportedLanguages
        {
            français,
            english,
            russian
        }

        public enum ThemeColor
        {
            Catastrophic,
            Dark,
            Light,
            Sick
        }

        public enum FileOpeningMethods
        {
            NotDefined,
            NotePad,
            NotePPP,
            WindowsDefault,
            Pctopo32,
            SurveyPad
        }

        #endregion

        #region Fields

        public int LoadedCount
        {
            get
            {
                return tsunami != null ? tsunami.tempNumberOfLoadedItem : 0;
            }
            set
            {
                if (tsunami != null)
                    tsunami.tempNumberOfLoadedItem = value;
            }
        }


        public Global GuiPrefs;
        public double na = TSU.DoubleValue.Na;
        public static string LastDirectory;

        public Dictionary<string, TsuBool> dontShowAgainFlags = new Dictionary<string, TsuBool>();
        public TsuBool dontShowAgain(string key, TsuBool dsa = null)
        {
            if (dsa == null)
            {
                if (dontShowAgainFlags.ContainsKey(key))
                    return dontShowAgainFlags[key];
                else
                {
                    TsuBool b = new TsuBool(false);
                    dontShowAgainFlags.Add(key, b);
                    return b;
                }
            }
            else
            {
                dontShowAgainFlags[key] = dsa;
                return null;
            }
        }


        public const double NotAvailableValueNa = -999.9;


        [Obsolete("This property is obsolete. Use InstrumentConnectionParameters instead.", false)]
        public Dictionary<string, string> At401IpAdresses;

        public List<ConnectionParameters> InstrumentConnectionParameters;

        public Common.Tsunami tsunami;
        public Rotation3DList Zone3DRotations;
        //public Dictionary<string, string> DependencieInstallerPaths;
        public List<SigmaForAInstrumentCouple> InstrumentSlashTargetCoupleSigmas;
        public List<Instrument> Instruments;
        public List<Common.Instruments.Module> InstrumentModules = new List<Common.Instruments.Module>();
        public List<Sensor> Sensors;
        public List<Reflector> Reflectors;
        public List<Zone> Zones;
        public List<SocketCode> SocketCodes;
        public List<SocketCode.PairForDetection> SocketCodesPairedWithKeyWords;

        public List<Views.ManagerView> Managers = new List<Views.ManagerView>();

        public static class Dependencies
        {
            public static List<AD.Application> Applications;

            public static bool Exist(string name)
            {
                return Applications.FirstOrDefault(o => o._Name.ToUpper() == name.ToUpper()) != null;
            }

            public static bool GetByBame(string name, out AD.Application application, string majorVersionNumber = "-1")
            {
                application = null;

                if (majorVersionNumber != "-1")
                    application = Applications.FirstOrDefault(x => x._Name.ToUpper() == name.ToUpper() && x._VersionNumbers.Major == majorVersionNumber);
                else
                    application = Applications.FirstOrDefault(x => x._Name.ToUpper() == name.ToUpper());

                if (application == null)
                    return false;
                else
                {
                    if (!application.IsInstalled)
                        return false;
                    else
                        return true;
                }
            }

            public static System.Diagnostics.Process Run(AD.Application app, string arguments = "", string workingDirectory = "", int maxRunningTime = 0, bool wait = false)
            {
                try
                {
                    System.Diagnostics.ProcessStartInfo cmdsi = new System.Diagnostics.ProcessStartInfo(app.Path);
                    cmdsi.CreateNoWindow = false;
                    //if (silent) cmdsi.Arguments += " -X ";
                    cmdsi.Arguments += arguments;
                    cmdsi.WorkingDirectory = workingDirectory;
                    System.Diagnostics.Process cmd = System.Diagnostics.Process.Start(cmdsi);

                    if (wait && maxRunningTime == 0)
                        cmd.WaitForExit();

                    TSU.Tsunami2.Preferences.Values.Processes.Add(new TsuProcess() { Application = app, Process = cmd });

                    if (wait && !cmd.WaitForExit(maxRunningTime))
                    {
                        if (!cmd.HasExited)
                        {
                            cmd.Kill();
                            cmd.Dispose();
                        }
                        throw new Exception($"{app._Name} did not end in {maxRunningTime} seconds");
                    }
                    return cmd;
                }
                catch (Exception ex)
                {
                    throw new Exception("NFC didn't run properly", ex);
                }
            }
        }

        public List<InstrumentTolerance> InstrumentsTolerances;
        public TSU.Common.Tolerances Tolerances;

        public List<Color> DarkRainBow = new List<Color>() { Color.DarkViolet, Color.DarkBlue, Color.DarkGreen, Color.DarkGoldenrod, Color.DarkOrange, Color.DarkRed, Color.DarkMagenta };

        public Paths Paths;
        public bool DfsAvailable;
        public O.OperationTree Operations;
        public System.Diagnostics.Process the3dProcess; //this is the Sugar³ process taht we want to start once only.
        public int ElementManagerPositionCounter = 0;

        public FileOpeningMethods TemporaryFileOpeningMethod = Preferences.FileOpeningMethods.NotDefined;
        public List<string> SecondaryStaffCode = new List<string>() { "10", "11", "16", "25" };

        public string UsedTeam
        {
            get
            {
                List<Common.Station.Module> modules = Common.Analysis.Module.FindAllStationModules(tsunami);
                foreach (var item in modules)
                {
                    string usedTeam = item._Station.ParametersBasic._Team;

                    if (usedTeam != R.String_UnknownTeam)
                        return usedTeam;
                }
                return R.String_UnknownTeam;
            }
        }

        public List<Instrument> UsedInstruments
        {
            get
            {
                List<Instrument> usedInstruments = new List<Instrument>();
                List<Common.Station.Module> modules = Common.Analysis.Module.FindAllStationModules(tsunami);
                foreach (var item in modules)
                {
                    Instrument usedInstrument = item._Station.ParametersBasic._Instrument;

                    if (usedInstrument != null)
                        usedInstruments.Add(usedInstrument);
                }
                return usedInstruments;
            }
        }

        internal void LoadFilePaths()
        {
            DfsAvailable = Common.Analysis.Network.QuickBestGuessAboutAccessibilityOfNetworkPath(@"\\cern.ch\dfs\Support\SurveyingEng\Computing");
            Paths = new Paths(instance);
            Paths.LoadFilePaths();
        }

        internal Rectangle WorkingArea
        {
            get
            {
                if (tsunami != null)
                {
                    if (tsunami.View != null)
                        return System.Windows.Forms.Screen.FromControl(tsunami.View).WorkingArea;
                }
                return System.Windows.Forms.Screen.PrimaryScreen.WorkingArea;
            }

        }

        [XmlIgnore]
        public List<TsuProcess> Processes { get; set; } = new List<TsuProcess>();


        private System.Timers.Timer TimerKillingOldProcesses;

        /// <summary>
        /// Create a timer that will checl if processes created more than 2 mintues ago still exist and then kills it if so.
        /// </summary>
        private void CreateTimerToKillBlockerProcesses()
        {
            TimerKillingOldProcesses = new System.Timers.Timer()
            {
                Interval = 1 * 60 * 1000,
                AutoReset = true,
                Enabled = true
            };
            TimerKillingOldProcesses.Elapsed += delegate
            {
                // Kill all processes, because it seems that a PLGC process running will block the process for minutes
                for (int i = this.Processes.Count - 1; i >= 0; i--) // iterate backward to safely remove the curretn item
                {
                    var item = this.Processes[i];

                    if (item.Process.HasExited)
                    {
                        this.Processes.RemoveAt(i);
                    }
                    else
                    {
                        TimeSpan elapsedTime = DateTime.Now - item.startingTime;

                        if (elapsedTime.Minutes > 5)
                        {
                            item.Kill();
                            this.Processes.RemoveAt(i);
                            Logs.Log.AddEntryAsPayAttentionOf(Tsunami2.Preferences.Tsunami, $"We had to kill: {item._Name} running '{item.Input}' for {elapsedTime.Minutes} minutes");
                        }
                    }
                }
            };
        }


        public List<MathNet.Numerics.LinearAlgebra.Matrix<double>> NfcTransformationMatrices { get; set; } = new List<MathNet.Numerics.LinearAlgebra.Matrix<double>>();
        
        public bool UsePLGC = false;


        #endregion

        #region Initialisation

        public static string TryToGetFromADictionnary(Dictionary<string, string> dic, string Key)
        {
            string value;
            if (!dic.TryGetValue(Key, out value))
                throw new Exception(string.Format(R.T467, Key, dic.ToString()));
            else
                return value;
        }

        public override void Initialize()
        {
            this._Name = R.T_M_PREF;
            this.CreateViewAutmatically = false;
            Paths = new Paths(instance);
            base.Initialize();

            defaultCulture = Thread.CurrentThread.CurrentCulture;
            defaultUICulture = Thread.CurrentThread.CurrentUICulture;
        }

        public void InitializeWithAllXmlParametersFiles()
        {
            Paths = new Paths(instance);
            Paths.LoadFilePaths();

            LoadGuiPref();

            LoadZones();
            LoadInstrumentsTcpIpParameters();
            LoadDependenciesFromXml();
            LoadInstrumentList();
            LoadCalibrationCoefficientSetsForDistanceCorrections();
            LoadInstrumentPrecisions();
            LoadSocketCodes();
            LoadOperationNumber();
            LoadInstrumentsTolerances();
            LoadTolerances();
        }
        #endregion

        #region Loading


        public void CleanTemporaryDirectories()
        {
            try
            {
                this.CleanTemporaryDirectories(256);
            }
            catch (Exception ex)
            {
                throw new Exception(R.T470 + ex.Message, ex);
            }
        }

        /// <summary>
        /// New rules NOW, the zones can be store in pref/zones.xml or any xml file in pref/zones/
        /// </summary>
        public void LoadZones()
        {
            void LoadFromCommonZoneFile(string path, ref List<Zone> zones, ref int count)
            {
                if (!File.Exists(path))
                    return;

                zones = Xml.DeserializeFile(typeof(List<Zone>), path) as List<Zone>;

                foreach (Zone zone in Zones)
                {
                    if (!zone.Validity)
                        throw new Exception(string.Format(R.Zone_NotValid, zone, Paths.Zones));
                    zone.PathSource = "zones.xml";
                }

                count = Zones.Count;
            }

            void LoadFromSeperatedZoneFiles(string path, ref List<Zone> zones, ref int count)
            {
                if (!Directory.Exists(path))
                    return;

                DirectoryInfo d = new DirectoryInfo(path);

                foreach (FileInfo file in d.GetFiles("*.xml"))
                {
                    try
                    {
                        if (file.Name.ToUpper() == "ZONES.XML")
                            continue;

                        Zone zone = Deserialize<Zone>(file.FullName, "Zones (local systems)");
                        zone.PathSource = file.Name;
                        zones.Add(zone);
                        count++;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"{ex.Message} in {file.FullName}", ex);
                    }
                }
            }

            try
            {
                //LoadFromCommonZoneFile(Paths.Zones + "ZONES.XML", ref MlaZones, ref tsunami.tempNumberOfLoadedItem);
                Zones = new List<Zone>();
                int count = LoadedCount;
                LoadFromSeperatedZoneFiles(Paths.Zones, ref Zones, ref count);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}: {1}\r\n", R.T471, ex.Message), ex);
            }
        }



        public void LoadInstrumentsTcpIpParameters()
        {
            try
            {
                // deprecticated IP adresses
                this.FillDictionnaryFromXmlFile(ref At401IpAdresses, Paths.At40xIp, "At40x Ip adresses");
                LoadedCount = At401IpAdresses.Count;

                // ConnectionParameters
                var temp = Xml.DeserializeFile(typeof(List<ConnectionParameters>), Paths.SSID_and_IP);
                if (temp != null)
                    InstrumentConnectionParameters = temp as List<ConnectionParameters>;
            }
            catch (Exception ex)
            {

                throw new Exception(string.Format("{0}: {1}\r\n", R.T474, ex.Message), ex);
            }
        }

        public void LoadInstrumentsTolerances()
        {
            try
            {
                InstrumentsTolerances = new List<InstrumentTolerance>();
                object o = InstrumentsTolerances;
                DeSerialize(ref o, Paths.InstrumentTolerances);
                InstrumentsTolerances = o as List<InstrumentTolerance>;
                LoadedCount = InstrumentsTolerances.Count;
            }
            catch (Exception ex)
            {

                throw new Exception(string.Format("{0}: {1}\r\n", R.T_LOADING_TOL, ex.Message), ex);
            }
        }

        public void LoadTolerances()
        {
            try
            {
                object o = new TSU.Common.Tolerances();
                DeSerialize(ref o, Paths.Tolerances);
                Tolerances = o as TSU.Common.Tolerances;
                LoadedCount = 1;
            }
            catch (Exception ex)
            {

                throw new Exception(string.Format("{0}: {1}\r\n", R.T_LOADING_TOL, ex.Message), ex);
            }
        }


        public int LoadDependenciesFromXml()
        {
            Preferences.Dependencies.Applications = new List<AD.Application>();
            string fullPath = Paths.Dependencies;

            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            if (baseDirectory.Last() != '\\')
                baseDirectory += '\\';
            if (!File.Exists(fullPath)) throw new Exception(string.Format(R.T_MissingFile, fullPath, baseDirectory));

            object susoftRecommendation = Xml.DeserializeFile(typeof(AD.Application), fullPath);

            AD.Application tsunami = susoftRecommendation as AD.Application;

            Preferences.Dependencies.Applications.AddRange(tsunami.Dependencies);

            return Preferences.Dependencies.Applications.Count;
        }
        public void LoadInstrumentList()
        {
            try
            {
                LoadedCount = T.Conversions.FileFormat.FillListFromGeodeInstrumentFile(ref Instruments, Paths.Instruments, "Instrument list");
                // create smaller list
                Sensors = new List<Sensor>();
                Sensors.AddRange(Instruments.FindAll(x => x is Sensor).Cast<Sensor>().ToList());
                Reflectors = new List<Reflector>();
                Reflectors.AddRange(Instruments.FindAll(x => x is Reflector).Cast<Reflector>().ToList());


                //if (!Paths.Instruments.Contains(Paths.Data))
                //{
                //    throw new WarningException($"{R.T495} from {TSU.Tsunami2.TsunamiPreferences.Values.Paths.Instruments}\r\n" +
                //        $"This file is dating from '{tsunami.Date}'," +
                //        $"Read more here: '{Views.MessageTsu.GetLink(TSU.Tsunami2.TsunamiPreferences.Values.Paths.Preferences+ "Read me.txt")}'");
                //}
            }
            catch (WarningException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new Exception(R.T475 + ex.Message, ex);
            }
        }

        public void LoadCalibrationCoefficientSetsForDistanceCorrections()
        {
            try
            {

                LoadedCount = T.Conversions.FileFormat.FillListFromGeodeEtalonnagesFile(ref Instruments, Paths.Etalonnages, out string warningMessage, "Instrument calibrations");

                if (warningMessage != "")
                {
                    string titleAndMessage = "Some problems with the instrument and calibration lists;" + warningMessage;
                    throw new WarningException(titleAndMessage);
                    new MessageInput(MessageType.Critical, titleAndMessage).Show();
                }


                if (!Paths.Etalonnages.Contains(Paths.Data))
                {
                    throw new WarningException($"{R.T496} from {MessageTsu.GetLink(M.Tsunami2.Preferences.Values.Paths.Etalonnages)}\r\n" +
                        $"This file is as old as this tsunami version, " +
                        $"read more here: '{MessageTsu.GetLink(M.Tsunami2.Preferences.Values.Paths.PersonnalPreferencesPath + "Read me.txt")}'");
                }
            }
            catch (WarningException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new Exception(R.T476 + ": " + ex.Message, ex);
            }
        }
        public void LoadInstrumentPrecisions()
        {
            try
            {
                LoadedCount = FillListFromXmlFile(ref InstrumentSlashTargetCoupleSigmas, Paths.InstrumentSlashTargetCoupleSigmas, "Instrument precisions");
            }
            catch (Exception ex)
            {
                throw new Exception(R.T477 + ex.Message, ex);
            }
        }

        public void DLLCheckPresence()
        {
            string errormessage = "";

            // T3000
            try
            {
                float un = TSU.Common.Instruments.Adapter.T3000_dll.GetHorizontalAngle(0);
                if (un != -1) throw new Exception($"T3000_3 {R.T_DLL_IS_MISSING}");
            }
            catch (Exception)
            {
                errormessage += $"T3000_3 dll is missing. ";
            }

            // GeoCom105
            try
            {
                double zero = 0;
                TSU.Common.Instruments.Adapter.GeoCom105_dll.VB_TMC_GetPrismCorr(ref zero);
            }
            catch (Exception)
            {
                errormessage += $"GeoCom105 {R.T_DLL_IS_MISSING}. ";
            }

            // GeoCom32
            try
            {
                double zero = 0;
                TSU.Common.Instruments.Adapter.GeoCom32_dll.VB_TMC_GetPrismCorr(ref zero);
            }
            catch (Exception)
            {
                errormessage += $"GeoCom32 {R.T_DLL_IS_MISSING}. ";
            }

            if (errormessage != "")
                throw new Exception(errormessage);

        }
        public void LoadOperationNumber()
        {
            try
            {
                // adding operation 9999
                if (File.Exists(Tsunami2.Preferences.Values.Paths.PersonalPreferencesPath))
                {
                    this.Operations = T.Conversions.FileFormat.OperationFile2Nodes(new FileInfo(Paths.Operations), out int count, new FileInfo(Tsunami2.Preferences.Values.Paths.PersonalPreferencesPath));
                    LoadedCount = count;
                }
                else
                {
                    this.Operations = T.Conversions.FileFormat.OperationFile2Nodes(new FileInfo(Paths.Operations), out int count);
                    LoadedCount = count;
                }

                O.Operation o999999 = O.Operation.NoOp;
                this.Operations.Nodes.Add(new Views.TsuNode() { Name = o999999.value.ToString(), Text = o999999.value.ToString() + ": " + o999999.Description, Tag = o999999 });

                if (!Paths.Operations.Contains(Paths.Data))
                {
                    throw new WarningException($"{R.T494} from {MessageTsu.GetLink(M.Tsunami2.Preferences.Values.Paths.Operations)}\r\n" +
                        $"This file is as old as this tsunami version, " +
                        $"read more here: '{MessageTsu.GetLink(M.Tsunami2.Preferences.Values.Paths.PersonnalPreferencesPath + "Read me.txt")}'");
                }
            }
            catch (WarningException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new Exception(R.T478 + ex.Message, ex);
            }
        }
        public void LoadSocketCodes()
        {
            try
            {
                object o = new List<SocketCode>();
                DeSerialize(ref o, Paths.SocketCodes);
                SocketCodes = o as List<SocketCode>;
                LoadedCount = SocketCodes.Count;



                object o2 = new List<SocketCode.PairForDetection>();
                DeSerialize(ref o2, Paths.SocketCodesPairedWithKeyWords);
                SocketCodesPairedWithKeyWords = o2 as List<SocketCode.PairForDetection>;
                LoadedCount = SocketCodesPairedWithKeyWords.Count;


            }
            catch (Exception ex)
            {
                throw new Exception(R.T_LF_SocketCodes + ex.Message, ex);
            }
        }

        public void LoadGuiPref(string filePath = "")
        {
            try
            {
                if (filePath == "")
                {
                    filePath = Paths.GuiPreferences;
                }
                // if save is waiting, save immediately before loading
                if (guiPrefsTimer != null)
                {
                    ClearGuiTimer();
                    SaveGuiPrefs();
                }

                object obj = TSU.IO.Xml.DeserializeFile(typeof(Global), filePath);
                instance.GuiPrefs = obj as Global;

                //instance.GuiPrefs.ReferenceEcartoTemperature = 20;
                this.SwitchLanguage(instance.GuiPrefs.Language);
                this.SwitchTheme(instance.GuiPrefs.Theme);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                }

                System.IO.File.Move(filePath, filePath + T.Conversions.Date.ToDateUpSideDown(DateTime.Now) + ".old");

                instance.GuiPrefs = new Global();

                throw new Exception($"{R.T_PROBLEM_LOADING_THE_GUI_PREFERENCES} {Paths.GuiPreferences}:\r\n{ex.Message}\r\nOld file backed up and new preference file created.", ex);
            }
        }

        /// <summary>
        /// will trigger a saveguipref in 1000ms
        /// </summary>
        public void SaveGuiPrefs()
        {
            if (guiPrefsTimer != null)
                ClearGuiTimer();
            guiPrefsTimer = new System.Timers.Timer(1000)
            {
                AutoReset = false,
            };
            guiPrefsTimer.Elapsed += SaveGuiPrefsCallback;
            guiPrefsTimer.Start();
        }

        private void SaveGuiPrefsCallback(object sender, ElapsedEventArgs e)
        {
            string s = Paths.GuiPreferences;
            try
            {
                TSU.IO.Xml.CreateFile(instance.GuiPrefs, s);
                //new MessageInput(MessageType.FYI, "Prefs saved").Show();
                ClearGuiTimer();
            }
            catch (Exception ex)
            {
                string titleAndMessage = $"{R.T_COULD_NOT_SAVE_THE_PREFERENCES};{R.T_DO_YOU_HAVE_ACCESS_TO} {s}\r\n\r\n{ex.Message}";
                new MessageInput(MessageType.Critical, titleAndMessage).Show();
            }
        }

        System.Timers.Timer guiPrefsTimer = null;

        private void ClearGuiTimer()
        {
            if (guiPrefsTimer != null)
            {
                guiPrefsTimer.Stop();
                guiPrefsTimer.Dispose();
                guiPrefsTimer = null;
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            ClearGuiTimer();
        }

        #endregion

        #region tools


        public static bool CleanFolderByModifiedDate(string folderPath, int daysThreshold, ref string errorMessage)
        {
            bool ok = true;
            // Get DirectoryInfo object for the folder
            DirectoryInfo directory = new DirectoryInfo(folderPath);

            // Get the current date
            DateTime currentDate = DateTime.Now;

            // Iterate through each file in the folder
            foreach (FileInfo file in directory.GetFiles())
            {
                // Calculate the difference in days between the current date and the last modification date of the file
                TimeSpan difference = currentDate - file.LastWriteTime;

                // Check if the file was last modified more than the threshold days ago
                if (difference.TotalDays > daysThreshold)
                {
                    try
                    {
                        // Delete the file
                        file.Delete();
                    }
                    catch (Exception ex)
                    {
                        ok = false;
                        // Handle any exceptions that occur during file deletion
                        errorMessage += ($"Error deleting file {file.Name}\r\n");
                    }
                }
            }

            // Recursively call the method for each subdirectory
            foreach (DirectoryInfo subdirectory in directory.GetDirectories())
            {
                bool subOk = CleanFolderByModifiedDate(subdirectory.FullName, daysThreshold, ref errorMessage);
                ok = ok && subOk;
            }
            return ok;
        }

        private void CleanTemporaryDirectories(int numberOfFileToKeepInEachFolder)
        {
            string temporaryFolder = Paths.Temporary;

            if (!File.Exists(temporaryFolder)) 
                Directory.CreateDirectory(temporaryFolder);

            string messageError = "";
            if (!CleanFolderByModifiedDate(temporaryFolder, 30, ref messageError))
            {
                string titleAndMessage = $"{R.T_CLEANING_PROBLEM};{R.T_CLEANING_PROBLEM_DETAILS}: " +
                                         $"{MessageTsu.GetLink(temporaryFolder)}\r\n\r\n" +
                                         $"{messageError}";
                new MessageInput(MessageType.Warning, titleAndMessage).Show();
            }
        }
        private int FillListFromXmlFile(ref List<SigmaForAInstrumentCouple> list, string fileName, string description = "An action")
        {
            list = new List<SigmaForAInstrumentCouple>();
            object o = list;
            DeSerialize(ref o, fileName);
            list = o as List<SigmaForAInstrumentCouple>;
            return list.Count;
        }

        internal void FillDictionnaryFromXmlFile(ref Dictionary<string, string> dico, string fileName, string description = "An action")
        {
            dico = new Dictionary<string, string>();
            DeSerializeDictionnary(ref dico, fileName);
        }


        private void DeSerializeDictionnary(ref Dictionary<string, string> d, string fileName)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(item[]), new XmlRootAttribute() { ElementName = "items" });

            string fullPath = fileName;
            if (!System.IO.File.Exists(fullPath)) throw new Exception(R.T480 + fullPath + " " + R.T479);
            try
            {
                StreamReader reader = new StreamReader(fullPath);
                d = ((item[])serializer.Deserialize(reader)).ToDictionary(i => i.key, i => i.value);
                reader.Close();
                reader.Dispose();
            }
            catch (Exception ex)
            {
                //  throw new Exception( string.Format(R.T481, fullPath, ex.Message));
                throw new Exception($"{R.T481} {fullPath}: {ex.Message}", ex);
            }
        }

        public T Deserialize<T>(string path, string description = "Action")
        {
            return (T)Xml.DeserializeFile(typeof(T), path);
        }

        public void DeSerialize(ref object obj, string path, string description = "Action")
        {
            obj = Xml.DeserializeFile(obj.GetType(), path);
        }

        private void Serialize(ref Dictionary<string, string> d, string fileName)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(item[]), new XmlRootAttribute() { ElementName = "items" });
            FileStream stream = System.IO.File.Create(fileName);
            serializer.Serialize(stream, d.Select(kv => new item() { key = kv.Key, value = kv.Value }).ToArray());
            stream.Close();
            stream.Dispose();
        }
        // To allow serialization of dictionnary
        public class item
        {
            [XmlAttribute]
            public string key;
            [XmlAttribute]
            public string value;
        }

        public EtalonnageParameter GetEtalonnageParameter(string SensorSN, string ReflectoSN)
        {
            List<Instrument> s = Instruments.Where(i => i._SerialNumber == SensorSN).ToList();
            List<TotalStation> t = s.Cast<TotalStation>().ToList();
            TotalStation ts = t[0];
            EtalonnageParameter.Theodolite a = (EtalonnageParameter.Theodolite)ts.EtalonnageParameterList.Where(i => i.PrismeRef._SerialNumber == ReflectoSN).ToList()[0];
            return a;
        }

        // brign this ip in front of the list if it exist or add it if not, the max lengyh of the list is kept to 5 entries because if not the pinging time will be longer than entering an ip manually.
        public void AddIpToListOfAt401IpAdresses(string serialNumber, string IP)
        {
            int count = 0;
            int countForThisSn = 0;
            KeyValuePair<string, string> existing = new KeyValuePair<string, string>("nf", "nf");
            KeyValuePair<string, string> oldest = new KeyValuePair<string, string>("nf", "nf");

            // check if existing and how many for this instrument...
            foreach (var item in At401IpAdresses)
            {
                count++;
                if (item.Key.Split('_')[0] == serialNumber)
                {
                    if (oldest.Key == "nf") oldest = item;
                    countForThisSn++;
                    if (item.Value == IP)
                        existing = item;
                }
            }

            // If exist bring to the end of the dico. (to be later tried first
            if (existing.Key != "nf")
            {
                At401IpAdresses.Remove(existing.Key);
                At401IpAdresses.Add(existing.Key, existing.Value);
            }
            else // of added
            {
                int i = 1;
                string key = serialNumber + "_" + i;
                while (At401IpAdresses.ContainsKey(key))
                {
                    i += 1;
                    key = serialNumber + "_" + i;
                }

                // remove oldest if too much already
                if (countForThisSn > 5) At401IpAdresses.Remove(oldest.Key);

                At401IpAdresses.Add(key, IP);
            }

            Serialize(ref At401IpAdresses, Paths.At40xIp);
        }



        #endregion

        #region Language

        public void SwitchLanguage(SupportedLanguages language, bool saveInPreferences = true)
        {
            // add it to tsuanami to remember the langauge of the tsu file
            if (tsunami != null)
                tsunami.Language = language;

            CultureInfo newCulture;
            switch (language)
            {
                case SupportedLanguages.français:
                    newCulture = new CultureInfo("fr-FR");
                    break;
                case SupportedLanguages.english:
                    newCulture = new CultureInfo("en-GB");
                    break;
                default:
                    newCulture = new CultureInfo("en-GB");
                    break;
            }

            //Thread.CurrentThread.CurrentCulture = newCulture;
            Thread.CurrentThread.CurrentUICulture = newCulture;
            R.Culture = newCulture;

            if (saveInPreferences)
            {
                if (instance.GuiPrefs.Language != language)
                {
                    instance.GuiPrefs.Language = language;
                    Preferences.Instance.SaveGuiPrefs();
                }
            }
        }

        #endregion
        #region ThemeColor
        public void SwitchTheme(Theme theme)
        {

            switch (TSU.Tsunami2.Preferences.Values.GuiPrefs.Theme.ThemeName)
            {
                case ENUM.ThemeColorName.Normal:
                    this.SetThemeToNormal();
                    break;
                case ENUM.ThemeColorName.Sick:
                    this.SetThemeToSick();
                    break;
                case ENUM.ThemeColorName.Pink:
                    this.SetThemeToPink();
                    break;
                case ENUM.ThemeColorName.RainBow:
                    this.SetThemeToRainbow();
                    break;
                case ENUM.ThemeColorName.Dark:
                    this.SetThemeToDark();
                    break;
                case ENUM.ThemeColorName.Light:
                    this.SetThemeToLight();
                    break;
                case ENUM.ThemeColorName.Gray:
                    this.SetThemeToGray();
                    break;
                default:
                    this.SetThemeToNormal();
                    break;
            }
            this.UpdateView();
        }
        public void SetThemeToSick()
        {
            instance.GuiPrefs.Theme.Colors.FullColorisation = true;
            instance.GuiPrefs.Theme.Colors.Good = Color.GreenYellow;
            instance.GuiPrefs.Theme.Colors.Attention = Color.LightSalmon;
            instance.GuiPrefs.Theme.Colors.Bad = Color.Red;
            instance.GuiPrefs.Theme.Colors.LightBackground = Color.Aqua;
            instance.GuiPrefs.Theme.Colors.NormalFore = Color.MidnightBlue;
            instance.GuiPrefs.Theme.Colors.Action = Color.Violet;
            instance.GuiPrefs.Theme.Colors.Highlight = Color.Magenta;
            instance.GuiPrefs.Theme.Colors.Object = Color.LemonChiffon;
            instance.GuiPrefs.Theme.Colors.Background = Color.SaddleBrown;
            instance.GuiPrefs.Theme.Colors.LightForeground = Color.PeachPuff;
            instance.GuiPrefs.Theme.Colors.DarkBackground = Color.DarkViolet;
            instance.GuiPrefs.Theme.Colors.TextHighLightBack = Color.MediumVioletRed;
            instance.GuiPrefs.Theme.Colors.TextHighLightFore = Color.White;
            instance.GuiPrefs.Theme.ThemeName = ENUM.ThemeColorName.Sick;
        }
        public void SetThemeToNormal()
        {
            instance.GuiPrefs.Theme.Colors.FullColorisation = true;
            instance.GuiPrefs.Theme.ThemeName = ENUM.ThemeColorName.Normal;

            instance.GuiPrefs.Theme.Colors.Good = Color.LimeGreen;
            instance.GuiPrefs.Theme.Colors.Attention = Color.Orange;
            instance.GuiPrefs.Theme.Colors.Bad = Color.Tomato;
            instance.GuiPrefs.Theme.Colors.Action = Globals.AppColor;
            instance.GuiPrefs.Theme.Colors.Choice = Color.DeepSkyBlue;
            instance.GuiPrefs.Theme.Colors.Highlight = Color.Cyan;
            instance.GuiPrefs.Theme.Colors.Object = Color.LightGray;

            instance.GuiPrefs.Theme.Colors.Background = Color.FromArgb(48, 48, 48);
            instance.GuiPrefs.Theme.Colors.NormalFore = Color.FromArgb(30, 30, 30);

            instance.GuiPrefs.Theme.Colors.LightBackground = Color.Snow;
            instance.GuiPrefs.Theme.Colors.LightForeground = Color.White;
            instance.GuiPrefs.Theme.Colors.DarkBackground = Color.Black;
            instance.GuiPrefs.Theme.Colors.TextHighLightBack = Color.DeepSkyBlue;
            instance.GuiPrefs.Theme.Colors.TextHighLightFore = Color.Black;
        }
        public void SetThemeToLight()
        {
            instance.GuiPrefs.Theme.ThemeName = ENUM.ThemeColorName.Light;

            instance.GuiPrefs.Theme.Colors.FullColorisation = true;
            instance.GuiPrefs.Theme.Colors.Good = Color.YellowGreen;
            instance.GuiPrefs.Theme.Colors.Attention = Color.LightSalmon;
            instance.GuiPrefs.Theme.Colors.Bad = Color.OrangeRed;


            instance.GuiPrefs.Theme.Colors.Action = Color.LightYellow;
            instance.GuiPrefs.Theme.Colors.Choice = Color.Cyan;
            instance.GuiPrefs.Theme.Colors.Object = Color.Gainsboro;

            instance.GuiPrefs.Theme.Colors.Background = Color.LightGray;
            instance.GuiPrefs.Theme.Colors.NormalFore = Color.Black;

            instance.GuiPrefs.Theme.Colors.LightBackground = Color.White;
            instance.GuiPrefs.Theme.Colors.Highlight = Color.LightBlue;
            instance.GuiPrefs.Theme.Colors.LightForeground = Color.White;
            instance.GuiPrefs.Theme.Colors.DarkBackground = Color.DarkGray;
            instance.GuiPrefs.Theme.Colors.TextHighLightBack = Color.LightBlue;
            instance.GuiPrefs.Theme.Colors.TextHighLightFore = Color.Black;
        }
        public void SetThemeToDark()
        {
            instance.GuiPrefs.Theme.ThemeName = ENUM.ThemeColorName.Dark;
            instance.GuiPrefs.Theme.Colors.FullColorisation = true;

            instance.GuiPrefs.Theme.Colors.Good = Color.DarkGreen;
            instance.GuiPrefs.Theme.Colors.Attention = Color.DarkOrange;
            instance.GuiPrefs.Theme.Colors.Bad = Color.DarkRed;

            instance.GuiPrefs.Theme.Colors.Background = Color.FromArgb(48, 48, 48);
            instance.GuiPrefs.Theme.Colors.DarkBackground = instance.GuiPrefs.Theme.Colors.Background;
            instance.GuiPrefs.Theme.Colors.NormalFore = Color.LightGray;


            instance.GuiPrefs.Theme.Colors.LightBackground = instance.GuiPrefs.Theme.Colors.Background;
            instance.GuiPrefs.Theme.Colors.LightForeground = instance.GuiPrefs.Theme.Colors.NormalFore;

            instance.GuiPrefs.Theme.Colors.Highlight = Color.MidnightBlue;
            instance.GuiPrefs.Theme.Colors.Action = Color.DarkOliveGreen;
            instance.GuiPrefs.Theme.Colors.Choice = Color.DarkSlateBlue;

            instance.GuiPrefs.Theme.Colors.Object = Color.DarkSlateGray;

            instance.GuiPrefs.Theme.Colors.TextHighLightBack = instance.GuiPrefs.Theme.Colors.Highlight;
            instance.GuiPrefs.Theme.Colors.TextHighLightFore = instance.GuiPrefs.Theme.Colors.NormalFore;
        }
        public void SetThemeToPink()
        {
            instance.GuiPrefs.Theme.Colors.FullColorisation = true;
            instance.GuiPrefs.Theme.Colors.Good = Color.LightGreen;
            instance.GuiPrefs.Theme.Colors.Attention = Color.LightCoral;
            instance.GuiPrefs.Theme.Colors.Bad = Color.Tomato;
            instance.GuiPrefs.Theme.Colors.LightBackground = Color.LightPink;
            instance.GuiPrefs.Theme.Colors.NormalFore = Color.Black;
            instance.GuiPrefs.Theme.Colors.Action = Color.HotPink;
            instance.GuiPrefs.Theme.Colors.Highlight = Color.Fuchsia;
            instance.GuiPrefs.Theme.Colors.Choice = Color.DarkMagenta;
            instance.GuiPrefs.Theme.Colors.Object = Color.Pink;
            instance.GuiPrefs.Theme.Colors.Background = Color.DeepPink;
            instance.GuiPrefs.Theme.Colors.LightForeground = Color.White;
            instance.GuiPrefs.Theme.Colors.DarkBackground = Color.Purple;
            instance.GuiPrefs.Theme.Colors.TextHighLightBack = Color.Fuchsia;
            instance.GuiPrefs.Theme.Colors.TextHighLightFore = Color.White;
            instance.GuiPrefs.Theme.ThemeName = ENUM.ThemeColorName.Pink;
        }
        public void SetThemeToRainbow()
        {

            instance.GuiPrefs.Theme.Colors.FullColorisation = true;
            instance.GuiPrefs.Theme.Colors.Good = Color.Green;
            instance.GuiPrefs.Theme.Colors.Attention = Color.Orange;
            instance.GuiPrefs.Theme.Colors.Bad = Color.Red;
            instance.GuiPrefs.Theme.Colors.LightBackground = Color.AliceBlue;
            instance.GuiPrefs.Theme.Colors.NormalFore = Color.MidnightBlue;
            instance.GuiPrefs.Theme.Colors.Action = Color.Yellow;
            instance.GuiPrefs.Theme.Colors.Highlight = Color.AliceBlue;
            instance.GuiPrefs.Theme.Colors.Choice = Color.SpringGreen;
            instance.GuiPrefs.Theme.Colors.Object = Color.Violet;
            instance.GuiPrefs.Theme.Colors.Background = Color.Indigo;
            instance.GuiPrefs.Theme.Colors.LightForeground = Color.NavajoWhite;
            instance.GuiPrefs.Theme.Colors.DarkBackground = Color.Indigo;
            instance.GuiPrefs.Theme.Colors.TextHighLightBack = Color.DeepSkyBlue;
            instance.GuiPrefs.Theme.Colors.TextHighLightFore = Color.NavajoWhite;
            instance.GuiPrefs.Theme.ThemeName = ENUM.ThemeColorName.RainBow;
        }
        public void SetThemeToGray()
        {
            instance.GuiPrefs.Theme.Colors.FullColorisation = true;
            instance.GuiPrefs.Theme.Colors.Good = Color.Gray;
            instance.GuiPrefs.Theme.Colors.Attention = Color.Gray;
            instance.GuiPrefs.Theme.Colors.Bad = Color.Gray;
            instance.GuiPrefs.Theme.Colors.LightBackground = Color.White;
            instance.GuiPrefs.Theme.Colors.NormalFore = Color.Black;
            instance.GuiPrefs.Theme.Colors.Action = Color.Silver;
            instance.GuiPrefs.Theme.Colors.Highlight = Color.DarkGray;
            instance.GuiPrefs.Theme.Colors.Choice = Color.Thistle;
            instance.GuiPrefs.Theme.Colors.Object = Color.LightGray;
            instance.GuiPrefs.Theme.Colors.Background = Color.FromArgb(48, 48, 48);
            instance.GuiPrefs.Theme.Colors.LightForeground = Color.White;
            instance.GuiPrefs.Theme.Colors.DarkBackground = Color.Black;
            instance.GuiPrefs.Theme.Colors.TextHighLightBack = Color.DarkGray;
            instance.GuiPrefs.Theme.Colors.TextHighLightFore = Color.White;
            instance.GuiPrefs.Theme.ThemeName = ENUM.ThemeColorName.Gray;
        }
        #endregion
        public static void SwitchUserBaseOnBetaOrNot()
        {
            // Change user to super
            if (Globals.AppName.ToUpper() == "TIRAMISU" && instance.GuiPrefs.UserType == Preferences.UserTypes.Guided)
                instance.GuiPrefs.UserType = Preferences.UserTypes.Advanced;
            else
                 if (instance.GuiPrefs.UserType == Preferences.UserTypes.Super && !System.Diagnostics.Debugger.IsAttached)
                instance.GuiPrefs.UserType = Preferences.UserTypes.Advanced;
        }

        #region export

        public void Export(string path)
        {
            TSU.IO.Xml.CreateFile(this.GuiPrefs, path);
        }

        #endregion

        public void AddButtonToUsedList(Views.BigButton b)
        {
            // bring to top if exist or add in front
            string buttonHash = b.GetHashCode().ToString();
            int position = GuiPrefs.LastUsedButtonsText.FindIndex(x => x == buttonHash);
            if (position >= 0) GuiPrefs.LastUsedButtonsText.RemoveAt(position);
            GuiPrefs.LastUsedButtonsText.Insert(0, b.GetHashCode().ToString());

            // Max 4 in the list
            while ((GuiPrefs.LastUsedButtonsText.Count > 4)) GuiPrefs.LastUsedButtonsText.RemoveAt(4);

            GuiPrefs.LastUsedButtons.Clear(); // so that it will be loadded again from the LastUsedButtonsText list
            this.Export(Paths.GuiPreferences);
        }

        private CultureInfo defaultCulture;
        private CultureInfo defaultUICulture;

        internal void CheckDecimalSeparator()
        {
            var dc = defaultCulture.NumberFormat.NumberDecimalSeparator;
            var dUc = defaultUICulture.NumberFormat.NumberDecimalSeparator;
            Debug.WriteInConsole($"defaultCulture.NumberFormat.NumberDecimalSeparator={dc}");
            Debug.WriteInConsole($"defaultUICulture.NumberFormat.NumberDecimalSeparator={dUc}");
            if (dc != ".")
                throw new Exception($"{R.T570} dc = '{dc}', dUIc = '{dUc}'");
            else if (dUc != ".")
                Logs.Log.AddEntryAsPayAttentionOf(null, "You seems to have the right ('.') decimal separator (but the UI Culture");
            else
                Logs.Log.AddEntryAsFinishOf(null, "You have the right ('.') decimal separator");
        }

        internal void AdminAndNetworkChecks()
        {
            void WriteLog(string s)
            {
                Logs.Log.AddEntry(this, s, Logs.LogEntry.LogEntryTypes.FYI);
            }

            var isAdmin = T.Access.IsAdministrator();
            WriteLog($"IsAdministrator={isAdmin}");

            var isCurrentProcessAdmin = T.Access.IsCurrentProcessAdmin();
            WriteLog($"IsCurrentProcessAdmin={isCurrentProcessAdmin}");

            if (isAdmin && isCurrentProcessAdmin)
            {
                foreach (var s in WiFi.GetWifiInteraceWithNetsh())
                    WriteLog($"Interface Found : \"{s.AdminStateEnabled},{s.StateConnected},{s.InterfaceName}\"");
                WiFi.Debug_ListNetworkInterfaces(WriteLog);
                WiFi.Debug_ListWifiInterfaces(WriteLog);
                WiFi.Debug_ListSSIDs(WriteLog);
            }
        }
    }

}
