using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using TSU.Common.Compute;
using TSU.Common.Compute.Transformation;
using TSU.Common.Elements;
using TSU.Common.Instruments.Reflector;
using TSU.IO.SUSoft;
using UnitTestGenerator;
using E = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using P = TSU.Preferences;
using R = TSU.Properties.Resources;

namespace TSU.Polar
{
    [Serializable]
    [XmlType(Namespace = "TSU.Polar", TypeName = "Measure")]
    public class Measure : TSU.Common.Measures.Measure, ICloneable, System.IEquatable<Measure>, IComparable<Measure>
    {
        //public bool GotoWanted { get; set; } = false;
        public bool DirectMeasurementWanted { get; set; } = false;

        public override string _Name
        {
            get
            {

                if (this.name != "Unknown" && !this.name.Contains("#"))
                    return this.name;
                string name = "";
                if (this._Point != null)
                    name = this._Point._Name;
                else
                    name = this._PointName;
                return $"Pm of {name}";
            }
        }


        public Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation StationVerticalisationState { get; set; }

        public virtual int NumberOfMeasureToAverage { get; set; }

        [XmlAttribute]
        public virtual bool UsedAsReference
        {

            get
            {
                return (
                    GeodeRole == Geode.Roles.Cala ||
                    GeodeRole == Geode.Roles.Radi);
            }
        }// virtaul for mockup...

        /// <summary>
        /// Special for AT, to remember teh mode used i.e. 'outdoor', 'fast',etc
        /// </summary>
        public virtual string ModeOfMeasure { get; set; }
        public virtual DoubleValue TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates { get; set; }
        public virtual M.MeasureOfAngles Angles { get; set; }
        public virtual M.MeasureOfAngles AnglesF1 { get; set; }
        public virtual M.MeasureOfAngles AnglesF2 { get; set; }


        public virtual I.SigmaForAInstrumentCouple APrioriSigmas { get; set; }

        public bool HasCorrectedAngles
        {
            get
            {
                return this.Angles.Corrected.Horizontal.Value != TSU.Tsunami2.Preferences.Values.na && this.Angles.Corrected.Vertical.Value != TSU.Tsunami2.Preferences.Values.na;
            }
        }

        public bool HasRawAngles
        {
            get
            {
                if (this.Angles.Raw == null) return false;
                return this.Angles.Raw.Horizontal.Value != TSU.Tsunami2.Preferences.Values.na && this.Angles.Raw.Vertical.Value != TSU.Tsunami2.Preferences.Values.na;
            }
        }

        public virtual M.MeasureOfDistance Distance { get; set; }
        public virtual M.MeasureOfDistance DistanceF1 { get; set; }
        public virtual M.MeasureOfDistance DistanceF2 { get; set; }

        [XmlIgnore]
        public M.DistanceHorizontal distanceHorizontal
        {
            get
            {
                double na = TSU.Tsunami2.Preferences.Values.na;
                if (Distance.Corrected != null && Angles.corrected.Vertical != null)
                {
                    M.DistanceHorizontal d = new M.DistanceHorizontal();
                    if (!(Distance.Corrected.Value == na || Angles.corrected.Vertical.Value == na))
                    {
                        d.Value = Survey.GetHorizontalDistance(Angles.corrected.Vertical.Value, Distance.Corrected.Value);
                    }
                    return d;
                }
                else
                    return null;
            }
        }


        [XmlAttribute]
        public virtual I.FaceType Face { get; set; }

        [XmlAttribute]
        public virtual bool IsMeasured
        {
            get
            {
                bool hor = (Angles.Raw.Horizontal.Value != TSU.Tsunami2.Preferences.Values.na || Angles.Corrected.Horizontal.Value != TSU.Tsunami2.Preferences.Values.na);
                bool ver = (Angles.Raw.Vertical.Value != TSU.Tsunami2.Preferences.Values.na || Angles.Corrected.Vertical.Value != TSU.Tsunami2.Preferences.Values.na);
                bool dis = (Distance.Raw.Value != TSU.Tsunami2.Preferences.Values.na || Distance.Corrected.Value != TSU.Tsunami2.Preferences.Values.na);

                return (hor && ver) || dis;
            }
        }

        [XmlAttribute]
        public virtual bool IsFullyMeasured
        {
            get
            {
                bool hor = (Angles.Raw.Horizontal.Value != TSU.Tsunami2.Preferences.Values.na || Angles.Corrected.Horizontal.Value != TSU.Tsunami2.Preferences.Values.na);
                bool ver = (Angles.Raw.Vertical.Value != TSU.Tsunami2.Preferences.Values.na || Angles.Corrected.Vertical.Value != TSU.Tsunami2.Preferences.Values.na);
                bool dis = (Distance.Raw.Value != TSU.Tsunami2.Preferences.Values.na || Distance.Corrected.Value != TSU.Tsunami2.Preferences.Values.na);

                return hor && ver && dis;
            }
        }

        public virtual bool _IsClosingMeasure { get; set; }
        [XmlIgnore]
        public virtual Reflector Reflector
        {
            get
            {
                return this.Distance.Reflector;
            }
            set
            {
                if (value is Reflector)
                {
                    if (this.DistanceF1 == null) { this.DistanceF1 = new M.MeasureOfDistance(); }
                    if (this.DistanceF2 == null) { this.DistanceF2 = new M.MeasureOfDistance(); }
                    if (this.Distance == null) { this.Distance = new M.MeasureOfDistance(); }
                    this.Distance.Reflector = value;
                    this.DistanceF1.Reflector = value;
                    this.DistanceF2.Reflector = value;
                }
            }
        }
        public E.CoordinatesInAllSystems Offsets { get; set; }

        [XmlIgnore]
        public int Delay { get; internal set; } = 0;

        [XmlIgnore]
        public int IdForLgc { get; internal set; }

        /// <summary>
        /// return a clone as a MeasareTheodolite
        /// </summary>
        /// <returns></returns>
        public Polar.Measure GetNiceClone()
        {
            return this.Clone() as Polar.Measure;
        }

        public override object Clone()
        {
            //var newMeasure = (MeasureTheodoliteOneFace)base.Clone(); // call clone from measure
            Polar.Measure newMeasure = (Polar.Measure)this.MemberwiseClone(); // to make a clone of the primitive types fields
            newMeasure._Status = (M.State)_Status.Clone();
            newMeasure.Angles = (M.MeasureOfAngles)Angles.Clone();
            newMeasure.Distance = (M.MeasureOfDistance)Distance.Clone();
            newMeasure.Face = Face;
            newMeasure.Extension = Extension.Clone() as DoubleValue;
            newMeasure.NumberOfMeasureToAverage = NumberOfMeasureToAverage;
            newMeasure._IsClosingMeasure = _IsClosingMeasure;

            newMeasure.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates = (DoubleValue)TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates.Clone();

            // to be done when needed after the clone call
            //newMeasure._Point = _Point.Clone() as TSU.Common.Elements.Point;
            //newMeasure.Guid = Guid.NewGuid();

            return newMeasure;
        }
        public Measure()
        {
            this._Status = new M.States.Unknown();
            this.Angles = new M.MeasureOfAngles();
            this.Distance = new M.MeasureOfDistance();
            this.Extension = new DoubleValue(0, 0);
            this.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates = new DoubleValue(0, 0);
            this.Reflector = new I.Reflector.Reflector();
            this.NumberOfMeasureToAverage = 2;
            this._IsClosingMeasure = false;
        }

        public override string ToString()
        {
            try
            {
                int precD = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
                int precA = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;
                // Good 'doubleface measurement' of 'Point' with 'reflector name' on 'No extension' and with the comment 'coment'.

                // extension
                string ext = "";
                if (this.Extension.Value == 0)
                    ext += R.Noextension;
                else
                {
                    if (this.Extension.Value == TSU.Tsunami2.Preferences.Values.na)
                    {
                        if (this._Point.SocketCode != null)
                            ext += R.ExtensionOf + " " + new DoubleValue(this._Point.SocketCode.DefaultExtensionForPolarMeasurement).ToString(precD - 3, 1000) + " m";
                        else
                            ext += "0.00 m";
                    }
                    else
                        ext += R.ExtensionOf + " " + this.Extension.ToString(precD - 3, 1000) + " m";
                }

                // code
                string code = "(code: ";
                if (this._Point?.SocketCode == null)
                    code += "N/a";
                else
                    code += this._Point.SocketCode.Id;
                code += ")";

                // comment
                string comment =
                    (this.CommentFromUser == "" || this.CommentFromUser == R.T_NO_COMMENT) ?
                    "" :
                    " and with the comment '" + this.CommentFromUser + "'";

                // obs
                string observations =
                    (this.IsMeasured) ?
                    string.Format(R.TS_TheoResult,
                        this.Angles.Corrected.Horizontal.ToString(precA, showSigma:true),
                        this.Angles.Corrected.Vertical.ToString(precA, showSigma: true),
                        this.Distance.Corrected.ToString(precD, showSigma: true)) :
                        "";

                // reflector
                string reflector = "";
                if (this.Distance.Reflector == null || this.Distance.Reflector._Name == R.String_Unknown)
                    reflector += "unknown reflector";
                else
                    reflector += this.Distance.Reflector._Name;

                string name = this._Point != null ? this._Point._Name : this.PointGUID.ToString("B");
                return $"Polar {this._Status._Name} '{this.NumberOfMeasureToAverage}-{this.Face} measurement' of '{name}' with '{reflector}' on {ext} {code} {comment}. Resulting observations are {observations}";
            }
            catch (Exception ex)
            {
                return $"Cannot write the theodolite measure as a string {ex.Message}.";
            }
        }

        /// <summary>
        /// Return a string base on a specified type:
        /// "err" will return angle diffferences and the inply distance error in the Station cs.
        /// "Ls" return a line for each info
        /// "L" for  as sentence with all info
        /// </summary>
        /// <param name="TypeOfFormating"></param>
        /// <returns></returns>
        public new string ToString(string TypeOfFormating)
        {
            if (TypeOfFormating == "err")
            {
                // Good 'doubleface measurement' of 'Point' with 'reflector name' on 'No extension' and with the comment 'coment'.

                int nAngle = (TSU.Tsunami2.Preferences.Values.GuiPrefs == null) ? 5 : TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;
                int nDist = (TSU.Tsunami2.Preferences.Values.GuiPrefs == null) ? 7 : TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;

                string s = "";
                s += string.Format("dHz = {0,10} gon  => {1,-10} mm", this.Angles.Corrected.Horizontal.ToString(nAngle), this._Point._Coordinates.StationCs.X.ToString(nDist - 3, 1000)) + "\r\n";
                s += string.Format("dVt = {0,10} gon  => {1,-10} mm", this.Angles.Corrected.Vertical.ToString(nAngle), this._Point._Coordinates.StationCs.Y.ToString(nDist - 3, 1000)) + "\r\n";
                s += string.Format("dDt = {0,10} m     => {1,-10} mm", this.Distance.Corrected.ToString(nDist), this._Point._Coordinates.StationCs.Z.ToString(nDist - 3, 1000)) + "\r\n";
                return s;

            }

            if (TypeOfFormating == "Ls" || TypeOfFormating == "L")
            {
                string ext =
                (this.Extension.Value == 0) ?
               R.Noextension :
               R.ExtensionOf + " " + this.Extension.Value.ToString() + " m";

                string comment =
                    (this.Comment == "" || this.Comment == R.T_NO_COMMENT) ?
                    R.T_NO_COMMENT :
                    "'" + this.CommentFromUser + "'";



                string observations = "";
                string source = "";
                if (TypeOfFormating == "Ls")
                {
                    source = "{0} '{1}' of '{2}'\r\n\tPrism = '{3}'\r\n\tRallonge = {4} \r\n\tComment = {5}\r\n{6}";
                    observations =
                    (this.IsMeasured) ?
                    string.Format("Correct observation: \r\n\tAH = {0} gon\r\n\tAV = {1} gon\r\n\tD = {2} m",
                        this.Angles.Corrected.Horizontal.ToString(5),
                        this.Angles.Corrected.Vertical.ToString(5),
                        this.Distance.Corrected.ToString(6)) :
                        "";
                }
                else
                {
                    source = "{0} '{1} measurement' of '{2}' with '{3}' on {4} and with the comment {5}\r\n{6}";
                    observations =
                    (this.IsMeasured) ?
                    string.Format(R.TS_TheoResult,
                        this.Angles.Corrected.Horizontal.ToString(5),
                        this.Angles.Corrected.Vertical.ToString(5),
                        this.Distance.Corrected.ToString(6)) :
                        "";
                }
                return string.Format(source,
                    this._Status._Name,
                    this.Face,
                    this._Point._Name,
                    (this.Distance.Reflector == null || this.Distance.Reflector._Name == R.String_Unknown) ? "unknown reflector" : this.Distance.Reflector._Name,
                    ext,
                    comment,
                    observations);
            }

            // else

            return this.ToString();
        }
        public override string ToString_List_Diff_Poin(bool returnHeader = false)
        {
            string s = "";
            int nD = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
            int nA = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;

            if (returnHeader) s = string.Format("{0,-10}{1,-25}{2,-15}{3,9}  {4,15}{5,15}{6,15}{7,15}", "State", "Name", "Reflector", "Ext.[mm]", "AH [gon]", "AV [gon]", "Dist [m]", "Time (day)") + "\r\n";

            string extention;
            int zeros = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
            int zerosmm = zeros - 3;
            if (TSU.Tsunami2.Preferences.Values.GuiPrefs.UnitForExtension == P.Preferences.DistanceUnit.mm)
                extention = (this.Extension.Value * 1000).ToString("F" + zerosmm.ToString());
            else
                extention = (this.Extension.Value).ToString("F" + zeros.ToString());
            s += string.Format("{0,-10}{1,-25}{2,-15}{3,9}  {4,15}{5,15}{6,15}{7,15}",
                    (this._Status is M.States.Questionnable) ? "Quest�?" : this._Status._Name,
                    this._Point._Name,
                    this.Distance.Reflector._Name,
                    extention,
                    this.Angles.Corrected.Horizontal.ToString(nA),
                    this.Angles.Corrected.Vertical.ToString(nA),
                    this.Distance.Corrected.ToString(nD),
                    this._Date.ToString("hh:mm:ss (d)"));
            return s;
        }



        public bool Equals(Polar.Measure obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || this.GetType() != obj.GetType())
                return false;

            Polar.Measure p = (Polar.Measure)obj;
            return (_Date == p._Date) && (_Point == p._Point);
        }
        //public override int GetHashCode()
        //{
        //    int angleHash = this.Angles.GetHashCode();
        //    int DistHash = this.Distance.GetHashCode();
        //    return angleHash ^ DistHash;
        //}
        // Default comparer for Management.Measure of level to sort the list.
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            Polar.Measure objAsMeas = obj as Polar.Measure;
            if (objAsMeas == null) return false;
            else return Equals(objAsMeas);
        }
        public int CompareTo(object obj)
        {
            if (obj == null) return 1;
            Polar.Measure objAsMeas = obj as Polar.Measure;
            if (objAsMeas == null) return 1;
            else return CompareTo(objAsMeas);
        }
        public int CompareTo(Polar.Measure compareMeas)
        {
            // A null value means that this object is greater.
            if (compareMeas == null)
                return 1;

            else
                return this._Point._Parameters.Cumul.CompareTo(compareMeas._Point._Parameters.Cumul);
        }
        /// <summary>
        /// Clean the RAW observations (Corrected ones stay by default to allow goto) and the corrections flags
        /// </summary>
        internal void Clean(bool removeAnglesForGoto = false)
        {
            // attention should be 'raw' and not 'Raw' not to clean the corrected angle an be able to do the goto
            this.Angles.raw = new E.Angles();
            if (this.AnglesF1 != null) this.AnglesF1.raw = new E.Angles();
            if (this.AnglesF2 != null) this.AnglesF2.raw = new E.Angles();

            this.Distance.Clean();
            if (this.DistanceF1 != null) this.DistanceF1.Clean();
            if (this.DistanceF2 != null) this.DistanceF2.Clean();

            if (removeAnglesForGoto)
            {
                this.Angles.corrected = new E.Angles();
                if (this.AnglesF1 != null) this.AnglesF1.corrected = new E.Angles();
                if (this.AnglesF2 != null) this.AnglesF2.corrected = new E.Angles();


                this.Distance.corrected = new DoubleValue();
                this.Distance.Raw = new DoubleValue();
                if (this.DistanceF1 != null)
                {
                    this.DistanceF1.corrected = new DoubleValue();
                    this.DistanceF1.Raw = new DoubleValue();
                }
                if (this.DistanceF2 != null)
                {
                    this.DistanceF2.corrected = new DoubleValue();
                    this.DistanceF2.Raw = new DoubleValue();
                }
            }

            this.CommentFromTsunami = "";
            this.CommentPrecisionFromTsunami = "";
            this.CommentFromUser = "";

        }
        public Polar.Measure DeepCopy()
        //copie avec un memory stream de toute la station level sauf l'instrument et la mire dont on garde la r�f�rence ancienne
        {
            using (MemoryStream stream = new MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                Polar.Measure deepCopy = (Polar.Measure)formatter.Deserialize(stream);
                //deepCopy._actualReflector = this._actualReflector.DeepCopy();

                deepCopy._Point = this._Point.DeepCopy();
                return deepCopy;
            }
        }
        internal void SelectDefaultExtension()
        {
            if (this._Point.SocketCode == null) this._Point.SocketCode = new E.SocketCode() { Id = "0" };
            this.Extension.Value = this._Point.SocketCode.DefaultExtensionForPolarMeasurement;
        }
        /// <summary>
        /// Switch from 1 face to double face for length setting
        /// </summary>
        internal void SwitchFace()
        {
            if (this.Face == I.FaceType.DoubleFace)
            {
                this.Face = I.FaceType.Face1;
            }
            else
            {
                this.Face = I.FaceType.DoubleFace;
            }
        }
        /// <summary>
        /// Change le reflecteur pour la mesure
        /// </summary>
        /// <param name="newReflector"></param>
        internal void SetActualReflector(Reflector newReflector)
        {
            this.Distance.Reflector = newReflector;
            this.DistanceF1.Reflector = newReflector;
            this.DistanceF2.Reflector = newReflector;
        }
        /// <summary>
        /// COnverti le H en Z CCS pour les coordonn�es du point pour une mesure en tenant compte de la rallonge
        /// </summary>
        /// <param name="geoide"></param>
        public void ConvertHToZMeasure(Coordinates.ReferenceSurfaces geoide)
        {
            Systems.ConvertHToZWithOffset(geoide, this.Extension.Value, this._Point);
        }
        /// <summary>
        /// COnverti le H en Z CCS pour les coordonn�es du point pour une station en tenant compte de la h instrument
        /// </summary>
        /// <param name="geoide"></param>
        public void ConvertHToZStation(Coordinates.ReferenceSurfaces geoide, double hInstrument)
        {
            Systems.ConvertHToZWithOffset(geoide, hInstrument, this._Point);
        }


        /// <summary>
        /// Transforme en MLA en choisissant la station comme origine
        /// </summary>
        /// <param name="ori"></param>
        /// <param name="geoid"></param>
        /// <returns></returns>
        public TSU.Result CCSToMLA(Polar.Measure station, Coordinates.ReferenceSurfaces geoid)
        {
            long resMLA = 0;
            TSU.Result result = new TSU.Result();
            var cZ = station._Point._Coordinates.GetCoordinatesInASystemByName("CCS-Z");
            double xOri = cZ.X.Value;
            double yOri = cZ.Y.Value;
            double zOri = cZ.Z.Value;
            double xTransform = this._Point._Coordinates.Ccs.X.Value;
            double yTransform = this._Point._Coordinates.Ccs.Y.Value;
            double zTransform = this._Point._Coordinates.Ccs.Z.Value;
            //resMLA = TSU.IO.SUSoft.SpatialObjDLL_32bit.TransformToMLA(xOri, yOri, zOri, ref xTransform, ref yTransform, ref zTransform, geoid);
            bool ok = TSU.Common.Compute.Transformation.Systems.Ccs2Mla(xOri, yOri, zOri, ref xTransform, ref yTransform, ref zTransform, geoid);
            if (!ok)
            {
                result.Success = false;
                result.ErrorMessage = R.T394;
                return result;
            }
            this._Point._Coordinates.Mla.X.Value = xTransform;
            this._Point._Coordinates.Mla.Y.Value = yTransform;
            this._Point._Coordinates.Mla.Z.Value = zTransform;
            return result;
        }
        /// <summary>
        /// Calcule la distance horizontale theorique entre la station et le point. Attention les coordonn�es doivent �tre en MLA
        /// </summary>
        /// <param name="measStation"></param>
        internal void CalculateDistHorizTheo(Polar.Measure measStation)
        {
            double na = TSU.Preferences.Preferences.NotAvailableValueNa;
            if (this._Point._Coordinates.Mla.X.Value != na && this._Point._Coordinates.Mla.Y.Value != na)
            {
                double X1 = measStation._Point._Coordinates.Mla.X.Value;
                double Y1 = measStation._Point._Coordinates.Mla.Y.Value;
                double X2 = this._Point._Coordinates.Mla.X.Value;
                double Y2 = this._Point._Coordinates.Mla.Y.Value;
                this.Distance.distTheoHoriz.Value = Math.Sqrt(Math.Pow(X1 - X2, 2) + Math.Pow(Y1 - Y2, 2));
            }
        }

    }





}
