﻿using System;
using System.Collections.Generic;
using System.Text;
using R = TSU.Properties.Resources;
using System.Windows.Forms;
using TSU.ENUM;
using System.Xml.Serialization;
using TSU.Views;
using E = TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using TSU.Common.Instruments;
using TSU.Common.Measures;
using TSU.Common.Operations;
using T = TSU.Tools;
using D = TSU.Common.Instruments.Device;
using L = TSU.Common.Compute.Compensations.Lgc2;
using LI = TSU.Common.Compute.Compensations.Lgc2.Input;
using LII = TSU.Common.Compute.Compensations.Lgc2.Input.InputLine;
using P = TSU.Common.Compute.Compensations.Plgc1;
using PI = TSU.Common.Compute.Compensations.Plgc1.Input;
using PII = TSU.Common.Compute.Compensations.Plgc1.Input.InputLine;
using TSU.IO.SUSoft;
using TSU.Common.Compute.Compensations;

namespace TSU.Polar.Compensations
{
    [Serializable]
    public class GotoForStationSettedUp : TSU.Common.Compute.Compensations.Strategies.Common
        {
            public GotoForStationSettedUp()
            {

            _strategy = TSU.Common.Compute.Compensations.Strategies.List.GotoForStationSettedUp;
            }
            public GotoForStationSettedUp(Polar.Station.Module module)
                : base(module)
            {
                _strategy = TSU.Common.Compute.Compensations.Strategies.List.GotoForStationSettedUp;
            }

        public override void Create_PLGC1_Input(Polar.Station station, string fileName, P plgc1, string title = "")
        {
        }

        public override void Create_LGC2_Input(Polar.Station station, string fileName, L lgc2, string title = "",  bool temporaryCompute = false)
        {
            lgc2.TreatFileName(fileName, "Goto");
            try
            {
                Polar.Station.Parameters p = station.Parameters2 as Polar.Station.Parameters;

                // create a virtual point on the Y axis (north)
                E.Point ghost = p._StationPoint.DeepCopy() as E.Point;
                ghost._Name = "GHOST";
                E.Coordinates c = lgc2.getTheRightCoordinates(ghost);
                if (ghost._Coordinates.Ccs.AreKnown && ghost._Coordinates.Ccs.Y.Value == c.Y.Value)
                    ghost._Coordinates.Ccs.Y.Value += 100;
                else if(ghost._Coordinates.Local.AreKnown && ghost._Coordinates.Local.Y.Value == c.Y.Value)
                    ghost._Coordinates.Local.Y.Value += 100;
                
                // Create
                if (title == "") title = $"{R.InputTSU} 'for Orientation Only' {T.Conversions.Date.ToDateUpSideDown(DateTime.Now)} ";
                string content = "";
                content += LII.Titr(title + " for goto computation");

                // Refence surface
                string refSurfOption = lgc2.getTheRightOption();
                content += refSurfOption + "\r\n";
                //content += InputLine.OptionFaut(); pas besoin de falut pour goto
                ///if (refSurfOption.ToUpper() != "*OLOC")
                //    content += InputLine.OptionCreatePunchHFile();
                // else
                //    content += InputLine.OptionCreatePunchFile();

                content += LII.OptionJSON(true);
                content += LII.Precision(7);
                content += LII.Instr();
                IPolarInstrument intrument = station.Parameters2._Instrument as IPolarInstrument;
                if (intrument == null) throw new Exception("Instrument is not selected or not a polar sensor");
                content += LII.Polar(intrument,
                                                T.Research.GetListOfReflectorUsedIn(station),
                                                new DoubleValue(0.0, 0.0),
                                                new DoubleValue(0.0, 0.0));

                // *CALA
                {
                    List<E.Point> cala = lgc2._Input.CALAs;
                    cala.Clear();

                    cala.Add(ghost);
                    cala.Add(station.Parameters2._StationPoint);
                    if (station.NextMeasure!=null)
                        cala.Add(station.NextMeasure._OriginalPoint);

                    E.Coordinates barycenter = null;
                    content += LII.AddCalaPoints(cala, lgc2.getTheRightCoordinates, out barycenter, station.MeasuresTaken);
                }
                

                string temp = LII.Tstn(station, true, true, useObservationSigma: false, useDistance: true, lgc2: lgc2);
                temp = temp.Replace("*ANGL", "*ANGL\r\n" + "   " + "   " + "   " + ghost._Name.ToUpper() + " " + (400 - station.Parameters2.Setups.BestValues.vZero.Value).ToString() );
                content += temp;
                content += LII.End();

                lgc2._Input.CreateInputFile(content);
            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_IMPOSSIBLE_TO_CREATE_INPUT_FOR_GOTO}\r\n{ex.Message}", ex);
            }
            
        }

        internal override void SetDefaultMeasureSelection(List<TsuObject> allElements)
        {
            foreach (CompensationItem item in allElements) // so selec all the one used
            {
                if (item.Measure.IsGood)
                {
                    item.Measure.GeodeRole = Geode.Roles.Cala;
                }
                else
                    item.Measure.GeodeRole = Geode.Roles.Unused;
            }
        }


        public override bool Update()
            {
                //if (CompensationResults == null) return false;
                //if (!CompensationResults.Ready) return false;

                //Updater.UpdateMeasuredPoint(station, CompensationResults.VariablePoints);

                return true;
            }

            internal override void CheckGeoid()
            {
                // fill delegate wih the right code to get the right coordincate
                this.CheckIfGeoidalComputeIsPossible(station);
            }

            internal override void ChooseMeasureForApproximateCompute()
            {
                // nothing to do setup is done
            }
            internal override void ComputeApproximateSetup()
            {
                // nothing to do setup is done
            }
            internal override void ChooseMeasureForCompensation()
            {
                // nothing to do setup is done
            }
            internal override void Compensate()
            {
                CompensationResults = base.RunLGC(false,false);
            }
            internal override void FindErrors()
            {
            }
            internal override string GetSetupLGC2Details()
            {
                try
                {
                    return string.Format("sigma0 = {0}, {1} = {2}",
                         CompensationResults.SigmaZero.ToString("F2"),
                         R.T_INSTRUMENT_HEIGHT, CompensationResults.InstrumentHeight);

                }
                catch (Exception ex)
                {
                    return $"{ R.T_COMPENSATION } {R.T_FAILED}: {ex.Message}";
                }
            }

            internal override string GetConfidenceEllipsoid1Sigma()
            {
                return "";
            }

            internal override string RenameFile(string fileName)
            {
                if (fileName == "")
                {
                    string folder = $@"{TSU.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay}computes\(P)LGC\Goto";
                    System.IO.Directory.CreateDirectory(folder);
                    return $@"{folder}\{TSU.Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now)}";
                }
                else
                    return fileName;
            }
    }
}
