﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using R = TSU.Properties.Resources;
using System.Windows.Forms;
using TSU.ENUM;
using System.Xml.Serialization;
using TSU.Views;
using E = TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using TSU.Common.Instruments;
using M = TSU.Common.Measures;
using TSU.Common.Operations;
using T = TSU.Tools;
using D = TSU.Common.Instruments.Device;
using L = TSU.Common.Compute.Compensations.Lgc2;
using LI = TSU.Common.Compute.Compensations.Lgc2.Input;
using LII = TSU.Common.Compute.Compensations.Lgc2.Input.InputLine;
using P = TSU.Common.Compute.Compensations.Plgc1;
using PI = TSU.Common.Compute.Compensations.Plgc1.Input;
using PII = TSU.Common.Compute.Compensations.Plgc1.Input.InputLine;
using TSU.IO.SUSoft;
using TSU.Common.Compute.Compensations;

namespace TSU.Polar.Compensations
{
    [Serializable]
    public class PointLancésLGC2 : TSU.Common.Compute.Compensations.Strategies.Common
    {
        public PointLancésLGC2()
        {
            _strategy = TSU.Common.Compute.Compensations.Strategies.List.PointLancés_LGC2;
        }
        public PointLancésLGC2(Polar.Station.Module module)
            : base(module)
        {
            _strategy = TSU.Common.Compute.Compensations.Strategies.List.PointLancés_LGC2;
        }

        public override bool Update()
        {
            if (CompensationResults == null) return false;
            if (!CompensationResults.Ready) return false;

            // make a tag that the updater know
            foreach (E.Point item in CompensationResults.VariablePoints)
            {
                if (item._Origin == R.String_Unknown)
                    item._Origin = "FAKE";
            }



            TSU.Common.Station.Updater.UpdateMeasuredPointsInStation(station, CompensationResults.VariablePoints,isApproximative: false); 

            return true;
        }

        internal override void SetDefaultMeasureSelection(List<TsuObject> allElements)
        {
            foreach (CompensationItem item in allElements) // so selec all the one used
            {
                if (item.Measure.IsGood)
                {
                    if (item.Measure._OriginalPoint._Coordinates.HasAny)
                    {
                        item.Measure.GeodeRole = Geode.Roles.Cala;
                    }
                    else
                        item.Measure.GeodeRole = Geode.Roles.Poin;
                }
                else
                    item.Measure.GeodeRole = Geode.Roles.Unused;
            }
        }
        

        public override void Create_PLGC1_Input(Polar.Station station, string fileName, P plgc1, string title = "")
        {
            try
            {
                if (title == "") title = R.T041;
                string content = "";

                content += PII.Titr(title);
                content += PII.OptionOloc();
                content += PII.AddVariablePoints(station.PointsMeasured, plgc1.getTheRightCoordinates);

                // create a virtual point on the Y axis (north)
                Polar.Station.Parameters p = station.Parameters2 as Polar.Station.Parameters;
                E.Point ghost = p._StationPoint.DeepCopy() as E.Point;
                ghost._Name = "GHOST";
                ghost.StorageStatus = Common.Tsunami.StorageStatusTypes.Temp;
                // with get coordinate we receive a clone so i have to add to all Ycoordinates defore the gettcoorinate
                if (ghost._Coordinates.Local.AreKnown) 
                    ghost._Coordinates.Local.Y.Value += 1;
                ghost._Coordinates.Ccs.Y.Value += 1;
                CloneableList<E.Point> calaPoints = new CloneableList<E.Point>();
                calaPoints.Add(ghost);
                calaPoints.Add(station.Parameters2._StationPoint);

                content += PII.AddCalaPoints(calaPoints, plgc1.getTheRightCoordinates);

                //make a fake station with needed info
                Polar.Station s = new Polar.Station();
                s.Parameters2 = station.Parameters2;
                // add last measure and the ghost one;
                s.MeasuresTaken.AddRange(station.MeasuresTaken);
                Polar.Measure m = new Polar.Measure();
                m._Point = ghost;
                m._OriginalPoint = ghost;
                m._Status = new M.States.Good();

                m.Angles.raw = new E.Angles() { Horizontal = new DoubleValue(400 - station.Parameters2.Setups.BestValues.vZero.Value, 0), Vertical = new DoubleValue() };

                m.Distance.Raw = new DoubleValue();
                s.MeasuresTaken.Add(m);

                content += PII.Tstn(s, plgc1.getTheRightCoordinates);
                content += PII.End();

                //this.usingGeoid = InputLine.usingGeoid;
                System.IO.File.WriteAllText(fileName + ".pinp", content);
            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_PROBLEM_TO_CREATE_PLGC_INPUT_FOR_POINTLANCÉ}\r\n{ex.Message}", ex);
            }
        }

        /// <summary>
        /// This will create an input with the station as cala, a orentation point theoreticaly computed with the V0 of the station
        /// </summary>
        /// <param name="station"></param>
        /// <param name="title"></param>
        public override void Create_LGC2_Input(Polar.Station station, string fileName, L lgc2, string title = "",  bool temporaryCompute = false)
        {
            lgc2.TreatFileName(fileName, "PointLancés");
            try
            {
                Polar.Station.Parameters p = station.Parameters2 as Polar.Station.Parameters;

                // create a virtual point on the Y axis (north)
                E.Point ghost = p._StationPoint.DeepCopy() as E.Point;
                ghost._Name = "Ghost";
                E.Coordinates c = lgc2.getTheRightCoordinates(ghost);
                //if (ghost._Coordinates.Local.AreKnown && ghost._Coordinates.Local.Y.Value == c.Y.Value)
                //    ghost._Coordinates.Local.Y.Value += 1;
                //else
                    ghost._Coordinates.Ccs.Y.Value += 1;
                // Create
                if (title == "") title = $"{R.InputTSU} 'point lancés' {T.Conversions.Date.ToDateUpSideDown(DateTime.Now)} ";
                string content = "";
                content += LII.Titr(title);

                // Refence surface
                string refSurfOption = lgc2.getTheRightOption();
                content += refSurfOption + "\r\n";
                content += LII.OptionFaut();
                if (refSurfOption.ToUpper() != "*OLOC")
                    content += LII.OptionCreatePunchHFile();
                else
                    content += LII.OptionCreatePunchFile();

                content += LII.OptionJSON(true);
                content += LII.Precision(7);
                content += LII.Instr();
                content += LII.Polar(station.Parameters2._Instrument as IPolarInstrument,
                                            T.Research.GetListOfReflectorUsedIn(station),
                                            new DoubleValue(0.0, 0.0),
                                            new DoubleValue(0.0, 0.0));

                List<E.Point> pointsInitialized = new List<E.Point>();

                E.Coordinates barycenter = null;

                // *CALA
                {
                    List<E.Point> cala = lgc2._Input.CALAs;
                    cala.Clear();
                    cala.Add(ghost);
                    cala.Add(station.Parameters2._StationPoint);

                    content += LII.AddCalaPoints(cala, lgc2.getTheRightCoordinates, out barycenter, station.MeasuresTaken);
                    pointsInitialized.AddRange(cala);
                }

                // *POIN
                {
                    List<E.Point> poin = lgc2._Input.POINs;
                    poin.Clear();
                    poin.AddRange(station.PointsMeasured);

                    content += LII.AddVariablePoints(poin, lgc2.getTheRightCoordinates, ref pointsInitialized, "*POIN", pointToBeCommented: null, barycenter);
                }
                
                string temp = LII.Tstn(station, 
                    instrumentHeightKnown: true, 
                    UseOnlyLastMeasure: false, 
                    useObservationSigma: false, 
                    useDistance: true, 
                    lgc2: lgc2);

                temp = temp.Replace("%insert_here\r\n", "   " + "   " + "   " + ghost._Name.ToUpper() + " " + (400 - station.Parameters2.Setups.BestValues.vZero.Value).ToString() + "\r\n");
                content += temp;
                content += LII.End();

                var inputPath = lgc2._Input.CreateInputFile(content);
                Common.Compute.Compensations.Lgc2.ProcessFileControlLines(inputPath);
            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_IMPOSSIBLE_TO_CREATE_INPUT_AS_RADIATED_POINT}\r\n{ex.Message}", ex);
            }
            
        }

        internal override void CheckGeoid()
        {
            // fill delegate wih the right code to get the right coordincate
            this.CheckIfGeoidalComputeIsPossible(station, new PointLancéLGC2());
        }

        internal override void ChooseMeasureForApproximateCompute()
        {
            // nothing to do
        }
        internal override void ComputeApproximateSetup()
        {
            if (Tsunami2.Preferences.Values.UsePLGC)
            {
                try
                {
                    P plgc = new P(station, this, getTheRightCoordinates, getTheRightOption, coordianteSystemType);
                    bool runned = plgc.Run(editInput, seeOutput);
                    if (runned)
                    {
                        TSU.Common.Station.Updater.UpdateMeasuredPoint(station, plgc.GetOutputCoordinates(), true);
                    }

                    this.SaveFilesPath(P._FileName);
                }
                catch (Exception ex)
                {
                    throw new Exception("PLGC failed", ex);
                }
            }
           
        }


        internal override void ChooseMeasureForCompensation()
        {
            // nothing to do
        }
        internal override void Compensate()
        {
            CompensationResults = base.RunLGC(showLog:false, showSigmaO: false);
            
            if (CompensationResults.Ready)
            {
                TSU.Common.Station.Updater.UpdateMeasuredPointsInStation(station, CompensationResults.VariablePoints, false);
            }

            this.SaveFilesPath(lgc2._Output.OutputFilePath);
        }
        internal override void FindErrors()
        {
        }

        internal override string GetSetupLGC2Details()
        {
            try
            {
                return string.Format("sigma0 = {0}, {1} = {2}",
                     CompensationResults.SigmaZero.ToString("F2"),
                     R.T_INSTRUMENT_HEIGHT, CompensationResults.InstrumentHeight);
            }

            catch (Exception ex)
            {
                return $"{ R.T_COMPENSATION } {R.T_FAILED}: {ex.Message}";
            }
        }

        internal override string GetConfidenceEllipsoid1Sigma()
        {
            return "";
        }

        internal override string RenameFile(string fileName)
        {
            if (fileName == "")
            {
                string folder = $@"{TSU.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay}computes\(P)LGC\PointsLances";
                System.IO.Directory.CreateDirectory(folder);
                return $@"{folder}\{TSU.Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now)}";
            }
            else
                return fileName;
        }
    }
}
