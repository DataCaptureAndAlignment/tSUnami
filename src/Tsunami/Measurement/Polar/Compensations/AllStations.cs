﻿using System;
using System.Collections.Generic;
using System.Text;
using R = TSU.Properties.Resources;
using System.Windows.Forms;
using TSU.ENUM;
using System.Xml.Serialization;
using TSU.Views;
using E = TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using TSU.Common.Instruments;
using M = TSU.Common.Measures;
using TSU.Common.Operations;
using T = TSU.Tools;
using D = TSU.Common.Instruments.Device;
using L = TSU.Common.Compute.Compensations.Lgc2;
using LI = TSU.Common.Compute.Compensations.Lgc2.Input;
using LII = TSU.Common.Compute.Compensations.Lgc2.Input.InputLine;
using TSU.IO.SUSoft;
using TSU.Common.Compute.Compensations;
using TSU.Common.Compute.ShapesOutput;

namespace TSU.Polar.Compensations
{
    [Serializable]
    public class AllStations : TSU.Common.Compute.Compensations.Strategies.Common
    {
        public AllStations()
        {
            _strategy = TSU.Common.Compute.Compensations.Strategies.List.AllCala;
        }

        public AllStations(Polar.Station.Module module)
            : base(module)
        {
            _strategy = TSU.Common.Compute.Compensations.Strategies.List.AllCala;
        }


        internal override void SetDefaultMeasureSelection(List<TsuObject> allElements)
        {
            foreach (CompensationItem item in allElements) // so selec all the one used
            {
                if (item.Measure.IsGood)
                {
                    item.Measure.GeodeRole = Geode.Roles.Cala;
                }
                else
                    item.Measure.GeodeRole = Geode.Roles.Unused;
            }
        }

        public override void Create_PLGC1_Input(Polar.Station station, string fileName, Plgc1 plgc1, string title = "")
        {
            throw new NotImplementedException();
        }
        

        public override void Create_LGC2_Input(Polar.Station station, string fileName, L lgc2, string title = "",  bool temporaryCompute = false)
        {
            lgc2.TreatFileName(fileName, "AllCala");

            // gives the right algo in delegates

            Polar.Station.Parameters p = station.Parameters2;
            if (title == "") title = $"{R.InputTSU} 'All cala' {T.Conversions.Date.ToDateUpSideDown(DateTime.Now)} ";
            string content = ""; content += LII.Titr(title + " with all points in CALA");


            // Refence surface
            string refSurfOption = lgc2.getTheRightOption();
            content += refSurfOption + "\r\n";
            content += LII.OptionFaut();
            if (refSurfOption.ToUpper() != "*OLOC")
                content += LII.OptionCreatePunchHFile();
            else
                content += LII.OptionCreatePunchFile();

            content += LII.OptionJSON(true);
            content += LII.Precision(6);
            content += LII.Instr();
            content += LII.Polar(station.Parameters2._Instrument as IPolarInstrument,
                                        T.Research.GetListOfReflectorUsedIn(station),
                                        new DoubleValue(0.0, 0.0),
                                        new DoubleValue(0.0, 0.0));
            //content += InputLine.AddVariablePoints(station.VariablePoints, lgc2.getTheRightCoordinates, "*POIN");
            List<E.Point> all = new List<E.Point>();
            all.AddRange(station.VariablePoints);
            all.AddRange(station.ReferencePoints);
            E.Coordinates barycenter = null;
            content += LII.AddCalaPoints(all, lgc2.getTheRightCoordinates, out barycenter, station.MeasuresTaken);

            //remove fake measur for goto if present
            M.Measure fake = station.MeasuresTaken.Find(x => (x as Measure).Angles.Corrected.Horizontal.Sigma == 200);
            if (fake != null) fake._Status = new M.States.Bad();

            content += LII.Tstn(station,
                (station.Parameters2.Setups.InitialValues.IsInstrumentHeightKnown == Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight.Known),
                UseOnlyLastMeasure: false, useObservationSigma: false, useDistance: true, lgc2: lgc2);

            // restore fake
            if (fake != null) fake._Status = new M.States.Good();

            content += LII.End();

            var inputPath = lgc2._Input.CreateInputFile(content);
            Common.Compute.Compensations.Lgc2.ProcessFileControlLines(inputPath);
        }



        internal override void CheckGeoid()
        {
            // fill delegate wih the right code to get the right coordincate
            this.CheckIfGeoidalComputeIsPossible(station);
        }

        public override bool Update()
        {
            return false;
        }

        internal override void ChooseMeasureForApproximateCompute()
        {

        }

        internal override void ComputeApproximateSetup()
        {

        }

        internal override void ChooseMeasureForCompensation()
        {

        }

        internal override void Compensate()
        {


        }
        internal override void FindErrors()
        {

        }

        internal override string GetSetupLGC2Details()
        {
            try
            {
                return $"{R.T_Sigma0is} {CompensationResults.SigmaZero.ToString("F2")}\r\n" +
                    $"{R.T_INSTRUMENT_HEIGHT} = {CompensationResults.InstrumentHeight}\r\n" +
                    CompensationResults.GetV0Information();

            }
            catch (Exception ex)
            {
                return $"{ R.T_COMPENSATION } {R.T_FAILED}: {ex.Message}";
            }
        }

        internal override string GetConfidenceEllipsoid1Sigma()
        {
            return "";
        }

        internal override string RenameFile(string fileName)
        {
            if (fileName == "")
            {
                string folder = $@"{TSU.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay}computes\(P)LGC\AllCala";
                System.IO.Directory.CreateDirectory(folder);
                return $@"{folder}\{TSU.Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now)}";
            }
            else
                return fileName;
        }
    }
}
