﻿using System;
using System.Collections.Generic;
using System.Text;
using R = TSU.Properties.Resources;
using System.Windows.Forms;
using TSU.ENUM;
using System.Xml.Serialization;
using TSU.Views;
using TSU.Common;
using TSU.Common.Elements.Composites;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using E = TSU.Common.Elements;
using TSU.IO.SUSoft;


using T = TSU.Tools;
using P = TSU.Common.Compute.Compensations.Plgc1;
using PII = TSU.Common.Compute.Compensations.Plgc1.Input.InputLine;
using L = TSU.Common.Compute.Compensations.Lgc2;
using LII = TSU.Common.Compute.Compensations.Lgc2.Input.InputLine;
using TSU.Common.Elements.Manager;
using TSU.Common.Compute;
using TSU.Common.Compute.Compensations;
using TSU.Common.Elements;
using System.Data;
using TSU.Common.Compute.ShapesOutput;

namespace TSU.Polar.Compensations
{
    [Serializable]
    public class NewFrame_LGC2 : TSU.Common.Compute.Compensations.Strategies.Common
    {
        public NewFrame_LGC2()
        {
            _strategy = TSU.Common.Compute.Compensations.Strategies.List.Local_LGC2;
        }
        public NewFrame_LGC2(Polar.Station.Module module)
            : base(module)
        {
            _strategy = TSU.Common.Compute.Compensations.Strategies.List.Local_LGC2;
        }
        internal override void SetDefaultMeasureSelection(List<TsuObject> allElements)
        {
            bool first = true;
            // selected only first measure as openinig
            foreach (CompensationItem item in allElements)
            {
                if (item.Measure.IsGood)
                {
                    if (first)
                    {
                        first = false;
                        item.Measure.GeodeRole = Geode.Roles.Pdor;
                        item.Measure._OriginalPoint._Coordinates = new E.CoordinatesInAllSystems();
                        item.Measure._OriginalPoint._Coordinates.Su = new E.Coordinates("", 0, 1, 0);
                    }
                    else
                        item.Measure.GeodeRole = Geode.Roles.Poin;
                }
                else
                    item.Measure.GeodeRole = Geode.Roles.Unused;
            }
        }

        public override void Create_PLGC1_Input(Polar.Station station, string fileName, P plgc1, string title = "")
        {
            try
            {
                if (title == "") title = "PLGC input created by tsunami for a new frame measurement";
                string content = "";

                //// ghost creation
                //Point ghost = new Point() { _Name = "ghost", _Coordinates = new CoordinatesSystems() { Local = new Coordinates("ghost", 0, 1, 0) } };
                //MM.MeasureTheodolite ghostMeasure = new MM.MeasureTheodolite() { _Point = ghost, _OriginalPoint = ghost };
                //ghostMeasure.Angles.Corrected.Horizontal.Value = (station.MeasuresTaken[0] as MM.MeasureTheodolite).Angles.Corrected.Horizontal.Value;
                //ghostMeasure.Angles.Corrected.Vertical.Value = 100;
                //ghostMeasure.Distance.Corrected.Value = 1;
                //ghostMeasure._Status = new MM.MeasureStateGood();
                //station.nextMeasure = ghostMeasure;

                // FContent creation
                content += PII.Titr(title);
                content += PII.OptionOloc();


                // *CALA
                {
                    content += PII.AddCalaPoints(new List<E.Point>() { station.Parameters2._StationPoint, station.References[0]._OriginalPoint }, plgc1.getTheRightCoordinates);

                }

                // *POIN
                {
                    List<E.Point> l = new List<E.Point>();
                    foreach (var item in station.Rays)
                    {
                        l.Add(item._OriginalPoint);
                    }
                    content += PII.AddVariablePoints(l, plgc1.getTheRightCoordinates);
                }

                content += PII.Tstn(station, plgc1.getTheRightCoordinates);
                content += PII.End();

                //remove ghost measure

                //PI.usingGeoid = PII.usingGeoid;
                System.IO.File.WriteAllText(fileName + ".pinp", content);
            }
            catch (Exception ex)
            {

                throw new Exception(R.T042, ex);
            }
            finally
            {
                station.NextMeasure = null;
            }
        }

        public override void Create_LGC2_Input(Polar.Station station, string fileName, L lgc2, string title = "",  bool temporaryCompute = false)
        {
            lgc2.TreatFileName(fileName, "LocalStation");
            try
            {
                Polar.Station.Parameters p = station.Parameters2 as Polar.Station.Parameters;


                // create input
                if (title == "") title = $"{R.InputTSU} 'Local Station' {T.Conversions.Date.ToDateUpSideDown(DateTime.Now)} ";
                string content = "";
                content += LII.Titr(title + " as a local station in a new frame");
                content += LII.OptionOloc() + "\r\n";
                content += LII.OptionJSON(true);
                content += LII.Precision(7);
                content += LII.Instr();
                content += LII.Polar(station.Parameters2._Instrument as I.IPolarInstrument,
                                            T.Research.GetListOfReflectorUsedIn(station),
                                            new DoubleValue(0.0, 0.0),
                                            new DoubleValue(0.0, 0.0));

                List<E.Point> pointsInitialized = new List<E.Point>();
                E.Coordinates barycenter = null;

                var stationPoint = station.Parameters2._StationPoint;
                if (stationPoint._Origin == R.String_Unknown)
                    stationPoint._Origin = station._Name;
                //stationPoint.StorageStatus = Tsunami.StorageStatusTypes.Keep;

                content += LII.AddPdorPoints(station.PdorPoints, lgc2.getTheRightCoordinates);
                // *CALA
                {
                    List<E.Point> cala = lgc2._Input.CALAs;
                    cala.Clear();
                    cala.AddRange(station.ReferencePoints);
                    cala.Add(stationPoint);

                    content += LII.AddCalaPoints(cala, lgc2.getTheRightCoordinates, out barycenter, station.MeasuresTaken);
                    barycenter = stationPoint._Coordinates.Local.Clone() as E.Coordinates;

                    pointsInitialized.AddRange(cala);
                }

                // *POIN
                {
                    List<E.Point> poin = lgc2._Input.POINs;
                    poin.Clear();
                    poin.AddRange(station.VariablePoints);

                    content += LII.AddVariablePoints(poin, lgc2.getTheRightCoordinates, ref pointsInitialized, "*POIN", pointToBeCommented: null, barycenter);
                }

                // *VXY
                {
                    lgc2._Input.VXYs.Clear();
                }


                bool heightKnown = true;
                content += LII.Tstn(station, heightKnown, UseOnlyLastMeasure: false, useObservationSigma: false, useDistance: true, lgc2: lgc2);
                content += LII.End();

                var inputPath = lgc2._Input.CreateInputFile(content);
                Common.Compute.Compensations.Lgc2.ProcessFileControlLines(inputPath);
            }
            catch (Exception ex)
            {

                throw new Exception(R.T044b, ex);
            }
            finally
            {
                station.NextMeasure = null;
            }

        }

        internal override void CancelSetup()
        {
            base.CancelSetup();

            // Cancel new points lancés
            foreach (var item in station.MeasuresTaken)
            {
                // restore starting roles
                item.GeodeRole = Geode.Roles.Poin;
                item._Point.Type = Point.Types.NewPoint;
                item._Point.StorageStatus = Tsunami.StorageStatusTypes.Hidden;
            }
            station.Parameters2._StationPoint.StorageStatus = Tsunami.StorageStatusTypes.Hidden;

            var a = Tsunami2.Properties.Points;

            TempPointsToBeUpdated.Clear();
            TempPointsToBeUpdatedStrategy = E.Manager.Module.AddingStrategy.RenameIfExistInSameGroup;
        }

        public override bool Update()
        {
            if (CompensationResults == null) return false;
            if (!CompensationResults.Ready) return false;

            if (CompensationResults.Verified)
            {
                var stationParameters = station.Parameters2;
                var stationPoint = stationParameters._StationPoint;

                // measure states
                foreach (var item in station.ReferencePoints)
                    item.Type = E.Point.Types.Reference;

                foreach (var item in station.VariablePoints)
                    item.Type = E.Point.Types.NewPoint;

                // Points
                List<E.Point> updatedPoints = TSU.Common.Station.Updater.UpdateMeasuredPointsInStation(station, CompensationResults.VariablePoints, isApproximative: false);
                updatedPoints.Insert(0, stationPoint);

                foreach (var p in updatedPoints)
                {
                    p.StorageStatus = Tsunami.StorageStatusTypes.Keep;
                    p.Type = E.Point.Types.Reference;
                }

                this.TurnToGoodAndMarkAsToBeUpdated(updatedPoints);

                var a = Tsunami2.Properties.Points;
                var b = Tsunami2.Properties.HiddenPoints;
                var c = Tsunami2.Properties.TempPoints;

                // setup
                var setup = stationParameters.Setups;
                setup.FinalValues = CompensationResults;
            }
            else
            {
                station.Parameters2.Setups.TemporaryValues = CompensationResults;
            }
            return true;
        }

        internal override void CheckGeoid()
        {
            // fill delegate wih the right code to get the right coordincate
            this.CheckIfGeoidalComputeIsPossible(station);
        }

        internal override void ChooseMeasureForApproximateCompute()
        {
            // Nothing to do because station is known.
        }
        internal override void ComputeApproximateSetup()
        {
            station.Parameters2._StationPoint._Coordinates.Su = new E.Coordinates("", 0, 0, 0);
            //station.Parameters2.Setups.TemporaryValues = new Polar.Station.Parameters.Setup.Values(null);
            //station.Parameters2.Setups.TemporaryValues.vZero = new DoubleValue(-station.[0].Angles.corrected.Horizontal.Value);
            //// compute coordinates of the first point if the station is 0 0 0
            //station.References[0]._OriginalPoint._Coordinates.Su = Survey.GetPointLancé(station.References[0], station);

            //string message;
            //if (!base.ComputeApproximativePosition(updateStationCoord: false, updateMeasuredPoints: true, message: out  message))
            //    //   throw new Exception(string.Format("Compute of approximated coordinates failed: " + message));

            //throw new Exception($"{R.T_COMPUTE_OF_APPROXIMATED_COORDINATES_FAILED};\r\n {message}");

        }
        internal override void ChooseMeasureForCompensation()
        {

        }
        internal override void Compensate()
        {
            CompensationResults = base.RunLGC(!Silent, !Silent);
        }
        internal override void FindErrors()
        {
        }

        internal override string GetSetupLGC2Details()
        {
            try
            {
                return string.Format("sigma0 = {0}, {1} = {2}",
                        CompensationResults.SigmaZero.ToString("F2"),
                        R.T_INSTRUMENT_HEIGHT, CompensationResults.InstrumentHeight);
            }

            catch (Exception ex)
            {
                return $"{R.T_COMPENSATION} {R.T_FAILED}: {ex.Message}";
            }
        }

        internal override string GetConfidenceEllipsoid1Sigma()
        {
            return "";
        }

        internal override string RenameFile(string fileName)
        {
            if (fileName == "")
            {
                string folder = $@"{TSU.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay}computes\(P)LGC\NewFrame";
                System.IO.Directory.CreateDirectory(folder);
                return $@"{folder}\{TSU.Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now)}";
            }
            else
                return fileName;
        }
    }
}
