﻿using System;
using System.Collections.Generic;
using TSU.Common.Compute.Compensations;
using TSU.Common.Instruments;
using TSU.IO.SUSoft;
using TSU.Views.Message;
using CC = TSU.Common.Compute;
using CS = TSU.Common.Compute.Compensations.Strategies;
using E = TSU.Common.Elements;
using L = TSU.Common.Compute.Compensations.Lgc2;
using LII = TSU.Common.Compute.Compensations.Lgc2.Input.InputLine;
using P = TSU.Common.Compute.Compensations.Plgc1;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Polar.Compensations.NonVerticalized
{
    public class Setup : FreeStation_LGC2
    {
        public Setup()
        {
            _strategy = CS.List.NonVerticalizedSetup;
        }

        public Setup(Station.Module module)
            : base(module)
        {
            _strategy = CS.List.NonVerticalizedSetup;
        }

        public override void Create_PLGC1_Input(Station station, string fileName, P plgc1, string title = "")
        {
        }


        public override bool Update()
        {
            if (CompensationResults == null) return false;
            if (!CompensationResults.Ready) return false;

            if (CompensationResults.Verified)
            {
                // if point known but computed as freestation, we should change the name by adding i.e. "_F"
                if (station.Parameters2.Setups.InitialValues.IsPositionKnown)
                {
                    Common.Station.Updater.ChangeStationName(station, _Module.ElementManager, station.Parameters2._StationPoint._Name + FreeStationSuffix);

                }
                station.Parameters2._StationPoint.Date = DateTime.Now;

                List<E.Point> updatedPoints = Common.Station.Updater.UpdateMeasuredPointsInStation(station, CompensationResults.VariablePoints, isApproximative: false);
                TurnToGoodAndMarkAsToBeUpdated(updatedPoints);
                TempPointsToBeUpdated.Add(station.Parameters2._StationPoint);

                station.Parameters2._StationPoint._Origin = station._Name;
                Common.Station.Updater.UpdateStationV0AndHt(station, CompensationResults);
                Common.Station.Updater.UpdateStationCoordinates(station, CompensationResults);



                // if (station.ParametersBasic._IsSetup) Updater.UpdatePointsLancésAfterRecompute(station);
            }
            else
            {
                station.Parameters2.Setups.TemporaryValues = CompensationResults;
            }
            return true;
        }


        internal override void CheckGeoid()
        {
            // fill delegate wih the right code to get the right coordincate
            CheckIfGeoidalComputeIsPossible(station);
        }

        internal override void ChooseMeasureForApproximateCompute()
        {

        }

        public override void Create_LGC2_Input(Station station, string fileName, L lgc2, string title = "",  bool temporaryCompute = false)
        {
            lgc2.TreatFileName(fileName, "NonVerticalized.Setup");

            // gives the right algo in delegates

            Station.Parameters p = station.Parameters2;
            if (title == "") title = $"{R.InputTSU} 'NonVerticalized Free station' {T.Conversions.Date.ToDateUpSideDown(DateTime.Now)} ";
            string content = "";
            content += LII.Titr(title + R.T_NON_VERTICALIZED_STATION);

            // Refence surface
            string refSurfOption = lgc2.getTheRightOption();
            content += refSurfOption + "\r\n";
            content += LII.OptionFaut();
            content += LII.OptionJSON(true);
            content += LII.Precision(6);
            content += LII.Instr();
            content += LII.Polar(station.Parameters2._Instrument as IPolarInstrument,
                                 T.Research.GetListOfReflectorUsedIn(station),
                                 new DoubleValue(0.0, 0.0),
                                 new DoubleValue(0.0, 0.0));

            List<E.Point> pointsInitialized = new List<E.Point>();

            // seperate cala and VXY based on radi is availbel or not

            List<E.Point> cala = lgc2._Input.CALAs;
            cala.Clear();

            List<E.Point> vxy = lgc2._Input.VXYs;
            vxy.Clear();

            List<E.Point> poin = lgc2._Input.POINs;
            poin.Clear();

            List<E.Point> vZ = new List<E.Point>();

            DispatchPointsBasedOnGeodeRole(station, refSurfOption, cala, vZ, vxy, poin);

            if (station.NextMeasure != null)
            {
                cala.Add(station.NextMeasure._OriginalPoint);
            }

            pointsInitialized.AddRange(cala);
            //poin.Add(station.Parameters2._StationPoint);

            E.Coordinates barycenter = null;
            content += LII.AddCalaPoints(cala, lgc2.getTheRightCoordinates, out barycenter, station.MeasuresTaken);
            content += LII.AddVariablePoints(vxy, lgc2.getTheRightCoordinates, ref pointsInitialized, "*VXY", pointToBeCommented: null, barycenter: null);
            content += LII.AddVariablePoints(poin, lgc2.getTheRightCoordinates, ref pointsInitialized, "*POIN", pointToBeCommented: null, barycenter);




            //*RADI must be after *POIN .... !?
            if (vxy.Count > 0)
            {
                content += LII.AddRadi(vxy);
                content += LII.AddRadiDesactivated(cala);  // we want to get bearing of the cala point commented to be able to reactivated it easily
            }

            frameForInstrument = GetFrameForSetup(station, barycenter);

            content += LII.Tstn(
                station,
                instrumentHeightKnown: true, // should be always true (station.Parameters2.Setup._InitiaValues._IsInstrumentHeightKnown == Stations.Theodolite.Parameters.SetupParameters.InitialValues.InstrumentTypeHeight.Known));
                UseOnlyLastMeasure: false,
                useObservationSigma: false,
                useDistance: true,
                lgc2: lgc2,
                vm: LII.VerticalMode.Setup,
                frame: frameForInstrument);

            content += LII.End();

            lgc2._Input.CreateInputFile(content);
        }

        public static Frame GetFrameForSetup(Station station, E.Coordinates stationCoordinates)
        {
            // Get the actual Z of the barycenter, because barycenter.Z.Value may contain the H
            double z;
            if (stationCoordinates.CoordinateSystem == E.Coordinates.CoordinateSystemsTypes._2DPlusH)
            {
                E.Coordinates.ReferenceSurfaces rs = E.Coordinates.GetReferenceSurface(stationCoordinates.ReferenceFrame);
                //string system = CC.Transformation.Systems.GetGeoidRefForSurveylibFromReferenceSurface(rs);
                z = CC.Transformation.Systems.HtoZ(stationCoordinates.X.Value, stationCoordinates.Y.Value, stationCoordinates.Z.Value, rs, out bool outOfGrid);
                if (outOfGrid)
                    Logs.Log.AddEntryAsPayAttentionOf(Tsunami2.Properties, $"NV station is out of the CERN grid");
            }
            else
            {
                z = stationCoordinates.Z.Value;
            }

            //Define the Frame
            return new Frame()
            {
                _Name = $"{station._Name}_SETUP",
                ProvisoryParameters = new Frame.Parameters()
                {
                    TX = new Frame.Parameters.Parameter() { Value = stationCoordinates.X.Value, IsFixed = false },
                    TY = new Frame.Parameters.Parameter() { Value = stationCoordinates.Y.Value, IsFixed = false },
                    TZ = new Frame.Parameters.Parameter() { Value = z, IsFixed = false },
                    RX = new Frame.Parameters.Parameter() { Value = 0, IsFixed = false },
                    RY = new Frame.Parameters.Parameter() { Value = 0, IsFixed = false },
                    RZ = new Frame.Parameters.Parameter() { Value = 0, IsFixed = false },
                    Scale = new Frame.Parameters.Parameter() { Value = 1, IsFixed = true },
                }
            };
        }

        internal override void ComputeApproximateSetup()
        {
            string message;
            if (!ComputeApproximativePosition(true, true, out message))
                //    throw new Exception(string.Format("Compute of approximated coordinates failed: " + message));
                throw new Exception($"{R.T_COMPUTE_OF_APPROXIMATED_COORDINATES_FAILED};\r\n {message}");

        }
        internal override void ChooseMeasureForCompensation()
        {

        }
        internal override void Compensate()
        {
            CompensationResults = RunLGC(!Silent, !Silent);
        }

        internal override string GetSetupLGC2Details()
        {
            try
            {
                E.Coordinates c = null;
                if (CompensationResults.VariablePoints[0]._Coordinates.HasLocal)
                    c = CompensationResults.VariablePoints[0]._Coordinates.Local;
                else
                    c = CompensationResults.VariablePoints[0]._Coordinates.Ccs;

                int dec = Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances - 3;
                string format = "F" + dec.ToString();
                return $"{R.T_REFERENCE_SURFACE} = {CompensationResults.ReferenceSurface}, "
                     + $"{R.T_SIGMA0} = {CompensationResults.SigmaZero:F2}, "
                     + $"{R.T_STATION_COORDINATE_RESIDUALS}: "
                     + $"sx= {(c.X.Sigma * 1000).ToString(format)} mm, "
                     + $"sy= {(c.Y.Sigma * 1000).ToString(format)} mm, "
                     + $"sz= {(c.Z.Sigma * 1000).ToString(format)} mm";
            }

            catch (Exception ex)
            {
                return $"{R.T_COMPENSATION} {R.T_FAILED}: {ex.Message}";
            }
        }

        internal override string GetConfidenceEllipsoid1Sigma()
        {
            if (CompensationResults == null || !CompensationResults.Ready)
                return "Confidence Ellipsoid not available";

            E.Coordinates c;
            if (CompensationResults.VariablePoints[0]._Coordinates.HasLocal)
                c = CompensationResults.VariablePoints[0]._Coordinates.Local;
            else
                c = CompensationResults.VariablePoints[0]._Coordinates.Ccs;

            int dec = Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances - 3;
            string format = "F" + dec.ToString();

            var estimatedParameters = CompensationResults?.computedFrame?.DeeperFrame?.EstimatedParameters;

            string message = $"Station coordinate Confidence Ellipsoid:\r\n";
            message += $"\t \t sX = {(c.X.Sigma * 1000).ToString(format)} mm  \t \t \t sRX = {Math.Round(estimatedParameters.RX.Sigma * 100 * 100)} cc \r\n";
            message += $"\t \t sY = {(c.Y.Sigma * 1000).ToString(format)} mm  \t \t \t sRY = {Math.Round(estimatedParameters.RY.Sigma * 100 * 100)} cc \r\n";
            message += $"\t \t sZ = {(c.Z.Sigma * 1000).ToString(format)} mm  \t \t \t sRZ = {Math.Round(estimatedParameters.RZ.Sigma * 100 * 100)} cc ";
            return message;
                
        }


        internal override void FindErrors()
        {
            //   this._Module.View.ShowMessageOfCritical(string.Format("Lgc2 compute failed;Seems like the compute of the approximated coordinates works, but the compensation with LGC2 failed."));
            string titleAndMessage2 = $"{R.T_LGC2_COMPUTE_FAILED}; {R.T_SEEMS_LIKE_THE_COMPUTE_OF_THE_APPROXIMATED_COORDINATES_WORKS_BUT_THE_COMPENSATION_WITH_LGC2_FAILED}";
            new MessageInput(MessageType.Critical, titleAndMessage2).Show();
            string v0AndHt = "V0s & HTs";
            string cala = "*CALA";
            // string respond = this._Module.View.ShowMessageOfChoice(string.Format("Identify wrong measures?;Do you want to restart the compute with everything in *CALA to identify the problematic measure(s)?, or to show the vZero and 'tourillon' height for each point?"), cala, R.T_NO, v0AndHt);
            string titleAndMessage = $"{R.T_IDENTIFY_WRONG_MEASURES};{R.T_DO_YOU_WANT_TO_RESTART_THE_COMPUTE_WITH_EVERYTHING_IN_CALA_TO_IDENTIFY_THE_PROBLEMATIC_MEASURES_OR_TO_SHOW_THE_VZERO_AND_TOURILLON_HEIGHT_FOR_EACH_POINT}";
            MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
            {
                ButtonTexts = new List<string> { v0AndHt , cala, R.T_NO }
            };
            string respond = mi.Show().TextOfButtonClicked;
            if (respond == cala)
            {
                CS.List old = _strategy;
                _strategy = CS.List.AllCala;
                seeOutput = true;
                RunLGC2();
                _strategy = old;
                //   this._Module.View.ShowMessageOfExclamation("Now, we will try again;Normally, you should have identify the probematic measure, we will try again to compute and you will be able to deactivate some measurement.");
                string titleAndMessage1 = $"{R.T_NOW_WE_WILL_TRY_AGAIN}; {R.T_NORMALLY_YOU_SHOULD_HAVE_IDENTIFY_THE_PROBEMATIC_MEASURE_WE_WILL_TRY_AGAIN_TO_COMPUTE_AND_YOU_WILL_BE_ABLE_TO_DEACTIVATE_SOME_MEASUREMENT}";
                new MessageInput(MessageType.Warning, titleAndMessage1).Show();
            }

            if (respond == v0AndHt)
            {
                Common.Analysis.Theodolite.FindWrongMeasureInCompensation(station, _strategy);
            }

        }

        internal override string RenameFile(string fileName)
        {
            if (fileName == "")
            {
                string folder = $@"{Tsunami2.Preferences.Values.Paths.MeasureOfTheDay}computes\(P)LGC\NonVerticalized.Setup";
                System.IO.Directory.CreateDirectory(folder);
                return $@"{folder}\{T.Conversions.Date.ToDateUpSideDown(DateTime.Now)}";
            }
            else
                return fileName;
        }

        public override void ParseOutPutAsTheodoliteResultsAsJSON(L.Output output)
        {
            base.ParseOutPutAsTheodoliteResultsAsJSON(output);

            // Store the computed Frame for Goto and PointLancé
            string fileName = output.OutputFilePath.ToUpper().Replace(".RES", ".JSON");
            Frame.FromJson_GetFrameResults(fileName, frameForInstrument._Name, ref frameForInstrument, out _, out _);
            output._Result.computedFrame = frameForInstrument.Clone() as Frame;

            // Init important fields
            output._Result.Strategy = _strategy;
            output._Result.VerticalisationState = Station.Parameters.Setup.Values.InstrumentVerticalisation.NotVerticalised;
        }
    }
}
