﻿using System;
using System.Collections.Generic;
using System.Linq;
using TSU.Common.Compute.Compensations;
using TSU.Common.Instruments;
using TSU.ENUM;
using TSU.IO.SUSoft;
using E = TSU.Common.Elements;
using L = TSU.Common.Compute.Compensations.Lgc2;
using LII = TSU.Common.Compute.Compensations.Lgc2.Input.InputLine;
using R = TSU.Properties.Resources;
using T = TSU.Tools;
using CS = TSU.Common.Compute.Compensations.Strategies;

namespace TSU.Polar.Compensations.NonVerticalized
{
    [Serializable]
    public class PointLancé : CS.Common
    {
        public PointLancé()
        {
            _strategy = CS.List.NonVerticalizedPointLancé;
        }
        public PointLancé(Station.Module module)
            : base(module)
        {
            _strategy = CS.List.NonVerticalizedPointLancé;
        }

        public override bool Update()
        {
            if (CompensationResults == null) return false;
            if (!CompensationResults.Ready) return false;

            Common.Station.Updater.UpdateLastMeasuredPoint(station.MeasuresTaken[station.MeasuresTaken.Count - 1] as Measure, CompensationResults.VariablePoints); // we dont wnat to recompute all point from station after every throwpoint, specailly beacuse it could modify 'original point' of the measure in case of stake out

            return true;
        }


        public override void Create_PLGC1_Input(Station station, string fileName, Plgc1 plgc1, string title = "")
        {
        }

        internal override void SetDefaultMeasureSelection(List<TsuObject> allElements)
        {
            foreach (CompensationItem item in allElements.OfType<CompensationItem>()) // so selec all the one used
            {
                if (item.Measure.IsGood)
                {
                    if (item.Measure._OriginalPoint._Coordinates.HasAny)
                    {
                        item.Measure.GeodeRole = Geode.Roles.Cala;
                    }
                    else
                        item.Measure.GeodeRole = Geode.Roles.Poin;
                }
                else
                    item.Measure.GeodeRole = Geode.Roles.Unused;
            }
        }

        public override void Create_LGC2_Input(Station station, string fileName, L lgc2, string title = "",  bool temporaryCompute = false)
        {
            try
            {
                lgc2.TreatFileName(fileName, "NonVerticalized.PointLancé");

                // gives the right algo in delegates

                if (title == "") title = $"{R.InputTSU} 'on Verticalized Point Lancé' {T.Conversions.Date.ToDateUpSideDown(DateTime.Now)} ";
                string content = "";
                content += LII.Titr(title + R.T_NON_VERTICALIZED_STATION);

                // Refence surface
                string refSurfOption = lgc2.getTheRightOption();
                content += refSurfOption + "\r\n";
                content += LII.OptionFaut();
                if (refSurfOption.ToUpper() != "*OLOC")
                    content += LII.OptionCreatePunchHFile();
                else
                    content += LII.OptionCreatePunchFile();

                content += LII.OptionJSON(true);
                content += LII.Precision(6);
                content += LII.Instr();
                content += LII.Polar(station.Parameters2._Instrument as IPolarInstrument,
                                     T.Research.GetListOfReflectorUsedIn(station),
                                     new DoubleValue(0.0, 0.0),
                                     new DoubleValue(0.0, 0.0));

                List<E.Point> pointsInitialized = new List<E.Point>();

                lgc2._Input.CALAs.Clear();
                lgc2._Input.VXYs.Clear();

                List<E.Point> poins = lgc2._Input.POINs;
                poins.Clear();
                poins.Add(station.MeasuresTaken[station.MeasuresTaken.Count - 1]._Point);
                content += LII.AddVariablePoints(poins,
                                                 lgc2.getTheRightCoordinates,
                                                 ref pointsInitialized,
                                                 "*POIN",
                                                 pointToBeCommented: null,
                                                 null);

                // Copy the frame computed by the Setup strategy
                frameForInstrument = station.Parameters2.Setups.BestValues.computedFrame.Clone() as Frame;

                // Use the EstimatedParameters as ProvisoryParameters, and fix all parameters
                frameForInstrument.ProvisoryParameters = frameForInstrument.EstimatedParameters.Clone() as Frame.Parameters;
                frameForInstrument.ProvisoryParameters.FixAll();

                content += LII.Tstn(station,
                                    instrumentHeightKnown: true,
                                    UseOnlyLastMeasure: false,
                                    useObservationSigma: false,
                                    useDistance: true,
                                    lgc2: lgc2,
                                    vm: LII.VerticalMode.PointLancé,
                                    frame: frameForInstrument);

                content += LII.End();

                lgc2._Input.CreateInputFile(content);


            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_IMPOSSIBLE_TO_CREATE_INPUT_AS_RADIATED_POINT}\r\n{ex.Message}", ex);
            }

        }

        internal override void CheckGeoid()
        {
            // fill delegate wih the right code to get the right coordincate
            this.CheckIfGeoidalComputeIsPossible(station, new PointLancéLGC2());
        }

        internal override void ChooseMeasureForApproximateCompute()
        {
            // nothing to do
        }
        internal override void ComputeApproximateSetup()
        {

        }


        internal override void ChooseMeasureForCompensation()
        {
            // nothing to do
        }
        internal override void Compensate()
        {
            CompensationResults = RunLGC(showLog: false, showSigmaO: false);

            this.SaveFilesPath(lgc2._Output.OutputFilePath);
        }
        internal override void FindErrors()
        {
        }

        internal override string GetSetupLGC2Details()
        {
            try
            {
                return string.Format("sigma0 = {0}, {1} = {2}",
                     CompensationResults.SigmaZero.ToString("F2"),
                     R.T_INSTRUMENT_HEIGHT, CompensationResults.InstrumentHeight);
            }
            catch (Exception ex)
            {
                return $"{R.T_COMPENSATION} {R.T_FAILED}: {ex.Message}";
            }
        }

        internal override string GetConfidenceEllipsoid1Sigma()
        {
            return "";
        }

        internal override string RenameFile(string fileName)
        {
            if (fileName == "")
            {
                string folder = $@"{Tsunami2.Preferences.Values.Paths.MeasureOfTheDay}computes\(P)LGC\NonVerticalized.PointLancé";
                System.IO.Directory.CreateDirectory(folder);
                return $@"{folder}\{T.Conversions.Date.ToDateUpSideDown(DateTime.Now)}";
            }
            else
                return fileName;
        }
    }
}
