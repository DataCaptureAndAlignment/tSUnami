﻿using System;
using System.Collections.Generic;
using TSU.Common.Compute.Compensations;
using TSU.Common.Instruments;
using TSU.IO.SUSoft;
using CS = TSU.Common.Compute.Compensations.Strategies;
using E = TSU.Common.Elements;
using L = TSU.Common.Compute.Compensations.Lgc2;
using LII = TSU.Common.Compute.Compensations.Lgc2.Input.InputLine;
using P = TSU.Common.Compute.Compensations.Plgc1;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Polar.Compensations.NonVerticalized
{
    [Serializable]
    public class Goto : CS.Common
        {
            public Goto()
            {
                _strategy = CS.List.NonVerticalizedGoto;
            }
            public Goto(Station.Module module)
                : base(module)
            {
                _strategy = CS.List.NonVerticalizedGoto;
            }

        public override void Create_PLGC1_Input(Station station, string fileName, P plgc1, string title = "")
        {
        }

        public override void Create_LGC2_Input(Station station, string fileName, L lgc2, string title = "",  bool temporaryCompute = false)
        {
            lgc2.TreatFileName(fileName, "NonVerticalized.Goto");
            try
            {
                Station.Parameters p = station.Parameters2;

                // Create
                if (title == "") title = $"{R.InputTSU} 'Non Verticalized Goto' {T.Conversions.Date.ToDateUpSideDown(DateTime.Now)} ";
                string content = "";
                content += LII.Titr(title + " for goto computation");

                // Refence surface
                string refSurfOption = lgc2.getTheRightOption();
                content += refSurfOption + "\r\n";
                //content += InputLine.OptionFaut(); pas besoin de falut pour goto
                ///if (refSurfOption.ToUpper() != "*OLOC")
                //    content += InputLine.OptionCreatePunchHFile();
                // else
                //    content += InputLine.OptionCreatePunchFile();

                content += LII.OptionJSON(true);
                content += LII.Precision(7);
                content += LII.Instr();
                IPolarInstrument instrument = station.Parameters2._Instrument as IPolarInstrument;
                if (instrument == null) throw new Exception("Instrument is not selected or not a polar sensor");
                content += LII.Polar(instrument,
                                     T.Research.GetListOfReflectorUsedIn(station),
                                     new DoubleValue(0.0, 0.0),
                                     new DoubleValue(0.0, 0.0));

                // *CALA
                {
                    List<E.Point> cala = lgc2._Input.CALAs;
                    cala.Clear();

                    //cala.Add(station.Parameters2._StationPoint);
                    if (station.NextMeasure!=null)
                        cala.Add(station.NextMeasure._OriginalPoint);

                    E.Coordinates barycenter = null;
                    content += LII.AddCalaPoints(cala, lgc2.getTheRightCoordinates, out barycenter, station.MeasuresTaken);
                }

                // Get the Setup strategy's results
                Station.Parameters.Setup.Values bestValues = station.Parameters2.Setups.BestValues;

                // Copy the frame computed by the Setup strategy
                frameForInstrument = bestValues.computedFrame.Clone() as Frame;

                // Use the EstimatedParameters as ProvisoryParameters, and fix all parameters
                frameForInstrument.ProvisoryParameters = frameForInstrument.EstimatedParameters.Clone() as Frame.Parameters;
                frameForInstrument.ProvisoryParameters.FixAll();

                content += LII.Tstn(station: station,
                                    instrumentHeightKnown: true,
                                    UseOnlyLastMeasure: true,
                                    useObservationSigma: false,
                                    useDistance: true,
                                    lgc2: lgc2,
                                    vm: LII.VerticalMode.Goto,
                                    frame: frameForInstrument);

                content += LII.End();

                lgc2._Input.CreateInputFile(content);
            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_IMPOSSIBLE_TO_CREATE_INPUT_FOR_GOTO}\r\n{ex.Message}", ex);
            }
            
        }

        internal override void SetDefaultMeasureSelection(List<TsuObject> allElements)
        {
            foreach (CompensationItem item in allElements) // so selec all the one used
            {
                if (item.Measure.IsGood)
                {
                    item.Measure.GeodeRole = Geode.Roles.Cala;
                }
                else
                    item.Measure.GeodeRole = Geode.Roles.Unused;
            }
        }


        public override bool Update()
            {
                //if (CompensationResults == null) return false;
                //if (!CompensationResults.Ready) return false;

                //Updater.UpdateMeasuredPoint(station, CompensationResults.VariablePoints);

                return true;
            }

            internal override void CheckGeoid()
            {
                // fill delegate wih the right code to get the right coordincate
                this.CheckIfGeoidalComputeIsPossible(station);
            }

            internal override void ChooseMeasureForApproximateCompute()
            {
                // nothing to do setup is done
            }
            internal override void ComputeApproximateSetup()
            {
                // nothing to do setup is done
            }
            internal override void ChooseMeasureForCompensation()
            {
                // nothing to do setup is done
            }
            internal override void Compensate()
            {
                CompensationResults = RunLGC(false,false);
            }
            internal override void FindErrors()
            {
            }
            internal override string GetSetupLGC2Details()
            {
                try
                {
                    return string.Format("sigma0 = {0}, {1} = {2}",
                         CompensationResults.SigmaZero.ToString("F2"),
                         R.T_INSTRUMENT_HEIGHT, CompensationResults.InstrumentHeight);

                }
                catch (Exception ex)
                {
                    return $"{ R.T_COMPENSATION } {R.T_FAILED}: {ex.Message}";
                }
            }

            internal override string GetConfidenceEllipsoid1Sigma()
            {
                return "";
            }

            internal override string RenameFile(string fileName)
            {
                if (fileName == "")
                {
                    string folder = $@"{Tsunami2.Preferences.Values.Paths.MeasureOfTheDay}computes\(P)LGC\NonVerticalized.Goto";
                    System.IO.Directory.CreateDirectory(folder);
                    return $@"{folder}\{T.Conversions.Date.ToDateUpSideDown(DateTime.Now)}";
                }
                else
                    return fileName;
            }
    }
}
