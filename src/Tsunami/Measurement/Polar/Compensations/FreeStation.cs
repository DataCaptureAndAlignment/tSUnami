﻿using System;
using System.Collections.Generic;
using System.Linq;
using TSU.Common.Compute.Compensations;
using TSU.Common.Elements;
using TSU.Common.Instruments;
using TSU.IO.SUSoft;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using L = TSU.Common.Compute.Compensations.Lgc2;
using LII = TSU.Common.Compute.Compensations.Lgc2.Input.InputLine;
using P = TSU.Common.Compute.Compensations.Plgc1;
using PII = TSU.Common.Compute.Compensations.Plgc1.Input.InputLine;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Polar.Compensations
{
    [Serializable]
    public class FreeStation_LGC2 : TSU.Common.Compute.Compensations.Strategies.Common
    {
        public FreeStation_LGC2()
        {
            _strategy = TSU.Common.Compute.Compensations.Strategies.List.FreeStation_LGC2;
        }
        public FreeStation_LGC2(Polar.Station.Module module)
            : base(module)
        {
            _strategy = TSU.Common.Compute.Compensations.Strategies.List.FreeStation_LGC2;
        }

        internal override void SetDefaultMeasureSelection(List<TsuObject> allElements)
        {
            bool isFirst = true;
            foreach (CompensationItem item in allElements) // so selec all the one used
            {
                if (item.Measure.IsGood)
                {
                    if (item.Measure._OriginalPoint.State != Element.States.Approximate
                     && item.Measure._OriginalPoint._Coordinates.HasAny)
                    {
                        if (isFirst)
                        {
                            isFirst = false;
                            item.Measure.GeodeRole = Geode.Roles.Cala;
                        }
                        else
                        {
                            if (item.Measure._OriginalPoint._Parameters.hasGisement && !item.Measure._OriginalPoint.IsPilier)
                                item.Measure.GeodeRole = Geode.Roles.Radi;
                            else

                                item.Measure.GeodeRole = Geode.Roles.Cala;
                        }
                    }
                    else
                        item.Measure.GeodeRole = Geode.Roles.Poin;
                }
                else
                    item.Measure.GeodeRole = Geode.Roles.Unused;

            }
        }


        internal override string RenameFile(string fileName)
        {
            if (fileName == "")
            {
                string folder = $@"{TSU.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay}computes\(P)LGC\FreeStation";
                System.IO.Directory.CreateDirectory(folder);
                return $@"{folder}\{TSU.Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now)}";
            }
            else
                return fileName;
        }

        public override void Create_PLGC1_Input(Polar.Station station, string fileName, P plgc1, string title = "")
        {
            try
            {
                if (title == "") title = R.T041;
                string content = "";
                content += PII.Titr(title);
                content += PII.OptionOloc();
                // content += InputLine.AddVariablePoints(new List<Point>() { (station.Parameters2 as Stations.Theodolite.Parameters)._StationPoint }, this.plgc1.getTheRightCoordinates);
                // changed on  22/10/2019

                content += PII.AddCalaPoints(station.ReferencePoints, plgc1.getTheRightCoordinates);

                List<E.Point> poin = new List<E.Point>();
                poin.Add(station.Parameters2._StationPoint);
                poin.AddRange(station.VariablePoints);
                content += PII.AddVariablePoints(poin, plgc1.getTheRightCoordinates);
                content += PII.Tstn(station, plgc1.getTheRightCoordinates);
                content += PII.End();

                //this.usingGeoid = InputLine.usingGeoid;
                System.IO.File.WriteAllText(fileName + ".pinp", content);

                TSU.Debug.WriteInConsole("PLGC COMPUTE");
                TSU.Debug.WriteInConsole(content);
            }
            catch (Exception ex)
            {
                throw new Exception(R.T_EM_FREE_ST_INP + "\r\n" + ex.Message, ex);
            }
        }

        private void AskForInstrumentheight()
        {
            string cancel = R.T_CANCEL;
            string titleAndMessage = string.Format(R.T_INST_HEIGHT);
            List<string> buttonTexts = new List<string> { R.T_OK, cancel };

            MessageTsu.ShowMessageWithTextBox(titleAndMessage, buttonTexts, out string buttonClicked, out string textInput, "0.0");

            if (buttonClicked != cancel)
            {
                this._Module.ChangeInstrumentHeight(T.Conversions.Numbers.ToDouble(textInput));
                //UpdateViewOfStationParameters();
            }
            else
            {
                // throw new Exception("Cancelled;Free station need a instrument height!");
                throw new Exception($"{R.T_CANCELLED}; {R.T_FREE_STATION_NEED_A_INSTRUMENT_HEIGHT}");
            }
        }

        public const string FreeStationSuffix = "_F";

        public override bool Update()
        {
            if (CompensationResults == null) return false;
            if (!CompensationResults.Ready) return false;

            if (CompensationResults.Verified)
            {

                List<E.Point> updatedPoints = TSU.Common.Station.Updater.UpdateMeasuredPointsInStation(station, CompensationResults.VariablePoints, isApproximative: false);

                station.Parameters2._StationPoint.Date = DateTime.Now;
                station.Parameters2._StationPoint._Origin = station._Name;
                TSU.Common.Station.Updater.UpdateStationV0AndHt(station, CompensationResults);
                TSU.Common.Station.Updater.UpdateStationCoordinates(station, CompensationResults);

                // if point known but computed as freestation, we should change the name by adding i.e. "_F"
                if (station.Parameters2.Setups.InitialValues.IsPositionKnown == true)
                {
                    string freeSuffix = FreeStationSuffix;
                    string newName = station.Parameters2._StationPoint._Name;
                    if (!(newName.EndsWith(freeSuffix) || newName.EndsWith($"{freeSuffix}.")))
                    {
                        if (newName.EndsWith("."))
                            newName = newName.Substring(0, newName.Length - 1) + freeSuffix + ".";
                        else
                            newName += freeSuffix;
                    }
                    TSU.Common.Station.Updater.ChangeStationName(station, this._Module.ElementManager, newName);

                }

                updatedPoints.Add(station.Parameters2._StationPoint);
                this.TurnToGoodAndMarkAsToBeUpdated(updatedPoints);

                //if (station.ParametersBasic._IsSetup) Updater.UpdatePointsLancésAfterRecompute(station);
            }
            else
            {
                station.Parameters2.Setups.TemporaryValues = CompensationResults;
            }
            return true;
        }


        internal override void CheckGeoid()
        {
            // fill delegate wih the right code to get the right coordincate
            this.CheckIfGeoidalComputeIsPossible(station);
        }

        internal override void ChooseMeasureForApproximateCompute()
        {

        }
        internal override void ComputeApproximateSetup()
        {
            string message;
            if (!base.ComputeApproximativePosition(true, true, out message))
                //    throw new Exception(string.Format("Compute of approximated coordinates failed: " + message));
                throw new Exception($"{R.T_COMPUTE_OF_APPROXIMATED_COORDINATES_FAILED};\r\n {message}");

        }

        public override void Create_LGC2_Input(Polar.Station station, string fileName, L lgc2, string title = "", bool temporaryCompute = false)
        {
            lgc2.TreatFileName(fileName, "FreeStation");

            // gives the right algo in delegates

            Polar.Station.Parameters p = station.Parameters2;
            if (title == "") title = $"{R.InputTSU} 'Free Station' {T.Conversions.Date.ToDateUpSideDown(DateTime.Now)} ";
            string content = "";
            content += LII.Titr(title + " as a free station");

            // Refence surface
            string refSurfOption = lgc2.getTheRightOption();
            content += refSurfOption + "\r\n";
            content += LII.OptionFaut();
            if (refSurfOption.ToUpper() != "*OLOC")
                content += LII.OptionCreatePunchHFile();
            else
                content += LII.OptionCreatePunchFile();

            content += LII.OptionJSON(true);
            content += LII.Precision(6);
            content += LII.Instr();
            content += LII.Polar(station.Parameters2._Instrument as IPolarInstrument,
                                        T.Research.GetListOfReflectorUsedIn(station),
                                        new DoubleValue(0.0, 0.0),
                                        new DoubleValue(0.0, 0.0));

            List<E.Point> pointsInitialized = new List<E.Point>();

            // To seperate cala and VXY based on *radi is availble or not
            List<E.Point> cala = lgc2._Input.CALAs;
            cala.Clear();
            List<E.Point> vxy = lgc2._Input.VXYs;
            vxy.Clear();
            List<E.Point> poin = lgc2._Input.POINs;
            poin.Clear();
            List<E.Point> vZ = new List<Point>();

            DispatchPointsBasedOnGeodeRole(station, refSurfOption, cala, vZ, vxy, poin);

            if (station.NextMeasure != null)
            {
                cala.Add(station.NextMeasure._OriginalPoint);
            }

            pointsInitialized.AddRange(cala);
            poin.Add(station.Parameters2._StationPoint);
            E.Coordinates barycenter = null;
            content += LII.AddCalaPoints(cala, lgc2.getTheRightCoordinates, out barycenter, station.MeasuresTaken);


            content += LII.AddVariablePoints(vxy, lgc2.getTheRightCoordinates, ref pointsInitialized, "*VXY", pointToBeCommented: null, barycenter: null);
            content += LII.AddVariablePoints(poin, lgc2.getTheRightCoordinates, ref pointsInitialized, "*POIN", pointToBeCommented: null, barycenter);



            //*RADI must be after *POIN .... !?
            if (vxy.Count > 0)
            {
                content += LII.AddRadi(vxy);

                // computes launch by tsunami early to get access to goto?
                if (temporaryCompute)  
                {
                    // we want to make sure there is only one solution seee tsu-3855//
                    content += LII.Add90DegreeRadi(vxy);
                }
                content += LII.AddRadiLine(cala, commented: true);  // we want to get bearing of the cala point commented to be able to reactivated it easily
            }

            content += LII.Tstn(station, instrumentHeightKnown: true,
                UseOnlyLastMeasure: false, useObservationSigma: false, useDistance: true, lgc2: lgc2); // should be always true (station.Parameters2.Setup._InitiaValues._IsInstrumentHeightKnown == Stations.Theodolite.Parameters.SetupParameters.InitialValues.InstrumentTypeHeight.Known));

            content += LII.End();

            var inputPath = lgc2._Input.CreateInputFile(content);
            Common.Compute.Compensations.Lgc2.ProcessFileControlLines(inputPath);
        }

       
        public override void ParseOutPutAsTheodoliteResultsAsJSON(TSU.Common.Compute.Compensations.Lgc2.Output output)
        {
            base.ParseOutPutAsTheodoliteResultsAsJSON(output);

            // let's find the station point
            E.Point stl = null;
            // in general there is only one point in freestation compute but just in cas lets check first for a station with 'stl' in the name
            var stls = output._Result.VariablePoints.FindAll(x => x._Name.ToUpper().Contains("STL"));
            if (stls != null && stls.Count>0)
                stl = stls[0];
            else
                stl = output._Result.VariablePoints[0];

            stl.StorageStatus = Common.Tsunami.StorageStatusTypes.Temp;
            output._Result.StationPoint = stl;

        }

        internal override void ChooseMeasureForCompensation()
        {

        }
        internal override void Compensate()
        {
            if (station.Parameters2.Setups.InitialValues.InstrumentHeight == null) this.AskForInstrumentheight();

            CompensationResults = base.RunLGC(!Silent, !Silent);
        }

        internal override string GetSetupLGC2Details()
        {
            if (CompensationResults != null)
            {
                if (CompensationResults.Ready)
                {
                    try
                    {
                        int dec = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances - 3;
                        //   return string.Format("Reference surface = {0}, sigma0 = {1}, Station coordinate residuals: sx= {2} mm, sy= {3} mm, sz= {4} mm",
                        //       CompensationResults.ReferenceSurface,
                        //       CompensationResults.SigmaZero.ToString("F2"),
                        //       (CompensationResults.VariablePoints[0].X.Sigma * 1000).ToString("F" + dec.ToString()),
                        //       (CompensationResults.VariablePoints[0].Y.Sigma*1000).ToString("F" + dec.ToString()),
                        //       (CompensationResults.VariablePoints[0].Z.Sigma*1000).ToString("F" + dec.ToString())
                        //       );

                        E.Coordinates c = null;
                        if (CompensationResults.VariablePoints[0]._Coordinates.HasLocal)
                            c = CompensationResults.VariablePoints[0]._Coordinates.Local;
                        else
                            c = CompensationResults.VariablePoints[0]._Coordinates.Ccs;



                        return $"{R.T_REFERENCE_SURFACE} = {CompensationResults.ReferenceSurface}, " +
                        $"{R.T_SIGMA0} = {CompensationResults.SigmaZero.ToString("F2")}, " +
                        $"{R.T_STATION_COORDINATE_RESIDUALS}: sx= {(c.X.Sigma * 1000).ToString("F" + dec.ToString())} mm, " +
                        $"sy= {(c.Y.Sigma * 1000).ToString("F" + dec.ToString())} mm, " +
                        $"sz= {(c.Z.Sigma * 1000).ToString("F" + dec.ToString())} mm";
                    }
                    catch (Exception ex)
                    {
                        return $"{R.T_COMPENSATION} {R.T_FAILED}: {ex.Message}";
                    }
                }
            }

            return $"{R.T_COMPENSATION} {R.T_FAILED}";
        }


        internal override string GetConfidenceEllipsoid1Sigma()
        {
            if (this.CompensationResults == null || !this.CompensationResults.Ready)
                return "Confidence Ellipsoid not available";

            int dec = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances - 3;
            if (CompensationResults.VariablePoints != null)
            {
                if (CompensationResults.VariablePoints.Count > 0)
                {
                    E.Coordinates c = null;
                    var stationPoint = CompensationResults.VariablePoints.FirstOrDefault(x=>x._Name == this.station.Parameters2._StationPoint._Name);
                    if (stationPoint._Coordinates.HasLocal)
                        c = stationPoint._Coordinates.Local;
                    else
                        c = stationPoint._Coordinates.Ccs;

                    string message =  string.Format("Confidence Ellipsoid(s) for the Station :\r\n"+
                        "\tAlong CERN CS axis:" +
                        "\t\t sX= {0} mm\t\t sY= {1} mm\t\t sZ= {2} mm",
                        (c.X.Sigma * 1000).ToString("F" + dec.ToString()),
                        (c.Y.Sigma * 1000).ToString("F" + dec.ToString()),
                        (c.Z.Sigma * 1000).ToString("F" + dec.ToString()));

                    foreach (var point in this.Points_Accelerator_Ellipses)
                    {
                        var pointName = point.Key;
                        if (point.Key == stationPoint._Name)
                        {
                            var ellipsesPerAccelerators = point.Value;
                            foreach (var accelerator in ellipsesPerAccelerators)
                            {
                                var acceleratorName = $"Along {accelerator.Key}:";
                                var ellipse = accelerator.Value;
                                string truncatedOrPaddedAcceleratorName = acceleratorName.Length > 21
                                    ? acceleratorName.Substring(0, 21)
                                    : acceleratorName.PadRight(21);
                                message += $"\r\n\t{truncatedOrPaddedAcceleratorName}";
                                message += $"\t\t sR= {ellipse.s1:F2} mm\t\t sL= {ellipse.s2:F2} mm\t\t sH= {ellipse.s3:F2} mm";
                            }
                        }
                    }
                    return message;
                }
            }
            return "N/A";
        }


        internal override void FindErrors()
        {
            //   this._Module.View.ShowMessageOfCritical(string.Format("Lgc2 compute failed;Seems like the compute of the approximated coordinates works, but the compensation with LGC2 failed."));
            string titleAndMessage2 = $"{R.T_LGC2_COMPUTE_FAILED}; {R.T_SEEMS_LIKE_THE_COMPUTE_OF_THE_APPROXIMATED_COORDINATES_WORKS_BUT_THE_COMPENSATION_WITH_LGC2_FAILED}";
            new MessageInput(MessageType.Critical, titleAndMessage2).Show();
            string v0AndHt = "V0s & HTs";
            string cala = "*CALA";
            // string respond = this._Module.View.ShowMessageOfChoice(string.Format("Identify wrong measures?;Do you want to restart the compute with everything in *CALA to identify the problematic measure(s)?, or to show the vZero and 'tourillon' height for each point?"), cala, R.T_NO, v0AndHt);
            string titleAndMessage = $"{R.T_IDENTIFY_WRONG_MEASURES};{R.T_DO_YOU_WANT_TO_RESTART_THE_COMPUTE_WITH_EVERYTHING_IN_CALA_TO_IDENTIFY_THE_PROBLEMATIC_MEASURES_OR_TO_SHOW_THE_VZERO_AND_TOURILLON_HEIGHT_FOR_EACH_POINT}";
            MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
            {
                ButtonTexts = new List<string> { v0AndHt, cala, R.T_NO }
            };
            string respond = mi.Show().TextOfButtonClicked;
            if (respond == cala)
            {
                TSU.Common.Compute.Compensations.Strategies.List old = this._strategy;
                this._strategy = TSU.Common.Compute.Compensations.Strategies.List.AllCala;
                this.seeOutput = true;
                base.RunLGC2();
                this._strategy = old;
                //   this._Module.View.ShowMessageOfExclamation("Now, we will try again;Normally, you should have identify the probematic measure, we will try again to compute and you will be able to deactivate some measurement.");
                string titleAndMessage1 = $"{R.T_NOW_WE_WILL_TRY_AGAIN}; {R.T_NORMALLY_YOU_SHOULD_HAVE_IDENTIFY_THE_PROBEMATIC_MEASURE_WE_WILL_TRY_AGAIN_TO_COMPUTE_AND_YOU_WILL_BE_ABLE_TO_DEACTIVATE_SOME_MEASUREMENT}";
                new MessageInput(MessageType.Warning, titleAndMessage1).Show();
            }

            if (respond == v0AndHt)
            {
                TSU.Common.Analysis.Theodolite.FindWrongMeasureInCompensation(this.station, this._strategy);
            }

        }

    }
}
