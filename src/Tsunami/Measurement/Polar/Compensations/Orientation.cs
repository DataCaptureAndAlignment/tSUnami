﻿using System;
using System.Collections.Generic;
using System.Text;
using R = TSU.Properties.Resources;
using System.Windows.Forms;
using TSU.ENUM;
using System.Xml.Serialization;
using TSU.Views;
using E = TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using TSU.Common.Instruments;
using TSU.Common.Measures;
using TSU.Common.Operations;
using T = TSU.Tools;
using D = TSU.Common.Instruments.Device;
using L = TSU.Common.Compute.Compensations.Lgc2;
using LI = TSU.Common.Compute.Compensations.Lgc2.Input;
using LII = TSU.Common.Compute.Compensations.Lgc2.Input.InputLine;
using P = TSU.Common.Compute.Compensations.Plgc1;
using PI = TSU.Common.Compute.Compensations.Plgc1.Input;
using PII = TSU.Common.Compute.Compensations.Plgc1.Input.InputLine;

using TSU.Common.Compute.Compensations;
using TSU.Common;
using TSU.IO.SUSoft;
using TSU.Views.Message;

namespace TSU.Polar.Compensations
{
    [Serializable]
    public class Orientation_LGC2 : TSU.Common.Compute.Compensations.Strategies.Common
    {
        public Orientation_LGC2()
        {
            _strategy = TSU.Common.Compute.Compensations.Strategies.List.OrientatedOnly_LGC2;
        }
        public Orientation_LGC2(Polar.Station.Module module)
            : base(module)
        {
            _strategy = TSU.Common.Compute.Compensations.Strategies.List.OrientatedOnly_LGC2;
        }
        public override bool Update()
        {
            try
            {
                if (CompensationResults == null) return false;
                if (!CompensationResults.Ready) return false;

                if (CompensationResults.Verified)
                {
                    List<E.Point> updatedPoints = TSU.Common.Station.Updater.UpdateMeasuredPointsInStation(station, CompensationResults.VariablePoints, isApproximative: false);
                    TSU.Common.Station.Updater.UpdateStationV0AndHt(station, CompensationResults);
                    this.TurnToGoodAndMarkAsToBeUpdated(updatedPoints);
                    
                    // recalculer les offsets

                }
                else
                {
                    station.Parameters2.Setups.TemporaryValues = CompensationResults;
                }
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }

        internal override void SetDefaultMeasureSelection(List<TsuObject> allElements)
        {
            foreach (CompensationItem item in allElements) // so selec all the one used
            {
                if (item.Measure.IsGood)
                {
                    if (item.Measure._OriginalPoint._Coordinates.HasAny)
                    {
                        if (item.Measure._OriginalPoint._Parameters.hasGisement && !item.Measure._OriginalPoint.IsPilier)
                            item.Measure.GeodeRole = Geode.Roles.Radi;
                        else
                            item.Measure.GeodeRole = Geode.Roles.Cala;
                    }
                    else
                        item.Measure.GeodeRole = Geode.Roles.Poin;
                }
                else
                    item.Measure.GeodeRole = Geode.Roles.Unused;
            }
        }
        

        public override void Create_PLGC1_Input(Polar.Station station, string fileName, P plgc1, string title = "")
        {
            try
            {
                if (title == "") title = R.T057;
                string content = "";
                content += PII.Titr(title);
                content += PII.OptionOloc();

                // add the station to compute its heigth even if it is fixed or plgc will fail if there is nothing to compute
                content += PII.AddVZPoints(new List<E.Point>() { (station.Parameters2 as Polar.Station.Parameters)._StationPoint }, plgc1.getTheRightCoordinates);

                content += PII.AddVariablePoints(station.VariablePoints, plgc1.getTheRightCoordinates);
                //List<Point> cala = new List<Point>() { (station.Parameters2 as Stations.Theodolite.Parameters)._StationPoint };
                List<E.Point> cala = new List<E.Point>();
                cala.AddRange(station.OriginalPointsFromMeasures);
                content += PII.AddCalaPoints(cala, plgc1.getTheRightCoordinates);
                content += PII.Tstn(station, plgc1.getTheRightCoordinates);
                content += PII.End();

                //this.usingGeoid = PII.usingGeoid;
                System.IO.File.WriteAllText(fileName + ".pinp", content);
            }
            catch (Exception ex)
            {

                throw new Exception(R.T_PROBLEM_TO_CREATE_PLGC_INPUT, ex);
            }
        }

        public override void Create_LGC2_Input(Polar.Station station, string fileName, L lgc2, string title = "",  bool temporaryCompute = false)
        {

            lgc2.TreatFileName(fileName, "Orientation");


            try
            {
                bool knownHeight = station.Parameters2.Setups.InitialValues.IsInstrumentHeightKnown == Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight.Known;
                Polar.Station.Parameters p = station.Parameters2 as Polar.Station.Parameters;
                if (title == "") title = $"{R.InputTSU} 'Orientation Only' {T.Conversions.Date.ToDateUpSideDown(DateTime.Now)} ";
                string content = "";
                content += LII.Titr(title + " for orientation" + ((knownHeight) ? " only" : " and height compensation"));

                // Refence surface
                string refSurfOption = lgc2.getTheRightOption();
                content += refSurfOption + "\r\n";
                content += LII.OptionFaut();
                if (refSurfOption.ToUpper() != "*OLOC")
                    content += LII.OptionCreatePunchHFile();
                else
                    content += LII.OptionCreatePunchFile();

                content += LII.OptionJSON(true);
                content += LII.Precision(7);
                content += LII.Instr();
                content += LII.Polar(station.Parameters2._Instrument as IPolarInstrument,
                                            T.Research.GetListOfReflectorUsedIn(station),
                                            new DoubleValue(0.0, 0.0),
                                            new DoubleValue(0.0, 0.0));
                

                List<E.Point> pointsInitialized = new List<E.Point>();
                
                // seperate cala and VXY based on radi is availbel or not
                {
                    List<E.Point> cala = lgc2._Input.CALAs;
                    cala.Clear();
                    List<E.Point> vxy = lgc2._Input.VXYs;
                    vxy.Clear();
                    List<E.Point> poin = lgc2._Input.POINs;
                    poin.Clear();

                    List<E.Point> vZ = new List<E.Point>();

                    DispatchPointsBasedOnGeodeRole(station, refSurfOption, cala, vZ, vxy, poin);

                    if (station.Parameters2._StationPoint == null)
                        throw new TsuException("No Station point defined");
                    cala.Add(station.Parameters2._StationPoint);
                    if (station.NextMeasure != null)
                    {
                        cala.Add(station.NextMeasure._OriginalPoint);
                    }

                    pointsInitialized.AddRange(cala);

                    poin.Add(station.Parameters2._StationPoint);

                    E.Coordinates barycenter = null;
                    content += LII.AddCalaPoints(cala, lgc2.getTheRightCoordinates, out barycenter, station.MeasuresTaken);
                    if (station.Parameters2.Setups.InitialValues.IsInstrumentHeightKnown == Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight.Known)
                    {
                        poin.AddRange(vxy);
                        content += LII.AddVariablePoints(poin, lgc2.getTheRightCoordinates, ref pointsInitialized, "*POIN", pointToBeCommented: null, barycenter);
                    }
                    else
                    {
                        content += LII.AddVariablePoints(vxy, lgc2.getTheRightCoordinates, ref pointsInitialized, "*VXY", pointToBeCommented: null, barycenter:null);
                        content += LII.AddVariablePoints(poin, lgc2.getTheRightCoordinates, ref pointsInitialized, "*POIN", pointToBeCommented: null, barycenter);
                    }
                   

                    //cala.Add(station.Parameters2._StationPoint);

                    //foreach (var item in station.ReferencePoints)
                    //{
                    //    if (item == station.ReferencePoints[0]) // this should be the station point in case of KnownStation and the first point measure in other cases.
                    //        cala.Add(item);
                    //    else
                    //    {
                    //        if (item.fileElementType == ElementType.Alesage &&
                    //            (item._Parameters.hasGisement)) //alesage + gisement ?
                    //        {
                    //            if (station.nextMeasure != null) // if running for goto
                    //            {
                    //                if (item._Name != station.nextMeasure._Point._Name)
                    //                {
                    //                    vxy.Add(item);
                    //                }
                    //                else
                    //                    cala.Add(item);
                    //            }
                    //            else
                    //            { 
                    //                vxy.Add(item);
                    //            }
                    //        }
                    //        else
                    //        {
                    //            if (item._Coordinates.HasAny && !item._Coordinates.AreApproximate)
                    //                cala.Add(item);
                    //            else
                    //                poin.Add(item);
                    //        }
                    //    }
                    //}

                    //if (station.nextMeasure != null)
                    //{
                    //    cala.Add(station.nextMeasure._OriginalPoint);
                    //}


                    //content += LII.AddCalaPoints(cala, lgc2.getTheRightCoordinates);
                    //pointsInitialized.AddRange(cala);

                    //if (station.Parameters2.Setup._InitiaValues._IsInstrumentHeightKnown == Stations.Theodolite.Parameters.SetupParameters.InitialValues.InstrumentTypeHeight.Known)
                    //{
                    //    poin.AddRange(vxy);
                    //    poin.AddRange(station.VariablePoints);
                    //    content += LII.AddVariablePoints(poin, lgc2.getTheRightCoordinates, ref pointsInitialized, "*POIN");
                    //}
                    //else
                    //{
                    //    poin.AddRange(station.VariablePoints);


                    //    content += LII.AddVariablePoints(vxy, lgc2.getTheRightCoordinates, ref pointsInitialized, "*VXY");
                    //    content += LII.AddVariablePoints(poin, lgc2.getTheRightCoordinates, ref pointsInitialized, "*POIN");
                    //}
                    
                    if (vxy.Count > 0)
                    {
                        content += LII.AddRadi(vxy);
                        content += LII.AddRadiLine(cala, commented: true);  // we want to get bearing of the cala point commented to be able to reactivated it easily
                    }
                }

                content += LII.Tstn(station, instrumentHeightKnown: knownHeight,
                UseOnlyLastMeasure: false, useObservationSigma: false, useDistance: true, lgc2: lgc2); // should be always true (station.Parameters2.Setup._InitiaValues._IsInstrumentHeightKnown == Stations.Theodolite.Parameters.SetupParameters.InitialValues.InstrumentTypeHeight.Known));
                
                content += LII.End();

                var inputPath = lgc2._Input.CreateInputFile(content);
                Common.Compute.Compensations.Lgc2.ProcessFileControlLines(inputPath);
            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_IMPOSSIBLE_TO_CREATE_INPUT_FOR_ORIENTATION}\r\n{ex.Message}", ex);
            }
        }
        internal override void CheckGeoid()
        {
            // fill delegate wih the right code to get the right coordincate
            this.CheckIfGeoidalComputeIsPossible(station);
        }

        internal override void ChooseMeasureForApproximateCompute()
        {
            // Nothing to do because station is known.
        }
        internal override void ComputeApproximateSetup()
        {
            string message;
            // only if there is unknown point measure before the setup if not with only one point knwon masure PLGC fail because vZ is not consider as CALA.
            if (this.station.VariablePoints.Count > 0)
            {
                if (!base.ComputeApproximativePosition(updateStationCoord: false, updateMeasuredPoints: true, message: out message))
                    throw new Exception($"{R.T_COMPUTE_OF_APPROXIMATED_COORDINATES_FAILED};\r\n {message}");
            }
        }
        internal override void ChooseMeasureForCompensation()
        {

        }
        internal override void Compensate()
        {
            CompensationResults = base.RunLGC( !Silent, !Silent);
        }

        internal override string GetSetupLGC2Details()
        {
            try
            {
                return string.Format("sigma0 = {0}, {1} = {2}",
                        CompensationResults.SigmaZero.ToString("F2"),
                        R.T_INSTRUMENT_HEIGHT, CompensationResults.InstrumentHeight.ToString(5)) +"\r\n" +
                    CompensationResults.GetV0Information();

            }
            catch (Exception ex)
            {
                return $"{ R.T_COMPENSATION } {R.T_FAILED}: {ex.Message}";
            }
        }

        internal override void FindErrors()
        {
            // this._Module.View.ShowMessageOfCritical(string.Format("Lgc2 compute sigma0 = {0};Seems like the compute of the approximated coordinates works, but the compensation with LGC2 failed.", CompensationResults.SigmaZero));
            string titleAndMessage2 = $"{R.T_LGC2_COMPUTE_SIGMA0} = {CompensationResults.SigmaZero};{R.T_SEEMS_LIKE_THE_COMPUTE_OF_THE_APPROXIMATED_COORDINATES_WORKS_BUT_THE_COMPENSATION_WITH_LGC2_FAILED}";
            new MessageInput(MessageType.Critical, titleAndMessage2).Show();
            string v0AndHt = "V0s & HTs";
            string cala = "*CALA";
            // string respond = this._Module.View.ShowMessageOfChoice(string.Format("Identify wrong measures?;Do you want to restart the compute with everything in *CALA to identify the problematic measure(s)?, or to show the vZero and 'tourillon' height for each point?"), cala, R.T_NO, v0AndHt);
            string titleAndMessage = $"{R.T_IDENTIFY_WRONG_MEASURES};{R.T_DO_YOU_WANT_TO_RESTART_THE_COMPUTE_WITH_EVERYTHING_IN_CALA_TO_IDENTIFY_THE_PROBLEMATIC_MEASURES_OR_TO_SHOW_THE_VZERO_AND_TOURILLON_HEIGHT_FOR_EACH_POINT}";
            MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
            {
                ButtonTexts = new List<string> { v0AndHt, cala, R.T_NO }
            };
            string respond = mi.Show().TextOfButtonClicked;

            if (respond == cala)
            {
                Common.Compute.Compensations.Strategies.List old = this._strategy;
                this._strategy = Common.Compute.Compensations.Strategies.List.AllCala;
                this.seeOutput = true;
                base.RunLGC2();
                this._strategy = old;
                //   this._Module.View.ShowMessageOfExclamation("Now, we will try again;Normally, you should have identify the problematic measure, we will try again to compute and you will be able to deactivate some measurement.");
                string titleAndMessage1 = $"{R.T_NOW_WE_WILL_TRY_AGAIN};{R.T_NORMALLY_YOU_SHOULD_HAVE_IDENTIFY_THE_PROBEMATIC_MEASURE_WE_WILL_TRY_AGAIN_TO_COMPUTE_AND_YOU_WILL_BE_ABLE_TO_DEACTIVATE_SOME_MEASUREMENT}";
                new MessageInput(MessageType.Warning, titleAndMessage1).Show();
                CompensationResults = null; // to redo the while
            }

            if (respond == v0AndHt)
            {
                TSU.Common.Analysis.Theodolite.FindWrongMeasureInCompensation(this.station, this._strategy);
                CompensationResults = null; // to redo the while
            }
        }

        internal override string GetConfidenceEllipsoid1Sigma()
        {
            return "";
        }

        internal override string RenameFile(string fileName)
        {
            if (fileName == "")
            {
                string folder = $@"{TSU.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay}computes\(P)LGC\Orientation";
                System.IO.Directory.CreateDirectory(folder);
                return $@"{folder}\{TSU.Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now)}";
            }
            else
                return fileName;
        }
    }
}
