﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using R = TSU.Properties.Resources;
using System.Windows.Forms;
using TSU.ENUM;
using System.Xml.Serialization;
using TSU.Views;
using E = TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using TSU.Common.Instruments;
using M = TSU.Common.Measures;
using TSU.Common.Operations;
using T = TSU.Tools;
using D = TSU.Common.Instruments.Device;
using L = TSU.Common.Compute.Compensations.Lgc2;
using LI = TSU.Common.Compute.Compensations.Lgc2.Input;
using LII = TSU.Common.Compute.Compensations.Lgc2.Input.InputLine;
using P = TSU.Common.Compute.Compensations.Plgc1;
using PI = TSU.Common.Compute.Compensations.Plgc1.Input;
using PII = TSU.Common.Compute.Compensations.Plgc1.Input.InputLine;
using TSU.Common.Compute.Compensations;
using TSU.Common.Elements;
using TSU.Common;

namespace TSU.Polar.Compensations
{
    [Serializable]
    public class InterssectionLGC2 : TSU.Common.Compute.Compensations.Strategies.Common
        {
            public InterssectionLGC2()
            {
                _strategy = TSU.Common.Compute.Compensations.Strategies.List.Intersections;
                lgc2 = new Lgc2();
            }
            public InterssectionLGC2(Polar.Station.Module module)
                : base(module)
            {
                _strategy = TSU.Common.Compute.Compensations.Strategies.List.Intersections;
            }

            public override bool Update()
            {
                if (CompensationResults == null) return false;
                if (!CompensationResults.Ready) return false;

            TSU.Common.Station.Updater.UpdateLastMeasuredPoint(station.MeasuresTaken[station.MeasuresTaken.Count - 1] as Polar.Measure, CompensationResults.VariablePoints); // we dont wnat to recompute all point from station after every throwpoint, specailly beacuse it could modify 'original point' of the measure in case of stake out

                return true;
            }

        internal override void SetDefaultMeasureSelection(List<TsuObject> allElements)
        {
            foreach (CompensationItem item in allElements) // so selec all the one used
            {
                if (item.Measure.IsGood)
                {
                    if (item.Measure._OriginalPoint._Coordinates.HasAny)
                    {
                        item.Measure.GeodeRole = TSU.IO.SUSoft.Geode.Roles.Cala;
                    }
                    else
                        item.Measure.GeodeRole = TSU.IO.SUSoft.Geode.Roles.Poin;
                }
                else
                    item.Measure.GeodeRole = TSU.IO.SUSoft.Geode.Roles.Unused;
            }
        }

        public override void Create_PLGC1_Input(Polar.Station station, string fileName, P plgc1, string title = "")
        {
            try
            {
                if (title == "") title = R.T041;
                string content = "";

                content += PII.Titr(title);
                content += PII.OptionOloc();
                content += PII.AddVariablePoints(new List<E.Point>() { station.PointsMeasured[station.PointsMeasured.Count - 1] }, plgc1.getTheRightCoordinates);

                // create a virtual point on the Y axis (north)
                Polar.Station.Parameters p = station.Parameters2 as Polar.Station.Parameters;
                E.Point ghost = p._StationPoint.DeepCopy() as E.Point;
                ghost._Name = "GHOST";
                // with get coordinate we receive a clone so i have to add to all Ycoordinates defore the gettcoorinate
                if (ghost._Coordinates.Local.Y!=null)
                    ghost._Coordinates.Local.Y.Value += 1;
                ghost._Coordinates.Ccs.Y.Value += 1;
                CloneableList<E.Point> calaPoints = new CloneableList<E.Point>();
                calaPoints.Add(ghost);
                calaPoints.Add(station.Parameters2._StationPoint);

                content += PII.AddCalaPoints(calaPoints, plgc1.getTheRightCoordinates);

                //make a fake station with needed info
                Polar.Station s = new Polar.Station();
                s.Parameters2 = station.Parameters2;
                // add last measure and the ghost one;
                s.MeasuresTaken.Add(station.MeasuresTaken.Last());
                Polar.Measure m = new Polar.Measure();

                ghost.StorageStatus = Tsunami.StorageStatusTypes.Temp;
                m._Point = ghost;
                m._OriginalPoint = ghost;
                m._Status = new M.States.Good();

                m.Angles.raw = new E.Angles() { Horizontal = new DoubleValue(400 - station.Parameters2.Setups.BestValues.vZero.Value, 0), Vertical = new DoubleValue() };

                m.Distance.Raw = new DoubleValue();
                s.MeasuresTaken.Add(m);

                content += PII.Tstn(s, plgc1.getTheRightCoordinates);
                content += PII.End();

                //this.usingGeoid = InputLine.usingGeoid;
                System.IO.File.WriteAllText(fileName + ".pinp", content);
            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_PROBLEM_TO_CREATE_PLGC_INPUT_FOR_POINTLANCÉ}\r\n{ex.Message}", ex);
            }
        }

        public override void Create_LGC2_Input(Polar.Station station, string fileName, Lgc2 lgc2, string title = "",  bool temporaryCompute = false)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This will create an input with the station as cala, a orentation point theoreticaly computed with the V0 of the station
        /// </summary>
        /// <param name="station"></param>
        /// <param name="title"></param>
        public void Create_LGC2_Input(List<Station.Module> PolarStationModules, List<Point> pointsToIntersect)
        {
            var stationModules = new List<Common.Station.Module>(PolarStationModules);

            Common.Compute.Compensations.Lgc2 lgc2 = new Common.Compute.Compensations.Lgc2(stationModuleList: stationModules, pointsToIntersect); // create the golbal input
            lgc2.Run(true, true, true);

            
            //this.lgc2._Input = new LI(lgc2, "");
            //RenameFile(); // give a default name if used with no paraemeters
            //CheckIfGeoidalComputeIsPossible(PolarStationModules[PolarStationModules.Count - 1], this);
            
            //try
            //{
            //    // Create header
            //    string content = LII.Titr(R.T051 + " for 'intersections'");

            //    // Refence surface
            //    string refSurfOption = this.getTheRightOption();
            //    content += refSurfOption + "\r\n";
            //    content += LII.OptionFaut();
            //    if (refSurfOption.ToUpper() != "*OLOC")
            //        content += LII.OptionCreatePunchHFile();
            //    else
            //        content += LII.OptionCreatePunchFile();

            //    content += LII.OptionJSON(true);
            //    content += LII.Precision(7);
            //    content += LII.Instr();


            //    string starInstrumentsContent = "";
            //    List<E.Point> cala = new List<Point>();
            //    string starCala = "";

            //    List<E.Point> poin = new List<Point>();
            //    string starPoin = "";

            //    string starStationsContent = "";

            //    foreach (var station in PolarStationModules)
            //    {
            //        Polar.Station.Parameters p = station.Parameters2 as Polar.Station.Parameters;
                
            //        // create a virtual point on the Y axis (north)
            //        E.Point ghost = p._StationPoint.DeepCopy() as E.Point;
            //        ghost._Name = "GHOST";
            //        E.Coordinates c = this.getTheRightCoordinates(ghost);

            //        if (ghost._Coordinates.Ccs.AreKnown)
            //            ghost._Coordinates.Ccs.Y.Value += 1; 
            //        else
            //            ghost._Coordinates.Local.Y.Value += 1;

            //        starInstrumentsContent += LII.Polar(station.Parameters2._Instrument as IPolarInstrument,
            //                                    T.Research.GetListOfReflectorUsedIn(station),
            //                                    new DoubleValue(0.0, 0.0),
            //                                    new DoubleValue(0.0, 0.0));

            //        List<E.Point> pointsInitialized = new List<E.Point>();

            //        E.Coordinates barycenter = null;
            //        // *CALA
            //        {
            //            cala.Add(ghost);
            //            cala.Add(station.Parameters2._StationPoint);

                        
            //            pointsInitialized.AddRange(cala);
            //        }

            //        // *POIN
            //        {
            //            // add all points
            //            poin.Add(station.PointsMeasured[station.PointsMeasured.Count - 1]);

            //            // Create a copy of list to avoid modifying the original list
            //            List<Point> pointToBeCommented = new List<Point>(poin);

            //            // Remove elements from listC that are contained in listB, they will be commented
            //            pointToBeCommented.RemoveAll(item => pointsToIntersect.Contains(item));

            //            starPoin += LII.AddVariablePoints(poin, this.getTheRightCoordinates, ref pointsInitialized, "*POIN", pointToBeCommented: null, barycenter);
            //        }

            //        starStationsContent = LII.Tstn(station, true, true, useObservationSigma: false, useDistance: true, lgc2: lgc2);
            //        starStationsContent = starStationsContent.Replace("*ANGL", "*ANGL\r\n" + "   " + "   " + "   " + ghost._Name.ToUpper() + " " + (400 - station.Parameters2.Setups.BestValues.vZero.Value).ToString());
            //    }

            //    content += LII.AddCalaPoints(cala, this.getTheRightCoordinates, out barycenter, station.MeasuresTaken);
            //    content += starInstrumentsContent;
            //    content += starCala;
            //    content += starPoin;
            //    content += starStationsContent;
            //    content += LII.End();

            //    this.lgc2._Input.CreateInputFile(content);
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception($"{R.T_IMPOSSIBLE_TO_CREATE_INPUT_AS_RADIATED_POINT}\r\n{ex.Message}", ex);
            //}
            
        }

        internal override void CheckGeoid()
            {
                // fill delegate wih the right code to get the right coordincate
                this.CheckIfGeoidalComputeIsPossible(station, new PointLancéLGC2());
            }

            internal override void ChooseMeasureForApproximateCompute()
            {
                // nothing to do
            }
            internal override void ComputeApproximateSetup()
            {
            try
            {
                P plgc = new P(station, this, getTheRightCoordinates, getTheRightOption, coordianteSystemType);
                bool runned = plgc.Run(editInput, seeOutput);
                if (runned)
                {
                    TSU.Common.Station.Updater.UpdateLastMeasuredPoint(station.MeasuresTaken[station.MeasuresTaken.Count - 1] as Polar.Measure, plgc.GetOutputCoordinates());
                }

                this.SaveFilesPath(P._FileName);
            }
            catch (Exception ex)
            {

                throw new Exception("Computation of approximate coordinates failed", ex);
            }
        }


        internal override void ChooseMeasureForCompensation()
        {
            // nothing to do
        }
        internal override void Compensate()
        {
            CompensationResults = base.RunLGC(showLog:false, showSigmaO: false);

            this.SaveFilesPath(lgc2._Output.OutputFilePath);
        }
        internal override void FindErrors()
        {
        }

        internal override string GetSetupLGC2Details()
        {
            try
            {
                return string.Format("sigma0 = {0}, {1} = {2}",
                     CompensationResults.SigmaZero.ToString("F2"),
                     R.T_INSTRUMENT_HEIGHT, CompensationResults.InstrumentHeight);

            }
            catch (Exception ex)
            {
                return $"{ R.T_COMPENSATION } {R.T_FAILED}: {ex.Message}";
            }
        }

        internal override string GetConfidenceEllipsoid1Sigma()
        {
            return "";
        }

        /// <summary>
        /// give a default name if used with no paraemeters
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        internal override string RenameFile(string fileName="")
        {
            if (fileName == "")
            {
                string folder = $@"{TSU.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay}computes\(P)LGC\Intersections";
                System.IO.Directory.CreateDirectory(folder);
                return $@"{folder}\{TSU.Tools.Conversions.Date.ToDateUpSideDownPrecise(DateTime.Now)}";
            }
            else
                return fileName;
        }

    }
}
