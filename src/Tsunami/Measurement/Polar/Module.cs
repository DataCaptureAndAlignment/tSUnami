using System;
using System.Collections.Generic;
using System.Text;
using R = TSU.Properties.Resources;
using System.Windows.Forms;
using TSU.ENUM;
using System.Xml.Serialization;
using TSU.Views;
using E = TSU.Common.Elements;
using TSU.Common.Instruments;
using TSU.Common.Measures;

using O = TSU.Common.Operations;
using TSU.Tools;
using TSU.Common.Elements.Composites;
using System.Activities.Expressions;
using TSU.Functionalities.ManagementModules;
using System.Linq;
using TSU.Views.Message;
using TSU.Common.Elements;
using TSU.Common.Compute.Compensations;
using TSU.Common.Compute.Compensations.Strategies;

namespace TSU.Polar
{
    [XmlType(TypeName = "Polar.Module")]
    public class Module : Common.FinalModule
    {
        public override Common.ObservationType ObservationType { get; set; } = Common.ObservationType.Polar;
        public enum MeasurementType
        {
            All,
            Dist,
            HzV,
            HzV_then_dist,
            dist_then_HzV
        }

        public MeasurementType measurementType;


        [XmlIgnore]
        public new View View
        {
            get
            {
                return this._TsuView as View;
            }
            set
            {
                this._TsuView = value;
            }
        }

        [XmlIgnore]
        public Polar.Station.Module StationTheodoliteModule
        {
            get
            {
                return this._ActiveStationModule as Polar.Station.Module;
            }
            set
            {
                this._ActiveStationModule = value;
            }
        }

        [XmlIgnore]
        public List<Polar.Station.Module> StationTheodoliteModules
        {
            get
            {
                List<Polar.Station.Module> l = new List<Polar.Station.Module>();
                foreach (Polar.Station.Module item in childModules)
                {
                    if (item is Polar.Station.Module) l.Add(item as Polar.Station.Module);
                }
                return l;
            }
            set
            {
                foreach (Polar.Station.Module item in value)
                {
                    childModules.Add(item);
                }
            }
        }



        [XmlIgnore]
        public override List<Magnet> MagnetsToBeAligned
        {
            get
            {
                CloneableList<E.Point> listOfPoints = new CloneableList<E.Point>();

                // collect all points
                foreach (var stm in this.StationTheodoliteModules)
                {
                    if (stm.StationTheodolite != null)
                    {
                        foreach (var meas in stm.StationTheodolite.MeasuresTaken)
                        {
                            if (meas._Point.IsAlesage == true)
                                listOfPoints.Add(meas._Point);
                        }
                        
                    }
                }

                // create magnets
                var magnets = CompositeElement.GetMagnets(listOfPoints);
                
                return magnets;
            }
        }
           
        public override string ToString()
        {
            return $"{this._Name} = {base.ToString()}";
        }

        [XmlIgnore]
        public bool PauseBetweenAnglesAndDistance = false;



        // Constructor
        public Module() : base()
        {
        }// this parameterless constructor is normaly used only by the serializer, we put here the initialization of what is not serialized (like views for example)

        public Module(TSU.Module parentModule, string nameAndDescription)
            : base(parentModule, nameAndDescription)
        {
            
        }
        
        public override void Initialize()
        {
            base.Initialize();
        }

        public static List<DsaFlag> DsaFlags_CreatesForAdvancedModule()
        {
            List<DsaFlag> dsaFlags = new List<DsaFlag>()
            {
                new DsaFlag("ClosureTriggeredByEndStation", DsaOptions.Never),// use to launch the compute after closure if the closure was launched from the compute button
                new DsaFlag("WarnForPreviousPrismUse", DsaOptions.Ask_to),
                new DsaFlag("DoStationSetupAfterClosure", DsaOptions.Ask_to),
                new DsaFlag("ShowMeasureDifferences", DsaOptions.Ask_to),
                new DsaFlag("ShowMeasureResults", DsaOptions.Ask_to),
                new DsaFlag("ShowResultsAtTheEndOfASequence", DsaOptions.Ask_to),
                new DsaFlag("ProposeToChangeCodeWhenRenaming", DsaOptions.Ask_to),
                new DsaFlag("IterateSequenceWithBracket", DsaOptions.Ask_to),
                new DsaFlag("ProposeFreeStationIfPointConnu", DsaOptions.Ask_to),
                new DsaFlag("ShowQuestionableResults", DsaOptions.Ask_to),
                new DsaFlag("RejectQuestionableMeasure", DsaOptions.Ask_to),
                new DsaFlag("KeepQuestionableMeasure", DsaOptions.Ask_to),
                new DsaFlag("SaveAcquisitionIfOffsetAccepted", DsaOptions.Ask_to),
                new DsaFlag("WantToIterateTheNewPointName", DsaOptions.Ask_to),
                new DsaFlag("CheckIfNameExistBeforeTheMeasurementHappen", DsaOptions.Ask_to),
                new DsaFlag("ShouldRemeasureExisting", DsaOptions.Ask_to),
                new DsaFlag("PutPreviousMeasureToBad", DsaOptions.Ask_to),
                new DsaFlag("ShowGotoFailedMessage", DsaOptions.Ask_to),
                new DsaFlag("MeasureEvenIfGotoWasNotPossible", DsaOptions.Ask_to),
                new DsaFlag("AskToRenameNewUnknownPoint", DsaOptions.Ask_to),
                new DsaFlag("RenameKeptQuestionableMeasure", DsaOptions.Ask_to)
            };
            return dsaFlags;
        }


        internal void NewStation()
        {
            Polar.Station.Module s = new Polar.Station.Module(this);

            if (_ActiveStationModule != null)
                CloneOrNotStationParameters(s, _ActiveStationModule);

            this.SwitchToANewStationModule(s);
        }

        private enum CloningParameters
        {
            Nothing,
            Admin_parameters_only,
            Station_parameters_without_station_point,
            Station_parameters,
            Station_parameters_with_references,
            Station_parameters_with_references_with_orientation,
            Station_parameters_with_all_points,
            Station_parameters_with_all_points_with_orientation,
        }

        private bool CloneOrNotStationParameters(Polar.Station.Module next, Common.Station.Module previous)
        {
            bool oldStyle = false;
            var pst = previous._Station as Polar.Station;
            int stationPointCount = pst.Parameters2.Setups.InitialValues.IsPositionKnown ? 0 : 1; // free station is considere as a variable point

            string cancel = R.T_CANCEL;
            string clone = R.T_CLONE;
            string dont = R.T_DONT;
            string message = $"{R.T_TH_ST_CLONE};'{previous._Station._Name}'\r\n" +
                $"{R.T_OPERATION}: {previous._Station.ParametersBasic?._Operation}\r\n" +
                $"Station point: {pst.Parameters2._StationPoint?._Name}\r\n" +
                $"Refs: {pst.ReferencePoints.Count}\r\n" +
                $"Measures: {pst.VariablePoints.Count - stationPointCount}\r\n\r\n" +
                $"Select what to clone: ";

            if (oldStyle)
            {
                MessageInput mi = new MessageInput(MessageType.Choice, message)
                {
                    ButtonTexts = new List<string> { cancel, clone, dont }
                };
                string respond = mi.Show().TextOfButtonClicked;

                if (respond == clone)
                    CloneStationParameters(next, previous);

                if (respond == cancel)
                    return false;

                return true;
            }
            else
            {
                BindingSource bindingSource = new BindingSource();
                string[] names = Enum.GetNames(typeof(CloningParameters));
                for (int i = 0; i < names.Length; i++)
                {
                    names[i] = names[i].Replace("_with_", "_+_");
                    names[i] = names[i].Replace("_", " ");
                }

                bindingSource.DataSource = names;

                var combobox = CreationHelper.GetComboBox(bindingSource);
                combobox.BindingContext = new BindingContext();
                combobox.SelectedIndex = 2;
                MessageInput mi = new MessageInput(MessageType.Choice, message);
                mi.ButtonTexts = new List<string> { clone, cancel };
                mi.Controls = new List<Control> { combobox };
                using (var r = mi.Show())
                {
                    if (r.TextOfButtonClicked == cancel)
                        return false; // cancel
                    else
                    {
                        string returnedText = (string)r.ReturnedControls[0].Text;
                        returnedText = returnedText.Replace(" ", "_");
                        returnedText = returnedText.Replace("_+_", "_with_");
                        Enum.TryParse(returnedText, out CloningParameters p);

                        if (p == CloningParameters.Nothing)
                            return false;
                        else
                        {
                            bool adminOnly = p == CloningParameters.Admin_parameters_only;
                            
                            CloneStationParameters(next, previous, adminOnly);
                            if (adminOnly)
                                return true;

                            bool keepStationPoint = !(p == CloningParameters.Station_parameters_without_station_point);
                            if (keepStationPoint)
                                Station.Module.Sequence_CloneStationPoint(next, (Station)previous._Station);

                            bool keepOrientation = (p == CloningParameters.Station_parameters_with_references_with_orientation || p == CloningParameters.Station_parameters_with_all_points_with_orientation);
                            if (keepOrientation)
                            {
                                Station.Module.Sequence_CloneSetupToKeepGoto(next, (Station)previous._Station);
                            }

                            if (p == CloningParameters.Station_parameters || p == CloningParameters.Station_parameters_without_station_point)
                                return true;
                            else
                            {
                                bool onlyRefs = !(p == CloningParameters.Station_parameters_with_all_points || p == CloningParameters.Station_parameters_with_all_points_with_orientation);
                                Station.Module.Sequence_ClonePointsToMeasure(next, (Station)previous._Station, onlyRefs);

                            }
                        }
                        return true;
                    }
                }
            }
        }

        private void CloneStationParameters(Polar.Station.Module s, Common.Station.Module activeStationModule, bool adminOnly = false, bool defaultMeasurementParameters = true)
        {
            Station.Parameters newOne = s._Station.ParametersBasic as Station.Parameters;
            Station.Parameters oldOne = activeStationModule._Station.ParametersBasic as Station.Parameters;
            newOne._Team = oldOne._Team;
            newOne._Operation = oldOne._Operation.Clone() as O.Operation;
            if (adminOnly)
                return;
            if (defaultMeasurementParameters)
            {
                newOne.DefaultMeasure = oldOne.DefaultMeasure.Clone() as Polar.Measure;
                var point = oldOne.DefaultMeasure._Point.Clone() as E.Point;
                point.StorageStatus = Common.Tsunami.StorageStatusTypes.Hidden;
                newOne.DefaultMeasure._Point = point;
                newOne.DefaultMeasure.Guid = Guid.NewGuid();
            }
            newOne._Instrument = oldOne._Instrument;
            // s.OnInstrumentChanged(s._Station.ParametersBasic._Instrument); will be done in 'SetInstrumentInActiveStationModule()'
        }

        internal void  ExploreStations()
        {
            TSU.Manager manager = CreateAStationManager();

            manager.View.ShowInMessageTsu(
                R.T_STATIONS_ABSTRACT,
                R.T_SELECT, 
                () =>
                {
                    this.SetActiveStationModule(this.StationModules.Find(x=>x._Station == manager._SelectedObjects[0]));
                }, R.T_CANCEL);
        }

        public Manager CreateAStationManager()
        {
            TSU.Manager manager = new TSU.Manager(Tsunami2.Properties, $"{R.T_STATIONS_ABSTRACT};...");
            manager.AllElements = new List<TsuObject>();
            manager.MultiSelection = false;
            manager.Initialize();

            foreach (var item in this.StationModules)
            {
                var station = item._Station;
                if (station.MeasuresTaken.Count != 0) // do not show empty station, specially usefull to hide the current one if it just started
                    manager.AllElements.Add(item._Station);
            }
            manager.SelectableObjects.AddRange(manager.AllElements);

            manager.View.ListviewColumnHeaderTexts = new List<string>() { R.T_STATION_NAME, "Date", "Point", "State" };
            manager.View.HideEmptyColumns = false;
            manager.View.ChangeStrategy(Common.Strategies.Views.Types.Tree);
            manager.View.currentStrategy.Initialize(); // to apply headers

            var last = manager.SelectableObjects.Last();
            manager._SelectedObjectInBlue = last;
            manager.AddSelectedObjects(last);

            return manager;
        }

        internal override void FinishModule()
        {
            bool canClose = true;
            string message = "";
            foreach (Polar.Station.Module item in this.StationTheodoliteModules)
            {
                if (!item.IsClosable)
                {
                    canClose = false;
                    message += $"{item._Name} => '{item.stationParameters._State._Description}'";
                }
            }
            if (canClose)
                base.FinishModule();
            else
            {
                string titleAndMessage = $"{R.T_CANNOT_FINISH_MODULE}; {R.T_ALL_STATIONS_MUST_BE_EITHER_CLOSED_OR_BAD}:\r\n{message}";
                new MessageInput(MessageType.Warning, titleAndMessage).Show();
            }
        }
        public override void OnNext(TsuObject TsuObject)
        {
            dynamic dynamic = TsuObject;
            OnNextBasedOn(dynamic);
        }
        public void OnNextBasedOn(E.Composites.TheoreticalElement t)
        {
            this._ElementManager.SetSelectableToAllUniquePoint();
            this._ElementManager.UpdateView();
        }



        internal new List<Common.Station> GetListOfStations()
        {
            List<Common.Station> stations = new List<Common.Station>();
            foreach (Polar.Station.Module s in this.childModules.OfType<Polar.Station.Module>())
            {
                if (!s.IsClosable)
                {
                    string titleAndMessage = $"{R.T_STATION_IN_PROGRESS}; {R.T_YOU_SHOULD_CLOSE_THE} {s.StationTheodolite._Name} {R.T_IN_ORDER_TO_EXPORT_IT}";
                    new MessageInput(MessageType.Warning, titleAndMessage).Show();
                }

                stations.Add(s.StationTheodolite);
            }

            return stations;
        }

        internal override IEnumerable<Common.Station> GetStations()
        {
            var stations = new List<Common.Station>();
            foreach (var item in this.StationModules)
            {
                stations.Add(item._Station);
            }
            return stations;
        }
    }
}
