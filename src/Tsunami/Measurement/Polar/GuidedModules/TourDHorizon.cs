﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using TSU.Views;
using MMMM = TSU;

using M = TSU.Common.Measures;
using I = TSU.Common.Instruments;

using A = TSU.Common.Analysis;
using System.Data;
using TSU.Polar.GuidedModules.Steps;
using TSU.Common.Compute;
using TSU.Common.Guided;
using TSU.Common.Guided.Steps;
using TSU.Views.Message;

namespace TSU.Polar.GuidedModules
{
    static public class TourDHorizon
    {
        static public TdH Get(MMMM.Module parentModule, string nameAndDescription)
        {
            TdH gm = new TSU.Polar.GuidedModules.TdH(parentModule, nameAndDescription);
            gm.guideModuleType = ENUM.GuidedModuleType.TourDHorizon;
            gm.ObservationType = Common.ObservationType.Polar;

            Decore.ShowRaisedEvent(gm);
            // Invisible step: : Add needed station modules
            Station.Module stm = new Station.Module(gm);
            //stm.View.Dispose();
            //stm.View = null;
            stm.stationParameters.DefaultMeasure.Face = I.FaceType.Face1;
            stm.stationParameters.DefaultMeasure.NumberOfMeasureToAverage = 3;

            stm.ChangeSetupStrategy(TSU.Common.Compute.Compensations.Strategies.List.NoComputation2);
            gm.SwitchToANewStationModule(stm);

            ViewsOption(gm);
            BuildSteps(gm);

            gm.Start();

            return gm;
        }

        private static void ViewsOption(Common.Guided.Module gm) // not serialized
        {
            Station.Module stm = gm._ActiveStationModule as Station.Module;

            stm.FaceMeasurementHasBeenManuallySet = true; // very important ! because if notorized it will then force to DF instead F1 (if not set to true.)
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShowMeasureDifferences").State = DsaOptions.Never; //stm.flags.ShowMeasureDifferences = false;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "DoStationSetupAfterClosure").State = DsaOptions.Never; //stm.flags.DoStationSetupAfterClosure = false;
            stm.ValidateComputeAsFastAsPossible = true;
            stm.RemoveMeasureFromNextPointListAfterReception = false;
            //stm.flags.
            //stm.flags.AddDefaultpoint = false;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "RenameWhenExist").State = DsaOptions.Never;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ProposeFreeStationIfPointConnu").State = DsaOptions.Never; //stm.flags.ProposeFreeStationIfPointConnu = false;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShouldRemeasureExisting").State = DsaOptions.Always; //stm.flags.ShouldRemeasureExisting = DsaOptions.Always; // dont ask because cannot cancel in the guided module

            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShouldChangeNameOfExistingBeforeMeasurement").State = DsaOptions.Never; //stm.flags.ShouldChangeNameOfExistingBeforeMeasurement = DsaOptions.Never;
        }

        private static void BuildSteps(Common.Guided.Module gm)
        {
            gm.Add(Management.Declaration(gm, gm.NameAndDescription));
            gm.Add(Management.ChooseTheoreticalFile(gm));
            gm.Add(Management.ChooseInstrument(gm, I.InstrumentClasses.POLAR));
            gm.Add(Management.EnterATeam(gm));
            gm.Add(Management.ChooseOperation(gm));
            //gm.Add(TdHNumberSelection(gm));
            gm.Add(Setup.StationPosition(gm));
            gm.Add(Setup.StationHeight(gm));
            gm.Add(Setup.StationDefaultMeasurementParameters(gm));
            gm.Add(Steps.Measurement.ChoosePointsToMeasure(gm));
            gm.Add(Steps.Measurement.MeasurementAbstractBefore(gm, Steps.Measurement.Types.Measurement));

            // Theodolite.MeasureNextPoint is a added a number of time when entering Theodolite.MeasurementAbstractBefore or Theodolite.MeasureNextPoint

            gm.Add(Steps.Measurement.Closure(gm));
            gm.Add(TdHCheckpoint(gm));

            // Theodolite.MeasureNextPoint is a added a number of time when leaving TdHcheckpoinst or Theodolite.MeasureNextPoint

            //gm.Steps.Add(GS.Management.ShowMeasurements(gm));
            gm.Add(TdHReductionAndExport(gm));
            gm.Steps.Add(Setup.End(gm, R.StringGuided_End));

        }

        static public void ReCreateWhatIsNotSerialized(Common.Guided.Module gm)
        {
            gm.Steps = new List<Step>();

            Decore.ShowRaisedEvent(gm);
            ViewsOption(gm);
            BuildSteps(gm);

            // RecreateRounds(gm);

            { // desire step to start after TSU opening
                string desireName = "";
                if (gm.Finished)
                    desireName = "Export";
                else
                    desireName = "CheckPoint";
                gm.currentIndex = gm.GetStepIndexBasedOnName(desireName);
            }
        }


        private static void RecreateRounds(Common.Guided.Module gm)
        {
            TdH tdhm = gm as TdH;
            tdhm.Rounds.Clear();
            foreach (var item in gm.StationModules)
            {
                Station.Module stm = item as Station.Module;
                string lastOrigin = "NoChanceToBeAnExistingOrigin";
                int lastNumber = 0;
                foreach (Measure m in stm.StationTheodolite.MeasuresTaken)
                {
                    if (m.Origin != lastOrigin)
                    {
                        lastOrigin = m.Origin;
                        tdhm.Rounds.Add(new M.TheodoliteRound(stm.StationTheodolite.MeasuresTaken.Cast<Measure>().ToList().FindAll(x => x.Origin == m.Origin)));
                        lastNumber++;
                        tdhm.Rounds.Last().Number = lastNumber;
                    }
                }

                if (tdhm.Rounds.Count == 1 && tdhm.RoundState.Count == 0)
                {
                    tdhm.Rounds[0].State = M.States.Types.Good;
                }
                else
                {
                    if (tdhm.Rounds.Count == tdhm.RoundState.Count)
                    {
                        for (int i = 0; i < tdhm.Rounds.Count; i++)
                        {
                            tdhm.Rounds[i].State = tdhm.RoundState[i];
                        }
                    }
                    else if (tdhm.Rounds.Count == tdhm.RoundState.Count + 1)
                    {
                        for (int i = 0; i < tdhm.Rounds.Count - 1; i++)
                        {
                            tdhm.Rounds[i].State = tdhm.RoundState[i];
                        }
                    }
                    else
                    {
                        string titleAndMessage = $"{R.T_ALL_ROUNDS_OF_THE_TOUR_DHORIZON_HAVE_BEEN_TURNED_TO_GOOD_STATE};{R.T_THEIR_STATE_WERE_LOST_SORRY}";
                        new MessageInput(MessageType.Warning, titleAndMessage).Show();
                    }
                }
            }

            tdhm.RoundState.Clear();
            tdhm.Rounds.ForEach(x => tdhm.RoundState.Add(x.State));
        }

        internal static Step TdHCheckpoint(Common.Guided.Module gm)
        {
            Step s = new Step(gm, R.T_TdHCheckpointTitle);
            s.Id = "CheckPoint";


            // refs
            Station.Module stm = s.GuidedModule._ActiveStationModule as Station.Module;
            Common.Station st = stm.StationTheodolite;
            Station.View v = stm.View;

            M.TheodoliteRounds rounds = (gm as TdH).Rounds;


            #region Actions

            BigButton AddARound = new BigButton(R.T_Tdh_add, R.Add);
            AddARound.BigButtonClicked += delegate
            {
                List<Measure> Measures = st.MeasuresTaken.Cast<Measure>().ToList();
                s.Test();
                if (s.State == StepState.Valid)
                {
                    I.FaceType previousFaceUsed = Measures.Last().Face;
                    I.FaceType nextFaceUsed = previousFaceUsed == I.FaceType.Face1 ? I.FaceType.Face2 : I.FaceType.Face1;

                    // removing the possible remaining step.
                    {
                        for (int i = 0; i < gm.Steps.Count; i++)
                        {
                            if (gm.Steps[i].TAG != null)
                                if (gm.Steps[i].TAG is Measure)
                                {
                                    gm.RemoveStep(i, true);
                                }
                        }
                    }

                    // copy measures in the opposite order from the previous face without the closure taht already is there
                    M.TheodoliteRound lastValidRound = rounds.LastValidRound;
                    if (lastValidRound != null)
                    {
                        lastValidRound.State = M.States.Types.Good;  // make the temporary rounds valid 
                        List<Measure> goodmeasures = lastValidRound.GoodMeasures;
                        goodmeasures.Reverse(1, goodmeasures.Count - 1);

                        s.GuidedModule.CustomMeasureToBeDone = new List<Measure>();
                        foreach (var item in goodmeasures)
                        {
                            Measure copy = item.GetNiceClone();
                            copy.Face = item.Face == I.FaceType.Face1 ? I.FaceType.Face2 : I.FaceType.Face1;
                            copy._Status = new M.States.Unknown();
                            copy.Clean(); // to reset the correction flags.
                            s.GuidedModule.CustomMeasureToBeDone.Add(copy);
                        }

                        // change the origin of the previous round.
                        string additionalOrigin = " (" + DateTime.Now.ToString() + ")";
                        foreach (var item in rounds.Last().Measures) item.Origin += additionalOrigin;

                        s.GuidedModule.MoveToStep("Abstract" + Steps.Measurement.Types.Measurement.ToString(), false);
                    }
                }
            };

            BigButton RemoveARound = new BigButton(R.T_TDH_REMOVE_A_AROUND, R.StatusBad,TSU.Tsunami2.Preferences.Theme.Colors.Bad);
            RemoveARound.BigButtonClicked += delegate
            {
                string titleAndMessage = $"{R.T_REMOVE_A_ROUND};{R.T_PLEASE_ENTER_THE_NUMBER_OF_THE_ROUND_YOU_WANT_TO_REMOVE}\r\n" + rounds.ToString(true);
                List<string> buttonTexts = new List<string> { R.T_REMOVE, R.T_CANCEL };

                MessageTsu.ShowMessageWithTextBox(titleAndMessage, buttonTexts, out string buttonClicked, out string textInput, rounds.Count.ToString());

                int roundToSuppress;
                if (buttonClicked != R.T_CANCEL && int.TryParse(textInput, out roundToSuppress))
                {
                    (gm as TdH).UpdateRounds(roundToSuppress - 1, M.States.Types.Bad);
                    s.Update();
                }
            };

            BigButton RestoreARound = new BigButton(R.T_TDH_RESTORE_A_AROUND, R.StatusGood,TSU.Tsunami2.Preferences.Theme.Colors.Good);
            RestoreARound.BigButtonClicked += delegate
            {
                string titleAndMessage = $"{R.T_RESTORE_A_ROUND};{R.T_PLEASE_ENTER_THE_NUMBER_OF_THE_ROUND_YOU_WANT_TO_RESTORE}\r\n" + rounds.ToString(true);
                List<string> buttonTexts = new List<string> { R.T_RESTORE, R.T_CANCEL };

                MessageTsu.ShowMessageWithTextBox(titleAndMessage, buttonTexts, out string buttonClicked, out string textInput, rounds.Count.ToString());

                int roundToSuppress;
                if (buttonClicked != R.T_CANCEL && int.TryParse(textInput, out roundToSuppress))
                {
                    (gm as TdH).UpdateRounds(roundToSuppress - 1, M.States.Types.Good);
                    s.Update();
                }
            };

            #endregion

            #region Step Events

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                List<Measure> Measures = st.MeasuresTaken.Cast<Measure>().ToList();
                // Get some view controls
                e.step.skipable = true;


                //Grid
                s.GuidedModule.View.grid = StakeOut.Tools.Grid.Initialize(DataGridViewAutoSizeColumnsMode.DisplayedCells);

                s.GuidedModule.View.grid.CellFormatting += On_Grid_CellFormatting;
                s.GuidedModule.View.grid.Tag = (s.GuidedModule._ActiveStationModule._Station as Station).Parameters2.AprioriSigmaForTheSelectedInstrumentAndConditon;
                e.step.View.ModifyLabel($"{R.T_TDH_HOW}.\r\n{R.T_SUMMARY}:", "tableau");
                s.View.AddGrid(s.GuidedModule.View.grid);

                // label and buttons
                e.ActiveStationModule._MeasureManager.MultiSelection = true;
                if (Measures.Count > 0)
                {
                    e.step.View.ModifyLabel(string.Format(R.T_TdhFace1NotOk, Measures.Last().Face.ToString()), "state");
                    e.step.View.ModifyLabel(string.Format("", Measures.Last().Face.ToString()), "closure");
                }
                else
                {
                    //   e.step.View.ModifyLabel(string.Format("No measure yet"), "state");
                    //   e.step.View.ModifyLabel(string.Format("No measure yet"), "closure");
                    e.step.View.ModifyLabel($"{R.T_NO_MEASURE_YET}", "state");
                    e.step.View.ModifyLabel($"{R.T_NO_MEASURE_YET}", "closure");
                }
                e.step.View.ModifyLabel(R.T_TdHCheckpoint, "next");
                e.step.View.AddSidedControls(new List<Control>() { RemoveARound, RestoreARound }, DockStyle.Top);
                e.step.View.ModifyLabel(R.T_Tdh_RoundNumber, "roundnumber");
                e.step.View.AddButton(AddARound);
                e.step.AddButton(e.ActiveStationModule.View.buttons.MeasureDetails, "mes");

                // Additional statistic
                e.step.View.ModifyLabel(R.T_Tdh_resume, "resumé");
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {

                e.ActiveStationModule._Station.MeasuresToExport.Clear(); // this will be fill when it is time to export but it can influence the lgc compute if is not empty because it is use to exprt as lgc format...

                RecreateRounds(e.guidedModule);

                if (rounds.Count > 0)
                    if (rounds.Last().State == M.States.Types.Temporary) rounds.Remove(rounds.Last());

                List<Measure> Measures = st.MeasuresTaken.Cast<Measure>().ToList();
                if (Measures.Count > 0)
                {
                    Measure lastMeasure = Measures.Last();
                    if (Measures.Count > 1)
                    {
                        string lastRoundName = rounds.Count > 0 ? rounds.Last().Name : "";
                        int lastRoundNumber = rounds.Count > 0 ? rounds.Last().Number : 0;
                        if (lastRoundName != lastMeasure.Origin)
                        {
                            M.TheodoliteRound tempRound = new M.TheodoliteRound(Measures.FindAll(x => x.Origin == lastMeasure.Origin));
                            tempRound.State = M.States.Types.Temporary;
                            tempRound.Number = lastRoundNumber + 1;
                            (gm as TSU.Polar.GuidedModules.TdH).Rounds.Add(tempRound);
                        }
                    }
                }
                e.ActiveStationModule._MeasureManager.SelectableObjects.Clear();
                e.ActiveStationModule._MeasureManager.View.CheckBoxesVisible = true;
                s.Update();
            };

            s.Updating += delegate (object source, StepEventArgs e)
            {
                try
                {
                    HourGlass.Set();

                    List<Measure> MeasuresTaken = st.MeasuresTaken.Cast<Measure>().ToList();

                    // Show parity of faces
                    if (st.MeasuresToDo.Count > 0) // always have one to new itiretaion number...
                    {
                        if (MeasuresTaken.Count > 1)
                            e.step.View.ModifyLabel(string.Format(R.T_TdhFace1NotOk, MeasuresTaken.Last().Face), "state");
                        else
                            e.step.View.ModifyLabel(string.Format(R.T_TdhFace1NotOk, I.FaceType.UnknownFace), "state");
                        e.step.View.ModifyLabelColor(MMMM.Tsunami2.Preferences.Theme.Colors.Bad, "state");
                    }
                    else
                    {
                        e.step.View.ModifyLabel(string.Format(R.T_TdHFace1Ok, MeasuresTaken.Last().Face), "state");
                        e.step.View.ModifyLabelColor(MMMM.Tsunami2.Preferences.Theme.Colors.Good, "state");
                    }

                    // Closure OK?
                    M.TheodoliteRound last = rounds.LastOrDefault();
                    if (last != null)
                    {
                        if (last.ControlMeasure != null)
                        {
                            e.step.View.ModifyLabel(string.Format(""), "closure");
                            e.step.View.ModifyLabelColor(MMMM.Tsunami2.Preferences.Theme.Colors.Good, "closure");
                        }
                        else
                        {
                            e.step.View.ModifyLabel($"{R.T_CLOSURE_NEEDED}", "closure");
                            e.step.View.ModifyLabelColor(MMMM.Tsunami2.Preferences.Theme.Colors.Bad, "closure");
                        }
                    }

                    string listOfReduction;
                    M.TheodoliteRounds reductedRounds = null;
                    List<Measure> average = null;
                    List<string> listOfPointNamesToPickFrom = null;

                    try // do reduction
                    {
                        reductedRounds = Survey.ReductFaceRounds(rounds, true);
                        average = rounds.Average;
                        reductedRounds.ComputeAndFillSpreadsToAverage(average);
                        listOfReduction = Survey.CompareRoundsOfTdH(rounds, reductedRounds, average, out listOfPointNamesToPickFrom);
                    }
                    catch (Exception ex)
                    {
                        string titleAndMessage = $"{R.T_IMPOSSIBLE_TO_DO_THE_REDUCTION_FOR_THE_MOMENT};{R.T_YOU_MUST_MISS_SOME_MEASUREMENT}\r\n\r\n{ex.Message}";
                        new MessageInput(MessageType.Warning, titleAndMessage).Show();
                        listOfReduction = R.T_NOT_YET_AVAILABLE;
                    }


                    e.step.View.ModifyLabel(R.T_Tdh_resume + "\r\n" + listOfReduction, "resumé");

                    e.step.View.ModifyLabel($"{R.T_Tdh_RoundNumber}\r\n{R.T_FACE_1}: {rounds.OfFace1.Count} {R.T_ROUNDS}\r\n{R.T_FACE_2}: {rounds.OfFace2.Count} {R.T_ROUNDS}", "roundnumber");

                    if (rounds.EqualNumberOfFace)
                        e.step.View.ModifyLabelColor(MMMM.Tsunami2.Preferences.Theme.Colors.Good, "roundnumber");
                    else
                        e.step.View.ModifyLabelColor(MMMM.Tsunami2.Preferences.Theme.Colors.Bad, "roundnumber");

                    if (listOfPointNamesToPickFrom != null)
                        UpdateTdhGrid(e.step, rounds, reductedRounds, average, listOfPointNamesToPickFrom);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    HourGlass.Remove();
                }
            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {
            };

            s.Testing += delegate (object source, StepEventArgs e)
            {

            };

            s.Desappearing += delegate (object source, StepEventArgs e)
            {
                e.step.CheckValidityOf(st.MeasuresToDo.Count == 1);
            };


            #endregion

            return s;
        }



        private static void On_Grid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (dgv.Columns[e.ColumnIndex].Name.Contains("CC") || dgv.Columns[e.ColumnIndex].Name.Contains("mm"))
            {
                string value = e.Value as string;
                if (value != null)
                {
                    if (value.Length > 3)
                    {
                        if (value[2] == '!')
                            e.CellStyle.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.Bad;
                        else if (value[2] == ')')
                            e.CellStyle.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.Attention;
                        else
                            e.CellStyle.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.Good;
                    }
                    else
                        e.CellStyle.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.Good;
                }
            }
            if (dgv.Columns[e.ColumnIndex].Name.Contains("AH"))
            {
                e.CellStyle.BackColor = MMMM.Tsunami2.Preferences.Theme.Colors.GetVariation(MMMM.Tsunami2.Preferences.Theme.Colors.LightBackground, 1);
            }
            else if (dgv.Columns[e.ColumnIndex].Name.Contains("AV"))
            {
                e.CellStyle.BackColor = MMMM.Tsunami2.Preferences.Theme.Colors.GetVariation(MMMM.Tsunami2.Preferences.Theme.Colors.LightBackground, 2);
            }
            else if (dgv.Columns[e.ColumnIndex].Name.Contains("Di"))
            {
                e.CellStyle.BackColor = MMMM.Tsunami2.Preferences.Theme.Colors.GetVariation(MMMM.Tsunami2.Preferences.Theme.Colors.LightBackground, 3);
            }
            else
            {
                e.CellStyle.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            }
        }


        private static void UpdateTdhGrid(Step step, M.TheodoliteRounds rounds, M.TheodoliteRounds reductedRounds,
        List<Measure> average, List<string> listOfPointNamesToPickFrom)
        {
            var grid = step.GuidedModule.View.grid;

            DataTable table = new DataTable();
            table.Columns.Add("Pos.");
            table.Columns.Add("Point");

            List<List<string>> tempStorage = new List<List<string>>();

            { // add columns
                table.Columns.Add($"{R.T_AVERAGE}\r\nAH\r\n[gon]");
                table.Columns.Add($"{R.T_AVERAGE}\r\nAV\r\n[gon]");
                table.Columns.Add($"{R.T_AVERAGE}\r\nDi\r\n[m]");
                foreach (var item in rounds)
                {
                    table.Columns.Add("Round " + item.Number + "\r\nAH\r\n[gon]");
                    table.Columns.Add("R" + item.Number + "\r\ndAH\r\n[CC]");
                    table.Columns.Add("R" + item.Number + "\r\nAV\r\n[gon]");
                    table.Columns.Add("R" + item.Number + "\r\ndAV\r\n[CC]");
                    table.Columns.Add("R" + item.Number + "\r\nDi\r\n[m]");
                    table.Columns.Add("R" + item.Number + "\r\ndDi\r\n[mm]");
                }
            }  // add columns

            for (int i = 0; i < listOfPointNamesToPickFrom.Count; i++)
            {
                AddRow(table, i, average, reductedRounds, listOfPointNamesToPickFrom[i], grid.Tag as List<I.SigmaForAInstrumentCouple>);
            }
            AddClosureRows(table, average, reductedRounds, (step.GuidedModule._ActiveStationModule._Station as Station).Parameters2.Tolerance);

            table.TableName = "Table";
            DataSet ds = new DataSet();
            ds.Tables.Add(table);
            grid.DataSource = ds;
            grid.DataMember = "Table";
            grid.Height = grid.Rows.GetRowsHeight(DataGridViewElementStates.Visible) + grid.ColumnHeadersHeight * 2;

            grid.Update();
        }

        private static void AddRow(DataTable table, int i, List<Measure> averages,
            M.TheodoliteRounds reductedRounds, string pointName, List<I.SigmaForAInstrumentCouple> scs)
        {
            int precA =TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;
            int precD =TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
            List<string> fields = new List<string>();

            var ave = averages.Find(x => x._Point._Name == pointName && x._Status is M.States.Good);

            fields.Add(i.ToString());
            fields.Add(pointName);
            fields.Add(ave.Angles.Corrected.Horizontal.ToString(precA));
            fields.Add(ave.Angles.Corrected.Vertical.ToString(precA));
            fields.Add(ave.Distance.Corrected.ToString(precD));

            foreach (var item in reductedRounds)
            {
                var mes = item.Measures.Find(x => x._Point._Name == pointName && x._Status is M.States.Good);
                var spread = item.Spreads.Find(x => x._Point._Name == pointName && x._Status is M.States.Good);

                I.SigmaForAInstrumentCouple sc = scs.Find(x => x.Target == mes.Distance.Reflector._InstrumentType.ToString());
                if (sc == null) sc = new I.SigmaForAInstrumentCouple();

                //AH
                fields.Add(mes.Angles.Corrected.Horizontal.ToString(precA));
                fields.Add(GetSpread(spread.Angles.Corrected.Horizontal, precA, 4, 10000, sc.sigmaAnglCc));

                //AV
                fields.Add(mes.Angles.Corrected.Vertical.ToString(precA));
                fields.Add(GetSpread(spread.Angles.Corrected.Vertical, precA, 4, 10000, sc.sigmaZenDCc));

                //Di
                fields.Add(mes.Distance.Corrected.ToString(precD));
                fields.Add(GetSpread(spread.Distance.Corrected, precD, 3, 1000, sc.sigmaDistMm));
            }

            table.Rows.Add(fields.ToArray());
        }

        private static string GetSpread(DoubleValue observation, int prec, int digit, int factor, double sigma)
        {
            string s;
            if (Math.Abs(observation.Value) > 2 * sigma / factor)
                s = "(!!) ";
            else if (Math.Abs(observation.Value) > sigma / factor)
                s = "(!) ";
            else
                s = "";

            return s + observation.ToString(prec - digit, factor);
        }

        private static void AddClosureRows(DataTable table, List<Measure> averages, M.TheodoliteRounds reductedRounds, I.InstrumentTolerance tol)
        {
            int precA =TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;
            int precD =TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;


            List<string> fields = new List<string>();
            List<string> fieldsDiff = new List<string>();

            fields.Add((averages.Count - 1).ToString());
            fields.Add(averages.Last()._PointName + "(Closure)");
            fields.Add(averages.Last().Angles.Corrected.Horizontal.ToString(precA));
            fields.Add(averages.Last().Angles.Corrected.Vertical.ToString(precA));
            fields.Add(averages.Last().Distance.Corrected.ToString(precD));


            fieldsDiff.Add(averages.Count.ToString());
            fieldsDiff.Add("Closure: Last-First");
            fieldsDiff.Add("");
            fieldsDiff.Add("");
            fieldsDiff.Add("");

            foreach (var item in reductedRounds)
            {
                //ah
                fields.Add(item.Measures.Last().Angles.Corrected.Horizontal.ToString(precA));
                fields.Add(GetSpread(item.Spreads.Last().Angles.Corrected.Horizontal, precA, 4, 10000, tol.Closure.H_CC));

                //av
                fields.Add(item.Measures.Last().Angles.Corrected.Vertical.ToString(precA));
                fields.Add(GetSpread(item.Spreads.Last().Angles.Corrected.Vertical, precA, 4, 10000, tol.Closure.V_CC));

                //di
                fields.Add(item.Measures.Last().Distance.Corrected.ToString(precD));
                fields.Add(GetSpread(item.Spreads.Last().Distance.Corrected, precD, 3, 1000, tol.Closure.D_mm));

                //diff ah
                fieldsDiff.Add((item.Measures.Last().Angles.Corrected.Horizontal - item.Measures.First().Angles.Corrected.Horizontal).ToString(precA));
                fieldsDiff.Add((item.Measures.Last().Angles.Corrected.Horizontal - item.Measures.First().Angles.Corrected.Horizontal).ToString(precA - 4, 10000));

                //diff av
                fieldsDiff.Add((item.Measures.Last().Angles.Corrected.Vertical - item.Measures.First().Angles.Corrected.Vertical).ToString(precA));
                fieldsDiff.Add((item.Measures.Last().Angles.Corrected.Vertical - item.Measures.First().Angles.Corrected.Vertical).ToString(precA - 4, 10000));

                //diff di
                fieldsDiff.Add((item.Measures.Last().Distance.Corrected - averages.First().Distance.Corrected).ToString(precD));
                fieldsDiff.Add((item.Measures.Last().Distance.Corrected - item.Measures.First().Distance.Corrected).ToString(precD - 3, 1000));
            }

            table.Rows.Add(fields.ToArray());
            table.Rows.Add(fieldsDiff.ToArray());
        }

        internal static Step TdHReductionAndExport(Common.Guided.Module gm)
        {
            Step s = new Step(gm, R.T_TH_Export);

            Station.Module stm = s.GuidedModule._ActiveStationModule as Station.Module;
            Common.Station st = stm.StationTheodolite;
            Station.View v = stm.View;
            List<M.Measure> measuresToExport = st.MeasuresToExport;

            M.TheodoliteRounds rounds = (gm as TdH).Rounds;

            Action PrepareMeasureToExport = () =>
            {
                measuresToExport.Clear();
                rounds.GoodOnes.ForEach(x => { measuresToExport.AddRange(x.GoodMeasures); measuresToExport.AddRange(x.MeasuresOfControl); });
            };

            Action ReductFaceRounds = () =>
            {
                measuresToExport.Clear();
                rounds.GoodOnes.ForEach(x => { measuresToExport.AddRange(x.ReductedMeasures); });
            };

            Action ReductFaceRoundsAndAverageThem = () =>
            {
                measuresToExport.Clear();
                if (rounds.Count == 0)
                {
                    string titleAndMessage = $"{R.T_NO_AVERAGES_AVAILABLE};{R.T_ROUND_AVERAGES_ARE_COMPUTED_IN_THE_PREVIOUS_STEP}";
                    new MessageInput(MessageType.Warning, titleAndMessage).Show();
                }

                measuresToExport.AddRange(rounds.Average);

                // rename comment
                measuresToExport.ForEach(x =>
                {
                    int plusPosition = x.CommentFromTsunami.IndexOf('+');
                    if (plusPosition != -1)
                        x.CommentFromTsunami = R.T_REDUCTED_AND_AVERAGED + " - " + x.CommentFromTsunami.Substring(plusPosition + 2);
                    else
                        x.CommentFromTsunami = R.T_REDUCTED_AND_AVERAGED;
                });
            };

            #region Step Events

            #region FirstUse

            s.FirstUse += delegate (object source, StepEventArgs e)
            {

                #region simple

                // in simple mode we leave the measure as they were taken
                BigButton simpleGeode = new BigButton(string.Format(R.T_Tdh_ExportSimple, " Geode"), R.Geode,TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                simpleGeode.BigButtonClicked += delegate
                {
                    try
                    {
                        PrepareMeasureToExport();
                        if (stm._MeasureManager.ExportToGeode())
                            s.View.ModifyLabel($"\r\n{R.T_RAW_MEASURES} {R.T_EXPORTED} {R.T_FOR} Geode ({DateTime.Now.ToString("hh:mm:ss")})", "save", addToExistingText: true);
                    }
                    catch (Exception ex)
                    {
                        string titleAndMessage = $"{R.T_FAILED};{ex.Message}";
                        new MessageInput(MessageType.Critical, titleAndMessage).Show();
                    }
                };
                BigButton simpleLGC2 = new BigButton(string.Format(R.T_Tdh_ExportSimple, " LGC2"), R.Lgc,TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                simpleLGC2.BigButtonClicked += delegate
                {
                    try
                    {
                        PrepareMeasureToExport();
                        if (e.guidedModule.ExportToLGC2())
                            s.View.ModifyLabel($"\r\n{R.T_RAW_MEASURES} {R.T_EXPORTED} {R.T_FOR} LGC2 ({DateTime.Now.ToString("hh:mm:ss")})", "save", addToExistingText: true);
                    }
                    catch (Exception ex)
                    {
                        string titleAndMessage = $"{R.T_FAILED};{ex.Message}";
                        new MessageInput(MessageType.Critical, titleAndMessage).Show();
                    }
                };

                #endregion FirstUse

                #region reducted

                // in reducted mode we reducte each round separately
                BigButton reductedGeode = new BigButton(string.Format(R.T_Tdh_ExportReduit, "Geode"), R.Geode,TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                reductedGeode.BigButtonClicked += delegate
                {
                    try
                    {
                        ReductFaceRounds();
                        // if (stm._MeasureManager.ExportToGeode())
                        if (IO.SUSoft.Geode.ToGeode(st, out _))
                            s.View.ModifyLabel($"\r\n{R.T_REDUCTED_ROUNDS} {R.T_EXPORTED} {R.T_FOR} Geode ({DateTime.Now.ToString("hh:mm:ss")})", "save", addToExistingText: true);
                    }
                    catch (Exception ex)
                    {
                        string titleAndMessage = $"{R.T_FAILED};{ex.Message}";
                        new MessageInput(MessageType.Critical, titleAndMessage).Show();
                    }
                };
                BigButton reductedLGC2 = new BigButton(string.Format(R.T_Tdh_ExportReduit, "LGC2"), R.Lgc,TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                reductedLGC2.BigButtonClicked += delegate
                {
                    try
                    {
                        ReductFaceRounds();
                        if (e.guidedModule.ExportToLGC2())
                            s.View.ModifyLabel($"\r\n{R.T_REDUCTED_ROUNDS} {R.T_EXPORTED} {R.T_FOR} LGC2 ({DateTime.Now.ToString("hh:mm:ss")})", "save", addToExistingText: true);
                    }
                    catch (Exception ex)
                    {
                        string titleAndMessage = $"{R.T_FAILED};{ex.Message}";
                        new MessageInput(MessageType.Critical, titleAndMessage).Show();
                    }
                };

                #endregion 

                #region averaged

                // in averaged mode we reducte each round separately then we average F1s and F2s then we average the 2 circles
                BigButton averagedGeode = new BigButton(string.Format(R.T_Tdh_ExportAverageReduit, "Geode"), R.Geode);
                averagedGeode.BigButtonClicked += delegate
                {
                    try
                    {
                        ReductFaceRoundsAndAverageThem();
                        if (stm._MeasureManager.ExportToGeode())
                            s.View.ModifyLabel($"\r\n{R.T_AVERAGED_REDUCTIONS} {R.T_EXPORTED} {R.T_FOR} Geode ({DateTime.Now.ToString("hh:mm:ss")})", "save", addToExistingText: true);
                    }
                    catch (Exception ex)
                    {
                        string titleAndMessage = $"{R.T_FAILED};{ex.Message}";
                        new MessageInput(MessageType.Critical, titleAndMessage).Show();
                    }
                };
                BigButton averagedLGC2 = new BigButton(string.Format(R.T_Tdh_ExportAverageReduit, "LGC2"), R.Lgc);
                averagedLGC2.BigButtonClicked += delegate
                {
                    try
                    {
                        ReductFaceRoundsAndAverageThem();
                        if (e.guidedModule.ExportToLGC2())
                            s.View.ModifyLabel($"\r\n{R.T_AVERAGED_REDUCTIONS} {R.T_EXPORTED} {R.T_FOR} LGC2 ({DateTime.Now.ToString("hh:mm:ss")})", "save", addToExistingText: true);
                    }
                    catch (Exception ex)
                    {
                        string titleAndMessage = $"{R.T_FAILED};{ex.Message}";
                        new MessageInput(MessageType.Critical, titleAndMessage).Show();
                    }
                };

                #endregion 

                BigButton otherKinds = new BigButton($"{R.t_Others};{R.T_TDH_OTHERS_EXPORT}", R.Export,TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                otherKinds.BigButtonClicked += delegate
                {
                    try
                    {
                        TsuPopUpMenu m = new TsuPopUpMenu(e.step.View, "export ");
                        List<Control> list = new List<Control>();
                        list.Add(simpleGeode);
                        list.Add(reductedGeode);
                        list.Add(simpleLGC2);
                        list.Add(reductedLGC2);
                        m.ShowSubMenu(list, "others");
                    }
                    catch (Exception ex)
                    {
                        string titleAndMessage = $"{R.T_FAILED};{ex.Message}";
                        new MessageInput(MessageType.Critical, titleAndMessage).Show();
                    }
                };

                s.View.AddSidedControls(new List<Control>() { averagedGeode, averagedLGC2, otherKinds }, DockStyle.Top);
                s.View.ModifyLabel("", "mes");
                s.View.ModifyLabel("", "save", MMMM.Tsunami2.Preferences.Theme.Colors.Good);
            };

            #endregion

            #region Entering

            s.Entering += delegate (object source, StepEventArgs e)
            {
                string t = "";
                s.View.ModifyLabel(t, "mes");

                // validate ingoing tempoprary round
                M.TheodoliteRound lastValidRound = rounds.LastValidRound;
                if (lastValidRound != null)
                {
                    lastValidRound.State = M.States.Types.Good;
                }
            };
            #endregion

            #region Updating

            s.Updating += delegate (object source, StepEventArgs e)
            {

            };

            #endregion

            #region Leaving

            s.Leaving += delegate (object source, StepEventArgs e)
            {

            };

            #endregion

            #region Testing

            s.Testing += delegate (object source, StepEventArgs e)
            {

            };

            #endregion

            #region Desappearing

            s.Desappearing += delegate (object source, StepEventArgs e)
            {
            };

            #endregion

            #endregion

            return s;
        }


    }
}

namespace TSU.Polar.GuidedModules
{
    public class TdH : Common.Guided.Module
    {
        public override Common.ObservationType ObservationType { get; set; } = Common.ObservationType.Polar;
        public List<M.States.Types> RoundState { get; set; } = new List<M.States.Types>();

        public M.TheodoliteRounds Rounds { get; } = new M.TheodoliteRounds();

        public TdH() : base()
        { }

        public TdH(MMMM.Module parentModule, string name) : base(parentModule, name)
        { }

        public void UpdateRounds(int roundNumber, M.States.Types newState)
        {
            if (roundNumber > Rounds.Count) throw new Exception(string.Format(R.T_INVALIDE_ROUND_NUMBER_IS_OUT_OF_RANGE, roundNumber, Rounds.Count));
            if (Rounds[roundNumber].State != newState)
            {
                Rounds[roundNumber].State = newState;
                RoundState[roundNumber] = newState;
            }
        }

    }
}
