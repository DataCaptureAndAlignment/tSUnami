﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSU.Common.Instruments.Reflector;
using M = TSU.Common.Measures;
using R = TSU.Properties.Resources;
using P = TSU.Polar;
using TSU.Common.Measures;
using I = TSU.Common.Instruments;
using System.ServiceModel.Activities.Configuration;
using TSU.Common.Measures.States;
using Castle.Core.Internal;
using TSU.Common.Instruments;
using TSU.Common.Guided.Steps;
using TSU.Views;
using TSU.Views.Message;
using static TSU.Tilt.Smart.ViewModel;

namespace TSU.Polar.GuidedModules.Steps
{
    internal class Setup_MeasureAllRefs : Step
    {
        DataGridView dataGridView;
        bool flag_MeasureInProgress;

        public Setup_MeasureAllRefs(Common.Guided.Module guidedModule)
            :base (guidedModule, "Measure Reference Points;Click on the point in the list to measure it next")
        {
            this.Id = "MeasureAllRefInOneStep";
            this.FirstUse += On_Step_FirstUse;
            this.Entering += On_Step_Entering;
            this.Leaving += On_Step_Leaving;
            this.Changed += On_Step_Changed;
            this.Testing += On_Step_Testing;
        }

        static List<ShowableAndEditablePolarMeasure> measuresToShow = null;

        

        private void Subscribe(Station.Module stationModule, bool remove=false)
        {
            I.Module imm = stationModule._InstrumentManager.SelectedInstrumentModule;
            stationModule.MeasureThreated -= On_Measurement_Threated;
            if (imm != null)
            {
                imm.StartingMeasurement -= On_Measurement_Started;
                imm.FinishingMeasurement -= On_Measurement_Finished;
                imm.MeasurementFailed -= On_Measurement_Failed;
                imm.MeasurementCancelled -= On_Measurement_Failed;
            }
            if (remove) return;
            stationModule.MeasureThreated += On_Measurement_Threated;
            if (imm != null)
            {
                imm.StartingMeasurement += On_Measurement_Started;
                imm.FinishingMeasurement += On_Measurement_Finished;
                imm.MeasurementFailed += On_Measurement_Failed;
                imm.MeasurementCancelled += On_Measurement_Failed;
            }
        }

        private void On_Measurement_Finished(object sender, MeasurementEventArgs e)
        {
            flag_MeasureInProgress = false;
        }

        private void On_Measurement_Failed(object sender, MeasurementEventArgs e)
        {
            flag_MeasureInProgress = false;
            DGV_HighLightMeasure(dataGridView, e.BeingMeasured as Polar.Measure,TSU.Tsunami2.Preferences.Theme.Colors.Bad);
        }

        private void On_Measurement_Started(object sender, MeasurementEventArgs e)
        {
            flag_MeasureInProgress = true;
            ShowProgressBar(sender, e);
            

        }

        private void ShowProgressBar(object sender, MeasurementEventArgs e)
        {
            if (!flag_InstrumentViewVisible) // we steal the progressbar if is not visible already
            {

                var stm = this.GuidedModule._ActiveStationModule as Polar.Station.Module;
                var view = stm._InstrumentManager.SelectedInstrumentModule.View;
                var waitingForm = view.WaitingForm;
                if (waitingForm != null)
                {
                    waitingForm.CoveredView = this.View;
                    waitingForm.CancelAction = () => { view.CancelAction(); };
                    waitingForm.MessageView.ParentView = this.View;
                    waitingForm.MessageView.CoverParentView(0);
                    waitingForm.Show();
                }
            }
        }

        private void On_Measurement_Threated(object sender, M.MeasurementEventArgs e)
        {
            DGV_HighLightMeasure(dataGridView, e.Measure as Polar.Measure,TSU.Tsunami2.Preferences.Theme.Colors.Good);
            if (measuresToShow == null) return;
            foreach (var item in measuresToShow)
            {
                if (e.Measure._Point._Name == item.Name)
                {
                    item.Done = false;
                    item.Done = true;
                }
            }
            this.Change();
        }

        private void On_Step_Changed(object sender, StepEventArgs e)
        {
            if (measuresToShow != null)
            {
                foreach (var measureToShow in measuresToShow)
                {
                    if (measureToShow.Done)
                    {
                        DGV_HighLightMeasure(dataGridView, measureToShow.polarMeasure, TSU.Tsunami2.Preferences.Theme.Colors.Good);
                    }
                }
            }
            
        }
        private void On_Step_FirstUse(object sender, StepEventArgs e)
        {
            Station.Module stationModule = e.ActiveStationModule as Station.Module;
            Station.View StationView = e.ActiveStationModule._TsuView as Station.View;
            var measures = stationModule.CreateMeasuresFromElement(
                    Tools.Conversions.Points.List2Composite(e.step.GuidedModule.PointsToBeMeasured), new TSU.Common.Measures.States.Unknown(), silent: true);
            
            measuresToShow = DGV_CreateDataModel(e.ActiveStation as Station, measures);
            e.step.Tag = measuresToShow;
        }

        private void On_Step_Entering(object sender, StepEventArgs e)
        {
            On_Step_FirstUse(sender, e);
            Station.Module stationModule = e.ActiveStationModule as Station.Module;
            stationModule.StationTheodolite.ClearMeasuresToDo();

            this.dataGridView = DGV_Create(stationModule, measuresToShow);
            e.step.View.AddGrid(this.dataGridView, DockStyle.Fill);

            if (measuresToShow != null)
            {
                foreach (var measureToShow in measuresToShow)
                {
                    foreach (var measureTaken in stationModule.StationTheodolite.MeasuresTaken)
                    {
                        if (measureToShow.Name == measureTaken._Point._Name)
                            if (measureTaken._Status.Type != M.States.Types.Bad)
                            {
                                measureToShow.Done = true;
                                break;
                            }
                    }
                }
            }
            Subscribe(stationModule, remove: false);
        }

        private void On_Step_Leaving(object sender, StepEventArgs e)
        {
            Station.Module stationModule = e.ActiveStationModule as Station.Module;
            Subscribe(stationModule, remove: true);
            flag_MeasureInProgress = false;
        }

        private void On_Step_Testing(object sender, StepEventArgs e)
        {
            Station.Module stationModule = e.ActiveStationModule as Station.Module;
            if (stationModule == null) return;
            var positionKnown = (e.step.GuidedModule._ActiveStationModule._Station as Polar.Station).Parameters2.Setups.InitialValues.IsPositionKnown;
            var minimimNumberofRefTomeasure = positionKnown ? 1 : 2;
            e.step.CheckValidityOf(e.ElementManager._SelectedObjects.Count >= minimimNumberofRefTomeasure);
            bool enoughMeasured = stationModule.StationTheodolite.GoodMeasuresTaken.Count >= minimimNumberofRefTomeasure;

            MessageFromTest = e.step.CheckValidityOf(enoughMeasured) ? "" : $"{minimimNumberofRefTomeasure}  {R.T_GOOD_MEASURES_ARE_NEEDED}";
        }


        private  DataGridView DGV_Create(Station.Module stm, object measuresToShow)
        {
            DataGridView dgv = new DataGridView();
            ((System.ComponentModel.ISupportInitialize)(dgv)).BeginInit();
            TSU.Tsunami2.Preferences.Theme.ApplyTo(dgv);

            dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            dgv.ReadOnly = false;
            dgv.SelectionMode = DataGridViewSelectionMode.CellSelect;
            dgv.AutoGenerateColumns = true;
            dgv.MultiSelect = false;
            dgv.DataSource = measuresToShow;
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            DGV_AddButtons(dgv, new List<string>() { "Goto", "Measure", "G+M", "More" });
            dgv.CellBeginEdit += On_DGV_CellBeginEdit;
            dgv.CellClick += On_DGV_ButtonClicked;
            dgv.ColumnHeaderMouseDoubleClick += delegate { this.flag_MeasureInProgress = false; };
            dgv.Tag = stm;
            ((System.ComponentModel.ISupportInitialize)(dgv)).EndInit();

            return dgv;
        }

        private  void On_DGV_ButtonClicked(object sender, DataGridViewCellEventArgs e)
        {
            if (flag_MeasureInProgress)
            {
                new MessageInput(MessageType.Warning, "Measure in progress").Show();
                return;
            }

            if (e.RowIndex == -1 || e.ColumnIndex == -1)
                return;

            var dgv = sender as DataGridView;
            var column = dgv.Columns[e.ColumnIndex];
            var stm = dgv.Tag as Station.Module;
            var row = dgv.Rows[e.RowIndex];
            var meas = row.DataBoundItem as ShowableAndEditablePolarMeasure;
            bool gotoIsPossible = stm.StationTheodolite.Parameters2.Setups.TemporaryValues !=null && stm.StationTheodolite.Parameters2.Setups.TemporaryValues.Ready && meas.polarMeasure._OriginalPoint._Coordinates.HasAny;

            if (column.Name == "Goto")
            {
                if (!gotoIsPossible)
                {
                    new MessageInput(MessageType.Warning, "Goto not available").Show();
                    return;
                }
                stm.StationTheodolite.ClearMeasuresToDo();
                (stm._InstrumentManager.SelectedInstrumentModule as PolarModule).GotoWanted=true;
                //meas.polarMeasure.GotoWanted = true;
                meas.polarMeasure.DirectMeasurementWanted = false;
                stm.Goto(meas.polarMeasure);
            }
            else if (column.Name == "Measure")
            {
                stm.StationTheodolite.ClearMeasuresToDo();
                DGV_HighLightMeasure(dgv, meas.polarMeasure, System.Drawing.Color.Purple);
                (stm._InstrumentManager.SelectedInstrumentModule as PolarModule).GotoWanted = false;
                stm.MeasureNow(meas.polarMeasure);
            }
            else if (column.Name == "G+M")
            {
                if (!gotoIsPossible)
                {
                    new MessageInput(MessageType.Warning, "Goto not available").Show();
                    return;
                }
                DGV_HighLightMeasure(dgv, meas.polarMeasure, System.Drawing.Color.Purple);
                (stm._InstrumentManager.SelectedInstrumentModule as PolarModule).GotoWanted = true;
                stm.MeasureNow(meas.polarMeasure);
            }
            else if (column.Name == "More")
            {
                flag_InstrumentViewVisible = !flag_InstrumentViewVisible;
                var view = stm._InstrumentManager.SelectedInstrumentModule.View;
                if (flag_InstrumentViewVisible)
                {
                    var test = this.View.Parent.Parent.Parent;
                    var splitcon = this.View.Parent.Parent as SplitContainer;
                    var p2 = splitcon.Panel1.Controls[0].GetType();


                    meas.polarMeasure.DirectMeasurementWanted = false;
                    if (meas.polarMeasure._Status.Type == M.States.Types.Good)
                        meas.polarMeasure._Status = new Questionnable();
                    stm.SendMeasureToInstrument(meas.polarMeasure);
                    view.AutoSize = false; // Disable AutoSize since Dock will handle the layout
                    view.Dock = DockStyle.Bottom; // Ensure it is docked to the bottom
                    view.Width = this.View.Width;
                    view.Height = 300; // Manually set height
                    this.View.Controls.Add(view); // Add to controls
                    this.View.AutoScroll = true;
                    view.Refresh();
                    view.BringToFront();
                }
                else
                {
                    this.View.Controls.Remove(view);
                }
            }
        }
        bool flag_InstrumentViewVisible = false;

        private void DGV_HighLightMeasure(DataGridView dgv, Polar.Measure meas, System.Drawing.Color color)
        {
            if (meas == null) return;
            foreach (DataGridViewRow row in dgv.Rows)
            {
                if ((row.DataBoundItem as ShowableAndEditablePolarMeasure).polarMeasure._Point._Name == meas._Point._Name)
                {
                    row.DefaultCellStyle.BackColor = color;
                }
            }
        }

        private  void On_DGV_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            var dgv = sender as DataGridView;
            var column = dgv.Columns[e.ColumnIndex];
            if (column.Name == "Reflector")
            {
                var row = dgv.Rows[e.RowIndex];
                var cell = dgv.Rows[e.RowIndex].Cells[e.ColumnIndex];
                var stm = dgv.Tag as Station.Module;
                var reflector = stm.reflectorManager.SelectInstrument(R.T153, selectables: null, multiSelection: false, preselected: null);
                var meas = row.DataBoundItem as ShowableAndEditablePolarMeasure;
                meas.polarMeasure.Reflector = reflector as Reflector;
            }
            if (column.Name == "Code")
            {
                var row = dgv.Rows[e.RowIndex];
                var cell = dgv.Rows[e.RowIndex].Cells[e.ColumnIndex];
                var stm = dgv.Tag as Station.Module;
                var meas = row.DataBoundItem as ShowableAndEditablePolarMeasure;
                stm.ChangeCode(meas.polarMeasure._Point.SocketCode, meas.polarMeasure);
            }
        }

        private  void DGV_AddButtons(DataGridView dgv, List<string> buttonTexts)
        {
            buttonTexts.Reverse();
            foreach (var item in buttonTexts)
            {
                if (dgv.Columns["uninstall_column"] == null)
                {
                    var dgvbc = new DataGridViewButtonColumn()
                    {
                        Text = item,
                        Name = item,
                        UseColumnTextForButtonValue = true,
                        AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells,
                    };
                    dgvbc.DefaultCellStyle.Padding = new Padding(5);
                    dgv.Columns.Insert(0, dgvbc);
                }
            }
        }

        private  List<ShowableAndEditablePolarMeasure> DGV_CreateDataModel(Station station, List<Polar.Measure> referencesToMeasure)
        {
            List<ShowableAndEditablePolarMeasure> measuresToShow = new List<ShowableAndEditablePolarMeasure>();
            foreach (var item in referencesToMeasure)
            {
                measuresToShow.Add(new ShowableAndEditablePolarMeasure(item as P.Measure));
            }
            return measuresToShow;
        }
    }
}
