﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TSU.Common;
using TSU.Common.Compute.Compensations.BeamOffsets;
using C = TSU.Common.Compute;
using TSU.Common.Elements;
using TSU.Common.Guided.Steps;
using TSU.Measurement.Common.Guided.Step;
using TSU.Views;
using TSU.Views.Message;
using static System.Net.Mime.MediaTypeNames;
using E = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Polar.GuidedModules.Steps
{
    public static class StakeOut
    {
        public enum DisplayTypes
        {
            Displacement, Coordinates, Offsets, Observations, Precisions
        }


        public static Step ShowMeasurements(Common.Guided.Module gm)
        {
            Step s = Management.ShowMeasurements(gm);
            s.Testing += null;

            s.FirstUse += ShowMeasurement_FirstUse;
            s.Entering += ShowMeasurement_Entering;
            s.Testing += ShowMeasurement_Testing;
            return s;
        }

        private static void ShowMeasurement_Testing(object sender, StepEventArgs e)
        {
            if (!e.step.CheckValidityOf(e.step.State == StepState.Valid))
                e.step.MessageFromTest = "Not all measures are validated";
        }

        public static Step StationDefaultMeasurementParameters(Common.Guided.Module gm)
        {
            Step s = Setup.StationDefaultMeasurementParameters(gm);

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                Measure dm = (e.guidedModule._ActiveStationModule._Station as Station).Parameters2.DefaultMeasure;

                e.step.View.AddSidedControls(new List<Control>()
                {
                 e.step.View.GetPreparedLabel(R.T_MEASUREMENT_SET_TO, ""),
                    e.step.View.GetPreparedLabel(dm.Face.ToString(), "face"),
                    new BigButton
                    (
                    //    "Switch between 1 face and double face measurement",
                        R.T_SWITCH_BETWEEN_1_FACE_AND_DOUBLE_FACE_MEASUREMENT,
                        R.Face,
                        () =>
                        {
                            (gm._ActiveStationModule as Station.Module).FaceMeasurementHasBeenManuallySet=true;
                            if (dm.Face == I.FaceType.DoubleFace)
                                dm.Face = I.FaceType.Face1;
                            else
                                dm.Face = I.FaceType.DoubleFace;
                            e.step.View.ModifyLabel(dm.Face.ToString(), "face");
                        },
                        color: null,
                        hasSubButtons: false
                    )
                }, DockStyle.Top);
            };
            return s;
        }

        private static void ShowMeasurement_FirstUse(object sender, StepEventArgs e)
        {
            // e.step.View.ModifyLabel("Questionnable measures have to be turn into 'good' state to be exported in the next step, Please use the 'Validate' button if all offset where acceptable","util");
            e.step.View.ModifyLabel(R.T_QUESTIONNABLE_MEASURES_HAVE_TO_BE_TURN_INTO_GOOD_STATE, "util");
            // e.step.View.ModifyLabel("All measures seem to be saved.", "savingState");
            e.step.View.ModifyLabel(R.T_ALL_MEASURES_SEEM_TO_BE_SAVED, "savingState");

            // Add Button to validate the questionnable to good
            BigButton validate = new BigButton(
                //   "Validate the pre-alginement;This will turn last 'questionnable' measure of each point into a 'good' state so that it can be exported in the next step",
                $"{R.T_VALIDATE_THE_PRE_ALGINEMENT};{R.T_THIS_WILL_TURN_LAST_QUESTIONNABLE_MEASURE_OF_EACH_POINT_INTO_A_GOOD_STATE}",
                R.Corrected,
                () =>
                {
                    Tools.SaveLastmeasurements(
                        e.guidedModule,
                        e.ActiveStation as Common.Station,
                        e.ActiveStationModule as Station.Module, exportTogeode: false);
                    e.step.Enter();
                },
                color: null,
                hasSubButtons: false);
            e.step.View.AddButton(validate, "_Valid", dockStyle: DockStyle.Top);
            validate.SendToBack();
        }

        private static void ShowMeasurement_Entering(object sender, StepEventArgs e)
        {
            string message = "";
            Measure m;
            Color color = TSU.Tsunami2.Preferences.Theme.Colors.Object;

            // cleaning measurement, to make sure they are not severall time present with the same date //TSU-2381
            List<M.Measure> l = e.guidedModule._ActiveStationModule._Station.MeasuresTaken;
            List<int> indexesToRemove = new List<int>();
            for (int i = 1; i < l.Count; i++)
            {
                if (l[i]._Date == l[i - 1]._Date)
                {
                    if (l[i]._Status is M.States.Good)
                        indexesToRemove.Add(i - 1);
                }
            }
            foreach (var item in indexesToRemove.Reverse<int>())
            {
                l.RemoveAt(item);
                e.ActiveStationModule._MeasureManager.AllElements.RemoveAt(item);
            }

            e.step.State = StepState.Valid;
            foreach (var item in e.guidedModule.PointsToBeAligned)
            {
                message += item._Name + ": ";
                m = Tools.FindLastMeasureOf(item, e.guidedModule._ActiveStationModule._Station.MeasuresTaken);
                if (m == null)
                    //    message += "not measured";
                    message += R.T_NOT_MEASURED;
                else
                {
                    if (m._Status is M.States.Questionnable)
                    {
                        //   message += "need to be validate";
                        message += R.T_NEED_TO_BE_VALIDATE;
                        color = TSU.Tsunami2.Preferences.Theme.Colors.Attention;
                        e.step.State = StepState.NotReady;
                    }
                    else if (m._Status is M.States.Good)
                    {
                        //     message += "is validate";
                        message += R.T_IS_VALIDATED;
                    }
                    else
                    {
                        //   message += "need to be remeasured";
                        message += R.T_NEED_TO_BE_REMEASURED;

                        color = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
                        e.step.State = StepState.NotReady;
                    }
                }
                message += "\r\n";
            }
            e.step.View.ModifyLabel(message, "savingState");
            e.step.View.ModifyLabelColor(color, "savingState");
        }

        private static void ShowMeasurement_Update(object sender, StepEventArgs e)
        {
        }

        /// <summary>
        /// Choose points from theorietcal file, it uses a modified "choose elements" step 
        /// </summary>
        /// <param name="gm"></param>
        /// <returns></returns>
        public static Step ChooseMagnetToPreAlign(Common.Guided.Module gm)
        {
            Step s = Management.ChooseElement(gm);

            s.Id = "ChooseElement";

            s._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringAlignment_SelectElement);
            s.Utility = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringAlignment_SelectElement);

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.ElementManager._SelectedObjects.Clear();
                e.ElementManager.MultiSelection = true;
                e.ElementManager.View.CheckBoxesVisible = true;
                e.ElementManager.SetSelectableToAllMagnets();
                e.step.View.AddControl(e.guidedModule._ElementManager);
                e.ElementManager._SelectedObjectInBlue = null;
                e.ElementManager.AddSelectedObjects(e.guidedModule.MagnetsToBeAligned.Cast<TsuObject>().ToList());
                e.ElementManager.UpdateView();
            };

            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                e.step.CheckValidityOf(e.ElementManager.SelectableObjects.Count > 0);
                e.step.CheckValidityOf(e.ElementManager._SelectedObjects.Count > 0);
            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {
                e.step.View.RemoveControl(e.ElementManager);

                ResetWhatToAlign(gm);

                // check serial numbers
                {
                    e.step.Cancel = false;
                    foreach (var item in gm.PointsToBeAligned)
                    {
                        if (!ElementWithvalidatedSerialNumber.Contains(item))
                        {
                            if (e.ActiveStation.MeasuresToDo.Find(x => x._Point._Name == item._Name) == null)
                            {
                                if (item.SerialNumber != "" && item._Point == "E")
                                {
                                    string serailNumberWithoutLast4Chars = item.SerialNumber.Substring(0, item.SerialNumber.Length - 4);
                                    string titleAndMessage = string.Format("{1} {0}?", R.T_M_SERIAL_CHECK, item._ClassAndNumero);

                                    MessageTsu.ShowMessageWithTextBox(titleAndMessage, null, out _, out string ret, serailNumberWithoutLast4Chars, false);

                                    if (item.SerialNumber != ret)
                                    { // wrong SN entered
                                        string cancel = R.T_CANCEL;
                                        MessageInput mi = new MessageInput(MessageType.Critical, R.T_M_WRONG_SERIAL_NUMBER)
                                        {
                                            ButtonTexts = new List<string> { R.T_I_KNOW, cancel }
                                        };
                                        if (mi.Show().TextOfButtonClicked == cancel)
                                        {
                                            e.step.Cancel = true;
                                            e.step.Enter();
                                            return;
                                        }
                                    }
                                    else // right SN entered
                                    {
                                        ElementWithvalidatedSerialNumber.Add(item);
                                    }
                                }
                            }
                        }
                    }
                }


            };

            return s;
        }
        static List<E.Point> ElementWithvalidatedSerialNumber = new List<E.Point>();

        private static void ResetWhatToAlign(Common.Guided.Module gm)
        {
            gm.MagnetsToBeAligned.Clear();
            gm.PointsToBeAligned.Clear();
            gm.PointsToBeMeasured.Clear();
            gm.ElementsTobeMeasured.Clear();


            gm._ActiveStationModule._Station.ClearMeasuresToDo();

            foreach (Element item in gm._ElementManager._SelectedObjects)
            {
                if (item is E.Composites.Magnet magnet)
                {
                    gm.MagnetsToBeAligned.Add(magnet);
                }

                foreach (var pt in item.GetPoints())
                {
                    if (!gm.PointsToBeAligned.Contains(pt))
                    {
                        gm.PointsToBeAligned.Add(pt);
                    }
                }
            }
        }

        internal static Step ChoosePointsToStakeOut(Common.Guided.Module gm)
        {
            Step s = Management.ChoosePoints(gm);
            s.Id = "ChoosePointsToStakeOut";
            s.NameAndDescription = $"{R.T_CHOOSE_POINTS_TO_STAKE_OUT};{R.T_SELECT_THE_POINT_THAT_YOU_WANT_TO_STAKE_OUT}";

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                e.ActiveStation.ClearMeasuresToDo();
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                // refs
                Station st = e.ActiveStation as Station;

                e.ElementManager._SelectedObjects.Clear();
                e.ElementManager.MultiSelection = true;
                e.ElementManager.View.CheckBoxesVisible = true;
                e.ElementManager.SetSelectableToAllUniquePoint();

                e.guidedModule.PointsToBeAligned.ForEach(x => e.ElementManager._SelectedObjects.Add(x));

                var a = e.ElementManager.SelectableObjects.FindAll(x => (x as E.Point)._Name == st.Parameters2._StationPoint._Name);
                a.ForEach(x => e.ElementManager.SelectableObjects.Remove(x));

                e.step.View.AddControl(e.ElementManager);
                e.ElementManager.UpdateView();
                s.Update();
            };

            // PointsToBeMeasured already added in choosePoint
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                ResetWhatToAlign(gm);

                e.step.View.RemoveControl(e.ElementManager);

                if (s.State == StepState.Valid)
                {
                    e.guidedModule.PointsToBeAligned.Clear();
                    e.ElementManager._SelectedObjects.ForEach(x => e.guidedModule.PointsToBeAligned.Add(x as E.Point));
                }
            };
            return s;
        }

        public static class CoordinateButtons
        {
            static BigButton MapButton;
            static Label typeOfHelp = new Label() { Text = "Help:", Font = TSU.Tsunami2.Preferences.Theme.Fonts.Normal, ForeColor = Color.White, TextAlign = ContentAlignment.MiddleCenter, AutoSize = true };

            static Label typeOfDisplay = new Label() { Text = "Type of display", Font = TSU.Tsunami2.Preferences.Theme.Fonts.Normal, ForeColor = Color.White, TextAlign = ContentAlignment.MiddleCenter, AutoSize = true };
            static BigButton button_Displacements = new BigButton($"{R.T_CST_DISPLACEMENT};{R.T_CST_DISPLACEMENT_DETAILS}", R.CS_Displacement);
            static BigButton button_Observations = new BigButton($"Observations;{R.T_CST_OBSERVATION_DETAILS}", R.Station);
            static BigButton button_Offsets = new BigButton($"{R.T_CST_OFFSETS};{R.T_CST_OFFSETS_DETAILS}", R.Offset);
            static BigButton button_Coordinates = new BigButton($"{R.T_CST_COORDINATES};{R.T_CST_COORDINATES_DETAILS}", R.CS_Coordinates);

            static Label typeOfCs = new Label() { Text = "Type of coordinate system", Font = TSU.Tsunami2.Preferences.Theme.Fonts.Normal, ForeColor = Color.White, TextAlign = ContentAlignment.MiddleCenter, AutoSize = true };

            static PictureBox picture_Systems = null;

            static bool firstCall = true;

            static EventHandler<TsuObjectEventArgs> previousHandler = null;


            public static void SetAction(Action action)
            {

                var buttons = new List<Control>();
                buttons.AddRange(GetDisplayTypeButtons());
                buttons.AddRange(GetCSTypeButtons());


                // first time set action to define type of display 
                if (firstCall) InitialiseStableActions(buttons);

                // remove previous handler
                foreach (Control control in buttons)
                {
                    if (control is BigButton bb)
                        bb.BigButtonClicked -= previousHandler;
                }

                // add new handler
                if (action != null)
                {
                    EventHandler<TsuObjectEventArgs> newHandler = delegate { action(); };
                    foreach (Control control in buttons)
                    {
                        if (control is BigButton bb)
                            bb.BigButtonClicked += newHandler;
                    }

                    // switch
                    previousHandler = newHandler;
                }
            }

            public static void InitialiseStableActions(List<Control> csBigButtons)
            {
                firstCall = false;
                var css = TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems;

                button_Observations.BigButtonClicked += delegate
                {
                    TSU.Tsunami2.Properties.ValueToDisplayType = DisplayTypes.Observations;
                };
                button_Displacements.BigButtonClicked += delegate
                {
                    TSU.Tsunami2.Properties.ValueToDisplayType = DisplayTypes.Displacement;
                };
                button_Coordinates.BigButtonClicked += delegate
                {
                    TSU.Tsunami2.Properties.ValueToDisplayType = DisplayTypes.Coordinates;
                };
                button_Offsets.BigButtonClicked += delegate
                {
                    TSU.Tsunami2.Properties.ValueToDisplayType = DisplayTypes.Offsets;
                };

                foreach (BigButton bb in csBigButtons)
                {
                    bb.BigButtonClicked += delegate
                    {
                        if (bb.Tag is CoordinateSystem cs)
                            css.SelectedType = cs;
                    };
                }
            }


            public static BigButton GetMapButton()
            {
                if (MapButton == null)
                {
                    // system description 
                    MapButton = new BigButton($"{R.T_COORDINATE_SYSTEM_MAP};{R.T_SEE_THE_DIFFERENT_COORDINATE_SYSTEMS}", R.Rotation);
                    MapButton.BigButtonClicked += delegate
                    {
                        var h = Screen.PrimaryScreen.Bounds.Height * 2 / 3;
                        Tsunami2.View.ShowMessageWithFillControl(new PictureBox()
                        {
                            Image = R.CS_Systems,
                            SizeMode = PictureBoxSizeMode.Zoom,
                            Height = h,
                            MinimumSize = new Size(Screen.PrimaryScreen.Bounds.Width * 2 / 3, h),
                        }, type: MessageType.FYI, $"{R.T_COORDINATES_SYSTEM_MAP}");
                    };
                    MapButton.HasSubButtons = false;
                }
                return MapButton;
            }
            public static List<Control> GetControlsList(E.Point samplePointToDetermineWhichFrameAreExisting = null)
            {

                List<Control> l = new List<Control>();
                l.Add(typeOfHelp);
                l.Add(GetMapButton());
                l.Add(typeOfDisplay);
                l.AddRange(GetDisplayTypeButtons());
                l.Add(typeOfCs);
                l.AddRange(GetCSTypeButtons(samplePointToDetermineWhichFrameAreExisting));

                return l;
            }

            public static List<Control> GetDisplayTypeButtons()
            {

                List<Control> l = new List<Control>();
                l.Add(button_Displacements);
                l.Add(button_Coordinates);
                l.Add(button_Offsets);
                l.Add(button_Observations);

                var colors = Tsunami2.Preferences.Theme.Colors;
                var displayType = TSU.Tsunami2.Properties.ValueToDisplayType;

                button_Displacements.RestoreColor();
                button_Observations.RestoreColor();
                button_Offsets.RestoreColor();
                button_Coordinates.RestoreColor();

                switch (displayType)
                {
                    case DisplayTypes.Displacement:
                        button_Displacements.HighLightWithColor();
                        break;
                    case DisplayTypes.Coordinates:
                        button_Coordinates.HighLightWithColor();
                        break;
                    case DisplayTypes.Offsets:
                        button_Offsets.HighLightWithColor();
                        break;
                    case DisplayTypes.Observations:
                        button_Observations.HighLightWithColor();
                        break;
                    case DisplayTypes.Precisions:
                    default:
                        break;
                }

                return l;
            }

            private static List<Control> csButtons = new List<Control>();

            public static List<Control> GetCSTypeButtons(E.Point samplePointToDetermineWhichFrameAreExisting = null, bool showSystemsSelectedInThePreferences = false)
            {
                List<Control> buttons = csButtons;

                var cs = TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems;

                foreach (var lastCS in cs.AllTypes)
                {
                    if (lastCS._Name == "TEMPCALCULATION" || lastCS._Name == "USERDEFINED")
                        continue;
                    bool wantToHideSomeSystem = samplePointToDetermineWhichFrameAreExisting != null;

                    BigButton bb = buttons.Find(x => x.Tag == lastCS) as BigButton;
                    if (bb == null)
                    {
                        bb = new BigButton($"{lastCS.LongName};{lastCS.Description}", lastCS.Image);
                        bb.Tag = lastCS;
                        bb.BigButtonClicked += delegate
                        {
                            cs.SelectedType = lastCS;
                        };
                        buttons.Add(bb);
                    }
                    if (showSystemsSelectedInThePreferences)
                    {
                        if (cs.SelectedTypes.Contains(lastCS))
                            bb.HighLightWithColor();
                        else
                            bb.RestoreColor();
                    }
                    else
                    {
                        if (lastCS == cs.SelectedType)
                            bb.HighLightWithColor();
                        else
                            bb.RestoreColor();
                    }
                }
                return buttons;
            }

            internal static void WarnAboutNoneCartesianCS(TsuView view)
            {
                string csName = TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedType._Name.ToUpper();
                DisplayTypes td = TSU.Tsunami2.Properties.ValueToDisplayType;

                if (csName == "BEAM" || csName == "CCS-Z")
                    if (td == DisplayTypes.Displacement || td == DisplayTypes.Offsets)
                    {
                        string titleAndMessage = $"{R.T_CS_WARNING};{R.T_CS_WARNING_DETAILS}";
                        new MessageInput(MessageType.Warning, titleAndMessage).Show();
                    }
            }

        }

        static public Step StakeOutNextPoint(Common.Guided.Module guidedModule, Measure measure)
        {
            Step s = Measurement.MeasureNextPointGeneral(guidedModule, measure, Measurement.Types.StakeOut); //new Step(guidedModule, "a");// MeasureNextPoint(guidedModule, pointName, tag);

            s.Id = "StakeOutNextPoint";

            // Usefull references
            Station.Module stm = null;
            Station.View stmv = null;
            Common.Station st = null;
            E.Point PointToStakeOut = null;
            CoordinatesInAllSystems offsets = null;

            // news buttons
            BigButton changeOffsetType = null;
            BigButton saveButton = null;
            Measure lastMeasure = null;
            DataGridView grid = new DataGridView();



            M.MeasurementEventHandler OnResultsAvailable = delegate (object source2, M.MeasurementEventArgs e2)
            {
                if (e2.Measure._Point._Name == PointToStakeOut._Name)
                {
                    lastMeasure = e2.Measure as Measure;
                    offsets = (e2.Measure as Measure).Offsets;

                    // remove old measure for this point and replace by new one
                    {
                        int i = s.GuidedModule.ReceivedMeasures.FindIndex(x => x._Point._Name == lastMeasure._Point._Name);
                        if (i != -1) s.GuidedModule.ReceivedMeasures.RemoveAt(i);
                        s.GuidedModule.ReceivedMeasures.Add(lastMeasure);
                    }

                    if (lastMeasure._Status.Type == M.States.Types.Good) s.State = StepState.Valid;
                    else if (lastMeasure._Status.Type == M.States.Types.Questionnable) s.State = StepState.ReadyToSkip;

                    s.Update();
                }
                else
                {
                    offsets = null;
                }
            };

            s.FirstUse += delegate (object source, StepEventArgs e)
            {

                stm = s.GuidedModule._ActiveStationModule as Station.Module;
                stmv = stm.View;
                st = stm.StationTheodolite;
                PointToStakeOut = (s.TAG as Measure)._OriginalPoint;

                DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShouldChangeNameOfExistingBeforeMeasurement").State = DsaOptions.Never; //stm.flags.ShouldChangeNameOfExistingBeforeMeasurement = DsaOptions.Never;
                DsaFlag.GetByNameOrAdd(stm.DsaFlags, "PutPreviousMeasureToBad").State = DsaOptions.Always; //stm.flags.AskToPutPreviousMeasureToBad = false;
                                                                                                           //stm.flags.AlwaysPutPreviousMeasureToBad = true;

                List<Control> controls = new List<Control>
                {
                    s.View.CreateLabel(R.T_IMP_CHOOSE, "type", s.View.DescriptionFont)
                };

                // offset selection button
                {
                    changeOffsetType = new BigButton(R.T_IMP_CHANGE_OFFSET_TYPE, R.Offset);
                    changeOffsetType.BigButtonClicked += delegate
                    {
                        s.View.ShowPopUpSubMenu(CoordinateButtons.GetControlsList(lastMeasure._Point), "CS ");
                    };
                    changeOffsetType.HasSubButtons = true;
                    //changeOffsetType.IsEndContextMenuItem = false;
                    controls.Add(changeOffsetType);
                }
                s.View.AddSidedControls(controls, DockStyle.Top);

                // add saving button
                {
                    saveButton = new BigButton(
                           $"{R.T_Keep};{R.T_KEEP_THIS_ACQUISITION}",
                           R.Save, delegate (object source2, TsuObjectEventArgs args2)
                           {
                               stm.AddLastPointAndMeasure(lastMeasure);
                               saveButton.Enabled = false;
                               s.State = StepState.Valid;
                           }, null, null, false);
                    saveButton.Enabled = false;
                    s.View.AddButton(saveButton);
                }

                e.step.View.ModifyLabel(" ", "next");

                // Set measure to "questionnable state"
                Measure m = stm.StationTheodolite.MeasuresToDo.Find(x => x._Point._Name == PointToStakeOut._Name) as Measure;
                if (m != null) m._Status = new M.States.Questionnable();

                // Subscribe to OnResultavailable
                stm.ResultsAvailable += OnResultsAvailable;

                // add saving button
                {
                    BigButton deltaHButton = new BigButton(
                           $"{R.T_SO_CHANGE_H};{R.T_SO_CHANGE_H_Details}",
                           R.Rallonge, delegate (object source2, TsuObjectEventArgs args2)
                           {
                               stm.StakeOut(m);
                               stm.SendMeasureToInstrument(m);
                           }, null, null, false);
                    deltaHButton.Enabled = true;
                    s.View.AddButton(deltaHButton);
                }

            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                // Prepare next step?
                Measurement.PrepareOrNotNextMeasurementStep(s.GuidedModule, s.TAG as Measure, Measurement.Types.StakeOut);

                // show old results?
                M.Measure m = s.GuidedModule.ReceivedMeasures.LastOrDefault(x => x._Point._Name == (s.TAG as Measure)._OriginalPoint._Name);
                if (m != null)
                    lastMeasure = m as Measure;

                CoordinateButtons.SetAction(() =>
                {
                    CoordinateButtons.WarnAboutNoneCartesianCS(s.View);
                    s.Update();
                });

                // Update
                s.Update();
            };

            s.Updating += delegate (object source, StepEventArgs e)
            {

                int d = TSU.Tsunami2.Preferences.Values.GuiPrefs == null ? 7 : TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
                if (s.State == StepState.Valid)
                    saveButton.Enabled = false;
                else
                    saveButton.Enabled = true;


                if (lastMeasure != null)
                {
                    if (lastMeasure._Point._Name == PointToStakeOut._Name)
                    {
                        List<Coordinates> results = Tools.ShowResults(
                           TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedType._Name,
                           TSU.Tsunami2.Properties.ValueToDisplayType,
                            changeOffsetType, s, lastMeasure, out string message);

                        string text = "";
                        foreach (var item in results)
                        {
                            if (item._Name == PointToStakeOut._Name)
                            {
                                if (TSU.Tsunami2.Properties.ValueToDisplayType != DisplayTypes.Coordinates)
                                    text += item.ToString("mm");
                                else
                                    text += item.ToString();
                                text += "\r\n";
                            }
                        }
                        s.View.ModifyLabel($"{message}\r\n{text}", "type");
                        // cahnge type to show if it is a quick measure
                        Color c = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
                        if (lastMeasure.isQuickMeasure) c = TSU.Tsunami2.Preferences.Theme.Colors.Attention;
                        s.View.ModifyLabelColor(c, "type");
                    }
                }

                Measurement.WhatsNext(e.step, (s.GuidedModule._ActiveStationModule as Station.Module).StationTheodolite);
            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {
            };

            #region Testing
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                e.step.CheckValidityOf(e.step.State == StepState.Valid);

            };

            #endregion
            return s;
        }



        static public Step PreAlignNextPoint(Common.Guided.Module guidedModule, Measure measure)
        {
            Step s = Measurement.MeasureNextPointGeneral(guidedModule, measure, Measurement.Types.PreAlignment); //new Step(guidedModule, "a");// MeasureNextPoint(guidedModule, pointName, tag);

            s.Id = "StakeOutNextPoint";

            // Usefull references
            Station.Module stm = null;
            Station.View stmv = null;
            Common.Station st = null;
            E.Point PointToStakeOut = null;
            CoordinatesInAllSystems offsets = null;

            // news buttons
            BigButton changeOffsetType = null;
            BigButton bocButton = null;
            Measure lastMeasure = null;


            M.MeasurementEventHandler OnResultsAvailable = delegate (object source2, M.MeasurementEventArgs e2)
            {
                if (e2.Measure._Point._Name == PointToStakeOut._Name)
                {
                    lastMeasure = e2.Measure as Measure;
                    offsets = (e2.Measure as Measure).Offsets;

                    // remove old measure for this point and replace by new one
                    {
                        int i = s.GuidedModule.ReceivedMeasures.FindIndex(x => x._Point._Name == lastMeasure._Point._Name);
                        if (i != -1) s.GuidedModule.ReceivedMeasures.RemoveAt(i);
                        s.GuidedModule.ReceivedMeasures.Add(lastMeasure);
                    }

                    // i remove this because then it is done 2 times, here and in the recordornot from staionmodule
                    //st.MeasuresTaken.Add(lastMeasure);
                    //stm._MeasureManager.AllElements.Add(lastMeasure);
                    s.Update();
                }
                else
                {
                    offsets = null;
                }
            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {
                // this step can be create so many tim ewe are running out of windwos handles so we have to dispose it and cereate everytime
                s.View.Dispose();
                s.firstUse = true;
            };


            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                Logs.Log.AddEntryAsFYI(stm, $"First use of step {s._Name}");

                stm = s.GuidedModule._ActiveStationModule as Station.Module;
                stmv = stm.View;
                stmv.SuspendLayout();

                st = stm.StationTheodolite;
                PointToStakeOut = (s.TAG as Measure)._OriginalPoint;

                DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShouldChangeNameOfExistingBeforeMeasurement").State = DsaOptions.Never; //stm.flags.ShouldChangeNameOfExistingBeforeMeasurement = DsaOptions.Never;
                DsaFlag.GetByNameOrAdd(stm.DsaFlags, "PutPreviousMeasureToBad").State = DsaOptions.Always; //stm.flags.AskToPutPreviousMeasureToBad = false;
                //stm.flags.AlwaysPutPreviousMeasureToBad = true;

                string chosenStrategy = guidedModule.GroupModule.SubGuidedModules[0].RabotStrategy == C.Rabot.Strategy.Not_Defined ? string.Empty : guidedModule.GroupModule.SubGuidedModules[0].RabotStrategy.ToString().Replace('_', ' '); ;
                List<Control> controls = new List<Control>
                {
                    s.View.CreateLabel(R.T_IMP_CHOOSE + chosenStrategy, "type", s.View.DescriptionFont)
                };

                // BOC button
                {
                    Action bco = () =>
                    {
                        var assemblyIds = s.GuidedModule.GetListOfSuAssembly();
                        ProgressMessage pm = new ProgressMessage(s.View,
                            R.T_TSUNAMI_IS_RUNNING_THINGS_FOR_YOU, R.T_PROGRESS,
                            stepsCount: assemblyIds.Count, 2000);
                        foreach (var assemblyId in assemblyIds)
                        {
                            try
                            {
                                if (pm.IsCancelled) break;
                                pm.BeginAstep($"Boc for {assemblyId}");
                                Results results = TSU.Common.Compute.Compensations.BeamOffsets.WorkFlow.Compute(
                                    e.guidedModule, Tsunami2.Properties.MeasurementModules.GetByObservationType(ObservationType.Polar, addRollModules: true), assemblyId, ObservationType.Polar);
                                WorkFlow.OffsetComputedInAllSystems.WaitOne(5 * 1000);
                                results.Obsolete = false;
                                pm.EndCurrentStep();
                            }
                            catch (TsuException ex)
                            {
                                MessageTsu.ShowMessage(ex, MessageType.Warning);
                            }
                            catch (Exception ex)
                            {
                                MessageTsu.ShowMessage(ex, MessageType.Important);
                            }
                            finally
                            {
                                pm.EndAll();
                                pm.Dispose();
                            }
                        }
                        s.Update();
                    };

                    bocButton = TSU.Common.Compute.Compensations.BeamOffsets.WorkFlow.Buttons.GetComputeButton(bco);
                    controls.Add(bocButton);
                }

                // offset selection button
                {
                    changeOffsetType = new BigButton(R.T_IMP_CHANGE_OFFSET_TYPE, R.Rotation);
                    changeOffsetType.BigButtonClicked += delegate
                    {
                        E.Point lastPoint = lastMeasure != null ? lastMeasure._Point : null;
                        s.View.ShowPopUpSubMenu(CoordinateButtons.GetControlsList(lastPoint), "CS ");
                        // s.View.PopUpMenu.ShowSubMenu(new List<Control>() { new PictureBox() { Image = R.CS_Systems } });
                    };
                    changeOffsetType.HasSubButtons = true;
                    controls.Add(changeOffsetType);
                }

                s.View.AddSidedControls(controls, DockStyle.Top, new List<int>() { 40, 30, 30 });

                //Tools.Grid.AddGuidedModule(s, dockStyle: DockStyle.Bottom);
                Tools.TreeGrid.AddGuidedModule(s, dockStyle: DockStyle.Top);
                //s.GuidedModule.View.grid.Visible = false;

                // Set measure to "questionable state"
                Measure m = stm.StationTheodolite.MeasuresToDo.Find(x => x._Point._Name == PointToStakeOut._Name) as Measure;
                if (m != null) m._Status = new M.States.Questionnable();

                // Subscribe to OnResultsAvailable
                stm.ResultsAvailable += OnResultsAvailable;

            };


            s.Entering += delegate (object source, StepEventArgs e)
            {
                // Prepare next step?
                Measurement.PrepareOrNotNextMeasurementStep(s.GuidedModule, s.TAG as Measure, Measurement.Types.PreAlignment);

                CoordinateButtons.SetAction(() =>
                {
                    CoordinateButtons.WarnAboutNoneCartesianCS(s.View);
                    s.Update();
                });

                s.Update();
            };

            #region Testing
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                e.step.CheckValidityOf(s.stepsRemovedByUser == true); // not visible means it as been remove b y the button click
            };

            #endregion

            s.Updating += delegate (object source, StepEventArgs e)
            {
                s.View.LayoutLayering(Common.Guided.Steps.View.LayoutOption.Suspend);
                Logs.Log.AddEntryAsFYI(stm, $"Updating of step {s._Name}");

                string cs = TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedType._Name;
                DisplayTypes td = TSU.Tsunami2.Properties.ValueToDisplayType;

                Tools.UpdateResults(TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedType._Name, td, s, s.GuidedModule.View.grid, s.GuidedModule.View.treeGrid, lastMeasure, changeOffsetType);


                //s.View.ModifyLabel($"{R.T_IMP_CHOOSE}\r\n{td} in {cs}\r\n", "type");

                s.View.LayoutLayering(Common.Guided.Steps.View.LayoutOption.Resume);
            };

            return s;
        }

        public static class Tools
        {
            public static class TreeGrid
            {
                public static void AddGuidedModule(Step s, DockStyle dockStyle = DockStyle.Top)
                {
                    if (s.GuidedModule.View.treeGrid == null || s.GuidedModule.View.treeGrid.IsDisposed)
                    {
                        s.GuidedModule.View.treeGrid = new Views.TreeGrid();
                        s.GuidedModule.View.treeGrid.GridPart.ColumnAdded += Grid.OnColumnAdded;
                        s.GuidedModule.View.treeGrid.GridPart.CellClick += Grid.OnClicked;
                        s.GuidedModule.View.treeGrid.GridPart.Validated += Grid.OnValidated;
                        //s.GuidedModule.View.treeGrid.GridPart.DataBindingComplete += Grid.OnDataBindingComplete;
                    }
                    s.GuidedModule.View.treeGrid.Height = 450;
                    s.View.AddGrid(s.GuidedModule.View.treeGrid, dockStyle);
                    // Grid_Validated(s.GuidedModule.View.grid, null); // useless the columns do not exist yet
                }
            }

            public static class Grid
            {

                public static void AddGuidedModule(Step s, DockStyle dockStyle = DockStyle.Top)
                {
                    var gmGrid = s.GuidedModule.View.grid;
                    if (gmGrid == null || gmGrid.IsDisposed)
                    {
                        gmGrid = Initialize(DataGridViewAutoSizeColumnsMode.Fill);
                        gmGrid.ColumnAdded += OnColumnAdded;
                        gmGrid.CellClick += OnClicked;

                        gmGrid.Validated += OnValidated;

                        //gmGrid.DataBindingComplete += OnDataBindingComplete;
                        s.GuidedModule.View.grid = gmGrid;
                    }
                    s.View.AddGrid(gmGrid, dockStyle);
                    // Grid_Validated(s.GuidedModule.View.grid, null); // useless the columns do not exist yet
                }

                public static DataGridView Initialize(DataGridViewAutoSizeColumnsMode width = DataGridViewAutoSizeColumnsMode.DisplayedCellsExceptHeader)
                {
                    DataGridView dgv = new DataGridView();

                    ((System.ComponentModel.ISupportInitialize)(dgv)).BeginInit();
                    dgv.AutoSizeColumnsMode = width;
                    TSU.Tsunami2.Preferences.Theme.ApplyTo(dgv);
                    dgv.Dock = System.Windows.Forms.DockStyle.Fill;
                    ((System.ComponentModel.ISupportInitialize)(dgv)).EndInit();
                    return dgv;
                }

                public static void OnColumnAdded(object sender, DataGridViewColumnEventArgs e)
                {
                    var cs = Tsunami2.Properties.CoordinatesSystems.SelectedType;
                    var dt = Tsunami2.Properties.ValueToDisplayType;

                    string prefix;
                    string suffix;
                    string unit;
                    switch (dt)
                    {
                        case DisplayTypes.Displacement:
                            prefix = "d(";
                            suffix = ")";
                            unit = "mm";
                            break;
                        case DisplayTypes.Coordinates:
                            prefix = "";
                            suffix = "";
                            unit = "m";
                            break;
                        case DisplayTypes.Offsets:
                            prefix = "o(";
                            suffix = ")";
                            unit = "mm";
                            break;
                        case DisplayTypes.Observations:
                        case DisplayTypes.Precisions:
                        default:
                            prefix = "";
                            suffix = "";
                            unit = "";
                            break;
                    }

                    switch (e.Column.Name)
                    {
                        case "Extension":
                            e.Column.HeaderText = $"Ext. [{unit}]";
                            break;
                        case "X":
                            e.Column.HeaderText = $"{prefix}{cs.AxisNames[0]}{suffix} [{unit}]";
                            break;
                        case "Y":
                            e.Column.HeaderText = $"{prefix}{cs.AxisNames[1]}{suffix} [{unit}]";
                            break;
                        case "Z":
                            e.Column.HeaderText = $"{prefix}{cs.AxisNames[2]}{suffix} [{unit}]";
                            break;
                        case "ExpO_Rbv_MM_":
                            e.Column.HeaderText = $"ExpO Rbv [{unit}]";
                            break;
                        case "ExpO_Hbv_MM_":
                            e.Column.HeaderText = $"ExpO Hbv [{unit}]";
                            break;
                        case "Move_Rbv_MM_":
                            e.Column.HeaderText = $"Move Rbv [{unit}]";
                            break;
                        case "Move_Hbv_MM_":
                            e.Column.HeaderText = $"Move Hbv [{unit}]";
                            break;
                        case "dRoll_mrad":
                            e.Column.HeaderText = $"dRoll [mrad]";
                            break;
                        case "sRoll_mrad":
                            e.Column.HeaderText = $"sRoll [mrad]";
                            break;
                        case "Unit":
                            e.Column.Visible = false;
                            break;
                        default:
                            break;
                    }
                    e.Column.MinimumWidth = 70;
                }



                public static void OnClicked(object sender, DataGridViewCellEventArgs e)
                {
                    if (e.RowIndex == -1) // header
                        return;
                    if (e.ColumnIndex == 0) // tree part
                        return;


                    // find the guided module containing the grid, and the TreeGrid
                    GetContextFromSender(sender, out Common.Guided.Module gm, out DataGridView dgv, out Views.TreeGrid tg);
                    var property = dgv.Columns[e.ColumnIndex];
                    var result = tg.FlattenCategories[e.RowIndex] as Result;
                    var measure = result.Measure as Measure;

                    //var measureToDo = gm._ActiveStationModule._Station.MeasuresToDo.Find(x => x._Point._Name == measureTaken._Point._Name);

                    // same first row to avoid jump in datagrid
                    gm.CurrentStep.gridRowIndex = dgv.FirstDisplayedScrollingRowIndex;

                    switch (property.Name)
                    {
                        case "Name":
                            MoveToGoodStep(result.Name, gm);
                            break;
                        case "Extension":
                            if (measure != null)
                                ChangeExtension(measure, gm);
                            break;
                        case "Reflector":
                            if (measure != null)
                                ChangeReflector(measure, gm);
                            break;
                        //case "type":
                        //    ChangeMeasureMode(measure, gm);
                        //    break;
                        case "Interfaces":
                            if (measure != null)
                                ChangeInterface(measure, gm);
                            break;
                        default:
                            break;
                    }
                }

                private static void MoveToGoodStep(string pointName, Common.Guided.Module gm)
                {
                    // check if we want to cahnge the step or if it is a reference points
                    if (Common.Analysis.Element.IsNameContainedIn(pointName, gm.PointsToBeAligned))
                    {
                        gm.PreviousScrollPosition = gm.CurrentStep.View.VerticalScroll.Value;
                        MoveToAlignmentStepOfAPoint(pointName, gm);
                    }
                }

                private static void ChangeInterface(Measure measure, Common.Guided.Module gm)
                {
                    var stm = gm._ActiveStationModule as Station.Module;
                    var stmv = stm.View;
                    var st = stm.StationTheodolite;
                    I.Device.Interfaces newInterFaces = new I.Device.Interfaces();

                    if (AskWhichMeasureToModify(out string choice))
                    {
                        var temporaryMeasureThatReceivesTheNewInterface = measure.Clone() as Measure;
                        stmv.SelectInterfaces(measure.Interfaces, temporaryMeasureThatReceivesTheNewInterface, additionalAction: () =>
                        {
                            if (choice == R.T_LAST)
                            {
                                stm.ChangeInterfacesBy(measure, temporaryMeasureThatReceivesTheNewInterface.Interfaces);
                            }
                            else
                            {
                                var measureModelForNextOnes = st.MeasuresToDo.LastOrDefault(x => x._Name.Contains(measure._Point._Name)) as Measure;
                                stm.ChangeInterfacesBy(measureModelForNextOnes, temporaryMeasureThatReceivesTheNewInterface.Interfaces);
                                if (choice == R.T_BOTH)
                                    measure.Interfaces = measureModelForNextOnes.Interfaces;
                                MoveToGoodStep(measure._Point._Name, gm);
                            }

                            gm.CurrentStep.Update();
                        });
                    }
                }

                private static void ChangeReflector(Measure measure, Common.Guided.Module gm)
                {
                    if (AskWhichMeasureToModify(out string choice, $"\r\n{R.FOR_THE_ALREADY_REFLECTOR}"))
                    {
                        var stm = gm._ActiveStationModule as Station.Module;
                        var stmv = stm.View;
                        var st = stm.StationTheodolite;
                        if (choice == R.T_LAST)
                        {
                            stmv.Reflector_Change(forceMeasureToModify: measure);
                        }
                        else
                        {
                            var measureModelForNextOnes = st.MeasuresToDo.LastOrDefault(x => x._Name.Contains(measure._Point._Name)) as Measure;
                            var beforePossibleModification = measureModelForNextOnes.Reflector;
                            stmv.Reflector_Change(forceMeasureToModify: measureModelForNextOnes);
                            if (choice == R.T_BOTH && measureModelForNextOnes.Reflector != beforePossibleModification)
                                measure.Reflector = measureModelForNextOnes.Reflector;
                            MoveToGoodStep(measure._Point._Name, gm);
                        }

                        gm.CurrentStep.Update();
                    }
                }

                private static void ChangeExtension(Measure measure, Common.Guided.Module gm)
                {
                    if (AskWhichMeasureToModify(out string choice))
                    {
                        var stm = gm._ActiveStationModule as Station.Module;
                        var stmv = stm.View;
                        var st = stm.StationTheodolite;
                        if (choice == R.T_LAST)
                        {
                            stmv.ChangeExtension(measure.Extension, forceMeasureToModify: measure);
                            stm.OnMeasureReceived(null, new M.MeasurementEventArgs() { JustMeasured = measure });
                        }
                        else
                        {
                            var measureModelForNextOnes = st.MeasuresToDo.LastOrDefault(x => x._Name.Contains(measure._Point._Name)) as Measure;
                            var extensionBeforePossibleModification = measureModelForNextOnes.Extension.Value;
                            stmv.ChangeExtension(measureModelForNextOnes.Extension, forceMeasureToModify: measureModelForNextOnes);
                            if (choice == R.T_BOTH && measureModelForNextOnes.Extension.Value != extensionBeforePossibleModification)
                                measure.Extension = measureModelForNextOnes.Extension.Clone() as DoubleValue;
                            MoveToGoodStep(measure._Point._Name, gm);
                        }

                        gm.CurrentStep.Update();
                    }
                }

                private static bool AskWhichMeasureToModify(out string choice, string additionMessage = "")
                {
                    MessageInput mi = new MessageInput(MessageType.Choice, $"{R.WHICH_MEASURE_TO_MODIFY} {additionMessage}")
                    {
                        ButtonTexts = new List<string>() { R.T_LAST, R.T_NEXT, R.T_BOTH, R.T_CANCEL }
                    };
                    var result = mi.Show();
                    choice = result.TextOfButtonClicked;
                    return choice != R.T_CANCEL;
                }

                internal static void OnDataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
                {
                    HideEmptyColumns((DataGridView)sender);
                }

                private static void HideEmptyColumns(DataGridView dataGridView)
                {
                    // Iterate through each column in the DataGridView
                    foreach (DataGridViewColumn column in dataGridView.Columns)
                    {
                        bool isEmpty = true;

                        // Check if any cell in the current column contains a non-empty value
                        foreach (DataGridViewRow row in dataGridView.Rows)
                        {
                            if (row.Cells[column.Index].Value != null)
                            {
                                string cellValue = row.Cells[column.Index].Value.ToString();
                                if (!string.IsNullOrEmpty(cellValue))
                                {
                                    isEmpty = false;
                                    break; // No need to continue checking if a non-empty value is found
                                }
                            }
                        }

                        // If all cells in the column are empty, hide the column
                        column.Visible = !isEmpty;
                    }
                }

                private static void SetFirstDisplayedRowIndex(DataGridView dgv, int rowIndex)
                {
                    // Use BeginInvoke to defer the operation to the UI thread
                    dgv.BeginInvoke((MethodInvoker)delegate
                    {
                        try
                        {
                            dgv.FirstDisplayedScrollingRowIndex = rowIndex;
                        }
                        catch (Exception ex)
                        {
                            Logs.Log.AddEntryAsPayAttentionOf(null, $"Cannot reset the datagridView to the row #{rowIndex} {ex}");
                        }
                    });
                }

                internal static void OnValidated(object sender, EventArgs e)
                {
                    GetContextFromSender(sender, out Common.Guided.Module gm, out DataGridView dgv, out Views.TreeGrid tg);

                    // restore vertical position  from previous step if the number of item is bigger thant the grid
                    if (gm != null)
                        SetFirstDisplayedRowIndex(dgv, gm.CurrentStep.gridRowIndex);

                    // set columns width
                    for (int i = 0; i <= dgv.Columns.Count - 1; i++)
                    {
                        //if (i == 1) // if first column (point name) take all the space left
                        //{
                        //    dgv.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        //}
                        //else
                        {
                            //datagrid has calculated it's widths so we can store them
                            dgv.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

                            //store autosized widths
                            int colw = dgv.Columns[i].Width;

                            //remove autosizing
                            dgv.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;

                            //set width to calculated by autosize
                            dgv.Columns[i].Width = colw;
                        }

                    }

                    // set row type 
                    for (int i = 0; i <= dgv.Rows.Count - 1; i++)
                    {
                        if (tg.FlattenCategories[i] is Result r && r.isRef)
                        {
                            dgv.Rows[i].DefaultCellStyle.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
                        }
                    }
                }
            }

            private static void GetContextFromSender(object sender, out Common.Guided.Module gm, out DataGridView dgv, out TSU.Views.TreeGrid tg)
            {
                dgv = sender as DataGridView;
                gm = null;
                tg = null;
                Control parent = dgv.Parent;
                while (parent != null)
                {
                    if (parent is Common.Guided.View cgv)
                    {
                        gm = cgv.Module as Common.Guided.Module;
                        tg = cgv.treeGrid;
                    }
                    parent = parent.Parent;
                }
            }

            public static void SaveLastmeasurements(Common.Guided.Module gm, Common.Station st, Station.Module stm, bool exportTogeode = false)
            {
                try
                {
                    // check if we use the plar mudule if not, nothing to do here
                    if (gm._ActiveStationModule._Station.MeasuresTaken.Count == 0)
                        return;

                    foreach (var item in gm.PointsToBeAligned)
                    {
                        bool weAreGood = false;
                        Measure lqm = FindLastMeasureOf(item, gm._ActiveStationModule._Station.MeasuresTaken, stateType: M.States.Types.Questionnable);
                        Measure lgm = FindLastMeasureOf(item, gm._ActiveStationModule._Station.MeasuresTaken, stateType: M.States.Types.Good);

                        if (lqm != null && lgm != null)
                        {
                            if (lgm._Date > lqm._Date)
                                continue; // the last measuer is already ggod, lets go to next point to align
                        }


                        if (lqm != null)
                        {
                            bool goodMeasureAlreadyPresent = true;
                            List<M.Measure> temp = new List<M.Measure>();
                            temp.AddRange(gm._ActiveStationModule._Station.MeasuresTaken);
                            while (goodMeasureAlreadyPresent)
                            {
                                Measure good = FindLastMeasureOf(item, temp, stateType: M.States.Types.Good);
                                if (good != null)
                                {
                                    string titleAndMessage = $"{R.T_A_MEASURE_CONSIDERED_AS_GOOD_IS_ALREADY_PRESENT_IN_THE_LIST}" +
                                                             $"'{item._Name}' {R.T_AT} {good._Date};" +
                                                             $"{R.T_DO_YOU_WANT_TO_TURN_THIS_PREVIOUS_MEASURE_INTO_A_BAD_STATE}";
                                    MessageInput mi = new MessageInput(MessageType.Warning, titleAndMessage)
                                    {
                                        ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                                    };
                                    string r = mi.Show().TextOfButtonClicked;

                                    if (r == R.T_YES)
                                    {
                                        // turn it bad
                                        good._Status = new M.States.Bad(previousStateType: M.States.Types.Good);
                                    }
                                    temp.Remove(good);
                                }
                                else
                                    goodMeasureAlreadyPresent = false;
                            }

                            lqm._Status = new M.States.Good();
                            stm.SavePoint(lqm._Point, E.Manager.Module.AddingStrategy.ReplaceIfSameGroup);

                            //st.PointsMeasured.Add(lqm._Point); // this list is temporary for the polar stations, it return the measuretaken points !
                        }
                    }

                    if (!stm.CheckOpeningHasBeenControled(out _))
                    {
                        //   throw new Exception (string.Format("{0};{1}", "Polar station not controled", "See you in the polar module to control the opening"));
                        throw new Exception($"{R.T_POLAR_STATION_NOT_CONTROLED};{R.T_SEE_YOU_IN_THE_POLAR_MODULE_TO_CONTROL_THE_OPENING}");
                    }
                    if (exportTogeode) stm._MeasureManager.ExportToGeode();
                }
                catch (Exception ex)
                {
                    // gm.View.ShowMessageOfExclamation(string.Format("{0};{1}", "Impossible to turn all last measures to 'good'", ex.Message));
                    string titleAndMessage = $"{R.T_IMPOSSIBLE_TO_TURN_ALL_LAST_MEASURES_TO_GOOD};{ex.Message}";
                    new MessageInput(MessageType.Warning, titleAndMessage).Show();
                }

                gm.Save();
            }

            public class Result
            {
                public virtual string Name { get; set; }

                [System.ComponentModel.DisplayName("Extension")]
                public string X { get; set; }
                public string Y { get; set; }
                public string Z { get; set; }
                public string ExpO_Rbv_MM_ { get; set; }
                public string ExpO_Hbv_MM_ { get; set; }
                public string Move_Rbv_MM_ { get; set; }
                public string Move_Hbv_MM_ { get; set; }
                public string Unit { get; set; }
                public string dRoll_mrad { get; set; }
                public string sRoll_mrad { get; set; }
                public string type { get; set; }
                public string Extension { get; set; }
                public string Reflector { get; set; }
                public string Interfaces { get; set; }
                public bool isRef { get; set; }
                public bool isObsolete { get; set; }
                public bool isHighlighted { get; set; }
                public string assemblyId { get; set; } = "";
                public object Measure { get; set; }

                public override string ToString()
                {
                    return $"{Name} obsolete {isObsolete}";
                }
            }

            static List<Result> results4Grid = new List<Result>();
            static List<E.Point> points4Grid = new List<E.Point>();
            static Timer GridUpdateTimer;

            public static void UpdateResults(string typeS, DisplayTypes typeD, Step s, DataGridView grid, Views.TreeGrid treeGrid, Measure lastMeasure, BigButton changeOffsetType)
            {
                //s.GuidedModule.View.grid.DataSource = null;

                Action Update = () =>
                {

                    s.View.LayoutLayering(Common.Guided.Steps.View.LayoutOption.Suspend);
                    int d = TSU.Tsunami2.Preferences.Values.GuiPrefs == null ? 7 : TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;

                    Func<Measure, Coordinates> GetCoordinates = GetRightFunction(typeS, typeD, out string typeString, changeOffsetType, s);

                    int indexOfElementfromThisStep = -1;
                    int count = 0;
                    results4Grid.Clear();
                    points4Grid.Clear();

                    List<E.Point> refs = (s.GuidedModule._ActiveStationModule._Station as Station).ReferencePoints.FindAll(x => !x.IsPilier);

                    points4Grid.AddRange(refs);
                    //points.AddRange(s.GuidedModule.PointsToBeAligned.Clone()); // why a clone here, every measure is creating a ful list of those points
                    points4Grid.AddRange(s.GuidedModule.PointsToBeAligned); // why a clone here, every measure is creating a ful list of those points

                    points4Grid.Sort();
                    //grid.Tag = null;
                    var adminModule = s.GuidedModule.GroupModule.SubGuidedModules[0];

                    //// if we are using rabot csv file for expected offset or relative displacment, we should force the display to by on 'Offesets and not dispalcemen or coordinates
                    //if (adminModule.RabotDisplayType != "Not Defined" && typeD != DisplayTypes.Offsets)
                    //{
                    //    typeD = DisplayTypes.Offsets;
                    //    TSU.Tsunami2.Properties.ValueToDisplayType = DisplayTypes.Offsets;
                    //    new MessageInput(MessageType.Warning, $"{R.T_DISPLAY_TYPE_FORCED_TO_OFFSET};{R.T_BECAUSE_YOU_ARE_USING_IMPORTED_OFFSETSDISPLACEMENTS}").Show();
                    //}

                    foreach (var item in points4Grid)
                    {
                        double smoothR = 0.0;
                        double roughMinusSmoothR = 0.0;
                        double smoothV = 0.0;
                        double roughMinusSmoothV = 0.0;
                        if (adminModule.VerticalRabot.ContainsKey(item._Name))
                        {
                            smoothV = adminModule.VerticalRabot[item._Name].Smooth.GetValueOrDefault();
                            roughMinusSmoothV = adminModule.VerticalRabot[item._Name].RoughMinusSmooth.GetValueOrDefault();
                        }
                        if (adminModule.RadialRabot.ContainsKey(item._Name))
                        {
                            smoothR = adminModule.RadialRabot[item._Name].Smooth.GetValueOrDefault();
                            roughMinusSmoothR = adminModule.RadialRabot[item._Name].RoughMinusSmooth.GetValueOrDefault();
                        }
                        if (item._Name == (s.GuidedModule.CurrentStep.TAG as Measure)._Point._Name)
                            indexOfElementfromThisStep = count;
                        Measure m = FindLastMeasureOf(item, s.GuidedModule._ActiveStationModule._Station.MeasuresTaken, M.States.Types.Unknown, M.States.Types.Control | M.States.Types.Bad);
                        Result r = null;
                        if (m == null)
                        {
                            string notAvailable = "-";
                            r = new Result()
                            {
                                Name = item._Name,
                                Extension = notAvailable,
                                Reflector = notAvailable,
                                Interfaces = notAvailable,
                                X = notAvailable,
                                Y = notAvailable,
                                Z = notAvailable,
                                ExpO_Rbv_MM_ = notAvailable,
                                ExpO_Hbv_MM_ = notAvailable,
                                Move_Rbv_MM_ = notAvailable,
                                Move_Hbv_MM_ = notAvailable,
                                Unit = notAvailable,
                                type = notAvailable
                            };
                        }
                        else
                        {
                            bool useMm = typeD != DisplayTypes.Coordinates;
                            Coordinates c = GetCoordinates(m);
                            DoubleValue thousand = new DoubleValue(1000);
                            if (!c.AreKnown)
                                c.Set();

                            r = new Result()
                            {
                                Name = item._Name,
                                Extension = useMm ? m.Extension.ToString(d - 3, 1000) : m.Extension.ToString(d),
                                Reflector = m.Distance.Reflector._Name,
                                Interfaces = m.Interfaces._Name,
                                Unit = useMm ? "mm" : "m",
                                type = m.CommentFromTsunami.Contains(R.T_QUICK_MEASURE) ? "Quick" : "Normal",
                                Measure = m
                            };


                            //default values, if no Radial CSV files provided or if the points are not present in the files
                            double expOffsetRadial = 0;
                            double expOffsetVertical = 0;
                            double moveRadial = c.X.Value * -1; //negative of offset
                            double moveVertical = c.Z.Value * -1;

                            bool isRabotUsed = adminModule.RabotStrategy != TSU.Common.Compute.Rabot.Strategy.Not_Defined;

                            if (!isRabotUsed)
                            {
                                r.X = useMm ? c.X.ToString(d - 3, 1000) : c.X.ToString(d);
                                r.Y = useMm ? c.Y.ToString(d - 3, 1000) : c.Y.ToString(d);
                                r.Z = useMm ? c.Z.ToString(d - 3, 1000) : c.Z.ToString(d);
                                r.ExpO_Rbv_MM_ = "n/a";
                                r.Move_Rbv_MM_ = "n/a";
                                r.ExpO_Hbv_MM_ = "n/a";
                                r.Move_Hbv_MM_ = "n/a";
                            }
                            else
                            {
                                // Offset to nominal
                                if (typeD == DisplayTypes.Displacement)
                                {
                                    c.X.Value *= -1;
                                    c.Y.Value *= -1;
                                    c.Z.Value *= -1;
                                }

                                r.X = useMm ? c.X.ToString(d - 3, 1000) : c.X.ToString(d);
                                r.Y = useMm ? c.Y.ToString(d - 3, 1000) : c.Y.ToString(d);
                                r.Z = useMm ? c.Z.ToString(d - 3, 1000) : c.Z.ToString(d);


                                // Expected Offset
                                if (adminModule.RabotStrategy == C.Rabot.Strategy.Expected_Offset)
                                {
                                    //Separating Vertical and radial
                                    if (adminModule.RadialRabot.ContainsKey(item._Name))
                                    {
                                        expOffsetRadial = smoothR;
                                    }
                                    if (adminModule.VerticalRabot.ContainsKey(item._Name))
                                    {
                                        expOffsetVertical = smoothV;
                                    }

                                    moveRadial = expOffsetRadial - c.X.Value * 1000;
                                    moveVertical = expOffsetVertical - c.Z.Value * 1000;

                                    r.ExpO_Rbv_MM_ = new DoubleValue(expOffsetRadial).ToString(d - 3);
                                    r.Move_Rbv_MM_ = new DoubleValue(moveRadial).ToString(d - 3);
                                    r.ExpO_Hbv_MM_ = new DoubleValue(expOffsetVertical).ToString(d - 3);
                                    r.Move_Hbv_MM_ = new DoubleValue(moveVertical).ToString(d - 3);
                                }
                                else // Relative Displacement 
                                {
                                    double firstMeasureRadial = Tsunami2.Preferences.Values.na;
                                    double firstMeasureVertical = Tsunami2.Preferences.Values.na;

                                    List<M.Measure> pointMeasureList = s.GuidedModule._ActiveStationModule._Station.MeasuresTaken
                                    .GetList()
                                    .Where(measure => measure._PointName == item._Name && measure._Status.Type != M.States.Types.Bad)
                                    .ToList();

                                    Measure firstMeasure = pointMeasureList.FirstOrDefault() as Measure;
                                    if (!firstMeasure.UsedAsReference) //Is THIS NEEDED?
                                    {
                                        //If there is only one measure, this will be move = - RoughMinusSmooth
                                        if (pointMeasureList.Count == 1)
                                        {
                                            firstMeasureRadial = c.X.Value;
                                            firstMeasureVertical = c.Z.Value;
                                        }
                                        else if (pointMeasureList.Count > 1)
                                        {
                                            firstMeasureRadial = firstMeasure.Offsets.BeamV.X.Value;
                                            firstMeasureVertical = firstMeasure.Offsets.BeamV.Z.Value;
                                        }
                                    }

                                    if (adminModule.RadialRabot.ContainsKey(item._Name))
                                    {
                                        if (firstMeasureRadial != Tsunami2.Preferences.Values.na)
                                            expOffsetRadial = firstMeasureRadial * 1000 - roughMinusSmoothR;
                                    }
                                    if (adminModule.VerticalRabot.ContainsKey(item._Name))
                                    {
                                        if (firstMeasureRadial != Tsunami2.Preferences.Values.na)
                                            expOffsetVertical = firstMeasureVertical * 1000 - roughMinusSmoothV;
                                    }

                                    moveRadial = expOffsetRadial - c.X.Value * 1000;
                                    moveVertical = expOffsetVertical - c.Z.Value * 1000;

                                    r.ExpO_Rbv_MM_ = new DoubleValue(expOffsetRadial).ToString(d - 3);
                                    r.Move_Rbv_MM_ = new DoubleValue(moveRadial).ToString(d - 3);
                                    r.ExpO_Hbv_MM_ = new DoubleValue(expOffsetVertical).ToString(d - 3);
                                    r.Move_Hbv_MM_ = new DoubleValue(moveVertical).ToString(d - 3);

                                    //if there is no firstMeasure, override NA
                                    if (firstMeasureRadial == Tsunami2.Preferences.Values.na)
                                    {
                                        r.ExpO_Rbv_MM_ = "na";
                                        r.Move_Rbv_MM_ = "na";
                                    }
                                    if (firstMeasureVertical == Tsunami2.Preferences.Values.na)
                                    {
                                        r.ExpO_Hbv_MM_ = "na";
                                        r.Move_Hbv_MM_ = "na";
                                    }
                                }
                            }


                        }
                        results4Grid.Add(r);
                        // check if we want to grey the line
                        r.isRef = !Common.Analysis.Element.IsNameContainedIn(item._Name, s.GuidedModule.PointsToBeAligned);

                        count++;
                    };

                    // GRID
                    //var bindingList = new System.ComponentModel.BindingList<Result>(results4Grid);
                    //var bindingSource = new BindingSource(bindingList, null);
                    ////bindingSource.DataMember.
                    //grid.DataSource = bindingSource;
                    //grid.DataSource = results4Grid;


                    //if (indexOfElementfromThisStep != -1)
                    //grid.CurrentCell = grid.Rows[indexOfElementfromThisStep].Cells[0];

                    //{ // set heigth
                    //    int height = grid.ColumnHeadersHeight;
                    //    foreach (DataGridViewRow dr in grid.Rows)
                    //    {
                    //        height += dr.Height; // Row height.
                    //    }
                    //    height += grid.ColumnHeadersHeight; // for the empty row
                    //    grid.Height = height;
                    //}

                    // TREEGRID
                    TreeGrid_Update(treeGrid, results4Grid, Tsunami2.Properties.BOC_Context.ExistingResults, typeD, typeS, adminModule.RabotStrategy);
                    TreeGrid_HighLight(treeGrid, s.TAG);

                    //Grid.OnValidated(s.GuidedModule.View.grid, null);
                    //grid.Update();

                    //treeGrid.ResumeLayout(false);
                    //grid.ResumeLayout(false);
                };

                Update();
            }

            private static void TreeGrid_HighLight(Views.TreeGrid treeGrid, object tAG)
            {
                var measure = tAG as Measure;
                var source = treeGrid.FlattenCategories;
                int count = -1;
                foreach (CategorizedResult item in source)
                {
                    count++;
                    var polarMeasure = item.Measure as Measure;
                    if (polarMeasure == null)
                        continue;
                    if (polarMeasure._Point._Name == measure._Point._Name)
                    {
                        treeGrid.GridPart.Rows[count].DefaultCellStyle.BackColor = Tsunami2.Preferences.Theme.Colors.Highlight;
                        treeGrid.GridPart.Rows[count].DefaultCellStyle.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
                        break;
                    }
                }
            }

            private static void TreeGrid_Update(
                Views.TreeGrid treeGrid,
                List<Result> results4Grid, List<Results> bOC_Results,
                DisplayTypes typeD, string typeS, Common.Compute.Rabot.Strategy rabotStrategy)
            {
                treeGrid.SuspendLayout();
                // les add BOC results to existing measurement results
                var polarBocResults = bOC_Results.GetByObservationType(observationType: ObservationType.Polar);
                //var stakeOutResult = TreeGrid_AddBOCResults(results4Grid, polarBocResults, typeD, typeS);

                ITreeGridCategory categorisedResults = Steps.CategorizedResult.Categorize(results4Grid, polarBocResults, typeD, typeS, out List<ITreeGridCategory> magnets);

                treeGrid.CastToRelevantType = (selectedFlatenHierarchy) =>
                {
                    // we received the selected Hierarchy from the treeview and need to create the datasource for the gridview
                    List<CategorizedResult> selectedResults = new List<CategorizedResult>();
                    foreach (var item in selectedFlatenHierarchy)
                    {
                        selectedResults.Add(item as CategorizedResult);
                    }
                    return selectedResults;
                };

                // Add the dR and its sigma
                TreeGrid_AddRollToTheMagnetCategories(magnets, polarBocResults);
                treeGrid.DataSource = categorisedResults;
                treeGrid.HiddenColumns = CategorizedResult.GetColumnsNameToHide();
                bool rabotIsUsed = rabotStrategy != C.Rabot.Strategy.Not_Defined;
                bool offsetOrMoveIsUsed = Tsunami2.Properties.ValueToDisplayType == DisplayTypes.Offsets || Tsunami2.Properties.ValueToDisplayType == DisplayTypes.Displacement;
                if (!rabotIsUsed || (rabotIsUsed && !offsetOrMoveIsUsed))
                    treeGrid.HiddenColumns.AddRange(new List<string> { "ExpO_Rbv_MM_", "ExpO_Hbv_MM_", "Move_Rbv_MM_", "Move_Hbv_MM_" });


                treeGrid.Update(rabotIsUsed);
            }

            private static void TreeGrid_AddRollToTheMagnetCategories(List<ITreeGridCategory> magnetsResults, List<Results> results)
            {
                foreach (var result in results)
                {
                    foreach (var magnetResult in magnetsResults)
                    {
                        if (magnetResult is CategorizedResult cr)
                        {
                            if (result.Id == magnetResult.Name)
                            {
                                cr.dRoll_mrad = result.dRoll.ToString("mrad-mrad");
                                cr.sRoll_mrad = new DoubleValue(result.dRoll.Sigma).ToString("mrad-mrad");
                            }
                        }
                    }
                }
            }


            public static List<Coordinates> ShowResults(string typeS, DisplayTypes typeD, BigButton changeOffsetType, Step s, Measure lastMeasure, out string message)
            {
                List<Coordinates> result = new List<Coordinates>();

                Func<Measure, Coordinates> GetCoordinates = GetRightFunction(typeS, typeD, out string typeString, changeOffsetType, s);

                message = typeString;
                foreach (var item in s.GuidedModule.PointsToBeMeasured)
                {
                    Measure m = FindLastMeasureOf(item, s.GuidedModule.ReceivedMeasures);
                    if (m != null)
                    {
                        Coordinates c = GetCoordinates(m);
                        c._Name = item._Name;
                        result.Add(c);
                    }
                }
                return result;
            }


            public static Func<Measure, Coordinates> GetRightCoordinates(string typeS, DisplayTypes typeD, out string typeString)
            {
                Func<Measure, Coordinates> GetCoordinates = null;
                typeString = R.String_Unknown;

                switch (typeD)
                {
                    case DisplayTypes.Displacement:

                        typeString = $"{R.T_CST_DISPLACEMENT} {R.T_IN} {Tsunami2.Properties.CoordinatesSystems.GetByName(typeS).LongName}";
                        GetCoordinates = (m) =>
                        {
                            if (m.Offsets == null)
                            {
                                var newC = new Coordinates();
                                newC.Set();
                                return newC;
                            }

                            var c = m.Offsets.GetCoordinatesInASystemByName(typeS);

                            return (c == null) ? new Coordinates() : GetZeroMinusOffset(c);
                        };
                        break;

                    case DisplayTypes.Offsets:

                        typeString = $"{R.T_CST_OFFSETS} {R.T_IN} {Tsunami2.Properties.CoordinatesSystems.GetByName(typeS).LongName}";
                        GetCoordinates = (m) =>
                        {
                            if (m.Offsets == null)
                                return new Coordinates();
                            var c = m.Offsets.GetCoordinatesInASystemByName(typeS);
                            return (c == null) ? new Coordinates() : c;
                        };
                        break;

                    case DisplayTypes.Coordinates:
                    default:

                        typeString = $"{R.T_CST_COORDINATES} {R.T_IN} {Tsunami2.Properties.CoordinatesSystems.GetByName(typeS).LongName}";
                        GetCoordinates = (m) =>
                        {
                            if (m._Point == null || m._Point._Coordinates == null)
                                return new Coordinates();
                            var c = m._Point._Coordinates.GetCoordinatesInASystemByName(typeS);
                            return (c == null) ? new Coordinates() : c;
                        };
                        break;
                }
                return GetCoordinates;
            }

            private static Coordinates GetZeroMinusOffset(Coordinates beamV)
            {
                if (!beamV.AreKnown)
                    return beamV;

                return E.Coordinates.GetZeroZeroZeroWithSameInfoAs(beamV) - beamV;
            }

            public static Func<Measure, Coordinates> GetRightFunction(string typeS, DisplayTypes typeD, out string typeString, BigButton changeOffsetType, Step s)
            {
                switch (typeD)
                {
                    case DisplayTypes.Displacement:
                        switch (typeS)
                        {
                            case "BeamV": changeOffsetType.ChangeImage(R.Rotation); break;
                            case "Beam": changeOffsetType.ChangeImage(R.Rotation); break;
                            case "ToStation": changeOffsetType.ChangeImage(R.Rotation); break;
                            case "SU": changeOffsetType.ChangeImage(R.Rotation); break;
                            case "PHYSICIST": changeOffsetType.ChangeImage(R.Rotation); break;
                            case "CCS": changeOffsetType.ChangeImage(R.Rotation); break;
                            default: break;
                        }
                        break;
                    case DisplayTypes.Offsets:
                        switch (typeS)
                        {
                            case "BeamV": changeOffsetType.ChangeImage(R.Bearing); break;
                            case "Beam": changeOffsetType.ChangeImage(R.Bearing); break;
                            case "ToStation": changeOffsetType.ChangeImage(R.Bearing); break;
                            case "SU": changeOffsetType.ChangeImage(R.Rotation); break;
                            case "PHYSICIST": changeOffsetType.ChangeImage(R.Rotation); break;
                            case "CCS":
                            default: changeOffsetType.ChangeImage(R.Rotation); break;
                        }
                        break;
                    case DisplayTypes.Coordinates:
                    default:
                        switch (typeS)
                        {
                            case "BeamV": changeOffsetType.ChangeImage(R.Bearing); break;
                            case "Beam": changeOffsetType.ChangeImage(R.Bearing); break;
                            case "ToStation": changeOffsetType.ChangeImage(R.Theodolite); break;
                            case "PHYSICIST": changeOffsetType.ChangeImage(R.Rotation); break;
                            case "CCS": changeOffsetType.ChangeImage(R.Rotation); break;
                            case "SU":
                            default: changeOffsetType.ChangeImage(R.Rotation); break;
                        }
                        break;
                }
                return GetRightCoordinates(typeS, typeD, out typeString);
            }

            public static Measure FindLastMeasureOf(E.Point item, List<M.Measure> listMeasures, M.States.Types stateType = M.States.Types.Unknown, M.States.Types notStateType = M.States.Types.Unknown)
            {
                if (stateType == M.States.Types.Unknown)
                {
                    if (notStateType == M.States.Types.Unknown)
                        return listMeasures.FindLast(x => x._OriginalPoint._Name == item._Name) as Measure;
                    else
                        return listMeasures.FindLast(x => x._OriginalPoint._Name == item._Name && (notStateType & x._Status.Type) != x._Status.Type) as Measure;
                }
                else
                {
                    return listMeasures.FindLast(x => x._OriginalPoint._Name == item._Name && x._Status.Type == stateType) as Measure;
                }
            }


            //public static E.Coordinates FindOffsetPerNameOf(string name, MeasureTheodolite m, Guided.Module gm)
            //{
            //    if (m.Offsets != null)
            //    {
            //        E.Coordinates c = m.Offsets.Find(x => x.FrameName == name);
            //        if (c != null) return c;
            //    }
            //    return new E.Coordinates();
            //}



            public static void MoveToAlignmentStepOfAPoint(string pointName, Common.Guided.Module gm)
            {
                Debug.WriteInConsole($"Moving To AlignmentStepOfAPoint {pointName}");
                int index = 0;
                bool wePassedTheCompensationStep = false;
                foreach (Step step in gm.Steps)
                {
                    if (step.Id == "Compensation")
                        wePassedTheCompensationStep = true;

                    if (step.TAG is Measure m)
                    {
                        if (m._Point._Name == pointName && wePassedTheCompensationStep)
                        {
                            gm.MoveToStep(index, runIntermediateSteps: false);
                            return;
                        }
                    }
                    index++;
                }

                // no step found..
                CreatingMissingSteps(gm, pointName);

                Debug.WriteInConsole($"Moved To AlignmentStepOfAPoint {pointName}");
            }

            private static void CreatingMissingSteps(Common.Guided.Module gm, string pointName)
            {
                gm.CancelAllStepUpdate = true;

                bool stepReached = false;

                int count = 0;
                gm.targetStepIndex = 0;
                HourGlass.Set();

                try
                {
                    HourGlass.Set();
                    while (!stepReached)
                    {
                        count++;
                        Measurement.PrepareOrNotNextMeasurementStep(gm, gm.CurrentStep.TAG as Measure, Measurement.Types.StakeOut);
                        gm.MoveToNextStep(skipUpdate: true);
                        M.Measure m = gm.CurrentStep.TAG as Measure;
                        if (m != null)
                            if (m._Point._Name == pointName)
                            {
                                stepReached = true;
                                gm._TsuView.UpdateView();
                            }
                        if (count > 100)

                            break;
                    }
                    gm.targetStepIndex = -1;
                    gm.CancelAllStepUpdate = false;

                    gm.CurrentStep.Update();
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    HourGlass.Remove();
                }
            }


            internal static bool CheckIfSaved(Measure m, CloneableList<M.Measure> measuresTaken)
            {
                M.Measure existing = measuresTaken.Find(x => x._Point._Name == m._Point._Name && x._Status is M.States.Good);
                return existing != null;
            }
        }

    }
}
