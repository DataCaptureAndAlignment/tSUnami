﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSU.Polar.GuidedModules
{
    internal class ShowableAndEditablePolarMeasure
    {
        public Measure polarMeasure;

        public ShowableAndEditablePolarMeasure(Measure polarMeasure)
        {
            this.polarMeasure = polarMeasure;
        }

        public bool Done
        {
            get;
            set;
        } = false;

        public string Name
        {
            get
            {
                return polarMeasure._Point._Name;
            }
            set
            {
                //polarMeasure._OriginalPoint._Name = value;

            }
        }

        public double Extension
        {
            get
            {
                if (polarMeasure.Extension.Value!= Tsunami2.Preferences.Values.na)
                {
                    return polarMeasure.Extension.Value;
                }
                else
                {
                    if (polarMeasure._Point != null && polarMeasure._Point.SocketCode != null)
                        return polarMeasure._Point.SocketCode.DefaultExtensionForPolarMeasurement;
                    else
                        return 0;
                }

            }
            set
            {
                polarMeasure.Extension.Value = value;
            }
        }

        public string Comment
        {
            get
            {
                return polarMeasure.CommentFromUser;
            }
            set
            {
                polarMeasure.CommentFromUser = value;

            }
        }
        public string Code
        {
            get
            {
                if (polarMeasure._Point != null && polarMeasure._Point.SocketCode != null)
                    return polarMeasure._Point.SocketCode.Id;
                else
                    return "-";
            }
            set
            {
                //

            }
        }

        public string Reflector
        {
            get
            {
                return polarMeasure.Distance.Reflector._Name;
            }
            set
            {
                // nothing cause it is done with reflector manager
            }
        }
    }
}
