﻿using System.Collections.Generic;
using System;
using TSU.Views;
using System.Security.Policy;
using System.Windows.Forms;
using TSU.Common.Compute.Compensations.BeamOffsets;
using static TSU.Polar.GuidedModules.Steps.StakeOut;
using TSU.Common.Elements;
using static TSU.Polar.GuidedModules.Steps.StakeOut.Tools;
using Ionic.Crc;
using System.Linq;
using System.Runtime.CompilerServices;
using TSU.Functionalities.ManagementModules;
using TSU.Common.Compute.Compensations.Strategies;

namespace TSU.Polar.GuidedModules.Steps
{
    public class CategorizedResult : StakeOut.Tools.Result, ITreeGridCategory
    {
        public string Categories { get; set; }
        public override string Name { get; set; }

        public string FullStructureName
        {
            get
            {
                var parentFullStructureName = Parent != null ? Parent.FullStructureName : "";
                var name = Name != "" ? Name : Categories;
                return parentFullStructureName + ":" + name;
            }
        }

        public object Tag { get; set; }
        public List<ITreeGridCategory> SubCategories { get; set; } = new List<ITreeGridCategory>();
        public ITreeGridCategory Parent { get; set; }
        public bool IsCollapsed { get; set; }
        public bool Obsolete { get; set; } = false;

        public bool Highlighted { get; set; } = false;
        public bool Grayout { get; set; } = false;

        public string AssemblyId { get; set; } = "";
        public bool IsLeaf
        {
            get
            {
                if (SubCategories == null) return true;
                return SubCategories.Count == 0;
            }
        }


        public bool IsLastNodeOfALevel
        {
            get
            {
                if (Parent != null)
                    if (Parent.SubCategories[Parent.SubCategories.Count - 1] == this)
                        return true;
                return false;
            }
        }

        int depth;

        public int Depth
        {
            get
            {
                if (depth == 0)
                // compute it
                {
                    var parent = this.Parent;
                    while (parent != null)
                    {
                        depth++;
                        parent = parent.Parent;
                    }

                }
                return depth;
            }
        }
        public Guid Guid { get; set; } = Guid.NewGuid();

        public override string ToString()
        {
            return $"{Name}-{Categories} obsolete {isObsolete}";
        }

        public void Add(ITreeGridCategory subNode)
        {
            this.SubCategories.Add(subNode);
            subNode.Parent = this;
        }
        public static ITreeGridCategory Categorize(List<StakeOut.Tools.Result> measurement_Results, List<Results> bOC_Results, DisplayTypes typeD, string typeS, out List<ITreeGridCategory> magnets)
        {
            ITreeGridCategory categorizedResults = new CategorizedResult() { Name = "Categories" };
            magnets = new List<ITreeGridCategory>();
            var pointsNameAlreadyInserted = new List<string>();
            string unit = "";
            foreach (StakeOut.Tools.Result result in measurement_Results)
            {
                unit = result.Unit;
                var measure = result.Measure as Measure;
                bool isMeasured = measure != null;
                bool obsolete = !isMeasured ? false : Tsunami2.Properties.BOC_Context.ObsoleteMeasures.Contains(measure.Guid);
                CategorizedResult toBeCategorizedResult = new CategorizedResult()
                {
                    Extension = result.Extension,
                    Interfaces = result.Interfaces,
                    isRef = result.isRef,
                    Categories = result.isRef ? "Ref" : Point.GetPointAkaSocketName(result.Name),
                    Name = result.Name,
                    Reflector = result.Reflector,
                    type = result.type,
                    Unit = result.Unit,
                    X = result.X,
                    Y = result.Y,
                    Z = result.Z,
                    ExpO_Rbv_MM_ = result.ExpO_Rbv_MM_,
                    ExpO_Hbv_MM_ = result.ExpO_Hbv_MM_,
                    Move_Rbv_MM_ = result.Move_Rbv_MM_,
                    Move_Hbv_MM_ = result.Move_Hbv_MM_,

                    dRoll_mrad = result.dRoll_mrad,
                    sRoll_mrad = result.sRoll_mrad,
                    Obsolete = obsolete,
                    Highlighted = result.isHighlighted,
                    Grayout = result.isRef,
                    Measure = result.Measure,
                };
                if (!result.isRef)
                {
                    bool addMeasure;
                    {
                        var lastBOC = bOC_Results.LastOrDefault();
                        DateTime lastComputeDate = (lastBOC != null && lastBOC.ComputedPoints != null && lastBOC.ComputedPoints.Find(x => x._Name == result.Name) != null)?
                            lastBOC.Date : DateTime.MinValue;

                        DateTime lastMeasureDate = measure != null? measure._Date : DateTime.MinValue;

                        addMeasure = lastMeasureDate >= lastComputeDate; 
                    }
                    if (addMeasure)
                    {
                        categorizedResults.SubCategories.InsertInHierarchy(categorizedResults, toBeCategorizedResult, ref magnets);
                        pointsNameAlreadyInserted.Add(toBeCategorizedResult.Name);
                            }
                }
                else
                {
                    categorizedResults.Add(toBeCategorizedResult);
                }
            }

            // boc results
            // identify assemblies from measured Points
            List<string> ids = new List<string>();
            foreach (var item in measurement_Results)
            {
                if (!item.isRef)
                {
                    string input = Point.GetAssembly(item.Name);
                    if (!ids.Contains(input))
                    {
                        ids.Add(input);
                    }
                }
            }

            int d = TSU.Tsunami2.Preferences.Values.GuiPrefs == null ? 7 : TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
            bool useMm = typeD != DisplayTypes.Coordinates;

            // select points from assemblies
            
            foreach (var id in ids)
            {
                foreach (var bocResult in bOC_Results)
                {
                    if (bocResult.AssemblyId != id)
                        continue;
                    var points = bocResult.ComputedPoints;
                    foreach (var point in points)
                    {
                        if (pointsNameAlreadyInserted.Contains(point._Name))
                            continue;
                        Coordinates c = null;
                        if (typeD == DisplayTypes.Coordinates)
                            c = point.GetCoordinatesBasedOn(typeS);
                        else // offset or displacement
                        {
                            //Common.Compute.Compensations.BeamOffsets.WorkFlow.OffsetComputedInAllSystems.WaitOne(10000);
                            var bocOffsets = bocResult.Offsets.LastOrDefault(x => x.Item1 == point._Name);
                            if (bocOffsets.Item2 != null)
                                c = bocOffsets.Item2.GetCoordinatesInASystemByName(typeS);

                            if (c == null) // not sure if there is cases but...
                            {
                                var beamTheoreticalPoints = Common.Elements.Manager.Module.GetTheoreticalPointsWithEquivalentNames(points, beamPointsOnly: true);
                                var offsets = Common.Compute.Compensations.BeamOffsets.WorkFlow.GetOffsets(
                                    new List<Point>() { point }, beamTheoreticalPoints, stationPoint: null);
                                if (offsets != null && offsets.Count > 0)
                                {
                                    var offset = offsets[0].Item2;
                                    c = offset.ListOfAllCoordinates.GetByFrameName(typeS);
                                }
                            }
                        }

                        int multiplier = typeD == DisplayTypes.Displacement ? -1 : 1;
                        CategorizedResult toBeCategorizedResult = new CategorizedResult()
                        {
                            AssemblyId = id,
                            Name = point._Name,
                            Categories = Point.GetPointAkaSocketName(point._Name),
                            type = "Comp.",
                            Unit = unit,
                            X = useMm ? c?.X.ToString(d - 3, 1000 * multiplier) : c?.X.ToString(d, multiplier),
                            Y = useMm ? c?.Y.ToString(d - 3, 1000 * multiplier) : c?.Y.ToString(d, multiplier),
                            Z = useMm ? c?.Z.ToString(d - 3, 1000 * multiplier) : c?.Z.ToString(d, multiplier),

                            dRoll_mrad = bocResult.dRoll.Value.ToString("F3"),
                            sRoll_mrad = bocResult.dRoll.Sigma.ToString("F3"),

                            Obsolete = bocResult.Obsolete,
                            Highlighted = true,
                            Grayout = false,
                            //Tag = result.Tag,
                        };
                        pointsNameAlreadyInserted.Add(point._Name);
                        categorizedResults.SubCategories.InsertInHierarchy(categorizedResults, toBeCategorizedResult, ref magnets);
                    }
                }
            }
            return categorizedResults;
        }

        internal static List<string> GetColumnsNameToHide()
        {
            return new List<string>() { "Grayout", "AssemblyId", "isObsolete", "Highlighted", "assemblyId", "isHighlighted", "FullStructureName", "isRef", "Measure" };
        }
    }

    public static class CategorizedResultExtensions
    {
        public static void InsertInHierarchy<T>(this T src, ITreeGridCategory root, ITreeGridCategory result, ref List<ITreeGridCategory> magnets) where T : List<ITreeGridCategory>
        {
            string name = result.Name;

            // Pillars
            if (name.EndsWith("."))
            {
                src.Add(result);
                return;
            }

            bool isBeam = name.StartsWith("BEAM_");
            bool isE = name.EndsWith(".E");
            bool isS = name.EndsWith(".S");

            // select or create the assembly Node
            //  [+] Categories
            //      [+] AssemblyId
            var assemblyId = "";
            if (result is CategorizedResult cr)
            {
                if (cr.AssemblyId != "")
                {
                    assemblyId = cr.AssemblyId;
                }
                else
                {
                    //beam_a.b.c1.c2.dd => a.b.c
                    //a.b.c1.c2.dd => a.b.c
                    //a.b.c.dd => a.b.c
                    int lastDot = name.LastIndexOf('.');
                    assemblyId = name.Substring(0, lastDot);

                    if (isBeam)
                        assemblyId = assemblyId.Substring(5);
                }

                if (!src.Contains(assemblyId))
                // create
                {
                    CategorizedResult magnetResult = new CategorizedResult() { Name = assemblyId, Grayout = true };
                    magnetResult.Parent = root;
                    src.Add(magnetResult);
                    // save it as a magnet
                    magnets.Add(magnetResult);
                }

                bool isAssemblyMaster = name.Contains(assemblyId);


                //  [+] Categories
                //      [+] AssemblyId
                //          [+] .E
                ITreeGridCategory cMagnet = src.Get(assemblyId);

                if (cMagnet is CategorizedResult crm) // always, but to get dRoll for the magnet category
                {
                    if (cr.dRoll_mrad != null)
                    {
                        crm.dRoll_mrad = cr.dRoll_mrad;
                        crm.sRoll_mrad = cr.sRoll_mrad;
                    }
                    cr.dRoll_mrad = null;
                    cr.sRoll_mrad = null;
                }


                if (!isBeam)
                {
                    //  [+] Categories
                    //      [+] AssemblyId
                    //          [+] Sockets
                    var sockets = cMagnet.GetOrCreate("Sockets");

                    if (isE || isS)
                        sockets.Add(result);
                    else
                        sockets.GetOrCreate("...").Add(result);
                    return;
                }

                var beamPointsNodeName = "Beam Pts.";
                //  [+] Categories
                //      [+] AssemblyId
                //          [+] Sockets
                //          [+] Beam Points
                if (isBeam)
                {
                    var bpNode = cMagnet.GetOrCreate(beamPointsNodeName, highlighted: true);
                    if (isAssemblyMaster && (isE || isS))
                        bpNode.Add(result);
                    else
                        bpNode.GetOrCreate("...", highlighted: true).Add(result);
                    return;
                }

            }
        }

        public static bool Contains<T>(this T src, string name) where T : List<ITreeGridCategory>
        {
            return src.Find(x => x.Name == name || x.Categories == name) != null;
        }

        public static ITreeGridCategory Get<T>(this T src, string name) where T : List<ITreeGridCategory>
        {
            return src.Find(x => x.Name == name || x.Categories == name);
        }

        public static ITreeGridCategory GetOrCreate<T>(this T src, string name, bool highlighted = false, bool grayout = false) where T : ITreeGridCategory
        {
            if (!src.SubCategories.Contains(name))
                src.Add(new CategorizedResult() { Name = "", Categories = name, Highlighted = highlighted, Grayout = grayout });
            return src.SubCategories.Get(name);
        }

    }
}