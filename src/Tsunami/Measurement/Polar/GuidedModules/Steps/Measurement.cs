﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using TSU.Common.Compute;
using TSU.Common.Guided;
using TSU.Common.Guided.Steps;
using TSU.Common.Instruments;
using TSU.IO.SUSoft;
using TSU.Length.GuidedModules;
using TSU.Polar.GuidedModules.Steps;
using TSU.Views;
using TSU.Views.Message;
using UnitTestGenerator;
using E = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using R = TSU.Properties.Resources;

namespace TSU.Polar.GuidedModules.Steps
{
    public static class Measurement
    {

        public enum Types
        {
            Setup,
            Measurement,
            StakeOut,
            PreAlignment,
            EquiDistance,
        }

        public enum MeasureTypeOptions
        {
            Normal,
            Quick1F,
            Quick2F,
        }

        public enum MeasureGotoOptions
        {
            WithGoto,
            WithoutGoto,
        }

        /// <summary>
        /// Choose points from theorietcal file, it uses a modified "choose elements" step 
        /// </summary>
        /// <param name="gm"></param>
        /// <returns></returns>
        public static Step ChoosePointsToMeasure(Common.Guided.Module gm)
        {
            Step s = Management.ChoosePoints(gm,
                $"{R.T_SELECTION_OF_POINTS};{R.T_CHOOSE_A_POINT_OR_SEVERAL_POINTS_THE_NEXT_STEPS_WILL_BE_CREATED_BASED_ON_THIS_SELECTION_ORDER}.");
            s.Id = "ChoosePointsToMeasure";

            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.ElementManager._SelectedObjects.Clear();
                e.ElementManager.MultiSelection = true;
                e.ElementManager.View.CheckBoxesVisible = true;
                e.ElementManager.SetSelectableToAllUniquePoint();

                e.guidedModule.PointsToBeMeasured.ForEach(x => e.ElementManager._SelectedObjects.Add(x));
                e.step.View.AddControl(e.ElementManager);

                Station st = e.ActiveStation as Station;
                if (st.Parameters2._StationPoint != null)
                {
                    var a = e.ElementManager.SelectableObjects.FindAll(x => (x as E.Point)._Name == st.Parameters2._StationPoint._Name);
                    a.ForEach(x => e.ElementManager.SelectableObjects.Remove(x));
                }
                e.ElementManager.UpdateView();
            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {
                gm.PointsToBeMeasured.Clear();
                gm.ElementsTobeMeasured.Clear();

                if (s.State == StepState.Valid)
                {
                    e.guidedModule.PointsToBeMeasured.Clear();
                    e.ElementManager._SelectedObjects.ForEach(x => e.guidedModule.PointsToBeMeasured.Add(x as E.Point));
                }

                e.step.View.RemoveControl(e.ElementManager);
            };

            return s;
        }


        internal static Step ChooseReferencePoints(Common.Guided.Module gm)
        {
            Step s = ChoosePointsToMeasure(gm);
            s.Id = "ChoosePointsToMeasure";
            s._Name = R.T_CHOOSE_REFERENCE_POINTS;
            s.Utility = R.T_SELECT_THE_POINT_THAT_YOU_WANT_TO_MEASURE_AS_REFERENCE + "\r\n" + R.T_GM_T_CHOOSEREF;


            s.Entering += delegate (object source, StepEventArgs e)
            {
                // refs
                Common.Station st = e.ActiveStation as Common.Station;

                //? why to remove only the point fromthis station? 
                //var a = e.ElementManager.SelectableObjects.FindAll(x => (x as E.Point)._Name == st.Parameters2._StationPoint._Name);
                //a.ForEach(x=> e.ElementManager.SelectableObjects.Remove(x));

                e.ElementManager.SetSelectableToAllUniquePoint();

                e.ElementManager.View.buttons.ShowTypes.Available = true;
                e.ElementManager.View.buttons.ShowPiliers.Available = true;
                e.ElementManager.View.buttons.ShowPoints.Available = true;
                e.ElementManager.View.buttons.ShowMagnets.Available = true;
                e.ElementManager.View.buttons.ShowAll.Available = true;
                e.ElementManager.View.buttons.ShowAlesages.Available = true;
                e.ElementManager.View.buttons.clean.Available = true;
                e.ElementManager.View.buttons.sequence.Available = false;
                e.ElementManager.View.buttons.add.Available = true;

                e.ElementManager.UpdateView();
                s.Update();
            };

            // override/replace tester
            s.Testing -= s.Tester;
            s.Tester = (sender, e) =>
            {
                var positionKnown = (e.step.GuidedModule._ActiveStationModule._Station as Polar.Station).Parameters2.Setups.InitialValues.IsPositionKnown;
                var minimimNumberofRefTomeasure = positionKnown ? 1 : 2;
                bool enoughMeasured = e.ElementManager._SelectedObjects.Count >= minimimNumberofRefTomeasure;
                e.step.MessageFromTest = e.step.CheckValidityOf(enoughMeasured) ? "" : $"{minimimNumberofRefTomeasure} {R.T_GOOD_MEASURES_ARE_NEEDED}";
            };

            s.Testing += s.Tester;

            return s;
        }

        internal static Step AddPointLancé(Common.Guided.Module gm)
        {
            Step s = new Step(gm, R.T_TH_ADDPOINTLANCE);
            Station.Module stm = null;
            Common.Station st = null;

            s.Id = "AddPointLancé";

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                stm = s.GuidedModule._ActiveStationModule as Station.Module;
                st = stm.StationTheodolite;

                s.View.AddButton(
                    R.T_TH_ADDPOINTLANCE,
                    R.Add,
                    delegate
                    {
                        string pointLancéName = "";
                        bool newStyle = true;
                        if (newStyle)
                        {
                            pointLancéName = stm.GetAvailableStationName();
                        }
                        else
                        {
                            if (e.ElementManager.zone != null)
                                pointLancéName = e.ElementManager.zone.Ccs2MlaInfo.OriginName + ".STL.###";
                            else
                                pointLancéName = (e.ElementManager.GetAllTheoreticalPoints().Find(x => x is E.Point) as E.Point)._Zone + ".STL.###.";

                            pointLancéName = Common.Analysis.Element.Iteration.GetNext(pointLancéName, e.ElementManager.GetPointsNames(), Common.Analysis.Element.Iteration.Types.All);
                        }

                        string message = R.T_EM_NEW_POINT_LANCE;

                        E.Point p = e.ElementManager.AddNewUnknownPoint(pointLancéName, message);
                        p.State = E.Element.States.FromStation;
                        if (p != null)
                        {
                            e.guidedModule.AddElementToBeMeasured(p);
                            //Measurement.PrepareOrNotNextMeasurementStep(e.guidedModule, actualMeasure: null, type: Types.Measurement, offsetStep:2);
                        }
                    }
                );

            };

            s.Entering += delegate (object source, StepEventArgs e)
            {

            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {

            };

            s.Updating += delegate (object source, StepEventArgs e)
            {
                string measuresTodo = st.MeasuresToDo.ToString();
                e.step.View.ModifyLabel(measuresTodo, "MeasuresTodoAsText");
            };

            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                e.step.CheckValidityOf(st.MeasuresToDo.Count > 0);
            };

            return s;
        }

        /// <summary>
        /// Resume the choices (default param and point list) and prepare the first measurement Step
        /// </summary>
        internal static Step MeasurementAbstractBefore(Common.Guided.Module guidedModule, Types type, bool CreateNextStep = true)
        {

            Step s = new Step(guidedModule, R.S_MeasurementAbstractBefore);
            s.Id = "Abstract" + type.ToString();
            // Usefull references
            Station.Module stm = null;
            Station.View stmv = null;
            Common.Station st = null;
            TsuBool instrumentIsShown = new TsuBool(false); // tu be able to pass as reference

            DataGridView grid = null;

            Action RemoveAllMeasurementAndSteps = () =>
            {

                s.GuidedModule._ActiveStationModule._Station.ClearMeasuresToDo();
                if (s.GuidedModule.NextStep != null)
                    while (s.GuidedModule.NextStep != null)
                    {
                        if (s.GuidedModule.NextStep.TAG as M.Measure == null) break;
                        s.GuidedModule.RemoveNextStep(true);
                    }
                s.Update();
            };

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                stm = s.GuidedModule._ActiveStationModule as Station.Module;
                stmv = stm.View;
                st = stm.StationTheodolite;

                // button clear
                e.step.View.AddButton(stmv.buttons.MeasureDetails.GetClone() as BigButton, "mes");


                e.step.View.AddButton(
                    $"{R.T_SHOW_MORE_OF_THE_INSTRUMENT}",
                    R.Instrument,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        // here is a trick this option will trigger the showmessageinTSU in the Changed event, 
                        // because this windows cannot be set to showdialog or it will block the main thread 
                        // and at the same time if is not it will lose the focus if it is shown before the change event.
                        instrumentIsShown.IsTrue = true;
                    }
                );


                e.step.View.AddButton(
                    R.T_Clear_ALL_SCHEDULED_MEASURES,
                    R.Tda5005_Red_circle,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;

                        string cancel = R.T_CANCEL;
                        string titleAndMessage = $"{R.T_REMOVE_ALL_SCHEDULE_MEASUREMENT}?;{R.T_ARE_YOU_SURE_THAT_YOU_WANT_TO_REMOVE_EVERYTHING_THAT_YOU_PREVIOUSLY_SELECTED}? ";
                        MessageInput mi = new MessageInput(MessageType.Warning, titleAndMessage)
                        {
                            ButtonTexts = new List<string> { R.T_REMOVE, cancel }
                        };
                        if (cancel != mi.Show().TextOfButtonClicked)
                        {
                            s.GuidedModule._ElementManager._SelectedObjects.Clear();
                            s.GuidedModule._ElementManager._SelectedObjectInBlue = null;
                            s.GuidedModule.PointsToBeMeasured.Clear();
                            s.GuidedModule.ElementsTobeMeasured.Clear();
                            s.GuidedModule.PointsToBeAligned.Clear();
                            RemoveAllMeasurementAndSteps();
                            e.step.View.GridHasBeenRearanged = true;
                        }
                    }
                );

                // for safety set goto and normal mesure state
                PolarModule instModule = s.GuidedModule._ActiveStationModule._InstrumentManager.SelectedInstrumentModule as PolarModule;
                if (instModule != null)
                {
                    instModule.GotoWanted = true;
                    instModule.N_A_GotoMessageWanted = false;
                    instModule.QuickMeasureOneFaceWanted = false;
                    instModule.QuickMeasureTwoFaceWanted = false;
                }

                e.step.View.ModifyLabel("", "Station");
                e.step.View.ModifyLabel("", "TobeMeasured");
                grid = e.step.View.AddRearrangableGrid(grid, DockStyle.Top);
                e.step.View.ModifyLabel("", "AlreadyMeasured");
            };


            Action<bool> ReCreatedMeasureToDoListAndNextStep = (noQuestion) =>
            {
                bool UseSpecialCaseOfCustumMesasureSetted = false;
                if (s.GuidedModule.CustomMeasureToBeDone != null)
                    if (s.GuidedModule.CustomMeasureToBeDone.Count > 0)
                        UseSpecialCaseOfCustumMesasureSetted = true;

                if (UseSpecialCaseOfCustumMesasureSetted)
                {
                    s.GuidedModule.PointsToBeMeasured.Clear();
                    foreach (var item in s.GuidedModule.CustomMeasureToBeDone)
                    {
                        st.MeasuresToDo.Add(item);
                        s.GuidedModule.PointsToBeMeasured.Add(item._Point);
                    }
                }
                else
                {
                    CloneableList<E.Point> listToPlayWith;
                    M.State state;
                    if (type != Types.Measurement && type != Types.Setup)
                    {
                        listToPlayWith = s.GuidedModule.PointsToBeAligned;
                        state = new M.States.Questionnable();
                        noQuestion = true; // point from alignment should always
                    }
                    else
                    {
                        listToPlayWith = s.GuidedModule.PointsToBeMeasured;
                        state = new M.States.Unknown();
                    }

                    if (listToPlayWith.Count == 0)
                    {
                        // in some reopeed .tsu there isnot point to be aligned in the polar module:  s.GuidedModule.PointsToBeAligned
                        // lets get them back from the admin module
                        {
                            var subModuleNotMeasureing = s.GuidedModule.GroupModule.SubGuidedModules.FindAll(x => x.ObservationType == Common.ObservationType.Unknown);
                            if (subModuleNotMeasureing.Count == 1)
                            {
                                var adminModule = subModuleNotMeasureing[0];
                                s.GuidedModule.PointsToBeAligned.AddRange(adminModule.PointsToBeAligned);
                                listToPlayWith = s.GuidedModule.PointsToBeAligned;
                            }
                        }
                    }
                    if (listToPlayWith.Count != 0)
                    {
                        List<E.Point> toBeRemove = new List<E.Point>();
                        DsaFlag remeasure = new DsaFlag();
                        remeasure.State = noQuestion ? DsaOptions.Never : DsaOptions.Ask_to;
                        foreach (E.Point item in listToPlayWith.ReverseCopy())
                        {
                            bool AlreadyGood = false;
                            if (stm.Has(Station.Module.HavingTypes.LastGoodMeasureOf, item._Name, out Measure dummy))
                            {
                                if (guidedModule.NextStep.Id.Contains("AllRef")) // TSU-3464 Supprimer Message de "re-measure?" après faire "Previous step"
                                    AlreadyGood = false;
                                else
                                {
                                    string titleAndMessage = $"{R.T_GOOD_MEASURE_ALREADY_PRESENT};{R.T_DO_YOU_WANT_TO_REMEASURE} '{item._Name}' ?";
                                    MessageResult r = new MessageInput(MessageType.Warning, titleAndMessage)
                                    {
                                        ButtonTexts = CreationHelper.GetYesNoButtons(),
                                        DontShowAgain = remeasure
                                    }.Show();

                                    AlreadyGood = r.TextOfButtonClicked == R.T_NO;
                                }
                            }
                            if (!AlreadyGood)
                            {
                                guidedModule.AddElementToBeMeasured(item, silent: true);
                                if (st.MeasuresToDo.Find(x => x._OriginalPoint != null && x._OriginalPoint._Name == item._Name) == null)
                                    st.MeasuresToDo.InsertRange(0, stm.CreateMeasuresFromElement(item, state, silent: true));
                            }
                            else
                            {
                                toBeRemove.Add(item);
                            }
                        }
                        toBeRemove.ForEach(x => listToPlayWith.Remove(x));
                    }

                }
                if (CreateNextStep)
                    Measurement.PrepareOrNotNextMeasurementStep(s.GuidedModule, null, type);
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                RemoveAllMeasurementAndSteps();
                ReCreatedMeasureToDoListAndNextStep(false); // false to ask if remeasure point laready measured.
                e.step.View.GridHasBeenRearanged = true; // to force the update of the grid
                s.Update();
            };

            Action<Step> UpdateNextPointsGrid = (step) =>
            {
                try
                {

                    Common.Guided.Module gm = step.GuidedModule;
                    TSU.Common.Guided.Steps.View v = step.View;

                    if (v.GridHasBeenRearanged)
                    {

                        v.GridHasBeenRearanged = false;
                        List<pointwithCumul> pointsWithCumul = new List<pointwithCumul>();
                        if (type != Types.Measurement && type != Types.Setup)
                        {
                            foreach (var item in gm.PointsToBeAligned)
                            {
                                pointsWithCumul.Add(new pointwithCumul() { pointName = item._Name, cumul = item._Parameters.Cumul });
                            }
                        }
                        else
                        {
                            foreach (var item in gm.PointsToBeMeasured)
                            {
                                pointsWithCumul.Add(new pointwithCumul() { pointName = item._Name, cumul = item._Parameters.Cumul });
                            }
                        }

                        var bindingList = new System.ComponentModel.BindingList<pointwithCumul>(pointsWithCumul);
                        var bindingSource = new BindingSource(bindingList, null);

                        if (grid != null)
                        {
                            if (pointsWithCumul.Count > 0)
                            {
                                int itemIndex = v.rowIndexFromMouseDown;
                                int newPosition = v.rowIndexOfItemUnderMouseToDrop;
                                if (newPosition == -1) newPosition = 0;

                                // change pos in pointwithcumul
                                var item = pointsWithCumul[itemIndex];
                                pointsWithCumul.RemoveAt(itemIndex);

                                pointsWithCumul.Insert(newPosition, item);

                                // change pos in point to be measured real list
                                if (type != Types.Measurement && type != Types.Setup)
                                {
                                    var item2 = gm.PointsToBeAligned[itemIndex];
                                    gm.PointsToBeAligned.RemoveAt(itemIndex);
                                    gm.PointsToBeAligned.Insert(newPosition, item2);
                                }
                                else
                                {
                                    var item2 = gm.PointsToBeMeasured[itemIndex];
                                    gm.PointsToBeMeasured.RemoveAt(itemIndex);
                                    gm.PointsToBeMeasured.Insert(newPosition, item2);
                                }
                            }

                            grid.DataSource = bindingSource;
                            grid.AllowDrop = true;

                            { // set heigth
                                int height = grid.ColumnHeadersHeight;
                                foreach (DataGridViewRow dr in grid.Rows)
                                {
                                    height += dr.Height; // Row height.
                                }
                                height += grid.ColumnHeadersHeight; // for the empty row
                                grid.Height = height;
                            }

                            RemoveAllMeasurementAndSteps();
                            ReCreatedMeasureToDoListAndNextStep(true); // true for no question on existing points
                        }
                    }
                }
                catch (Exception ex)
                {
                    s.View.ShowMessageOfBug("Problem to update the small datagrid showing the points sequence", ex);
                }
            };

            s.Updating += delegate (object source, StepEventArgs e)
            {
                string l = "\r\n";
                l += "Station management:" + "\r\n";
                l += "-------------------" + "\r\n";
                l += e.ActiveStation.ToString("MANAGEMENT");
                l += "\r\n";
                l += "Station pre-setup:" + "\r\n";
                l += "------------------" + "\r\n";
                l += e.ActiveStation.ToString("SETUP");
                l += "\r\n";
                l += "\r\n";

                e.step.View.ModifyLabel(l, "Station");


                l = $"{R.T_POINTS_TO_MEASURE_NEXT}: " + "\r\n";
                l += "------------------------" + "\r\n";
                l += "\r\n";

                e.step.View.ModifyLabel(l, "ToBeMeasured");


                UpdateNextPointsGrid(e.step);

                // replaced by grid
                //string ps = "";
                //foreach (MeasureTheodolite item in st.MeasuresToDo.Reverse())
                //{
                //    ps = item._OriginalPoint._Name + "\r\n"+ ps;
                //}

                //l += ps+"\r\n";

                l = "\r\n";
                l += $"{R.T_POINTS_ALREADY_MEASURED_FROM_THIS_STATION}:" + "\r\n";
                l += "------------------------------------------" + "\r\n";
                l += "\r\n";

                foreach (E.Point item in st.PointsMeasured)
                {
                    l += item._Name + "\r\n";
                }

                e.step.View.ModifyLabel(l, "AlreadyMeasured");

                // put back the measure as questionnable to be compatoble with guided step align, because after live data it seems it become unknown

            };

            s.Changed += delegate (object source, StepEventArgs e)
            {
                ShowNotDialogInstrumentViewIsNecessery(instrumentIsShown, e);
            };

            s.Testing += delegate (object source, StepEventArgs e)
            {
                var step = e.step;
                bool ok =step.CheckValidityOf(e.InstrumentModule != null, errorMessage: R.T_INSTRUMENT_SEEMS_TO_BE_OFF);
                ok = step.UpdateValidityOf(currentlyValid: ok, resultOfAnExpression: (e.InstrumentModule.InstrumentIsOn), errorMessage: R.T_INSTRUMENT_SEEMS_TO_BE_OFF);
                ok = step.UpdateValidityOf(currentlyValid: ok, resultOfAnExpression: (e.guidedModule.PointsToBeMeasured.Count > 0), errorMessage: R.T_NOT_ENOUGH_POINTS);
            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {
                List<E.Point> toRemove = new List<E.Point>();
                foreach (var item in e.guidedModule.PointsToBeMeasured)
                {
                    if (st.MeasuresToDo.Find(x => x._OriginalPoint != null && x._OriginalPoint._Name == item._Name) == null)
                        toRemove.Add(item);
                }
                toRemove.ForEach(x => { e.guidedModule.PointsToBeMeasured.Remove(x); e.guidedModule.ElementsTobeMeasured.Remove(x); });
            };

            return s;
        }

        public class pointwithCumul
        {
            public string pointName { get; set; }
            public double cumul { get; set; }
        }

        /// <summary>
        /// Why not DIALOG? i turn it to dialog for TSU-3939
        /// </summary>
        /// <param name="instrumentIsShown"></param>
        /// <param name="e"></param>
        private static void ShowNotDialogInstrumentViewIsNecessery(TsuBool instrumentIsShown, StepEventArgs e)
        {

            try
            {
                bool needToBeShow = true;
                if (!instrumentIsShown.IsTrue) return;
                if (e.guidedModule._ActiveStationModule._InstrumentManager.SelectedInstrumentModule == null && e.guidedModule._ActiveStationModule._InstrumentManager.SelectedInstrument != null)
                {

                }

                if (e.guidedModule._ActiveStationModule._InstrumentManager.SelectedInstrumentModule.View.ParentView != null) needToBeShow = false;


                if (needToBeShow)
                {
                    e.guidedModule._ActiveStationModule._InstrumentManager.SelectedInstrumentModule.View.ShowInMessageTsu(
                            R.STM_SD_I, R.T_OK, () =>
                            {
                                instrumentIsShown.IsTrue = false;
                                e.guidedModule._ActiveStationModule._InstrumentManager.SelectedInstrumentModule.View.ParentView = null;
                                e.guidedModule._ActiveStationModule._InstrumentManager.SelectedInstrumentModule.View.OnRemovedFromView();

                                e.step.ControlChange();
                            }, null, null, null, null, null, null, showdialog: true); // Why not DIALOG? i turn it to dialog for TSU - 3939, it screw everythign if the view stay open during step changes
                }
                else
                {
                    e.guidedModule._ActiveStationModule._InstrumentManager.SelectedInstrumentModule.View.ParentView.BringToFront();
                }
            }
            catch (Exception ex)
            {
                string titleAndMessage = $"{R.T_NO_INSTRUMENT_SELECTED}?;{ex.Message}";
                new MessageInput(MessageType.Warning, titleAndMessage).Show();
            }
        }

        /// <summary>
        /// Will add a measurement step to measure the newt point, actualPoint: if null then next point is the first point of the _ToMeasurePointsSequence list
        /// </summary>
        internal static void PrepareOrNotNextMeasurementStep(Common.Guided.Module module, Measure actualMeasure, Types type = Types.Measurement, int offsetStep = 1)
        {
            var measures = module._ActiveStationModule._Station.MeasuresToDo;

            // No point? then return
            if (measures.Count < 1) return;

            // actual Index?
            int index = actualMeasure == null ? -1 : index = measures.IndexOf(actualMeasure); // -1 special case for the first point 

            // Last point? then return
            if (index >= measures.Count - 1) return;

            // Next visible step is already exisitng with the next point? then return
            if (module.NextVisibleStep != null)
                if (module.NextVisibleStep.TAG == measures[index + 1]) return;

            // Then lets create the next step
            Measure meas = measures[index + 1] as Measure;


            switch (type)
            {
                case Types.StakeOut: module.AddNewStepAfterCurrentOne(StakeOut.StakeOutNextPoint(module, meas), offsetStep); break;
                case Types.PreAlignment: module.AddNewStepAfterCurrentOne(StakeOut.PreAlignNextPoint(module, meas), offsetStep); break;
                case Types.EquiDistance: module.AddNewStepAfterCurrentOne(EquiDistance.EquiDistNextPoint(module, meas), offsetStep); break;
                case Types.Measurement:
                case Types.Setup:
                default: module.AddNewStepAfterCurrentOne(MeasureNextPoint(module, meas, type), offsetStep); break;
            }
        }

        static public Step MeasurementFace(Module guidedModule)
        {
            Step s = new Step(guidedModule, R.T261);

            s.Id = "MeasurementFace";

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                // Add button from station theodolite view
                Station.View v = e.ActiveStationModule._TsuView as Station.View;

                e.step.View.AddButton(v.buttons.RenamePoint.GetClone());
                e.step.View.AddButton(v.buttons.DefaultReflector.GetClone());
                e.step.View.AddButton(v.buttons.Extension.GetClone());

            };

            s.Entering += delegate (object source, StepEventArgs e) { };

            s.ControlChanged += delegate (object source, StepEventArgs e)
            {
                Station station = e.ActiveStation as Station;

                if (station.Parameters2.Setups.InitialValues.IsInstrumentHeightKnown == Station.Parameters.Setup.Values.InstrumentTypeHeight.Known)
                    e.step.View.AddDescription(R.T259 + station.Parameters2._InstrumentHeight + " m.");
                else
                    e.step.View.AddDescription(R.T260);
            };

            s.Testing += delegate (object source, StepEventArgs e) { };


            return s;
        }

        static public void SendMeasureToInstrument(Measure m, Station.Module stm)
        {
            // Send right measure to module and instrument
            if (m != null)
            {
                stm.View.usedMeasure = m; ;
                stm.SendMeasureToInstrument(m);
            }
        }

        static public Step ArrayAcquisition(Common.Guided.Module guidedModule)
        {
            return null;
        }


        /// <summary>
        /// Measure a given point and prepare the step for the follwing one
        /// the tag is storing the ref of the point to measure from the ActiveStation._ToMeasurePointsSequence
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <param name="pointName"></param>
        /// <param name="tag"></param>
        /// <returns></returns>
        static public Step MeasureNextPoint(Common.Guided.Module guidedModule, Measure measure, Types type)
        {
            Step s = MeasureNextPointGeneral(guidedModule, measure, type);


            s.Id = "MeasureNextPoint";

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                // Prepare next step?
                Measurement.PrepareOrNotNextMeasurementStep(s.GuidedModule, s.TAG as Measure);
            };
            #region Entering
            s.Entering += delegate (object source, StepEventArgs e)
            {
                // Prepare next step?
                Measurement.PrepareOrNotNextMeasurementStep(s.GuidedModule, s.TAG as Measure);
            };
            #endregion

            s.Updating += delegate (object source, StepEventArgs e)
            {

                WhatsNext(e.step, (s.GuidedModule._ActiveStationModule as Station.Module).StationTheodolite);
            };

            #region Testing
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                e.step.CheckValidityOf(e.step.State == StepState.Valid);

            };

            #endregion

            return s;
        }



        static public Step MeasureNextPointGeneral(Common.Guided.Module guidedModule, Measure measure, Types type)
        {
            PolarModule polarModule = null;
            TsuBool instrumentIsShown = new TsuBool(false);

            #region Definition
            string nameAndDescription;
            string pointName = measure._OriginalPoint._Name;
            switch (type)
            {
                case Types.Setup: nameAndDescription = $"{"RTo"}: {pointName};{R.T_REFERENCE_TAKING_ON} '{pointName}'"; break;
                case Types.StakeOut: nameAndDescription = $"{"SOo"}: {pointName};{R.T_STAKING_OUT_OF} '{pointName}'"; break;
                case Types.PreAlignment: nameAndDescription = $"{"Ao"}: {pointName};{R.T_ALIGNMENT} {R.T_OF} '{pointName}'"; break;
                case Types.EquiDistance:
                case Types.Measurement:
                default:
                    nameAndDescription = $"{"Mo"}: {pointName};{R.T_MEASUREMENT} {R.T_OF} '{pointName}'"; break;
            }

            Step s = new Step(guidedModule, nameAndDescription, measure);
            bool measuredSuccesfully = false;

            // Usefull references
            Station.Module stm = s.GuidedModule._ActiveStationModule as Station.Module;
            Station.View stmv = stm.View;
            Station st = stm.StationTheodolite;


            string buttonMessage = "";
            string buttonCtrlMessage = "";
            BigButton MeasureButton = null;
            BigButton cancelAcquisition = null;
            BigButton removeMeasurementAndStep = null;
            BigButton controlOpening = null;
            BigButton changeExtension = null;
            BigButton changeInterfacesButton = null;
            BigButton changeReflector = null;
            BigButton changeComment = null;
            BigButton moreOptions = null;
            List<Control> moreOptionsControls = new List<Control>();
            BigButton options = null;
            List<Control> measurementOptionsControls = new List<Control>();
            bool closingJustMeasuring = false;
            StepState saveStepState = StepState.ReadyToSkip;
            #endregion

            #region measurement delegate

            M.MeasurementEventHandler OnAcquisitionThreated = delegate (object source2, M.MeasurementEventArgs e2)
            {
                if (s.GuidedModule.CurrentStep != s) return;
                string stepPointName = (s.TAG as Measure)._Point._Name;
                string OpeningPointName = stm.GetOpeningMeasure()._Point._Name;
                string lastPointName = e2.Measure._Point._Name;
                if (lastPointName == stepPointName || lastPointName == OpeningPointName)
                {
                    stm.SendMeasureToInstrument(s.TAG as Measure);
                }
            };

            Func<Measure, bool> IsTheRightMeasure = (x) =>
            {
                if (x != null)
                    return x._Point._Name == (s.TAG as Measure)._Point._Name && x._OriginalPoint._Name == (s.TAG as Measure)._OriginalPoint._Name;
                else
                    return false;
            };

            Action<Measure> ShowButtonAsReady = (measureReadyToBeMeasured) =>
            {
                if (MeasureButton == null) return;
                // refs
                PolarModule instModule = s.GuidedModule._ActiveStationModule._InstrumentManager.SelectedInstrumentModule as PolarModule;
                Measure m = measureReadyToBeMeasured;
                Station.Parameters.Setup.Values setupValues = (s.GuidedModule._ActiveStationModule._Station as Station).Parameters2.Setups.BestValues;
                MeasureButton.EnableButton();
                if (controlOpening != null)
                {
                    controlOpening.EnableButton();
                    controlOpening.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Action);
                }
                //MeasureButton.Enabled = true;
                moreOptionsControls.ForEach(b => { b.Enabled = true; });

                // modify buttons look

                // Measurebutton
                {
                    string message = "";
                    Color color = TSU.Tsunami2.Preferences.Theme.Colors.Action;
                    Bitmap image = R.At40x_Meas;
                    //On ne veut pas changer le mesure bouton si c'est une mesure de fermeture
                    bool quickmeasure = s.GuidedModule.MeasureTypeOption == MeasureTypeOptions.Quick1F || s.GuidedModule.MeasureTypeOption == MeasureTypeOptions.Quick2F;

                    if (!closingJustMeasuring)
                    {
                        if (measuredSuccesfully)
                        {
                            string quickOrNot = quickmeasure ? " (quick)" : "";
                            message = $"Re-Measure{quickOrNot};{0} Measured Successfully"; image = R.StatusGood; color = TSU.Tsunami2.Preferences.Theme.Colors.Good;
                        }
                        else
                        {
                            // find out if goto and or quick measure
                            bool gotoAvailable = m == null ? false : m.HasCorrectedAngles;
                            bool gotoWanted = s.GuidedModule.MeasureGotoOption == MeasureGotoOptions.WithGoto;

                            if (gotoWanted && gotoAvailable && !quickmeasure)
                            {
                                message = "Goto and measure;Goto and measure {0}"; image = R.At40x_GotoAll; color = TSU.Tsunami2.Preferences.Theme.Colors.Action;
                            }
                            if (gotoWanted && gotoAvailable && quickmeasure)
                            {
                                message = "Goto and quick measure;Goto and quick measure {0}"; image = instModule.QuickMeasureOneFaceWanted ? R.QuickMeasure : R.quickmeasure2F; color = TSU.Tsunami2.Preferences.Theme.Colors.Action;
                            }
                            if (gotoWanted && !gotoAvailable && !quickmeasure)
                            {
                                message = "Measure;(N/A goto) Measure {0}"; image = R.At40x_Meas; color = TSU.Tsunami2.Preferences.Theme.Colors.Action;
                            }
                            if (gotoWanted && !gotoAvailable && quickmeasure)
                            {
                                message = "Quick measure;(N/A goto) Quick measure {0}"; image = instModule.QuickMeasureOneFaceWanted ? R.QuickMeasure : R.quickmeasure2F; color = TSU.Tsunami2.Preferences.Theme.Colors.Action;
                            }
                            if (!gotoWanted && !quickmeasure)
                            {
                                message = "Measure;Measure {0}"; image = R.At40x_Meas; color = TSU.Tsunami2.Preferences.Theme.Colors.Action;
                            }
                            if (!gotoWanted && quickmeasure)
                            {
                                message = "Quick measure;Quick measure {0}"; image = instModule.QuickMeasureOneFaceWanted ? R.QuickMeasure : R.quickmeasure2F; color = TSU.Tsunami2.Preferences.Theme.Colors.Action;
                            }
                        }
                        buttonMessage = string.Format(message, m._Point._Name);
                        MeasureButton.SetAttributes(buttonMessage, image, color);
                    }
                    else
                    {
                        /// to complete
                        controlOpening.SetAttributes($"{R.T_CONTROL_ON_THE_OPENING_POINT};{R.T_WILL_GOTO_AND_MEASURE_THE_OPENING_POINT_FIRST_GOOD_MEASURE_OF_THE_STATION}", R.StatusControl, TSU.Tsunami2.Preferences.Theme.Colors.Action);
                        s.State = saveStepState;
                    }
                }
                // option button
                {
                    if (options != null && instModule != null)
                    {
                        string text = "Normal meas.";
                        if (instModule.QuickMeasureOneFaceWanted)
                            text = "Q-1F meas.";
                        if (instModule.QuickMeasureTwoFaceWanted)
                            text = "Q-2F meas.";
                        if (instModule.GotoWanted)
                            text += " with Goto";
                        options.ChangeNameAndDescription($"{text} (Change);{R.T_GM_TH_CHANGE_OPTION}");
                    }
                }
            };

            Action<Measure> ShowButtonAsMeasuring = (measureBeingMeasured) =>
            {
                // easy ref 
                Measure m = measureBeingMeasured;

                if (m == null) return;
                MeasureButton.DisableButton();
                if (controlOpening != null)
                {
                    controlOpening.DisableButton();
                }
                moreOptionsControls.ForEach(b => { b.Enabled = false; });

                if (!closingJustMeasuring)
                {
                    // this is ths step of another measure?
                    if (IsTheRightMeasure(m))
                    {
                        // enabled cancel button and show measure button as busy for this point 
                        cancelAcquisition.EnableButton();
                        // buttonMessage = string.Format("{0} Measured Successfully", (m)._Point._Name);
                        buttonMessage = $"{R.T_MEASURING};{R.T_MEASURING} {m._OriginalPoint._Name}";
                    }
                    else
                    {
                        // buttonMessage = string.Format("Busy Measuring another point: {0}", m._OriginalPoint._Name);
                        buttonMessage = $"{R.T_BUSY_MEASURING_ANOTHER_POINT};{R.T_BUSY_MEASURING_ANOTHER_POINT}: {m._OriginalPoint._Name}";
                    }
                    MeasureButton.SetAttributes(buttonMessage, R.At40x_Orange, TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                }
                else
                {
                    buttonCtrlMessage = $"{R.T_MEASURING};{R.T_MEASURING} {m._OriginalPoint._Name}";
                    controlOpening.SetAttributes(buttonCtrlMessage, R.At40x_Orange, TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                }
                //s.Update();
                s.View.Refresh();
            };

            Action TryToSetup = () =>
            {
                // if not setup try to setup
                if (!stm.stationParameters._IsSetup)
                {
                    if (stm.stationParameters.Setups.InitialValues.IsPositionKnown)
                    {
                        if (st.MeasuresTaken.Count > 0)
                        {
                            stm.ChangeSetupStrategy(TSU.Common.Compute.Compensations.Strategies.List.OrientatedOnly_LGC2);
                            stm._SetupStrategy.Compute2(true, false);
                        }
                    }
                    else if (st.MeasuresTaken.FindAll(x => x._Status is M.States.Good).Count > 1)
                    {
                        stm.ChangeSetupStrategy(TSU.Common.Compute.Compensations.Strategies.List.FreeStation_LGC2);
                        stm._SetupStrategy.Compute2(true, false);
                    }
                }
            };

            M.MeasurementEventHandler OnTreatedMeasureReceived = delegate (object source2, M.MeasurementEventArgs e2)
            {
                TSU.Debug.WriteInConsole($"OnTreatedMeasureReceived by step {s.NameAndDescription}");

                // easy ref 
                Measure m2 = e2.Measure as Measure;
                if (m2 == null)
                    return;

                // we dont want to change the state of the step just because the last measure is not the same anymore
                Measure m = null;
                if (m2.IsControl)
                {
                    m = Common.Analysis.Measure.GetLastByStateException<M.States.Control>(s.GuidedModule.ReceivedMeasures);
                    s.State = saveStepState;
                }
                else
                {
                    m = m2;
                    if (IsTheRightMeasure(m))
                    {
                        // show measure result
                        s.View.ModifyLabel(
                            string.Format("\r\n" + R.PolarMeasure_R, m.Face, m._Point._Name, m.Distance.Reflector._Name, m.Extension.Value,
                            m.Angles.Corrected.Horizontal.ToString(4), m.Angles.Corrected.Vertical.ToString(4), m.Distance.Corrected.ToString(6)), "doneMeasurementDescription",
                            TSU.Tsunami2.Preferences.Theme.Colors.Good);

                        if (!(s.GuidedModule.GroupModule != null && s.GuidedModule.GroupModule.Type == ENUM.GuidedModuleType.Alignment))
                            measuredSuccesfully = true;

                        // if stake out point coordinates
                        //s.View.ModifyLabel(m._Point.ToString(E.CoordinatesInAllSystems.StringFormat.BeamStnLrv), "CoordinatePointMeasured",
                        //     TSU.Tsunami2.Preferences.Theme.Colors.Good);
                    }
                    TryToSetup();
                }
                ShowButtonAsReady(s.TAG as Measure);
                s.Update();
            };

            M.MeasurementEventHandler OnStartMeasurement = delegate (object source2, M.MeasurementEventArgs e2)
            {
                Measure m = e2.BeingMeasured as Measure;
                MeasureButton.DisableButton();
                cancelAcquisition.EnableButton();
                removeMeasurementAndStep.DisableButton();
                if (controlOpening != null)
                {
                    controlOpening.DisableButton();
                }
                measuredSuccesfully = false;
                s.State = StepState.ReadyToSkip;
                ShowButtonAsMeasuring(e2.BeingMeasured as Measure);
                s.View.Refresh();
            };

            M.MeasurementEventHandler OnSubMeasurementStarted = delegate (object source2, M.MeasurementEventArgs e)
            {
                Measure m = e.BeingMeasured as Measure;
                MeasureButton.DisableButton();
                if (controlOpening != null && closingJustMeasuring)
                {
                    controlOpening.DisableButton();
                    MeasureButton.DisableButton();
                    string message;
                    if (m != null)
                    {
                        message = string.Format("{0}, {1}/{2} of {3}",
                            buttonCtrlMessage,
                            1 + m.NumberOfMeasureToAverage - (e.InstrumentModule as PolarModule).NumberOfMeasureOnThisFace,
                            m.NumberOfMeasureToAverage,
                            m.Face);
                    }
                    else
                        message = R.T_MEASURING;
                    controlOpening.SetAttributes(message, R.At40x_Orange, TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                }
                if (!closingJustMeasuring)
                {
                    string message;
                    if (m != null)
                    {
                        message = string.Format("{0}, {1}/{2} of {3}",
                            buttonMessage,
                            1 + m.NumberOfMeasureToAverage - (e.InstrumentModule as PolarModule).NumberOfMeasureOnThisFace,
                            m.NumberOfMeasureToAverage,
                            m.Face);
                    }
                    else
                        message = R.T_MEASURING;
                    MeasureButton.SetAttributes(message, R.At40x_Orange, TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                }
                else
                {
                    string message;
                    if (m != null)
                    {
                        message = string.Format("{0}, {1}/{2} of {3}",
                            buttonCtrlMessage,
                            1 + m.NumberOfMeasureToAverage - (e.InstrumentModule as PolarModule).NumberOfMeasureOnThisFace,
                            m.NumberOfMeasureToAverage,
                            m.Face);
                    }
                    else
                        message = R.T_MEASURING;
                    controlOpening.SetAttributes(message, R.At40x_Orange, TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                }
                if (s.View.InvokeRequired)
                    s.View.BeginInvoke(new System.Action(() => s.View.Refresh()));
                else
                    s.View.Refresh();
            };

            EventHandler OnInstrumentReady = delegate (object source2, EventArgs e)
            {
                MeasureButton.EnableButton();
                if (controlOpening != null)
                {
                    controlOpening.EnableButton();
                    controlOpening.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Action);
                }
                if (MeasureButton != null)
                {
                    ShowButtonAsReady(s.TAG as Measure);
                }
            };

            M.MeasurementEventHandler OnMoving = delegate (object source2, M.MeasurementEventArgs e)
            {
                MeasureButton.DisableButton();
                if (controlOpening != null && closingJustMeasuring)
                {
                    controlOpening.DisableButton();
                    MeasureButton.DisableButton();
                    string message = string.Format("{0}, {1}",
                        buttonCtrlMessage, "Moving");
                    controlOpening.SetAttributes(message, R.At40x_Orange, TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                }
                else
                {
                    string message = string.Format("{0}, {1}",
                        buttonMessage, "Moving");
                    MeasureButton.SetAttributes(message, R.At40x_Orange, TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                }

            };

            M.MeasurementEventHandler OnLocking = delegate (object source2, M.MeasurementEventArgs e)
            {
                Measure m = e.BeingMeasured as Measure;
                MeasureButton.DisableButton();
                if (controlOpening != null && closingJustMeasuring)
                {
                    controlOpening.DisableButton();
                    MeasureButton.DisableButton();
                    string message;
                    if (m != null)
                    {
                        message = string.Format("{0}, {1}/{2} of {3}",
                            buttonCtrlMessage,
                            1 + m.NumberOfMeasureToAverage - (e.InstrumentModule as PolarModule).NumberOfMeasureOnThisFace,
                            m.NumberOfMeasureToAverage,
                            m.Face);
                    }
                    else
                        message = R.T_MEASURING;
                    controlOpening.SetAttributes(message + " (Locking)", R.At40x_Orange, TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                }
                else
                {
                    string message;
                    if (m != null)
                    {
                        message = string.Format("{0}, {1}/{2} of {3}",
                            buttonMessage,
                            1 + m.NumberOfMeasureToAverage - (e.InstrumentModule as PolarModule).NumberOfMeasureOnThisFace,
                            m.NumberOfMeasureToAverage,
                            m.Face);
                    }
                    else
                        message = R.T_MEASURING;
                    MeasureButton.SetAttributes(message + " (Locking)", R.At40x_Orange, TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                }
            };

            M.MeasurementEventHandler OnChangingFace = delegate (object source2, M.MeasurementEventArgs e)
            {
                MeasureButton.DisableButton();
                if (controlOpening != null && closingJustMeasuring)
                {
                    controlOpening.DisableButton();
                    MeasureButton.DisableButton();
                    string message = string.Format("{0}, {1}",
                    buttonCtrlMessage, R.T_CHANGING_FACE);
                    controlOpening.SetAttributes(message, R.At40x_Orange, TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                }
                else
                {
                    string message = string.Format("{0}, {1}",
                        buttonMessage, R.T_CHANGING_FACE);
                    MeasureButton.SetAttributes(message, R.At40x_Orange, TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                }

            };

            M.MeasurementEventHandler OnMeasurementFinished = delegate (object source2, M.MeasurementEventArgs e2)
            {
                // easy ref 
                Measure m = e2.JustMeasured as Measure;

                PreparePolarModuleForOptions(polarModule, s.GuidedModule);

                guidedModule.InvokeOnApplicationDispatcher(MeasureButton.EnableButton);
                if (!(m._Status is M.States.Questionnable))
                {
                    if (!measuredSuccesfully)
                    {
                        guidedModule.InvokeOnApplicationDispatcher(MeasureButton.EnableButton);
                        if (controlOpening != null)
                        {
                            guidedModule.InvokeOnApplicationDispatcher(MeasureButton.EnableButton);
                        }
                    }
                    removeMeasurementAndStep.DisableButton();
                }
                ShowButtonAsReady(s.TAG as Measure);
                cancelAcquisition.DisableButton();
                removeMeasurementAndStep.EnableButton();
                closingJustMeasuring = false;
                s.Update();
            };

            M.MeasurementEventHandler OnMeasurementCancelled = delegate (object source2, M.MeasurementEventArgs e2)
            {
                // easy ref 
                Measure m = e2.ToBeMeasured as Measure;

                PreparePolarModuleForOptions(polarModule, s.GuidedModule);

                if (m == null) return;
                //if (IsTheRightMeasure(m))
                //{
                //    cancelAcquisition.DisableButton();
                //}
                cancelAcquisition.DisableButton();
                MeasureButton.EnableButton();
                removeMeasurementAndStep.EnableButton();
                if (controlOpening != null)
                {
                    controlOpening.EnableButton();
                    controlOpening.SetAttributes($"{R.T_CONTROL_ON_THE_OPENING_POINT};{R.T_WILL_GOTO_AND_MEASURE_THE_OPENING_POINT_FIRST_GOOD_MEASURE_OF_THE_STATION}", R.StatusControl, TSU.Tsunami2.Preferences.Theme.Colors.Action);
                }
                ShowButtonAsReady(s.TAG as Measure);
                closingJustMeasuring = false;
            };

            M.MeasurementEventHandler OnMeasurementFailed = delegate (object source2, M.MeasurementEventArgs e2)
            {
                // easy ref 
                Measure failed = e2.JustMeasured as Measure;

                if (failed == null) return;
                PreparePolarModuleForOptions(polarModule, s.GuidedModule);

                if (IsTheRightMeasure(failed))
                {
                    cancelAcquisition.DisableButton();
                    MeasureButton.EnableButton();
                    if (controlOpening != null)
                    {
                        controlOpening.EnableButton();
                    }
                    if (!closingJustMeasuring)
                    {
                        MeasureButton.SetAttributes($"{failed._Point._Name} {R.T_MEASUREMENT_FAILED_RE_MEASURE}", R.StatusBad, TSU.Tsunami2.Preferences.Theme.Colors.Bad);
                    }
                    else
                    {
                        controlOpening.SetAttributes($"{R.T_CONTROL_ON_THE_OPENING_POINT};{R.T_WILL_GOTO_AND_MEASURE_THE_OPENING_POINT_FIRST_GOOD_MEASURE_OF_THE_STATION}", R.StatusControl, TSU.Tsunami2.Preferences.Theme.Colors.Action);
                    }
                }
                else // we are not in the step that launches the measurement.
                {
                    Measure fromStep = s.TAG as Measure;
                    if (!closingJustMeasuring) //
                    {
                        ShowButtonAsReady(s.TAG as Measure);


                        if (fromStep._Point._Name != failed._Point._Name)
                        {
                            new MessageInput(MessageType.Critical, $"{R.T_MEASUREMENT_FAILED};{failed._Point._Name} not masured.").Show();

                            // changed because of TSU-3943
                            //string yes = R.T_YES;
                            //string titleAndMessage = $"{R.T_PLEASE_CONFIRM};{R.T_THE_INSTRUMENT_WAS_ALREADY_PREPARE_TO_MEASURE} {fromStep._Point._Name}, {R.T_DO_YOU_WANT_TO_REMEASURE} {failed._Point._Name}?";
                            //MessageInput mi = new MessageInput(MessageType.Warning, titleAndMessage)
                            //{
                            //    ButtonTexts = new List<string> { yes, fromStep._Point._Name }
                            //};
                            //string r = mi.Show().TextOfButtonClicked;
                            //if (r == yes)
                            //{
                            //    stm.SendMeasureToInstrument(failed);
                            //    polarModule.Measure();
                            //}
                            //else
                            //    stm.SendMeasureToInstrument(fromStep);
                        }
                    }
                    else
                    {
                        cancelAcquisition.DisableButton();
                        MeasureButton.EnableButton();
                        stm.SendMeasureToInstrument(fromStep);
                        new MessageInput(MessageType.Critical, "Control failed!; To try again hit the bottom 'Control' button, not the top 'Measure' button")
                        {
                            ButtonTexts = new List<string> { R.T_UNDERSTOOD }
                        }.Show();
                        if (controlOpening != null)
                        {
                            controlOpening.EnableButton();
                        }
                        controlOpening.SetAttributes($"{R.T_CONTROL_ON_THE_OPENING_POINT};{R.T_WILL_GOTO_AND_MEASURE_THE_OPENING_POINT_FIRST_GOOD_MEASURE_OF_THE_STATION}", R.StatusControl, TSU.Tsunami2.Preferences.Theme.Colors.Action);
                    }
                }
                removeMeasurementAndStep.EnableButton();
                closingJustMeasuring = false;

            };

            Action<Measure> ModifyMeasureButton = delegate (Measure activeMeasure)
            {
                try
                {
                    PolarModule instModule = s.GuidedModule._ActiveStationModule._InstrumentManager.SelectedInstrumentModule as PolarModule;

                    bool measuring = instModule.State == null ? false : instModule.State.Value == InstrumentState.type.Measuring;

                    if (measuring)
                        OnStartMeasurement(null, new M.MeasurementEventArgs(activeMeasure));
                    else
                        ShowButtonAsReady(s.TAG as Measure);

                    s.Update();
                }
                catch (Exception ex)
                {
                    //   throw new Exception("Problem during the drawing of 'the measure button'" + "r\n" + ex.Message);
                    throw new Exception(R.T_PROBLEM_DURING_THE_DRAWING_OF_THE_MEASURE_BUTTON + "r\n" + ex.Message, ex);
                }
            };

            #endregion

            Action<PolarModule> UnSubscribe = (im) =>
            {
                im.StartingMeasurement -= OnStartMeasurement;
                im.FinishingMeasurement -= OnMeasurementFinished;
                im.MeasurementFailed -= OnMeasurementFailed;

                im.SubMeasurementStarted -= OnSubMeasurementStarted;
                im.LockingForTarget -= OnLocking;
                im.Moving -= OnMoving;
                im.InstrumentReady -= OnInstrumentReady;
                im.ChangingFace -= OnChangingFace;
                im.MeasureAcquiring -= OnSubMeasurementStarted;
                im.MeasurementCancelled -= OnMeasurementCancelled;
            };

            Action<PolarModule> Subscribe = (im) =>
            {
                UnSubscribe(im); // to avoid to 2 suvscribed twice.

                im.StartingMeasurement += OnStartMeasurement;
                im.FinishingMeasurement += OnMeasurementFinished;
                im.MeasurementFailed += OnMeasurementFailed;
                im.SubMeasurementStarted += OnSubMeasurementStarted;

                im.LockingForTarget += OnLocking;
                im.Moving += OnMoving;
                im.InstrumentReady += OnInstrumentReady;
                im.ChangingFace += OnChangingFace;
                im.MeasureAcquiring += OnSubMeasurementStarted;
                im.MeasurementCancelled += OnMeasurementCancelled;
            };

            #region step events

            #region FirstUse

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                // set up point measurement
                if (st.MeasuresToDo.Count < 1) return;

                if (e.step.TAG == null) e.step.TAG = st.MeasuresToDoCome;
                Measure m = e.step.TAG as Measure;
                var stepView = e.step.View;

                // Set behaviours
                e.step.UnValidate();

                // add controls
                stm.MeasureThreated += OnTreatedMeasureReceived;

                stepView.ModifyLabel(string.Format(R.T267, m._Point._Name), "Title");

                stepView.ModifyLabel(st.ToString(m), "Description");

                polarModule = stm._InstrumentManager.SelectedInstrumentModule as PolarModule;

                // Reflector button
                {
                    changeReflector = stmv.buttons.Reflector.GetClone();
                    changeReflector.BigButtonClicked += delegate
                    {
                        s.Update();
                    };
                }
                // extension
                {
                    if (changeExtension != null) changeExtension.Dispose();
                    changeExtension = new BigButton($"Extension={m.Extension.Value}m;{R.T_CHANGE_EXTENTION}", R.Rallonge);
                    changeExtension.BigButtonClicked += delegate
                    {
                        stmv.ChangeExtension(m.Extension);
                        s.Update();
                    };
                }
                // Comment
                {
                    changeComment = stmv.buttons.ChangeComment.GetClone();
                    changeComment.BigButtonClicked += delegate
                    {
                        if (measuredSuccesfully)// this means the mesaure have already happened in this step
                        {
                            string change = $"{R.T_CHANGE} {R.T_PREVIOUS}";
                            string titleAndMessage =
                                $"{R.T_ALREADY_MEASURED};{R.T_DO_YOU_WANT_TO_CHANGE_THE_COMMENT_OF_THE_PREVIOUS_MEASUREMENT}";
                            MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                            {
                                ButtonTexts = { change, R.T_NEXT }
                            };

                            if (mi.Show().TextOfButtonClicked == change)
                            {
                                M.Measure previous = s.GuidedModule._ActiveStationModule._Station.MeasuresTaken.Last(x => x._OriginalPoint._Name == m._OriginalPoint._Name);

                                previous.CommentFromTsunami = m.CommentFromTsunami;
                                previous.CommentFromUser = m.CommentFromUser;
                            }
                        }
                    };
                }
                // Interfaces
                {
                    changeInterfacesButton = new BigButton("Select different interfaces", R.Interfaces, () =>
                    {
                        stmv.SelectInterfaces(m.Interfaces, m, null, null, additionalAction: () =>
                        {
                            s.Update();
                            stm.SendMeasureToInstrument(m);
                        });
                    });
                    changeInterfacesButton.HasSubButtons = true;
                }
                // Cancel aquisition ablility
                {
                    if (cancelAcquisition != null) cancelAcquisition.Dispose();
                    cancelAcquisition = new BigButton($"{R.T_CANCEL};{R.T_CANCEL_THE_ACQUISITION_IN_PROGRESS}", R.MessageTsu_Problem, TSU.Tsunami2.Preferences.Theme.Colors.Bad);
                    cancelAcquisition.BigButtonClicked += delegate
                    {
                        e.InstrumentModule.CancelMeasureInProgress();

                    };
                    if (s.TAG!=null)
                        ShowButtonAsReady(s.TAG as Measure);
                    cancelAcquisition.DisableButton();
                }
                // Remove point measurement ability
                {
                    if (removeMeasurementAndStep != null) removeMeasurementAndStep.Dispose();
                    removeMeasurementAndStep = new BigButton($"{R.T_REMOVE};{R.T_CANCEL_THE_MEASUREMENT_OF_THIS_POINT_THIS_IS_DEFINITIVE}", R.MessageTsu_Problem, TSU.Tsunami2.Preferences.Theme.Colors.Bad);
                    removeMeasurementAndStep.BigButtonClicked += delegate
                    {
                        string titleAndMessage = R.T_REMOVE + " ?";
                        MessageInput mi = new MessageInput(MessageType.Critical, titleAndMessage)
                        {
                            ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                        };
                        string respond = mi.Show().TextOfButtonClicked;
                        if (respond == R.T_NO)
                            return;
                        if (m != null)
                        {
                            st.RemoveMeasureFromToDoList(m);
                        }

                        e.guidedModule.PointsToBeMeasured.Remove(m._OriginalPoint);
                        e.guidedModule.ElementsTobeMeasured.Remove(m._OriginalPoint);
                        e.guidedModule.PointsToBeAligned.Remove(m._OriginalPoint);
                        foreach (Control ctrl in s.View.Controls)
                        {
                            if (!(ctrl is DataGridView))
                                ctrl.Enabled = false;
                        }
                        guidedModule.InvokeOnApplicationDispatcher(MeasureButton.DisableButton);
                        if (controlOpening != null) guidedModule.InvokeOnApplicationDispatcher(controlOpening.DisableButton);
                        //MeasureButton.Enabled = false;
                        s.stepsRemovedByUser = true;
                        s.State = StepState.Valid;
                    };
                }
                // Control Opening
                {
                    if (controlOpening != null) controlOpening.Dispose();
                    controlOpening = new BigButton($"{R.T_CONTROL_ON_THE_OPENING_POINT};{R.T_WILL_GOTO_AND_MEASURE_THE_OPENING_POINT_FIRST_GOOD_MEASURE_OF_THE_STATION}", R.StatusControl);
                    controlOpening.isSpecial = true; // in this case it will not trigger a step controlChange that would have reset the next point to the one of the step instead of keeping the control point // ok but then the next if control is succes is not the step one but the first of the tobemeasure list....
                    controlOpening.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Action);
                    controlOpening.BigButtonClicked += delegate
                                            {
                                                if (MeasureButton != null)
                                                {
                                                    MeasureButton.DisableButton();
                                                }
                                                closingJustMeasuring = true;
                                                saveStepState = s.State;
                                                controlOpening.DisableButton();
                                                controlOpening.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                                                DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShowMeasureDifferences").State = DsaOptions.Never; //stm.flags.ShowMeasureDifferences = false;
                                                (stm._InstrumentManager.SelectedInstrumentModule as PolarModule).GotoWanted = true;
                                                stm.SendMeasureToInstrument(stm.GetOpeningMeasure(), new M.States.Control());
                                                stm.AskIntrumentToMeasure();
                                            };
                }
                // Measure
                {
                    if (MeasureButton != null) MeasureButton.Dispose();
                    MeasureButton = new BigButton(R.STM_B_M, R.At40x_Measure);

                    MeasureButton.BigButtonClicked += delegate
                    {
                        if (measuredSuccesfully) // ok it was already measure in the step without leaving it
                        {
                            M.Measure previous = s.GuidedModule._ActiveStationModule._Station.MeasuresTaken.LastOrDefault(
                                x => x._OriginalPoint._Name == m._OriginalPoint._Name
                                &&
                                x._Status is M.States.Good);
                            if (previous != null)
                            {
                                string r;
                                string titleAndMessage1 = $"{R.T_ALREADY_MEASURED};{R.T_THERE_IS_ALREADY_A_MEASURE_CONSIDERED_AS_GOOD}, {R.T_MEASURE_AGAIN}?";
                                MessageInput mi1 = new MessageInput(MessageType.Warning, titleAndMessage1)
                                {
                                    ButtonTexts = new List<string> { R.T_CANCEL, R.T_Re_measure }
                                };
                                r = mi1.Show().TextOfButtonClicked;

                                if (r == R.T_CANCEL) return;
                                string titleAndMessage = $"{R.T_TURN_PREVIOUS_TO_BAD}?;{R.T_TURN_PREVIOUS_TO_BAD}";
                                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                                {
                                    ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                                };
                                r = mi.Show().TextOfButtonClicked;

                                if (r == R.T_YES)
                                {
                                    previous = s.GuidedModule._ActiveStationModule._Station.MeasuresTaken.Last(x => x._OriginalPoint._Name == m._OriginalPoint._Name);
                                    previous._Status = new M.States.Bad();
                                }
                            }
                        }
                        closingJustMeasuring = false;
                        if (controlOpening != null) controlOpening.DisableButton();
                        PreparePolarModuleForOptions(polarModule, guidedModule);
                        stmv.Measure();
                        //s.View.TryAction(()=> { stm._InstrumentManager.Measure(); });
                    };
                }
                // measurement options
                {
                    if (options != null) options.Dispose();
                    options = new BigButton($"{R.T_MEASURE} {R.T_OPTIONS};{R.T_GM_TH_CHANGE_OPTION}", R.Instrument, hasSubButtons:true);
                    options.BigButtonClicked += delegate
                    {
                        //foreach (BigButton item in optionsControls)
                        //{
                        //    if (item != null) item.Dispose();
                        //}

                        measurementOptionsControls.Clear();

                        { // type by default
                            BigButton defaultTypeOfMeasurement = new BigButton(
                                $"{R.T_NORMAL_MEASURE};{R.T_RESTORE_THE_MODE_OF_MEASURE_PREVIOUSLY_SELECTED} ",
                                R.Tda5005_ALL, delegate (object source2, TsuObjectEventArgs args2)
                                {
                                    s.GuidedModule.MeasureTypeOption = MeasureTypeOptions.Normal;
                                    PreparePolarModuleForOptions(polarModule, s.GuidedModule);
                                    ShowButtonAsReady(s.TAG as Measure);
                                }, null, null, false);
                            measurementOptionsControls.Add(defaultTypeOfMeasurement);
                        }
                        { // type quick 2 face
                            BigButton quickBut2Face = new BigButton(
                                $"{R.T_QUICK_2F};{R.T_QUICK_2F_DETAILS}",
                                R.quickmeasure2F, delegate (object source2, TsuObjectEventArgs args2)
                                {
                                    s.GuidedModule.MeasureTypeOption = MeasureTypeOptions.Quick2F;
                                    PreparePolarModuleForOptions(polarModule, s.GuidedModule);
                                    ShowButtonAsReady(s.TAG as Measure);
                                }, null, TSU.Tsunami2.Preferences.Theme.Colors.Action, false);
                            measurementOptionsControls.Add(quickBut2Face);
                        }
                        { // type quick 1 face
                            BigButton quick = new BigButton(
                                $"{R.T_QUICK};{R.T_QUICK_DETAILS}",
                                R.QuickMeasure, delegate (object source2, TsuObjectEventArgs args2)
                                {
                                    s.GuidedModule.MeasureTypeOption = MeasureTypeOptions.Quick1F;
                                    PreparePolarModuleForOptions(polarModule, s.GuidedModule);
                                    ShowButtonAsReady(s.TAG as Measure);
                                }, null, TSU.Tsunami2.Preferences.Theme.Colors.Action, false);
                            measurementOptionsControls.Add(quick);
                        }
                        { // do goto if available
                            BigButton doGoto = new BigButton(
                                R.T_DO_A_GOTO_BEFORE_MEASUREMENT_IF_POSSIBLE,
                                R.At40x_Goto, delegate (object source2, TsuObjectEventArgs args2)
                                {
                                    s.GuidedModule.MeasureGotoOption = MeasureGotoOptions.WithGoto;
                                    PreparePolarModuleForOptions(polarModule, s.GuidedModule);
                                    ShowButtonAsReady(s.TAG as Measure);
                                }, null, null, false);
                            measurementOptionsControls.Add(doGoto);
                        }
                        { // dont do goto
                            BigButton dontDoGoto = new BigButton(
                                R.T_DO_NOT_DO_A_GOTO_BEFORE_MEASUREMEN,
                                R.At40x_NoGoto, delegate (object source2, TsuObjectEventArgs args2)
                                {
                                    s.GuidedModule.MeasureGotoOption = MeasureGotoOptions.WithoutGoto;
                                    PreparePolarModuleForOptions(polarModule, s.GuidedModule);
                                    ShowButtonAsReady(s.TAG as Measure);
                                }, null, null, false);
                            measurementOptionsControls.Add(dontDoGoto);
                        }
                        measurementOptionsControls.Add(changeExtension);
                        measurementOptionsControls.Add(changeInterfacesButton);
                        measurementOptionsControls.Add(changeComment);
                        measurementOptionsControls.Add(changeReflector);

                        e.step.View.ShowPopUpMenu(measurementOptionsControls, "options");
                    };
                }
                // more instrument options
                {
                    string instrumentName = "Instrument";
                    try
                    {
                        instrumentName = st.ParametersBasic._Instrument._Model;
                    }
                    catch (Exception)
                    {
                    }

                    string buttonText = $"{R.T_MORE_ACTION_FROM_INSTRUMENT} ({instrumentName});Control opening + {R.T_TAKE_CONTROL_OF_THE_INSTRUMENT_MEASURE_GOTO_JOG_ETC}";
                    moreOptions = new BigButton(buttonText, R.Instrument);
                    moreOptions.BigButtonClicked += delegate
                    {
                        moreOptionsControls.Clear();
                        moreOptionsControls.Add(controlOpening);
                        moreOptionsControls.Add(removeMeasurementAndStep);
                        BigButton gotoButton = null;
                        { // Add goto button
                            E.Point PointToStakeOut = (s.TAG as Measure)._OriginalPoint;

                            IMotorized motorizedInstrument = stm._InstrumentManager.SelectedInstrumentModule as IMotorized;
                            if (motorizedInstrument != null)
                            {
                                gotoButton = new BigButton(
                                    $"{R.T_GOTO} '{PointToStakeOut._Name}';{R.T_GOTO_DETAILS}",
                                    R.At40x_Goto,
                                    delegate (object source2, TsuObjectEventArgs args2)
                                    {
                                        motorizedInstrument.Goto();
                                    }, null, null, false);
                                //ludr.Add(gotoButton);
                            }
                            if (gotoButton != null)
                                moreOptionsControls.Add(gotoButton);
                        } // Add goto button

                        { // add buttons for jog
                            bool showJogButtons = false;
                            if (e.InstrumentModule is I.Device.AT40x.Module) showJogButtons = true;

                            if (showJogButtons)
                            {
                                I.Device.AT40x.Module at = e.InstrumentModule as I.Device.AT40x.Module;

                                // creating zones for jog buttons
                                moreOptionsControls.Add(new BigButton($"{R.T_UP};{R.T_MOVE_THE_INSTRUMENT_UP}", R.Up, () => { at._motor.MovebyDirection(ENUM.movingDirection.Up); }, null, true));
                                moreOptionsControls.Add(new BigButton($"{R.T_LEFT};{R.T_MOVE_THE_INSTRUMENT_TO_THE_LEFT}", R.Left, () => { at._motor.MovebyDirection(ENUM.movingDirection.Left); }, null, true));
                                moreOptionsControls.Add(new BigButton($"{R.T_RIGHT};{R.T_MOVE_THE_INSTRUMENT_TO_THE_RIGHT}", R.Right, () => { at._motor.MovebyDirection(ENUM.movingDirection.Right); }, null, true));
                                moreOptionsControls.Add(new BigButton($"{R.T_DOWN};{R.T_MOVE_THE_INSTRUMENT_DOWN}", R.Down, () => { at._motor.MovebyDirection(ENUM.movingDirection.Down); }, null, true));

                                foreach (BigButton item in moreOptionsControls)
                                {
                                    item.PreviewKeyDown += at.View.jogView.On_TsuView_PreviewKeyDown;
                                }

                                // refresh button 

                                moreOptionsControls.Add(new BigButton($"{"Refresh AT status"};{"Problems? try me, it can help..."}", R.Update, () => { at.View.ClearStateAndUpdate(); }, null, true));
                            }
                        } // add buttons for jog 

                        { // advanced instrument view
                            BigButton advanced = null;

                            advanced = new BigButton(
                                R.T_SHOW_MORE_OF_THE_INSTRUMENT,
                                R.Instrument, delegate (object source2, TsuObjectEventArgs args2)
                                {
                                    // here is a trick this option will trigger the showmessageinTSU in the Changed event, 
                                    // because this windows cannot be set to showdialog or it will block the main thread 
                                    // and at the same time if is not it will lose the focus if it is shown before the change event.
                                    instrumentIsShown.IsTrue = true;
                                    e.step.ControlChange();
                                }, null, null, false);
                            moreOptionsControls.Add(advanced);

                        } // advanced instrument view
                        e.step.View.ShowPopUpMenu(moreOptionsControls, "ludr");
                    };
                    moreOptions.HasSubButtons = true;
                }


                List<Control> topRowButtons = new List<Control>
                {
                    MeasureButton,
                    cancelAcquisition,
                    options,
                    moreOptions
                };

                if (s.GuidedModule.quickMeasureAllowed && s.GuidedModule._ActiveStationModule._Station.ParametersBasic._IsSetup)
                    e.step.View.AddSidedControls(topRowButtons, DockStyle.Top, new List<int>() { 25, 25, 25, 25 });
                else
                    e.step.View.AddSidedControls(topRowButtons, DockStyle.Top);
            
                s.View.ModifyLabel("No coord.", "CoordinatePointMeasured", color: Color.Gray, bChangeDockStyle: true, dockstyle: DockStyle.Bottom);
                s.View.ModifyLabel("No Meas", "doneMeasurementDescription", color: Color.Gray, bChangeDockStyle: true, dockstyle: DockStyle.Bottom);

                stm.AcquisitionTreated += OnAcquisitionThreated;

            };
            #endregion



            #region Entering
            s.Entering += delegate (object source, StepEventArgs e)
            {
            measuredSuccesfully = false; // in case of simple measurement the step would have dessapeared, and in case of pre algn if we come back it is to remeasure.
                                         // to disable the measure buttons during a measurement
                                         //   if (e.InstrumentModule == null) throw new Exception("No instrument selected");
            if (e.InstrumentModule == null)
            {
                new MessageInput(MessageType.Critical, R.T_NO_INSTRUMENT_SELECTED).Show();
            }

            Subscribe(e.InstrumentModule as PolarModule);

            e.step.Update();

            SendMeasureToInstrument(s.TAG as Measure, stm);

            if (e.InstrumentModule is ILiveData)
            {
                (e.InstrumentModule as ILiveData).LiveData.HasStopped += delegate
                {
                    ResendMeasureTaggedInTheStop(e.step, stm);
                };
            }

            // 
            M.Measure active =
                s.GuidedModule._ActiveStationModule._InstrumentManager.SelectedInstrumentModule.BeingMeasured == null ?
                s.GuidedModule._ActiveStationModule._InstrumentManager.SelectedInstrumentModule.ToBeMeasuredData :
                s.GuidedModule._ActiveStationModule._InstrumentManager.SelectedInstrumentModule.BeingMeasured;

            ModifyMeasureButton(active as Measure);
        };
            #endregion
            #region Subscribing
            s.SubscribingEvents += delegate (object source, StepEventArgs e)
            {
                if (e.InstrumentModule != null)
                    UnSubscribe(e.InstrumentModule as PolarModule);
            };
            #endregion
            #region UnSubscribing
            s.UnSubscribingEvents += delegate (object source, StepEventArgs e)
            {
                if (e.InstrumentModule != null)
                    UnSubscribe(e.InstrumentModule as PolarModule);
            };
            #endregion
            #region Changed
            s.ControlChanged += delegate (object source, StepEventArgs e)
            {
                if (e.InstrumentModule != null)
                    if (e.InstrumentModule.BeingMeasured == null)
                        ResendMeasureTaggedInTheStop(e.step, stm);


            };
            #endregion

            #region Updating

            s.Updating += delegate (object source, StepEventArgs e)
            {
                Measure m = e.step.TAG as Measure;

                s.View.ModifyLabel(st.ToString(m), "Description");

                if (m != null)
                {
                    changeExtension.ChangeNameAndDescription($"Extension={m.Extension.Value}m;{R.T_CHANGE_EXTENTION}");
                    //s.View.ModifyLabel(st.ToString(m), "Description");
                }
            };

            s.Changed += delegate (object source, StepEventArgs e)
            {
                ShowNotDialogInstrumentViewIsNecessery(instrumentIsShown, e);
            };

            #endregion

            #region Leaving

            s.Leaving += delegate (object source, StepEventArgs e)
            {
                if (e.step.State == StepState.Valid)
                {
                    Measure m = e.step.TAG as Measure;
                    e.guidedModule.PointsToBeMeasured.Remove(m._OriginalPoint);
                    e.guidedModule.ElementsTobeMeasured.Remove(m._OriginalPoint);

                    st.RemoveMeasureFromToDoList(m);

                    e.step.Visible = false;
                    e.guidedModule.RemoveCurrent();

                }

                UnSubscribe(e.InstrumentModule as PolarModule);

                stm.MeasureThreated -= OnTreatedMeasureReceived;

                // Removing the step if the measurement was sucecssful
                if (s.State == StepState.Valid)
                    s.GuidedModule.RemoveCurrent();
            };
            #endregion


            #endregion

            #region Desappearing
            s.Desappearing += delegate (object source, StepEventArgs e)
            {
                stm.MeasureThreated -= OnTreatedMeasureReceived;
                e.InstrumentManager.SelectedInstrumentModule.MeasurementAvailable -= e.step.MeasureReceived;
            };
            return s;
            #endregion

        }

        private static void ResendMeasureTaggedInTheStop(Step step, Station.Module stm)
        {
            if (stm.StationTheodolite.MeasuresToDo.Count < 1) return;
            Measure m = step.TAG as Measure;

            if (m != null)
                stm.SendMeasureToInstrument(m);
        }


        private static void PreparePolarModuleForOptions(PolarModule polarModule, Common.Guided.Module guidedModule)
        {

            switch (guidedModule.MeasureTypeOption)
            {
                case MeasureTypeOptions.Quick1F:

                    polarModule.QuickMeasureOneFaceWanted = true;
                    polarModule.QuickMeasureTwoFaceWanted = false;
                    break;
                case MeasureTypeOptions.Quick2F:
                    polarModule.QuickMeasureOneFaceWanted = false;
                    polarModule.QuickMeasureTwoFaceWanted = true;
                    break;
                case MeasureTypeOptions.Normal:
                default:
                    polarModule.QuickMeasureOneFaceWanted = false;
                    polarModule.QuickMeasureTwoFaceWanted = false;
                    break;
            }

            switch (guidedModule.MeasureGotoOption)
            {
                case MeasureGotoOptions.WithoutGoto:
                    polarModule.GotoWanted = false;
                    polarModule.N_A_GotoMessageWanted = true;
                    break;
                case MeasureGotoOptions.WithGoto:
                default:
                    polarModule.GotoWanted = true;
                    polarModule.N_A_GotoMessageWanted = false;
                    break;
            }
        }

        public static BigButton GetRestartButton(StepEventArgs e)
        {
            return new BigButton(
                        $"{R.T_RESTART} {R.T_THE} {R.T_STATION};{R.T_TURN_ALL_MEASURE_STATE_TO_BAD_AND_REMEASURE_THE_SAME_POINT_PLEASE_CHECK_THE_STABILITY_AND_THE_LEVELLING_OF_YOUR_INSTRUMENT}",
                        R.Update, () => { RestartSetup(e); }, TSU.Tsunami2.Preferences.Theme.Colors.Attention, false);
        }

        private static void RestartSetup(StepEventArgs e)
        {
            Common.Guided.Module gm = e.guidedModule;

            // ask confirmation
            {
                var restart = R.T_RESTART;
                string titleAndMessage = $"{restart} {R.T_THE} {R.T_STATION} ?;{R.T_ARE_YOU_SURE_OU_WANT_TO_TURN_ALL_MEASURE_TO_BAD_AND_REMEASURE_EVERYTHING}";
                MessageInput mi = new MessageInput(MessageType.Important, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_CANCEL, restart },
                    TextOfButtonToBeLocked = restart
                };
                if (R.T_CANCEL == mi.Show().TextOfButtonClicked)
                {
                    return;
                }
            }

            Station st = e.ActiveStation as Station;
            List<E.Point> points = new List<E.Point>();

            foreach (var item in st.MeasuresTaken)
            {
                // record the point measured
                if (item._Status is M.States.Good && st.References.Contains(item))
                    points.Add(item._OriginalPoint);

                // turn the old one to bad
                item._Status = new M.States.Bad(item._Status.Type);
                item.GeodeRole = Geode.Roles.Unused;
            }
            var stParams = st.Parameters2;
            var stSetups = stParams.Setups;
            stParams.Setups.FinalValues = null;
            // ask if goto was ok
            {
                string titleAndMessage = $"{R.T_KEEP_ORIENTATION}?;{R.T_KEEP_ORIENTATION_DETAILS}?";
                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_Keep, R.T_Reject }
                };
                if (R.T_Reject == mi.Show().TextOfButtonClicked)
                {
                    // if free station, remove coordinates of the station
                    if (!stSetups.InitialValues.IsPositionKnown)
                    {
                        stParams._StationPoint._Coordinates = new E.CoordinatesInAllSystems();
                    }
                    stSetups.TemporaryValues = null;

                }
                else
                    stSetups.TemporaryValues = stSetups.BestValues;
            }
            gm.MeasureTypeOption = MeasureTypeOptions.Normal;
            PreparePolarModuleForOptions(gm._ActiveStationModule._InstrumentManager.SelectedInstrumentModule as PolarModule, gm);


            stSetups.FinalValues = null;
            gm.PointsToBeMeasured.Clear();
            gm.PointsToBeMeasured.AddRange(points);

            gm.MoveToStep("ChoosePointsToMeasure", false);
        }

        static public Step Closure(Common.Guided.Module gm)
        {
            Step s = new Step(gm, R.T_GS_CLOSESTATION);
            string closureId = "Closure";
            List<Step> existingClosureSteps = gm.Steps.FindAll(x => x.Id.Contains(closureId));
            s.Id = closureId + (existingClosureSteps.Count + 1);

            // Usefull references
            Station.Module stm = null;
            Station.View stmv = null;
            Station st = null;
            string buttonMessage = null;
            BigButton MeasureButton = null;
            TsuBool instrumentIsShown = new TsuBool(false); // tu be able to pass as reference
            Measure stepMeasure = null;            // Delcaration of event handler codes
            M.MeasurementEventHandler OnMeasureReceived = delegate (object source2, M.MeasurementEventArgs e2)
            {
                if (stepMeasure != null)
                {
                    if (e2.Measure._Point._Name == stepMeasure._Point._Name)
                    {
                        s.Update();
                        s.ControlChange();
                    }
                }
            };

            M.MeasurementEventHandler OnStartMeasurement = delegate (object source2, M.MeasurementEventArgs e2)
            {
                MeasureButton.Enabled = false;
                //s.MeasureReceived += OnMeasureReceived;
                buttonMessage = $"{R.T_MEASURING} {e2.BeingMeasured._Point._Name}";
                MeasureButton.SetAttributes(buttonMessage, R.At40x_Meas, TSU.Tsunami2.Preferences.Theme.Colors.Attention);
            };

            M.MeasurementEventHandler OnMeasurementFinished = delegate (object source2, M.MeasurementEventArgs e2)
            {
                MeasureButton.Enabled = true;
                //if (e2.Measure._Status is MeasureStateGood) e2.Measure._Status = new MeasureStateTemporary();
                //s.MeasureReceived -= OnMeasureReceived;
                buttonMessage = $"{e2.JustMeasured._Point._Name} R.T_MEASURED";
                MeasureButton.SetAttributes(buttonMessage, R.StatusGood, TSU.Tsunami2.Preferences.Theme.Colors.Good);
                if (st.MeasuresToDo.Count > 0)
                    if (st.MeasuresToDoCome._Point._Name == e2.JustMeasured._Point._Name)
                        st.RemoveMeasureFromToDoListAt(0);
                s.Update();

                s.ControlChange();
            };

            M.MeasurementEventHandler OnMeasurementCancelled = delegate (object source2, M.MeasurementEventArgs e2)
            {
                // easy ref 
                Measure m = e2.ToBeMeasured as Measure;

                if (m == null) return;
                MeasureButton.Enabled = true;
                MeasureButton.SetAttributes(buttonMessage, R.StatusBad, TSU.Tsunami2.Preferences.Theme.Colors.Bad);
            };

            M.MeasurementEventHandler OnMeasurementFailed = delegate (object source2, M.MeasurementEventArgs e2)
            {
                MeasureButton.Enabled = true;
                //s.MeasureReceived -= OnMeasureReceived;
                buttonMessage = $"{e2.JustMeasured._Point._Name} {R.T_MEASUREMENT_FAILED_REMEASURE}?";
                if (st.MeasuresToDo.Count > 0)
                    if (st.MeasuresToDoCome._Point._Name == e2.JustMeasured._Point._Name)
                        st.RemoveMeasureFromToDoListAt(0);
                MeasureButton.SetAttributes(buttonMessage, R.StatusBad, TSU.Tsunami2.Preferences.Theme.Colors.Bad);
            };

            M.MeasurementEventHandler OnSubMeasurementStarted = delegate (object source2, M.MeasurementEventArgs e)
            {
                string message = string.Format("{0}, {1}/{2} of {3}",
                    buttonMessage,
                    1 + (e.BeingMeasured as Measure).NumberOfMeasureToAverage - (e.InstrumentModule as PolarModule).NumberOfMeasureOnThisFace,
                    (e.BeingMeasured as Measure).NumberOfMeasureToAverage,
                    (e.BeingMeasured as Measure).Face);
                MeasureButton.SetAttributes(message, R.At40x_Orange, TSU.Tsunami2.Preferences.Theme.Colors.Attention);
            };

            Action<I.Module> UnSubscribe = (im) =>
            {
                if (im == null) return;
                im.StartingMeasurement -= OnStartMeasurement;
                im.FinishingMeasurement -= OnMeasurementFinished;
                im.MeasurementFailed -= OnMeasurementFailed;
                im.MeasurementFailed -= OnMeasurementCancelled;

                (im as PolarModule).SubMeasurementStarted -= OnSubMeasurementStarted;
            };

            Action<I.Module> Subscribe = (im) =>
            {
                if (im == null) return;
                UnSubscribe(im); // to avoid to 2 suvscribed twice.

                im.StartingMeasurement += OnStartMeasurement;
                im.FinishingMeasurement += OnMeasurementFinished;
                im.MeasurementFailed += OnMeasurementFailed;
                im.MeasurementCancelled += OnMeasurementCancelled;

                (im as PolarModule).SubMeasurementStarted += OnSubMeasurementStarted;
            };

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                buttonMessage = "";
                stm = s.GuidedModule._ActiveStationModule as Station.Module;
                stmv = stm.View;
                st = stm.StationTheodolite;

                stm.MeasureThreated += OnMeasureReceived;

                //e.step.MeasureReceived += OnMeasureReceived;
                MeasureButton = new BigButton(R.T_SM_ClosureButton, R.StatusControl);
                MeasureButton.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Action);
                MeasureButton.BigButtonClicked += delegate
                {
                    DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShowMeasureDifferences").State = DsaOptions.Never; //stm.flags.ShowMeasureDifferences = false;

                    stm.SetNextPointToOpeningPoint();
                    stepMeasure = st.MeasuresToDoCome;

                    (stm._InstrumentManager.SelectedInstrumentModule as PolarModule).GotoWanted = true;
                    (stm._InstrumentManager.SelectedInstrumentModule as PolarModule).N_A_GotoMessageWanted = false;

                    stm.AskIntrumentToMeasure();
                };
                e.step.View.AddButton(MeasureButton, "Ctrl");
                e.step.View.AddButton(
                    $"{R.T_SHOW_MORE_OF_THE_INSTRUMENT}",
                    R.Instrument,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module guidedModule = args2.TsuObject as Common.Guided.Module;
                        // here is a trick this option will trigger the showmessageinTSU in the Changed event, 
                        // because this windows cannot be set to showdialog or it will block the main thread 
                        // and at the same time if is not it will lose the focus if it is shown before the change event.
                        stm.SetNextPointToOpeningPoint();
                        instrumentIsShown.IsTrue = true;
                    }
                );
                e.step.View.AddButton(
                    $"{R.T_SELECT_ANOTHER_CONTROL_POINT}",
                    R.StatusControl,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module guidedModule = args2.TsuObject as Common.Guided.Module;
                        

                        List<Polar.Measure> measures = new List<Polar.Measure>();
                        foreach (var item in e.ActiveStation.MeasuresTaken)
                        {
                            if (item._Status.Type == M.States.Types.Good)
                                measures.Add(item as Polar.Measure);
                        };
                        BindingSource bs = new BindingSource() { DataSource = measures };
                        ComboBox comboBox = CreationHelper.GetComboBox(bs);
                        comboBox.DropDownStyle = ComboBoxStyle.DropDownList;

                        var r = new MessageInput(MessageType.Choice, R.T_SELECT_ANOTHER_CONTROL_POINT) { ButtonTexts = new List<string>(){ R.T_OK, R.T_CANCEL }, Controls =new List<Control>(){ comboBox } }.Show();
                        if (r.TextOfButtonClicked != R.T_CANCEL)
                        {
                            var measure = comboBox.SelectedItem as Polar.Measure;
                            stm.ForcedOpeningMeasure = measure; 
                            stm.SetNextPointToOpeningPoint();
                        }
                    }
                );

                e.step.View.ModifyLabel("", "ResultAsText");
                e.step.View.ModifyLabel("", R.T_WARNING);
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                I.Module im = e.InstrumentManager.SelectedInstrumentModule;

                // to disable the measure buttons during a measurement
                if (im != null)
                    Subscribe(im);

                s.Update();
            };

            s.Updating += delegate (object source, StepEventArgs e)
            {
                Measure open = GetOpeningMeasure(stm);
                int precA = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;
                int precD = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;

                string result = "";
                if (open != null)
                {
                    Measure last = GetClosure(stm, open);


                    // Opening Values
                    result += string.Format(R.T_SM_ClosureResuts, open._Point._Name) + "\r\n" + "\r\n";
                    result += string.Format(
                        "{0,-24}{1,-12}{2,-10}{3,-15}{4,-15}{5,-15}",
                        "Date", "Reflector", "Ext.[mm]", "AH [gon]", "AV [gon]", "Dist [m]", "State") + "\r\n";

                    // All measuremetn of the openign point
                    foreach (Measure item in stm.StationTheodolite.MeasuresTaken.FindAll(
                        x => x._Point._Name == open._Point._Name))
                    {
                        result += string.Format("{0,-24}{1,-12}{2,-10}{3,-15}{4,-15}{5,-15}",
                            item._Date.ToString(), item.Distance.Reflector._Name, item.Extension.ToString(precD - 3, 1000),
                            item.Angles.Corrected.Horizontal.ToString(precA), item.Angles.Corrected.Vertical.ToString(precA), item.Distance.Corrected.ToString(precD)) + "\r\n";
                    }

                    // checkin glast measure if available
                    if (last != null)
                    {
                        bool over = false;
                        bool dummy = false;
                        Measure diff;
                        string message;
                        double tolH = (e.ActiveStationModule._Station as Station).Parameters2.Tolerance.Same_Face.H_CC; // was ooposite-face tsu-3125
                        double tolV = (e.ActiveStationModule._Station as Station).Parameters2.Tolerance.Same_Face.V_CC; // was ooposite-face tsu-3125
                        double tolD = (e.ActiveStationModule._Station as Station).Parameters2.Tolerance.Same_Face.D_mm;// was ooposite-face tsu-3125

                        if (open._Date != last._Date)
                            diff = Survey.GetDifference(last, open, tolH, tolV, tolD, ref over, out message, ref dummy);
                        else
                            diff = null;

                        // Set button depending on the last measure
                        if (last._Status is M.States.Control)
                        {
                            TimeSpan span = DateTime.Now.Subtract(last._Date); // If control was done more tahn 15 minutes let s ask for it ...WHY? measure after control or not should be enough !
                            if (span.TotalMinutes < 15)
                                MeasureButton.SetAttributes($"{R.T_OPENING_POINT} {R.T_MEASURED}", R.StatusGood, TSU.Tsunami2.Preferences.Theme.Colors.Good);
                            else
                                MeasureButton.SetAttributes($"{R.T_OPENING_POINT} {R.T_MEASURED}, {R.T_BUT_MORE_THAN_15_MINUTES_AGO}", R.StatusControl, TSU.Tsunami2.Preferences.Theme.Colors.Action);
                        }
                        else
                        {
                            MeasureButton.SetAttributes(R.T_SM_ClosureButton, R.StatusControl, TSU.Tsunami2.Preferences.Theme.Colors.Action);
                        }

                        if (diff != null)
                        {

                            string h = (last.Angles.Corrected.Horizontal.Round(precA) - open.Angles.Corrected.Horizontal.Round(precA)).ToString(precA - 4, 10000);
                            string v = (last.Angles.Corrected.Vertical.Round(precA) - open.Angles.Corrected.Vertical.Round(precA)).ToString(precA - 4, 10000);
                            string d = (last.Distance.Corrected.Round(precD) - open.Distance.Corrected.Round(precD)).ToString(precD - 3, 1000);

                            result += "\r\n" + R.T_SM_ClosureDiff + "\r\n";
                            result += $"{"",-24}{"",-12}{"",-10}{"[cc]",-15}{"[cc]",-15}{"mm",-15}\r\n";

                            string warning1 = "";
                            string warning2 = "";
                            if (over)
                            {
                                warning1 = $"{R.T_BAD} {R.T_CLOSURE} !!!";
                                warning2 = string.Format(R.T_SM_ClosureExceed, $"H:{tolH} CC & V:{tolV} CC &{tolD} mm");

                                e.step.View.ModifyLabelColor(TSU.Tsunami2.Preferences.Theme.Colors.Bad, R.T_WARNING);
                            }

                            string difference = $"{warning1,-24}{"",-12}{"",-10}{h,-15}{v,-15}{d,-15}\r\n{warning2}";
                            e.step.View.ModifyLabel(difference, R.T_WARNING);

                            if (over && !(e.guidedModule is TdH))
                            {
                                // check if station ok and if not propose to restart the station
                                e.step.AddButton(GetRestartButton(e), "restart");
                            }
                        }
                    }
                    else
                    {
                        MeasureButton.SetAttributes(R.T_SM_ClosureButton, R.StatusQuestionnable, TSU.Tsunami2.Preferences.Theme.Colors.Action);
                    }
                }
                e.step.View.ModifyLabel(result, "ResultAsText");
                e.step.Test();
            };

            s.Changed += delegate (object source, StepEventArgs e)
            {
                ShowNotDialogInstrumentViewIsNecessery(instrumentIsShown, e);
            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {
                I.Module im = e.InstrumentManager.SelectedInstrumentModule;
                stm = s.GuidedModule._ActiveStationModule as Station.Module;

                // unsubscribe event subscribed in entering
                UnSubscribe(im);

            };

            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = false;
                e.step.CheckValidityOf(CheckIfClosureOK(stm, out e.step.MessageFromTest) == true);
            };

            return s;
        }

        private static Measure GetClosure(Station.Module stm, Measure open)
        {
            Measure GetLastValid = stm.StationTheodolite.MeasuresTaken.Last(x => !(x._Status is M.States.Bad)) as Measure;
            if (GetLastValid != open && GetLastValid._Point._Name == open._Point._Name)
                return GetLastValid;
            else
                return null;
        }

        private static Measure GetOpeningMeasure(Station.Module stm)
        {
            Measure first = stm.FindFirstValidMeasureOftheStation() as Measure;
            if (stm.ForcedOpeningMeasure != null)
                first = stm.ForcedOpeningMeasure;
            if (first != null)
            {
                Measure clone = first.Clone() as Measure;
                clone.Guid = Guid.NewGuid();
                return clone;
            }
            else
                return null;
        }

        private static bool CheckIfClosureOK(Station.Module stm, out string message)
        {
            Measure open = GetOpeningMeasure(stm);
            if (open == null)
            {
                message = R.T_STATION_NOT_EVEN_STARTED;
                return false;
            }
            else
            {
                Measure last = GetClosure(stm, open);
                if (last == null)
                {
                    message = R.T_M_CLOSURE_NEEDED;
                    return false;
                }
                else
                {
                    message = "";
                    return true;
                }
            }
        }

        #region manager modules showing

        static public Step ShowAdvancedTheodoliteModule(Module guidedModule)
        {
            Step s = new Step(guidedModule, "Advanced Module");

            s.Id = "ShowAdvancedTheodoliteModule";

            s.FirstUse += delegate (object source, StepEventArgs e)
            {

                e.step.View.AddControl(e.guidedModule._ActiveStationModule);

            };

            s.Entering += delegate (object source, StepEventArgs e) { };

            return s;
        }

        #endregion

        static public void WhatsNext(Step s, Station st)
        {
            bool reachTheActualPoint = false;


            // Add resumé list
            // Find the one still to be measured and the one skipped
            string toCome = "";
            string skipped = "";
            string separaror = "";
            foreach (Measure m in st.MeasuresToDo)
            {
                bool isActualPoint = false;
                if (!reachTheActualPoint)
                {
                    if (s.TAG == null) throw new Exception(R.T_NO_POINT_LINKED_TO_THE_STEP);
                    if (m == s.TAG as Measure)
                    {
                        reachTheActualPoint = true;
                        isActualPoint = true;

                        Station stt = st as Station;
                        s.View.ModifyLabel(stt.ToString(m), "Description");
                    }
                }
                if (!reachTheActualPoint && !m.IsMeasured)
                {
                    skipped += separaror + m._Point._Name;
                    separaror = ", ";
                }
                else
                {
                    if (isActualPoint)
                        toCome += separaror + R.T_THIS_POINT;
                    else
                        toCome += separaror + m._Point._Name;
                    separaror = ", ";
                }
            }
            if (toCome == "") toCome = $"{R.T_NONE}.";
            if (skipped == "") skipped = $"{R.T_NONE}.";
            s.View.ModifyLabel($"{R.T_STILL_TO_BE_MEASURED}:\r\n----------------------\r\n{toCome} \r\n\r\n{R.T_SKIPPED}:\r\n---------\r\n{skipped}\r\n", R.T_NEXT);

        }

        internal static Step PolarStationManagement(Common.Guided.Module gm)
        {
            // is suppose to replace into a single step:
            //    gm.Steps.Add(GS.Management.ChooseInstrument(gm));
            //    gm.Steps.Add(Setup.StationSetupStrategy(gm));
            //    gm.Steps.Add(StakeOut.StationDefaultMeasurementParameters(gm));

            Step s = new Step(gm, "Station parameters;Please define the following parameters");
            s.Id = "StationSingleStepSetupAdmin";

            s.FirstUse += On_PolarStationManagement_FirstUse;
            s.Entering += On_PolarStationManagement_Enter;
            s.Updating += On_PolarStationManagement_Update;
            s.Testing += On_PolarStationManagement_Test;

            return s;
        }

        private static void On_PolarStationManagement_FirstUse(object sender, StepEventArgs e)
        {
            var stepView = e.step.View;
            var stm = e.ActiveStationModule as Polar.Station.Module;
            var v = stm.View;
            var stp = stm.stationParameters;
            var defaultMeasure = stp.DefaultMeasure;

            stepView.AddSidedControls(new List<Control>()
            {
                e.step.View.GetPreparedLabel("Strategy", "n"),
                Setup.Setup_GetFreeStationButton(v, stp, "freeStation"),
                Setup.Setup_GetKnownPointStationButton(v, stp, "knownStation"),
                Setup.Setup_GetOrientationButton(v, stp, "orientationStation"),
            }, DockStyle.Top);

            stepView.AddDescription("", "ih");
            stepView.AddDescription("", "stationPointName");
            stepView.AddDescription("", "StationName");

            // add instrument selection
            stepView.AddSidedControls(new List<Control>()
            {
                stepView.GetPreparedLabel(R.T_INSTRUMENT + ":", ""),
                stepView.GetPreparedLabel(stp._Instrument._Name, "instrument"),
                new BigButton(
                string.Format(R.String_GM_SelectInstrument, ""),
                R.Tilt,
                delegate (object source2, TsuObjectEventArgs args2)
                {
                    e.InstrumentManager.View.CheckBoxesVisible = false;
                    var preselected = new List<TsuObject>();
                    if (Management.FindInstrumentToPropose(e.ActiveStation, e.InstrumentManager, out I.Instrument instrument))
                    {
                        preselected.Add(instrument);
                    }
                    var selectables = e.InstrumentManager.GetByClass(InstrumentClasses.POLAR);
                    var choosedInstrument = e.InstrumentManager.SelectInstrument(R.T_SELECT, selectables, multiSelection: false, preselected, showCancel: true);
                    if (choosedInstrument!=null)
                    {
                        // connect and initialised
                        //e.InstrumentManager.SelectedInstrumentModule.View.ShowInMessageTsu("Connect and initialise?", "Done", null); // this way will not give the hand to user during init
                        var iv= e.InstrumentManager.SelectedInstrumentModule.View;
                        iv.Dock= DockStyle.Bottom;
                        iv.TopLevel=false;
                        stepView.Controls.Add(iv);
                    }
                   e.step.Test();
                })
            }, DockStyle.Top);

            stepView.AddSidedControls(new List<Control>()
            {
                stepView.GetPreparedLabel(R.T265, ""),
                stepView.GetPreparedLabel(defaultMeasure.Distance.Reflector._Name, "reflector"),
                v.buttons.DefaultReflector.GetClone()
            }, DockStyle.Top);

            stepView.AddSidedControls(new List<Control>()
            {
                stepView.GetPreparedLabel(R.T_MEASUREMENT_SET_TO, ""),
                stepView.GetPreparedLabel(defaultMeasure.Face.ToString(), "face"),
                new BigButton
                (
                //    "Switch between 1 face and double face measurement",
                    R.T_SWITCH_BETWEEN_1_FACE_AND_DOUBLE_FACE_MEASUREMENT,
                    R.Face,
                    () =>
                    {
                        stm.FaceMeasurementHasBeenManuallySet=true;
                        if (defaultMeasure.Face == I.FaceType.DoubleFace)
                            defaultMeasure.Face = I.FaceType.Face1;
                        else
                            defaultMeasure.Face = I.FaceType.DoubleFace;
                        stepView.ModifyLabel(defaultMeasure.Face.ToString(), "face");
                    },
                    color: null,
                    hasSubButtons: false
                )
            }, DockStyle.Top);

            stepView.AddSidedControls(new List<Control>()
            {
                stepView.GetPreparedLabel(R.T_GMPOL_Numbermeasure, ""),
                stepView.GetPreparedLabel(defaultMeasure.NumberOfMeasureToAverage.ToString(), "number"),
                v.buttons.Number.GetClone()
            }, DockStyle.Top);

            stepView.AddSidedControls(new List<Control>()
            {
                stepView.GetPreparedLabel(R.T264 + ":", ""),
                stepView.GetPreparedLabel(defaultMeasure._Name, "pointName"),
                v.buttons.RenamePoint.GetClone()
            }, DockStyle.Top);

            stepView.AddSidedControls(new List<Control>()
            {
                stepView.GetPreparedLabel(string.Format("Default code for new points", ""),""),

                stepView.GetPreparedLabel(string.Format(defaultMeasure._Point.SocketCode.ToString()),"code"),
                v.buttons.DefaultCode.GetClone()
            }, DockStyle.Top);

            // extension
            {
                string textension;
                if (defaultMeasure.Extension.Value == TSU.Tsunami2.Preferences.Values.na)
                    textension = "Managed by point Code";
                else
                    textension = defaultMeasure.Extension.ToString() + "m";
                stepView.AddSidedControls(new List<Control>()
                {
                    stepView.GetPreparedLabel(string.Format(R.T266, ""),""),
                    stepView.GetPreparedLabel(string.Format(textension),"extension"),
                    v.buttons.Extension.GetClone()
                }, DockStyle.Top);
            }
            //Interfaces
            {
                stepView.AddSidedControls(new List<Control>()
                {
                    stepView.GetPreparedLabel($"{R.T_DEFAULT_INTERFACES}",""),
                    stepView.GetPreparedLabel(defaultMeasure.Interfaces.ToString(),"interfaces"),
                    v.GetNewInterfacesButton(defaultMeasure.Interfaces, defaultMeasure,
                    additionalAction: () => { stepView.ModifyLabel(defaultMeasure.Interfaces.ToString(),"interfaces"); })
                }, DockStyle.Top);
            }
        }

        private static void On_PolarStationManagement_Enter(object sender, StepEventArgs e)
        {

            (e.ActiveStationModule as Station.Module).View.usedMeasure = (e.ActiveStation as Station).Parameters2.DefaultMeasure;
        }


        private static void On_PolarStationManagement_Test(object sender, StepEventArgs e)
        {
            var stm = e.ActiveStationModule as Polar.Station.Module;
            var v = stm.View;
            var stp = stm.stationParameters;
            var dm = stp.DefaultMeasure as Polar.Measure;

            var step = e.step;
            bool ok = step.CheckValidityOf(resultOfAnExpression: (stp.Setups.InitialValues.HeightSetup), errorMessage: R.T_STATION_HEIGHT_MISSING);
            ok = step.UpdateValidityOf(currentlyValid: ok, resultOfAnExpression: (stp._StationPoint != null), errorMessage: R.T_STATION_POINT_MISSING);
            ok = step.UpdateValidityOf(currentlyValid: ok, resultOfAnExpression: (stp._Instrument != null), errorMessage: R.StringSmart_NoInstrumentSelected);
            bool reflectorOk = ((dm.Distance.Reflector != null && dm.Distance.Reflector._Name != R.String_Unknown) || stp._Instrument._InstrumentClass == InstrumentClasses.LASER_TRACKER);
            ok = step.UpdateValidityOf(currentlyValid: ok, resultOfAnExpression: (reflectorOk), errorMessage: R.T_AT_UnknownReflector);
            ok = step.UpdateValidityOf(currentlyValid: ok, resultOfAnExpression: (stp._StationPoint != null), errorMessage: R.T_STATION_POINT_MISSING);
        }

        private static void On_PolarStationManagement_Update(object sender, StepEventArgs e)
        {
            var stm = e.ActiveStationModule as Polar.Station.Module;
            var v = stm.View;
            var stp = stm.stationParameters;
            var dm = stp.DefaultMeasure;

            var goodColor = Tsunami2.Preferences.Theme.Colors.Good;

            // Strategy 
            bool heightKnown = false;
            bool stationPointDefined = false;
            bool stationPointKnown = false;
            if (stp.Setups.InitialValues.IsInstrumentHeightKnown == Station.Parameters.Setup.Values.InstrumentTypeHeight.Known)
            {
                heightKnown = true;
                if (stp._InstrumentHeight != null)
                    e.step.View.ModifyLabel(R.T259 + stp._InstrumentHeight.Value + " m.", "ih", goodColor);
            }
            else
                   if (stp.Setups.InitialValues.IsInstrumentHeightKnown == Station.Parameters.Setup.Values.InstrumentTypeHeight.Unknown)
                e.step.View.ModifyLabel(R.T260, "ih", TSU.Tsunami2.Preferences.Theme.Colors.Attention);
            else
                e.step.View.ModifyLabel("", "ih");

            if (stp._StationPoint != null)
                if (stp._StationPoint._Name != R.String_Unknown)
                {
                    stationPointDefined = true;
                    stationPointKnown = !stp._StationPoint._Name.ToUpper().Contains("STL");
                    e.step.View.ModifyLabel(R.T255 + stp._StationPoint._Name, "stationPointName", goodColor);
                    if (stp._Station != null)
                        e.step.View.ModifyLabel(R.T256 + stp._Station._Name, "stationName", goodColor);
                }

            // cahnge strategy button colors
            if (stationPointDefined)
            {
                if (!stationPointKnown)
                {
                    e.step.View.HighlightButtonColorByButtonName("freeStation", goodColor);
                    // restore button colors
                    e.step.View.RestoreButtonColorByButtonName("knownStation");
                    e.step.View.RestoreButtonColorByButtonName("orientationStation");
                }
                else
                {
                    if (!heightKnown)
                    {
                        e.step.View.HighlightButtonColorByButtonName("knownStation", goodColor);
                        // restore button colors
                        e.step.View.RestoreButtonColorByButtonName("freeStation");
                        e.step.View.RestoreButtonColorByButtonName("orientationStation");
                    }
                    else
                    {
                        e.step.View.HighlightButtonColorByButtonName("orientationStation", goodColor);
                        // restore button colors
                        e.step.View.RestoreButtonColorByButtonName("knownStation");
                        e.step.View.RestoreButtonColorByButtonName("freeStation");
                    }
                }
            }


            // instrument
            e.step.View.ModifyLabel(stp._Instrument._Name, "instrument");

            // default measure
            e.step.View.ModifyLabel(dm._PointName, "pointName");

            { // reflector
                e.step.View.ModifyLabel(dm.Distance.Reflector._Name, "reflector");
                if (stm._InstrumentManager.SelectedInstrument is I.Device.AT40x.Instrument)
                    if (dm.Distance.Reflector._Name == R.String_Unknown)
                        e.step.View.ModifyLabel(R.T_MANAGED_BY_AT40X, "reflector");
            } // reflector

            string textension;
            if (dm.Extension.Value == TSU.Tsunami2.Preferences.Values.na)
                textension = R.T_MANAGED_BY_POINT_CODE;
            else
                textension = dm.Extension.ToString() + "m";
            e.step.View.ModifyLabel(textension, "extension");

            e.step.View.ModifyLabel(dm.NumberOfMeasureToAverage.ToString(), "number");

            e.step.View.ModifyLabel(dm._Point.SocketCode.ToString(), "code");
        }
    }
}
