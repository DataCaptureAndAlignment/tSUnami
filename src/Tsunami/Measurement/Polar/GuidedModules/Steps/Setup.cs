﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TSU.Common.Guided.Steps;
using TSU.Common.Instruments.Reflector;
using TSU.Views;
using TSU.Views.Message;
using I = TSU.Common.Instruments;
using R = TSU.Properties.Resources;

namespace TSU.Polar.GuidedModules.Steps
{

    public static class Setup
    {

        #region station setup steps

        static public Step StationPosition(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.T_GMTH_POSITION);
            s.TAG = "StationPosition";
            s.Id = "StationPosition";

            Station.Module stm = null;
            Station.View stmv = null;

            // setup orentation strategy when button clicked
            BigButton known = null;
            EventHandler<TsuObjectEventArgs> SetupOnKnownPoint = null;

            // setup free station strategy when button clicked
            BigButton unKnown = null;
            EventHandler<TsuObjectEventArgs> SetupOnUnKnownPoint = null;

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                // set common varible here to be update for next stations
                stm = s.GuidedModule._ActiveStationModule as Station.Module;
                stmv = stm.View;
                known = stmv.buttons.KnownPosition.GetClone();
                unKnown = stmv.buttons.NewPosition.GetClone();
                // unKnown.SetAttributes("New point (Free station);Create a new point", null);
                unKnown.SetAttributes($"{R.T_NEW_POINT_FREE_STATION};{R.T_CREATE_A_NEW_POINT}", null);
                SetupOnUnKnownPoint = delegate
                {
                    stm.stationParameters.Setups.InitialValues.IsPositionKnown = false;
                };
                SetupOnKnownPoint = delegate
                {
                    stm.stationParameters.Setups.InitialValues.IsPositionKnown = true;
                };

                // add button for free station
                e.step.View.AddButton(known);

                // add button for free station
                e.step.View.AddButton(unKnown);
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                // subscribe to additional delegates
                known.BigButtonClicked += SetupOnKnownPoint;
                unKnown.BigButtonClicked += SetupOnUnKnownPoint;

                s.Update();
            };

            s.Testing += delegate (object source, StepEventArgs e)
            {
                Station station = e.ActiveStation as Station;
                e.step.CheckValidityOf(station.Parameters2._StationPoint != null);
                if (station.Parameters2._StationPoint != null)
                {
                    if (stm.stationParameters.Setups.InitialValues.IsPositionKnown == false)
                    {
                        unKnown.HighLightWithColor(TSU.Tsunami2.Preferences.Theme.Colors.Good);
                        known.RestoreColor();
                    }
                    else
                    {
                        known.HighLightWithColor(TSU.Tsunami2.Preferences.Theme.Colors.Good);
                        unKnown.RestoreColor();
                    }
                }
                else
                {
                    known.RestoreColor();
                    unKnown.RestoreColor();
                }
                //   e.step.MessageFromTest = "Station point missing";
                e.step.MessageFromTest = R.T_STATION_POINT_MISSING;
            };

            s.Updating += delegate (object source, StepEventArgs e)
            {
                Station.Parameters p = (e.ActiveStation as Station).Parameters2;
                if (p._StationPoint != null)
                    if (p._StationPoint._Name != R.String_Unknown)
                    {
                        e.step.View.ModifyLabel(R.T255 + p._StationPoint._Name, "PointName");
                        e.step.View.ModifyLabel(R.T256 + p._Station._Name, "StationName");
                    }
            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {
                // UNsubscribe to additional delegates
                known.BigButtonClicked -= SetupOnKnownPoint;
                unKnown.BigButtonClicked -= SetupOnUnKnownPoint;
            };
            return s;
        }

        static public Step StationSetupStrategy(Common.Guided.Module guidedModule, bool freeStationAvailable = true)
        {
            Step s = new Step(guidedModule, R.T_TH_FREEORKNOWN);
            s.isAStepOfChoice = true;

            Station.View v = null;
            Station.Parameters p = null;

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                v = s.GuidedModule._ActiveStationModule._TsuView as Station.View;
                p = (s.GuidedModule._ActiveStationModule._Station as Station).Parameters2;

                if (freeStationAvailable)
                    e.step.View.AddButton(Setup_GetFreeStationButton(v, p, "freeStation"));
                e.step.View.AddButton(Setup_GetKnownPointStationButton(v, p, "knownStation"));
                e.step.View.AddButton(Setup_GetOrientationButton(v, p, "orientationStation"));
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {

            };

            s.Testing += delegate (object source, StepEventArgs e)
            {
                bool ok = p.Setups.InitialValues.HeightSetup;
                ok = ok && p._StationPoint != null;
                e.step.CheckValidityOf(ok);

                //   e.step.MessageFromTest = "Station height or position is missing";
                e.step.MessageFromTest = R.T_STATION_HEIGHT_OR_POSITION_IS_MISSING;
            };

            s.Updating += delegate (object source, StepEventArgs e)
            {
                if (p.Setups.InitialValues.IsInstrumentHeightKnown == Station.Parameters.Setup.Values.InstrumentTypeHeight.Known)
                {
                    if (p._InstrumentHeight != null)
                        e.step.View.ModifyLabel(R.T259 + p._InstrumentHeight.Value + " m.", "ih", TSU.Tsunami2.Preferences.Theme.Colors.Good);
                }
                else
                    if (p.Setups.InitialValues.IsInstrumentHeightKnown == Station.Parameters.Setup.Values.InstrumentTypeHeight.Unknown)
                    e.step.View.ModifyLabel(R.T260, "ih", TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                else
                    e.step.View.ModifyLabel("", "ih");

                if (p._StationPoint != null)
                    if (p._StationPoint._Name != R.String_Unknown)
                    {
                        e.step.View.ModifyLabel(R.T255 + p._StationPoint._Name, "PointName", TSU.Tsunami2.Preferences.Theme.Colors.Good);
                        e.step.View.ModifyLabel(R.T256 + p._Station._Name, "StationName", TSU.Tsunami2.Preferences.Theme.Colors.Good);
                    }
            };

            return s;
        }

        public static BigButton Setup_GetOrientationButton(Station.View v, Station.Parameters p, string descriptionName)
        {
            BigButton orientation = new BigButton(R.T_TH_OrientationONLY, R.InstrumentHeightKnown);
            orientation.BigButtonClicked += delegate
            {
                if (p._IsSetup) throw new Exception(R.T_ALREADY_SETUP);
                v.SetStationPointFromList();
                if (p.Setups.InitialValues.StationPoint != null)
                {
                    p.Setups.InitialValues.IsInstrumentHeightKnown = Station.Parameters.Setup.Values.InstrumentTypeHeight.Known;
                    v.AskInstrumentHeightWithCode(p._StationPoint._Name);
                }
            };
            orientation.Name = descriptionName;
            return orientation;
        }

        public static BigButton Setup_GetKnownPointStationButton(Station.View v, Station.Parameters p, string descriptionName)
        {
            BigButton knwon = new BigButton(R.T_TH_KnownPosition, R.InstrumentHeight_Unknown);
            knwon.BigButtonClicked += delegate
            {
                if (p._IsSetup) throw new Exception(R.T_ALREADY_SETUP);
                v.SetStationPointFromList();
                p.Setups.InitialValues.IsInstrumentHeightKnown = Station.Parameters.Setup.Values.InstrumentTypeHeight.Unknown;
            };

            knwon.Name = descriptionName;
            return knwon;
        }

        public static BigButton Setup_GetFreeStationButton(Station.View v, Station.Parameters p, string descriptionName)
        {
            BigButton Free = new BigButton(R.T_TH_FREESTATION, R.InstrumentHeight0);
            Free.BigButtonClicked += delegate
            {
                if (p._IsSetup) throw new Exception(R.T_ALREADY_SETUP);
                p.Setups.InitialValues.IsInstrumentHeightKnown = Station.Parameters.Setup.Values.InstrumentTypeHeight.Known;
                p.Setups.InitialValues.InstrumentHeight = new DoubleValue(0, 0);
                v.SetNewStationPoint();
            };
            Free.Name = descriptionName;
            return Free;
        }

        static public Step StationHeight(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.T257);
            s.Id = "StationHeight";

            Station.View v = null;
            Station.Parameters p = null;

            BigButton hk = null;
            BigButton hu = null;
            BigButton h0 = null;

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                v = s.GuidedModule._ActiveStationModule._TsuView as Station.View;
                p = (s.GuidedModule._ActiveStationModule._Station as Station).Parameters2;

                // Add button from station theodolite view
                hu = v.buttons.HeightUnKnown.GetClone();
                e.step.View.AddButton(hu);

                hk = v.buttons.Height.GetClone();
                e.step.View.AddButton(hk);

                // New button for height=0;
                h0 = new BigButton(R.T_G_T_H0, R.InstrumentHeight0);
                h0.BigButtonClicked += delegate
                {
                    p.Setups.InitialValues.IsInstrumentHeightKnown = Station.Parameters.Setup.Values.InstrumentTypeHeight.Known;
                    p.Setups.InitialValues.InstrumentHeight = new DoubleValue(0, 0);
                };
                e.step.View.AddButton(h0);
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                if ((e.ActiveStation as Station).Parameters2.Setups.InitialValues.IsPositionKnown == false)
                    hu.Visible = false;
                else
                    hu.Visible = true;
                s.Update();
            };

            s.Testing += delegate (object source, StepEventArgs e)
            {
                bool ok = p.Setups.InitialValues.HeightSetup;
                e.step.CheckValidityOf(ok);

                hk.RestoreColor();
                hu.RestoreColor();
                h0.RestoreColor();

                switch (p.Setups.InitialValues.IsInstrumentHeightKnown)
                {
                    case Station.Parameters.Setup.Values.InstrumentTypeHeight.Known:
                        if (p.Setups.InitialValues.InstrumentHeight.Value == 0)
                        {
                            h0.HighLightWithColor();
                        }
                        else
                        {
                            hk.HighLightWithColor();
                        }
                        break;
                    case Station.Parameters.Setup.Values.InstrumentTypeHeight.Unknown:
                        hu.HighLightWithColor();
                        break;
                    case Station.Parameters.Setup.Values.InstrumentTypeHeight.NotDefinedYet:
                    default:
                        break;
                }

                //  e.step.MessageFromTest = "Station height missing";
                e.step.MessageFromTest = R.T_STATION_HEIGHT_MISSING;
            };

            s.Updating += delegate (object source, StepEventArgs e)
            {
                if (p.Setups.InitialValues.IsInstrumentHeightKnown == Station.Parameters.Setup.Values.InstrumentTypeHeight.Known)
                    e.step.View.ModifyLabel(R.T259 + p._InstrumentHeight.Value + " m.", "ih");
                else
                    if (p.Setups.InitialValues.IsInstrumentHeightKnown == Station.Parameters.Setup.Values.InstrumentTypeHeight.Unknown)
                    e.step.View.ModifyLabel(R.T260, "ih");
                else
                    e.step.View.ModifyLabel("", "ih");
            };

            return s;
        }

        static public Step ChooseFaces(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.T257);

            // refs
            Station.Module stm = null;
            Station st = null;
            Station.View v = null;

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                stm = s.GuidedModule._ActiveStationModule as Station.Module;
                st = stm.StationTheodolite;
                v = stm.View;


                // Add button from station theodolite view
                //set the change to default measure.
                e.step.View.ModifyLabel(R.T_GM_T_SELECTFACE, "sel");
                v.usedMeasure = st.Parameters2.DefaultMeasure;
                //by default put to double face
                v.usedMeasure.Face = I.FaceType.DoubleFace;
                e.step.View.AddButton(v.buttons.Face1);
                e.step.View.AddButton(v.buttons.DoubleFace);
                e.step.View.ModifyLabel(string.Format(R.T_GM_T_SELECTEDFACE, st.Parameters2.DefaultMeasure.Face.ToString()), "done");
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                s.Update();
            };

            s.Testing += delegate (object source, StepEventArgs e)
            {
                I.FaceType actual = st.Parameters2.DefaultMeasure.Face;
                e.step.CheckValidityOf(actual == I.FaceType.DoubleFace || actual == I.FaceType.Face1);

            };

            s.Updating += delegate (object source, StepEventArgs e)
            {
                e.step.View.ModifyLabel(string.Format(R.T_GM_T_SELECTEDFACE, st.Parameters2.DefaultMeasure.Face.ToString()), "done");
            };

            return s;
        }

        static public Step StationDefaultMeasurementParameters(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.T261);
            s.Id = "StationDefaultMeasurementParameters";

            Station st = null;
            Station.View v = null;
            

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                var stepView = e.step.View;

                // refs
                st = e.ActiveStation as Station;
                v = e.ActiveStationModule._TsuView as Station.View;

                // mods
                var defaultMeasure = st.Parameters2.DefaultMeasure;
                v.usedMeasure = defaultMeasure;

                // find reflector
                I.Device.AT40x.Module atm = e.InstrumentManager.SelectedInstrumentModule as I.Device.AT40x.Module;
                if (atm != null)
                {
                    Reflector r = null;
                    try
                    {
                        r = atm.ActualReflector;
                    }
                    catch (Exception)
                    {
                        r = null;
                    }
                    finally
                    {
                        if (r != null) defaultMeasure.Distance.Reflector = r;
                    }
                }

                // change zone name in point name if possible
                if (defaultMeasure._Point._Name == "ZONE.NEW.POINT.###")
                {
                    defaultMeasure._Point._Name = $"{v.Module.GetBestZoneName()}.NEW.POINT.###";
                }

                stepView.AddSidedControls(new List<Control>()
                {
                    stepView.GetPreparedLabel(R.T264 + ":", ""),
                    stepView.GetPreparedLabel(defaultMeasure._Name, "n"),
                    v.buttons.RenamePoint.GetClone()
                }, DockStyle.Top);


                stepView.AddSidedControls(new List<Control>()
                {
                    stepView.GetPreparedLabel(R.T265 ,  ""),
                    stepView.GetPreparedLabel(defaultMeasure.Distance.Reflector._Name,  "r"),
                    v.buttons.DefaultReflector.GetClone()
                }, DockStyle.Top);

                stepView.AddSidedControls(new List<Control>()
                {
                    stepView.GetPreparedLabel(string.Format("Default code for new points", ""),""),
                    stepView.GetPreparedLabel(string.Format(defaultMeasure._Point.SocketCode.ToString()),"c"),
                    v.buttons.DefaultCode.GetClone()
                }, DockStyle.Top);

                string textension;
                if (defaultMeasure.Extension.Value == TSU.Tsunami2.Preferences.Values.na)
                    textension = R.T_MANAGED_BY_POINT_CODE;
                else
                    textension = defaultMeasure.Extension.ToString() + "m";
                stepView.AddSidedControls(new List<Control>()
                {
                    stepView.GetPreparedLabel(string.Format(R.T266, ""),""),
                    stepView.GetPreparedLabel(string.Format(textension),"e"),
                    v.buttons.Extension.GetClone()
                }, DockStyle.Top);

                stepView.AddSidedControls(new List<Control>()
                {
                    stepView.GetPreparedLabel( R.T_GMPOL_Numbermeasure, ""),
                    stepView.GetPreparedLabel(defaultMeasure.NumberOfMeasureToAverage.ToString(), "numb"),
                    v.buttons.Number.GetClone()
                }, DockStyle.Top);

                //Interfaces
                {
                    stepView.AddSidedControls(new List<Control>()
                {
                    stepView.GetPreparedLabel($"{R.T_DEFAULT_INTERFACES}",""),
                    stepView.GetPreparedLabel(defaultMeasure.Interfaces.ToString(),"interfaces"),
                    v.GetNewInterfacesButton(defaultMeasure.Interfaces, defaultMeasure,
                    additionalAction: () => { stepView.ModifyLabel(defaultMeasure.Interfaces.ToString(),"interfaces"); })
                }, DockStyle.Top);
                }
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                (e.ActiveStationModule as Station.Module).View.usedMeasure = (e.ActiveStation as Station).Parameters2.DefaultMeasure;
            };

            s.Updating += delegate (object source, StepEventArgs e)
            {
                Station.Parameters p = (e.ActiveStation as Station).Parameters2;
                var defaultMeasure = p.DefaultMeasure;
                e.step.View.ModifyLabel(defaultMeasure._PointName, "n");

                { // reflector
                    e.step.View.ModifyLabel(defaultMeasure.Distance.Reflector._Name, "r");
                    if (e.step.GuidedModule._ActiveStationModule._InstrumentManager.SelectedInstrument is I.Device.AT40x.Instrument)
                        if (defaultMeasure.Distance.Reflector._Name == R.String_Unknown)
                            e.step.View.ModifyLabel(R.T_MANAGED_BY_AT40X, "r");
                } // reflector

                string textension;
                if (defaultMeasure.Extension.Value == TSU.Tsunami2.Preferences.Values.na)
                    textension = R.T_MANAGED_BY_POINT_CODE;
                else
                    textension = defaultMeasure.Extension.ToString() + "m";
                e.step.View.ModifyLabel(textension, "e");

                e.step.View.ModifyLabel(defaultMeasure.NumberOfMeasureToAverage.ToString(), "numb");

                e.step.View.ModifyLabel(defaultMeasure._Point.SocketCode.ToString(), "c");
            };

            s.Testing += delegate (object source, StepEventArgs e)
            {
                Station.Parameters p = (e.ActiveStation as Station).Parameters2;
                if (p.DefaultMeasure.Distance.Reflector != null) if (p.DefaultMeasure.Distance.Reflector._Name != "Unknown") e.step.skipable = true;
                bool test1 = p.DefaultMeasure._PointName != "Unknown";

                bool test2;
                if (e.InstrumentModule != null)
                {
                    if (e.InstrumentModule.Instrument is I.Device.AT40x.Instrument)
                        test2 = true;
                    else
                        test2 = p.DefaultMeasure.Distance.Reflector._Name != "Unknown";
                }
                else
                    test2 = false;

                e.step.CheckValidityOf(test1 && test2);
            };

            return s;
        }



        internal static Step End(Common.Guided.Module gm, string titleAndDescription)
        {
            Step s = Management.End(gm, titleAndDescription);
            s.Id = "End";

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                e.step.AddButton(
                  new BigButton(
                      $"{R.T_ADD_STATION};{R.T_ADD_A_NEW_STATION_TO_THE_MODULE}",
                      R.Add,
                      delegate (object source2, TsuObjectEventArgs args2)
                      {
                          CreateCloneWithSameParameters(gm, e);

                      }, null, null, false), "Add");

                foreach (Station.Module stm in s.GuidedModule.StationModules)
                {
                    stm.DecoupleFromInstrument();
                    stm.CloseStation(out bool dummy);
                }


            };

            return s;
        }

        private static void CreateCloneWithSameParameters(Common.Guided.Module gm, StepEventArgs e)
        {
            // create a new GM
            Common.Guided.Module newGm = null;
            switch (gm.guideModuleType)
            {
                case ENUM.GuidedModuleType.NoType:
                    break;
                case ENUM.GuidedModuleType.Alignment:
                    break;
                case ENUM.GuidedModuleType.AlignmentEcartometry:
                    break;
                case ENUM.GuidedModuleType.AlignmentImplantation:
                    break;
                case ENUM.GuidedModuleType.AlignmentLevelling:
                    break;
                case ENUM.GuidedModuleType.AlignmentLevellingCheck:
                    break;
                case ENUM.GuidedModuleType.AlignmentManager:
                    break;
                case ENUM.GuidedModuleType.AlignmentTilt:
                    break;
                case ENUM.GuidedModuleType.AlignmentLengthSetting:
                    break;
                case ENUM.GuidedModuleType.Cheminement:
                    break;
                case ENUM.GuidedModuleType.Ecartometry:
                    break;
                case ENUM.GuidedModuleType.Example:
                    break;
                case ENUM.GuidedModuleType.Implantation:
                    newGm = Implantation.Get(gm.ParentModule, gm.NameAndDescription);
                    break;
                case ENUM.GuidedModuleType.LengthSetting:
                    break;
                case ENUM.GuidedModuleType.PointLance:
                    newGm = PointLancé.Get(gm.ParentModule, gm.NameAndDescription);
                    break;
                case ENUM.GuidedModuleType.PreAlignment:
                    break;
                case ENUM.GuidedModuleType.Survey1Face:
                    newGm = Survey1Face.Get(gm.ParentModule, gm.NameAndDescription);
                    break;
                case ENUM.GuidedModuleType.Survey2Face:
                    newGm = Survey2Face.Get(gm.ParentModule, gm.NameAndDescription);
                    break;
                case ENUM.GuidedModuleType.Survey2FaceArray:
                    break;
                case ENUM.GuidedModuleType.SurveyTilt:
                    break;
                case ENUM.GuidedModuleType.TourDHorizon:
                    break;
                case ENUM.GuidedModuleType.EquiDistance:
                    break;
                default:
                    break;
            }

            if (newGm == null)
                throw new Exception("Not allowed");

            // Copy parameters
            Common.Station.Parameters param = gm._ActiveStationModule._Station.ParametersBasic;
            Common.Station.Parameters newParam = param.Clone() as Common.Station.Parameters;

            // move the most probably neede step
            newGm.MoveToStep("StationPosition", true);

            // close this module
            Management.FinishModule(gm, e);

            // start the new one
            TSU.Tsunami2.Preferences.Tsunami.Add(newGm);
        }

        internal static Step RelevementClosure(Common.Guided.Module gm)
        {
            return null;
        }

        internal static Step Compensation(Common.Guided.Module gm)
        {
            Step s = new Step(gm, R.T_TH_FREE_STATION_COMP);
            s.Id = "Compensation";
            Station.Module stm = null;
            Station st = null;
            Station.View v = null;

            BigButton cancelSetup = null;
            BigButton editMeasures = null;
            BigButton restartStation = null;
            BigButton Compensate = null;
            bool confirmed = false;

            #region Step Events


            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                // Sigma0"
                // s.View.ModifyLabel(string.Format(R.T_LGC_SIGMA0, "To be computed"), "sigma0");
                s.View.ModifyLabel($"{R.T_LGC_SIGMA0} {R.T_TO_BE_COMPUTED}", "sigma0");

                // Edit
                editMeasures = new BigButton(R.T_Edit_MEASURE, R.Measurement);
                editMeasures.BigButtonClicked += delegate
                {
                    v.ShowMeasureModule();
                };
                editMeasures.Visible = true;
                e.step.View.AddButton(editMeasures);
                editMeasures.Dock = DockStyle.Bottom;

                //// LGC
                Compensate = new BigButton(R.T_B_COMPUTE_LGC2, R.Lgc);
                Compensate.BigButtonClicked += delegate
                {
                    //st.Parameters2.Setup.FinalSetupValues.SigmaZero = Tsunami2.Preferences.Values.na;

                    v.Compute();
                    if (st.Parameters2._IsSetup)
                        confirmed = st.Parameters2.Setups.FinalValues.SigmaZero < TSU.Common.Compute.Compensations.Strategies.Common.ProblematicSigma0 && st.Parameters2.Setups.FinalValues.SigmaZero > 0.05;
                    s.Update();
                };
                Compensate.Visible = false;
                s.View.AddButton(Compensate);

                // cancelSetup
                cancelSetup = new BigButton(R.T_Cancel_Setup, R.Cancel);
                cancelSetup.BigButtonClicked += delegate
                {
                    stm._SetupStrategy.CancelSetup();
                    s.Update();
                };
                cancelSetup.Visible = false;
                s.View.AddButton(cancelSetup);
            };



            s.Entering += delegate (object source, StepEventArgs e)
            {
                stm = s.GuidedModule._ActiveStationModule as Station.Module;
                st = stm.StationTheodolite;
                v = stm.View;
                s.skipable = false;
            };


            s.Updating += delegate (object source, StepEventArgs e)
            {
                if (st.Parameters2._IsSetup)
                {
                    cancelSetup.Visible = true;
                    Compensate.Visible = false;
                }
                else
                {
                    cancelSetup.Visible = false;
                    Compensate.Visible = true;
                }

                if (st.Parameters2._IsSetup)
                {
                    if (st.Parameters2.Setups.FinalValues.SigmaZero < TSU.Common.Compute.Compensations.Strategies.Common.ProblematicSigma0 &&
                    st.Parameters2.Setups.FinalValues.SigmaZero > 0.05)
                    {
                        s.View.ModifyLabel($"{R.T_LGC_SIGMA0} {st.Parameters2.Setups.FinalValues.SigmaZero}", "sigma0");
                        s.View.ModifyLabelColor(TSU.Tsunami2.Preferences.Theme.Colors.Good, "sigma0");
                        restartStation = null;
                    }
                    else
                    {
                        if (!st.Parameters2._IsSetup)
                            s.View.ModifyLabel($"{R.T_LGC_SIGMA0} {st.Parameters2.Setups.FinalValues.SigmaZero}", "sigma0");
                        else
                            s.View.ModifyLabel($"{R.T_LGC_SIGMA0} {st.Parameters2.Setups.FinalValues.SigmaZero} {R.T__BUT_WAS_VALIDATED_BY_THE_USER}", "sigma0");
                        s.View.ModifyLabelColor(TSU.Tsunami2.Preferences.Theme.Colors.Bad, "sigma0");
                        editMeasures.Visible = true;

                        restartStation = Measurement.GetRestartButton(e);

                    }
                }
                if (restartStation != null)
                    s.AddButton(restartStation, "restart");
            };

            //s.Leaving += delegate (object source, GS.StepEventArgs e)
            //{

            //};

            s.Testing += delegate (object source, StepEventArgs e)
            {
                s.CheckValidityOf(st.Parameters2._IsSetup && st.Parameters2.Setups.FinalValues.SigmaZero < 10);
                if (st.Parameters2.Setups.FinalValues != null)
                {
                    if (st.Parameters2.Setups.FinalValues.SigmaZero > 10)
                        s.MessageFromTest = $"{R.T_LGC_SIGMA0} {st.Parameters2.Setups.FinalValues.SigmaZero} (s>10)\r\n{R.T_CALL_YOUR_SUPERVISOR}";
                }
                else
                {
                    s.MessageFromTest = $"Compute {R.T_NEED_TO_BE_VALIDATE}";
                }

            };


            //s.Desappearing += delegate (object source, GS.StepEventArgs e)
            //{

            //};

            s.CheckValidityToMoveToPreviousStep += delegate (object source, StepEventArgs e)
            {
                if (!st.Parameters2._IsSetup)
                {
                    s.okToMoveBack = true;
                    return;
                }
                else
                {
                    string titleAndMessage = $"{R.T_NOT_RECOMMENDED_TO_MOVE_BACK};{R.T_THE_STATION_IS_SETUP_THERE_IS_NO_REASON_TO_MOVE_BACK_IN_THE_PREVIOUS_STEPS}";
                    MessageInput mi = new MessageInput(MessageType.ImportantChoice, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.T_FORCE },
                        TextOfButtonToBeLocked = R.T_FORCE
                    };
                    string r = mi.Show().TextOfButtonClicked;
                    if (r == R.T_FORCE)
                        s.okToMoveBack = true;
                    else
                        s.okToMoveBack = false;
                }
            };
            #endregion

            return s;
        }

        internal static Step Compensationold(Common.Guided.Module gm)
        {
            Step s = new Step(gm, R.T_TH_FREE_STATION_COMP);

            Station.Module stm = null;
            Station st = null;
            Station.View v = null;

            BigButton confirm = null;
            BigButton editMeasures = null;
            BigButton Compensate = null;
            bool confirmed = false;

            #region Step Events

            #region FirstUse

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                stm = s.GuidedModule._ActiveStationModule as Station.Module;
                st = stm.StationTheodolite;
                v = stm.View;

                // Get some view controls
                s.skipable = false;

                // LGC
                Compensate = new BigButton(R.T_B_COMPUTE_LGC2, R.Lgc);
                Compensate.BigButtonClicked += delegate
                {
                    //st.Parameters2.Setup.FinalSetupValues.SigmaZero = Tsunami2.Preferences.Values.na;
                    v.Compute();
                    if (st.Parameters2._IsSetup)
                        confirmed = st.Parameters2.Setups.FinalValues.SigmaZero < 2 && st.Parameters2.Setups.FinalValues.SigmaZero > 0;
                    s.Update();
                };
                s.View.AddButton(Compensate);

                // Sigma0"
                s.View.ModifyLabel($"{R.T_LGC_SIGMA0} {R.T_TO_BE_COMPUTED}", "sigma0");

                // Edit
                editMeasures = new BigButton(R.T_Edit_MEASURE, R.Measurement, TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                editMeasures.BigButtonClicked += delegate
                {
                    v.ShowMeasureModule();
                };
                editMeasures.Visible = false;
                e.step.View.AddButton(editMeasures);

                // Valid
                confirm = new BigButton(string.Format(R.T_TH_CONFIRM_SETUP, R.String_Unknown), R.Lgc, TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                confirm.BigButtonClicked += delegate
                {
                    string titleAndMessage = string.Format(R.T_TH_CONFIRM_SETUP);
                    MessageInput mi = new MessageInput(MessageType.ImportantChoice, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_Cancel_Setup, R.T_CANCEL, R.T_VALID },
                        TextOfButtonToBeLocked = R.T_VALID
                    };
                    string choice = mi.Show().TextOfButtonClicked;
                    confirmed = choice == R.T_VALID;
                    if (choice == R.T_Cancel_Setup)
                        stm._SetupStrategy.CancelSetup();
                };
                confirm.Visible = false;
                s.View.AddButton(confirm);
            };

            s.AutoFillForDebug += delegate (object source, StepEventArgs e)
            {
                //st.Parameters2.Setup.TemporarySetupValues.SigmaZero = 1.1111111;

            };

            #endregion

            #region Entering

            s.Entering += delegate (object source, StepEventArgs e)
            {

            };

            #endregion

            #region Updating

            s.Updating += delegate (object source, StepEventArgs e)
            {
                if (st.Parameters2._IsSetup)
                {
                    if (st.Parameters2.Setups.FinalValues.SigmaZero < 2 && st.Parameters2.Setups.FinalValues.SigmaZero > 0)
                    {
                        s.View.ModifyLabel($"{R.T_LGC_SIGMA0} {st.Parameters2.Setups.FinalValues.SigmaZero}", "sigma0");
                        s.View.ModifyLabelColor(TSU.Tsunami2.Preferences.Theme.Colors.Good, "sigma0");
                    }
                    else
                    {
                        if (!confirmed)
                        {
                            s.View.ModifyLabel($"{R.T_LGC_SIGMA0} {st.Parameters2.Setups.FinalValues.SigmaZero}, {R.T_SIGMA_TOO_BIG}.", "sigma0");
                            //s.View.ModifyLabel(string.Format( R.T_LGC_SIGMA0 +   ", the value is higher than expected (you can force the step if you still want to continue in those not recommended conditions.",  st.Parameters2.Setup.FinalSetupValues.SigmaZero), "sigma0");
                        }
                        else
                        {
                            s.View.ModifyLabel($"{R.T_LGC_SIGMA0} {st.Parameters2.Setups.FinalValues.SigmaZero}, {R.T_SIGMA_TOO_BIG}.", "sigma0");
                            //s.View.ModifyLabel(string.Format( R.T_LGC_SIGMA0 +  ", the value is higher than expected (you can force the step if you still want to continue in those not recommended conditions.",  st.Parameters2.Setup.FinalSetupValues.SigmaZero), "sigma0");
                        }

                        s.View.ModifyLabelColor(TSU.Tsunami2.Preferences.Theme.Colors.Bad, "sigma0");
                        confirm.Visible = true;
                        editMeasures.Visible = true;
                    }
                }

                if (confirmed)
                {
                    confirm.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Good);
                    Compensate.Visible = false;
                }
                else
                {
                    confirm.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                    Compensate.Visible = true;
                }

            };

            #endregion

            #region Leaving

            s.Leaving += delegate (object source, StepEventArgs e)
            {

            };

            #endregion

            #region Testing

            s.Testing += delegate (object source, StepEventArgs e)
            {
                s.CheckValidityOf(confirmed);
            };

            #endregion

            #region Desappearing

            s.Desappearing += delegate (object source, StepEventArgs e)
            {
            };

            #endregion

            #endregion

            return s;
        }

        #endregion
    }
}
