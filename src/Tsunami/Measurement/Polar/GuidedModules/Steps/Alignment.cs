﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using TSU.Views;
using System.Windows.Forms;
using M = TSU;
using I = TSU.Common.Instruments;
using TSU.Common.Guided.Steps;

namespace TSU.Polar.GuidedModules.Steps
{

    static public class Alignment
    {
        static public Common.Guided.Module Get(M.Module parentModule)
        {
            Common.Guided.Group.Module GrGm = parentModule as Common.Guided.Group.Module;

            Common.Guided.Module gm = new Common.Guided.Module(parentModule, R.T_B_LENGTH_ALIGN);

            gm.guideModuleType = ENUM.GuidedModuleType.AlignmentImplantation;
            gm.ObservationType = Common.ObservationType.Polar;

            // Invisible step: : Add needed station modules
            Station.Module stm = new Station.Module(gm);

            stm.stationParameters.DefaultMeasure.Face = I.FaceType.DoubleFace;
            stm.ChangeSetupStrategy(TSU.Common.Compute.Compensations.Strategies.List.NoComputation2);
            gm.SwitchToANewStationModule(stm);

            ViewsOption(gm);

            BuildSteps(gm);
            gm.Start();
            return gm;
        }

        private static void ViewsOption(Common.Guided.Module gm) // not serialized
        {
            Station.Module stm = gm._ActiveStationModule as Station.Module;
            stm.RemoveMeasureFromNextPointListAfterReception = false;

            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShowQuestionableResults").State = DsaOptions.Never; //stm.flags.ShowQuestionableResults = false; // not to have a message with the result but either raise the event ResultAvailable.
            //stm.flags.AddDefaultpoint = false; // not to add the default new point when list empty
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShowMeasureDifferences").State = DsaOptions.Never; //stm.flags.ShowMeasureDifferences = false;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "DoStationSetupAfterClosure").State = DsaOptions.Never; //stm.flags.DoStationSetupAfterClosure = false;

            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShouldRemeasureExisting").State = DsaOptions.Always; //stm.flags.ShouldRemeasureExisting = DsaOptions.Always;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShouldChangeNameOfExistingBeforeMeasurement").State = DsaOptions.Never; //stm.flags.ShouldChangeNameOfExistingBeforeMeasurement = DsaOptions.Never;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ProposeFreeStationIfPointConnu").State = DsaOptions.Never; //stm.flags.ProposeFreeStationIfPointConnu = false;

            gm.quickMeasureAllowed = true;
        }

        private static void BuildSteps(Common.Guided.Module gm)
        {
            // mise en station
            {
                bool useNewSingleRefMeasurementSteps = true;
                bool useNewSingleAdminSetupSteps = true;

                if (useNewSingleAdminSetupSteps)
                {
                    gm.Steps.Add(Measurement.PolarStationManagement(gm));
                }
                else
                {
                    gm.Steps.Add(Management.ChooseInstrument(gm, I.InstrumentClasses.POLAR));
                    gm.Steps.Add(Setup.StationSetupStrategy(gm));
                    gm.Steps.Add(StakeOut.StationDefaultMeasurementParameters(gm));
                }
                gm.Steps.Add(Measurement.ChooseReferencePoints(gm));
                gm.Steps.Add(Measurement.MeasurementAbstractBefore(gm, Measurement.Types.Setup, CreateNextStep: !useNewSingleRefMeasurementSteps));
                //Theodolite.MeasureNextPoint is a added a number of time when entering Theodolite.MeasurementAbstractBefore or Theodolite.MeasureNextPoint
                gm.Steps.Add(new Setup_MeasureAllRefs(gm));

                gm.Steps.Add(Measurement.Closure(gm));
            }

            // measure and comparaion
            {
                gm.Steps.Add(Setup.Compensation(gm));
                gm.Steps.Add(StakeOut.StationDefaultMeasurementParameters(gm));

                gm.Steps.Add(Measurement.MeasurementAbstractBefore(gm, Measurement.Types.PreAlignment));
                // Theodolite.MeasureNextPoint is a added a number of time when entering Theodolite.MeasurementAbstractBefore or Theodolite.MeasureNextPoint
            }

            // fin
            {
                gm.Steps.Add(Measurement.Closure(gm));
                gm.Steps.Add(StakeOut.ShowMeasurements(gm));
                gm.Steps.Add(Management.Export(gm));
                gm.Steps.Add(Management.Declaration(gm, R.StringGuided_End));
            }
        }

        internal static void ReCreateWhatIsNotSerialized(Common.Guided.Module gm)
        {
            gm.Steps = new List<Step>();
            ViewsOption(gm);
            BuildSteps(gm);

            { // desire step to start after TSU opening
                string desireName = "";

                if (gm.Finished)
                    desireName = "Export";
                else if (gm._ActiveStationModule._Station.ParametersBasic._IsSetup)
                    desireName = "Abstract" + Measurement.Types.PreAlignment;
                else
                    desireName = R.soai; // selection of an instrument
                gm.currentIndex = gm.GetStepIndexBasedOnName(desireName);
            }
        }
    }
}
