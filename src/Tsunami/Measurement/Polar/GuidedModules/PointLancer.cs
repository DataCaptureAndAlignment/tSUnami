﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using TSU.Views;
using M = TSU;
using I = TSU.Common.Instruments;
using TSU.Polar.GuidedModules.Steps;
using TSU.Common.Guided.Steps;

namespace TSU.Polar.GuidedModules
{

    static public class PointLancé
    {
        static public Common.Guided.Module Get(M.Module parentModule, string nameAndDescription)
        {
            Common.Guided.Module gm = new Common.Guided.Module(parentModule, R.T_B_POINTLANCE);
            gm.View.Image = R.I_PointLancé;
            gm.guideModuleType = ENUM.GuidedModuleType.PointLance;
            gm.ObservationType = Common.ObservationType.Polar;

            // Invisible step: : Add needed station modules
            Station.Module stm = new Station.Module(gm);
            stm.stationParameters.DefaultMeasure.Face = I.FaceType.DoubleFace;

            stm.stationParameters.DefaultMeasure.NumberOfMeasureToAverage = 3;
            stm.ChangeSetupStrategy(TSU.Common.Compute.Compensations.Strategies.List.OrientatedOnly_LGC2);
            gm.SwitchToANewStationModule(stm);

            ViewsOption(gm);

            BuildSteps(gm);

            gm.Start();

            return gm;
        }


        private static void ViewsOption(Common.Guided.Module gm) // not serialized
        {
            Station.Module stm = gm._ActiveStationModule as Station.Module;
            stm.RemoveMeasureFromNextPointListAfterReception = false;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShowMeasureDifferences").State = DsaOptions.Never; //stm.flags.ShowMeasureDifferences = false;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "DoStationSetupAfterClosure").State = DsaOptions.Never; //stm.flags.DoStationSetupAfterClosure = false;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ProposeFreeStationIfPointConnu").State = DsaOptions.Never; //stm.flags.ProposeFreeStationIfPointConnu = false;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShouldRemeasureExisting").State = DsaOptions.Always; //stm.flags.ShouldRemeasureExisting = DsaOptions.Always;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShouldChangeNameOfExistingBeforeMeasurement").State = DsaOptions.Never; //stm.flags.ShouldChangeNameOfExistingBeforeMeasurement = DsaOptions.Never;
        }

        private static void BuildSteps(Common.Guided.Module gm)
        {
            //
            // Admin
            //
            gm.Steps.Add(Management.ChooseTheoreticalFile(gm));
            gm.Steps.Add(Management.ChooseInstrument(gm, I.InstrumentClasses.POLAR));
            gm.Steps.Add(Management.EnterATeam(gm));
            gm.Steps.Add(Management.ChooseOperation(gm));

            //
            // mise en station
            //
            gm.Steps.Add(Setup.StationSetupStrategy(gm, freeStationAvailable: false));
            gm.Add(StakeOut.StationDefaultMeasurementParameters(gm));

            gm.Steps.Add(GuidedModules.Steps.Measurement.ChooseReferencePoints(gm));
            gm.Steps.Add(GuidedModules.Steps.Measurement.MeasurementAbstractBefore(gm, GuidedModules.Steps.Measurement.Types.Setup));
            // Theodolite.MeasureNextPoint is a added a number of time when entering Theodolite.MeasurementAbstractBefore or Theodolite.MeasureNextPoint
            gm.Steps.Add(GuidedModules.Steps.Measurement.Closure(gm));
            gm.Steps.Add(Setup.Compensation(gm));

            // measure and comparaion

            gm.Steps.Add(GuidedModules.Steps.Measurement.AddPointLancé(gm));
            gm.Steps.Add(GuidedModules.Steps.Measurement.MeasurementAbstractBefore(gm, GuidedModules.Steps.Measurement.Types.Measurement));

            gm.Steps.Add(GuidedModules.Steps.Measurement.Closure(gm));
            gm.Steps.Add(PointLancé.Export(gm));

            gm.Steps.Add(Setup.End(gm, R.StringGuided_End));

        }

        static public void ReCreateWhatIsNotSerialized(Common.Guided.Module gm)
        {
            gm.Steps = new List<Step>();
            ViewsOption(gm);
            BuildSteps(gm);

            { // desire step to start after TSU opening
                string desireName = "";
                if (gm.Finished)
                    desireName = "Export";
                else if (gm._ActiveStationModule._Station.ParametersBasic._IsSetup)
                    desireName = "Abstract" + GuidedModules.Steps.Measurement.Types.Measurement;
                else
                    desireName = "Abstract" + GuidedModules.Steps.Measurement.Types.Setup;
                gm.currentIndex = gm.GetStepIndexBasedOnName(desireName);
            }
        }

        internal static Step Export(Common.Guided.Module guidedModule)
        {
            Step s = Management.Export(guidedModule);
            s.TAG = "Export";

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                e.step.AddButton(new BigButton(R.T_EXPORT_NP_TO_THEO_DAT, R.Element_File, () =>
                {
                    s.GuidedModule._ActiveStationModule.ExportNewPointToATheoreticalFile();
                }));
            };


            return s;
        }


    }
}
