﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using TSU.Views;
using M = TSU;
using I = TSU.Common.Instruments;
using TSU.Polar.GuidedModules.Steps;
using TSU.Common.Guided;
using TSU.Common.Guided.Steps;

namespace TSU.Polar.GuidedModules
{
    static public class Implantation
    {
        static public Common.Guided.Module Get(M.Module parentModule, string nameAndDescription)
        {
            Common.Guided.Module gm = new Common.Guided.Module(parentModule, R.MN_IMP);

            Decore.ShowRaisedEvent(gm);

            gm.View.Image = R.I_Implantation;
            gm.guideModuleType = ENUM.GuidedModuleType.Implantation; // used by DEP to swithc on recreatafeter deserailization...
            gm.ObservationType = Common.ObservationType.Polar;

            // Invisible step: : Add needed station modules
            Station.Module stm = new Station.Module(gm);

            stm.stationParameters.DefaultMeasure.Face = I.FaceType.Face1;
            stm.ChangeSetupStrategy(TSU.Common.Compute.Compensations.Strategies.List.NoComputation2);
            gm.SwitchToANewStationModule(stm);

            ViewsOption(gm);
            BuildSteps(nameAndDescription, gm);
            gm.Start();
            return gm;
        }

        private static void ViewsOption(Common.Guided.Module gm) // not serialized
        {
            Station.Module stm = gm._ActiveStationModule as Station.Module;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShowMeasureDifferences").State = DsaOptions.Never; //stm.flags.ShowMeasureDifferences = false;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "DoStationSetupAfterClosure").State = DsaOptions.Never; //stm.flags.DoStationSetupAfterClosure = false;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShowQuestionableResults").State = DsaOptions.Never; //stm.flags.ShowQuestionableResults = false; // not to have a message with the result but either raise the event ResultAvailable.
            stm.RemoveMeasureFromNextPointListAfterReception = false;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShouldRemeasureExisting").State = DsaOptions.Always; //stm.flags.ShouldRemeasureExisting = DsaOptions.Always;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShouldChangeNameOfExistingBeforeMeasurement").State = DsaOptions.Never; //stm.flags.ShouldChangeNameOfExistingBeforeMeasurement = DsaOptions.Never;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ProposeFreeStationIfPointConnu").State = DsaOptions.Never; //stm.flags.ProposeFreeStationIfPointConnu = false;
            //stm.flags.AddDefaultpoint = false;
            gm.quickMeasureAllowed = true;
        }

        private static void BuildSteps(string nameAndDescription, Common.Guided.Module gm)
        {
            // Admin
            {
                gm.Steps.Add(Management.Declaration(gm, nameAndDescription));
                gm.Add(Management.ChooseTheoreticalFile(gm));
                gm.Add(Management.ChooseInstrument(gm, I.InstrumentClasses.POLAR));
                gm.Add(Management.EnterATeam(gm));
                gm.Add(Management.ChooseOperation(gm));
            }

            // mise en station
            {
                gm.Add(Setup.StationSetupStrategy(gm));

                gm.Add(StakeOut.StationDefaultMeasurementParameters(gm));
                gm.Add(GuidedModules.Steps.Measurement.ChooseReferencePoints(gm));
                gm.Add(GuidedModules.Steps.Measurement.MeasurementAbstractBefore(gm, GuidedModules.Steps.Measurement.Types.Setup));  // Theodolite.MeasureNextPoint is a added a number of time when entering Theodolite.MeasurementAbstractBefore or Theodolite.MeasureNextPoint
                gm.Steps.Add(GuidedModules.Steps.Measurement.Closure(gm));
                gm.Add(Setup.Compensation(gm));
            }

            // measure and comparaion
            {
                gm.Add(Setup.StationDefaultMeasurementParameters(gm));

                gm.Add(StakeOut.ChoosePointsToStakeOut(gm));
                gm.Add(GuidedModules.Steps.Measurement.MeasurementAbstractBefore(gm, GuidedModules.Steps.Measurement.Types.StakeOut));  // Theodolite.MeasureNextPoint is a added a number of time when entering Theodolite.MeasurementAbstractBefore or Theodolite.MeasureNextPoint
            }

            // Ending
            {
                gm.Add(GuidedModules.Steps.Measurement.Closure(gm));
                gm.Add(Management.Export(gm));
                gm.Add(Setup.End(gm, R.StringGuided_End));
            }
        }


        internal static void ReCreateWhatIsNotSerialized(Common.Guided.Module gm)
        {
            gm.Steps = new List<Step>();
            Decore.ShowRaisedEvent(gm);
            ViewsOption(gm);
            BuildSteps(gm.NameAndDescription, gm);

            { // desire step to start after TSU opening
                string desireName = "";
                if (gm.Finished)
                    desireName = "Export";
                else if (gm._ActiveStationModule._Station.ParametersBasic._IsSetup)
                    desireName = "Abstract" + GuidedModules.Steps.Measurement.Types.StakeOut;
                else
                    desireName = "Abstract" + GuidedModules.Steps.Measurement.Types.Setup;
                gm.currentIndex = gm.GetStepIndexBasedOnName(desireName);
            }
        }

    }
}
