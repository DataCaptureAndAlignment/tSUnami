﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using M = TSU;
using I = TSU.Common.Instruments;
using TSU.Polar.GuidedModules.Steps;
using TSU.Common.Guided.Steps;

namespace TSU.Polar.GuidedModules
{
    static public class Survey2FaceArray
    {
        static public Common.Guided.Module Get(M.Module parentModule, string nameAndDescription)
        {
            Common.Guided.Module gm = new Common.Guided.Module(parentModule, nameAndDescription);
            gm.guideModuleType = ENUM.GuidedModuleType.Survey2Face;
            gm.ObservationType = Common.ObservationType.Polar;

            // Invisible step: : Add needed station modules
            Station.Module stm = new Station.Module(gm);
            gm.SwitchToANewStationModule(stm);

            stm.stationParameters.DefaultMeasure.Face = I.FaceType.DoubleFace;
            stm.ChangeSetupStrategy(TSU.Common.Compute.Compensations.Strategies.List.NoComputation2);

            ViewsOption(gm);

            BuildSteps(gm);
            gm.Start();

            return gm;
        }

        private static void ViewsOption(Common.Guided.Module gm) // not serialized
        {
            Station.Module stm = gm._ActiveStationModule as Station.Module;

            stm.FaceMeasurementHasBeenManuallySet = true; // very important ! because if notorized it will then force to DF instead F1 (if not set to true.)
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShowMeasureDifferences").State = DsaOptions.Never; //stm.flags.ShowMeasureDifferences = false;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "DoStationSetupAfterClosure").State = DsaOptions.Never; //stm.flags.DoStationSetupAfterClosure = false;
            stm.RemoveMeasureFromNextPointListAfterReception = false;
            //stm.flags.AddDefaultpoint = false;
            stm.ValidateComputeAsFastAsPossible = true;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShouldRemeasureExisting").State = DsaOptions.Always; //stm.flags.ShouldRemeasureExisting = DsaOptions.Always;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShouldChangeNameOfExistingBeforeMeasurement").State = DsaOptions.Never; //stm.flags.ShouldChangeNameOfExistingBeforeMeasurement = DsaOptions.Never;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ProposeFreeStationIfPointConnu").State = DsaOptions.Never; //stm.flags.ProposeFreeStationIfPointConnu = false;
        }

        private static void BuildSteps(Common.Guided.Module gm)
        {
            gm.Steps.Add(Management.Declaration(gm, R.GM_S2F_ND));
            gm.Steps.Add(Management.ChooseTheoreticalFile(gm));
            gm.Steps.Add(Management.ChooseInstrument(gm, I.InstrumentClasses.POLAR));
            gm.Steps.Add(Management.EnterATeam(gm));
            gm.Steps.Add(Management.ChooseOperation(gm));
            gm.Steps.Add(Setup.StationPosition(gm));
            gm.Steps.Add(Setup.StationHeight(gm));

            gm.Steps.Add(Setup.StationDefaultMeasurementParameters(gm));
            gm.Steps.Add(GuidedModules.Steps.Measurement.ChoosePointsToMeasure(gm));

            gm.Steps.Add(GuidedModules.Steps.Measurement.ArrayAcquisition(gm));
            gm.Steps.Add(GuidedModules.Steps.Measurement.MeasurementAbstractBefore(gm, GuidedModules.Steps.Measurement.Types.Measurement));

            // Theodolite.MeasureNextPoint is a added a number of time when entering Theodolite.MeasurementAbstractBefore or Theodolite.MeasureNextPoint
            gm.Steps.Add(Management.ShowMeasurementsWithoutValidation(gm));

            gm.Steps.Add(GuidedModules.Steps.Measurement.Closure(gm));
            //gm.Steps.Add(Step.Management.AddAStation(gm)); // temporary remove because not doing what it should
            gm.Steps.Add(Management.Export(gm));
            gm.Steps.Add(Setup.End(gm, R.StringGuided_End));
        }

        static public void ReCreateWhatIsNotSerialized(Common.Guided.Module gm)
        {
            gm.Steps = new List<Step>();
            ViewsOption(gm);
            BuildSteps(gm);

            { // desire step to start after TSU opening
                string desireName = "";
                if (gm.Finished)
                    desireName = "Export";
                else
                    desireName = "Abstract" + GuidedModules.Steps.Measurement.Types.Measurement;
                gm.currentIndex = gm.GetStepIndexBasedOnName(desireName);
            }
        }
    }
}
