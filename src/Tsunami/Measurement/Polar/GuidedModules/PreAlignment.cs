﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using M = TSU.Common.Measures;
using I = TSU.Common.Instruments;
using TSU.Polar.GuidedModules.Steps;
using TSU.Common;
using TSU.Tools.Exploration;
using TSU.Common.Guided.Steps;

namespace TSU.Polar.GuidedModules
{

    static public class PreAlignment
    {
        static public Common.Guided.Module Get(TSU.Module parentModule, string nameAndDescription)
        {
            Common.Guided.Module gm = new Common.Guided.Module(parentModule, R.MN_PA);
            gm.guideModuleType = ENUM.GuidedModuleType.PreAlignment;
            gm.ObservationType = Common.ObservationType.Polar;

            // look for existing station ?
            Station.Module stm = null;
            List<Station.Module> l = Common.Analysis.Module.GetModules_SettedUpSTM(Tsunami2.Properties);
            if (l.Count > 0)
            {
                Explorator em = new Tools.Exploration.Explorator(gm);
                em.AllElements.AddRange(l);
                em.SelectableObjects.Clear();
                if (em.View.ShowInMessageTsu(
                  //   "Continue station?;Do you want to use an already setted up station from this list?",
                  $"{R.T_CONTINUE_STATION};{R.T_DO_YOU_WANT_TO_USE_AN_ALREADY_SETTED_UP_STATION_FROM_THIS_LIST}",
                    R.T_YES, null, R.T_NO) == R.T_YES)
                {
                    stm = em._SelectedObjects[0] as Station.Module;
                }
            }

            if (stm == null) stm = new Station.Module(gm);

            gm.SwitchToANewStationModule(stm);

            stm.stationParameters.DefaultMeasure.Face = I.FaceType.Face1;
            stm.ChangeSetupStrategy(TSU.Common.Compute.Compensations.Strategies.List.NoComputation2);

            ViewsOption(gm);


            BuildSteps(gm);
            gm.Start();

            return gm;
        }

        private static void ViewsOption(Common.Guided.Module gm) // not serialized
        {
            Station.Module stm = gm._ActiveStationModule as Station.Module;
            //stm.flags.AddDefaultpoint = false;
            stm.RemoveMeasureFromNextPointListAfterReception = false;

            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "KeepQuestionableMeasure").State = DsaOptions.Never; //stm.flags.KeepQuestionableMeasure = false;      //   because we dont want to save until the grid is full and all element aligned  
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "RejectQuestionableMeasure").State = DsaOptions.Always; //stm.flags.RejectQuestionableMeasure = true;      //   because we dont want to save until the grid is full and all element aligned  

            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShowMeasureDifferences").State = DsaOptions.Never; //stm.flags.ShowMeasureDifferences = false;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "DoStationSetupAfterClosure").State = DsaOptions.Never; //stm.flags.DoStationSetupAfterClosure = false;

            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ProposeFreeStationIfPointConnu").State = DsaOptions.Never; //stm.flags.ProposeFreeStationIfPointConnu = false;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShouldRemeasureExisting").State = DsaOptions.Always; //stm.flags.ShouldRemeasureExisting = DsaOptions.Always;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShouldChangeNameOfExistingBeforeMeasurement").State = DsaOptions.Never; //stm.flags.ShouldChangeNameOfExistingBeforeMeasurement = DsaOptions.Never;

            gm.quickMeasureAllowed = true;
        }

        private static void BuildSteps(Common.Guided.Module gm)
        {
            // Admin
            {
                gm.Steps.Add(Management.Declaration(gm, R.GM_P3D));
                gm.Steps.Add(Management.ChooseTheoreticalFile(gm));
                gm.Steps.Add(Management.ChooseInstrument(gm, I.InstrumentClasses.POLAR));

                //gm.Steps.Add(Step.Management.ChooseALignmentAdministrativeParameters(gm));
                gm.Steps.Add(Management.EnterATeam(gm));
                gm.Steps.Add(Management.ChooseOperation(gm));
            }

            // Setup
            {
                gm.Steps.Add(Setup.StationPosition(gm));
                gm.Steps.Add(Setup.StationHeight(gm));
                gm.Steps.Add(StakeOut.StationDefaultMeasurementParameters(gm));
                gm.Steps.Add(GuidedModules.Steps.Measurement.ChooseReferencePoints(gm));
                gm.Steps.Add(GuidedModules.Steps.Measurement.MeasurementAbstractBefore(gm, GuidedModules.Steps.Measurement.Types.Setup));
                gm.Steps.Add(GuidedModules.Steps.Measurement.Closure(gm));
            }

            // measure and comparaison
            {
                gm.Steps.Add(Setup.Compensation(gm));
                gm.Steps.Add(StakeOut.StationDefaultMeasurementParameters(gm));


                gm.Steps.Add(StakeOut.ChooseMagnetToPreAlign(gm));
                gm.Steps.Add(GuidedModules.Steps.Measurement.MeasurementAbstractBefore(gm, GuidedModules.Steps.Measurement.Types.PreAlignment));

                // here happens all addition steps for measuement
            }

            // fin
            {
                gm.Steps.Add(GuidedModules.Steps.Measurement.Closure(gm));
                gm.Steps.Add(StakeOut.ShowMeasurements(gm));
                gm.Steps.Add(Management.Export(gm));
                gm.Steps.Add(Setup.End(gm, R.StringGuided_End));
            }
        }

        static public void ReCreateWhatIsNotSerialized(Common.Guided.Module gm)
        {
            gm.Steps = new List<Step>();
            ViewsOption(gm);
            BuildSteps(gm);
            ReLinkMeasureReceivedList(gm);

            { // desire step to start after TSU opening
                string desireName = "";
                if (gm.Finished)
                    desireName = "Export";
                else if (gm._ActiveStationModule._Station.ParametersBasic._IsSetup)
                    desireName = "Abstract" + GuidedModules.Steps.Measurement.Types.PreAlignment;
                else
                    desireName = "Abstract" + GuidedModules.Steps.Measurement.Types.Setup;
                gm.currentIndex = gm.GetStepIndexBasedOnName(desireName);
            }
        }

        private static void ReLinkMeasureReceivedList(Common.Guided.Module gm)
        {
            List<M.Measure> newList = new List<M.Measure>();
            foreach (var item in gm.ReceivedMeasures)
            {
                newList.Add(gm._ActiveStationModule._Station.MeasuresTaken.Find(x => x.Guid == item.Guid));
            }
            gm.ReceivedMeasures = newList;
        }
    }
}
