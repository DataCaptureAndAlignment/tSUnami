using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using TSU.ENUM;
using TSU.Views;
using TSU.Views.Message;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Polar
{
    public class View : CompositeView
    {
        private Buttons buttons;
        public new Polar.Module Module
        {
            get
            {
                return this._Module as Polar.Module;
            }
        }
        public Polar.Module TheodoliteModule
        {
            get
            {
                return this._Module as Polar.Module;
            }
        }
        public View(Polar.Module parent) : base(parent, Orientation.Horizontal, ModuleType.Theodolite)
        {
            this.Image = R.Theodolite;
            this.buttons = new Buttons(this);
            string creationDate = "(" + R.StringBigButton_CreatedOn + this.Module.CreatedOn.ToString("G", CultureInfo.CreateSpecificCulture("nl-BE")) + ")";
            string buttontext = string.Format("{0};{1}{2}", T.Conversions.StringManipulation.SeparateTitleFromMessage(this._Module.NameAndDescription), creationDate, T.Conversions.StringManipulation.SeparateMessageFromTitle(this._Module.NameAndDescription));
            this.AddTopButton(buttontext,
                    R.Theodolite, this.ShowContextMenuGlobal);
            base.AddPlusView(buttons.NewStation);
            this.Visible = true;
            UpdateView();
        }

        protected override void Dispose(bool disposing)
        {
            this.compositeViewStrategy = null;
            this.buttons = null;
            base.Dispose(disposing);
        }


        internal void ShowExportMenu()
        {
            List<Control> cm = new List<Control>();
            cm.Add(buttons.Export2Lgc2);
            cm.Add(buttons.Export2Geode);
            cm.Add(buttons.Export2PLGC);
            this.ShowPopUpSubMenu(cm, "export");
        }

        private void ShowContextMenuGlobal()
        {
            this.contextButtons.Clear();

            bool readyToExport = true;
            foreach (TSU.Polar.Station.Module item in this.Module.StationTheodoliteModules)
            {
                if (item.IsClosable)
                    readyToExport = true;
            }

            this.contextButtons.Add(buttons.NewStation);
            this.contextButtons.Add(buttons.FinishModule);
            if (readyToExport) this.contextButtons.Add(buttons.ExportMenu);

            //this.contextButtons.Add(buttons.Save);
            Common.FinalModule m;



            List<BigButton> stationButtons = new List<BigButton>();
            if ((m = this.Module) != null)
            {
                int stationNumber = 0;
                foreach (Common.Station.Module item in m.StationModules)
                {
                    string creationDate = "(" + R.StringBigButton_CreatedOn + item.CreatedOn.ToString("G", CultureInfo.CreateSpecificCulture("nl-BE")) + ")";

                    Station.Module stationModule = item as Station.Module;
                    if (stationModule != null)
                    {
                        stationNumber++;
                        System.Drawing.Image i = this.Image;
                        if (stationModule._Station.ParametersBasic._State is Common.Station.State.Bad) i = R.StatusBad;
                        if (stationModule._Station.ParametersBasic._State is Common.Station.State.Questionnable) i = R.StatusQuestionnable;
                        string text = $"ST {stationNumber} {creationDate};{stationModule._Station._Name}: {stationModule._Station.ParametersBasic._State._Description}";
                        stationButtons.Add(new BigButton(text, i, stationModule.ShowInParentModule));
                    }
                }

                // add a button to a manager if the number of station is huge
                {
                    if (m.StationModules.Count > 5)
                    {
                        stationButtons.Add(new BigButton("Stations details", R.Station, () => this.Module.ExploreStations()));
                    }
                }

                //Inverse la liste des stations
                for (int i = stationButtons.Count - 1; i >= 0; i--)
                {
                    this.contextButtons.Add(stationButtons[i]);
                }
            }
            this.ShowPopUpMenu(contextButtons);

        }
        private void OnNewStation(object sender, EventArgs e)
        {
            NewStation();
        }
        public void NewStation()
        {
            // check closure
            Polar.Station.Module stm = this.Module._ActiveStationModule as Polar.Station.Module;
            if (stm != null)
                if (!(stm.stationParameters._State is Common.Station.State.Bad))
                    if (!stm.CheckOpeningHasBeenControled(out bool cancelled))
                    {
                        if (cancelled)
                            return;
                        string titleAndMessage = $"{R.T_NO} {R.T_CONTROL_ON_THE_OPENING_POINT};{R.T_ARE_YOU_SURE_YOU_WANT_TO_CONTINUE_WITHOUT_CHECKING_THE_CLOSURE_ON_THE_OPENING_POINT}";
                        MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                        {
                            ButtonTexts = new List<string> { R.T_CONTROL, R.T_CONTINUE }
                        };
                        if (mi.Show().TextOfButtonClicked != R.T_CONTINUE) return;
                    }

            //  this.TryActionWithMessage(this.Module.NewStation, Logs.LogEntry.types.Success, "Creation of a new station");
            this.TryActionWithMessage(this.Module.NewStation, R.T_CREATION_OF_A_NEW_STATION);
        }
        private void FinishModule()
        {
            this.TryAction(this.TheodoliteModule.FinishModule);
        }
        //public void Save()
        //{
        //    this.TryAction(this.Module.SaveAs, Logs.LogEntry.types.Error, "Saving of data");
        //}
        //public void Open()
        //{
        //    this.TryAction(this.TheodoliteModule.Open);
        //}

        class Buttons
        {
            private Polar.View view;
            public BigButton NewStation;
            //public BigButton Open;
            //public BigButton Save;
            public BigButton ExportMenu;
            public BigButton Export2Geode;
            public BigButton Export2Lgc2;
            public BigButton Export2PLGC;
            public BigButton FinishModule;
            public Buttons(Polar.View view)
            {
                this.view = view;
                //Open = new BigButton(string.Format(R.T_OpenModule, this.view.Module._Name),R.Open, view.Open);
                //Save = new BigButton(string.Format(R.T_SaveModule, this.view.Module._Name),R.Save, view.Save);
                Export2Geode = new BigButton(R.T_B_MeasureExport2Geode, R.Geode, view.ExportToGeode);
                Export2Lgc2 = new BigButton(R.T_B_MeasureExportLGC2, R.Lgc, view.ExportToLGC2);
                Export2PLGC = new BigButton($"{"Export to PLGC"}", R.PLGC, view.ExportToPLGC);
                FinishModule = new BigButton(R.String_GM_FinishModule, R.FinishHim, view.FinishModule);
                NewStation = new BigButton(R.T210, R.Add, view.NewStation);
                ExportMenu = new BigButton(R.T_TH_Export, R.Export, view.ShowExportMenu, hasSubButtons: true);
            }
        }

        internal void ExportToLGC2()
        {
            this.TryAction(() => this.Module.ExportToLGC2(), "Export to LGC2 Format");
        }

        internal void ExportToPLGC()
        {
            this.TryAction(() => this.Module.ExportToPLGC(), "Export to PLGC Format");
        }

        internal void ExportToGeode()
        {
            this.TryAction(() => this.Module.ExportToGeode(), "Export to Geode Format");
        }
    }
}
