﻿using System;
using System.Xml.Serialization;
using TSU.Common.Elements;
using TSU.Common.Measures;

using System.Collections.Generic;


namespace TSU.Polar
{
    public partial class Station
    {
        [XmlIgnore]
        public List<Measure> GoodMeasuresTaken
        { 
            get
            {
                List<Measure> measures = new List<Measure>();
                foreach (var item in MeasuresTaken)
                {
                    if (item._Status.Type == Common.Measures.States.Types.Good)
                        measures.Add(item as Polar.Measure);
                }
                return measures;
            }

        }

        [Serializable]
        
        public class LSResults
        {
            public DoubleValue _Observed { get; set; }
            public double _Computed { get; set; }
            public double _Residual { get; set; }
            public double _ResBySigma { get; set; }
            public string _TargetName { get; set; }
            public double _TargetHeight { get; set; }
            public LSResults()
            {
                this._Observed = new DoubleValue();
            }

            public virtual void ReadXml(System.Xml.XmlReader reader)
            {
                _Computed = double.Parse(reader["Computed"]);
                _Residual = double.Parse(reader["Residual"]);
                _ResBySigma = double.Parse(reader["ResBySigma"]);
                _TargetName = reader["TargetName"];
                _TargetHeight = double.Parse(reader["TargetHeight"]);
                reader.ReadToFollowing("Observed"); _Observed.ReadXml(reader);
            }
            public virtual void WriteXml(System.Xml.XmlWriter writer)
            {
                writer.WriteAttributeString("Computed", _Computed.ToString());
                writer.WriteAttributeString("Residual", _Residual.ToString());
                writer.WriteAttributeString("ResBySigma", _ResBySigma.ToString());
                writer.WriteAttributeString("TargetName", _TargetName);
                writer.WriteAttributeString("TargetHeight", _TargetHeight.ToString());
                writer.WriteStartElement("Observed"); _Observed.WriteXml(writer); writer.WriteEndElement();
            }

            public System.Xml.Schema.XmlSchema GetSchema()
            {
                return null;
            }
        }
        public class LSAngleResults : LSResults
        {
            public double _Offset { get; set; }
            public override void ReadXml(System.Xml.XmlReader reader)
            {
                base.ReadXml(reader);
                _Offset = double.Parse(reader["Offset"]);
            }
            public override void WriteXml(System.Xml.XmlWriter writer)
            {
                base.WriteXml(writer);
                writer.WriteAttributeString("Offset", _Offset.ToString());
            }

        }
        public class LSDistanceResults : LSResults
        {
            public double _Sensibility { get; set; }
            public DoubleValue _Constante { get; set; }
            public LSDistanceResults()
            {
                this._Constante = new DoubleValue();
            }
            public override void ReadXml(System.Xml.XmlReader reader)
            {
                _Sensibility = double.Parse(reader["Sensibility"]);
                base.ReadXml(reader);
                reader.ReadToFollowing("Constante"); _Constante.ReadXml(reader);

            }
            public override void WriteXml(System.Xml.XmlWriter writer)
            {

                writer.WriteAttributeString("Sensibility", _Sensibility.ToString());
                base.WriteXml(writer);
                writer.WriteStartElement("Constante"); _Constante.WriteXml(writer); writer.WriteEndElement();
            }

        }
    }
}
