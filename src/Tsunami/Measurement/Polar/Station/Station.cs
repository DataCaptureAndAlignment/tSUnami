using System;
using System.Collections.Generic;
using System.Xml;
using System.Linq;
using System.Xml.Serialization;
using TSU.ENUM;
using R = TSU.Properties.Resources;
using TSU.Common.Elements;
using I = TSU.Common.Instruments;
using TSU.Common.Elements.Composites;
using M = TSU.Common.Measures;
using TSU.IO.SUSoft;
using TSU.Common.Measures.States;
using System.Xaml;
using System.Collections.ObjectModel;

namespace TSU.Polar
{
    [Serializable]

    [XmlType(TypeName = "Polar.Station")]
    public partial class Station : Common.Station
    {

        [XmlIgnore]
        public virtual TSU.Polar.Station.Parameters Parameters2
        {
            get
            {
                return this.ParametersBasic as TSU.Polar.Station.Parameters;
            }
            set
            {
                this.ParametersBasic = value;
            }
        }

        [XmlIgnore]
        public M.WeatherConditions WeatherConditions
        {
            get
            {
                if (this.MeasuresTaken.Count > 0)
                    return (this.MeasuresTaken[0] as Polar.Measure).Distance.WeatherConditions;
                else
                    return new M.WeatherConditions();
            }
        }

        [XmlIgnore]
        public virtual List<Polar.Measure> References
        {
            get
            {
                return this.MeasuresTaken.Cast<Polar.Measure>().ToList().FindAll(x => (x as Polar.Measure).UsedAsReference);
            }
        }

        [XmlIgnore]
        public virtual List<Polar.Measure> Rays
        {
            get
            {
                return this.MeasuresTaken.Cast<Polar.Measure>().ToList().FindAll(x => x.UsedAsReference == false && !x.IsControl);
            }

        }

        [XmlIgnore]
        public bool IsANewFrame
        {
            get
            {
                var goodMeasuresOfKnownPoints = this.MeasuresTaken.FindAll(
                    x => x._Status.Type == Types.Good 
                    && x._OriginalPoint != null 
                    && x._OriginalPoint._Coordinates !=null 
                    && x._OriginalPoint._Coordinates.HasAny);
                return goodMeasuresOfKnownPoints.Count == 0;
            }
            internal set
            {
            }
        }

        public List<Point> PdorPoints
        {
            get
            {
                List<Point> l = new List<Point>();
                foreach (var item in this.MeasuresTaken)
                {
                    if (!item.IsControl)
                    {
                        if (item._Point.Type != Point.Types.Reference)
                        {
                            if ((item.GeodeRole & Geode.Roles.Pdor) == Geode.Roles.Pdor)
                            {
                                bool hasNewPointCoordinates = item._Point?._Coordinates?.HasAny ?? false;
                                bool hasOrigianlPointCoordinates = item._OriginalPoint?._Coordinates?.HasAny ?? false;
                                if (hasNewPointCoordinates && !hasOrigianlPointCoordinates)
                                    l.Add(item._Point);
                                else
                                    l.Add(item._OriginalPoint);
                            }
                        }
                    }
                }
                return l;
            }
        }

        [XmlIgnore]
        public List<Point> VariablePoints
        {
            get
            {
                List<Point> l = new List<Point>();
                foreach (var item in this.MeasuresTaken)
                {
                    if (!item.IsControl && !(item._Status.Type == Types.Bad)) // if we keep the bad than we can endup with variable points withotu observations tsu-3064
                    {
                        if (item._Point.Type != Point.Types.Reference)
                        {
                            if ((item.GeodeRole & (Geode.Roles.Poin | Geode.Roles.Unknown | Geode.Roles.Pdor)) > 0)
                            {
                                bool hasOriginalCoordinates = item._OriginalPoint?._Coordinates?.HasAny ?? false;
                                bool hasNewPointCoordinates = item._Point?._Coordinates?.HasAny ?? false;
                                if (hasNewPointCoordinates || !hasOriginalCoordinates)
                                    l.Add(item._Point);
                                else
                                    l.Add(item._OriginalPoint);
                            }
                        }
                    }
                }
                return l;
            }
        }

        [XmlIgnore]
        public List<Point> ReferencePoints
        {
            get
            {
                List<Point> l = new List<Point>();
                foreach (var item in this.MeasuresTaken)
                {
                    if ((item as Polar.Measure).UsedAsReference)
                    {
                        if (item._OriginalPoint != null)
                            l.Add(item._OriginalPoint);
                    }
                }

                // compatibility
                // if ref=0 but station is setup => role unknown = cala
                if (l.Count == 0)
                {
                    foreach (var item in this.MeasuresTaken)
                    {
                        if ((item as Polar.Measure).GeodeRole == Geode.Roles.Unknown)
                            if (item._OriginalPoint != null)
                                l.Add(item._OriginalPoint);
                    }
                }

                //// warning, if station point is in reference point list, it should be first to help the radi/vxy selection
                //if (_SetupStrategy == null)
                //    if (this.Parameters2._StationPoint != null)
                //    {
                //        if (this.Parameters2._StationPoint._Coordinates.HasAny)
                //            Parameters2.CompensationStrategyType = Advanced.Theodolite.Polar.Compensations.List.OrientatedOnly_LGC2;
                //        else
                //            Parameters2.CompensationStrategyType = Advanced.Theodolite.Polar.Compensations.List.FreeStation_LGC2;
                //    }
                //    else
                //        Parameters2.CompensationStrategyType = Advanced.Theodolite.Polar.Compensations.List.Local_LGC2;



                //bool stationPointIsFixed = true;
                //if (_SetupStrategy._strategy == Advanced.Theodolite.Polar.Compensations.List.FreeStation_LGC2) stationPointIsFixed = false;
                //if (_SetupStrategy._strategy == Advanced.Theodolite.Polar.Compensations.List.FreeStation) stationPointIsFixed = false;
                //if (_SetupStrategy._strategy == Advanced.Theodolite.Polar.Compensations.List.NonVerticalized) stationPointIsFixed = false;

                //if (stationPointIsFixed)
                //    if (Parameters2._StationPoint != null)
                //        l.Add(Parameters2._StationPoint);

                //foreach (MeasureTheodolite item in this.References)
                //{
                //    if (item._OriginalPoint._Coordinates.HasAny)
                //    { // normaly coordinate theo are in the original point
                //        if (item._OriginalPoint._Origin != this._Name)
                //            l.Add(item._OriginalPoint);
                //    }
                //    else
                //    { // but it the point is new and coordinates aprox have been compute there are in _point
                //        if (item._Point._Coordinates != null)
                //        {
                //            if (item._Point._Coordinates.HasAny)
                //            {
                //                if (item._Point._Origin != this._Name)
                //                    l.Add(item._Point);
                //            }
                //        }
                //    }
                //}

                //foreach (MeasureTheodolite item in this.Rays)
                //{
                //    if (item._OriginalPoint._Origin != this._Name)
                //    {
                //        if (item._OriginalPoint.IsTheo)
                //            l.Add(item._OriginalPoint);
                //    }
                //}

                //if (nextMeasure != null) l.Add(nextMeasure._OriginalPoint);

                return l;
            }
        }


        [XmlIgnore]
        public override ReadOnlyCollection<Point> PointsMeasured
        {
            get
            {
                CloneableList<Point> l = new CloneableList<Point>();
                foreach (var item in MeasuresTaken)
                {
                    if (item !=null && item._Status.Type != Types.Control)
                        l.Add(item._Point);
                }
                return new ReadOnlyCollection<Point>(l);
            }
        }

        [XmlAttribute("Name")]
        public new string _Name
        {
            get
            {
                string s = "";

                if (this.Parameters2._StationPoint == null)
                    s = "STtheo_" + this.Parameters2._Date.ToString("yyMMdd_HHmmss");
                else

                    //s = "STtheo_" + this.Parameters2._Date.ToString("yyMMdd_HHmmss") + "_" + this.Parameters2._StationPoint._Name;
                    s = "STtheo_" + this.Parameters2._Date.ToString("yyMMdd_HHmmss") + "_" + this.Parameters2._StationPoint._Name.Replace('.', '_');// if point a re used it will mess with the composite element who want t ocut at each points
                base._Name = s;
                return s;
            }
            set
            {
                //throw new Exception(Station Cannot be set, it is build from date and station point name..);
            }
        }

        [XmlIgnore]
        public Polar.Measure MeasuresToDoCome
        {
            get
            {
                if (MeasuresToDo == null) return null;
                if (MeasuresToDo.Count == 0) return null;
                return MeasuresToDo[0] as Polar.Measure;
            }
        }

        public Polar.Measure ClosureMeasure
        {
            get
            {
                return MeasuresTaken.FirstOrDefault(x => x._Status is M.States.Good) as Polar.Measure;
            }
        }


        public Polar.Measure NextMeasure;

        public Station()
        {
            this.Parameters2 = new TSU.Polar.Station.Parameters(this);
        }
        public Station(Module module)
        {
            this.Parameters2 = new TSU.Polar.Station.Parameters(module);
        }

        public override object Clone()
        {
            Station newStation = base.Clone() as Station;
            newStation.Guid = Guid.NewGuid();
            newStation.Parameters2 = this.Parameters2.Clone() as TSU.Polar.Station.Parameters;


            return newStation;
        }

        public override string ToString()
        {
            string s = String.Format(R.TS_Station, this._Name);
            return s += "\r\n" + this.Parameters2.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"> MANAGEMENT,SETUP, </param>
        /// <returns></returns>
        public override string ToString(string param)
        {

            string s = "";
            try
            {
                switch (param.ToUpper())
                {
                    case "MANAGEMENT":
                        s = base.ToString(param);
                        break;
                    case "SETUP":
                        if (this.Parameters2 != null)
                        {
                            if (this.Parameters2._StationPoint != null)
                            {
                                string pos = R.T_POSITION + ":";
                                string ptc = R.T_PT_CODE + ":";
                                string dIHfC = R.T_DEFAULT_IH_FROM_CODE + " [m]:";
                                s += $"{pos,25} {(this.Parameters2._StationPoint != null ? this.Parameters2._StationPoint._Name : "?")}\r\n";
                                s += $"{ptc,25} {(this.Parameters2._StationPoint.SocketCode != null ? this.Parameters2._StationPoint.SocketCode.Id : "0")}  \r\n";
                                s += $"{dIHfC,25} {(this.Parameters2._StationPoint.SocketCode != null ? this.Parameters2._StationPoint.SocketCode.InstrumentHeightForPolarMeasurement : 0)}\r\n";
                            }
                            if (this.Parameters2._InstrumentHeight != null)
                            {
                                string ig = R.T_IH + ":";
                                s += $"{ig,25} {this.Parameters2._InstrumentHeight}\r\n";
                            }
                            if (this.Parameters2.DefaultMeasure != null)
                            {
                                string dr = R.T_DEFAULT_REFL + ":";
                                string mt = R.T_MEASURE_TYPE + ":";
                                s += $"{dr,25} {this.Parameters2.DefaultMeasure.Distance.Reflector._Name}\r\n";
                                s += $"{mt,25} {this.Parameters2.DefaultMeasure.NumberOfMeasureToAverage + " * " + this.Parameters2.DefaultMeasure.Face}\r\n";
                            }
                        }
                        break;

                    default:
                        return $"{R.T_CANNOT_WRITE_THE_THEODOLITE_STATION_AS_A_STRING_UNKNOWN_PARAMETER}.";
                }
            }
            catch (Exception ex)
            {
                return $"{R.T_CANNOT_WRITE_THE_THEODOLITE_STATION_AS_A_STRING} {ex.Message}.";
            }
            return s;
        }

        public string ToString(Polar.Measure m)
        {
            string declaration = "";
            try
            {
                string codeSt = (this.Parameters2._StationPoint == null || this.Parameters2._StationPoint.SocketCode == null) ? $"Code {R.String_Unknown}" : this.Parameters2._StationPoint.SocketCode._Name;
                string codeP = (m._Point.SocketCode == null) ? $"Code {R.String_Unknown}" : $"{m._Point.SocketCode._Name}: '{m._Point.SocketCode.Description}'";

                string descriptionIH =
                    (this.Parameters2.Setups.InitialValues.IsInstrumentHeightKnown == TSU.Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight.Known) ?
                     this.Parameters2._InstrumentHeight.Value.ToString() : "to be computed in ";

                string extension = m.Extension.Value != na ? m.Extension.ToString("m-mm") + " mm" : m._Point.SocketCode != null ? (m._Point.SocketCode.DefaultExtensionForPolarMeasurement * 1000).ToString() + " mm" : "0.0 mm";

                declaration = string.Format(R.T268,
                    m.Face, $"{m._Point._Name} ({codeP})", m.Distance.Reflector._Name, extension,
                    (this.Parameters2._StationPoint != null) ? this.Parameters2._StationPoint._Name : "station", descriptionIH, codeSt, m.Interfaces._Name);
                if (m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates != null)
                    if (m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates.Value != 0)
                        declaration += $" (dH = {m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates.Value * 1000} mm)";
            }
            catch (Exception)
            {
                declaration = "cannot render";
            }
            return declaration;
        }


        internal bool IsAllActiveMeasuresHaveAFaceXMeasurement(I.FaceType type)
        {
            foreach (Polar.Measure item1 in this.MeasuresTaken)
            {
                string seek = item1._Point._Name;
                bool found = false;
                foreach (Polar.Measure item2 in this.MeasuresTaken)
                {
                    if (item2._Point._Name == seek)
                        if (item2.Face == type)
                            found = true;
                }
                if (!found) return false;
            }
            return true;
        }
        internal I.FaceType GetTypeOfTheMeasuredFaces()
        {
            if (IsAllActiveMeasuresHaveAFaceXMeasurement(I.FaceType.DoubleFace))
                return I.FaceType.DoubleFace;
            if (IsAllActiveMeasuresHaveAFaceXMeasurement(I.FaceType.Face1))
                if (!IsAllActiveMeasuresHaveAFaceXMeasurement(I.FaceType.Face2))
                    return I.FaceType.Face1;
            return I.FaceType.UnknownFace;
        }

        public override int GetHashCode()
        {
            return
                base.GetHashCode() ^
                Parameters2.GetHashCode();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsRoughlySetup()
        {
            if (this.Parameters2._StationPoint == null)
                return false;
            else
                return
                    this.Parameters2._StationPoint._Coordinates.HasLocal &&
                    this.Parameters2.vZero != null &&
                    this.Parameters2._InstrumentHeight != null;
        }

        // will remove the measure from the list (but clean also the related point from the Tsunami.propertires.points) why? noo!
        internal void RemoveMeasureFromToDoList(M.Measure m)
        {
            MeasuresToDo.Remove(m);
        }

        // will remove the measure of this position from the list but clean also the related point from the Tsunami.propertires.points
        internal void RemoveMeasureFromToDoListAt(int index)
        {
            var measure = MeasuresToDo[index];
            if (measure != null)
            {
                RemoveMeasureFromToDoList(measure);
            }
        }
    }
}
