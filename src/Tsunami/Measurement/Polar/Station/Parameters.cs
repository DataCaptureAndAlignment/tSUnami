﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using TSU.Common.Elements;
using TSU.Common.Instruments.Reflector;
using C = TSU.Common.Compute.Compensations;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using R = TSU.Properties.Resources;

namespace TSU.Polar
{
    public partial class Station
    {
        [Serializable]
        [XmlType(TypeName = "Polar.Station.Parameters")]
        public new class Parameters : Common.Station.Parameters
        {
            public virtual Setup Setups { get; set; }

            [Serializable]
            public class Setup
            {
                [Serializable]
                public class Values
                {
                    [XmlIgnore]
                    public Module Module;
                    private C.Strategies.List compensationStrategyType;

                    public Values()
                    {

                    }

                    public C.Strategies.List CompensationStrategyType
                    {
                        get
                        {
                            return compensationStrategyType;
                        }
                        set
                        {
                            compensationStrategy = null;
                            compensationStrategyType = value;
                        }
                    }

                    /// <summary>
                    /// compute launch by tsunai and not by user
                    /// use to now if wes should use a 90° radi to avoid 2 correct solution of the least square adjutment see TSU-3855
                    /// </summary>
                    public bool TemporaryCompute { get; set; } = false; 

                    public string Lgc2ProjectPath { get; set; } = "";

                    private C.Strategies.Common compensationStrategy;

                    [XmlIgnore]
                    public C.Strategies.Common CompensationStrategy
                    {
                        get
                        {
                            if (compensationStrategy == null)
                                compensationStrategy = C.Strategies.Common.GetStrategy(CompensationStrategyType, Module);

                            return compensationStrategy;
                        }
                    }

                    public enum InstrumentTypeHeight
                    {
                        NotDefinedYet, Known, Unknown,
                    }

                    [XmlIgnore]
                    public bool HeightSetup
                    {
                        get
                        {
                            if (IsInstrumentHeightKnown == InstrumentTypeHeight.Unknown) return true;
                            if (IsInstrumentHeightKnown == InstrumentTypeHeight.Known)
                                if (InstrumentHeight != null) return true;
                            return false;
                        }
                    }

                    public enum InstrumentVerticalisation
                    {
                        Verticalised, NotVerticalised, Unknown,
                    }

                    public DoubleValue InstrumentHeight { get; set; }

                    public InstrumentVerticalisation VerticalisationState { get; set; }

                    [XmlAttribute]
                    public bool Ready { get; set; }
                    [XmlAttribute]
                    public string Version { get; set; }
                    [XmlAttribute]
                    public double SigmaZero { get; set; } = Tsunami2.Preferences.Values.na;

                    [XmlAttribute]
                    public string ReferenceSurface { get; set; }

                    [XmlAttribute]
                    public bool Verified { get; set; }
                    public InstrumentTypeHeight IsInstrumentHeightKnown { get; set; }
                    public Coordinates.CoordinateSystemsTypes CoordinatesSystemsUsedForCompute { get; set; }
                    public C.Strategies.List Strategy;
                    public bool IsPositionKnown { get; set; }
                    public DoubleValue vZero { get; set; }
                    public List<Point> VariablePoints { get; set; }
                    public List<Measure> Measures { get; set; }


                    [XmlIgnore]
                    public virtual Point StationPoint   //virtual for mockup
                    {
                        get
                        {
                            return Point.GetGlobalPointByGuid(guid: StationPointGUID);
                        }
                        set
                        {
                            if (value == null)
                            {
                                StationPointGUID = Guid.Empty;
                                return;
                            }
                            if (this.StationPoint != null)
                                TSU.Debug.WriteInConsole($"._Point replacement in SetupValue: from \r\n{this.StationPoint._Name} by \r\n{value._Name}");
                            Point.SetAsGlobalPoint(value, guid: ref StationPointGUID);
                        }
                    }

                    [XmlAttribute]
                    public Guid StationPointGUID;
                    public string LgcInputContent { get; set; }
                    public string LgcOutputContent { get; set; }

                    public C.Frame computedFrame;

                    public Values(Module module)
                    {
                        Module = module;
                    }

                    public object Clone()
                    {
                        Values clone = this.MemberwiseClone() as Values;
                        if (this.vZero != null)
                            clone.vZero = this.vZero.Clone() as DoubleValue;
                        if (this.InstrumentHeight != null)
                            clone.InstrumentHeight = this.InstrumentHeight.Clone() as DoubleValue;
                        if (this.VariablePoints != null)
                        {
                            clone.VariablePoints = new List<Point>();
                            clone.VariablePoints.AddRange(this.VariablePoints);
                        }
                        if (this.Measures != null)
                        {
                            clone.Measures = new List<Measure>();
                            clone.Measures.AddRange(this.Measures);
                        }
                        if (this.StationPoint != null)
                            clone.StationPointGUID = this.StationPointGUID;
                        if (this.computedFrame != null)
                            clone.computedFrame = this.computedFrame.Clone() as C.Frame;

                        return clone;
                    }


                    internal static Values Merge(Values tobeUpdated, Values newData)
                    {
                        Values updated = tobeUpdated.Clone() as Values;
                        updated.CoordinatesSystemsUsedForCompute = newData.CoordinatesSystemsUsedForCompute;
                        updated.InstrumentHeight = newData.InstrumentHeight;
                        updated.LgcInputContent = newData.LgcInputContent;
                        updated.LgcOutputContent = newData.LgcOutputContent;
                        updated.Measures = newData.Measures;
                        updated.Ready = newData.Ready;
                        updated.ReferenceSurface = newData.ReferenceSurface;
                        updated.SigmaZero = newData.SigmaZero;

                        //updated.StationPointGUID = newData.StationPointGUID; // this is losing some info of the point that are now following during LGC compute such as SocketCode
                        if(!updated.IsPositionKnown)
                            updated.StationPoint._Coordinates = newData.StationPoint._Coordinates;
                        
                        updated.VariablePoints = newData.VariablePoints;
                        updated.Verified = newData.Verified;
                        updated.Version = newData.Version;
                        updated.vZero = newData.vZero;
                        updated.computedFrame = newData.computedFrame;

                        return updated;
                    }

                    internal string GetV0Information()
                    {
                        // V0 sigma and error created per m or at controled distance
                        double sigmaGon = vZero.Sigma;

                        double sigmaCc = sigmaGon * 100 * 100;
                        double sigmaRad = sigmaGon / 200 * Math.PI;
                        double errorInMmAtOneMeter = 1000 * Math.Sin(sigmaRad); // error in mm
                        string controlArFirstPoint = Measures[0]._Point._Name;
                        double errorAtControlPoint = Measures[0].distanceHorizontal.Value * errorInMmAtOneMeter;


                        return $"V0 sigma = {sigmaCc.ToString("0")} cc <=> {errorInMmAtOneMeter,0:0.00} mm/m" +
                            $" <=> {errorAtControlPoint,0:0.00} mm at {controlArFirstPoint} distance.\r\n\r\n";
                    }
                }

                public Setup()
                {

                }
                public Setup(Module module)
                {
                    InitialValues = new Values(module);
                }
                #region State

                public enum States
                {
                    Unknown,
                    Approximated,
                    VerifiedApproximation,
                    Done
                }

                [XmlIgnore]
                public States ActualState
                {
                    get
                    {
                        if (IsSetForGood) return States.Done;
                        if (TemporaryValues != null)
                            if (TemporaryValues.Verified)
                                return States.VerifiedApproximation;
                            else
                                return States.Approximated;
                        return States.Unknown;
                    }
                }

                [XmlIgnore]
                public bool IsSetForGood
                {
                    get
                    {
                        return (this.FinalValues != null);
                    }
                }
                public Setup Clone()
                {
                    Setup clone = this.MemberwiseClone() as Setup; // to make a clone of the primitive types fields
                    var a = clone.InitialValues; if (a != null) clone.InitialValues = a.Clone() as Values;
                    var b = clone.TemporaryValues; if (b != null) clone.TemporaryValues = b.Clone() as Values;
                    var c = clone.FinalValues; if (c != null) clone.FinalValues = c.Clone() as Values;
                    return clone;
                }

                public virtual Values InitialValues { get; set; }
                
                /// <summary>
                /// temporary values, used for Goto, before the compensation is validated
                /// </summary>
                public Values TemporaryValues { get; set; }

                /// <summary>
                /// Values computed in the Compensation Manager, awaiting validation
                /// </summary>
                public Values ProposedValues { get; set; }

                /// <summary>
                /// final values
                /// </summary>
                public Values FinalValues { get; set; }


                [XmlIgnore]
                public Values BestValues
                {
                    get
                    {
                        if (IsSetForGood) return FinalValues;
                        if (ProposedValues != null) return ProposedValues;
                        if (TemporaryValues != null) return TemporaryValues;
                        return null;
                    }
                }

                public bool IsAFreeStation
                {
                    get
                    {
                        return (!InitialValues.IsPositionKnown && InitialValues.IsInstrumentHeightKnown == Values.InstrumentTypeHeight.Known);
                    }
                    #endregion
                }

            }

            public enum TypeOfPointMeasured
            {
                UnknownYet,
                UnknownPointsOnly,
                KnownPointsOnly,
                BothKnownAndUnknown
            }

            [XmlIgnore]
            public Module Module;




            [XmlIgnore]
            public override bool _IsSetup
            {
                get
                {
                    return (this.Setups.FinalValues != null);
                }
            }


            public TypeOfPointMeasured TypeOfPointsMeasured { get; set; }

            public string Condition { get; set; } = "DEFAULT";

            public override object Clone()
            {
                Parameters clone = (Parameters)MemberwiseClone(); // to make a clone of the primitive types fields
                clone.Setups = this.Setups.Clone() as Setup;
                return clone;
            }

            #region sigmas of instrument

            // need to be deserialized if the user change them manually
            public List<I.SigmaForAInstrumentCouple> CustomAprioriSigma { get; set; } = new List<I.SigmaForAInstrumentCouple>();


            private List<I.SigmaForAInstrumentCouple> aprioriSigmaForTheSelectedInstrumentAndConditon;
            [XmlIgnore]
            public List<I.SigmaForAInstrumentCouple> AprioriSigmaForTheSelectedInstrumentAndConditon
            {
                get
                {
                    // check if need to sort everything again
                    if (aprioriSigmaForTheSelectedInstrumentAndConditon != null)
                        return aprioriSigmaForTheSelectedInstrumentAndConditon;
                    List<I.SigmaForAInstrumentCouple> sigmas = null;

                    // sort from all sigma
                    if (this._Instrument != null)
                    {
                        sigmas = new List<I.SigmaForAInstrumentCouple>();
                        sigmas = Common.Analysis.Instrument.GetAprioriSigma(this._Instrument._InstrumentType.ToString(), this.Condition,
                           Tsunami2.Preferences.Values.InstrumentSlashTargetCoupleSigmas.Concat(this.CustomAprioriSigma).ToList());

                        if (sigmas != null) if (sigmas.Count == 0)
                                sigmas = Common.Analysis.Instrument.GetAprioriSigma("DEFAULT", this.Condition, Tsunami2.Preferences.Values.InstrumentSlashTargetCoupleSigmas);

                        if (sigmas != null) if (sigmas.Count == 0)
                                sigmas = Common.Analysis.Instrument.GetAprioriSigma("DEFAULT", "DEFAULT", Tsunami2.Preferences.Values.InstrumentSlashTargetCoupleSigmas);

                    }
                    return sigmas;
                }
            }

            internal void ResetAprioriSigma()
            {
                aprioriSigmaForTheSelectedInstrumentAndConditon = null; // to force the full list to be reprocess
            }

            #endregion

            private I.InstrumentTolerance tolerance;
            public I.InstrumentTolerance Tolerance
            {
                get
                {
                    if (tolerance == null)
                    {
                        if (this._Instrument != null)
                        {
                            tolerance = Tsunami2.Preferences.Values.InstrumentsTolerances.Find(x => x.InstrumentType == this._Instrument._InstrumentType);
                            if (tolerance != null)
                                return tolerance;
                        }
                        return new I.InstrumentTolerance(I.InstrumentTypes.Unknown, 0, 0, 0, 0, 0, 0);
                    }
                    else
                    {
                        return tolerance;
                    }
                }
                set
                {
                    tolerance = value;
                }
            }

            [XmlIgnore]
            public new Measure DefaultMeasure
            {
                get
                {
                    return base.DefaultMeasure as Measure;
                }
                set
                {
                    base.DefaultMeasure = value;
                }
            }

            [XmlIgnore]
            public virtual DoubleValue _InstrumentHeight // virtual for mockup
            {
                get
                {
                    switch (Setups.InitialValues.IsInstrumentHeightKnown)
                    {
                        case Setup.Values.InstrumentTypeHeight.Known:
                            return Setups.InitialValues.InstrumentHeight;
                        case Setup.Values.InstrumentTypeHeight.Unknown:
                            if (Setups.BestValues != null) return Setups.BestValues.InstrumentHeight;
                            break;
                        default:
                            break;
                    }
                    throw new Exception(R.T_INSTRUMENT_HEIGHT_NOT_DEFINED);
                }
            }

            [XmlIgnore]
            public virtual Point _StationPoint  // virtual for mockup
            {
                get
                {
                    var best = Setups.BestValues;
                    if (best != null)
                        if (best.StationPoint != null  && best.StationPoint._Coordinates!=null && best.StationPoint._Coordinates.HasAny)
                            return best.StationPoint;
                    return Setups.InitialValues.StationPoint;
                }
                set
                {
                    if (_IsSetup) throw new TsuException(R.T_ALREADY_SETUP);
                    if (Setups.TemporaryValues != null)
                        Setups.TemporaryValues.StationPoint = value;
                    Setups.InitialValues.StationPoint = value;
                }
            }

            [XmlIgnore]
            public virtual Guid _StationPointGuid  // virtual for mockup
            {
                get
                {
                    if (Setups.BestValues != null)
                        if (Setups.BestValues.StationPoint != null)
                            return Setups.BestValues.StationPointGUID;
                    return Setups.InitialValues.StationPointGUID;
                }
                set
                {
                    if (_IsSetup) throw new TsuException(R.T_ALREADY_SETUP);
                    if (Setups.TemporaryValues != null)
                        Setups.TemporaryValues.StationPointGUID = value;
                    Setups.InitialValues.StationPointGUID = value;
                }
            }

            [XmlIgnore]
            public DoubleValue vZero
            {
                get
                {
                    if (Setups.BestValues != null) return Setups.BestValues.vZero;
                    return null;
                }
            }




            #region construction

            public Parameters()
                : base()
            {

            }

            public Parameters(Station station)
                : base(station)
            {
                this.InitializeProperties();
            }

            public Parameters(Module module)
                : base(module.StationTheodolite)
            {
                this.Module = module;
                this.InitializeProperties();
            }

            private void InitializeProperties()
            {
                this.Setups = new Setup(this.Module);
                this.Setups.InitialValues = new Setup.Values(this.Module) { IsInstrumentHeightKnown = Setup.Values.InstrumentTypeHeight.NotDefinedYet };
               
                if (this.DefaultMeasure == null)
                {
                    var point = new Point()
                    {
                        SocketCode = Tsunami2.Preferences.Values.SocketCodes.Find(x => x.Id == "0"),
                        _Name = FindBestDefaultName(),
                        StorageStatus = Common.Tsunami.StorageStatusTypes.Hidden,
                    };

                    this.DefaultMeasure = new Measure()
                    {
                        Extension = new DoubleValue(Tsunami2.Preferences.Values.na),
                        Face = I.FaceType.DoubleFace,
                        PointGUID = point.Guid
                    };
                    if (this._Station != null) this.DefaultMeasure.Origin = this._Station._Name;
                }
            }

            public static string FindBestDefaultName()
            {
                // find best zone
                string zoneName = "ZONE";
                foreach (var item in Common.Analysis.Module.GetModules_StationThedolite(Tsunami2.Properties))
                {
                    zoneName = item.GetBestZoneName();
                }

                return $"{zoneName}.NEW.POINT.###";
            }

            #endregion

            public override string ToString()
            {
                string s = base.ToString();

                string ih = "";
                switch (this.Setups.InitialValues.IsInstrumentHeightKnown)
                {
                    case Setup.Values.InstrumentTypeHeight.Known: ih = $"{R.T_THE_INSTRUMENT_HEIGHT_IS} '{this._InstrumentHeight.ToString(5)}'"; break;
                    case Setup.Values.InstrumentTypeHeight.Unknown: ih = R.T_THE_INSTRUMENT_HEIGHT_IS_NOT_KNOWN_AND_WILL_BE_DETERMINE_IN_THE_STATION_ADJUSTMENT; break;
                    default: ih = R.T_IT_IS_NOT_YET_DEFINED_IF_THE_INSTRUMENT_HEIGHT_IS_KNOWN_OR_NOT; break;
                }
                string n = (this._StationPoint != null) ? this._StationPoint._Name : R.T_NO_NAME;
                s += "\r\n" + $"{R.T_THE_STATION_IS_SETUP_ON_THE_POINT} '{n}'. {ih}";
                s += "\r\nDefault " + DefaultMeasure.ToString();
                return s;
            }

            public override int GetHashCode()
            {
                int temp = 1;
                if (_StationPoint != null)
                    temp = _StationPoint.GetHashCode();
                return
                    base.GetHashCode() ^ temp;

            }

            // IXmlSerializable  the automatic one was not working with _paramers ?
            public override System.Xml.Schema.XmlSchema GetSchema()
            {
                return null;
            }


            internal void GetGhostPointForV0(out Point ghostPoint, out Measure ghostMeasure, Reflector reflector, bool bLocal)
            {
                ghostPoint = _StationPoint.DeepCopy() as Point;
                ghostPoint._Name = "GHOST";
                if (bLocal)
                    ghostPoint._Coordinates.Local.Y.Value += 100;
                else
                    //TODO check if it is ok to do like that in  CCS??? seems ok, Camille checks the V0 computated with 2 stations, one with gost technic the other with the refs that determine that v0.
                    ghostPoint._Coordinates.Ccs.Y.Value += 100;

                ghostMeasure = new Measure()
                {
                    Angles = new M.MeasureOfAngles()
                    {
                        Corrected = new Angles()
                        {
                            Horizontal = new DoubleValue(-this.vZero.Value)
                        }

                    },
                    Distance = new M.MeasureOfDistance() { Reflector = reflector }
                };

                ghostMeasure._OriginalPoint = ghostPoint;
                ghostMeasure._Point = ghostPoint;
            }
        }
    }
}
