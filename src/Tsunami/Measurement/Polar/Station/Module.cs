using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;
using TSU.Common.Compute;
using TSU.Common.Compute.Transformation;
using TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using TSU.Common.Instruments.Device;
using TSU.Common.Instruments.Reflector;
using TSU.Common.Measures.States;
using TSU.ENUM;
using TSU.IO.SUSoft;
using TSU.Views;
using TSU.Views.Message;
using static TSU.Polar.Station.Parameters.Setup;
using CS = TSU.Common.Compute.Compensations.Strategies;
using E = TSU.Common.Elements;
using EC = TSU.Common.Elements.Composites;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using MID = TSU.Common.Instruments.Device;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;
using T = TSU.Tools;
using Z = TSU.Common.Zones;

namespace TSU.Polar
{
    public partial class Station
    {
        

        [XmlType(TypeName = "Polar.Station.Module")]
        [Serializable]
        public new partial class Module : Common.Station.Module
        {
            [XmlIgnore]
            public string NAME_BASED_ON_TIME = "Based on time";

            [XmlIgnore]
            public new List<DsaFlag> DsaFlags
            {
                get => this.FinalModule?.DsaFlags;
            }

            [XmlIgnore]
            public bool FaceMeasurementHasBeenManuallySet = false;

            public bool pinnedTableStatus = false;
            public List<string> pinnedPointName = new List<string>();

            #region Fields and Properties

            [XmlIgnore]
            public Measure LastMeasureReceived { get; set; }

            [XmlIgnore]
            public E.Manager.Module ElementManager
            {
                get
                {
                    return FinalModule._ElementManager;
                }

            }

            [XmlIgnore]
            public override ReadOnlyCollection<M.Measure> MeasuresReceived
            {
                get
                {
                    var list = new List<M.Measure>();
                    return new ReadOnlyCollection<M.Measure>(list);
                }
            }

            [XmlIgnore]
            public Station StationTheodolite
            {
                get
                {
                    return this._Station as Station;
                }
                set
                {
                    this._Station = value;
                }
            }

            [XmlIgnore]
            public new View View
            {
                get
                {
                    return this._TsuView as View;
                }
                set
                {
                    this._TsuView = value;
                }
            }
            public Parameters stationParameters
            {
                get
                {
                    return this._Station.ParametersBasic as Parameters;
                }
            }



            [XmlAttribute]
            public Polar.Module PolarModule
            {
                get
                {
                    return this.FinalModule as Polar.Module;
                }
            }

            [XmlIgnore]
            public override string _Name
            {
                get
                {
                    if (this.StationTheodolite != null)
                        return this.StationTheodolite._Name;
                    else
                        return "StationTheodolite";
                }
                set
                {

                }
            }

            [XmlIgnore]
            public bool GotoWanted
            {
                get
                {
                    return (_InstrumentManager.SelectedInstrumentModule as I.PolarModule).GotoWanted;
                }
                set
                {
                    (_InstrumentManager.SelectedInstrumentModule as I.PolarModule).GotoWanted = value;
                }
            }

            [XmlIgnore]
            public bool N_A_GotoMessageWanted
            {
                get
                {
                    return (_InstrumentManager.SelectedInstrumentModule as I.PolarModule).N_A_GotoMessageWanted;
                }
                set
                {
                    (_InstrumentManager.SelectedInstrumentModule as I.PolarModule).N_A_GotoMessageWanted = value;
                }
            }


            /// <summary>
            /// to known if the user already choosed if he want 1face or double face meaurement, if not set then we can set something based on the instrument selected, ie. double face if motorized
            /// </summary>

            public bool IsBad
            {
                get
                {
                    return this.stationParameters._State is State.Bad;
                }
            }
            public bool IsClosed
            {
                get
                {
                    return this.stationParameters._State is State.Closed;
                }
            }

            public bool IsClosable
            {
                get
                {
                    if (this.stationParameters._State is State.Closed) return true;
                    if (this.stationParameters._State is State.Good) return true;
                    if (this.stationParameters._State is State.Bad) return true;
                    if (this.stationParameters._State is State.Opening) return false;
                    if (this.stationParameters._State is State.Measuring) return IsControlled();
                    return false;
                }
            }

            public Measure MeasureToCome
            {
                get
                {
                    return this.StationTheodolite.MeasuresToDoCome;
                }

            }

            [XmlIgnore]
            public Reflector BestHypothesisForNextReflector
            {
                get
                {

                    if (MeasuresPlanned)
                    {
                        if (MeasureToCome.Distance.Reflector != null)
                            if (MeasureToCome.Distance.Reflector._Name != R.String_Unknown)
                                return MeasureToCome.Distance.Reflector;
                    }
                    if (this.StationTheodolite.MeasuresTaken.Count > 0)
                    {
                        Measure last = this.StationTheodolite.MeasuresTaken[this.StationTheodolite.MeasuresTaken.Count - 1] as Measure;
                        if (last.Distance.Reflector != null)
                            if (last.Distance.Reflector._Name != R.String_Unknown)
                                return last.Distance.Reflector;
                    }

                    Reflector defaultR = this.StationTheodolite.Parameters2.DefaultMeasure.Distance.Reflector;
                    if (defaultR != null)
                        if (defaultR._Name != R.String_Unknown)
                            return defaultR;

                    return null;
                }
            }

            [XmlIgnore]
            public bool MeasuresPlanned
            {
                get
                {
                    return MeasureToCome != null;
                }
            }

            [XmlIgnore]
            public CS.Common _SetupStrategy
            {
                get
                {
                    CS.Common s = this.stationParameters.Setups.InitialValues.CompensationStrategy;

                    // pathetic way to have the strategy nwoning the module for message and questions
                    if (s == null)
                        return null;
                    s._Module = this;

                    return s;
                }
            }

            private bool ClosureWasTriggeredAndForcedByCompute = false;

            public override string ToString()
            {
                return $"STM dealing with '{this._Station.ToString()}'";
            }


            [XmlIgnore]
            internal I.Manager.Module reflectorManager;

            internal void CreatePointFromMeasure(Measure m)
            {
                Measure clone = m.Clone() as Measure;
                DetermineFullPointLancé(StationTheodolite, this, clone);
                clone._Point.State = Element.States.FromStation;
                SavePoint(clone);
            }

            private Measure previousMeasure;

            public bool gotoMustBeRecompute;
            #endregion



            #region Constructor
            public Module() : base()
            {
            }

            public Module(Common.FinalModule module) : base(module, string.Format("{0};{1}", R.T_STATION_MODULE_THEODOLITE, R.T_MODULE_MANAGING_A_THEODOLITE_STATION))
            {

            }

            public override void Initialize()// This method is called by the base class "Module" when this object is created
            {
                // Instanciation
                this.StationTheodolite = new Station(this);
                this.ChangeSetupStrategy(CS.List.NoComputation);


                base.Initialize(); // this create the view and more !

                LoadInstrumentTypes();

                // SetStartingPreferences
                SetStartingPreferences();
                DsaFlag.GetByNameOrAdd(this.DsaFlags, "ShowQuestionableResults").State = DsaOptions.Always; //this.flags.ShowQuestionableResults = true; // not in 'setstartingpref, because this run by guided module avfeter deserailasation and this show not be override  to false for guided modules
                
                this._MeasureManager.MeasureStateChange += On_MeasureStateChangeInMan;

            }

            /// <summary>
            /// If the setup is not final, wechack if we should cancel the temporary setup when a measure is set to bad
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void On_MeasureStateChangeInMan(object sender, M.MeasurementEventArgs e)
            {
                var setups = this.stationParameters.Setups;

                // do nothing if setup is validated
                if (setups.FinalValues != null) return;

                // do nothing if no setup validated
                if (setups.BestValues == null) return;

                // do nothing if temp setup is good
                if (setups.TemporaryValues.vZero.Value < 3) return;


                var measure = e.Measure as Measure;

                // do nothing if modify measure is not from the current station
                var measureTaken = this.StationTheodolite.MeasuresTaken;
                if (!measureTaken.Contains(measure)) return;

                // forget setup and try to recompute
                this.Goto_ResetTemporarySetup();
            }

            private void Goto_ResetTemporarySetup()
            {
                var setups = this.stationParameters.Setups;
                setups.TemporaryValues = null;
                foreach (Polar.Measure item in StationTheodolite.MeasuresToDo)
                {
                    item.Clean(removeAnglesForGoto: true);
                }
                this.TryToImproveStationCompensation();
            }

            internal override void CoupleToInstrument(I.Module im)
            {
                base.CoupleToInstrument(im);
                { // 20200929, to avoid set reflector failed when renaming a wrong AT40x at connection
                    if (!im.ConnectionInProgress)
                    {
                        SendNextMeasureToInstrument();
                    }
                }
            }

            internal enum HavingTypes
            {
                MeasurOf, GoodMeasureOf, LastGoodMeasureOf
            }

            internal bool Has(HavingTypes type, string name, out Measure measureByRef)
            {
                M.Measure m;
                measureByRef = null;
                switch (type)
                {
                    case HavingTypes.GoodMeasureOf:
                        m = this.StationTheodolite.MeasuresTaken.LastOrDefault(x => (x._Point._Name == name) && (x._Status is M.States.Good));
                        measureByRef = (m != null) ? m as Measure : null;
                        return (measureByRef != null);
                    case HavingTypes.LastGoodMeasureOf:
                        m = this.StationTheodolite.MeasuresTaken.LastOrDefault(x => (x._Point._Name == name));
                        if (m != null)
                            if (m is Measure mt)
                                if (mt._Status is M.States.Good)
                                    measureByRef = mt;
                        return (measureByRef != null);
                    default:
                        m = this.StationTheodolite.MeasuresTaken.LastOrDefault(x => (x._Point._Name == name));
                        measureByRef = (m != null) ? m as Measure : null;
                        return (measureByRef != null);
                }
            }


            internal override void OnMeasureReceived(object sender, M.MeasurementEventArgs e)
            {
                try
                {
                    // var a = Tsunami2.Properties.Points;

                    if (!CheckStationState(this.stationParameters, IsClosed, this.View)) return;
                    wasTheLastOfASequence = Sequence_InProgressOfLastMeasure();
                    Measure m = e.JustMeasured as Measure;
                    m.Guid = Guid.NewGuid();
                    this.LastMeasureReceived = m;

                    OnMeasureReceived(m); // all module that subscribed to event : MeasureReceived will be notified
                    m.DirectMeasurementWanted = false;
                    this.TreatObservations(m);

                    if (IsMeasureCanceled(m)) { this.SendNextMeasureToInstrument(); return; }

                    if (Sequence_InProgress() && stationParameters._IsSetup)
                        this.TreatementOfASequenceAcquisition(m);
                    else
                        this.NormalTreatement(m);

                    DetermineIfShouldShowNextMeasurebutton();

                    this.RecordOrNot(m);

                    if (m._Status.Type == M.States.Types.Good)
                    {
                        this.TryToImproveStationCompensation();
                    }

                    this.CloseStationOrNot(m);

                    //pour mettre à jour le datagrid alignment result
                    if (this.FinalModule.ParentModule is Common.Guided.Group.Module)
                    {
                        Common.Guided.Group.Module GrGm = this.FinalModule.ParentModule as Common.Guided.Group.Module;
                        //pour remettre l'instrument dans le module actif après la mesure
                        if (GrGm.activeSubGuidedModuleIndex == 0)
                        {
                            GrGm.ActiveSubGuidedModule.SetInstrumentInActiveStationModule();
                        }
                    }

                    OnAcquisitionTreated(m);
                }
                catch (Exception ex)
                {
                    string titleAndMessage = CreationHelper.GetNiceMessageFromInnerExceptions(R.T_STATION_MODULE_COULD_NOT_THREAT_THE_MEASUREMENT, ex);
                    new MessageInput(MessageType.Critical, titleAndMessage).Show();
                    //this.View.ShowMessageOfCritical(string.Format("{0};{1}", R.T_STATION_MODULE_COULD_NOT_THREAT_THE_MEASUREMENT, ex));
                }
            }

            private void DetermineIfShouldShowNextMeasurebutton()
            {
                showButtonToMeasureNextPoint = false;
                {
                    if (this.ParentModule is Polar.Module)
                        if (this.stationParameters.Setups.BestValues != null)
                            if (this.stationParameters.Setups.BestValues.Ready || this.stationParameters.Setups.BestValues.Verified)
                                if (this.StationTheodolite.MeasuresToDo.Count >= 2)
                                    if (this.StationTheodolite.MeasuresToDo[1] is Measure m)
                                        if (m._Status is M.States.Unknown)
                                            showButtonToMeasureNextPoint = true;
                }
            }

            private void TryToImproveStationCompensation()
            {
                try
                {
                    Station station = this.StationTheodolite;
                    Parameters parameters = station.Parameters2;
                    Parameters.Setup setups = parameters.Setups;

                    if (setups.IsSetForGood) return;
                    // if (setups.TemporaryValues == null) return false; // this is not OK or a station will never do a background compute to be able to do 'goto/move to point'

                    CS.List compensationStrategyType;

                    // Check if the computation may be possible and choose the strategy
                    if (setups.InitialValues.VerticalisationState == Parameters.Setup.Values.InstrumentVerticalisation.NotVerticalised)
                    {
                        if (station.GoodMeasuresTaken.Count < 3)
                            return;
                        compensationStrategyType = CS.List.NonVerticalizedSetup;
                    }
                    else if (station.IsANewFrame)
                    {
                        return;
                    }
                    else if (setups.IsAFreeStation)
                    {
                        if (station.GoodMeasuresTaken.Count < 2)
                            return;
                        compensationStrategyType = CS.List.FreeStation_LGC2;
                    }
                    else
                    {
                        if (station.GoodMeasuresTaken.Count < 1)
                            return;
                        compensationStrategyType = CS.List.OrientatedOnly_LGC2;
                    }
                    Parameters.Setup.Values trialSetup = setups.InitialValues.Clone() as Parameters.Setup.Values;
                    trialSetup.CompensationStrategyType = compensationStrategyType;

                    trialSetup.TemporaryCompute = true;
                    trialSetup = CS.Common.Run(trialSetup);


                    if (setups.TemporaryValues == null || trialSetup.SigmaZero <= setups.TemporaryValues.SigmaZero)
                    {
                        setups.TemporaryValues = trialSetup;
                        stationParameters.Setups.TemporaryValues.Ready = true;
                    }

                    //TSU.Debug.ShowPointsInConsole();
                }
                catch (Exception ex)
                {
                    new MessageInput(MessageType.Critical, R.T_THE_GOTO_CALCULATION_FAILED_AND_NEXT_TRIALS_COULD_ALSO_FAILED_DO_YOU_TO_CONTINUE_TO_SHOW_WARNINGS).Show(); ;
                }
            }

            internal override void On_MeasurementFailed(object sender, M.MeasurementEventArgs e)
            {
                // a measurement failed or was cancelled
                // If we are in a sequence we want the sequence to change to 'start with manual start' for the next point of the list
                var measures = this.StationTheodolite.MeasuresToDo;
                if (measures != null && measures.Count>0 )
                {
                    var polarMeasure = measures[0] as Measure;
                    if (polarMeasure.DirectMeasurementWanted)
                    {
                        polarMeasure.DirectMeasurementWanted = false;
                    }
                    if (polarMeasure._Status.Type == M.States.Types.Good)
                    {
                        polarMeasure._Status = new M.States.Unknown();
                    }
                }
                this.View.UpdateViewOfNextPoint();
            }


            internal void RecomputeCoordinates(Measure m)
            {
                try
                {
                    //this.TreatObservations(m);
                    this.TreatPoint(m);
                    Element p = m._Point.Clone() as Point;
                    (p as Point).FillPropertiesFrom(p._Name);
                    this.ElementManager.AddElement("Compute_Manually_triggered", p, false, false, true);
                }
                catch (Exception ex)
                {
                    string titleAndMessage = string.Format("{0};{1}", R.T_STATION_MODULE_COULD_NOT_THREAT_THE_MEASUREMENT, ex.Message);
                    new MessageInput(MessageType.Critical, titleAndMessage).Show();
                }
            }

            private void SetStartingPreferences()
            {
                if (this.DsaFlags != null)
                {
                    //this.DsaFlags.stm = this;
                    DsaFlag.GetByNameOrAdd(this.DsaFlags, "WantToIterateTheNewPointName").State = DsaOptions.Always; //.WantToIterateTheNewPointName = true;
                }
            }

            public static I.Reflector.Manager.Module CreateReflectorManager(Module stm)
            {
                List<I.InstrumentClasses> classesWanted;
                List<I.InstrumentTypes> typesWanted = null;

                classesWanted = new List<I.InstrumentClasses>{
                    I.InstrumentClasses.PRISME};

                I.Reflector.Manager.Module rm;
                rm = new I.Reflector.Manager.Module(stm);
                rm.AllElements = rm.GetByClassesAndTypes(classesWanted, typesWanted);

                rm.View.ShowAsTreeView();

                return rm;
            }
            #endregion

            #region Action as observer

            [field: XmlIgnore]
            public event M.MeasurementEventHandler ResultsAvailable;
            [field: XmlIgnore]
            public event M.MeasurementEventHandler AcquisitionTreated;

            public event StationEventHandler ControlDuringSetupHappened;



            public void OnResultsAvailable(Measure m)
            {
                if (this.ResultsAvailable != null) ResultsAvailable(this, new M.MeasurementEventArgs(m));
            }

            public void OnAcquisitionTreated(Measure m)
            {
                if (this.AcquisitionTreated != null) AcquisitionTreated(this, new M.MeasurementEventArgs(m));
            }

            public override void OnNext(TsuObject tsuObject)
            {
                dynamic dynamic = tsuObject;
                OnNextBasedOn(dynamic);
            }

            public void NormalTreatement(Measure m)
            {
                try
                {
                    // show measure differences
                    if (!(m._Status is M.States.Questionnable))
                        ShowMeasureDifferencesIfPointAlreadyMeasured(m);


                    if (m._Status is M.States.Cancel)
                    {
                        SendNextMeasureToInstrument(); // was added for t2 to have something to measure  when rejecteing control measuement
                        return;
                    }

                    TreatPoint(m);

                }
                catch (Exception ex)
                {
                    throw new Exception($"Couldn't do the normal treatment of {m._PointName} measure", ex);
                }
            }

            private void TreatPoint(Measure m)
            {
                try
                {
                    m._Point._Coordinates = null;
                    if (this.stationParameters.Setups.BestValues != null)
                    {
                        if (!this.stationParameters._IsSetup && ValidateComputeAsFastAsPossible)
                        {
                            this.stationParameters.Setups.FinalValues = this.stationParameters.Setups.TemporaryValues;
                            this.stationParameters._IsSetup = true;
                            this.stationParameters._State = new State.Measuring();
                            this.ElementManager.AddElement(this.stationParameters.Setups.FinalValues.StationPoint);
                            this.stationParameters.Setups.FinalValues.StationPoint.State = Element.States.Approximate;
                        }
                        DetermineFullPointLancé(this.StationTheodolite, this, m);
                        if (!this.stationParameters._IsSetup)
                            m._Point.State = Element.States.Approximate;
                        else
                            m._Point.State = Element.States.Good;
                    }
                    else
                    {
                        NeutralizeMeasuresBecauseItIsAUnknownPoints(m);
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception($"Couldn't threat the point from {m}", ex);
                }
            }

            List<Point> measuredPointsComingFromAnSequenceMeasurement = new List<Point>();
            List<Measure> measureComingFromAnSequenceMeasurement = new List<Measure>();

            public void TreatementOfASequenceAcquisition(Measure m)
            {
                m._Status = new M.States.Questionnable();

                // treat point
                if (this.stationParameters._IsSetup)
                {
                    DetermineFullPointLancé(this.StationTheodolite, this, m);
                    m.Offsets = ComputeDifferences(m);
                }
                else // check if the point is knwon or not to propose to not to use unknown point for compensation
                    NeutralizeMeasuresBecauseItIsAUnknownPoints(m);

                // add point to the list to show at the end of the sequence
                measuredPointsComingFromAnSequenceMeasurement.Add(m._Point);
                measureComingFromAnSequenceMeasurement.Add(m);
            }

            private void CloseStationOrNot(Measure m)
            {
                if (m._Status.Type == M.States.Types.Control
                    &&
                    DsaFlag.IsSetToAlways(this.DsaFlags, "ClosureTriggeredByEndStation"))
                {
                    this.CloseStation(out bool dummy);
                }
            }

            private bool IsMeasureCanceled(Measure m)
            {
                switch (m._Status.Type)
                {
                    case M.States.Types.Good:
                    case M.States.Types.Unknown:
                    case M.States.Types.Questionnable:
                    case M.States.Types.Control:
                    case M.States.Types.Temporary:
                    case M.States.Types.Bad:
                        return false;
                    case M.States.Types.Failed:
                    case M.States.Types.Replace:
                    case M.States.Types.Cancel:
                    default:
                        return true;
                }
            }


            internal void ChangeSetupStrategy(CS.List strategy)
            {
                switch (strategy)
                {
                    case CS.List.TourDHorizon:
                        break;
                    //case CompensationStrategy.Strategies.FreeStation:
                    //    _SetupStrategy = new CompensationStrategy.FreeStationStrategy(this);
                    //    break;
                    //case CompensationStrategy.Strategies.OrientatedOnly:
                    //    _SetupStrategy = new CompensationStrategy.OrientationStrategy(this);
                    //    break;
                    //case CompensationStrategy.Strategies.NoComputation:
                    //    _SetupStrategy = new CompensationStrategy.MeasureWithoutComputationStrategy(this);
                    //    break;
                    case CS.List.Local:
                        stationParameters.Setups.InitialValues.CompensationStrategyType = CS.List.Local_LGC2;
                        break;
                    case CS.List.Tour_DHorizon_LGC2:
                        break;
                    case CS.List.FreeStation_LGC2:
                        stationParameters.Setups.InitialValues.CompensationStrategyType = CS.List.FreeStation_LGC2;
                        break;
                    case CS.List.OrientatedOnly_LGC2:
                        stationParameters.Setups.InitialValues.CompensationStrategyType = CS.List.OrientatedOnly_LGC2;
                        break;
                    case CS.List.Local_LGC2:
                        stationParameters.Setups.InitialValues.CompensationStrategyType = CS.List.Local_LGC2;
                        break;

                    case CS.List.AllCala:
                        stationParameters.Setups.InitialValues.CompensationStrategyType = CS.List.AllCala;
                        break;
                    case CS.List.NonVerticalizedSetup:
                        stationParameters.Setups.InitialValues.CompensationStrategyType = CS.List.NonVerticalizedSetup;
                        break;
                    default:
                        break;
                }
            }

            public void TriggerControlDuringSetupHappened()
            {
                this.ControlDuringSetupHappened?.Invoke(this, new StationEventArgs(this));
            }



            #region Sequence

            bool Sequence_InProgress()
            {
                switch (this.StationTheodolite.MeasuresToDo.Count)
                {
                    case 0: return false;
                    case 1: return (this.StationTheodolite.MeasuresToDo[0] as Measure).DirectMeasurementWanted && this.measureComingFromAnSequenceMeasurement.Count != 0;
                    default: return (this.StationTheodolite.MeasuresToDo[1] as Measure).DirectMeasurementWanted; // because the first one can be in unknown to wait for the launch
                }
            }

            bool Sequence_InProgressOfLastMeasure()
            {
                switch (this.StationTheodolite.MeasuresToDo.Count)
                {
                    case 0: return false;
                    case 1: return (this.StationTheodolite.MeasuresToDo[0] as Measure).DirectMeasurementWanted; // green
                    default:
                        var m1 = this.StationTheodolite.MeasuresToDo[0] as Measure;
                        var m2 = this.StationTheodolite.MeasuresToDo[1] as Measure;
                        return m1.DirectMeasurementWanted && !m2.DirectMeasurementWanted; // green, blue, ...
                }
            }

            bool sequenceBeingPrepared = false;
            /// <summary>
            /// if list contain more tahn 1 points is create a sequence, il not it ask to choose point from the elementmanger
            /// a sequence always start with the station point 
            /// </summary>
            internal void Sequence_CreateOneFromNextPointsList()
            {
                if (!stationParameters._IsSetup) Tsunami2.Properties.Save("", "sequence for station setup"); // to avoid multiples 'save as' messages during the sequence

                bool nextPointsListWasEmpty = false;
                if (this.StationTheodolite.MeasuresToDo.Count == 0)
                {
                    nextPointsListWasEmpty = true;
                    EC.CompositeElement e = SelectPoints();
                    this.sequenceBeingPrepared = true;
                    PrepareNextPoints(e, new M.States.Unknown());
                    this.sequenceBeingPrepared = false;
                }

                if (this.StationTheodolite.MeasuresToDo.Count < 2)
                {
                    string titleAndMessage = $"{R.T_YOU_NEED_NEED_AT_LEAST_2_KNOWN_POINTS_IN_THE_NEXT_POINTS_LIST}.";
                    new MessageInput(MessageType.Warning, titleAndMessage).Show();
                    return;
                }

                EC.SequenceTH seq = new EC.SequenceTH();
                seq._Name = MeasureToCome._OriginalPoint._Name + "..." + this.StationTheodolite.MeasuresToDo[this.StationTheodolite.MeasuresToDo.Count - 1]._OriginalPoint._Name;
                seq._Origin = this.StationTheodolite._Name;
                seq._OriginType = ElementType.SequenceTH;
                if (this.StationTheodolite.Parameters2._StationPoint == null)
                    seq.Add(new Point());
                else
                    seq.Add(this.StationTheodolite.Parameters2._StationPoint);
                foreach (Measure item in this.StationTheodolite.MeasuresToDo)
                {
                    seq.Add(item._OriginalPoint);
                }
                lastSequence = seq;
                this.ElementManager.AddElement(seq);

                if (nextPointsListWasEmpty) // lets check if poin thave been already measure and if we wwant brakets
                {
                    this.StationTheodolite.ClearMeasuresToDo();
                    Sequence_Load(seq);
                }

                this.View.UpdateViewOfNextPoint();
                SendNextMeasureToInstrument();
            }

            private EC.SequenceTH lastSequence;

            internal void Sequence_ReMeasureExisting()
            {
                EC.SequenceTH seq = this.ElementManager.SelectOneTheodoliteSequence(R.T_CHOOSE_A_SEQUENCE);
                if (seq == null) return;
                lastSequence = seq;

                // check if the sequence have points with coordinates
                Sequence_CheckIfAllPointHaveCoordinates(seq);

                Sequence_Load(seq);
            }


            internal void Sequence_ReMeasureLast()
            {
                if (lastSequence != null)
                    Sequence_Load(lastSequence);
                else
                    Sequence_ReMeasureExisting();
            }

            private void Sequence_CheckIfAllPointHaveCoordinates(EC.SequenceTH seq)
            {
                try
                {
                    foreach (var item in seq.Elements)
                    {
                        if (item is Point)
                        {
                            Point p1 = item as Point;
                            // if point has no coord let's look if a version of the point exist in the element manager
                            bool found = false;
                            if (!p1._Coordinates.HasAny)
                            {
                                foreach (Point p2 in this.ElementManager.GetPointsInAllElements())
                                {
                                    if (p2._Name == p1._Name)
                                    {
                                        if (p2._Coordinates.HasAny)
                                        {
                                            p1._Coordinates = p2._Coordinates.Clone() as CoordinatesInAllSystems;
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (!found)
                                Logs.Log.AddEntryAsPayAttentionOf(this, $"{p1._Name} do not have coordinates, goto will be impossible");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.Log.AddEntryAsPayAttentionOf(this, $"Problem when cheking that all points of the sequence have coordinates, '{ex.Message}'");
                }
            }

            /// <summary>
            /// should be in the order of the measurement wanted
            /// </summary>
            /// <param name="seq"></param>
            /// <param name="state"></param>
            private void Sequence_Load(EC.SequenceTH seq, M.State state = null)
            {
                if (seq.Elements == null) return;
                if (seq.Elements.Count < 2) return; // we need minimum a station and a point

                if (state == null)
                {
                    state = new M.States.Unknown();
                }

                EC.SequenceTH copy = new EC.SequenceTH();
                foreach (var item in seq.Elements)
                {
                    copy.Add(item.Clone() as Element);
                }

                SearchAndAddForPointsMissingFromSequences(seq);


                { //  choose what to do with the first point
                    bool useFirstPointAsStation;
                    if (this.stationParameters._IsSetup)
                    {
                        // then no brainer, we dont change the station position, we just remove the first point
                        useFirstPointAsStation = false;
                    }
                    else
                    {
                        if (this.stationParameters.Setups.InitialValues.IsPositionKnown)
                        {
                            string titleAndMessage = $"{R.T_SEQ_FIRST_POINT_CHOICE};{R.T_SEQ_FIRST_POINT_CHOICE_DETAILS}\r\n\r\n({R.T_CLEAR_PREVIOUS_SEQUENCE_DETAILS_DEFAULT})";
                            MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                            {
                                ButtonTexts = new List<string> { R.T_REPLACE, R.T_DONT },
                                Sender = this._Name
                            };
                            string r = mi.Show().TextOfButtonClicked;
                            if (r == R.T_REPLACE)
                                useFirstPointAsStation = true;
                            else
                                useFirstPointAsStation = false;
                        }
                        else
                        {
                            // no brainer, we use the first point for the station position.
                            useFirstPointAsStation = true;
                        }
                    }

                    if (useFirstPointAsStation)
                    {
                        var stationPoint = copy.Elements[0] as Point;


                        bool isFreeStation = stationPoint._Name.ToUpper().Contains("STL");

                        // rename the freestation point with a name of today
                        if (isFreeStation)
                        {
                            string proposedName = this.GetAvailableStationName();
                            string message = "Station point name?";
                            if (!this.ElementManager.AskName(proposedName, out string newName, message))
                                return;
                            stationPoint = new Point() { _Name = newName };
                            copy.Elements[0] = stationPoint;
                        }

                        this.stationParameters.Setups.InitialValues.StationPoint = stationPoint;

                        // Set height based on Point SocketCode
                        double ih = 0;
                        Parameters.Setup.Values.InstrumentTypeHeight ith;
                        if (stationPoint.SocketCode != null)
                        {
                            ith = Parameters.Setup.Values.InstrumentTypeHeight.Known;
                            ih = stationPoint.SocketCode.InstrumentHeightForPolarMeasurement;
                        }
                        else if (isFreeStation) // station libre
                        {
                            ith = Parameters.Setup.Values.InstrumentTypeHeight.Known;
                            ih = 0;
                            stationPoint.SocketCode = SocketCode.GetCodeById("10");
                        }
                        else
                        {
                            ih = na;
                            ith = Parameters.Setup.Values.InstrumentTypeHeight.NotDefinedYet;
                        }

                        this.stationParameters.Setups.InitialValues.IsInstrumentHeightKnown = ith;
                        this.stationParameters.Setups.InitialValues.InstrumentHeight = new DoubleValue() { Value = ih };
                        this.stationParameters.Setups.InitialValues.IsPositionKnown = true;
                        this.View.UpdateViewOfStationParameters();
                    }

                    copy.Elements.RemoveAt(0);

                } //  choose what to do with the first point

                this.Sequence_CheckIfPointAlreadyMeasured(ref copy);


                var measures = this.PrepareMeasuresFromElements(copy, state);
                this.StationTheodolite.MeasuresToDo.AddRange(measures);
                this.Sequence_SetAutomatiqueMode(automaticMode);
                SendNextMeasureToInstrument();
                this.View.UpdateViewOfNextPoint();
            }

            private void SearchAndAddForPointsMissingFromSequences(EC.CompositeElement e)
            {
                foreach (Element item in e)
                {
                    // Name the station if it is a sequence file
                    if (item.fileElementType == ElementType.SequenceTH)
                    {
                        if (View.WantToUseSequenceForStationName(item._Name))
                        {
                            Point p = (this.ElementManager.GetElementByName(item._Name) as Point).Clone() as Point;
                            this.stationParameters._StationPoint = p;
                            if (this.stationParameters._StationPoint == null)
                                this.stationParameters._StationPoint = new Point(item._Name);
                        }
                    }
                }
            }

            private void Sequence_CheckIfPointAlreadyMeasured(ref EC.SequenceTH copy)
            {
                List<Measure> mths = T.Research.FindGoodMeasuresByPointNames(this.StationTheodolite.MeasuresTaken, copy.Elements, M.States.Types.Good);
                if (mths.Count > 0)
                {
                    string foundPointName = "";
                    mths.ForEach(x => foundPointName += x._Point._Name + ":" + x._Date + "\r\n");

                    EC.SequenceTH temp = copy;
                    Action actionDo = () =>
                    {
                        EC.SequenceTH newE = new EC.SequenceTH();
                        foreach (Point item in temp.Elements)
                        {
                            Point nextP = item.Clone() as Point;
                            nextP._Name += "_[###]";
                            newE.Add(nextP);
                        }
                        temp = newE;
                    };

                    string message = $"{R.T_SOME_POINTS_HAS_BEEN_MEASURED_ALREADY};" +
                        $"{foundPointName}" + $"{R.T_DO_YOU_WANT_TO_RENAME_THE_POINTS_FROM}";

                    MessageInput mi = new MessageInput(MessageType.Choice, message)
                    {
                        ButtonTexts = CreationHelper.GetYesNoButtons(),
                        DontShowAgain = DsaFlag.GetByNameOrAdd(this.DsaFlags, "IterateSequenceWithBracket")
                    };
                    var r = mi.Show();

                    if (r.TextOfButtonClicked == R.T_YES)
                        actionDo();

                    //flags.IterateSequenceWithBracket = flags.React(flags.IterateSequenceWithBracket,
                    //    message, R.T_YES, actionDo, R.T_NO, null);

                    copy = temp; //not even needed because it is the same ref but help to understand.
                }
            }

            bool wasTheLastOfASequence;
            bool wasARealSequenceOfMoreThanOnePoint = true;


            private void Sequence_Reject()
            {
                foreach (var item in measureComingFromAnSequenceMeasurement)
                {
                    item._Status = new M.States.Bad();
                }
                measuredPointsComingFromAnSequenceMeasurement.Clear();
                measureComingFromAnSequenceMeasurement.Clear();
                this.Sequence_Load(lastSequence);
                this.View.UpdateViewOfPreviousPoint();
            }

            private void Sequence_Keep()
            {
                foreach (var item in measuredPointsComingFromAnSequenceMeasurement)
                {
                    this.SavePoint(item, E.Manager.Module.AddingStrategy.ReplaceIfSameGroup);
                }

                foreach (var item in measureComingFromAnSequenceMeasurement)
                {
                    if (item._Status.Type != Types.Bad)
                        item._Status = new M.States.Good();
                }

                measuredPointsComingFromAnSequenceMeasurement.Clear();
                measureComingFromAnSequenceMeasurement.Clear();

                this.View.UpdateViewOfPreviousPoint();
                Tsunami2.Properties.Save(path: "", triggerMessage: "sequence kept");
            }

            internal void Sequence_ReproduceStation()
            {
                var manager = this.PolarModule.CreateAStationManager();
                var view = manager.View;

                var r = view.ShowInMessageTsu(
                    "Select one station",
                    R.T_SELECT, view.ValidateSelection, R.T_CANCEL);

                if (r == R.T_CANCEL)
                    return;

                manager.View.ValidateSelection();
                if (manager._SelectedObjects.Count != 1)
                    return;

                var stationModel = (manager._SelectedObjects[0] as Station);
                if (stationModel == null)
                    return;

                string titleAndMessage = $"{R.T_KEEP_ORIENTATION}?;{R.T_KEEP_ORIENTATION_DETAILS}?";
                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_Keep, R.T_Reject }
                };
                bool keepGoto = (R.T_Reject != mi.Show().TextOfButtonClicked);

                Sequence_ClonePointsToMeasure(this, stationModel, keepGoto);
                Sequence_CloneStationPoint(this, stationModel);
                if (keepGoto) Sequence_CloneSetupToKeepGoto(this, stationModel);

                this.SendNextMeasureToInstrument();
            }

            /// <summary>
            /// Clone the best setup of the model as the temporart setup for the new station
            /// </summary>
            /// <param name="newStationModule"></param>
            /// <param name="model"></param>
            internal static void Sequence_CloneSetupToKeepGoto(Module newStationModule, Station model)
            {
                newStationModule.StationTheodolite.Parameters2.Setups.TemporaryValues = model.Parameters2?.Setups?.BestValues.Clone() as Values;
                Point newStationPoint = newStationModule.StationTheodolite.Parameters2.Setups.InitialValues?.StationPoint;
                Point oldStationPoint = newStationModule.StationTheodolite.Parameters2.Setups.TemporaryValues.StationPoint;
                newStationPoint._Coordinates = oldStationPoint._Coordinates;
                newStationModule.StationTheodolite.Parameters2.Setups.TemporaryValues.StationPoint = newStationPoint;
            }

            /// <summary>
            /// If not station point is already set, we use the same station point if is a known point, create a new point if it was a free station
            /// </summary>
            /// <param name="newStationModule"></param>
            /// <param name="model"></param>
            /// <param name="keepGoto"></param>
            internal static void Sequence_CloneStationPoint(Module newStationModule, Station model)
            {
                var newStationParameters = newStationModule?.StationTheodolite?.Parameters2;
                bool newStationPointIsSet = newStationParameters._StationPoint != null;

                if (!newStationPointIsSet)
                {
                    var modelStationParameters = model?.Parameters2;
                    var modelStationPoint = modelStationParameters?._StationPoint;
                    var modelInitialSetups = model.Parameters2?.Setups?.InitialValues;
                    bool modelStationPointIsKnown = modelStationPoint != null && modelInitialSetups != null && modelInitialSetups.IsPositionKnown;

                    if (modelStationPointIsKnown)
                    {
                        newStationParameters._StationPoint = modelStationPoint;
                        newStationParameters.Setups.InitialValues = modelStationParameters.Setups.InitialValues.Clone() as Values;
                    }
                    else
                        newStationModule.SetNewStationPoint();
                }

                newStationModule.View.UpdateViewOfStationParameters();
            }

            internal static void Sequence_ClonePointsToMeasure(Module stm, Station model, bool OnlyRefs = false)
            {
                foreach (var measure in model.MeasuresTaken)
                {
                    if (OnlyRefs && !(measure.GeodeRole == Geode.Roles.Cala || measure.GeodeRole == Geode.Roles.Radi || measure.GeodeRole == Geode.Roles.Pdor))
                        continue;

                    Measure clonedMeasure = measure.Clone() as Measure;
                    clonedMeasure.Guid = Guid.NewGuid();
                    clonedMeasure._Point = measure._Point.Clone() as Point;
                    clonedMeasure.Clean(true);

                    if (measure._Status is M.States.Control)
                        clonedMeasure._Status = new M.States.Control();
                    else
                        clonedMeasure._Status = new M.States.Unknown();

                    if (Tsunami2.Preferences.Values.GuiPrefs.AddPointsToTheEndOfTheNextPointsList.IsTrue)
                        stm.StationTheodolite.MeasuresToDo.Add(clonedMeasure);
                    else
                        stm.StationTheodolite.MeasuresToDo.Insert(0,clonedMeasure);

                    // if newPoint give it original coordinates
                    if (!clonedMeasure._OriginalPoint._Coordinates.HasAny && clonedMeasure._Point._Coordinates.HasAny)
                    {
                        clonedMeasure._OriginalPoint._Coordinates = clonedMeasure._Point._Coordinates.Clone() as CoordinatesInAllSystems;
                    }
                }
                stm.View.UpdateViewOfNextPoint();
            }

            internal void EditNextPointsListInTextEditor()
            {
                List<string> OriginalLines = new List<string>();
                OriginalLines.Add(
                    $"{"% Name",-30} " +
                    $";{"Reflector",-10} " +
                    $";{R.T_EXTENSION,-10} " +
                    $";{"Code",-5} " +
                    $";{"dH4S",-10} " +
                    $";{"Face",-10} " +
                    $";{"Comment",-30} " +
                    $";{"Status",-15} " +
                    $";{"meas#",-5}");
                foreach (var item in this.StationTheodolite.MeasuresToDo)
                {
                    OriginalLines.Add(
                        $"{item._Point._Name,-30} " +
                        $";{(item as Measure).Distance.Reflector,-10} " +
                        $";{item.Extension,-10} " +
                        $";{item._Point.SocketCode.Id,-5} " +
                        $";{(item as Measure).TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates,-10} " +
                        $";{(item as Measure).Face,-10} " +
                        $";{item.CommentFromUser,-30} " +
                        $";{item._Status._Name,-15} " +
                        $";{(item as Measure).NumberOfMeasureToAverage,-5}");
                }
                OriginalLines.Add("% Add new points only after this line...");

                // write file
                string path = Tsunami2.Preferences.Values.Paths.Temporary + "NextPointsList.txt";
                string OriginalContent = "";
                foreach (var item in OriginalLines)
                {
                    OriginalContent += item + "\r\n";
                }
                System.IO.File.WriteAllText(path, OriginalContent);

                //show the file
                Shell.ExecutePathInDialogView(path, "Default Text Editor", "Cancel");

                //read the file
                string newContent = System.IO.File.ReadAllText(path);
                string errorMessage = "";
                int lineCount = -1;
                using (System.IO.StringReader reader = new System.IO.StringReader(newContent))
                {
                    string line;
                    bool isANewMeasureToAdd = false;
                    while ((line = reader.ReadLine()) != null)
                    {
                        lineCount++;
                        Measure pm;
                        if (lineCount >= OriginalLines.Count)
                        {
                            isANewMeasureToAdd = true;
                            pm = new Measure();
                        }
                        else
                        {
                            isANewMeasureToAdd = false;
                            if (OriginalLines[lineCount] == line) continue;
                            pm = this.StationTheodolite.MeasuresToDo[lineCount - 1] as Measure;
                        }

                        line = System.Text.RegularExpressions.Regex.Replace(line.Trim(), @"\s+", " ");

                        string[] tokens = line.Split(';');
                        if (tokens.Length != 9)
                        {
                            errorMessage += $"Error: wrond field count in line {lineCount + 1}\r\n";
                        }
                        else
                        {
                            try
                            {
                                pm._Point._Name = tokens[0];
                                pm._OriginalPoint = pm._Point;
                                pm.Distance.Reflector = I.Reflector.Reflector.GetbyName(tokens[1]);
                                pm.Extension = new DoubleValue(double.Parse(tokens[2]), na);
                                pm._Point.SocketCode = SocketCode.GetById(tokens[3]);
                                pm.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates = new DoubleValue(double.Parse(tokens[4]), na);
                                pm.Face = (I.FaceType)Enum.Parse(typeof(I.FaceType), tokens[5]);
                                pm._Point.CommentFromUser = tokens[6];
                                pm._Status = M.State.GetStateByName(tokens[7]);
                                pm.NumberOfMeasureToAverage = int.Parse(tokens[8]);
                                if (isANewMeasureToAdd)
                                {
                                    if (Tsunami2.Preferences.Values.GuiPrefs.AddPointsToTheEndOfTheNextPointsList.IsTrue)
                                        this.StationTheodolite.MeasuresToDo.Add(pm);
                                    else
                                        this.StationTheodolite.MeasuresToDo.Insert(0, pm);
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage += $"Error on line {lineCount + 1}: {ex.Message}\r\n";
                            }
                        }
                    }
                }
                View.UpdateViewOfNextPoint();
                if (errorMessage != "")
                {
                    string titleAndMessage = $"{R.T_ERROR}:{errorMessage}";
                    new MessageInput(MessageType.Warning, titleAndMessage).Show();
                }
            }

            private void Sequence_ShowPointsFromMeasurement()
            {
                // possibility to analyse data in excel before chosing to keep or not the point
                if (ExcelLinkedPath != "")
                {
                    foreach (var item in measureComingFromAnSequenceMeasurement)
                    {
                        IO.Excel.WriteInWorkBook(IO.Excel.CreateObjectsFrom(item));
                    }
                }

                if (measuredPointsComingFromAnSequenceMeasurement.Count == 0) return;
                E.Manager.Module em = new E.Manager.Module();
                em.AllElements = new List<TsuObject>();
                em.AllElements.AddRange(measuredPointsComingFromAnSequenceMeasurement);
                em.SelectableObjects = new List<TsuObject>();
                em._SelectedObjects = new List<TsuObject>();
                em.View = new E.Manager.View(em);
                em.MultiSelection = true;
                em.View.CheckBoxesVisible = true;
                em.View.ShowDockedFill();
                em.UpdateView();

                _MeasureManager.SelectableObjects.Clear();
                _MeasureManager.SelectableObjects.AddRange(measureComingFromAnSequenceMeasurement);
                _MeasureManager.MultiSelection = true;
                _MeasureManager.View.CheckBoxesVisible = true;
                _MeasureManager.View.ShowDockedFill();
                _MeasureManager.UpdateView();
                // show both in  one windows
                TsuView v = new TsuView();
                SplitContainer s = new SplitContainer() { Orientation = Orientation.Horizontal, Dock = DockStyle.Fill };
                Tsunami2.Preferences.Theme.ApplyTo(s);
                s.SizeChanged += delegate
                {
                    try
                    {
                        int min = s.Panel1.MinimumSize.Height;
                        int wanted = s.Height / 2;
                        //wanted = Math.Min(min, wanted);
                        s.SplitterDistance = wanted;
                    }
                    catch (Exception ex)
                    {

                    }
                };
                v.Controls.Add(s);

                s.Panel1.Controls.Add(em.View);
                s.Panel2.Controls.Add(_MeasureManager.View);
                v.ShowInMessageTsu(R.T_ELEMENTS_MEASURED_DURING_THE_SEQUENCE_ARE_CHECKED, R.T_Keep, Sequence_Keep, R.T_Reject, Sequence_Reject);
            }

            #endregion Sequence

            public void RecordOrNot(Measure m)
            {
                Timer t = null;
                try
                {
                    this.OnMeasureThreated(this.LastMeasureReceived); // all module taht subscribed to event : MeasureThreated will be notified


                }
                catch (Exception ex)
                {
                    string titleAndMessage = $"{R.T_MEASURE_THREATED_BY_THE_STATION_MODULE_BUT};{ex}";
                    new MessageInput(MessageType.Critical, titleAndMessage).Show();
                }

                //debug
                //var a = this.StationTheodolite.MeasuresTaken;
                //var b = this._MeasureManager.AllElements;


                try
                {
                    if (this.RecordAcquisition(m) || Sequence_InProgress())
                    {

                        // if line do not remove it we propbablu want to  measure differents points on the line

                        bool removeMeasure = this.RemoveMeasureFromNextPointListAfterReception;

                        if (Sequence_InProgress())
                        {
                            removeMeasure = true;
                        }
                        else
                        {
                            if (IsMeasuringAShape())
                            {
                                removeMeasure = false;
                            }
                        }

                        if (removeMeasure)
                            this.RemovePointAndSwitchtoNext(m);
                        else if (this.StationTheodolite.MeasuresToDo.Count > 0
                              && this.StationTheodolite.MeasuresToDo[0] != null
                              && this.StationTheodolite.MeasuresToDo[0]._Status.Type == M.States.Types.Good)
                            this.StationTheodolite.MeasuresToDo[0]._Status = new M.States.Unknown();

                        // timer should be launched by main thread to avoid them to died before execution if the mother thread finishes before it.
                        Tsunami2.Properties.InvokeOnApplicationDispatcher(() =>
                        {
                            t = new Timer() { Enabled = true, Interval = 300 };

                            // let use a timer ...// no! why? if so than in module guide the measure button doent have the right color because he look if the measure is still in the ist to know if is was good
                            t.Tick += delegate
                            {
                                t.Stop();
                                t.Dispose();

                                FinalModule.Save("Acquisition recorded");

                                ComputeOrNot(m);

                                bool showResults = !DsaFlag.IsSetToNever(this.DsaFlags, "ShowResultsAtTheEndOfASequence");
                                if (showResults && this.wasTheLastOfASequence && this.wasARealSequenceOfMoreThanOnePoint)
                                {
                                    this.Sequence_ShowPointsFromMeasurement();
                                }
                            };
                        });

                        Timer t2 = new Timer() { Enabled = true, Interval = 500 };
                        t2.Tick += delegate
                        {
                            t2.Stop();
                            t2.Dispose();
                            SendNextMeasureToInstrument();
                        };

                    }
                }
                catch (Exception)
                {
                    if (t != null)
                    {
                        t.Stop();
                        t.Dispose();
                    }
                    throw;
                }
            }

            private void ComputeOrNot(Measure m)
            {
                if (StationTheodolite.ParametersBasic._IsSetup)
                    return;

                if (m._Status.Type != Types.Control)
                    return;


                if (ClosureWasTriggeredAndForcedByCompute)
                    DetermineHowtoAndThenComputeWithLGC2();
                else
                    this.TriggerControlDuringSetupHappened();
            }

            public bool automaticMode = false;

            internal void Sequence_SetAutomatiqueMode()
            {
                automaticMode = !automaticMode;
                Sequence_SetAutomatiqueMode(automaticMode);
            }

            internal void Sequence_SetAutomatiqueMode(bool automaticMode)
            {
                // get measures as Polar
                var measuresToCome = this.StationTheodolite.MeasuresToDo.OfType<Measure>().ToList();

                // switch to the new state
                for (int i = 1; i < measuresToCome.Count; i++) // skip the [o] because we always want it manual, event o start the automatic sequence
                {
                    measuresToCome[i].DirectMeasurementWanted = automaticMode;
                }

                // StationTheodolite.MeasuresToDo.ForEach(x => x._Status = new M.States.Unknown()); // all way to cancel the automatic mode

                this.View.UpdateViewOfNextPoint();
            }

            internal void InvertNextPointsList()
            {
                if (StationTheodolite.MeasuresToDo.Count < 2) return;
                StationTheodolite.MeasuresToDo.Reverse();

                // in case of sequence inverse the states
                {
                    if (StationTheodolite.MeasuresToDo[0]._Status is M.States.Good)
                    {
                        StationTheodolite.MeasuresToDo[0]._Status = new M.States.Unknown();
                        if (StationTheodolite.MeasuresToDo[StationTheodolite.MeasuresToDo.Count - 1]._Status is M.States.Unknown)
                            StationTheodolite.MeasuresToDo[StationTheodolite.MeasuresToDo.Count - 1]._Status = new M.States.Good();
                    }
                }

                this.View.UpdateViewOfNextPoint();
                SendNextMeasureToInstrument();

            }

            private bool IsMeasuringAShape()
            {
                if (MeasuresPlanned) // should always be
                {
                    if (MeasureToCome._OriginalPoint._OriginType == ElementType.LineAsPoint)
                    {
                        return true;
                    }
                }
                return false;
            }





            internal void ResetFlags()
            {
                this.ElementManager.ResetFlags();

                if (this._InstrumentManager.SelectedInstrumentModule != null)
                    DsaFlag.GetByNameOrAdd(this._InstrumentManager.SelectedInstrumentModule.FinalModule.DsaFlags, "ShowReflectorAndExtensionBeforeMeasurement").State = DsaOptions.Always;
            }

            private bool showButtonToMeasureNextPoint = false;

            // add the measure and the point to the station if wanted
            private bool RecordAcquisition(Measure n)
            {
                if (n == null) return false;

                bool stationIsSetup = this.stationParameters._IsSetup && !ValidateComputeAsFastAsPossible; // we don't want the guided module that compute station just for goto to compute coordinates

                if (this.Sequence_InProgress())
                {
                    this.AddMeasureToStation(n);
                    n._Point.StorageStatus = Common.Tsunami.StorageStatusTypes.Keep; // Tsunami2.Properties.Points.AddIfDoNotExistWithSameGUID(n._Point);
                    return false;
                }

                switch (n._Status.Type)
                {
                    case M.States.Types.Good:
                        {
                            if (stationIsSetup)
                            {
                                n.Offsets = ComputeDifferences(n);
                                SavePoint(n);
                            }
                            else // we still want to serialize the new point from this measurement
                                n._Point.StorageStatus = Common.Tsunami.StorageStatusTypes.Hidden;

                            if (!DsaFlag.IsSetToNever(this.DsaFlags, "ShowMeasureResults"))
                            {
                                string coordinate = stationIsSetup ? n._Point._Coordinates.ToString(CoordinatesInAllSystems.StringFormat.AvailableCoordinatesAsTable) : "";
                                string messageOfSuccess = string.Format("{0};{1}\r\n{2}", R.T_POINT_SUCCESSFULLY_MEASURED, n.ToString("Ls"), coordinate);

                                wasARealSequenceOfMoreThanOnePoint = true; //this.flags.ShowResultsAtTheEndOfASequence = true; // to be set false only if the single next measure it triggered from the result showing green message.

                                DsaFlag dsa = DsaFlag.GetByNameOrAdd(this.DsaFlags, "ShowMeasureResults");
                                if (showButtonToMeasureNextPoint && this.StationTheodolite.MeasuresToDo.Count > 1)
                                {
                                    var nextMeasure = this.StationTheodolite.MeasuresToDo[1] as Polar.Measure;
                                    string mn = "Measure next now";

                                    if (!nextMeasure._OriginalPoint._Coordinates.HasAny)
                                        mn += $" (No Goto)";

                                    MessageInput mi = new MessageInput(MessageType.GoodNews, messageOfSuccess)
                                    {
                                        ButtonTexts = new List<string> { R.T_OK + "!", mn },
                                        DontShowAgain = dsa,
                                        UseMonoSpacedFont = true
                                    };
                                    var r = mi.Show();

                                    if (r.TextOfButtonClicked == mn)
                                    {
                                        wasARealSequenceOfMoreThanOnePoint = false;
                                        if (dsa.State == DsaOptions.Never)
                                        {
                                            // we cancel the never because the 'measure next was used'
                                            dsa.State = DsaOptions.Ask_to;
                                        }
                                        nextMeasure.DirectMeasurementWanted = true;
                                        this.GotoWanted = true;
                                    }
                                }
                                else
                                {
                                    MessageInput mi = new MessageInput(MessageType.GoodNews, messageOfSuccess)
                                    {
                                        ButtonTexts = new List<string> { R.T_OK + "!" },
                                        DontShowAgain = dsa,
                                        UseMonoSpacedFont = true
                                    };
                                    mi.Show();
                                }
                            }
                        }
                        break;
                    case M.States.Types.Questionnable:
                        {
                            if (stationIsSetup)
                            {
                                n.Offsets = ComputeDifferences(n);
                                bool keep = this.ShowResultsAndValideIt(n.Offsets);

                                if (!keep)
                                {
                                    OnResultsAvailable(n);
                                    this.AddMeasureToStation(n);

                                    Tsunami2.Properties.Save("", "Accepting offset from stake out");
                                    return false;
                                }
                                else
                                {
                                    string name = n._Point._Name;

                                    // if we are staking out a line, we want to rename de point differently
                                    if (n._Point._OriginType == ElementType.LineAsPoint)
                                    {
                                        n._Point = n._Point.Clone() as Point;
                                        name = name.Substring(0, 10) + "_P###";
                                        name = this.ElementManager.Iterate(name);
                                        n._Point._Name = name;
                                    }

                                    if (bRenameThePoint && Common.Analysis.Element.ExistanceProblematicOf(n._Point, this.ElementManager.GetPointsInAllElements(), out List<Point> dumy, out List<Point> dumy2))
                                    {
                                        if (name.ToUpper().Contains("["))
                                            name = this.ElementManager.Iterate(n._Point._Name);
                                        else
                                            name = this.ElementManager.Iterate(n._Point._Name + "[###]");
                                        n._Point._Name = name;
                                    }
                                    n._Status = new M.States.Good();
                                    SavePoint(n);
                                }
                            }
                        }
                        break;
                    case M.States.Types.Control:
                    case M.States.Types.Bad:
                    case M.States.Types.Temporary:
                        break;
                    case M.States.Types.Failed:
                    case M.States.Types.Cancel:
                    case M.States.Types.Unknown:
                    case M.States.Types.Replace:
                    default:
                        return false;
                }

                this.AddMeasureToStation(n);

                if (Tsunami2.Preferences.Values.GuiPrefs.Automation != null && Tsunami2.Preferences.Values.GuiPrefs.Automation.Activated)
                {
                    Tsunami2.Preferences.Values.GuiPrefs.Automation.Run(n);
                }

                Tsunami2.Properties.Save("", "Acquisition received");

                return true;
            }

            public string ExcelLinkedPath = "";

            public void ExportToExcel(object sender, M.MeasurementEventArgs e)
            {
                IO.Excel.WriteInWorkBook(IO.Excel.CreateObjectsFrom(e.Measure as Measure));
            }

            public void ExportToBatch(object sender, M.MeasurementEventArgs e)
            {
                IO.Batch.Create(e.Measure, this.BatchPath);
            }

            private void SavePoint(Measure measure)
            {
                // lets add the point to the main list
                measure._Point.StorageStatus = Common.Tsunami.StorageStatusTypes.Keep; // Tsunami2.Properties.Points.AddIfDoNotExistWithSameGUID(measure._Point);
                // possibility to annalyse data in excel before chosing to keep or not the point
                if (ExcelLinkedPath != "")
                {
                    IO.Excel.WriteInWorkBook(IO.Excel.CreateObjectsFrom(measure));
                }

                

                E.Manager.Module.AddingStrategy strategy = E.Manager.Module.AddingStrategy.ReplaceIfSameGroup;
                if (IsAlreadyMeasured(measure._Point, measure._Status))
                {
                    DsaFlag dsaFlag = DsaFlag.GetByNameOrAdd(this.DsaFlags, "ShouldChangeNameOfExistingAfterMeasurement");
                    if (bRenameThePoint && WantToChangeThePointName(measure._Point, ref dsaFlag))
                    {
                        measure._Point._Name = GetNameProposition(measure._Point);
                        strategy = E.Manager.Module.AddingStrategy.RenameIfExistInSameGroup;
                    }
                    else
                    {
                        strategy = E.Manager.Module.AddingStrategy.ReplaceIfSameGroup;
                        DealWithPreviousMeasureOfThePoint(measure._Point);
                    }
                }
                this.SavePoint(measure._Point, strategy);
            }

            internal override void SavePoint(Point p, E.Manager.Module.AddingStrategy strategy = E.Manager.Module.AddingStrategy.RenameIfExistInSameGroup)
            {
                if (strategy != E.Manager.Module.AddingStrategy.ReplaceIfSameGroup)
                    if (Common.Analysis.Element.ContainAPointIn(p._Name, Element.States.Good, this.ElementManager.GetPointsInAllElements()))
                        p.State = Element.States.FromStation;

                p.StorageStatus = Common.Tsunami.StorageStatusTypes.Keep;
                this.ElementManager.AddElement(this, p, strategy);
            }

            internal void AddLastPointAndMeasure(Measure n)
            {
                if (n != null)
                {
                    n._Status = new M.States.Good();
                    this.SavePoint(n._Point);

                    // if ref-link is still present the measure already exist with a state turned from questionnable to good, but if the link is broken (restart of tsunami) it is not.
                    this.AddMeasureToStation(n, true);
                }
            }

            internal override void ReCreateWhatIsNotSerialized(bool saveSomeMemory = false)
            {
                try
                {
                    //this.flags = new Flags();
                    bool saveSomeMemoryForThisStation = saveSomeMemory;
                    //if (!((this.stationParameters._State is Stations.Station.State.Closed) || (this.stationParameters._State is Stations.Station.State.Bad)))
                    //saveSomeMemoryForThisStation = false;
                    base.ReCreateWhatIsNotSerialized(saveSomeMemoryForThisStation);

                    if (stationParameters._IsSetup && !(this.stationParameters._State is State.Closed
                        || this.stationParameters._State is State.Bad
                        || this.stationParameters._State is State.Good))
                        this.stationParameters._State = new State.Measuring();

                    SetStartingPreferences();

                    ReCreateRefLinkWithElementManager();
                    RecreateRefLinksWithParameters();

                    this.View?.PinTableView?.ReCreatePinnedTable();

                    this._MeasureManager.MeasureStateChange += On_MeasureStateChangeInMan;
                }
                catch (Exception ex)
                {
                    throw new Exception($"Cannot re-create what was not serialized in {this._Name}", ex);
                }
            }

            private void RecreateRefLinksWithParameters()
            {
                var a = this.StationTheodolite.Parameters2;
                if (a != null) a.Module = this;

                var b = this.StationTheodolite.Parameters2.Setups;
                if (b != null)
                {
                    if (b.InitialValues != null) b.InitialValues.Module = this;
                    if (b.TemporaryValues != null) b.TemporaryValues.Module = this;
                    if (b.FinalValues != null) b.FinalValues.Module = this;
                }
            }

            private void ReCreateRefLinkWithElementManager()
            {
                //List<E.Point> pointInElementManger = this.ElementModule.GetPointsInAllElements();

                //E.Point reference;
                //foreach (var item in this.StationTheodolite.MeasuresTaken)
                //{
                //    reference = pointInElementManger.Find(x => (x as TsuObject).Id == item._OriginalPoint.Id);
                //    if (reference != null) item._OriginalPoint = reference;
                //    reference = pointInElementManger.Find(x => (x as TsuObject).Id == item._Point.Id);
                //    if (reference != null) item._Point = reference;
                //}
                //foreach (var item in this.StationTheodolite.MeasuresToDo)
                //{
                //    reference = pointInElementManger.Find(x => (x as TsuObject).Id == item._OriginalPoint.Id);
                //    if (reference != null) item._OriginalPoint = reference;
                //    reference = pointInElementManger.Find(x => (x as TsuObject).Id == item._Point.Id);
                //    if (reference != null) item._Point = reference;
                //}
            }



            // This is use to do special treatement if known et unknwon are measured during the setup
            private void NeutralizeMeasuresBecauseItIsAUnknownPoints(Measure n)
            {
                Parameters.TypeOfPointMeasured type = this.stationParameters.TypeOfPointsMeasured;

                // si point inconnu durant le setup
                if (ElementManager.GetPointsInAllElements().Find(x => x._Name == n._Point._Name) == null)
                {
                    if (type == Parameters.TypeOfPointMeasured.UnknownYet) type = Parameters.TypeOfPointMeasured.UnknownPointsOnly;
                    if (type == Parameters.TypeOfPointMeasured.KnownPointsOnly) type = Parameters.TypeOfPointMeasured.BothKnownAndUnknown;
                    if (ElementManager.GetPointsInAllElements().FindAll(x => x._Coordinates != null && x._Coordinates.HasAny).Count > 3)
                    {
                        MessageInput mi = new MessageInput(MessageType.Choice, R.T_NewPoint)
                        {
                            ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                        };
                        if (mi.Show().TextOfButtonClicked == R.T_YES)
                            n._Status = new M.States.Questionnable();
                    }

                    //this.ElementManager.AddElement(this, n._Point);
                    n.GeodeRole = Geode.Roles.Unknown;
                }
                else
                {
                    if (type == Parameters.TypeOfPointMeasured.UnknownYet) type = Parameters.TypeOfPointMeasured.KnownPointsOnly;
                    if (type == Parameters.TypeOfPointMeasured.UnknownPointsOnly) type = Parameters.TypeOfPointMeasured.BothKnownAndUnknown;
                }
                this.stationParameters.TypeOfPointsMeasured = type;
            }


            public void OnNextBasedOn(I.Sensor newSensor)
            {
                this.StationTheodolite.Parameters2._Instrument = newSensor;
            }
            public void OnNextBasedOn(EC.TheoreticalElement t)
            {

            }

            #endregion

            #region Station Setup

            #region New Station

            internal string GetAvailableStationName()
            {
                var addtionnalPoints = new List<Point>();
                foreach (var item in Tsunami2.Properties.GetStations())
                {
                    if (item is Station ps)
                        if (ps.Parameters2._StationPoint!=null)
                            addtionnalPoints.Add(ps.Parameters2._StationPoint);
                }
                if (this.FinalModule.defaultUseNameFromNewStationName != null)
                    return Common.Analysis.Element.Iteration.GetNextWithChoice(
                        this.FinalModule.defaultUseNameFromNewStationName,
                        this.ElementManager,
                        "", addtionnalPoints);

                string name = "";

                // for the zone
                string zone = GetBestZoneName();
                name += zone + ".STL.";
                name += DateTime.Now.ToString("yyyyMMdd");

                // add team
                if (this.StationTheodolite.Parameters2._Team != R.String_UnknownTeam)
                {
                    string t = this.StationTheodolite.Parameters2._Team;
                    if (t.Length > 5)
                        t = t.Substring(0, 5);
                    name += t;
                }
                else
                    name += "-";
                name += "##.";
                name = Common.Analysis.Element.Iteration.GetNextWithChoice(name, this.ElementManager, "", addtionnalPoints);
                return name;

            }

            internal string GetBestZoneName()
            {
                string zone = "";
                // 1. if a zone is defined, take it
                Z.Zone z = this.ElementManager.zone;
                if (z != null)
                    zone += z._Name;
                else
                {
                    // 2. if default point name is defined to somethin gelse than zone, take it
                    string zoneFromDefaultMeasureName = this.stationParameters.DefaultMeasure._Point.ZoneOrAcc;
                    if (zoneFromDefaultMeasureName != "ZONE")
                    {
                        zone = zoneFromDefaultMeasureName;
                    }
                    else
                    {
                        // 3. get most common zone from element manager
                        string acc = this.ElementManager.GetMostCommonZone();
                        if (acc != "")
                            zone = acc;
                        else
                        {
                            // 4. keep "zone"
                            zone = "ZONE";
                        }
                    }
                }
                return zone;
            }

            internal void ChangeTolerance(I.InstrumentTolerance.Type type, double newHCC, double newVCC, double newMM)
            {
                type.H_CC = newHCC;
                type.V_CC = newVCC;
                type.D_mm = newMM;

                _TsuView.UpdateView();
            }


            internal void ChangeSigma(I.SigmaForAInstrumentCouple couple, double newSAH, double newSAV, double newSD)
            {
                couple.sigmaAnglCc = newSAH;
                couple.sigmaZenDCc = newSAV;
                couple.sigmaDistMm = newSD;

            }


            internal void ChangeStationPointToANewOne(string name, SocketCode code, double isntrumentHeight)
            {
                if (name != null)
                {
                    Point p = new Point() { _Name = name, SocketCode = code, StorageStatus = Common.Tsunami.StorageStatusTypes.Hidden };

                    // p._Coordinates.Su = new Coordinates(p._Name, 0, 0, 0); ben non pas forcement a 000...
                    this.stationParameters.Setups.InitialValues.IsPositionKnown = false;
                    this.stationParameters._StationPointGuid = p.Guid;
                    p._Origin = this._Name;
                    //this.ElementManager.AddElement(this, p);
                    foreach (Point item in this.StationTheodolite.PointsMeasured)
                    {
                        item._Origin = this.StationTheodolite._Name;
                    }
                    foreach (Measure item in this.StationTheodolite.MeasuresTaken)
                    {
                        item.Origin = this.StationTheodolite._Name;
                        item._Point._Origin = this.StationTheodolite._Name;
                    }
                    this.stationParameters.Setups.InitialValues.IsInstrumentHeightKnown = Parameters.Setup.Values.InstrumentTypeHeight.Known;
                    this.stationParameters.Setups.InitialValues.InstrumentHeight = new DoubleValue(isntrumentHeight, 0);
                    if (this.StationTheodolite.MeasuresToDo.Count > 0)
                    {
                        Goto_ComputeTheNeeds(MeasureToCome);
                        this._InstrumentManager.SetNextMeasure(MeasureToCome);
                    }
                    _TsuView.UpdateView();
                }
            }

            internal void SetNewStationPoint()
            {
                string proposedName = this.GetAvailableStationName();
                string message = "Station point name?";
                if (!this.ElementManager.AskName(proposedName, out string newName, message))
                    return;

                if (newName != null && newName != "")
                {
                    SocketCode.GetSmartCodeFromName(newName, out SocketCode smartCode, out string message2);
                    
                    double instrumentHeight = 0;
                    //if (code != null) // value has been added manually
                    //{
                    //    instrumentHeight = code.InstrumentHeightForPolarMeasurement;
                    //}
                    //else
                    //{
                    //    instrumentHeight = dummy;
                    //}
                    this.View.TryAction(() => { this.ChangeStationPointToANewOne(newName, smartCode, instrumentHeight); }, R.T_FREE_STATION_PREPARATION);
                }

                this.View.UpdateModuleName();
            }

            //
            //Setup of the 'Strategy deisgn pattern'
            //
            #endregion

            #region Station Point

            internal void ChangeStationPointToKnownOne()
            {
                Point p = this.ElementManager.SelectOnePoint(string.Format("{0};{1}", R.T_POINT_STATIONNED, R.T_PLEASE_SELECT_THE_KNOWN_POINT_USE_FOR_THE_STATION_POSITION));
                if (p != null) this.stationParameters._StationPoint = p.Clone() as Point;
                this.stationParameters.Setups.InitialValues.IsPositionKnown = true;
                foreach (Point item in this.StationTheodolite.PointsMeasured)
                {
                    item._Origin = this.StationTheodolite._Name;
                }
                foreach (Measure item in this.StationTheodolite.MeasuresTaken)
                {
                    item.Origin = this.StationTheodolite._Name;
                    item._Point._Origin = this.StationTheodolite._Name;
                }
                if (this.StationTheodolite.MeasuresToDo.Count > 0)
                {
                    Goto_ComputeTheNeeds(MeasureToCome);
                    this._InstrumentManager.SetNextMeasure(MeasureToCome);
                }
                _TsuView.UpdateView();
            }

            #endregion

            #region Management.Instrument

            internal override void OnInstrumentChanged(I.Sensor i = null)
            {
                // if (this.IsBad || this.IsClosed) return; // to avoid reopinig of tsu to connect instrumetnt oevery station, remove because it you reopen a tsu file, it can lead to problem if the instrument manger have no instrument.
                I.Sensor previous = StationTheodolite.Parameters2._Instrument;

                if (this._InstrumentManager.AllElements.Count == 0)
                    this._InstrumentManager.AllElements.AddRange(Tsunami2.Preferences.Values.Instruments);

                var selectables = this._InstrumentManager.GetByClass(I.InstrumentClasses.POLAR);

                if (i == null)
                   i = _InstrumentManager.SelectInstrument("Select instrument", selectables, multiSelection: false, preselected: null, true) as I.Sensor;

                base.OnInstrumentChanged(i);

                I.Sensor newOne = null;
                if (i == null)
                    newOne = this.tempInstrument;
                else
                    newOne = i;

                bool newTypeOfInstrument = newOne._InstrumentClass != previous._InstrumentClass;

                this.StationTheodolite.Parameters2.Tolerance = null;

                // reset sigma apriori
                if (newTypeOfInstrument)
                {
                    if (newOne != null)
                    {
                        if (previous._Name != newOne._Name)
                            this.StationTheodolite.Parameters2.ResetAprioriSigma();
                    }
                    else
                        this.StationTheodolite.Parameters2.ResetAprioriSigma();
                }


                if (this.StationTheodolite.Parameters2._Instrument != null)
                {
                    if (this.StationTheodolite.Parameters2._Instrument._Name != R.String_Unknown)
                    {
                        _InstrumentManager.SelectedInstrumentModule.WaitingForDetailsOfNextMeasurement += OnMeasurementDetailsAskedByInstrumentModule;
                        if (i is MID.AT40x.Instrument || this.StationTheodolite.Parameters2._Instrument is MID.AT40x.Instrument)
                        {
                            (_InstrumentManager.SelectedInstrumentModule as MID.AT40x.Module).CompensatorStateChanged += OnInstrumentCompensatorStatusChanged;
                            (_InstrumentManager.SelectedInstrumentModule as MID.AT40x.Module).ReflectorChanged += OnReflectorChanged;
                            (_InstrumentManager.SelectedInstrumentModule as MID.AT40x.Module).LiveDataStarted += On_LiveDataStarted;
                            (_InstrumentManager.SelectedInstrumentModule as MID.AT40x.Module).LiveDataStopped += On_LiveDataStopped;

                            //set the compensator based on the station state
                            {
                                try
                                {
                                    (_InstrumentManager.SelectedInstrumentModule as I.PolarModule).ChangeVerticalisationState(this.stationParameters.Setups.InitialValues.VerticalisationState);
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }

                        SetDefaultDedicatedReflectorBasedOnInstrument(i);

                        _InstrumentManager.SelectedInstrumentModule.View.estimatedTimeInMs = 3;

                        if (View != null)
                            this.InvokeOnApplicationDispatcher(View.UpdateInstrumentView);

                        this.reflectorManager = CreateReflectorManager(this);

                        I.PolarModule pm = _InstrumentManager.SelectedInstrumentModule as I.PolarModule;

                        if (newTypeOfInstrument)
                            SetDefaultFaceMeasurement(pm);

                        SendNextMeasureToInstrument();
                    }
                }

                this.InvokeOnApplicationDispatcher(() => { this.View.UpdateViewOfStationParameters(); });
            }



            private void SetDefaultDedicatedReflectorBasedOnInstrument(I.Instrument i)
            {
                var instrument = this.StationTheodolite.Parameters2._Instrument;

                void ChangeReflector(M.Measure measure)
                {
                    var polarMeasure = measure as Polar.Measure;
                    if (polarMeasure?.Distance?.Reflector?._Name == "Unknown")
                    {
                        if (i is MID.AT40x.Instrument || instrument is MID.AT40x.Instrument)
                        {
                            polarMeasure.Reflector = new Reflector() { _Name = R.T_MANAGED_BY_AT40X };
                        }
                        else if (i is MID.TDA5005.Instrument || instrument is MID.TDA5005.Instrument)
                        {
                            polarMeasure.Reflector = GetCCR1_5Number1();
                        }
                        else if (i is MID.TS60.Instrument || instrument is MID.TS60.Instrument)
                        {
                            polarMeasure.Reflector = GetCCR1_5Number1();
                        }
                    }
                }

                ChangeReflector(this.stationParameters.DefaultMeasure);
                var defaultMeasure = this.stationParameters.DefaultMeasure;

                foreach (var item in this.StationTheodolite.MeasuresToDo)
                {
                    ChangeReflector(item);
                }
            }

            internal override void OnMeasurementDetailsAskedByInstrumentModule(object sender, M.MeasurementEventArgs e)
            {
                if (this.StationTheodolite.MeasuresToDo.Count == 0)
                    this.AddNewPoint(AskForName: false);
                SendNextMeasureToInstrument();
                this.InvokeOnApplicationDispatcher(this.View.UpdateViewOfNextPoint);
            }

            private void SetDefaultFaceMeasurement(I.PolarModule pm)
            {
                if (pm == null) return;
                if (this.FaceMeasurementHasBeenManuallySet) return;

                pm.SetDefaultStationParameter(this.stationParameters);
            }

            internal void StakeOutKnownLine()
            {
                try
                {
                    this.ElementManager._SelectedObjects.Clear();
                    this.ElementManager.SelectableObjects.Clear();
                    Element e = this.ElementManager.View.SelectOne("Choose a line that contains a bearing and a slope", E.Manager.View.ElementShownInListTypes.Shapes);

                    if (e is EC.FittedShape)
                        if ((e as EC.FittedShape).parametricShape is Line3D)
                        {
                            Point line = GetLineFromFittedShaped((e as EC.FittedShape));
                            this.StationTheodolite.MeasuresToDo.InsertRange(0, this.CreateMeasuresFromElement(line, new M.States.Questionnable()));
                            this.SendNextMeasureToInstrument();
                            this.View.UpdateViewOfNextPoint();
                        }
                }
                catch (Exception ex)
                {
                    string titleAndMessage = $"failed;{ex.Message}";
                    new MessageInput(MessageType.Critical, titleAndMessage).Show();
                }
            }


            internal void StakeOutLine()
            {
                List<Point> points = new List<Point>();


                bool selectPointInsteadOfusingNextPoints = true;
                if (selectPointInsteadOfusingNextPoints)
                {
                    this.ElementManager._SelectedObjects.Clear();
                    EC.CompositeElement e = this.ElementManager.SelectPoints("Choose points to fit with a line;The line will be present in the element as a point named 'Line....' that contains a bearing and a slope so that the coordinates in the Beam-Line CS represente the coordinates in the Line CS");
                    if (e != null)
                        points.AddRange(e.GetPoints());
                }
                else
                {
                    // create a point from the 'next points' list
                    foreach (var item in this.StationTheodolite.MeasuresToDo)
                    {
                        if (item._OriginalPoint._Coordinates.HasAny)
                            points.Add(item._OriginalPoint);
                    }
                }
                if (points.Count > 1)
                {
                    EC.FittedShape line3D = LinearRegression.Get3dLine(points, out string message, Tsunami2.Properties.Zone);

                    if (message != R.T_NO_RESIDUALS_AVAILABLE)
                    {
                        string titleAndMessage = string.Format("{0};{1}\r\n{2}", R.T_LINE_RESIDUALS, R.T_DO_YOU_WANT_TO_KEEP_THIS_LINE, message);
                        MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                        {
                            ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                        };
                        if (mi.Show().TextOfButtonClicked == R.T_NO)
                            return;
                    }

                    this.ElementManager.AddElement(line3D);

                    Point line = GetLineFromFittedShaped(line3D);
                    this.StationTheodolite.MeasuresToDo.InsertRange(0, this.CreateMeasuresFromElement(line, new M.States.Questionnable()));
                    this.SendNextMeasureToInstrument();
                    this.View.UpdateViewOfNextPoint();
                }
                else
                {
                    throw new Exception(string.Format("{0};{1}", R.T_NOT_ENOUGHT_POINTS, R.T_YOU_NEED_NEED_AT_LEAST_2_KNOWN_POINTS_IN_THE_NEXT_POINTS_LIST));
                }
            }
            private Point GetLineFromFittedShaped(EC.FittedShape line3D)
            {
                if (line3D.parametricShape is Line3D)
                {
                    Point line = new Point();
                    line._Name = line3D.parametricShape._Name;
                    line._Coordinates = line3D.parametricShape._Coordinates;
                    line._Parameters.Slope = (line3D.parametricShape as Line3D).Slope.Value;
                    line._Parameters.GisementFaisceau = (line3D.parametricShape as Line3D).Bearing.Value;
                    line._OriginType = ElementType.LineAsPoint;
                    return line;
                }
                else
                    throw new Exception(R.T_THE_TREATMENT_OF_THE_FITTED_SHAPE_SELECTED_IS_NOT_IMPLEMENTED_YET);
            }


            private Reflector GetCCR1_5Number1()
            {
                return Tsunami2.Preferences.Values.Instruments.Find(x => x._Name == "CCR1.5_1") as Reflector;
            }

            //private bool letmeAloneWithDefaultReflector = false;


            private void OnReflectorChanged(object sender, MID.AT40x.ReflectorEventArgs e)
            {
                if (e.Target == I.InstrumentTypes.Unknown) return;
                Reflector r = Tsunami2.Preferences.Values.Instruments.Find(x => (x._InstrumentClass == I.InstrumentClasses.PRISME) && (x._InstrumentType == e.Target)) as Reflector;

                if (r != null)
                {
                    //this.stationParameters.DefaultMeasurdefe.Distance.Reflector = r;
                    SetDefaultReflectorIfNotAlreadySet(r);
                    if (this.StationTheodolite.MeasuresToDo.Count > 0)
                    {
                        // check if prism is not cahnge jst by type but wthout a serial number ('1')
                        M.MeasureOfDistance d = MeasureToCome.Distance;
                        bool sameType = (r._InstrumentType == d.Reflector._InstrumentType);
                        bool newOneHaveNoSerial = (r._SerialNumber == "1");
                        if (!(sameType && newOneHaveNoSerial))
                            d.Reflector = r;
                    }
                    Tsunami2.Properties.InvokeOnApplicationDispatcher(() => { this.View.UpdateViewOfStationParameters(); });

                    Tsunami2.Properties.InvokeOnApplicationDispatcher(this.View.UpdateViewOfNextPoint);
                }
                else
                    throw new Exception(R.T_AT_UnknownReflector);
            }

            private void OnInstrumentCompensatorStatusChanged(object sender, MID.AT40x.CompensatorEventArgs e)
            {
                // this.stationParameters.Setup._InitiaValues.VerticalisationState = e.InstrumentVerticalisation; no! this should be decide in the station and not by the instrument!

                this.UpdateView();
            }

            internal override void OnInstrumentConnected(object sender, I.StatusEventArgs e)
            {
                try
                {
                    (_InstrumentManager.SelectedInstrumentModule as I.PolarModule).ChangeVerticalisationState(this.stationParameters.Setups.InitialValues.VerticalisationState);
                }
                catch (Exception)
                {
                }

            }

            #endregion

            #region Management.Instrument Height

            internal void OnInstrumentHeightKnowledgeChanged(bool p)
            {
                this.stationParameters.Setups.InitialValues.InstrumentHeight = null;
                this.stationParameters.Setups.InitialValues.IsInstrumentHeightKnown = Parameters.Setup.Values.InstrumentTypeHeight.Unknown;
            }

            internal void ChangeInstrumentHeight(double h)
            {
                this.stationParameters.Setups.InitialValues.IsInstrumentHeightKnown = Parameters.Setup.Values.InstrumentTypeHeight.Known;
                this.stationParameters.Setups.InitialValues.InstrumentHeight = new DoubleValue(h, 0);
                //this.stationParameters._IsInstrumentHeightKnown = (h != 0) ? true : false;
            }
            #endregion

            #region V0

            internal void SetVZero()
            {
                PrepareOpeningMeasurement();
            }
            internal void SetV0Manually(double p)
            {
                bool ok = true;

                // check value
                if (p>400 || p<-400)
                {
                    p =Survey.Modulo400(p);
                    MessageInput mi = new MessageInput(MessageType.Warning, $"Module 400!;v0 = {p} gon ?")
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL }
                    };
                    ok = mi.Show().TextOfButtonClicked != R.T_CANCEL;
                }

                if (!ok) return;

                if (!this.StationTheodolite.Parameters2._StationPoint._Coordinates.HasAny)
                    throw new TsuException("Station position is not known!");
                stationParameters.Setups.InitialValues.CompensationStrategyType = CS.List.OrientatedOnly_LGC2;
                this.StationTheodolite.Parameters2.Setups.FinalValues = new Parameters.Setup.Values(this);
                this.StationTheodolite.Parameters2.Setups.FinalValues.Verified = true;

                this.StationTheodolite.Parameters2.Setups.FinalValues.vZero = new DoubleValue(p, 1.0);
                this.StationTheodolite.ParametersBasic._State = new State.Measuring();
            }

            private bool MakeInitialChecks(bool ignoreCloseCheck = false)
            {
                if (!ignoreCloseCheck)
                    if (!CheckStationState(this.stationParameters, IsClosed, this.View))
                        return false;
                // Check station position
                if (stationParameters.Setups.InitialValues.StationPoint == null)
                    throw new Exception(R.T_NO_POSITION_CHOOSED_FOR_THE_STATION);

                if (this.stationParameters._StationPoint == null)
                    throw new Exception(R.T149);

                // check station inst. heigth
                if (stationParameters.Setups.InitialValues.IsInstrumentHeightKnown == Parameters.Setup.Values.InstrumentTypeHeight.NotDefinedYet)
                    throw new Exception(R.T_E_NO_INSTRUMENT_HEIGHT_STRATEGY);

                // check for measure
                if (StationTheodolite.MeasuresTaken.Find(x => x._Status is M.States.Good) == null)
                    throw new Exception(R.T_E_NO_MEASURE);

                return CheckOpeningHasBeenControled(out _);
            }

            internal bool CheckOpeningHasBeenControled(out bool cancelled)
            {
                cancelled = false;
                if (!IsControlled())
                {
                    var prepare = "Prepare";
                    var measure = "Prepare, Goto and Measure";

                    if (!(this.FinalModule is Polar.Module))
                    {
                        prepare = ""; // to not show in guided module because you are not necessary in the good step a that moment
                        measure = "";
                    }

                    string titleAndMessage = $"{R.T_M_CLOSURE_NEEDED};{R.T_M_CLOSURE_DETAILS}";
                    MessageInput mi = new MessageInput(MessageType.Important, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { prepare, measure, R.T_CANCEL, R.T_FORCE },
                        TextOfButtonToBeLocked = R.T_FORCE
                    };
                    var r = mi.Show();

                    cancelled = (r.TextOfButtonClicked == R.T_CANCEL);

                    if (r.TextOfButtonClicked == R.T_FORCE)
                    {
                        if (this.StationTheodolite.Parameters2._IsSetup)
                            this.StationTheodolite.ParametersBasic._State = new State.Bad();
                        return true;
                    }
                    else if (r.TextOfButtonClicked == prepare)
                    {
                        this.PrepareOpeningMeasurement();
                    }
                    else if (r.TextOfButtonClicked == measure)
                    {
                        this.PrepareOpeningMeasurement(measureNow: true);
                    }
                    ClosureWasTriggeredAndForcedByCompute = true;
                    return false;
                }
                ClosureWasTriggeredAndForcedByCompute = false;
                return true;
            }

            internal bool IsControlled()
            {
                M.Measure m = this.FindFirstValidMeasureOftheStation();
                if (m == null) return false;
                List<M.Measure> goodOnes = this._Station.MeasuresTaken.FindAll(x => x._Status.GetType() != typeof(M.States.Bad));

                bool isControled = (goodOnes)[goodOnes.Count - 1]._Status.Type == M.States.Types.Control;

                if (isControled)
                    if (m._Point._Name != (goodOnes)[goodOnes.Count - 1]._Point._Name)
                        new MessageInput(MessageType.Warning, R.T_NOT_THE_OPENING_POINT) { AsNotification = true }.Show();

                return isControled;
            }

            /// <summary>
            /// When the closure have been done, it will choose a strategy based on available data
            /// </summary>
            internal void DetermineHowtoAndThenComputeWithLGC2()
            {
                try
                {
                    // Already setup?
                    if (stationParameters._IsSetup) throw new I.CancelException(R.T_ALREADY_SETUP);

                    // Closure check etc..
                    if (!MakeInitialChecks()) return;

                    // Choose strategy
                    CheckIfThereIsEnoughToCompute(0, out int numberOfUsabelMeasures);

                    if (this.stationParameters.Setups.InitialValues.IsPositionKnown)
                    {
                        if (numberOfUsabelMeasures < 2 || DsaFlag.IsSetToNever(this.DsaFlags, "ProposeFreeStationIfPointConnu"))
                        {
                            this.ChangeSetupStrategy(CS.List.OrientatedOnly_LGC2);
                        }
                        else
                        {
                            MessageInput mi = new MessageInput(MessageType.Choice, R.T150)
                            {
                                ButtonTexts = new List<string> { R.T_ORIENTATION_ONLY, R.T_FREE_STATION }
                            };
                            string respond = mi.Show().TextOfButtonClicked;
                            if (respond == R.T_FREE_STATION)
                            {
                                // this.FinalModule.defaultUseNameFromNewStationName = this.stationParameters.Setups.InitialValues.StationPoint._Name;
                                this.ChangeSetupStrategy(CS.List.FreeStation_LGC2);
                            }
                            else
                            {
                                if (numberOfUsabelMeasures > 0)
                                    this.ChangeSetupStrategy(CS.List.OrientatedOnly_LGC2);
                                else
                                    throw new Exception($"{R.T_NOT_POSSIBLE};{R.T_COULD_NOT_COMPUTE_WITH_NO_USABLE_MEASURE}");
                            }
                        }
                    }
                    else
                    {
                        if (stationParameters.Setups.InitialValues.VerticalisationState == Parameters.Setup.Values.InstrumentVerticalisation.NotVerticalised)
                        {
                            this.ChangeSetupStrategy(CS.List.NonVerticalizedSetup);
                        }
                        else
                        {
                            if (numberOfUsabelMeasures > 1)
                            {
                                this.ChangeSetupStrategy(CS.List.FreeStation_LGC2);
                                this.FinalModule.defaultUseNameFromNewStationName = this.stationParameters.Setups.InitialValues.StationPoint._Name;
                            }
                            else
                            {
                                string one = R.T_MEASURE_MORE_POINTS;
                                string two = R.T_COMPUTE_IN_A_NEW_FRAME;

                                MessageInput mi = new MessageInput(MessageType.Choice, R.T151)
                                {
                                    ButtonTexts = new List<string> { one, two }
                                };
                                string r = mi.Show().TextOfButtonClicked;
                                if (r == two)
                                    this.ChangeSetupStrategy(CS.List.Local_LGC2);
                                else
                                {
                                    this.SelectNextPoints();
                                    return;
                                }
                            }
                        }
                    }

                    // compute
                    CS.Common s = this.stationParameters.Setups.InitialValues.CompensationStrategy;

                    // pathetic way to have the strategy nwoning the module for message and questions
                    s._Module = this;

                    s.FileShowingStrategy = CS.FileEditionOptions.Ask;

                    bool succeed = s.Compute2(false, false);


                    // threat result
                    if (succeed)
                    {
                        //this._SetupStrategy.Update();
                        StationTheodolite.Parameters2._IsSetup = true;
                        FinalModule.Save("Station Setup");

                        StationTheodolite.Parameters2._State = new State.Measuring();

                        CS.Common str = this.stationParameters.Setups.InitialValues.CompensationStrategy;
                        // pathetic way to have the strategy nwoning the module for message and questions
                        str._Module = this;

                        foreach (var item in str.TempPointsToBeUpdated)
                        {
                            this.SavePoint(item, str.TempPointsToBeUpdatedStrategy);
                        }

                    }
                    else
                        this.stationParameters.Setups.InitialValues.CompensationStrategyType = CS.List.NoComputation;

                    this._TsuView.UpdateView();

                }
                catch (I.CancelException ex)
                {
                    string titleAndMessage = $"{R.T_CANCELLATION_OF_COMPENSATION};{ex.Message}";
                    new MessageInput(MessageType.Critical, titleAndMessage).Show();
                }
                catch (Exception ex)
                {
                    string titleAndMessage = $"{R.T_STATION_COMPENSATION_FAILED};{ex.Message}";
                    new MessageInput(MessageType.Critical, titleAndMessage).Show();
                }
            }

            internal bool CheckIfThereIsEnoughToCompute(int neededNumber, out int numberOfKnownPointWithGoodMeasure)
            {
                bool result = false;
                int numberOfGoodMeasure = 0;
                numberOfKnownPointWithGoodMeasure = 0;
                List<string> pointNamesAlreadyInTheList = new List<string>();
                foreach (Measure m in this.StationTheodolite.MeasuresTaken)
                {
                    if (m._Status is M.States.Good)
                    {
                        if (m._OriginalPoint != null && !pointNamesAlreadyInTheList.Contains(m._OriginalPoint._Name))
                        {
                            pointNamesAlreadyInTheList.Add(m._OriginalPoint._Name);
                            numberOfGoodMeasure += 1;
                            if (m._OriginalPoint._Coordinates != null && m._OriginalPoint._Coordinates.HasAny)
                                numberOfKnownPointWithGoodMeasure += 1;
                        }
                    }
                }
                if (numberOfKnownPointWithGoodMeasure < neededNumber)
                    return false;
                else
                    result = true;
                return result;
            }
            #endregion



            #endregion

            #region Points

            #region Set
            internal void RemovePointAndSwitchtoNext(Measure m)
            {
                if (m == null) return;

                List<M.Measure> nexts = this.StationTheodolite.MeasuresToDo.FindAll(x => x._OriginalPoint._Name == m._OriginalPoint._Name);
                if (nexts.Count > 0)
                {
                    this.StationTheodolite.RemoveMeasureFromToDoList(nexts[0]);
                }
                else
                {
                    nexts = this.StationTheodolite.MeasuresToDo.FindAll(x => x._Point._Name == m._Point._Name);
                    if (nexts.Count > 0)
                    {
                        this.StationTheodolite.RemoveMeasureFromToDoList(nexts[0]);
                    }
                    else
                    {
                        if (this.StationTheodolite.MeasuresToDo.Count > 0)
                        {
                            this.StationTheodolite.RemoveMeasureFromToDoListAt(0);
                        }
                    }
                }

                this.View.UpdateViewOfNextPoint();
            }

            internal void SelectNextPoints()
            {
                EC.CompositeElement e = SelectPoints();
                if (e == null) return;
                if (this.StationTheodolite.MeasuresTaken.Count > 0 && CheckIfPointIsTheOpeningOne(e))
                    SetNextPointToOpeningPoint();
                else
                    PrepareNextPoints(e, new M.States.Unknown());
            }


            private bool CheckIfPointIsTheOpeningOne(EC.CompositeElement e)
            {
                List<Point> points = e.GetPoints();
                if (points.Count == 1)
                {
                    Measure openingMeasure = this.GetOpeningMeasure();
                    if (openingMeasure != null)
                    {
                        if (points[0]._Name == this.GetOpeningMeasure()._Point._Name)
                        {
                            string control = R.T_CONTROL;
                            string titleAndMessage = string.Format("{0};{1}",
                                R.T_DO_YOU_WANT_TO_CONTROL_OR_CLOSE_THE_STATION,
                                $"{R.T_YOU_HAVE_SELECTED_THE_OPENING_FOR_POINT_FOR_THE_NEXT_MEAUREMENT}\r\n{R.T_DO_YOU_WANT_TO_MEASURE_IT_AS_A_NORMAL_POINT_OR_TO_USE_IT_TO_CHECK_OR_CLOSE_THE_STATION}");
                            MessageInput mi = new MessageInput(MessageType.Warning, titleAndMessage)
                            {
                                ButtonTexts = new List<string> { control, R.T_NORMAL_POINT }
                            };
                            if (mi.Show().TextOfButtonClicked == control)
                                return true;
                        }
                    }
                }
                return false;
            }

            internal void SelectNextPointsToStakeOut()
            {
                EC.CompositeElement e = SelectPoints();
                PrepareNextPoints(e, new M.States.Questionnable());
            }

            internal void PrepareNextPoints(EC.CompositeElement compositeElement, M.State state)
            {
                if (compositeElement == null) return;
                if (compositeElement.Elements.Count == 0) return;

                if (compositeElement is EC.SequenceTH)
                {
                    this.Sequence_Load(compositeElement as EC.SequenceTH, state);
                }
                else if (compositeElement is EC.FittedShape fit)
                {
                    Point lineAsPoint = GetLineFromFittedShaped(fit);
                    lineAsPoint._OriginType = ElementType.LineAsPoint;
                    this.StationTheodolite.MeasuresToDo.AddRange(
                        this.PrepareMeasuresFromElements(
                            new EC.CompositeElement() { Elements = new CloneableList<Element>() { lineAsPoint } }, new M.States.Questionnable()));

                }
                else
                {
                    SearchAndAddForPointsMissingFromSequences(compositeElement);
                    this.StationTheodolite.MeasuresToDo.AddRange(this.PrepareMeasuresFromElements(compositeElement, state));
                }

                SendNextMeasureToInstrument();
                _TsuView.UpdateView();
            }



            private EC.CompositeElement SelectPoints()
            {
                ElementManager._SelectedObjects.Clear();
                ElementManager.SetSelectableToAllPoints();
                ElementManager._SelectedObjectInBlue = null;
                EC.CompositeElement e = ElementManager.SelectPoints();
                return e;
            }


            internal void SetNextPointToUnknownOneOld()
            {
                // now moved to AddNewPoint() and AddNewPointWithCustomName()
            }

            private SocketCode SetCode(Point p)
            {
                SocketCode defaultCode = this.stationParameters.DefaultMeasure._Point.SocketCode;
                SocketCode.GetSmartCodeFromName(p._Name, out SocketCode smartCode, out string message);

                if ((smartCode != null && defaultCode != smartCode) || defaultCode == null)
                    if (SocketCode.Ask(this.View, Tsunami2.Preferences.Values.SocketCodes, smartCode, "NameForPTH", out SocketCode code, out double dummy))
                        return code;
                return defaultCode;
            }

            internal Measure ForcedOpeningMeasure = null;

            internal Measure GetOpeningMeasure(out bool hadTobeCreate)
            {
                hadTobeCreate = false;
                Measure m = null;
                Measure firstValid = this.FindFirstValidMeasureOftheStation();


                if (ForcedOpeningMeasure != null)
                    firstValid = ForcedOpeningMeasure;

                if (firstValid == null)
                {
                    if (StationTheodolite.MeasuresToDo.Count > 1)
                    {
                        string titleAndMessage = string.Format("{0};{1}", R.T_NO_OPENING_AVAILABLE_YET, R.T_DO_YOU_WANT_TO_USE_THE_FIRST_POINT_OF_THE_NEXT_POINTS_LIST_AS_THE_OPENING_AND_ADD_IT_TO_THE_END_OF_THIS_SAME_LIST);
                        MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                        {
                            ButtonTexts = new List<string> { R.T_YES, R.T_CANCEL }
                        };
                        if (R.T_YES == mi.Show().TextOfButtonClicked)
                        {
                            m = MeasureToCome.Clone() as Measure;
                            m.Guid = Guid.NewGuid();
                            m._Status = new M.States.Control();
                            hadTobeCreate = true;
                        }
                    }
                    else
                        throw new Exception(R.T_NO_VALID_MEASURE_YET);
                }
                else
                {
                    m = firstValid;
                }
                return GetCleanedMeasure(m);
            }

            internal void SetNextPointToOpeningPoint(bool measureNow = false)
            {
                Measure ouverture = GetOpeningMeasure(out bool hadtobeCreate);

                if (hadtobeCreate)
                {
                    if (Tsunami2.Preferences.Values.GuiPrefs.AddPointsToTheEndOfTheNextPointsList.IsTrue)
                        this.StationTheodolite.MeasuresToDo.Add(ouverture);
                    else
                        this.StationTheodolite.MeasuresToDo.Insert(0, ouverture);
                    View.UpdateViewOfNextPoint();
                    return;
                }
                if (ouverture == null) return;
                ouverture.DirectMeasurementWanted = false;
                ouverture._Status = new M.States.Control();
                if (measureNow)
                    ouverture.DirectMeasurementWanted = true;
                SetNextPoint(ouverture);
            }

            private Measure GetCleanedMeasure(Measure firstValid)
            {
                if (firstValid == null) return null;
                Measure m = firstValid.Clone() as Measure;
                m.Clean();
                m.Guid = Guid.NewGuid();
                m.CommentFromUser = R.T_NO_COMMENT;
                return m;
            }

            /// <summary>
            /// will reused an existing measure as template for a new one, the originalPoint (nominal value) will be kept.
            /// New coordinates will be computed from the new measure ans stored in ._Point.
            /// Exception: if the template do not have a OriginalPoint (because if was the first measuremetn of this point) then the .Point will be clone and set for the OriginalPoint (and play as new nominal)
            /// </summary>
            /// <param name="m"></param>
            /// <param name="askName"></param>
            /// <param name="iterate"></param>
            internal void SetNextPointToAlreadyMeasured(Measure m, bool askName = false, bool iterate = false)
            {
                Measure clone = m.Clone() as Measure;
                clone.Guid = Guid.NewGuid();

                if (clone._OriginalPoint == null || !clone._OriginalPoint._Coordinates.HasAny)
                {
                    clone._OriginalPoint = m._Point;
                }

                var point = m._Point.Clone() as Point;
                point.StorageStatus = Common.Tsunami.StorageStatusTypes.Temp;
                clone._Point = point;
                clone._Status = new M.States.Unknown();
                clone.GeodeRole = Geode.Roles.Unknown;
                clone.Clean();


                string proposedName = clone._Point._Name;
                if (iterate) proposedName = GetNameProposition(clone._Point);
                TsuOptionalShowing.Type dsaType = TsuOptionalShowing.Type.DontShowDontShowAgainBox;
                if (askName) if (!this.View.RenamePoint(clone, ref dsaType, proposedName: proposedName)) return;

                SetNextPoint(clone);
            }

            internal void SetNextPoint(Measure m)
            {
                this.StationTheodolite.MeasuresToDo.Insert(0, m);
                SendNextMeasureToInstrument();
                _TsuView.UpdateView();
            }
            internal void SetNextPointsToAlreadyMeasured(bool reverse = false)
            {
                List<M.Measure> ms = this._MeasureManager.Select();

                // cancel
                if (ms == null) return;

                foreach (var item in ms)
                {
                    Measure m = item.Clone() as Measure;
                    m.Clean();
                    m._Status = new M.States.Unknown();
                    if (Tsunami2.Preferences.Values.GuiPrefs.AddPointsToTheEndOfTheNextPointsList.IsTrue)
                        this.StationTheodolite.MeasuresToDo.Add(m);
                    else
                        this.StationTheodolite.MeasuresToDo.Insert(0, m);
                }

                SendNextMeasureToInstrument();
                _TsuView.UpdateView();
            }
            internal void SetNextPointsToControlFace2()
            {
                this.StationTheodolite.MeasuresToDo = this.StationTheodolite.MeasuresTaken.ReverseCopy().Clone();
                foreach (Measure m in this.StationTheodolite.MeasuresToDo)
                {
                    m.Face = I.FaceType.Face2;
                    m._Status = new M.States.Unknown();
                    m.Clean();
                }
                SendNextMeasureToInstrument();
                _TsuView.UpdateView();
            }

            #endregion

            #region sequences
            internal void MoveNextPointDown(Measure m)
            {
                //up is down because the list is shown reversed;

                // Change state if in autosequence measuerment.

                if (this.StationTheodolite.MeasuresToDo.Count > 1)
                {
                    if ((StationTheodolite.MeasuresToDo[1] as Measure).DirectMeasurementWanted)
                    {
                        if (this.StationTheodolite.MeasuresToDo[1] == m)
                        {
                            MeasureToCome.DirectMeasurementWanted = true;
                            m.DirectMeasurementWanted = false;
                        }
                    }
                }
                this.StationTheodolite.MeasuresToDo.MoveUp(m);
                SendNextMeasureToInstrument();
                _TsuView.UpdateView();
            }
            internal void MoveNextPointUp(Measure m)
            {
                //up is down because the list is shown reversed;

                // Change state if in autosequence measuerment.
                if (this.StationTheodolite.MeasuresToDo.Count > 1)
                {
                    if ((StationTheodolite.MeasuresToDo[1] as Measure).DirectMeasurementWanted)
                    {
                        if (MeasureToCome == m)
                        {
                            (StationTheodolite.MeasuresToDo[1] as Measure).DirectMeasurementWanted = false;
                            m.DirectMeasurementWanted = true;
                        }
                    }
                }
                this.StationTheodolite.MeasuresToDo.MoveDown(m);
                SendNextMeasureToInstrument();
                _TsuView.UpdateView();
            }
            #endregion


            #endregion

            #region AddMeasure

            internal void CreateMeasuresFromElement()
            {
                throw new NotImplementedException();
            }

            public Measure CreateMeasureFromPoint(Point originalPoint, M.State state, bool silent, out bool cancel)
            {
                cancel = false;
                // Create Measure
                Measure m = this.StationTheodolite.Parameters2.DefaultMeasure.Clone() as Measure;
                m.Guid = Guid.NewGuid();

                if (!silent)
                    CheckTargetHeightCoherence(m);

                if (m.Distance.Reflector != null)
                    if (m.Distance.Reflector._Name == R.StringTilt_Unknown)
                        if (this._InstrumentManager.SelectedInstrument != null)
                            if (this._InstrumentManager.SelectedInstrument._Model.ToUpper().Contains("AT40"))
                                if (this._InstrumentManager.SelectedInstrumentModule is MID.AT40x.Module atm)
                                    if (atm.ActualReflector != null)
                                    {
                                        if (atm.ActualReflector._Name != R.StringTilt_Unknown)
                                            m.Distance.Reflector = atm.ActualReflector;
                                    }
                                    else
                                    {
                                        m.Distance.Reflector._Name = R.T_MANAGED_BY_AT40X;
                                    }

                m.CommentFromUser = R.T_NO_COMMENT;
                m._Status = state;
                m._OriginalPoint = originalPoint;  // I dont know why it was a clone here, it should not or the modification of the point in element manager will not reflect in computes...// m._OriginalPoint = p.Clone() as Point ;

                var point = originalPoint.Clone() as Point;
                point.Type = Point.Types.NewPoint;
                point.StorageStatus = Common.Tsunami.StorageStatusTypes.Hidden; // to have it availavle if we restart tsunami with something in the next point lis.

                m.PointGUID = point.Guid;
                if (state is M.States.Unknown)
                {
                    if (point._Name != NAME_BASED_ON_TIME) // because if it is set to timedName it is never the same
                    {
                        if (IsAlreadyMeasured(point, state) && !this.sequenceBeingPrepared && !silent)
                        {
                            if (!WantToReMeasureExisting(point))
                            {
                                cancel = true;
                                return null;
                            }

                            DsaFlag dsaFlag = DsaFlag.GetByNameOrAdd(this.DsaFlags, "ShouldChangeNameOfExistingAfterMeasurement");
                            if (WantToChangeThePointName(point, ref dsaFlag))
                            {
                                point._Name = GetNameProposition(point);
                            }
                        }
                    }
                }
                return m;
            }

            public override List<Measure> CreateMeasuresFromElement(Element e, M.State state, bool silent = false)
            {
                List<Measure> measures = new List<Measure>();

                if (e is EC.CompositeElement)
                {
                    // e.Elements.Reverse(); for the Setup_measureAllRefs to show in the right order
                    foreach (Element item in e.Elements)
                    {
                        measures.AddRange(CreateMeasuresFromElement(item, state, silent));
                    }
                }
                else
                {
                    Measure m = CreateMeasureFromPoint(e as Point, state, silent, out bool cancel);

                    if (!cancel)
                        measures.Add(m);
                }
                return measures;
            }

            private void CheckTargetHeightCoherence(Measure m)
            {
                if (m == null) return;
                if (m._Point == null) return;
                if (m._Point.SocketCode == null) return;
                if (m.Extension.Value == Tsunami2.Preferences.Values.na) return;

                double de = m._Point.SocketCode.DefaultExtensionForPolarMeasurement;



                if (de != m.Extension.Value)
                {
                    string titleAndMessage = $"{R.T_EXTENSION} {R.T_VALIDATION} {R.T_FOR} {m._Point._Name};{R.T_THE_CODE_DEFINE_AN_EXTENSION_OF} {(de * 1000).ToString()} mm," +
                                             $" {R.T_BUT_THE_EXTENSION_IS_DEFINED_TO} {m.Extension.ToString("m-mm")} mm\r\n" +
                                             $"{R.T_DO_YOU_WANT_TO_USE} {m.Extension.ToString("m-mm")} mm ?";
                    MessageInput mi = new MessageInput(MessageType.Warning, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                    };
                    string r = mi.Show().TextOfButtonClicked;
                    if (r == R.T_NO)
                    {
                        m.Extension = new DoubleValue(de);
                    }
                }
            }

            private bool WantToChangeThePointName(Point p, ref DsaFlag option)
            {
                switch (option.State)
                {
                    case DsaOptions.Always: return true;
                    case DsaOptions.Never: return false;
                    case DsaOptions.Ask_to:
                    case DsaOptions.Ask_to_with_pre_checked_dont_show_again:
                    default:
                        string message = $"{p._Name} {R.T_ALREADY_MEASURED}, {R.T_DO_YOU_WANT_TO_CHANGE_THE_NAME};{R.T_NO_NEW_COORD}";
                        MessageInput mi = new MessageInput(MessageType.Choice, message)
                        {
                            ButtonTexts = new List<string> { R.T_CHANGE, R.T_DONT },
                            DontShowAgain = option
                        };
                        var r = mi.Show();

                        return r.TextOfButtonClicked == R.T_CHANGE;
                }
            }

            private void DealWithPreviousMeasureOfThePoint(Point p)
            {
                DsaFlag dsaFlag = DsaFlag.GetByNameOrAdd(this.DsaFlags, "PutPreviousMeasureToBad");

                string titleAndMessage = string.Format(R.T_INVALID_PREVIUS_MES, p._Name);
                var r = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = CreationHelper.GetYesNoButtons(),
                    DontShowAgain = dsaFlag
                }.Show();
                //TsuBool dontShowAgain = new TsuBool(!flags.AskToPutPreviousMeasureToBad);
                //if (this.View.ShowMessageOfChoice(string.Format(R.T_INVALID_PREVIUS_MES, p._Name),
                //    R.T_YES, R.T_NO, "", "", false, dontShowAgain)
                //    == R.T_YES)
                if (r.TextOfButtonClicked == R.T_YES)
                {
                    this.StationTheodolite.MeasuresTaken.FindAll(x => x._Point._Name == p._Name).ForEach(c => c._Status = new M.States.Bad());
                    foreach (var c in this.StationTheodolite.PointsMeasured.Where((x => x._Name == p._Name)))
                        if (c.Type == Point.Types.NewPoint) c.State = Element.States.Bad;
                }
            }

            private bool WantToReMeasureExisting(Point p)
            {
                DsaFlag dsaFlag = DsaFlag.GetByNameOrAdd(this.DsaFlags, "ShouldRemeasureExisting");

                string message = $"{R.T_ALREADY_MEASURED}; {R.T_A_POINT_WITH_THE_NAME} '{p._Name}' {R.T_HAVE_ALREADY_BEEN_MEASURED_FROM_THIS_STATION} ({this._Station._Name})\r\n{R.T_DO_YOU_WANT_TO_MEASURE_IT_AGAIN}";

                //TsuBool dontShowAgain = new TsuBool(false); 
                //string rep = this.View.ShowMessageOfChoice(message, R.T_CANCEL, R.T_Re_measure, "", "", false, dontShowAgain);

                var r = new MessageInput(MessageType.Choice, message)
                {
                    ButtonTexts = new List<string> { R.T_Re_measure, R.T_CANCEL },
                    DontShowAgain = dsaFlag
                }.Show();

                return r.TextOfButtonClicked != R.T_CANCEL;
            }

            private string GetNameProposition(Point point)
            {
                return Common.Analysis.Element.Iteration.GetNext(point._Name, this.ElementManager.GetPointsNames(), Common.Analysis.Element.Iteration.Types.All);
            }

            // return true if there is a good measure for this point in the measuretaken list
            private bool IsAlreadyMeasured(Point p, M.State state)
            {
                bool pointFound = this.StationTheodolite.PointsMeasured.Any(x => x._Name == p._Name);
                bool measureFound = this.StationTheodolite.MeasuresTaken.Any(x => x._Point._Name == p._Name && x._Status is M.States.Good);
                bool stateIsControl = (state is M.States.Control);

                return (pointFound && measureFound && !stateIsControl);
            }

            /// <summary>
            /// if several element they should be in the order you want to measurethem
            /// </summary>
            /// <param name="e"></param>
            /// <param name="state"></param>
            internal List<Measure> PrepareMeasuresFromElements(EC.CompositeElement e, M.State state)
            {
                // propose to iterate de names of the sequences
                if (e is EC.SequenceTH)
                {
                    if (e.Elements.FindAll(x => x._Name.Contains("[")).Count > 0)
                        Common.Analysis.Element.Iteration.GetNext(e as EC.SequenceTH, ElementManager);
                }

                Reflector_ProposeToCancelIfNotSet(this.StationTheodolite.Parameters2.DefaultMeasure);

                List<Measure> measures = new List<Measure>();
                // prepare measurements
                foreach (Element item in e.Elements.ReverseCopy())
                {
                    measures.InsertRange(0, CreateMeasuresFromElement(item, state));
                }
                return measures;
            }

            private void Reflector_ProposeToCancelIfNotSet(Measure measure)
            {
                if (!Reflector_CheckIfManagedByInstrument(measure, out string errorMessage))
                {
                    string titleAndMessage = $"{errorMessage}";
                    if (new MessageInput(MessageType.Warning, titleAndMessage)
                        {
                            ButtonTexts = new List<string> { R.T_CONTINUE, R.T_CANCEL }
                        }.Show().TextOfButtonClicked.Equals(R.T_CANCEL))
                    {
                        throw new TsuCancelException(errorMessage);
                    }
                }
            }

            internal void Verticalisation_SetToOn()
            {
                if (this.stationParameters._Instrument == null) throw new Exception(R.T_NO_INSTRUMENT_SELECTED);
                if (!(this._InstrumentManager.SelectedInstrumentModule is I.PolarModule)) throw new Exception(R.T_INSTRUMENT_MUST_BE_A_TOTALSTATION_OR_A_TRACKER);
                (this._InstrumentManager.SelectedInstrumentModule as I.PolarModule).TurnOnCompensator();
                this.stationParameters.Setups.InitialValues.VerticalisationState = Parameters.Setup.Values.InstrumentVerticalisation.Verticalised;

            }

            internal void Verticalisation_SetToOff()
            {
                if (this.stationParameters._Instrument == null) throw new Exception(R.T_NO_INSTRUMENT_SELECTED);
                if (!(this._InstrumentManager.SelectedInstrumentModule is I.PolarModule)) throw new Exception(R.T_INSTRUMENT_MUST_BE_A_TRACKER);
                (this._InstrumentManager.SelectedInstrumentModule as I.PolarModule).TurnOffCompensator();
                this.stationParameters.Setups.InitialValues.VerticalisationState = Parameters.Setup.Values.InstrumentVerticalisation.NotVerticalised;
            }

            internal void MovetoTop(Measure m)
            {
                StationTheodolite.MeasuresToDo.Remove(m);
                StationTheodolite.MeasuresToDo.Insert(0, m);
                _TsuView.UpdateView();
            }

            #endregion

            #region Measurement

            #region Measures

            internal void AskIntrumentToMeasure(Measure m = null)
            {
                try
                {
                    if (m != null)
                        this._InstrumentManager.SetNextMeasure(m);
                    this._InstrumentManager.Measure();
                }
                catch (Exception ex)
                {
                    new MessageInput(MessageType.Critical, ex.Message).Show();
                }
            }

            /// <summary>
            ///  if possible and needed, compute angles between station and point to measure:
            /// </summary>
            private void PrepareTheMeasure(Measure m)
            {
                if (m == null)
                    return;

                // extension set to be managed by code?
                if (m.Extension.Value == Tsunami2.Preferences.Values.na)
                {
                    if (m._Point.SocketCode != null)
                        m.Extension = new DoubleValue(m._Point.SocketCode.DefaultExtensionForPolarMeasurement);
                    else
                        m.Extension = new DoubleValue(0);

                }

                if (flagAskForExtension)
                {
                    double ext;
                    string titleAndMessage = string.Format(
                        "{3};{4} '{0}' = '{1}' = '{2}', {5}",
                        m._OriginalPoint._Name,
                        m._OriginalPoint.SocketCode._Name,
                        m._OriginalPoint.SocketCode.Description,
                        R.T_APPLY_DEFAULT_EXTENSION,
                        R.T_THE_SOCKET_CODE_FOR_THE_POINT,
                        R.T_DO_YOU_WANT_TO_APPLY_THE_DEFAULT_ASSOCIATED_EXTENSION);
                    List<string> buttonTexts = new List<string> { R.T_Apply, R.T_DONT };

                    MessageTsu.ShowMessageWithTextBox(titleAndMessage, buttonTexts, out string buttonClicked, out string textInput, m._OriginalPoint.SocketCode.DefaultExtensionForPolarMeasurement.ToString(), true);

                    if (buttonClicked == R.T_Apply)
                        if (Double.TryParse(textInput, out ext))
                            m.Extension = new DoubleValue(ext, Tsunami2.Preferences.Values.na);
                }

                // AddPreviousReflector if empty
                bool noReflectorSet = (m.Distance.Reflector == null || m.Distance.Reflector._Name == R.String_Unknown);

                if (noReflectorSet)
                {
                    int count = this.StationTheodolite.MeasuresTaken.Count;
                    if (count > 0)
                    {
                        m.Distance.Reflector = (this.StationTheodolite.MeasuresTaken[count - 1] as Measure).Distance.Reflector;
                        DsaFlag dsaFlag = DsaFlag.GetByNameOrAdd(this.DsaFlags, "WarnForPreviousPrismUse");
                        //if (flags.WarnForPreviousPrismUse)
                        //{
                        //TsuBool dontShowAgain = new TsuBool(false);
                        ////View.ShowMessageOfExclamation(string.Format("{0};{1}",R.T_PREVIOUS_REFLECTOR_USE, R.T_THE_NEXT_MEASURE_DO_NOT_DEFINED_A_REFLECTOR), R.T_OK, "", "", this._Name, dsaFlag);
                        string titleAndMessage = string.Format("{0};{1}", R.T_PREVIOUS_REFLECTOR_USE, R.T_THE_NEXT_MEASURE_DO_NOT_DEFINED_A_REFLECTOR);
                        new MessageInput(MessageType.Important, titleAndMessage)
                        {
                            DontShowAgain = dsaFlag
                        }.Show();
                        //flags.WarnForPreviousPrismUse = !dontShowAgain.IsTrue;
                        View.UpdateViewOfNextPoint();
                        //}
                    }
                }

                // chose the apriori sigma, choosed by reflector from the actual lst in the stationparam define by actual instrument and condition
                m.APrioriSigmas = Common.Analysis.Instrument.GetAprioriSigma(this.StationTheodolite, m);

                // Goto
                if (Goto_CheckIfNeeded(m))
                {
                    ApplyVerticalOffsetForStakeOut(m);
                    Goto_ComputeTheNeeds(m);
                }
                Goto_CorrectAngleToMatchFace(m);

                m.SendingStationModule = this;
            }

            private void ApplyVerticalOffsetForStakeOut(Measure m)
            {
                if (m == null) m = MeasureToCome;
                // recompute 'original point' coordinates in case of tehre is a difference in H between theo and stakeout coorinates
                double dH = m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates.Value;
                if (dH != 0)
                {
                    Point modifiedPoint = m._OriginalPoint.Clone() as Point;

                    // Modifiy H
                    Systems.HtoZ(modifiedPoint, Coordinates.ReferenceSurfaces.RS2K);
                    Coordinates ccs = modifiedPoint._Coordinates.Ccs.Clone() as Coordinates;
                    double oX = ccs.X.Value;
                    double oY = ccs.Y.Value;
                    double oZ = ccs.Z.Value;
                    double x = 0;
                    double y = 0;
                    //SpatialObjDLL_32bit.TransformFromMLA(
                    //    oX, oY, oZ,
                    //    ref x, ref y, ref dH,
                    //    "CG2000");
                    Common.Compute.Transformation.Systems.Mla2CcsH(oX, oY, oZ, ref x, ref y, ref dH, Coordinates.ReferenceSurfaces.RS2K);
                    modifiedPoint._Coordinates = new CoordinatesInAllSystems();
                    modifiedPoint._Coordinates.Ccs = new Coordinates("new", x, y, dH);
                    Systems.ZToH(modifiedPoint, Coordinates.ReferenceSurfaces.RS2K);

                    //Recompute the Rest
                    Z.Zone z = Tsunami2.Properties.Zone;
                    if (z == null)
                        z = new Z.Zone();
                    if (z.Ccs2MlaInfo == null)
                        z.Ccs2MlaInfo = new Z.Zone.Ccs2Mla() { ReferenceSurface = Coordinates.ReferenceSurfaces.RS2K };
                    Systems.TryToFixMissingCoordinates(
                        new List<Point>(),
                        new List<Point>() { modifiedPoint },
                        new List<Point>() { modifiedPoint },
                        new List<Point>() { modifiedPoint },
                        Coordinates.GetReferenceFrame(z.Ccs2MlaInfo.ReferenceSurface, Coordinates.CoordinateSystemsTypes._2DPlusH),
                        z);

                    m._OriginalPointModifiedInH = modifiedPoint;
                }
                else
                    m._OriginalPointModifiedInH = null;
            }

            private void Goto_CorrectAngleToMatchFace(Measure m)
            {
                bool verticalAngleSaysF1 = m.Angles.Corrected.Vertical.Value < 200;
                if (m.Face == I.FaceType.Face2 && verticalAngleSaysF1)
                    m.Angles.corrected = Survey.TransformToOppositeFace(m.Angles.corrected);
            }

            private bool Goto_CheckIfNeeded(Measure m)
            {
                if (m._OriginalPoint == null) return false;
                if (m.HasRawAngles) return false;    // if contain raws measure it meant it has been measured and not computed so we keep the angles
                else if (gotoMustBeRecompute) return true;
                else if (!m.HasCorrectedAngles) return true;
                else return false;
            }

            /// <summary>
            /// if next point have known coordinates then we run the lgc compute with the refs as cala and the next mesure with huge sigma so that it doenst influence the compute but we can extract the computed observation
            /// </summary>
            private void Goto_ComputeTheNeeds(Measure m)
            {
                Station s = this.StationTheodolite;
                Parameters p = s.Parameters2;
                const double hugeAngleSigma = 200.0000;
                const double hugeDistanceSigma = 1000.000;
                s.MeasuresToExport.Clear();

                try
                {
                    // If the point has no coordinate we cannot do anything
                    if (m._OriginalPoint._Coordinates == null || !m._OriginalPoint._Coordinates.HasAny) return;

                    // We need at least a temporary or validated setup
                    if (p.Setups.BestValues == null) return;

                    // Enough to compute something?
                    bool enoughToCompute = false;
                    // If the station is not verticalized
                    if (p.Setups.InitialValues.VerticalisationState == Parameters.Setup.Values.InstrumentVerticalisation.NotVerticalised)
                    {
                        // We need a setup, even temporary, with a computed frame
                        if (p.Setups.BestValues.computedFrame != null)
                            enoughToCompute = true;
                    }
                    else
                    {
                        if (p._IsSetup) enoughToCompute = true;

                        if (p.Setups.BestValues.Ready) enoughToCompute = true;
                        // If the station is at a known point, we need one measure
                        if (p._StationPoint._Coordinates.HasAny
                            && s.MeasuresTaken.FindAll(x => x._Status is M.States.Good).Count > 0)
                            enoughToCompute = true;
                        // If the station is a free station, we need two measures
                        if (!p._StationPoint._Coordinates.HasAny
                            && s.MeasuresTaken.FindAll(x => x._Status is M.States.Good).Count > 1)
                            enoughToCompute = true;
                    }
                    if (!enoughToCompute) return;

                    // Create and add a fake measure with huge sigmas
                    Measure fakeMeasure = m.Clone() as Measure;
                    fakeMeasure.Angles.Corrected.Horizontal = new DoubleValue(0, hugeAngleSigma);
                    fakeMeasure.Angles.Corrected.Vertical = new DoubleValue(100, hugeAngleSigma);
                    fakeMeasure.Distance.Corrected = new DoubleValue(10, hugeDistanceSigma);
                    fakeMeasure.Distance.Reflector = this.BestHypothesisForNextReflector;
                    fakeMeasure._Status = new M.States.Good();
                    s.NextMeasure = fakeMeasure;
                    Point fakePoint = null;
                    if (m._OriginalPointModifiedInH != null)
                        fakePoint = m._OriginalPointModifiedInH.Clone() as Point;
                    else
                        fakePoint = m._OriginalPoint.Clone() as Point;
                    fakePoint._Name = "ForGotoOf_" + fakePoint._Name;
                    fakePoint.StorageStatus = Common.Tsunami.StorageStatusTypes.Temp;
                    fakeMeasure._Point = fakePoint;
                    fakeMeasure._OriginalPoint = fakePoint;
                    fakeMeasure.GeodeRole = Geode.Roles.Cala;

                    // If point already measured then we should change the name to be able to compute it separately in lgc.
                    Point existing = s.PointsMeasured.FirstOrDefault(x => x._Name == fakePoint._Name);
                    int count = 1;
                    while (existing != null)
                    {
                        fakePoint._Name += "(" + count.ToString() + ")";
                        existing = s.PointsMeasured.FirstOrDefault(x => x._Name == fakePoint._Name);
                        count++;
                    }

                    CS.List gotoStrategyType;
                    if (p.Setups.InitialValues.VerticalisationState == Parameters.Setup.Values.InstrumentVerticalisation.NotVerticalised)
                        gotoStrategyType = CS.List.NonVerticalizedGoto;
                    else
                        gotoStrategyType = CS.List.GotoForStationSettedUp;

                    // Compute with LGC
                    bool lgcSucced;
                    CS.Common gotoStrategy;
                    try
                    {
                        gotoStrategy = CS.Common.GetStrategy(gotoStrategyType, this);
                        gotoStrategy.FileShowingStrategy = CS.FileEditionOptions.HideAll;
                        lgcSucced = gotoStrategy.Compute2(silent: true, autoValidation: false);
                    }
                    catch (Exception ex)
                    {
                        throw new I.CancelException($"{R.T_BACKGROUND_COMPUTE} {R.T_FAILED}", ex);
                    }

                    if (lgcSucced)
                    {
                        // Fill result into next measure angles
                        var compensatedMeasures = gotoStrategy.CompensationResults.Measures;

                        // Add Observation to next measure
                        Measure computedMeasure = compensatedMeasures.Find(x => Math.Round(Math.Abs(x.Angles.Raw.Horizontal.Sigma)) == hugeAngleSigma);

                        // If there's no measure with high sigma, use the last measure
                        if (computedMeasure == null)
                            computedMeasure = compensatedMeasures[compensatedMeasures.Count - 1];

                        // put the compute angle as goto angle
                        m.Angles.Corrected = new Angles()
                        {
                            Horizontal = new DoubleValue(computedMeasure.Angles.Corrected.Horizontal.Value, Tsunami2.Preferences.Values.na),
                            Vertical = computedMeasure.Angles.Corrected.Vertical
                        };
                        // put the compute distance as goto angle
                        m.Distance.Corrected = computedMeasure.Distance.Corrected;
                    }
                    else
                    {
                        throw new Exception(R.T_LGC2_FAILED);
                    }
                    gotoMustBeRecompute = false;
                }

                catch (I.CancelException ex)
                {
                    DsaFlag dsaFlag = DsaFlag.GetByNameOrAdd(this.DsaFlags, "ShowGotoFailedMessage");
                    string titleAndMessage = $"{R.T_GOTO_FAILED};{ex.ToString().Replace("System.Exception: ", "")}";
                    new MessageInput(MessageType.Important, titleAndMessage)
                    {
                        DontShowAgain = dsaFlag
                    }.Show();

                    //if (flags.ShowGotoFailedMessage)
                    //{
                    //    TsuBool dontShowAgain = new TsuBool(!flags.ShowGotoFailedMessage);
                    //    View.ShowMessageOfExclamation($"{R.T_ERROR};{ex.ToString().Replace("System.Exception: ", "")}", R.T_OK, "", "", "", dontShowAgain);
                    //    flags.ShowGotoFailedMessage = !dontShowAgain.IsTrue;
                    //    return;
                    //}
                }
                catch (Exception ex)
                {
                    DsaFlag dsaFlag = DsaFlag.GetByNameOrAdd(this.DsaFlags, "ShowGotoFailedMessage");
                    if (dsaFlag.IsTrue)
                    {
                        if (ex.HResult == -2146233088) // LGC compute failed
                        {
                            //TsuBool dontShowAgain = new TsuBool(!flags.ShowGotoFailedMessage);
                            //View.ShowMessageOfCritical($"{R.T_ERROR};{ex.ToString().Replace("System.Exception: ", "")}", R.T_OK, "", "", "", dontShowAgain);
                            string titleAndMessage = $"{R.T_ERROR};{ex.Message}";
                            new MessageInput(MessageType.Critical, titleAndMessage)
                            {
                                DontShowAgain = dsaFlag
                            }.Show();
                            return;
                        }

                        string titleAndMessage1 = $"{R.T_FAILED_TO_COMPUTE_THEORETICAL_ANGLES};{R.T_IMPOSSIBLE_TO_POINT_IN_THE_RIGHT_DIRECTION_OF} '{m._Point._Name}'\r\n" +
                                                  $"{R.T_THE_GOTO_CALCULATION_FAILED_AND_NEXT_TRIALS_COULD_ALSO_FAILED_DO_YOU_TO_CONTINUE_TO_SHOW_WARNINGS}?\r\n" +
                                                  $"{R.T_FAILED_BECAUSE} : {ex.Message}";
                        MessageInput mi = new MessageInput(MessageType.Critical, titleAndMessage1)
                        {
                            ButtonTexts = new List<string> { R.T_NO, R.T_YES }
                        };
                        if (R.T_NO == mi.Show().TextOfButtonClicked)
                        {
                            dsaFlag.State = DsaOptions.Never;
                        }
                    }
                }
                finally
                {
                    // Remove the fake measure
                    s.NextMeasure = null;
                }
            }

            private static bool ComputePointlancéInSilenceWithLGC(Station station, Module module, bool deleteLgcFiles = false)
            {
                CS.Common strategy;
                if (station.Parameters2.Setups.InitialValues.VerticalisationState == Parameters.Setup.Values.InstrumentVerticalisation.NotVerticalised)
                {
                    strategy = new Compensations.NonVerticalized.PointLancé(module);
                }
                else
                {
                    strategy = new Compensations.PointLancéLGC2(module);
                }

                try
                {
                    strategy.FileShowingStrategy = CS.FileEditionOptions.HideAll;
                    if (strategy.Compute2(silent: true, autoValidation: false, deleteLgcFiles: deleteLgcFiles))
                    {

                        strategy.Update();
                        return true;
                    }
                    else
                        return false;
                }

                catch (Exception ex)
                {
                    throw new Exception($"{R.T_NEW_POINT_DETERMINATION_FAILED}", ex);
                }
            }

            internal void SendNextMeasureToInstrument()
            {
                SendMeasureToInstrument(MeasureToCome);
            }

            internal List<string> GetAllPointNames()
            {
                List<string> names = new List<string>();

                // Add names from element manager
                foreach (Point point in this.ElementManager.GetPointsInAllElements())
                {
                    names.Add(point._Name);
                }

                // From measures taken
                foreach (var item in this.StationTheodolite.MeasuresTaken)
                {
                    if (!(item._Status is M.States.Bad))
                    {
                        if (item._Point != null)
                            names.Add(item._Point._Name);
                    }
                }

                // From measures to do
                foreach (var item in this.StationTheodolite.MeasuresToDo)
                {
                    names.Add(item._Point._Name);
                }
                return names;
            }

            internal Measure LastMeasureSendToInstrument = null;

            internal void SendMeasureToInstrument(Measure m, M.State state = null)
            {
                if (m == null)
                    this._InstrumentManager.SetNextMeasure(null);
                else
                {
                    this.PrepareTheMeasure(m);
                    LastMeasureSendToInstrument = m;
                    Measure n = m.Clone() as Measure;
                    //n._Point = m._Point.Clone() as Point;
                    //Tsunami2.Properties.Points.AddIfDoNotExistWithSameGUID(n._Point); done with _point.Set()
                    if (state != null) n._Status = state;
                    this._InstrumentManager.SetNextMeasure(n);
                }
            }

            /// <summary>
            /// return if station is open
            /// </summary>
            /// <returns></returns>
            internal static bool CheckStationState(Parameters stationParameters, bool isClosed, View view)
            {
                if (isClosed)
                {
                    string cancel = R.T_CANCEL;
                    string titleAndMessage = string.Format("{0};{1}", R.T_STATION_IS_CLOSED, R.T_DO_YOU_WANT_TO_OPEN_IT_AGAIN);
                    MessageInput mi = new MessageInput(MessageType.Warning, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_OK, cancel }
                    };
                    if (mi.Show().TextOfButtonClicked != cancel)
                    {
                        if (stationParameters._IsSetup)
                            stationParameters._State = new State.Measuring();
                        else
                            stationParameters._State = new State.Opening();
                        view.UpdateViewOfStationParameters();
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }

            internal void RemoveNextMeasure(Measure m)
            {
                m = m ?? MeasureToCome;
                int index = this.StationTheodolite.MeasuresToDo.LastIndexOf(m);
                if (index >= 0) this.StationTheodolite.RemoveMeasureFromToDoListAt(index);

                SendMeasureToInstrument(MeasureToCome);

                this.View.UpdateViewOfNextPoint();
            }

            internal void RemoveAllMeasures()
            {
                this.StationTheodolite.ClearMeasuresToDo();
                //SetNextPointToUnknownOne();

                if (this.StationTheodolite.MeasuresToDo.Count > 0)
                    SendNextMeasureToInstrument();
                else
                    SendMeasureToInstrument(null);

                this.View.UpdateViewOfNextPoint();
            }

            internal Measure FindFirstValidMeasureOftheStation()
            {
                // for tour dhorizon it should be the firrst valid having the same origin name...
                int count = this.StationTheodolite.MeasuresTaken.Count;
                if (count == 0) return null;

                Measure lastMeasure = this.StationTheodolite.MeasuresTaken[count - 1] as Measure;
                if (lastMeasure == null) return null;


                foreach (Measure item in this.StationTheodolite.MeasuresTaken.FindAll(x => x.Origin == lastMeasure.Origin))
                {
                    if (item._Status is M.States.Good) return item;
                }
                return null;
            }

            internal Measure GetOpeningMeasure()
            {
                return FindFirstValidMeasureOftheStation();
            }

            internal double lastUsedTheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates = 0;
            /// Prepare the measure as a stake out put the state in questionnable and set the "extension1" 
            internal void StakeOut(Measure m, double theoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates = 0)
            {
                if (m == null && StationTheodolite.MeasuresToDo.Count > 0) m = MeasureToCome;

                // If we have ccs coordinates, we need to know if the point we stakeout is not at the same height as the theoretical one
                // Because a different H will lead to a different Xccs and Yccs => so if we look at dXccs dYccs the offset will be wrong.

                if (m._OriginalPoint._Coordinates.HasCcs)
                {
                    m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates.Value =
                        this.View.AskVerticalOffsetToStakeut(m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates.Value);
                    // Based on this, original point coordinates will be or not recomputed by the instrument at the moment of the goto.
                }

                gotoMustBeRecompute = true;
                this.ChangeMeasureStatus(m, new M.States.Questionnable());
            }

            #endregion

            #region  Treatements

            /// <summary>
            /// Add the measure to the measure manager, and check if the station can e roughly setup
            /// </summary>
            /// <param name="m"></param>
            public void AddMeasureToStation(Measure m, bool silent = false)
            {
                try
                {
                    AddInListBasedOnTime(m, this._MeasureManager.AllElements);
                    AddInListBasedOnTime(m, this._Station.MeasuresTaken);

                    m.GeodeRole = M.Measure.GetDefaultGeodeRole(m, StationTheodolite);

                }
                catch (Exception e)
                {
                    if (!silent)
                    {
                        string titleAndMessage = "Attention;" + e.Message;
                        new MessageInput(MessageType.Warning, titleAndMessage).Show();
                    }
                }

                //this._Station.PointsMeasured.Add(((Measure)m)._Point); // this list is temporary for the polar stations, it return the measuretaken points !

                this.View.UpdateViewOfPreviousPoint();

                if (this.View.PinTableView.isVisible)
                {
                    this.View.PinTableView.UpdateLastMeasInDataGridViewOffsetColumn(m);
                    if (Tsunami2.Preferences.Values.GuiPrefs.BocMeasuresAreNeverExpired.IsTrue)
                        if (Tsunami2.Preferences.Values.GuiPrefs.BOC_Run_on_measure_update.IsTrue)
                            if (Tsunami2.Properties.BOC_Context.ExistingResults.Find(x=>x.Parameters.PointsToUseAsOBSXYZ.Find(y=>y.Point == m._Point._Name && y.Used == true) != null) != null)
                            {
                                var magnets = Magnet.GetMagnets( new List<Point>() { m._Point });
                                this.View.PinTableView.BOC_Compute(this.FinalModule, magnets[0]._Name);
                            }
                }
            }

            // TODO mix those 2 methods
            private void AddInListBasedOnTime(Measure m, List<TsuObject> list)
            {
                if (list.Count == 0)
                    list.Add(m);
                else
                    for (int i = list.Count - 1; i > -1; i--)
                    {
                        Measure n = list[i] as Measure;
                        if (n._Date == m._Date && n._Status.Type == m._Status.Type)
                        {
                            if (!Debug.SimulateAt40x)
                                throw new Exception($"{R.T_MEASURE_ALREADY_PRESENT};{R.T_WITH_DATE}: {m._Date}\r\n\r\n{n}");

                        }
                        if ((list[i] as Measure)._Date < m._Date)
                        {
                            list.Insert(i + 1, m);
                            break;
                        }
                    }
            }

            // TODO mix those 2 methods
            private void AddInListBasedOnTime(Measure m, CloneableList<M.Measure> list)
            {
                if (list.Count == 0)
                    list.Add(m);
                else
                    for (int i = list.Count - 1; i > -1; i--)
                    {
                        if ((list[i] as Measure)._Date == m._Date)
                        {
                            if (!Debug.SimulateAt40x)
                                throw new Exception($"{R.T_MEASURE_ALREADY_PRESENT};{R.T_WITH_DATE}: {m._Date}");
                        }
                        if ((list[i] as Measure)._Date < m._Date)
                        {
                            list.Insert(i + 1, m);
                            break;
                        }
                    }
            }

            /// <summary>
            /// Fill the measure with station info
            /// </summary>
            private void TreatObservations(Measure m)
            {
                try
                {
                    if (m._Status is M.States.Questionnable)
                        m.GeodeRole = Geode.Roles.Poin;
                    if (m._Status is M.States.Unknown) m._Status = new M.States.Good();

                    // exception to check that the distance was in the calibrated range
                    if (m.Distance._Status is M.States.Bad)
                    {
                        m._Status = new M.States.Bad();
                        m.CommentFromTsunami = m.Distance.CommentFromTsunami;
                        string titleAndMessage = string.Format("{0};{1}", R.T_WARNING, m.Comment + " " + R.T_MEASURE_AS_BEEN_TURNED_INTO_BAD_STATE);
                        new MessageInput(MessageType.Warning, titleAndMessage).Show();
                    }

                    m.Origin = this.StationTheodolite._Name;
                    m.StationVerticalisationState = this.stationParameters.Setups.InitialValues.VerticalisationState;
                    if (m._Point._Name == R.String_Unknown)
                        GiveANameToTheMeasure(m);

                    if (DsaFlag.IsSetToNever(this.DsaFlags, "WantToIterateTheNewPointName"))
                    {
                        m._Point._Name = m._Point._Name.Replace(NAME_BASED_ON_TIME, DateTime.Now.ToString("P_yyyy-MM-dd_HH-mm-ss"));
                        m._OriginalPoint._Name = m._Point._Name;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"Couldn't treat the observation {m}", ex);
                }
            }


            /// <summary>
            /// If station in measuring state, then compute "point lancé"
            /// </summary>
            private static Point DeterminePointFromMeasure(Station st, Module module, Measure m, bool silente = false,
                bool deleteLgcFiles = false, bool validateComputeAsFastAsPossible = false, TsuView view = null,
                bool forceEvenIfNotInMeasuringState = false)
            {
                bool added = false;

                List<M.Measure> measuresToExport = st.MeasuresToExport;
                List<M.Measure> measuresTaken = st.MeasuresTaken;

                try
                {
                    // We determine news coordinates only if the station is already setup or if quick compute set
                    bool notMeasuring = !st.ParametersBasic._IsSetup;
                    bool notInFastMeasure = !(validateComputeAsFastAsPossible && !m._OriginalPoint._Coordinates.HasAny);

                    if (notMeasuring && notInFastMeasure && forceEvenIfNotInMeasuringState)
                        throw new I.CancelException();

                    //temporaray adding to be in the compute
                    measuresTaken.Add(m);
                    measuresToExport.Add(m);

                    var point = m._Point;
                    var originalPoint = m._OriginalPoint;


                    if (point == null)
                        point = originalPoint != null ? originalPoint.Clone() as Point : new Point() { _Name = "CreatedByDeterminePointFromMeasure" };

                    if (!m.IsFullyMeasured)
                        point._Coordinates = new CoordinatesInAllSystems();
                    else if (originalPoint?._Coordinates != null)
                        point._Coordinates = originalPoint._Coordinates.Clone() as CoordinatesInAllSystems;
                    else
                        point._Coordinates = new CoordinatesInAllSystems();

                    point._Origin = st._Name;
                    point.Date = m._Date;

                    if (!st.Parameters2._IsSetup || m._Status.Type == M.States.Types.Control)
                        point.StorageStatus = Common.Tsunami.StorageStatusTypes.Hidden;
                    m._Point = point;

                    //st.PointsMeasured.Add(m._Point);
                    added = true;

                    bool point_calculable = m.IsFullyMeasured &&
                                            (st.Parameters2.vZero != null) &&
                                            (st.Parameters2._InstrumentHeight != null);

                    if (point_calculable)
                    {
                        // LGC2
                        bool succeed = ComputePointlancéInSilenceWithLGC(st, module, deleteLgcFiles);
                        if (!succeed)
                        {
                            // m._Status = new M.States.MeasureStateCancel();
                        }
                        Point toReturn = m._Point;
                        toReturn._Origin = st._Name;
                        toReturn.Date = m._Date;
                        if (validateComputeAsFastAsPossible) toReturn._Coordinates.AreApproximate = true;
                        return toReturn;
                    }
                    else
                        Logs.Log.AddEntryAsFYI(module, "Point not calculable");

                    return null;
                }
                catch (I.CancelException)
                {
                    // do nothing
                    Logs.Log.AddEntryAsFYI(module, "Cancelled because not is measureing state AND not in fast mode AND force even if not in measuring state set to true");

                    return null;
                }
                catch (Exception ex)
                {
                    if (m._Status is M.States.Control)
                        return null;

                    string message = $"{R.T_FAILED_TO_DETERMINE_POINT_LANCÉ_COORDINATES}";

                    if (!silente)
                    {
                        if (view == null)
                            throw new Exception(message, ex);
                        else
                        {
                            string titleAndMessage = $"{message};{ex}";
                            new MessageInput(MessageType.Critical, titleAndMessage).Show();
                        }
                    }
                    else
                        Logs.Log.AddEntryAsError(module, message + ex);

                    return null;
                }
                finally
                {
                    if (added)
                    {
                        // remove because could rejected later by the user
                        measuresTaken.Remove(m);
                        measuresToExport.Remove(m);
                        //if (st.PointsMeasured.Count > 0)
                        //    st.PointsMeasured.RemoveAt(st.PointsMeasured.Count - 1);
                    }
                }
            }

            public static bool DetermineFullPointLancé(Station station, Module module, Measure m, bool silent = false,
                bool deleteLgcFiles = false, bool forceEvenIfNotInMeasuringState = false)
            {
                try
                {
                    if (m._Point != null)
                        m._Point.Date = DateTime.Now;
                    // compute "point lancé"
                    if (DeterminePointFromMeasure(station, module, m, silent, deleteLgcFiles, false, null, forceEvenIfNotInMeasuringState) != null)
                    {
                        Z.Zone zone = null;
                        TsuView view = null;
                        if (module != null)
                        {
                            zone = module.ElementManager.zone;
                            view = module.View;
                        }

                        CoordinatesInAllSystems.FillMissingCoordinates(station.Parameters2._StationPoint, m._Point, m._OriginalPoint, silent, zone, view);
                        return true;
                    }

                    return false;
                }
                catch (Exception ex)
                {
                    throw new Exception($"Couldn't determine coordinates for the measured point", ex);
                }
            }

            internal static CoordinatesInAllSystems ComputeDifferences(Measure measure)
            {
                // if measure is not complete we dont compute
                if (!measure.IsFullyMeasured || measure._OriginalPoint == null)
                    return new CoordinatesInAllSystems();

                // theoretical coordinates
                CoordinatesInAllSystems theoreticalCoordinates = measure._OriginalPoint._Coordinates;

                // juste measure coordinates
                CoordinatesInAllSystems measuredCoordinates = measure._Point._Coordinates;

                return CoordinatesInAllSystems.ComputeDifferences(measuredCoordinates, theoreticalCoordinates);
            }

            public string resultInfo;
            bool bRenameThePoint = false;

            // show all different coordinates avaible + a mix of CsStation(V) and BeamCS(L_R) if both existing
            private bool ShowResultsAndValideIt(CoordinatesInAllSystems coordinateList)
            {
                resultInfo = "";

                resultInfo += coordinateList.ToString(CoordinatesInAllSystems.StringFormat.AvailableOffsetsAsTable);

                if (DsaFlag.IsSetToAlways(this.DsaFlags, "KeepQuestionableMeasure"))
                {
                    return true;
                }
                else
                {
                    if (DsaFlag.IsSetToAlways(this.DsaFlags, "RejectQuestionableMeasure"))
                    {
                        if (DsaFlag.IsSetToNever(this.DsaFlags, "RenameKeptQuestionableMeasure"))
                            bRenameThePoint = false;
                        else
                            bRenameThePoint = false;
                        return false;
                    }
                    else
                    {
                        if (!DsaFlag.IsSetToNever(this.DsaFlags, "ShowQuestionableResults"))
                        {
                            if (resultInfo == "")
                                resultInfo = "Not available, not enough observation";
                            string message = string.Format(R.T_KEEP_REJECT_ACQU, "\r\n" + "\r\n" + resultInfo + "\r\n" + "\r\n");
                            Button helpButton = new Button() { Text = "Help! show me the CS", Font = Tsunami2.Preferences.Theme.Fonts.Large, Height = 77 };
                            helpButton.Click += delegate { this.View.ShowMessageOfImage("", R.T_OK, R.CS_Systems); };

                            DsaFlag dsaFlag = new DsaFlag("temp", DsaOptions.Ask_to);
                            MessageInput mi = new MessageInput(MessageType.Choice, message)
                            {
                                ButtonTexts = new List<string> { R.T_Keep, "Keep and Rename", R.T_Reject },
                                Controls = new List<System.Windows.Forms.Control> { helpButton },
                                DontShowAgain = dsaFlag,
                                UseMonoSpacedFont = true
                            };
                            using (MessageResult r = mi.Show())
                            {
                                if (dsaFlag.State == DsaOptions.Always)
                                {
                                    if (r.TextOfButtonClicked == R.T_Keep)
                                    {
                                        DsaFlag.GetByNameOrAdd(this.DsaFlags, "KeepQuestionableMeasure").State = DsaOptions.Ask_to; //flags.KeepQuestionableMeasure = true;
                                        DsaFlag.GetByNameOrAdd(this.DsaFlags, "RenameKeptQuestionableMeasure").State = DsaOptions.Never; //flags.RenameKeptQuestionableMeasure = false;
                                        DsaFlag.GetByNameOrAdd(this.DsaFlags, "ShowQuestionableResults").State = DsaOptions.Never; //flags.ShowQuestionableResults = false;
                                    }
                                    else
                                    {
                                        DsaFlag.GetByNameOrAdd(this.DsaFlags, "KeepQuestionableMeasure").State = DsaOptions.Ask_to; //flags.KeepQuestionableMeasure = true;
                                        DsaFlag.GetByNameOrAdd(this.DsaFlags, "RenameKeptQuestionableMeasure").State = DsaOptions.Ask_to; //flags.RenameKeptQuestionableMeasure = true;
                                        DsaFlag.GetByNameOrAdd(this.DsaFlags, "ShowQuestionableResults").State = DsaOptions.Never; //flags.ShowQuestionableResults = false;
                                    }

                                }

                                if (r.TextOfButtonClicked == "Keep and Rename")
                                    bRenameThePoint = true;
                                else
                                    bRenameThePoint = false;

                                return (r.TextOfButtonClicked != R.T_Reject);
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }

            // test the guisetting tolerance to decide if warning is needed, exception for temporary measure = closure, that will be shown 
            public bool ShowMeasureDifferencesIfPointAlreadyMeasured(Measure m)
            {
                // list of all measures + the last one still not validated
                List<M.Measure> l = new List<M.Measure>();

                l.AddRange(Common.Analysis.Measure.GetOkToCompare(_Station.MeasuresTaken, m));
                l.Add(m);

                List<M.Measure> diffs = Common.Analysis.Theodolite.Compare(l,
                   this.stationParameters.Tolerance.Closure.H_CC,
                   this.stationParameters.Tolerance.Closure.V_CC, this.stationParameters.Tolerance.Closure.D_mm);


                View.ShowMeasureDifferences(diffs);

                if (l.Count > 1)
                    // SHow? temporary is the intermidiar closure
                    if (m._Status is M.States.Control)
                    {
                        m.GeodeRole = Geode.Roles.Unused;
                        Measure errDiff = diffs.FindLast(x => x._Name.Contains("M0")) as Measure;
                        string error = "error";
                        string respond;
                        if (errDiff != null)
                        {
                            error = errDiff.Offsets.Local._Name;

                            if (errDiff._Status is M.States.Questionnable)
                            {
                                string titleAndMessage = string.Format("{0};{1}{2}", R.T_CLOSURE_OUT_OF_TOLERANCE, R.T_DO_YOU_WANT_TO_TRY_AGAIN, "\r\n\r\n" + error);
                                respond = new MessageInput(MessageType.Critical, titleAndMessage)
                                {
                                    ButtonTexts = new List<string> { R.T_Reject, R.T_Keep }
                                }.Show().TextOfButtonClicked;
                            }
                            else
                            {
                                string mn = "Measure Next";
                                string titleAndMessage = string.Format("{0}; {1}", R.T_CLOSURE_OK, error);
                                respond = new MessageInput(MessageType.GoodNews, titleAndMessage)
                                {
                                    ButtonTexts = new List<string> { R.T_OK + "!", showButtonToMeasureNextPoint ? mn : "" }
                                }.Show().TextOfButtonClicked;
                                if (respond == mn)
                                    if (this.StationTheodolite.MeasuresToDo.Count > 1)
                                        this.StationTheodolite.MeasuresToDo[1]._Status = new M.States.Good();
                            }
                        }
                        else
                        {
                            string titleAndMessage = string.Format("{0};{1}{2}", R.T_CLOSURE_OUT_OF_TOLERANCE, R.T_DO_YOU_WANT_TO_TRY_AGAIN, "\r\n\r\n" + error);
                            respond = new MessageInput(MessageType.Critical, titleAndMessage)
                            {
                                ButtonTexts = new List<string> { R.T_Reject, R.T_Keep }
                            }.Show().TextOfButtonClicked;
                        }
                        if (respond == R.T_Reject)
                            m._Status = new M.States.Cancel();

                        return true;
                    }

                return false;
            }
            private void GiveANameToTheMeasure(Measure m)
            {
                bool needANewName;
                if (m.Face == I.FaceType.DoubleFace)
                {
                    if (IsWaitingForToCompleteDoubleFaceMeasurement)
                    {
                        GivePreviousNameToTheMeasure(m);
                        IsWaitingForToCompleteDoubleFaceMeasurement = false;
                        needANewName = false;
                    }
                    else
                    {
                        needANewName = true;
                        IsWaitingForToCompleteDoubleFaceMeasurement = true;
                    }
                }
                else
                    needANewName = true;

                if (needANewName)
                {
                    string cancel = "Deny";
                    string tempName;
                    tempName = "temp_" + String.Format("{0:yyyyMMdd_HHmmss}", m._Date);
                    List<string> buttonTexts = new List<string> { R.T_NAME_IT, cancel };

                    MessageTsu.ShowMessageWithTextBox(R.T152, buttonTexts, out string buttonClicked, out string textInput, tempName);

                    if (buttonClicked != cancel)
                    {
                        m._Point = new Point(textInput);
                    }
                    else
                    {
                        m._Point = new Point("temp" + m._Date.ToString());
                        m._Status = new M.States.Bad();
                    }
                    m._Name = m._Point._Point;
                    previousMeasure = m;
                }
            }
            private void GivePreviousNameToTheMeasure(Measure m)
            {
                m._Point = this.previousMeasure._Point;
                m._Name = this.previousMeasure._Name;
                m._Point._Name = this.previousMeasure._PointName;
            }

            #endregion

            #region Change measurement parameters

            private void TestMeasureToModify(ref Measure usedMeasure,
                out bool isDefaultOne, bool askToApplyForAllIfIsDefault, bool askToApplyForAllIFIsNotDefault, out bool applyToAllSequence,
                out bool measureMustBeSentToInstrument)
            {
                // to start
                isDefaultOne = false;
                measureMustBeSentToInstrument = false;
                applyToAllSequence = false; // to start

                // Default measure? change for all sequence?
                if (usedMeasure == this.stationParameters.DefaultMeasure)
                {
                    isDefaultOne = true;

                    if (this.StationTheodolite.MeasuresToDo.Count > 0)
                    {
                        if (askToApplyForAllIfIsDefault)
                            applyToAllSequence = (this.View.AskToApplyNewDefautParameters());
                        measureMustBeSentToInstrument = true;
                    }
                }
                else // not the default measurement
                {
                    if (usedMeasure == null)
                    { // if not specified use next one
                        if (MeasuresPlanned)
                            usedMeasure = MeasureToCome;
                        else
                            throw new Exception(R.T_NO_MEASURE_TO_MODIFY);
                    }
                    if (this.StationTheodolite.MeasuresToDo.Count > 1)
                    {
                        if (askToApplyForAllIFIsNotDefault)
                            applyToAllSequence = (this.View.AskToApplyNewDefautParameters());
                    }
                    if (this.StationTheodolite.MeasuresToDo.Count > 0)
                        if (usedMeasure == MeasureToCome)
                            measureMustBeSentToInstrument = true;
                }
            }

            #region Reflector

            internal void Reflector(Measure usedMeasure, Reflector forcedReflector = null)
            {
                Reflector selected = null;
                if (forcedReflector == null)
                {
                    // apply to the used measure and set it as default is no default
                    CheckReflectorManager();

                    var preselected = new List<TsuObject>();
                    if (usedMeasure != null &&usedMeasure.Distance.Reflector != null)
                        preselected.Add(usedMeasure.Distance.Reflector);
                    
                    selected = reflectorManager.SelectInstrument(R.T153, selectables: null, multiSelection: false, preselected) as Reflector;

                    if (selected == null)
                        throw new OperationCanceledException(R.T_CANCEL_BY_USER);
                }
                else
                    selected = forcedReflector;

                if ((usedMeasure.Distance.Reflector = selected) != null)
                {
                    SetDefaultReflectorIfNotAlreadySet(usedMeasure.Distance.Reflector);
                }

                // Test
                TestMeasureToModify(ref usedMeasure, out bool isDefaultOne,
                    askToApplyForAllIfIsDefault: true, askToApplyForAllIFIsNotDefault: false,
                    applyToAllSequence: out bool applyToAllSequence, measureMustBeSentToInstrument: out bool measureMustBeSentToInstrument);

                // apply to the sequence? 
                if (applyToAllSequence) this.StationTheodolite.MeasuresToDo.ForEach(n => (n as Measure).Distance.Reflector = selected);

                // Update View
                this.View.UpdateSomeMainNodes(isDefaultOne, false, true);

                // Send to instrument
                if (measureMustBeSentToInstrument)
                {
                    this.gotoMustBeRecompute = true;
                    this.SendNextMeasureToInstrument();
                }
            }

            private void SetDefaultReflectorIfNotAlreadySet(Reflector reflector)
            {

            }

            /// <summary>
            /// Cheak if module exist, if not create it, then clean the list based on available calibration
            /// </summary>
            private void CheckReflectorManager()
            {
                if (reflectorManager == null)
                    if (_InstrumentManager == null)
                        throw new Exception(R.T155);
                    else
                    {
                        this.reflectorManager = CreateReflectorManager(this);
                    }


                // autorize only reflectorknown by at
                if (this.stationParameters._Instrument is MID.AT40x.Instrument)
                {
                    List<I.InstrumentTypes> typesWanted = new List<I.InstrumentTypes>{
                    I.InstrumentTypes.RRR0_5,
                    I.InstrumentTypes.RRR1_5,
                    I.InstrumentTypes.CCR0_5,
                    I.InstrumentTypes.CCR1_5,
                    I.InstrumentTypes.RFI0_5,
                    I.InstrumentTypes.TBR0_5,
                    };
                    reflectorManager.AllElements = reflectorManager.AllElements.FindAll(x => typesWanted.Contains((x as I.Instrument)._InstrumentType));
                    return;
                }
                // authorise reflector that have correction coefficients
                if (this.stationParameters._Instrument is I.TotalStation)
                {
                    I.TotalStation ts = this.stationParameters._Instrument as I.TotalStation;
                    reflectorManager.SelectableObjects = reflectorManager.AllElements.FindAll(
                        x => ts.EtalonnageParameterList.Find(y => y.PrismeRef._Name == x._Name) != null
                        );

                }
            }

            #endregion

            internal void ChangeNumberOfMeasurement(int number, Measure usedMeasure)
            {
                // Test
                TestMeasureToModify(ref usedMeasure, out bool isDefaultOne,
                    askToApplyForAllIfIsDefault: true, askToApplyForAllIFIsNotDefault: false,
                    applyToAllSequence: out bool applyToAllSequence, measureMustBeSentToInstrument: out bool measureMustBeSentToInstrument);

                // apply to the used measure
                usedMeasure.NumberOfMeasureToAverage = number;

                // apply to the sequence? 
                if (applyToAllSequence) this.StationTheodolite.MeasuresToDo.ForEach(n => (n as Measure).NumberOfMeasureToAverage = number);

                // Update View
                this.View.UpdateSomeMainNodes(isDefaultOne, false, true);

                // Send to instrument
                if (measureMustBeSentToInstrument)
                {
                    this.gotoMustBeRecompute = true;
                    this.SendNextMeasureToInstrument();
                }
            }

            internal void ChangeExtension(DoubleValue extension, Measure usedMeasure)
            {
                // Test
                TestMeasureToModify(ref usedMeasure, out bool isDefaultOne,
                    askToApplyForAllIfIsDefault: true, askToApplyForAllIFIsNotDefault: false,
                    applyToAllSequence: out bool applyToAllSequence, measureMustBeSentToInstrument: out bool measureMustBeSentToInstrument);

                // apply to the used measure
                usedMeasure.Extension = extension;

                // apply to the sequence? 
                if (isDefaultOne && applyToAllSequence) this.StationTheodolite.MeasuresToDo.ForEach(n => n.Extension = extension.Clone() as DoubleValue);

                // Update View
                this.View.UpdateSomeMainNodes(isDefaultOne, false, true);


                this.gotoMustBeRecompute = true;

                // Send to instrument
                if (measureMustBeSentToInstrument)
                {
                    this.SendNextMeasureToInstrument();
                }
            }

            internal void ChangeComment(string comment, Measure usedMeasure)
            {
                // Test
                TestMeasureToModify(ref usedMeasure, out bool isDefaultOne,
                    askToApplyForAllIfIsDefault: false, askToApplyForAllIFIsNotDefault: false,
                    applyToAllSequence: out bool applyToAllSequence, measureMustBeSentToInstrument: out bool measureMustBeSentToInstrument);

                // apply to the used measure
                usedMeasure.CommentFromUser = comment;

                // apply to the sequence? 
                if (applyToAllSequence)
                    this.StationTheodolite.MeasuresToDo.ForEach(n => (n as Measure).CommentFromUser = comment);

                // Update View
                this.View.UpdateSomeMainNodes(isDefaultOne, false, true);

                // Send to instrument
                if (measureMustBeSentToInstrument)
                {
                    this.gotoMustBeRecompute = true;
                    this.SendNextMeasureToInstrument();
                }
            }

            /// <summary>
            ///  Change le numéro d'opération dans chaque station line
            /// </summary>
            /// <param name="o"></param>
            internal override void ChangeOperationID(O.Operation o)
            {
                this.stationParameters._Operation = o;

                Tsunami2.Preferences.Values.GuiPrefs.LastOpUsed = o;
            }

            internal void ChangePointName(string name, Measure usedMeasure)
            {
                // Test
                TestMeasureToModify(ref usedMeasure, out bool isDefaultOne,
                    askToApplyForAllIfIsDefault: false, askToApplyForAllIFIsNotDefault: false,
                    applyToAllSequence: out bool applyToAllSequence, measureMustBeSentToInstrument: out bool measureMustBeSentToInstrument);

                // Change coordinates to new or exsiting ones
                Point existingPoint = this.ElementManager.GetPointsInAllElements().Find(x => x._Name.ToUpper() == name.ToUpper());

                // apply to the used measure
                usedMeasure._Point._Name = name.ToUpper();
                usedMeasure._Point._Coordinates = new CoordinatesInAllSystems();

                if (existingPoint != null)
                {
                    usedMeasure._OriginalPoint = existingPoint;
                }
                else
                {
                    Point p = usedMeasure._Point.Clone() as Point;
                    p.StorageStatus = Common.Tsunami.StorageStatusTypes.Hidden;
                    usedMeasure.OriginalPointGUID = p.Guid;
                }


                if (isDefaultOne && name != NAME_BASED_ON_TIME)
                    DsaFlag.GetByNameOrAdd(this.DsaFlags, "WantToIterateTheNewPointName").State = DsaOptions.Always; //this.flags.WantToIterateTheNewPointName = true;


                // apply to the sequence? 
                //TODO make something t change the pointname comming lnly from default name
                //if (applyToAllSequence) this.StationTheodolite.MeasuresToDo.ForEach(n => if (n._Point._Name = name);

                // Update View
                this.View.UpdateSomeMainNodes(isDefaultOne, false, true);

                // Send to instrument
                if (measureMustBeSentToInstrument)
                {
                    this.gotoMustBeRecompute = true;
                    this.SendNextMeasureToInstrument();
                }

            }

            public void ChangeCode(SocketCode code, Measure usedMeasure, bool ask = true)
            {
                if (usedMeasure == null) return;


                if (code == null)
                    ask = true;
                if (ask)
                {
                    SocketCode.GetSmartCodeFromName(usedMeasure._Point._Name, out SocketCode smartCode, out string pessage2);
                    var preSelectedCode = code.Id == "0" ? smartCode : code;
                    if (!SocketCode.Ask(this.View, Tsunami2.Preferences.Values.SocketCodes, preSelectedCode, "NameForPTH", out SocketCode receivedCode, out double dummy))
                        return;
                    code = receivedCode;
                }

                if (code == null)
                    return;
                // Test
                TestMeasureToModify(ref usedMeasure, out bool isDefaultOne,
                    askToApplyForAllIfIsDefault: true, askToApplyForAllIFIsNotDefault: false,
                    applyToAllSequence: out bool applyToAllSequence, measureMustBeSentToInstrument: out bool measureMustBeSentToInstrument);

                // apply to the used measure
                usedMeasure._Point.SocketCode = code;
                if (usedMeasure._OriginalPoint != null)
                    usedMeasure._OriginalPoint.SocketCode = code;


                if (isDefaultOne)
                {
                    bool updateExtension = true;

                    if (usedMeasure.Extension.Value == Tsunami2.Preferences.Values.na)
                        updateExtension = false;

                    if (updateExtension)
                        usedMeasure.Extension = new DoubleValue(code.DefaultExtensionForPolarMeasurement, 0);

                    // apply to the sequence? 
                    if (applyToAllSequence)
                    {
                        foreach (var item in this.StationTheodolite.MeasuresToDo)
                        {
                            item._Point.SocketCode = code;
                        }
                    }
                }
                else
                {
                    var v = usedMeasure.Extension.Value;
                    if (v != na && v != code.DefaultExtensionForPolarMeasurement)
                    {
                        string titleAndMessage = $"{R.T_UPDATE} {R.T_EXTENSION}?;" +
                                                 $"{R.T_DO_YOU_WANT_TO} {R.T_Apply} {code.DefaultExtensionForPolarMeasurement} mm {R.T_TO_THE_MEASUREMENT_OF}' {usedMeasure._Point._Name}' ?";
                        MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                        {
                            ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                        };
                        string r = mi.Show().TextOfButtonClicked;
                        if (r == R.T_YES)
                        {
                            usedMeasure.Extension = new DoubleValue(code.DefaultExtensionForPolarMeasurement);
                        }
                    }
                }

                // Update View
                this.View.UpdateSomeMainNodes(isDefaultOne, false, true);

                // Send to instrument
                if (measureMustBeSentToInstrument)
                {
                    this.gotoMustBeRecompute = true;
                    this.SendNextMeasureToInstrument();
                }
            }

            public void AddNewPoint(bool AskForName)
            {
                Measure m = this.StationTheodolite.Parameters2.DefaultMeasure.Clone() as Measure;
                m.Guid = Guid.NewGuid();

                var newPoint = this.StationTheodolite.Parameters2.DefaultMeasure._Point.Clone() as Point;

                //reflector
                Reflector_ProposeToCancelIfNotSet(m);


                string iteratedName = Common.Analysis.Element.Iteration.GetNext(newPoint._Name,
                            this.GetAllPointNames(), Common.Analysis.Element.Iteration.Types.SharpCharOnly);
                if (AskForName)
                {
                    if (!this.ElementManager.AskName(iteratedName, out string newName))
                        return;
                    newPoint._Name = newName;
                }
                else
                {
                    if (DsaFlag.IsSetToAlways(this.DsaFlags, "WantToIterateTheNewPointName"))
                        newPoint._Name = iteratedName;
                }

                newPoint.SocketCode = SetCode(newPoint);
                newPoint._Origin = "New point before station setup";
                newPoint.StorageStatus = Common.Tsunami.StorageStatusTypes.Hidden;
                m.OriginalPointGUID = newPoint.Guid;

                newPoint._Origin = this.StationTheodolite._Name;
                m.PointGUID = newPoint.Guid;

                //newPoint._Origin = this.StationTheodolite._Name;

                if (Tsunami2.Preferences.Values.GuiPrefs.AddPointsToTheEndOfTheNextPointsList.IsTrue)
                    this.StationTheodolite.MeasuresToDo.Add(m);
                else
                    this.StationTheodolite.MeasuresToDo.Insert(0, m);

                SendNextMeasureToInstrument();
                UpdateView();
            }

            /// <summary>
            /// ususally reflector should be selected by the user in the default measurement parameters or in each individual measure.
            /// The exception if for the tracket who deal with the prism itself, it needs to know internally whhch reflecor is used.
            /// We call this exception 'managed by Instrument', the prisme is selected once in the instrument mananger and not in the station manager.
            /// </summary>
            /// <param name="m"></param>
            private bool Reflector_CheckIfManagedByInstrument(Measure m, out string errorMessage)
            {
                errorMessage = "";
                bool reflectorIsKnown = m.Reflector != null && m.Reflector._Name != R.String_Unknown;
                bool instrumentIsSelected = this._InstrumentManager.SelectedInstrument != null && this._InstrumentManager.SelectedInstrument._Name != R.String_Unknown;

                if (!instrumentIsSelected)
                {
                    errorMessage = R.T_NO_INSTRUMENT_SELECTED;
                    return false;
                }

                if (!reflectorIsKnown)
                {
                    bool instrumentIsManagingTheReflector = this._InstrumentManager.SelectedInstrument._Model.ToUpper().Contains("AT40");
                    if (instrumentIsManagingTheReflector)
                    {
                        return true;
                    }
                    else
                    {
                        errorMessage = R.T_YOU_SHOULD_SELECT_A_REFLECTOR;
                        return false;
                    }
                }
                return true;
            }

            internal void ChangeFace(Measure usedMeasure, I.FaceType f)
            {
                // Test
                TestMeasureToModify(ref usedMeasure, out bool isDefaultOne,
                    askToApplyForAllIfIsDefault: true, askToApplyForAllIFIsNotDefault: false,
                    applyToAllSequence: out bool applyToAllSequence, measureMustBeSentToInstrument: out bool measureMustBeSentToInstrument);
                // apply to the used measure
                usedMeasure.Face = f;

                // apply to the sequence? 
                if (applyToAllSequence) this.StationTheodolite.MeasuresToDo.ForEach(n => (n as Measure).Face = f);

                // avoid overriding if set manually.
                if (isDefaultOne)
                    FaceMeasurementHasBeenManuallySet = true;

                // Update View
                this.View.UpdateSomeMainNodes(isDefaultOne, false, true);

                // Send to instrument
                if (measureMustBeSentToInstrument)
                {
                    this.gotoMustBeRecompute = true;
                    this.SendNextMeasureToInstrument();
                }
            }

            internal void ChangeMeasureStatus(Measure m)
            {
                m = m ?? MeasureToCome;
                if (m._Status.GetType() == typeof(M.States.Questionnable))
                    m._Status = new M.States.Unknown();
                else
                    m._Status = new M.States.Questionnable();

                SendNextMeasureToInstrument();
            }

            internal void ChangeMeasureStatus(Measure m, M.State forcedState)
            {
                if (this.StationTheodolite.Parameters2._IsSetup)
                    m._Status = forcedState;
                else
                {
                    new MessageInput(MessageType.Warning, R.T_NOSETUP_NOSTAKE).Show();
                }

                if (MeasuresPlanned)
                    if (MeasureToCome == m)
                        SendNextMeasureToInstrument();
            }


            #endregion

            #endregion

            #region Tests & checks

            #region Is

            //private bool IsStationHavingAnInstrument()
            //{
            //    if (this.stationParameters._Instrument == null)
            //    {
            //        this._TsuView.ShowMessageOfExclamation(R.T156, R.T_OK);
            //    }
            //    return (this.stationParameters._Instrument != null);
            //}
            //private bool IsStationHaveANewPoint()
            //{
            //    string s = this._TsuView.ShowMessageOfInput(
            //           R.T157,
            //            R.T_NAME_IT,
            //            R.T_CANCEL,
            //            "Station_01");
            //    if (s != R.T_CANCEL)
            //    {
            //        this.StationTheodolite.PointsMeasured[0] = new E.Point();
            //        //TheodoliteStation.s = s;
            //        this._TsuView.UpdateView();
            //        return true;
            //    }
            //    else
            //        return false;
            //}

            //private bool IsStationHaveAnHeight()
            //{
            //    string no = $"{R.T_NO} {R.T_UNKNOWN_HEIGHT}";
            //    string s = this._TsuView.ShowMessageOfChoice(
            //       R.T158,
            //        R.T_YES,
            //        no,
            //        R.T_CANCEL);
            //    if (s == R.T_YES)
            //    {
            //        s = this._TsuView.ShowMessageOfInput(
            //           R.T_TM_STATION_HEIGHT_SET,
            //            R.T_VALIDATE,
            //            R.T_CANCEL,
            //            "0.00000");
            //        if (s == R.T_CANCEL)
            //            return false;
            //        else
            //        {
            //            //stationParameters._IsInstrumentHeightKnown = true;
            //            stationParameters._InstrumentHeight.Value = T.Conversions.Numbers.ToDouble(s);
            //            this._TsuView.UpdateView();
            //            return true;
            //        }
            //    }
            //    else if (s == no)
            //    {
            //        stationParameters.Setups.InitialValues._IsInstrumentHeightKnown = TSU.Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight.Unknown;
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            private bool IsWaitingForToCompleteDoubleFaceMeasurement;

            private bool flagAskForExtension;
            internal string BatchPath;
            internal bool RemoveMeasureFromNextPointListAfterReception = true;
            internal bool ValidateComputeAsFastAsPossible = false;

            #endregion

            #region Controls

            internal void PrepareOpeningMeasurement(bool measureNow = false)
            {
                this.SetNextPointToOpeningPoint(measureNow: measureNow);
            }

            internal void ControlSelectedMeasureWithFace2(Measure m)
            {
                if (Common.Analysis.Measure.GetFaceType(this.StationTheodolite.MeasuresTaken) != I.FaceType.Face1) throw new Exception("Face2 exist");

                Measure clone = m.Clone() as Measure;
                clone.Guid = Guid.NewGuid();
                clone._Point = m._Point.Clone() as Point;
                clone.Face = I.FaceType.Face2;
                SetNextPoint(clone);

            }


            internal void CloseStation(out bool forcedBecauseNoClosure)
            {
                forcedBecauseNoClosure = false;

                DsaFlag.GetByNameOrAdd(this.DsaFlags, "ClosureTriggeredByEndStation").State = DsaOptions.Always; //flags.ClosureTriggeredByEndStation = true;
                if (!MakeInitialChecks(ignoreCloseCheck: true)) // because we want to close anyway
                {
                    return;
                }
                else
                {
                    forcedBecauseNoClosure = true;
                }

                DsaFlag.GetByNameOrAdd(this.DsaFlags, "ClosureTriggeredByEndStation").State = DsaOptions.Never; //flags.ClosureTriggeredByEndStation = false;

                if (_SetupStrategy == null) stationParameters.Setups.InitialValues.CompensationStrategyType = CS.List.AllCala;

                if (!(this.stationParameters._State is State.Bad))
                    this.stationParameters._State = new State.Closed();
                this.DecoupleFromInstrument();
                this.FinalModule.Save("Station closed");
                this._MeasureManager.MeasureStateChange -= On_MeasureStateChangeInMan;
                this.View.UpdateViewOfStationParameters();
            }

            internal void DisconnectInstrument()
            {
                if (this._InstrumentManager.SelectedInstrumentModule != null)
                    this._InstrumentManager.SelectedInstrumentModule.Disconnect();
            }

            #endregion

            #endregion

            #region export

            internal override void ExportToLGC2()
            {
                string fileName = this.View.GetSavingName(
                   Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, Tsunami2.Preferences.Values.PathOfTheSavedFile,
                    "LGC2 input (*.inp)|*.inp");

                int precisionForOutput = Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;



                if (fileName != "") Lgc2.ToLgc2(this, fileName, precisionForOutput);

            }

            internal void EditPointList()
            {


            }



            internal void Goto(Measure measure = null)
            {
                if (measure == null)
                {
                    measure = this.StationTheodolite.MeasuresToDoCome;
                }
                if (measure == null)
                    throw new TsuException("Nothing to 'goto'");

                // must be clone because we have to change the status to avoid good => mesure sequence
                Measure clone = measure.Clone() as Measure;
                clone._Status = new M.States.Unknown();

                this.SendMeasureToInstrument(clone);
                (this._InstrumentManager.SelectedInstrumentModule as I.PolarModule).Goto();
                // to make sure that the measure dont stay in case the next point list is empety
                this.SendMeasureToInstrument(null);
                this.SendNextMeasureToInstrument();
            }

            internal void MeasureNow(Measure measure = null)
            {
                if (measure == null) measure = this.StationTheodolite.MeasuresToDoCome;

                if (measure == null)
                {
                    AddNewPoint(AskForName: false);
                    measure = this.StationTheodolite.MeasuresToDoCome;
                }

                MovetoTop(measure);
                measure.DirectMeasurementWanted = true;
                SendMeasureToInstrument(measure);
                measure.DirectMeasurementWanted = false;
            }



            #endregion

            #region LiveData

            private void On_LiveDataStopped(object sender, EventArgs e)
            {
                if (this.stationParameters._Instrument is MID.AT40x.Instrument)
                {
                    MID.AT40x.Module m = this._InstrumentManager.SelectedInstrumentModule as MID.AT40x.Module;
                    if (m != null)
                    {
                        m.LiveData.LastResultAvailable -= OnLiveDataResultsAvailable;
                    }
                }
                else
                    throw new NotImplementedException();
            }

            public void On_LiveDataStarted(object sender, EventArgs e)
            {
                if (this.stationParameters._Instrument is MID.AT40x.Instrument)
                {
                    MID.AT40x.Module m = this._InstrumentManager.SelectedInstrumentModule as MID.AT40x.Module;
                    if (m != null)
                    {
                        m.LiveData.LastResultAvailable -= OnLiveDataResultsAvailable;
                        m.LiveData.LastResultAvailable += OnLiveDataResultsAvailable;
                    }
                }
                else
                    throw new NotImplementedException();
            }
            private void OnLiveDataResultsAvailable(object sender, MID.AT40x.LiveData.LiveDataEventArgs e)
            {
                if (this.View.PinTableView.isVisible)
                {
                    this.View.PinTableView.GetLiveDataCoordinates(e.Measure);
                }
            }

            internal void RollComputeWithFrame()
            {
                // select 2 measured alesage
                var measureManager = this._MeasureManager;
                measureManager.Select();
                var selected = measureManager._SelectedObjects;

                var points = new List<Point>();
                foreach (var obj in selected)
                {
                    if (obj is M.Measure measure)
                    {
                        points.Add(measure._OriginalPoint);
                    }
                }
                // check cumul are similar?


            }

            internal void Reflector_ManagedByAt(Measure usedMeasure)
            {
                Reflector(usedMeasure, new Reflector() { _Name = R.T_MANAGED_BY_AT40X });
            }

            /// <summary>
            /// ask the view to show interface choices and then trigger or not the interface cahnge in the module
            /// </summary>
            /// <param name="measure"></param>
            internal void ChangeInterfaces(Measure usedMeasure)
            {
                this.View.SelectInterfaces(
                    previousInterFaces: usedMeasure.Interfaces,
                    currenObject: usedMeasure,
                    model: null,
                    fan: null,
                    additionalAction: UpdateView);
            }

            /// <summary>
            /// Replace the interface in a measure
            /// </summary>
            /// <param name="currenObject"></param>
            /// <param name="newInterFaces"></param>
            internal override void ChangeInterfacesBy(TsuObject currenObject, Interfaces newInterFaces)
            {
                var measure = currenObject as Polar.Measure;
                measure.Interfaces = newInterFaces;

                if (measure == this.stationParameters.DefaultMeasure)
                {
                    if (this.View.AskToApplyNewDefautParameters())
                    {
                        foreach (var item in this.StationTheodolite.MeasuresToDo)
                        {
                            item.Interfaces = newInterFaces;
                        }
                    }
                }
                measure.Interfaces = newInterFaces;
            }

            /// <summary>
            /// Here we want to replace the point and original point by the first ones of the station
            /// </summary>
            /// <param name="usedMeasure"></param>
            internal bool Control_ReplacePointsByOriginal(Measure measure)
            {
                // Here we want to replace the point and original point by the first ones of the station
                var pointName = measure._Point._Name;
                var station = this.StationTheodolite;
                int count = 0;
                foreach (var item in station.MeasuresTaken)
                {
                    count++;
                    if (item._Point._Name == pointName && item._Status.Type != M.States.Types.Bad)
                    {
                        measure.PointGUID = item.PointGUID;
                        measure.OriginalPointGUID = item.OriginalPointGUID;
                        new MessageInput(MessageType.Warning, $"{R.T_NOT_USING_FIRST_POINT_OF_THE_STATION} {count}") { AsNotification = true }.Show();
                        return true;
                    }
                }
                new MessageInput(MessageType.Critical, $"{R.T_CAN_CONTROL_ONLY_POINT_MEASURED}").Show();
                return false;
            }

            #endregion
        }
    }
}

