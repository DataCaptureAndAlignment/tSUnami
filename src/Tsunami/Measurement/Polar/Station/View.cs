﻿using Castle.Core.Smtp;
using LibVLCSharp.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSU.Common;
using TSU.Common.Analysis;
using TSU.Common.Compute.CsvImporter;
using TSU.Common.Elements;
using TSU.ENUM;
using TSU.Tools;
using TSU.Views;
using TSU.Views.Message;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TrackBar;
using static TSU.Views.Message.MessageTsu;
using E = TSU.Common.Elements;
using Elements = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using P = TSU.Preferences;
using R = TSU.Properties.Resources;
using SO = TSU.Polar.GuidedModules.Steps.StakeOut;
using T = TSU.Tools;
using V = TSU.Common.Strategies.Views;

namespace TSU.Polar
{

    public partial class Station
    {
        public partial class View : Common.Station.View
        {
            #region Fields
            public new TSU.Polar.Station.Module Module
            {
                get
                {
                    return this._Module as TSU.Polar.Station.Module;
                }
            }


            private DoubleValue _PreviousRallonge;
            public Polar.Measure usedMeasure;
            internal new Buttons buttons;



            #endregion

            #region Constructor
            public View()
                : base()
            {

            }
            public Station.PinTableView PinTableView;
            public View(TSU.Polar.Station.Module modelModule)
                : base(modelModule, Orientation.Vertical, ModuleType.StationTheodolite)
            {
                isInitializaling = true;
                Text = modelModule._Name;
                icon = R.Theodolite;

                SplitPanelAndAddInstrumentView();
                PinTableView = new Station.PinTableView(this.Module);

                InitializeMenu();
                InitializeTreeView();
                PinTableView.InitializeDataGridView();
                ShowDockedFill();
                //ShowInstrumentSelectionMenu();
                ShowTreeViewAndInstrumentView();
                _PreviousRallonge = new DoubleValue(0, 0);
                buttons.IterationVersusTime.Available = false;
                Module.ControlDuringSetupHappened += OnControlDuringSetupHappened;

                var position = Tsunami2.Preferences.Values.GuiPrefs.PinnedPointsViewPosition;

                var a = splitContainerStationVsInstrument;
                
                int distance = Tsunami2.Preferences.Values.GuiPrefs.InstrumentSplitDistance;

                if (Macro.IsPlaying || Macro.IsRecording ||  Debug.IsRunningInATest)
                    distance -= 50;

                a.SplitterDistance = distance;
                a.Orientation = Tsunami2.Preferences.Values.GuiPrefs.InstrumentSplitOrientation;

                isInitializaling = false;
            }


            private void OnControlDuringSetupHappened(object sender, StationEventArgs e)
            {
                var dsaFlag = DsaFlag.GetByNameOrAdd(Module.DsaFlags, "DoStationSetupAfterClosure");
                string showed_valued = new MessageInput(MessageType.Choice, R.T_DO_YOU_WANT_TO_START_THE_STATION_SETUP_COMPENSATION)
                {
                    ButtonTexts = CreationHelper.GetYesNoButtons(),
                    DontShowAgain = dsaFlag
                }.Show().TextOfButtonClicked;
                if (R.T_YES == showed_valued)
                    Compute();
            }

            private void ImportCSVFile()
            {
                Console.WriteLine("Polar Module Import CSV FIle");
                string openedFile = CsvImporter.ImportRabotCSV(this.Module.FinalModule);

                //update button text
                var b = buttons.ImportCSV;

                if (openedFile != "")
                {
                    string newText = $"{b.Title};";
                    newText += b.Description.Contains("csv:") ? b.Description + "\r\n" : "";
                    if (!newText.Contains(openedFile)) 
                    {
                        newText += openedFile;
                        b.ChangeNameAndDescription(newText);
                    }

                }
                Console.WriteLine("Rabot dictionary size:");

                if (this.Module.FinalModule.RadialRabot.Count > 0 || this.Module.FinalModule.VerticalRabot.Count > 0)
                    PinTableView.UpdateAllTable();
            }
            private string GetName(bool askWithMessage = false)
            {
                string stringByDefault;
                if (Module.ParentModule != null)
                    stringByDefault = T.Research.FindNextName("Station_", Module.ParentModule.childModules.Cast<TsuObject>().ToList());
                else
                    stringByDefault = "Station_001";

                if (askWithMessage)
                {
                    if (Module._Station._Name == R.String_Unknown)
                    {
                        string cancel = R.T_CANCEL;

                        List<string> buttonTexts = new List<string> { R.T_OK, cancel };

                        MessageTsu.ShowMessageWithTextBox(R.T160, buttonTexts, out string buttonClicked, out string textInput, stringByDefault);

                        if (buttonClicked != cancel)
                            return textInput;
                        else
                            return R.String_Unknown;
                    }
                    else
                        return Module._Station._Name;
                }
                return stringByDefault;
            }

            bool firstSplitPanelDistanceChange = true;
            private void SplitPanelAndAddInstrumentView()
            {
                var position = Tsunami2.Preferences.Values.GuiPrefs.PinnedPointsViewPosition;
                var orientation = Tsunami2.Preferences.Values.GuiPrefs.InstrumentSplitOrientation;


                var split = new SplitContainer() { Dock = DockStyle.Fill, Orientation = orientation };

                Tsunami2.Preferences.Theme.ApplyTo(split);

                ToolTip.SetToolTip(split, R.T_DOUBLE_CLICK_TO_SWITCH_BETWEEN_VERTICAL_AND_HORIZONTAL_SPLITTING);

                split.DoubleClick += delegate
                {
                    // change main splitcontainer
                    orientation = this.SetSplitContainerOrientation(split.Orientation, invert: true);
                    Tsunami2.Preferences.Values.SaveGuiPrefs();
                };

                _PanelBottom.Controls.Add(split);

                this.splitContainerStationVsInstrument = split;
                bool shouldSaveNewValue = false;

                split.SplitterMoving += delegate
                {
                    shouldSaveNewValue = true;
                };
                    
                split.SplitterMoved += delegate
                {
                    if (shouldSaveNewValue)
                    {
                        Tsunami2.Preferences.Values.GuiPrefs.InstrumentSplitDistance = split.SplitterDistance;
                        Tsunami2.Preferences.Values.GuiPrefs.InstrumentSplitOrientation = split.Orientation;

                        Tsunami2.Preferences.Values.SaveGuiPrefs();
                    }
                    shouldSaveNewValue = false;
                };

                ChangeInstrumentViewToEmpty();
            }
            #endregion



            #region Treeview
            private TreeView treeView;
            private Label seperatorLine;
            private TreeNode selectedNode;
            private TreeView ghostTreeView;
            public void InitializeTreeView()
            {
                this.treeView = new TreeView() { Dock = DockStyle.Fill, ItemHeight = 32, Font = Tsunami2.Preferences.Theme.Fonts.Normal };

                this.treeView.BackColor = Tsunami2.Preferences.Theme.Colors.LightBackground;

                ghostTreeView = new TreeView();
                treeView.ImageList = TreeNodeImages.ImageListInstance;
                treeView.SelectedImageKey = "Selected";
                treeView.ShowNodeToolTips = true;
                treeView.NodeMouseClick += new TreeNodeMouseClickEventHandler(treeview1_OnClick);
                treeView.DragDrop += new DragEventHandler(treeView1_DragDrop);
                treeView.ItemDrag += new ItemDragEventHandler(treeView1_ItemDrag);
                treeView.DragEnter += new DragEventHandler(treeView1_DragEnter);
                treeView.DragOver += new DragEventHandler(treeView1_DragOver);
                treeView.KeyUp += new KeyEventHandler(treeView1_KeyUp);
                treeView.AllowDrop = true;
                treeView.GotFocus += delegate
                {
                    Debug.WriteInConsole("Treeview got focus");
                };
                treeView.LostFocus += delegate
                {
                    Debug.WriteInConsole("Treeview lost focus");
                };
                treeView.Nodes.Add(new TsuNode());
                treeView.Nodes.Add(new TsuNode());
                treeView.Nodes.Add(new TsuNode());
                SaveTreeViewLook();
                treeView.ExpandAll();

                // init the line separator
                seperatorLine = new Label() { Text = "", Height = 2, Width = 100, BorderStyle = BorderStyle.Fixed3D, Visible = false };
            }

            private void treeView1_KeyUp(object sender, KeyEventArgs e)
            {
                if (sender is TreeView tv)
                {
                    if (e.KeyCode == Keys.Right)
                    {
                        if (tv.SelectedNode.Nodes.Count == 0)
                            ShowContextMenuOfTheNode((TsuNode)tv.SelectedNode);
                    }
                    else if (e.KeyCode == Keys.Apps)
                        ShowContextMenuOfTheNode((TsuNode)tv.SelectedNode);
                }
            }

            public void ShowTreeViewAndInstrumentView()
            {
                UpdateView();

                var split = this.splitContainerStationVsInstrument;

                var sv = split.Panel1;
                var iv = split.Panel2;
                sv.Controls.Clear();
                if (PinTableView.isVisible)
                {
                    var splitContainreWithTable = PinTableView.splitContainerWithPinTableInPanel2;
                    if (splitContainreWithTable == null)
                        splitContainreWithTable = PinTableView.SplitPanel_GetOrCreate();

                    sv.Controls.Add(splitContainreWithTable);
                    splitContainreWithTable.Panel1.Controls.Add(treeView);
                    splitContainreWithTable.Panel1.Controls.Add(seperatorLine);
                }
                else
                {
                    iv.Controls.Clear();
                    sv.Controls.Add(treeView);
                    sv.Controls.Add(seperatorLine);
                    instrumentView.TopLevel = false;
                    instrumentView.Dock = DockStyle.Fill;
                    instrumentView.Visible = true;
                    instrumentView.BringToFront();
                    iv.Controls.Add(instrumentView);
                    instrumentView.Visible = true;
                    instrumentView.BringToFront();
                }
            }
            #region Dragging
            private void treeView1_ItemDrag(object sender, ItemDragEventArgs e)
            {
                DoDragDrop(e.Item, DragDropEffects.Move);
            }
            private void treeView1_DragEnter(object sender, DragEventArgs e)
            {
                seperatorLine.Visible = true;
                seperatorLine.BringToFront();
                e.Effect = DragDropEffects.Move;
            }

            private void treeView1_DragOver(object sender, DragEventArgs e)
            {
                // Retrieve the client coordinates of the drop location.
                System.Drawing.Point targetPoint = treeView.PointToClient(new System.Drawing.Point(e.X, e.Y));

                // Retrieve the node at the drop location.
                TreeNode targetNode = treeView.GetNodeAt(targetPoint);

                // Retrieve the node that was dragged.
                TreeNode draggedNode = (TsuNode)e.Data.GetData(typeof(TsuNode));

                if (targetNode != null && draggedNode != null)
                {
                    seperatorLine.Height = 3;
                    seperatorLine.Left = targetNode.Bounds.Left;
                    seperatorLine.Width = targetNode.Bounds.Width;


                    if (targetNode.Bounds.Top > draggedNode.Bounds.Top)
                        seperatorLine.Top = targetNode.Bounds.Top + targetNode.Bounds.Height;
                    else
                        seperatorLine.Top = targetNode.Bounds.Top;
                }
            }

            private void treeView1_DragDrop(object sender, DragEventArgs e)
            {
                try
                {
                    // hide the blakc line
                    seperatorLine.Visible = false;

                    // Retrieve the client coordinates of the drop location.
                    System.Drawing.Point targetPoint = treeView.PointToClient(new System.Drawing.Point(e.X, e.Y));

                    // Retrieve the node at the drop location.
                    TreeNode targetNode = treeView.GetNodeAt(targetPoint);

                    // Retrieve the node that was dragged.
                    TreeNode draggedNode = (TsuNode)e.Data.GetData(typeof(TsuNode));

                    if (targetNode == null || draggedNode == null) return;

                    if (targetNode.Parent == draggedNode.Parent && draggedNode.Parent != null) // si on la lache dans la meme root
                    {
                        // Confirm that the node at the drop location is not 
                        // the dragged node and that target node isn't null
                        // (for example if you drag outside the control)
                        if (!draggedNode.Equals(targetNode) && targetNode != null)
                        {
                            // Remove the node from its current 
                            // location and add it to the node at the drop location.

                            if (draggedNode.Tag is Polar.Measure)
                            {
                                M.IMeasure m = targetNode.Tag as Polar.Measure;
                                int index = Module.StationTheodolite.MeasuresToDo.IndexOf(m as Polar.Measure);
                                Module.StationTheodolite.MeasuresToDo.Remove(draggedNode.Tag as Polar.Measure);
                                Module.StationTheodolite.MeasuresToDo.Insert(index, draggedNode.Tag as Polar.Measure);
                            }
                            draggedNode.Remove();
                            int index2 = targetNode.Parent.Nodes.IndexOf(targetNode);
                            targetNode.Parent.Nodes.Insert(index2, draggedNode);

                            UpdateViewOfNextPoint();
                            Module.SendNextMeasureToInstrument();

                            // Expand the node at the location 
                            // to show the dropped node.
                            //targetNode.Expand();
                        }
                    }
                }
                catch (Exception)
                {

                }
            }
            #endregion

            #region Treeview Expansion and collapsing
            public void ApplyPreviousTreeViewLook()
            {
                for (int i = 0; i < treeView.Nodes.Count; i++)
                {
                    ApplyPreviousTreeNodeLook(treeView.Nodes[i], ghostTreeView.Nodes[i]);
                }
            }
            public void ApplyPreviousTreeNodeLook(TreeNode node, TreeNode ghostNode)
            {
                //if (node.Nodes.Count > 0)
                if (node.Nodes.Count == ghostNode.Nodes.Count)
                {
                    for (int i = 0; i < node.Nodes.Count; i++)
                        ApplyPreviousTreeNodeLook(node.Nodes[i], ghostNode.Nodes[i]);
                }
                if (ghostNode.IsExpanded) node.Expand();
                if ((bool)(ghostNode.Tag))
                {
                    treeView.SelectedNode = node;
                }
            }
            private void SaveTreeViewLook()
            {
                ghostTreeView.Nodes.Clear();
                foreach (TreeNode item in treeView.Nodes)
                    ghostTreeView.Nodes.Add(SaveTreeNodeLook(item));
            }
            private TreeNode SaveTreeNodeLook(TreeNode realNode)
            {
                TreeNode ghostNode = new TreeNode();
                foreach (TreeNode item in realNode.Nodes)
                    ghostNode.Nodes.Add(SaveTreeNodeLook(item));
                if (realNode.IsExpanded) ghostNode.Expand(); else ghostNode.Collapse();
                if (realNode == selectedNode)
                {
                    ghostNode.Tag = true;
                }
                else
                {
                    ghostNode.Tag = false;
                }
                ghostNode.Text = realNode.Text;
                return ghostNode;
            }

            #endregion

            #endregion

            #region  Menus

            private void InitializeMenu()
            {
                //Creation of buttons
                buttons = new Buttons(this);

                AddTopButton(string.Format(R.T161, _Module._Name),
                        icon, ShowContextMenuOfModule);
            }

            private void CreateContextMenuBasedOn(StationProperties p)
            {
                bool fullySetup = base.Module._Station.ParametersBasic._IsSetup;
                bool showLgc = fullySetup || (Debug.Debugging && ((Station)base.Module._Station).Parameters2.Setups.BestValues != null);
                switch (p)
                {
                    //
                    //station
                    //
                    case StationProperties.All:
                        contextButtons.Add(buttons.ShowMissingInfo);
                        contextButtons.Add(buttons.ChangeState);
                        break;
                    case StationProperties.dHStakeOut:
                        contextButtons.Add(buttons.StakeOut);
                        break;
                    case StationProperties.Admin:
                        CreateContextMenuBasedOn(StationProperties.Date);
                        CreateContextMenuBasedOn(StationProperties.Team);
                        CreateContextMenuBasedOn(StationProperties.Operation);
                        AddMoreOrLessButtonToContextMenu();
                        break;
                    case StationProperties.Tolerance:
                        AddMoreOrLessButtonToContextMenu();
                        break;
                    case StationProperties.Date: break;
                    case StationProperties.Team:
                        contextButtons.Add(buttons.Team); break;
                    case StationProperties.Operation:
                        contextButtons.Add(buttons.Operation);
                        contextButtons.Add(buttons.OperationNew);
                        break;
                    case StationProperties.Instrument:
                        contextButtons.Add(buttons.Instrument);
                        Module._InstrumentManager.AddButtonForLastInstrument(contextButtons, base.Module, I.InstrumentClasses.POLAR);
                        Module._InstrumentManager.AddButtonForSsidInstrument(contextButtons, base.Module, I.InstrumentClasses.POLAR);
                        Module._InstrumentManager.AddButtonToScanForSsidInstrument(contextButtons, base.Module, I.InstrumentClasses.POLAR);

                        break;
                    case StationProperties.TheoFile:
                        contextButtons.Add(buttons.TheoFile);
                        contextButtons.Add(buttons.ImportCSV);
                        break;
                    case StationProperties.NextDefault:
                        contextButtons.Add(buttons.Reflector);
                        contextButtons.Add(buttons.Extension);
                        contextButtons.Add(buttons.Face1);
                        contextButtons.Add(buttons.DoubleFace);
                        contextButtons.Add(buttons.QuickMeasure);
                        AddMoreOrLessButtonToContextMenu();
                        break;
                    case StationProperties.DefaultPointName:
                        contextButtons.Add(buttons.RenamePoint);
                        contextButtons.Add(buttons.TimeVersusIteration);
                        contextButtons.Add(buttons.IterationVersusTime);
                        break;

                    case StationProperties.Theo_Sigmas:
                        contextButtons.Add(buttons.ChangeSigmaValuesForPreset);
                        break;
                    case StationProperties.Theo_Sigma:
                        contextButtons.Add(buttons.ChangeSigmaValues);
                        break;
                    case StationProperties.Theo_Tolerances:
                        contextButtons.Add(buttons.ChangeToleranceValues);
                        break;
                    case StationProperties.DefaultPointCode:
                        contextButtons.Add(buttons.DefaultCode);
                        contextButtons.Add(buttons.NoDefaultCode);
                        break;
                    case StationProperties.SocketCode:
                        contextButtons.Add(buttons.DefaultCode);
                        break;
                    case StationProperties.Interfaces:
                        contextButtons.AddRange(GetInterfacesButtons(usedMeasure.Interfaces, usedMeasure, null, null, additionalAction: () => { UpdateView(); }));
                        break;
                    case StationProperties.Point:
                        contextButtons.Add(buttons.KnownPosition);
                        contextButtons.Add(buttons.NewPosition);
                        break;
                    case StationProperties.InstrumentHeight:
                        if (!fullySetup) contextButtons.Add(buttons.Height0);
                        if (!fullySetup) contextButtons.Add(buttons.HeightUnKnown);
                        if (!fullySetup) contextButtons.Add(buttons.Height);

                        break;
                    case StationProperties.NumberOfMeasurement:
                        contextButtons.Add(buttons.Number);
                        break;
                    case StationProperties.Orientation:
                        if (!fullySetup) contextButtons.Add(buttons.Compute);
                        if (!fullySetup) contextButtons.Add(buttons.SetVZero);
                        if (fullySetup) contextButtons.Add(buttons.ForgetCompute);
                        if (showLgc) contextButtons.Add(buttons.ShowComputeLgcFiles);
                        break;
                    case StationProperties.Setup:
                        if (!this.Module.StationTheodolite.Parameters2.Setups.IsAFreeStation)
                            if (!fullySetup) contextButtons.Add(buttons.FreeStation);
                        if (!fullySetup) contextButtons.Add(buttons.Compute);
                        if (this.Module.StationTheodolite.Parameters2.Setups.IsAFreeStation)
                            if (!fullySetup) contextButtons.Add(buttons.FreeStation);
                        if (fullySetup) contextButtons.Add(buttons.ForgetCompute);
                        if (showLgc) contextButtons.Add(buttons.ShowComputeLgcFiles);

                        AddMoreOrLessButtonToContextMenu();
                        break;

                    //
                    // Management.Measure
                    //
                    case StationProperties.Measures:
                        contextButtons.Add(buttons.MeasureVisualOption);
                        contextButtons.Add(PinTableView.Buttons.PinAllPointsMeasured);
                        contextButtons.Add(PinTableView.Buttons.ShowPinnedTable);
                        contextButtons.Add(buttons.MeasureDetails);
                        if (Common.Analysis.Measure.GetFaceType(Module.StationTheodolite.MeasuresTaken) == I.FaceType.Face1)
                            contextButtons.Add(buttons.ControlFace2);
                        contextButtons.Add(buttons.ReMeasure);
                        AddMoreOrLessButtonToContextMenu();
                        break;
                    case StationProperties.Measure:
                        contextButtons.Add(buttons.SelectedReMeasureWithDifferentName);
                        contextButtons.Add(buttons.SelectedReMeasure);
                        contextButtons.Add(buttons.GotoThatPoint);
                        contextButtons.Add(buttons.ChangeComment);
                        contextButtons.Add(buttons.SelectedMeasureDetails);
                        contextButtons.Add(buttons.SelectPointsToRecompute);
                        contextButtons.Add(PinTableView.Buttons.SelectedAddToPinnedTable);
                        break;
                    //
                    // Point
                    //
                    case StationProperties.Verticalisation:
                        if (!fullySetup) contextButtons.Add(buttons.Verticalised);
                        if (!fullySetup) contextButtons.Add(buttons.NotVerticalised);
                        break;
                    case StationProperties.NextPoints:
                        contextButtons.Add(buttons.SelectPointsToMeasure);
                        contextButtons.Add(buttons.AddNewPoint);
                        contextButtons.Add(buttons.AddNewPointWithCustomName);
                        if (Module.stationParameters._IsSetup) contextButtons.Add(buttons.StakeOutMenu);
                        contextButtons.Add(buttons.ControlOpening);
                        contextButtons.Add(buttons.SequenceMenu);
                        contextButtons.Add(buttons.RemoveAll);
                        // contextButtons.Add(buttons.EditNextPointsList); // not working yet
                        contextButtons.Add(PinTableView.Buttons.PinAllPointsNext);
                        AddMoreOrLessButtonToContextMenu();
                        break;
                    case StationProperties.NextPoint:
                        contextButtons.Add(buttons.RenamePoint);
                        contextButtons.Add(buttons.RemoveNext);
                        contextButtons.Add(buttons.MoveAndMeasure);
                        contextButtons.Add(PinTableView.Buttons.PinSingleNextPoint);
                        if (Module.stationParameters._IsSetup) contextButtons.Add(buttons.StakeOut);
                        contextButtons.Add(buttons.MoveTop);
                        contextButtons.Add(buttons.MoveUp);
                        contextButtons.Add(buttons.MoveDown);
                        AddMoreOrLessButtonToContextMenu();
                        break;
                    case StationProperties.Extension:
                        contextButtons.Add(buttons.Extension);
                        contextButtons.Add(buttons.ExtensionFromCode);
                        break;
                    case StationProperties.NextReflector:
                        contextButtons.Add(buttons.Reflector);
                        contextButtons.Add(buttons.Reflector);
                        if (this.Module?._InstrumentManager?.SelectedInstrument?._Model.ToUpper().Contains("AT40") == true)
                            contextButtons.Add(buttons.ReflectorManagedByAt40x);
                        break;
                    case StationProperties.NextComment:
                        contextButtons.Add(buttons.ChangeComment); break;
                    case StationProperties.Status:
                        contextButtons.Add(buttons.StatusGood);
                        contextButtons.Add(buttons.StatusUnknown);
                        contextButtons.Add(buttons.StatusQuestionnable);
                        contextButtons.Add(buttons.StatusControl);
                        break;
                    case StationProperties.NextFace:
                        contextButtons.Add(buttons.DoubleFace);
                        contextButtons.Add(buttons.Face1);
                        contextButtons.Add(buttons.Face2);
                        break;
                    default: break;
                }
            }

            private void AddMoreOrLessButtonToContextMenu()
            {
                if (selectedNode.Nodes.Count == 0)
                    return;
                if (selectedNode.IsExpanded)
                    contextButtons.Add(buttons.Less);
                else
                    contextButtons.Add(buttons.More);
            }



            private void ShowContextMenuOfModule()
            {
                contextButtons.Clear();
                contextButtons.Add(buttons.ElementDetails);
                contextButtons.Add(buttons.MeasureDetails);
                contextButtons.Add(buttons.ChangeState);
                contextButtons.Add(buttons.CloseStation);
                contextButtons.Add(buttons.ResetDontShowAgain);
                contextButtons.Add(buttons.PinTableOptions);
                contextButtons.Add(buttons.LinkExcelOpenedWorkbook);
                contextButtons.Add(buttons.UnLinkAnyExcelWorkbook);
                contextButtons.Add(buttons.LinkBatch);
                contextButtons.Add(buttons.UnLinkBatch);

                this.ShowPopUpMenu(contextButtons);
            }

            private void OpenStation()
            {
                Module.OpenFromXml();

            }

            private void SaveStation()
            {
                Module.SaveToXml();
            }

            internal bool WantToUseSequenceForStationName(string p)
            {
                string titleAndMessage = "Sequence;" + string.Format(R.T162, p);
                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                };
                return mi.Show().TextOfButtonClicked == R.T_YES;
            }

            protected override void OnGotFocus(EventArgs e)
            {
                Invalidate();
                base.OnGotFocus(e);
            }

            protected override void OnLostFocus(EventArgs e)
            {
                base.OnLostFocus(e);
                Invalidate();
            }


            #region Buttons
            internal new class Buttons : Common.Station.View.Buttons
            {
                private TSU.Polar.Station.View _view;
                public List<BigButton> all;

                public BigButton CloseStation;
                public BigButton ResetDontShowAgain;
                public BigButton Face1;
                public BigButton Face2;
                public BigButton DoubleFace;
                public BigButton QuickMeasure;
                public BigButton KnownPosition;
                public BigButton NewPosition;
                public BigButton FreeStation;
                public BigButton IterationVersusTime;
                public BigButton TimeVersusIteration;

                public BigButton Height;
                public BigButton Height0;
                public BigButton HeightUnKnown;

                public BigButton Compute;
                public BigButton ForgetCompute;
                public BigButton ShowComputeLgcFiles;
                public BigButton SetVZero;
                public BigButton ControlOpening;
                public BigButton ControlFace2;
                public BigButton ReMeasure;
                public BigButton GotoThatPoint;


                //
                // MeasureVisualOption
                //
                public BigButton MeasureVisualOption;
                public BigButton ShowMeasurePrecision;
                public BigButton ShowCSMap;
                public BigButton SelectedMeasureDetails;

                public BigButton SelectedReMeasure;
                public BigButton SelectedReMeasureWithDifferentName;

                public BigButton SelectPointsToMeasure;
                public BigButton AddNewPoint;
                public BigButton AddNewPointWithCustomName;
                public BigButton SelectPointsToStakeOut;
                public BigButton SelectPointsToRecompute;
                public BigButton RenamePoint;
                public BigButton RemoveNext;
                public BigButton RemoveAll;

                public BigButton Verticalised;
                public BigButton NotVerticalised;

                public BigButton MoveUp;
                public BigButton MoveDown;
                public BigButton MoveTop;
                public BigButton MoveAndMeasure;
                public BigButton EditNextPointsList;
                public BigButton StakeOutMenu;
                public BigButton StakeOutLine;
                public BigButton StakeOutKnownLine;
                public BigButton StakeOut;
                public BigButton StakeOutNextPoint;
                public BigButton StakeOutNextPoints;
                public BigButton Extension;
                public BigButton ExtensionFromCode;
                public BigButton Reflector;
                public BigButton ReflectorManagedByAt40x;
                public BigButton DefaultReflector;
                public BigButton DefaultCode;
                public BigButton Interfaces; // not used, there is a method in Station.View. to get interfaces buttons
                public BigButton NoDefaultCode;
                public BigButton ChangeComment;
                public BigButton StatusGood;
                public BigButton StatusQuestionnable;
                public BigButton StatusUnknown;
                public BigButton StatusControl;
                public BigButton Measure;


                public BigButton SequenceMenu;
                public BigButton CreateSequence;
                public BigButton SequenceSelectExisting;
                public BigButton SequenceSelectLast;
                public BigButton InvertSequence;
                public BigButton AutomatiqueSequence;
                public BigButton ReproduceAStation;

                public BigButton Close;

                public BigButton Number;
                public BigButton ShowMissingInfo;

                public BigButton ChangeSigmaValuesForPreset;
                public BigButton ChangeSigmaValues;
                public BigButton ChangeToleranceValues;

                public BigButton More;
                public BigButton Less;

                public BigButton ImportCSV;


                public BigButton PinTableOptions;
                public BigButton LinkExcelOpenedWorkbook;
                public BigButton UnLinkAnyExcelWorkbook;

                public BigButton LinkBatch;
                public BigButton UnLinkBatch;

                public Buttons(View view)
                    : base(view)
                {
                    _view = view;

                    all = new List<BigButton>();

                    CloseStation = new BigButton(R.T_CloseStation, R.Checked_box, _view.CloseStation); all.Add(CloseStation);
                    ResetDontShowAgain = new BigButton(R.T_RESET_DONT_SHOW, R.Update, _view.Module.ResetFlags); all.Add(ResetDontShowAgain);
                    string openFilesDescription = "";
                    if (this._view.Module.FinalModule.RabotFiles.Count > 0) openFilesDescription += string.Join("\n", this._view.Module.FinalModule.RabotFiles);
                    string importCSVDescription = openFilesDescription == "" ? R.T_WITH_EXPECTED_OFFSETS_OR_RELATIVE_DISPLACEMENT : openFilesDescription;
                    ImportCSV = new BigButton($"{R.T_IMPORT_A_CSV_FILE};{importCSVDescription}",
                       R.Open, _view.ImportCSVFile);

                    ShowMissingInfo = new BigButton(R.ShowMissingInfo, R.MessageTsu_Interrogation, _view.ExpandNodeWithMissingInfo); all.Add(ShowMissingInfo);

                    OpenStation = new BigButton(string.Format(R.T_OpenModule, R.T_A_STATION_MODULE), R.Open, _view.OpenStation); all.Add(OpenStation);
                    SaveStation = new BigButton(string.Format(R.T_SaveModule, _view.Module._Name), R.Save, _view.SaveStation);
                    Face1 = new BigButton(R.T169, R.Face, _view.ChangeToFace1); all.Add(Face1);
                    Face2 = new BigButton(R.T170, R.Face, _view.ChangeToFace2); all.Add(Face2);
                    DoubleFace = new BigButton(R.T171, R.Face, _view.ChangeToDoubleFace); all.Add(DoubleFace);
                    QuickMeasure = new BigButton("Quick measure", R.QuickMeasure, _view.ChangeToQuickMeasure); all.Add(QuickMeasure);
                    KnownPosition = new BigButton(R.T172, R.Element_PointKnown, _view.SetStationPointFromList); all.Add(KnownPosition);
                    NewPosition = new BigButton(R.T173, R.Element_PointUnKnown, _view.SetNewStationPoint); all.Add(NewPosition);
                    FreeStation = new BigButton($"{R.T_PREP_4_FREESTATION};{R.T_PREP_4_FREESTATION_DETAILS}", R.Element_PointUnKnown, _view.SetNewStationPoint); all.Add(FreeStation);
                    IterationVersusTime = new BigButton(R.T174, R.Iteration, _view.SetIterationVersusTime); all.Add(IterationVersusTime);
                    TimeVersusIteration = new BigButton(R.T175, R.Time, _view.SetTimeVersusIteration); all.Add(TimeVersusIteration);

                    Height = new BigButton(R.T176, R.InstrumentHeightKnown, () => { _view.AskInstrumentHeightWithCode(); }); all.Add(Height);
                    Height0 = new BigButton(R.T_BUTTON_Hto0, R.InstrumentHeight0, _view.ChangeInstrumentHeightTo0); all.Add(Height0);
                    HeightUnKnown = new BigButton(R.T177, R.InstrumentHeight_Unknown, _view.SetInstrumentHeightToUnkown); all.Add(HeightUnKnown);

                    Number = new BigButton(R.T_B_NUMBER_MEAS, R.I_Number, () => { _view.AskAndChangeNumberOfMeasurement(); }); all.Add(Number);
                    //Compute = new BigButton(R.T_B_COMPUTE_LGC1, R.Compute, _view.Compute);
                    Compute = new BigButton(R.T_B_COMPUTE_LGC2, R.Compute, _view.Compute); all.Add(Compute);
                    ForgetCompute = new BigButton(R.T_Cancel_Setup, R.MessageTsu_Problem, _view.ForgetCompute); all.Add(ForgetCompute);
                    SetVZero = new BigButton(R.T_B_SET_V0, R.Compute, _view.SetV0Manually); all.Add(SetVZero);
                    ShowComputeLgcFiles = new BigButton($"{R.T_LGC_FILES}", R.Element_File, _view.ShowInputAndOutput); all.Add(ShowComputeLgcFiles);
                    Close = new BigButton(R.T_CloseStation, R.Compute, _view.CloseStation); all.Add(Close);

                    ControlOpening = new BigButton(R.T182, R.Face, _view.ControlFermeture); all.Add(ControlOpening);
                    ControlFace2 = new BigButton(R.T183, R.Face, _view.ControlWithFace2); all.Add(ControlFace2);
                    ReMeasure = new BigButton(R.T184, R.Update, _view.MeasureEverythingAgain); all.Add(ReMeasure);
                    GotoThatPoint = new BigButton($"{R.T_GOTO};{R.T_USE_THE_OBSERVATIO}", R.At40x_Goto, _view.GotoThatMeasure); all.Add(GotoThatPoint);


                    { // MeasureVisualOption
                        MeasureVisualOption = new BigButton($"{R.T_CHANGE_VISUAL_DETAILS};{R.T_SWITCH_WHAT_YOU_SEE_IN_THE_PREVIOUS_OF_THE_MEASUREMENT}", R.View,
                            _view.ShowMeasureVisualDetails,
                                color: null, hasSubButtons: true); all.Add(MeasureVisualOption);
                        ShowMeasurePrecision = new BigButton($"{R.T_SHOW_TYPE_AND_PRECISION};{R.T_SEE_NUMBER_OF_FACE_AND_SIGMAS}", R.Measurement, _view.ShowMeasurePrecision); all.Add(ShowMeasurePrecision);
                        ShowCSMap = new BigButton($"{R.T_COORDINATES_SYSTEM_MAP};{R.T_SEE_GRAPHICAL_REPRESENTATION_OF_COORDINATES_SYSTEMS}", R.CS_Systems, _view.ShowCSMap); all.Add(ShowCSMap);
                    }
                    EditNextPointsList = new BigButton(R.NpEdit, R.Tsunami, _view.Module.EditPointList); all.Add(EditNextPointsList);
                    SelectedMeasureDetails = new BigButton(R.T185, R.Measurement, _view.ShowSelectedMeasure); all.Add(SelectedMeasureDetails);

                    SelectedReMeasure = new BigButton(R.T184, R.At40x_Loop, _view.MeasureSelectedAgain); all.Add(SelectedReMeasure);
                    SelectedReMeasureWithDifferentName = new BigButton(R.RemeasDiffName, R.Update, _view.MeasureSelectedAgainWithDifferentName); all.Add(SelectedReMeasureWithDifferentName);
                    SelectPointsToMeasure = new BigButton(R.T_SELECT_POINTS_TO_MEAS, R.Element_PointKnown, _view.SetNextPoints); all.Add(SelectPointsToMeasure);
                    SelectPointsToRecompute = new BigButton($"{R.T_RECOMPUTE};{R.T_CALCUL_ONE_MORE_TIME_THE_COORDINATES_OF_THIS_POINT_BASED_ON_THIS_MEASURE}", R.Compute, _view.Recompute); all.Add(SelectPointsToRecompute);
                    AddNewPoint = new BigButton($"{R.T_ADD_NEW_POINT};{R.T_IT_WILL_BASED_ON_THE_DEFAULT_POINT_DEFINED_IN_THE_STATION_PARAMETERS_ABOVE}",
                        R.Element_Point, () => { _view.Module.AddNewPoint(AskForName: false); }); all.Add(AddNewPoint);
                    AddNewPointWithCustomName = new BigButton(R.NewPointCustom, R.Element_Point,
                        () => { _view.Module.AddNewPoint(AskForName: true); }); all.Add(AddNewPointWithCustomName);
                    SelectPointsToStakeOut = new BigButton(R.T_SELECT_POINTS_TO_STAKEOUT, R.Offset, _view.SetNextPointsForStakeOut); all.Add(SelectPointsToStakeOut);
                    RenamePoint = new BigButton(R.T188, R.Element_Point, () =>
                    {
                        TsuOptionalShowing.Type os = TsuOptionalShowing.Type.DontShowDontShowAgainBox;
                        _view.RenamePoint(ref os);
                    });

                    RemoveNext = new BigButton(R.T_ATM_RemovePoint, R.MessageTsu_Problem, _view.RemoveNextMeasure); all.Add(RemoveNext);
                    RemoveAll = new BigButton(R.T_ATM_RemoveAllPoints, R.MessageTsu_Problem, _view.RemoveAllMeasures); all.Add(RemoveAll);

                    Verticalised = new BigButton(R.T_Button_Verticalised, R.MessageTsu_OK, _view.SetToVerticalised); all.Add(Verticalised);
                    NotVerticalised = new BigButton(R.T_Button_NOT_Verticalised, R.MessageTsu_Problem, _view.SetToNotVerticalised); all.Add(NotVerticalised);

                    MoveUp = new BigButton(R.T190, R.Up, _view.MoveNextPointDown); all.Add(MoveUp);
                    MoveTop = new BigButton(R.T_B_MOVETOP, R.Up, _view.MoveNextPointToTop); all.Add(MoveTop);
                    MoveAndMeasure = new BigButton(R.MeasureNow, R.Up, _view.MeasureNow); all.Add(MoveAndMeasure);
                    MoveDown = new BigButton(R.T191, R.Down, _view.MoveNextPointUp); all.Add(MoveDown);

                    StakeOutMenu = new BigButton($"{R.T_STAKEOUT_MENU};{R.T_SELECT_POINTS_TO_STAKEOUT_CREATE_A_LINE}", R.Element_PointUnKnown,
                        () => { _view.ShowPopUpSubMenu(new List<Control>() { SelectPointsToStakeOut, StakeOutNextPoints, StakeOutLine, StakeOutKnownLine }, "Stakeout"); },
                            color: null, hasSubButtons: true); all.Add(StakeOutMenu);

                    StakeOutLine = new BigButton($"{R.T_CREATE_A_LINE};{R.T_WILL_USE_ALL_THE_POINTS_FROM_THE_NEXT_POINTS_LIST_TO_FIT_A_3D_LINE_AND_WILL_CREATE_A_POINT_THAT_CONTAIN_THE_RESULTING_BEARING_AND_SLOPE}", R.Line, _view.StakeOutLine); all.Add(StakeOutLine);
                    StakeOutKnownLine = new BigButton($"{R.T_EXISTING_LINE};{R.T_STAKE_OUT} {R.T_EXISTING_LINE}", R.Line, _view.StakeOutKnownLine); all.Add(StakeOutKnownLine);

                    StakeOut = new BigButton(R.T_B_STAKEOUT, R.Element_Point, _view.ChangeStatusToStakeut); all.Add(StakeOut);
                    StakeOutNextPoint = new BigButton(R.T_B_STAKEOUT_NEXT_POINT, R.Offset, _view.ChangeStatusToStakeut); all.Add(StakeOutNextPoint);
                    StakeOutNextPoints = new BigButton(R.T_B_StakeOutAllPoints, R.Offset, _view.ChangeAllStatusToStakeut); all.Add(StakeOutNextPoints);


                    Extension = new BigButton(R.T192, R.Rallonge, () => { _view.ChangeExtension(null); }); all.Add(Extension);
                    ExtensionFromCode = new BigButton($"{R.T_GIVEN_BY_POINT_CODE};{R.T_USE_EXTENSIONS_DEFINED_BY_THE_DEFAULT_CODE}", R.Rallonge, () => { _view.ChangeExtensionToCode(); }); all.Add(ExtensionFromCode);

                    DefaultReflector = new BigButton(R.T193, R.ButtonPictureTC2002_Reflector, () => _view.Reflector_Change()); all.Add(DefaultReflector);

                    DefaultCode = new BigButton($"{R.T_CHANGE_SOCKET_CODE};{R.T_THE_CODE_IS_WHAT_DEFINE_THE_VERTICAL_EXTENSION_THAT_TSUNAMI_WILL_APPLY_BY_DEFAULT}", R.Element_Code, _view.ChangeCode_try); all.Add(DefaultCode);
                    NoDefaultCode = new BigButton($"{R.T_CHANGE_SOCKET_CODE} to undefined;{"The code will be ask for each point"}", R.Element_Code, _view.ChangeCode_ToNo); all.Add(NoDefaultCode);

                    SequenceMenu = new BigButton($"{R.T_SEQUENCE};{R.T_SEQUENCE_MENU}", R.Element_Sequence,
                        () => { _view.ShowPopUpSubMenu(new List<Control>() { CreateSequence, SequenceSelectExisting, SequenceSelectLast, InvertSequence, ReproduceAStation, AutomatiqueSequence }, "sequence "); },
                            color: null, hasSubButtons: true); all.Add(SequenceMenu);

                    SequenceSelectLast = new BigButton($"{R.T_REMEASURE_LAST_SEQUENCE};{R.T_REMEASURE_EXISTING}", R.Element_Sequence_Select, _view.ReMeasureLastSequence); all.Add(SequenceSelectLast);
                    SequenceSelectExisting = new BigButton($"{R.T_B_REMEASURE_SEQ};{R.T_REMEASURE_EXISTING}", R.Element_Sequence_Select, _view.ReMeasureASequence); all.Add(SequenceSelectExisting);
                    CreateSequence = new BigButton(R.T_B_MeasureSequence, R.Element_Sequence, _view.Module.Sequence_CreateOneFromNextPointsList); all.Add(CreateSequence);
                    InvertSequence = new BigButton($"{R.T_INVERT_SEQUENCE};{R.T_INVERT_SEQUENCE_DETAILS}", R.Update, _view.Module.InvertNextPointsList); all.Add(InvertSequence);
                    ReproduceAStation = new BigButton($"{R.T_REPRODUCE_A_STATION};{R.T_USE_THE_MEASURED_POINTS_LIST_OF_A_EXISTING_STATION}", R.Station, _view.Module.Sequence_ReproduceStation); all.Add(ReproduceAStation);
                    AutomatiqueSequence = new BigButton($"{R.T_AUTOMATIC_SEQUENCE};{R.T_AUTOMATIC_SEQUENCE_DETAILS}", R.Element_Sequence_Auto, _view.Module.Sequence_SetAutomatiqueMode); all.Add(AutomatiqueSequence);

                    ChangeSigmaValuesForPreset = new BigButton($"{R.T_CHANGE_APRIORI_SIGMA};{R.T_SIGMA_USED_FOR_THE_COMPENSATION}", R.Sigma, _view.ChangeSigmasToAPreset); all.Add(ChangeSigmaValuesForPreset);

                    ChangeSigmaValues = new BigButton($"{R.T_CHANGE_APRIORI_SIGMA};{R.T_SIGMA_USED_FOR_THE_COMPENSATION}", R.Sigma, _view.CreateCustonSigma); all.Add(ChangeSigmaValues);
                    ChangeToleranceValues = new BigButton($"{R.T_CHANGE_WARNING_TOLERANCES};{R.T_WARNING_MESSAGES_WILL_APPEAR_IF_THE_OFFSETS_ARE_BIGGER_THAN_THOSE_TOLERANCES}", R.Tolerances, _view.ChangeTolerances); all.Add(ChangeToleranceValues);

                    Reflector = new BigButton(R.T194, R.ButtonPictureTC2002_Reflector, () => _view.Reflector_Change()); all.Add(Reflector);
                    ReflectorManagedByAt40x = new BigButton(R.T_MANAGED_BY_AT40X, R.At40x_RRR1_5, _view.Reflector_ManagedByAt); all.Add(Reflector);

                    ChangeComment = new BigButton(R.T195, R.Comment, _view.ChangeComment); all.Add(ChangeComment);
                    StatusGood = new BigButton(R.T_B_STATUS_GOOD, R.StatusGood, _view.ChangeStatusToGood); all.Add(StatusGood);
                    StatusUnknown = new BigButton(R.T_B_STATUS_UNKNOWN, R.Status, _view.ChangeStatusUnknown); all.Add(StatusUnknown);
                    StatusQuestionnable = new BigButton(R.T_B_STATUS_QUESTIONNABLE, R.StatusQuestionnable, _view.ChangeStatusToQuestionnable); all.Add(StatusQuestionnable);
                    StatusControl = new BigButton(R.T_B_STATUS_CONTROL, R.StatusControl, _view.ChangeStatusToControl); all.Add(StatusControl);
                    Measure = new BigButton(R.STM_B_M, R.At40x_Measure, _view.Measure); all.Add(Measure);
                    ShowInstrumentModule = new BigButton(R.STM_B_SIM, R.Instrument, () => { _view.ShowInstrumentModuleAsDialog(); }); all.Add(ShowInstrumentModule);


                    More = new BigButton(R.BT_More, R.Add, _view.ExpandSelectedNode); all.Add(More);
                    Less = new BigButton(R.T_SHOW_LESS, R.Remove, _view.CollapseSelectedNode); all.Add(Less);


                    PinTableOptions = new BigButton($"Pinned Point Table Options;Pinned Point Table Options", R.Pinned_Table, _view.PinTableView.CreatePopUpMenuDataGridView, hasSubButtons: true); all.Add(PinTableOptions);
                    LinkExcelOpenedWorkbook = new BigButton($"{R.T_LINK_WITH_EXCEL};{R.T_LINK_THE_WORKBOOK_PRESENTLY_OPENED}", R.Excel, _view.LinkExcelOpenWorkbook); all.Add(LinkExcelOpenedWorkbook);
                    UnLinkAnyExcelWorkbook = new BigButton($"{R.T_UNLINK_EXCEL};{R.T_UNLINK_ANY_WORKBOOK_LINKED}", R.Excel, _view.UnLinkExcelOpenWorkbook); all.Add(UnLinkAnyExcelWorkbook);

                }

                public void Dispose()
                {
                    _view = null;
                    if (all != null)
                    {
                        for (int i = 0; i < all.Count; i++)
                        {
                            if (all[i] != null)
                                all[i] = null;
                        }
                        all.Clear();
                        all = null;
                    }


                }

            }


            private void Reflector_ManagedByAt()
            {
                Module.Reflector_ManagedByAt(usedMeasure);
            }

            private void MeasureSelectedAgainWithDifferentName()
            {
                Module.SetNextPointToAlreadyMeasured(selectedNode.Tag as Polar.Measure, askName: true, iterate: true);
            }

            private void EditNextPointsListInTextEditor()
            {
                Module.EditNextPointsListInTextEditor();
            }

            private void UnLinkExcelOpenWorkbook()
            {
                Module.ExcelLinkedPath = "";
                Module.MeasureThreated -= Module.ExportToExcel;
            }

            private void LinkExcelOpenWorkbook()
            {
                string s = IO.Excel.FindOpenWorkbook();

                if (s == "")
                {
                    new MessageInput(MessageType.Warning, "No open workbook;Unable to find open Excel WorkBook, sorry.").Show();
                }
                else
                {
                    string titleAndMessage = $"Nice;Measures will be exported in {s}, you are welcome.";
                    new MessageInput(MessageType.GoodNews, titleAndMessage).Show();
                }

                Module.ExcelLinkedPath = s;

            }



            private void UnLinkToBatchFile()
            {
                Module.ExcelLinkedPath = "";
                Module.MeasureThreated -= Module.ExportToBatch;
            }

            private void LinkToBatchFile()
            {
                Module.BatchPath = @"c:\data\tsunami\my_computation.bat";
                Module.MeasureThreated += Module.ExportToBatch;
            }




            private void GotoThatMeasure()
            {
                if (usedMeasure == null)
                    return;

                Module.Goto(usedMeasure);

            }

            private void ChangeExtensionToCode()
            {
                if (usedMeasure == null) return;

                if (usedMeasure == Module.stationParameters.DefaultMeasure)
                {
                    // it will be set to -999.9 and will show "managed by point code in treeviews
                    DoubleValue rall = new DoubleValue(Tsunami2.Preferences.Values.na);
                    Module.ChangeExtension(rall, usedMeasure);
                }
                else
                {
                    // will set the Extension to the one from the code
                    double codeExtension = Module.stationParameters.DefaultMeasure._Point.SocketCode.DefaultExtensionForPolarMeasurement;
                    Module.ChangeExtension(new DoubleValue(codeExtension), usedMeasure);
                }

            }

            private void ShowCSMap()
            {
                var h = Screen.PrimaryScreen.Bounds.Height * 2 / 3;
                ShowMessageWithFillControl(new PictureBox()
                {
                    Image = R.CS_Systems,
                    SizeMode = PictureBoxSizeMode.Zoom,
                    Height = h,
                    MinimumSize = new System.Drawing.Size(Screen.PrimaryScreen.Bounds.Width * 2 / 3, h),
                }, type: MessageType.FYI, $"{R.T_COORDINATES_SYSTEM_MAP}");
            }

            private void ShowMeasurePrecision()
            {
                Tsunami2.Properties.ValueToDisplayType = SO.DisplayTypes.Precisions;
                this.UpdateViewOfPreviousPoint(TsuNode.NodeDetailsType.Shorted);

            }

            private void ShowMeasureVisualDetails()
            {
                SO.CoordinateButtons.SetAction(() =>
                {
                    var displayType = Tsunami2.Properties.ValueToDisplayType;
                    SO.CoordinateButtons.WarnAboutNoneCartesianCS(this);
                    this.PinTableView.UpdateDataGridViewOffsetColumn();
                    this.UpdateViewOfPreviousPoint(Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedType._Name, displayType);

                });

                //SO.CoordinateButtons.HighLightSelected();
                List<Control> controls = new List<Control>();
                controls.AddRange(SO.CoordinateButtons.GetControlsList());
                controls.Insert(2, buttons.ShowMeasurePrecision);
                controls.Insert(3, new Label());
                controls.Insert(3, new Label());
                ShowPopUpSubMenu(controls);

            }


            private void Recompute()
            {
                Polar.Measure m = selectedNode.Tag as Polar.Measure;
                Module.RecomputeCoordinates(m.Clone() as Polar.Measure);
            }

            private void ShowInputAndOutput()
            {
                Parameters.Setup.Values psv = Module.stationParameters.Setups.BestValues;
                if (psv != null)
                {
                    string inputPath = Common.Compute.Compensations.Lgc2.Restore.RestoreLgcDirectory(psv);
                    Shell.ExecutePathInDialogView(inputPath);
                }
                else
                {
                    throw new Exception("Station not Setup");
                }
            }

            private void ChangeSigmasToAPreset()
            {
                // sigma existing for this instrument
                List<I.SigmaForAInstrumentCouple> sigmaForInstrument = Instrument.GetAprioriSigma(
                    this.Module.StationTheodolite.Parameters2._Instrument._InstrumentType.ToString(),
                    Tsunami2.Preferences.Values.InstrumentSlashTargetCoupleSigmas.Concat(this.Module.StationTheodolite.Parameters2.CustomAprioriSigma).ToList());


                List<string> conditions = Instrument.GetListOfSigmaConditionsAvailableFromAList(sigmaForInstrument);
                BindingSource bs = new BindingSource();
                bs.DataSource = conditions;

                string titleAndMessage = $"{R.T_CONDITION};{R.T_CHOOSE_THE_ACTUAL_CONDITION}";
                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = CreationHelper.GetOKCancelButtons(),
                    Controls = new List<Control> { CreationHelper.GetComboBox(bs) }
                };
                using (MessageResult r = mi.Show())
                {
                    if (r.TextOfButtonClicked != R.T_CANCEL)
                    {
                        Module.stationParameters.ResetAprioriSigma();


                        Module.stationParameters.Condition = (r.ReturnedControls[0] as ComboBox).Text;

                        UpdateViewOfStationParameters("apriori");
                    }
                }
            }

            private void CreateCustonSigma()
            {
                if (selectedNode.Tag is I.SigmaForAInstrumentCouple)
                {
                    I.SigmaForAInstrumentCouple couple = selectedNode.Tag as I.SigmaForAInstrumentCouple;
                    List<I.SigmaForAInstrumentCouple> sigmasToClone = Module.StationTheodolite.Parameters2.AprioriSigmaForTheSelectedInstrumentAndConditon;
                    List<I.SigmaForAInstrumentCouple> customSigmas = new List<I.SigmaForAInstrumentCouple>();
                    I.SigmaForAInstrumentCouple customSigmaTomodify = null;

                    // check if we should clone the condition or replace the custom one

                    customSigmas.Clear();
                    foreach (I.SigmaForAInstrumentCouple item in sigmasToClone)
                    {
                        I.SigmaForAInstrumentCouple newCouple = null;
                        if (item.Condition == "CUSTOM")
                            newCouple = item;
                        else
                            newCouple = item.Clone() as I.SigmaForAInstrumentCouple;

                        if (item == couple)
                        {
                            customSigmaTomodify = newCouple;
                        }
                        newCouple.Condition = "CUSTOM";
                        customSigmas.Add(newCouple);
                    }

                    string message = $"{R.T_ENTER_THE_NEW_APRIORI_SIGMAS};{R.T_THAT_WILL_BE_USED_FOR_THE_COMPENSATIONS_FOR_THE_TARGET + couple.Target.ToString() + "'"}";

                    string sH = "Horizontal " + R.T_ANGLE_IN_CC;
                    string sV = "Vertical " + R.T_ANGLE_IN_CC;
                    string sD = R.T_DISTANCE_IN_MM;
                    TableLayoutPanel tlp = CreationHelper.GetTableLayoutForSigmas(
                        new List<string> { sH, sV, sD },
                        new List<string> { customSigmaTomodify.sigmaAnglCc.ToString(), customSigmaTomodify.sigmaZenDCc.ToString(), customSigmaTomodify.sigmaDistMm.ToString() }
                        );

                    MessageInput mi = new MessageInput(MessageType.Choice, message)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL },
                        Controls = new List<Control> { tlp }
                    };
                    using (MessageResult result = mi.Show())
                    {
                        if (result.TextOfButtonClicked == R.T_CANCEL)
                            return;

                        // Get the Sigmas
                        TableLayoutPanel tlpr = (TableLayoutPanel)result.ReturnedControls[0];

                        if (double.TryParse(tlpr.Controls[sH].Text, out double newSAH) &&
                            double.TryParse(tlpr.Controls[sV].Text, out double newSAV) &&
                            double.TryParse(tlpr.Controls[sD].Text, out double newSD))
                        {
                            Module.ChangeSigma(customSigmaTomodify, newSAH, newSAV, newSD);
                            Module.stationParameters.Condition = "CUSTOM";
                            Module.stationParameters.ResetAprioriSigma();
                            Module.stationParameters.CustomAprioriSigma.AddRange(customSigmas);
                            UpdateViewOfStationParameters("apriori");
                        }
                        else
                        {
                            throw new Exception(R.String_WrongTolerance);
                        }
                    }
                }
            }

            private void ChangeTolerances()
            {
                if (selectedNode.Tag is I.InstrumentTolerance.Type type)
                {
                    string message = $"{R.T_ENTER_THE_NEW_TOLERANCES};{R.T_THAT_WILL_TRIGGER_A_WARNING_WHEN_THE_OFFSET_FOR_ + type.type.ToString()}";

                    TableLayoutPanel tlp = CreationHelper.GetTableLayoutForSigmas(
                        new List<string> { R.T_ANGLE_IN_CC + "_H", R.T_ANGLE_IN_CC + "_V", R.T_DISTANCE_IN_MM },
                        new List<string> { type.H_CC.ToString(), type.V_CC.ToString(), type.D_mm.ToString() }
                        );

                    MessageInput mi = new MessageInput(MessageType.Choice, message)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL },
                        Controls = new List<Control> { tlp }
                    };
                    using (MessageResult result = mi.Show())
                    {
                        if (result.TextOfButtonClicked == R.T_CANCEL)
                            return;

                        // Get the Sigmas
                        TableLayoutPanel tlpr = (TableLayoutPanel)result.ReturnedControls[0];

                        if (double.TryParse(tlpr.Controls[R.T_ANGLE_IN_CC + "_H"].Text, out double newHCC) &&
                            double.TryParse(tlpr.Controls[R.T_ANGLE_IN_CC + "_V"].Text, out double newVCC) &&
                            double.TryParse(tlpr.Controls[R.T_DISTANCE_IN_MM].Text, out double newMM))
                        {
                            Module.ChangeTolerance(type, newHCC, newVCC, newMM);
                        }
                        else
                        {
                            throw new Exception(R.String_WrongTolerance);
                        }
                    }
                }
            }



            private void StakeOutLine()
            {
                Module.StakeOutLine();
                // this.TryAction(Module.StakeOutLine); only called from a button which run the same tryaction
            }

            private void StakeOutKnownLine()
            {
                Module.StakeOutKnownLine();
                //this.TryAction(Module.StakeOutKnownLine); only called from a button which run the same tryaction
            }



            private void ChangeCode_try()
            {
                ChangeCode();
                //this.TryAction(ChangeCode); only called from a button which run the same tryaction
            }

            private void ChangeCode_ToNo()
            {
                usedMeasure._Point.SocketCode = null;
            }

            private void ChangeCode()
            {
                Module.ChangeCode(usedMeasure._Point.SocketCode, usedMeasure);
                this.UpdateSomeMainNodes(true, false, false);
            }


            private void ReMeasureASequence()
            {
                Module.Sequence_ReMeasureExisting();
                //this.TryAction(this.Module.Sequence_ReMeasureExisting); only called from a button which run the same tryaction
            }

            private void ReMeasureLastSequence()
            {
                Module.Sequence_ReMeasureLast();
                //this.TryAction(this.Module.Sequence_ReMeasureExisting); only called from a button which run the same tryaction
            }


            private void CollapseSelectedNode()
            {
                treeView.SelectedNode.Collapse();
            }

            private void ExpandSelectedNode()
            {
                treeView.SelectedNode.ExpandAll();
            }

            internal void SetIterationVersusTime()
            {
                Module.stationParameters.DefaultMeasure._Point._Name = Parameters.FindBestDefaultName();
                DsaFlag.GetByNameOrAdd(Module.DsaFlags, "WantToIterateTheNewPointName").State = DsaOptions.Always; //Module.flags.WantToIterateTheNewPointName = true;
                buttons.IterationVersusTime.Available = false;
                buttons.TimeVersusIteration.Available = true;

                UpdateViewOfStationParameters();
                UpdateView();
            }
            internal void SetTimeVersusIteration()
            {
                Module.stationParameters.DefaultMeasure._Point._Name = Module.NAME_BASED_ON_TIME;
                DsaFlag.GetByNameOrAdd(Module.DsaFlags, "WantToIterateTheNewPointName").State = DsaOptions.Never; //Module.flags.WantToIterateTheNewPointName = false;
                buttons.IterationVersusTime.Available = true;
                buttons.TimeVersusIteration.Available = false;

                UpdateViewOfStationParameters();
            }

            internal void SetV0Manually()
            {
                string cancel = R.T_CANCEL;
                List<string> buttonTexts = new List<string> { R.T_OK, cancel };

                MessageTsu.ShowMessageWithTextBox(R.T_TM_SET_V0, buttonTexts, out string buttonClicked, out string textInput, "0");

                if (buttonClicked != cancel)
                {
                    Module.SetV0Manually(T.Conversions.Numbers.ToDouble(textInput));
                    UpdateViewOfStationParameters();
                }

            }





            #endregion

            #endregion

            #region Actions

            internal void treeview1_OnClick(object sender, TreeNodeMouseClickEventArgs e)
            {
                if (e.Clicks == 2) return;
                if (!e.Node.Bounds.Contains(e.Location))
                {
                    //if (this.PopUpMenu.Visible == true) this.PopUpMenu.Hide();
                    return;
                }
                if (e.Node == null) return;

                Tag = e.Node.Tag;

                ShowContextMenuOfTheNode((TsuNode)e.Node);

            }

            private void ShowContextMenuOfTheNode(TsuNode n)
            {
                selectedNode = n;
                SaveTreeViewLook();
                try
                {
                    usedMeasure = (n.Tag as Polar.Measure);
                    Tag = n.Tag;
                    StationProperties p = n._StationParameters;

                    // Context Menu
                    contextButtons.Clear();
                    CreateContextMenuBasedOn(p);

                    if (contextButtons.Count > 0)
                    {
                        if (contextButtons.Count == 1 && Tsunami2.Preferences.Values.GuiPrefs.ShowOneButtonSubMenu.IsFalse && contextButtons[0] is BigButton bb)
                            bb.Do();
                        else
                            this.ShowPopUpMenu(contextButtons);
                    }
                }
                catch (Exception ex)
                {
                    MessageTsu.ShowMessage(ex);
                }
            }

            #region Face
            private void ChangeToFace1()
            {
                Module.ChangeFace(usedMeasure, I.FaceType.Face1);
            }
            private void ChangeToFace2()
            {
                Module.ChangeFace(usedMeasure, I.FaceType.Face2);
            }
            private void ChangeToDoubleFace()
            {
                Module.ChangeFace(usedMeasure, I.FaceType.DoubleFace);
            }
            private void ChangeToQuickMeasure()
            {
                Module.ChangeFace(usedMeasure, I.FaceType.Face1);
                Module.ChangeNumberOfMeasurement(1, usedMeasure);
            }

            #endregion

            #region Management.Measure

            private void ShowSelectedMeasure()
            {

                base.Module._MeasureManager.SelectableObjects.Clear();

                base.Module._MeasureManager._SelectedObjects.Clear();
                base.Module._MeasureManager._SelectedObjects.Add(selectedNode.Tag as Measure);
                base.Module._MeasureManager.SelectableObjects.Add(selectedNode.Tag as Measure);
                base.Module._MeasureManager._SelectedObjectInBlue = selectedNode.Tag as Measure;
                Module._MeasureManager.View.ShowAsTreeView();
                Module._MeasureManager.View.AutoCheckChildren = false;
                Module._MeasureManager.MultiSelection = false;
                Module._MeasureManager.Show();
                base.Module._MeasureManager.SelectableObjects.Clear();
                UpdateViewOfPreviousPoint();
            }

            internal override void ShowMeasureModule()
            {
                M.Manager mm = Module._MeasureManager;
                mm.SetSelectableList(null);
                mm._SelectedObjects.Clear();
                mm._SelectedObjectInBlue = null;
                mm.View.SetStrategy(V.Types.List);

                mm.MultiSelection = true;
                mm.View.AutoCheckChildren = false;
                base.ShowMeasureModule();
                UpdateViewOfPreviousPoint();
            }


            internal void ShowMeasureDifferences(List<M.Measure> diffs)
            {
                if (!DsaFlag.IsSetToNever(this.Module.DsaFlags, "ShowMeasureDifferences") && diffs.Count > 1)
                {

                    M.Manager mod = new M.Manager(Module);
                    mod.AllElements.AddRange(diffs);
                    mod.View.ShowAsListView();

                    DsaFlag dsaflag = DsaFlag.GetByNameOrAdd(Module.DsaFlags, "ShowMeasureDifferences");
                    // TsuBool dontShowAgain = new TsuBool(!Module.flags.ShowMeasureDifferences);
                    mod.View.ShowInMessageTsu(R.T198, R.T_OK, null, null, null, null, null, null, null, true, false, dsaflag);
                    //Module.flags.ShowMeasureDifferences = !dontShowAgain.IsTrue;

                }
                UpdateViewOfPreviousPoint();
            }

            private void RemoveNextMeasure()
            {
                Module.RemoveNextMeasure(usedMeasure);
            }



            internal void RemoveAllMeasures()
            {
                Module.RemoveAllMeasures();
            }

            private void ChangeStatus()
            {
                Module.ChangeMeasureStatus(usedMeasure);
                UpdateViewOfNextPoint();
            }

            private void ChangeStatusToQuestionnable()
            {
                Module.ChangeMeasureStatus(usedMeasure, new M.States.Questionnable());
                UpdateViewOfNextPoint();
            }

            private void ChangeStatusToControl()
            {
                if (Module.Control_ReplacePointsByOriginal(usedMeasure))
                    Module.ChangeMeasureStatus(usedMeasure, new M.States.Control());

                UpdateViewOfNextPoint();
            }

            private void ChangeStatusToStakeut()
            {
                try
                {
                    Module.StakeOut(usedMeasure);
                }
                catch (Exception ex)
                {
                    new MessageInput(MessageType.Critical, ex.Message).Show();
                }
                finally
                {
                    UpdateViewOfNextPoint();
                }
            }

            internal double AskVerticalOffsetToStakeut(double currentOne, double proposedOne = -999.9)
            {
                if (proposedOne == -999.9)
                    proposedOne = Module.lastUsedTheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates;
                string titleAndMessage = $"{R.T_APPLY_A_VERTICAL_OFFSET};{R.T_IN_CCS_HEIGHT_MATTERS}\r\n{R.T_THIS_IS_DIFFERENT_FROM_THE_PHYSICAL}\r\n{R.T_RALLONGE2_EXAMPLE}\r\n{R.T_CURRENT_VALUE} {currentOne * 1000} mm";
                string initialTextBoxText = $"{proposedOne * 1000}";
                List<string> buttonTexts = new List<string> { R.T_YES, R.T_NO };

                MessageTsu.ShowMessageWithTextBox(titleAndMessage, buttonTexts, out string buttonClicked, out string textInput, initialTextBoxText, true);

                if (buttonClicked == R.T_NO)
                    return currentOne;
                else
                {
                    double value;
                    if (Double.TryParse(textInput, out value))
                    {
                        Module.lastUsedTheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates = value / 1000;
                        return value / 1000;
                    }
                    else
                    {
                        throw new Exception(string.Format(R.T_WRONG_VALUE_ENCODED));
                    }
                }
            }

            private void ChangeAllStatusToStakeut()
            {
                try
                {

                    foreach (Polar.Measure item in base.Module._Station.MeasuresToDo)
                    {
                        Module.StakeOut(item);
                    }
                }
                catch (Exception ex)
                {
                    new MessageInput(MessageType.Critical, ex.Message).Show();
                }
                finally
                {
                    UpdateViewOfNextPoint();
                }
            }

            internal void ChangeStatusToGood()
            {
                Module.ChangeMeasureStatus(usedMeasure, new M.States.Good());
                UpdateViewOfNextPoint();
            }

            internal void ChangeStatusUnknown()
            {
                Module.ChangeMeasureStatus(usedMeasure, new M.States.Unknown());
                UpdateViewOfNextPoint();
            }


            #endregion

            #region Points
            public void SetNextPoints()
            {
                TryAction(Module.SelectNextPoints, R.T_NEXT_POINT_SELECTION);
                UpdateViewOfNextPoint();

                //this.UpdateNextInDataGridView();
            }

            private void SetNextPointsForStakeOut()
            {
                TryAction(Module.SelectNextPointsToStakeOut, R.T_PREPARATION_FOR_STAKE_OUT);
                UpdateViewOfNextPoint();
            }

            internal bool RenamePoint(Polar.Measure m, ref TsuOptionalShowing.Type showingOption, string proposedName = "")
            {
                usedMeasure = m;
                return RenamePoint(ref showingOption, proposedName: proposedName);
            }

            internal bool RenamePoint(ref TsuOptionalShowing.Type showingOption, string proposedName = "")
            {
                if (proposedName == "")
                    proposedName = usedMeasure._Point._Name;

                // if default measure, possibly put the zone from the theofile
                if (usedMeasure == Module.stationParameters.DefaultMeasure)
                {
                    if (usedMeasure._Point._Name == "ZONE.NEW.POINT.###")
                    {
                        proposedName = $"{Module.GetBestZoneName()}.NEW.POINT.###";
                    }
                }

                string newName = "";
                string cancel = R.T_CANCEL;

                if (TsuOptionalShowing.IsShowingNeeded(showingOption))
                {
                    if (!Module.ElementManager.AskName(proposedName, out newName, "", Module.GetAllPointNames()))
                        return false;

                    newName = newName.Trim();
                    if (newName.Split('.').Length >= 4)
                    {
                        SocketCode proposedCode = Common.Analysis.Element.DetermineCodeToPropose(usedMeasure, Module.stationParameters.DefaultMeasure, newName, out bool fromName, out string message);
                        SocketCode.Ask(this, Tsunami2.Preferences.Values.SocketCodes, proposedCode, "NameForPTH", out SocketCode newSocketCode, out double dummy, customMessage: message, fromName);
                        if (newSocketCode == null)
                            return false;
                        Module.ChangeCode(newSocketCode, usedMeasure, ask: false);
                    }

                    Module.ChangePointName(newName, usedMeasure);
                }
                UpdateViewOfNextPoint();
                return true;
            }

            private void ChangeComment()
            {
                string proposedComment = (usedMeasure.CommentFromUser == R.T_NO_COMMENT) ? "Blabla" : usedMeasure.CommentFromUser;
                List<string> buttonTexts = new List<string> { R.T_OK, R.T_CANCEL };

                MessageTsu.ShowMessageWithTextBox(R.T201, buttonTexts, out string buttonClicked, out string textInput, proposedComment);

                if (buttonClicked != R.T_CANCEL)
                {
                    Module.ChangeComment(textInput, usedMeasure);
                }
                UpdateView();
            }




            #region Sequence
            private void MoveNextPointDown()
            {
                Module.MoveNextPointDown(usedMeasure);
                treeView.SelectedNode = treeView.SelectedNode.NextNode;
            }
            private void MoveNextPointUp()
            {
                Module.MoveNextPointUp(usedMeasure);
                treeView.SelectedNode = treeView.SelectedNode.PrevNode;
            }
            #endregion

            #endregion

            #region Reflector
            public void Reflector_Change(Measure forceMeasureToModify = null)
            {
                var measureToModify = forceMeasureToModify ?? usedMeasure;
                Module.Reflector(measureToModify);
            }
            #endregion

            #region Extension


            internal void ChangeExtension(DoubleValue proposedValue = null, Measure forceMeasureToModify = null)
            {
                bool useMM = (Tsunami2.Preferences.Values.GuiPrefs.UnitForExtension == P.Preferences.DistanceUnit.mm);
                string value;

                if (proposedValue == null)
                    proposedValue = _PreviousRallonge;

                var measureToModify = forceMeasureToModify ?? usedMeasure;

                if (AskExtension(this, proposedValue, measureToModify, useMM, out value))
                {
                    if (measureToModify != null)
                    {
                        if (measureToModify.Extension.Value == Tsunami2.Preferences.Values.na)

                            _PreviousRallonge = new DoubleValue(measureToModify._Point.SocketCode != null ? measureToModify._Point.SocketCode.DefaultExtensionForPolarMeasurement : 0);
                        else
                            _PreviousRallonge = measureToModify.Extension;

                    }
                    DoubleValue rall = T.Conversions.Numbers.ToDoubleValue(value);
                    if (useMM) rall.Value /= 1000;
                    Module.ChangeExtension(rall, measureToModify);
                }
            }

            public static bool AskExtension(TsuView view, DoubleValue _PreviousRallonge, Polar.Measure mes, bool useMM, out string respond)
            {
                string cancel = R.T_CANCEL;
                respond = cancel;
                DoubleValue actualIncludingManagedByCode;
                BindingSource bindingSource = new BindingSource();
                List<string> defaultValues = new List<string>
                {
                 "0.0", "70.0", "140.0", "-70.0", "26.0"
                };

                if (mes.Extension.Value == Tsunami2.Preferences.Values.na)
                {
                    actualIncludingManagedByCode = new DoubleValue(mes._Point.SocketCode != null ? mes._Point.SocketCode.DefaultExtensionForPolarMeasurement : 0);
                }
                else
                {
                    actualIncludingManagedByCode = mes.Extension;
                }

                string boxMessage = "";
                string actual;
                if (useMM)
                {
                    actual = (actualIncludingManagedByCode.Value * 1000).ToString();
                    string previous = (_PreviousRallonge.Value * 1000).ToString("N1");
                    if (!defaultValues.Contains(previous)) defaultValues.Add(previous);
                    boxMessage = string.Format(R.T_ENTER_EXTENSION_MM, actual);
                }
                else
                {
                    actual = actualIncludingManagedByCode.Value.ToString();
                    string previous = (_PreviousRallonge.Value).ToString();
                    defaultValues = defaultValues.Select(value =>
                    {
                        double millimeters = double.Parse(value);
                        double meters = millimeters / 1000;
                        return meters.ToString();
                    }).ToList();
                    if (!defaultValues.Contains(previous)) defaultValues.Add(previous);
                    boxMessage = string.Format(R.T_ENTER_EXTENSION_M, actual);
                }

                bindingSource.DataSource = defaultValues;
                ComboBox comboBox = CreationHelper.GetComboBox(bindingSource);
                comboBox.DropDownStyle = ComboBoxStyle.DropDown;
                MessageInput mi = new MessageInput(MessageType.Choice, boxMessage)
                {
                    ButtonTexts = CreationHelper.GetOKCancelButtons(),
                    Controls = new List<Control> { comboBox }
                };
                using (MessageResult messageResult = mi.Show())
                {
                    respond = actual;
                    if (messageResult.TextOfButtonClicked == R.T_OK)
                        respond = comboBox.Text;
                }

                // to be able to restore "managed by code" without typing -999900 ou -999.9
                if (respond.ToUpper() == "M" || respond.ToUpper() == "C")
                {
                    int mutliplicator = 1;

                    if (useMM)
                        mutliplicator = 1000;

                    respond = $"{Tsunami2.Preferences.Values.na * mutliplicator}";
                }

                if (respond != cancel)
                    return true;
                else
                    return false;
            }
            #endregion

            #region StationSetup

            internal void ExpandNodeWithMissingInfo()
            {
                foreach (TreeNode item in treeView.Nodes)
                {
                    ExpandNodeWithMissingInfo(item);
                }
                UpdateView();
            }

            private void ExpandNodeWithMissingInfo(TreeNode n)
            {
                switch ((n as TsuNode).State)
                {
                    case StateType.NotReady:
                        n.Expand();
                        break;
                    case StateType.Unknown:
                    case StateType.Normal:
                    case StateType.Ready:
                    case StateType.Acceptable:
                    case StateType.ToBeFilled:
                    default:
                        break;
                }
                foreach (TreeNode item in n.Nodes)
                {
                    ExpandNodeWithMissingInfo(item);
                }
            }

            internal void SetStationPointFromList()
            {
                TryAction(Module.ChangeStationPointToKnownOne, R.T_STATION_POINT_DEFINITION);
                if (Module.stationParameters.Setups.InitialValues.StationPoint != null)
                    if (Module.stationParameters.Setups.InitialValues.StationPoint.SocketCode != null)
                        TryAction(ChangeInstrumentHeightToKnownOne, R.T_INSTRUMENT_HEIGHT_DEFINITION);

                UpdateViewOfStationParameters();
                UpdateModuleName();
            }

            internal void UpdateModuleName()
            {
                _Module._Name = Module.StationTheodolite._Name;
                AddTopButton(string.Format(R.T161, _Module._Name),
                        icon, ShowContextMenuOfModule);
            }

            // change the instrument height in accord with th socket code
            private void ChangeInstrumentHeightToKnownOne()
            {
                if (Module.stationParameters.Setups.InitialValues.StationPoint.SocketCode != null)
                {
                    Module.ChangeInstrumentHeight(Module.stationParameters.Setups.InitialValues.StationPoint.SocketCode.InstrumentHeightForPolarMeasurement);
                }
            }

            internal void SetNewStationPoint()
            {
                Module.SetNewStationPoint();
            }

            internal string AskNewStationPoint()
            {
                string cancel = R.T_CANCEL;
                string initialTextBoxText = Module.GetAvailableStationName();
                List<string> buttonTexts = new List<string> { R.T_USE, cancel };

                MessageTsu.ShowMessageWithTextBox(R.T_ENTER_STTN_NAME, buttonTexts, out string buttonClicked, out string textInput, initialTextBoxText);

                if (buttonClicked != cancel)
                    return textInput;
                else
                    return null;
            }

            internal bool AskAndChangeNumberOfMeasurement()
            {
                // select proposed one
                int valueToPropose;
                if (usedMeasure.NumberOfMeasureToAverage == 1)
                    valueToPropose = 2;
                else
                    valueToPropose = 1;

                KeyPressEventHandler myKeyPressHandler = (sender, e) =>
                {
                    if (e.KeyChar == (char)Keys.Enter || e.KeyChar == (char)Keys.Return)
                    {
                        TextBox tb = sender as TextBox;
                        MessageTsu mt = tb.Parent.Parent.Parent as MessageTsu;
                        MessageResponseButton b = mt.GetButtonByName("Custom") as MessageResponseButton;
                        b.Button.PerformClick();
                    }
                };

                // ask message
                string titleAndMessage = string.Format(R.T_M_NUMBER_MEAS, "\r\n", usedMeasure.NumberOfMeasureToAverage.ToString());
                MessageTsu.ShowMessageWithTextBox(
                    titleAndMessage,
                    new List<string> { "10", "3", "2", "1", "Custom", R.T_CANCEL },
                    out string buttonClicked,
                    out string valueReceived,
                    valueToPropose.ToString(),
                    SelectText: true,
                    type: MessageType.Choice,
                    onKeyPress: myKeyPressHandler);

                // results
                if (buttonClicked == R.T_CANCEL) return false;

                string numberAsString;

                if (buttonClicked == "Custom")
                    numberAsString = valueReceived;
                else
                    numberAsString = buttonClicked;

                if (!int.TryParse(numberAsString, out int number))
                    throw new Exception(R.T_M_NUMBER_MEAS);
                if (number < 1) throw new Exception(R.T_M_NUMBER_MEAS);

                // Update
                Module.ChangeNumberOfMeasurement(number, usedMeasure);
                if (usedMeasure == Module.stationParameters.DefaultMeasure)
                    UpdateViewOfStationParameters();
                else
                    UpdateViewOfNextPoint();

                return true;
            }



            private void tre(object sender, KeyPressEventArgs e)
            {
                throw new NotImplementedException();
            }

            internal void SetInstrumentHeightToUnkown()
            {
                Module.OnInstrumentHeightKnowledgeChanged(false);
                UpdateViewOfStationParameters();
            }

            /// <summary>
            /// Message asking the user to select a code or enter an height
            /// </summary>
            /// <exception cref="Exception"></exception>
            internal bool AskInstrumentHeightWithCode(string pointName = "")
            {
                //List<E.SocketCode> possibleCodes = E.SocketCode.GetCodesPossibleForAPolarStationHeight();

                SocketCode.GetSmartCodeFromName(pointName, out SocketCode codeFromName, out _);

                // choose preselected one
                E.SocketCode preselectedCode = null;
                Elements.Point stationPoint = this.Module.StationTheodolite.Parameters2._StationPoint;
                if (stationPoint != null)
                    if (stationPoint.SocketCode != null)
                        preselectedCode = stationPoint.SocketCode;
                    else
                        preselectedCode = codeFromName;

                ;
                // show message
                MessageInput mi = new MessageInput(MessageType.Choice, R.T_INST_HEIGHT)
                {
                    ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL },
                    Controls = CreationHelper.GetSocketCodeComboBox(SocketCode.GetCodesPossibleForAPolarStationHeight(), preselectedCode, "NameForIH", null)
                };
                using (var results = mi.Show())
                {
                    // treat respond
                    if (results.TextOfButtonClicked == R.T_CANCEL)
                        return false;

                    var comboReturned = (results.ReturnedControls[0] as ComboBox);
                    var receivedCode = comboReturned.SelectedValue as SocketCode;
                    if (receivedCode != null)
                    {
                        Module.ChangeInstrumentHeight(receivedCode.InstrumentHeightForPolarMeasurement);
                        UpdateViewOfStationParameters();
                        return true;
                    }
                    return false;
                }
            }

            internal void ChangeInstrumentHeightTo0()
            {
                Module.ChangeInstrumentHeight(0);
                UpdateViewOfStationParameters();
            }

            internal void Compute()
            {
                TryAction(Module.DetermineHowtoAndThenComputeWithLGC2, R.T_STATION_SETUP_COMPENSATION);
            }

            internal void ForgetCompute()
            {
                TryAction(() => { Module._SetupStrategy.CancelSetup(); }, R.T_CANCELLATION_OF_COMPENSATION);
                UpdateView();
            }

            internal void SetVZero()
            {
                TryAction(Module.SetVZero, R.T_MANUAL_V0_SETUP);
            }
            internal bool AskIfInputEditionIsWanted()
            {
                string titleAndMessage = string.Format(R.T205, Module._SetupStrategy._strategy.ToString())
                                         + ". " + R.T_Q_EDIT_INPUT;
                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                };
                return (mi.Show().TextOfButtonClicked == R.T_YES);
            }

            internal void AskIfInputEditionIsWanted(ref bool editInput, ref bool showOutput)
            {
                string titleAndMessage = string.Format(R.T205,
                                             Module._SetupStrategy._strategy.ToString())
                                         + ". " + R.T_Q_EDIT_INPUT;
                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_QR_EDIT_None, R.T_QR_EDIT_INPUT, R.T_QR_EDIT_OUTPUT }
                };
                string respond = mi.Show().TextOfButtonClicked;

                editInput = false; showOutput = false;
                if (respond != R.T_QR_EDIT_None) showOutput = true;
                if (respond == R.T_QR_EDIT_INPUT) editInput = true;
            }
            #endregion

            #region Control
            private void ControlWithFace2()
            {
                TryAction(Module.SetNextPointsToControlFace2, "Face 2 preparation");
            }
            private void MeasureEverythingAgain()
            {
                TryAction(() => { Module.SetNextPointsToAlreadyMeasured(); });
            }
            private void MeasureSelectedAgain()
            {
                Module.SetNextPointToAlreadyMeasured(selectedNode.Tag as Polar.Measure, askName: false, iterate: false);

            }
            private void ControlSelectedMeasureWithFace2()
            {
                Module.ControlSelectedMeasureWithFace2(selectedNode.Tag as Polar.Measure);
            }
            private void ControlFermeture()
            {
                TryAction(() => { this.Module.PrepareOpeningMeasurement(); }, R.T_CONTROL_ON_THE_OPENING_POINT);
            }

            internal void CloseStation()
            {
                string cancel = R.T_CANCEL;
                string titleAndMessage = string.Format("{0};{1}", R.T_CLOSE_THE_STATION, R.T_ARE_YOU_SURE_THE_STATION_WILL_NOT_RECEIVE_DATA_FROM_THE_INSTRUMENT_ANYMORE);
                MessageInput mi = new MessageInput(MessageType.Warning, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_OK, cancel }
                };
                if (mi.Show().TextOfButtonClicked != cancel)
                {
                    TryAction(() => { Module.CloseStation(out bool dummy); }, R.T_TERMINATION_OF_THE_STATION);
                    UpdateViewOfStationParameters();
                }
                // why to disconnect instrument??? not usefull...
                //if (Module.stationParameters._State is Station.State.Closed && this.StationModule._InstrumentManager.SelectedInstrumentModule is I.IConnectable)
                //    if (this.ShowMessageOfExclamation(string.Format("{0};{1}", R.T_DISCONNECT_THE_INSTRUMENT, R.T_DO_YOU_WANT_TO_DISCONNECT_THE_INSTRUMENT_FROM_THE_COMPUTER),  R.T_NO, R.T_DISCONNECT) != R.T_NO)
                //    {
                //        TryAction(this.Module.DisconnectInstrument, Logs.LogEntry.types.PayAttention, R.T_INSTRUMENT_DISCONNECTION);
                //        UpdateViewOfStationParameters();
                //    }
            }

            #endregion

            internal bool AskToApplyNewDefautParameters()
            {
                bool doIt = true;
                if (Visible && Module.StationTheodolite.MeasuresToDo.Count > 0)
                {
                    MessageInput mi = new MessageInput(MessageType.Choice, R.DP_DOWTA)
                    {
                        ButtonTexts = new List<string> { R.T_Apply, R.T_DONT + " " + R.T_Apply }
                    };
                    doIt = mi.Show().TextOfButtonClicked == R.T_Apply;
                }
                return doIt;
            }

            #endregion

            #region updates
            // PopulateTreeView(this.theodoliteModule._Station, treeView);  // is an other possiblity of updating via a serailization
            internal override void UpdateInstrumentView()
            {
                UpdateViewOfStationParameters();

                var split = this.splitContainerStationVsInstrument;

                TsuView v = Module._InstrumentManager.SelectedInstrumentModule._TsuView;
                if (v.IsDisposed)
                {
                    I.Manager.Module.GetTheRightModuleBasedOnInstrument(Module._InstrumentManager.SelectedInstrument, Module._InstrumentManager);
                    v = Module._InstrumentManager.SelectedInstrumentModule._TsuView;
                    split.Panel2.Controls.Clear();
                    v.ShowDockedFill();
                    split.Panel2.Controls.Add(v);
                    v.CheckOrientation();
                    v.BringToFront();
                }
                if (!split.Panel2.Controls.Contains(v))
                {
                    v.TopLevel = false;
                    v.Visible = true;
                    v.Dock = DockStyle.Fill;
                    split.Panel2.Controls.Add(v);
                    v.CheckOrientation();
                    v.BringToFront();
                }
            }
            public void UpdateView(M.IMeasure m)
            {
                UpdateViewOfPreviousPoint();
            }
            public void UpdateStationPoint(Elements.Point p)
            {
                UpdateViewOfStationParameters();
            }
            public override void UpdateView()
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new System.Action(() => UpdateView()));
                    return;
                }
                UpdateViewOfNextPoint();
                UpdateViewOfPreviousPoint();
                UpdateViewOfStationParameters();

                //  MENU | FINAL MODULE BIGBUTTON
                //       |   STATION MODULE BIGBUTTON
                //       |      A (TOP)
                //       |          AA (TOP)
                //       |          AB (BOTTOM)  
                //       |      B (BOTTOM)
                //       |
                
                var iv = Module?._InstrumentManager.SelectedInstrumentModule?.View;
                var sv = this.treeView;

                var split = this.splitContainerStationVsInstrument;
                var A = split.Panel1.Controls;
                var B = split.Panel2.Controls;

                if (PinTableView.isVisible)
                {
                    this.PinTableView.UpdateAllTable();
                    var pv = PinTableView.dataGridViewPinnedTable;

                    var AX = PinTableView.SplitPanel_GetOrCreate();
                    //A.Clear();
                    PinTableView.isInitializaling = true;
                    A.Add(AX);

                    PinTableView.isInitializaling = false;

                    var AA = AX.Panel1.Controls;
                    var AB = AX.Panel2.Controls;

                    B.Clear();
                    AA.Clear();
                    AB.Clear();

                    var pos = Tsunami2.Preferences.Values.GuiPrefs.PinnedPointsViewPosition;
                    int distance;
                    switch (pos)
                    {
                        case PinTableView.SplitContainerPosition.GrandParent:
                        case PinTableView.SplitContainerPosition.HorizontalInGrandParent:
                        case PinTableView.SplitContainerPosition.VerticalInGrandParent:
                            AA.Add(sv);
                            AB.Add(iv);
                            iv?.BringToFront();
                            B.Add(pv);
                            pv?.BringToFront();
                            break;

                        case PinTableView.SplitContainerPosition.None:
                        case PinTableView.SplitContainerPosition.Horizontal:
                        case PinTableView.SplitContainerPosition.Vertical:
                        case PinTableView.SplitContainerPosition.Parent:
                        case PinTableView.SplitContainerPosition.HorizontalInParent:
                            AX.Orientation = Orientation.Horizontal;

                            distance = Tsunami2.Preferences.Values.GuiPrefs.PintableSplitDistance;
                            if (Macro.IsPlaying || Macro.IsRecording)
                                distance -= 50;
                            AX.SplitterDistance = distance;

                            // 2 * (AX.Height / 3);
                            AA.Add(sv);
                            AB.Add(pv);
                            B.Add(iv);
                            iv?.BringToFront();
                            break;
                        case PinTableView.SplitContainerPosition.VerticalInParent:
                        default:
                            AX.Orientation = Orientation.Vertical;

                            distance = Tsunami2.Preferences.Values.GuiPrefs.PintableSplitDistance;
                            if (Macro.IsPlaying || Macro.IsRecording)
                                distance -= 50;
                            AX.SplitterDistance = distance;

                            AA.Add(sv);
                            AB.Add(pv);
                            B.Add(iv);
                            iv?.BringToFront();
                            break;
                    }
                    Tsunami2.Preferences.Values.GuiPrefs.PinnedPointsViewPosition = pos;
                    //Tsunami2.Preferences.Values.GuiPrefs.PintableSplitDistance = AX.SplitterDistance;
                }
                else
                {
                    // Check if the instrumetn view is there or if if has been move somewhere else
                    if (!(ParentView is Common.Guided.View))
                    {
                        if (sv !=null)
                        {
                            // re add instrument view
                            try
                            {
                                A.Clear();
                                A.Add(sv);
                                sv?.BringToFront();
                            }
                            catch (Exception ex)
                            {
                                new MessageInput(MessageType.Bug, ex.Message).Show();
                            }
                        }
                        if (iv !=null)
                        {
                            // re add instrument view
                            try
                            {
                                B.Clear();
                                B.Add(iv);
                                iv?.BringToFront();
                            }
                            catch (Exception ex)
                            {
                                new MessageInput(MessageType.Bug, ex.Message).Show();
                            }
                        }
                    }
                }
            }

            public event EventHandler<ModuleEventArgs> NextPointListUpdated;

            public void UpdateViewOfNextPoint()
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new System.Action(() => UpdateViewOfNextPoint()));
                    return;
                }
                try
                {
                    if (this.Module == null) { return; }
                    UpdateOneOfTheMainNodeWith(2,
                        new TsuNode((this.Module._Station as Station).MeasuresToDo, TsuNode.NodeDetailsType.Special));
                    treeView.Nodes[2].Expand();

                    if (NextPointListUpdated != null) NextPointListUpdated(this, new Common.ModuleEventArgs(this.Module));
                }
                catch (Exception ex)
                {
                    MessageTsu.ShowMessage(ex);
                }
            }
            public void UpdateViewOfPreviousPoint(TsuNode.NodeDetailsType type = TsuNode.NodeDetailsType.Shorted)
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new System.Action(() => UpdateViewOfPreviousPoint(type)));
                    return;
                }
                try
                {
                    var displayType = Tsunami2.Properties.ValueToDisplayType;
                    // if a type of coordiante is selected we show it instaed of precision like before
                    if (this.Module?.PolarModule != null)
                        if (this.Module.PolarModule.View != null)
                            if (displayType != SO.DisplayTypes.Precisions)
                            {
                                this.UpdateViewOfPreviousPoint(Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedType._Name, displayType);
                                return;
                            }

                    // else show as usual
                    UpdateOneOfTheMainNodeWith(1,
                        new TsuNode(Module?._MeasureManager, type));
                }
                catch (Exception ex)
                {
                    MessageTsu.ShowMessage(ex);
                }
            }

            public void UpdateViewOfPreviousPoint(string typeS, SO.DisplayTypes typeD)
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new System.Action(() => UpdateViewOfPreviousPoint(typeS, typeD)));
                    return;
                }
                TsuNode node = new TsuNode();
                node.BasedOn(Module._MeasureManager, typeS, typeD);

                UpdateOneOfTheMainNodeWith(1, node);

            }

            public void UpdateViewOfStationParameters(string nameOfNodeToOpen = "")
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new System.Action(() => UpdateViewOfStationParameters(nameOfNodeToOpen)));
                    return;
                }
                try
                {
                    if (this.Module == null) return;
                    UpdateOneOfTheMainNodeWith(0,
                        new TsuNode(Module.StationTheodolite.Parameters2));

                    if (nameOfNodeToOpen != "")
                    {
                        treeView.Nodes.Find(nameOfNodeToOpen, true)[0].Expand();
                    }
                }
                catch (Exception ex)
                {
                    MessageTsu.ShowMessage(ex);
                }

            }
            public void UpdateOneOfTheMainNodeWith(int nodeIndex, TsuNode newNode)
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new System.Action(() => UpdateOneOfTheMainNodeWith(nodeIndex, newNode)));
                    return;
                }
                //treeView.BeginUpdate(); // with this the main thread was sometime fully block on treeView.Nodes.Insert(nodeIndex, newNode);

                // Before mod, we check if it is open
                TreeNode n = treeView.Nodes[nodeIndex];
                bool WasExpanded = n.IsExpanded;
                List<bool> l = new List<bool>();
                foreach (TreeNode item in n.Nodes)
                {
                    if (item.IsExpanded)
                        l.Add(true);
                    else
                        l.Add(false);
                }
                // Remove the old node
                n.Remove();
                // Add the new one
                treeView.Nodes.Insert(nodeIndex, newNode);

                //Reexpand after mod
                n = treeView.Nodes[nodeIndex];
                if (WasExpanded) n.Expand();
                int i = 0;
                foreach (bool item in l)
                {
                    if (item)
                        if (i < n.Nodes.Count)
                            n.Nodes[i].Expand();
                    i++;
                }

                //treeView.EndUpdate(); // with this the main thread was sometime fully block on treeView.Nodes.Insert(nodeIndex, newNode);
            }

            internal void UpdateSomeMainNodes(bool stationParemeters, bool previousMeasures, bool nextMeasures)
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new System.Action(() => UpdateSomeMainNodes(stationParemeters, previousMeasures, nextMeasures)));
                    return;
                }

                if (stationParemeters) UpdateViewOfStationParameters();
                if (previousMeasures) UpdateViewOfPreviousPoint();
                if (nextMeasures) UpdateViewOfNextPoint();
            }



            #endregion

            internal void SetToVerticalised()
            {
                TryAction(Module.Verticalisation_SetToOn);
                UpdateViewOfStationParameters();
            }

            internal void SetToNotVerticalised()
            {
                string cancel = R.T_CANCEL;
                string doIt = R.T_VALID;
                string titleAndMessage = $"{R.T_ARE_YOU_SURE};{R.T_THIS_WILL_DISABLE_THE_INSTRUMENT_COMPENSATOR}";
                MessageInput mi = new MessageInput(MessageType.Important, titleAndMessage)
                {
                    ButtonTexts = new List<string> { cancel, doIt },
                    TextOfButtonToBeLocked = doIt
                };
                MessageResult messageResult = mi.Show();
                if (messageResult.TextOfButtonClicked == doIt)
                    TryAction(Module.Verticalisation_SetToOff);
                UpdateViewOfStationParameters();
            }

            internal void MoveNextPointToTop()
            {
                Module.MovetoTop(usedMeasure);
                Module.SendMeasureToInstrument(usedMeasure);
            }

            internal void MeasureNow()
            {
                Module.GotoWanted = true;
                Module.MeasureNow(usedMeasure);

            }

            public class NextPointEventArgs
            {
            }
        }
    }
}
