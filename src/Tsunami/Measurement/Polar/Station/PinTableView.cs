﻿using LibVLCSharp.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TSU.Common;
using TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using TSU.Common.Instruments;
using TSU.Tools;
using TSU.Views;
using TSU.Views.Message;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using static TSU.Polar.GuidedModules.Steps.StakeOut;
using BOC = TSU.Common.Compute.Compensations.BeamOffsets;
using Color = System.Drawing.Color;
using E = TSU.Common.Elements;
using M = TSU.Common.Measures;
using P = TSU.Preferences;
using R = TSU.Properties.Resources;
using SO = TSU.Polar.GuidedModules.Steps.StakeOut;

namespace TSU.Polar
{
    public partial class Station
    {

        public class PinTableView
        {
            Station.Module module;

            public PinTable_Buttons Buttons;

            internal bool isVisible = false;
            internal bool isInitializaling = false;

            private readonly double NaN = P.Preferences.NotAvailableValueNa;
            internal List<M.Measure> MeasuresRemovedLiveData = new List<M.Measure>();
            internal List<M.Measure> measuresContainingPointsPinned = new List<M.Measure>();
            internal Dictionary<string, int> pointsPinnedIndex = new Dictionary<string, int>();

            private readonly Color liveDataColor = Color.Purple;
            private readonly Color goodMeasColor = Tsunami2.Preferences.Values.GuiPrefs.Theme.Colors.LightForeground;
            private readonly Color questionMeasColor = Tsunami2.Preferences.Values.GuiPrefs.Theme.Colors.Attention;
            private readonly Color buttonColor = Tsunami2.Preferences.Values.GuiPrefs.Theme.Colors.Object;
            private readonly Color highlightColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
            private readonly Color cellBackColor = TSU.Tsunami2.Preferences.Theme.Colors.DarkBackground;
            private readonly Color cellForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            private readonly Color headerBackColor = TSU.Tsunami2.Preferences.Theme.Colors.DarkBackground;
            private readonly Color headerForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            private readonly Color selectionBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
            private readonly Color selectionForeColor = TSU.Tsunami2.Preferences.Theme.Colors.TextHighLightFore;


            public PinTableView(Station.Module module)
            {
                this.module = module;
                Buttons = new PinTable_Buttons(this);

                // Subscribe to BOC results changes to adapt type on the obsolete state
                Tsunami2.Properties.BOC_Context.ResultsUpdated -= OnBOCResultsUpdated;
                Tsunami2.Properties.BOC_Context.ResultsUpdated += OnBOCResultsUpdated;
            }

            private void OnBOCResultsUpdated(object sender, BOC.Results.EventArgs e)
            {
                var stationModule = sender as Polar.Station.Module;
                if (stationModule == null)
                    return;

                var polarModule = stationModule.ParentModule as Polar.Module;
                var updatedBOCResults = e.Results;
                foreach (var measure in measuresContainingPointsPinned)
                {
                    // Already obsolete?
                    if (measure._Status.Type == M.States.Types.Questionnable)
                        continue;
                    // not a BOC?
                    if (!measure._Point._Name.ToUpper().StartsWith("BEAM_"))
                        continue;
                    // update newly obsolete BOC results to a questionnable state
                    foreach (var item in updatedBOCResults)
                    {
                        if (measure._Point._Name.ToUpper().Contains(item.Id + "."))
                        {
                            measure._Status = new M.States.Questionnable(measure._Status.Type);
                        }
                    }
                }
                this.UpdateAllTable();
            }

            #region DataGrid View

            public DataGridView dataGridViewPinnedTable;
            private DataGridViewTextBoxColumn pointNum;
            private DataGridViewTextBoxColumn pointName;
            private DataGridViewTextBoxColumn xDisplay;
            private DataGridViewTextBoxColumn yDisplay;
            private DataGridViewTextBoxColumn zDisplay;
            private DataGridViewTextBoxColumn xExpDisplay;
            private DataGridViewTextBoxColumn zExpDisplay;
            private DataGridViewTextBoxColumn xMoveDisplay;
            private DataGridViewTextBoxColumn zMoveDisplay;
            private DataGridViewTextBoxColumn dcum;
            private DataGridViewImageColumn pointRemove;
            private DataGridViewImageColumn pointReset;
            private DataGridViewImageColumn pointNext;
            private DataGridViewImageColumn pointGoto;
            private DataGridViewImageColumn pointMeasure;
            private DataGridViewImageColumn pointGotoAndMeasure;
            private DataGridViewImageColumn pointLiveData;
            private DataGridViewImageColumn pointGotoAndLiveData;

            private DataGridViewImageColumn pointPrev;
            private readonly List<DataGridViewImageColumn> imageColumnList = new List<DataGridViewImageColumn>();
            private readonly List<DataGridViewTextBoxColumn> textBoxColumnList = new List<DataGridViewTextBoxColumn>();
            private Rectangle dragBoxFromMouseDown;
            private int rowIndexFromMouseDown;
            private int rowIndexOfItemUnderMouseToDrop;
            private System.Drawing.Point dataGridViewColClick;
            private bool firstClick = true;



            public void InitializeDataGridView()
            {
                dataGridViewPinnedTable = new DataGridView() { Dock = DockStyle.Fill };
                dataGridViewPinnedTable.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.True;

                SetStyleDataGridView();
                CreateColumnsDataGridView();
                dataGridViewPinnedTable.Rows.Clear();

                dataGridViewPinnedTable.CellClick += new DataGridViewCellEventHandler(DataGridViewPinnedTable_CellClick);
                dataGridViewPinnedTable.MouseDown += new MouseEventHandler(DataGridViewPinnedTable_MouseDown);
                dataGridViewPinnedTable.MouseMove += new MouseEventHandler(DataGridViewPinnedTable_MouseMove);
                dataGridViewPinnedTable.DragDrop += new DragEventHandler(DataGridViewPinnedTable_DragDrop);
                dataGridViewPinnedTable.DragOver += new DragEventHandler(DataGridViewPinnedTable_DragOver);
                dataGridViewPinnedTable.MouseClick += new MouseEventHandler(DataGridViewPinnedTable_MouseClick);


            }

            public void ShowOrHide()
            {
                if (isVisible)
                {
                    Hide();
                }
                else
                {
                    Show();
                }
            }

            private void Hide()
            {
                this.isVisible = false;// this change polar.module.view.updateView behaviour) 
                this.module.View.UpdateView();
                this.SetButtonForDataGridView(buttonColor, R.ShowTable, R.ShowPinnedPoints, false);
            }

            private void Show()
            {
                isVisible = true; // this change polar.module.view.updateView behaviour) 
                UpdateAllTable();
                this.module.View.UpdateView();
                this.SetButtonForDataGridView(highlightColor, R.HideTable, R.HidePinnedPoints, true);
            }

            public void SetStyleDataGridView()
            {

                DataGridViewContentAlignment midCenterAlign = DataGridViewContentAlignment.MiddleCenter;

                dataGridViewPinnedTable.ReadOnly = true;
                dataGridViewPinnedTable.AllowUserToResizeColumns = true;
                dataGridViewPinnedTable.AllowUserToResizeRows = false;
                dataGridViewPinnedTable.AllowDrop = true;
                dataGridViewPinnedTable.AllowUserToAddRows = false;
                dataGridViewPinnedTable.AllowUserToDeleteRows = false;
                dataGridViewPinnedTable.EnableHeadersVisualStyles = false;
                dataGridViewPinnedTable.RowHeadersVisible = false;
                dataGridViewPinnedTable.RowTemplate.Height = 40;
                dataGridViewPinnedTable.ScrollBars = ScrollBars.Both;
                dataGridViewPinnedTable.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
                dataGridViewPinnedTable.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
                dataGridViewPinnedTable.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                dataGridViewPinnedTable.SelectionMode = DataGridViewSelectionMode.FullRowSelect;


                // define default datagridview cell style
                this.dataGridViewPinnedTable.DefaultCellStyle.BackColor = cellBackColor;
                this.dataGridViewPinnedTable.DefaultCellStyle.ForeColor = cellForeColor;
                this.dataGridViewPinnedTable.DefaultCellStyle.SelectionBackColor = selectionBackColor;
                this.dataGridViewPinnedTable.DefaultCellStyle.SelectionForeColor = selectionForeColor;
                this.dataGridViewPinnedTable.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                this.dataGridViewPinnedTable.DefaultCellStyle.Alignment = midCenterAlign;
                this.dataGridViewPinnedTable.DefaultCellStyle.Font = TSU.Tsunami2.Preferences.Values.GuiPrefs.Theme.Fonts.Normal;

                // default column header style
                this.dataGridViewPinnedTable.ColumnHeadersDefaultCellStyle.BackColor = headerBackColor;
                this.dataGridViewPinnedTable.ColumnHeadersDefaultCellStyle.ForeColor = headerForeColor;
                this.dataGridViewPinnedTable.ColumnHeadersDefaultCellStyle.SelectionBackColor = selectionBackColor;
                this.dataGridViewPinnedTable.ColumnHeadersDefaultCellStyle.SelectionForeColor = selectionForeColor;
                this.dataGridViewPinnedTable.ColumnHeadersDefaultCellStyle.Alignment = midCenterAlign;
                this.dataGridViewPinnedTable.ColumnHeadersDefaultCellStyle.Font = TSU.Tsunami2.Preferences.Values.GuiPrefs.Theme.Fonts.MonospaceFont;


            }

            public string GetColumnName(string headerName, string measUnit)
            {
                string newHeaderName = headerName.Replace("_", " ");
                if (newHeaderName.Contains("Move")) return $"{newHeaderName} {measUnit}";
                return $"{newHeaderName} {measUnit}";
            }

            public void SetBeamVColumns(string measUnit)
            {
                List<string> headers = GetOffsetColumnsName();
                if (headers.Count >= 7 && TSU.Tsunami2.Properties.CoordinatesSystems.SelectedType._Name == "BEAMV" &&
                    (TSU.Tsunami2.Properties.ValueToDisplayType == DisplayTypes.Offsets ||
                    TSU.Tsunami2.Properties.ValueToDisplayType == DisplayTypes.Displacement))
                {
                    xExpDisplay.HeaderText = GetColumnName(headers[3], measUnit);
                    zExpDisplay.HeaderText = GetColumnName(headers[4], measUnit);
                    xMoveDisplay.HeaderText = GetColumnName(headers[5], measUnit);
                    zMoveDisplay.HeaderText = GetColumnName(headers[6], measUnit);
                    xDisplay.HeaderText = $"O{headers[0]} {measUnit}";
                    yDisplay.HeaderText = $"O{headers[1]} {measUnit}";
                    zDisplay.HeaderText = $"O{headers[2]} {measUnit}";

                    hideOrNotBEAMVColumns(true);
                }
                else hideOrNotBEAMVColumns(false);
            }
            public void SetColumnsDataGridView()
            {
                //Set column name depending on the type of coordinate system and type of display (offset, displacements, coordination)                
                List<string> headers = GetOffsetColumnsName();
                string measUnit = "[mm]";

                var displayType = TSU.Tsunami2.Properties.ValueToDisplayType;

                if (displayType == SO.DisplayTypes.Displacement)
                {
                    xDisplay.HeaderText = $"D{headers[0]} {measUnit}";
                    yDisplay.HeaderText = $"D{headers[1]} {measUnit}";
                    zDisplay.HeaderText = $"D{headers[2]} {measUnit}";
                    SetBeamVColumns(measUnit);
                }
                else if (displayType == SO.DisplayTypes.Offsets)
                {
                    xDisplay.HeaderText = $"O{headers[0]} {measUnit}";
                    yDisplay.HeaderText = $"O{headers[1]} {measUnit}";
                    zDisplay.HeaderText = $"O{headers[2]} {measUnit}";
                    SetBeamVColumns(measUnit);
                }
                else if (displayType == SO.DisplayTypes.Coordinates)
                {
                    xDisplay.HeaderText = headers[0];
                    yDisplay.HeaderText = headers[1];
                    zDisplay.HeaderText = headers[2];
                    hideOrNotBEAMVColumns(false);
                }
                else if (displayType == SO.DisplayTypes.Observations)
                {
                    xDisplay.HeaderText = "Hz [gon]";
                    yDisplay.HeaderText = "V [gon]";
                    zDisplay.HeaderText = "D [m]";
                    hideOrNotBEAMVColumns(false);
                }
            }

            private void SetStyleTextColumn()
            {
                var sortMode = DataGridViewColumnSortMode.NotSortable;
                var alignMode = DataGridViewContentAlignment.MiddleCenter;

                var minWidthColName = 50;
                var defWidthColName = 300;
                var defWidthColumns = 150;
                var defWidthColNo = 50;
                var minWidthColumns = 20;

                foreach (var item in textBoxColumnList)
                {
                    item.DefaultCellStyle.Alignment = alignMode;
                    string colName = item.Name;
                    if (colName != R.ColName)
                    {
                        item.Width = colName != R.ColNo ? defWidthColumns : defWidthColNo;
                        item.MinimumWidth = minWidthColumns;
                        dataGridViewPinnedTable.Columns[item.Index].SortMode = sortMode;
                    }
                    else
                    {
                        item.MinimumWidth = minWidthColName;
                        item.Width = defWidthColName;
                    }
                }
            }
            private void SetStyleImageColumn()
            {
                DataGridViewImageCellLayout zoom = DataGridViewImageCellLayout.Zoom;
                DataGridViewTriState @false = DataGridViewTriState.False;
                Color selectionBackColor = Tsunami2.Preferences.Values.GuiPrefs.Theme.Colors.LightBackground;
                var minWidthImageColumn = 35;
                var defaultHeaderText = "";

                foreach (var item in imageColumnList)
                {
                    item.Width = minWidthImageColumn;
                    item.ImageLayout = zoom;
                    item.Resizable = @false;
                    item.DefaultCellStyle.SelectionBackColor = selectionBackColor;
                    item.HeaderText = defaultHeaderText;
                }
            }
            private void SetIconAndToolTipText(int rowIndex, string colName, Bitmap icon, string tooltipText)
            {

                dataGridViewPinnedTable.Rows[rowIndex].Cells[colName].Value = icon;
                dataGridViewPinnedTable.Rows[rowIndex].Cells[colName].ToolTipText = tooltipText;

            }
            private void SetButtonForDataGridView(Color buttonColor, String title, String description, bool status)
            {
                Buttons.ShowPinnedTable.SetColors(buttonColor);
                Buttons.ShowPinnedTable.Title = title;
                Buttons.ShowPinnedTable.Description = description;
                this.isVisible = status;
                module.pinnedTableStatus = status;

            }


            public void CreateColumnsDataGridView()
            {

                //text cells
                CreateTextColumn();
                SetStyleTextColumn();

                // button cells            
                CreateImageColumn();
                SetStyleImageColumn();

                // Name of offset column
                SetColumnsDataGridView();

            }

            private void CreateTextColumn()
            {
                pointNum = new DataGridViewTextBoxColumn
                {
                    Name = R.ColNo,
                    HeaderText = R.ColNo + "."
                };

                pointName = new DataGridViewTextBoxColumn
                {
                    Name = R.ColName,
                    HeaderText = R.ColName,
                };
                xDisplay = new DataGridViewTextBoxColumn { Name = R.ColX };
                yDisplay = new DataGridViewTextBoxColumn { Name = R.ColY };
                zDisplay = new DataGridViewTextBoxColumn { Name = R.ColZ };
                xExpDisplay = new DataGridViewTextBoxColumn { Name = "Exp_" + R.ColX };
                zExpDisplay = new DataGridViewTextBoxColumn { Name = "Exp_" + R.ColZ };
                xMoveDisplay = new DataGridViewTextBoxColumn { Name = "Move_" + R.ColX };
                zMoveDisplay = new DataGridViewTextBoxColumn { Name = "Move_" + R.ColZ };
                dcum = new DataGridViewTextBoxColumn
                {
                    Name = R.ColDcum,
                    HeaderText = R.ColDcum + " [m]"
                };

                textBoxColumnList.AddRange(new DataGridViewTextBoxColumn[] {
                    pointNum,
                    pointName,
                    xDisplay,
                    yDisplay,
                    zDisplay,
                    xExpDisplay,
                    zExpDisplay,
                    xMoveDisplay,
                    zMoveDisplay,
                    dcum});

                foreach (DataGridViewTextBoxColumn col in textBoxColumnList)
                    dataGridViewPinnedTable.Columns.Add(col);
            }

            private void hideOrNotBEAMVColumns(bool beamv)
            {
                foreach (DataGridViewColumn column in dataGridViewPinnedTable.Columns)
                {
                    if (column.Name.Equals("Exp_" + R.ColX) || column.Name.Equals("Move_" + R.ColX))
                    {
                        column.Visible = beamv && (this.module.PolarModule.RadialRabot.Count > 0);
                    }
                    else if (column.Name.Equals("Exp_" + R.ColZ) || column.Name.Equals("Move_" + R.ColZ))
                    {
                        column.Visible = beamv && (this.module.PolarModule.VerticalRabot.Count > 0);
                    }
                }
            }



            private void CreateImageColumn()
            {
                pointPrev = new DataGridViewImageColumn { Name = R.ColPrev };
                pointNext = new DataGridViewImageColumn { Name = R.ColNext };
                pointGoto = new DataGridViewImageColumn { Name = R.ColGoto };
                pointMeasure = new DataGridViewImageColumn { Name = R.ColMeasure };
                pointGotoAndMeasure = new DataGridViewImageColumn { Name = R.ColGotoAndMeasure };
                pointLiveData = new DataGridViewImageColumn { Name = R.ColLiveData };
                pointGotoAndLiveData = new DataGridViewImageColumn { Name = R.ColGotoAndLiveData };
                pointReset = new DataGridViewImageColumn { Name = R.ColReset };
                pointRemove = new DataGridViewImageColumn { Name = R.ColRemove };
                imageColumnList.AddRange(new DataGridViewImageColumn[]
                {
                    pointPrev,
                    pointNext,
                    pointGoto,
                    pointMeasure,
                    pointGotoAndMeasure,
                    pointLiveData,
                    pointGotoAndLiveData,
                    pointReset,
                    pointRemove
                });

                foreach (var col in imageColumnList)
                    dataGridViewPinnedTable.Columns.Add(col);
            }


            public void UpdateDataGridViewOffsetColumn()
            {
                SetColumnsDataGridView();
                UpdateOffsetRows();
            }
            private List<string> GetOffsetColumnsName()
            {

                List<Common.CoordinateSystem> csAll = Tsunami2.Properties.CoordinatesSystems.AllTypes;
                Common.CoordinateSystem csType = TSU.Tsunami2.Properties.CoordinatesSystems.SelectedType;
                List<string> headers;

                int index = csAll.FindIndex(x => x._Name == csType._Name);
                headers = index >= 0 ? csAll[index].AxisNames : new List<string>() { NaN.ToString(), NaN.ToString(), NaN.ToString() };
                List<string> extraHeaders = new List<string>() { "ExpO_Rbv", "ExpO_Hbv", "Move_Rbv", "Move_Hbv" };
                if (csType._Name == "BEAMV" && this.module.FinalModule.RabotStrategy != Common.Compute.Rabot.Strategy.Not_Defined && !headers.Contains("Expected_Rbv")) headers.AddRange(extraHeaders);
                for (int i = 0; i < headers.Count; i++)
                {
                    string item = headers[i];
                    item += (TSU.Tsunami2.Properties.ValueToDisplayType == DisplayTypes.Coordinates) ? "[m]" : "[mm]";
                }

                return headers;

            }

            public void UpdateLastMeasInDataGridViewOffsetColumn(Polar.Measure measure)
            {
                PinNewMeasToPinnedPoint(measure);

            }

            #endregion

            #region Split Container

            internal SplitContainer splitContainerWithPinTableInPanel2;

            [Flags]
            public enum SplitContainerPosition
            {
                None = 0,
                Horizontal = 1,
                Vertical = 2,
                Parent = 4,
                GrandParent = 8,

                HorizontalInParent = Horizontal | Parent,
                VerticalInParent = Vertical | Parent,

                HorizontalInGrandParent = Horizontal | GrandParent,
                VerticalInGrandParent = Vertical | GrandParent
            }

            public static SplitContainerPosition SplitPanel_GetNextPosition(SplitContainerPosition position)
            {
                switch (position)
                {
                    case SplitContainerPosition.HorizontalInParent:
                        return SplitContainerPosition.VerticalInParent;
                    case SplitContainerPosition.VerticalInParent:
                        return SplitContainerPosition.HorizontalInGrandParent;
                    case SplitContainerPosition.HorizontalInGrandParent:
                    case SplitContainerPosition.VerticalInGrandParent:
                    case SplitContainerPosition.None:
                    case SplitContainerPosition.Horizontal:
                    case SplitContainerPosition.Vertical:
                    case SplitContainerPosition.Parent:
                    case SplitContainerPosition.GrandParent:
                    default:
                        return SplitContainerPosition.HorizontalInParent;
                }
            }

            public static Orientation SplitPanel_GetOrientation(SplitContainerPosition position)
            {
                return (position & SplitContainerPosition.Vertical) != 0 ? Orientation.Vertical : Orientation.Horizontal;
            }

            public bool SplitPanel_IsGrandParent(SplitContainerPosition position)
            {
                return (position & SplitContainerPosition.GrandParent) != 0;
            }

            public SplitContainer SplitPanel_GetOrCreate()
            {
                if (splitContainerWithPinTableInPanel2 == null)
                {
                    var position = TSU.Tsunami2.Preferences.Values.GuiPrefs.PinnedPointsViewPosition;
                    Orientation orientation = SplitPanel_GetOrientation(position);

                    // split splitContainer into two Panels (treeview + datagridview)
                    splitContainerWithPinTableInPanel2 = new SplitContainer
                    {
                        Dock = DockStyle.Fill,
                        Orientation = orientation,
                        SplitterWidth = 20,
                        SplitterDistance = module.View.splitContainerStationVsInstrument.Panel1.Height / 3
                    };
                    module.View.toolTipTsuView.SetToolTip(splitContainerWithPinTableInPanel2, R.T_DOUBLE_CLICK_TO_SWITCH_BETWEEN_VERTICAL_AND_HORIZONTAL_SPLITTING);

                    splitContainerWithPinTableInPanel2.DoubleClick += SplitPanel_ChangePosition;

                    bool shouldSaveNEwValue = false;
                    splitContainerWithPinTableInPanel2.SplitterMoving += delegate
                    {
                        shouldSaveNEwValue = true;
                    };

                    splitContainerWithPinTableInPanel2.SplitterMoved += delegate
                    {
                        if (shouldSaveNEwValue)
                        {
                            Tsunami2.Preferences.Values.GuiPrefs.PintableSplitDistance = splitContainerWithPinTableInPanel2.SplitterDistance;
                            Tsunami2.Preferences.Values.SaveGuiPrefs();
                        }
                        shouldSaveNEwValue = false;
                    };
                }
                return splitContainerWithPinTableInPanel2;
            }

            private void SplitPanel_ChangePosition(object sender, EventArgs e)
            {
                var pos = TSU.Tsunami2.Preferences.Values.GuiPrefs.PinnedPointsViewPosition;
                pos = SplitPanel_GetNextPosition(pos);
                TSU.Tsunami2.Preferences.Values.GuiPrefs.PinnedPointsViewPosition = pos;
                TSU.Tsunami2.Preferences.Values.SaveGuiPrefs();

                //Orientation orientation = SplitPanel_GetOrientation(position);

                //if (SplitPanel_IsGrandParent(position))
                //{
                //    var instrumentViewPanel = module.View.splitContainerStationVsInstrument.Panel2;
                //    var tabelviewViewPanel = (parent.Controls[0] as SplitContainer).Panel2;

                //    // switch table with instrument
                //    Control[] tempControls = instrumentViewPanel.Controls.Cast<Control>().ToArray();
                //    instrumentViewPanel.Controls.Clear();
                //    instrumentViewPanel.Controls.AddRange(tabelviewViewPanel.Controls.Cast<Control>().ToArray());
                //    tabelviewViewPanel.Controls.Clear();
                //    tabelviewViewPanel.Controls.AddRange(tempControls);
                //}
                //else
                //{
                //    // change orientation datagrid splitcontainer
                //    splitContainerWithPinTableInPanel2.Orientation = orientation;

                //    int size = orientation == Orientation.Horizontal ? parent.Height : parent.Width;
                //    splitContainerWithPinTableInPanel2.SplitterDistance = 2 * (size / 3);
                //    //ShowPinnedPointsTable();
                //}
                this.module.UpdateView();

            }

            #endregion

            #region PopUp Menu

            internal void CreatePopUpMenuDataGridView()
            {
                List<Control> pinnedTablePopupMenu = new List<Control>();
                pinnedTablePopupMenu.Clear();
                pinnedTablePopupMenu.Add(Buttons.ResetAllPointsMeasured);
                pinnedTablePopupMenu.Add(Buttons.BOC_Menu);
                pinnedTablePopupMenu.Add(Buttons.RollComute);
                pinnedTablePopupMenu.Add(Buttons.Pin_Menu);
                pinnedTablePopupMenu.Add(Buttons.DisplayOption);
                //pinnedTablePopupMenu.Add(Buttons.PinAllPointsMeasured);
                //pinnedTablePopupMenu.Add(Buttons.PinAllPointsNext);
                //pinnedTablePopupMenu.Add(Buttons.RemoveAllPin);
                pinnedTablePopupMenu.Add(Buttons.Refresh);

                this.module.View.ShowPopUpMenu(pinnedTablePopupMenu, "pinned Table");
            }


            private void CreateVisualDetailSubMenu()
            {

                SO.CoordinateButtons.SetAction(() =>
                {
                    var displayType = Tsunami2.Properties.ValueToDisplayType;
                    SO.CoordinateButtons.WarnAboutNoneCartesianCS(this.module._TsuView);
                    this.UpdateDataGridViewOffsetColumn();
                    this.module.View.UpdateViewOfPreviousPoint(TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedType._Name, displayType);

                });

                List<Control> cm = new List<Control>();
                cm.AddRange(SO.CoordinateButtons.GetControlsList());
                cm.Insert(2, new Label());
                cm.Insert(2, new Label());
                cm.Insert(2, new Label());
                this.module.View.ShowPopUpSubMenu(cm, "CS ");


            }



            #endregion

            #region Dragging and Clicking in DataGrid

            private void DataGridViewPinnedTable_CellClick(object sender, DataGridViewCellEventArgs e)
            {
                try
                {
                    // Check if click on Remove, Prev or Next button in DataGridView
                    int row = e.RowIndex;
                    var col = e.ColumnIndex;
                    if (row == -1) //header
                    {
                        if (col > 1 && col < 5) // coord.
                        {
                            CreateVisualDetailSubMenu();
                        }
                    }
                    else if (row >= 0)
                    {
                        // Ignore the MouseClick just after a CellClick,
                        // to prevent showing the menu when we click the delete button
                        lastCellClick = DateTime.UtcNow;

                        if (col == pointRemove.Index)
                            RemovePinnedPoint(row);
                        else if (col == pointPrev.Index)
                            ShowPrevPinnedPoint(row);
                        else if (col == pointNext.Index)
                            ShowNextPinnedPoint(row);
                        else if (col == pointReset.Index)
                            ResetPinnedPoint(row);
                        else if (col == pointGoto.Index)
                            MeasurePointFrom(row, doGoto: true, measureNow: false, liveDataNow: false);
                        else if (col == pointMeasure.Index)
                            MeasurePointFrom(row, doGoto: false, measureNow: true, liveDataNow: false);
                        else if (col == pointGotoAndMeasure.Index)
                            MeasurePointFrom(row, doGoto: true, measureNow: true, liveDataNow: false);
                        else if (col == pointLiveData.Index)
                            MeasurePointFrom(row, doGoto: false, measureNow: false, liveDataNow: true);
                        else if (col == pointGotoAndLiveData.Index)
                            MeasurePointFrom(row, doGoto: true, measureNow: false, liveDataNow: true);
                    }
                }
                catch (Exception ex)
                {
                    new MessageInput(MessageType.Critical, ex.Message).Show();
                }
            }


            private void DataGridViewPinnedTable_MouseDown(object sender, MouseEventArgs e)
            {

                rowIndexFromMouseDown = dataGridViewPinnedTable.HitTest(e.X, e.Y).RowIndex;
                int colIndexFromMouseDown = dataGridViewPinnedTable.HitTest(e.X, e.Y).ColumnIndex;

                if (rowIndexFromMouseDown != -1)
                {
                    Size dragSize = SystemInformation.DragSize;
                    dragBoxFromMouseDown = new Rectangle(new System.Drawing.Point(e.X - (dragSize.Width / 2), e.Y - (dragSize.Height / 2)), dragSize);

                    if (e.Button == MouseButtons.Right)
                    {
                        try
                        {
                            dataGridViewPinnedTable.CurrentCell = dataGridViewPinnedTable.Rows[rowIndexFromMouseDown].Cells[colIndexFromMouseDown];
                            dataGridViewPinnedTable.Rows[rowIndexFromMouseDown].Selected = true;
                            dataGridViewPinnedTable.Focus();

                        }
                        catch (Exception)
                        {

                        }
                    }
                }
                else
                    dragBoxFromMouseDown = Rectangle.Empty;


            }

            private void DataGridViewPinnedTable_MouseMove(object sender, MouseEventArgs e)
            {
                if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
                {
                    if (dragBoxFromMouseDown != Rectangle.Empty && !dragBoxFromMouseDown.Contains(e.X, e.Y) && rowIndexFromMouseDown >= 0)
                    {
                        dataGridViewPinnedTable.DoDragDrop(dataGridViewPinnedTable.Rows[rowIndexFromMouseDown], DragDropEffects.Move);
                        dataGridViewPinnedTable.ClearSelection();
                        dataGridViewPinnedTable.Rows[rowIndexFromMouseDown].Selected = true;
                    }
                }
            }

            private void DataGridViewPinnedTable_DragOver(object sender, DragEventArgs e)

            {
                e.Effect = DragDropEffects.Move;
            }

            private void DataGridViewPinnedTable_DragDrop(object sender, DragEventArgs e)
            {

                System.Drawing.Point clientPoint = dataGridViewPinnedTable.PointToClient(new System.Drawing.Point(e.X, e.Y));
                rowIndexOfItemUnderMouseToDrop = dataGridViewPinnedTable.HitTest(clientPoint.X, clientPoint.Y).RowIndex;

                if (e.Effect == DragDropEffects.Move)
                {
                    DataGridViewRow rowToMove = e.Data.GetData(typeof(DataGridViewRow)) as DataGridViewRow;
                    if (rowToMove.Index >= 0 && rowIndexOfItemUnderMouseToDrop >= 0)
                    {

                        dataGridViewPinnedTable.Rows.RemoveAt(rowIndexFromMouseDown);
                        dataGridViewPinnedTable.Rows.Insert(rowIndexOfItemUnderMouseToDrop, rowToMove);
                    }
                }
            }

            private DateTime lastCellClick = DateTime.MinValue;

            private void DataGridViewPinnedTable_MouseClick(object sender, MouseEventArgs e)
            {
                // Ignore the MouseClick just after a CellClick,
                // to prevent showing the menu when we click the delete button
                if (DateTime.UtcNow - lastCellClick < TimeSpan.FromSeconds(1))
                    return;

                if (firstClick)
                {
                    dataGridViewColClick = new System.Drawing.Point(e.X, e.Y);
                    firstClick = false;
                }

                int col = dataGridViewPinnedTable.HitTest(e.X - 30, e.Y - 10).ColumnIndex;
                System.Drawing.Point clickPoint = new System.Drawing.Point(e.X, e.Y);

                if (col < 0)
                {
                    if (!clickPoint.Equals(dataGridViewColClick))
                    {
                        CreatePopUpMenuDataGridView();
                    }
                    else
                    {
                        clickPoint.X += 1;
                        clickPoint.Y += 1;
                    }
                    dataGridViewColClick = clickPoint;
                }
            }

            #endregion 

            #region Manipulating Data in DataGrid
            private void RemovePinnedPoint(int rowIndex)
            {
                // remove point from datagridview table
                string pName = GetPinnedPointName(rowIndex);
                try
                {
                    // remove from datagrid table
                    dataGridViewPinnedTable.Rows.RemoveAt(rowIndex);
                    dataGridViewPinnedTable.Refresh();
                    // remove points from dictionary and list
                    RemovePointFromPinnedPoints(pName);
                    module.pinnedPointName.Remove(pName);
                    this.UpdateXMLFile("Remove point from table");
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    throw new Exception($"Could not remove row in pinned points table : {ex.Message}");
                }
            }

            private void RemovePinnedPoints()
            {
                // remove all points from datagridview table
                string titleAndMessage = String.Format(R.CleanTabConf);
                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL }
                };
                string respond = mi.Show().TextOfButtonClicked;
                if (respond == R.T_OK)
                {
                    foreach (DataGridViewRow row in dataGridViewPinnedTable.Rows)
                    {
                        var pName = GetPinnedPointName(row.Index);
                        RemovePointFromPinnedPoints(pName);
                        module.pinnedPointName.Remove(pName);
                    }
                    dataGridViewPinnedTable.Rows.Clear();
                    this.UpdateXMLFile("Remove points from table");
                }
            }

            private void ShowPrevPinnedPoint(int rowIndex)
            {
                // Show previous measurement value for point pinned in the dataGridView
                string pName = GetPinnedPointName(rowIndex);
                if (pointsPinnedIndex.ContainsKey(pName) && pointsPinnedIndex[pName] > 0)
                {
                    pointsPinnedIndex[pName] -= 1;
                    UpdateRow(rowIndex, GetAllMeasuresInPinTable(pName));
                }
            }

            private void ShowNextPinnedPoint(int rowIndex)
            {
                // Show next measurement value for point pinned in the dataGridView
                string pName = GetPinnedPointName(rowIndex);
                if (pointsPinnedIndex.ContainsKey(pName))
                {
                    var pIndex = pointsPinnedIndex[pName];
                    List<M.Measure> pointsMeasured = GetAllMeasuresInPinTable(pName);
                    if (pIndex >= 0 && pIndex < (pointsMeasured.Count - 1))
                    {
                        pointsPinnedIndex[pName] += 1;
                        UpdateRow(rowIndex, pointsMeasured);
                    }
                }
            }

            private void AddNewPinnedPoint(double x, double y, double z, double d, string pointName, int measCount, Color color)
            {

                int pIndex = pointsPinnedIndex[pointName];
                int rIndex = dataGridViewPinnedTable.Rows.Add();
                // Add new button cells to dataGridView
                SetIconAndToolTipText(rIndex, R.ColRemove, R.StatusBad, R.DelPinPoint);
                SetIconAndToolTipText(rIndex, R.ColReset, R.Pinned_Reset, R.ResetPin);
                var iconPrev = measCount == 1 ? R.LeftNotOk : R.LeftOk;
                SetIconAndToolTipText(rIndex, R.ColPrev, iconPrev, R.ShowPrevM);
                SetIconAndToolTipText(rIndex, R.ColNext, R.RightNotOk, R.ShowNextM);
                SetIconAndToolTipText(rIndex, R.ColGoto, R.At40x_Goto, R.ColGoto);
                SetIconAndToolTipText(rIndex, R.ColMeasure, R.At40x_Meas, R.ColMeasure);
                SetIconAndToolTipText(rIndex, R.ColGotoAndLiveData, R.At40x_LiveGoto, R.ColGotoAndLiveData);
                SetIconAndToolTipText(rIndex, R.ColLiveData, R.At40x_Live, R.ColLiveData);
                SetIconAndToolTipText(rIndex, R.ColGotoAndMeasure, R.At40x_GotoAll, R.ColGotoAndMeasure);

                // Add new value to text cells to dataGridView
                dataGridViewPinnedTable.Rows[rIndex].Cells[R.ColName].Value = pointName;
                UpdatePointsNumber(rIndex, pIndex, measCount);
                UpdateOffsetRow(rIndex, x, y, z);
                if (this.module.PolarModule.RadialRabot.ContainsKey(pointName) || this.module.PolarModule.VerticalRabot.ContainsKey(pointName) && TSU.Tsunami2.Properties.CoordinatesSystems.SelectedType._Name == "BEAMV")
                    UpdateOffsetRowExpectedMove(rIndex, x, y, z, pointName);
                UpdateDcum(rIndex, d);
                UpdateRowColor(rIndex, color);

                // Add point to list to save in XML file
                if (!module.pinnedPointName.Contains(pointName))
                    module.pinnedPointName.Add(pointName);
            }


            private int GetPinnedPointRowIndex(string pointName)
            {
                // Get row index in DataGridView of given point
                DataGridViewRow row = dataGridViewPinnedTable.Rows
                                            .Cast<DataGridViewRow>()
                                            .First(r => r.Cells[R.ColName].Value.ToString().Equals(pointName));
                return row.Index;
            }

            private string GetPinnedPointName(int rowIndex)
            {
                // Get point name from given row in DataGridView
                return dataGridViewPinnedTable.Rows[rowIndex].Cells[R.ColName].Value.ToString();
            }

            private int GetPinnedPointsCount(string pointName)
            {
                // Count number of points with given name, inside dataGridView
                int pointCount = dataGridViewPinnedTable.Rows
                            .Cast<DataGridViewRow>()
                            .Where(row => row.Cells[R.ColName].Value.ToString().ToUpper() == pointName)
                            .Count();
                return pointCount;
            }
            private bool CheckIfPointIsInTable(string pointName)
            {
                // Check if point is already inside dataGridView
                bool pContain = dataGridViewPinnedTable.Rows
                            .Cast<DataGridViewRow>()
                            .Any(row => row.Cells[R.ColName].Value.ToString().ToUpper() == pointName);

                return pContain;
            }

            private void ResetPinnedPoints()
            {
                // Set all measurements from all points as obsolete
                int pChanged = 0;
                foreach (DataGridViewRow row in dataGridViewPinnedTable.Rows)
                {
                    pChanged += SetMeasurementsAsQuestionnable(row.Index);
                }
                if (pChanged > 0)
                    this.UpdateXMLFile("State of measurements changed");
            }

            private void ResetPinnedPoint(int rowIndex)
            {
                // Set all measurements from one points as obsolete
                int pChanged = SetMeasurementsAsQuestionnable(rowIndex);
                if (pChanged > 0)
                    this.UpdateXMLFile("State of measurement change");
            }

            private void MeasurePointFrom(int rowIndex, bool doGoto, bool measureNow, bool liveDataNow)
            {
                // Set all measurements from one points as obsolete
                string pName = GetPinnedPointName(rowIndex);

                if (pName.StartsWith("BEAM_"))
                {
                    new MessageInput(MessageType.Warning, "Cannot measure beam points").Show();
                    return;
                }

                try
                {
                    if (pointsPinnedIndex.ContainsKey(pName))
                    {
                        bool measuresExistIntheToDoList = false;
                        bool letUserModifyParametersBeforeMeasuring = false;
                        bool resetMeasure = false;
                        Measure measureFromTheToDoList = null;
                        Measure measureToUse = null;
                        bool useCloneOfPintTableMeasure = false;
                        Measure selectedMeasureFromPinTable = null;

                        // is there a measure in the todo list?
                        var existingMeasuresToDoForThisPoint = module.StationTheodolite.MeasuresToDo.FindAll(x => x._Point._Name == pName);
                        if (existingMeasuresToDoForThisPoint.Count > 0)
                        {
                            measuresExistIntheToDoList = true;
                            measureFromTheToDoList = existingMeasuresToDoForThisPoint[0] as Polar.Measure;
                        }

                        // is there already a measure in the pin table?
                        List<M.Measure> pointsMeasured = GetAllMeasuresInPinTable(pName);
                        selectedMeasureFromPinTable = pointsMeasured.LastOrDefault(x => x.IsFake == false) as Polar.Measure;
                        bool pointAlreadyMeasuredInPinnedTable = selectedMeasureFromPinTable != null;

                        // choose the measure to use
                        if (pointAlreadyMeasuredInPinnedTable)
                        {
                            if (measuresExistIntheToDoList)
                            {
                                // compare them, il they are the same use the one already exsiting in the todo list
                                var identicalExistingMeasureToDoForThisPoint = module.StationTheodolite.MeasuresToDo
                                    .OfType<Polar.Measure>() // Filters items of type Polar.Measure
                                    .Where(x =>
                                        x._Point._Name == pName
                                        && x.Extension == selectedMeasureFromPinTable.Extension
                                        && x.Interfaces == selectedMeasureFromPinTable.Interfaces
                                        && x.Face == selectedMeasureFromPinTable.Face
                                        && x.NumberOfMeasureToAverage == selectedMeasureFromPinTable.NumberOfMeasureToAverage
                                        && x._Point.SocketCode == selectedMeasureFromPinTable._Point.SocketCode).ToList();
                                
                                if (identicalExistingMeasureToDoForThisPoint.Count > 0)
                                {
                                    measureToUse = identicalExistingMeasureToDoForThisPoint[0];
                                }
                                else
                                {
                                    // need to ask
                                    var r = new MessageInput(MessageType.Warning,
                                           "Which measurement parameters to used?" +
                                           ";" +
                                           "You already have a measurement with different parameters prepared in the next point(s) list for this point, do you want to use this one? (or to reproduce the measurement that ended up in the pin table)")
                                    { ButtonTexts = new List<string>() { R.T_YES, $"{R.T_NO}" } }.Show();
                                    if (r.TextOfButtonClicked == R.T_YES)
                                        measureToUse = measureFromTheToDoList;
                                    else
                                    {
                                        useCloneOfPintTableMeasure = true;
                                        resetMeasure = true;
                                    }
                                }
                            }
                            else
                            {
                                // use the measure from the pintable
                                useCloneOfPintTableMeasure = true;
                                resetMeasure = true;
                            }
                        }
                        else
                        {
                            if (measuresExistIntheToDoList)
                            {
                                measureToUse = measureFromTheToDoList;
                            }
                            else
                            {
                                // we have to create a new measure based on the default measurement parameters
                                // find the point in element manager and create new  measurement with default settings
                                var point = Tsunami2.Properties.Points.FirstOrDefault(x => x._Name == pName && !(x.State is Element.States.Bad));
                                if (point != null)
                                {
                                    this.module.Use(module =>
                                    {
                                        measureToUse = module.CreateMeasureFromPoint(point, new M.States.Unknown(), true, out _);
                                        letUserModifyParametersBeforeMeasuring = true;
                                        new MessageInput(MessageType.GoodNews, "Measurement prepared for you but;As this is the first time you measure this point, I let you modify the measurement parameters.").Show();
                                    });
                                }
                            }
                        }

                        if (useCloneOfPintTableMeasure)
                        {
                            measureToUse = selectedMeasureFromPinTable.Clone() as Polar.Measure;
                            measureToUse.Clean(removeAnglesForGoto:true); // to reset the goto
                        }

                        // use the measure
                        if (measureToUse != null)
                        {
                            this.module.Use(module =>
                            {
                                // trevview update
                                if (resetMeasure)
                                    module.SetNextPointToAlreadyMeasured(measureToUse, askName: false, iterate: false);
                                else
                                {
                                    module.MovetoTop(measureToUse);
                                    module.SendMeasureToInstrument(measureToUse);
                                }

                                // goto ?
                                module.N_A_GotoMessageWanted = doGoto;
                                if (doGoto) 
                                    module.Goto(measureToUse);

                                // measure ?
                                if (measureNow && !letUserModifyParametersBeforeMeasuring) 
                                    module.MeasureNow();

                                // livedata ?
                                if (liveDataNow)
                                {
                                    if (module?._InstrumentManager?.SelectedInstrument?._Model.ToUpper().Contains("AT40") == true)
                                    {
                                        var at40x = module._InstrumentManager.SelectedInstrumentModule as Common.Instruments.Device.AT40x.Module;
                                        at40x.LiveData.Start();
                                    }
                                }
                            });
                        }
                    }
                }
                catch (CancelException ex)
                {
                    Logs.Log.AddEntryAsFYI(this.module, "Measure from Pin Point Table cancelled");
                }
                catch (Exception ex)
                {
                    new MessageInput(MessageType.Critical, $"Measure not possible from Pin Point Table;{ex.Message}").Show();
                }
            }

            public void ReCreatePinnedTable()
            {
                // Recreate table after close based on XMLfile
                if (module.pinnedPointName.Count > 0)
                    foreach (var name in module.pinnedPointName)
                        PinAPoint(name);

                if (module.pinnedTableStatus)
                    ShowOrHide();
            }

            #endregion

            #region Updating DataGrid View
            private void UpdateRow(int rowIndex, List<M.Measure> pointsMeasured)
            {
                // Update all informations in one row
                if (pointsMeasured.Count > 0)
                {
                    (double x, double y, double z) = (NaN, NaN, NaN);
                    var color = this.goodMeasColor;
                    var pName = pointsMeasured[0]._PointName;
                    (x, y, z) = GetValuesFromPinnedPoints(pointsMeasured);
                    color = GetColorFromPinnedPoints(pointsMeasured);

                    int currIndex = pointsPinnedIndex[pName];
                    int lastIndex = pointsMeasured.Count;
                    UpdatePointsNumber(rowIndex, currIndex, lastIndex); // update numbers
                    UpdateOffsetRow(rowIndex, x, y, z); // update offset
                    if (this.module.PolarModule.RadialRabot.ContainsKey(pName) || this.module.PolarModule.VerticalRabot.ContainsKey(pName) && TSU.Tsunami2.Properties.CoordinatesSystems.SelectedType._Name == "BEAMV")
                        UpdateOffsetRowExpectedMove(rowIndex, x, y, z, pName);
                    UpdatePrevNextRow(rowIndex, currIndex, lastIndex); // update Next/Prev
                    UpdateRowColor(rowIndex, color); // update type
                }
            }

            internal void UpdateOffsetRow(int rowIndex, double x, double y, double z)
            {
                // Update values of offset/displacement/coordinate
                try
                {
                    if (TSU.Tsunami2.Properties.ValueToDisplayType == DisplayTypes.Displacement &&
                        this.module.PolarModule.RabotStrategy != Common.Compute.Rabot.Strategy.Not_Defined)
                    {
                        x = x * -1;
                        y = y * -1;
                        z = z * -1;
                    }
                    this.SetOffsetValue(x, rowIndex, R.ColX);
                    this.SetOffsetValue(y, rowIndex, R.ColY);
                    this.SetOffsetValue(z, rowIndex, R.ColZ);
                }
                catch (Exception) { }

            }
            internal void UpdateOffsetRowExpectedMove(int rowIndex, double x, double y, double z, string pName)
            {
                // Update values of offset/displacement/coordinate
                try
                {
                    DisplayTypes displayType = TSU.Tsunami2.Properties.ValueToDisplayType;
                    double smoothR = Tsunami2.Preferences.Values.na;
                    double roughMinusSmoothR = Tsunami2.Preferences.Values.na;
                    double smoothV = Tsunami2.Preferences.Values.na;
                    double roughMinusSmoothV = Tsunami2.Preferences.Values.na;
                    int d = TSU.Tsunami2.Preferences.Values.GuiPrefs == null ? 7 : TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
                    if (this.module.PolarModule.VerticalRabot.ContainsKey(pName))
                    {
                        smoothV = this.module.PolarModule.VerticalRabot[pName].Smooth.GetValueOrDefault() / 1000;
                        roughMinusSmoothV = this.module.PolarModule.VerticalRabot[pName].RoughMinusSmooth.GetValueOrDefault() / 1000;
                    }
                    if (this.module.PolarModule.RadialRabot.ContainsKey(pName))
                    {
                        smoothR = this.module.PolarModule.RadialRabot[pName].Smooth.GetValueOrDefault() / 1000;
                        roughMinusSmoothR = this.module.PolarModule.RadialRabot[pName].RoughMinusSmooth.GetValueOrDefault() / 1000;
                    }
                    double firstMeasureRadial = Tsunami2.Preferences.Values.na;
                    double firstMeasureVertical = Tsunami2.Preferences.Values.na;

                    List<M.Measure> pointMeasureList = this.module._Station.MeasuresTaken
                    .GetList()
                    .Where(measure => measure._PointName == pName && measure._Status.Type != M.States.Types.Bad)
                    .ToList();

                    double expORbv = Tsunami2.Preferences.Values.na;
                    double expOHbv = Tsunami2.Preferences.Values.na;
                    double moveRbv = Tsunami2.Preferences.Values.na;
                    double moveHbv = Tsunami2.Preferences.Values.na;

                    bool rabotCalculations = this.module.PolarModule.RabotStrategy != Common.Compute.Rabot.Strategy.Not_Defined;
                    bool hasRadialEntries = this.module.PolarModule.RadialRabot.Count > 0;
                    bool hasVerticalEntries = this.module.PolarModule.VerticalRabot.Count > 0;

                    if (rabotCalculations && (displayType == DisplayTypes.Displacement || displayType == DisplayTypes.Offsets))
                    {
                        if (displayType == DisplayTypes.Displacement)
                        {
                            x = x * -1;
                            y = y * -1;
                            z = z * -1;
                        }
                        if (this.module.PolarModule.RabotStrategy == Common.Compute.Rabot.Strategy.Expected_Offset)
                        {
                            SetOffsetValuesAndVisibility(hasRadialEntries, smoothR, smoothR - x, rowIndex, "Exp_" + R.ColX, "Move_" + R.ColX);
                            SetOffsetValuesAndVisibility(hasVerticalEntries, smoothV, smoothV - z, rowIndex, "Exp_" + R.ColZ, "Move_" + R.ColZ);
                        }
                        else
                        {
                            if (pointMeasureList.Count == 1)
                            {
                                moveRbv = -roughMinusSmoothR;
                                moveHbv = -roughMinusSmoothV;
                                expORbv = moveRbv + x;
                                expOHbv = moveHbv + z;
                            }
                            else if (pointMeasureList.Count > 1)
                            {
                                Polar.Measure firstMeasure = pointMeasureList.FirstOrDefault() as Polar.Measure;
                                firstMeasureRadial = firstMeasure.Offsets.BeamV.X.Value;
                                firstMeasureVertical = firstMeasure.Offsets.BeamV.Z.Value;
                                expORbv = firstMeasureRadial - roughMinusSmoothR;
                                expOHbv = firstMeasureVertical - roughMinusSmoothV;
                                moveRbv = expORbv - x;
                                moveHbv = expOHbv - z;
                            }
                            SetOffsetValuesAndVisibility(hasRadialEntries, expORbv, moveRbv, rowIndex, "Exp_" + R.ColX, "Move_" + R.ColX);
                            SetOffsetValuesAndVisibility(hasVerticalEntries, expOHbv, moveHbv, rowIndex, "Exp_" + R.ColZ, "Move_" + R.ColZ);
                        }
                    }
                }
                catch (Exception) { }

            }

            private void SetOffsetValuesAndVisibility(bool hasRabotEntries,
                                                      double expectValue, double moveValue, int rowIndex,
                                                      string expectedCol, string moveCol)
            {
                if (hasRabotEntries)
                {
                    this.SetOffsetValue(expectValue, rowIndex, expectedCol);
                    this.SetOffsetValue(moveValue, rowIndex, moveCol);

                    SetDataGridColumnVisibility(expectedCol, true);
                    SetDataGridColumnVisibility(moveCol, true);
                }
                else
                {
                    // Hide the DataGrid column if the Rabot collection is empty
                    SetDataGridColumnVisibility(expectedCol, false);
                    SetDataGridColumnVisibility(moveCol, false);
                }
            }

            private void SetDataGridColumnVisibility(string columnName, bool isVisible)
            {
                var column = dataGridViewPinnedTable.Columns[columnName];
                if (column != null)
                {
                    column.Visible = isVisible;
                }
            }


            private string GetPrecision(double value)
            {
                int precisionDigits = (Tsunami2.Preferences.Values.GuiPrefs == null) ? 5 : Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
                int factor = 1;
                // Get precision based on given value
                if (TSU.Tsunami2.Properties.ValueToDisplayType == SO.DisplayTypes.Coordinates) // m 
                {
                    factor = 1;
                }
                else // mm
                {
                    precisionDigits = precisionDigits - 3;
                    factor = 1000;
                }
                string GetPrecision(double x) => (x * factor).ToString("F" + precisionDigits);
                return GetPrecision(value);

            }
            private void SetOffsetValue(double value, int rowIndex, string colName)
            {
                // Get value of offset or displacemnt with given precision
                string def = value == NaN || Double.IsNaN(value) ? "n/a" : this.GetPrecision(value);
                dataGridViewPinnedTable.Rows[rowIndex].Cells[colName].Value = def;
            }

            private void UpdateOffsetRows()
            {
                // Update values of offset after changing coordinate system
                foreach (DataGridViewRow row in dataGridViewPinnedTable.Rows)
                {
                    string pName = GetPinnedPointName(row.Index);
                    List<M.Measure> pMeas = GetAllMeasuresInPinTable(pName);
                    if (pMeas.Count > 0)
                        UpdateRow(row.Index, pMeas);
                }
            }
            public void UpdateAllTable()
            {

                foreach (DataGridViewRow row in dataGridViewPinnedTable.Rows)
                {
                    string pName = GetPinnedPointName(row.Index);
                    if (!this.bShowBeamPoints && pName.StartsWith("BEAM_"))
                        row.Visible = false;
                    else
                        row.Visible = true;
                    int pNum = GetNumberOfExistingMeasures(pName);

                    if (pNum > 0)
                    {
                        int curIndex = pointsPinnedIndex[pName];
                        if (curIndex != pNum)
                        {
                            pointsPinnedIndex[pName] = pNum - 1;
                            UpdatePrevNextRow(row.Index, curIndex, curIndex + 1);
                        }
                    }
                }
                UpdateOffsetRows();
                SetBeamVColumns("[mm]");
                foreach (DataGridViewColumn column in dataGridViewPinnedTable.Columns)
                {
                    if (column.HeaderText.Contains(" "))
                    {
                        // Enable wrap mode to allow multiline headers
                        column.HeaderCell.Style.WrapMode = DataGridViewTriState.True;

                        dataGridViewPinnedTable.AutoResizeColumn(column.Index, DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader);
                    }
                }



            }

            private void UpdatePrevNextRow(int rowIndex, int currentIndex, int lastIndex)
            {
                // Update button cells Next and Prev
                dataGridViewPinnedTable.Rows[rowIndex].Cells[R.ColPrev].Value = currentIndex == 0
                   ? R.LeftNotOk
                   : R.LeftOk;

                dataGridViewPinnedTable.Rows[rowIndex].Cells[R.ColNext].Value = currentIndex == (lastIndex - 1)
                  ? R.RightNotOk
                  : R.RightOk;

            }


            private void UpdatePointsNumber(int rowIndex, int currentIndex, int lastIndex)
            {
                // Update number of points
                lastIndex = (currentIndex < 0) ? 0 : lastIndex;
                dataGridViewPinnedTable.Rows[rowIndex].Cells[R.ColNo].Value = $"{currentIndex + 1}/{lastIndex}";
            }

            private void UpdateDcum(int rowIndex, double dcum)
            {
                // Update Dcum value
                int precisionDigits = (Tsunami2.Preferences.Values.GuiPrefs == null) ?
                    7 : (Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances - 3);

                string defD = dcum != NaN ? dcum.ToString("F" + precisionDigits) : "n/a";
                dataGridViewPinnedTable.Rows[rowIndex].Cells[R.ColDcum].Value = defD;

            }


            private void UpdateRowColor(int rowIndex, Color color)
            {
                // Update type of points in DataGridView (normal meas, obsolete or liveData)
                dataGridViewPinnedTable.Rows[rowIndex].DefaultCellStyle.ForeColor = color;
                //if (color != cellForeColor)
                //    dataGridViewPinnedTable.Rows[rowIndex].DefaultCellStyle.SelectionForeColor = color;
            }   

            private void UpdateXMLFile(string action)
            {
                // Save XMLfile
                Tsunami2.Properties.Save("", action);
            }

            #endregion

            #region LiveData
            internal void GetLiveDataCoordinates(Polar.Measure pointLiveData)
            {

                // Get measurement from LiveData 
                string pName = pointLiveData._PointName;
                if (pName != "Unknown")
                {
                    int pCount = GetPinnedPointsCount(pName.ToUpper());
                    pointLiveData.IsLiveData = true;


                    //if (pCount == 0 && pName != "Unknown")
                    //{
                    //    this.FillPinnedPointsList(pName);
                    //    this.pointsPinned.Add(pointLiveData);
                    //    this.pointsPinnedIndex[pName] += 1;

                    //    var (x, y, z) = (this.NaN, this.NaN, this.NaN);
                    //    if (pointLiveData.Offsets != null)
                    //        (x, y, z) = this.GetOffsetsCoordinates(pointLiveData);
                    //    var pCountUpdate = this.GetCountByNameFromPinnedPointsList(pointLiveData._PointName);

                    //    this.AddNewPinnedPoint(x, y, z, pointLiveData._Point._Parameters.Cumul, pName, pCountUpdate, pointLiveData.IsLiveData);
                    //}
                    //else
                    if (pCount > 0)
                    {
                        PinNewMeasToPinnedPoint(pointLiveData);
                    }
                }
            }



            private void GetLiveDataFromSaved(string pointName)
            {
                List<M.Measure> pointsLiveData = MeasuresRemovedLiveData.FindAll(p => p._PointName == pointName);

                if (pointsLiveData.Count > 0)
                {
                    foreach (M.Measure p in pointsLiveData)
                    {
                        measuresContainingPointsPinned.Add(p);
                    }

                    if (!pointsPinnedIndex.ContainsKey(pointName))
                    {
                        pointsPinnedIndex.Add(pointName, (pointsLiveData.Count - 1));
                    }
                    else
                    {
                        pointsPinnedIndex[pointName] += pointsLiveData.Count;
                    }
                }
            }

            private IEnumerable<M.Measure> GetPointsWithoutLiveData(IEnumerable<M.Measure> points)
            {
                return points.Where(p => p.IsLiveData == false);
            }
            #endregion

            #region PinPointTable
            internal void PinPointToTable()
            {
                // Add single point to the dataGridView but check if point already exists in Measured List
                if (!isVisible)
                    Show();

                var pName = module.View.usedMeasure._PointName;

                if (PinAPoint(pName))
                    this.UpdateXMLFile("Add new point to table");

            }

            public bool PinAPoint(string pName)
            {
                if (!CheckIfPointIsInTable(pName))
                {
                    (double x, double y, double z, double dcum) = (NaN, NaN, NaN, NaN);
                    Color pColor = this.goodMeasColor;
                    AddMeasure(pName);
                    var pCount = GetNumberOfExistingMeasures(pName);

                    if (pCount > 0)
                    {
                        Measure measureToAdd = (Measure)GetLastMeasure(pName);

                        (x, y, z) = GetRelevantValues(measureToAdd);
                        dcum = measureToAdd._Point._Parameters.Cumul;
                        pColor = GetRelevantColor(measureToAdd._Status, measureToAdd.IsLiveData, false);
                    }

                    pCount = (pCount == 0) ? 1 : pCount;
                    AddNewPinnedPoint(x, y, z, dcum, pName, pCount, pColor);
                    return true;
                }
                return false;
            }

            /// <summary>
            /// Add all measured points to the dataGridView from measuresTaken list (all measured points)
            /// </summary>
            internal void PinAllMeasuredPointsToTable()
            {
                if (!isVisible)
                    Show();
                if (module.StationTheodolite.MeasuresTaken.Count > 0)
                {
                    if (!AllPointsUpdated(module.StationTheodolite.MeasuresTaken))
                    {
                        List<List<M.Measure>> groupedPointList = GetGroupedPinnedPoints(module.StationTheodolite.MeasuresTaken);

                        foreach (List<M.Measure> point in groupedPointList)
                        {
                            var pName = point[0]._PointName;
                            var pIndex = (pointsPinnedIndex.ContainsKey(pName) == false) ? -1 : pointsPinnedIndex[pName];
                            (double x, double y, double z) = (NaN, NaN, NaN);

                            if (pIndex < 0)
                            {
                                AddMeasure(pName);
                                Polar.Measure pointMeas = (Measure)GetLastMeasure(pName);

                                (x, y, z) = GetRelevantValues(pointMeas);
                                Color color = GetRelevantColor(pointMeas._Status, pointMeas.IsLiveData, false);
                                AddNewPinnedPoint(x, y, z, point[0]._Point._Parameters.Cumul, pName, pointsPinnedIndex[pName] + 1, color);
                            }
                            else if (pIndex != point.Count())
                            {
                                int pCountOutControl = point.Count();
                                foreach (M.Measure meas in point)
                                {
                                    if (!measuresContainingPointsPinned.Contains(meas))
                                    {
                                        if (!meas.IsControl)
                                        {
                                            measuresContainingPointsPinned.Add(meas);
                                            pointsPinnedIndex[pName] += 1;
                                            (x, y, z) = GetValuesFromPinnedPoints(point);

                                            int rIndex = GetPinnedPointRowIndex(pName);
                                            int pIndexUpdate = pointsPinnedIndex[pName];
                                            Color pColor = GetColorFromPinnedPoints(point);

                                            UpdatePointsNumber(rIndex, pIndexUpdate, pCountOutControl);
                                            UpdateOffsetRow(rIndex, x, y, z);
                                            if (this.module.PolarModule.RadialRabot.ContainsKey(pName) || this.module.PolarModule.VerticalRabot.ContainsKey(pName) && TSU.Tsunami2.Properties.CoordinatesSystems.SelectedType._Name == "BEAMV")
                                                UpdateOffsetRowExpectedMove(rIndex, x, y, z, pName);
                                            UpdatePrevNextRow(rIndex, pIndexUpdate, pIndexUpdate + 1);
                                            UpdateRowColor(rIndex, pColor);
                                        }
                                        else
                                            pCountOutControl -= 1;
                                    }
                                }
                            }
                        }
                        this.UpdateXMLFile("Add all measured points to table");
                    }

                    // show table
                    if (!this.isVisible)
                        this.ShowOrHide();
                }
            }

            internal void PinAllNextPointsToTable()
            {
                if (!isVisible)
                    Show();

                // Add all next points to the dataGridView from measuresToDo list (all to be measure points)
                if (module.StationTheodolite.MeasuresToDo.Count > 0)
                {
                    if (!AllPointsUpdated(module.StationTheodolite.MeasuresToDo))
                    {
                        List<List<M.Measure>> groupedPointList = GetGroupedPinnedPoints(module.StationTheodolite.MeasuresToDo);
                        foreach (List<M.Measure> point in groupedPointList)
                        {
                            string pName = point[0]._PointName;
                            if (!pointsPinnedIndex.ContainsKey(pName))
                            {
                                AddMeasure(pName);
                                var pCount = GetNumberOfExistingMeasures(pName);
                                if (pCount == 0)
                                {
                                    AddNewPinnedPoint(NaN, NaN, NaN, point[0]._Point._Parameters.Cumul, pName, 1, cellForeColor);
                                }
                                else
                                {
                                    Polar.Measure pointMeas = (Measure)GetLastMeasure(pName);
                                    (double x, double y, double z) = GetRelevantValues(pointMeas);
                                    Color color = GetRelevantColor(pointMeas._Status, pointMeas.IsLiveData, false);
                                    AddNewPinnedPoint(x, y, z, point[0]._Point._Parameters.Cumul, pName, pCount, color);
                                }
                            }
                        }
                        this.UpdateXMLFile("Add all next points to table");
                    }
                }
            }

            internal void PinNewMeasToPinnedPoint(Polar.Measure measure)
            {
                if (!isVisible)
                    this.Show();

                if (!measure.IsControl)
                {
                    // Check if point already exists in the table
                    var pName = measure._PointName;
                    if (CheckIfPointIsInTable(pName))
                    {
                        (double x, double y, double z) = (NaN, NaN, NaN);


                        (x, y, z) = GetRelevantValues(measure);

                        measuresContainingPointsPinned.Add(measure);
                        int lastIndex = GetNumberOfExistingMeasures(pName);
                        pointsPinnedIndex[pName] = lastIndex - 1;

                        int rIndex = GetPinnedPointRowIndex(pName);
                        var pColor = GetRelevantColor(measure._Status, measure.IsLiveData, false);

                        // Update table
                        UpdatePointsNumber(rIndex, lastIndex - 1, lastIndex);
                        UpdateOffsetRow(rIndex, x, y, z);
                        if (this.module.PolarModule.RadialRabot.ContainsKey(pName) || this.module.PolarModule.VerticalRabot.ContainsKey(pName) && TSU.Tsunami2.Properties.CoordinatesSystems.SelectedType._Name == "BEAMV")
                            UpdateOffsetRowExpectedMove(rIndex, x, y, z, pName);
                        UpdatePrevNextRow(rIndex, lastIndex - 1, lastIndex);
                        UpdateRowColor(rIndex, pColor);

                        if (measure._Point._Parameters.Cumul == NaN)
                            UpdateDcum(rIndex, measure._Point._Parameters.Cumul);

                    }
                }

            }

            private Color GetColorFromPinnedPoints(List<M.Measure> measures)
            {
                var name = measures[0]._PointName;
                int pIndex = pointsPinnedIndex[name];
                var measure = measures[pIndex];
                var pStatus = measure._Status;
                var isLiveData = measure.IsLiveData;
                bool isBOCComputed = measure.IsFake;

                return GetRelevantColor(pStatus, isLiveData, isBOCComputed);

            }
            private Color GetRelevantColor(M.State pointState, bool isLiveData, bool isBOCComputed)
            {
                if (isLiveData)
                {
                    return this.liveDataColor;
                }
                if (isBOCComputed)
                {
                    return highlightColor;
                }

                return pointState.Type == M.States.Types.Questionnable ? this.questionMeasColor : this.goodMeasColor;
            }

            private (double x, double y, double z) GetValuesFromPinnedPoints(List<M.Measure> measures)
            {
                // Get offset values from list of measured points
                (double x, double y, double z) = (NaN, NaN, NaN);
                List<Measure> pointPolar = measures.ConvertAll(p => (Polar.Measure)p);
                int index = pointsPinnedIndex[pointPolar[0]._PointName];

                (x, y, z) = GetRelevantValues(pointPolar[index]);


                return (x, y, z);
            }

            private (double x, double y, double z) GetRelevantValues(Polar.Measure measure)
            {
                // Get offset values from point depending on the coordinates system              

                Coordinates values = new Coordinates();
                var csType = TSU.Tsunami2.Properties.CoordinatesSystems.SelectedType;
                var displayType = TSU.Tsunami2.Properties.ValueToDisplayType;

                //Get column values depending on the type of view (offset, displacements, coordinate)
                if (displayType == SO.DisplayTypes.Offsets)
                {
                    if (measure.Offsets != null)
                        values = measure.Offsets.GetCoordinatesInASystemByName(csType._Name);

                }
                else if (displayType == SO.DisplayTypes.Displacement)
                {
                    if (measure.Offsets != null)
                    {
                        var temp = measure.Offsets.GetCoordinatesInASystemByName(csType._Name);
                        if (temp != null)
                        {
                            values = (Coordinates)temp.Clone();
                            values?.Set(values.X.Value * -1, values.Y.Value * -1, values.Z.Value * -1);
                        }
                    }
                }
                else if (displayType == SO.DisplayTypes.Coordinates)
                {
                    values = measure?._Point?._Coordinates?.GetCoordinatesInASystemByName(csType._Name);
                }
                else if (displayType == SO.DisplayTypes.Observations)
                {
                    return (measure.Angles.Corrected.Horizontal.Value, measure.Angles.Corrected.Vertical.Value, measure.Distance.Corrected.Value);
                }


                if (values == null || values != null && values.X == null)
                    return (double.NaN, double.NaN, double.NaN);

                return (values.X.Value, values.Y.Value, values.Z.Value);

            }


            private List<List<M.Measure>> GetGroupedPinnedPoints(List<M.Measure> measures)
            {
                // return list of points grouped by name
                List<List<M.Measure>> groupedPointList = measures
                        .GroupBy(n => n._PointName)
                        .Select(grp => grp.ToList())
                        .ToList();
                return groupedPointList;
            }
            private List<M.Measure> GetAllMeasuresInPinTable(string pointName)
            {
                List<M.Measure> tempMeas = measuresContainingPointsPinned.FindAll(point => point._PointName == pointName);
                tempMeas.Sort((a, b) => DateTime.Compare(a._Date, b._Date));

                return tempMeas;
            }

            private M.Measure GetLastMeasure(string pointName)
            {
                List<M.Measure> tempMeas = GetAllMeasuresInPinTable(pointName);
                var indexInTable = pointsPinnedIndex[pointName];
                if (indexInTable == -1)
                    indexInTable = tempMeas.Count - 1; // get last one
                var pLast = tempMeas[indexInTable];
                return pLast;
            }

            private int GetNumberOfExistingMeasures(string pointName)
            {
                return measuresContainingPointsPinned.Where(point => point._PointName == pointName).Count();

            }

            private bool AllPointsUpdated(List<M.Measure> measures)
            {
                List<List<M.Measure>> groupedPointList = GetGroupedPinnedPoints(measures);
                var pointsName = groupedPointList.Select(p => p[0]._PointName).Distinct();
                var allUpdate = pointsName.All(p => pointsPinnedIndex.ContainsKey(p));
                return allUpdate;

            }
            private void AddMeasure(string pointName)
            {
                // Add points with all measurements to list of pinned points and dictionary
                IEnumerable<M.Measure> pMeas = module.StationTheodolite.MeasuresTaken.Where(p => p._PointName == pointName && p.IsControl == false);
                var pCount = pMeas.Count();

                try
                {
                    if (pCount == 0)
                    {
                        pointsPinnedIndex.Add(pointName, -1);
                    }
                    else
                    {
                        foreach (M.Measure p in pMeas)
                            measuresContainingPointsPinned.Add(p);

                        pointsPinnedIndex.Add(pointName, (pCount - 1));
                        GetLiveDataFromSaved(pointName);
                    }

                    measuresContainingPointsPinned.Sort((a, b) => DateTime.Compare(a._Date, b._Date));

                }
                catch (Exception e)
                {

                }


            }

            private void RemovePointFromPinnedPoints(string pointName)
            {
                List<M.Measure> pMeas = GetAllMeasuresInPinTable(pointName);
                if (pMeas.Any())
                {
                    // store live-data after remove
                    IEnumerable<M.Measure> liveP = pMeas.FindAll(l => l.IsLiveData == true).Except(MeasuresRemovedLiveData);
                    MeasuresRemovedLiveData.AddRange(liveP);

                    // remove pinned points from list and dictionary
                    measuresContainingPointsPinned.RemoveAll(p => p._PointName == pointName);
                    pointsPinnedIndex.Remove(pointName);
                }
                else
                {
                    pointsPinnedIndex.Remove(pointName);
                }



            }
            private int SetMeasurementsAsQuestionnable(int rowIndex)
            {
                string pName = GetPinnedPointName(rowIndex);
                int pSaved = 0;
                if (pointsPinnedIndex.ContainsKey(pName))
                {
                    List<M.Measure> pointsMeasured = GetAllMeasuresInPinTable(pName);
                    if (pointsMeasured.Count > 0)
                    {
                        pointsPinnedIndex[pName] = pointsMeasured.Count - 1;

                        IEnumerable<M.Measure> pointsSansLiveData = GetPointsWithoutLiveData(pointsMeasured);

                        foreach (M.Measure p in pointsSansLiveData)
                        {
                            if (p._Status.Type != M.States.Types.Questionnable)
                            {
                                p._Status = new M.States.Questionnable();
                                pSaved += 1;
                            }
                        }
                        UpdateRow(rowIndex, pointsMeasured);
                    }
                }
                return pSaved;
            }


            private void BOCShowMenu()
            {
                List<Control> buttons = new List<Control>();
                buttons.Add(Common.Compute.Compensations.BeamOffsets.WorkFlow.Buttons.GetComputeButton(() =>
                {

                    //List<Magnet> magnets = this.measuresContainingPointsPinned.GetMagnetFrom(); ?? sometime the GetMagnetBeingAlignedFrom do not return anything?
                    List<Magnet> magnets = this.measuresContainingPointsPinned.GetMagnetBeingAlignedFrom();
                    var fm = this.module.FinalModule;
                    foreach (var id in BOC.WorkFlow.DetermineAssemblyIds(magnets))
                    {
                        BOC_Compute(fm, id);
                    }
                }
                ));

                buttons.Add(Common.Compute.Compensations.BeamOffsets.WorkFlow.Buttons.GetSetupButton(() =>
                {
                    //List<Magnet> magnets = this.measuresContainingPointsPinned.GetMagnetFrom();sometime the GetMagnetBeingAlignedFrom do not return anything?
                    List<Magnet> magnets = this.measuresContainingPointsPinned.GetMagnetBeingAlignedFrom();
                    var fm = this.module.FinalModule;
                    foreach (var id in Common.Compute.Compensations.BeamOffsets.WorkFlow.DetermineAssemblyIds(magnets))
                    {
                        BOC.Results results = BOC.WorkFlow.Compute(fm, Tsunami2.Properties.MeasurementModules.GetByObservationType(ObservationType.Polar, addRollModules: true), id, ObservationType.Polar, forceSetup: true);
                        BOC_AddFakeMeasureWithPointsCoordinatesAndOffsets(id, results.ComputedPoints, results.Offsets);
                        string titleAndMessage = results.ToString("4ALL");
                        new MessageInput(MessageType.FYI, titleAndMessage).Show();
                    }
                }
                ));

                buttons.Add(new BigButton("Hide/Show Beam points", R.Poin, () =>
                {
                    bShowBeamPoints = !bShowBeamPoints;
                    this.UpdateAllTable();
                }));

                this.module.View.ShowPopUpSubMenu(buttons, "CS ");

            }

            public void BOC_Compute(FinalModule finalModule, string AssemblyID)
            {
                BOC.Results results = BOC.WorkFlow.Compute(finalModule, Tsunami2.Properties.MeasurementModules.GetByObservationType(ObservationType.Polar, addRollModules: true), AssemblyID, ObservationType.Polar);

                if (results.OK)
                {
                    BOC.WorkFlow.OffsetComputedInAllSystems.WaitOne(10000);
                    BOC_AddFakeMeasureWithPointsCoordinatesAndOffsets(AssemblyID, results.ComputedPoints, results.Offsets);
                    string titleAndMessage = results.ToString("4ALL");
                    new MessageInput(MessageType.FYI, titleAndMessage).Show();
                }
            }

            private bool bShowBeamPoints = true;

            private void BOC_AddFakeMeasureWithPointsCoordinatesAndOffsets(string id, List<E.Point> points, List<(string, CoordinatesInAllSystems)> coords)
            {
                List<Polar.Measure> fakeMesures = new List<Measure>();


                foreach (var coord in coords)
                {
                    string pName = coord.Item1;
                    E.Point point = E.Point.NameContains_Last(pName, points);
                    if (point == null)
                    {
                        continue;
                    }
                    Polar.Measure fakeMeasure = new Polar.Measure()
                    {
                        _Point = point,
                        Offsets = coord.Item2,
                        IsFake = true

                    };

                    fakeMesures.Add(fakeMeasure);
                    PinAPoint(pName);

                    this.measuresContainingPointsPinned.Add(fakeMeasure);


                    AddMeasure(pName);
                }

                try
                {
                    module.StationTheodolite.MeasuresTaken.AddRange(fakeMesures);

                    this.UpdateAllTable();

                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    foreach (var item in fakeMesures)
                    {
                        module.StationTheodolite.MeasuresTaken.Remove(item);
                    }
                }
            }

            #endregion

            #region Buttons

            public class PinTable_Buttons
            {
                private TSU.Polar.Station.PinTableView _view;
                public List<BigButton> all;

                public BigButton ShowPinnedTable;
                public BigButton PinAllPointsMeasured;

                public BigButton SelectedAddToPinnedTable;

                public BigButton Pin_Menu;
                public BigButton PinSingleNextPoint;
                public BigButton PinAllPointsNext;

                public BigButton Refresh;
                public BigButton RemoveAllPin;
                public BigButton ResetAllPointsMeasured;

                public BigButton DisplayOption;
                public BigButton BOC_Menu;
                public BigButton RollComute;

                public PinTable_Buttons(TSU.Polar.Station.PinTableView view)
                {
                    _view = view;

                    all = new List<BigButton>();

                    ShowPinnedTable = new BigButton(R.ShowTableAll, R.Pinned_Table, _view.ShowOrHide); all.Add(ShowPinnedTable);
                    PinAllPointsMeasured = new BigButton(R.PinAllM, R.PinnedAllMeas, _view.PinAllMeasuredPointsToTable); all.Add(PinAllPointsMeasured);

                    SelectedAddToPinnedTable = new BigButton(R.PinPointToTable, R.PinMeas, _view.PinPointToTable); all.Add(SelectedAddToPinnedTable);

                    PinAllPointsNext = new BigButton(R.PinAllN, R.PinnedAllNext, _view.PinAllNextPointsToTable); all.Add(PinAllPointsNext);
                    PinSingleNextPoint = new BigButton(R.PinPointToTable, R.PinNext, _view.PinPointToTable); all.Add(PinSingleNextPoint);

                    Refresh = new BigButton(R.PinRef, R.Update, _view.UpdateAllTable); all.Add(Refresh);
                    RemoveAllPin = new BigButton(R.RemAllPin, R.Length_Delete_Grey, _view.RemovePinnedPoints); all.Add(RemoveAllPin);
                    ResetAllPointsMeasured = new BigButton(R.ResetAllPin, R.Pinned_Reset, _view.ResetPinnedPoints); all.Add(ResetAllPointsMeasured);

                    DisplayOption = new BigButton($"{R.T_CHANGE_VISUAL_DETAILS};{R.T_SWITCH_WHAT_YOU_SEE_IN_THE_PREVIOUS_OF_THE_MEASUREMENT}",
                                                                R.View, _view.CreateVisualDetailSubMenu, color: null, hasSubButtons: true); all.Add(DisplayOption);

                    RollComute = new BigButton($"{"Compute Roll;Select sockets to compute roll with LGC frame"}", R.Tilt, _view.module.RollComputeWithFrame); all.Add(Refresh);
                    BOC_Menu = Common.Compute.Compensations.BeamOffsets.WorkFlow.Buttons.GetMenuButton(_view.BOCShowMenu);
                    Pin_Menu = new BigButton("Pin/Unpin;Add or remove point from the table", R.Pinned_Table, _view.ShowUnPinMenu, hasSubButtons: true); all.Add(Pin_Menu);

                }


                public void Dispose()
                {
                    _view = null;
                    if (all != null)
                    {
                        for (int i = 0; i < all.Count; i++)
                        {
                            if (all[i] != null)
                                all[i] = null;
                        }
                        all.Clear();
                        all = null;
                    }
                }

            }

            private void ShowUnPinMenu()
            {
                this.module.View.ShowPopUpSubMenu(new List<Control>()
                {
                    this.Buttons.PinAllPointsMeasured,
                    this.Buttons.PinAllPointsNext,
                    this.Buttons.RemoveAllPin,
                    this.Buttons.ShowPinnedTable,
                }, name: "subMenu_pin");
            }
        }


        #endregion
    }
}
