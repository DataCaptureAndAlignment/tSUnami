﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using TSU.Views;
using System.Windows.Forms;
using M = TSU;
using L = TSU.Length;
using TSU.Common.Guided.Steps;

namespace TSU.Length.GuidedModules.Steps
{

    static public class Alignment
    {
        static public Common.Guided.Module Get(M.Module parentModule)
        {
            Common.Guided.Group.Module GrGm = parentModule as Common.Guided.Group.Module;

            // Guided.Module gm = new Guided.Module(parentModule, String.Format(R.StringCheminement_StationName, "", "A", "B"));
            Common.Guided.Module gm = new Common.Guided.Module(parentModule, R.StringLength_StationName);
            gm.guideModuleType = ENUM.GuidedModuleType.AlignmentLengthSetting;
            gm.ObservationType = Common.ObservationType.Length;
            // Invisible step: : Add needed station modules
            gm.SwitchToANewStationModule(new Station.Module(gm));

            BuildSteps(gm);

            gm.Start();

            return gm;
        }

        private static void BuildSteps(Common.Guided.Module gm)
        {
            //// Step 0: Choose Management.Instrument
            gm.Steps.Add(Measurement.ChooseTheodolite(gm));

            //// Step 1: Choose default staff
            gm.Steps.Add(Measurement.ChooseDefaultMeasureParameters(gm));

            /// Step 2 : Choose Cala Points
            gm.Steps.Add(Measurement.ChooseReferencePointsForElementAlignment(gm));

            /// Step 3 : Mesures alignement
            gm.Steps.Add(Measurement.MeasureLengthSettingAlignement(gm));

            /// Step 4 : Show dat saved file
            gm.Steps.Add(Management.ShowDatFiles(gm));
        }

        internal static void ReCreateWhatIsNotSerialized(Common.Guided.Module gm)
        {
            gm.Steps = new List<Step>();
            Station.Module st = gm._ActiveStationModule as Station.Module;
            if (st != null) { st.View.moduleType = ENUM.ModuleType.Guided; }
            //gm.currentIndex = 0;
            BuildSteps(gm);
            //gm.Start();
            //gm.MoveToStep(gm.currentIndex);
        }
    }
}
