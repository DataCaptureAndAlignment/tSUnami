﻿using System;
using System.Linq;
using TSU.Common.Elements;
using TSU.Common.Guided.Steps;
using TSU.Views;
using Drawing = System.Drawing;
using F = System.Windows.Forms;
using I = TSU.Common.Instruments;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Length.GuidedModules.Steps
{
    public static class Measurement
    {
        /// <summary>
        /// Step mesure Length Setting
        /// </summary>
        /// <param name="gm"></param>
        /// <returns></returns>
        internal static Step MeasureLengthSettingAlignement(Common.Guided.Module guidedModule)
        {
            Station.Module st = guidedModule._ActiveStationModule as Station.Module;
            Step s = new Step(guidedModule, R.StringLength_AlignmentMeasure);
            bool firstUse = true;
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                //Remove title and description to have more space for the measurements
                e.step.View.ClearAllControls();
                st.View.MaximizeDataGridView();
                st.View.ShowMessageSelectStation();
            };
            s.Entering += delegate (object source, StepEventArgs e)
            {
                st = e.ActiveStationModule as Station.Module;
                st.View.ChangeModuleType(ENUM.ModuleType.Guided);
                guidedModule.Utility = st.Utility;
                //Cache la colonne delete point
                st.View.dataGridViewLength.Columns[0].Visible = false;
                st.View.UpdateListMeasure();
                e.step.View.AddControl(st);

            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                e.step.Test();
                guidedModule.Utility = st.Utility;
                //To set the scroll bar to left and at the top
                if (firstUse) st.View.SetScrollBarAtZero();
                firstUse = false;
                //Guided.Group.Module GrGm = e.guidedModule.ParentModule as Guided.Group.Module;
                //// L'admin module est toujours le premier dans la liste des guided modules lors d'un alignement d'aimant
                //// Le step 3 est step avec les résultats d'alignement qu'il faut mettre à jour
                //if (GrGm.SubGuidedModules[0].currentIndex == 3)
                //{
                //    GrGm.SubGuidedModules[0].CurrentStep.Update();
                //}
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                e.step.CheckValidityOf(st._LengthStation._Parameters._State is Common.Station.State.StationLengthSaved);
            };
            s.Changed += delegate (object source, StepEventArgs e)
            {

            };
            //Leaving() is triggered by Leave()
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                st.View.dataGridViewLength.CancelEdit();
                if (e.step.State == StepState.ReadyToSkip && st._LengthStation._Parameters._State is Common.Station.State.StationLengthReadyToBeSaved)
                {
                    st.View.ShowMessageSave();
                }
            };
            return s;
        }
        /// <summary>
        /// Select the default measure parameters
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step ChooseDefaultMeasureParameters(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.String_GM_AdministrativeParameters);
            Station.Module st = guidedModule._ActiveStationModule as Station.Module;
            BigButton buttonSelectReflector = new BigButton();
            BigButton buttonSwitchFace = new BigButton();
            BigButton buttonMeasMode = new BigButton();
            F.TextBox numberMeasTextBox = new F.TextBox();
            Drawing.Font TitleFont = new Drawing.Font("Microsoft Sans Serif", 15.75F, Drawing.FontStyle.Regular, Drawing.GraphicsUnit.Point, 0);
            int numberMeas = st._LengthStation._Parameters._DefaultNumberMeas;
            string ccr_1_name = st._defaultReflector._Name;
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                // add a button to directly select a default reflector
                e.step.View.AddButton(
                    string.Format(R.StringLength_GM_SelectDefaultReflector, st._defaultReflector._Name),
                    R.Reflector,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        st.SelectDefaultReflector();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectReflector = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectReflector.SetColors(Tsunami2.Preferences.Theme.Colors.Attention);
                // add a button to switch between face
                e.step.View.AddButton(
                    R.StringLength_GM_SwitchBetweenFace,
                    R.SurveyOneFace,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        st.SwitchDefaultDoubleFace();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSwitchFace = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSwitchFace.SetColors(Tsunami2.Preferences.Theme.Colors.Good);
                // add a button to select the measure mode for AT40X
                e.step.View.AddButton(
                    string.Format(R.StringLength_GM_SelectDefaultMeasMode, "..."),
                    R.At40x_Precision_Standard,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        if (st._InstrumentManager.SelectedInstrumentModule is I.Device.AT40x.Module)
                        {
                            I.Device.AT40x.Module ATModule = st._InstrumentManager.SelectedInstrumentModule as I.Device.AT40x.Module;
                            switch (ATModule._measurement.mode)
                            {
                                case I.Device.AT40x.Module.aT40xMeasurementMode.Standard:
                                    ATModule._measurement.mode = I.Device.AT40x.Module.aT40xMeasurementMode.Precise;
                                    break;
                                case I.Device.AT40x.Module.aT40xMeasurementMode.Precise:
                                    ATModule._measurement.mode = I.Device.AT40x.Module.aT40xMeasurementMode.Fast;
                                    break;
                                case I.Device.AT40x.Module.aT40xMeasurementMode.Fast:
                                    ATModule._measurement.mode = I.Device.AT40x.Module.aT40xMeasurementMode.Outdoor;
                                    break;
                                case I.Device.AT40x.Module.aT40xMeasurementMode.Outdoor:
                                    ATModule._measurement.mode = I.Device.AT40x.Module.aT40xMeasurementMode.Standard;
                                    break;
                                default:
                                    ATModule._measurement.mode = I.Device.AT40x.Module.aT40xMeasurementMode.Standard;
                                    break;
                            }
                        }
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonMeasMode = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonMeasMode.SetColors(Tsunami2.Preferences.Theme.Colors.Good);
                /// ajout nom de l'équipe
                e.step.View.AddTitle(R.StringLength_GM_DefaultNumberOfMeas, "measLabel");
                e.step.View.AddTextBox(guidedModule._TsuView, numberMeas.ToString());
                numberMeasTextBox = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as F.TextBox;
                numberMeasTextBox.BackColor = Tsunami2.Preferences.Theme.Colors.Good;
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {

                numberMeas = st._LengthStation._Parameters._DefaultNumberMeas;
                numberMeasTextBox.Text = numberMeas.ToString();
                numberMeasTextBox.Focus();
                numberMeasTextBox.SelectAll();
                numberMeasTextBox.Focus();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = false;
                bool stepValid = true;
                ///Vérification si default reflector changé pour un autre que le CCR1.5.1
                if (ccr_1_name != st._defaultReflector._Name)
                {
                    buttonSelectReflector.SetColors(Tsunami2.Preferences.Theme.Colors.Good);
                    buttonSelectReflector.ChangeImage(R.StatusGood);
                    buttonSelectReflector.ChangeNameAndDescription(string.Format(R.StringLength_GM_SelectDefaultReflector, st._defaultReflector._Name));
                }
                else
                {
                    stepValid = false;
                }
                ///Switch double face (pas obligatoire)
                if (st._LengthStation._Parameters._DefaultDoubleFace)
                {
                    buttonSwitchFace.ChangeImage(R.Survey2Face);
                }
                else
                {
                    buttonSwitchFace.ChangeImage(R.SurveyOneFace);
                }
                ///Selection du mode de mesure AT40X
                if (st._InstrumentManager.SelectedInstrumentModule is I.Device.AT40x.Module)
                {
                    buttonMeasMode.Available = true;
                    I.Device.AT40x.Module ATModule = st._InstrumentManager.SelectedInstrumentModule as I.Device.AT40x.Module;
                    ATModule.measModeSetByuser = true;
                    switch (ATModule._measurement.mode)
                    {
                        case I.Device.AT40x.Module.aT40xMeasurementMode.Standard:
                            buttonMeasMode.ChangeImage(R.At40x_Precision_Standard);
                            buttonMeasMode.ChangeNameAndDescription(string.Format(R.StringLength_GM_SelectDefaultMeasMode, R.t_Standard));
                            break;
                        case I.Device.AT40x.Module.aT40xMeasurementMode.Precise:
                            buttonMeasMode.ChangeImage(R.At40x_Precision_Precise);
                            buttonMeasMode.ChangeNameAndDescription(string.Format(R.StringLength_GM_SelectDefaultMeasMode, R.t_Precise));
                            break;
                        case I.Device.AT40x.Module.aT40xMeasurementMode.Fast:
                            buttonMeasMode.ChangeImage(R.At40x_Precision_Fast);
                            buttonMeasMode.ChangeNameAndDescription(string.Format(R.StringLength_GM_SelectDefaultMeasMode, R.t_Fast));
                            break;
                        case I.Device.AT40x.Module.aT40xMeasurementMode.Outdoor:
                            buttonMeasMode.ChangeImage(R.At40x_Precision_Outdoor);
                            buttonMeasMode.ChangeNameAndDescription(string.Format(R.StringLength_GM_SelectDefaultMeasMode, R.t_Outdoor));
                            break;
                        default:
                            buttonMeasMode.ChangeImage(R.At40x_Precision_Standard);
                            buttonMeasMode.ChangeNameAndDescription(string.Format(R.StringLength_GM_SelectDefaultMeasMode, R.t_Standard));
                            break;
                    }
                }
                else
                {
                    buttonMeasMode.Available = false;
                }
                ///Change le nombre de mesure par défault
                string tempMeas = numberMeasTextBox.Text;
                int i = T.Conversions.Numbers.ToInt(tempMeas, -1);
                if (i > 0)
                {
                    st.ChangeDefaultMeasNumber(i);
                }
                else
                {
                    st.View.ShowMessage(R.StringLength_WrongMeasNumber);
                }
                e.step.skipable = true;
                e.step.CheckValidityOf(stepValid);
            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {
                //applique les paramètres par défaut à toutes les mesures
                foreach (TSU.Polar.Measure mes in st._LengthStation._MeasureOfTheodolite)
                {
                    mes.Face = st._LengthStation._Parameters._DefaultDoubleFace ? I.FaceType.DoubleFace : I.FaceType.Face1;
                    mes.NumberOfMeasureToAverage = st._LengthStation._Parameters._DefaultNumberMeas;
                    mes.SetActualReflector(st._LengthStation._Parameters._DefaultReflector);
                }

                if (st._InstrumentManager.SelectedInstrumentModule is I.Device.AT40x.Module)
                {
                    I.Device.AT40x.Module ATModule = st._InstrumentManager.SelectedInstrumentModule as I.Device.AT40x.Module;
                    if (ATModule.InstrumentIsOn)
                    {
                        ATModule._measurement.SetMode();
                    }
                }
            };
            return s;
        }
        /// <summary>
        /// Choose the theodolite to do the measurement 
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step ChooseTheodolite(Common.Guided.Module guidedModule)
        {
            Step s = Management.ChooseInstrument(guidedModule, I.InstrumentClasses.POLAR);
            //string instrumentname = "";
            //s.Entering += delegate (object source, StepEventArgs e)
            //{
            //    StationLength.Station.Module st = e.ActiveStationModule as StationLength.Station.Module;
            //    if (st._LengthStation._Parameters._Instrument != null) instrumentname = st._LengthStation._Parameters._Instrument._Name;

            //};
            //s.Leaving += delegate (object source, StepEventArgs e)
            //{
            //    StationLength.Station.Module st = e.ActiveStationModule as StationLength.Station.Module;
            //    if (st._LengthStation._Parameters._Instrument != null)
            //    {
            //        if (instrumentname != st._LengthStation._Parameters._Instrument._Name) st.View.MaximizeDataGridView();
            //    }
            //};
            return s;
        }
        /// <summary>
        /// Select the reference points
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step ChooseReferencePointsForElementAlignment(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.T251);
            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.ElementManager.MultiSelection = true;
                e.ElementManager.View.CheckBoxesVisible = true;
                e.ElementManager.SetSelectableListToAllAlesages();
                //e.ElementManager.View.buttons.ShowOnlyButtons(e.ElementManager.View.buttons.open);
                //e.ElementManager.View.ViewChoiceButton.Available = false;
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.View.ChangeModuleType(ENUM.ModuleType.Guided);
                stationModule.SetTheoCalaPointsInElementManager();
                if (stationModule.FinalModule.PointsToBeAligned != null && stationModule.ElementModule.SelectableObjects != null)
                {
                    foreach (Point item in stationModule.FinalModule.PointsToBeAligned)
                    {
                        TsuObject objectFound = stationModule.ElementModule.SelectableObjects.Find(x => (x as Point)._Name == item._Name);
                        if (objectFound != null)
                        {
                            stationModule.ElementModule.RemoveSelectableObjects(objectFound);
                        }
                    }
                }
                e.ElementManager.UpdateView();
                e.step.View.AddControl(e.ElementManager);
                e.ElementManager.View.buttons.ShowTypes.Available = true;
                e.ElementManager.View.buttons.ShowPiliers.Available = true;
                e.ElementManager.View.buttons.ShowPoints.Available = true;
                e.ElementManager.View.buttons.ShowMagnets.Available = true;
                e.ElementManager.View.buttons.ShowAll.Available = true;
                e.ElementManager.View.buttons.ShowAlesages.Available = true;
                e.ElementManager.View.buttons.clean.Available = false;
                e.ElementManager.View.buttons.importRefenceFile.Available = true;

                e.step.Test();
            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                e.step.Test();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                e.step.MessageFromTest = R.String_GM_MsgBlock1Point;
                e.step.CheckValidityOf(e.ElementManager.GetPointsInSelectedObjects().Count() >= 1);
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                CloneableList<Point> pointsList = e.ElementManager.GetPointsInSelectedObjects().Clone();
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                //Recupere tous les points de cala
                foreach (Point item in pointsList)
                {
                    item.LGCFixOption = ENUM.LgcPointOption.CALA;
                    int index = stationModule.FinalModule.PointsToBeAligned.FindIndex(x => x._Name == item._Name && x._Origin == item._Origin);
                    if (index != -1)
                    {
                        //Dans le cas ou le point sélectionné en référence fait déjà partie des points à aligner
                        stationModule.FinalModule.PointsToBeAligned.RemoveAt(index);
                        stationModule.FinalModule.CalaPoints.Add(item);
                        int index5 = stationModule.FinalModule.PointsToBeMeasured.FindIndex(x => x._Name == item._Name && x._Origin == item._Origin);
                        if (index5 != -1) { stationModule.FinalModule.PointsToBeMeasured[index5].LGCFixOption = ENUM.LgcPointOption.CALA; }
                        int index1 = stationModule._TheoPoint.FindIndex(x => x._Name == item._Name && x._Origin == item._Origin);
                        if (index1 != -1) { stationModule._TheoPoint[index1].LGCFixOption = ENUM.LgcPointOption.CALA; }
                        int index3 = stationModule._LengthStation._MeasureOfTheodolite.FindIndex(x => x._PointName == item._Name && x._Point._Origin == item._Origin);
                        if (index3 != -1) { stationModule._LengthStation._MeasureOfTheodolite[index3]._Point.LGCFixOption = ENUM.LgcPointOption.CALA; }
                    }
                }
                stationModule.FinalModule.CalaPoints = pointsList.Clone();
                pointsList.AddRange(stationModule.FinalModule.PointsToBeAligned.Clone());
                stationModule.SetPointsToMeasure(pointsList.Clone());
                stationModule.UpdateCalaVSPointToBeAligned(pointsList);
                stationModule.UpdateOffsets(true, false);
                e.step.View.RemoveControl(e.ElementManager);
            };
            return s;
        }
    }
}
