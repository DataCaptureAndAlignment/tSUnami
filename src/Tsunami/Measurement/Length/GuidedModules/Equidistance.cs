﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TSU.Common.Compute;
using TSU.Common.Guided.Steps;
using TSU.Polar;
using E = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using PolarGuidedSteps = TSU.Polar.GuidedModules.Steps;
using R = TSU.Properties.Resources;

namespace TSU.Length.GuidedModules
{

    static public class EquiDistance
    {
        static public Common.Guided.Module Get(TSU.Module parentModule, string nameAndDescription)
        {
            Common.Guided.Module gm = new Common.Guided.Module(parentModule, "equiDistance");
            gm.View.Image = R.I_PointLancé;
            gm.guideModuleType = ENUM.GuidedModuleType.EquiDistance;
            gm.ObservationType = Common.ObservationType.Length;

            // Invisible step: : Add needed station modules
            Polar.Station.Module stm = new Polar.Station.Module(gm);
            stm.stationParameters.DefaultMeasure.Face = I.FaceType.Face1;

            stm.stationParameters.DefaultMeasure.NumberOfMeasureToAverage = 1;
            gm.SwitchToANewStationModule(stm);

            ViewsOption(gm);

            BuildSteps(gm, nameAndDescription);

            gm.Start();

            return gm;
        }


        private static void ViewsOption(Common.Guided.Module gm) // not serialized
        {
            Polar.Station.Module stm = gm._ActiveStationModule as Polar.Station.Module;
            stm.RemoveMeasureFromNextPointListAfterReception = false;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShowMeasureDifferences").State = DsaOptions.Ask_to; //stm.flags.ShowMeasureDifferences = false;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "DoStationSetupAfterClosure").State = DsaOptions.Never; //stm.flags.DoStationSetupAfterClosure = false;
            DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ProposeFreeStationIfPointConnu").State = DsaOptions.Never; //stm.flags.ProposeFreeStationIfPointConnu = false;
        }

        private static void BuildSteps(Common.Guided.Module gm, string nameAndDescription)
        {
            //
            // Admin
            //
            gm.Steps.Add(Management.Declaration(gm, nameAndDescription));
            gm.Steps.Add(Management.ChooseTheoreticalFile(gm));
            gm.Steps.Add(Management.ChooseInstrument(gm, I.InstrumentClasses.POLAR));
            //gm.Steps.Add(GS.Management.EnterATeam(gm));
            //gm.Steps.Add(GS.Management.ChooseOperation(gm));
            gm.Steps.Add(PolarGuidedSteps.Setup.StationDefaultMeasurementParameters(gm));

            gm.Steps.Add(ChooseMiddlePoint(gm));
            gm.Steps.Add(ChooseExtremityPoints(gm));
            gm.Steps.Add(PolarGuidedSteps.Measurement.MeasurementAbstractBefore(gm, PolarGuidedSteps.Measurement.Types.EquiDistance));

            gm.Steps.Add(Management.Export(gm));

            gm.Steps.Add(Management.Declaration(gm, R.StringGuided_End));
            gm.Steps.Add(PolarGuidedSteps.Setup.End(gm, R.StringGuided_End));

        }

        static public void ReCreateWhatIsNotSerialized(Common.Guided.Module gm)
        {
            gm.Steps = new List<Step>();
            ViewsOption(gm);
            BuildSteps(gm, gm.NameAndDescription);
        }


        internal static Step ChooseMiddlePoint(Common.Guided.Module gm)
        {
            Step s = Management.ChooseElement(gm);

            //   s._Name = "Selection of the middle point";
            //   s.utility = "This is the point from which the distance measurement will be proceed";
            s._Name = R.T_SELECTION_OF_THE_MIDDLE_POINT;
            s.Utility = R.T_THIS_IS_THE_POINT_FROM_WHICH_THE_DISTANCE_MEASUREMENT_WILL_BE_PROCEED;

            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.ElementManager._SelectedObjects.Clear();
                e.ElementManager.MultiSelection = true;
                e.ElementManager.View.CheckBoxesVisible = true;
                e.ElementManager.SetSelectableToAllPoints();
                e.step.View.AddControl(e.guidedModule._ElementManager);
                e.ElementManager.AddSelectedObjects(e.guidedModule.MagnetsToBeAligned.Cast<TsuObject>().ToList());
                e.ElementManager.UpdateView();
            };

            s.ControlChanged += delegate (object source, StepEventArgs e)
            {

            };

            s.Updating += delegate (object source, StepEventArgs e)
            {

            };

            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = false;
                e.step.CheckValidityOf(e.ElementManager._SelectedObjects.Count == 1);
            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {
                if (e.ElementManager._SelectedObjects[0] is E.Point p)
                {
                    gm.PointsToBeAligned = new CloneableList<E.Point>() { p };
                    (gm._ActiveStationModule._Station as Polar.Station).Parameters2.Setups.InitialValues.StationPoint = p;
                }

                e.step.View.RemoveControl(e.guidedModule._ElementManager);
            };

            return s;
        }

        internal static Step ChooseExtremityPoints(Common.Guided.Module gm)
        {
            Step s = Management.ChooseElement(gm);

            //    s._Name = "Selection of the points to measure";
            //    s.utility = "You will be shown the differences of the theoretical and the measured 'horizontal distance' between the point stationned and the ones measured";
            s._Name = R.T_SELECTION_OF_THE_POINTS_TO_MEASURE;
            s.Utility = R.T_YOU_WILL_BE_SHOWN_THE_DIFFERENCES;

            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.ElementManager._SelectedObjects.Clear();
                e.ElementManager.MultiSelection = true;
                e.ElementManager.View.CheckBoxesVisible = true;
                e.ElementManager.SetSelectableToAllPoints();
                e.step.View.AddControl(e.guidedModule._ElementManager);
                gm.PointsToBeAligned.ForEach(x => e.ElementManager.AddSelectedObjects(x));
                e.ElementManager.UpdateView();
            };

            s.ControlChanged += delegate (object source, StepEventArgs e)
            {

            };

            s.Updating += delegate (object source, StepEventArgs e)
            {

            };

            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = false;
                e.step.CheckValidityOf(e.ElementManager._SelectedObjects.Count > 0);
                //   e.step.MessageFromTest = "At least one point should be measured";
                e.step.MessageFromTest = R.T_AT_LEAST_ONE_POINT_SHOULD_BE_MEASURED;
            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {
                gm.PointsToBeAligned.Clear();
                e.ElementManager._SelectedObjects.ForEach(x => gm.PointsToBeAligned.Add(x as E.Point));
                e.step.View.RemoveControl(e.guidedModule._ElementManager);
            };

            return s;
        }

        internal static Step EquiDistNextPoint(Common.Guided.Module gm, Measure measure)
        {
            Step s = PolarGuidedSteps.Measurement.MeasureNextPointGeneral(gm, measure, PolarGuidedSteps.Measurement.Types.EquiDistance);

            s.Id = "EquiDistNextPoint";

            // Usefull references
            Polar.Station.Module stm = null;
            Polar.Station.View stmv = null;
            Polar.Station st = null;
            E.Point PointToStakeOut = null;

            void OnTreatedMeasureReceived(object source2, M.MeasurementEventArgs e2)
            {
                // easy ref 
                if (e2.Measure is Measure m)
                {
                    if (m._Point._Name == PointToStakeOut._Name)
                    {
                        // compute theoretical horizontal distance
                        double dHor = Survey.GetHorizontalDistance(
                            st.Parameters2._StationPoint,
                            m._OriginalPoint,
                            E.Coordinates.CoordinateSystemsTsunamiTypes.CCS);

                        // show measure result
                        s.View.ModifyLabel(
                            string.Format(
                                "\r\n" + R.PolarMeasure_R,
                                m.Face,
                                m._Point._Name,
                                m.Distance.Reflector._Name,
                                m.Extension.Value,
                                m.Angles.Corrected.Horizontal.ToString(4),
                                m.Angles.Corrected.Vertical.ToString(4),
                                m.Distance.Corrected.ToString(6)),
                            "m",
                            TSU.Tsunami2.Preferences.Theme.Colors.Good);
                        s.Validate();

                        // if stake outpoint coordinates
                        s.View.ModifyLabel(
                            m._Point.ToString(E.CoordinatesInAllSystems.StringFormat.BeamStnLrv),
                            "CoordinatePointMeasured");
                    }
                    // measuredSuccesfully = false;
                    s.Update();
                }
            }

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                stm = s.GuidedModule._ActiveStationModule as Polar.Station.Module;
                stmv = stm.View;
                st = stm.StationTheodolite;
                PointToStakeOut = (s.TAG as Measure)._OriginalPoint;

                DsaFlag.GetByNameOrAdd(stm.DsaFlags, "ShouldChangeNameOfExistingBeforeMeasurement").State = DsaOptions.Never; //stm.flags.ShouldChangeNameOfExistingBeforeMeasurement = DsaOptions.Never;
                DsaFlag.GetByNameOrAdd(stm.DsaFlags, "PutPreviousMeasureToBad").State = DsaOptions.Always; //stm.flags.AskToPutPreviousMeasureToBad = false;
                                                                                                           //stm.flags.AlwaysPutPreviousMeasureToBad = true;

                List<Control> controls = new List<Control>
                {
                    s.View.CreateLabel(R.T_IMP_CHOOSE, "type", s.View.TitleFont)
                };

                s.View.AddSidedControls(controls, DockStyle.Top);

                // Set measure to "questionnable state"
                if (stm.StationTheodolite.MeasuresToDo.Find(x => x._Point._Name == PointToStakeOut._Name) is Measure m)
                    m._Status = new M.States.Questionnable();

                // Subscribe to OnResultavailable
                stm.MeasureThreated += OnTreatedMeasureReceived;
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                // Prepare next step?
                PolarGuidedSteps.Measurement.PrepareOrNotNextMeasurementStep(
                    s.GuidedModule,
                    s.TAG as Measure,
                    PolarGuidedSteps.Measurement.Types.PreAlignment);
            };

            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                e.step.CheckValidityOf(s.stepsRemovedByUser); // not visible means it as been remove b y the button click
            };

            s.Updating += delegate (object source, StepEventArgs e)
            {
                s.View.ModifyLabel(R.T_IMP_CHOOSE + "\r\n", "type");
            };

            return s;
        }
    }
}
