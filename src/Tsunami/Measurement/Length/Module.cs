using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using TSU.Views;

namespace TSU.Length
{
    [Serializable]
    [XmlType(TypeName = "Length.Module")]
    public class Module : Common.FinalModule
    {
        // Fields
        //public StationLevelModule stationLevelModule;

        public override Common.ObservationType ObservationType { get; set; } = Common.ObservationType.Length;

        [XmlIgnore]
        public new CompositeView View
        {
            get
            {
                return this._TsuView as CompositeView;
            }
            set
            {
                this._TsuView = value;
            }
        }
        // Constructor
        public Module() // need a parameterless constructor for xml serialazation
            : base()
        {

        }

        public Module(TSU.Module parentModule, string nameAndDescription)
            : base(parentModule, nameAndDescription)
        {

        }
        // Methods
        public override void Initialize()// This method is called by the base class "Module" when this object is created
        {
            base.Initialize();
        }
        /// <summary>
        /// Cr�e une nouvelle station module sans copier les param�tres admin de l'ancienne
        /// </summary>
        internal void NewStation()
        {
            this.SwitchToANewStationModule(new Length.Station.Module(this));
        }
        internal override void SetActiveStationModule(Common.Station.Module stationModule, bool createView = true)
        {
            base.SetActiveStationModule(stationModule, createView);
            this.View?.SetStationNameInTopButton(stationModule);
        }
        /// <summary>
        /// Cr�e une nouvelle station module en copiant les param�tres admin de l'ancienne
        /// </summary>
        /// <param name="cloneAdminParam"></param>
        internal void CloneActualStation()
        {
            Length.Station.Module newStationModule = new Station.Module(this);
            if (this._ActiveStationModule != null)
            {
                Length.Station.Module oldStationModule = this._ActiveStationModule as Length.Station.Module;
                newStationModule.ChangeOperationID(oldStationModule._LengthStation.ParametersBasic._Operation);
                newStationModule.ChangeTeam(oldStationModule._LengthStation.ParametersBasic._Team);
                newStationModule.SetGeodeFilePathInNewStationLengthModule(oldStationModule);
                newStationModule.SetInstrumentInNewStationLengthModule(oldStationModule);
                newStationModule.SetDefaultMeasParametersInNewStationLengthModule(oldStationModule);
                newStationModule.ChangeTolerance(oldStationModule._LengthStation._Parameters._Tolerance);
                newStationModule.SetCoordinateSystemInNewStationLengthModule(oldStationModule);
                newStationModule.CreatedOn = DateTime.Now;
            }
            this.SwitchToANewStationModule(newStationModule);
        }
        /// <summary>
        /// Efface la station actuelle et choisit la pr�c�dente station
        /// On ne peut pas effacer la premi�re station
        /// </summary>
        internal void DeleteActualStation()
        {
            if (this._ActiveStationModule != null)
            {
                int index = this.StationModules.FindIndex(x => x._Station._Name == this._ActiveStationModule._Station._Name);
                if (this.StationModules.Count != 1)
                {
                    this.StationModules.Remove(this._ActiveStationModule);
                    if (index > 0) { index -= 1; }
                    else { index = 0; }
                    this.SetActiveStationModule(this.StationModules[index]);
                }
            }
        }
        /// <summary>
        /// dispatcheur d'evenement si un type n'est pas connu il est trait� par OnNextBasedOn(TsuObject TsuObject)
        /// </summary>
        /// <param name="TsuObject"></param>
        public override void OnNext(TsuObject TsuObject)
        {
            dynamic dynamic = TsuObject;
            OnNextBasedOn(dynamic);
        }
        /// <summary>
        /// �v�nement li� � la r�ception de points de l'Element MOdule Manager et ajout � la s�quence de points
        /// </summary>
        /// <param name="elementsFromElementModule"></param>
        public new void OnNextBasedOn(TSU.Common.Elements.Composites.CompositeElement elementsFromElementModule)
        {
            //lance la fonction pour donner les points � mesurer
            if (elementsFromElementModule != null)
            {
                (this._ActiveStationModule as Length.Station.Module).SetPointsToMeasure(elementsFromElementModule);
            }
        }

        internal override IEnumerable<Common.Station> GetStations()
        {
            var stations = new List<Common.Station>();
            foreach (var item in this.StationModules)
            {
                stations.Add(item._Station);
            }
            return stations;
        }
    }

}
