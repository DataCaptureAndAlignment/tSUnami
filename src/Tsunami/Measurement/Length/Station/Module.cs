using System;
using System.Collections.Generic;
using TSU.ENUM;
using R = TSU.Properties.Resources;

using E = TSU.Common.Elements;
using EC = TSU.Common.Elements.Composites;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using O = TSU.Common.Operations;
using Z = TSU.Common.Zones;
using X = TSU.Tools.Exploration;
using F = TSU.Functionalities;
using System.Xml;
using System.Xml.Serialization;
using TSU.Common.Elements.Manager;
using TSU.Common.Instruments.Reflector;
using TSU.Common.Compute;
using TSU.Common.Instruments;
using System.Collections.ObjectModel;
using TSU.Views;
using TSU.Views.Message;
using System.Linq;

namespace TSU.Length
{
    public partial class Station
    {
        [Serializable]
        [XmlType(TypeName = "Length.Station.Module")]
        public class Module : Common.Station.Module
        {
            #region files and props
            [XmlIgnore]
            internal new View View
            {
                get
                {
                    return this._TsuView as View;
                }
                set
                {
                    this._TsuView = value;
                }
            }

            [XmlIgnore]
            public override ReadOnlyCollection<M.Measure> MeasuresReceived
            {
                get
                {
                    var list = new List<M.Measure>();
                    return new ReadOnlyCollection<M.Measure>(list);
                }
            }

            private Reflector defaultReflector;
            public Reflector _defaultReflector
            {
                get
                {
                    //Evite de mettre en r�f�rence la defaultstaff
                    if (this.defaultReflector != null)
                    {
                        Reflector reflector = this.defaultReflector.DeepCopy();
                        return reflector;
                    }
                    else
                    {
                        return null;
                    }
                }
                set { this.defaultReflector = value; }
            }
            internal Length.Station _LengthStation;
            public CloneableList<E.Point> _TheoPoint { get; set; }
            public CloneableList<E.Point> _MeasPoint { get; set; }
            public string measGroupName { get; set; }
            [XmlIgnore]
            public I.Manager.Module reflectorModule { get; set; }
            public TsuBool ShowSaveMessageOfSuccess { get; set; }
            [XmlIgnore]
            public bool changeDefaultReflector = true;
            [XmlIgnore]
            public string tempMsg = "";
            internal string actualPointFocussed;
            [XmlIgnore]
            private bool messageSelectStationShown = false;

            

            #endregion

            #region Construction & init
            public Module()
                : base()
            {

            }
            public Module(Common.FinalModule module) : base(module, $"{R.StringLength_StationModuleLengthSetting};{R.StringLength_Module_Managing_Length_station}")
            {

            }
            //Methods
            public override void Initialize() // This method is called by the base class "Module" when this object is created
            {
                // Add a view to the module
                this.ShowSaveMessageOfSuccess = new TsuBool(false);
                this._LengthStation = new Length.Station();
                this._Station = this._LengthStation;
                this.measGroupName = this._LengthStation._Name;
                this._TheoPoint = new CloneableList<E.Point>();
                this._MeasPoint = new CloneableList<E.Point>();
                this.Utility = "A--B";
                base.Initialize();  // this will add a instrument Manager but with no instrument in the list, need to call _InstrumentManager.LoadInstruments()
                this.LoadInstrumentTypes();
                this.AddNewReflectorModules();
                TsuObject reflector =
                this.reflectorModule.AllElements.Find(x => (x as Reflector)._Model == "CCR1.5"
                && (x as Reflector)._SerialNumber == "1");
                this._LengthStation._Parameters._DefaultReflector = reflector as Reflector;
                this._LengthStation._Parameters._Tolerance = 0.0005; //Set default tolerance for length to 0.5mm
                this.defaultReflector = this._LengthStation._Parameters._DefaultReflector;
            }
            public override string ToString()
            {
                return $"STM of {this._LengthStation._Name}";
            }

            internal override void ReCreateWhatIsNotSerialized(bool saveSomeMemory = false)
            {
                try
                {

                    this._LengthStation = this._Station as Length.Station;
                    this.FinalModule.SetActiveStationModule(this);
                    //Dans le cas ou l'on ouvre une sauvegarde, l'op s�lectionn�e n'est plus coch�e
                    O.Operation oldOp = (this.FinalModule.OperationManager.AllElements.Find(x => x._Name == this._LengthStation.ParametersBasic._Operation._Name) as O.Operation);
                    if (oldOp != null)
                    {
                        this.FinalModule.OperationManager._SelectedObjectInBlue = oldOp;
                        this.FinalModule.OperationManager.AddSelectedObjects(oldOp);
                        this.FinalModule.OperationManager.UpdateView();
                    }
                    this.AddNewReflectorModules();
                    foreach (Polar.Measure item in this._LengthStation._MeasureOfTheodolite)
                    {
                        item.Reflector = item.Distance.Reflector;
                    }
                    if (this._LengthStation._OpeningMeas != null) { this._LengthStation._OpeningMeas.Reflector = this._LengthStation._OpeningMeas.Distance.Reflector; }
                    if (this._LengthStation._ClosingMeas != null) { this._LengthStation._ClosingMeas.Reflector = this._LengthStation._ClosingMeas.Distance.Reflector; }
                    base.ReCreateWhatIsNotSerialized(saveSomeMemory);
                    ///Reselection du bon instrument dans l'instrument manager apr�s r�ouverture fichier TSU
                    if (this._LengthStation.ParametersBasic._Instrument.Id != null)
                    {
                        var stp = this._LengthStation._Parameters;

                        I.Instrument oldInstrument = this._InstrumentManager.AllElements.OfType<I.Instrument>().FirstOrDefault(
                                   inst => inst._SerialNumber == stp._Instrument._SerialNumber
                                   && (inst._Model == stp._Instrument._Model));

                        I.Sensor i = oldInstrument as I.Sensor;
                        this._InstrumentManager._SelectedObjects.Clear();
                        this._InstrumentManager._SelectedObjectInBlue = i;
                        this._InstrumentManager._SelectedObjects.Add(i);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"Cannot re-create what was not serialized in {this._Name}", ex);
                }
            }
            internal void AddNewReflectorModules()
            //Ajoute un module Management.Instrument
            {
                this.reflectorModule = new I.Manager.Module(this);
                this.reflectorModule.AllElements = this.reflectorModule.GetByClassesAndTypes(new List<I.InstrumentClasses> { I.InstrumentClasses.PRISME });
            }
            #endregion

            #region updates


            #endregion

            #region observations
            public void OnNextBasedOn(I.Instrument TsuObject)
            //already threated in "Management.Instrument.Level SelectionLevel()"
            {
                if (TsuObject is I.Sensor)
                {
                    this.ChangeInstrument(TsuObject as I.Sensor);
                }
                ///this.changeReflector permet de ne pas changer le r�flecteur par d�faut lorsqu'on le fait pour une seule mesure
                if (TsuObject is Reflector && this.changeDefaultReflector)
                {
                    this.ChangeDefaultReflector(TsuObject as Reflector);
                }
            }
            public override void OnNext(TsuObject TsuObject)
            // dispatcheur d'evenement si un type n'est pas connu il est trait� par OnNextBasedOn(TsuObject TsuObject)
            {
                dynamic dynamic = TsuObject;
                OnNextBasedOn(dynamic);
            }
            public void OnNextBasedOn(TSU.Polar.Measure measureFromInstrument)
            //�v�nement li� � la r�ception d'une mesure de l'instrument
            {
                if (!(measureFromInstrument._Status is M.States.Cancel))
                {
                    int index1 = this._LengthStation._MeasureOfTheodolite.FindIndex(x => x._PointName == measureFromInstrument._PointName);
                    if (index1 != -1)
                    {
                        this._LengthStation.ParametersBasic.LastChanged = DateTime.Now;
                        if (!measureFromInstrument._IsClosingMeasure)
                        {
                            this._LengthStation._MeasureOfTheodolite[index1] = measureFromInstrument.DeepCopy();
                            if (this._LengthStation._OpeningMeas == null)
                            {
                                this._LengthStation._OpeningMeas = measureFromInstrument.DeepCopy();
                                this._LengthStation._OpeningMeas._Status = new M.States.Bad();
                                this._LengthStation._OpeningMeas.CommentFromTsunami = R.StringLength_Comment_Opening;
                                this.CalculateVzero(measureFromInstrument);
                            }
                        }
                        else
                        {
                            this.CalculateClosing(measureFromInstrument);
                        }
                        this.UpdateOffsets();
                    }
                    else
                    {

                    }
                    //pour mettre � jour le datagrid alignment result
                    if (this.FinalModule.ParentModule is Common.Guided.Group.Module)
                    {
                        Common.Guided.Group.Module GrGm = this.FinalModule.ParentModule as Common.Guided.Group.Module;
                        // L'admin module est toujours le premier dans la liste des guided modules lors d'un alignement d'aimant
                        // Le step 3 est step avec les r�sultats d'alignement qu'il faut mettre � jour
                        if (GrGm.SubGuidedModules[0].currentIndex == 3)
                        {
                            GrGm.SubGuidedModules[0].CurrentStep.Update();
                        }
                        //pour remettre l'instrument dans le module actif apr�s la mesure
                        if (GrGm.activeSubGuidedModuleIndex == 0)
                        {
                            GrGm.ActiveSubGuidedModule.SetInstrumentInActiveStationModule();
                        }
                    }
                }
            }
            #endregion
            #region SelectionInstrument
            /// <summary>
            /// Changement de l'instrument
            /// </summary>
            /// <param name="newLevel"></param>
            internal void ChangeInstrument(I.Sensor newSensor = null)
            {
                if (newSensor != null)
                {
                    this._LengthStation._Parameters._Instrument = newSensor;
                    this._LengthStation._Parameters._Instrument._Model = newSensor._Model;
                    this._LengthStation._Parameters._Instrument = newSensor;
                    this._LengthStation._Parameters._Instrument._Model = newSensor._Model;
                    this._LengthStation._Parameters._State = new Common.Station.State.ElementSelection();
                    this._LengthStation._Parameters.LastChanged = System.DateTime.Now;
                    I.PolarModule polarModule = this._InstrumentManager.SelectedInstrumentModule as I.PolarModule;
                    polarModule.allowAlwaysButtonMeas = false;
                    this._InstrumentManager.SetNextMeasure(null);
                    if (this._LengthStation._MeasureOfTheodolite.Count != 0)
                    {
                        this.UpdateOffsets();
                    }
                    this.View.UpdateInstrumentView();
                    this.View.RedrawTreeviewParameters();
                }
            }
            internal void ChangeDefaultReflector(Reflector newReflector = null)
            //s�lection du r�flecteur par d�faut pour les points mesur�s
            {
                if (newReflector != null)
                {
                    this.defaultReflector = newReflector;
                    this._LengthStation._Parameters._DefaultReflector = newReflector;
                    this.View.UpdateListMeasure();
                }
            }
            internal Reflector SelectDefaultReflector()
            {
                if (this.reflectorModule != null)
                {
                    var preselected = new List<TsuObject>();
                    
                    TsuObject oldReflector =this.reflectorModule.AllElements
                        .Find(x => (x as Reflector)._Model == this.defaultReflector._Model
                            && (x as Reflector)._SerialNumber == this.defaultReflector._SerialNumber);

                    preselected.Add(oldReflector);
                    var selectables = this._InstrumentManager.GetByClassesAndTypes(null, new List<I.InstrumentTypes>{
                    I.InstrumentTypes.RRR0_5,
                    I.InstrumentTypes.RRR1_5,
                    I.InstrumentTypes.CCR0_5,
                    I.InstrumentTypes.CCR1_5,
                    I.InstrumentTypes.RFI0_5,
                    I.InstrumentTypes.TBR0_5,
                    });

                    // Authorise reflector that have correction coefficients
                    if (this._LengthStation._Parameters._Instrument is I.TotalStation)
                    {
                        I.TotalStation ts = this._LengthStation._Parameters._Instrument as I.TotalStation;
                        selectables = selectables.FindAll(x => ts.EtalonnageParameterList.Find(y => y.PrismeRef._Name == x._Name) != null);
                    }

                    return this.reflectorModule.SelectInstrument(R.StringLength_DefaultReflectorChoice, selectables, multiSelection: false, preselected, false) as Reflector;
                }
                else
                {
                    return null;
                }

            }

            /// <summary>
            /// Change le reflecteur pour la mesure
            /// </summary>
            /// <param name="measTheodolite"></param>
            /// <param name="newReflector"></param>
            /// <param name="updateView"></param>
            internal void ChangeReflector(Polar.Measure measTheodolite, Reflector newReflector = null, bool updateView = true)
            //change le r�flecteur pour la mesure
            {
                this.changeDefaultReflector = false;
                if (newReflector == null && this.reflectorModule != null)
                {
                    newReflector = this.reflectorModule.SelectInstrument(R.StringLength_ReflectorChoice, selectables: null, multiSelection: false, preselected: null) as Reflector;
                }
                if (newReflector != null)
                {
                    if (measTheodolite.Distance.Reflector._Name != newReflector._Name)
                    {
                        measTheodolite.Distance.Reflector = newReflector;
                        measTheodolite.DistanceF1.Reflector = newReflector;
                        measTheodolite.DistanceF2.Reflector = newReflector;
                        measTheodolite.Reflector = newReflector;
                        this._LengthStation.ParametersBasic.LastChanged = DateTime.Now;
                        //this.UpdateOffsets(); 
                        if (updateView)
                        {
                            this.View.UpdateListMeasure();
                            this.FinalModule.Save();
                        }
                    }
                }
                this.changeDefaultReflector = true;
            }

            /// <summary>
            /// S�lection de l'instrument dans l'instrument manager
            /// </summary>
            /// <returns></returns>
            internal I.Sensor SelectInstrument()
            {
                var preselected = new List<TsuObject>();
                if (this._LengthStation._Parameters._Instrument != null)
                {
                    var stp = this._LengthStation._Parameters;

                    I.Instrument oldInstrument = this._InstrumentManager.AllElements.OfType<I.Instrument>().FirstOrDefault(
                               inst => inst._SerialNumber == stp._Instrument._SerialNumber
                               && (inst._Model == stp._Instrument._Model));


                    if (oldInstrument != null)
                        preselected.Add(oldInstrument);
                }
                var selectables = this._InstrumentManager.GetByClass(I.InstrumentClasses.POLAR);
                return this._InstrumentManager.SelectInstrument(R.StringLength_InstrumentChoice, selectables, multiSelection: false, preselected: null) as I.Sensor;
            }
            /// <summary>
            /// Set the instrument from the old station module to the new one 
            /// </summary>
            /// <param name="oldStationModule"></param>
            internal void SetInstrumentInNewStationLengthModule(Length.Station.Module oldStationModule)
            {
                TSU.TsuObject selectedInstrument = this._InstrumentManager.AllElements.Find(x => x._Name == oldStationModule._LengthStation.ParametersBasic._Instrument._Name);
                this._InstrumentManager._SelectedObjects.Clear();
                this._InstrumentManager.AddSelectedObjects(selectedInstrument);
                this._InstrumentManager._SelectedObjectInBlue = selectedInstrument;
                this._InstrumentManager.ValidateSelection();
            }
            /// <summary>
            /// Set the reflector from the old station module to the new one 
            /// </summary>
            /// <param name="oldStationModule"></param>
            internal void SetDefaultMeasParametersInNewStationLengthModule(Length.Station.Module oldStationModule)
            {
                TSU.TsuObject selectedReflector = this.reflectorModule.AllElements.Find(x => x._Name == oldStationModule.defaultReflector._Name);
                this.reflectorModule._SelectedObjects.Clear();
                this.reflectorModule.AddSelectedObjects(selectedReflector);
                this.reflectorModule._SelectedObjectInBlue = selectedReflector;
                this.reflectorModule.ValidateSelection();
                this._LengthStation._Parameters._DefaultDoubleFace = oldStationModule._LengthStation._Parameters._DefaultDoubleFace;
                this._LengthStation._Parameters._DefaultNumberMeas = oldStationModule._LengthStation._Parameters._DefaultNumberMeas;
            }
            #endregion

            #region On changes
            internal override void OnInstrumentChanged(I.Sensor i = null)
            {
                base.OnInstrumentChanged(i);

                this.InvokeOnApplicationDispatcher(this.View.UpdateInstrumentView);
            }
            #endregion

            #region Selection
            /// <summary>
            /// permet � l'utilisateur de changer le nom de la station
            /// </summary>
            /// <param name="MsgResponse"></param>
            internal void RenameStation(string MsgResponse = "NotSet")
            {
                //Toutes les stations level doivent avoir la m�me racine de nom
                this._LengthStation.Rename(MsgResponse);
                this.RemoveFromElementModule(this.measGroupName);
                //Change le nom de station en celui de la station level en enlevant le caract�re donnant le sens
                this.measGroupName = this._LengthStation._Name;
                this.AddElementsToElementModule();
                if (this.FinalModule is Length.Module) (this.FinalModule as Length.Module).View.SetStationNameInTopButton(this);
                //TODO//if (this.FinalModule.ParentModule is TSU.Guided.Group.Module) (this.FinalModule.ParentModule as TSU.Guided.Group.Module).UpdateBigButtonTop();
            }
            /// <summary>
            /// Enl�ve les points de l'element module avec l'origine origin
            /// </summary>
            /// <param name="origin"></param>
            internal void RemoveFromElementModule(string origin)
            {
                CloneableList<E.Point> savedMeasPointList = new CloneableList<E.Point>();
                foreach (E.Point pt in this._MeasPoint)
                {
                    E.Point copyP = pt.DeepCopy();
                    savedMeasPointList.Add(copyP);
                }
                string saveName = this._Name;
                origin = origin.Replace('.', '_');
                this._Name = origin;
                this.ElementModule.Clear(this);
                this._Name = saveName;
                this._MeasPoint = savedMeasPointList;
            }
            /// <summary>
            /// ajoute tous les points theo s�lectionn� dans element module
            /// </summary>
            internal void AddElementsToElementModule()
            //ajoute tous les points theo s�lectionn� dans element module
            {
                string groupName = this.measGroupName;
                groupName = groupName.Replace('.', '_');
                string saveName = this._Name;
                this._Name = groupName;
                foreach (E.Point point in this._MeasPoint)
                {
                    point.Type = E.Point.Types.NewPoint;
                    this.SavePoint(point);
                }
                this._Name = saveName;
            }
            /// <summary>
            /// Ajoute les derni�res mesures dans le final module dans la liste des received measures
            /// </summary>
            private void AddAllMeasuresToFinalModule()
            {
                this.RemoveAllMeasuresFromFinalModule();
                double na = TSU.Preferences.Preferences.NotAvailableValueNa;
                List<Polar.Measure> measTheodoliteToAdd = this._LengthStation._MeasureOfTheodolite.FindAll(x => x.Distance.distAvgMove.Value != na);
                if (measTheodoliteToAdd != null)
                {
                    foreach (Polar.Measure item in measTheodoliteToAdd)
                    {
                        this.FinalModule.ReceivedMeasures.Add(item.Distance);
                    }
                }
            }
            /// <summary>
            /// Enl�ve toutes les mesuresOfOffset du final module dans la liste des received measures
            /// </summary>
            private void RemoveAllMeasuresFromFinalModule()
            {
                this.FinalModule.ReceivedMeasures.RemoveAll(x => x is M.MeasureOfDistance);
            }
            /// <summary>
            /// Set the Geode file path from the old station level Module into the new station level module
            /// </summary>
            /// <param name="oldStationModule"></param>
            internal void SetGeodeFilePathInNewStationLengthModule(Length.Station.Module oldStationModule)
            {
                this._LengthStation.ParametersBasic._GeodeDatFilePath = oldStationModule._LengthStation.ParametersBasic._GeodeDatFilePath;
                this.ShowSaveMessageOfSuccess = oldStationModule.ShowSaveMessageOfSuccess;
            }
            /// <summary>
            /// Set the same coordinate system during cloning of station level Module
            /// </summary>
            /// <param name="oldStationModule"></param>
            internal void SetCoordinateSystemInNewStationLengthModule(Length.Station.Module oldStationModule)
            {
                this.ChangeCoordType(oldStationModule._LengthStation._Parameters._CoordType, false);
            }
            internal void ChangeTeam(string newTeam)
            //change l'�quipe dans chaque station level
            {
                this._LengthStation._Parameters._Team = newTeam;
                this._LengthStation.ParametersBasic.LastChanged = DateTime.Now;
                this.CheckIfReadyToBeSaved();

                TSU.Tsunami2.Preferences.Values.GuiPrefs.AddLastTeamtUsed(newTeam);
            }
            /// <summary>
            ///  Change le num�ro d'op�ration dans chaque station level
            /// </summary>
            /// <param name="o"></param>
            internal override void ChangeOperationID(O.Operation op)
            {

                if (this._LengthStation == null) return;
                if (op.value != this._LengthStation._Parameters._Operation.value)
                {
                    this._LengthStation.ParametersBasic._Operation = op;
                    this._LengthStation._Parameters._Operation = op;
                    this._LengthStation.ParametersBasic.LastChanged = System.DateTime.Now;
                    this.CheckIfReadyToBeSaved();
                }
            }
            /// <summary>
            /// Switch entre la mesure par d�faut en 1 face ou 2 faces
            /// </summary>
            internal void SwitchDefaultDoubleFace()
            {
                if (this._LengthStation._Parameters._DefaultDoubleFace)
                {
                    this._LengthStation._Parameters._DefaultDoubleFace = false;
                }
                else
                {
                    this._LengthStation._Parameters._DefaultDoubleFace = true;
                }
                this.View.UpdateListMeasure();
            }
            /// <summary>
            /// Change la tolerance d'alignement pour la station
            /// </summary>
            /// <param name="p"></param>
            internal void ChangeTolerance(double p)
            {
                this._LengthStation._Parameters._Tolerance = p;
                this.UpdateOffsets();
            }
            /// <summary>
            /// Change le nombre de measure par d�faut
            /// </summary>
            /// <param name="n"></param>
            internal void ChangeDefaultMeasNumber(int n)
            {
                if (n > 0)
                {
                    this._LengthStation._Parameters._DefaultNumberMeas = n;
                }
                this.View.UpdateListMeasure();
            }
            /// <summary>
            /// Change le Z type pour toutes les stations level
            /// </summary>
            /// <param name="coordinatesType"></param>
            internal void ChangeCoordType(CoordinatesType coordinatesType, bool updateOffset = true)
            {
                this._LengthStation._Parameters._CoordType = coordinatesType;
                if (updateOffset) this.UpdateOffsets();
            }
            /// <summary>
            /// Change le commentaire de la mesure
            /// </summary>
            /// <param name="measTheodolite"></param>
            internal void ChangeComment(Polar.Measure measTheodolite, string newComment)
            //change le commentaire
            {
                //newComment = newComment.ToUpper();
                measTheodolite._Date = System.DateTime.Now;
                this._LengthStation.ParametersBasic.LastChanged = System.DateTime.Now;
                measTheodolite.CommentFromUser = newComment;
                this.UpdateOffsets();
            }
            internal void SelectPoints()
            //Selectionne des �lements dans un fichier theo
            {
                this.SetTheoPointsInElementManager();
                //this.ElementModule.View.buttons.ShowOnlyButtons(this.ElementModule.View.buttons.open);
                //this.ElementModule.View.ViewChoiceButton.Available = false;
                // ouvre la fen�tre du module �lement de s�lection des points 
                this.ElementModule.SelectPoints(null, R.T_VALID, R.T_CANCEL, selectOnlyPointsForWire: false, selectOnlyAlesage: true);
                if (!messageSelectStationShown) this.InvokeOnApplicationDispatcher(this.View.ShowMessageSelectStation);
                messageSelectStationShown = true;
            }
            /// <summary>
            /// Set the actual theoretical points in the element manager
            /// </summary>
            internal void SetTheoPointsInElementManager()
            {
                this.SetTheoPointsInElementManager(this.ElementModule);
            }
            /// <summary>
            /// Set the actual theoretical points in the element manager
            /// </summary>
            internal void SetTheoPointsInElementManager(E.Manager.Module elementModule)
            {
                ///Coche les points d�j� s�lectionn� dans l'element module
                ///Ne permet de s�lectionner que les points al�sages, pas les piliers
                elementModule.SetSelectableListToAllAlesages();
                elementModule._SelectedObjects.Clear();
                if (this._TheoPoint != null && elementModule.SelectableObjects != null)
                {
                    List<TsuObject> selected = new List<TsuObject>();
                    foreach (E.Point item in this._TheoPoint)
                    {
                        TsuObject objectFound = elementModule.SelectableObjects.Find(x => ((x as E.Point)._Name == item._Name) && ((x as E.Point)._Origin == item._Origin));
                        if (objectFound != null)
                        {
                            elementModule.AddSelectedObjects(objectFound);
                        }
                    }
                }
            }
            /// <summary>
            /// Set the actual Cala theoretical points in the element manager
            /// </summary>
            internal void SetTheoCalaPointsInElementManager()
            {
                ///Coche les points d�j� s�lectionn� dans l'element module
                this.ElementModule.SetSelectableListToAllAlesages();
                this.ElementModule._SelectedObjects.Clear();
                if (this._TheoPoint != null && this.ElementModule.SelectableObjects != null)
                {
                    foreach (E.Point item in this._TheoPoint)
                    {
                        TsuObject objectFound = this.ElementModule.SelectableObjects.Find(x => ((x as E.Point)._Name == item._Name) && ((x as E.Point)._Origin == item._Origin) && (item.LGCFixOption == LgcPointOption.CALA));
                        if (objectFound != null)
                        {
                            this.ElementModule.AddSelectedObjects(objectFound);
                        }
                    }
                }
            }
            /// <summary>
            /// Override the method in Final Module when received event to add points to measure
            /// </summary>
            /// <param name="compositeSelected"></param>
            internal override void SetPointsToMeasure(EC.CompositeElement compositeSelected)
            {
                CloneableList<E.Point> pointsSelected = new CloneableList<E.Point>();
                if (compositeSelected != null)
                {
                    if (!(compositeSelected is EC.TheoreticalElement) && (!(compositeSelected is EC.Sequence)))
                    ///permet d'�viter le message de non s�lection de point si ouverture d'un fichier theo
                    {
                        foreach (var item in compositeSelected)
                        {
                            if (item is E.Point)
                            {
                                pointsSelected.Add(item as E.Point);
                            }
                            if (item is E.Composites.Magnet)
                            {
                                pointsSelected.AddRange((item as E.Composites.Magnet).GetAlesages());
                            }
                        }
                        this.SetPointsToMeasure(pointsSelected);
                    }
                }
            }
            /// <summary>
            /// AJout de la liste des points � mesurer
            /// </summary>
            /// <param name="pointsSelected"></param>
            /// <param name="isSequence"></param>
            /// <param name="saveModule"></param>
            internal void SetPointsToMeasure(CloneableList<E.Point> pointsSelected, bool saveModule = true)
            {
                if (pointsSelected != null)
                {
                    if (pointsSelected.Count >= 2)
                    {
                        this.AddPoints(pointsSelected);
                        if (this.View != null)
                        {
                            switch (this.View.moduleType)
                            {
                                case ModuleType.Guided:
                                    if (!this.CheckIfAcrossOrigin())
                                    {
                                        this.SortMeasure();
                                    }
                                    else
                                    {
                                        this.SortMeasAcrossOrigin();
                                    }
                                    break;
                                case ModuleType.Advanced:
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                        {
                            if (!this.CheckIfAcrossOrigin())
                            {
                                this.SortMeasure();
                            }
                            else
                            {
                                this.SortMeasAcrossOrigin();
                            }
                        }
                        this.UpdateOffsets(saveModule);
                    }
                    else
                    {
                        this.InvokeOnApplicationDispatcher(this.ShowMeassageNotEnoughPointsSelected);
                    }
                }
            }
            /// <summary>
            /// Affiche le message indiquant au'il n'y a pas assez de points s�lectionn�
            /// </summary>
            private void ShowMeassageNotEnoughPointsSelected()
            {
                new MessageInput(MessageType.Warning, R.StringLength_SelectAtleast2Point).Show();
            }
            private void AddPoints(CloneableList<E.Point> selectedPoint)
            //Ajoute des �l�ments points au set des mesures de nivellement
            {
                try
                {
                    this.CleanMeasureOfTheodolite(selectedPoint);
                    CloneableList<E.Point> cleanedSelectedPoint = CheckIfPointAlreadyMeasured(selectedPoint);
                    foreach (E.Point pt in cleanedSelectedPoint)
                    //Cr�e une nouvelle mesure dans la liste des mesures et y affecte les points s�lectionn�s
                    {
                        if (pt.LGCFixOption != LgcPointOption.CALA) pt.LGCFixOption = LgcPointOption.POIN;
                        this._LengthStation.UpdateListMeasureTheodolite(this, pt);
                        this._LengthStation.ParametersBasic.LastChanged = DateTime.Now;
                        //N'ajoute le nouveau point dans les lectures theo que si le point n'existe pas encore    
                        if (!this._TheoPoint.Contains(pt))
                        {
                            this._TheoPoint.Add(pt.DeepCopy());
                        }
                        else
                        {
                            // doit remplacer les points pour avoir les coordonn�es venant de l'element module � jour.
                            int indexTheo = this._TheoPoint.FindIndex(x => x._Name == pt._Name && x._Origin == pt._Origin);
                            if (indexTheo != -1) this._TheoPoint[indexTheo] = pt.DeepCopy();
                            int indexMeas = this._MeasPoint.FindIndex(x => x._Name == pt._Name && x._Name == pt._Origin);
                            if (indexMeas != -1) this._MeasPoint[indexMeas] = pt.DeepCopy();
                            int i = this._LengthStation._MeasureOfTheodolite.FindIndex(x => x._PointName == pt._Name && x._Point._Origin == pt._Origin);
                            if (i != -1)
                            {
                                this._LengthStation._MeasureOfTheodolite[i]._Point = pt.DeepCopy();
                                this._LengthStation._MeasureOfTheodolite[i].Distance._Point = this._LengthStation._MeasureOfTheodolite[i]._Point;
                                this._LengthStation._MeasureOfTheodolite[i]._OriginalPoint = pt.DeepCopy();
                            }
                        }
                    }
                    //this.CheckPointCalaNumber();
                }
                catch (Exception e)
                {

                    if (e is NullReferenceException) { e.Data.Clear(); }
                    else
                    {
                        throw;
                    }
                }
            }
            private CloneableList<E.Point> CheckIfPointAlreadyMeasured(CloneableList<E.Point> selectedPoint)
            //V�rifie si des points s�lectionn� sont d�j� dans la liste des measures of level et les enl�ve pour �viter les doublons
            {
                CloneableList<E.Point> cleanedSelectedPoint = new CloneableList<E.Point>();
                foreach (E.Point pt in selectedPoint)
                {
                    if (this._LengthStation._MeasureOfTheodolite.FindIndex(x => x._Point._Name == pt._Name && x._Point._Origin == pt._Origin) == -1)
                    {
                        cleanedSelectedPoint.Add(pt);
                    }
                }
                return cleanedSelectedPoint;
            }
            /// <summary>
            /// Lorsqu'on change les options de cala de points d�j� existant dans les modules guid�s
            /// </summary>
            /// <param name="pointList"></param>
            internal void UpdateCalaVSPointToBeAligned(CloneableList<E.Point> pointList)
            {
                foreach (E.Point pt in pointList)
                {
                    int index = this._LengthStation._MeasureOfTheodolite.FindIndex(x => x._PointName == pt._Name && x._Point._Origin == pt._Origin);
                    if (pt.LGCFixOption != LgcPointOption.CALA) pt.LGCFixOption = LgcPointOption.POIN;
                    if (index != -1) this._LengthStation._MeasureOfTheodolite[index]._Point.LGCFixOption = pt.LGCFixOption;
                    int index2 = this._TheoPoint.FindIndex(x => x._Name == pt._Name);
                    if (index2 != -1)
                    {
                        this._TheoPoint[index2].LGCFixOption = pt.LGCFixOption;
                        this._LengthStation.ParametersBasic.LastChanged = DateTime.Now;
                    }
                }
            }
            /// <summary>
            /// Enl�ve les measures of theodolite qui ne sont plus s�lectionn�es dans l'element manager
            /// Enl�ve ces mesures dans toutes les stations length, pts theo et pts mesur�s
            /// </summary>
            /// <param name="selectedPoint"></param>
            private void CleanMeasureOfTheodolite(CloneableList<E.Point> selectedPoint)
            {

                CloneableList<Polar.Measure> measToRemove = new CloneableList<Polar.Measure>();
                foreach (Polar.Measure meas in this._LengthStation._MeasureOfTheodolite)
                {
                    if (selectedPoint.FindIndex(x => x._Name == meas._Point._Name && x._Origin == meas._Point._Origin) == -1)
                    {
                        measToRemove.Add(meas);
                    }
                }
                foreach (Polar.Measure meas in measToRemove)
                {
                    this.DeletePointInAllStation(meas._Point._Name, false);
                }
            }
            /// <summary>
            /// Enleve le point toutes les stations (aller-retour et reprise)
            /// </summary>
            /// <param name="pointName"></param>
            internal void DeletePointInAllStation(string pointName, bool refreshOffset = true)
            {
                E.Point point = this._TheoPoint.Find(x => x._Name == pointName).DeepCopy();
                int i = this._LengthStation._MeasureOfTheodolite.FindIndex(x => x._PointName == point._Name && x._Point._Origin == point._Origin);
                if (i != -1)
                {
                    this._LengthStation._MeasureOfTheodolite.RemoveAt(i);
                    this._LengthStation.ParametersBasic.LastChanged = DateTime.Now;
                }
                this.ElementModule.Clear(this);
                this._TheoPoint.Remove(this._TheoPoint.Find(x => x._Name == point._Name && x._Origin == point._Origin));
                this._MeasPoint.Remove(this._MeasPoint.Find(x => x._Name == point._Name));
                if (refreshOffset)
                {
                    this.AddElementsToElementModule();
                    this.UpdateOffsets();
                }
            }
            /// <summary>
            /// Trie les mesures  dans le sens des Dcum ou inverse Dcum
            /// </summary>
            internal void SortMeasure()
            {
                this.SortPoints(this._TheoPoint);
                this.SortPoints(this._MeasPoint);
                this.SortPoints(this.FinalModule.PointsToBeMeasured);
                this.SortPoints(this.FinalModule.CalaPoints);
                this.SortPoints(this.FinalModule.PointsToBeAligned);
                switch (this.sortDirection)
                {
                    case SortDirection.Dcum:
                        //this._WorkingStationLevel.SortMeasures();
                        this._LengthStation.SortMeasures();
                        break;
                    case SortDirection.InvDcum:
                        //this._WorkingStationLevel.ReverseMeasures();
                        this._LengthStation.ReverseMeasures();
                        break;
                    default:
                        break;
                }
            }
            /// <summary>
            /// Trie les points et les mesures pour un fil passant par l'origine d'un acc�l�rateur
            /// </summary>
            private void SortMeasAcrossOrigin()
            {
                if (this._TheoPoint.FindIndex(X => X._Accelerator == "SPS") != -1)
                {
                    this.SortPointsForSPSOrigin(this._TheoPoint);
                    this.SortPointsForSPSOrigin(this._MeasPoint);
                    this.SortPointsForSPSOrigin(this.FinalModule.PointsToBeMeasured);
                    this.SortPointsForSPSOrigin(this.FinalModule.CalaPoints);
                    this.SortPointsForSPSOrigin(this.FinalModule.PointsToBeAligned);
                    this._LengthStation.SortMeasForSPSOrigin();
                }
                if (this._TheoPoint.FindIndex(X => X._Accelerator == "PR") != -1)
                {
                    this.SortPointsForPROrigin(this._TheoPoint);
                    this.SortPointsForPROrigin(this._MeasPoint);
                    this.SortPointsForPROrigin(this.FinalModule.PointsToBeMeasured);
                    this.SortPointsForPROrigin(this.FinalModule.CalaPoints);
                    this.SortPointsForPROrigin(this.FinalModule.PointsToBeAligned);
                    this._LengthStation.SortMeasForPROrigin();
                }
                if (this._TheoPoint.FindIndex(X => X._Accelerator == "BR") != -1)
                {
                    this.SortPointsForBROrigin(this._TheoPoint);
                    this.SortPointsForBROrigin(this._MeasPoint);
                    this.SortPointsForBROrigin(this.FinalModule.PointsToBeMeasured);
                    this.SortPointsForBROrigin(this.FinalModule.CalaPoints);
                    this.SortPointsForBROrigin(this.FinalModule.PointsToBeAligned);
                    this._LengthStation.SortMeasForBROrigin();
                }
                if (this._TheoPoint.FindIndex(X => X._Accelerator == "DR") != -1)
                {
                    this.SortPointsForDROrigin(this._TheoPoint);
                    this.SortPointsForDROrigin(this._MeasPoint);
                    this.SortPointsForDROrigin(this.FinalModule.PointsToBeMeasured);
                    this.SortPointsForDROrigin(this.FinalModule.CalaPoints);
                    this.SortPointsForDROrigin(this.FinalModule.PointsToBeAligned);
                    this._LengthStation.SortMeasForDROrigin();
                }
            }
            /// <summary>
            /// Check if the wire is crossing the origin of an accelerator
            /// </summary>
            private bool CheckIfAcrossOrigin()
            {
                // Pour le SPS cherche si la liste des points contient un aimant dans le sextant 6 et dans le sextant 1
                if (this._TheoPoint.FindIndex(X => X._Accelerator == "SPS" && X._Numero.StartsWith("6")) != -1
                    && this._TheoPoint.FindIndex(X => X._Accelerator == "SPS" && X._Numero.StartsWith("1")) != -1)

                    return true;
                //// Pour le PS ring cherche si dans les num�ros, on a des 0 et des 9 en m�me temps
                if (this._TheoPoint.FindIndex(X => X._Accelerator == "PR" && X._Numero.StartsWith("0")) != -1
                && this._TheoPoint.FindIndex(X => X._Accelerator == "PR" && X._Numero.StartsWith("9")) != -1)

                    return true;
                //// Pour le booster ring cherche si dans les num�ros, on a des 1 et des 16 en m�me temps
                if (this._TheoPoint.FindIndex(X => X._Accelerator == "BR" && X._Numero.StartsWith("1")
                && !X._Numero.Contains("16")
                && !X._Numero.Contains("15")
                && !X._Numero.Contains("14")
                && !X._Numero.Contains("13")
                && !X._Numero.Contains("12")
                && !X._Numero.Contains("11")
                && !X._Numero.Contains("10")) != -1
                && this._TheoPoint.FindIndex(X => X._Accelerator == "BR" && X._Numero.StartsWith("16")) != -1)

                    return true;
                //// Pour AD ring cherche si dans les num�ros, on a des 0 et des 5 en m�me temps
                if (this._TheoPoint.FindIndex(X => X._Accelerator == "DR" && X._Numero.StartsWith("0")) != -1
                && this._TheoPoint.FindIndex(X => X._Accelerator == "DR" && X._Numero.StartsWith("5")) != -1)

                    return true;
                return false;
            }
            /// <summary>
            /// Inverse le status d'une mesure entre Unknown et bad.
            /// </summary>
            /// <param name="pointName"></param>
            internal void ReverseMeasureStatus(string pointName)
            {
                Polar.Measure m = this._LengthStation._MeasureOfTheodolite.Find(x => x._PointName == pointName);
                if (m != null)
                {
                    if (m._Status is M.States.Bad)
                    {
                        m._Status = new M.States.Unknown();
                        if (m.CommentFromTsunami == R.StringLength_BadMeas) m.CommentFromTsunami = "";
                    }
                    else
                    {

                        m._Status = new M.States.Bad();
                        //Pour mesure, on a droit � 80 caract�re pour le comnmentaire
                        if ((m.Comment.Length + R.StringLength_BadMeas.Length) <= 80 && !m.Comment.Contains(R.StringLength_BadMeas))
                            m.CommentFromTsunami = R.StringLength_BadMeas + m.CommentFromTsunami;

                    }
                    this._LengthStation.ParametersBasic.LastChanged = DateTime.Now;
                }
                this.UpdateOffsets();
            }
            /// <summary>
            /// Lance une mesure
            /// </summary>
            /// <param name="measureTheodolite"></param>
            internal void LaunchMeasure(Polar.Measure measureTheodolite)
            {
                I.PolarModule polarModule = this._InstrumentManager.SelectedInstrumentModule as I.PolarModule;
                this.SetTobeMeasureData(measureTheodolite);
                this.InvokeOnApplicationDispatcher(polarModule.View.MeasureAsync);
                //this.InvokeOnApplicationDispatcher(this.View.UpdateInstrumentView);
            }
            /// <summary>
            /// Lance un contr�le fermeture
            /// </summary>
            internal void LaunchOpeningCheck()
            {
                if (this._LengthStation._OpeningMeas != null)
                {
                    Polar.Measure closing = this._LengthStation._OpeningMeas.DeepCopy();
                    closing._IsClosingMeasure = true;
                    this.LaunchMeasure(closing);
                }
            }
            /// <summary>
            /// Check if the toBeMeasuredData has to be changed in the polar Module
            /// </summary>
            /// <param name="measureTheodolite"></param>
            internal void SetTobeMeasureData(Polar.Measure measureTheodolite)
            {
                if (this._InstrumentManager != null)
                {
                    if (this._InstrumentManager.SelectedInstrumentModule != null)
                    {
                        if (measureTheodolite != null && measureTheodolite._PointName != this._LengthStation._Parameters._StationPoint._Name)
                        {
                            I.PolarModule polarModule = this._InstrumentManager.SelectedInstrumentModule as I.PolarModule;
                            Polar.Measure toBeMeasured = polarModule._ToBeMeasureTheodoliteData;
                            if (toBeMeasured != null)
                            {
                                ///V�rifie que quelque chose doit �tre chang� dans la mesure � faire par le polar module
                                if (toBeMeasured._PointName != measureTheodolite._PointName
                                    || toBeMeasured.Reflector._Name != measureTheodolite.Reflector._Name
                                    || toBeMeasured.Extension.Value != measureTheodolite.Extension.Value
                                    || toBeMeasured.Face != measureTheodolite.Face
                                    || toBeMeasured.NumberOfMeasureToAverage != measureTheodolite.NumberOfMeasureToAverage)
                                {
                                    ChangePolarToBeMeasuredData(measureTheodolite);
                                }
                                if (polarModule.beingMeasured == null)
                                {
                                    ChangePolarToBeMeasuredData(measureTheodolite);
                                }
                            }
                            else
                            {
                                ChangePolarToBeMeasuredData(measureTheodolite);
                            }
                        }
                        else
                        {
                            this._InstrumentManager.SetNextMeasure(null);
                        }
                    }
                }
            }
            /// <summary>
            /// Change the toBeMeasuredData in the polar module
            /// </summary>
            /// <param name="measureTheodolite"></param>
            /// <param name="polarModule"></param>
            private void ChangePolarToBeMeasuredData(Polar.Measure measureTheodolite)
            {
                I.PolarModule polarModule = this._InstrumentManager.SelectedInstrumentModule as I.PolarModule;
                E.Point originalPoint = this._TheoPoint.Find(x => x._Name == measureTheodolite._PointName);
                if (originalPoint != null)
                {
                    measureTheodolite._OriginalPoint = originalPoint;
                }
                Polar.Measure copy = measureTheodolite.DeepCopy();
                copy._Status = new M.States.Unknown();
                this.CalculateCorrectedAngleForGoto(copy);
                if (copy.HasCorrectedAngles)
                {
                    polarModule.GotoWanted = true;
                }
                copy.Clean();
                this._InstrumentManager.SetNextMeasure(copy);
                this.actualPointFocussed = copy._PointName;
                //if (this.View.instrumentView != null) this.View.instrumentView.UpdateView();
            }
            /// <summary>
            /// update the toBeMeasuredData in the polar module with new goto
            /// </summary>
            /// <param name="measureTheodolite"></param>
            /// <param name="polarModule"></param>
            internal void UpdatePolarToBeMeasuredData()
            {

                if (this._InstrumentManager != null)
                {
                    I.PolarModule polarModule = this._InstrumentManager.SelectedInstrumentModule as I.PolarModule;
                    if (polarModule != null)
                    {
                        Polar.Measure currentMeas = polarModule._ToBeMeasureTheodoliteData;
                        if (currentMeas != null)
                        {
                            int index1 = this._LengthStation._MeasureOfTheodolite.FindIndex(x => x._PointName == currentMeas._PointName);
                            if (index1 != -1)
                            {
                                this.ChangePolarToBeMeasuredData(this._LengthStation._MeasureOfTheodolite[index1]);
                            }
                        }
                    }
                }
            }
            /// <summary>
            /// Met � jour le goto si h instrument chang�e
            /// </summary>
            internal void UpdateGotoForAllMeas()
            {
                I.PolarModule polarModule = this._InstrumentManager.SelectedInstrumentModule as I.PolarModule;
                if (polarModule.GotoWanted == true)
                {
                    foreach (Polar.Measure mes in this._LengthStation._MeasureOfTheodolite)
                    {
                        this.CalculateCorrectedAngleForGoto(mes);
                    }
                }
            }
            #endregion

            #region computes & checks
            /// <summary>
            /// Check if the station can be saved and update the state of the working station level
            /// </summary>
            internal bool CheckIfReadyToBeSaved()
            {
                bool a = this._LengthStation._Parameters._Team != R.String_UnknownTeam;
                bool b = this._LengthStation._Parameters._Instrument != null;
                bool c = this._LengthStation._MeasureOfTheodolite.Count != 0;
                bool d = this._LengthStation._MeasureOfTheodolite.FindIndex(x => x.Distance.Raw.Value != TSU.Preferences.Preferences.NotAvailableValueNa) != -1;
                bool e = this._LengthStation._Parameters._Operation.IsSet;
                bool f = this._LengthStation.ParametersBasic.LastChanged > this._LengthStation.ParametersBasic.LastSaved;

                if (a && b && c && d && f)
                {

                    this._LengthStation._Parameters._State = new Common.Station.State.StationLengthReadyToBeSaved();
                    return true;
                }
                else
                {
                    if (a && b && c && d)
                    {
                        this._LengthStation._Parameters._State = new Common.Station.State.StationLengthSaved();
                        return false;
                    }
                    if (a && b && c)
                    {
                        this._LengthStation._Parameters._State = new Common.Station.State.MeasureToEnter();
                        return false;
                    }
                }
                return false;
            }
            internal void UpdateOffsets(bool updateView = true, bool saveModule = true)
            //met � jour toutes les mesures de nivellement
            {
                this._LengthStation.UpdateTheoCoordinates(this._TheoPoint);
                this._LengthStation.ResetMeas();
                //Efface les coordonn�es mesur�es et les remplace par les coordonn�es th�oriques
                this.RemoveFromElementModule(this.measGroupName);
                this._MeasPoint.Clear();
                this.ChooseDefaultStationAndReference();
                this._LengthStation.CalculateDistHorizTheo();
                this._LengthStation.CalculateDistHorizMes();
                this._LengthStation.CalculateDiffOffsetDistance();
                this._LengthStation.CalculateAverageMove();
                this.CheckIfReadyToBeSaved();
                this.AddAllMeasuresToFinalModule();
                this.AddElementsToElementModule();
                if (updateView) this.InvokeOnApplicationDispatcher(this.View.UpdateView);
                //Sauvegarde du module mise en longueur
                if (saveModule) this.FinalModule.Save();
            }
            /// <summary>
            /// Set a default station and reference if not yet been set
            /// </summary>
            private void ChooseDefaultStationAndReference()
            {
                if (this._LengthStation._Parameters._StationPoint._Name == R.String_Unknown || this._LengthStation._MeasureOfTheodolite.FindIndex(x => x._PointName == this._LengthStation._Parameters._StationPoint._Name) == -1)
                {
                    if (this._LengthStation._MeasureOfTheodolite.Count > 1)
                    {
                        this.SetPointAsStation(this._LengthStation._MeasureOfTheodolite[0]._PointName);
                        //Pas de point cala obligatoire
                        //if (this._LengthStation._MeasureOfTheodolite.FindAll(x => x._Point.LGCFixOption == LgcPointOption.CALA).Count == 0)
                        //{
                        //    this._TheoPoint[this._TheoPoint.Count - 1].LGCFixOption = TSU.ENUM.LgcPointOption.CALA;
                        //    int index2 = this.FinalModule.CalaPoints.FindIndex(x => x._Name == this._TheoPoint[this._TheoPoint.Count - 1]._Name);
                        //    if (index2 != -1)
                        //    {
                        //        this.FinalModule.CalaPoints.RemoveAt(index2);
                        //    }
                        //    this.FinalModule.CalaPoints.Add(this._TheoPoint[this._TheoPoint.Count - 1]);
                        //    int index1 = this.FinalModule.PointsToBeAligned.FindIndex(x => x._Name == this._TheoPoint[this._TheoPoint.Count - 1]._Name);
                        //    if (index1 != -1) { this.FinalModule.PointsToBeAligned.RemoveAt(index1); }
                        //    int index3 = this._LengthStation._MeasureOfTheodolite.FindIndex(x => x._PointName == this._TheoPoint[this._TheoPoint.Count - 1]._Name);
                        //    if (index3 != -1) this._LengthStation._MeasureOfTheodolite[index3]._Point.LGCFixOption = TSU.ENUM.LgcPointOption.CALA;
                        //    this._LengthStation.ParametersBasic.LastChanged = DateTime.Now;
                        //}
                    }
                }
            }
            /// <summary>
            /// Update the stationned point
            /// </summary>
            /// <param name="pointName"></param>
            internal void SetPointAsStation(string pointName)
            {
                ///Le point stationn� ne doit pas �tre en cala dans le cas des quads SPS
                //int index2 = this._TheoPoint.FindIndex(x => x._Name == pointName);
                //if (index2 != -1)
                //{
                //    this._TheoPoint[index2].LGCFixOption = TSU.ENUM.LgcPointOption.CALA;
                //    this.FinalModule.CalaPoints.Add(this._TheoPoint[index2]);
                //}
                //int index1 = this.FinalModule.PointsToBeAligned.FindIndex(x => x._Name == pointName);
                //if (index1 != -1) { this.FinalModule.PointsToBeAligned.RemoveAt(index1); }
                int index5 = this._LengthStation._MeasureOfTheodolite.FindIndex(x => x._PointName == pointName);
                if (index5 != -1)
                {
                    //this._LengthStation._MeasureOfTheodolite[index5]._Point.LGCFixOption = TSU.ENUM.LgcPointOption.CALA;
                    //une station ne peut pas �tre en bad.
                    if (this._LengthStation._MeasureOfTheodolite[index5]._Status is M.States.Bad)
                    {
                        this.ReverseMeasureStatus(pointName);
                    }
                    this._LengthStation.SetStationPoint(this._LengthStation._MeasureOfTheodolite[index5]._Point);
                    this._LengthStation.FullCleanAllMeas();
                }
            }
            /// <summary>
            /// Calcule l'erreur de fermeture.
            /// </summary>
            public void CalculateClosing(Polar.Measure closingMeas)
            {
                bool over = false;
                bool dummy = false;
                Polar.Measure diff;
                string message;
                this._LengthStation._ClosingMeas = closingMeas.DeepCopy();
                this._LengthStation._ClosingMeas._Status = new M.States.Bad();
                this._LengthStation._ClosingMeas.CommentFromTsunami = R.StringLength_Comment_Closing;
                double tolH = this._LengthStation._Parameters.Tolerance.Opposite_Face.H_CC;
                double tolV = this._LengthStation._Parameters.Tolerance.Opposite_Face.V_CC;
                double tolD = this._LengthStation._Parameters.Tolerance.Opposite_Face.D_mm;
                diff = Survey.GetDifference(closingMeas, this._LengthStation._OpeningMeas, tolH, tolV, tolD, ref over, out message, ref dummy);
                this.tempMsg = message;
                if (over)
                {
                    this.InvokeOnApplicationDispatcher(this.ShowMessageBadClosing);
                }
                else
                {
                    this.InvokeOnApplicationDispatcher(this.ShowMessageGoodClosing);
                }
            }

            private void ShowMessageGoodClosing()
            {
                string titleAndMessage = string.Format(R.StringLength_MessageClosingError, this.tempMsg);
                new MessageInput(MessageType.GoodNews, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_OK + "!" }
                }.Show();
            }

            private void ShowMessageBadClosing()
            {
                string titleAndMessage = string.Format(R.StringLength_MessageClosingError, this.tempMsg);
                new MessageInput(MessageType.Critical, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_OK + "!" }
                }.Show();
            }
            /// <summary>
            /// Calcule le vZero de la station
            /// </summary>
            /// <param name="meas"></param>
            public void CalculateVzero(Polar.Measure meas)
            {
                if (this._LengthStation._Parameters._StationPoint._Name != R.String_Unknown && this._LengthStation._Parameters._CoordType == CoordinatesType.CCS)
                {
                    DoubleValue vZero = new DoubleValue();
                    switch (this._LengthStation._Parameters._CoordType)
                    {
                        case CoordinatesType.CCS:
                            vZero = Survey.VZeroOneMeasure(this._LengthStation._Parameters._StationPoint._Coordinates.Ccs, meas._Point._Coordinates.Ccs, meas.Angles.Raw.Horizontal);
                            break;
                        case CoordinatesType.SU:
                            vZero = Survey.VZeroOneMeasure(this._LengthStation._Parameters._StationPoint._Coordinates.Su, meas._Point._Coordinates.Su, meas.Angles.Raw.Horizontal);
                            break;
                        default:
                            vZero = Survey.VZeroOneMeasure(this._LengthStation._Parameters._StationPoint._Coordinates.Ccs, meas._Point._Coordinates.Ccs, meas.Angles.Raw.Horizontal);
                            break;
                    }
                    if (vZero != null)
                    {
                        this._LengthStation._Parameters.Vzero = vZero;
                    }
                }
            }
            /// <summary>
            /// Calcule le goto horizontal et vertical pour la mesure � faire
            /// </summary>
            /// <param name="meas"></param>
            public void CalculateCorrectedAngleForGoto(Polar.Measure meas)
            {
                double na = TSU.Preferences.Preferences.NotAvailableValueNa;
                if (this._LengthStation._Parameters.Vzero != na)
                {
                    DoubleValue gotoHorizontal = null;
                    switch (this._LengthStation._Parameters._CoordType)
                    {
                        case CoordinatesType.CCS:
                            gotoHorizontal = Survey.GotoHorizOneMeasure(this._LengthStation._Parameters._StationPoint._Coordinates.Ccs,
                                meas._Point._Coordinates.Ccs,
                                this._LengthStation._Parameters.Vzero);
                            break;
                        case CoordinatesType.SU:
                            gotoHorizontal = Survey.GotoHorizOneMeasure(this._LengthStation._Parameters._StationPoint._Coordinates.Su,
                                meas._Point._Coordinates.Su,
                                this._LengthStation._Parameters.Vzero);
                            break;
                        default:
                            break;
                    }
                    DoubleValue gotoVerticalFace1 = null;
                    switch (this._LengthStation._Parameters._CoordType)
                    {
                        case CoordinatesType.CCS:
                            gotoVerticalFace1 = Survey.GotoVerticOneMeasure(this._LengthStation._Parameters._StationPoint._Coordinates.Ccs,
                                meas._Point._Coordinates.Ccs,
                                meas.Extension.Value,
                                this._LengthStation._Parameters.HInstrument);
                            break;
                        case CoordinatesType.SU:
                            gotoVerticalFace1 = Survey.GotoVerticOneMeasure(this._LengthStation._Parameters._StationPoint._Coordinates.Su,
                                meas._Point._Coordinates.Su,
                                meas.Extension.Value,
                                this._LengthStation._Parameters.HInstrument);
                            break;
                        default:
                            break;
                    }
                    if (gotoHorizontal != null && gotoVerticalFace1 != null)
                    {
                        meas.Angles.Corrected.Horizontal = gotoHorizontal;
                        meas.Angles.Corrected.Vertical = gotoVerticalFace1;
                    }
                }
            }
            #endregion
            #region Save
            /// <summary>
            /// Exporte la station
            /// </summary>
            internal void ExportToGeode()
            //sauvegarde des mesures dans un fichier
            {
                DsaFlag dsaFlag = DsaFlag.GetByNameOrAdd(this.FinalModule.DsaFlags, "ShowSaveMessageOfSuccess");

                if (this._LengthStation._Parameters._State is Common.Station.State.StationLengthReadyToBeSaved)
                {
                    Result saveDatOK = this.ExportDAT();
                    string respond = "";
                    if (saveDatOK.Success)
                    {
                        string titleAndMessage = String.Format(R.StringLength_StationSaved, ":");
                        base.ShowGeodeExportSuccess(titleAndMessage, this._LengthStation.ParametersBasic._GeodeDatFilePath);
                        //    respond = new MessageInput(MessageType.GoodNews, titleAndMessage)
                        //    {
                        //        ButtonTexts = new List<string>() { R.T_OK, R.T_OpenFileLocation },
                        //        DontShowAgain = dsaFlag
                        //    }.Show().TextOfButtonClicked;
                        //}
                        //if (respond == R.T_OpenFileLocation)
                        //{
                        //    if (System.IO.File.Exists(this._LengthStation.ParametersBasic._GeodeDatFilePath))
                        //    {
                        //        System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo("explorer.exe", " /select, " + this._LengthStation.ParametersBasic._GeodeDatFilePath));
                        //    }
                    }
                    this.FinalModule.Save();
                }
                this.View.UpdateView();
            }
            /// <summary>
            /// Exporte la station en fichier dat
            /// </summary>
            /// <returns></returns>
            internal Result ExportDAT()
            //sauvegarde la station nivellement dans un fichier dat pour insertion dans Geode
            {
                TSU.Result result = new Result();
                List<string> textToWriteInFile = new List<string>();
                int lineNumber = 1;
                string lineText = "";
                string filepath =TSU.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay + TSU.IO.SUSoft.Geode.GetFileName(this._LengthStation._Parameters, "LENGTH");
                List<int> lineNumbersAlreadyRecorded = new List<int>();
                //string filepath = String.Format("{0}{1}_{2}_{3}_{4}_{5}.dat",TSU.Tsunami2.TsunamiPreferences.Values.Paths.MeasureOfTheDay,
                //    this._LengthStation._Parameters._Operation.value,
                //    this._LengthStation._Parameters._Team, DateTime.Now.Day.ToString("00"), 
                //    DateTime.Now.Month.ToString("00"), (DateTime.Now.Year - 2000).ToString());
                //lecture du fichier existant s'il existe
                try
                {
                    string[] readedTextInFile;
                    readedTextInFile = System.IO.File.ReadAllLines(filepath);
                    ///Ajoute la ligne RE en ent�te si inexistante
                    if (readedTextInFile.Length > 0)
                    {
                        if (!readedTextInFile[0].Contains(";RE;"))
                        {
                            this.AddLineRE(textToWriteInFile, ref lineNumber, ref lineText);
                        }
                    }
                    else
                    {
                        this.AddLineRE(textToWriteInFile, ref lineNumber, ref lineText);
                    }
                    foreach (string line in readedTextInFile)
                    {
                        if (line != "")
                        {
                            textToWriteInFile.Add(line);
                            //int numberOfLine;
                            ////incr�mentation du num�ro de ligne
                            //if (int.TryParse(line.Substring(0, 3), out numberOfLine))
                            //{
                            //    lineNumber = numberOfLine + 1;
                            //}
                            if (line.IndexOf(this._LengthStation._Name) != -1)
                            {
                                lineNumbersAlreadyRecorded.Add(lineNumber);
                            }
                            lineNumber++;
                        }
                    }
                }
                catch (Exception e)
                //Exception g�n�r�e si fichier inexistant
                {
                    //Cr�e le r�pertoire Mesures si n�cessaire
                    if (!System.IO.Directory.Exists(TSU.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay)) { System.IO.Directory.CreateDirectory(TSU.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay); }
                    this.AddLineRE(textToWriteInFile, ref lineNumber, ref lineText);
                    e.Data.Clear();
                }
                finally
                {
                    //Ajoute le texte avec la date de compensation du TS60
                    string checkAndAdjust = "";
                    if (this._LengthStation.ParametersBasic._Instrument is TSU.Common.Instruments.Device.TS60.Instrument)
                    {
                        TSU.Common.Instruments.Device.TS60.Instrument TS60 = this._LengthStation.ParametersBasic._Instrument as TSU.Common.Instruments.Device.TS60.Instrument;
                        if (TS60._LastCompensationDate != DateTime.MinValue)
                        {
                            checkAndAdjust = "Check and Adjust ";
                            if (TS60._Comp_L) checkAndAdjust += "l, t,";
                            if (TS60._Comp_i_indexV) checkAndAdjust += "i, c,";
                            if (TS60._Comp_a_AxeA) checkAndAdjust += "a,";
                            if (TS60._Comp_ATRHz) checkAndAdjust += "ATR";
                            checkAndAdjust += " done the ";
                            checkAndAdjust += TS60._LastCompensationDate.Day + "-";
                            checkAndAdjust += TS60._LastCompensationDate.Month + "-";
                            checkAndAdjust += TS60._LastCompensationDate.Year;
                        }
                    }
                    //Ecriture ent�te station
                    this.AddLineRefStation(textToWriteInFile, ref lineNumber, lineNumbersAlreadyRecorded);
                    //Ecrit la station length dans le fichier
                    this.AddStationPoint(textToWriteInFile, ref lineNumber, checkAndAdjust);
                    List<string> textAllAH = new List<string>();
                    List<string> textAllAV = new List<string>();
                    List<string> textAllDist = new List<string>();
                    List<string> textAllEcart = new List<string>();
                    if (this._LengthStation._OpeningMeas != null)
                    {
                        Polar.Measure mOpen = this._LengthStation._MeasureOfTheodolite.Find(x => x._PointName == this._LengthStation._OpeningMeas._PointName && x._Date == this._LengthStation._OpeningMeas._Date);
                        if (mOpen == null)
                        {
                            this.AddAHMeas(ref lineNumber, textAllAH, this._LengthStation._OpeningMeas);
                            this.AddAVMeas(ref lineNumber, textAllAV, this._LengthStation._OpeningMeas);
                            this.AddDDMeas(ref lineNumber, textAllDist, this._LengthStation._OpeningMeas);
                        }
                    }
                    if (this._LengthStation._ClosingMeas != null)
                    {
                        Polar.Measure mClose = this._LengthStation._MeasureOfTheodolite.Find(x => x._PointName == this._LengthStation._ClosingMeas._PointName && x._Date == this._LengthStation._ClosingMeas._Date);
                        if (mClose == null)
                        {
                            this.AddAHMeas(ref lineNumber, textAllAH, this._LengthStation._ClosingMeas);
                            this.AddAVMeas(ref lineNumber, textAllAV, this._LengthStation._ClosingMeas);
                            this.AddDDMeas(ref lineNumber, textAllDist, this._LengthStation._ClosingMeas);
                        }
                    }
                    foreach (Polar.Measure meas in this._LengthStation._MeasureOfTheodolite)
                    {
                        this.AddAHMeas(ref lineNumber, textAllAH, meas);
                        this.AddAVMeas(ref lineNumber, textAllAV, meas);
                        this.AddDDMeas(ref lineNumber, textAllDist, meas);
                        this.AddEcarts(ref lineNumber, textAllEcart, meas);
                    }
                    textToWriteInFile.AddRange(textAllAH);
                    textToWriteInFile.AddRange(textAllAV);
                    textToWriteInFile.AddRange(textAllDist);
                    textToWriteInFile.AddRange(textAllEcart);
                    System.IO.File.WriteAllLines(filepath, textToWriteInFile.ToArray());
                    //Affichage du fichier enregistr� et s�lection de l'�tat de la station
                    if (System.IO.File.Exists(filepath))
                    {
                        result.Success = true;
                        this._LengthStation.ParametersBasic.LastSaved = DateTime.Now;
                        this._LengthStation._Parameters._State = new Common.Station.State.StationLengthSaved();
                        this._Station.ParametersBasic._GeodeDatFilePath = filepath;
                    }
                    else
                    {
                        result.Success = false;
                        result.ErrorMessage = R.StringLevel_DatNotCreated;
                        this._LengthStation._Parameters._State = new Common.Station.State.StationLengthReadyToBeSaved();
                        string titleAndMessage = String.Format(R.StringLength_StationSaved, filepath);
                        new MessageInput(MessageType.Critical, titleAndMessage).Show();
                    }
                }
                return result;
            }
            /// <summary>
            /// Ecrit la ligne d'information d�but de station pour v�rifier si station en double
            /// </summary>
            /// <param name="lineNumber"></param>
            /// <returns></returns>
            private void AddLineRefStation(List<string> textToWriteInFile, ref int lineNumber, List<int> lineNumberAlreadyRecorded)
            {
                string lineText = "! ";
                lineText = String.Format("{0}----------", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += String.Format("{0} Length Station recorded at ", this._LengthStation._Name);
                lineText += String.Format("{0,-8}", DateTime.Now.ToString("T", System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR")));
                if (lineNumberAlreadyRecorded.Count != 0)
                {
                    int i = 1;
                    foreach (int item in lineNumberAlreadyRecorded)
                    {
                        if (i == 1) { lineText += String.Format(" ALREADY RECORDED AT LINE {0}", item); }
                        else { lineText += String.Format(", {0}", item); }
                        i++;
                    }
                }
                lineText += "----------";
                textToWriteInFile.Add(lineText);
            }
            /// <summary>
            /// Ajoute le point stationn�
            /// </summary>
            /// <param name="textToWriteInFile"></param>
            /// <param name="lineNumber"></param>
            private void AddStationPoint(List<string> textToWriteInFile, ref int lineNumber, string additionalText)
            {
                //Ecriture nom station
                string lineText = "";
                lineText = String.Format("  {0}OB;", WriteLineNumber(lineNumber, lineText));
                lineNumber++;
                lineText += String.Format("{0,-9};", this._LengthStation._Parameters._Instrument._Model);
                lineText += String.Format("{0,-12};", this._LengthStation._Parameters._Instrument._SerialNumber);
                if (this._LengthStation._Parameters._StationPoint._Point != "")
                //cas d'un al�sage
                {
                    lineText += String.Format("{0,-10};", this._LengthStation._Parameters._StationPoint._Accelerator);
                    lineText += String.Format("{0,-33};;", this._LengthStation._Parameters._StationPoint._ClassAndNumero + "." + this._LengthStation._Parameters._StationPoint._Point);
                }
                else
                //cas d'un pilier
                {
                    lineText += String.Format("{0,-10};", this._LengthStation._Parameters._StationPoint._Zone);
                    lineText += String.Format("{0,-33};;", this._LengthStation._Parameters._StationPoint._ClassAndNumero + ".");
                }
                lineText += String.Format("{0, 8};;", Math.Round(this._LengthStation._Parameters.HInstrument, 5).ToString("F5", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                if (this._LengthStation.WeatherConditions.WetTemperature !=TSU.Tsunami2.Preferences.Values.na)
                    lineText += String.Format("{0, 5};", Math.Round(this._LengthStation.WeatherConditions.WetTemperature, 1).ToString("F1", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                else
                    lineText += String.Format("{0, 5};", "");
                lineText += String.Format("{0, 5};", Math.Round(this._LengthStation.WeatherConditions.dryTemperature, 1).ToString("F1", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                lineText += String.Format("{0, 5};", Math.Round(this._LengthStation.WeatherConditions.pressure, 1).ToString("F1", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                lineText += String.Format("{0, 5};00;;HI_FIX;", this._LengthStation.WeatherConditions.humidity.ToString("F1", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                lineText += additionalText;
                textToWriteInFile.Add(lineText);
            }

            /// <summary>
            /// Ajoute l'�cart en longueur
            /// </summary>
            /// <param name="lineNumber"></param>
            /// <param name="textAllEcart"></param>
            /// <param name="meas"></param>
            private void AddEcarts(ref int lineNumber, List<string> textAllEcart, Polar.Measure meas)
            {
                double na = TSU.Preferences.Preferences.NotAvailableValueNa;
                string lineText;
                //Ecarts
                if (meas.Distance.distAvgMove.Value != na)
                {
                    ///Met un % devant les mesures bad
                    if (meas._Status is M.States.Bad)
                    {
                        lineText = "% ";
                    }
                    else
                    {
                        lineText = "  ";
                    }
                    lineText = String.Format("{0}EC;", WriteLineNumber(lineNumber, lineText));
                    lineNumber++;
                    //cas d'un al�sage
                    if (meas._Point._Point != "")
                    {
                        lineText += String.Format("{0,-10};", meas._Point._Accelerator);
                        lineText += String.Format("{0,-33};", meas._Point._ClassAndNumero + "." + meas._Point._Point);
                    }
                    //cas d'un pilier
                    else
                    {
                        lineText += String.Format("{0,-10};", meas._Point._Zone);
                        lineText += String.Format("{0,-33};", meas._Point._ClassAndNumero + ".");
                    }
                    lineText += String.Format("          ;{0,10};          ;          ;dr dl dh dRoll [mm]|[mrad];", Math.Round(-meas.Distance.distAvgMove.Value * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                    textAllEcart.Add(lineText);
                }
            }

            /// <summary>
            /// Ajoute la distance au fichier dat geode.
            /// </summary>
            /// <param name="lineNumber"></param>
            /// <param name="lineText"></param>
            /// <param name="na"></param>
            /// <param name="textAllDist"></param>
            /// <param name="meas"></param>
            private void AddDDMeas(ref int lineNumber, List<string> textAllDist, Polar.Measure meas)
            {
                double na = TSU.Preferences.Preferences.NotAvailableValueNa;
                string lineText;
                ///Distance
                if (meas.Distance.Raw.Value != na && meas._PointName != this._LengthStation._Parameters._Station._Name)
                {
                    ///Met un % devant les mesures bad
                    if (meas._Status is M.States.Bad)
                    {
                        lineText = "% ";
                    }
                    else
                    {
                        lineText = "  ";
                    }
                    //Ecriture de toutes les lectures
                    lineText = String.Format("{0}DD;", WriteLineNumber(lineNumber, lineText));
                    lineNumber++;
                    lineText += String.Format("{0,-9};", meas.Distance.Reflector._Model);
                    lineText += String.Format("{0,-12};", meas.Distance.Reflector._SerialNumber);
                    if (meas._Point._Point != "")
                    //cas d'un al�sage
                    {
                        lineText += String.Format("{0,-10};", meas._Point._Accelerator);
                        lineText += String.Format("{0,-33};", meas._Point._ClassAndNumero + "." + meas._Point._Point);
                    }
                    else
                    //cas d'un pilier
                    {
                        lineText += String.Format("{0,-10};", meas._Point._Zone);
                        lineText += String.Format("{0,-33};", meas._Point._ClassAndNumero + ".");
                    }
                    //lecture raw
                    lineText += String.Format("{0,10};", Math.Round(meas.Distance.Raw.Value, 6).ToString("F6", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                    //rallonge
                    lineText += String.Format("{0,8};", Math.Round(meas.Extension.Value, 6).ToString("F6", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                    //decalage de 0
                    lineText += String.Format("{0,8};;;;;;;;;", "0.000000");
                    // R�le
                    if (meas._Point.LGCFixOption == LgcPointOption.CALA) lineText += "R;";
                    else lineText += "A;";
                    //Operation terrain 01
                    lineText += String.Format("{0,2};", "01");
                    //date
                    lineText += String.Format("{0,-8};", meas._Date.ToString("T", System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR")));
                    //commentaire
                    lineText += String.Format("{0,-1};", meas.Comment);
                    // interfaces
                    var interfaces = meas.Interfaces;
                    lineText += interfaces.ToString(1, ';');
                    lineText += interfaces.ToString(2, ';');
                    lineText += interfaces.ToString(3, ';');
                    textAllDist.Add(lineText);
                }
            }
            /// <summary>
            /// ajoute l'angle vertical au fichier dat geode
            /// </summary>
            /// <param name="lineNumber"></param>
            /// <param name="textAllAV"></param>
            /// <param name="meas"></param>
            private void AddAVMeas(ref int lineNumber, List<string> textAllAV, Polar.Measure meas)
            {
                double na = TSU.Preferences.Preferences.NotAvailableValueNa;
                string lineText;
                ///Angle AV
                if (meas.Angles.Raw.Vertical.Value != na && meas._PointName != this._LengthStation._Parameters._Station._Name)
                {
                    ///Met un % devant les mesures bad
                    if (meas._Status is M.States.Bad)
                    {
                        lineText = "% ";
                    }
                    else
                    {
                        lineText = "  ";
                    }
                    //Ecriture de toutes les lectures
                    lineText = String.Format("{0}AV;", WriteLineNumber(lineNumber, lineText));
                    lineNumber++;
                    lineText += String.Format("{0,-9};", meas.Distance.Reflector._Model);
                    lineText += String.Format("{0,-12};", meas.Distance.Reflector._SerialNumber);
                    if (meas._Point._Point != "")
                    //cas d'un al�sage
                    {
                        lineText += String.Format("{0,-10};", meas._Point._Accelerator);
                        lineText += String.Format("{0,-33};", meas._Point._ClassAndNumero + "." + meas._Point._Point);
                    }
                    else
                    //cas d'un pilier
                    {
                        lineText += String.Format("{0,-10};", meas._Point._Zone);
                        lineText += String.Format("{0,-33};", meas._Point._ClassAndNumero + ".");
                    }
                    //lecture raw
                    lineText += String.Format("{0,10};", Math.Round(meas.Angles.Raw.Vertical.Value, 6).ToString("F6", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                    //rallonge
                    lineText += String.Format("{0,8};", Math.Round(meas.Extension.Value, 6).ToString("F6", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                    //decalage de 0
                    lineText += String.Format("{0,8};;;;;;;;;", "0.000000");
                    // R�le
                    if (meas._Point.LGCFixOption == LgcPointOption.CALA) lineText += "R;";
                    else lineText += "A;";
                    //Operation terrain 01
                    lineText += String.Format("{0,2};", "01");
                    //date
                    lineText += String.Format("{0,-8};", meas._Date.ToString("T", System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR")));
                    //commentaire
                    lineText += String.Format("{0,-1};", meas.Comment);
                    // interfaces
                    var interfaces = meas.Interfaces;
                    lineText += interfaces.ToString(1, ';');
                    lineText += interfaces.ToString(2, ';');
                    lineText += interfaces.ToString(3, ';');
                    textAllAV.Add(lineText);
                }
            }
            /// <summary>
            /// Ajoute l'angle horizontal au fichier geode 
            /// </summary>
            /// <param name="lineNumber"></param>
            /// <param name="textAllAH"></param>
            /// <param name="meas"></param>
            private void AddAHMeas(ref int lineNumber, List<string> textAllAH, Polar.Measure meas)
            {
                double na = TSU.Preferences.Preferences.NotAvailableValueNa;
                string lineText;
                ///Angle AH
                if (meas.Angles.Raw.Horizontal.Value != na && meas._PointName != this._LengthStation._Parameters._Station._Name)
                {
                    ///Met un % devant les mesures bad
                    if (meas._Status is M.States.Bad)
                    {
                        lineText = "% ";
                    }
                    else
                    {
                        lineText = "  ";
                    }
                    //Ecriture de toutes les lectures
                    lineText = String.Format("{0}AH;", WriteLineNumber(lineNumber, lineText));
                    lineNumber++;
                    lineText += String.Format("{0,-9};", meas.Distance.Reflector._Model);
                    lineText += String.Format("{0,-12};", meas.Distance.Reflector._SerialNumber);
                    if (meas._Point._Point != "")
                    //cas d'un al�sage
                    {
                        lineText += String.Format("{0,-10};", meas._Point._Accelerator);
                        lineText += String.Format("{0,-33};", meas._Point._ClassAndNumero + "." + meas._Point._Point);
                    }
                    else
                    //cas d'un pilier
                    {
                        lineText += String.Format("{0,-10};", meas._Point._Zone);
                        lineText += String.Format("{0,-33};", meas._Point._ClassAndNumero + ".");
                    }
                    //lecture raw
                    lineText += String.Format("{0,10};", Math.Round(meas.Angles.Raw.Horizontal.Value, 6).ToString("F6", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                    //rallonge
                    lineText += String.Format("{0,8};", Math.Round(meas.Extension.Value, 6).ToString("F6", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                    //decalage de 0
                    lineText += String.Format("{0,8};;;;;;;;;", "0.000000");
                    // R�le
                    if (meas._Point.LGCFixOption == LgcPointOption.CALA) lineText += "R;";
                    else lineText += "A;";
                    //Operation terrain 01
                    lineText += String.Format("{0,2};", "01");
                    //date
                    lineText += String.Format("{0,-8};", meas._Date.ToString("T", System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR")));
                    //commentaire
                    lineText += String.Format("{0,-1};", meas.Comment);
                    // interfaces
                    var interfaces = meas.Interfaces;
                    lineText += interfaces.ToString(1, ';');
                    lineText += interfaces.ToString(2, ';');
                    lineText += interfaces.ToString(3, ';');
                    textAllAH.Add(lineText);
                }
            }

            /// <summary>
            /// Ajoute la ligne RE en ent�te fichier
            /// </summary>
            /// <param name="stationLevelModule"></param>
            /// <param name="textToWriteInFile"></param>
            /// <param name="lineNumber"></param>
            /// <param name="lineText"></param>
            private void AddLineRE(List<string> textToWriteInFile, ref int lineNumber, ref string lineText)
            {
                //Ligne ent�te date + team + Op�ration seulement si fichier existe pas
                lineText = String.Format("  {0}RE;", WriteLineNumber(lineNumber, lineText));
                lineNumber++;
                lineText += WriteDate(this._LengthStation._Parameters._Date);
                lineText += String.Format("{0,-10};", this._LengthStation._Parameters._Team);
                lineText += String.Format("{0,6};", this._LengthStation._Parameters._Operation.value.ToString());
                lineText += $"Tsunami {Tsunami2.Properties.Version};";
                textToWriteInFile.Add(lineText);
            }

            private string WriteDate(DateTime date)
            //Ecrit la date dans l'ent�te pour le fichier txt pour Geode
            {
                //jour
                string dateText = date.Day.ToString() + "-";
                switch (date.Month)
                {
                    //Choix du mois
                    case 1:
                        dateText = dateText + "Jan-";
                        break;
                    case 2:
                        dateText = dateText + "Feb-";
                        break;
                    case 3:
                        dateText = dateText + "Mar-";
                        break;
                    case 4:
                        dateText = dateText + "Apr-";
                        break;
                    case 5:
                        dateText = dateText + "May-";
                        break;
                    case 6:
                        dateText = dateText + "Jun-";
                        break;
                    case 7:
                        dateText = dateText + "Jul-";
                        break;
                    case 8:
                        dateText = dateText + "Aug-";
                        break;
                    case 9:
                        dateText = dateText + "Sep-";
                        break;
                    case 10:
                        dateText = dateText + "Oct-";
                        break;
                    case 11:
                        dateText = dateText + "Nov-";
                        break;
                    case 12:
                        dateText = dateText + "Dec-";
                        break;
                    default:
                        dateText = dateText + "Jan-";
                        break;
                }
                //ann�e
                return dateText += date.Year + ";";
            }
            private string WriteLineNumber(int lineNumber, string lineText)
            //�crit le num�ro de ligne dans le fichier txt pour Geode
            {
                for (int i = 0; i < 3 - lineNumber.ToString().Length; i++)
                {
                    lineText = lineText + "0";
                }
                return lineText + lineNumber.ToString() + ";";
            }
            #endregion

        }
    }
}
