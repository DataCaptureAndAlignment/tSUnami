﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Globalization;
using TSU.ENUM;
using TSU.Views;
using TSU.Common.Instruments;
using M = TSU.Common.Measures;
using TSU.Common.Instruments.Device;
using TSU.Tools;
using TSU.Views.Message;
using D = TSU.Common.Instruments.Device;
using T = TSU.Tools;
using I = TSU.Common.Instruments;
using O = TSU.Common.Operations;

using E = TSU.Common.Elements;
using R = TSU.Properties.Resources;


namespace TSU.Length
{
    public partial class Station
    {
        public partial class View : Common.Station.View
        {
            #region Fields & props

            internal Length.Station.Module _stationLengthModule;
            //Ligne et colonne active dans le datagridview
            internal int rowActiveCell = 0;
            internal int columnActiveCell = 5;
            internal int nombreReprise = 0;
            internal int _numeroReprise = 0;
            //Main Button
            internal BigButton bigbuttonTop;
            internal BigButton bigButton_Elements;
            internal BigButton bigButton_save;
            internal BigButton bigButton_Cancel;
            internal BigButton bigButton_Sort;
            internal BigButton bigButton_ShowDatFile;
            internal BigButton bigButton_Closing;
            internal new ContextMenuStrip contextMenuStrip;
            internal TSU.ENUM.ModuleType moduleType = ModuleType.Advanced;
            internal TsuNode node_Parameters;
            internal TsuNode node_Admin;
            internal TsuNode node_DefaultMeas;
            private bool checkCurrentCell = false;
            private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_Top;
            #endregion

            #region Construction & init

            public View(Module parentModule)
                : base(parentModule, System.Windows.Forms.Orientation.Vertical)
            //affichage du form lors de son lancement
            {
                InitializeComponent();
                this.ApplyThemeColors();
                this._stationLengthModule = (Module)parentModule;
                InitializeMenu();
                AddWaitingView();
                this.ShowDockedFill();
            }

            private void ApplyThemeColors()
            {
                this._PanelBottom.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                this._PanelTop.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                this.splitContainer1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                this.splitContainer1.Panel1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                this.splitContainer1.Panel2.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                this.splitContainerLength.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                this.splitContainerLength.Panel1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                this.splitContainerLength.Panel2.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                this.treeView_Parameters.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                this.dataGridViewLength.ColumnHeadersDefaultCellStyle.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
                this.dataGridViewLength.ColumnHeadersDefaultCellStyle.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
                this.dataGridViewLength.ColumnHeadersDefaultCellStyle.SelectionBackColor = TSU.Tsunami2.Preferences.Theme.Colors.TextHighLightBack;
                this.dataGridViewLength.ColumnHeadersDefaultCellStyle.SelectionForeColor = TSU.Tsunami2.Preferences.Theme.Colors.TextHighLightFore;
                this.dataGridViewLength.GridColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                this.dataGridViewLength.DefaultCellStyle.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
                this.dataGridViewLength.DefaultCellStyle.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
                this.dataGridViewLength.DefaultCellStyle.SelectionBackColor = TSU.Tsunami2.Preferences.Theme.Colors.TextHighLightBack;
                this.dataGridViewLength.DefaultCellStyle.SelectionForeColor = TSU.Tsunami2.Preferences.Theme.Colors.TextHighLightFore;
                this.treeView_Parameters.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
                this.treeView_Parameters.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
                this.LengthDataGridviewStation.HeaderText = R.StringDataGridLength_Station;
                this.LeveldataGridViewName.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Name;
                this.Use.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Use;
                this.LengthdataGridViewCode.HeaderText = R.StringDataGridLength_SocketCode;
                this.LengthdataGridViewDepla.HeaderText = R.StringDataGridLength_AvgMove;
                this.LengthdataGridViewDiffDist.HeaderText = R.StringDataGridLength_Diff;
                this.LengthDataGridViewDmes.HeaderText = R.StringDataGridLength_Dmes;
                this.LengthDataGridViewDoubleFace.HeaderText = R.StringDataGridLength_Face;
                this.LengthdataGridViewDtheo.HeaderText = R.StringDataGridLength_DTheo;
                this.LengthDataGridViewGotoMeas.HeaderText = R.StringDataGridLength_Measure;
                this.LengthDataGridViewNbreMeas.HeaderText = R.StringDataGridLength_NbreMeas;
                this.LengthdataGridViewRall.HeaderText = R.StringDataGridLength_Extension;
                this.LengthdataGridViewReflector.HeaderText = R.StringDataGridLength_Reflector;
                this.LengthDataGridviewStation.HeaderText = R.StringDataGridLength_Station;
                this.LeveldataGridViewComment.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Comment;
                this.LeveldataGridViewTime.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Time;
                this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            }
            //Methods
            //Initialization du treeView parametre fil
            private void InitializeMenu()
            {
                //Main Button
                bigbuttonTop = new BigButton(R.StringLength_BigButtonTop, R.LengthSetting, this.ShowContextMenuGlobal);
                this._PanelTop.Controls.Add(bigbuttonTop);
                this.CreateContextMenuButtons();
                if (this._stationLengthModule._LengthStation != null)
                    this._stationLengthModule._LengthStation._Parameters._State = new Common.Station.State.InstrumentSelection();
                this.InitializeTreeviewParameters();
                this.InitializedataGridViewLength();
            }
            /// <summary>
            /// Creation du menu context button
            /// </summary>
            private void CreateContextMenuButtons()
            //creation du menu de boutons
            {
                bigButton_Elements = new BigButton(R.StringLength_SelectElement, R.Open, buttonSelectPoint_Click);
                bigButton_save = new BigButton(R.StringLength_Save, R.Save, buttonSave_Click);
                bigButton_Cancel = new BigButton(R.StringLevel_CancelMenu, R.Cancel, buttonCancel_Click);
                bigButton_Sort = new BigButton(R.StringLevel_Sort, R.Vertical, buttonSort_Click);
                bigButton_ShowDatFile = new BigButton(R.StringLength_OpenDatFile, R.Export, bigButton_ShowDatFile_Click);
                bigButton_Closing = new BigButton(R.StringLength_OpeningCheck, R.StatusQuestionnable, bigButton_Closing_Click);
            }



            private void ShowContextMenuGlobal()
            {
                this.CheckDatagridIsInEditMode();
                switch (this.moduleType)
                {
                    case ModuleType.Guided:
                        break;
                    case ModuleType.Advanced:
                        // creation de la liste des boutons de menu pour le module avancé
                        contextButtons = new List<Control>();
                        if (this._stationLengthModule._LengthStation._Parameters._Instrument._Model != null)
                        {
                            contextButtons.Add(bigButton_Elements);
                        }
                        if (this._stationLengthModule._LengthStation._Parameters._State is Common.Station.State.StationLengthReadyToBeSaved)
                        {
                            contextButtons.Add(bigButton_save);
                        }
                        string filepath = this._stationLengthModule._LengthStation.ParametersBasic._GeodeDatFilePath;
                        if (filepath != "") contextButtons.Add(bigButton_ShowDatFile);
                        if (this.dataGridViewLength.RowCount != 0)
                        {
                            contextButtons.Add(bigButton_Sort);
                        }
                        if (this._stationLengthModule._LengthStation._OpeningMeas != null)
                        {
                            contextButtons.Add(bigButton_Closing);
                        }
                        this.contextButtons.Add(bigButton_Cancel);
                        bool ok = this.ShowPopUpMenu(contextButtons);
                        // il peut arriver lors de l'ajout d'un nouveau bouton que l'affichage des contrôles dans le popup menu ne soit pas dans l'ordre. Le réaffichage du pop up semble résoudre le problème.
                        //if (!ok) { this.ShowContextMenuGlobal(); }
                        break;
                    default:
                        break;
                }
            }
            private void AddWaitingView()
            // Add a waiting instrument module
            {
                TsuView v = new TsuView();
                v.BackgroundImage = R.Instrument;
                v.BackgroundImageLayout = ImageLayout.Zoom;
                v.ShowDockedFill();
                splitContainer1.Panel2.Controls.Add(v);
            }
            #endregion
            #region Updates
            public override void UpdateView()
            {
                base.UpdateView();
                //this.UpdateInstrumentView();
                this.UpdateListMeasure();
            }
            internal void UpdateListMeasure(bool cellFocus = true)
            //met à jour le datagrid en fonction de la working station level
            {
                //RefreshGlobalContextMenuItemCollection();
                this._stationLengthModule.CheckIfReadyToBeSaved();
                this.UpdateBigButtonImage();
                this.RedrawTreeviewParameters();
                this.UpdateDatagridViewLength();
                if (cellFocus) { this.CellFocusDataGridView(); }
            }
            /// <summary>
            /// Creation des boutons du TableLayoutTOp
            /// </summary>
            private void CreateTableLayoutTopButtonS()
            {
                this.tableLayoutPanel_Top = new System.Windows.Forms.TableLayoutPanel();
                this.tableLayoutPanel_Top.ColumnCount = 2;
                this.tableLayoutPanel_Top.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
                this.tableLayoutPanel_Top.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
                this.tableLayoutPanel_Top.Dock = System.Windows.Forms.DockStyle.Fill;
                this.tableLayoutPanel_Top.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
                this.tableLayoutPanel_Top.Location = new System.Drawing.Point(0, 0);
                this.tableLayoutPanel_Top.Name = "tableLayoutPanel_Top";
                this.tableLayoutPanel_Top.RowCount = 1;
                this.tableLayoutPanel_Top.RowStyles.Add(new System.Windows.Forms.RowStyle());
                this.tableLayoutPanel_Top.Size = new System.Drawing.Size(1202, 75);
                this.tableLayoutPanel_Top.TabIndex = 0;
                this.tableLayoutPanel_Top.Controls.Clear();
            }
            internal void UpdateBigButtonImage()
            //met à jour l'image et texte du bigbuttonTop
            {
                string lastSaved = "";
                if (this._stationLengthModule._LengthStation.ParametersBasic.LastSaved != DateTime.MinValue)
                {
                    lastSaved = string.Format(R.StringLength_LastExported, this._stationLengthModule._LengthStation.ParametersBasic.LastSaved.ToString("HH:mm:ss", CultureInfo.InvariantCulture));
                }
                this._PanelTop.Controls.Clear();
                switch (this.moduleType)
                {
                    case ModuleType.Guided:
                        ///Met un table layout top avec sauvegarde et contrôle fermeture
                        this.CreateTableLayoutTopButtonS();
                        this.bigButton_save = new BigButton(String.Format(R.StringLength_ExportButtonSaving,
                            this._stationLengthModule._LengthStation._Parameters._Instrument._Name,
                            lastSaved),
                            R.Save,
                            this.buttonSave_Click);
                        this.bigButton_save.SetAttributes(String.Format(R.StringLength_ExportButtonSaving,
                        this._stationLengthModule._LengthStation._Parameters._Instrument._Name,
                        lastSaved),
                           R.Save);
                        this.bigButton_save.Available = (this._stationLengthModule._LengthStation._Parameters._State is Common.Station.State.StationLengthReadyToBeSaved) ? true : false;

                        bigButton_save.Margin = new Padding(0);
                        this.tableLayoutPanel_Top.Controls.Add(bigButton_save, 0, 0);

                        bigButton_Closing = new BigButton(R.StringLength_OpeningCheck, R.StatusQuestionnable, bigButton_Closing_Click);
                        bigButton_Closing.Available = (this._stationLengthModule._LengthStation._OpeningMeas != null) ? true : false;
                        bigButton_Closing.Margin = new Padding(0);
                        this.tableLayoutPanel_Top.Controls.Add(this.bigButton_Closing, 1, 0);

                        this._PanelTop.Controls.Add(this.tableLayoutPanel_Top);
                        break;
                    case ModuleType.Advanced:
                        bigbuttonTop = new BigButton(R.StringLength_BigButtonTop, R.LengthSetting, this.ShowContextMenuGlobal);
                        this._PanelTop.Controls.Add(this.bigbuttonTop);
                        break;
                    default:
                        break;
                }
            }
            /// <summary>
            /// make the datagridview in full window
            /// </summary>
            internal void MaximizeDataGridView()
            {
                // Hide the treeview
                this.splitContainerLength.Panel1MinSize = 0;
                this.splitContainerLength.SplitterDistance = 0;
            }
            #endregion
            #region DatagridView
            public void InitializedataGridViewLength()
            //Initialization du dataGridView
            {
                dataGridViewLength.ColumnHeadersHeight = 400;
                dataGridViewLength.DefaultCellStyle.Font = TSU.Tsunami2.Preferences.Theme.Fonts.NormalItalic;
                dataGridViewLength.RowHeadersDefaultCellStyle.Font = TSU.Tsunami2.Preferences.Theme.Fonts.NormalItalic;
                dataGridViewLength.ColumnHeadersDefaultCellStyle.Font = TSU.Tsunami2.Preferences.Theme.Fonts.NormalItalic;
                dataGridViewLength.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
            internal void UpdateDatagridViewLength()
            //met à jour le datagridview
            {
                this.dataGridViewLength.CurrentCellChanged -= this.dataGridViewLength_CurrentCellChanged;
                this.dataGridViewLength.CellValueChanged -= this.dataGridViewLength_CellValueChanged;
                Length.Station st = this._stationLengthModule._LengthStation;
                //this.MaximizeDataGridView();
                dataGridViewLength.Rows.Clear();
                List<List<object>> listRow = new List<List<object>>();
                this.AddMeasuresToDatagrid(listRow);
                ///N'ajoute les ligne de fermeture qu'en module avancé
                if (st._OpeningMeas != null)
                {
                    this.AddBlankLineToDatagrid(listRow);
                    this.AddTextLineClosingToDatagrid(listRow);
                    this.AddOpeningLineToDatagrid(listRow);
                    if (st._ClosingMeas != null)
                    {
                        this.AddClosingLineToDatagrid(listRow);
                    }
                    else
                    {
                        this.AddBlankLineToDatagrid(listRow);
                    }
                }
                foreach (List<Object> ligne in listRow)
                {
                    dataGridViewLength.Rows.Add(ligne.ToArray());
                }

                for (int i = 0; i < dataGridViewLength.RowCount; i++)
                {
                    if (i < st._MeasureOfTheodolite.Count)
                    {
                        dataGridViewLength[12, i].ReadOnly = false;
                        //Met en jaune la colonne raw meas et rallonge
                        if (dataGridViewLength[2, i].Value.ToString() != "")
                        {
                            dataGridViewLength[6, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Action;
                            dataGridViewLength[12, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Action;
                            dataGridViewLength[15, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Action;
                        }
                        //met en gris les lignes si c'est un point de calage
                        if ((dataGridViewLength[3, i].Value as System.Drawing.Bitmap).Width == 128)
                        {
                            dataGridViewLength[0, i].Value = R.Length_Delete_Grey;
                            if ((dataGridViewLength[1, i].Value as System.Drawing.Bitmap).Width != 128)
                            {
                                dataGridViewLength[1, i].Value = R.Length_Case_Vide_light_Grey;
                            }
                            dataGridViewLength[2, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewLength[3, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewLength[4, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewLength[5, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewLength[7, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewLength[8, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewLength[9, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewLength[10, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewLength[11, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewLength[14, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewLength[16, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
                        }
                        //met en cyan les lignes si c'est un point stationné
                        if ((dataGridViewLength[1, i].Value as System.Drawing.Bitmap).Width == 128)
                        {
                            dataGridViewLength[0, i].Value = R.Length_Case_Vide_light_Blue;
                            dataGridViewLength[1, i].Value = R.LengthStation;
                            dataGridViewLength[2, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
                            ///Si case vide, signifie que la station n'est pas en lock
                            if ((dataGridViewLength[3, i].Value as System.Drawing.Bitmap).Width == 65)
                            {
                                dataGridViewLength[3, i].Value = R.Length_Case_Vide_light_Blue;
                            }
                            //dataGridViewLevel[4, i].Value = R.Length_Case_Vide_light_Blue;
                            dataGridViewLength[5, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
                            dataGridViewLength[6, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Attention;
                            dataGridViewLength[7, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
                            dataGridViewLength[8, i].Value = R.Length_Case_Vide_light_Blue;
                            dataGridViewLength[9, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
                            dataGridViewLength[9, i].Value = "";
                            dataGridViewLength[10, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
                            dataGridViewLength[10, i].Value = "";
                            dataGridViewLength[11, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
                            dataGridViewLength[12, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
                            dataGridViewLength[12, i].Value = "";
                            dataGridViewLength[12, i].ReadOnly = true;
                            dataGridViewLength[13, i].Value = R.Length_Case_Vide_light_Blue;
                            dataGridViewLength[14, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
                            dataGridViewLength[14, i].Value = "";
                            dataGridViewLength[15, i].Value = "";
                            dataGridViewLength[15, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
                            dataGridViewLength[15, i].ReadOnly = true;
                            dataGridViewLength[16, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
                            dataGridViewLength[16, i].Value = "";
                        }
                        //met en rouge le fond de la cellule déplacement si le déplacement moyen est > à la tolérance d'alignement ou le etxte en vert si deplacement < tolerance
                        double p = T.Conversions.Numbers.ToDouble(dataGridViewLength[11, i].Value.ToString(), true, -9999);
                        ///Temporairement n'affiche plus les couleurs en module guidé tant que les tolérances ne sont pas décidées.
                        if (p != -9999 && this.moduleType == ModuleType.Advanced)
                        {
                            dataGridViewLength[11, i].Style.BackColor = (Math.Abs(p) > st._Parameters._Tolerance * 1000) ? TSU.Tsunami2.Preferences.Theme.Colors.Bad : dataGridViewLength[10, i].Style.BackColor;
                            dataGridViewLength[11, i].Style.ForeColor = (Math.Abs(p) > st._Parameters._Tolerance * 1000) ? dataGridViewLength[10, i].Style.ForeColor : TSU.Tsunami2.Preferences.Theme.Colors.Good;
                        }
                        //met en bleu le point focus sauf si c'est le point stationné
                        if (this._stationLengthModule.actualPointFocussed != "")
                        {
                            int f = this._stationLengthModule._LengthStation._MeasureOfTheodolite.FindIndex(x => x._PointName == this._stationLengthModule.actualPointFocussed);
                            if (f != -1)
                            {
                                dataGridViewLength[2, f].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.TextHighLightBack;
                                dataGridViewLength[2, f].Style.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.TextHighLightFore;
                            }
                        }
                    }
                    else
                    {
                        ///Ligne de fermetures en module avancé
                        dataGridViewLength.Rows[i].ReadOnly = true;
                        ///Ligne titre closing
                        if (i == dataGridViewLength.RowCount - 3)
                        {
                            dataGridViewLength[0, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                            dataGridViewLength[1, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                            dataGridViewLength[2, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                            dataGridViewLength[3, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                            dataGridViewLength[4, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                            dataGridViewLength[5, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                            dataGridViewLength[6, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                            dataGridViewLength[7, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                            dataGridViewLength[8, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                            dataGridViewLength[9, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                            dataGridViewLength[10, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                            dataGridViewLength[11, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                            dataGridViewLength[12, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                            dataGridViewLength[13, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                            dataGridViewLength[14, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                            dataGridViewLength[15, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                            dataGridViewLength[16, i].Style.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                        }
                        ///Dernière ligne du datagrid avec la valeur de la fermeture
                        if (i == dataGridViewLength.RowCount - 1)
                        {
                            double p = T.Conversions.Numbers.ToDouble(dataGridViewLength[10, i].Value.ToString(), true, -9999);
                            ///La
                            if (p != -9999)
                            {
                                dataGridViewLength[10, i].Style.BackColor = (Math.Abs(p) > st._Parameters.Tolerance.Opposite_Face.D_mm) ? TSU.Tsunami2.Preferences.Theme.Colors.Bad : dataGridViewLength[10, i].Style.BackColor;
                                dataGridViewLength[10, i].Style.ForeColor = (Math.Abs(p) > st._Parameters.Tolerance.Opposite_Face.D_mm) ? dataGridViewLength[10, i].Style.ForeColor : TSU.Tsunami2.Preferences.Theme.Colors.Good;
                            }
                        }
                    }

                }
                this.dataGridViewLength.CurrentCellChanged += new System.EventHandler(this.dataGridViewLength_CurrentCellChanged);
                this.dataGridViewLength.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewLength_CellValueChanged);
            }
            /// <summary>
            /// Ajoute toutes les mesures au datagrid
            /// </summary>
            /// <param name="listRow"></param>
            private void AddMeasuresToDatagrid(List<List<object>> listRow)
            {
                Length.Station st = this._stationLengthModule._LengthStation;
                double na = TSU.Preferences.Preferences.NotAvailableValueNa;
                //ajoute toutes les mesures dans le tableau ligne par ligne
                foreach (Polar.Measure measureOfTheodolite in this._stationLengthModule._LengthStation._MeasureOfTheodolite)
                {
                    List<object> row = new List<object>();
                    //colonne suppression mesure 0
                    row.Add(R.Length_Delete_White);
                    //colonne pt stationné 1
                    row.Add(R.Length_Case_Vide_White);
                    //colonne nom 2
                    row.Add("");
                    //colonne cala 3
                    row.Add(R.Length_Case_Vide_White);
                    //colonne checkbox use meas 4
                    row.Add(R.CheckedBoxDatagridView);
                    //Code Alesage 5
                    row.Add("");
                    //Rallonge 6
                    row.Add(0);
                    //colonne Dist theo 7
                    row.Add(0);
                    //colonne GotoAll 8
                    row.Add(R.Length_GotoAll);
                    //colonne Dist mesurée corrigée 9
                    row.Add(0);
                    //colonne Différence distance mesurée - distance theo 10
                    row.Add(0);
                    //colonne déplacement 11
                    row.Add(0);
                    //colonne nbre mesure 12
                    row.Add(0);
                    //colonne simple/double face 13
                    row.Add(R.Length_Case_Vide_White);
                    //Reflecteur 14
                    row.Add("");
                    //Commentaire 15
                    row.Add("");
                    //Time 16
                    row.Add("");
                    //point stationné
                    if (st._Parameters._StationPoint._Name == measureOfTheodolite._PointName)
                    {
                        row[1] = R.LengthStation;
                    }
                    //Nom du point
                    row[2] = measureOfTheodolite._PointName;
                    //Point en cala
                    if (measureOfTheodolite._Point.LGCFixOption == LgcPointOption.CALA)
                    {
                        row[3] = R.Length_Lock;
                    }
                    //Utilisation mesure
                    if (measureOfTheodolite._Status is M.States.Bad)
                    {
                        row[4] = R.UncheckedBoxDatagridView;
                    }
                    //code alésage
                    row[5] = measureOfTheodolite._Point.SocketCode.ToString();
                    //Rallonge/h instrument
                    if (st._Parameters._StationPoint._Name == measureOfTheodolite._PointName)
                    {
                        row[6] = Math.Round(st._Parameters.HInstrument * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                    }
                    else
                    {
                        row[6] = Math.Round(measureOfTheodolite.Extension.Value * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                    }
                    //Distance theo
                    row[7] = (measureOfTheodolite.Distance.distTheoHoriz.Value == na) ? "" : Math.Round(measureOfTheodolite.Distance.distTheoHoriz.Value, 5).ToString("F5", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                    //Distance corrigée horizontale
                    row[9] = (measureOfTheodolite.Distance.distMesHoriz.Value == na) ? "" : Math.Round(measureOfTheodolite.Distance.distMesHoriz.Value, 5).ToString("F5", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                    //diff dist mesurée - horiz
                    row[10] = (measureOfTheodolite.Distance.distMesHoriz.Value == na || measureOfTheodolite.Distance.distTheoHoriz.Value == na) ?
                        "" : Math.Round((measureOfTheodolite.Distance.distMesHoriz.Value - measureOfTheodolite.Distance.distTheoHoriz.Value) * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                    //Déplacement moyen
                    row[11] = (measureOfTheodolite.Distance.distAvgMove.Value == na) ? "" : Math.Round(measureOfTheodolite.Distance.distAvgMove.Value * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                    //Nbre mesure
                    row[12] = measureOfTheodolite.NumberOfMeasureToAverage.ToString();
                    //Simple/double face
                    if (measureOfTheodolite.Face == FaceType.DoubleFace && measureOfTheodolite._Point.LGCFixOption == LgcPointOption.POIN)
                    {
                        row[13] = R.Length2Face_White;
                    }
                    if (measureOfTheodolite.Face == FaceType.DoubleFace && measureOfTheodolite._Point.LGCFixOption == LgcPointOption.CALA)
                    {
                        row[13] = R.Length2Face_Grey;
                    }
                    if (measureOfTheodolite.Face == FaceType.Face1 && measureOfTheodolite._Point.LGCFixOption == LgcPointOption.CALA)
                    {
                        row[13] = R.LengthOneFace_Grey;
                    }
                    if (measureOfTheodolite.Face == FaceType.Face1 && measureOfTheodolite._Point.LGCFixOption == LgcPointOption.POIN)
                    {
                        row[13] = R.LengthOneFace_White;
                    }
                    //Reflecteur
                    row[14] = measureOfTheodolite.Reflector._Name;
                    //Commentaire
                    row[15] = measureOfTheodolite.Comment;
                    //TIme
                    if (measureOfTheodolite._Date != DateTime.MinValue) row[16] = measureOfTheodolite._Date.ToString("T", System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR"));
                    listRow.Add(row);
                }
            }

            /// <summary>
            /// Ajoute une ligne blanche
            /// </summary>
            /// <param name="listRow"></param>
            private void AddBlankLineToDatagrid(List<List<object>> listRow)
            {
                List<object> row = new List<object>();
                //colonne suppression mesure 0
                row.Add(R.Length_Case_Vide_White);
                //colonne pt stationné 1
                row.Add(R.Length_Case_Vide_White);
                //colonne nom 2
                row.Add("");
                //colonne cala 3
                row.Add(R.Length_Case_Vide_White);
                //colonne checkbox use meas 4
                row.Add(R.Length_Case_Vide_White);
                //Code Alesage 5
                row.Add("");
                //Rallonge 6
                row.Add("");
                //colonne Dist theo 7
                row.Add("");
                //colonne GotoAll 8
                row.Add(R.Length_Case_Vide_White);
                //colonne Dist mesurée corrigée 9
                row.Add("");
                //colonne Différence distance mesurée - distance theo 10
                row.Add("");
                //colonne déplacement 11
                row.Add("");
                //colonne nbre mesure 12
                row.Add("");
                //colonne simple/double face 13
                row.Add(R.Length_Case_Vide_White);
                //Reflecteur 14
                row.Add("");
                //Commentaire 15
                row.Add("");
                //Time 16
                row.Add("");
                listRow.Add(row);
            }
            /// <summary>
            /// Ajoute une ligne fermeture au datagrid
            /// </summary>
            /// <param name="listRow"></param>
            private void AddClosingLineToDatagrid(List<List<object>> listRow)
            {
                Length.Station st = this._stationLengthModule._LengthStation;
                double na = TSU.Preferences.Preferences.NotAvailableValueNa;
                List<object> row = new List<object>();
                //colonne suppression mesure 0
                row.Add(R.Length_Case_Vide_White);
                //colonne pt stationné 1
                row.Add(R.Length_Case_Vide_White);
                //colonne nom 2
                row.Add(st._ClosingMeas._PointName);
                //colonne cala 3
                row.Add(R.Length_Case_Vide_White);
                //colonne checkbox use meas 4
                row.Add(R.Length_Case_Vide_White);
                //Code Alesage 5
                row.Add("");
                //Rallonge 6
                row.Add("");
                //colonne Dist theo 7
                row.Add("");
                //colonne GotoAll 8
                row.Add(R.Length_Case_Vide_White);
                //colonne Dist mesurée corrigée 9
                row.Add("");
                //colonne Différence distance mesurée - distance theo 10
                row.Add("");
                //colonne déplacement 11
                row.Add("");
                //colonne nbre mesure 12
                row.Add("");
                //colonne simple/double face 13
                row.Add(R.Length_Case_Vide_White);
                //Reflecteur 14
                row.Add("");
                //Commentaire 15
                row.Add("");
                //Time 16
                row.Add("");
                //Distance corrigée horizontale
                row[9] = (st._ClosingMeas.Distance.distMesHoriz.Value == na) ? "" : Math.Round(st._ClosingMeas.Distance.distMesHoriz.Value, 5).ToString("F5", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                //diff dist mesurée horiz ouverture -- fermeture
                row[10] = (st._ClosingMeas.Distance.distMesHoriz.Value == na) || (st._OpeningMeas.Distance.distMesHoriz.Value == na) ?
                    "" : Math.Round((st._ClosingMeas.Distance.distMesHoriz.Value - st._OpeningMeas.Distance.distMesHoriz.Value) * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                //Nbre mesure
                row[12] = (st._ClosingMeas.NumberOfMeasureToAverage.ToString());
                //Simple/double face
                if (st._ClosingMeas.Face == FaceType.DoubleFace)
                {
                    row[13] = R.Length2Face_White;
                }
                if (st._ClosingMeas.Face == FaceType.Face1)
                {
                    row[13] = R.LengthOneFace_White;
                }
                //Reflecteur
                row[14] = st._ClosingMeas.Reflector._Name;
                //Commentaire
                row[15] = st._ClosingMeas.Comment;
                //TIme
                if (st._ClosingMeas._Date != DateTime.MinValue) row[16] = st._ClosingMeas._Date.ToString("T", System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR"));
                listRow.Add(row);
            }
            /// <summary>
            /// Ajoute une ligne fermeture au datagrid
            /// </summary>
            /// <param name="listRow"></param>
            private void AddOpeningLineToDatagrid(List<List<object>> listRow)
            {
                Length.Station st = this._stationLengthModule._LengthStation;
                double na = TSU.Preferences.Preferences.NotAvailableValueNa;
                List<object> row = new List<object>();
                //colonne suppression mesure 0
                row.Add(R.Length_Case_Vide_White);
                //colonne pt stationné 1
                row.Add(R.Length_Case_Vide_White);
                //colonne nom 2
                row.Add(st._OpeningMeas._PointName);
                //colonne cala 3
                row.Add(R.Length_Case_Vide_White);
                //colonne checkbox use meas 4
                row.Add(R.Length_Case_Vide_White);
                //Code Alesage 5
                row.Add("");
                //Rallonge 6
                row.Add("");
                //colonne Dist theo 7
                row.Add("");
                //colonne GotoAll 8
                row.Add(R.Length_Case_Vide_White);
                //colonne Dist mesurée corrigée 9
                row.Add("");
                //colonne Différence distance mesurée - distance theo 10
                row.Add("");
                //colonne déplacement 11
                row.Add("");
                //colonne nbre mesure 12
                row.Add("");
                //colonne simple/double face 13
                row.Add(R.Length_Case_Vide_White);
                //Reflecteur 14
                row.Add("");
                //Commentaire 15
                row.Add("");
                //Time 16
                row.Add("");
                //Distance corrigée horizontale
                row[9] = (st._OpeningMeas.Distance.distMesHoriz.Value == na) ? "" : Math.Round(st._OpeningMeas.Distance.distMesHoriz.Value, 5).ToString("F5", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                //Nbre mesure
                row[12] = (st._OpeningMeas.NumberOfMeasureToAverage.ToString());
                //Simple/double face
                if (st._OpeningMeas.Face == FaceType.DoubleFace)
                {
                    row[13] = R.Length2Face_White;
                }
                if (st._OpeningMeas.Face == FaceType.Face1)
                {
                    row[13] = R.LengthOneFace_White;
                }
                //Reflecteur
                row[14] = st._OpeningMeas.Reflector._Name;
                //Commentaire
                row[15] = st._OpeningMeas.Comment;
                //TIme
                if (st._OpeningMeas._Date != DateTime.MinValue) row[16] = st._OpeningMeas._Date.ToString("T", System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR"));
                listRow.Add(row);
            }
            /// <summary>
            /// Ajoute une ligne blanche
            /// </summary>
            /// <param name="listRow"></param>
            private void AddTextLineClosingToDatagrid(List<List<object>> listRow)
            {
                List<object> row = new List<object>();
                //colonne suppression mesure 0
                row.Add(R.Length_Case_Vide_light_Grey);
                //colonne pt stationné 1
                row.Add(R.Length_Case_Vide_light_Grey);
                //colonne nom 2
                row.Add(R.StringDatagridLength_Closing);
                //colonne cala 3
                row.Add(R.Length_Case_Vide_light_Grey);
                //colonne checkbox use meas 4
                row.Add(R.Length_Case_Vide_light_Grey);
                //Code Alesage 5
                row.Add("");
                //Rallonge 6
                row.Add("");
                //colonne Dist theo 7
                row.Add("");
                //colonne GotoAll 8
                row.Add(R.Length_Case_Vide_light_Grey);
                //colonne Dist mesurée corrigée 9
                row.Add("");
                //colonne Différence distance mesurée - distance theo 10
                row.Add("");
                //colonne déplacement 11
                row.Add("");
                //colonne nbre mesure 12
                row.Add("");
                //colonne simple/double face 13
                row.Add(R.Length_Case_Vide_light_Grey);
                //Reflecteur 14
                row.Add("");
                //Commentaire 15
                row.Add("");
                //Time 16
                row.Add("");
                listRow.Add(row);
            }
            /// <summary>
            /// Check if datagrid is in edit mode and validate it before leaving
            /// </summary>
            internal void CheckDatagridIsInEditMode()
            {
                if (dataGridViewLength.IsCurrentCellInEditMode)
                {
                    //this.dataGridViewLevel.CellValueChanged -= this.dataGridViewLevel_CellValueChanged;
                    int saveRow = rowActiveCell;
                    int saveColumn = columnActiveCell;
                    //Try car de temps en temps endEdit peut faire une exception
                    try
                    {
                        dataGridViewLength.EndEdit();

                    }
                    catch (Exception ex)
                    {
                        ex.Data.Clear();
                    }
                    //this.dataGridViewLevel.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewLevel_CellValueChanged);
                    rowActiveCell = saveRow;
                    columnActiveCell = saveColumn;
                }
            }
            internal void CellFocusDataGridView()
            //remet le focus sur la cellule active avant refresh du tableau
            {
                this.dataGridViewLength.CurrentCellChanged -= this.dataGridViewLength_CurrentCellChanged;
                if (rowActiveCell >= dataGridViewLength.RowCount || rowActiveCell < 0 || rowActiveCell >= this._stationLengthModule._LengthStation._MeasureOfTheodolite.Count)
                {
                    rowActiveCell = 0;
                }
                if (columnActiveCell >= dataGridViewLength.ColumnCount || columnActiveCell < 0 || columnActiveCell == 16)
                {
                    columnActiveCell = 6;
                }
                if (dataGridViewLength.RowCount > 0)
                {
                    if (dataGridViewLength[columnActiveCell, rowActiveCell].Visible == false)
                    {
                        rowActiveCell = 0;
                        columnActiveCell = 6;
                    }
                    dataGridViewLength.Focus();
                    dataGridViewLength.ClearSelection();
                    dataGridViewLength.CurrentCell = dataGridViewLength[columnActiveCell, rowActiveCell];
                    dataGridViewLength.Rows[rowActiveCell].Cells[columnActiveCell].Selected = true;
                    //dataGridViewLevel.BeginEdit(true);
                    ///Change la mesure à faire dans l'instrument
                    if (dataGridViewLength.CurrentCell.RowIndex < this._stationLengthModule._LengthStation._MeasureOfTheodolite.Count
                        && dataGridViewLength.CurrentCell.ColumnIndex != 8
                        && this._stationLengthModule._LengthStation._Parameters._StationPoint._Name != dataGridViewLength[2, dataGridViewLength.CurrentCell.RowIndex].Value.ToString())
                    {
                        int index1 = this._stationLengthModule._LengthStation._MeasureOfTheodolite.FindIndex(x => x._PointName == dataGridViewLength[2, this.rowActiveCell].Value.ToString());
                        if (index1 != -1)
                        {
                            this._stationLengthModule.SetTobeMeasureData(this._stationLengthModule._LengthStation._MeasureOfTheodolite[index1]);
                        }
                    }
                }
                this.dataGridViewLength.CurrentCellChanged += new System.EventHandler(this.dataGridViewLength_CurrentCellChanged);
            }
            internal void CellFocusDataGridView(string pointName)
            //remet le focus sur la cellule mesure du point name
            {
                for (int i = 0; i < dataGridViewLength.RowCount; i++)
                {
                    if (dataGridViewLength[1, i].Value.ToString() == pointName)
                    {
                        rowActiveCell = i;
                        columnActiveCell = 6;
                        CellFocusDataGridView();
                    }
                }
            }
            private void dataGridViewLength_CurrentCellChanged(object sender, EventArgs e)
            {
                /// change le current index du datagrid
                if (dataGridViewLength.CurrentCell != null)
                {
                    if (dataGridViewLength.CurrentCell.RowIndex != -1 && dataGridViewLength.CurrentCell.ColumnIndex != -1 && dataGridViewLength.CurrentCell.RowIndex < this._stationLengthModule._LengthStation._MeasureOfTheodolite.Count)
                    {
                        if ((this.rowActiveCell != dataGridViewLength.CurrentCell.RowIndex || this.columnActiveCell != dataGridViewLength.CurrentCell.ColumnIndex) && this.checkCurrentCell)
                        {
                            ////BeginInvoke crèe un processus asynchrone qui empêche de tourner en boucle lorsqu'on change la current cell
                            Delegate delegateCurrentCell = new MethodInvoker(() =>
                            {
                                //this.Module.InvokeOnApplicationDispatcher(SetCurrentCell);
                                this.CellFocusDataGridView();
                                this.checkCurrentCell = false;
                            });
                            IAsyncResult async = this.BeginInvoke(delegateCurrentCell);
                        }
                        //this.rowActiveCell = dataGridViewLevel.CurrentCell.RowIndex;
                        //this.columnActiveCell = dataGridViewLevel.CurrentCell.ColumnIndex;
                        /////Change la mesure à faire dans l'instrument
                        //if (dataGridViewLevel.CurrentCell.RowIndex < this._stationLengthModule._LengthStation._MeasureOfTheodolite.Count 
                        //    && dataGridViewLevel.CurrentCell.ColumnIndex != 8
                        //    && this._stationLengthModule._LengthStation._Parameters._StationPoint._Name != dataGridViewLevel[2, dataGridViewLevel.CurrentCell.RowIndex].Value.ToString())
                        //{
                        //    int index1 = this._stationLengthModule._LengthStation._MeasureOfTheodolite.FindIndex(x => x._PointName == dataGridViewLevel[2, this.rowActiveCell].Value.ToString());
                        //    if (index1 != -1)
                        //    {
                        //        this._stationLengthModule.SetTobeMeasureData(this._stationLengthModule._LengthStation._MeasureOfTheodolite[index1]);
                        //    }
                        //}
                    }
                }
            }
            /// <summary>
            /// Evenements si on clique dans une case du datagridView
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void dataGridViewLength_CellClick(object sender, DataGridViewCellEventArgs e)
            {
                this.dataGridViewLength.CurrentCellChanged -= this.dataGridViewLength_CurrentCellChanged;
                double na = TSU.Tsunami2.Preferences.Values.na;
                if (e.RowIndex != -1 && e.ColumnIndex != -1 && e.RowIndex < this._stationLengthModule._LengthStation._MeasureOfTheodolite.Count)
                {

                    if (this.rowActiveCell != e.RowIndex)
                    {
                        this.rowActiveCell = e.RowIndex;
                        if ((e.RowIndex < this._stationLengthModule._LengthStation._MeasureOfTheodolite.Count)
                    && this._stationLengthModule._LengthStation._Parameters._StationPoint._Name != dataGridViewLength[2, e.RowIndex].Value.ToString()
                    && e.ColumnIndex != 1)
                        {
                            this._stationLengthModule.actualPointFocussed = dataGridViewLength[2, e.RowIndex].Value.ToString();
                        }
                        this.UpdateDatagridViewLength();
                        //si on change de station, on ne veut pas focus dessus et envoyer la mesure à l'instrument
                        if (e.ColumnIndex != 1)
                        {
                            this.CellFocusDataGridView();
                        }
                    }
                    //Ne permet de mettre le focus sur la case de suppression car cela pose des problèmes dans les modules guidés lors du focus sur une cellule invisible
                    if (e.ColumnIndex != 0)
                        this.columnActiveCell = e.ColumnIndex;
                    else
                        this.columnActiveCell = 6;
                }
                if (dataGridViewLength.IsCurrentCellInEditMode) return;
                if (e.RowIndex == -1 && e.ColumnIndex != -1 && e.RowIndex < this._stationLengthModule._LengthStation._MeasureOfTheodolite.Count)
                {
                    if (dataGridViewLength.RowCount > 1)
                    {
                        this.buttonSort_Click();
                    }
                    return;
                }
                ///Point effacé
                if ((e.ColumnIndex == 0) && (dataGridViewLength.RowCount > 1) && (e.RowIndex < this._stationLengthModule._LengthStation._MeasureOfTheodolite.Count)
                    && this._stationLengthModule._LengthStation._Parameters._StationPoint._Name != dataGridViewLength[2, e.RowIndex].Value.ToString())
                {
                    string titleAndMessage = String.Format(R.StringLevel_ConfirmDeletePoint, dataGridViewLength[2, e.RowIndex].Value.ToString());
                    MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.T_OK)
                    {
                        this._stationLengthModule.DeletePointInAllStation(dataGridViewLength[2, e.RowIndex].Value.ToString());
                    }
                }
                ///Changement reflecteur
                if ((e.ColumnIndex == 14) && (dataGridViewLength.RowCount >= 1) && (e.RowIndex < this._stationLengthModule._LengthStation._MeasureOfTheodolite.Count)
                    && this._stationLengthModule._LengthStation._Parameters._StationPoint._Name != dataGridViewLength[2, e.RowIndex].Value.ToString())
                {
                    this._stationLengthModule.ChangeReflector(this._stationLengthModule._LengthStation._MeasureOfTheodolite.Find(x => x._PointName == dataGridViewLength[2, e.RowIndex].Value.ToString()));
                    this._stationLengthModule.SetTobeMeasureData(this._stationLengthModule._LengthStation._MeasureOfTheodolite.Find(x => x._PointName == dataGridViewLength[2, e.RowIndex].Value.ToString()));
                }
                ///Changement status mesure
                if ((e.ColumnIndex == 4) && (dataGridViewLength.RowCount > 1) && (e.RowIndex < this._stationLengthModule._LengthStation._MeasureOfTheodolite.Count)
                    )//&& this._stationLengthModule._LengthStation._Parameters._StationPoint._Name != dataGridViewLevel[2, e.RowIndex].Value.ToString())
                {
                    this._stationLengthModule.ReverseMeasureStatus(dataGridViewLength[2, e.RowIndex].Value.ToString());
                    this._stationLengthModule.SetTobeMeasureData(this._stationLengthModule._LengthStation._MeasureOfTheodolite.Find(x => x._PointName == dataGridViewLength[2, e.RowIndex].Value.ToString()));
                }
                //CALA Autorise de ne pas avoir de points de cala car le calcul de l'average move ne se fera plus
                if (e.ColumnIndex == 3
                    && (e.RowIndex < this._stationLengthModule._LengthStation._MeasureOfTheodolite.Count))
                //&& this._stationLengthModule._LengthStation._Parameters._StationPoint._Name != dataGridViewLevel[2, e.RowIndex].Value.ToString())
                {
                    //Ajoute un ancrage si on clique sur cellule CALA. cliquer sur point stationné ne change rien au calcul. Un point stationné n'est pas en CALA
                    System.Drawing.Bitmap image = dataGridViewLength[3, e.RowIndex].Value as System.Drawing.Bitmap;
                    string pointName = dataGridViewLength[2, e.RowIndex].Value.ToString();
                    //Astuce image ancrage vide fait 65 de large et on ne veut pas que le point stationné soit un ancrage
                    if (image.Width == 65)
                    {
                        int index2 = this._stationLengthModule._TheoPoint.FindIndex(x => x._Name == pointName);
                        if (index2 != -1)
                        {
                            this._stationLengthModule._TheoPoint[index2].LGCFixOption = TSU.ENUM.LgcPointOption.CALA;
                            this._stationLengthModule.FinalModule.CalaPoints.Add(this._stationLengthModule._TheoPoint[index2]);
                        }
                        int index1 = this._stationLengthModule.FinalModule.PointsToBeAligned.FindIndex(x => x._Name == pointName);
                        if (index1 != -1) { this._stationLengthModule.FinalModule.PointsToBeAligned.RemoveAt(index1); }
                        this._stationLengthModule._LengthStation._MeasureOfTheodolite.Find(x => x._PointName == pointName)._Point.LGCFixOption = TSU.ENUM.LgcPointOption.CALA;
                        this._stationLengthModule._LengthStation.ParametersBasic.LastChanged = DateTime.Now;
                    }
                    else
                    {
                        //List<MeasureTheodolite> listMeas = this._stationLengthModule._LengthStation._MeasureOfTheodolite.FindAll(x => x._Point.LGCFixOption == TSU.ENUM.LgcPointOption.CALA);
                        //if ( listMeas.Count > 1)
                        //{
                        int index3 = this._stationLengthModule._TheoPoint.FindIndex(x => x._Name == pointName);
                        if (index3 != -1)
                        {
                            this._stationLengthModule._TheoPoint[index3].LGCFixOption = TSU.ENUM.LgcPointOption.POIN;
                            this._stationLengthModule.FinalModule.PointsToBeAligned.Add(this._stationLengthModule._TheoPoint[index3]);
                        }
                        int index4 = this._stationLengthModule.FinalModule.CalaPoints.FindIndex(x => x._Name == pointName);
                        if (index4 != -1) { this._stationLengthModule.FinalModule.CalaPoints.RemoveAt(index4); }
                        this._stationLengthModule._LengthStation._MeasureOfTheodolite.Find(x => x._PointName == pointName)._Point.LGCFixOption = TSU.ENUM.LgcPointOption.POIN;
                        //}
                        //else
                        //{
                        //    this._stationLengthModule.View.ShowMessageOfCritical(R.StringLength_ErrorNumberOfPoint, R.T_OK);
                        //}
                    }
                    this._stationLengthModule.SetTobeMeasureData(this._stationLengthModule._LengthStation._MeasureOfTheodolite.Find(x => x._PointName == dataGridViewLength[2, e.RowIndex].Value.ToString()));
                    this._stationLengthModule.UpdateOffsets();
                }
                //else
                //{
                //    if (e.ColumnIndex == 3 && (e.RowIndex < this._stationLengthModule._LengthStation._MeasureOfTheodolite.Count))
                //    {
                //        this.ShowMessageOfExclamation(R.StringLength_ChangeCalaNotAvailable, R.T_OK);
                //    }
                //}
                //Point stationned. Ne change pas le point stationné si on clique dans la case du point déjà stationné
                if (e.ColumnIndex == 1
                    && (e.RowIndex < this._stationLengthModule._LengthStation._MeasureOfTheodolite.Count)
                    && this._stationLengthModule._LengthStation._Parameters._StationPoint._Name != dataGridViewLength[2, e.RowIndex].Value.ToString())
                {
                    //Change le point stationné. cliquer sur point stationné ne change rien au calcul. Un point stationné n'est pas dessiné en CALA dans le tableau
                    System.Drawing.Bitmap image = dataGridViewLength[1, e.RowIndex].Value as System.Drawing.Bitmap;
                    string pointName = dataGridViewLength[2, e.RowIndex].Value.ToString();
                    //Astuce image ancrage vide fait 65 de large 
                    if (image.Width == 65)
                    {
                        string respond = R.T_OK;
                        if (this._stationLengthModule._LengthStation._MeasureOfTheodolite.FindIndex(x => x.Distance.Raw.Value != na) != -1)
                        {
                            MessageInput mi = new MessageInput(MessageType.ImportantChoice, R.StringLength_ChangeStationPoint)
                            {
                                ButtonTexts = new List<string> { R.T_CANCEL, R.T_OK },
                                TextOfButtonToBeLocked = R.T_OK
                            };
                            respond = mi.Show().TextOfButtonClicked;
                        }
                        if (respond == R.T_OK)
                        {
                            this._stationLengthModule.SetPointAsStation(pointName);
                            this._stationLengthModule._LengthStation.ParametersBasic.LastChanged = DateTime.Now;
                        }
                    }
                    //this._stationLengthModule.SetTobeMeasureData(this._stationLengthModule._LengthStation._MeasureOfTheodolite.Find(x => x._PointName == dataGridViewLevel[2, e.RowIndex].Value.ToString()));
                    this._stationLengthModule.SetTobeMeasureData(null);
                    this._stationLengthModule.UpdateOffsets();
                }
                //Inversion double/simple face
                if (e.ColumnIndex == 13
                    && (e.RowIndex < this._stationLengthModule._LengthStation._MeasureOfTheodolite.Count)
                    && this._stationLengthModule._LengthStation._Parameters._StationPoint._Name != dataGridViewLength[2, e.RowIndex].Value.ToString())
                {
                    //Inversion entre double et simple face
                    string pointName = dataGridViewLength[2, e.RowIndex].Value.ToString();
                    int index1 = this._stationLengthModule._LengthStation._MeasureOfTheodolite.FindIndex(x => x._PointName == pointName);
                    if (index1 != -1)
                    {
                        this._stationLengthModule._LengthStation._MeasureOfTheodolite[index1].SwitchFace();
                    }
                    this._stationLengthModule.SetTobeMeasureData(this._stationLengthModule._LengthStation._MeasureOfTheodolite.Find(x => x._PointName == dataGridViewLength[2, e.RowIndex].Value.ToString()));
                    this.UpdateListMeasure();
                }
                ///Lance une mesure sur une mesure à faire
                if (e.ColumnIndex == 8
                 && (e.RowIndex < this._stationLengthModule._LengthStation._MeasureOfTheodolite.Count)
                 && this._stationLengthModule._LengthStation._Parameters._StationPoint._Name != dataGridViewLength[2, e.RowIndex].Value.ToString())
                {
                    string pointName = dataGridViewLength[2, e.RowIndex].Value.ToString();
                    int index1 = this._stationLengthModule._LengthStation._MeasureOfTheodolite.FindIndex(x => x._PointName == pointName);
                    if (index1 != -1)
                    {
                        this._stationLengthModule.LaunchMeasure(this._stationLengthModule._LengthStation._MeasureOfTheodolite[index1]);
                    }
                }
                if (e.ColumnIndex != 8 && e.ColumnIndex != 1 && e.ColumnIndex != 13
                    && e.ColumnIndex != 0 && e.ColumnIndex != 3 && e.ColumnIndex != 4 && e.ColumnIndex != 14 && e.ColumnIndex != -1
                    && e.ColumnIndex != 1 && e.RowIndex < this._stationLengthModule._LengthStation._MeasureOfTheodolite.Count
                    && this._stationLengthModule._LengthStation._Parameters._StationPoint._Name != dataGridViewLength[2, e.RowIndex].Value.ToString())
                {
                    this._stationLengthModule.SetTobeMeasureData(this._stationLengthModule._LengthStation._MeasureOfTheodolite.Find(x => x._PointName == dataGridViewLength[2, e.RowIndex].Value.ToString()));
                }
                this.dataGridViewLength.CurrentCellChanged += new System.EventHandler(this.dataGridViewLength_CurrentCellChanged);
            }
            private void dataGridViewLength_CellValueChanged(object sender, DataGridViewCellEventArgs e)
            {
                //garde en mémoire la cellule active
                if (e.RowIndex < dataGridViewLength.Rows.Count && e.RowIndex != -1 && e.RowIndex < this._stationLengthModule._LengthStation._MeasureOfTheodolite.Count)
                {
                    if (this.rowActiveCell != e.RowIndex)
                    {
                        this.rowActiveCell = e.RowIndex;
                        this.UpdateDatagridViewLength();
                    }
                }
                if (e.ColumnIndex < dataGridViewLength.ColumnCount && e.ColumnIndex != -1)
                {
                    columnActiveCell = e.ColumnIndex;
                }
                this.checkCurrentCell = true;
                //commentaire entré
                if (e.RowIndex > -1 && e.ColumnIndex == 15)
                {
                    if (e.RowIndex >= 0)
                    {
                        //Reste sur la même ligne car cela change également la mesure dans l'instrument
                        if (rowActiveCell < this._stationLengthModule._LengthStation._MeasureOfTheodolite.Count - 1)
                        {
                            columnActiveCell = 15;
                            //rowActiveCell++;
                        }
                    }
                    if (dataGridViewLength[e.ColumnIndex, e.RowIndex].Value != null)
                    {
                        string newComment = dataGridViewLength[e.ColumnIndex, e.RowIndex].Value.ToString();
                        if (newComment.Length < 126)
                        {
                            if (!newComment.Contains(";") && !newComment.Contains("%") && !newComment.Contains("$")) // tsu-3144 (System.Text.RegularExpressions.Regex.IsMatch(newComment, @"^[a-zA-Z0-9 ]+$"))))
                            {
                                Polar.Measure mesureEntree = this._stationLengthModule._LengthStation._MeasureOfTheodolite.Find(x => x._PointName == dataGridViewLength[2, e.RowIndex].Value.ToString());
                                this._stationLengthModule.ChangeComment(mesureEntree, newComment);
                                return;
                            }
                            else
                            {
                                this.ShowMessage(R.StringTilt_WrongCommentSemicolon);
                                this.UpdateView();
                                return;
                            }
                        }
                        else
                        {
                            this.ShowMessage(R.StringLength_TooLongComment125);
                            this.UpdateView();
                            return;
                        }
                    }
                    else
                    {
                        //Commentaire effacé
                        Polar.Measure mesureEntree = this._stationLengthModule._LengthStation._MeasureOfTheodolite.Find(x => x._PointName == dataGridViewLength[2, e.RowIndex].Value.ToString());
                        this._stationLengthModule.ChangeComment(mesureEntree, "");
                        return;
                    }
                }
                //Rallonge entrée ou h instrument entrée
                if (e.RowIndex > -1 && e.ColumnIndex == 6)
                {
                    Polar.Measure mesureEntree = this._stationLengthModule._LengthStation._MeasureOfTheodolite.Find(x => x._PointName == dataGridViewLength[2, e.RowIndex].Value.ToString());
                    //Cas d'une rallonge entrée
                    if (mesureEntree._PointName != this._stationLengthModule._LengthStation._Parameters._StationPoint._Name)
                    {
                        // evite nullreferenceException si case vide
                        if (dataGridViewLength[e.ColumnIndex, e.RowIndex].Value == null)
                        {
                            //encode la nouvelle rallonge et lance la mise à jour
                            mesureEntree.Extension.Value = 0;
                            mesureEntree._Date = System.DateTime.Now;
                            this._stationLengthModule.SetTobeMeasureData(mesureEntree);
                            this._stationLengthModule._LengthStation.ParametersBasic.LastChanged = System.DateTime.Now;
                            this._stationLengthModule.UpdateOffsets();
                        }
                        else
                        {
                            double datagridNumber = T.Conversions.Numbers.ToDouble(dataGridViewLength[e.ColumnIndex, e.RowIndex].Value.ToString(), true, -9999);
                            if (datagridNumber != -9999)
                            {
                                mesureEntree.Extension.Value = datagridNumber / 1000;
                                this._stationLengthModule.SetTobeMeasureData(mesureEntree);
                                mesureEntree._Date = System.DateTime.Now;
                                this._stationLengthModule._LengthStation.ParametersBasic.LastChanged = System.DateTime.Now;
                                this._stationLengthModule.UpdateOffsets();
                            }
                            else
                            {
                                //message erreur, valeur encodée incorrecte
                                this.ShowMessage(R.StringLength_WrongCellValue);
                                this.UpdateDatagridViewLength();
                            }
                        }
                    }
                    //Cas d'une h instrument entrée
                    else
                    {
                        // evite nullreferenceException si case vide
                        if (dataGridViewLength[e.ColumnIndex, e.RowIndex].Value == null)
                        {
                            //encode la nouvelle h instrument et lance la mise à jour goto et meas en cours

                            this._stationLengthModule._LengthStation._Parameters.HInstrument = 0;
                            mesureEntree._Date = System.DateTime.Now;
                            this._stationLengthModule._LengthStation.ParametersBasic.LastChanged = System.DateTime.Now;
                            this._stationLengthModule.UpdateGotoForAllMeas();
                            this._stationLengthModule.UpdatePolarToBeMeasuredData();
                            this._stationLengthModule.UpdateOffsets();
                        }
                        else
                        {
                            double datagridNumber = T.Conversions.Numbers.ToDouble(dataGridViewLength[e.ColumnIndex, e.RowIndex].Value.ToString(), true, -9999);
                            if (datagridNumber != -9999)
                            {
                                this._stationLengthModule._LengthStation._Parameters.HInstrument = datagridNumber / 1000;
                                mesureEntree._Date = System.DateTime.Now;
                                this._stationLengthModule._LengthStation.ParametersBasic.LastChanged = System.DateTime.Now;
                                this._stationLengthModule.UpdateGotoForAllMeas();
                                this._stationLengthModule.UpdatePolarToBeMeasuredData();
                                this._stationLengthModule.UpdateOffsets();
                            }
                            else
                            {
                                //message erreur, valeur encodée incorrecte
                                this.ShowMessage(R.StringLength_WrongCellValue);
                                this.UpdateListMeasure();
                            }
                        }
                    }
                }
                //nbre de mesures entrées
                if (e.RowIndex > -1 && e.ColumnIndex == 12)
                {
                    Polar.Measure mesureEntree = this._stationLengthModule._LengthStation._MeasureOfTheodolite.Find(x => x._PointName == dataGridViewLength[2, e.RowIndex].Value.ToString());
                    // evite nullreferenceException si case vide
                    if (dataGridViewLength[e.ColumnIndex, e.RowIndex].Value == null)
                    {
                        //encode la nouvelle rallonge et lance la mise à jour
                        mesureEntree.NumberOfMeasureToAverage = 1;
                        mesureEntree._Date = System.DateTime.Now;
                        this._stationLengthModule._LengthStation.ParametersBasic.LastChanged = System.DateTime.Now;
                        this.UpdateListMeasure();
                    }
                    else
                    {
                        int datagridNumber = T.Conversions.Numbers.ToInt(dataGridViewLength[e.ColumnIndex, e.RowIndex].Value.ToString(), -9999);
                        if (datagridNumber != -9999 && datagridNumber > 0)
                        {
                            mesureEntree.NumberOfMeasureToAverage = datagridNumber;
                            mesureEntree._Date = System.DateTime.Now;
                            this._stationLengthModule._LengthStation.ParametersBasic.LastChanged = System.DateTime.Now;
                            this.UpdateListMeasure();
                        }
                        else
                        {
                            //message erreur, valeur encodée incorrecte
                            this.ShowMessage(R.StringLength_WrongCellValue);
                            this.UpdateListMeasure();
                        }
                    }
                }
            }
            private void SetFirstDisplayedRowIndex(int rowIndex)
            {
                // Use BeginInvoke to defer the operation to the UI thread
                dataGridViewLength.BeginInvoke((MethodInvoker)delegate
                {
                    try
                    {
                        dataGridViewLength.FirstDisplayedScrollingRowIndex = rowIndex;
                    }
                    catch (Exception ex)
                    {
                        Logs.Log.AddEntryAsPayAttentionOf(this.Module, $"Cannot reset the datagridView to the row #{rowIndex} {ex}");
                    }
                });
            }

            /// <summary>
            /// Met la scroll bar horizontale à gauche et la vertical en haut
            /// </summary>
            internal void SetScrollBarAtZero()
            {
                if (this.dataGridViewLength.Columns.Count > 0 && this.dataGridViewLength.Rows.Count > 0)
                {
                    if (this.dataGridViewLength.Columns[0].Visible && this.dataGridViewLength.Rows[0].Visible)
                    {
                        this.dataGridViewLength.FirstDisplayedScrollingColumnIndex = 0;
                        SetFirstDisplayedRowIndex(0);
                    }
                    else
                    {
                        if (this.dataGridViewLength.Columns.Count > 1 && this.dataGridViewLength.Rows.Count > 0)
                        {
                            if (this.dataGridViewLength.Columns[1].Visible && this.dataGridViewLength.Rows[0].Visible)
                            {
                                this.dataGridViewLength.FirstDisplayedScrollingColumnIndex = 1;
                                SetFirstDisplayedRowIndex(0);
                            }
                        }
                    }
                }
            }
            #endregion
            #region Treeview
            public void InitializeTreeviewParameters()
            //creation du treeview avec tous les nodes
            {
                if (this.treeView_Parameters.Nodes.Count == 0)
                {
                    this.treeView_Parameters.BeginUpdate();
                    this.treeView_Parameters.Nodes.Clear();
                    //Noeud Parameter
                    this.treeView_Parameters.ImageList = TreeNodeImages.ImageListInstance;
                    this.treeView_Parameters.SelectedImageKey = "StationParameters";
                    this.node_Parameters = new TsuNode();
                    this.node_Parameters.SelectedImageKey = "StationParameters";
                    this.node_Parameters.ImageKey = "StationParameters";
                    this.treeView_Parameters.Nodes.Add(this.node_Parameters);
                    this.DressNodeParameter();
                    //Noeud Admin
                    this.node_Admin = new TsuNode();
                    this.node_Parameters.Nodes.Add(this.node_Admin);
                    this.node_Admin.BasedOn(this);
                    //Noeud DefaultMeasParameter
                    this.node_DefaultMeas = new TsuNode();
                    this.node_Parameters.Nodes.Add(this.node_DefaultMeas);
                    this.node_DefaultMeas.DefaultMeasBasedOn(this._stationLengthModule._LengthStation._Parameters);
                    this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
                    this.contextMenuStrip.Click += new EventHandler(this.Context_OnClick);
                    this.treeView_Parameters.ExpandAll();
                    this.treeView_Parameters.EndUpdate();
                }
                else
                {
                    this.RedrawTreeviewParameters();
                }
            }
            public void RedrawTreeviewParameters()
            //creation du treeview avec tous les nodes
            {
                TsuNode ghostNode_Admin = new TsuNode();
                TsuNode ghostNode_DefaultMeas = new TsuNode();
                this.treeView_Parameters.BeginUpdate();
                this.DressNodeParameter();
                //Noeud Admin
                ghostNode_Admin.BasedOn(this);
                ghostNode_DefaultMeas.DefaultMeasBasedOn(this._stationLengthModule._LengthStation._Parameters);
                this.node_Admin.UpdateTsuNode(ghostNode_Admin);
                this.node_DefaultMeas.UpdateTsuNode(ghostNode_DefaultMeas);
                this.treeView_Parameters.EndUpdate();
            }
            /// <summary>
            /// Choix du nom du noeud paramètre et de son format
            /// </summary>
            internal void DressNodeParameter()
            {
                //Choix de l'entête du noeud paramètre
                this.node_Parameters.Name = "Parameters__";
                if (this._stationLengthModule._LengthStation != null)
                    this.node_Parameters.Text = string.Format(R.StringLevel_Parameters, this._stationLengthModule._LengthStation._Name, this._stationLengthModule._LengthStation._Parameters._State._Description);
                this.node_Parameters.State = ENUM.StateType.Normal;
                if (this._stationLengthModule._LengthStation._Parameters._State is Common.Station.State.Opening) this.node_Parameters.State = ENUM.StateType.Normal;
                if (this._stationLengthModule._LengthStation._Parameters._State is Common.Station.State.SFBcalculationSuccessfull) this.node_Parameters.State = ENUM.StateType.Normal;
                if (this._stationLengthModule._LengthStation._Parameters._State is Common.Station.State.StationLengthReadyToBeSaved) this.node_Parameters.State = ENUM.StateType.Ready;
                if (this._stationLengthModule._LengthStation._Parameters._State is Common.Station.State.StationLengthSaved) this.node_Parameters.State = ENUM.StateType.Ready;
                if (this._stationLengthModule._LengthStation._Parameters._State is Common.Station.State.SFBcalculationFailed) this.node_Parameters.State = ENUM.StateType.NotReady;
                this.node_Parameters.ColorNodeBaseOnState(this.node_Parameters);
                this.node_Parameters.Tag = this._stationLengthModule._LengthStation;
            }
            /// <summary>
            /// Evenement lorsqu'on clique dans le treeview
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void treeView_Parameters_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
            {
                if (!e.Node.Bounds.Contains(e.Location)) return;
                if (e.Node == null) return;
                this.CheckDatagridIsInEditMode();
                String[] nameSplit;
                Char splitChar = '_';
                nameSplit = e.Node.Name.Split(splitChar);
                if (this.moduleType == ENUM.ModuleType.Advanced)
                {
                    if ((nameSplit[0] == "Parameters") && (nameSplit[1] == "")) { this.RenameStation(); }
                    if (nameSplit[1] == "Team") { this.ChangeTeam(); }
                    if (nameSplit[1] == "OperationID") { this.ChangeOperation(); }
                    if (nameSplit[1] == "Instrument") { this.ChangeInstrument(); }
                    if (nameSplit[1] == "Tolerance") { this.ChangeTolerance(); }
                }
                if (nameSplit[1] == "Reflector") { this.SelectDefaultReflector(); }
                if (nameSplit[1] == "DoubleFace") { this.SwitchDefaultDoubleFace(); }
                if (nameSplit[1] == "NbreMeas") { this.SelectDefaultMeasNumber(); }
            }
            #endregion
            #region Encodage mesure guided module element alignment

            #endregion

            #region Instrument view
            /// <summary>
            /// Redimensionne le clavier du manual ecartometer lorsqu'on deplace le splitter
            /// </summary>
            private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
            {
                if (this._stationLengthModule != null)
                {
                    if (this._stationLengthModule._InstrumentManager != null)
                    {
                        if (this._stationLengthModule._InstrumentManager.SelectedInstrumentModule != null)
                        {
                            this.UpdateInstrumentView();
                        }
                    }
                }
            }
            internal override void UpdateInstrumentView()
            //permet d'afficher le form de l'instrument dans le panel du dessous
            {
                splitContainer1.Panel2.Controls.Clear();
                if (this._stationLengthModule._InstrumentManager.SelectedInstrumentModule == null)
                    this._stationLengthModule._InstrumentManager.SetInstrument(this._stationLengthModule._LengthStation.ParametersBasic._Instrument);
                TsuView v = this._stationLengthModule._InstrumentManager.SelectedInstrumentModule._TsuView;
                v.Dock = DockStyle.None;
                v.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                v.ShowDockedFill();
                splitContainer1.Panel2.Controls.Add(v);
                this.instrumentView = v;
            }
            private void SelectDefaultReflector()
            {
                this.dataGridViewLength.CancelEdit();
                this._stationLengthModule.SelectDefaultReflector();
            }
            #endregion


            #region button and click actions

            internal void buttonSave_Click()
            //enregistre les mesures dans un fichier dat
            {
                this.dataGridViewLength.CancelEdit();
                this._stationLengthModule.ExportToGeode();
            }
            internal void buttonSelectPoint_Click()
            //sélection d'élement
            {
                this.dataGridViewLength.CancelEdit();
                this._stationLengthModule.SelectPoints();
            }
            internal void Context_OnClick(object sender, EventArgs e)
            //cache le menu context si on clique dessus
            {
                contextMenuStrip.Hide();
            }

            /// <summary>
            /// Cache le context menu si on l'a ouvert par mégarde
            /// </summary>
            private void buttonCancel_Click()
            {
                this.CloseMenu();
            }
            /// <summary>
            /// Trie par Dcum ou inverse dcum
            /// </summary>
            private void buttonSort_Click()
            {
                this.dataGridViewLength.CancelEdit();
                switch (this._stationLengthModule.sortDirection)
                {
                    case SortDirection.Dcum:
                        this._stationLengthModule.sortDirection = SortDirection.InvDcum;
                        break;
                    case SortDirection.InvDcum:
                        this._stationLengthModule.sortDirection = SortDirection.Dcum;
                        break;
                    default:
                        break;
                }
                this._stationLengthModule.SortMeasure();
                this.UpdateListMeasure(false);
            }
            /// <summary>
            /// Do a opening check
            /// </summary>
            private void bigButton_Closing_Click()
            {
                this.dataGridViewLength.CancelEdit();
                this._stationLengthModule.LaunchOpeningCheck();
            }
            /// <summary>
            /// Montre le fichier dat exporté
            /// </summary>
            private void bigButton_ShowDatFile_Click()
            {
                this.dataGridViewLength.CancelEdit();
                string filepath = this._stationLengthModule._LengthStation.ParametersBasic._GeodeDatFilePath;
                if (System.IO.File.Exists(filepath)) Shell.Run(filepath);
            }
            /// <summary>
            /// change le type de module entre avancé et guidé
            /// Met à jour également le big button top
            /// </summary>
            /// <param name="moduleType"></param>
            internal void ChangeModuleType(TSU.ENUM.ModuleType newModuleType = ModuleType.Advanced)
            {
                this.moduleType = newModuleType;
            }
            #endregion

            #region On Changes

            /// <summary>
            /// Change l'instrument de mesure
            /// </summary>
            private void ChangeInstrument()
            {
                //choix de l'instrument
                //selection de l'instrument pour les mesures de longueur.
                Sensor instrument = this._stationLengthModule.SelectInstrument();
            }
            /// <summary>
            /// Change le nom de l'équipe terrain
            /// </summary>
            private void ChangeTeam()
            {
                List<string> buttonTexts = new List<string> { R.T_OK, R.T_CANCEL };

                MessageTsu.ShowMessageWithTextBox(R.String_TeamChoice, buttonTexts, out string buttonClicked, out string textInput, this._stationLengthModule._LengthStation._Parameters._Team.ToString());

                if (buttonClicked != R.T_CANCEL)
                {
                    //Vérifie que le nom d'équipe contient que des caractères alphanumériques et est de maximum 8 caractères
                    if (System.Text.RegularExpressions.Regex.IsMatch(textInput, @"^[a-zA-Z]+$") && (textInput.Length < 9 || textInput == R.String_UnknownTeam))
                    {
                        textInput = textInput.ToUpper();
                        this._stationLengthModule.ChangeTeam(textInput);
                        this.RedrawTreeviewParameters();
                    }
                    else
                    {
                        this.ShowMessage(R.String_WrongTeam);
                    }
                }
            }
            private void ChangeOperation()
            {
                SelectOperation();
            }
            /// <summary>
            /// Change la tolérance d'alignement
            /// </summary>
            private void ChangeTolerance()
            {
                //changement de la tolérance d'alignement
                string initialTextBoxText = Math.Round(this._stationLengthModule._LengthStation._Parameters._Tolerance * 1000, 2).ToString();
                List<string> buttonTexts = new List<string> { R.T_OK, R.T_CANCEL };

                MessageTsu.ShowMessageWithTextBox(R.StringLength_AlignTolerance, buttonTexts, out string buttonClicked, out string textInput, initialTextBoxText);

                if (buttonClicked != R.T_CANCEL)
                {
                    double newTolerance = T.Conversions.Numbers.ToDouble(textInput, true, -9999);
                    if (newTolerance != -9999)
                    {
                        this._stationLengthModule.ChangeTolerance(Math.Abs(newTolerance) / 1000);
                        this.RedrawTreeviewParameters();
                    }
                    else
                    {
                        this.ShowMessage(R.String_WrongTolerance);
                    }
                }
            }
            public void SelectOperation()
            {
                O.Operation o = this._stationLengthModule.FinalModule.OperationManager.SelectOperation();

                if (o != null)
                {
                    this._stationLengthModule.ChangeOperationID(o);
                    this.RedrawTreeviewParameters();
                }
            }
            private void SwitchDefaultDoubleFace()
            {
                this._stationLengthModule.SwitchDefaultDoubleFace();
            }
            private void SelectDefaultMeasNumber()
            {
                List<string> buttonTexts = new List<string> { R.T_OK, R.T_CANCEL };

                MessageTsu.ShowMessageWithTextBox(R.StringLength_DefaultMeasureNumber, buttonTexts, out string buttonClicked, out string textInput, this._stationLengthModule._LengthStation._Parameters._DefaultNumberMeas.ToString());

                if (buttonClicked != R.T_CANCEL)
                {
                    int i = T.Conversions.Numbers.ToInt(textInput, -1);
                    if (i > 0)
                    {
                        this._stationLengthModule.ChangeDefaultMeasNumber(i);
                    }
                    else
                    {
                        this.ShowMessage(R.StringLength_WrongMeasNumber);
                    }
                }
            }
            /// <summary>
            /// Permet à l'utilisateur de renommer la station comme il le veut
            /// </summary>
            private void RenameStation()
            {
                List<string> buttonTexts = new List<string> { R.T_OK, R.T_CANCEL };

                MessageTsu.ShowMessageWithTextBox(R.String_StationName, buttonTexts, out string buttonClicked, out string textInput, this._stationLengthModule._LengthStation.specialPartOfStationName);

                if (buttonClicked != R.T_CANCEL)
                {
                    //Vérifie que le nom personalisé de la station contient que des caractères alphanumériques et est de maximum 8 caractères
                    if ((System.Text.RegularExpressions.Regex.IsMatch(textInput, @"^[a-zA-Z0-9._-]+$") || textInput == "") && (textInput.Length < 20))
                    {
                        textInput = textInput.ToUpper();
                        this._stationLengthModule.RenameStation(textInput);
                        this.RedrawTreeviewParameters();
                    }
                    else
                    {
                        this.ShowMessage(R.String_WrongStationName);
                    }
                }
            }
            #endregion

            #region Message

            internal void ShowMessage(string titreAndMessage)
            //Affiche la fenêtre d'erreur si encodage incorrect
            {
                new MessageInput(MessageType.Critical, titreAndMessage).Show();
            }

            internal void ShowMessageSave()
            //affiche message de sauvegarde avant changement station aller-retour-reprise
            {
                if (!(this._stationLengthModule._LengthStation._Parameters._State is Common.Station.State.StationLengthSaved))
                {
                    if (this._stationLengthModule._LengthStation._Parameters._State is Common.Station.State.StationLengthReadyToBeSaved)
                    {
                        MessageInput mi = new MessageInput(MessageType.Choice, R.StringLength_AskSaving)
                        {
                            ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                        };
                        if (mi.Show().TextOfButtonClicked == R.T_YES)
                        {
                            this._stationLengthModule.ExportToGeode();
                        }
                    }
                    else
                    {
                        new MessageInput(MessageType.Warning, R.StringLength_CannotSave)
                        {
                            ButtonTexts = new List<string> { R.T_OK + "!" }
                        }.Show();
                    }
                }

            }
            internal void ShowMessageSelectStation()
            {
                new MessageInput(MessageType.GoodNews, R.StringLength_MessageSelectStation).Show();
            }
            private void splitContainer_AlignText_Panel1_Paint(object sender, PaintEventArgs e)
            {

            }

            #endregion
        }
    }
}
