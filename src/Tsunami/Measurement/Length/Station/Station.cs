using System;
using System.Collections.Generic;
using System.Xml;
using System.Linq;
using System.Xml.Serialization;
using TSU.ENUM;
using R = TSU.Properties.Resources;
using TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using M = TSU.Common.Measures;
using I = TSU.Common.Instruments;
using System.IO;
using TSU.Common.Compute;

namespace TSU.Length
{
    [XmlType(TypeName = "Length.Station")]
    [Serializable]
    public partial class Station : Common.Station
    {
        public override CloneableList<M.Measure> MeasuresTaken
        {
            get
            {
                CloneableList<M.Measure> l = new CloneableList<M.Measure>();
                l.AddRange(_MeasureOfTheodolite);
                return l;
            }
        }

        //variables
        public List<Polar.Measure> _MeasureOfTheodolite { get; set; }
        public Polar.Measure _OpeningMeas { get; set; }
        public Polar.Measure _ClosingMeas { get; set; }
        public bool nameSetByUser { get; set; }
        public string specialPartOfStationName { get; set; }
        [XmlIgnore]
        public Length.Station.Parameters _Parameters
        {
            get
            {
                return this.ParametersBasic as Length.Station.Parameters;
            }
            set
            {
                this.ParametersBasic = value;
            }
        }
        [XmlIgnore]
        public M.WeatherConditions WeatherConditions
        {
            get
            {
                if (this._OpeningMeas != null)
                    return (this._OpeningMeas as Polar.Measure).Distance.WeatherConditions;
                else
                    return new M.WeatherConditions();
            }
        }
        public Station()
            //constructeur
            : base()
        {
            this._Parameters = new Length.Station.Parameters();
            this._MeasureOfTheodolite = new List<Polar.Measure>();
            this._OpeningMeas = null;
            this._ClosingMeas = null;
            this._Name = "STDIST_" + String.Format("{0:yyyyMMdd_HHmmss}" + "_", DateTime.Now);
            this._Parameters._StationName = this._Name;
            this._Parameters._Station = this;
            this.specialPartOfStationName = "";
            this.nameSetByUser = false;
        }

        public override string ToString()
        {
            return String.Format(R.TS_Station, this._Name);
        }

        public new Station DeepCopy()
        //copie avec un memory stream de toute la station level sauf l'instrument et la mire dont on garde la r�f�rence ancienne
        {
            using (MemoryStream stream = new MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                Station deepCopy = (Station)formatter.Deserialize(stream);
                // r�f�rence vers l'instrument actuel
                deepCopy._Parameters._Instrument = this._Parameters._Instrument;
                deepCopy.ParametersBasic._Instrument = deepCopy._Parameters._Instrument;
                return deepCopy;
            }
        }
        internal void Rename(string userStationName = "NotSet")
        //Renomme le nom de la station en fonction du point stationn�
        {
            string commonPart = this._Name.Substring(0, 22) + "_";
            ///Change the name with the correct levelling direction
            this._Name = commonPart + this.specialPartOfStationName;
            if (this._Parameters._StationPoint._Point != R.String_Unknown && this.nameSetByUser == false)
            {
                this._Name = commonPart + this._Parameters._StationPoint._Point;
                this._Parameters._StationName = this._Name;
                specialPartOfStationName = this._Parameters._StationPoint._Point;
            }
            if (userStationName != "NotSet")
            {
                this._Name = commonPart + userStationName;
                this._Parameters._StationName = this._Name;
                this.nameSetByUser = true;
                this.specialPartOfStationName = userStationName;
            }
        }
        /// <summary>
        /// Trie les mesures dans l'ordre des Dcum
        /// </summary>
        internal void SortMeasures()
        {
            this._MeasureOfTheodolite.Sort();
        }
        /// <summary>
        /// Trie les mesures dans l'ordre inverse des Dcum
        /// </summary>
        internal void ReverseMeasures()
        {
            this._MeasureOfTheodolite.Sort();
            this._MeasureOfTheodolite.Reverse();
        }
        internal void UpdateListMeasureTheodolite(TSU.Length.Station.Module stationLengthModule, Point point)
        //cr�ation d'une mesure vide avec les param�tres par d�faut.
        {
            Polar.Measure blankMeasureOfTheodolite = new Polar.Measure();
            blankMeasureOfTheodolite._Date = System.DateTime.MinValue;
            blankMeasureOfTheodolite.enableCommentWithStatus = false;
            blankMeasureOfTheodolite._Point = (TSU.Common.Elements.Point)point.DeepCopy();
            blankMeasureOfTheodolite.Distance._Point = blankMeasureOfTheodolite._Point;
            if (point.LGCFixOption == TSU.ENUM.LgcPointOption.CALA)
            {
                blankMeasureOfTheodolite._Point.LGCFixOption = TSU.ENUM.LgcPointOption.CALA;
            }
            else
            {
                blankMeasureOfTheodolite._Point.LGCFixOption = TSU.ENUM.LgcPointOption.POIN;
            }
            blankMeasureOfTheodolite.SelectDefaultExtension();
            blankMeasureOfTheodolite.Reflector = stationLengthModule._defaultReflector.DeepCopy();
            blankMeasureOfTheodolite.NumberOfMeasureToAverage = this._Parameters._DefaultNumberMeas;
            if (this._Parameters._DefaultDoubleFace) blankMeasureOfTheodolite.Face = I.FaceType.DoubleFace;
            else blankMeasureOfTheodolite.Face = I.FaceType.Face1;
            this._MeasureOfTheodolite.Add(blankMeasureOfTheodolite);
        }

        internal void SetStationPoint(Point point)
        {
            this._Parameters._StationPoint = point.DeepCopy();
            this._Parameters.HInstrument = point.SocketCode.InstrumentHeightForPolarMeasurement;
        }
        /// <summary>
        /// Calcule la distance theo pour tous les points
        /// </summary>
        internal void CalculateDistHorizTheo()
        {
            int index = this._MeasureOfTheodolite.FindIndex(x=>x._PointName==this._Parameters._StationPoint._Name);
            if (index != -1)
            {
                Polar.Measure measStation = this._MeasureOfTheodolite[index];
                if (this.CheckIfTheoPointHasCoordinate(measStation._Point))
                {
                    ///Passe en premier lieu les coordonn�es de la station en coordonn�es MLA
                    switch (this._Parameters._CoordType)
                    {
                        case CoordinatesType.CCS:
                            //Transforme les coordonn�es theo de H vers Z CCS
                            measStation.ConvertHToZStation(Coordinates.ReferenceSurfaces.RS2K, this._Parameters.HInstrument);
                            //Passe dans un syst�me MLA
                            measStation.CCSToMLA(measStation, Coordinates.ReferenceSurfaces.RS2K);
                            break;
                        case CoordinatesType.SU:
                            ///Syst�me SU est d�j� un syst�me MLA
                            measStation._Point._Coordinates.Mla.X.Value = measStation._Point._Coordinates.Su.X.Value;
                            measStation._Point._Coordinates.Mla.Y.Value = measStation._Point._Coordinates.Su.Y.Value;
                            measStation._Point._Coordinates.Mla.Z.Value = measStation._Point._Coordinates.Su.Z.Value+ this._Parameters.HInstrument;
                            break;
                        default:
                            //Transforme les coordonn�es theo de H vers Z CCS
                            measStation.ConvertHToZStation(Coordinates.ReferenceSurfaces.RS2K, this._Parameters.HInstrument);
                            //Passe dans un syst�me MLA
                            measStation.CCSToMLA(measStation, Coordinates.ReferenceSurfaces.RS2K);
                            break;
                    }
                    measStation.CalculateDistHorizTheo(measStation); //must be 0
                    foreach (Polar.Measure meas in this._MeasureOfTheodolite)
                    {
                        this.CalculateDistHorizTheo(measStation, meas);
                    }
                }

            }
        }
        /// <summary>
        /// Calcule la distance horizontale theorique entre la station et le point mesur�
        /// </summary>
        /// <param name="measStation"></param>
        /// <param name="meas"></param>
        private void CalculateDistHorizTheo(Polar.Measure measStation, Polar.Measure meas)
        {
            if (this.CheckIfTheoPointHasCoordinate(measStation._Point) && meas._PointName != measStation._PointName)
            {
                switch (this._Parameters._CoordType)
                {
                    case CoordinatesType.CCS:
                        //Transforme les coordonn�es theo de H vers Z CCS
                        meas.ConvertHToZMeasure(Coordinates.ReferenceSurfaces.RS2K);
                        //Passe dans un syst�me MLA
                        meas.CCSToMLA(measStation, Coordinates.ReferenceSurfaces.RS2K);
                        break;
                    case CoordinatesType.SU:
                        ///Syst�me SU est d�j� un syst�me MLA
                        meas._Point._Coordinates.Mla.X.Value = meas._Point._Coordinates.Su.X.Value;
                        meas._Point._Coordinates.Mla.Y.Value = meas._Point._Coordinates.Su.Y.Value;
                        meas._Point._Coordinates.Mla.Z.Value = meas._Point._Coordinates.Su.Z.Value+meas.Extension.Value;
                        break;
                    default:
                        //Transforme les coordonn�es theo de H vers Z CCS
                        meas.ConvertHToZMeasure(Coordinates.ReferenceSurfaces.RS2K);
                        //Passe dans un syst�me MLA
                        meas.CCSToMLA(measStation, Coordinates.ReferenceSurfaces.RS2K);
                        break;
                }
                meas.CalculateDistHorizTheo(measStation);
            }
        }

        /// <summary>
        /// Met � jour les coordonn�es au th�orique pour chaque point mesur�
        /// </summary>
        /// <param name="listTheoPoint"></param>
        internal void UpdateTheoCoordinates(List<Point> listTheoPoint)
        // Met � jour les coordonn�es au th�orique pour chaque point mesur�
        {
            foreach (Polar.Measure meas in this._MeasureOfTheodolite)
            {
                meas._Point._Coordinates = listTheoPoint.Find(x => x._Name == meas._PointName)._Coordinates.DeepCopy();
            }
        }
        /// <summary>
        /// Remet � la valeur par d�faut pour les distances calcul�es dans chaque mesure th�odolite
        /// </summary>
        internal void ResetMeas()
        {
            double na =TSU.Tsunami2.Preferences.Values.na;
            foreach (Polar.Measure meas in this._MeasureOfTheodolite)
            {
                meas.Distance.distTheoHoriz.Value = na;
                meas.Distance.distMesHoriz.Value = na;
                meas.Distance.distAvgMove.Value = na;
                meas.Distance.distOffsetDiffTheoMeas.Value = na;
            }

        }
        /// <summary>
        /// V�rifie si le meas point a des coordonn�es.
        /// </summary>
        /// <param name="measPoint"></param>
        /// <returns></returns>
        internal bool CheckIfTheoPointHasCoordinate(Point measPoint)
        {
            bool hasCoord = true;
            double na =TSU.Tsunami2.Preferences.Values.na;
            switch (this._Parameters._CoordType)
            {
                case CoordinatesType.CCS:
                    if (measPoint._Coordinates.Ccs.X.Value == na || measPoint._Coordinates.Ccs.Y.Value == na || measPoint._Coordinates.Ccs.Z.Value == na)
                    {
                        hasCoord = false;
                    }
                    break;
                case CoordinatesType.SU:
                    if (measPoint._Coordinates.Su.X.Value == na || measPoint._Coordinates.Su.Y.Value == na || measPoint._Coordinates.Su.Z.Value == na)
                    {
                        hasCoord = false;
                    }
                    break;
                default:
                    break;
            }

            return hasCoord;
        }
        /// <summary>
        /// Calcule la distance horizontale mesur�e
        /// </summary>
        internal void CalculateDistHorizMes()
        {
            foreach (Polar.Measure meas in this._MeasureOfTheodolite)
            {
                CalculateDistHorizMes(meas);
            }
            if (this._OpeningMeas!=null)
            {
                this.CalculateDistHorizMes(this._OpeningMeas);
            }
            if (this._ClosingMeas!=null)
            {
                this.CalculateDistHorizMes(this._ClosingMeas);
            }
        }
        /// <summary>
        /// Calcule la distance horizontale mesur�e entre le point mesur� et la station 
        /// </summary>
        /// <param name="meas"></param>
        private void CalculateDistHorizMes(Polar.Measure meas)
        {
            if (this._Parameters._StationPoint._Name == meas._PointName)
            {
                ///Distance mesur�e du point stationn� = 0
                meas.Distance.distMesHoriz.Value = 0;
            }
            else
            {
                double na =TSU.Tsunami2.Preferences.Values.na;
                if (meas.Distance.Corrected != null && meas.Angles.corrected.Vertical != null)
                {
                    if ((meas.Distance.Corrected.Value != na && meas.Angles.corrected.Vertical.Value != na))
                    {
                        meas.Distance.distMesHoriz.Value = Survey.GetHorizontalDistance(meas.Angles.corrected.Vertical.Value, meas.Distance.Corrected.Value);
                    }
                    else
                    {
                        meas.Distance.distMesHoriz = new DoubleValue();
                    }
                }
                else
                {
                    meas.Distance.distMesHoriz = new DoubleValue();
                }
            }
        }

        /// <summary>
        /// Calcule la diff�rence entre la distance mesur�e et theo met le bon signe pour l'offset en fonction de la dcum station
        /// </summary>
        internal void CalculateDiffOffsetDistance()
        {
            double dcumStation = this._Parameters._StationPoint._Parameters.Cumul;
            double na =TSU.Tsunami2.Preferences.Values.na;
            foreach (Polar.Measure mes in this._MeasureOfTheodolite)
            {
                if (mes.Distance.distTheoHoriz.Value!=na && mes.Distance.distMesHoriz.Value != na)
                {
                    ///Met le bon signe � l'offset
                    if (mes._Point._Parameters.Cumul >= dcumStation)
                    {
                        mes.Distance.distOffsetDiffTheoMeas.Value = mes.Distance.distMesHoriz.Value - mes.Distance.distTheoHoriz.Value;
                    }
                    else
                    {
                        mes.Distance.distOffsetDiffTheoMeas.Value = mes.Distance.distTheoHoriz.Value - mes.Distance.distMesHoriz.Value;
                    }
                }
            }

        }
        /// <summary>
        /// Calcule le d�placement moyen � faire pour chaque point
        /// </summary>
        internal void CalculateAverageMove()
        {
            double na = TSU.Preferences.Preferences.NotAvailableValueNa;
            //Regarde si on a au moins un point en cala et pas en bad pour faire le calcul
            int indexCala = this._MeasureOfTheodolite.FindIndex(x => x._Point.LGCFixOption == LgcPointOption.CALA && !(x._Status is M.States.Bad));
            if (indexCala != -1)
            {
                //On ne fait le calcul que si toutes les r�f�rences ont �t� mesur�es
                double avg = 0;
                double numberOfCala = 0;
                foreach (Polar.Measure mes in this._MeasureOfTheodolite)
                {
                    if (mes._Point.LGCFixOption == LgcPointOption.CALA && !(mes._Status is M.States.Bad))
                    {
                        //Si un point en cala n'a pas de mesure, ne fait pas la moyenne
                        if (mes.Distance.distOffsetDiffTheoMeas.Value == na){ return; }
                        numberOfCala++;
                        avg += mes.Distance.distOffsetDiffTheoMeas.Value;
                    }
                }
                if (numberOfCala != 0)
                {
                    avg = avg / numberOfCala;
                }
                // Calcul du d�placement moyen corrig� de la moyenne
                foreach (Polar.Measure mes in this._MeasureOfTheodolite)
                {
                    if (mes.Distance.distOffsetDiffTheoMeas.Value != na)
                    {
                        mes.Distance.distAvgMove.Value = -(mes.Distance.distOffsetDiffTheoMeas.Value - avg);
                    }
                }
                List<Polar.Measure> mesAlign = this._MeasureOfTheodolite.FindAll(x => x.Distance.distOffsetDiffTheoMeas.Value != na);
                List<List<string>> magnetList = new List<List<string>>();
                List<string> l = new List<string>();
                if (mesAlign != null)
                {
                    ///Cr�e la liste de tous les types de magnets diff�rents
                    foreach (Polar.Measure item in mesAlign)
                    {
                        string magnetName = item._Point._Accelerator + ";" + item._Point._ClassAndNumero;
                        if (l.FindIndex(x=>x==magnetName)==-1)
                        {
                            l.Add(magnetName);
                            List<string> row = new List<string>
                            {
                                //Accelerateur
                                item._Point._Accelerator,
                                item._Point._Class,
                                item._Point._Numero
                            };
                            magnetList.Add(row);
                        }
                    }
                    foreach (List<string> row in magnetList)
                    {
                        ///Cr�e des listes de mesures associ�es pour chaque type d'aimant
                        List<Polar.Measure> magnet = mesAlign.FindAll(x => row[0] == x._Point._Accelerator && row[1] == x._Point._Class && row[2] == x._Point._Numero);
                        ///Calcule la moyenne des d�placements pour chaque aimant
                        double avgAlign = 0;
                        int n = 0;
                        foreach (Polar.Measure m in magnet)
                        {
                            if (m.Distance.distAvgMove.Value!=na && !(m._Status is M.States.Bad))
                            {
                                avgAlign += m.Distance.distAvgMove.Value;
                                n++;
                            }
                        }
                        if (n!=0)
                        {
                            avgAlign = avgAlign / n;
                        }
                        //Remplace le d�placement par la valeur moyenne pour chaque aimant
                        foreach (Polar.Measure m in magnet)
                        {
                            if (avgAlign!=na)
                            {
                                m.Distance.distAvgMove.Value = avgAlign;
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Reinitialize toutes les mesures faites car changement de station
        /// </summary>
        internal void FullCleanAllMeas()
        {
            double na = TSU.Preferences.Preferences.NotAvailableValueNa;
            this.ResetMeas();
            foreach (Polar.Measure m in this._MeasureOfTheodolite)
            {
                m.Clean(removeAnglesForGoto: true);
            }
            this._OpeningMeas = null;
            this._ClosingMeas = null;
            this._Parameters.Vzero.Value = na;
        }
        /// <summary>
        /// Trie les mesures du SPS en mettant le sextant 6 avant le sextant 1  
        /// </summary>
        internal void SortMeasForSPSOrigin()
        {
            List<Polar.Measure> sextant6 = new List<Polar.Measure>();
            sextant6 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("6"));
            sextant6.Sort();
            List<Polar.Measure> sextant1 = new List<Polar.Measure>();
            sextant1 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("1"));
            sextant1.Sort();
            this._MeasureOfTheodolite.Clear();
            this._MeasureOfTheodolite.AddRange(sextant6);
            this._MeasureOfTheodolite.AddRange(sextant1);
        }
        /// <summary>
        /// Trie les mesures du PS ring dans l'ordre correct pour la partie proche de l'origine  
        /// </summary>
        internal void SortMeasForPROrigin()
        {
            List<Polar.Measure> part8 = new List<Polar.Measure>();
            part8 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("8"));
            List<Polar.Measure> part9 = new List<Polar.Measure>();
            part9 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("9"));
            part9.Sort();
            List<Polar.Measure> part00 = new List<Polar.Measure>();
            part00 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("00"));
            part00.Sort();
            List<Polar.Measure> part0 = new List<Polar.Measure>();
            part0 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("01")
            || x._Point._Numero.StartsWith("02")
            || x._Point._Numero.StartsWith("03")
            || x._Point._Numero.StartsWith("04")
            || x._Point._Numero.StartsWith("05")
            || x._Point._Numero.StartsWith("06")
            || x._Point._Numero.StartsWith("07")
            || x._Point._Numero.StartsWith("08")
            || x._Point._Numero.StartsWith("09"));
            part0.Sort();
            List<Polar.Measure> part1 = new List<Polar.Measure>();
            part1 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("1"));
            part1.Sort();
            List<Polar.Measure> mergeList = new List<Polar.Measure>();
            mergeList.AddRange(part8);
            mergeList.AddRange(part9);
            mergeList.AddRange(part00);
            mergeList.AddRange(part0);
            mergeList.AddRange(part1);
            foreach (Polar.Measure item in mergeList)
            {
                this._MeasureOfTheodolite.Remove(item);
            }
            this._MeasureOfTheodolite.AddRange(mergeList);
        }
        /// <summary>
        /// Trie les mesures du Booster ring dans l'ordre correct pour la partie proche de l'origine  
        /// </summary>
        internal void SortMeasForBROrigin()
        {
            List<Polar.Measure> part1 = new List<Polar.Measure>();
            part1 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("1")
            && !x._Point._Numero.Contains("16")
            && !x._Point._Numero.Contains("15")
            && !x._Point._Numero.Contains("14")
            && !x._Point._Numero.Contains("13")
            && !x._Point._Numero.Contains("12")
            && !x._Point._Numero.Contains("11")
            && !x._Point._Numero.Contains("10"));
            part1.Sort();
            List<Polar.Measure> part2 = new List<Polar.Measure>();
            part2 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("2"));
            part2.Sort();
            List<Polar.Measure> part3 = new List<Polar.Measure>();
            part3 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("3"));
            part3.Sort();
            List<Polar.Measure> part4 = new List<Polar.Measure>();
            part4 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("4"));
            part4.Sort();
            List<Polar.Measure> part5 = new List<Polar.Measure>();
            part5 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("5"));
            part5.Sort();
            List<Polar.Measure> part6 = new List<Polar.Measure>();
            part6 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("6"));
            part6.Sort();
            List<Polar.Measure> part7 = new List<Polar.Measure>();
            part7 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("7"));
            part7.Sort();
            List<Polar.Measure> part8 = new List<Polar.Measure>();
            part8 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("8"));
            part8.Sort();
            List<Polar.Measure> part9 = new List<Polar.Measure>();
            part9 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("9"));
            part9.Sort();
            List<Polar.Measure> part10 = new List<Polar.Measure>();
            part10 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("10"));
            part10.Sort();
            List<Polar.Measure> part11 = new List<Polar.Measure>();
            part11 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("11"));
            part11.Sort();
            List<Polar.Measure> part12 = new List<Polar.Measure>();
            part12 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("12"));
            part12.Sort();
            List<Polar.Measure> part13 = new List<Polar.Measure>();
            part13 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("13"));
            part13.Sort();
            List<Polar.Measure> part14 = new List<Polar.Measure>();
            part14 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("14"));
            part14.Sort();
            List<Polar.Measure> part15 = new List<Polar.Measure>();
            part15 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("15"));
            part15.Sort();
            List<Polar.Measure> part16 = new List<Polar.Measure>();
            part16 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("16"));
            part16.Sort();
            List<Polar.Measure> mergeList = new List<Polar.Measure>();
            mergeList.AddRange(part8);
            mergeList.AddRange(part9);
            mergeList.AddRange(part10);
            mergeList.AddRange(part11);
            mergeList.AddRange(part12);
            mergeList.AddRange(part13);
            mergeList.AddRange(part14);
            mergeList.AddRange(part15);
            mergeList.AddRange(part16);
            mergeList.AddRange(part1);
            mergeList.AddRange(part2);
            mergeList.AddRange(part3);
            mergeList.AddRange(part4);
            mergeList.AddRange(part5);
            mergeList.AddRange(part6);
            mergeList.AddRange(part7);
            foreach (Polar.Measure item in mergeList)
            {
                this._MeasureOfTheodolite.Remove(item);
            }
            this._MeasureOfTheodolite.AddRange(mergeList);
        }
        /// <summary>
        /// Met les points de AD ring dans l'ordre
        /// </summary>
        /// <param name="theoPoint"></param>
        internal void SortMeasForDROrigin()
        {
            List<Polar.Measure> part0 = new List<Polar.Measure>();
            part0 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("0"));
            part0.Sort();
            List<Polar.Measure> part1 = new List<Polar.Measure>();
            part1 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("1"));
            part1.Sort();
            List<Polar.Measure> part2 = new List<Polar.Measure>();
            part2 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("2"));
            part2.Sort();
            List<Polar.Measure> part3 = new List<Polar.Measure>();
            part3 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("3"));
            part3.Sort();
            List<Polar.Measure> part4 = new List<Polar.Measure>();
            part4 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("4"));
            part4.Sort();
            List<Polar.Measure> part5 = new List<Polar.Measure>();
            part5 = this._MeasureOfTheodolite.FindAll(x => x._Point._Numero.StartsWith("5"));
            part5.Sort();
            List<Polar.Measure> mergeList = new List<Polar.Measure>();
            mergeList.AddRange(part3);
            mergeList.AddRange(part4);
            mergeList.AddRange(part5);
            mergeList.AddRange(part0);
            mergeList.AddRange(part1);
            mergeList.AddRange(part2);
            foreach (Polar.Measure item in mergeList)
            {
                this._MeasureOfTheodolite.Remove(item);
            }
            this._MeasureOfTheodolite.AddRange(mergeList);
        }
    }
}
