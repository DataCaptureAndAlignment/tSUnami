﻿using System;
using R = TSU.Properties.Resources;
using System.Xml;
using System.Xml.Serialization;
using TSU.Common.Instruments;
using System.Collections.Generic;
using TSU.Common.Elements;
using M = TSU.Common.Measures;
using I = TSU.Common.Instruments;
using E = TSU.Common.Elements;
using System.IO;
using TSU.Common.Instruments.Reflector;


namespace TSU.Length
{
    public partial class Station
    {
        [Serializable]
        [XmlType(TypeName = "Length.Station.Parameters")]
        public class Parameters : Common.Station.Parameters
        //pour les stations de mise en longueur
        {
            [XmlIgnore]
            public new Sensor _Instrument
            {
                get
                {
                    return base._Instrument as Sensor;
                }
                set
                {
                    base._Instrument = value;
                }
            }
            public Reflector _DefaultReflector;
            public bool _DefaultDoubleFace;
            public int _DefaultNumberMeas;
            public virtual double _Tolerance { get; set; } //tolérance pour l'alignement des éléments
            public virtual TSU.ENUM.CoordinatesType _CoordType { get; set; } // type de coordonnée à utiliser pour le calcul du H station
            public virtual Point _StationPoint { get; set; }
            public virtual double HInstrument { get; set; }
            private InstrumentTolerance tolerance;
            public InstrumentTolerance Tolerance //Tolérance pour l'instrument et fermeture
            {
                get
                {
                    if (tolerance == null)
                    {
                        if (this._Instrument != null)
                        {
                            tolerance = TSU.Tsunami2.Preferences.Values.InstrumentsTolerances.Find(x => x.InstrumentType == this._Instrument._InstrumentType);
                            if (tolerance != null)
                                return tolerance;
                        }
                        return new InstrumentTolerance(InstrumentTypes.Unknown, 0, 0, 0, 0, 0, 0);
                    }
                    else
                    {
                        return tolerance;
                    }
                }
                set
                {
                    tolerance = value;
                }
            }
            public virtual DoubleValue Vzero { get; set; }
            public Parameters()
                : base()
            //Constructeurs
            {
                this._DefaultReflector = new Reflector();
                this._DefaultDoubleFace = false;
                this._DefaultNumberMeas = 3;
                this._Date = DateTime.Now;
                this._Team = base._Team;
                this._Operation = base._Operation;
                this._IsSetup = false;
                this._Instrument = new I.Level();
                this._State = new Common.Station.State.Opening();
                this._GeodeDatFilePath = "";
                this._Tolerance =TSU.Tsunami2.Preferences.Values.Tolerances.Alignment_Radial_mm / 1000;
                this._CoordType = ENUM.CoordinatesType.CCS;
                this._StationPoint = new Point();
                this.HInstrument = 0;
                this.Vzero = new DoubleValue();
            }
        }
    }
}
