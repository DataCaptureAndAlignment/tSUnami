﻿
using TSU.Common;
using R = TSU.Properties.Resources;

namespace TSU.Length
{
    public partial class Station
    {
        partial class View
        {
            /// <summary>
            /// Required designer variable.
            /// </summary>
            private System.ComponentModel.IContainer components = null;

            /// <summary>
            /// Clean up any resources being used.
            /// </summary>
            /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }

            #region Windows Form Designer generated code

            /// <summary>
            /// Required method for Designer support - do not modify
            /// the contents of this method with the code editor.
            /// </summary>
            private void InitializeComponent()
            {
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();

                this.splitContainer1 = new System.Windows.Forms.SplitContainer();
                Tsunami2.Preferences.Theme.ApplyTo(splitContainer1);

                this.splitContainerLength = new System.Windows.Forms.SplitContainer();
                Tsunami2.Preferences.Theme.ApplyTo(splitContainerLength);

                this.treeView_Parameters = new System.Windows.Forms.TreeView();
                this.dataGridViewLength = new System.Windows.Forms.DataGridView();
                this.LengthdataGridViewDelete = new System.Windows.Forms.DataGridViewImageColumn();
                this.LengthDataGridviewStation = new System.Windows.Forms.DataGridViewImageColumn();
                this.LeveldataGridViewName = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewCala = new System.Windows.Forms.DataGridViewImageColumn();
                this.Use = new System.Windows.Forms.DataGridViewImageColumn();
                this.LengthdataGridViewCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LengthdataGridViewRall = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LengthdataGridViewDtheo = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LengthDataGridViewGotoMeas = new System.Windows.Forms.DataGridViewImageColumn();
                this.LengthDataGridViewDmes = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LengthdataGridViewDiffDist = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LengthdataGridViewDepla = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LengthDataGridViewNbreMeas = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LengthDataGridViewDoubleFace = new System.Windows.Forms.DataGridViewImageColumn();
                this.LengthdataGridViewReflector = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewComment = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this._PanelBottom.SuspendLayout();
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
                this.splitContainer1.Panel1.SuspendLayout();
                this.splitContainer1.SuspendLayout();
                ((System.ComponentModel.ISupportInitialize)(this.splitContainerLength)).BeginInit();
                this.splitContainerLength.Panel1.SuspendLayout();
                this.splitContainerLength.Panel2.SuspendLayout();
                this.splitContainerLength.SuspendLayout();
                ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLength)).BeginInit();
                this.SuspendLayout();
                // 
                // instrumentView
                // 
                this.instrumentView.Location = new System.Drawing.Point(225, 225);
                // 
                // _PanelBottom
                // 
                this._PanelBottom.Controls.Add(this.splitContainer1);
                this._PanelBottom.Location = new System.Drawing.Point(5, 84);
                this._PanelBottom.MinimumSize = new System.Drawing.Size(0, 120);
                this._PanelBottom.Size = new System.Drawing.Size(1202, 647);
                // 
                // _PanelTop
                // 
                this._PanelTop.Size = new System.Drawing.Size(1202, 79);
                // 
                // splitContainer1
                // 
                this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
                this.splitContainer1.Location = new System.Drawing.Point(0, 0);
                this.splitContainer1.Name = "splitContainer1";
                this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
                // 
                // splitContainer1.Panel1
                // 
                this.splitContainer1.Panel1.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.splitContainer1.Panel1.Controls.Add(this.splitContainerLength);
                // 
                // splitContainer1.Panel2
                // 
                this.splitContainer1.Panel2.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
                this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
                this.splitContainer1.Panel2MinSize = 240;
                this.splitContainer1.Size = new System.Drawing.Size(1202, 647);
                this.splitContainer1.SplitterDistance = 403;
                this.splitContainer1.TabIndex = 1;
                this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
                // 
                // splitContainerLength
                // 
                this.splitContainerLength.Dock = System.Windows.Forms.DockStyle.Fill;
                this.splitContainerLength.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
                this.splitContainerLength.Location = new System.Drawing.Point(0, 0);
                this.splitContainerLength.Name = "splitContainerLength";
                this.splitContainerLength.Orientation = System.Windows.Forms.Orientation.Horizontal;
                // 
                // splitContainerLength.Panel1
                // 
                this.splitContainerLength.Panel1.Controls.Add(this.treeView_Parameters);
                // 
                // splitContainerLength.Panel2
                // 
                this.splitContainerLength.Panel2.Controls.Add(this.dataGridViewLength);
                this.splitContainerLength.Size = new System.Drawing.Size(1202, 403);
                this.splitContainerLength.SplitterDistance = 186;
                this.splitContainerLength.TabIndex = 0;
                // 
                // treeView_Parameters
                // 
                this.treeView_Parameters.AllowDrop = true;
                this.treeView_Parameters.Dock = System.Windows.Forms.DockStyle.Fill;
                this.treeView_Parameters.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.treeView_Parameters.ItemHeight = 32;
                this.treeView_Parameters.Location = new System.Drawing.Point(0, 0);
                this.treeView_Parameters.Name = "treeView_Parameters";
                this.treeView_Parameters.ShowNodeToolTips = true;
                this.treeView_Parameters.Size = new System.Drawing.Size(1202, 186);
                this.treeView_Parameters.TabIndex = 0;
                this.treeView_Parameters.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView_Parameters_NodeMouseClick);
                // 
                // dataGridViewLength
                // 
                this.dataGridViewLength.AllowUserToAddRows = false;
                this.dataGridViewLength.AllowUserToDeleteRows = false;
                this.dataGridViewLength.AllowUserToResizeColumns = true;
                this.dataGridViewLength.AllowUserToResizeRows = false;
                this.dataGridViewLength.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
                dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                dataGridViewCellStyle12.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                dataGridViewCellStyle12.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dataGridViewCellStyle12.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
                dataGridViewCellStyle12.SelectionBackColor = Tsunami2.Preferences.Theme.Colors.TextHighLightBack;
                dataGridViewCellStyle12.SelectionForeColor = Tsunami2.Preferences.Theme.Colors.TextHighLightFore;
                dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                this.dataGridViewLength.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
                this.dataGridViewLength.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
                this.dataGridViewLength.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LengthdataGridViewDelete,
            this.LengthDataGridviewStation,
            this.LeveldataGridViewName,
            this.LeveldataGridViewCala,
            this.Use,
            this.LengthdataGridViewCode,
            this.LengthdataGridViewRall,
            this.LengthdataGridViewDtheo,
            this.LengthDataGridViewGotoMeas,
            this.LengthDataGridViewDmes,
            this.LengthdataGridViewDiffDist,
            this.LengthdataGridViewDepla,
            this.LengthDataGridViewNbreMeas,
            this.LengthDataGridViewDoubleFace,
            this.LengthdataGridViewReflector,
            this.LeveldataGridViewComment,
            this.LeveldataGridViewTime});
                this.dataGridViewLength.Dock = System.Windows.Forms.DockStyle.Fill;
                this.dataGridViewLength.EnableHeadersVisualStyles = false;
                this.dataGridViewLength.GridColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.dataGridViewLength.Location = new System.Drawing.Point(0, 0);
                this.dataGridViewLength.MultiSelect = false;
                this.dataGridViewLength.Name = "dataGridViewLength";
                this.dataGridViewLength.RowHeadersVisible = false;
                this.dataGridViewLength.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                this.dataGridViewLength.RowTemplate.Height = 30;
                this.dataGridViewLength.ShowCellErrors = false;
                this.dataGridViewLength.ShowEditingIcon = false;
                this.dataGridViewLength.ShowRowErrors = false;
                this.dataGridViewLength.Size = new System.Drawing.Size(1202, 213);
                this.dataGridViewLength.TabIndex = 18;
                this.dataGridViewLength.TabStop = false;
                this.dataGridViewLength.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewLength_CellClick);
                this.dataGridViewLength.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewLength_CellValueChanged);
                this.dataGridViewLength.CurrentCellChanged += new System.EventHandler(this.dataGridViewLength_CurrentCellChanged);
                // 
                // LengthdataGridViewDelete
                // 
                this.LengthdataGridViewDelete.HeaderText = "";
                this.LengthdataGridViewDelete.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
                this.LengthdataGridViewDelete.Name = "LengthdataGridViewDelete";
                this.LengthdataGridViewDelete.ReadOnly = true;
                this.LengthdataGridViewDelete.Resizable = System.Windows.Forms.DataGridViewTriState.False;
                this.LengthdataGridViewDelete.Width = 30;
                // 
                // LengthDataGridviewStation
                // 
                this.LengthDataGridviewStation.HeaderText = "Station";
                this.LengthDataGridviewStation.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
                this.LengthDataGridviewStation.Name = "LengthDataGridviewStation";
                this.LengthDataGridviewStation.ReadOnly = true;
                this.LengthDataGridviewStation.Resizable = System.Windows.Forms.DataGridViewTriState.False;
                this.LengthDataGridviewStation.Width = 30;
                // 
                // LeveldataGridViewName
                // 
                dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                this.LeveldataGridViewName.DefaultCellStyle = dataGridViewCellStyle13;
                this.LeveldataGridViewName.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Name;
                this.LeveldataGridViewName.Name = "LeveldataGridViewName";
                this.LeveldataGridViewName.ReadOnly = true;
                this.LeveldataGridViewName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                this.LeveldataGridViewName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
                this.LeveldataGridViewName.Width = 200;
                // 
                // LeveldataGridViewCala
                // 
                this.LeveldataGridViewCala.HeaderText = "R";
                this.LeveldataGridViewCala.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
                this.LeveldataGridViewCala.MinimumWidth = 30;
                this.LeveldataGridViewCala.Name = "LeveldataGridViewCala";
                this.LeveldataGridViewCala.ReadOnly = true;
                this.LeveldataGridViewCala.Width = 30;
                // 
                // Use
                // 
                this.Use.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Use;
                this.Use.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
                this.Use.MinimumWidth = 40;
                this.Use.Name = "Use";
                this.Use.ReadOnly = true;
                this.Use.Resizable = System.Windows.Forms.DataGridViewTriState.False;
                this.Use.Width = 40;
                // 
                // LengthdataGridViewCode
                // 
                dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LengthdataGridViewCode.DefaultCellStyle = dataGridViewCellStyle14;
                this.LengthdataGridViewCode.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_CodeAlesage;
                this.LengthdataGridViewCode.Name = "LengthdataGridViewCode";
                this.LengthdataGridViewCode.ReadOnly = true;
                this.LengthdataGridViewCode.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                this.LengthdataGridViewCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
                // 
                // LengthdataGridViewRall
                // 
                dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LengthdataGridViewRall.DefaultCellStyle = dataGridViewCellStyle15;
                this.LengthdataGridViewRall.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Extension;
                this.LengthdataGridViewRall.Name = "LengthdataGridViewRall";
                this.LengthdataGridViewRall.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
                // 
                // LengthdataGridViewDtheo
                // 
                dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LengthdataGridViewDtheo.DefaultCellStyle = dataGridViewCellStyle16;
                this.LengthdataGridViewDtheo.HeaderText = "DTheo (m)";
                this.LengthdataGridViewDtheo.Name = "LengthdataGridViewDtheo";
                this.LengthdataGridViewDtheo.ReadOnly = true;
                this.LengthdataGridViewDtheo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
                // 
                // LengthDataGridViewGotoMeas
                // 
                this.LengthDataGridViewGotoMeas.HeaderText = "Goto + Meas";
                this.LengthDataGridViewGotoMeas.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
                this.LengthDataGridViewGotoMeas.MinimumWidth = 30;
                this.LengthDataGridViewGotoMeas.Name = "LengthDataGridViewGotoMeas";
                this.LengthDataGridViewGotoMeas.ReadOnly = true;
                this.LengthDataGridViewGotoMeas.Resizable = System.Windows.Forms.DataGridViewTriState.False;
                this.LengthDataGridViewGotoMeas.Width = 30;
                // 
                // LengthDataGridViewDmes
                // 
                dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LengthDataGridViewDmes.DefaultCellStyle = dataGridViewCellStyle17;
                this.LengthDataGridViewDmes.HeaderText = "DMes (m)";
                this.LengthDataGridViewDmes.Name = "LengthDataGridViewDmes";
                this.LengthDataGridViewDmes.ReadOnly = true;
                this.LengthDataGridViewDmes.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
                // 
                // LengthdataGridViewDiffDist
                // 
                dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LengthdataGridViewDiffDist.DefaultCellStyle = dataGridViewCellStyle18;
                this.LengthdataGridViewDiffDist.HeaderText = "Diff (mm)";
                this.LengthdataGridViewDiffDist.Name = "LengthdataGridViewDiffDist";
                this.LengthdataGridViewDiffDist.ReadOnly = true;
                this.LengthdataGridViewDiffDist.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
                // 
                // LengthdataGridViewDepla
                // 
                dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LengthdataGridViewDepla.DefaultCellStyle = dataGridViewCellStyle19;
                this.LengthdataGridViewDepla.HeaderText = "Move (mm)";
                this.LengthdataGridViewDepla.Name = "LengthdataGridViewDepla";
                this.LengthdataGridViewDepla.ReadOnly = true;
                this.LengthdataGridViewDepla.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                this.LengthdataGridViewDepla.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
                // 
                // LengthDataGridViewNbreMeas
                // 
                dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LengthDataGridViewNbreMeas.DefaultCellStyle = dataGridViewCellStyle20;
                this.LengthDataGridViewNbreMeas.HeaderText = "Nbre Meas";
                this.LengthDataGridViewNbreMeas.Name = "LengthDataGridViewNbreMeas";
                this.LengthDataGridViewNbreMeas.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
                this.LengthDataGridViewNbreMeas.Width = 50;
                // 
                // LengthDataGridViewDoubleFace
                // 
                this.LengthDataGridViewDoubleFace.HeaderText = "Double Face";
                this.LengthDataGridViewDoubleFace.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
                this.LengthDataGridViewDoubleFace.MinimumWidth = 30;
                this.LengthDataGridViewDoubleFace.Name = "LengthDataGridViewDoubleFace";
                this.LengthDataGridViewDoubleFace.ReadOnly = true;
                this.LengthDataGridViewDoubleFace.Resizable = System.Windows.Forms.DataGridViewTriState.False;
                this.LengthDataGridViewDoubleFace.Width = 30;
                // 
                // LengthdataGridViewReflector
                // 
                dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LengthdataGridViewReflector.DefaultCellStyle = dataGridViewCellStyle21;
                this.LengthdataGridViewReflector.HeaderText = "Reflector";
                this.LengthdataGridViewReflector.Name = "LengthdataGridViewReflector";
                this.LengthdataGridViewReflector.ReadOnly = true;
                this.LengthdataGridViewReflector.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                this.LengthdataGridViewReflector.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
                this.LengthdataGridViewReflector.Width = 150;
                // 
                // LeveldataGridViewComment
                // 
                this.LeveldataGridViewComment.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Comment;
                this.LeveldataGridViewComment.MaxInputLength = 150;
                this.LeveldataGridViewComment.Name = "LeveldataGridViewComment";
                this.LeveldataGridViewComment.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                this.LeveldataGridViewComment.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
                this.LeveldataGridViewComment.Width = 300;
                // 
                // LeveldataGridViewTime
                // 
                dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LeveldataGridViewTime.DefaultCellStyle = dataGridViewCellStyle22;
                this.LeveldataGridViewTime.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Time;
                this.LeveldataGridViewTime.Name = "LeveldataGridViewTime";
                this.LeveldataGridViewTime.ReadOnly = true;
                this.LeveldataGridViewTime.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                this.LeveldataGridViewTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
                // 
                // StationLengthModuleView
                // 
                this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
                this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
                this.ClientSize = new System.Drawing.Size(1212, 736);
                this.Name = "StationLengthModuleView";
                this.Text = "LevellingModule";
                this.Controls.SetChildIndex(this._PanelTop, 0);
                this.Controls.SetChildIndex(this._PanelBottom, 0);
                this._PanelBottom.ResumeLayout(false);
                this.splitContainer1.Panel1.ResumeLayout(false);
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
                this.splitContainer1.ResumeLayout(false);
                this.splitContainerLength.Panel1.ResumeLayout(false);
                this.splitContainerLength.Panel2.ResumeLayout(false);
                ((System.ComponentModel.ISupportInitialize)(this.splitContainerLength)).EndInit();
                this.splitContainerLength.ResumeLayout(false);
                ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLength)).EndInit();
                this.ResumeLayout(false);

            }

            #endregion

            private System.Windows.Forms.SplitContainer splitContainer1;
            private System.Windows.Forms.SplitContainer splitContainerLength;
            internal System.Windows.Forms.TreeView treeView_Parameters;
            internal System.Windows.Forms.DataGridView dataGridViewLength;
            private System.Windows.Forms.DataGridViewImageColumn LengthdataGridViewDelete;
            private System.Windows.Forms.DataGridViewImageColumn LengthDataGridviewStation;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewName;
            private System.Windows.Forms.DataGridViewImageColumn LeveldataGridViewCala;
            private System.Windows.Forms.DataGridViewImageColumn Use;
            private System.Windows.Forms.DataGridViewTextBoxColumn LengthdataGridViewCode;
            private System.Windows.Forms.DataGridViewTextBoxColumn LengthdataGridViewRall;
            private System.Windows.Forms.DataGridViewTextBoxColumn LengthdataGridViewDtheo;
            private System.Windows.Forms.DataGridViewImageColumn LengthDataGridViewGotoMeas;
            private System.Windows.Forms.DataGridViewTextBoxColumn LengthDataGridViewDmes;
            private System.Windows.Forms.DataGridViewTextBoxColumn LengthdataGridViewDiffDist;
            private System.Windows.Forms.DataGridViewTextBoxColumn LengthdataGridViewDepla;
            private System.Windows.Forms.DataGridViewTextBoxColumn LengthDataGridViewNbreMeas;
            private System.Windows.Forms.DataGridViewImageColumn LengthDataGridViewDoubleFace;
            private System.Windows.Forms.DataGridViewTextBoxColumn LengthdataGridViewReflector;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewComment;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewTime;
        }
    }
}