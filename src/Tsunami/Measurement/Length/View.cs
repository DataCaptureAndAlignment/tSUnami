﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using R = TSU.Properties.Resources;
using System.Windows.Forms;
using TSU.ENUM;
using System.Xml.Serialization;
using TSU.Views;
using TSU.Common.Elements;
using TSU.Common.Instruments;
using TSU.Common.Measures;
using T = TSU.Tools;
using M = TSU;
using System.Globalization;


namespace TSU.Length
{
    public class View : CompositeView
    {
        private Buttons buttons;
        public Module LengthModule
        {
            get
            {
                return this._Module as Module;
            }
        }
        public View(Module parent)
            : base(parent, Orientation.Horizontal, ModuleType.LengthSetting)
        {
            this.Image =R.LengthSetting;
            string creationDate = "(" + R.StringBigButton_CreatedOn + this.Module.CreatedOn.ToString("G", CultureInfo.CreateSpecificCulture("nl-BE")) + ")";
            string buttontext = string.Format("{0} {1};{2}",T.Conversions.StringManipulation.SeparateTitleFromMessage(this._Module.NameAndDescription), creationDate, T.Conversions.StringManipulation.SeparateTitleFromMessage(this._Module.NameAndDescription));
            this.buttons = new Buttons(this);
            this.AddTopButton(buttontext,
                    R.LengthSetting, this.ShowContextMenuGlobal);
            base.AddPlusView(buttons.New);
            this.Visible = true;
            UpdateView();
        }
        private void ShowContextMenuGlobal()
        {
            this.contextButtons.Clear();
            this.contextButtons.Add(buttons.New);
            this.contextButtons.Add(buttons.FinishModule);
            if (this.LengthModule._ActiveStationModule != null)
            {
                Length.Station.Module actualStationLengthModule = this.LengthModule._ActiveStationModule as Length.Station.Module;
                Length.Station.View v = this.LengthModule._ActiveStationModule.View as Length.Station.View;
                if (v != null) v.CheckDatagridIsInEditMode();
                if (actualStationLengthModule._LengthStation._Parameters._Instrument._Model != null)
                {
                    this.contextButtons.Add(buttons.Clone);
                    this.contextButtons.Remove(buttons.New);
                }
            }
            Common.FinalModule m;
            List<BigButton> stationButtons = new List<BigButton>();
            if ((m = this.LengthModule) != null)
            {
                int stationNumber = 0;
                bool enableDeleteButton = true;
                if (this.LengthModule.StationModules.Count > 1)
                {
                    if (this.LengthModule._ActiveStationModule != null)
                    {
                        Length.Station.Module am = this.LengthModule._ActiveStationModule as Length.Station.Module;
                        if (am._LengthStation._MeasureOfTheodolite.FindIndex(x => x.Distance.Raw.Value != TSU.Preferences.Preferences.NotAvailableValueNa) != -1)
                        {
                            enableDeleteButton = false;
                        }
                        if (enableDeleteButton) this.contextButtons.Add(buttons.deleteActualStation);
                    }
                }
                foreach (Station.Module stationModule in m.StationModules)
                {
                    if (stationModule != null)
                    {
                        stationNumber++;
                        BigButton stationButton = new BigButton(
                            string.Format(R.StringLength_BigButtonLengthModuleView, stationNumber, stationModule._Station._Name),
                            this.Image, stationModule.ShowInParentModule);
                        if (stationModule == this.LengthModule._ActiveStationModule)
                        {
                            stationButton.SetColors(M.Tsunami2.Preferences.Theme.Colors.Highlight);
                        }
                        stationButtons.Add(stationButton);
                    }
                }
                //Inverse la liste des stations
                for (int i = stationButtons.Count - 1; i >= 0; i--)
                {
                    this.contextButtons.Add(stationButtons[i]);
                }
            }
            bool ok = this.ShowPopUpMenu(contextButtons);
            // il peut arriver lors de l'ajout d'un nouveau bouton que l'affichage des contrôles dans le popup menu ne soit pas dans l'ordre. Le réaffichage du pop up semble résoudre le problème.
            if (!ok) { this.ShowContextMenuGlobal(); }
        }
        private void OnNewStation(object sender, EventArgs e)
        {
            NewStation();
        }
        public void NewStation()
        {
            this.TryAction(this.LengthModule.NewStation);
        }
        public void Save()
        {
            this.TryAction(()=> { this.LengthModule.Save(); });
        }
        public void CloneActualStation()
        {
            this.TryAction(this.LengthModule.CloneActualStation);
        }
        internal void DeleteActualStation()
        {
            this.TryAction(this.LengthModule.DeleteActualStation);
        }
        private void FinishModule()
        {
            this.TryAction(this.LengthModule.FinishModule);
        }
        /// <summary>
        /// Change le nom de la station active dans le top button
        /// </summary>
        /// <param name="stationLengthModule"></param>
        internal override void SetStationNameInTopButton(Common.Station.Module stationLengthModule)
        {
            this._PanelTop.Controls.Clear();
            string creationDate = "(" + R.StringBigButton_CreatedOn + this.Module.CreatedOn.ToString("G", CultureInfo.CreateSpecificCulture("nl-BE")) + ")";
            string buttontext = string.Format("{0} {1};{2}", T.Conversions.StringManipulation.SeparateTitleFromMessage(this._Module.NameAndDescription), creationDate, T.Conversions.StringManipulation.SeparateMessageFromTitle(this._Module.NameAndDescription));
            Common.FinalModule m;
            if ((m = this.LengthModule) != null)
            {
                int stationNumber = 0;
                foreach (Station.Module stationModule in m.StationModules)
                {
                    if (stationModule != null)
                    {
                        stationNumber++;
                        if (stationModule._Station._Name == stationLengthModule._Station._Name)
                        {
                            buttontext = string.Format("{0} {1}: {2};{3}",
                                T.Conversions.StringManipulation.SeparateTitleFromMessage(this._Module.NameAndDescription),
                                creationDate,
                                string.Format(T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringLength_BigButtonLengthModuleView), stationNumber),
                                string.Format("{0} ({1})", T.Conversions.StringManipulation.SeparateMessageFromTitle(this._Module.NameAndDescription), stationModule._Station._Name));
                        }
                    }
                }
            }
            this.AddTopButton(buttontext, R.LengthSetting, this.ShowContextMenuGlobal);
        }
        class Buttons
        {
            private View view;
            public BigButton New;
            public BigButton Save;
            public BigButton Clone;
            public BigButton deleteActualStation;
            public BigButton FinishModule;
            public Buttons(View view)
            {
                this.view = view;
                New = new BigButton(R.StringLength_NewStation, R.Add, view.NewStation);
                Save = new BigButton(string.Format(R.T_SaveModule, this.view.LengthModule._Name), R.Save, view.Save);
                Clone = new BigButton(R.StringLength_Clone, R.Copy, view.CloneActualStation);
                deleteActualStation = new BigButton(R.StringLength_DeleteActualStation, R.Level_Delete, view.DeleteActualStation);
                FinishModule = new BigButton(R.String_GM_FinishModule, R.FinishHim, view.FinishModule);
            }
        }
    }
}
