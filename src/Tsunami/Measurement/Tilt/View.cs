﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using TSU.ENUM;
using TSU.Views;
using M = TSU;
using R = TSU.Properties.Resources;
using T = TSU.Tools;


namespace TSU.Tilt
{
    public class View : CompositeView
    {
        private Buttons buttons;
        private double na { get; set; } = Tsunami2.Preferences.Values.na;
        public Module TiltModule
        {
            get
            {
                return this._Module as Module;
            }
        }
        public View(Module parent)
            : base(parent, Orientation.Horizontal, ModuleType.Tilt, R.Tilt)
        {
            this.Image = R.Tilt;
            string creationDate = "(" + R.StringBigButton_CreatedOn + this.Module.CreatedOn.ToString("G", CultureInfo.CreateSpecificCulture("nl-BE")) + ")";
            string buttontext = string.Format("{0} {1};{2}", T.Conversions.StringManipulation.SeparateTitleFromMessage(this._Module.NameAndDescription), creationDate, T.Conversions.StringManipulation.SeparateMessageFromTitle(this._Module.NameAndDescription));
            this.buttons = new Buttons(this);
            this.AddTopButton(buttontext,
                    R.Tilt, this.ShowContextMenuGlobal);
            base.AddPlusView(buttons.New);
            this.Visible = true;
            UpdateView();
        }
        /// <summary>
        /// Change le nom de la station active dans le top button
        /// </summary>
        /// <param name="stationLevelModule"></param>
        internal override void SetStationNameInTopButton(Common.Station.Module stationLineModule)
        {
            this._PanelTop.Controls.Clear();
            string creationDate = "(" + R.StringBigButton_CreatedOn + this.Module.CreatedOn.ToString("G", CultureInfo.CreateSpecificCulture("nl-BE")) + ")";
            string buttontext = string.Format("{0} {1};{2}", T.Conversions.StringManipulation.SeparateTitleFromMessage(this._Module.NameAndDescription), creationDate, T.Conversions.StringManipulation.SeparateMessageFromTitle(this._Module.NameAndDescription));
            Common.FinalModule m;
            if ((m = this.TiltModule) != null)
            {
                int stationNumber = 0;
                foreach (Station.Module stationModule in m.StationModules)
                {
                    if (stationModule != null)
                    {
                        stationNumber++;
                        if (stationModule._Station._Name == stationLineModule._Station._Name)
                        {
                            buttontext = string.Format("{0} {1}: {2};{3}",
                                T.Conversions.StringManipulation.SeparateTitleFromMessage(this._Module.NameAndDescription),
                                creationDate,
                                string.Format(T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringTilt_BigButtonTiltModuleView), stationNumber),
                                string.Format("{0} ({1})", T.Conversions.StringManipulation.SeparateMessageFromTitle(this._Module.NameAndDescription), stationModule._Station._Name));
                        }
                    }
                }
            }
            this.AddTopButton(buttontext, R.Tilt, this.ShowContextMenuGlobal);
        }
        private void ShowContextMenuGlobal()
        {
            this.contextButtons.Clear();
            //this.contextButtons.Add(buttons.Open);
            //this.contextButtons.Add(buttons.Save);
            //Les stations modules partagent le même final module ou sont stockés les stations tilts donc il ne sert à rien d'avoir plusieurs stationtilt module dans le même tilt module
            this.contextButtons.Add(buttons.New);
            this.contextButtons.Add(buttons.FinishModule);
            if (this.TiltModule._ActiveStationModule != null)
            {
                Station.View v = this.TiltModule._ActiveStationModule.View as Station.View;
                if (v != null) v.CheckDatagridIsInEditMode();
                if (this.TiltModule._ActiveStationModule._Station.ParametersBasic._Instrument._Model != null)
                {
                    this.contextButtons.Add(buttons.Clone);
                    this.contextButtons.Remove(buttons.New);
                }

            }
            Common.FinalModule m;
            List<BigButton> stationButtons = new List<BigButton>();
            if ((m = this.TiltModule) != null)
            {
                int stationNumber = 0;
                ///Ajoute le bouton de suppression de la station actuelle que s'il n'y a pas de mesures encodées et s'il y a plusieurs stations
                if (this.TiltModule.StationModules.Count > 1)
                {
                    bool addDeleteButton = true;
                    if (this.TiltModule._ActiveStationModule != null)
                    {
                        Station.Module am = this.TiltModule._ActiveStationModule as Station.Module;
                        foreach (Station item in am._StationTiltSequences)
                        {
                            int index = item._MeasuresTilt.FindIndex(x => x._AverageBothDirection.Value != na);
                            if (index != -1) addDeleteButton = false;
                        }
                        if (addDeleteButton) this.contextButtons.Add(buttons.DeleteActualSetOfRoll);
                    }
                }
                foreach (Station.Module stationModule in m.StationModules)
                {
                    if (stationModule != null)
                    {
                        stationNumber++;
                        BigButton stationButton = new BigButton(
                            string.Format(R.StringTilt_BigButtonTiltModuleView, stationNumber, stationModule._Station._Name),
                            this.Image, stationModule.ShowInParentModule);
                        if (stationModule == this.TiltModule._ActiveStationModule)
                        {
                            stationButton.SetColors(M.Tsunami2.Preferences.Theme.Colors.Highlight);
                        }
                        stationButtons.Add(stationButton);
                    }
                }
                //Inverse la liste des stations
                for (int i = stationButtons.Count - 1; i >= 0; i--)
                {
                    this.contextButtons.Add(stationButtons[i]);
                }
            }
            bool ok = this.ShowPopUpMenu(contextButtons);
            // il peut arriver lors de l'ajout d'un nouveau bouton que l'affichage des contrôles dans le popup menu ne soit pas dans l'ordre. Le réaffichage du pop up semble résoudre le problème.
            //if (!ok) { this.ShowContextMenuGlobal(); }
        }
        private void OnNewStation(object sender, EventArgs e)
        {
            NewStation();
        }
        public void NewStation()
        {
            this.TryAction(this.TiltModule.NewStation);
        }
        public void Save()
        {
            this.TryAction(() => this.TiltModule.Save());
        }
        //public void Open()
        //{
        //    this.TryAction(this.TiltModule.Open);
        //}
        public void CloneStation()
        {
            this.TryAction(this.TiltModule.CloneStation);
        }
        private void DeleteActualStation()
        {
            this.TryAction(this.TiltModule.DeleteActualStation);
        }

        private void FinishModule()
        {
            this.TryAction(this.TiltModule.FinishModule);
        }
        class Buttons
        {
            private View view;
            public BigButton New;
            public BigButton Save;
            public BigButton Clone;
            public BigButton DeleteActualSetOfRoll;
            public BigButton FinishModule;
            public Buttons(View view)
            {
                this.view = view;
                Save = new BigButton(string.Format(R.T_SaveModule, this.view.TiltModule._Name), R.Save, view.Save);
                New = new BigButton(R.T074, R.Add, view.NewStation);
                Clone = new BigButton(R.StringTilt_CloneSetOfRoll, R.Copy, view.CloneStation);
                DeleteActualSetOfRoll = new BigButton(R.StringTilt_DeleteActualSetOfRoll, R.Delete_Ancrage, view.DeleteActualStation);
                FinishModule = new BigButton(R.String_GM_FinishModule, R.FinishHim, view.FinishModule);
            }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // _PanelBottom
            // 
            this._PanelBottom.Size = new System.Drawing.Size(738, 168);
            // 
            // PopUpMenu
            // 
            this.PopUpMenu.BackColor = System.Drawing.Color.LightBlue;
            this.PopUpMenu.Location = new System.Drawing.Point(234, 234);
            // 
            // View
            // 
            this.ClientSize = new System.Drawing.Size(748, 244);
            this.Name = "View";
            this.Controls.SetChildIndex(this._PanelTop, 0);
            this.Controls.SetChildIndex(this._PanelBottom, 0);
            this.Controls.SetChildIndex(this.buttonForFocusWithTabIndex0, 0);
            this.ResumeLayout(false);

        }
    }
}
