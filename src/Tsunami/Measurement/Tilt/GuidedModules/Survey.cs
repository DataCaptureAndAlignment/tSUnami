﻿using System.Collections.Generic;
using TSU.Common.Guided.Steps;
using M = TSU;
using R = TSU.Properties.Resources;
using TiltGuidedSteps = TSU.Tilt.GuidedModules.Steps;

namespace TSU.Tilt.GuidedModules
{
    static public class Survey
    {
        static public Common.Guided.Module Get(M.Module parentModule, string nameAndDescription)
        {
            Common.Guided.Module gm = new Common.Guided.Module(parentModule, R.StringGuidedTilt_RollSurvey);
            gm.guideModuleType = ENUM.GuidedModuleType.SurveyTilt;
            gm.ObservationType = Common.ObservationType.Roll;

            // Invisible step: : Add needed station modules
            gm.SwitchToANewStationModule(new Station.Module(gm));
            Station.Module st = gm._ActiveStationModule as Station.Module;
            //gm.Steps.Add(Step.TiltMeter.ShowAdvancedTModule(gm));

            BuildSteps(gm);

            gm.Start();

            return gm;
        }
        /// <summary>
        /// Ajoute les steps nécessaires
        /// </summary>
        /// <param name="gm"></param>
        private static void BuildSteps(Common.Guided.Module gm)
        {
            // Step 0: Presentation
            gm.Steps.Add(Management.Declaration(gm, R.StringGuidedTilt_TiltSurveyPresentation));
            // Step 1: choix administratif
            gm.Steps.Add(TiltGuidedSteps.Measurement.ChooseAllAdministrativeparameters(gm));

            //*************************************************
            ////// Step 1: Choose Theoretical File
            //gm.Steps.Add(Step.Management.ChooseTheoreticalFile(gm));

            ////// Step 2: Choose Management.Instrument
            //gm.Steps.Add(Step.Management.ChooseInstrument(gm));

            ////// Step 3: Choose Team
            //gm.Steps.Add(Step.Management.EnterATeam(gm));

            ////// Step 4: Choose initial Operation
            //gm.Steps.Add(Step.TiltMeter.ChooseOrCreateNewInitOperation(gm));

            ////// Step 5: Choose adjusting Operation this step is optional
            //gm.Steps.Add(Step.TiltMeter.ChooseOrCreateNewAdjOperation(gm));
            //********************************************************

            //// Step 2: Choose Point to measure
            gm.Steps.Add(TiltGuidedSteps.Measurement.ChooseRollMagnets(gm));

            // steps qui sont créé par défaut
            //// Step 3: StationTilt Module mesure initiale
            gm.Steps.Add(TiltGuidedSteps.Measurement.MeasureInitStationTilt(gm));

            //// Step 4: StationTilt Module mesure réglage this step is optional
            gm.Steps.Add(TiltGuidedSteps.Measurement.MeasureAdjStationTilt(gm));

            //// Step 5: show Geode dat file initial operation
            gm.Steps.Add(TiltGuidedSteps.Measurement.ShowIniDatFiles(gm));

            //// Step 6: show Geode dat file initial operation
            gm.Steps.Add(TiltGuidedSteps.Measurement.ShowAdjDatFiles(gm));

            //// Step 7: End step that only close the current module and go back to previous step
            gm.Steps.Add(Management.End(gm));
        }

        /// <summary>
        /// Reconstruit le module guidé lors ouverture sauvegarde
        /// </summary>
        /// <param name="gm"></param>
        static public void ReCreateWhatIsNotSerialized(Common.Guided.Module gm)
        {
            gm.Steps = new List<Step>();
            //gm.currentIndex = 0;
            BuildSteps(gm);
            //gm.Start();
            //gm.MoveToStep(gm.currentIndex);
        }
    }
}