﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using TSU.Common.Guided.Steps;
using TSU.Views;
using TSU.Views.Message;
using C = TSU.Common.Elements.Composites;
using E = TSU.Common.Elements;
using F = System.Windows.Forms;
using I = TSU.Common.Instruments;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Tilt.GuidedModules.Steps
{
    public static class Measurement
    {

        /// <summary>
        /// Allows to select an operation or to create new operation
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        static public Step ChooseOrCreateNewInitOperation(Module guidedModule)
        {
            Step s = new Step(guidedModule, R.SNAD_T_CRION);

            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };

            // Entering() is triggered by Enter()
            s.Entering += delegate (object source, StepEventArgs e)
            {
                //e.step.Update();
                Station.Module stationTiltModule = e.ActiveStationModule as Station.Module;
                // put the team selected in previous step in init station tilt and adjusting station tilt
                stationTiltModule.ChangeTeam(stationTiltModule._ActualStationTilt.ParametersBasic._Team);
                stationTiltModule._ActualStationTilt = (e.ActiveStationModule as Station.Module)._InitStationTilt;
                e.step.View.AddControl(e.guidedModule.OperationManager);
                //e.OperationManager.OperationTree.ExpandAll();
                e.OperationManager._SelectedObjects.Clear();
                O.Operation operation = e.OperationManager.AllElements.Find(x => x._Name == (e.ActiveStationModule as Station.Module)._InitStationTilt.ParametersBasic._Operation._Name) as O.Operation;
                e.OperationManager._SelectedObjectInBlue = operation;
                e.OperationManager.AddSelectedObjects(operation);
                e.OperationManager.UpdateView();

            };

            // Changed() is triggered by Change()
            s.ControlChanged += delegate (object source, StepEventArgs e)
            {
                e.ActiveStationModule.ChangeOperationID(e.OperationManager.SelectedOperation);
            };

            // Updating() is triggered by Update()
            s.Updating += delegate (object source, StepEventArgs e)
            {
                O.Operation o = (e.ActiveStationModule as Station.Module)._InitStationTilt.ParametersBasic._Operation;
                //e.step.View.ModifyLabel(string.Format(R.String_GuidedTilt_InitOPSelected, o.value.ToString(), o.Description), "opDescription");
            };

            // Testing() is triggered by Test()
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                e.step.CheckValidityOf((e.ActiveStationModule as Station.Module)._InitStationTilt.ParametersBasic._Operation.IsSet);
            };

            // Leaving() is triggered by Leave()
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                //Important because it will unsuscri to event Changed() of manager
                e.step.View.RemoveControl(e.guidedModule.OperationManager);
            };

            return s;
        }
        static public Step ChooseAllAdministrativeparameters(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.String_GM_AdministrativeParameters);
            BigButton buttonSelectTheoElementFile = new BigButton();
            BigButton buttonSelectInitialOperation = new BigButton();
            BigButton buttonCreateInitialOperation = new BigButton();
            BigButton buttonSelectAdjustingOperation = new BigButton();
            BigButton buttonCreateAdjustingOperation = new BigButton();
            BigButton buttonSelectTiltmeter = new BigButton();
            F.TextBox textBoxTeam = new F.TextBox();
            Font TitleFont = TSU.Tsunami2.Preferences.Theme.Fonts.Normal;
            string initialOperationName = "...";
            string adjustingOperationName = "...";
            string team = "";
            string instrumentName = "";
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                Station.Module stationTiltModule = e.ActiveStationModule as Station.Module;
                // add a button to directly browse for theoretical file
                e.step.View.AddButton(
                    string.Format(R.String_GM_SelectTheoElementFile, "..."),
                    R.Open,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        gm._ElementManager.MultiSelection = false;
                        gm._ElementManager.View.CheckBoxesVisible = false;
                        gm._ElementManager.SetSelectableToAllTheoreticalPoints();
                        gm._ElementManager.View.buttons.HideAllButtons();
                        gm._ElementManager.View.TryOpen();
                        s.Test();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectTheoElementFile = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectTheoElementFile.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Bad);
                // add button to select instrument
                e.step.View.AddButton(
                    string.Format(R.String_GM_SelectInstrument, instrumentName),
                    R.Tilt,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        stationTiltModule.SelectionTiltMeter();
                        s.Test();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectTiltmeter = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectTiltmeter.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Bad);
                // add a button to select initial operation
                e.step.View.AddButton(
                    string.Format(R.String_GM_SelectInitialOperation, initialOperationName),
                    R.Operation,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        stationTiltModule._ActualStationTilt = (e.ActiveStationModule as Station.Module)._InitStationTilt;
                        O.Operation operation = e.OperationManager.AllElements.Find(x => x._Name == stationTiltModule._InitStationTilt.ParametersBasic._Operation._Name) as O.Operation;
                        e.OperationManager._SelectedObjects.Clear();
                        e.OperationManager.AddSelectedObjects(operation);
                        e.OperationManager.View.currentStrategy.ShowSelectionInButton();
                        e.OperationManager._SelectedObjectInBlue = operation;
                        e.OperationManager.UpdateView();
                        operation = e.OperationManager.SelectOperation();
                        if (operation != null)
                        {
                            stationTiltModule.ChangeOperationID(operation);
                        }
                        s.Test();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectInitialOperation = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectInitialOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Attention);
                // add a button to create initial operation
                e.step.View.AddButton(
                    string.Format(R.String_GM_CreateInitialOperation, initialOperationName),
                    R.Operation_Add,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        stationTiltModule._ActualStationTilt = (e.ActiveStationModule as Station.Module)._InitStationTilt;
                        e.OperationManager.View.AddNew();
                        O.Operation operation = e.OperationManager.SelectedOperation;
                        if (operation != null)
                        {
                            stationTiltModule.ChangeOperationID(operation);
                        }
                        s.Test();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonCreateInitialOperation = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonCreateInitialOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Attention);
                // add a button to select adjusting operation
                e.step.View.AddButton(
                    string.Format(R.String_GM_SelectAdjustingOperation, adjustingOperationName),
                    R.Operation,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        stationTiltModule._ActualStationTilt = (e.ActiveStationModule as Station.Module)._AdjustingStationTilt;
                        O.Operation operation = e.OperationManager.AllElements.Find(x => x._Name == stationTiltModule._AdjustingStationTilt.ParametersBasic._Operation._Name) as O.Operation;
                        e.OperationManager._SelectedObjects.Clear();
                        e.OperationManager.AddSelectedObjects(operation);
                        e.OperationManager.View.currentStrategy.ShowSelectionInButton();
                        e.OperationManager._SelectedObjectInBlue = operation;
                        e.OperationManager.UpdateView();
                        operation = e.OperationManager.SelectOperation();
                        if (operation != null)
                        {
                            stationTiltModule.ChangeOperationID(operation);
                        }
                        s.Test();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectAdjustingOperation = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectAdjustingOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Attention);
                // add a button to create operation
                e.step.View.AddButton(
                    string.Format(R.String_GM_CreateAdjustingOperation, adjustingOperationName),
                    R.Operation_Add,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        stationTiltModule._ActualStationTilt = (e.ActiveStationModule as Station.Module)._AdjustingStationTilt;
                        e.OperationManager.View.AddNew();
                        O.Operation operation = e.OperationManager.SelectedOperation;
                        if (operation != null)
                        {
                            stationTiltModule.ChangeOperationID(operation);
                        }
                        s.Test();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonCreateAdjustingOperation = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonCreateAdjustingOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Attention);
                /// ajout nom de l'équipe
                e.step.View.AddTitle(R.String_GM_TeamLabel, "teamLabel");
                e.step.View.AddTextBox(guidedModule._TsuView, team);
                textBoxTeam = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as F.TextBox;
                textBoxTeam.BackColor = Tsunami2.Preferences.Theme.Colors.Bad;
                textBoxTeam.Enter += delegate
                {
                    textBoxTeam.SelectAll();
                };
                s.Test();
            };
            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                if (st._Station.ParametersBasic._Instrument != null)
                {
                    e.InstrumentManager._SelectedObjects.Clear();
                    instrumentName = st._Station.ParametersBasic._Instrument._Name;
                    I.Sensor instrument = e.InstrumentManager.AllElements.Find(x => x._Name == st._Station.ParametersBasic._Instrument._Name) as I.Sensor;
                    e.InstrumentManager.AddSelectedObjects(instrument);
                    e.InstrumentManager.View.currentStrategy.ShowSelectionInButton();
                    e.InstrumentManager._SelectedObjectInBlue = instrument;
                    e.InstrumentManager.UpdateView();
                }
                if (e.ActiveStationModule._Station.ParametersBasic._Team != R.String_UnknownTeam)
                {
                    team = e.ActiveStationModule._Station.ParametersBasic._Team;
                    textBoxTeam.Text = team;
                }
                else
                {
                    textBoxTeam.Text = "";
                }
                initialOperationName = st._InitStationTilt.ParametersBasic._Operation.ToString();
                adjustingOperationName = st._AdjustingStationTilt.ParametersBasic._Operation.ToString();
                textBoxTeam.Focus();
                textBoxTeam.SelectAll();
                textBoxTeam.Focus();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = false;
                bool stepValid = true;
                bool stepIsSkipable = false;
                string msgBlocked = R.String_GM_MsgBlockAdministrative;
                Station.Module st = e.ActiveStationModule as Station.Module;
                ///Vérification si fichier theo sélectionné
                if (e.ElementManager.GetAllTheoreticalPoints().Count > 0)
                {
                    buttonSelectTheoElementFile.SetColors(Tsunami2.Preferences.Theme.Colors.Good);
                    buttonSelectTheoElementFile.ChangeImage(R.StatusGood);
                    buttonSelectTheoElementFile.ChangeNameAndDescription(string.Format(R.String_GM_SelectTheoElementFile, (e.ElementManager.AllElements[0] as E.Element)._Origin));
                }
                else
                {
                    stepValid = false;
                    msgBlocked += " " + R.String_GM_MsgBlockTheoFile;
                }
                ///Vérification si instrument sélectionné
                if (e.InstrumentManager._SelectedObjects.Count == 1)
                {
                    I.Instrument instrument = e.InstrumentManager._SelectedObjects[0] as I.Instrument;
                    if (instrument._Model != null)
                    {
                        buttonSelectTiltmeter.SetColors(Tsunami2.Preferences.Theme.Colors.Good);
                        buttonSelectTiltmeter.ChangeImage(R.StatusGood);
                        if (st._ActualStationTilt.ParametersBasic._Instrument != null) instrumentName = st._ActualStationTilt.ParametersBasic._Instrument._Name;
                        buttonSelectTiltmeter.ChangeNameAndDescription(string.Format(R.String_GM_SelectInstrument, instrumentName));
                    }
                    else
                    {
                        stepValid = false;
                        if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                        msgBlocked += " " + R.String_GM_MsgBlockInstrument;
                    }
                }
                else
                {
                    if (st._ActualStationTilt.ParametersBasic._Instrument == null)
                    {
                        stepValid = false;
                        if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                        msgBlocked += " " + R.String_GM_MsgBlockInstrument;
                    }
                    else
                    {
                        if (st._ActualStationTilt.ParametersBasic._Instrument._Name == R.String_Unknown)
                        {
                            stepValid = false;
                            if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                            msgBlocked += " " + R.String_GM_MsgBlockInstrument;
                        }
                    }
                }
                //Vérification si opération initial sélectionnée, si 0 met bouton en red
                if (st._InitStationTilt.ParametersBasic._Operation != null)
                {
                    if (st._InitStationTilt.ParametersBasic._Operation.IsSet)// && (st._InitStationTilt.ParametersBasic._Operation.value != 0))
                    {
                        buttonSelectInitialOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Good);
                        buttonSelectInitialOperation.ChangeImage(R.StatusGood);
                        initialOperationName = st._InitStationTilt.ParametersBasic._Operation._Name;
                        buttonSelectInitialOperation.ChangeNameAndDescription(string.Format(R.String_GM_SelectInitialOperation, initialOperationName));
                        buttonCreateInitialOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Good);
                        buttonCreateInitialOperation.ChangeImage(R.StatusGood);
                        buttonCreateInitialOperation.ChangeNameAndDescription(string.Format(R.String_GM_CreateInitialOperation, initialOperationName));
                    }
                    else
                    {
                        if (!st._InitStationTilt.ParametersBasic._Operation.IsSet)// || st._InitStationTilt.ParametersBasic._Operation.value == 0)
                        {
                            buttonSelectInitialOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Attention);
                            buttonSelectInitialOperation.ChangeImage(R.Operation);
                            initialOperationName = st._InitStationTilt.ParametersBasic._Operation.ToString();
                            buttonSelectInitialOperation.ChangeNameAndDescription(string.Format(R.String_GM_SelectInitialOperation, initialOperationName));
                            buttonCreateInitialOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Attention);
                            buttonCreateInitialOperation.ChangeImage(R.Operation_Add);
                            buttonCreateInitialOperation.ChangeNameAndDescription(string.Format(R.String_GM_CreateInitialOperation, initialOperationName));
                            stepIsSkipable = true;
                        }
                    }
                }
                //Vérification si opération réglage sélectionnée, si 0 met bouton en red
                if (st._AdjustingStationTilt.ParametersBasic._Operation != null)
                {
                    if (st._AdjustingStationTilt.ParametersBasic._Operation.IsSet)//&& (st._AdjustingStationTilt.ParametersBasic._Operation.value != 0))
                    {
                        buttonSelectAdjustingOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Good);
                        buttonSelectAdjustingOperation.ChangeImage(R.StatusGood);
                        adjustingOperationName = st._AdjustingStationTilt.ParametersBasic._Operation.ToString();
                        buttonSelectAdjustingOperation.ChangeNameAndDescription(string.Format(R.String_GM_SelectAdjustingOperation, adjustingOperationName));
                        buttonCreateAdjustingOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Good);
                        buttonCreateAdjustingOperation.ChangeImage(R.StatusGood);
                        buttonCreateAdjustingOperation.ChangeNameAndDescription(string.Format(R.String_GM_CreateAdjustingOperation, adjustingOperationName));
                    }
                    else
                    {
                        if (!st._AdjustingStationTilt.ParametersBasic._Operation.IsSet) // || st._AdjustingStationTilt.ParametersBasic._Operation.value == 0)
                        {
                            buttonSelectAdjustingOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Attention);
                            buttonSelectAdjustingOperation.ChangeImage(R.Operation);
                            adjustingOperationName = st._AdjustingStationTilt.ParametersBasic._Operation.ToString();
                            buttonSelectAdjustingOperation.ChangeNameAndDescription(string.Format(R.String_GM_SelectAdjustingOperation, adjustingOperationName));
                            buttonCreateAdjustingOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Attention);
                            buttonCreateAdjustingOperation.ChangeImage(R.Operation_Add);
                            buttonCreateAdjustingOperation.ChangeNameAndDescription(string.Format(R.String_GM_CreateAdjustingOperation, adjustingOperationName));
                            stepIsSkipable = true;
                        }
                    }
                }
                //Vérifie que le nom d'équipe contient que des caractères alphanumériques et est de maximum 8 caractères
                string tempTeam = textBoxTeam.Text;
                if (System.Text.RegularExpressions.Regex.IsMatch(tempTeam, @"^[a-zA-Z]+$") && (tempTeam.Length < 9 || tempTeam == R.String_UnknownTeam))
                {
                    tempTeam = tempTeam.ToUpper();
                    e.ActiveStationModule._Station.ParametersBasic._Team = tempTeam;
                    team = e.ActiveStationModule._Station.ParametersBasic._Team;
                    textBoxTeam.BackColor = Tsunami2.Preferences.Theme.Colors.Good;
                }
                else
                {
                    stepValid = false;
                    textBoxTeam.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
                    if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                    msgBlocked += " " + R.String_GM_MsgBlockTeam;
                }
                if (stepValid && stepIsSkipable)
                {
                    e.step.skipable = true;
                    stepValid = false;
                }
                e.guidedModule.CurrentStep.MessageFromTest = msgBlocked;
                e.step.CheckValidityOf(stepValid);
                e.step.Update();
            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                if (st._AdjustingStationTilt.ParametersBasic._Operation != null)
                {
                    if (!st._AdjustingStationTilt.ParametersBasic._Operation.IsSet)
                    {
                        new MessageInput(MessageType.GoodNews, R.StringGuidedTilt_SkipAdjOP).Show();
                    }

                    (e.ActiveStationModule as Station.Module).View.showAdjustingOp = false;
                }
                //Affiche ou cache les steps de mesures de réglage tilt si pas d'opération choisie
                Station.Module stationTiltModule = e.ActiveStationModule as Station.Module;
                if (!stationTiltModule._AdjustingStationTilt.ParametersBasic._Operation.IsSet)
                {
                    foreach (Step step in e.guidedModule.Steps)
                    {
                        if (step._Name == R.StringGuidedTilt_AdjustingRollMeas)
                        {
                            step.Visible = false;
                        }
                        if (step._Name == T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringTilt_OpenAdjDatFile))
                        {
                            step.Visible = false;
                        }
                    }
                }
                else
                {
                    foreach (Step step in e.guidedModule.Steps)
                    {
                        if (step._Name == R.StringGuidedTilt_AdjustingRollMeas)
                        {
                            step.Visible = true;
                        }
                        if (step._Name == T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringTilt_OpenAdjDatFile))
                        {
                            step.Visible = true;
                        }
                    }
                }
            };
            return s;
        }
        static public Step ChooseOrCreateNewAdjOperation(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.SNAD_T_CROON);

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                (e.ActiveStationModule as Station.Module)._ActualStationTilt = (e.ActiveStationModule as Station.Module)._AdjustingStationTilt;
                e.step.View.AddControl(e.guidedModule.OperationManager);
                //e.OperationManager.OperationTree.ExpandAll();
                e.OperationManager._SelectedObjects = new List<TsuObject>() { (e.ActiveStationModule as Station.Module)._AdjustingStationTilt.ParametersBasic._Operation };
                e.OperationManager._SelectedObjects.Clear();
                O.Operation operation = e.OperationManager.AllElements.Find(x => x._Name == (e.ActiveStationModule as Station.Module)._AdjustingStationTilt.ParametersBasic._Operation._Name) as O.Operation;
                e.OperationManager._SelectedObjectInBlue = operation;
                e.OperationManager.AddSelectedObjects(operation);
                e.OperationManager.UpdateView();
            };

            s.ControlChanged += delegate (object source, StepEventArgs e)
            {
                //e.OperationManager.ValidateSelection();
                e.ActiveStationModule.ChangeOperationID(e.OperationManager.SelectedOperation);
            };

            // Testing() is triggered by Test()
            s.Testing += delegate (object source, StepEventArgs e)
            {

                //(e.ActiveStationModule as Tilt.Station.Module)._AdjustingStationTilt.ParametersBasic._Operation = e.OperationManager.SelectedOperation;
                e.step.skipable = true;
                e.step.CheckValidityOf((e.ActiveStationModule as Station.Module)._AdjustingStationTilt.ParametersBasic._Operation.IsSet);
            };

            // Updating() is triggered by Update()
            s.Updating += delegate (object source, StepEventArgs e)
            {
                O.Operation o = (e.ActiveStationModule as Station.Module)._AdjustingStationTilt.ParametersBasic._Operation;
                //e.step.View.ModifyLabel(string.Format(R.String_GuidedTilt_AdjOPSelected, o.value.ToString(), o.Description), "opDescription");
            };

            // Leaving() is triggered by Leave()
            s.Leaving += delegate (object source, StepEventArgs e)
            {

                if (e.step.State == StepState.ReadyToSkip)
                {
                    new MessageInput(MessageType.GoodNews, R.StringGuidedTilt_SkipAdjOP).Show();
                    (e.ActiveStationModule as Station.Module).View.showAdjustingOp = false;

                }
                //Affiche ou cache les steps de mesures de réglage tilt si pas d'opération choisie
                Station.Module stationTiltModule = e.ActiveStationModule as Station.Module;
                if (!stationTiltModule._AdjustingStationTilt.ParametersBasic._Operation.IsSet)
                {
                    foreach (Step step in e.guidedModule.Steps)
                    {
                        if (step._Name == R.StringGuidedTilt_AdjustingRollMeas)
                        {
                            step.Visible = false;
                        }
                        if (step._Name == T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringTilt_OpenAdjDatFile))
                        {
                            step.Visible = false;
                        }
                    }
                }
                else
                {
                    foreach (Step step in e.guidedModule.Steps)
                    {
                        if (step._Name == R.StringGuidedTilt_AdjustingRollMeas)
                        {
                            step.Visible = true;
                        }
                        if (step._Name == T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringTilt_OpenAdjDatFile))
                        {
                            step.Visible = true;
                        }
                    }
                }

                //Important because it will unsuscribe to event Changed() of manager
                e.step.View.RemoveControl(e.guidedModule.OperationManager);
            };

            return s;
        }
        /// <summary>
        /// Mesure de la station tilt initiale
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step MeasureInitStationTilt(Common.Guided.Module guidedModule, string theoPointName = "")
        {
            Step s = new Step(guidedModule, string.Format("{0}", R.StringGuidedTilt_InitialRollMeas));
            bool firstUse = true;
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                //Pour les mesures initiales pendant un survey tilt met la tolérance à 0.1 mrad
                st.ChangeTolerance(0.0001);
            };
            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                st.View.moduleType = ENUM.ModuleType.Guided;
                ///Vérifie si ce ne sont pas des dipoles SPS
                //if (st.FinalModule.ElementsTobeMeasured.FindIndex(x=>x._Accelerator=="SPS" && (x._Class=="MBA"||x._Class=="MBB"))==-1)
                //{
                //    st.View.ChangeViewType(ENUM.TiltViewType.DataGridView);
                //}
                //else
                //{
                //    st.View.ChangeViewType(ENUM.TiltViewType.Treeview);
                //    st.View.HideInstrumentView();
                //}
                //Met d'office un datagridview et demandera d'encoder toutes les mesures de tilt à chaque fois
                st.View.ChangeViewType(ENUM.TiltViewType.DataGridView);
                st.View.showAdmin = false;
                st.View.showAdjustingOp = false;
                //st.View.showElement = true;
                st.View.showInitOp = true;
                st._ActualStationTilt = st._InitStationTilt;
                //st.UpdateView();
                e.step.View.AddControl(st);
                //e.step.Update();
                //e.guidedModule.UpdateView();
            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                e.step.Test();
                //To set the scroll bar to left and at the top
                Station.Module st = e.ActiveStationModule as Station.Module;
                if (firstUse) st.View.SetScrollBarAtZero();
                firstUse = false;
            };
            s.Changed += delegate (object source, StepEventArgs e)
            {
                //Tilt.Station.Module st = e.ActiveStationModule as Tilt.Station.Module;
                //st.View.Timer_Keep_Cell_Focus.Enabled = true;
                //st.View.Timer_Keep_Cell_Focus.Start();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                //Guided.Module gm = e.guidedModule;
                Station.Module st = e.ActiveStationModule as Station.Module;
                e.step.skipable = true;
                Station.Module stationTiltModule = e.ActiveStationModule as Station.Module;
                e.step.CheckValidityOf(stationTiltModule._ActualStationTilt.TiltParameters._State is Common.Station.State.StationTiltSaved);
                st.View.SetCurrentCell();
                //e.guidedModule.View.UpdateNextButtonColor();
                //e.step.CheckValidityOf(readyToBeSaved);
            };
            //Leaving() is triggered by Leave()
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                if (e.step.View != null)
                {
                    Station.Module m = e.ActiveStationModule as Station.Module;
                    foreach (Station station in m._StationTiltSequences)
                    {
                        if (station.TiltParameters._TiltInitOrAdjusting == ENUM.TiltInitOrAdjusting.Initial && station.TiltParameters._State is Common.Station.State.StationTiltReadyToBeSaved)
                        {
                            string save = R.T_SAVE;
                            string titleAndMessage = string.Format(R.StringGuidedTilt_ExportMeasure, station.TheoPoint._ClassAndNumero);
                            MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                            {
                                ButtonTexts = new List<string> { save, R.T_DONT }
                            };
                            string respond = mi.Show().TextOfButtonClicked;
                            if (respond == save)
                            {
                                m.ChangePointTheo(station.TheoPoint._Name);
                                m._ActualStationTilt = m._InitStationTilt;
                                m.ExportToGeode(false, true);
                            }
                        }
                    }
                }
            };
            return s;
        }
        /// <summary>
        /// Step special pour l'alignement d'un élément en forçant la tolérance à 0.05mrad
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step MeasureTiltForElementAlignment(Common.Guided.Module guidedModule, string theoPointName = "")
        {
            //Step s = S.TiltMeter.MeasureInitStationTilt(guidedModule, String.Format("{0}", R.StringGuidedTilt_InitialRollMeas));
            Step s = MeasureAdjStationTilt(guidedModule, string.Format("{0}", R.StringGuidedTilt_AdjustingRollMeas));
            s._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringGuidedTilt_AdjustingRollMeas);
            s.Utility = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringGuidedTilt_AdjustingRollMeas);
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                //Pour les mesures de réglage après alignement met la tolérance à 0.05 mrad
                st.ChangeTolerance(0.00005);
            };
            s.Entering += delegate (object source, StepEventArgs e)
            {

            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                Common.Guided.Group.Module GrGm = e.guidedModule.ParentModule as Common.Guided.Group.Module;
                // L'admin module est toujours le premier dans la liste des guided modules lors d'un alignement d'aimant
                // Le step 5 est step avec les résultats d'alignement qu'il faut mettre à jour
                if (GrGm.SubGuidedModules[0].currentIndex == 3)
                {
                    GrGm.SubGuidedModules[0].CurrentStep.Update();
                }
            };
            return s;
        }
        //mesure de la station tilt de réglage
        internal static Step MeasureAdjStationTilt(Common.Guided.Module guidedModule, string theoPointName = "")
        {
            Step s = new Step(guidedModule, string.Format("{0}", R.StringGuidedTilt_AdjustingRollMeas));
            bool firstUse = true;
            //string actualPointName = "";
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                //Pour les mesures de réglage après alignement met la tolérance à 0.05 mrad
                st.ChangeTolerance(0.00005);
            };
            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                st._ActualStationTilt = st._AdjustingStationTilt;
                st.View.moduleType = ENUM.ModuleType.Guided;
                /////Vérifie si ce ne sont pas des dipoles SPS
                //if (st.FinalModule.ElementsTobeMeasured.FindIndex(x => x._Accelerator == "SPS" && (x._Class == "MBA" || x._Class == "MBB")) == -1)
                //{
                //    st.View.ChangeViewType(ENUM.TiltViewType.DataGridView);
                //}
                //else
                //{
                //    st.View.ChangeViewType(ENUM.TiltViewType.Treeview);
                //    st.View.HideInstrumentView();
                //}
                st.View.ChangeViewType(ENUM.TiltViewType.DataGridView);
                st.View.showAdmin = false;
                st.View.showAdjustingOp = true;
                //st.View.showElement = true;
                st.View.showInitOp = false;
                st.View.UpdateView();
                //actualPointName = st._ActualStationTilt.TheoPoint._Name;
                //st.UpdateView();
                e.step.View.AddControl(st);
                //e.step.Update();
            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                e.step.Test();
                //To set the scroll bar to left and at the top
                Station.Module st = e.ActiveStationModule as Station.Module;
                if (firstUse) st.View.SetScrollBarAtZero();
                firstUse = false;
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                Station.Module stationTiltModule = e.ActiveStationModule as Station.Module;
                e.step.skipable = true;
                e.step.CheckValidityOf(stationTiltModule._AdjustingStationTilt.TiltParameters._State is Common.Station.State.StationTiltSaved);
            };
            s.Changed += delegate (object source, StepEventArgs e)
            {
                //Tilt.Station.Module st = e.ActiveStationModule as Tilt.Station.Module;
                //st.View.Timer_Keep_Cell_Focus.Enabled = true;
                //st.View.Timer_Keep_Cell_Focus.Start();
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {

                Station.Module m = e.ActiveStationModule as Station.Module;
                foreach (Station station in m._StationTiltSequences)
                {
                    if (station.TiltParameters._TiltInitOrAdjusting == ENUM.TiltInitOrAdjusting.Adjusting && station.TiltParameters._State is Common.Station.State.StationTiltReadyToBeSaved)
                    {
                        string save = R.T_SAVE;
                        string titleAndMessage = string.Format(R.StringGuidedTilt_ExportMeasure, station.TheoPoint._ClassAndNumero);
                        MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                        {
                            ButtonTexts = new List<string> { save, R.T_DONT }
                        };
                        string respond = mi.Show().TextOfButtonClicked;
                        if (respond == save)
                        {
                            m.ChangePointTheo(station.TheoPoint._Name);
                            m._ActualStationTilt = m._ActualStationTilt;
                            m.ExportToGeode(false, true);
                        }
                    }
                }
            };
            return s;
        }
        // choisit les aimants dans l'element manager et crèe le nombre de steps nécessaire.
        internal static Step ChooseRollMagnets(Common.Guided.Module guidedModule)
        {
            Step s = Management.ChooseElement(guidedModule);

            s._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.SNAD_CM);
            s.Utility = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.SNAD_CM);

            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.ElementManager._SelectedObjects.Clear();
                e.ElementManager.View.CheckBoxesVisible = true;
                e.ElementManager.MultiSelection = true;
                e.ElementManager.SetSelectableToAllMagnets();
                e.ElementManager.View.buttons.HideOnlyButtons(e.ElementManager.View.buttons.sequence);
                Station.Module st = e.ActiveStationModule as Station.Module;
                st.SetSelectedMagnetInElementModule();
                e.step.View.AddControl(e.guidedModule._ElementManager);
                e.ElementManager.UpdateView();
                e.ElementManager.View.buttons.ShowTypes.Available = true;
                e.ElementManager.View.buttons.ShowPiliers.Available = false;
                e.ElementManager.View.buttons.ShowPoints.Available = false;
                e.ElementManager.View.buttons.ShowMagnets.Available = true;
                e.ElementManager.View.buttons.ShowAll.Available = false;
                e.ElementManager.View.buttons.ShowAlesages.Available = true;
                e.ElementManager.View.buttons.clean.Available = false;
                e.ElementManager.View.buttons.importRefenceFile.Available = true;
                e.ElementManager.View.buttons.add.Available = false;
            };
            s.ControlChanged += delegate (object source, StepEventArgs e)
            {

            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                ///ne passe au step suivant que si on a sélectionné que des aimants
                e.guidedModule.CurrentStep.MessageFromTest = R.String_GM_MsgBlockMagnet;
                if (e.ElementManager._SelectedObjects.Count > 0)
                {
                    if (e.ElementManager._SelectedObjects.FindIndex(x => !(x is C.Magnet)) != -1)
                    {
                        e.step.CheckValidityOf(false);
                    }
                }
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                e.ElementManager.ValidateSelection();
                e.step.View.RemoveControl(e.ElementManager);
            };

            return s;
        }
        internal static Step ShowIniDatFiles(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.StringTilt_OpenInitDatFile);
            s._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringTilt_OpenInitDatFile);
            s.Utility = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringTilt_OpenInitDatFile);
            Process process = null;
            BigButton buttonOpen = new BigButton();
            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                if (process == null)
                {
                    if (st._InitStationTilt.ParametersBasic._GeodeDatFilePath != "")
                    {
                        if (System.IO.File.Exists(st._InitStationTilt.ParametersBasic._GeodeDatFilePath))
                        {
                            buttonOpen = e.step.View.AddButton(
                                R.String_GM_OpendatFile,
                                R.Open,
                                delegate (object source2, TsuObjectEventArgs args2)
                                {
                                    Shell.Run(st._InitStationTilt.ParametersBasic._GeodeDatFilePath);
                                }
                            );
                            process = s.View.AddShowDatFilePanel(st._InitStationTilt.ParametersBasic._GeodeDatFilePath);
                        }
                        else
                        {
                            new MessageInput(MessageType.Warning, R.StringTilt_MsgNoInitDatFile).Show();
                        }
                    }
                    else
                    {
                        new MessageInput(MessageType.Warning, R.StringTilt_MsgNoInitDatFile).Show();
                    }
                }

            };
            s.Updating += delegate (object source, StepEventArgs e)
            {

            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                if (process != null)
                {
                    e.step.View.Controls.Remove(buttonOpen);
                    e.step.View.ControlList.Remove(buttonOpen);
                    process.CloseMainWindow();
                    process = null;
                }
            };
            return s;
        }

        internal static Step ShowAdjDatFiles(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.StringTilt_OpenAdjDatFile);
            Process process = null;
            BigButton buttonOpen = new BigButton();
            ; s.FirstUse += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                if (process == null)
                {
                    if (st._AdjustingStationTilt.ParametersBasic._GeodeDatFilePath != "")
                    {
                        if (System.IO.File.Exists(st._AdjustingStationTilt.ParametersBasic._GeodeDatFilePath))
                        {
                            buttonOpen = e.step.View.AddButton(
                                R.String_GM_OpendatFile,
                                R.Open,
                                delegate (object source2, TsuObjectEventArgs args2)
                                {
                                    Shell.Run(st._AdjustingStationTilt.ParametersBasic._GeodeDatFilePath);
                                }
                            );
                            process = s.View.AddShowDatFilePanel(st._AdjustingStationTilt.ParametersBasic._GeodeDatFilePath);
                        }
                        else
                        {
                            new MessageInput(MessageType.Warning, R.StringTilt_MsgNoAdjDatFile).Show();
                        }
                    }
                    else
                    {
                        new MessageInput(MessageType.Warning, R.StringTilt_MsgNoAdjDatFile).Show();
                    }
                }
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                //Station.Module st = e.ActiveStationModule as Station.Module;
                //if (process == null)
                //{
                //    if (st._AdjustingStationTilt.ParametersBasic._GeodeDatFilePath != "")
                //    {
                //        if (System.IO.File.Exists(st._AdjustingStationTilt.ParametersBasic._GeodeDatFilePath))
                //        {
                //            process = s.View.AddShowDatFilePanel(st._AdjustingStationTilt.ParametersBasic._GeodeDatFilePath);
                //        }
                //        else
                //        {
                //            new MessageInput(MessageType.Warning, R.StringTilt_MsgNoAdjDatFile).Show();
                //        }
                //    }
                //    else
                //    {
                //        new MessageInput(MessageType.Warning, R.StringTilt_MsgNoAdjDatFile).Show();
                //    }
                //}
            };
            s.Updating += delegate (object source, StepEventArgs e)
            {

            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                if (process != null)
                {
                    e.step.View.Controls.Remove(buttonOpen);
                    e.step.View.ControlList.Remove(buttonOpen);
                    process.CloseMainWindow();
                    process = null;
                }
            };
            return s;
        }
        /// <summary>
        /// Allows to select a isntrument
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        static public Step ChooseInstrument(Common.Guided.Module guidedModule)
        {
            Step s = Management.ChooseInstrument(guidedModule, I.InstrumentClasses.TILT);

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {

            };

            s.ControlChanged += delegate (object source, StepEventArgs e)
            {

            };

            s.Testing += delegate (object source, StepEventArgs e)
            {

            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.SetPointsToMeasure(stationModule.FinalModule.MagnetsToBeAligned);
                if (e.OperationManager.SelectedOperation != null) { stationModule.ChangeOperationID(e.OperationManager.SelectedOperation); };
            };

            return s;
        }
    }
}
