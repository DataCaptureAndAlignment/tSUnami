﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using TSU.Views;
using System.Windows.Forms;
using M = TSU;
using T = TSU.Tilt;
using TSU.Common.Guided.Steps;

namespace TSU.Tilt.GuidedModules.Steps
{

    static public class Alignment
    {
        static public Common.Guided.Module Get(M.Module parentModule, string nameAndDescription)
        {
            Common.Guided.Module gm = new Common.Guided.Module(parentModule, R.StringGuidedTilt_RollSurvey);
            gm.guideModuleType = ENUM.GuidedModuleType.AlignmentTilt;
            gm.ObservationType = Common.ObservationType.Roll;

            // Invisible step: : Add needed station modules
            gm.SwitchToANewStationModule(new Station.Module(gm));
            Station.Module st = gm._ActiveStationModule as Station.Module;
            st._ActualStationTilt = st._AdjustingStationTilt;
            //gm.Steps.Add(Step.TiltMeter.ShowAdvancedTModule(gm));
            BuildSteps(gm);

            gm.Start();

            return gm;
        }

        private static void BuildSteps(Common.Guided.Module gm)
        {
            // steps qui sont créé par défaut
            //// Step 0: Choose Management.Instrument
            gm.Steps.Add(Measurement.ChooseInstrument(gm));

            //// Step 1: StationTilt Module mesure initiale
            gm.Steps.Add(Measurement.MeasureTiltForElementAlignment(gm));

            //// Step 2: show Geode dat file adjusting operation
            gm.Steps.Add(Measurement.ShowAdjDatFiles(gm));
        }

        internal static void ReCreateWhatIsNotSerialized(Common.Guided.Module gm)
        {
            gm.Steps = new List<Step>();
            Station.Module st = gm._ActiveStationModule as Station.Module;
            if (st != null) { st.View.moduleType = ENUM.ModuleType.Guided; }
            //gm.currentIndex = 0;
            BuildSteps(gm);
            //gm.Start();
            //gm.MoveToStep(gm.currentIndex);
        }
    }
}