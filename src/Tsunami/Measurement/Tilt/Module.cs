using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using TSU.Common.Elements;
using TSU.Common.Measures.States;
using TSU.Views;

namespace TSU.Tilt
{
    [Serializable]
    [XmlType(TypeName = "Tilt.Module")]
    public class Module : Common.FinalModule
    {
        #region Fields
        //public StationTiltModule stationTiltModule;
        //{
        //    get
        //    {
        //        return this._ActiveStationModule as StationTiltModule;
        //    }
        //}

        public override Common.ObservationType ObservationType { get; set; } = Common.ObservationType.Roll;

        [XmlIgnore]
        public new CompositeView View
        {
            get
            {
                return this._TsuView as CompositeView;
            }
            set
            {
                this._TsuView = value;
            }
        }
        #endregion

        #region Constructor
        public Module() // need a parameterless constructor for xml serialazation
            : base()
        {

        }

        public Module(TSU.Module parentModule, string nameAndDescription)
            : base(parentModule, nameAndDescription)
        {

        }
        #endregion

        #region Observer

        /// <summary>
        /// dispatcheur d'evenement si un type n'est pas connu il est trait� par OnNextBasedOn(TsuObject TsuObject)
        /// </summary>
        /// <param name="TsuObject"></param>
        public override void OnNext(TsuObject TsuObject)
        {
            dynamic dynamic = TsuObject;
            OnNextBasedOn(dynamic);
        }

        /// <summary>
        /// �v�nement li� � la r�ception de points de l'Element MOdule Manager et ajout � la s�quence de points
        /// </summary>
        /// <param name="elementsFromElementModule"></param>
        public new void OnNextBasedOn(TSU.Common.Elements.Composites.CompositeElement elementsFromElementModule)
        {
            //lance la fonction pour donner les points � mesurer
            (this._ActiveStationModule as Tilt.Station.Module).SetPointsToMeasure(elementsFromElementModule);
        }

        #endregion

        #region Methods
        public override void Initialize()// This method is called by the base class "Module" when this object is created
        {
            base.Initialize();
        }

        internal void NewStation()
        {
            this.SwitchToANewStationModule(new Tilt.Station.Module(this));
        }
        /// <summary>
        /// Cr�e un nouveau set de tilts en conservant les param�tres du set de tilts actuel
        /// </summary>
        internal void CloneStation()
        {
            Tilt.Station.Module newStationTiltModule = new Tilt.Station.Module(this);
            if (this._ActiveStationModule != null)
            {
                Tilt.Station.Module oldStationTiltModule = this._ActiveStationModule as Tilt.Station.Module;
                newStationTiltModule.ElementModule._SelectedObjects.Clear();
                newStationTiltModule.SetOperationsInNewStationTiltModule(oldStationTiltModule);
                newStationTiltModule.ChangeTeam(oldStationTiltModule._Station.ParametersBasic._Team);
                newStationTiltModule.SetGeodeFilePathInNewStationTiltModule(oldStationTiltModule);
                newStationTiltModule.SetInstrumentInNewStationTiltModule(oldStationTiltModule);
                newStationTiltModule.View.RedrawTreeviewParameters();

                newStationTiltModule.CreatedOn = DateTime.Now;
            }
            this.SwitchToANewStationModule(newStationTiltModule);
        }
        internal override void SetActiveStationModule(Common.Station.Module stationModule, bool createView = true)
        {
            base.SetActiveStationModule(stationModule, createView);
            this.View?.SetStationNameInTopButton(stationModule);
        }
        /// <summary>
        /// Efface la station actuelle et choisit la pr�c�dente station
        /// On ne peut pas effacer la premi�re station
        /// </summary>
        internal void DeleteActualStation()
        {
            if (this._ActiveStationModule != null)
            {
                int index = this.StationModules.FindIndex(x => x._Station._Name == this._ActiveStationModule._Station._Name);
                if (this.StationModules.Count != 1)
                {
                    this.StationModules.Remove(this._ActiveStationModule);
                    if (index > 0) { index -= 1; }
                    else { index = 0; }
                    this.SetActiveStationModule(this.StationModules[index]);
                }
            }
        }
        /// <summary>
        /// mise � jour point qui a �t� modifi� dans autre �l�ment manager
        /// </summary>
        /// <param name="originalPoint"></param>
        /// <param name="pointModified"></param>
        internal override void UpdateTheoCoordinates(Point originalPoint, Point pointModified)
        {
            base.UpdateTheoCoordinates(originalPoint, pointModified);
            foreach (Tilt.Station.Module stm in this.StationModules)
            {
                stm.UpdateTheoCoordinates(originalPoint, pointModified);
            }
        }

        #endregion

        public override string ToString()
        {
            return $"{this._Name} = {base.ToString()}";
        }


        internal override List<Common.Measures.Measure> GetMeasures()
        {
            List<Common.Measures.Measure> measures = new List<Common.Measures.Measure>();
            foreach (var station in GetStations())
            {
                foreach (var item in station.MeasuresTaken)
                {
                    var m = item.Clone() as Tilt.Measure;
                    if (m._AverageBothDirection != na && m.PointGUID != Guid.Empty)
                    {
                        m.Guid = Guid.NewGuid();
                        if (item._Point != null)
                        {
                            m._Point = item._Point.Clone() as Point;
                            m._Point._Name = (station as Tilt.Station)._MeasPoint._Name;
                        }
                        m._InstrumentSN = station.ParametersBasic._Instrument._Name;
                        m._Status = new Good();
                        measures.Add(m);
                    }
                }
            }
            return measures;
        }

        internal override IEnumerable<Common.Station> GetStations()
        {
            var stations = new List<Common.Station>();
            foreach (var item in this.StationModules)
            {
                stations.Add(item._Station);
            }
            return stations;
        }

        internal static string FormatDcum(double value)
        {
            int decimalToShow = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
            string ds = "F" + decimalToShow.ToString();
            return value.ToString(ds, System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
        }

        internal static string FormatAngleInMrad(double value)
        {
            int decimalToShow = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles - 1; //because 1CC = 0.0015 mrad
            string ds = "F" + decimalToShow.ToString();

            double valueInMRads = value * 1000d;

            return valueInMRads.ToString(ds, System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
        }
    }
}
