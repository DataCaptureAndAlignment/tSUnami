﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using TSU.ENUM;
using TSU.Views;
using TSU.Views.Message;
using D = TSU.Common.Instruments.Device;
using I = TSU.Common.Instruments.Device.WYLER;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;
using T = TSU.Tools;


namespace TSU.Tilt
{
    public partial class Station
    {
        public partial class View : Common.Station.View
        {
            #region Variables
            internal Module _stationTiltModule
            {
                get
                {
                    return this._Module as Module;
                }
            }

            internal D.View TiltMeterView
            {
                get
                {
                    if (this._stationTiltModule._InstrumentManager.SelectedInstrumentModule != null)
                        return this._stationTiltModule._InstrumentManager.SelectedInstrumentModule.View as D.View;
                    else return null;
                }
            }


            //Main Button
            internal BigButton bigbuttonTop;
            // boutons du menu contexte
            internal BigButton bigButton_Elements;
            //internal BigButton bigButton_saveInitial;
            //internal BigButton bigButton_saveAdjusting;
            internal BigButton bigButton_saveAll;
            //internal BigButton bigButton_AddMeas;
            //internal BigButton bigButton_DelMeas;
            //internal BigButton bigButton_DeleteSequence;
            //internal BigButton bigButton_NextPointInSequence;
            //internal BigButton bigButton_PreviousPointInSequence;
            internal BigButton bigButton_Cancel;
            internal BigButton bigButton_Sort;
            //internal BigButton bigButton_ChangePtTheo;
            internal BigButton bigButton_ShowInitDatFile;
            internal BigButton bigButton_ShowAdjDatFile;
            internal BigButton bigButton_SwitchBetweenRollType;
            internal BigButton bigButton_Help;
            internal TreeView treeView_Parameters;
            internal TsuNode node_Parameters;
            //internal TsuNode node_MeasuresInit;
            //internal TsuNode node_MeasuresAjusting;
            //internal TsuNode node_Element;
            internal TsuNode node_Admin;
            internal new ContextMenuStrip contextMenuStrip;
            internal TiltDirection tiltDirection = TiltDirection.Beam;
            /// <summary>
            /// change view with datagridview or treeview
            /// </summary>
            private TiltViewType ViewType = TiltViewType.Treeview;
            //internal int measureNumber = 0;
            //internal string pointNameSelected = "";
            //private bool measureSelected = false;
            //private bool pointSelected = false;
            //private bool delMeasAvailable = false;
            //private bool reorderElementNode = false;
            internal int rowActiveCell = 0;
            internal int columnActiveCell = 0;
            /// <summary>
            /// affiche ou cache le noeud mesure init et l'opération initiale dans le treeview.
            /// </summary>
            internal bool showInitOp = true;
            /// <summary>
            /// affiche ou cache le noeud mesure de réglage et l'opération de reglage dans le treeview.
            /// </summary>
            internal bool showAdjustingOp = true;
            /// <summary>
            /// Affiche ou cache le noeud des points de sequence dans le treeview
            /// </summary>
            //internal bool showElement = true;
            /// <summary>
            /// Affiche ou cache le noeud admin dans le treeview
            /// </summary>
            internal bool showAdmin = true;
            internal bool setCurrentCell = false;
            internal bool setCurrentNode = false;
            //DataGridView for Guided module
            internal DataGridView dataGridViewMesures;
            private DataGridViewImageColumn Remove;
            private DataGridViewTextBoxColumn Dcum;
            private DataGridViewTextBoxColumn PointName;
            private DataGridViewTextBoxColumn TiltTheo;
            private DataGridViewTextBoxColumn ReadingBeamTheo;
            private DataGridViewTextBoxColumn BeamDirection;
            private DataGridViewTextBoxColumn BeamOpposite;
            private DataGridViewTextBoxColumn Average;
            private DataGridViewTextBoxColumn Offset;
            private DataGridViewImageColumn SaveState;
            private DataGridViewTextBoxColumn SavingTime;
            private DataGridViewTextBoxColumn Comment;
            private DataGridViewTextBoxColumn Instrument;
            private DataGridViewTextBoxColumn Interfaces;
            private DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            //SplitContainer Treeview + splitContainerDAtaGridView
            private SplitContainer splitContainer2;
            /// <summary>
            /// Propriété qui défini si c'est un module guidé ou avancé
            /// </summary>
            internal ModuleType moduleType = ModuleType.Advanced;
            //internal System.Windows.Forms.Timer Timer_Keep_Cell_Focus;
            private bool stopCheckingContextMenuOrder = false;
            #endregion

            #region Constructeur
            /// <summary>
            /// Constructeur de la vue du station tilt module
            /// </summary>
            /// <param name="ParentModule"></param>
            public View(Module ParentModule)
                : base(ParentModule, Orientation.Vertical)
            //affichage du form lors de son lancement
            {
                InitializeComponent();
                this.ApplyThemeColors();
                AddWaitingView();
                LoadTiltModuleView();
                //this.Timer_Keep_Cell_Focus = new System.Windows.Forms.Timer(this.components);
                //this.Timer_Keep_Cell_Focus.Interval = 500;
                //this.Timer_Keep_Cell_Focus.Enabled = false;
                //this.Timer_Keep_Cell_Focus.Tick += new System.EventHandler(this.Timer_Keep_Cell_Focus_Tick);
            }
            private void ApplyThemeColors()
            {
                this._PanelBottom.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this._PanelTop.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.splitContainer1.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.splitContainer1.Panel1.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.splitContainer1.Panel2.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.tableLayoutPanel_MainTop.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
            }
            /// <summary>
            /// Actions à faire lors du chargement du tiltModuleView
            /// </summary>
            internal void LoadTiltModuleView()
            {
                // Actions à faire lors du chargement du tiltModuleView
                //Main Button
                this.ChangeViewType(this.ViewType);
                //this.splitContainer1.SplitterDistance = this.splitContainer1.Height;
                this.ShowDockedBottom();
                this.UpdateInstrumentView();
                this.UpdateDockStyle();
            }

            private void UpdateBigButtonTop()
            {

                string rollType = "";
                if (showInitOp) rollType = R.StringTilt_Init;
                if (showAdjustingOp) rollType = R.StringTilt_Adj;
                switch (this.ViewType)
                {
                    case TiltViewType.Treeview:
                        bigbuttonTop = new BigButton(
                            string.Format(
                                R.StringTilt_RollStation,
                                Tilt.Module.FormatAngleInMrad(this._stationTiltModule._ActualStationTilt.TiltParameters._Tolerance),
                                rollType),
                            R.Tilt,
                            bigButtonTopShowMenu_Click);
                        break;
                    case TiltViewType.DataGridView:
                        bigbuttonTop = new BigButton(
                            R.StringGuidedTilt_ExportAllRoll,
                            R.Save,
                            bigButtonTopSaveAll_Click);
                        break;
                    default:
                        break;
                }

                //this.tableLayoutPanel_MainTop.Controls.Clear();
                //this.tableLayoutPanel_MainTop.ColumnStyles.RemoveAt(1);
                //this.tableLayoutPanel_MainTop.ColumnStyles.RemoveAt(0);
                //if ((this._PanelTop.Width - bigButton_Help.Width - 10) > 0)
                //{
                //    this.tableLayoutPanel_MainTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, (this._PanelTop.Width - bigButton_Help.Width - 10)));
                //    this.tableLayoutPanel_MainTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, bigButton_Help.Width + 10));
                //}
                //else
                //{
                //    this.tableLayoutPanel_MainTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
                //    this.tableLayoutPanel_MainTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
                //}

                //bigbuttonTop.Margin = new Padding(0);
                //bigButton_Help.Margin = new Padding(0);
                //this.tableLayoutPanel_MainTop.Controls.Add(bigbuttonTop, 0, 0);
                //this.tableLayoutPanel_MainTop.Controls.Add(bigButton_Help, 1, 0);
                this._PanelTop.Controls.Clear();
                this._PanelTop.Controls.Add(bigbuttonTop);
            }
            /// <summary>
            /// Save all tilt that are ready to be saved
            /// </summary>
            private void bigButtonTopSaveAll_Click()
            {
                SaveAllTiltSequenceOnlyInActualOperation();
            }
            /// <summary>
            /// Sauvegarde toutes les stations tilts dans les 2 opérations
            /// </summary>
            private void bigButtonSaveAll_Click()
            {
                SaveAllTiltSequenceBothOperation();
            }
            /// <summary>
            /// Sauveagrde tous les stations roll uniquement de l'opération initiale ou adjusting actuelle
            /// </summary>
            /// <returns></returns>
            internal bool SaveAllTiltSequenceOnlyInActualOperation()
            {
                TiltInitOrAdjusting stationInitOrAdj = this._stationTiltModule._ActualStationTilt.TiltParameters._TiltInitOrAdjusting;
                bool saveAllSuccess = true;
                foreach (Station st in this._stationTiltModule._StationTiltSequences)
                {
                    if (st.TiltParameters._TiltInitOrAdjusting == stationInitOrAdj
                        && st.TiltParameters._State is State.StationTiltReadyToBeSaved)
                    {

                        this._stationTiltModule.ChangePointTheo(st.TheoPoint._Name);
                        switch (stationInitOrAdj)
                        {
                            case TiltInitOrAdjusting.Initial:
                                this._stationTiltModule._ActualStationTilt = this._stationTiltModule._InitStationTilt;
                                break;
                            case TiltInitOrAdjusting.Adjusting:
                                this._stationTiltModule._ActualStationTilt = this._stationTiltModule._AdjustingStationTilt;
                                break;
                            default:
                                break;
                        }
                        if (this._stationTiltModule.ExportToGeode(false, true).Success == false) saveAllSuccess = false;
                    }
                }
                return saveAllSuccess;
            }
            /// <summary>
            /// Sauvegarde toutes les stations tilt dans les 2 opérations
            /// </summary>
            /// <returns></returns>
            internal bool SaveAllTiltSequenceBothOperation()
            {
                TiltInitOrAdjusting stationInitOrAdj = this._stationTiltModule._ActualStationTilt.TiltParameters._TiltInitOrAdjusting;
                string actualPointName = this._stationTiltModule._ActualStationTilt.TheoPoint._Name;
                bool saveAllSuccess = true;
                string respond = "";
                string geodeFilePath = this._stationTiltModule._ActualStationTilt.ParametersBasic._GeodeDatFilePath;
                foreach (Station st in this._stationTiltModule._StationTiltSequences)
                {
                    if (st.TiltParameters._State is State.StationTiltReadyToBeSaved)
                    {
                        this._stationTiltModule.ChangePointTheo(st.TheoPoint._Name);
                        switch (st.TiltParameters._TiltInitOrAdjusting)
                        {
                            case TiltInitOrAdjusting.Initial:
                                this._stationTiltModule._ActualStationTilt = this._stationTiltModule._InitStationTilt;
                                break;
                            case TiltInitOrAdjusting.Adjusting:
                                this._stationTiltModule._ActualStationTilt = this._stationTiltModule._AdjustingStationTilt;
                                break;
                            default:
                                break;
                        }
                        if (this._stationTiltModule.ExportToGeode(false, true).Success == false) saveAllSuccess = false;
                    }
                }
                int index = this._stationTiltModule._StationTiltSequences.FindIndex(x => x.TiltParameters._TiltInitOrAdjusting == stationInitOrAdj && x.TheoPoint._Name == actualPointName);
                if (index != -1)
                {
                    this._stationTiltModule._ActualStationTilt = (this._stationTiltModule._StationTiltSequences[index]);
                }

                this.Module.ShowGeodeExportSuccess(R.StringTilt_SaveAllSuccess, geodeFilePath);
                //if (saveAllSuccess && geodeFilePath != "")
                //{
                //    string titleAndMessage = String.Format(R.StringTilt_SaveAllSuccess);
                //    MessageInput mi = new MessageInput(MessageType.GoodNews, titleAndMessage)
                //    {
                //        ButtonTexts = new List<string> { R.T_OK, R.T_OpenFileLocation }
                //    };
                //    respond = mi.Show().TextOfButtonClicked;
                //}
                //else
                //{
                //    if (saveAllSuccess)
                //    {
                //        string titleAndMessage = String.Format(R.StringTilt_SaveAllSuccess);
                //        new MessageInput(MessageType.GoodNews, titleAndMessage).Show();
                //    }
                //}
                //if (respond == R.T_OpenFileLocation)
                //{
                //    if (System.IO.File.Exists(geodeFilePath))
                //    {
                //        System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo("explorer.exe", " /select, " + geodeFilePath));
                //    }
                //}
                return saveAllSuccess;
            }
            #endregion
            #region Treeview
            //Initialization de la vue avec le treeview
            private void InitializeAdvancedViewWithTreeview()
            {
                this.showInitOp = true;
                this.showAdjustingOp = false;
                this.CreateSplitContainer2();
                this.InitializeSplitContainer2();
                this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
                this.CreateContextMenuButtons();
                //this.CreateTableLayoutTopButtonS();
                //RefreshGlobalContextMenuItemCollection();
                this.treeView_Parameters = new TreeView();
                this.splitContainer2.Panel1.Controls.Add(this.treeView_Parameters);
                this.InitializeTreeviewParameters();
                this.CreateDatagridViewMesures();
                this.InitializeDatagridViewMesures();
                this.splitContainer2.Panel2.Controls.Add(this.dataGridViewMesures);
                this.UpdateBigButtonTop();
                this.HideInstrumentView();
            }
            public void InitializeTreeviewParameters()
            //creation du treeview avec tous les nodes
            {
                // 
                // treeView_Parameters
                // 
                this.treeView_Parameters.BeginUpdate();
                this.treeView_Parameters.AllowDrop = true;
                this.treeView_Parameters.Dock = DockStyle.Fill;
                this.treeView_Parameters.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.treeView_Parameters.ItemHeight = 32;
                this.treeView_Parameters.Location = new System.Drawing.Point(0, 0);
                this.treeView_Parameters.Name = "treeView_Parameters";
                this.treeView_Parameters.ShowNodeToolTips = true;
                this.treeView_Parameters.Size = new System.Drawing.Size(1202, 427);
                this.treeView_Parameters.TabIndex = 0;
                this.treeView_Parameters.NodeMouseClick += new TreeNodeMouseClickEventHandler(this.treeView_Parameters_NodeMouseClick);
                this.treeView_Parameters.NodeMouseDoubleClick += new TreeNodeMouseClickEventHandler(this.treeView_Parameters_NodeMouseDoubleClick);
                this.treeView_Parameters.ImageList = TreeNodeImages.ImageListInstance;
                this.treeView_Parameters.SelectedImageKey = "StationParameters";
                //Noeud parametre
                this.node_Parameters = new TsuNode();
                this.node_Parameters.SelectedImageKey = "StationParameters";
                this.node_Parameters.ImageKey = "StationParameters";
                this.treeView_Parameters.Nodes.Add(this.node_Parameters);
                this.DressNodeParameter();
                //Noeud Admin
                this.node_Admin = new TsuNode();
                if (showAdmin)
                {
                    this.node_Parameters.Nodes.Add(node_Admin);
                    this.node_Admin.BasedOn(this._stationTiltModule._ActualStationTilt.TiltParameters, this._stationTiltModule._InitStationTilt.TiltParameters._Operation,
                        this._stationTiltModule._AdjustingStationTilt.TiltParameters._Operation,
                        this.showInitOp, this.showAdjustingOp);
                }
                //Noeud element
                //this.node_Element = new TsuNode();
                //if (this.showElement)
                //{
                //    this.node_Parameters.Nodes.Add(node_Element);
                //    this.node_Element.BaseOn(this._stationTiltModule._StationTiltSequences, this._stationTiltModule._ActualStationTilt.TheoPoint);
                //}
                //Noeud Mesures initiales
                //this.node_MeasuresInit = new TsuNode();
                //if (this.showInitOp)
                //{
                //    this.treeView_Parameters.Nodes.Add(this.node_MeasuresInit);
                //    this.node_MeasuresInit.BasedOn(this._stationTiltModule._InitStationTilt);
                //}
                //Noeud mesures reglages
                //this.node_MeasuresAjusting = new TsuNode();
                //if (this.showAdjustingOp)
                //{
                //    this.treeView_Parameters.Nodes.Add(this.node_MeasuresAjusting);
                //    this.node_MeasuresAjusting.BasedOn(this._stationTiltModule._AdjustingStationTilt);
                //}
                //menu contextuel
                this.contextMenuStrip = new ContextMenuStrip();
                this.contextMenuStrip.Click += new EventHandler(this.Context_OnClick);
                this.treeView_Parameters.ExpandAll();
                this.treeView_Parameters.EndUpdate();
                //this.ResetBigButtonAvailable();
            }
            /// <summary>
            /// Choix du nom du noeud paramètre et de son format
            /// </summary>
            private void DressNodeParameter()
            {
                //Choix de l'entête du noeud paramètre
                this.node_Parameters.Name = "Parameters__";
                if (this._stationTiltModule._ActualStationTilt != null)
                    this.node_Parameters.Text = string.Format(R.StringTilt_Parameter, this._stationTiltModule._ActualStationTilt._Name, this._stationTiltModule._ActualStationTilt.TiltParameters._State._Description);
                this.node_Parameters.State = StateType.Normal;
                if (this._stationTiltModule._ActualStationTilt.TiltParameters._State is State.Opening) this.node_Parameters.State = StateType.Normal;
                if (this._stationTiltModule._ActualStationTilt.TiltParameters._State is State.TiltcalculationSuccessfull) this.node_Parameters.State = StateType.Normal;
                if (this._stationTiltModule._ActualStationTilt.TiltParameters._State is State.StationTiltReadyToBeSaved) this.node_Parameters.State = StateType.Ready;
                if (this._stationTiltModule._ActualStationTilt.TiltParameters._State is State.StationTiltSaved) this.node_Parameters.State = StateType.Ready;
                if (this._stationTiltModule._ActualStationTilt.TiltParameters._State is State.TiltcalculationFailed) this.node_Parameters.State = StateType.NotReady;
                this.node_Parameters.ColorNodeBaseOnState(this.node_Parameters);
                this.node_Parameters.Tag = this._stationTiltModule._ActualStationTilt;
            }
            public void RedrawTreeviewParameters()
            //creation du treeview avec tous les nodes
            {
                //TsuNode ghostNode_MeasuresInit = new TsuNode() ;
                //TsuNode ghostNode_MeasuresAjusting = new TsuNode();
                //TsuNode ghostNode_Element = new TsuNode();
                TsuNode ghostNode_Admin = new TsuNode();
                this.treeView_Parameters.BeginUpdate();
                //Noeud parametre
                this.DressNodeParameter();
                //Noeud Admin
                if (this.showAdmin)
                {
                    ghostNode_Admin.BasedOn(this._stationTiltModule._ActualStationTilt.TiltParameters, this._stationTiltModule._InitStationTilt.TiltParameters._Operation,
                        this._stationTiltModule._AdjustingStationTilt.TiltParameters._Operation,
                        this.showInitOp, this.showAdjustingOp);
                    this.node_Admin.UpdateTsuNode(ghostNode_Admin);
                }
                else
                {
                    this.node_Admin.Nodes.Clear();
                }
                // //Noeud element
                // if (this.showElement)
                // {
                //     ghostNode_Element.BaseOn(this._stationTiltModule._StationTiltSequences, this._stationTiltModule._ActualStationTilt.TheoPoint);
                //    this.node_Element.UpdateTsuNode(ghostNode_Element,reorderElementNode);
                //    reorderElementNode = false;
                // }
                //else
                //{
                //    this.node_Element.Nodes.Clear();
                //}
                // //noeud Mesures initiales
                // if (this.showInitOp)
                // {
                //    ghostNode_MeasuresInit.BasedOn(this._stationTiltModule._InitStationTilt);
                //    this.node_MeasuresInit.UpdateTsuNode(ghostNode_MeasuresInit);
                //}
                //else
                //{
                //    this.node_MeasuresInit.Nodes.Clear();
                //}
                // //noeud Mesures réglages
                // if (this.showAdjustingOp)
                // {
                //     ghostNode_MeasuresAjusting.BasedOn(this._stationTiltModule._AdjustingStationTilt);
                //    this.node_MeasuresAjusting.UpdateTsuNode(ghostNode_MeasuresAjusting);
                // }
                //else
                //{
                //    this.node_MeasuresAjusting.Nodes.Clear();
                //}
                this.treeView_Parameters.EndUpdate();
                //this.ResetBigButtonAvailable();
            }
            /// <summary>
            /// Evenements liés si on clique dans le treeview
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void treeView_Parameters_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
            //Evenements liés si on clique dans le treeview
            {
                //bool showMenu = true;
                //this.ResetBigButtonAvailable();
                //measureSelected = false;
                //pointSelected = false;
                //delMeasAvailable = false;
                //measureNumber = -1; // permet d'indiquer qu'on a pas sélectionné un numéro de mesure
                if (!e.Node.Bounds.Contains(e.Location)) return;
                if (e.Node == null) return;
                this.CheckDatagridIsInEditMode();
                this.treeView_Parameters.SelectedNode = e.Node;
                String[] nameSplit;
                Char splitChar = '_';
                nameSplit = e.Node.Name.Split(splitChar);
                ////permet de voir si on est dans une station tilt de réglage ou une station tilt initiale
                //if (nameSplit[0] == "Adjusting")
                //{
                //    this._stationTiltModule._ActualStationTilt = this._stationTiltModule._AdjustingStationTilt;
                //}
                //else
                //{
                //    this._stationTiltModule._ActualStationTilt = this._stationTiltModule._InitStationTilt;
                //}
                if (e.Button == MouseButtons.Left)
                {
                    ////Ajout des boutons dans le context menu
                    ////Affichage bouton delete mesure
                    //if ((this._stationTiltModule._ActualStationTilt._MeasuresTilt.Count > 1)
                    //    && ((nameSplit[1] == "Measure")
                    //    || (nameSplit[1] == "MeasTiltBeam")
                    //    || (nameSplit[1] == "MeasTiltOpposite")
                    //    || (nameSplit[1] == "MeasAverageBothDirection")))
                    //{
                    //    // TryParse returns true if the conversion succeeded
                    //    // and stores the result in j.
                    //    int j;
                    //    if (Int32.TryParse(nameSplit[2], out j)) measureNumber = j;
                    //}
                    //this.SetSaveButtonAvailibility();
                    //Affichage bouton ajout mesure
                    //if ((nameSplit[1] == "Comment")
                    //    || (nameSplit[1] == "AverageTilt")
                    //    || (nameSplit[1] == "EcartTilt")
                    //    || (nameSplit[1] == "EmqTilt")
                    //    || (nameSplit[1] == "Measure")
                    //    || (nameSplit[1] == "MeasTiltBeam")
                    //    || (nameSplit[1] == "MeasTiltOpposite")
                    //    || (nameSplit[1] == "MeasAverageBothDirection")
                    //    || (nameSplit[1] == "TheoBeam")
                    //    || ((nameSplit[1] == "NodeMeasure") && (this._stationTiltModule._ActualStationTilt.TheoPoint._Name != "Unknown")))
                    //{
                    //    measureSelected = true;
                    //}
                    //Affichage bouton delete mesure
                    //if ((nameSplit[1] == "Measure")
                    //    || (nameSplit[1] == "MeasTiltBeam")
                    //    || (nameSplit[1] == "MeasTiltOpposite")
                    //    || (nameSplit[1] == "MeasAverageBothDirection"))
                    //{
                    //    delMeasAvailable = true;
                    //}
                    // n'affiche pas le menu contextuel sur le noeud des mesures à encoder pour permettre encodage mesure via interface instrument
                    //if (nameSplit[1] == "MeasTiltBeam" || nameSplit[1] == "MeasTiltOpposite")
                    // {
                    //     int j;
                    //     if (Int32.TryParse(nameSplit[2], out j))
                    //     {
                    //         measureNumber = j;
                    //         switch (nameSplit[1])
                    //         {
                    //             case "MeasTiltBeam":
                    //                 tiltDirection = TiltDirection.Beam;
                    //                 measureSelected = true;
                    //                 showMenu = false;
                    //                 break;
                    //             case "MeasTiltOpposite":
                    //                 tiltDirection = TiltDirection.Opposite;
                    //                 measureSelected = true;
                    //                 showMenu = false;
                    //                 break;
                    //             default:
                    //                 //lorsqu'on est sur la racine des mesures
                    //                 break;
                    //         }
                    //     }
                    // }
                    ///// Changement station tilt dans la séquence
                    //if (nameSplit[1] == "Sequence")
                    //{
                    //    if ((nameSplit[2] != "Main")) //&& (this.moduleType == ModuleType.Advanced))
                    //    {
                    //        pointSelected = true;
                    //        this.pointNameSelected = nameSplit[2];
                    //        showMenu = true;
                    //    }
                    //}
                    //Ne permet plus de sélectionner des points theo si on a pas sélectionné d'instrument avant
                    if ((nameSplit[1] == "Element") && (this.moduleType == ModuleType.Advanced))
                    {
                        //showMenu = false;
                        if (this._stationTiltModule._ActualStationTilt.TiltParameters._Instrument._Model != null) { this.SelectPoint(); }
                    }
                    //changement du numéro d'opération initiale
                    if ((nameSplit[1] == "OperationID") && (nameSplit[0] == "Initial"))
                    {
                        this.SelectInitialOperation();
                        //showMenu = false;
                    }
                    //changement du numéro d'opération réglage
                    if ((nameSplit[1] == "OperationID") && (nameSplit[0] == "Adjusting"))
                    {
                        this.SelectAdjustingOperation();
                        //showMenu = false;
                    }
                    //Changement du nom de l'équipe
                    if (nameSplit[1] == "Team")
                    {
                        this.ChangeTeam();
                        //showMenu = false;
                    }
                    if (nameSplit[1] == "Instrument")
                    {
                        this.ChangeInstrument();
                        //showMenu = false;
                    }
                    //if (nameSplit[1] == "Comment") { this.ChangeComment(); showMenu = false;}
                    if (nameSplit[1] == "Tolerance")
                    {
                        this.ChangeTolerance();
                        //showMenu = false;
                    }
                    //bigButton_DeleteSequence = new BigButton( string.Format(R.StringTilt_DeletePoint, pointNameSelected),R.Tilt_Delete, buttonDelSeq_Click);
                    //string mesureName = "";
                    //switch (this._stationTiltModule._ActualStationTilt.TiltParameters._TiltInitOrAdjusting)
                    //{
                    //    case TiltInitOrAdjusting.Initial:
                    //        mesureName = R.StringTilt_InitialMeasure + (measureNumber+1);
                    //        break;
                    //    case TiltInitOrAdjusting.Adjusting:
                    //        mesureName = R.StringTilt_AdjustingMeasure + (measureNumber+1);
                    //        break;
                    //    default:
                    //        break;
                    //}
                    //bigButton_DelMeas = new BigButton(string.Format(R.StringTilt_DeleteMeasure, mesureName),R.Tilt_Delete, buttonDelMeas_Click);
                    //if (showMenu){ this.ShowContextMenuGlobal(); }

                }
            }
            /// <summary>
            /// Double click sur treeview pour entrer une mesure avec clavier
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void treeView_Parameters_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
            // Double click sur treeview pour entrer une mesure avec clavier
            {
                //measureSelected = false;
                //pointSelected = false;
                //delMeasAvailable = false;
                //this.treeView_Parameters.SelectedNode = e.Node;
                //String[] nameSplit;
                // Char splitChar = '_';
                // nameSplit = e.Node.Name.Split(splitChar);
                // //Choix de la actualStationTilt en fonction si c'est mesure initiale ou réglage
                // if (nameSplit[0] == "Adjusting")
                // {
                //     this._stationTiltModule._ActualStationTilt = this._stationTiltModule._AdjustingStationTilt;
                // }
                // else
                // {
                //     this._stationTiltModule._ActualStationTilt = this._stationTiltModule._InitStationTilt;
                // }
                // //ENcodage nouvelle mesure
                // if ((nameSplit[1] == "MeasTiltBeam")
                //     || (nameSplit[1] == "MeasTiltOpposite"))
                // {
                //     measureSelected = true;
                //    this.EnterTiltInDatagridPopUp();
                // string typeMes = "";
                // typeMes = nameSplit[1];
                // // TryParse returns true if the conversion succeeded
                // // and stores the result in j.
                // int j;
                // double previousValue;
                //MeasureOfTilt newMeasTilt = new MeasureOfTilt();
                // if (Int32.TryParse(nameSplit[2], out j))
                //     measureNumber = j;
                // switch (typeMes)
                // {
                //     case "MeasTiltBeam":
                //         tiltDirection = TiltDirection.Beam;
                //         previousValue = this._stationTiltModule._ActualStationTilt._MeasuresTilt[measureNumber]._ReadingBeamDirection.Value;
                //        if (this.EnterTiltValue(previousValue, newMeasTilt, measureNumber)!=R.T_CANCEL)
                //        {
                //            this.AddInstrumentMeasure(newMeasTilt, false);
                //            //Demande automatique l'autre direction lors mesure d'un tilt
                //            tiltDirection = TiltDirection.Opposite;
                //            previousValue = this._stationTiltModule._ActualStationTilt._MeasuresTilt[measureNumber]._ReadingOppositeBeam.Value;
                //            this.EnterTiltValue(previousValue, newMeasTilt, measureNumber);
                //            this.AddInstrumentMeasure(newMeasTilt, true);
                //        }
                //         break;
                //     case "MeasTiltOpposite":
                //        tiltDirection = TiltDirection.Opposite;
                //        previousValue = this._stationTiltModule._ActualStationTilt._MeasuresTilt[measureNumber]._ReadingOppositeBeam.Value;
                //        if (this.EnterTiltValue(previousValue, newMeasTilt, measureNumber) != R.T_CANCEL)
                //        {
                //            this.AddInstrumentMeasure(newMeasTilt, false);
                //            //Demande automatique l'autre direction lors mesure d'un tilt
                //            tiltDirection = TiltDirection.Beam;
                //            previousValue = this._stationTiltModule._ActualStationTilt._MeasuresTilt[measureNumber]._ReadingBeamDirection.Value;
                //            this.EnterTiltValue(previousValue, newMeasTilt, measureNumber);
                //            this.AddInstrumentMeasure(newMeasTilt, true);
                //        }
                //         break;
                //     default:
                //         //lorsqu'on est sur la racine des mesures
                //         break;
                // }
                //}
            }

            #endregion
            #region datagridviewMesures
            public void CreateDatagridViewMesures()
            {
                this.dataGridViewMesures = new DataGridView();
                this.Remove = new DataGridViewImageColumn();
                this.PointName = new DataGridViewTextBoxColumn();
                this.Dcum = new DataGridViewTextBoxColumn();
                this.TiltTheo = new DataGridViewTextBoxColumn();
                this.ReadingBeamTheo = new DataGridViewTextBoxColumn();
                this.BeamDirection = new DataGridViewTextBoxColumn();
                this.BeamOpposite = new DataGridViewTextBoxColumn();
                this.Average = new DataGridViewTextBoxColumn();
                this.Offset = new DataGridViewTextBoxColumn();
                this.SaveState = new DataGridViewImageColumn();
                this.SavingTime = new DataGridViewTextBoxColumn();
                this.Comment = new DataGridViewTextBoxColumn();
                this.Instrument = new DataGridViewTextBoxColumn();
                this.Interfaces = new DataGridViewTextBoxColumn();
            }
            private void InitializeGuidedViewWithDataGridView()
            {
                this.CreateContextMenuButtons();
                this.CreateDatagridViewMesures();
                this.InitializeDatagridViewMesures();
                this.splitContainer1.Panel1.Controls.Add(this.dataGridViewMesures);
                this.UpdateBigButtonTop();
                this.HideInstrumentView();
            }
            public void InitializeDatagridViewMesures()
            {
                ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMesures)).BeginInit();
                // 
                // dataGridView1
                // 
                this.dataGridViewMesures.AllowUserToAddRows = false;
                this.dataGridViewMesures.AllowUserToDeleteRows = false;
                this.dataGridViewMesures.AllowUserToResizeColumns = true;
                this.dataGridViewMesures.AllowUserToResizeRows = false;
                this.dataGridViewMesures.MultiSelect = false;
                this.dataGridViewMesures.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
                dataGridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridViewCellStyle1.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dataGridViewCellStyle1.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
                dataGridViewCellStyle1.SelectionBackColor = Tsunami2.Preferences.Theme.Colors.TextHighLightBack;
                dataGridViewCellStyle1.SelectionForeColor = Tsunami2.Preferences.Theme.Colors.TextHighLightFore;
                dataGridViewCellStyle1.WrapMode = DataGridViewTriState.True;
                this.dataGridViewMesures.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
                this.dataGridViewMesures.ColumnHeadersHeight = 400;
                this.dataGridViewMesures.DefaultCellStyle.Font = Tsunami2.Preferences.Theme.Fonts.NormalItalic;
                this.dataGridViewMesures.RowHeadersDefaultCellStyle.Font = Tsunami2.Preferences.Theme.Fonts.NormalItalic;
                this.dataGridViewMesures.ColumnHeadersDefaultCellStyle.Font = Tsunami2.Preferences.Theme.Fonts.NormalItalic;
                this.dataGridViewMesures.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.dataGridViewMesures.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
                this.dataGridViewMesures.AllowUserToAddRows = false;
                this.dataGridViewMesures.AllowUserToDeleteRows = false;
                this.dataGridViewMesures.Columns.AddRange(
                    this.Remove,
                    this.PointName,
                    this.Dcum,
                    this.TiltTheo,
                    this.ReadingBeamTheo,
                    this.BeamDirection,
                    this.BeamOpposite,
                    this.Average,
                    this.Offset,
                    this.Instrument,
                    this.Interfaces,
                    this.SaveState,
                    this.SavingTime,
                    this.Comment);
                this.dataGridViewMesures.Dock = DockStyle.Fill;
                this.dataGridViewMesures.EnableHeadersVisualStyles = false;
                this.dataGridViewMesures.GridColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.dataGridViewMesures.Location = new System.Drawing.Point(0, 0);
                this.dataGridViewMesures.MultiSelect = false;
                this.dataGridViewMesures.Name = "dataGridViewMesures";
                this.dataGridViewMesures.RowHeadersVisible = false;
                this.dataGridViewMesures.RowTemplate.Height = Tsunami2.Preferences.Theme.Fonts.NormalItalic.Height+5;
                this.dataGridViewMesures.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                this.dataGridViewMesures.ShowCellErrors = false;
                this.dataGridViewMesures.ShowEditingIcon = false;
                this.dataGridViewMesures.ShowRowErrors = false;
                this.dataGridViewMesures.Size = new System.Drawing.Size(1206, 223);
                this.dataGridViewMesures.TabIndex = 15;
                // 
                // Remove 0
                // 
                this.Remove.HeaderText = "";
                this.Remove.ImageLayout = DataGridViewImageCellLayout.Stretch;
                this.Remove.SortMode = DataGridViewColumnSortMode.NotSortable;
                this.Remove.Name = "Remove";
                this.Remove.ReadOnly = true;
                this.Remove.Resizable = DataGridViewTriState.False;
                this.Remove.Width = 30;
                // 
                // PointName 1
                // 
                this.PointName.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.PointName.HeaderText = R.StringTilt_DataGrid_PointName;
                this.PointName.Resizable = DataGridViewTriState.True;
                this.PointName.SortMode = DataGridViewColumnSortMode.Automatic;
                this.PointName.Name = "PointName";
                this.PointName.ReadOnly = true;
                this.PointName.Width = 250;
                this.PointName.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                // 
                // Dcum 2
                // 
                this.Dcum.SortMode = DataGridViewColumnSortMode.Automatic;
                this.Dcum.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.Dcum.HeaderText = R.StringTilt_DataGrid_Dcum;
                this.Dcum.Name = "Dcum";
                this.Dcum.Width = 100;
                this.Dcum.ReadOnly = true;
                this.Dcum.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                // 
                // TiltTheo 3
                // 
                this.TiltTheo.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.TiltTheo.HeaderText = R.StringTilt_DataGrid_TiltTheo;
                this.TiltTheo.SortMode = DataGridViewColumnSortMode.Automatic;
                this.TiltTheo.Name = "TiltTheo";
                this.TiltTheo.ReadOnly = true;
                this.TiltTheo.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                // 
                // ReadingBeamTheo 4
                // 
                this.ReadingBeamTheo.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.ReadingBeamTheo.HeaderText = R.StringTilt_DataGrid_ReadingBeamTheo;
                this.ReadingBeamTheo.SortMode = DataGridViewColumnSortMode.NotSortable;
                this.ReadingBeamTheo.Name = "ReadingBeamTheo";
                this.ReadingBeamTheo.ReadOnly = true;
                this.TiltTheo.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                // 
                // BeamDirection 5
                // 
                this.BeamDirection.SortMode = DataGridViewColumnSortMode.NotSortable;
                this.BeamDirection.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.BeamDirection.HeaderText = R.StringTilt_DataGrid_TiltBeam;
                this.BeamDirection.Name = "BeamDirection";
                this.TiltTheo.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                // 
                // BeamOpposite 6
                // 
                this.BeamOpposite.SortMode = DataGridViewColumnSortMode.NotSortable;
                this.BeamOpposite.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.BeamOpposite.HeaderText = R.StringTilt_DataGrid_TiltOpposite;
                this.BeamOpposite.Name = "BeamOpposite";
                this.TiltTheo.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                // 
                // Average 7
                // 
                this.Average.SortMode = DataGridViewColumnSortMode.NotSortable;
                this.Average.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.Average.HeaderText = R.StringTilt_DataGrid_TiltAverage;
                this.Average.Name = "Average";
                this.Average.ReadOnly = true;
                this.TiltTheo.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                // 
                // Offset 8
                // 
                this.Offset.SortMode = DataGridViewColumnSortMode.NotSortable;
                this.Offset.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.Offset.HeaderText = R.StringTilt_DataGrid_Offset;
                this.Offset.Name = "Offset";
                this.Offset.ReadOnly = true;
                this.Offset.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                // 
                // Instrument 9
                // 
                this.Instrument.SortMode = DataGridViewColumnSortMode.NotSortable;
                this.Instrument.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.Instrument.HeaderText = R.StringTilt_DataGrid_Instrument;
                this.Instrument.Name = "Instrument";
                this.Instrument.Width = 150;
                this.Instrument.ReadOnly = true;
                this.Instrument.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                // 
                // Instrument 10
                // 
                this.Interfaces.SortMode = DataGridViewColumnSortMode.NotSortable;
                this.Interfaces.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.Interfaces.HeaderText = "Interfaces";
                this.Interfaces.Name = "Interfaces";
                this.Interfaces.Width = 150;
                this.Interfaces.ReadOnly = true;
                this.Interfaces.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                // 
                // SaveState 11
                //
                this.SaveState.SortMode = DataGridViewColumnSortMode.NotSortable;
                this.SaveState.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.SaveState.HeaderText = "";
                this.SaveState.Name = "SaveState";
                this.SaveState.ReadOnly = true;
                this.SaveState.ImageLayout = DataGridViewImageCellLayout.Zoom;
                this.SaveState.Resizable = DataGridViewTriState.False;
                this.SaveState.Width = 100;
                this.SaveState.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                // 
                // Saving time 12
                // 
                this.SavingTime.SortMode = DataGridViewColumnSortMode.NotSortable;
                this.SavingTime.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.SavingTime.HeaderText = R.StringTilt_DataGrid_LastExportingTime;
                this.SavingTime.Name = "SavingTime";
                this.SavingTime.Width = 150;
                this.SavingTime.ReadOnly = true;
                // 
                // Comment 13
                // 
                this.Comment.SortMode = DataGridViewColumnSortMode.NotSortable;
                this.Comment.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.Comment.HeaderText = R.StringTilt_DataGrid_Comment;
                this.Comment.Name = "Comment";
                this.Comment.MaxInputLength = 150;
                this.Comment.Width = 300;
                this.splitContainer1.ResumeLayout(false);
                this.dataGridViewMesures.CellValueChanged += new DataGridViewCellEventHandler(this.dataGridViewMesures_CellValueChanged);
                this.dataGridViewMesures.CellClick += new DataGridViewCellEventHandler(this.dataGridViewMeasure_CellClick);
                this.dataGridViewMesures.CurrentCellChanged += new EventHandler(this.dataGridViewMesure_CurrentCellChanged);
                this.dataGridViewMesures.ColumnHeaderMouseClick += new DataGridViewCellMouseEventHandler(this.dataGridViewMesures_ColumnHeaderMouseClick);
                this.dataGridViewMesures.MouseClick += new MouseEventHandler(this.dataGridViewMesures_MouseClick);
                this.dataGridViewMesures.CellDoubleClick += new DataGridViewCellEventHandler(this.dataGridViewMesures_CellDoubleClick);
                this.dataGridViewMesures.MouseEnter += new EventHandler(this.dataGridViewMesures_MouseEnter);
                ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMesures)).EndInit();
            }

            /// <summary>
            /// Redraw the datagridview
            /// </summary>
            private void RedrawDataGridViewMesures()
            {
                this.dataGridViewMesures.CellValueChanged -= this.dataGridViewMesures_CellValueChanged;
                //this.dataGridViewMesures.CellClick -= this.dataGridViewMeasure_CellClick;
                //this.dataGridViewMesures = new System.Windows.Forms.DataGridView();
                //this.InitializeDatagridViewMesures();
                DataGridUtility.SaveSorting(dataGridViewMesures);
                dataGridViewMesures.Rows.Clear();
                List<List<object>> listRow = new List<List<object>>();
                int actualLineIndex = 0;
                List<int> index = new List<int>();
                foreach (Station item in this._stationTiltModule._StationTiltSequences)
                {
                    if (this.showInitOp == true && item.TiltParameters._TiltInitOrAdjusting == TiltInitOrAdjusting.Initial)
                    {
                        AddNewRowInDataGridView(listRow, item);
                        if (item._MeasuresTilt.Count > 1) index.Add(actualLineIndex);
                        actualLineIndex++;
                    }
                    if (this.showAdjustingOp == true && item.TiltParameters._TiltInitOrAdjusting == TiltInitOrAdjusting.Adjusting)
                    {
                        AddNewRowInDataGridView(listRow, item);
                        if (item._MeasuresTilt.Count > 1) index.Add(actualLineIndex);
                        actualLineIndex++;
                    }
                }
                foreach (List<Object> ligne in listRow)
                {
                    dataGridViewMesures.Rows.Add(ligne.ToArray());
                }
                for (int i = 0; i < dataGridViewMesures.RowCount; i++)
                {
                    ///Met en jaune les cases de mesure à encoder
                    if (dataGridViewMesures[0, i].Value.ToString() != "")
                    {
                        dataGridViewMesures[5, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Action;
                        dataGridViewMesures[6, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Action;
                    }
                    ///Temporairement n'affiche les couleurs que en module avancé.
                    ///Met en rouge les écarts si > à la tolérance d'alignement
                    double p = T.Conversions.Numbers.ToDouble(dataGridViewMesures[8, i].Value.ToString(), true, -9999);
                    if (p != -9999 && this.moduleType == ModuleType.Advanced)
                    {
                        dataGridViewMesures[8, i].Style.BackColor = (Math.Abs(p) > this._stationTiltModule._ActualStationTilt.TiltParameters._Tolerance * 1000) ? Tsunami2.Preferences.Theme.Colors.Bad : dataGridViewMesures.Rows[i].DefaultCellStyle.BackColor;
                        dataGridViewMesures[8, i].Style.ForeColor = (Math.Abs(p) > this._stationTiltModule._ActualStationTilt.TiltParameters._Tolerance * 1000) ? dataGridViewMesures.Rows[i].DefaultCellStyle.ForeColor : Tsunami2.Preferences.Theme.Colors.Good;
                    }
                    ///Met en read only si il y a plusieurs mesures de tilt pour un élément
                    if (index.FindIndex(x => x == i) != -1)
                    {
                        dataGridViewMesures[5, i].ReadOnly = true;
                        dataGridViewMesures[6, i].ReadOnly = true;
                        dataGridViewMesures[4, i].Value = "";
                    }
                }
                DataGridUtility.RestoreSorting(dataGridViewMesures);
                //this.dataGridViewMesures.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewMeasure_CellClick);
                this.dataGridViewMesures.CellValueChanged += new DataGridViewCellEventHandler(this.dataGridViewMesures_CellValueChanged);
            }
            /// <summary>
            /// Check that all points from station tilt sequence are already in the datagridview
            /// </summary>
            /// <returns></returns>
            private bool CheckIfSamePoints()
            {
                //si le nombre de ligne est différent du nombre de station tilt/2 (adjusting + initial) return false
                if (this._stationTiltModule._StationTiltSequences.Count / 2 != this.dataGridViewMesures.RowCount) { return false; }
                foreach (Station item in this._stationTiltModule._StationTiltSequences)
                {
                    for (int i = 0; i <= this.dataGridViewMesures.RowCount; i++)
                    {
                        if (i == this.dataGridViewMesures.RowCount) { return false; }
                        if (this.dataGridViewMesures[0, i].Value.ToString() == item.TheoPoint._Name) { break; }
                    }
                }
                return true;
            }
            /// <summary>
            /// Add a new row of tilt meaure in the datagridview
            /// </summary>
            /// <param name="listRow"></param>
            /// <param name="item"></param>
            private static void AddNewRowInDataGridView(List<List<object>> listRow, Station item)
            {
                ///ne garde qu'une seule mesure de tilt pour chaque élément pour la vue en tableau (module guide)
                //    if (item._MeasuresTilt.Count>1)
                //{
                //       item._MeasuresTilt.RemoveRange(1, item._MeasuresTilt.Count - 1);
                //       item.AverageTiltCalculation();
                //}
                List<object> row = new List<object>();
                row.Add(R.Tilt_Delete); // 0 Remove
                row.Add(""); // 1 Point name
                row.Add(""); // 2 Dcum
                row.Add(""); // 3 TiltTheo
                row.Add(""); // 4 ReadingBeamTheo
                row.Add(""); // 5 BeamDirection mes 1
                row.Add(""); // 6 BeamOpposite mes 1
                row.Add(""); // 7 Average mes 1
                row.Add(""); // 8 Offset
                row.Add(""); // 9 Instrument
                row.Add(""); // 10 Interfaces
                row.Add(""); // 11 Save
                row.Add(""); // 12 Last Saving Time
                row.Add(""); // 13 Comment
                listRow.Add(row);
                listRow[listRow.Count - 1][1] = item.TheoPoint._Name;
                int decimalToShow = Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles - 1; //because 1CC = 0.0015 mrad
                string ds = "F" + decimalToShow.ToString();

                if (item.TheoPoint._Parameters.Cumul != Tsunami2.Preferences.Values.na)
                    listRow[listRow.Count - 1][2] = Tilt.Module.FormatDcum(item.TheoPoint._Parameters.Cumul);
                listRow[listRow.Count - 1][3] = Tilt.Module.FormatAngleInMrad(item.TheoPoint._Parameters.Tilt);
                if (item._EcartTheo.Value != 9999)
                    listRow[listRow.Count - 1][4] = Tilt.Module.FormatAngleInMrad(item._MeasureAverageBeamDirection.Value - item._EcartTheo.Value);
                if (item._MeasureAverageBeamDirection.Value != 9999)
                    listRow[listRow.Count - 1][5] = Tilt.Module.FormatAngleInMrad(item._MeasureAverageBeamDirection.Value);
                if (item._MeasureAverageOppositeDirection.Value != 9999)
                    listRow[listRow.Count - 1][6] = Tilt.Module.FormatAngleInMrad(item._MeasureAverageOppositeDirection.Value);
                if (item._MeasureAverageBothDirection.Value != 9999)
                    listRow[listRow.Count - 1][7] = Tilt.Module.FormatAngleInMrad(item._MeasureAverageBothDirection.Value);
                if (item._EcartTheo.Value != 9999)
                    listRow[listRow.Count - 1][8] = Tilt.Module.FormatAngleInMrad(item._EcartTheo.Value);
                listRow[listRow.Count - 1][9] = item.TiltParameters._Instrument._Name;
                listRow[listRow.Count - 1][10] = item._MeasuresTilt[0].Interfaces.ToString();
                if (item.ParametersBasic.LastSaved != DateTime.MinValue)
                {
                    if (item.TiltParameters._State is State.StationTiltSaved)
                    {
                        listRow[listRow.Count - 1][11] = R.Tilt_Saved;
                    }
                    else
                    {
                        listRow[listRow.Count - 1][11] = R.Tilt_Save;
                    }
                }
                else
                {
                    if (item._MeasureAverageBothDirection.Value != 9999)
                    {
                        listRow[listRow.Count - 1][11] = R.Tilt_Save;
                    }
                    else
                    {
                        listRow[listRow.Count - 1][11] = R.Tilt_Save_Blank;
                    }

                }
                //Last saving time
                if (item.ParametersBasic.LastSaved != DateTime.MinValue)
                {
                    CultureInfo ci = CultureInfo.InvariantCulture;
                    listRow[listRow.Count - 1][12] = item.ParametersBasic.LastSaved.ToString("HH:mm:ss", ci);
                }
                listRow[listRow.Count - 1][13] = item.TiltParameters._Comment;
            }

            private void dataGridViewMesures_CellValueChanged(object sender, DataGridViewCellEventArgs e)
            //événement lié si on change une mesure dans le tableau
            {
                if (e.RowIndex < dataGridViewMesures.Rows.Count && e.RowIndex != -1)
                {
                    rowActiveCell = e.RowIndex;
                }
                if (e.ColumnIndex < dataGridViewMesures.Columns.Count && e.ColumnIndex != -1)
                {
                    columnActiveCell = e.ColumnIndex;
                }
                /// mesure sens faisceau entree colonne 5

                if (e.RowIndex >= 0 && e.ColumnIndex == 5)
                {
                    if (dataGridViewMesures[e.ColumnIndex, e.RowIndex].Value != null)
                    {
                        double datagridNumber = T.Conversions.Numbers.ToDouble(dataGridViewMesures[e.ColumnIndex, e.RowIndex].Value.ToString(), true, -9999);
                        tiltDirection = TiltDirection.Beam;
                        this.SearchTilt(dataGridViewMesures[1, e.RowIndex].Value.ToString());
                        if (datagridNumber != -9999)
                        {
                            if (this._stationTiltModule._ActualStationTilt._MeasuresTilt[0]._ReadingBeamDirection.Value != datagridNumber / 1000)
                            {
                                //encode la mesure du tilt sens faisceau 
                                this._stationTiltModule._ActualStationTilt._MeasuresTilt[0]._ReadingBeamDirection.Value = datagridNumber / 1000;
                                // Récupère le sigma pour les mesures faites par un Wyler
                                if (dataGridViewMesures[e.ColumnIndex, e.RowIndex].Tag is DoubleValue dv
                                    && Math.Abs(dv.Value - datagridNumber) < 0.001d)
                                    this._stationTiltModule._ActualStationTilt._MeasuresTilt[0]._ReadingBeamDirection.Sigma = dv.Sigma / 1000;
                                this._stationTiltModule._ActualStationTilt.ParametersBasic.LastChanged = DateTime.Now;
                                //sélectionne la case tilt inverse pour encoder le tilt
                                columnActiveCell++;
                                this.setCurrentCell = true;
                                this._stationTiltModule.UpdateOffset();
                                this.SetCurrentCell();
                                return;
                            }
                            else
                            {
                                //remet l'ancienne valeur dans la case au bon format
                                this.dataGridViewMesures.CellValueChanged -= this.dataGridViewMesures_CellValueChanged;
                                this.dataGridViewMesures[e.ColumnIndex, e.RowIndex].Value = Tilt.Module.FormatAngleInMrad(this._stationTiltModule._ActualStationTilt._MeasuresTilt[0]._ReadingBeamDirection.Value);
                                this.dataGridViewMesures.CellValueChanged += new DataGridViewCellEventHandler(this.dataGridViewMesures_CellValueChanged);
                                return;
                            }
                        }
                        else
                        {
                            //message erreur, valeur encodée incorrecte
                            this.ShowMessage(R.StringTilt_WrongRoll);
                            //remet l'ancienne valeur dans la case au bon format
                            this.dataGridViewMesures.CellValueChanged -= this.dataGridViewMesures_CellValueChanged;
                            this.dataGridViewMesures[e.ColumnIndex, e.RowIndex].Value = Tilt.Module.FormatAngleInMrad(this._stationTiltModule._ActualStationTilt._MeasuresTilt[0]._ReadingBeamDirection.Value);
                            this.dataGridViewMesures.CellValueChanged += new DataGridViewCellEventHandler(this.dataGridViewMesures_CellValueChanged);
                            return;
                        }
                    }
                    else
                    {
                        //contenu de la case effacé
                        tiltDirection = TiltDirection.Beam;
                        this.SearchTilt(dataGridViewMesures[1, e.RowIndex].Value.ToString());
                        this._stationTiltModule.ResetMeasForBeamDirectionReadingDeleted(0);
                        this._stationTiltModule._ActualStationTilt.ParametersBasic.LastChanged = DateTime.Now;
                        columnActiveCell++;
                        this.setCurrentCell = true;
                        this._stationTiltModule.UpdateOffset();
                        this.SetCurrentCell();
                        return;
                    }
                }
                /// mesure sens inverse faisceau entree colonne 6
                if (e.RowIndex >= 0 && e.ColumnIndex == 6)
                {
                    if (dataGridViewMesures[e.ColumnIndex, e.RowIndex].Value != null)
                    {
                        double datagridNumber = T.Conversions.Numbers.ToDouble(dataGridViewMesures[e.ColumnIndex, e.RowIndex].Value.ToString(), true, -9999);
                        tiltDirection = TiltDirection.Opposite;
                        this.SearchTilt(dataGridViewMesures[1, e.RowIndex].Value.ToString());
                        if (datagridNumber != -9999)
                        {
                            if (this._stationTiltModule._ActualStationTilt._MeasuresTilt[0]._ReadingOppositeBeam.Value != datagridNumber / 1000)
                            {
                                //encode la mesure du tilt sens faisceau 
                                this._stationTiltModule._ActualStationTilt._MeasuresTilt[0]._ReadingOppositeBeam.Value = datagridNumber / 1000;
                                // Récupère le sigma pour les mesures faites par un Wyler
                                if (dataGridViewMesures[e.ColumnIndex, e.RowIndex].Tag is DoubleValue dv
                                    && Math.Abs(dv.Value - datagridNumber) < 0.001d)
                                    this._stationTiltModule._ActualStationTilt._MeasuresTilt[0]._ReadingOppositeBeam.Sigma = dv.Sigma / 1000;
                                this._stationTiltModule._ActualStationTilt.ParametersBasic.LastChanged = DateTime.Now;
                                //Sélectionne le tilt faisceau de l'élément suivant
                                if (rowActiveCell < dataGridViewMesures.RowCount - 1)
                                {
                                    columnActiveCell = 5;
                                    rowActiveCell++;
                                }
                                this.setCurrentCell = true;
                                this._stationTiltModule.UpdateOffset();
                                this.SetCurrentCell();
                                return;
                            }
                            else
                            {
                                //remet l'ancienne valeur dans la case au bon format
                                this.dataGridViewMesures.CellValueChanged -= this.dataGridViewMesures_CellValueChanged;
                                this.dataGridViewMesures[e.ColumnIndex, e.RowIndex].Value = Tilt.Module.FormatAngleInMrad(this._stationTiltModule._ActualStationTilt._MeasuresTilt[0]._ReadingOppositeBeam.Value);
                                this.dataGridViewMesures.CellValueChanged += new DataGridViewCellEventHandler(this.dataGridViewMesures_CellValueChanged);
                                return;
                            }

                        }
                        else
                        {
                            //message erreur, valeur encodée incorrecte
                            this.ShowMessage(R.StringTilt_WrongRoll);
                            //remet l'ancienne valeur dans la case au bon format
                            this.dataGridViewMesures.CellValueChanged -= this.dataGridViewMesures_CellValueChanged;
                            this.dataGridViewMesures[e.ColumnIndex, e.RowIndex].Value = Tilt.Module.FormatAngleInMrad(this._stationTiltModule._ActualStationTilt._MeasuresTilt[0]._ReadingOppositeBeam.Value);
                            this.dataGridViewMesures.CellValueChanged += new DataGridViewCellEventHandler(this.dataGridViewMesures_CellValueChanged);
                            return;
                        }
                    }
                    else
                    {
                        //contenu de la case effacé
                        tiltDirection = TiltDirection.Opposite;
                        this.SearchTilt(dataGridViewMesures[1, e.RowIndex].Value.ToString());
                        this._stationTiltModule.ResetMeasForBeamOppositeDirectionReadingDeleted(0);
                        this._stationTiltModule._ActualStationTilt.ParametersBasic.LastChanged = DateTime.Now;
                        //Sélectionne le tilt faisceau de l'élément suivant
                        if (rowActiveCell < dataGridViewMesures.RowCount - 1)
                        {
                            columnActiveCell = 5;
                            rowActiveCell++;
                        }
                        this.setCurrentCell = true;
                        this._stationTiltModule.UpdateOffset();
                        this.SetCurrentCell();
                        return;
                    }
                }
                //commentaire entré
                if (e.RowIndex >= 0 && e.ColumnIndex == 13)
                {
                    if (e.RowIndex >= 0)
                    {
                        //Sélectionne le commentaire de l'élement suivant
                        if (rowActiveCell < dataGridViewMesures.RowCount - 1)
                        {
                            columnActiveCell = 13;
                            rowActiveCell++;
                        }
                        this.setCurrentCell = true;
                    }
                    if (dataGridViewMesures[e.ColumnIndex, e.RowIndex].Value != null)
                    {
                        string newComment = dataGridViewMesures[e.ColumnIndex, e.RowIndex].Value.ToString();
                        if (!newComment.Contains(";") && !newComment.Contains("%") && !newComment.Contains("$")) // tsu-3144&& (System.Text.RegularExpressions.Regex.IsMatch(newComment, @"^[a-zA-Z0-9 ]+$"))))
                        {
                            ////Vérifie que le commentaire est de maximum 17 caractères (suppression de la limite de longueur du commentaire)
                            //if (newComment.Length < 88)
                            //{
                            this.SearchTilt(dataGridViewMesures[1, e.RowIndex].Value.ToString());
                            this._stationTiltModule.ChangeComment(newComment);
                            return;
                            //}
                            //else
                            //{
                            //    this.ShowMessage(R.StringTilt_WrongTooLongComment87);
                            //    this.UpdateView();
                            //    return;
                            //}
                        }
                        else
                        {
                            this.ShowMessage(R.StringTilt_WrongCommentSemicolon);
                            this.UpdateView();
                            return;
                        }
                    }
                    else
                    {
                        //Commentaire effacé
                        this.SearchTilt(dataGridViewMesures[1, e.RowIndex].Value.ToString());
                        this._stationTiltModule.ChangeComment("");
                        return;
                    }

                }


                //this.dataGridViewMesures.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewMesures_CellValueChanged);

            }
            private void dataGridViewMeasure_CellClick(object sender, DataGridViewCellEventArgs e)
            //événements si on clique dans le tableau (effacement points, fil)
            {

                Debug.WriteInConsole($"cell clicked");

                if (dataGridViewMesures.IsCurrentCellInEditMode) { return; }
                //garde en mémoire la cellule active
                if (e.RowIndex < dataGridViewMesures.Rows.Count && e.RowIndex != -1)
                {
                    rowActiveCell = e.RowIndex;
                }
                if (e.ColumnIndex < dataGridViewMesures.Columns.Count && e.ColumnIndex != -1)
                {
                    columnActiveCell = e.ColumnIndex;
                }
                if (e.RowIndex == -1 || e.ColumnIndex == -1) { this.SetCurrentCell(); }
                else
                {
                    string actualPointName = this._stationTiltModule._ActualStationTilt.TheoPoint._Name;
                    this.SearchTilt(dataGridViewMesures[1, e.RowIndex].Value.ToString());
                    //measureSelected = true;
                    //measureNumber = 0;
                    switch (e.ColumnIndex)
                    {
                        case 0:
                            ///Point effacé
                            string titleAndMessage = String.Format(R.StringTilt_ConfirmDeletePoint, dataGridViewMesures[1, e.RowIndex].Value.ToString());
                            MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                            {
                                ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL }
                            };
                            string respond = mi.Show().TextOfButtonClicked;
                            if (respond == R.T_OK)
                            {
                                this.AskIfStationTiltHasToBeSaved();
                                this._stationTiltModule.DeletePoint(dataGridViewMesures[1, e.RowIndex].Value.ToString());
                            }
                            break;
                        case 5:
                            tiltDirection = TiltDirection.Beam;
                            //this.EnterSeveralMeasure(e);
                            if (this._stationTiltModule._ActualStationTilt._MeasuresTilt.Count > 1)
                            {
                                this.EnterTiltInDatagridPopUp();
                                //Sélectionne le tilt faisceau de l'élément suivant
                                if (rowActiveCell < dataGridViewMesures.RowCount - 1)
                                {
                                    columnActiveCell = 5;
                                    rowActiveCell++;
                                }
                                this.SetCurrentCell();
                            }
                            break;
                        case 6:
                            tiltDirection = TiltDirection.Opposite;
                            //this.EnterSeveralMeasure(e);
                            if (this._stationTiltModule._ActualStationTilt._MeasuresTilt.Count > 1)
                            {
                                this.EnterTiltInDatagridPopUp();
                                //Sélectionne le tilt faisceau de l'élément suivant
                                if (rowActiveCell < dataGridViewMesures.RowCount - 1)
                                {
                                    columnActiveCell = 5;
                                    rowActiveCell++;
                                }
                                this.SetCurrentCell();
                            }

                            break;
                        case 9:
                            //selection instrument 
                            //this.SearchTilt(dataGridViewMesures[1,e.RowIndex].Value.ToString());
                            this._stationTiltModule.SelectionTiltMeter();
                            break;
                        case 10:
                            this._stationTiltModule.ChangeInterfaces();

                            break;
                        case 11:
                            //sauvegarde la mesure 
                            if (dataGridViewMesures[1, e.RowIndex].Value.ToString() != "")
                            {
                                if (e.RowIndex < dataGridViewMesures.Rows.Count && e.RowIndex != -1)
                                {
                                    rowActiveCell = e.RowIndex;
                                }
                                if (this._stationTiltModule._ActualStationTilt.TiltParameters._State is State.StationTiltReadyToBeSaved)
                                {
                                    this._stationTiltModule.ExportToGeode(false, true);
                                }
                            }
                            break;
                        default:
                            break;
                    }
                    if (this._stationTiltModule._ActualStationTilt.TheoPoint._Name != actualPointName) this.RedrawTreeviewParameters();
                }
            }

            private void dataGridViewMesures_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
            {
                //garde en mémoire la cellule active
                if (e.RowIndex < dataGridViewMesures.Rows.Count && e.RowIndex != -1)
                {
                    rowActiveCell = e.RowIndex;
                }
                if (e.ColumnIndex < dataGridViewMesures.Columns.Count && e.ColumnIndex != -1)
                {
                    columnActiveCell = e.ColumnIndex;
                }
                if (e.RowIndex == -1 || e.ColumnIndex == -1) { this.SetCurrentCell(); }
                else
                {
                    string actualPointName = this._stationTiltModule._ActualStationTilt.TheoPoint._Name;
                    this.SearchTilt(dataGridViewMesures[1, e.RowIndex].Value.ToString());
                    //measureSelected = true;
                    //measureNumber = 0;
                    switch (e.ColumnIndex)
                    {
                        case 5:
                            tiltDirection = TiltDirection.Beam;
                            this.EnterTiltInDatagridPopUp();
                            //Sélectionne le tilt faisceau de l'élément suivant
                            if (rowActiveCell < dataGridViewMesures.RowCount - 1)
                            {
                                columnActiveCell = 5;
                                rowActiveCell++;
                            }
                            this.SetCurrentCell();
                            break;
                        case 6:
                            tiltDirection = TiltDirection.Opposite;
                            this.EnterTiltInDatagridPopUp();
                            //Sélectionne le tilt faisceau de l'élément suivant
                            if (rowActiveCell < dataGridViewMesures.RowCount - 1)
                            {
                                columnActiveCell = 5;
                                rowActiveCell++;
                            }
                            this.SetCurrentCell();
                            break;
                        default:
                            break;
                    }
                    if (this._stationTiltModule._ActualStationTilt.TheoPoint._Name != actualPointName) this.RedrawTreeviewParameters();
                }
            }
            ///// <summary>
            ///// Demande l'entrée de plusieurs mesures de tilt dans le cas ou l'on a plusieurs tilt par élément
            ///// </summary>
            ///// <param name="e"></param>
            //private void EnterSeveralMeasure(DataGridViewCellEventArgs e)
            //{
            //    if (this._stationTiltModule._ActualStationTilt._MeasuresTilt.Count > 1)
            //    {
            //        double previousValue;
            //        this.measureNumber = 0;
            //        MeasureOfTilt newMeasTilt = new MeasureOfTilt();
            //        foreach (MeasureOfTilt m in this._stationTiltModule._ActualStationTilt._MeasuresTilt)
            //        {
            //            this.tiltDirection = TSU.ENUM.TiltDirection.Beam;
            //            previousValue = this._stationTiltModule._ActualStationTilt._MeasuresTilt[this.measureNumber]._ReadingBeamDirection.Value;
            //            if (this.EnterTiltValue(previousValue, newMeasTilt, this.measureNumber) != R.T_CANCEL)
            //            {
            //                this.AddInstrumentMeasure(newMeasTilt, false);
            //            }
            //            else
            //            {
            //                // si clique cancel arrete la demande de mesure
            //                this._stationTiltModule.UpdateOffset();
            //                return;
            //            }
            //            //Demande automatique l'autre direction lors mesure d'un tilt
            //            this.tiltDirection = TSU.ENUM.TiltDirection.Opposite;
            //            previousValue = this._stationTiltModule._ActualStationTilt._MeasuresTilt[this.measureNumber]._ReadingOppositeBeam.Value;
            //            if (this.EnterTiltValue(previousValue, newMeasTilt, this.measureNumber) != R.T_CANCEL)
            //            {
            //                this.AddInstrumentMeasure(newMeasTilt, false);
            //            }
            //            else
            //            {
            //                // si clique cancel arrete la demande de mesure
            //                this._stationTiltModule.UpdateOffset();
            //                return;
            //            }
            //            this.measureNumber++;
            //        }
            //        //Sélectionne le tilt faisceau de l'élément suivant
            //        if (rowActiveCell < dataGridViewMesures.RowCount - 1)
            //        {
            //            columnActiveCell = 3;
            //            rowActiveCell++;
            //        }
            //        this.setCurrentCell = true;
            //        this._stationTiltModule.UpdateOffset();
            //    }
            //}
            /// <summary>
            /// Click sur entete d'une colonne fait le tri par dcum ou inverse dcum
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void dataGridViewMesures_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
            {
                DataGridView dgv = sender as DataGridView;
                Debug.WriteInConsole($"datagridView sorted by {dgv.SortOrder} {dgv.SortedColumn}");

                if (dataGridViewMesures.IsCurrentCellInEditMode) { return; }
            }
            
            /// <summary>
            /// Evenement qui arrive quand on change la current cell
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void dataGridViewMesure_CurrentCellChanged(object sender, EventArgs e)
            {

                DataGridView dgv = sender as DataGridView;
                Debug.WriteInConsole($"Current Cell Changed");

                //permet de détecter quand le datagridview change automatiquement la ligne courante après avoir entré une valeur dans une case.  
                if (setCurrentCell && this.dataGridViewMesures.CurrentCell != null)
                {

                    ////BeginInvoke crèe un processus asynchrone qui empêche de tourner en boucle lorsqu'on change la current cell
                    Delegate delegateCurrentCell = new MethodInvoker(() =>
                    {
                        //this.Module.InvokeOnApplicationDispatcher(SetCurrentCell);
                        this.SetCurrentCell();
                    });
                    IAsyncResult async = this.BeginInvoke(delegateCurrentCell);
                    this.dataGridViewMesures.Focus();
                    this.setCurrentCell = false;
                }
            }
            private void dataGridViewMesures_MouseClick(object sender, MouseEventArgs e)

            //affiche le context menu si on clique droit
            {
                //if (e.Button == System.Windows.Forms.MouseButtons.Right)
                //{
                //    //this.RefreshGlobalContextMenuItemCollection();
                //    this.ShowPopUpMenu();
                //}
            }
            /// <summary>
            /// Pour forcer le datagrid à reprendre le focus dans module guidé
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void dataGridViewMesures_MouseEnter(object sender, EventArgs e)
            {
                //DataGridView dgv = sender as DataGridView;
                //TSU.Debug.WriteInConsole($"DgV mouse enter");

                //Action a = () =>
                //{
                //    dataGridViewMesures.Focus();
                //};
                //Tsunami2.Properties.InvokeOnApplicationDispatcher(a);
            }
            /// <summary>
            /// Check if datagrid is in edit mode and validate it before leaving
            /// </summary>
            internal void CheckDatagridIsInEditMode()
            {
                if (dataGridViewMesures != null)
                {
                    if (dataGridViewMesures.IsCurrentCellInEditMode)
                    {
                        //this.dataGridViewLevel.CellValueChanged -= this.dataGridViewLevel_CellValueChanged;
                        int saveRow = rowActiveCell;
                        int saveColumn = columnActiveCell;
                        //Try car de temps en temps endEdit peut faire une exception
                        try
                        {
                            dataGridViewMesures.EndEdit();

                        }
                        catch (Exception ex)
                        {
                            ex.Data.Clear();
                        }
                        //this.dataGridViewLevel.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewLevel_CellValueChanged);
                        rowActiveCell = saveRow;
                        columnActiveCell = saveColumn;
                    }
                }
            }
            /// <summary>
            /// search the tilt station with the point name associated
            /// </summary>
            /// <param name="pointName"></param>
            internal void SearchTilt(string pointName)
            {
                this._stationTiltModule._ActualStationTilt = this._stationTiltModule._StationTiltSequences.Find(
                    x => x.TheoPoint._Name == pointName 
                      && x.TiltParameters._TiltInitOrAdjusting == this._stationTiltModule._ActualStationTilt.TiltParameters._TiltInitOrAdjusting
                );
                this._stationTiltModule._InitStationTilt = this._stationTiltModule._StationTiltSequences.Find(
                    x => x.TheoPoint._Name == pointName 
                      && x.TiltParameters._TiltInitOrAdjusting == TiltInitOrAdjusting.Initial
                );
                this._stationTiltModule._AdjustingStationTilt = this._stationTiltModule._StationTiltSequences.Find(
                    x => x.TheoPoint._Name == pointName 
                      && x.TiltParameters._TiltInitOrAdjusting == TiltInitOrAdjusting.Adjusting);
                this._stationTiltModule._Station = this._stationTiltModule._ActualStationTilt;
            }
            /// <summary>
            /// Set the current cell in datagridview as the columnactivecell and rowactivecell
            /// </summary>
            internal void SetCurrentCell()
            {
                Action a = () =>
                {
                    this.dataGridViewMesures.CurrentCellChanged -= this.dataGridViewMesure_CurrentCellChanged;
                    if (rowActiveCell == -1 && this.dataGridViewMesures.RowCount > 0)
                    {
                        rowActiveCell = 0;
                        columnActiveCell = 5;
                    }
                    if (columnActiveCell == -1 && this.dataGridViewMesures.RowCount > 0)
                    {
                        rowActiveCell = 0;
                        columnActiveCell = 5;
                    }
                    if (columnActiveCell != 5 && columnActiveCell != 6 && columnActiveCell != 12 && this.dataGridViewMesures.RowCount > 0)
                    {
                        columnActiveCell = 5;
                    }
                    dataGridViewMesures.Focus();
                    dataGridViewMesures.ClearSelection();
                    if ((columnActiveCell < this.dataGridViewMesures.ColumnCount) && (columnActiveCell >= 0) && (rowActiveCell < this.dataGridViewMesures.RowCount) && (rowActiveCell >= 0))
                    {
                        dataGridViewMesures.CurrentCell = this.dataGridViewMesures[columnActiveCell, rowActiveCell];
                        dataGridViewMesures.Rows[rowActiveCell].Cells[columnActiveCell].Selected = true;

                        //dataGridViewMesures.BeginEdit(true);
                    }
                    this.dataGridViewMesures.CurrentCellChanged += new EventHandler(this.dataGridViewMesure_CurrentCellChanged);
                };
                Tsunami2.Properties.InvokeOnApplicationDispatcher(a);
            }

            private void SetFirstDisplayedRowIndex(int rowIndex)
            {

                // Use BeginInvoke to defer the operation to the UI thread
                dataGridViewMesures.BeginInvoke((MethodInvoker)delegate
                {
                    try
                    {
                        try
                        {
                            dataGridViewMesures.FirstDisplayedScrollingRowIndex = rowIndex;
                        }
                        catch (Exception ex)
                        {
                            Logs.Log.AddEntryAsPayAttentionOf(this.Module, $"Cannot reset the datagridView to the row #{rowIndex} {ex}");
                        }
                    }
                    catch (Exception ex)
                    {
                        Logs.Log.AddEntryAsPayAttentionOf(this.Module, $"Cannot reset the datagridView to the row #{rowIndex} {ex}");
                    }
                });
            }

            /// <summary>
            /// Met la scroll bar horizontale à gauche et la vertical en haut
            /// </summary>
            internal void SetScrollBarAtZero()
            {
                if (this.dataGridViewMesures.Columns.Count > 0 && this.dataGridViewMesures.Rows.Count > 0)
                {
                    if (this.dataGridViewMesures.Columns[0].Visible && this.dataGridViewMesures.Rows[0].Visible)
                    {
                        this.dataGridViewMesures.FirstDisplayedScrollingColumnIndex = 0;
                        SetFirstDisplayedRowIndex(0);
                    }
                    else
                    {
                        if (this.dataGridViewMesures.Columns.Count > 1 && this.dataGridViewMesures.Rows.Count > 0)
                        {
                            if (this.dataGridViewMesures.Columns[1].Visible && this.dataGridViewMesures.Rows[0].Visible)
                            {
                                this.dataGridViewMesures.FirstDisplayedScrollingColumnIndex = 1;
                                SetFirstDisplayedRowIndex(0);
                            }
                        }
                    }
                }
            }
            ///// <summary>
            ///// garde le focus sur la current cell dans le datagrid
            ///// </summary>
            ///// <param name="sender"></param>
            ///// <param name="e"></param>
            //private void Timer_Keep_Cell_Focus_Tick(object sender, EventArgs e)
            //{
            //    this.Timer_Keep_Cell_Focus.Enabled = false;
            //    this.Timer_Keep_Cell_Focus.Stop();
            //    this.Timer_Keep_Cell_Focus.Dispose();
            //    //MouseEventArgs eventCell = new MouseEventArgs(MouseButtons.Left,1,dataGridViewMesures.Size.Width, dataGridViewMesures.Size.Height, 0);
            //    //dataGridViewMesures_MouseClick(this.dataGridViewMesures, eventCell);

            //    //SetCurrentCell();
            //}
            #endregion
            #region Splitcontainer 2
            public void CreateSplitContainer2()
            {
                this.splitContainer2 = new SplitContainer();

                Tsunami2.Preferences.Theme.ApplyTo(splitContainer2);
            }
            public void InitializeSplitContainer2()
            {
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
                this.splitContainer2.Panel1.SuspendLayout();
                this.splitContainer2.Panel2.SuspendLayout();
                this.splitContainer2.SuspendLayout();
                this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
                // 
                // splitContainer2
                // 
                this.splitContainer2.Dock = DockStyle.Fill;
                this.splitContainer2.FixedPanel = FixedPanel.Panel1;
                this.splitContainer2.IsSplitterFixed = false;
                this.splitContainer2.Location = new System.Drawing.Point(0, 0);
                this.splitContainer2.Name = "splitContainer2";
                this.splitContainer2.Orientation = Orientation.Horizontal;
                // 
                // splitContainer2.Panel1
                // 
                //this.splitContainer2.Panel1.Controls.Add(this.tableLayoutPanel_ButtonTop);
                // 
                // splitContainer2.Panel2
                // 
                //this.splitContainer2.Panel2.Controls.Add(this.dataGridViewMesures);
                //this.splitContainer2.Size = new System.Drawing.Size(1202, 506);
                this.splitContainer2.SplitterDistance = 186;
                this.splitContainer2.TabIndex = 0;
                this.splitContainer2.Panel1.ResumeLayout(false);
                this.splitContainer2.Panel1.PerformLayout();
                this.splitContainer2.Panel2.ResumeLayout(false);
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            }
            public void UpdateDockStyle()
            {
                if (this.splitContainer2 != null)
                {
                    this.splitContainer2.Dock = DockStyle.None;
                    this.splitContainer2.Height = this.splitContainer1.Panel1.Height;
                    this.splitContainer2.Width = this.splitContainer1.Panel1.Width;
                    this.splitContainer2.Dock = DockStyle.Fill;
                }
                if (this.dataGridViewMesures != null)
                {
                    this.dataGridViewMesures.Dock = DockStyle.None;
                    this.dataGridViewMesures.Dock = DockStyle.Fill;
                }
                if (this.treeView_Parameters != null)
                {
                    this.treeView_Parameters.Dock = DockStyle.None;
                    this.treeView_Parameters.Dock = DockStyle.Fill;
                }
            }
            private void splitContainer1_SizeChanged(object sender, EventArgs e)
            {
                this.UpdateDockStyle();
            }
            #endregion
            #region Methods

            /// <summary>
            /// Mise à jour taille boutons dans le panel top
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void _PanelTop_Resize(object sender, EventArgs e)
            {
                this.UpdateView();
            }
            /// <summary>
            /// Creation des boutons du TableLayoutTOp
            /// </summary>
            //private void CreateTableLayoutTopButtonS()
            //{
            //    bigButton_NextPointInSequence = new BigButton(
            //    R.StringTilt_NextPoint,
            //    R.Next, buttonNextPoint_Click);
            //    bigButton_NextPointInSequence.Available = true;
            //    //bigButton_NextPointInSequence.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            //    //bigButton_NextPointInSequence.Dock = DockStyle.Left;
            //    bigButton_PreviousPointInSequence = new BigButton(
            //   R.StringTilt_PreviousPoint,
            //   R.Previous, buttonPreviousPoint_Click);
            //    bigButton_PreviousPointInSequence.Available = true;
            //    //bigButton_PreviousPointInSequence.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            //    //bigButton_PreviousPointInSequence.Dock = DockStyle.Left;
            //    bigButton_saveAll = new BigButton(
            //   R.StringTilt_ExportAllUnknown,
            //   R.Save,
            //    buttonSaveAll_Click);
            //    bigButton_saveAll.Available = false;
            //    //bigButton_saveAll.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            //    //bigButton_saveAll.Dock = DockStyle.Left;
            //    this.tableLayoutPanel_ButtonTop.Controls.Clear();
            //    this.tableLayoutPanel_ButtonTop.Controls.Add(bigButton_PreviousPointInSequence,0,0);
            //    this.tableLayoutPanel_ButtonTop.Controls.Add(bigButton_NextPointInSequence, 2, 0);
            //    this.tableLayoutPanel_ButtonTop.Controls.Add(bigButton_saveAll, 1, 0);
            //}
            private void CreateContextMenuButtons()
            //creation des boutons utilisés dans les menus contextuels
            {
                bigButton_Elements = new BigButton(
                   R.StringTilt_ElementSelection,
                   R.Open, buttonSelectPoint_Click);

                //bigButton_AddMeas = new BigButton(
                //    R.StringTilt_AddMeasure,
                //    R.Add, buttonAddMeas_Click);
                //bigButton_DelMeas = new BigButton(
                //   string.Format(R.StringTilt_DeleteMeasure, measureNumber),
                //    R.Tilt_Delete, buttonDelMeas_Click);
                //bigButton_saveInitial = new BigButton(
                //   R.StringTilt_ExportInitial,
                //   R.Save,
                //    buttonSaveInitial_Click);
                //bigButton_saveAdjusting = new BigButton(
                //   R.StringTilt_ExportAdjusting,
                //   R.Save,
                //    buttonSaveAdjusting_Click);

                bigButton_saveAll = new BigButton(
                R.StringTilt_ExportAllButton,
                R.Save,
                bigButtonSaveAll_Click);

                //bigButton_DeleteSequence = new BigButton(
                //   string.Format(R.StringTilt_DeletePoint,pointNameSelected),
                //   R.Tilt_Delete, buttonDelSeq_Click);

                bigButton_Sort = new BigButton(
                   R.StringTilt_Sort,
                   R.Vertical,
                    buttonSort_Click);

                bigButton_Cancel = new BigButton(
                   R.StringTilt_CancelMenu,
                   R.Cancel, buttonCancel_Click);
                //bigButton_ChangePtTheo = new BigButton(
                //   R.StringTilt_ChangePtTheo,
                //   R.Selected,
                //    buttonChangePtTheo_Click);

                bigButton_ShowInitDatFile = new BigButton(
                R.StringTilt_OpenInitDatFile,
                R.Export,
                bigButton_ShowInitDatFile_Click);

                bigButton_ShowAdjDatFile = new BigButton(
                R.StringTilt_OpenAdjDatFile,
                R.Export,
                bigButton_ShowAdjDatFile_Click);

                bigButton_SwitchBetweenRollType = new BigButton(
                R.StringTilt_SwitchRollType,
                R.Tilt_Angle_Both,
                bigButton_SwitchBetweenRollType_Click);

                bigButton_Help = new BigButton(
                "Help;Show graphical help",
                R.MessageTsu_Interrogation,
                button_Help_Click);
            }

            private void ShowContextMenuGlobal()
            {
                this.CheckDatagridIsInEditMode();
                //creation de la liste des boutons de menu
                contextButtons.Clear();
                //this.SetSaveButtonAvailibility();
                //Dans le module guidé n'affiche pas le context menu
                switch (this.moduleType)
                {
                    case ModuleType.Guided:
                        break;
                    case ModuleType.Advanced:
                        if (this._stationTiltModule._ActualStationTilt.TiltParameters._Instrument._Name != R.String_Unknown) contextButtons.Add(bigButton_Elements);

                        //if (this.pointSelected == true
                        //        && this._stationTiltModule._StationTiltSequences.Count > 2)
                        //    {
                        //        contextButtons.Add(bigButton_ChangePtTheo);
                        //    }    

                        //if (this.pointSelected == true 
                        //        && this._stationTiltModule._StationTiltSequences.Count > 1)
                        //    {
                        //        int pointInitIndex = this._stationTiltModule._StationTiltSequences.FindIndex(x => x.TheoPoint._Name == pointNameSelected && x.TiltParameters._TiltInitOrAdjusting == TiltInitOrAdjusting.Initial);
                        //        int pointAdjIndex = this._stationTiltModule._StationTiltSequences.FindIndex(x => x.TheoPoint._Name == pointNameSelected && x.TiltParameters._TiltInitOrAdjusting == TiltInitOrAdjusting.Adjusting);
                        //        if (pointInitIndex!=-1 && pointAdjIndex!=-1)
                        //        {
                        //            int indexInit = this._stationTiltModule._StationTiltSequences[pointInitIndex]._MeasuresTilt.FindIndex(x => x._AverageBothDirection.Value != na);
                        //            int indexAdj = this._stationTiltModule._StationTiltSequences[pointAdjIndex]._MeasuresTilt.FindIndex(x => x._AverageBothDirection.Value != na);
                        //            if (indexInit == -1 && indexAdj == -1)
                        //            {
                        //                contextButtons.Add(bigButton_DeleteSequence);
                        //            }
                        //        }
                        //    }
                        contextButtons.Add(bigButton_SwitchBetweenRollType);
                        contextButtons.Add(bigButton_saveAll);
                        contextButtons.Add(bigButton_Help);

                        //if (showInitOp
                        //        && this._stationTiltModule._InitStationTilt.TiltParameters._State is TSU.Stations.Station.State.StationTiltReadyToBeSaved)
                        //        contextButtons.Add(bigButton_saveInitial);

                        int geodeInitFilePathExist = this._stationTiltModule._StationTiltSequences.FindIndex(x => x.TiltParameters._TiltInitOrAdjusting == TiltInitOrAdjusting.Initial && x.ParametersBasic._GeodeDatFilePath != "");
                        if (geodeInitFilePathExist != -1)
                            contextButtons.Add(bigButton_ShowInitDatFile);

                        //if (showAdjustingOp
                        //    && this._stationTiltModule._AdjustingStationTilt.TiltParameters._State is TSU.Stations.Station.State.StationTiltReadyToBeSaved )
                        //    contextButtons.Add(bigButton_saveAdjusting);

                        int geodeAdjFilePathExist = this._stationTiltModule._StationTiltSequences.FindIndex(x => x.TiltParameters._TiltInitOrAdjusting == TiltInitOrAdjusting.Adjusting && x.ParametersBasic._GeodeDatFilePath != "");
                        if (geodeAdjFilePathExist != -1)
                            contextButtons.Add(bigButton_ShowAdjDatFile);

                        if (this._stationTiltModule._StationTiltSequences.Count > 2) contextButtons.Add(bigButton_Sort);

                        //if (this.measureSelected == true) contextButtons.Add(bigButton_AddMeas);

                        //if ((this.delMeasAvailable == true)
                        //        && (this._stationTiltModule._StationTiltSequences.Count > 2)
                        //        && this.measureNumber != -1)
                        //    {
                        //        if (this._stationTiltModule._ActualStationTilt._MeasuresTilt[measureNumber]._AverageBothDirection.Value == 9999)
                        //        {
                        //            contextButtons.Add(bigButton_DelMeas);
                        //        }
                        //    }

                        contextButtons.Add(bigButton_Cancel);
                        break;
                    default:
                        break;
                }
                bool ok = this.ShowPopUpMenu(contextButtons);
                // il peut arriver lors de l'ajout d'un nouveau bouton que l'affichage des contrôles dans le popup menu ne soit pas dans l'ordre. Le réaffichage du pop up semble résoudre le problème.
                // Ajout 24-03-20 pour bouton default staff et secondary staff, ne le fait plus qu'une seule fois pour éviter une boucle infinie
                //if (!ok && !stopCheckingContextMenuOrder)
                //{
                //    stopCheckingContextMenuOrder = true;
                //    this.ShowContextMenuGlobal();
                //    stopCheckingContextMenuOrder = false;
                //}
            }
            private void AddWaitingView()
            // Add a waiting instrument module
            {
                TsuView v = new TsuView();
                v.BackgroundImage = R.Instrument;
                v.BackgroundImageLayout = ImageLayout.Zoom;
                v.ShowDockedFill();
                splitContainer1.Panel2.Controls.Add(v);
            }
            public override void UpdateView()
            {
                base.UpdateView();
                this.UpdateViewType();
                this.UpdateInstrumentView();
                if (dataGridViewMesures != null)
                {
                    this.SetCurrentCell();
                    this.Module.Change();
                }
                //this.dataGridViewMesures.CurrentCell = this.dataGridViewMesures[columnActiveCell, rowActiveCell];
            }
            /// <summary>
            /// change view type between treeview or datagridview
            /// </summary>
            /// <param name="viewType"></param>
            internal void ChangeViewType(TiltViewType viewType)
            {
                this.splitContainer1.Panel1.Controls.Clear();
                this.UpdateBigButtonTop();
                this.ViewType = viewType;
                switch (viewType)
                {
                    case TiltViewType.Treeview:
                        this.InitializeAdvancedViewWithTreeview();
                        break;
                    case TiltViewType.DataGridView:
                        this.InitializeGuidedViewWithDataGridView();
                        break;
                    default:
                        this.InitializeAdvancedViewWithTreeview();
                        break;
                }
                this.SetCurrentCell();
            }
            /// <summary>
            /// update datagridview or treeview following the view chose
            /// </summary>
            internal void UpdateViewType()
            {
                this.UpdateBigButtonTop();
                switch (this.ViewType)
                {
                    case TiltViewType.Treeview:
                        this.RedrawTreeviewParameters();
                        this.RedrawDataGridViewMesures();
                        this.SetCurrentCell();
                        break;
                    case TiltViewType.DataGridView:
                        this.RedrawDataGridViewMesures();
                        this.SetCurrentCell();
                        break;
                    default:
                        this.RedrawTreeviewParameters();
                        break;
                }
            }
            /// <summary>
            /// Cache la vue instrument
            /// </summary>
            internal void HideInstrumentView()
            {
                //this.splitContainer1.SplitterDistance = this.splitContainer1.Height;
            }
            /// <summary>
            /// update the middle button in the table layout panel on the top
            /// </summary>
            //private void UpdateTableLayoutPanel()
            //{
            //    string tiltMoy = "";
            //    string ecartTilt = "";
            //    if (this._stationTiltModule._ActualStationTilt._MeasureAverageBothDirection.Value == 9999)
            //    {
            //        tiltMoy = R.T_NA;
            //    }
            //    else
            //    {
            //        tiltMoy = Math.Round(this._stationTiltModule._ActualStationTilt._MeasureAverageBothDirection.Value * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            //    }
            //    if (this._stationTiltModule._ActualStationTilt._EcartTheo.Value == 9999)
            //    {
            //        ecartTilt = R.T_NA;
            //    }
            //    else
            //    {
            //        ecartTilt = Math.Round(this._stationTiltModule._ActualStationTilt._EcartTheo.Value * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            //    }
            //    string actualResult = "";
            //    if (this._stationTiltModule._ActualStationTilt._MeasureAverageBothDirection.Value != 9999)
            //    {
            //        switch (this._stationTiltModule._ActualStationTilt.TiltParameters._TiltInitOrAdjusting)
            //        {
            //            case TiltInitOrAdjusting.Initial:
            //                actualResult = R.StringTilt_Initial;
            //                break;
            //            case TiltInitOrAdjusting.Adjusting:
            //                actualResult = R.StringTilt_Adjusting;
            //                break;
            //            default:
            //                break;
            //        }
            //        actualResult += string.Format(R.StringTilt_Average_Offset, tiltMoy, ecartTilt);
            //    }
            //    if (this._stationTiltModule._ActualStationTilt.TheoPoint._Name != R.String_Unknown)
            //    {
            //        string classe = this._stationTiltModule._ActualStationTilt.TheoPoint._Class;
            //        string numero = this._stationTiltModule._ActualStationTilt.TheoPoint._Numero;
            //        this.tableLayoutPanel_ButtonTop.Controls.Clear();
            //        bigButton_saveAll = new BigButton(
            //        string.Format(R.StringTilt_ExportAll, classe, numero, Math.Round(this._stationTiltModule._ActualStationTilt.TheoPoint._Parameters.Tilt * 1000, 2).ToString()) + actualResult,
            //        R.Save,
            //        buttonSaveAll_Click);

            //        if (this._stationTiltModule._InitStationTilt.ParametersBasic.LastSaved >= this._stationTiltModule._InitStationTilt.ParametersBasic.LastChanged)
            //        {
            //            this.tableLayoutPanel_ButtonTop.Controls.Clear();
            //            bigButton_saveAll = new BigButton(
            //            string.Format(R.StringTilt_ExportAll, classe, numero, Math.Round(this._stationTiltModule._ActualStationTilt.TheoPoint._Parameters.Tilt * 1000, 2).ToString()) + actualResult,
            //             R.Tilt_Save_Init_Saved,
            //        buttonSaveAll_Click);
            //        }
            //        if (this._stationTiltModule._AdjustingStationTilt.ParametersBasic.LastSaved >= this._stationTiltModule._AdjustingStationTilt.ParametersBasic.LastChanged)
            //        {
            //            this.tableLayoutPanel_ButtonTop.Controls.Clear();
            //            bigButton_saveAll = new BigButton(
            //            string.Format(R.StringTilt_ExportAll, classe, numero, Math.Round(this._stationTiltModule._ActualStationTilt.TheoPoint._Parameters.Tilt * 1000, 2).ToString()) + actualResult,
            //            R.Tilt_Save_Adj_Saved,
            //            buttonSaveAll_Click);
            //        }
            //        if (this._stationTiltModule._AdjustingStationTilt.ParametersBasic.LastSaved >= this._stationTiltModule._AdjustingStationTilt.ParametersBasic.LastChanged
            //            && this._stationTiltModule._InitStationTilt.ParametersBasic.LastSaved >= this._stationTiltModule._InitStationTilt.ParametersBasic.LastChanged)
            //        {
            //            this.tableLayoutPanel_ButtonTop.Controls.Clear();
            //            bigButton_saveAll = new BigButton(
            //            string.Format(R.StringTilt_ExportAll, classe, numero, Math.Round(this._stationTiltModule._ActualStationTilt.TheoPoint._Parameters.Tilt * 1000, 2).ToString()) + actualResult,
            //            R.Tilt_Save_Both_Saved,
            //            buttonSaveAll_Click);
            //        }
            //    }
            //    if ((this._stationTiltModule._InitStationTilt.TiltParameters._State is TSU.Stations.Station.State.StationTiltReadyToBeSaved)
            //        || (this._stationTiltModule._AdjustingStationTilt.TiltParameters._State is TSU.Stations.Station.State.StationTiltReadyToBeSaved))
            //    {
            //        bigButton_saveAll.Available = true;
            //    }
            //    else
            //    {
            //        bigButton_saveAll.Available = false;
            //    }
            //    int firstActualStationIndex = this._stationTiltModule._StationTiltSequences.FindIndex(x => x.TheoPoint._Name == this._stationTiltModule._ActualStationTilt.TheoPoint._Name);
            //    if (firstActualStationIndex == 0 || firstActualStationIndex == 1)
            //    {
            //        bigButton_PreviousPointInSequence.Available = false;
            //    }
            //    else
            //    {
            //        bigButton_PreviousPointInSequence.Available = (firstActualStationIndex == -1) ? false : true;
            //    }
            //    int lastActualStationIndex = this._stationTiltModule._StationTiltSequences.FindLastIndex(x => x.TheoPoint._Name == this._stationTiltModule._ActualStationTilt.TheoPoint._Name);
            //    if (lastActualStationIndex == this._stationTiltModule._StationTiltSequences.Count - 1
            //        || lastActualStationIndex == this._stationTiltModule._StationTiltSequences.Count - 2)
            //    {
            //        bigButton_NextPointInSequence.Available = false;
            //    }
            //    else
            //    {
            //        bigButton_NextPointInSequence.Available = (lastActualStationIndex == -1) ? false : true;
            //    }
            //    this.tableLayoutPanel_ButtonTop.Controls.Add(bigButton_PreviousPointInSequence, 0, 0);
            //    this.tableLayoutPanel_ButtonTop.Controls.Add(bigButton_NextPointInSequence, 2, 0);
            //    this.tableLayoutPanel_ButtonTop.Controls.Add(bigButton_saveAll, 1, 0);
            //}
            /// <summary>
            /// creation du treeview avec tous les nodes
            /// </summary>

            ///// <summary>
            ///// Reset l'ètat par défaut des big buttons dans le context menu
            ///// </summary>
            //private void ResetBigButtonAvailable()
            //{
            //    this.bigButton_ChangePtTheo.Available = false;
            //    this.bigButton_Elements.Available = false;
            //    this.bigButton_AddMeas.Available = false;
            //    this.bigButton_DelMeas.Available = false;
            //    this.bigButton_saveInitial.Available = false;
            //    this.bigButton_saveAdjusting.Available = false;
            //    this.bigButton_DeleteSequence.Available = false;
            //    int actualInitIndex = this._stationTiltModule._StationTiltSequences.LastIndexOf(this._stationTiltModule._InitStationTilt);
            //    int totalNumberOfTiltStation = this._stationTiltModule._StationTiltSequences.Count;
            //    this.bigButton_NextPointInSequence.Available = actualInitIndex > totalNumberOfTiltStation - 4 ? false : true;
            //    this.bigButton_PreviousPointInSequence.Available = actualInitIndex < 2 ? false : true;
            //}
            /// <summary>
            /// permet d'afficher le form de l'instrument dans le panel du dessous
            /// </summary>
            /// <param name="i"></param>
            internal override void UpdateInstrumentView()
            //permet d'afficher la vue de l'instrument dans le panel du dessous
            {
                if (this.TiltMeterView != null)
                {
                    if (this._stationTiltModule._InstrumentManager.SelectedInstrumentModule == null)
                        this._stationTiltModule._InstrumentManager.SetInstrument(this._stationTiltModule._ActualStationTilt.ParametersBasic._Instrument);
                    TsuView v = this._stationTiltModule._InstrumentManager.SelectedInstrumentModule._TsuView;
                    splitContainer1.Panel2.Controls.Clear();
                    v.TopLevel = false;
                    v.Dock = DockStyle.None;
                    v.Show();
                    v.FormBorderStyle = FormBorderStyle.None;
                    splitContainer1.Panel2.Controls.Add(v);
                    this.instrumentView = v;
                    this.ScaleInstrumentView();
                }
            }
            /// <summary>
            /// Met à jour l'echelle de la vue instrument
            /// </summary>
            internal void ScaleInstrumentView()
            //Met à jour l'echelle de la vue instrument
            {
                D.View v = this._stationTiltModule._InstrumentManager.SelectedInstrumentModule._TsuView as D.View;
                if (v != null)
                {
                    float factorScale = Convert.ToSingle(splitContainer1.Panel2.Height) / Convert.ToSingle(v.startingHeight);
                    if (!double.IsNaN(factorScale) && !double.IsInfinity(factorScale))
                    {
                        v.Width = Convert.ToInt32(v.startingWidth * factorScale);
                        v.Height = splitContainer1.Panel2.Height;
                        (v as D.Manual.TiltMeter.View).ChangeScale(factorScale);
                    }
                }
            }

            /// <summary>
            /// enregistre les mesures initiales dans un fichier dat
            /// </summary>
            //internal void buttonSaveInitial_Click()
            //    //enregistre les mesures initiales dans un fichier dat
            //{
            //    this._stationTiltModule._ActualStationTilt = this._stationTiltModule._InitStationTilt;
            //    this.Save(false);
            //}
            /// <summary>
            /// enregistre les mesures de réglage dans un fichier dat
            /// </summary>
            //internal void buttonSaveAdjusting_Click()
            ////enregistre les mesures de réglage dans un fichier dat
            //{
            //    this._stationTiltModule._ActualStationTilt = this._stationTiltModule._AdjustingStationTilt;
            //    this.Save(false);
            //}
            /// <summary>
            /// enregistre les mesures de réglage dans un fichier dat
            /// </summary>
            //internal void buttonSaveAll_Click()
            ////enregistre toutes les mesures dans un fichier dat
            //{
            //    switch (this.moduleType)
            //    {
            //        case ModuleType.Guided:
            //            //save only the actual station tilt in the actual step
            //            if (this._stationTiltModule._ActualStationTilt.TiltParameters._State is TSU.Stations.Station.State.StationTiltReadyToBeSaved)
            //            {
            //                this.Save(false);
            //            }
            //            //if (this._stationTiltModule._AdjustingStationTilt.TiltParameters._State is TSU.Stations.Station.State.StationTiltReadyToBeSaved)
            //            //{
            //            //    this._stationTiltModule._ActualStationTilt = this._stationTiltModule._AdjustingStationTilt;
            //            //    this.Save(false);
            //            //}
            //            break;
            //        case ModuleType.Advanced:
            //            if (this._stationTiltModule._InitStationTilt.TiltParameters._State is TSU.Stations.Station.State.StationTiltReadyToBeSaved)
            //            {
            //                this._stationTiltModule._ActualStationTilt = this._stationTiltModule._InitStationTilt;
            //                this.Save(false);
            //            }
            //            //else
            //            //{
            //            //    this.ShowMessageOfExclamation(R.StringTilt_TiltInitNotExported, R.T_OK);
            //            //}
            //            if (this._stationTiltModule._AdjustingStationTilt.TiltParameters._State is TSU.Stations.Station.State.StationTiltReadyToBeSaved)
            //            {
            //                this._stationTiltModule._ActualStationTilt = this._stationTiltModule._AdjustingStationTilt;
            //                this.Save(false);
            //            }
            //            //else
            //            //{
            //            //    this.ShowMessageOfExclamation(R.StringTilt_TiltAdjNotExported, R.T_OK);
            //            //}
            //            break;
            //        default:
            //            break;
            //    }        
            //}
            /// <summary>
            /// Passe au point suivant dans la séquence
            /// </summary>
            //private void buttonNextPoint_Click()
            //{
            //    this.AskIfStationTiltHasToBeSaved();
            //    this._stationTiltModule.GoNextPoint();
            //    this.UpdateView();
            //    this.node_MeasuresInit.ExpandAll();
            //    this.node_MeasuresAjusting.ExpandAll();
            //}
            /// <summary>
            /// Passe au point précédent dans la séquence
            /// </summary>
            //private void buttonPreviousPoint_Click()
            //{
            //    this.AskIfStationTiltHasToBeSaved();
            //    this._stationTiltModule.GoPreviousPoint();
            //    //this.RedrawTreeviewParameters();
            //    this.UpdateView();
            //    this.node_MeasuresInit.ExpandAll();
            //    this.node_MeasuresAjusting.ExpandAll();
            //}
            /// <summary>
            /// Vérifie si les stations tilts de réglage et initiale sont sauvées avant de passer au point suivant ou précédent
            /// </summary>
            private void AskIfStationTiltHasToBeSaved()
            {
                //Sauvegarde le tilt initial si nécessaire
                if (this._stationTiltModule._InitStationTilt.TiltParameters._State is State.StationTiltReadyToBeSaved)
                {
                    string save = R.T_SAVE;
                    string titleAndMessage = String.Format(R.StringTilt_ExportInitialRoll, this._stationTiltModule._InitStationTilt.TheoPoint._ClassAndNumero);
                    MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { save, R.T_DONT }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == save)
                    {
                        this._stationTiltModule._ActualStationTilt = this._stationTiltModule._InitStationTilt;
                        this._stationTiltModule.ExportToGeode(false, true);
                    }
                }
                //Sauvegarde le tilt réglage si nécessaire
                if (this._stationTiltModule._AdjustingStationTilt.TiltParameters._State is State.StationTiltReadyToBeSaved)
                {
                    string save = R.T_SAVE;
                    string titleAndMessage = String.Format(R.StringTilt_ExportAdjustingRoll, this._stationTiltModule._AdjustingStationTilt.TheoPoint._ClassAndNumero);
                    MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { save, R.T_DONT },
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == save)
                    {
                        this._stationTiltModule._ActualStationTilt = this._stationTiltModule._AdjustingStationTilt;
                        this._stationTiltModule.ExportToGeode(false, true);
                    }
                }
            }
            /// <summary>
            /// Selection d'un nouveau point théorique
            /// </summary>
            internal void buttonSelectPoint_Click()
            //Selection d'un nouveau point théorique
            {
                this.SelectPoint();
            }
            /// <summary>
            /// Ajoute une nouvelle mesure de tilt
            /// </summary>
            //internal void buttonAddMeas_Click()
            ////Ajoute une nouvelle mesure de tilt
            //{
            //    this.AddMeasure();
            //}
            /// <summary>
            /// Efface une mesure de tilt
            /// </summary>
            //internal void buttonDelMeas_Click()
            ////Ajoute une nouvelle mesure de tilt
            //{
            //    //string respond = this.ShowMessageOfChoice(String.Format(R.StringTilt_ConfirmDeleteMeas, this.measureNumber+1, this.pointNameSelected), R.T_OK, R.T_CANCEL);
            //    //if (respond == R.T_OK)
            //    //{
            //    //    this.AskIfStationTiltHasToBeSaved();
            //        this.DeleteMeasure();
            //    //}
            //}
            /// <summary>
            /// Efface le point sélectionné de la séquence si on clique dessus
            /// </summary>
            //private void buttonDelSeq_Click()
            //{
            //    string respond = this.ShowMessageOfChoice(String.Format(R.StringTilt_ConfirmDeletePoint, this.pointNameSelected), R.T_OK, R.T_CANCEL);
            //    if (respond == R.T_OK)
            //    {
            //        this.AskIfStationTiltHasToBeSaved();
            //        this.DeletePointInSequence(this.pointNameSelected);
            //    }
            //}
            /// <summary>
            /// Cache le context menu si on l'a ouvert par mégarde
            /// </summary>
            private void buttonCancel_Click()
            {
                //if (this.PopUpMenu.Visible==true)
                //{
                //    this.PopUpMenu.Focus();
                //    this.CloseMenu();
                //}

            }
            /// <summary>
            /// Montre le context menu lorsqu'on clique dessus
            /// </summary>
            private void bigButtonTopShowMenu_Click()
            {
                //this.ResetBigButtonAvailable();
                //this.measureSelected = false;
                //this.pointSelected = false;
                //this.delMeasAvailable = false;
                this.ShowContextMenuGlobal();
            }
            /// <summary>
            /// Trie les points par Dcum ou inverse dcum
            /// </summary>
            private void buttonSort_Click()
            {
                switch (this._stationTiltModule.sortDirection)
                {
                    case SortDirection.Dcum:
                        this._stationTiltModule.sortDirection = SortDirection.InvDcum;
                        break;
                    case SortDirection.InvDcum:
                        this._stationTiltModule.sortDirection = SortDirection.Dcum;
                        break;
                    default:
                        break;
                }
                this._stationTiltModule.SortMeasure();
                //reorderElementNode = true;
                this.UpdateViewType();
            }
            ///// <summary>
            ///// Change the theo point
            ///// </summary>
            //private void buttonChangePtTheo_Click()
            //{
            //    if (this.pointSelected==true)
            //    {
            //        this.AskIfStationTiltHasToBeSaved();
            //        this._stationTiltModule.ChangePointTheo(this.pointNameSelected);
            //    }
            //}
            /// <summary>
            /// Montre le fichier initial dat exporté
            /// </summary>
            private void bigButton_ShowInitDatFile_Click()
            {
                string filepath = this._stationTiltModule._InitStationTilt.ParametersBasic._GeodeDatFilePath;
                if (filepath == "")
                {
                    int index = this._stationTiltModule._StationTiltSequences.FindIndex(x => x.TiltParameters._TiltInitOrAdjusting == TiltInitOrAdjusting.Initial && x.ParametersBasic._GeodeDatFilePath != "");
                    if (index != -1) filepath = this._stationTiltModule._StationTiltSequences[index].ParametersBasic._GeodeDatFilePath;
                }
                if (System.IO.File.Exists(filepath)) Shell.Run(filepath);
            }
            /// <summary>
            /// Montre le fichier de réglage dat exporté
            /// </summary>
            private void bigButton_ShowAdjDatFile_Click()
            {
                string filepath = this._stationTiltModule._AdjustingStationTilt.ParametersBasic._GeodeDatFilePath;
                if (filepath == "")
                {
                    int index = this._stationTiltModule._StationTiltSequences.FindIndex(x => x.TiltParameters._TiltInitOrAdjusting == TiltInitOrAdjusting.Adjusting && x.ParametersBasic._GeodeDatFilePath != "");
                    if (index != -1) filepath = this._stationTiltModule._StationTiltSequences[index].ParametersBasic._GeodeDatFilePath;
                }
                if (System.IO.File.Exists(filepath)) Shell.Run(filepath);
            }
            /// <summary>
            /// Affiche soit les mesures initiales ou les mesures de réglage
            /// </summary>
            private void bigButton_SwitchBetweenRollType_Click()
            {
                if (showInitOp)
                {
                    showInitOp = false;
                    showAdjustingOp = true;
                    this._stationTiltModule._ActualStationTilt = this._stationTiltModule._AdjustingStationTilt;
                }
                else
                {
                    showInitOp = true;
                    showAdjustingOp = false;
                    this._stationTiltModule._ActualStationTilt = this._stationTiltModule._InitStationTilt;
                }
                this.UpdateViewType();
            }
            /// <summary>
            /// Affichage aide roll
            /// </summary>
            private void button_Help_Click()
            //Affichage aide roll
            {
                this.ShowMessageOfImage(R.StringTilt_Help, R.T_OK, R.RollRefAngle3D_5);
            }
            /// <summary>
            /// cache le menu context si on clique dessus
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            internal void Context_OnClick(object sender, EventArgs e)
            //cache le menu context si on clique dessus
            {
                contextMenuStrip.Hide();
            }
            ///// <summary>
            ///// Enable or disable the save button in context menu global
            ///// </summary>
            //private void SetSaveButtonAvailibility()
            //{
            //    ////Affichage bouton save mesure initiale
            //    if (this._stationTiltModule._InitStationTilt.TiltParameters._State is TSU.Stations.Station.State.StationTiltReadyToBeSaved)
            //    {
            //        this.bigButton_saveInitial.Available = true;
            //    }
            //    else
            //    {
            //        this.bigButton_saveInitial.Available = false;
            //    }
            //    //Affichage bouton save mesure reglage
            //    if (this._stationTiltModule._AdjustingStationTilt.TiltParameters._State is TSU.Stations.Station.State.StationTiltReadyToBeSaved)
            //    {
            //        this.bigButton_saveAdjusting.Available = true;
            //    }
            //    else
            //    {
            //        this.bigButton_saveAdjusting.Available = false;
            //    }
            //}
            /// <summary>
            /// Change la tolérance max d'alignement
            /// </summary>
            private void ChangeTolerance()
            {
                //changement de la tolérance d'alignement
                string initialTextBoxText = Math.Round(this._stationTiltModule._ActualStationTilt.TiltParameters._Tolerance * 1000, 2).ToString();
                List<string> buttonTexts = new List<string> { R.T_OK, R.T_CANCEL };

                MessageTsu.ShowMessageWithTextBox(R.StringTilt_Tolerance, buttonTexts, out string buttonClicked, out string textInput, initialTextBoxText);

                if (buttonClicked != R.T_CANCEL)
                {
                    double newTolerance = T.Conversions.Numbers.ToDouble(textInput, true, -9999);
                    if (newTolerance != -9999)
                    {
                        this._stationTiltModule.ChangeTolerance(newTolerance / 1000);
                        //this.RedrawTreeviewParameters();
                    }
                    else
                    {
                        this.ShowMessage(R.StringTilt_WrongTolerance);
                    }
                }
            }

            /// <summary>
            /// Change le commentaire de la mesure
            /// </summary>
            //private void ChangeComment()
            //{
            //    string MsgResponse = "";
            //    MsgResponse = this.ShowMessageOfInput(R.StringTilt_Comment,R.T_OK,R.T_CANCEL, this._stationTiltModule._ActualStationTilt.TiltParameters._Comment.ToString());
            //    if ((MsgResponse !=R.T_CANCEL) && (!MsgResponse.Contains(";") && (System.Text.RegularExpressions.Regex.IsMatch(MsgResponse, @"^[a-zA-Z0-9 ]+$"))))
            //    {
            //        //Vérifie que le commentaire est de maximum 27 caractères
            //        if (MsgResponse.Length < 28)
            //        {
            //            this._stationTiltModule.ChangeComment(MsgResponse);
            //            //this.RedrawTreeviewParameters();
            //        }
            //        else
            //        {
            //            this.ShowMessage(R.StringTilt_WrongComment27);
            //        }
            //    }
            //    else
            //    {
            //        if (MsgResponse !=R.T_CANCEL)
            //        {
            //            this.ShowMessage(R.StringTilt_WrongCommentSemicolon);
            //        }
            //    }
            //}
            /// <summary>
            /// Change le nom de l'équipe terrain
            /// </summary>
            private void ChangeTeam()
            {
                List<string> buttonTexts = new List<string> { R.T_OK, R.T_CANCEL };

                MessageTsu.ShowMessageWithTextBox(R.StringTilt_Team, buttonTexts, out string buttonClicked, out string textInput, this._stationTiltModule._ActualStationTilt.TiltParameters._Team.ToString());

                if (buttonClicked != R.T_CANCEL)
                {
                    //Vérifie que le nom d'équipe contient que des caractères alphanumériques et est de maximum 8 caractères
                    if (System.Text.RegularExpressions.Regex.IsMatch(textInput, @"^[a-zA-Z]+$") && (textInput.Length < 9 || textInput == R.String_UnknownTeam))
                    {
                        textInput = textInput.ToUpper();
                        this._stationTiltModule.ChangeTeam(textInput);
                        //this.RedrawTreeviewParameters();
                    }
                    else
                    {
                        this.ShowMessage(R.StringTilt_WrongTeam);
                    }
                }
            }
            /// <summary>
            /// Give choice, between selecting an op from the list or to add an new number
            /// </summary>
            //private void ChangeOperation2Initial()
            //// Give choice, between selecting an op from the list or to add an new number
            //{
            //    contextMenuStrip.Items.Clear();
            //    contextMenuStrip.Items.Add(new BigButtonMenuItem(  bigButton_InitialExistingOperation));
            //    //contextMenuStrip.Items.Add(bigButton_NewInitialOperation);
            //    contextMenuStrip.Show(MousePosition.X - 70, MousePosition.Y - 40);
            //}
            /// <summary>
            /// Give choice, between selecting an op from the list or to add an new number
            /// </summary>
            //private void ChangeOperation2Adjusting()
            //// Give choice, between selecting an op from the list or to add an new number
            //{
            //    contextMenuStrip.Items.Clear();
            //    contextMenuStrip.Items.Add(new BigButtonMenuItem(bigButton_AdjustingExistingOperation));
            //    //contextMenuStrip.Items.Add(bigButton_NewAdjustingOperation);
            //    contextMenuStrip.Show(MousePosition.X - 70, MousePosition.Y - 40);
            //}
            /// <summary>
            /// Select operation from a list
            /// </summary>
            private void SelectInitialOperation()
            //Select operation from a list
            {
                this._stationTiltModule._ActualStationTilt = this._stationTiltModule._InitStationTilt;
                this._stationTiltModule.FinalModule.OperationManager._SelectedObjects.Clear();
                O.Operation operation = (this._stationTiltModule.FinalModule.OperationManager.AllElements.Find(x => x._Name == this._stationTiltModule._ActualStationTilt.ParametersBasic._Operation._Name) as O.Operation);
                this._stationTiltModule.FinalModule.OperationManager._SelectedObjectInBlue = operation;
                this._stationTiltModule.FinalModule.OperationManager.AddSelectedObjects(operation);
                O.Operation o = this._stationTiltModule.FinalModule.OperationManager.SelectOperation();
                if (o != null)
                {
                    this._stationTiltModule.ChangeOperationID(o);
                }
            }
            /// <summary>
            /// Select operation from a list
            /// </summary>
            private void SelectAdjustingOperation()
            //Select operation from a list
            {
                this._stationTiltModule._ActualStationTilt = this._stationTiltModule._AdjustingStationTilt;
                this._stationTiltModule.FinalModule.OperationManager._SelectedObjects.Clear();
                O.Operation operation = (this._stationTiltModule.FinalModule.OperationManager.AllElements.Find(x => x._Name == this._stationTiltModule._ActualStationTilt.ParametersBasic._Operation._Name) as O.Operation);
                this._stationTiltModule.FinalModule.OperationManager._SelectedObjectInBlue = operation;
                this._stationTiltModule.FinalModule.OperationManager.AddSelectedObjects(operation);
                O.Operation o = this._stationTiltModule.FinalModule.OperationManager.SelectOperation();
                if (o != null)
                {
                    this._stationTiltModule.ChangeOperationID(o);
                }

            }
            ///// <summary>
            ///// Add a new operation number
            ///// </summary>
            //private void AddNewInitialOperation()
            //// Add a new operation number
            //    /// this function is added in operation manager
            //{
            //    this._stationTiltModule._ActualStationTilt = this._stationTiltModule._InitStationTilt;
            //    string MsgResponse = "";
            //    MsgResponse = this.ShowMessageOfInput("OperationID Initiale;Choix du numéro d'opération initiale", R.T_OK, R.T_CANCEL, this._stationTiltModule._ActualStationTilt._Parameters._OperationId.ToString());
            //    if (MsgResponse != R.T_CANCEL)
            //    {
            //        int newOperationID;
            //        if (int.TryParse(MsgResponse, out newOperationID))
            //        {
            //            this._stationTiltModule.ChangeOperationID(newOperationID);
            //            this.RedrawTreeviewParameters();
            //            //cannot be used in guided module
            //            //if (this._stationTiltModule._InitStationTilt._Parameters._Operation.IsSet)
            //            //{
            //            //    bigButton_InitialExistingOperation = new BigButtonMenuItem(
            //            //        String.Format("Op Initiale existante;Ajout d'une opération initiale existante.OP actuelle {0}", this._stationTiltModule._InitStationTilt._Parameters._OperationId),
            //            //        R.Operation,
            //            //        this.SelectInitialOperation);
            //            //    bigButton_NewInitialOperation = new BigButtonMenuItem(
            //            //        String.Format("Nouvelle Op Initiale;Ajout d'une nouvelle operation initiale.OP actuelle {0}", this._stationTiltModule._InitStationTilt._Parameters._OperationId),
            //            //        R.Operation,
            //            //        AddNewInitialOperation);
            //            //}
            //        }
            //        else
            //        {
            //            this.ShowMessage("Numéro d'opération invalide;Veuillez entrer un numéro d'opération valide");
            //        }
            //    }
            //}
            ///// <summary>
            ///// Add a new operation number
            ///// </summary>
            //private void AddNewAdjustingOperation()
            //// Add a new operation number
            //// this function is added in operation manager
            //{
            //    this._stationTiltModule._ActualStationTilt = this._stationTiltModule._AdjustingStationTilt;
            //    string MsgResponse = "";
            //    MsgResponse = this.ShowMessageOfInput("OperationID Réglage;Choix du numéro d'opération de réglage", R.T_OK, R.T_CANCEL, this._stationTiltModule._ActualStationTilt._Parameters._OperationId.ToString());
            //    if (MsgResponse != R.T_CANCEL)
            //    {
            //        int newOperationID;
            //        if (int.TryParse(MsgResponse, out newOperationID))
            //        {
            //            this._stationTiltModule.ChangeOperationID(newOperationID);
            //            //cannot be used in guided module
            //            //if (this._stationTiltModule._AdjustingStationTilt._Parameters._Operation.IsSet)
            //            //{
            //            //    bigButton_AdjustingExistingOperation = new BigButtonMenuItem(
            //            //        String.Format("Op Reglage Existante;Ajout d'une opération réglage existante.OP actuelle {0}", this._stationTiltModule._AdjustingStationTilt._Parameters._OperationId),
            //            //        R.Operation,
            //            //        this.SelectAdjustingOperation);
            //            //    bigButton_NewAdjustingOperation = new BigButtonMenuItem(
            //            //        String.Format("Nouvelle Op Reglage;Ajout d'une nouvelle operation réglage.OP actuelle {0}", this._stationTiltModule._AdjustingStationTilt._Parameters._OperationId),
            //            //        R.Operation,
            //            //        this.AddNewAdjustingOperation);
            //            //}
            //            this.RedrawTreeviewParameters();
            //        }
            //        else
            //        {
            //            this.ShowMessage("Numéro d'opération invalide;Veuillez entrer un numéro d'opération valide");
            //        }
            //    }
            //}
            /// <summary>
            /// Change l'instrument de mesure
            /// </summary>
            private void ChangeInstrument()
            {
                //choix de l'instrument
                this._stationTiltModule.SelectionTiltMeter();
                //this.HideManualTiltmeterView();
                //this.TryAction(this._stationTiltModule.UpdateOffset);
                //this.UpdateInstrumentView();
            }
            /// <summary>
            /// hide the manual instrument panel 
            /// </summary>
            internal void HideManualTiltmeterView()
            {
                this.splitContainer1.Panel2MinSize = 0;
                this.splitContainer1.SplitterDistance = this.splitContainer1.Size.Height - this.splitContainer1.SplitterWidth;
            }
            /// <summary>
            /// Choix du point mesurer
            /// </summary>
            private void SelectPoint()
            //Choix du point à mesurer
            {
                this.TryAction(this._stationTiltModule.SelectSequenceOfElement);
                //this.TryAction(this._stationTiltModule.UpdateOffset);
                this.treeView_Parameters.ExpandAll();
            }
            /// <summary>
            /// Ajoute une mesure de tilt à la liste
            /// </summary>
            //private void AddMeasure()
            //// Ajoute une mesure de tilt à la liste
            //{
            //    this.TryAction(this._stationTiltModule.AddMeas);
            //}
            /// <summary>
            /// Enleve une mesure de la liste
            /// </summary>
            //private void DeleteMeasure()
            //{
            //    this._stationTiltModule.DeleteMeas(measureNumber);
            //    //this.RedrawTreeviewParameters();
            //}
            /// <summary>
            /// Efface un points dans la sequence de mesure
            /// </summary>
            //private void DeletePointInSequence(string pointTheo)
            //{
            //    this._stationTiltModule.DeletePointInSequence(pointTheo);
            //}
            /// <summary>
            /// sauvegarde la station tilt
            /// </summary>
            //private void Save()
            //    //Sauvegarde la station tilt
            //{
            //    if (this._stationTiltModule._ActualStationTilt.TiltParameters._State is TSU.Stations.Station.State.StationTiltReadyToBeSaved)
            //    {
            //        this._stationTiltModule.SaveTilt();
            //    }
            //}
            //private void Save(bool showFile)
            ////Sauvegarde la station tilt
            //{
            //    if (this._stationTiltModule._ActualStationTilt.TiltParameters._State is TSU.Stations.Station.State.StationTiltReadyToBeSaved)
            //    {
            //        this._stationTiltModule.SaveTilt(showFile, true);
            //    }
            //}
            /// <summary>
            /// Affiche le menu global context si on clique droit
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            //private void treeView_Parameters_MouseClick(object sender, MouseEventArgs e)
            ////Affiche le menu global context si on clique droit
            //{
            //    if (e.Button == System.Windows.Forms.MouseButtons.Right)
            //    {
            //        this.ShowContextMenuGlobal();
            //    }
            //}
            internal void ShowMessage(string titleAndDescription)
            //Affiche la fenêtre d'erreur si encodage incorrect
            {
                new MessageInput(MessageType.Critical, titleAndDescription).Show();
            }

            /// <summary>
            /// ajoute la mesure venant dans l'instrument dans le treeview
            /// </summary>
            /// <param name="measureFromInstrument"></param>
            internal void AddInstrumentMeasure(Measure measureFromInstrument, bool updateOffsetNow = true)
            //ajoute la mesure venant dans l'instrument dans le treeview
            {
                //if (measureSelected)
                //{
                this.CheckDatagridIsInEditMode();
                switch (tiltDirection)
                {
                    case TiltDirection.Beam:
                        if (this._stationTiltModule._ActualStationTilt._MeasuresTilt.Count > 1)
                        {
                            this._stationTiltModule._ActualStationTilt._MeasuresTilt.RemoveRange(1, this._stationTiltModule._ActualStationTilt._MeasuresTilt.Count - 1);
                        }
                        this._stationTiltModule.UpdateMeasuresBeam(0, measureFromInstrument);
                        break;
                    case TiltDirection.Opposite:
                        if (this._stationTiltModule._ActualStationTilt._MeasuresTilt.Count > 1)
                        {
                            this._stationTiltModule._ActualStationTilt._MeasuresTilt.RemoveRange(1, this._stationTiltModule._ActualStationTilt._MeasuresTilt.Count - 1);
                        }
                        this._stationTiltModule.UpdateMeasuresOppositeBeam(0, measureFromInstrument);
                        break;
                    default:
                        break;
                }
                if (updateOffsetNow) { this._stationTiltModule.UpdateOffset(); }
                //this.RedrawTreeviewParameters();
                //}
            }

            /// <summary>
            /// Met à jour la vue instrument lorsqu'on déplace le splitter
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
            {
                if (this.TiltMeterView != null)
                {
                    this.CheckDatagridIsInEditMode();
                    this.UpdateInstrumentView();
                }
                this.UpdateDockStyle();
            }
            /// <summary>
            /// Affiche le form pour encoder une mesure manuellement
            /// </summary>
            //internal string EnterTiltValue(double previousValue, MeasureOfTilt newMeasTilt, int measureNumber = 1)
            ////Affiche le form pour encoder une mesure manuellement
            //{
            //    string MsgResponse = "";
            //    string theoReadingBeam = "---";
            //    string tiltTheo = Math.Round(this._stationTiltModule._ActualStationTilt.TheoPoint._Parameters.Tilt * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            //    string pointName = this._stationTiltModule._ActualStationTilt.TheoPoint._Name;
            //    string previousValueText = Math.Round(previousValue * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            //    if (this._stationTiltModule._ActualStationTilt._EcartTheo.Value != 9999 && this._stationTiltModule._ActualStationTilt._MeasuresTilt.Count ==1)
            //    {
            //        theoReadingBeam = Math.Round((this._stationTiltModule._ActualStationTilt._MeasureAverageBeamDirection.Value - this._stationTiltModule._ActualStationTilt._EcartTheo.Value) * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            //    }
            //    if (previousValue==9999)
            //    {
            //        switch (tiltDirection)
            //        {
            //            case TiltDirection.Beam:

            //                MsgResponse = this.ShowMessageOfInput(pointName + "\r\n" + string.Format(R.StringTilt_EnterRollBeamDirection, tiltTheo,theoReadingBeam, measureNumber+1),
            //                   R.T_OK,R.T_CANCEL, "");
            //                break;
            //            case TiltDirection.Opposite:
            //                MsgResponse = this.ShowMessageOfInput(pointName + "\r\n" + string.Format(R.StringTilt_EnterRollOppositeDirection, tiltTheo, theoReadingBeam, measureNumber+1), 
            //                   R.T_OK,R.T_CANCEL, "");
            //                break;
            //            default:
            //                break;
            //        }  
            //    }
            //    else
            //    {
            //        switch (tiltDirection)
            //        {
            //            case TiltDirection.Beam:
            //                MsgResponse = this.ShowMessageOfInput(pointName + "\r\n" + string.Format(R.StringTilt_EnterRollBeamDirection, tiltTheo, theoReadingBeam, measureNumber+1), 
            //                   R.T_OK,R.T_CANCEL, previousValueText);
            //                break;
            //            case TiltDirection.Opposite:
            //                MsgResponse = this.ShowMessageOfInput(pointName + "\r\n" + string.Format(R.StringTilt_EnterRollOppositeDirection, tiltTheo, theoReadingBeam, measureNumber+1),
            //                   R.T_OK,R.T_CANCEL, previousValueText);
            //                break;
            //            default:
            //                break;
            //        }
            //    }
            //    if (MsgResponse !=R.T_CANCEL)
            //    {
            //        if (MsgResponse=="")
            //        {
            //            //Effacement de la mesure
            //            newMeasTilt._ReadingBeamDirection.Value = na;
            //        }
            //        else
            //        {
            //            double j = T.Conversions.Numbers.ToDouble(MsgResponse, true, -9999);

            //            if (j != -9999)
            //            {
            //                newMeasTilt._ReadingBeamDirection.Value = j / 1000;
            //            }
            //            else
            //            {
            //                ///si mauvais tilt entre considere comme un click sur cancel
            //                this.ShowMessage(R.StringTilt_WrongRoll);
            //                newMeasTilt._ReadingBeamDirection.Value = previousValue;
            //                MsgResponse = R.T_CANCEL;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        newMeasTilt._ReadingBeamDirection.Value = previousValue;
            //    }
            //    return MsgResponse;
            //}

            private void PopUpMenu_Load(object sender, EventArgs e)
            {

            }
            #endregion
            #region datagridPopUp
            /// <summary>
            /// Enter tilt in a datagrid pop up
            /// </summary>
            internal void EnterTiltInDatagridPopUp()
            {

                string title = "";
                switch (this._stationTiltModule._ActualStationTilt.TiltParameters._TiltInitOrAdjusting)
                {
                    case TiltInitOrAdjusting.Initial:
                        title = R.StringTilt_EnterInitialDataGridMessage + "\r\n"
                              + this._stationTiltModule._ActualStationTilt.TheoPoint._Name + "\r\n"
                              + Tilt.Module.FormatAngleInMrad(this._stationTiltModule._ActualStationTilt.TheoPoint._Parameters.Tilt) + " mRad";
                        break;
                    case TiltInitOrAdjusting.Adjusting:
                        title = R.StringTilt_EnterAdjustingDataGridMessage + "\r\n"
                              + this._stationTiltModule._ActualStationTilt.TheoPoint._Name + "\r\n"
                              + Tilt.Module.FormatAngleInMrad(this._stationTiltModule._ActualStationTilt.TheoPoint._Parameters.Tilt) + " mRad";
                        break;
                    default:
                        break;
                }
                bool showToleranceColors = true;
                if (this.moduleType == ModuleType.Guided) showToleranceColors = false;
                I.Module instrumentModule = new I.Module(this._stationTiltModule, this._stationTiltModule._InstrumentManager.SelectedInstrument);
                string respond = this.ShowMessageOfEnterTiltInDatagrid(title, R.T_OK, R.T_CANCEL, this._stationTiltModule._ActualStationTilt, showToleranceColors, instrument: this._stationTiltModule._InstrumentManager.SelectedInstrument, instrumentModule: instrumentModule);
                if (respond == R.T_OK) this._stationTiltModule.UpdateOffset();
                
             }
            #endregion
        }
    }
}
