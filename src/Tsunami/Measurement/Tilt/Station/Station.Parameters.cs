﻿using System;
using System.Xml;
using System.Xml.Serialization;
using I = TSU.Common.Instruments;
using R = TSU.Properties.Resources;

namespace TSU.Tilt
{
    partial class Station
    {
        /// <summary>
        /// Paramètres pour les stations de tilt
        /// </summary>
        [Serializable]
        [XmlType(TypeName = "Tilt.Station.Parameters")]
        public class Parameters : Common.Station.Parameters
        //pour les stations de tilt
        {
            public virtual double _Tolerance { get; set; } //tolérance pour l'alignement des éléments
            [XmlAttribute]
            private string userComment = "";
            public virtual string _Comment
            {
                get
                {
                    string comment = "";
                    // from User
                    if (userComment != "")
                    {
                        if (userComment != R.T_NO_COMMENT)
                            comment += $"user: '{userComment}'";
                    }
                    return comment;
                }
                set
                {
                    string comment = "";
                    if (value is string) comment = value;
                    comment = comment.Replace("user: ", "");
                    userComment = comment;
                }
            }
            public virtual TSU.ENUM.TiltInitOrAdjusting _TiltInitOrAdjusting { get; set; }

            [XmlIgnore]
            public new I.TiltMeter _Instrument
            {
                get
                {
                    return base._Instrument as I.TiltMeter;
                }
                set
                {
                    base._Instrument = value;
                }
            }
            public Parameters()
                : base()
            //Constructeurs
            {
                this._Date = DateTime.Now;
                this._Team = base._Team;
                this._Operation = base._Operation;
                this._IsSetup = false;
                this._Instrument = new I.TiltMeter();
                this._State = new Common.Station.State.Opening();
                this._Tolerance = TSU.Tsunami2.Preferences.Values.Tolerances.Alignment_Radial_mm / 1000;
                this._Comment = "";
                this._TiltInitOrAdjusting = TSU.ENUM.TiltInitOrAdjusting.Initial;
                this._GeodeDatFilePath = "";
            }
        }
    }
}
