﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using TSU.Common.Compute.Compensations.Strategies;
using TSU.Common.Elements;
using R = TSU.Properties.Resources;


namespace TSU.Tilt
{
    [Serializable]
    [XmlType(TypeName = "Tilt.Station")]
    public partial class Station : Common.Station, IEquatable<Station>, IComparable<Station>
    {
        public override CloneableList<TSU.Common.Measures.Measure> MeasuresTaken
        {
            get
            {
                CloneableList<TSU.Common.Measures.Measure> l = new CloneableList<TSU.Common.Measures.Measure>();
                l.AddRange(_MeasuresTilt);
                return l;
            }
        }

        //variables

        public CloneableList<Tilt.Measure> _MeasuresTilt { get; set; }
        public DoubleValue _MeasureAverageBothDirection { get; set; }
        public DoubleValue _MeasureAverageBeamDirection { get; set; }
        public DoubleValue _MeasureAverageOppositeDirection { get; set; }
        public DoubleValue _EcartTheo { get; set; }
        public Point TheoPoint { get; set; }
        public Point _MeasPoint { get; set; }
        public Station() : base()
        {
            //constructeur
            this.TiltParameters = new Parameters();
            _MeasuresTilt = new CloneableList<Measure>();
            _MeasureAverageBothDirection = new DoubleValue();
            _MeasureAverageBeamDirection = new DoubleValue();
            _MeasureAverageOppositeDirection = new DoubleValue();
            _EcartTheo = new DoubleValue();
            TheoPoint = new Point();
            _MeasPoint = new Point();
            this._Name = "STTilt_Ini_" + string.Format("{0:yyyyMMdd_HHmmss}" + "_", DateTime.Now);
            this.TiltParameters._StationName = this._Name;
            this.TiltParameters._Station = this;
            // ? this.TiltParameters._Station._Name = this._Name;
            this._MeasureAverageBothDirection.Value = 9999;
            this._MeasureAverageBothDirection.Sigma = 9999;
            this._MeasureAverageBeamDirection.Value = 9999;
            this._MeasureAverageBeamDirection.Sigma = 9999;
            this._MeasureAverageOppositeDirection.Value = 9999;
            this._MeasureAverageOppositeDirection.Sigma = 9999;
            this._EcartTheo.Value = 9999;
            this._EcartTheo.Sigma = 9999;
            //MeasureOfTilt firstTilt = new MeasureOfTilt();
            //_MeasuresTilt.Add(firstTilt);
        }
        [XmlIgnore]
        public Parameters TiltParameters
        {
            get
            {
                return this.ParametersBasic as Parameters;
            }
            set
            {
                this.ParametersBasic = value;
            }
        }
        /// <summary>
        /// Renomme le nom de la station en fonction de l'élément à aligner
        /// </summary>
        /// <param name="levellingDirectionStrategy"></param>
        internal void Rename()
        //Renomme le nom de la station en fonction de l'élément à aligner
        {
            string[] nameSplit = this._Name.Split('_');
            switch (this.TiltParameters._TiltInitOrAdjusting)
            {
                case TSU.ENUM.TiltInitOrAdjusting.Initial:
                    nameSplit[1] = "Ini";
                    break;
                case TSU.ENUM.TiltInitOrAdjusting.Adjusting:
                    nameSplit[1] = "Adj";
                    break;
                default:
                    nameSplit[1] = "Ini";
                    break;
            }
            this._Name = $"{nameSplit[0]}_{nameSplit[1]}_{nameSplit[2]}_{nameSplit[3]}_{this.TheoPoint._Name}";
            this.TiltParameters._StationName = this._Name;
            this.TiltParameters._Station._Name = this._Name;
        }

        public override string ToString()
        {
            return string.Format(R.TS_Station, this._Name);
        }

        public override object Clone()
        {
            Station newStation = (Station)base.Clone();
            newStation._MeasuresTilt = _MeasuresTilt.Clone();
            newStation._MeasureAverageBothDirection = (DoubleValue)_MeasureAverageBothDirection.Clone();
            newStation._MeasureAverageBeamDirection = (DoubleValue)_MeasureAverageBeamDirection.Clone();
            newStation._MeasureAverageOppositeDirection = (DoubleValue)_MeasureAverageOppositeDirection.Clone();
            newStation._EcartTheo = (DoubleValue)_EcartTheo.Clone();
            newStation.TheoPoint = (Point)TheoPoint.Clone();
            newStation._MeasPoint = (Point)_MeasPoint.Clone();
            // référence vers l'instrument actuel
            newStation.TiltParameters._Instrument = TiltParameters._Instrument;
            return newStation;
        }

        /// <summary>
        /// Calcule le tilt moyen initial de la station si possible
        /// </summary>
        internal Result AverageTiltCalculation()
        //Calcule le tilt moyen de la station si possible
        {
            Result result = new Result();
            int nbTiltMeas = 0;
            double averageTiltBothDirection = 0;
            double averageTiltBeamDirection = 0;
            double averageTiltOppositeDirection = 0;
            foreach (Tilt.Measure mesTilt in this._MeasuresTilt)
            {
                mesTilt.AverageTiltCalculation();
                if (this.TheoPoint._Parameters.Tilt != TSU.Tsunami2.Preferences.Values.na)
                {
                    mesTilt.EcartCalculation(this.TheoPoint._Parameters.Tilt);
                }
                if (mesTilt._AverageBothDirection.Value != na)
                {
                    nbTiltMeas++;
                    averageTiltBothDirection += mesTilt._AverageBothDirection.Value;
                    averageTiltBeamDirection += mesTilt._ReadingBeamDirection.Value;
                    averageTiltOppositeDirection += mesTilt._ReadingOppositeBeam.Value;
                }
            }
            if (nbTiltMeas > 0)
            {
                averageTiltBothDirection = averageTiltBothDirection / nbTiltMeas;
                averageTiltBeamDirection = averageTiltBeamDirection / nbTiltMeas;
                averageTiltOppositeDirection = averageTiltOppositeDirection / nbTiltMeas;
                this._MeasureAverageBothDirection.Value = averageTiltBothDirection;
                this._MeasPoint._Parameters.Tilt = averageTiltBothDirection;
                this._MeasureAverageOppositeDirection.Value = averageTiltOppositeDirection;
                this._MeasureAverageBeamDirection.Value = averageTiltBeamDirection;
                //si tilt theo présent
                if (this.TheoPoint._Parameters.Tilt != TSU.Tsunami2.Preferences.Values.na)
                {
                    this._EcartTheo.Value = averageTiltBothDirection - this.TheoPoint._Parameters.Tilt;
                }
            }
            switch (nbTiltMeas)
            {
                case 0:
                    //si une seule mesure de tilt, la moyenne est la mesure
                    if (this._MeasuresTilt[0]._ReadingBeamDirection.Value != na) { this._MeasureAverageBeamDirection.Value = this._MeasuresTilt[0]._ReadingBeamDirection.Value; }
                    if (this._MeasuresTilt[0]._ReadingOppositeBeam.Value != na) { this._MeasureAverageOppositeDirection.Value = this._MeasuresTilt[0]._ReadingOppositeBeam.Value; }
                    result.Success = false;
                    this.TiltParameters._State = new Common.Station.State.TiltcalculationFailed();
                    break;
                case 1:
                    this._MeasureAverageBothDirection.Sigma = 0;
                    this._MeasureAverageBeamDirection.Sigma = 0;
                    this._MeasureAverageOppositeDirection.Sigma = 0;
                    this._EcartTheo.Sigma = 0;
                    result.Success = true;
                    this.TiltParameters._Date = System.DateTime.Now;
                    this.TiltParameters._State = new Common.Station.State.TiltcalculationSuccessfull();
                    break;
                default:
                    this.EmqCalculation();
                    result.Success = true;
                    this.TiltParameters._Date = System.DateTime.Now;
                    this.TiltParameters._State = new Common.Station.State.TiltcalculationSuccessfull();
                    break;
            }
            
            return result;
        }
        /// <summary>
        /// Calcul l'Emq de la moyenne dans les 2 directions
        /// </summary>
        private void EmqCalculation()
        // Calcul l'Emq de la moyenne dans les 2 directions
        {
            double emqBothDirection = 0;
            double emqBeamDirection = 0;
            double emqOppositeDirection = 0;
            int nbTiltMes = 0;
            foreach (Tilt.Measure mesTilt in this._MeasuresTilt)
            {
                if (mesTilt._AverageBothDirection.Value != na)
                {
                    nbTiltMes++;
                    emqBothDirection = Math.Pow(this._MeasureAverageBothDirection.Value - mesTilt._AverageBothDirection.Value, 2);
                    emqBeamDirection = Math.Pow(this._MeasureAverageBeamDirection.Value - mesTilt._ReadingBeamDirection.Value, 2);
                    emqOppositeDirection = Math.Pow(this._MeasureAverageOppositeDirection.Value - mesTilt._ReadingOppositeBeam.Value, 2);
                }
            }
            this._MeasureAverageBothDirection.Sigma = Math.Sqrt(emqBothDirection / (nbTiltMes - 1));
            this._MeasureAverageBeamDirection.Sigma = Math.Sqrt(emqBeamDirection / (nbTiltMes - 1));
            this._MeasureAverageOppositeDirection.Sigma = Math.Sqrt(emqBothDirection / (nbTiltMes - 1));
            this._EcartTheo.Sigma = this._MeasureAverageBothDirection.Sigma;
        }

        /// <summary>
        /// Efface une des mesures de tilt de la liste
        /// </summary>
        /// <param name="measureNumber"></param>
        internal void DeleteMeas(int measureNumber)
        {
            this._MeasuresTilt.RemoveAt(measureNumber);
            //this.AverageTiltCalculation();
        }

        // Default comparer for stationTilt to sort list of points.
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            Station objAsPoint = obj as Station;
            if (objAsPoint == null) return false;
            else return Equals(objAsPoint);
        }
        public new int CompareTo(object obj)
        {
            if (obj == null) return 1;
            Station objAsMeas = obj as Station;
            if (objAsMeas == null) return 1;
            else return CompareTo(objAsMeas);
        }
        public int CompareTo(Station compareStation)
        {
            // A null value means that this object is greater.
            if (compareStation == null)
                return 1;

            else
                return this.TheoPoint._Parameters.Cumul.CompareTo(compareStation.TheoPoint._Parameters.Cumul);
        }
        public override int GetHashCode()
        {
            return this.TheoPoint._Parameters.Cumul.GetHashCode();
        }
        public bool Equals(Station other)
        {
            if (other == null) return false;
            return
                (this._Name == other._Name);
        }
        /// <summary>
        /// Ajoute 4 mesures à la liste des mesures de tilt pour avoir 5 mesures dans le cas d'un dipole SPS
        /// </summary>
        internal void CreateMeasForSPSDipole()
        {
            if ((this.TheoPoint._Accelerator == "SPS") && ((this.TheoPoint._Class == "MBA") || (this.TheoPoint._Class == "MBB")))
            {
                for (int i = 0; i < 4; i++)
                {
                    Tilt.Measure newMeas = new Tilt.Measure();
                    _MeasuresTilt.Add(newMeas);
                }
            }
        }
        /// <summary>
        /// Met à jour la station tilt dans le cas ou l'on efface une lecture sens faisceau
        /// </summary>
        /// <param name="measureNumber"></param>
        internal void ResetMeasForBeamDirectionReadingDeleted(int measureNumber)
        {
            this._MeasuresTilt[measureNumber].ResetMeasForBeamDirectionReadingDeleted();
            this._EcartTheo.Value = 9999;
            this._MeasureAverageBothDirection.Value = 9999;
            this._MeasureAverageBothDirection.Sigma = 9999;
            this._MeasureAverageBeamDirection.Value = 9999;
            this._MeasureAverageBeamDirection.Sigma = 9999;
            this._EcartTheo.Value = 9999;
            this._EcartTheo.Sigma = 9999;
            this.ParametersBasic.LastChanged = DateTime.Now;
        }
        /// <summary>
        /// Met à jour la station tilt dans le cas ou l'on efface une lecture sens inverse faisceau
        /// </summary>
        /// <param name="measureNumber"></param>
        internal void ResetMeasForBeamOppositeDirectionReadingDeleted(int measureNumber)
        {
            this._MeasuresTilt[measureNumber].ResetMeasForBeamOppositeDirectionReadingDeleted();
            this._EcartTheo.Value = 9999;
            this._MeasureAverageBothDirection.Value = 9999;
            this._MeasureAverageBothDirection.Sigma = 9999;
            this._MeasureAverageOppositeDirection.Value = 9999;
            this._MeasureAverageOppositeDirection.Sigma = 9999;
            this._EcartTheo.Value = 9999;
            this._EcartTheo.Sigma = 9999;
            this.ParametersBasic.LastChanged = DateTime.Now;
        }
    }

}