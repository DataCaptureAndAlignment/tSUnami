using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using TSU.Common.Elements;
using C = TSU.Common.Elements.Composites;
using E = TSU.ENUM;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;
using BOC = TSU.Common.Compute.Compensations.BeamOffsets;
using System.Collections.ObjectModel;
using TSU.Common.Instruments.Device;
using TSU.Common.Instruments;
using MathNet.Numerics.LinearAlgebra.Complex.Solvers;
using TSU.Common.Blocks;
using TSU.Common.Measures;
using TSU.Views.Message;
using System.Xml.Linq;

namespace TSU.Tilt
{
    public partial class Station
    {
        [Serializable]
        [XmlType(TypeName = "Tilt.Station.Module")]
        public new class Module : Common.Station.Module
        {
            #region Fields and properties

            [XmlIgnore]
            public List<Station> _AdjustingStationsTilt
            {
                get
                {
                    return this._StationTiltSequences.FindAll(x => x.TiltParameters._TiltInitOrAdjusting == E.TiltInitOrAdjusting.Adjusting);
                }
            }

            [XmlIgnore]
            public List<Station> _InitialStationsTilt
            {
                get
                {
                    return this._StationTiltSequences.FindAll(x => x.TiltParameters._TiltInitOrAdjusting == E.TiltInitOrAdjusting.Initial);
                }
            }


            [XmlIgnore]
            internal new View View
            {
                get
                {
                    return _TsuView as View;
                }
                set
                {
                    _TsuView = value;
                }
            }

            [XmlIgnore]
            public Station _ActualStationTilt
            {
                get
                {
                    return _Station as Station;
                }
                set
                {

                    _Station = value;
                }
            }

            [XmlIgnore]
            public override ReadOnlyCollection<M.Measure> MeasuresReceived
            {
                get
                {
                    var list = new List<M.Measure>();
                    return new ReadOnlyCollection<M.Measure>(list);
                }
            }

            public Station _InitStationTilt { get; set; }

            public Station _AdjustingStationTilt { get; set; }

            public TsuBool ShowSaveMessageOfSuccess { get; set; }

            public List<Station> _StationTiltSequences { get; set; }

            public CloneableList<Element> ElementsTobeMeasured { get; set; }


            

            #endregion

            #region Construction

            // Constructor
            public Module()
                : base()
            {

            }

            public Module(Common.FinalModule module, bool createView = true)
                : base(module, $"{R.T_STATION_MODULE_TILT};{R.T_MODULE_MANAGING_A_TILT_STATION}", createView)
            {

            }

            #endregion

            #region Methods

            /// <summary>
            /// This method is called by the base class "Module" when this object is created
            /// </summary>
            public override void Initialize()
            {
                ShowSaveMessageOfSuccess = new TsuBool(false);
                ElementsTobeMeasured = new CloneableList<Element>();
                _StationTiltSequences = new List<Station>();
                _InitStationTilt = new Station();
                _InitStationTilt._MeasuresTilt.Add(new Measure());
                _AdjustingStationTilt = new Station();
                _AdjustingStationTilt._MeasuresTilt.Add(new Measure());
                _AdjustingStationTilt.TiltParameters._TiltInitOrAdjusting = E.TiltInitOrAdjusting.Adjusting;
                _ActualStationTilt = _InitStationTilt;
                base.Initialize(); // this will add an instrument Manager but with no instrument in the list, need to call _InstrumentManager.LoadInstruments()
                _Station = _ActualStationTilt;
                LoadInstrumentTypes();
            }

            internal override void ReCreateWhatIsNotSerialized(bool saveSomeMemory = false)
            {
                try
                {
                    foreach (Station st in _StationTiltSequences)
                    {
                        st.ParametersBasic.LastSaved = DateTime.MinValue;
                        st.ParametersBasic.LastChanged = DateTime.Now;
                        CheckIfReadyToBeSaved(st);
                    }
                    //Dans le cas ou l'on ouvre une sauvegarde, l'op s�lectionn�e n'est plus coch�e
                    O.Operation oldOp;
                    if (_ActualStationTilt == null)
                    {
                        oldOp = new O.Operation(9999, "lost");
                    }
                    else
                    {
                        oldOp = FinalModule.OperationManager.AllElements.Find(
                            x => x._Name == _ActualStationTilt.ParametersBasic._Operation._Name
                        ) as O.Operation;
                    }

                    if (oldOp != null)
                    {
                        FinalModule.OperationManager._SelectedObjectInBlue = oldOp;
                        FinalModule.OperationManager.AddSelectedObjects(oldOp);
                        FinalModule.OperationManager.UpdateView();
                    }
                    base.ReCreateWhatIsNotSerialized(saveSomeMemory);
                    ///Reselection du bon instrument dans l'instrument manager apr�s r�ouverture fichier TSU
                    if (_ActualStationTilt != null
                        && _ActualStationTilt.ParametersBasic._Instrument.Id != null)
                    {
                        I.Sensor i = _InstrumentManager.AllElements.OfType<I.Sensor>().FirstOrDefault(
                            x => (x._SerialNumber == _ActualStationTilt.ParametersBasic._Instrument._SerialNumber)
                              && (x._Model == _ActualStationTilt.ParametersBasic._Instrument._Model)
                        );
                        _InstrumentManager._SelectedObjects.Clear();
                        _InstrumentManager._SelectedObjectInBlue = i;
                        _InstrumentManager._SelectedObjects.Add(i);
                        //this._InstrumentManager.ValidateSelection();
                    }
                    if (View != null)
                    {
                        View.HideManualTiltmeterView();
                        switch (_ActualStationTilt.TiltParameters._TiltInitOrAdjusting)
                        {
                            case E.TiltInitOrAdjusting.Initial:
                                View.showAdjustingOp = false;
                                View.showInitOp = true;
                                break;
                            case E.TiltInitOrAdjusting.Adjusting:
                                View.showAdjustingOp = true;
                                View.showInitOp = false;
                                break;
                            default:
                                break;
                        }
                        View.UpdateView();
                    }

                    if (FinalModule is Tilt.Module tm)
                        tm.View.SetStationNameInTopButton(this);
                }
                catch (Exception ex)
                {
                    throw new Exception($"Cannot re-create what was not serialized in {_Name}", ex);
                }

            }
            /// <summary>
            /// Met � jour la vue en executant la fonction de base qui renvoie vers la fonction updateView dans le station tilt module view
            /// </summary>
            public override void UpdateView()
            {
                base.UpdateView();
            }
            /// <summary>
            /// Copie l'instrument du oldStationTiltModule vers l'actuel stationTiltModule
            /// </summary>
            /// <param name="oldStationTiltModule"></param>
            internal void SetInstrumentInNewStationTiltModule(Module oldStationTiltModule)
            {
                TsuObject selectedInstrument = _InstrumentManager.AllElements.Find(x => x._Name == oldStationTiltModule._ActualStationTilt.TiltParameters._Instrument._Name);
                _InstrumentManager._SelectedObjects.Clear();
                _InstrumentManager._SelectedObjects.Add(selectedInstrument);
                _InstrumentManager._SelectedObjectInBlue = selectedInstrument;
                _InstrumentManager.ValidateSelection();
                View.HideManualTiltmeterView();
            }
            /// <summary>
            /// Set the Geode file path from the old station tilt Module into the new station level module
            /// </summary>
            /// <param name="oldStationTiltModule"></param>
            internal void SetGeodeFilePathInNewStationTiltModule(Module oldStationTiltModule)
            {
                _ActualStationTilt.TiltParameters._GeodeDatFilePath = oldStationTiltModule._ActualStationTilt.TiltParameters._GeodeDatFilePath;
            }
            /// <summary>
            /// Clone the operations for the initial and adjusting station tilt from the old station tilt module
            /// </summary>
            /// <param name="oldStationTiltModule"></param>
            internal void SetOperationsInNewStationTiltModule(Module oldStationTiltModule)
            {
                _InitStationTilt.TiltParameters._Operation = oldStationTiltModule._InitStationTilt.TiltParameters._Operation;
                _InitStationTilt.ParametersBasic._Operation = oldStationTiltModule._InitStationTilt.ParametersBasic._Operation;
                _AdjustingStationTilt.TiltParameters._Operation = oldStationTiltModule._AdjustingStationTilt.TiltParameters._Operation;
                _AdjustingStationTilt.ParametersBasic._Operation = oldStationTiltModule._AdjustingStationTilt.ParametersBasic._Operation;
            }
            #endregion

            #region Observer
            /// <summary>
            /// already threated in "Management.Instrument.Level SelectionLevel()"
            /// </summary>
            /// <param name="tsuObject"></param>
            public void OnNextBasedOn(I.Instrument tsuObject)
            //already threated in "Management.Instrument.Level SelectionLevel()"
            {
                if (tsuObject is I.TiltMeter)
                {
                    SelectionTiltMeter(tsuObject as I.TiltMeter);
                }
            }
            /// <summary>
            /// dispatcheur d'evenement si un type n'est pas connu il est trait� par OnNextBasedOn(TsuObject TsuObject)
            /// </summary>
            /// <param name="TsuObject"></param>
            public override void OnNext(TsuObject TsuObject)
            // dispatcheur d'evenement si un type n'est pas connu il est trait� par OnNextBasedOn(TsuObject TsuObject)
            {
                dynamic dynamic = TsuObject;
                OnNextBasedOn(dynamic);
            }
            /// <summary>
            /// �v�nement li� � la r�ception d'une mesure de l'instrument et lancement de la fonction pour l'ajouter dans le treeview
            /// </summary>
            /// <param name="measureFromInstrument"></param>
            public void OnNextBasedOn(Common.Measures.Measure measureFromInstrument)
            //�v�nement li� � la r�ception d'une mesure de l'instrument et lancement de la fonction pour l'ajouter dans le treeview
            {
                //lance la fonction pour 
                View.AddInstrumentMeasure(measureFromInstrument as Measure, true);
            }
            #endregion

            #region Selection


            internal override void OnInstrumentChanged(I.Sensor i = null)
            {
                base.OnInstrumentChanged(i);
                View.UpdateInstrumentView();
            }

            /// <summary>
            /// selection de l'instrument pour les mesures de tilt.
            /// </summary>
            /// <returns></returns>
            internal void SelectionTiltMeter()
            {
                //TiltMeter tiltMeterSelected = null;
                //Ne force plus de s�lectionner un instrument
                //selection de l'instrument pour les mesures de tilt.
                var preselected = new List<TsuObject>() { _ActualStationTilt.ParametersBasic._Instrument };
                var selectables = _InstrumentManager.GetByClassesAndTypes(
                    new List<I.InstrumentClasses>
                    {
                        I.InstrumentClasses.TILTMETRE,
                        I.InstrumentClasses.JAUGE_A_TILT,
                        I.InstrumentClasses.GABARIT,
                        I.InstrumentClasses.INTERFACE,
                    });
                _InstrumentManager.SelectInstrument(R.StringTilt_ChooseTiltmeter, selectables, multiSelection: false, preselected, true);
                //tiltMeterSelected = this._InstrumentManager.SelectInstrument(R.StringTilt_ChooseTiltmeter) as TiltMeter;
                //this.SelectionTiltMeter(tiltMeterSelected);
            }

            /// <summary>
            /// Chnagement du tiltmeter
            /// </summary>
            /// <param name="instrument"></param>
            internal void SelectionTiltMeter(I.TiltMeter tiltMeterSelected)
            {
                if (tiltMeterSelected != null)
                {
                    if (_InitStationTilt.ParametersBasic._Instrument != null)
                    {
                        if (_InitStationTilt.ParametersBasic._Instrument._Name != tiltMeterSelected._Name)
                        {
                            _InitStationTilt.ParametersBasic.LastChanged = DateTime.Now;
                            _AdjustingStationTilt.ParametersBasic.LastChanged = DateTime.Now;
                            _InitStationTilt.TiltParameters._Instrument = tiltMeterSelected;
                            _InitStationTilt.ParametersBasic._Instrument = tiltMeterSelected;
                            _InitStationTilt.TiltParameters._Instrument._Model = tiltMeterSelected._Model;
                            _InitStationTilt.ParametersBasic._Instrument._Model = tiltMeterSelected._Model;
                        }
                        CheckIfReadyToBeSaved(_InitStationTilt);
                    }
                    if (_AdjustingStationTilt.ParametersBasic._Instrument != null)
                    {
                        if (_AdjustingStationTilt.ParametersBasic._Instrument._Name != tiltMeterSelected._Name)
                        {
                            _InitStationTilt.ParametersBasic.LastChanged = DateTime.Now;
                            _AdjustingStationTilt.ParametersBasic.LastChanged = DateTime.Now;
                            _AdjustingStationTilt.TiltParameters._Instrument = tiltMeterSelected;
                            _AdjustingStationTilt.ParametersBasic._Instrument = tiltMeterSelected;
                            _AdjustingStationTilt.TiltParameters._Instrument._Model = tiltMeterSelected._Model;
                            _AdjustingStationTilt.ParametersBasic._Instrument._Model = tiltMeterSelected._Model;
                        }
                        CheckIfReadyToBeSaved(_AdjustingStationTilt);
                    }
                    foreach (Station stationTilt in _StationTiltSequences)
                    {
                        if (stationTilt.TiltParameters._Instrument._Name == R.StringTilt_Unknown)
                        {
                            stationTilt.TiltParameters._Instrument = tiltMeterSelected;
                            stationTilt.TiltParameters._Instrument._Model = tiltMeterSelected._Model;
                            stationTilt.ParametersBasic._Instrument = tiltMeterSelected;
                            stationTilt.ParametersBasic._Instrument._Model = tiltMeterSelected._Model;
                            stationTilt.ParametersBasic.LastChanged = DateTime.Now;
                        }
                    }
                    View?.UpdateView();
                }
            }
            /// <summary>
            /// selection de l'element � aligner
            /// </summary>
            internal void SelectOneElement()
            //Selectionne des �lements dans un fichier theo
            {
                SetSelectedMagnetInElementModule();
                // ouvre la fen�tre du module �lement de s�lection des points 
                ElementModule.SelectOnePoint();
                ///la validation de la fen�tre �lement manager envoie �galement les points avec � l'observateur final qui lance la fonction SetPointsToMeasure
            }
            /// <summary>
            /// selection de la s�quence de mesure des �l�ments dans le fichier theo
            /// </summary>
            internal void SelectSequenceOfElement()
            /// selection de la s�quence de mesure des �l�ments dans le fichier theo
            {
                SetSelectedMagnetInElementModule();
                ElementModule.View.buttons.HideOnlyButtons(ElementModule.View.buttons.sequence);
                ElementModule.SelectMagnets();
                ///la validation de la fen�tre �lement manager envoie �galement les points avec � l'observateur final qui lance la fonction SetPointsToMeasure
            }
            /// <summary>
            /// D�fini les �l�ments affich�s et d�j� coch�s dans l'element module
            /// </summary>
            internal void SetSelectedMagnetInElementModule()
            {
                ElementModule.MultiSelection = true;
                ElementModule._SelectedObjects.Clear();
                ElementModule.SetSelectableToAllMagnets();
                //this.ElementModule.AddSelectedObjects(this.FinalModule.ElementsTobeMeasured.Cast<TsuObject>().ToList());
                ElementModule.AddSelectedObjects(ElementsTobeMeasured.Cast<TsuObject>().ToList());
            }
            /// <summary>
            /// Met � jour les mesures et coordonn�es theo suite � une modification dans l'element manager
            /// </summary>
            /// <param name="originalPoint"></param>
            /// <param name="pointModified"></param>
            internal override void UpdateTheoCoordinates(Point originalPoint, Point pointModified)
            {
                bool update = false;
                foreach (Station st in _StationTiltSequences)
                {
                    if ((st.TheoPoint._Name == originalPoint._Name) && (st.TheoPoint._Origin == originalPoint._Origin))
                        st.TheoPoint.AssignProperties(pointModified);
                }
                if (_ActualStationTilt.TheoPoint._Name == originalPoint._Name && _ActualStationTilt.TheoPoint._Origin == originalPoint._Origin)
                {
                    _ActualStationTilt.TheoPoint.AssignProperties(pointModified);
                    update = true;
                }
                if (_AdjustingStationTilt.TheoPoint._Name == originalPoint._Name && _AdjustingStationTilt.TheoPoint._Origin == originalPoint._Origin)
                    _AdjustingStationTilt.TheoPoint.AssignProperties(pointModified);
                if (_InitStationTilt.TheoPoint._Name == originalPoint._Name && _InitStationTilt.TheoPoint._Origin == originalPoint._Origin)
                    _InitStationTilt.TheoPoint.AssignProperties(pointModified);

                if (update) UpdateOffset();
            }

            /// <summary>
            /// Set the magnets selected in the element module 
            /// </summary>
            /// <param name="magnetsSelected"></param>
            internal void SetPointsToMeasure(List<C.Magnet> magnetsSelected)
            {
                if (magnetsSelected != null || magnetsSelected.Count != 0)
                {
                    //this.CheckMagnetNotAlreadySelected(magnetsSelected);

                    FinalModule.ElementsTobeMeasured.Clear();
                    FinalModule.ElementsTobeMeasured.AddRange(magnetsSelected);
                    ElementsTobeMeasured.Clear();
                    ElementsTobeMeasured.AddRange(magnetsSelected);
                    //this.FinalModule.MagnetToElement_ElementsTobeMeasured(magnetsSelected);
                    List<Point> pointsSelected = ChangeToMagnetRollRefPoint(magnetsSelected);

                    CleanStationTiltSequence(pointsSelected);

                    for (int i = 0; i < pointsSelected.Count; i++)
                    {
                        Point point = pointsSelected[i];
                        bool pointAlreadyThere = false;
                        pointAlreadyThere = _StationTiltSequences.Exists(
                            x => x.TheoPoint._Accelerator == point._Accelerator
                                && x.TheoPoint._ClassAndNumero == point._ClassAndNumero
                        );

                        if (!pointAlreadyThere)
                        {
                            //Cree une station tilt mesure initiale et reglage pour chaque point theo
                            Station newStationInit = null;
                            newStationInit = new Station();
                            Measure m = null;
                            m = new Measure();
                            newStationInit._MeasuresTilt.Add(m);
                            newStationInit.TiltParameters._TiltInitOrAdjusting = E.TiltInitOrAdjusting.Initial;
                            newStationInit.TiltParameters._Instrument = _InitStationTilt.ParametersBasic._Instrument as I.TiltMeter;
                            newStationInit.TiltParameters._Operation = _InitStationTilt.ParametersBasic._Operation;
                            newStationInit.TiltParameters._Team = _InitStationTilt.ParametersBasic._Team;
                            newStationInit.ParametersBasic._Instrument = _InitStationTilt.ParametersBasic._Instrument;
                            newStationInit.ParametersBasic._Operation = _InitStationTilt.ParametersBasic._Operation;
                            newStationInit.ParametersBasic._Team = _InitStationTilt.ParametersBasic._Team;
                            newStationInit.TiltParameters._Tolerance = _InitStationTilt.TiltParameters._Tolerance;
                            newStationInit.ParametersBasic.LastChanged = DateTime.Now;
                            newStationInit.TheoPoint = point;
                            newStationInit._MeasPoint = point.DeepCopy();
                            m._Point = newStationInit._MeasPoint;
                            newStationInit.Rename();
                            newStationInit.CreateMeasForSPSDipole();
                            _StationTiltSequences.Add(newStationInit);
                            Station newStationAdjusting = null;
                            newStationAdjusting = (Station)newStationInit.Clone();
                            newStationAdjusting.TiltParameters._TiltInitOrAdjusting = E.TiltInitOrAdjusting.Adjusting;
                            newStationAdjusting.TiltParameters._Operation = _AdjustingStationTilt.ParametersBasic._Operation;
                            newStationAdjusting.Rename();
                            _StationTiltSequences.Add(newStationAdjusting);
                        }
                    }

                    SortMeasure();

                    if (_ActualStationTilt.TheoPoint._Name == R.StringTilt_Unknown
                        || !_StationTiltSequences.Exists(x => x.TheoPoint._Name == _ActualStationTilt.TheoPoint._Name))
                    {
                        foreach (Station stationTilt in _StationTiltSequences)
                        {
                            if (stationTilt.TiltParameters._TiltInitOrAdjusting == E.TiltInitOrAdjusting.Initial)
                            {
                                _ActualStationTilt = stationTilt;
                                _InitStationTilt = stationTilt;
                                foreach (Station stationTilt2 in _StationTiltSequences)
                                {
                                    if ((stationTilt2.TheoPoint._Name == _ActualStationTilt.TheoPoint._Name) && (stationTilt2.TiltParameters._TiltInitOrAdjusting == E.TiltInitOrAdjusting.Adjusting))
                                    {
                                        _AdjustingStationTilt = stationTilt2;
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                }

                UpdateOffset();
            }

            /// <summary>
            /// Enleve les stations tilt de la sequence si elle ne sont plus pr�sentes dans la liste des points s�lectionn�s
            /// </summary>
            /// <param name="pointsSelected"></param>
            private void CleanStationTiltSequence(List<Point> pointsSelected)
            {
                List<Station> stationTiltToRemove = new List<Station>();
                foreach (Station stationTilt in _StationTiltSequences)
                {
                    if (pointsSelected.Find(x =>
                        x._Accelerator == stationTilt.TheoPoint._Accelerator
                        && x._ClassAndNumero == stationTilt.TheoPoint._ClassAndNumero) == null)
                    {
                        stationTiltToRemove.Add(stationTilt);
                    }
                }
                foreach (Station stationTilt in stationTiltToRemove)
                {
                    _StationTiltSequences.Remove(stationTilt);
                }
            }
            /// <summary>
            /// Override the method in Final Module when received event to add points to measure
            /// </summary>
            /// <param name="compositeSelected"></param>
            internal override void SetPointsToMeasure(C.CompositeElement compositeSelected)
            {
                List<C.Magnet> magnetsList = new List<C.Magnet>();
                foreach (var item in compositeSelected)
                {
                    if (item is C.Magnet)
                    {
                        magnetsList.Add(item as C.Magnet);
                    }
                    if (item is Point)
                    {
                        Point ptSelected = item as Point;
                        if (ptSelected.IsAlesage)
                        {
                            ElementModule.SetSelectableToAllMagnets();
                            int index = ElementModule.SelectableObjects.FindIndex(x => (x as C.Magnet)._Accelerator == ptSelected._Accelerator
                           && (x as C.Magnet)._ClassAndNumero == ptSelected._ClassAndNumero);
                            if (index != -1)
                            {
                                C.Magnet magnet = ElementModule.SelectableObjects[index] as C.Magnet;
                                foreach (Point pt in magnet.GetAlesages())
                                {
                                    if (ptSelected._Name == pt._Name)
                                    {
                                        pt.IsReferenceForRoll = true;
                                    }
                                    else
                                    {
                                        pt.IsReferenceForRoll = false;
                                    }
                                }
                                magnetsList.Add(magnet);
                            }
                        }
                    }
                }
                /// Permet d'afficher les aimants au lieu des points lors ouverture d'un fichier theo dans l'element manager
                if (magnetsList.Count == 0)
                {
                    ElementModule.SetSelectableToAllMagnets();
                    ElementModule.UpdateView();
                }
                else
                {
                    SetPointsToMeasure(magnetsList);
                }

            }
            /// <summary>
            /// Si les points proviennent d'un fichier dat Geode, utilise le point entr�e ou le point choisi manuellement par l'utilisateur comme pt de ref tilt
            /// return les �l�ments sous la forme d'une liste de points
            /// </summary>
            /// <param name="pointsSelected"></param>
            internal List<Point> ChangeToMagnetRollRefPoint(List<C.Magnet> magnetsSelected)
            {
                List<Point> pointsSelected = new List<Point>();
                foreach (C.Magnet magnet in magnetsSelected)
                {
                    // S�lectionne le point entr�e comme point de r�f�rence pour le tilt de l'aimant
                    List<Point> allMagnetPoints = magnet.GetAlesages();
                    int index = allMagnetPoints.FindIndex(x => x.IsReferenceForRoll);
                    //Choisi le pt ref tilt de l'utilisateur en priorit�
                    if (index != -1)
                    {
                        pointsSelected.Add(allMagnetPoints[index]);
                    }
                    else
                    {
                        if (C.Magnet.GetReferenceAlesageForTilt(out Point entryPoint, allMagnetPoints))
                        {
                            if (!pointsSelected.Exists(x => x._Name == entryPoint._Name))
                                pointsSelected.Add(entryPoint);
                        }
                        //// choisi le pt E comme tilt ref par d�faut
                        //Point entryPoint = allMagnetPoints.Find(x => x._Point == "E" && x._Origin.Contains(".dat"));
                        //if (entryPoint != null && !pointsSelected.Exists(x => x._Name == entryPoint._Name))
                        //{
                        //    pointsSelected.Add(entryPoint);
                        //}
                        //else
                        //{
                        //    ///Dans le cas ou un aimant n'a pas d'entr�e, ajoute le premier point de l'aimant
                        //    if (allMagnetPoints.Count > 0)
                        //    {
                        //        // pointsSelected.Add(allMagnetPoints[0]); // should be ok, but to make sure:
                        //        pointsSelected.Add(allMagnetPoints.OrderBy(x => x._Parameters.Cumul).First());
                        //    }
                        //}
                    }
                }
                return pointsSelected;
            }
            /// <summary>
            /// change l'�quipe dans la station
            /// </summary>
            /// <param name="newTeam"></param>
            internal void ChangeTeam(string newTeam)
            //change l'�quipe dans la station
            {
                if (_ActualStationTilt.ParametersBasic._Team != newTeam)
                {
                    foreach (Station stationTilt in _StationTiltSequences)
                    {
                        stationTilt.TiltParameters._Team = newTeam;
                        stationTilt.ParametersBasic.LastChanged = DateTime.Now;
                    }
                    _ActualStationTilt.TiltParameters._Team = newTeam;
                    _InitStationTilt.TiltParameters._Team = newTeam;
                    _AdjustingStationTilt.TiltParameters._Team = newTeam;
                    _ActualStationTilt.ParametersBasic._Team = newTeam;
                    _InitStationTilt.ParametersBasic._Team = newTeam;
                    _AdjustingStationTilt.ParametersBasic._Team = newTeam;
                    _ActualStationTilt.ParametersBasic.LastChanged = DateTime.Now;
                    _AdjustingStationTilt.ParametersBasic.LastChanged = DateTime.Now;
                    _InitStationTilt.ParametersBasic.LastChanged = DateTime.Now;
                    _Station.ParametersBasic.LastChanged = DateTime.Now;
                    _Station.ParametersBasic._Team = newTeam;
                    CheckIfReadyToBeSaved();
                    View.UpdateView();
                    TSU.Tsunami2.Preferences.Values.GuiPrefs.AddLastTeamtUsed(newTeam);
                }

            }
            /// <summary>
            /// change le num�ro d'op�ration dans la station
            /// </summary>
            /// <param name="newOpId"></param>
            internal override void ChangeOperationID(O.Operation newOperation)
            //change le num�ro d'op�ration dans la station
            {
                if (_ActualStationTilt == null) return;
                if (newOperation == null) newOperation = O.Operation.NoOp;
                if (newOperation._Name != _ActualStationTilt.ParametersBasic._Operation._Name)
                {
                    foreach (Station stationTilt in _StationTiltSequences)
                    {
                        if (stationTilt.TiltParameters._TiltInitOrAdjusting == _ActualStationTilt.TiltParameters._TiltInitOrAdjusting)
                        {
                            stationTilt.TiltParameters._Operation = newOperation;
                            stationTilt.ParametersBasic._Operation = newOperation;
                            stationTilt.ParametersBasic.LastChanged = DateTime.Now;
                        }
                    }
                    switch (_ActualStationTilt.TiltParameters._TiltInitOrAdjusting)
                    {
                        case E.TiltInitOrAdjusting.Initial:
                            _InitStationTilt.TiltParameters._Operation = newOperation;
                            _InitStationTilt.ParametersBasic._Operation = newOperation;
                            _InitStationTilt.ParametersBasic.LastChanged = DateTime.Now;
                            break;
                        case E.TiltInitOrAdjusting.Adjusting:
                            _AdjustingStationTilt.TiltParameters._Operation = newOperation;
                            _AdjustingStationTilt.ParametersBasic._Operation = newOperation;
                            _AdjustingStationTilt.ParametersBasic.LastChanged = DateTime.Now;
                            break;
                        default:
                            break;
                    }
                    CheckIfReadyToBeSaved();
                    View.UpdateView();
                }

                TSU.Tsunami2.Preferences.Values.GuiPrefs.LastOpUsed = newOperation;

            }
            /// <summary>
            /// Selectionne le num�ro d'op�ration
            /// </summary>
            //internal void SelectOperation()
            //{
            //    //if (o != null)
            //    //{
            //    //    this.ChangeOperationID(o);
            //    //}
            //}
            /// <summary>
            /// Change la tol�rance d'alignement dans la station tilt actuelle
            /// </summary>
            /// <param name="newTolerance"></param>
            internal void ChangeTolerance(double newTolerance)
            {
                foreach (Station st in _StationTiltSequences)
                {
                    st.TiltParameters._Tolerance = Math.Abs(newTolerance);
                }
                _ActualStationTilt.TiltParameters._Tolerance = Math.Abs(newTolerance);
                _AdjustingStationTilt.TiltParameters._Tolerance = Math.Abs(newTolerance);
                _InitStationTilt.TiltParameters._Tolerance = Math.Abs(newTolerance);
                //this.CheckIfReadyToBeSaved();
                View.UpdateView();
            }

            /// <summary>
            /// S�lectionne les stations dans la liste de station du final module en fonction du point theo
            /// </summary>
            /// <param name="pointTheo"></param>
            internal void ChangePointTheo(string pointTheo)
            {
                E.TiltInitOrAdjusting typeTilt = _ActualStationTilt.TiltParameters._TiltInitOrAdjusting;
                foreach (Station stationTilt in _StationTiltSequences)
                {
                    if (stationTilt.TheoPoint._Name == pointTheo)
                    {
                        switch (stationTilt.TiltParameters._TiltInitOrAdjusting)
                        {
                            case E.TiltInitOrAdjusting.Initial:
                                _InitStationTilt = stationTilt;
                                break;
                            case E.TiltInitOrAdjusting.Adjusting:
                                _AdjustingStationTilt = stationTilt;
                                break;
                            default:
                                break;
                        }
                    }
                }
                switch (typeTilt)
                {
                    case E.TiltInitOrAdjusting.Initial:
                        _ActualStationTilt = _InitStationTilt;
                        break;
                    case E.TiltInitOrAdjusting.Adjusting:
                        _ActualStationTilt = _AdjustingStationTilt;
                        break;
                    default:
                        _ActualStationTilt = _InitStationTilt;
                        break;
                }
                State initState = _InitStationTilt.TiltParameters._State;
                State adjState = _AdjustingStationTilt.TiltParameters._State;
                View?.UpdateView();
                _InitStationTilt.TiltParameters._State = initState;
                _AdjustingStationTilt.TiltParameters._State = adjState;
            }

            ///// <summary>
            ///// efface la station tilt avec le point theo design� de la liste des station tilt du final module
            ///// </summary>
            ///// <param name="pointTheo"></param>
            //internal void DeletePointInSequence(string pointTheo)
            //{
            //    List<StationTilt> stationsToRemove = new List<StationTilt>();
            //    int indexOfNewStationTilt = 0;
            //    foreach (StationTilt stationTilt in this._StationTiltSequences)
            //    {
            //        if (stationTilt.TheoPoint._Name == pointTheo)
            //        {
            //            stationsToRemove.Add(stationTilt);
            //            indexOfNewStationTilt = this._StationTiltSequences.IndexOf(stationTilt);
            //        }
            //    }
            //    //Change pour la station suivante ou pr�c�dente si c'�tait la station actuelle
            //    if ((this._ActualStationTilt.TheoPoint._Name == pointTheo))
            //    {
            //        if (indexOfNewStationTilt <= this._StationTiltSequences.Count - 3)
            //        {
            //            this.GoNextPoint();
            //        }
            //        else
            //        {
            //            this.GoPreviousPoint();
            //        }
            //    }
            //    if (this._StationTiltSequences.Count >= stationsToRemove.Count + 2)
            //    {
            //        foreach (StationTilt stationToRemove in stationsToRemove)
            //        {
            //            this._StationTiltSequences.Remove(stationToRemove);
            //            Element elementToRemove = this.FinalModule.ElementsTobeMeasured.Find(x =>
            //                    x._Accelerator == stationToRemove.TheoPoint._Accelerator
            //                    && x._ClassAndNumero == stationToRemove.TheoPoint._ClassAndNumero);
            //            if (elementToRemove != null) { this.FinalModule.ElementsTobeMeasured.Remove(elementToRemove); }
            //            elementToRemove = this.ElementsTobeMeasured.Find(x =>
            //                    x._Accelerator == stationToRemove.TheoPoint._Accelerator
            //                    && x._ClassAndNumero == stationToRemove.TheoPoint._ClassAndNumero);
            //            if (elementToRemove != null) { this.ElementsTobeMeasured.Remove(elementToRemove); }
            //        }
            //    }
            //    else
            //    {
            //        this.View.ShowMessageOfCritical(R.StringTilt_ErrorSequence, R.T_OK);
            //    }
            //    this.UpdateOffset();

            //}
            /// <summary>
            /// Efface le point des mesures tilt � faire
            /// </summary>
            /// <param name="pointTheo"></param>
            internal void DeletePoint(string pointTheo)
            {
                List<Station> stationsToRemove = new List<Station>();
                foreach (Station stationTilt in _StationTiltSequences)
                {
                    if (stationTilt.TheoPoint._Name == pointTheo)
                    {
                        stationsToRemove.Add(stationTilt);
                    }
                }
                foreach (Station stationToRemove in stationsToRemove)
                {
                    _StationTiltSequences.Remove(stationToRemove);
                    Element elementToRemove = FinalModule.ElementsTobeMeasured.Find(
                        x => x._Accelerator == stationToRemove.TheoPoint._Accelerator
                          && x._ClassAndNumero == stationToRemove.TheoPoint._ClassAndNumero
                    );
                    if (elementToRemove != null)
                    {
                        FinalModule.ElementsTobeMeasured.Remove(elementToRemove);
                    }
                    elementToRemove = ElementsTobeMeasured.Find(
                        x => x._Accelerator == stationToRemove.TheoPoint._Accelerator
                          && x._ClassAndNumero == stationToRemove.TheoPoint._ClassAndNumero
                    );
                    if (elementToRemove != null)
                    {
                        ElementsTobeMeasured.Remove(elementToRemove);
                    }
                }
                UpdateOffset();

            }
            /// <summary>
            /// Passe au point suivant de la s�quence
            /// </summary>
            //internal void GoNextPoint()
            //{
            //    int actualInitIndex = this._StationTiltSequences.LastIndexOf(this._InitStationTilt);
            //    int totalNumberOfTiltStation = this._StationTiltSequences.Count;
            //    if (actualInitIndex <= this._StationTiltSequences.Count - 3)
            //    {
            //        string newPointTheo = this._StationTiltSequences[actualInitIndex + 2].TheoPoint._Name;
            //        this.ChangePointTheo(newPointTheo);
            //    }
            //}
            /// <summary>
            /// Passe au point pr�c�dent de la s�quence
            /// </summary>
            //internal void GoPreviousPoint()
            //{
            //    int actualInitIndex = this._StationTiltSequences.LastIndexOf(this._InitStationTilt);
            //    if (actualInitIndex >= 2)
            //    {
            //        string newPointTheo = this._StationTiltSequences[actualInitIndex - 2].TheoPoint._Name;
            //        this.ChangePointTheo(newPointTheo);
            //    }
            //}
            /// <summary>
            /// Trie les stations tilts par dcum ou inverse dcum
            /// </summary>
            internal void SortTiltStations()
            {
                _StationTiltSequences.Sort();
                if (sortDirection == E.SortDirection.InvDcum)
                {
                    _StationTiltSequences.Reverse();
                }
            }
            #endregion

            #region Management.Measure
            internal void ChangeComment(string MsgResponse)
            //change le commentaire de la station tilt
            {
                //MsgResponse = MsgResponse.ToUpper();
                if (_ActualStationTilt.TiltParameters._Comment != MsgResponse)
                {
                    _ActualStationTilt.TiltParameters._Comment = MsgResponse;
                    _ActualStationTilt.ParametersBasic.LastChanged = DateTime.Now;
                    CheckIfReadyToBeSaved();
                    View.UpdateView();
                }
            }

            //Permet lorsqu'on cr�e un point � partir de l'element manager de mettre l'origine � created by user
            internal override string GetNewPointGroupName()
            {
                return R.T_CREATED_BY_USER;
            }

            /// <summary>
            /// Met � jour la mesure tilt "measureNumber" dans le sens faisceau
            /// </summary>
            /// <param name="measureNumber"></param>
            /// <param name="measureFromInstrument"></param>
            internal void UpdateMeasuresBeam(int measureNumber, Measure measureFromInstrument)
            {
                //Met � jour la mesure tilt "measureNumber" dans le sens faisceau
                if (measureFromInstrument._ReadingBeamDirection.Value == na) /// lecture sens faisceau effac�e
                {
                    ResetMeasForBeamDirectionReadingDeleted(measureNumber);
                }
                else
                {
                    if (_ActualStationTilt._MeasuresTilt[measureNumber]._ReadingBeamDirection.Value != measureFromInstrument._ReadingBeamDirection.Value)
                    {
                        _ActualStationTilt._MeasuresTilt[measureNumber]._ReadingBeamDirection.Value = measureFromInstrument._ReadingBeamDirection.Value;
                        _ActualStationTilt._MeasuresTilt[measureNumber]._ReadingBeamDirection.Sigma = measureFromInstrument._ReadingBeamDirection.Sigma;
                        _ActualStationTilt.ParametersBasic.LastChanged = DateTime.Now;
                    }
                }
            }
            /// <summary>
            /// Met � jour la station tilt dans le cas ou l'on efface une lecture sens faisceau
            /// </summary>
            /// <param name="measureNumber"></param>
            internal void ResetMeasForBeamDirectionReadingDeleted(int measureNumber)
            {
                _ActualStationTilt.ResetMeasForBeamDirectionReadingDeleted(measureNumber);
            }

            internal void UpdateMeasuresOppositeBeam(int measureNumber, Measure measureFromInstrument)
            {
                //Met � jour la mesure tilt "measureNumber" dans le sens faisceau
                if (measureFromInstrument._ReadingBeamDirection.Value == na) /// lecture sens oppos� faisceau effac�e
                {
                    ResetMeasForBeamOppositeDirectionReadingDeleted(measureNumber);
                }
                else
                {
                    if (_ActualStationTilt._MeasuresTilt[measureNumber]._ReadingOppositeBeam.Value != measureFromInstrument._ReadingBeamDirection.Value)
                    {
                        _ActualStationTilt._MeasuresTilt[measureNumber]._ReadingOppositeBeam.Value = measureFromInstrument._ReadingBeamDirection.Value;
                        _ActualStationTilt._MeasuresTilt[measureNumber]._ReadingOppositeBeam.Sigma = measureFromInstrument._ReadingBeamDirection.Sigma;
                        _ActualStationTilt.ParametersBasic.LastChanged = DateTime.Now;
                    }
                }
            }
            internal void ResetMeasForBeamOppositeDirectionReadingDeleted(int measureNumber)
            {
                if (_ActualStationTilt != null)
                    _ActualStationTilt.ResetMeasForBeamOppositeDirectionReadingDeleted(measureNumber);
            }
            /// <summary>
            /// Met � jour les �carts
            /// </summary>
            internal void UpdateOffset()
            //Met � jour les �carts
            {
                foreach (var meas in _ActualStationTilt._MeasuresTilt)
                {
                    this.CheckBocValidity(meas);
                }
                _ActualStationTilt.AverageTiltCalculation();
                //Desactivation de l'ajout des points mesur�s en tilt dans l'element module car je ne vois pas le cas ou l'on veut utiliser une mesure de tilt pour d'autres calculs
                //this.AddAllElementsToElementModule();
                AddAllMeasuresToFinalModule();
                CheckIfReadyToBeSaved();
                View?.UpdateView();
                //this.Change();
                FinalModule.Save();
            }
            /// <summary>
            /// Ajoute les derni�res mesures dans le final module dans la liste des received measures pour l'element alignment
            /// </summary>
            private void AddAllMeasuresToFinalModule()
            {
                RemoveAllMeasuresFromFinalModule();
                List<Measure> measToAdd = new List<Measure>();
                foreach (Station st in _StationTiltSequences)
                {
                    if (st.TiltParameters._TiltInitOrAdjusting == E.TiltInitOrAdjusting.Adjusting && st._EcartTheo.Value != 9999)
                    {
                        Measure m = new Measure();
                        m._Point = st.TheoPoint.DeepCopy();
                        m._InstrumentSN = this.tempInstrument._Name;
                        m._Ecart.Value = st._EcartTheo.Value;
                        m._AverageBothDirection.Value = st._MeasureAverageBothDirection.Value;
                        measToAdd.Add(m);
                    }
                }
                if (measToAdd.Count != 0)
                {
                    FinalModule.ReceivedMeasures.AddRange(measToAdd);
                }
            }
            /// <summary>
            /// Enl�ve toutes les mesuresOfOffset du final module dans la liste des received measures
            /// </summary>
            private void RemoveAllMeasuresFromFinalModule()
            {
                FinalModule.ReceivedMeasures.RemoveAll(x => x is Measure);
            }
            /// <summary>
            /// v�rifie si la station peut-�tre sauv�e et met le status � jour dans les param�tres
            /// </summary>
            /// <returns></returns>
            internal bool CheckIfReadyToBeSaved(Station stationTilt)
            // v�rifie si la station peut-�tre sauv�e et met le status � jour dans les param�tres
            {
                bool a = stationTilt._MeasureAverageBothDirection.Value != 9999;
                bool b = stationTilt.TiltParameters._Team != R.String_UnknownTeam;
                //bool c = stationTilt.TiltParameters._Operation.IsSet;
                bool d = stationTilt.TiltParameters._Instrument._Name != R.StringTilt_Unknown;
                bool e = stationTilt.TheoPoint._Name != R.StringTilt_Unknown;
                //bool f = CheckAllTiltValueEntered(stationTilt); // demande Elisabeth d'avoir la sauvegarde possible si pas tous les tilts encod�
                bool g = stationTilt.ParametersBasic.LastChanged > stationTilt.ParametersBasic.LastSaved;
                //if (a && b && d && e && f && g) //operation can be equal to 9999 for saving
                if (a && b && d && e && g)
                {
                    stationTilt.TiltParameters._State = new State.StationTiltReadyToBeSaved();
                    return true;
                }
                else
                {
                    //if (a && b && d && e && f)
                    if (a && b && d && e)
                    {
                        stationTilt.TiltParameters._State = new State.StationTiltSaved();
                    }
                    return false;
                }
            }
            /// <summary>
            /// v�rifie si la station peut-�tre sauv�e et met le status � jour dans les param�tres
            /// </summary>
            /// <returns></returns>
            internal bool CheckIfReadyToBeSaved()
            // v�rifie si la station peut-�tre sauv�e et met le status � jour dans les param�tres
            {
                return CheckIfReadyToBeSaved(_ActualStationTilt);
            }
            /// <summary>
            /// Trie les stations tilt par dcum ou inverse dcum
            /// </summary>
            internal void SortMeasure()
            {
                //this.SortMagnets(this.FinalModule.MagnetsToBeAligned);
                SortPoints(FinalModule.PointsToBeAligned);
                SortTiltStations();
            }
            #endregion

            #region save in file
            /// <summary>
            /// Sauvegarde la station tilt en affichant ou pas le fichier suvegard�
            /// </summary>
            /// <param name="showFile"></param>
            /// <returns></returns>
            internal Result ExportToGeode(bool showFile = true, bool showMessageOfSuccess = true,
                TiltStationToGeode.OnExisting onExisting = TiltStationToGeode.OnExisting.AppendWithComment)
            {
                Result result = new Result();
                Station actualStationTilt = _ActualStationTilt;
                string geodeFilePath = TiltStationToGeode.ExportToGeode(actualStationTilt, onExisting);
                //Affichage du fichier enregistr� et s�lection de l'�tat de la station
                if (File.Exists(geodeFilePath))
                {
                    result.Success = true;
                    actualStationTilt.TiltParameters._State = new State.StationTiltSaved();
                    actualStationTilt.ParametersBasic.LastSaved = DateTime.Now;
                    actualStationTilt.ParametersBasic._GeodeDatFilePath = geodeFilePath;
                    foreach (Station item in _StationTiltSequences)
                    {
                        if (item.TiltParameters._TiltInitOrAdjusting == actualStationTilt.TiltParameters._TiltInitOrAdjusting)
                        {
                            item.ParametersBasic._GeodeDatFilePath = geodeFilePath;
                        }
                    }
                    if (showFile)
                    {
                        Shell.Run(geodeFilePath);
                    }
                    DsaFlag dsaFlag = DsaFlag.GetByNameOrAdd(FinalModule.DsaFlags, "ShowSaveMessageOfSuccess");
                    if (showMessageOfSuccess)
                    {
                        string titleAndMessage = null;
                        string respond = "";
                        switch (actualStationTilt.TiltParameters._TiltInitOrAdjusting)
                        {
                            case E.TiltInitOrAdjusting.Initial:
                                titleAndMessage = string.Format(
                                    R.StringTilt_TiltInitExported,
                                    geodeFilePath,
                                    actualStationTilt.TheoPoint._Name);
                                break;
                            case E.TiltInitOrAdjusting.Adjusting:
                                titleAndMessage = string.Format(
                                    R.StringTilt_TiltAdjExported,
                                    geodeFilePath,
                                    actualStationTilt.TheoPoint._Name);
                                break;
                            default:
                                break;
                        }

                        base.ShowGeodeExportSuccess(titleAndMessage, geodeFilePath);
                        //if (titleAndMessage != null)
                        //{
                        //    respond = new MessageInput(MessageType.GoodNews, titleAndMessage)
                        //    {
                        //        ButtonTexts = new List<string> { R.T_OK, R.T_OpenFileLocation },
                        //        DontShowAgain = dsaFlag
                        //    }.Show().TextOfButtonClicked;
                        //}
                        //if (respond == R.T_OpenFileLocation && File.Exists(geodeFilePath))
                        //{
                        //    Process.Start(new ProcessStartInfo("explorer.exe", " /select, " + geodeFilePath));
                        //}
                    }
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = R.StringTilt_DatNotCreated;
                    actualStationTilt.TiltParameters._State = new State.StationTiltReadyToBeSaved();
                    View.ShowMessage(R.StringTilt_TiltNotExported);
                }
                View?.UpdateView();
                Change();
                View?.UpdateView();
                FinalModule.Save();

                return result;
            }


            /// <summary>
            /// ask the view to show interface choices and then trigger or not the interface cahnge in the module
            /// </summary>
            /// <param name="measure"></param>
            internal void ChangeInterfaces()
            {
                var currentStation = _ActualStationTilt;
                this.View.SelectInterfaces(
                    previousInterFaces: currentStation._MeasuresTilt[0].Interfaces,
                    currenObject: currentStation,
                    model: _InitialStationsTilt,
                    fan: currentStation,
                    additionalAction: UpdateView);
            }

            /// <summary>
            /// Replace the interface in a meaure
            /// </summary>
            /// <param name="currenObject"></param>
            /// <param name="newInterFaces"></param>
            internal override void ChangeInterfacesBy(TsuObject currenObject, Interfaces newInterFaces)
            {
                var stationTilt = currenObject as Tilt.Station;
                stationTilt._MeasuresTilt[0].Interfaces = newInterFaces;
            }
            #endregion

        }
    }
}