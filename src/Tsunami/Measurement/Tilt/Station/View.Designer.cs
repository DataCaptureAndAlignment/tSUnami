﻿
using TSU.Common;
using TSU.Views;
namespace TSU.Tilt
{
    public partial class Station
    {
        partial class View
        {
            /// <summary>
            /// Required designer variable.
            /// </summary>
            private System.ComponentModel.IContainer components = null;

            /// <summary>
            /// Clean up any resources being used.
            /// </summary>
            /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }

            #region Windows Form Designer generated code

            /// <summary>
            /// Required method for Designer support - do not modify
            /// the contents of this method with the code editor.
            /// </summary>
            private void InitializeComponent()
            {
                this.splitContainer1 = new System.Windows.Forms.SplitContainer();
                Tsunami2.Preferences.Theme.ApplyTo(splitContainer1);
                //this.tableLayoutPanel_ButtonTop = new System.Windows.Forms.TableLayoutPanel();
                this.tableLayoutPanel_MainTop = new System.Windows.Forms.TableLayoutPanel();
                this._PanelBottom.SuspendLayout();
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
                this.splitContainer1.SuspendLayout();
                this.SuspendLayout();
                // 
                // instrumentView
                // 
                this.instrumentView.Location = new System.Drawing.Point(75, 75);
                // 
                // _PanelBottom
                // 
                this._PanelBottom.Controls.Add(this.splitContainer1);
                this._PanelBottom.Location = new System.Drawing.Point(5, 80);
                this._PanelBottom.MinimumSize = new System.Drawing.Size(0, 120);
                this._PanelBottom.Size = new System.Drawing.Size(1202, 651);
                // 
                // _PanelTop
                // 
                this._PanelTop.Size = new System.Drawing.Size(1202, 75);
                this._PanelTop.Resize += new System.EventHandler(this._PanelTop_Resize);
                // 
                // splitContainer1
                // 
                this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
                this.splitContainer1.Location = new System.Drawing.Point(0, 0);
                this.splitContainer1.Name = "splitContainer1";
                this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
                // 
                // splitContainer1.Panel1
                // 
                this.splitContainer1.Panel1.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.splitContainer1.Panel1MinSize = 0;
                // 
                // splitContainer1.Panel2
                // 
                this.splitContainer1.Panel2.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.splitContainer1.Panel2.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
                this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
                this.splitContainer1.Panel2MinSize = 0;
                this.splitContainer1.Size = new System.Drawing.Size(1202, 651);
                this.splitContainer1.SplitterDistance = 506;
                this.splitContainer1.TabIndex = 1;
                this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
                this.splitContainer1.SizeChanged += new System.EventHandler(this.splitContainer1_SizeChanged);
                //// 
                //// tableLayoutPanel_ButtonTop
                //// 
                //this.tableLayoutPanel_ButtonTop.ColumnCount = 3;
                //this.tableLayoutPanel_ButtonTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
                //this.tableLayoutPanel_ButtonTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
                //this.tableLayoutPanel_ButtonTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
                //this.tableLayoutPanel_ButtonTop.Dock = System.Windows.Forms.DockStyle.Fill;
                //this.tableLayoutPanel_ButtonTop.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
                //this.tableLayoutPanel_ButtonTop.Location = new System.Drawing.Point(0, 0);
                //this.tableLayoutPanel_ButtonTop.Name = "tableLayoutPanel_ButtonTop";
                //this.tableLayoutPanel_ButtonTop.RowCount = 1;
                //this.tableLayoutPanel_ButtonTop.RowStyles.Add(new System.Windows.Forms.RowStyle());
                //this.tableLayoutPanel_ButtonTop.Size = new System.Drawing.Size(1202, 75);
                //this.tableLayoutPanel_ButtonTop.TabIndex = 0;
                // 
                // tableLayoutPanel_MainTop
                // 
                this.tableLayoutPanel_MainTop.ColumnCount = 2;
                this.tableLayoutPanel_MainTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
                this.tableLayoutPanel_MainTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
                this.tableLayoutPanel_MainTop.Dock = System.Windows.Forms.DockStyle.Fill;
                this.tableLayoutPanel_MainTop.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
                this.tableLayoutPanel_MainTop.Location = new System.Drawing.Point(0, 0);
                this.tableLayoutPanel_MainTop.Name = "tableLayoutPanel_MainTop";
                this.tableLayoutPanel_MainTop.RowCount = 1;
                this.tableLayoutPanel_MainTop.RowStyles.Add(new System.Windows.Forms.RowStyle());
                this.tableLayoutPanel_MainTop.Size = new System.Drawing.Size(1202, 75);
                this.tableLayoutPanel_MainTop.TabIndex = 0;
                // 
                // StationTiltModuleView
                // 
                this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
                this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
                this.ClientSize = new System.Drawing.Size(1212, 736);
                this.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.Name = "StationTiltModuleView";
                this.Text = "RollModule";
                this.Controls.SetChildIndex(this._PanelTop, 0);
                this.Controls.SetChildIndex(this._PanelBottom, 0);
                this._PanelBottom.ResumeLayout(false);
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
                this.splitContainer1.ResumeLayout(false);
                this.ResumeLayout(false);

            }

            #endregion
            private System.Windows.Forms.SplitContainer splitContainer1;
            //private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_ButtonTop;
            private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_MainTop;
        }
    }
}