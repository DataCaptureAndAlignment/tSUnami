﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace TSU.Tilt
{
    internal class TiltStationToGeode
    {

        private readonly string geodeFilePath;
        private readonly Station station;
        private readonly List<int> lineNumbersAlreadyRecorded = new List<int>();
        private readonly List<string> textToWriteInFile = new List<string>();
        private int lineNumber = 1;

        private TiltStationToGeode(Station actualStationTilt)
        {
            this.station = actualStationTilt;
            geodeFilePath = Tsunami2.Preferences.Values.Paths.MeasureOfTheDay
                             + IO.SUSoft.Geode.GetFileName(actualStationTilt.TiltParameters, "ROLL");
        }

        private void ReadExistingFileAndTreatLines(OnExisting onExisting)
        {
            string[] readedTextInFile = File.ReadAllLines(geodeFilePath);

            // Ajoute la ligne RE en entête si inexistante
            if (readedTextInFile.Length == 0
                || !readedTextInFile[0].Contains(";RE;"))
            {
                AddLineRE();
            }

            int linesToDrop = 0;
            int linesToMarkAsBad = 0;
            foreach (string line in readedTextInFile)
            {
                if (line != string.Empty)
                {
                    if (linesToDrop > 0)
                    {
                        linesToDrop--;
                    }
                    else if (linesToMarkAsBad > 0)
                    {
                        linesToMarkAsBad--;
                        WriteLine("%" + line.Substring(1));
                    }
                    else if (line.IndexOf(station._Name) != -1)
                    {
                        switch (onExisting)
                        {
                            case OnExisting.AppendWithComment:
                                lineNumbersAlreadyRecorded.Add(lineNumber);
                                WriteLine(line);
                                break;
                            case OnExisting.MarkExistingAsBad:
                                lineNumbersAlreadyRecorded.Add(lineNumber);
                                //Copy the current line, which is a comment
                                WriteLine(line);
                                //Mark the next 2 lines (AT & EC) as Bad
                                linesToMarkAsBad = 2;
                                break;
                            case OnExisting.Replace:
                                //Drop the current line and the next 2 lines (AT & EC)
                                linesToDrop = 2;
                                break;
                        }
                    }
                    else
                    {
                        WriteLine(line);
                    }
                }
            }
        }

        private void InitFile()
        {
            //Crèe le répertoire Mesures si nécessaire
            if (!Directory.Exists(Tsunami2.Preferences.Values.Paths.MeasureOfTheDay))
                Directory.CreateDirectory(Tsunami2.Preferences.Values.Paths.MeasureOfTheDay);
            AddLineRE();
        }

        private void AppendNewLines()
        {
            //Ecriture entête station
            AddLineRefStation();
            //Ecriture nom station
            AddLineAT();
            //Ecriture des écarts sur les éléments
            AddLineEC();
            File.WriteAllLines(geodeFilePath, textToWriteInFile);
        }

        private void AddLineAT()
        {
            string lineText = string.Format("  {0};AT;", ExportLineNumber(lineNumber));
            lineText += string.Format("{0,-9};", station.TiltParameters._Instrument._Model);
            lineText += string.Format("{0,-6};", station.TiltParameters._Instrument._SerialNumber);
            lineText += string.Format("{0,-10};", station._MeasPoint._Accelerator);
            lineText += string.Format("{0,-33};", station._MeasPoint._ClassAndNumero + "." + station._MeasPoint._Point);
            //lecture moyenne
            lineText += string.Format("{0,10};;;;;;;;;;;;;", ExportAngleInRad(station._MeasureAverageBothDirection.Value));
            //date
            lineText += string.Format("{0,-8};", station.TiltParameters._Date.ToString("T", CultureInfo.CreateSpecificCulture("fr-FR")));
            //Commentaire
            //Commentaire avec lecture individuelle
            int mesNumber = 1;
            foreach (Tilt.Measure meas in station._MeasuresTilt)
            {
                lineText += "B" + mesNumber + "=" + ExportAngleInMrads(meas._ReadingBeamDirection.Value) + " ";
                if (!meas._ReadingBeamDirection.Sigma.IsNa() )
                    lineText += "sB" + mesNumber + "=" + ExportAngleInMrads(meas._ReadingBeamDirection.Sigma) + " ";
                lineText += "I" + mesNumber + "=" + ExportAngleInMrads(meas._ReadingOppositeBeam.Value) + " ";
                if (!meas._ReadingOppositeBeam.Sigma.IsNa())
                    lineText += "sI" + mesNumber + "=" + ExportAngleInMrads(meas._ReadingOppositeBeam.Sigma) + " ";
                if (mesNumber == station._MeasuresTilt.Count)
                    lineText += "mrad ";
                else
                    lineText = lineText.Substring(0,lineText.Length-1) + ", ";
                mesNumber++;
            }
            lineText += string.Format("{0};", station.TiltParameters._Comment);
            //Commentaire avec lecture moyenne
            //lineText += String.Format("sens faisceau={0,-13}", Math.Round(actualStationTilt._MeasureAverageBeamDirection.Value * 1000, 2).ToString("F2", CultureInfo.CreateSpecificCulture("en-US")) + "mrad ");
            //lineText += String.Format("sens inverse={0,-13}", Math.Round(actualStationTilt._MeasureAverageOppositeDirection.Value * 1000, 2).ToString("F2", CultureInfo.CreateSpecificCulture("en-US")) + "mrad ");
            //lineText += String.Format("{0,-97};", actualStationTilt.TiltParameters._Comment);
            
            lineText += station._MeasuresTilt[0].Interfaces.ToString(1, ';');
            lineText += station._MeasuresTilt[0].Interfaces.ToString(2, ';');
            lineText += station._MeasuresTilt[0].Interfaces.ToString(3, ';');
            WriteLine(lineText);
        }

        private void WriteLine(string lineText)
        {
            textToWriteInFile.Add(lineText);
            lineNumber++;
        }

        private void AddLineEC()
        {
            string lineText = string.Format("  {0};EC;", ExportLineNumber(lineNumber));
            lineText += string.Format("{0,-10};", station._MeasPoint._Accelerator);
            lineText += string.Format("{0,-33};", station._MeasPoint._ClassAndNumero + "." + station._MeasPoint._Point);
            lineText += string.Format("          ;          ;          ;{0,10};dr dl dh dRoll [mm]|[mrad];", ExportAngleInMrads(station._EcartTheo.Value));
            WriteLine(lineText);
        }

        private void AddLineRE()
        {
            //Ligne entête date + team + Opération (seulement écrite si le fichier dat n'existe pas encore.
            Station.Parameters tp = station.TiltParameters;
            string lineText = $"  {ExportLineNumber(lineNumber)};RE;{ExportDate(tp._Date)};{tp._Team,-10};{tp._Operation.value,6};Tsunami {Tsunami2.Properties.Version};";
            WriteLine(lineText);
        }

        /// <summary>
        /// Ecrit la ligne d'information début de station pour vérifier si station en double
        /// </summary>
        /// <param name="lineNumber"></param>
        /// <returns></returns>
        private void AddLineRefStation()
        {
            string lineText = string.Format("! {0} ----------", ExportLineNumber(lineNumber));
            lineText += string.Format("{0} Roll recorded at ", station._Name);
            lineText += string.Format("{0,-8}", DateTime.Now.ToString("T", CultureInfo.CreateSpecificCulture("fr-FR")));
            if (lineNumbersAlreadyRecorded.Count != 0)
            {
                for (int i = 0; i < lineNumbersAlreadyRecorded.Count; i++)
                {
                    int item = lineNumbersAlreadyRecorded[i];
                    if (i == 0)
                        lineText += string.Format(" ALREADY RECORDED AT LINE {0}", item);
                    else
                        lineText += string.Format(", {0}", item);
                }
            }
            lineText += "----------";
            WriteLine(lineText);
        }

        private static string ExportAngleInMrads(double angleInRad)
        {
            double angleInMrad = angleInRad * 1000d;
            string str = angleInMrad.ToString("F3", CultureInfo.CreateSpecificCulture("en-US"));
            return str;
        }

        private static string ExportAngleInRad(double angleInRad)
        {
            string str = angleInRad.ToString("F6", CultureInfo.CreateSpecificCulture("en-US"));
            return str;
        }

        private static string ExportLineNumber(int lineNumber)
        {
            string str = lineNumber.ToString("D3", CultureInfo.CreateSpecificCulture("en-US"));
            return str;
        }

        private static string ExportDate(DateTime date)
        {
            return $"{date.Day}-{ExportMonth(date.Month)}-{date.Year}";
        }

        private static string ExportMonth(int month)
        {
            switch (month)
            {
                case 1: return "Jan";
                case 2: return "Feb";
                case 3: return "Mar";
                case 4: return "Apr";
                case 5: return "May";
                case 6: return "Jun";
                case 7: return "Jul";
                case 8: return "Aug";
                case 9: return "Sep";
                case 10: return "Oct";
                case 11: return "Nov";
                case 12: return "Dec";
                default: return "Jan";
            }

        }

        internal static bool InExistingFile(Station actualStationTilt)
        {
            TiltStationToGeode exporter = new TiltStationToGeode(actualStationTilt);

            // Read the existing file and look for the station name
            try
            {
                return File.ReadLines(exporter.geodeFilePath)
                           .Any(line => line.IndexOf(exporter.station._Name) != -1);
            }
            // FileNotFoundException is ok, we return false
            catch (FileNotFoundException)
            {
                return false;
            }
            // Other Exception is a problem
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// What to do when there is alredy a tilt measure for the station
        /// </summary>
        internal enum OnExisting
        {
            /// <summary>
            /// Keep the existing tilt measures
            /// Append the new tilt measure to the end of the file with a comment mentionning the lines of the existing tilt measures
            /// </summary>
            AppendWithComment,

            /// <summary>
            /// Mark the existing Measures as Bad
            /// Append the new tilt measure to the end of the file with a comment mentionning the lines of the existing tilt measures
            /// </summary>
            MarkExistingAsBad,

            /// <summary>
            /// Clear the exisiting Measures
            /// Append the new tilt measure to the end of the file
            /// </summary>
            Replace
        }

        internal static string ExportToGeode(Station actualStationTilt, OnExisting onExisting)
        {
            TiltStationToGeode exporter = new TiltStationToGeode(actualStationTilt);

            //lecture du fichier existant s'il existe
            try
            {
                exporter.ReadExistingFileAndTreatLines(onExisting);
            }
            //Exception générée si fichier inexistant
            catch (FileNotFoundException)
            {
                exporter.InitFile();
            }
            //Autre Exception
            catch (Exception)
            {
                throw;
            }

            exporter.AppendNewLines();

            return exporter.geodeFilePath;
        }

    }
}