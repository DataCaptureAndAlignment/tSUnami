﻿using TSU.Views;

namespace TSU.Tilt.Smart.View
{
    public partial class AdminView : TsuUserControl
    {
        public AdminView() : base() => InitializeComponent();

        public override TsuView MainView => DataContext is ViewModel vm ? vm.MainView : base.MainView;
    }
}
