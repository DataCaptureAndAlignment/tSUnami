﻿namespace TSU.Tilt.Smart.View
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_SmartModule_Name = new System.Windows.Forms.Label();
            this._PanelTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // _PanelBottom
            // 
            this._PanelBottom.Location = new System.Drawing.Point(10, 48);
            this._PanelBottom.Margin = new System.Windows.Forms.Padding(6);
            this._PanelBottom.Size = new System.Drawing.Size(1580, 807);
            // 
            // _PanelTop
            // 
            this._PanelTop.BackColor = System.Drawing.Color.Blue;
            this._PanelTop.Controls.Add(this.label_SmartModule_Name);
            this._PanelTop.ForeColor = System.Drawing.Color.White;
            this._PanelTop.Location = new System.Drawing.Point(10, 10);
            this._PanelTop.Margin = new System.Windows.Forms.Padding(6);
            this._PanelTop.MinimumSize = new System.Drawing.Size(0, 38);
            this._PanelTop.Size = new System.Drawing.Size(1580, 38);
            this._PanelTop.Click += new System.EventHandler(this.PanelTop_Click);
            // 
            // label_SmartModule_Name
            // 
            this.label_SmartModule_Name.AutoSize = true;
            this.label_SmartModule_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_SmartModule_Name.Location = new System.Drawing.Point(6, 7);
            this.label_SmartModule_Name.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label_SmartModule_Name.Name = "label_SmartModule_Name";
            this.label_SmartModule_Name.Size = new System.Drawing.Size(308, 25);
            this.label_SmartModule_Name.TabIndex = 0;
            this.label_SmartModule_Name.Text = "SMARTNAMI - ROLL MODULE";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1600, 865);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Main";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Text = "Main";
            this.Controls.SetChildIndex(this.buttonForFocusWithTabIndex0, 0);
            this.Controls.SetChildIndex(this._PanelTop, 0);
            this.Controls.SetChildIndex(this._PanelBottom, 0);
            this._PanelTop.ResumeLayout(false);
            this._PanelTop.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label_SmartModule_Name;
    }
}