﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;
using TSU.Common.Blocks;
using TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using TSU.Tools;
using TSU.Views;
using TSU.Views.Message;
using System.Windows.Input;
using R = TSU.Properties.Resources;

namespace TSU.Tilt.Smart
{
    public partial class ViewModel : ViewModelBase
    {
        private string addManuallyMachine = "LHC";

        public string AddManuallyMachine
        {
            get => addManuallyMachine;
            set => SetProperty(ref addManuallyMachine, value);
        }

        private string addManuallyClass = "MB";

        public string AddManuallyClass
        {
            get => addManuallyClass;
            set => SetProperty(ref addManuallyClass, value);
        }

        private string addManuallyNum = "A8R1";

        public string AddManuallyNum
        {
            get => addManuallyNum;
            set => SetProperty(ref addManuallyNum, value);
        }

        private string addManuallyPt = "E";

        public string AddManuallyPt
        {
            get => addManuallyPt;
            set => SetProperty(ref addManuallyPt, value);
        }

        private string addManuallyRoll = "0.000";

        public string AddManuallyRoll
        {
            get => addManuallyRoll;
            set => SetProperty(ref addManuallyRoll, value);
        }

        #region BtnAddComponentManually

        private DelegateCommand btnAddComponentManually;

        [XmlIgnore]
        public ICommand BtnAddComponentManually
        {
            get
            {
                if (btnAddComponentManually == null)
                    btnAddComponentManually =
                        new DelegateCommand(BtnAddComponentManuallyClick, BtnAddComponentManuallyCanExecute);
                return btnAddComponentManually;
            }
        }

        private bool BtnAddComponentManuallyCanExecute()
        {
            try
            {
                return !string.IsNullOrWhiteSpace(AddManuallyMachine)
                       && !string.IsNullOrWhiteSpace(AddManuallyClass)
                       && !string.IsNullOrWhiteSpace(AddManuallyNum)
                       && !string.IsNullOrWhiteSpace(AddManuallyPt)
                       && !string.IsNullOrWhiteSpace(AddManuallyRoll)
                       && double.TryParse(AddManuallyRoll.Trim(), out _);
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        private void BtnAddComponentManuallyClick()
        {
            try
            {
                string name =
                    $"{AddManuallyMachine.Trim()}.{AddManuallyClass.Trim()}.{AddManuallyNum.Trim()}.{AddManuallyPt.Trim()}";
                double tilt = double.Parse(AddManuallyRoll.Trim());
                Point p = new Point(name)
                {
                    fileElementType = ENUM.ElementType.Alesage
                };
                p._Parameters.Tilt = tilt;
                CompositeElement ce = SmartModule._ElementManager.AllElements[0] as CompositeElement;
                ce.AddInHierarchy(p, Element.HowToRename.AddParenthesis);

                List<Point> refs = GetRefPointsForPoint(p);
                if (refs.Count == 0)
                {
                    ce.Remove(p);
                    new MessageInput(MessageType.Important, R.StringSmart_AddManually_NotEligible).Show();
                }
                else
                {
                    Point refPoint = refs[0];
                    if (refPoint.Equals(p))
                    {
                        Points.Add(p);
                        OnPropertyChanged(nameof(SelectablePoints));
                    }
                    else if (Points.Contains(refPoint))
                    {
                        string titleAndMessage = string.Format(
                            R.StringSmart_AddManually_ReferenceAlreadyInList,
                            refPoint._Name
                        );
                        new MessageInput(MessageType.Important, titleAndMessage).Show();
                    }
                    else
                    {
                        ce.Remove(p);
                        string titleAndMessage = string.Format(
                            R.StringSmart_AddManually_AddReferenceInstead,
                            refPoint._Name
                        );
                        MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                        {
                            ButtonTexts = CreationHelper.GetYesNoButtons(),
                            Sender = SmartModule._Name
                        };
                        if (mi.Show().TextOfButtonClicked == R.T_YES)
                        {
                            Points.Add(refPoint);
                            OnPropertyChanged(nameof(SelectablePoints));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        private List<Point> GetRefPointsForPoint(Point p)
        {
            // Get all magnets from the element manager
            List<Magnet> allMagnets = new List<Magnet>();
            foreach (var ce in SmartModule._ElementManager.AllElements.OfType<CompositeElement>())
                allMagnets.AddRange(ce.GetMagnets());

            // Make a list with only the parent magnet for the point
            List<Magnet> parentMagnet = new List<Magnet>
            {
                FindMagnetForPoint(p, allMagnets)
            };

            // Setup the stations for this list of magnets
            return StationTiltModule.ChangeToMagnetRollRefPoint(parentMagnet);
        }

        #endregion

        /// <summary>
        /// List of all points; made public only for serialization needs
        /// </summary>
        public List<Point> Points { get; set; } = new List<Point>();

        /// <summary>
        /// List of selected points; made public only for serialization needs
        /// </summary>
        public List<Point> SelectedPoints { get; set; } = new List<Point>();

        [XmlIgnore]
        public List<Selectable<Point>> SelectablePoints
        {
            get
            {
                List<Selectable<Point>> res = new List<Selectable<Point>>();

                try
                {
                    foreach (Point p in Points)
                        res.Add(InitSelectablePoint(p));
                }
                catch (Exception ex)
                {
                    ShowMessageOfBug(ex);
                }

                return res;
            }
        }

        private Selectable<Point> InitSelectablePoint(Point p)
        {
            Selectable<Point> selectable = new Selectable<Point>(this.SelectedPoints, p);
            selectable.OnSelectedChanged += (a, b) =>
            {
                OnPropertyChanged(nameof(Selectable<Point>.Selected));
                OnPropertyChanged(nameof(SelectablePoints));
            };
            return selectable;
        }

        private Selectable<Point> currentPoint;

        [XmlIgnore]
        public Selectable<Point> CurrentPoint
        {
            get { return currentPoint; }

            set
            {
                if (value != currentPoint && ConfirmDiscard())
                    SetProperty(ref currentPoint, value);
            }
        }

        #region BtnAddComponentsFromTheoFile

        private DelegateCommand btnAddComponentsFromTheoFile;

        [XmlIgnore]
        public ICommand BtnAddComponentsFromTheoFile
        {
            get
            {
                if (btnAddComponentsFromTheoFile == null)
                    btnAddComponentsFromTheoFile = new DelegateCommand(BtnAddComponentsFromTheoFileClick);
                return btnAddComponentsFromTheoFile;
            }
        }

        private void BtnAddComponentsFromTheoFileClick()
        {
            try
            {
                // Get all magnets from the element manager
                List<Magnet> allMagnets = new List<Magnet>();
                foreach (var ce in SmartModule._ElementManager.AllElements.OfType<CompositeElement>())
                    allMagnets.AddRange(ce.GetMagnets());

                // Get the parent magnet for each selected point, without copies
                List<Magnet> parentMagnets = new List<Magnet>();
                foreach (Point point in SmartModule._ElementManager.SelectedPoints)
                {
                    var m = allMagnets.Find(magnet => magnet.GetPoints().Contains(point));
                    if (m != null && !parentMagnets.Contains(m))
                        parentMagnets.Add(m);
                }

                // Get the entry point for each magnet
                var newPoints = StationTiltModule.ChangeToMagnetRollRefPoint(parentMagnets);

                // And add them to the list, without copies
                foreach (Point point in newPoints)
                    if (!Points.Contains(point))
                        Points.Add(point);

                OnPropertyChanged(nameof(SelectablePoints));
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        #region BtnSelectAllNone

        private DelegateCommand btnSelectAllNone;

        [XmlIgnore]
        public ICommand BtnSelectAllNone
        {
            get
            {
                if (btnSelectAllNone == null)
                    btnSelectAllNone = new DelegateCommand(BtnSelectAllNoneClick);
                return btnSelectAllNone;
            }
        }

        private void BtnSelectAllNoneClick()
        {
            try
            {
                //For performance, we don't want to trigger events for every point Selected, only once in the end
                GroupSelection(() =>
                {
                    //New value is false if and only if all points are selected
                    bool newValue = !SelectablePoints.TrueForAll(Selectable<Point>.IsSelected);

                    foreach (Selectable<Point> p in SelectablePoints)
                        p.Selected = newValue;

                });
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        #region BtnDelete

        private DelegateCommand btnDeleteComponent;

        [XmlIgnore]
        public ICommand BtnDeleteComponent
        {
            get
            {
                if (btnDeleteComponent == null)
                    btnDeleteComponent = new DelegateCommand(BtnDeleteComponentClick, SomePointsAreSelected);
                return btnDeleteComponent;
            }
        }

        private void BtnDeleteComponentClick()
        {
            try
            {
                if (!SomePointsAreSelected())
                    return;

                MessageInput mi = new MessageInput(MessageType.Choice, R.StringSmart_ConfirmDeleteComponent)
                {
                    ButtonTexts = CreationHelper.GetYesNoButtons(),
                    Sender = SmartModule._Name
                };
                if (mi.Show().TextOfButtonClicked != R.T_YES)
                    return;

                foreach (var p in SelectedPoints)
                    Points.Remove(p);

                SelectedPoints.Clear();

                //Refresh the view
                OnPropertyChanged(nameof(SelectablePoints));
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        #region BtnMoveDown

        private DelegateCommand btnMoveDown;

        [XmlIgnore]
        public ICommand BtnMoveDown
        {
            get
            {
                if (btnMoveDown == null)
                    btnMoveDown = new DelegateCommand(BtnMoveDownClick, BtnMoveDownCanExecute);
                return btnMoveDown;
            }
        }

        private bool BtnMoveDownCanExecute()
        {
            try
            {
                if (!SomePointsAreSelected())
                    return false;

                return GetContiguousSelection(out _, out int lastSelected)
                       && lastSelected < SelectablePoints.Count - 1;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        private void BtnMoveDownClick()
        {
            try
            {
                GetContiguousSelection(out int firstSelected, out int lastSelected);
                // moving a contiguous selection down is equivalent to moving the point after the selection before it
                Point toMove = Points[lastSelected + 1];
                Points.RemoveAt(lastSelected + 1);
                Points.Insert(firstSelected, toMove);

                //Refresh the view
                OnPropertyChanged(nameof(SelectablePoints));
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        #region BtnMoveUp

        private DelegateCommand btnMoveUp;

        [XmlIgnore]
        public ICommand BtnMoveUp
        {
            get
            {
                if (btnMoveUp == null)
                    btnMoveUp = new DelegateCommand(BtnMoveUpClick, BtnMoveUpCanExecute);
                return btnMoveUp;
            }
        }

        private bool BtnMoveUpCanExecute()
        {
            try
            {
                if (!SomePointsAreSelected())
                    return false;

                return GetContiguousSelection(out int firstSelected, out _)
                       && firstSelected > 0;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }

        }

        private void BtnMoveUpClick()
        {
            try
            {
                GetContiguousSelection(out int firstSelected, out int lastSelected);

                // moving a contiguous selection up is equivalent to moving the point before the selection after it
                Point toMove = Points[firstSelected - 1];
                Points.RemoveAt(firstSelected - 1);
                //Notice that we removing the point shifted the indexes, so we don't add 1 to the index
                Points.Insert(lastSelected, toMove);

                //Refresh the view
                OnPropertyChanged(nameof(SelectablePoints));
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        #region BtnRevertSelection

        private DelegateCommand btnRevertSelection;

        [XmlIgnore]
        public ICommand BtnRevertSelection
        {
            get
            {
                if (btnRevertSelection == null)
                    btnRevertSelection = new DelegateCommand(BtnRevertSelectionClick);
                return btnRevertSelection;
            }
        }

        private void BtnRevertSelectionClick()
        {
            try
            {
                //For performance, we don't want to trigger OnPropertyChanged for every point Selected
                GroupSelection(() =>
                {
                    foreach (Selectable<Point> p in SelectablePoints)
                        p.Selected = !p.Selected;
                });
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        #region BtnSortAlpha

        private DelegateCommand btnSortAlpha;

        [XmlIgnore]
        public ICommand BtnSortAlpha
        {
            get
            {
                if (btnSortAlpha == null)
                    btnSortAlpha = new DelegateCommand(BtnSortAlphaClick);
                return btnSortAlpha;
            }
        }

        private bool reverseAplhaSort = false;

        private void BtnSortAlphaClick()
        {
            try
            {
                GroupSelection(() =>
                {
                    if (reverseAplhaSort)
                        Points.Sort((a, b) => -Sorting.CompareNatural(a._Name, b._Name));
                    else
                        Points.Sort((a, b) => Sorting.CompareNatural(a._Name, b._Name));

                    reverseAplhaSort = !reverseAplhaSort;
                });
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        #region BtnSortDcum

        private DelegateCommand btnSortDcum;

        [XmlIgnore]
        public ICommand BtnSortDcum
        {
            get
            {
                if (btnSortDcum == null)
                    btnSortDcum = new DelegateCommand(BtnSortDcumClick);
                return btnSortDcum;
            }
        }

        private bool reverseDcumSort = false;

        private void BtnSortDcumClick()
        {
            try
            {
                GroupSelection(() =>
                {
                    if (reverseDcumSort)
                        Points.Sort((a, b) => -a._Parameters.Cumul.CompareTo(b._Parameters.Cumul));
                    else
                        Points.Sort((a, b) => a._Parameters.Cumul.CompareTo(b._Parameters.Cumul));
                    reverseDcumSort = !reverseDcumSort;
                });
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        #region BtnSortSel

        private DelegateCommand btnSortSel;

        [XmlIgnore]
        public ICommand BtnSortSel
        {
            get
            {
                if (btnSortSel == null)
                    btnSortSel = new DelegateCommand(BtnSortSelClick, BtnSortSelCanExecute);
                return btnSortSel;
            }
        }

        private bool BtnSortSelCanExecute()
        {
            try
            {
                return SomePointsAreSelected();
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        private bool reverseSelSort = false;

        private void BtnSortSelClick()
        {
            try
            {
                GroupSelection(() =>
                {
                    if (!SomePointsAreSelected())
                        return;

                    var newComponents = new List<Point>();
                    foreach (Point p in SelectedPoints)
                        newComponents.Add(p);
                    foreach (Selectable<Point> p in SelectablePoints.Where(Selectable<Point>.IsNotSelected))
                        newComponents.Add(p.Item);

                    // handle the reverse flag
                    if (reverseSelSort)
                        newComponents.Reverse();
                    reverseSelSort = !reverseSelSort;

                    Points = newComponents;
                });
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        #region BtnSortTheo

        private DelegateCommand btnSortTheo;

        [XmlIgnore]
        public ICommand BtnSortTheo
        {
            get
            {
                if (btnSortTheo == null)
                    btnSortTheo = new DelegateCommand(BtnSortTheoClick);
                return btnSortTheo;
            }
        }

        private bool reverseTheoSort = false;

        private void BtnSortTheoClick()
        {
            try
            {
                GroupSelection(() =>
                {
                    var newComponents = new List<Point>();

                    // first add the points from the file, in the file order
                    foreach (Point p in SmartModule._ElementManager.GetAllTheoreticalPoints())
                        if (Points.Contains(p))
                        {
                            newComponents.Add(p);
                            Points.Remove(p);
                        }

                    // then add the other points
                    newComponents.AddRange(Points);

                    // handle the reverse flag
                    if (reverseTheoSort)
                        newComponents.Reverse();
                    reverseTheoSort = !reverseTheoSort;

                    Points = newComponents;
                });
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        private bool GetContiguousSelection(out int firstSelected, out int lastSelected)
        {
            List<Selectable<Point>> allPoints = SelectablePoints;
            firstSelected = allPoints.FindIndex(Selectable<Point>.IsSelected);
            lastSelected = allPoints.FindLastIndex(Selectable<Point>.IsSelected);
            int count = lastSelected - firstSelected + 1;
            return count == SelectedPoints.Count;
        }

        private bool SomePointsAreSelected()
        {
            return SelectedPoints.Any();
        }

        private static readonly string[] componentPropertiesToCheck =
        {
            nameof(Selectable<Point>.Selected),
            nameof(SelectablePoints)
        };

        private void GroupSelection(Action a)
        {
            MainView.WaitingForm = new Views.Message.ProgressMessage(MainView, R.T_TSUNAMI_IS_WAITING_FOR,
                $"{R.T_STARTING}...", 3, 2000, false);
            MainView.WaitingForm.Show();
            MainView.WaitingForm.BeginAstep("Updating Selection");

            //Stop treating changes
            groupSelectionPending = true;

            //Do the action
            a();

            MainView.WaitingForm.EndCurrentStep();
            MainView.WaitingForm.BeginAstep("Updating view");

            //Trigger the events for the view, once each
            OnPropertyChanged(nameof(SelectablePoints));
            OnPropertyChanged(nameof(Selectable<Point>.Selected));

            MainView.WaitingForm.EndCurrentStep();
            MainView.WaitingForm.BeginAstep("Updating stations");

            //Do the module update only once
            SetPointsToMeasure();

            MainView.WaitingForm.EndCurrentStep();
            MainView.WaitingForm.Stop();

            //Treat next changes normally
            groupSelectionPending = false;
        }

        private bool groupSelectionPending = false;

        private void OnChangeComponentsPage(object sender, PropertyChangedEventArgs e)
        {
            if (!groupSelectionPending && componentPropertiesToCheck.Contains(e.PropertyName))
                SetPointsToMeasure();
        }

        private void SetPointsToMeasure()
        {
            // Get all magnets from the element manager
            List<Magnet> allMagnets = new List<Magnet>();
            foreach (var ce in SmartModule._ElementManager.AllElements.OfType<CompositeElement>())
                allMagnets.AddRange(ce.GetMagnets());

            // Get the parent magnet for each selected point
            var elemsLst = SelectedPoints.Select(point => FindMagnetForPoint(point, allMagnets)).ToList();

            // Setup the stations for this list of magnets
            StationTiltModule.SetPointsToMeasure(elemsLst);
        }

        private static Magnet FindMagnetForPoint(Point point, List<Magnet> allMagnets)
        {
            return allMagnets.Find(magnet => magnet.GetPoints().Contains(point));
        }

        #region BtnAddComponentManually

        private DelegateCommand btnShowTooltips;

        [XmlIgnore]
        public ICommand BtnShowTooltips
        {
            get
            {
                if (btnShowTooltips == null)
                    btnShowTooltips = new DelegateCommand(PerformBtnShowTooltips);
                return btnShowTooltips;
            }
        }

        private void PerformBtnShowTooltips()
        {
            try
            {
                ShowTooltips = !ShowTooltips;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        private bool showTooltips = false;
        public bool ShowTooltips
        {
            get => showTooltips;
            set => SetProperty(ref showTooltips, value);
        }

    }
}