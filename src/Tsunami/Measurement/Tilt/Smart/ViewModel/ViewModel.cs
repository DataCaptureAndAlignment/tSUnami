﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using TSU.Common.Blocks;
using TSU.Common.Elements;
using TSU.Tilt.Smart.View;
using System.Windows.Input;
using T = TSU.Tilt;

namespace TSU.Tilt.Smart
{
    [XmlType("Tilt.Smart.ViewModel")]
    public partial class ViewModel : ViewModelBase
    {
        [XmlIgnore]
        public Smart.Module SmartModule { get; private set; }

        [XmlIgnore]
        public Main MainView => SmartModule?.View as Main;

        [XmlIgnore]
        public T.Station StationTilt => SmartModule?._ActiveStationModule?._Station as T.Station;

        [XmlIgnore]
        public T.Station.Module StationTiltModule
        {
            get
            {
                return SmartModule?._ActiveStationModule as T.Station.Module;
            }
        }

        /// <summary>
        /// Parameterless constructor for serialization
        /// </summary>
        public ViewModel()
        {
        }

        public ViewModel(Smart.Module smartModule) : this()
        {
            SmartModule = smartModule;
            InitializeAdminPage();
            InitializeMeasuresPages();
        }

        public void LoadModule(Smart.Module smartModuleToLoad)
        {
            Debug.WriteInConsole("LoadModule called");
            SmartModule = smartModuleToLoad;
            //We add the listener here, so that the OnChange/CanExecute aren't called before we have a link to the module
            PropertyChanged += OnChange;
            //Restore the current page
            UpdateCurrentPage();
            //Restore the current point
            Point theoPoint = StationTiltModule?._ActualStationTilt?.TheoPoint;
            if (theoPoint == null)
            {
                CurrentPoint = null;
            }
            else
            {
                CurrentPoint = SelectablePoints.Find(sp => sp.Item.Equals(theoPoint));
            }
            //Recompute
            ComputeMeasuresPages();
            if (currentPage == Page.LogPage)
                UpdateLogPage();
        }

        [XmlType("Tilt.Smart.ViewModel.Page")]
        public enum Page
        {
            Admin,
            Components,
            InitialMeasures,
            FinalMeasures,
            LogPage
        }

        private Page currentPage;

        public Page CurrentPage
        {
            get => currentPage;
            set
            {
                // When leaving a measures page, ask for confirmation
                if ((currentPage == Page.InitialMeasures || currentPage == Page.FinalMeasures)
                    && !ConfirmDiscard())
                    return;
                currentPage = value;
                UpdateCurrentPage();
            }
        }

        private void UpdateCurrentPage()
        {
            switch (currentPage)
            {
                case Page.Admin:
                    MainView?.SwitchToAdmin();
                    break;
                case Page.Components:
                    MainView?.SwitchToComponents();
                    break;
                case Page.InitialMeasures:
                    MainView?.SwitchToInitialMeasures();
                    break;
                case Page.FinalMeasures:
                    MainView?.SwitchToFinalMeasures();
                    break;
                case Page.LogPage:
                    MainView?.SwitchToLog();
                    break;
            }
        }

        #region BtnReturnToAdministrativePage
        private DelegateCommand btnReturnToAdministrativePage;
        [XmlIgnore]
        public ICommand BtnReturnToAdministrativePage
        {
            get
            {
                if (btnReturnToAdministrativePage == null)
                    btnReturnToAdministrativePage = new DelegateCommand(BtnReturnToAdministrativePageClick);
                return btnReturnToAdministrativePage;
            }
        }
        private void BtnReturnToAdministrativePageClick()
        {
            try
            {
                CurrentPage = Page.Admin;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }
        #endregion

        #region BtnComponents
        private DelegateCommand btnComponents;
        [XmlIgnore]
        public ICommand BtnComponents
        {
            get
            {
                if (btnComponents == null)
                    btnComponents = new DelegateCommand(BtnComponentsClick);
                return btnComponents;
            }
        }
        private void BtnComponentsClick()
        {
            try
            {
                CurrentPage = Page.Components;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }
        #endregion

        #region BtnInitialMeasure
        private DelegateCommand btnInitialMeasure;
        [XmlIgnore]
        public ICommand BtnInitialMeasure
        {
            get
            {
                if (btnInitialMeasure == null)
                    btnInitialMeasure = new DelegateCommand(BtnInitialMeasureClick, BtnInitialMeasureCanExecute);
                return btnInitialMeasure;
            }
        }
        private bool BtnInitialMeasureCanExecute()
        {
            try
            {
                return (currentPoint != null) && CheckOperation(InitialOperationNumber, out _, out _);
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }
        private void BtnInitialMeasureClick()
        {
            try
            {
                CurrentPage = Page.InitialMeasures;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }
        #endregion

        #region BtnFinalMeasure
        private DelegateCommand btnFinalMeasure;
        [XmlIgnore]
        public ICommand BtnFinalMeasure
        {
            get
            {
                if (btnFinalMeasure == null)
                    btnFinalMeasure = new DelegateCommand(BtnFinalMeasureClick, BtnFinalMeasureCanExecute);
                return btnFinalMeasure;
            }
        }
        private bool BtnFinalMeasureCanExecute()
        {
            try
            {
                return (currentPoint != null) && CheckOperation(FinalOperationNumber, out _, out _);
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }
        private void BtnFinalMeasureClick()
        {
            try
            {
                CurrentPage = Page.FinalMeasures;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }
        #endregion

        #region BtnLog
        private DelegateCommand btnLog;

        [XmlIgnore]
        public ICommand BtnLog
        {
            get
            {
                if (btnLog == null)
                    btnLog = new DelegateCommand(BtnLogClick);
                return btnLog;
            }
        }
        private void BtnLogClick()
        {
            try
            {
                CurrentPage = Page.LogPage;
                UpdateLogPage();
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }
        #endregion


        private void OnChange(object sender, PropertyChangedEventArgs e)
        {
            OnChangeAdminPage(sender, e);
            OnChangeComponentsPage(sender, e);
            OnChangeMeasuresPage(sender, e);
        }

        private void ShowMessageOfBug(Exception ex, [CallerMemberName] string callerMemberName = "?")
        {
            string title = $"Exception in {callerMemberName}";
            string message = ex.Message;

            if (MainView != null)
            {
                MainView.ShowMessageOfBug($"{title};{message}", ex);
            }
            else
            {
                System.Windows.MessageBox.Show(message, title);
            }
        }
    }
}
