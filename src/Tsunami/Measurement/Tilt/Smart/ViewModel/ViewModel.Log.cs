﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using TSU.Common.Blocks;
using TSU.ENUM;
using CS = TSU.Common.Smart;
using T = TSU.Tilt;

namespace TSU.Tilt.Smart
{
    public partial class ViewModel : ViewModelBase
    {
        private List<PointWithStationsModel> pointsWithStations;

        [XmlIgnore]
        public List<PointWithStationsModel> PointsWithStations
        {
            get => pointsWithStations;
            set => SetProperty(ref pointsWithStations, value);
        }

        public class PointWithStationsModel
        {
            public PointWithStationsModel(string name)
            {
                Name = name;
                Stations = new List<StationModel>();
            }

            public string Name { get; private set; }

            public List<StationModel> Stations { get; private set; }

            public bool? Status
            {
                get
                {
                    // Check if there is an initial station with a reading
                    if (!Stations.Exists(
                        s => s.TiltInitOrAdjusting == TiltInitOrAdjusting.Initial
                          && s.Reading != string.Empty
                    ))
                        return null;

                    // Check if there is an initial station outside tolerance
                    if (!Stations.Exists(
                        s => s.TiltInitOrAdjusting == TiltInitOrAdjusting.Initial
                          && !s.DeviationWithinTolerance
                    ))
                        return true;

                    // Check if it has been adjusted successfully
                    if (Stations.Exists(
                        s => s.TiltInitOrAdjusting == TiltInitOrAdjusting.Adjusting
                          && s.DeviationWithinTolerance
                    ))
                        return true;
                    else
                        return false;
                }
            }

        }

        public class StationModel
        {
            public StationModel(T.Station station, PointWithStationsModel parentPoint)
            {
                Station = station;
                ParentPoint = parentPoint;
            }

            private readonly T.Station Station;

            private readonly PointWithStationsModel ParentPoint;

            internal TiltInitOrAdjusting TiltInitOrAdjusting => Station.TiltParameters._TiltInitOrAdjusting;

            public string OperationType => TiltInitOrAdjusting == TiltInitOrAdjusting.Initial ? "Ini" : "Fin";

            public string Reading => CS.MeasureFormatting.FormatAngle(Station._MeasureAverageBothDirection);

            public string Deviation => CS.MeasureFormatting.FormatAngle(Station._EcartTheo);

            public bool DeviationWithinTolerance => CS.MeasureFormatting.CheckToleranceForAngle(Station._EcartTheo);

            public string Instrument => Station.TiltParameters._Instrument._Name;

            public string DateAndTime => Station.TiltParameters._Date.ToString();

            public bool? Status => ParentPoint.Status;
        }

        private void UpdateLogPage()
        {
            //Build a Dictionnary in order to regroup the stations for the same point
            Dictionary<string, PointWithStationsModel> stationsSaved = new Dictionary<string, PointWithStationsModel>();
            List<T.Station> allStations = StationTiltModule._StationTiltSequences;
            UpdateLogPage_TreatSubList(stationsSaved, allStations.Where(s => s.TiltParameters._TiltInitOrAdjusting == TiltInitOrAdjusting.Initial));
            UpdateLogPage_TreatSubList(stationsSaved, allStations.Where(s => s.TiltParameters._TiltInitOrAdjusting == TiltInitOrAdjusting.Adjusting));

            // Set the list using the property (to cause a refresh)
            PointsWithStations = stationsSaved.Values.ToList();
        }

        private static void UpdateLogPage_TreatSubList(Dictionary<string, PointWithStationsModel> stationsSaved, IEnumerable<Station> stationsSubList)
        {
            foreach (T.Station st in stationsSubList)
            {
                string name = st._MeasPoint._Name;
                if (stationsSaved.TryGetValue(name, out PointWithStationsModel lst))
                {
                    lst.Stations.Add(new StationModel(st, lst));
                }
                else
                {
                    lst = new PointWithStationsModel(name);
                    lst.Stations.Add(new StationModel(st, lst));
                    stationsSaved.Add(name, lst);
                }
            }
        }
    }
}
