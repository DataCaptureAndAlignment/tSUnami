﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using System.Xml.Serialization;
using TSU.Common.Blocks;
using TSU.Common.Elements;
using TSU.Views.Message;
using CS = TSU.Common.Smart;
using E = TSU.ENUM;
using I = TSU.Common.Instruments;
using R = TSU.Properties.Resources;

namespace TSU.Tilt.Smart
{
    public partial class ViewModel : ViewModelBase
    {
        private void InitializeMeasuresPages()
        {
            currentInstrumentName = R.StringSmart_InstrumentSelection;
        }

        private string currentInstrumentName;
        public string CurrentInstrumentName
        {
            get => currentInstrumentName;
            set => SetProperty(ref currentInstrumentName, value);
        }

        private string instrumentError;
        public string InstrumentError
        {
            get => instrumentError;
            set => SetProperty(ref instrumentError, value);
        }

        private bool connectableInstrument;
        public bool ConnectableInstrument
        {
            get => connectableInstrument;
            set => SetProperty(ref connectableInstrument, value);
        }

        private string btnConnectInstrumentName = R.StringSmart_Connect;
        public string BtnConnectInstrumentName
        {
            get => btnConnectInstrumentName;
            set => SetProperty(ref btnConnectInstrumentName, value);
        }

        #region BtnSelectInstrument

        private DelegateCommand btnSelectInstrument;

        [XmlIgnore]
        public ICommand BtnSelectInstrument
        {
            get
            {
                if (btnSelectInstrument == null)
                    btnSelectInstrument = new DelegateCommand(BtnSelectInstrumentClick);
                return btnSelectInstrument;
            }
        }

        private void BtnSelectInstrumentClick()
        {
            try
            {
                I.Manager.Module im = SmartModule._ActiveStationModule._InstrumentManager;
                if (im._TsuView == null)
                {
                    im.CreateViewAutmatically = true;
                    im.CreateView();
                }
                var instruments = im.GetByClass(I.InstrumentClasses.TILT);
                I.Instrument i = im.SelectInstrument(R.StringSmart_InstrumentSelection, selectables: instruments, multiSelection: false, preselected: null);

                if (i == null) return;

                ConnectableInstrument = i is I.Device.WYLER.Instrument;
                CurrentInstrumentName = i._Name;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        #region BtnConnectInstrument

        private DelegateCommand btnConnectInstrument;

        [XmlIgnore]
        public ICommand BtnConnectInstrument
        {
            get
            {
                if (btnConnectInstrument == null)
                    btnConnectInstrument = new DelegateCommand(BtnConnectInstrumentClick, BtnConnectInstrumentCanExecute);
                return btnConnectInstrument;
            }
        }

        private void BtnConnectInstrumentClick()
        {
            try
            {
                if (SmartModule._ActiveStationModule._InstrumentManager?.SelectedInstrumentModule is I.Device.WYLER.Module module)
                {
                    if (!module.wyBusConnected)
                    {
                        Result connectionResult = module.Connect();
                        Logs.Log.AddEntryAsResult(module, connectionResult);
                        if (connectionResult.Success)
                        {
                            new MessageInput(MessageType.GoodNews, connectionResult.AccessToken).Show();
                            BtnConnectInstrumentName = R.StringSmart_Disconnect;
                        }
                        else
                            new MessageInput(MessageType.Critical, connectionResult.ErrorMessage).Show();
                    }
                    else
                    {
                        Result disconnectionResult = module.Disconnect();
                        Logs.Log.AddEntryAsResult(module, disconnectionResult);
                        if (disconnectionResult.Success)
                        {
                            new MessageInput(MessageType.GoodNews, disconnectionResult.AccessToken).Show();
                            BtnConnectInstrumentName = R.StringSmart_Connect;
                        }
                        else
                            new MessageInput(MessageType.Critical, disconnectionResult.ErrorMessage).Show();
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        private bool BtnConnectInstrumentCanExecute()
        {
            try
            {
                return ConnectableInstrument;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        #endregion

        #region BtnMeasureBeam

        private DelegateCommand btnMeasureBeam;

        [XmlIgnore]
        public ICommand BtnMeasureBeam
        {
            get
            {
                if (btnMeasureBeam == null)
                    btnMeasureBeam = new DelegateCommand(BtnMeasureBeamClick, BtnMeasureBeamCanExecute);
                return btnMeasureBeam;
            }
        }

        private void BtnMeasureBeamClick()
        {
            try
            {
                if (SmartModule._ActiveStationModule._InstrumentManager?.SelectedInstrumentModule is I.Device.WYLER.Module module)
                {
                    if (!module.wyBusConnected)
                    {
                        Result connectionResult = module.Connect();
                        Logs.Log.AddEntryAsResult(module, connectionResult);
                        if (!connectionResult.Success)
                            new MessageInput(MessageType.Critical, connectionResult.ErrorMessage).Show();
                    }

                    if (module.ReadingAngle(out DoubleValue angle))
                    {
                        // Using FormatAngle(double), because ReadingAngle returns the value in mrads
                        BeamReading = CS.MeasureFormatting.FormatAngle(angle.Value);
                        beamReadingSigma = angle.Sigma;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        private bool BtnMeasureBeamCanExecute()
        {
            try
            {
                return ConnectableInstrument;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        #endregion

        #region BtnMeasureReverse

        private DelegateCommand btnMeasureReverse;

        [XmlIgnore]
        public ICommand BtnMeasureReverse
        {
            get
            {
                if (btnMeasureReverse == null)
                    btnMeasureReverse = new DelegateCommand(BtnMeasureReverseClick, BtnMeasureReverseCanExecute);
                return btnMeasureReverse;
            }
        }

        private void BtnMeasureReverseClick()
        {
            try
            {
                if (SmartModule._ActiveStationModule._InstrumentManager?.SelectedInstrumentModule is I.Device.WYLER.Module module)
                {
                    if (!module.wyBusConnected)
                    {
                        Result connectionResult = module.Connect();
                        Logs.Log.AddEntryAsResult(module, connectionResult);
                        if (!connectionResult.Success)
                            new MessageInput(MessageType.Critical, connectionResult.ErrorMessage).Show();
                    }

                    if (module.ReadingAngle(out DoubleValue angle))
                    {
                        // Using FormatAngle(double), because ReadingAngle returns the value in mrads
                        ReverseReading = CS.MeasureFormatting.FormatAngle(angle.Value);
                        reverseReadingSigma = angle.Sigma;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        private bool BtnMeasureReverseCanExecute()
        {
            try
            {
                return ConnectableInstrument;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        #endregion

        public string CurrentComponentNameWithLabel
        {
            get
            {
                string name = CurrentPoint?.Item?._Name;
                if (string.IsNullOrEmpty(name))
                {
                    return string.Empty;
                }
                else
                {
                    return string.Format(R.StringSmart_CurrentComponent, name);
                }
            }
        }

        private string currentPointError;
        public string CurrentPointError
        {
            get => currentPointError;
            set => SetProperty(ref currentPointError, value);
        }

        public string CurrentProgressInitial => CurrentProgress(E.TiltInitOrAdjusting.Initial);

        public string CurrentProgressFinal => CurrentProgress(E.TiltInitOrAdjusting.Adjusting);

        private string CurrentProgress(E.TiltInitOrAdjusting tiltInitOrAdjusting)
        {
            int total = 0;
            int saved = 0;

            foreach (Station s in StationTiltModule._StationTiltSequences)
                if (s.TiltParameters._TiltInitOrAdjusting == tiltInitOrAdjusting)
                {
                    total++;
                    if (s.TiltParameters._State is Common.Station.State.StationTiltSaved)
                        saved++;
                }

            return string.Format(R.StringSmart_CurrentProgress, saved, total);
        }

        #region BtnPreviousComponent

        private DelegateCommand btnPreviousComponent;

        [XmlIgnore]
        public ICommand BtnPreviousComponent
        {
            get
            {
                if (btnPreviousComponent == null)
                    btnPreviousComponent = new DelegateCommand(BtnPreviousComponentClick, BtnPreviousComponentCanExecute);
                return btnPreviousComponent;
            }
        }

        private bool BtnPreviousComponentCanExecute()
        {
            try
            {
                return PreviousSelectedPoint() != null;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        private void BtnPreviousComponentClick()
        {
            try
            {
                Selectable<Point> selectable = PreviousSelectedPoint();
                if (selectable == null)
                {
                    new MessageInput(MessageType.Important, R.StringSmart_NoOtherPointSelected).Show();
                }
                else
                {
                    CurrentPoint = selectable;
                }
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        private Selectable<Point> PreviousSelectedPoint()
        {
            if (currentPoint == null) return null;
            return PreviousSelectedPointInList(SelectablePoints, currentPoint.Item);
        }

        public static Selectable<Point> PreviousSelectedPointInList(List<Selectable<Point>> lst, Point item)
        {
            int cur = lst.FindIndex(sp => sp.Item.Equals(item));

            //cur == -1 shouldn't happen, but we test just in case
            if (cur == -1)
                return null;

            //We want to loop at the end, but not infinitely
            //So we make new list (note: it may be faster to iterate twice)
            List<Selectable<Point>> lst2;
            if (cur == 0)
            {
                lst2 = lst;
            }
            else
            {
                int nbPointsAfter = cur;
                int nbPointsBefore = lst.Count - nbPointsAfter;
                lst2 = lst.GetRange(nbPointsAfter, nbPointsBefore);
                lst2.AddRange(lst.GetRange(0, nbPointsAfter));
            }

            int previous = lst2.FindLastIndex(Selectable<Point>.IsSelected);

            //previous == -1 will happen if there is no more selected component
            if (previous == -1)
                return null;

            return lst2[previous];
        }

        #endregion

        #region BtnNextComponent

        private DelegateCommand btnNextComponent;

        [XmlIgnore]
        public ICommand BtnNextComponent
        {
            get
            {
                if (btnNextComponent == null)
                    btnNextComponent = new DelegateCommand(BtnNextComponentClick, BtnNextComponentCanExecute);
                return btnNextComponent;
            }
        }

        private bool BtnNextComponentCanExecute()
        {
            try
            {
                return NextSelectedPoint() != null;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        private void BtnNextComponentClick()
        {
            try
            {
                Selectable<Point> selectable = NextSelectedPoint();
                if (selectable == null)
                {
                    new MessageInput(MessageType.Important, R.StringSmart_NoOtherPointSelected).Show();
                }
                else
                {
                    CurrentPoint = selectable;
                }
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        private bool ConfirmDiscard()
        {
            //If there's no input, continue as if discarded
            if (string.IsNullOrWhiteSpace(BeamReading) && string.IsNullOrWhiteSpace(ReverseReading))
                return true;

            //If there's valid input, but it's the same as saved, continue as if discarded
            if (beamReading.HasValue && reverseReading.HasValue)
            {
                LoadMeasuresFromStation(out string beamReadingFromStation, out string reverseReadingFromStation, out _, out _);
                if (beamReadingFromStation == CS.MeasureFormatting.FormatAngle(beamReading) && reverseReadingFromStation == CS.MeasureFormatting.FormatAngle(reverseReading))
                    return true;
            }

            //Check is saving is possible
            bool canSave = CheckMeasuresPages();

            //Select question and possible answers
            string question;
            List<string> choices = new List<string>();
            if (canSave)
            {
                question = R.StringSmart_ConfirmDiscard_WithSave;
                choices.Add(R.StringSmart_ConfirmDiscard_Save);
            }
            else
            {
                question = R.StringSmart_ConfirmDiscard_NoSave;
            }
            choices.Add(R.StringSmart_ConfirmDiscard_Discard);
            choices.Add(R.T_CANCEL);

            //Ask
            MessageInput mi = new MessageInput(MessageType.Choice, question)
            {
                ButtonTexts = choices,
                Sender = SmartModule._Name
            };
            string answer = mi.Show().TextOfButtonClicked;

            if (answer == R.StringSmart_ConfirmDiscard_Discard)
            {
                BeamReading = string.Empty;
                beamReadingSigma = null;
                ReverseReading = string.Empty;
                reverseReadingSigma = null;
                return true;
            }
            else if (answer == R.StringSmart_ConfirmDiscard_Save)
            {
                E.TiltInitOrAdjusting type;
                if (currentPage == Page.InitialMeasures)
                    type = E.TiltInitOrAdjusting.Initial;
                else
                    type = E.TiltInitOrAdjusting.Adjusting;
                SaveMeasure(type);
                return true;
            }
            else
                return false;
        }

        private Selectable<Point> NextSelectedPoint(Predicate<Selectable<Point>> match = null)
        {
            if (currentPoint == null) return null;
            return NextSelectedPointInList(SelectablePoints, currentPoint.Item, match);
        }

        public static Selectable<Point> NextSelectedPointInList(List<Selectable<Point>> lst, Point item, Predicate<Selectable<Point>> match = null)
        {
            int cur = lst.FindIndex(sp => sp.Item.Equals(item));

            //Shouldn't happen, but we test just in case
            if (cur == -1)
                return null;

            //We want to loop at the end, but not infinitely
            //So we make new list (note: it may be faster to iterate twice)
            List<Selectable<Point>> lst2;
            if (cur == lst.Count)
            {
                lst2 = lst;
            }
            else
            {
                int nbPointsAfter = cur + 1;
                int nbPointsBefore = lst.Count - nbPointsAfter;
                lst2 = lst.GetRange(nbPointsAfter, nbPointsBefore);
                lst2.AddRange(lst.GetRange(0, nbPointsAfter));
            }

            if (match == null)
                match = Selectable<Point>.IsSelected;
            int next = lst2.FindIndex(match);

            //next == -1 will happen if there is no more selected component
            if (next == -1)
                return null;

            return lst2[next];
        }

        #endregion

        private double? beamReadingSigma;
        private double? beamReading;
        private string beamReadingString;
        public string BeamReading
        {
            get => beamReadingString;
            set
            {
                beamReading = ParseToNullableDouble(value);
                SetProperty(ref beamReadingString, value);
            }
        }

        private string beamReadingError;
        public string BeamReadingError
        {
            get => beamReadingError;
            set => SetProperty(ref beamReadingError, value);
        }

        private double? reverseReadingSigma;
        private double? reverseReading;
        private string reverseReadingString;
        public string ReverseReading
        {
            get => reverseReadingString;
            set
            {
                reverseReading = ParseToNullableDouble(value);
                SetProperty(ref reverseReadingString, value);
            }
        }

        private string reverseReadingError;
        public string ReverseReadingError
        {
            get => reverseReadingError;
            set => SetProperty(ref reverseReadingError, value);
        }

        private double? ParseToNullableDouble(string value)
        {
            if (double.TryParse(value, out double val))
                return val;
            else
                return null;
        }

        private double? beamDeviation;
        public string BeamDeviation => CS.MeasureFormatting.FormatAngle(beamDeviation);
        public bool BeamDeviationWithinTolerance => CS.MeasureFormatting.CheckToleranceForAngle(beamDeviation);

        private double? beamObjective;
        public string BeamObjective => CS.MeasureFormatting.FormatAngle(beamObjective);

        private double? reverseDeviation;
        public string ReverseDeviation => CS.MeasureFormatting.FormatAngle(reverseDeviation);
        public bool ReverseDeviationWithinTolerance => CS.MeasureFormatting.CheckToleranceForAngle(reverseDeviation);

        private double? reverseObjective;
        public string ReverseObjective => CS.MeasureFormatting.FormatAngle(reverseObjective);

        private double? averageReading;
        public string AverageReading => CS.MeasureFormatting.FormatAngle(averageReading);

        private double? averageDeviation;
        public string AverageDeviation => CS.MeasureFormatting.FormatAngle(averageDeviation);
        public bool AverageDeviationWithinTolerance => CS.MeasureFormatting.CheckToleranceForAngle(averageDeviation);

        private double? averageObjective;
        public string AverageObjective => CS.MeasureFormatting.FormatAngle(averageObjective);

        private static readonly string[] measuresPropertiesToCheckCompute = {
            nameof(BeamReading),
            nameof(ReverseReading),
            nameof(CurrentPoint),
        };

        private static readonly string[] measuresPropertiesToCheckLoad = {
            nameof(CurrentPoint),
            nameof(CurrentPage)
        };

        private void OnChangeMeasuresPage(object sender, PropertyChangedEventArgs e)
        {
            if (measuresPropertiesToCheckLoad.Contains(e.PropertyName))
            {
                LoadMeasuresFromStation(out string beamReadingFromStation, out string reverseReadingFromStation,
                                        out double? beamReadingSigmaFromStation, out double? reverseReadingSigmaFromStation);
                BeamReading = beamReadingFromStation;
                beamReadingSigma = beamReadingSigmaFromStation;
                ReverseReading = reverseReadingFromStation;
                reverseReadingSigma = reverseReadingSigmaFromStation;
            }
            if (measuresPropertiesToCheckCompute.Contains(e.PropertyName))
                ComputeMeasuresPages();
        }

        private void LoadMeasuresFromStation(out string beamReadingFromStation, out string reverseReadingFromStation,
                                             out double? sigmaBeamReadingFromStation, out double? sigmaReverseReadingFromStation)
        {
            beamReadingFromStation = string.Empty;
            reverseReadingFromStation = string.Empty;
            sigmaBeamReadingFromStation = null;
            sigmaReverseReadingFromStation = null;

            if ((CurrentPage == Page.InitialMeasures || CurrentPage == Page.FinalMeasures)
                && currentPoint != null && currentPoint.Item != null && currentPoint.Selected)
            {
                Point p = currentPoint.Item;

                //Select the right station pair
                StationTiltModule.ChangePointTheo(p._Name);

                //Select the right station in the pair
                if (CurrentPage == Page.InitialMeasures)
                    StationTiltModule._ActualStationTilt = StationTiltModule._InitStationTilt;
                else
                    StationTiltModule._ActualStationTilt = StationTiltModule._AdjustingStationTilt;

                Station st = StationTiltModule._ActualStationTilt;

                if (st != null && st._MeasuresTilt != null && st._MeasuresTilt.Count > 0)
                {
                    Measure measure = st._MeasuresTilt[0];
                    beamReadingFromStation = CS.MeasureFormatting.FormatAngle(measure._ReadingBeamDirection);
                    sigmaBeamReadingFromStation = CS.MeasureFormatting.SigmaOrNull(measure._ReadingBeamDirection);
                    reverseReadingFromStation = CS.MeasureFormatting.FormatAngle(measure._ReadingOppositeBeam);
                    sigmaReverseReadingFromStation = CS.MeasureFormatting.SigmaOrNull(measure._ReadingOppositeBeam);
                }
            }
        }

        private void ComputeMeasuresPages()
        {
            // Try to get the theoretical tilt
            double? tmpTilt = CurrentPoint?.Item?._Parameters?.Tilt;
            averageObjective = tmpTilt != null ? tmpTilt * 1000d : null;

            // Compute the values depending on what we have
            // Note : calculations with nullable types return null if one of the operands is null
            averageReading = (beamReading - reverseReading) / 2d;
            averageDeviation = averageReading - averageObjective;
            beamDeviation = beamReading - averageObjective;
            beamObjective = beamReading - averageDeviation;
            reverseDeviation = reverseReading + averageObjective;
            reverseObjective = reverseReading + averageDeviation;

            // Mark all fields for update
            OnPropertyChanged(nameof(CurrentComponentNameWithLabel));
            OnPropertyChanged(nameof(CurrentProgressFinal));
            OnPropertyChanged(nameof(CurrentProgressInitial));
            OnPropertyChanged(nameof(BeamDeviation));
            OnPropertyChanged(nameof(BeamDeviationWithinTolerance));
            OnPropertyChanged(nameof(BeamObjective));
            OnPropertyChanged(nameof(ReverseDeviation));
            OnPropertyChanged(nameof(ReverseDeviationWithinTolerance));
            OnPropertyChanged(nameof(ReverseObjective));
            OnPropertyChanged(nameof(AverageReading));
            OnPropertyChanged(nameof(AverageDeviation));
            OnPropertyChanged(nameof(AverageDeviationWithinTolerance));
            OnPropertyChanged(nameof(AverageObjective));
        }

        private bool BtnSaveAndXxxCanExecute()
        {
            try
            {
                return CheckMeasuresPages();
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        #region BtnSaveAndNext

        private DelegateCommand btnSaveAndNext;

        [XmlIgnore]
        public ICommand BtnSaveAndNext
        {
            get
            {
                if (btnSaveAndNext == null)
                    btnSaveAndNext = new DelegateCommand(BtnSaveAndNextClick, BtnSaveAndXxxCanExecute);
                return btnSaveAndNext;
            }
        }

        private void BtnSaveAndNextClick()
        {
            try
            {
                SaveMeasure(E.TiltInitOrAdjusting.Initial);

                Selectable<Point> selectable = NextSelectedPoint();
                if (selectable == null)
                {
                    new MessageInput(MessageType.Important, R.StringSmart_NoOtherPointSelected).Show();
                }
                else
                {
                    CurrentPoint = selectable;
                }
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        private void SaveMeasure(E.TiltInitOrAdjusting tiltInitOrAdjusting)
        {
            if (!CheckMeasuresPages())
                throw new Exception(R.StringSmart_CheckIsKO);

            Point p = currentPoint.Item;

            //Select the right station pair
            StationTiltModule.ChangePointTheo(p._Name);

            //Select the right station in the pair
            if (tiltInitOrAdjusting == E.TiltInitOrAdjusting.Initial)
                StationTiltModule._ActualStationTilt = StationTiltModule._InitStationTilt;
            else
                StationTiltModule._ActualStationTilt = StationTiltModule._AdjustingStationTilt;
            Station st = StationTiltModule._ActualStationTilt;

            // Ask if we overwrite the measure, comment it and make a new one, or cancel
            TiltStationToGeode.OnExisting onExisting = TiltStationToGeode.OnExisting.AppendWithComment;
            if (TiltStationToGeode.InExistingFile(st))
            {
                MessageInput mi = new MessageInput(MessageType.Choice, R.StringSmart_ExistingMeasure)
                {
                    ButtonTexts = new List<string> {
                        R.T_REPLACE,
                        R.T_TURN_PREVIOUS_TO_BAD,
                        R.T_CANCEL
                    },
                    Sender = SmartModule._Name
                };
                string choice = mi.Show().TextOfButtonClicked;

                if (choice == R.T_TURN_PREVIOUS_TO_BAD)
                    onExisting = TiltStationToGeode.OnExisting.MarkExistingAsBad;
                else if (choice == R.T_REPLACE)
                    onExisting = TiltStationToGeode.OnExisting.Replace;
                else
                    return;
            }

            //Remove the extra measures for SPS dipoles
            if (st._MeasuresTilt.Count > 1)
                st._MeasuresTilt.RemoveRange(1, st._MeasuresTilt.Count - 1);

            //Get the last measure for this point
            Measure m = st._MeasuresTilt[0];
            UpdateMeasure(m);
            StationTiltModule.UpdateOffset();
            StationTiltModule.ExportToGeode(false, false, onExisting);

            //Reset the field to prevent confirmation
            BeamReading = string.Empty;
            beamReadingSigma = null;
            ReverseReading = string.Empty;
            reverseReadingSigma = null;
        }

        private bool CheckMeasuresPages()
        {
            bool allGood = true;

            if (currentPoint != null && currentPoint.Selected)
            {
                CurrentPointError = string.Empty;
            }
            else
            {
                allGood = false;
                CurrentPointError = R.StringSmart_CurrentPointsIsUnchecked;
            }

            if (StationTiltModule._ActualStationTilt.TiltParameters._Instrument._Name == R.StringTilt_Unknown)
            {
                allGood = false;
                InstrumentError = R.StringSmart_NoInstrumentSelected;
            }
            else
            {
                InstrumentError = string.Empty;
            }

            if (beamReading.HasValue)
            {
                BeamReadingError = string.Empty;
            }
            else
            {
                allGood = false;
                BeamReadingError = R.StringSmart_BeamReadingInvalid;
            }

            if (reverseReading.HasValue)
            {
                ReverseReadingError = string.Empty;
            }
            else
            {
                allGood = false;
                ReverseReadingError = R.StringSmart_OppositeReadingInvalid;
            }

            return allGood;
        }

        private void UpdateMeasure(Measure m)
        {
            if (beamReading.HasValue)
            {
                m._ReadingBeamDirection = new DoubleValue(beamReading.Value / 1000d);
                if (beamReadingSigma.HasValue)
                    m._ReadingBeamDirection.Sigma = beamReadingSigma.Value / 1000d;
            }

            if (reverseReading.HasValue)
            {
                m._ReadingOppositeBeam = new DoubleValue(reverseReading.Value / 1000d);
                if (reverseReadingSigma.HasValue)
                    m._ReadingOppositeBeam.Sigma = reverseReadingSigma.Value / 1000d;
            }
        }

        #endregion

        #region BtnSaveAndCorrect

        private DelegateCommand btnSaveAndCorrect;

        [XmlIgnore]
        public ICommand BtnSaveAndCorrect
        {
            get
            {
                if (btnSaveAndCorrect == null)
                    btnSaveAndCorrect = new DelegateCommand(BtnSaveAndCorrectClick, BtnSaveAndXxxCanExecute);
                return btnSaveAndCorrect;
            }
        }

        private void BtnSaveAndCorrectClick()
        {
            try
            {
                SaveMeasure(E.TiltInitOrAdjusting.Initial);

                CurrentPage = Page.FinalMeasures;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        #region BtnSaveAndInitial

        private DelegateCommand btnSaveAndInitial;

        [XmlIgnore]
        public ICommand BtnSaveAndInitial
        {
            get
            {
                if (btnSaveAndInitial == null)
                    btnSaveAndInitial = new DelegateCommand(BtnSaveAndInitialClick, BtnSaveAndXxxCanExecute);
                return btnSaveAndInitial;
            }
        }

        private void BtnSaveAndInitialClick()
        {
            try
            {
                SaveMeasure(E.TiltInitOrAdjusting.Adjusting);

                // Find the next point selected without initial measure
                Selectable<Point> selectable = NextPointToBeMeasured(E.TiltInitOrAdjusting.Initial);
                if (selectable == null)
                {
                    new MessageInput(MessageType.Important, R.StringSmart_NoOtherPointSelected).Show();
                }
                else
                {
                    CurrentPoint = selectable;
                    CurrentPage = Page.InitialMeasures;
                }
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        #region BtnSaveAndFinal

        private DelegateCommand btnSaveAndFinal;

        [XmlIgnore]
        public ICommand BtnSaveAndFinal
        {
            get
            {
                if (btnSaveAndFinal == null)
                    btnSaveAndFinal = new DelegateCommand(BtnSaveAndFinalClick, BtnSaveAndXxxCanExecute);
                return btnSaveAndFinal;
            }
        }

        private void BtnSaveAndFinalClick()
        {
            try
            {
                SaveMeasure(E.TiltInitOrAdjusting.Adjusting);

                // Find the next point selected without initial measure
                Selectable<Point> selectable = NextPointToBeMeasured(E.TiltInitOrAdjusting.Adjusting);
                if (selectable == null)
                {
                    new MessageInput(MessageType.Important, R.StringSmart_NoOtherPointSelected).Show();
                }
                else
                {
                    CurrentPoint = selectable;
                    CurrentPage = Page.FinalMeasures;
                }
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        private Selectable<Point> NextPointToBeMeasured(E.TiltInitOrAdjusting tiltInitOrAdjusting)
        {
            // Find the next point selected without final measure
            bool PointToBeMeasured(Selectable<Point> sp)
            {
                // We don't want the points not selected
                if (!sp.Selected)
                    return false;

                // We don't want the points with a saved station
                if (StationTiltModule._StationTiltSequences.Exists((s) => s.TheoPoint == sp.Item
                        && s.TiltParameters._TiltInitOrAdjusting == tiltInitOrAdjusting
                        && s.TiltParameters._State is Common.Station.State.StationTiltSaved))
                    return false;

                return true;
            }

            Selectable<Point> nextPoint = NextSelectedPoint(PointToBeMeasured);

            return nextPoint;
        }



    }
}
