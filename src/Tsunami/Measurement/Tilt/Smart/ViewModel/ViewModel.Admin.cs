﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using TSU.Common.Blocks;
using TSU.Common.Elements;
using C = TSU.Common.Elements.Composites;
using System.Windows.Input;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;
using EM = TSU.Common.Elements.Manager;

namespace TSU.Tilt.Smart
{
    public partial class ViewModel : ViewModelBase
    {
        private void InitializeAdminPage()
        {
            if (StationTilt?.ParametersBasic != null 
                && !string.IsNullOrWhiteSpace(StationTilt.ParametersBasic._Team) 
                && StationTilt.ParametersBasic._Team != R.String_UnknownTeam)
            {
                Team = StationTilt.ParametersBasic._Team;
            }
            else
            {
                Team = string.Empty;
            }

            TheoFilePath = R.StringSmart_NoTheoreticalFile;
            if (SmartModule?._ElementManager?.filesOpened != null)
            {
                List<C.TheoreticalElement> filesOpened = SmartModule._ElementManager.filesOpened;
                if (filesOpened.Count > 0)
                {
                    C.TheoreticalElement lastFile = filesOpened[filesOpened.Count - 1];
                    if (!string.IsNullOrEmpty(lastFile._Name))
                        TheoFilePath = lastFile._Name;
                }
            }

            if (StationTiltModule?._InitStationTilt?.ParametersBasic?._Operation != null)
                InitialOperationNumber = StationTiltModule._InitStationTilt.ParametersBasic._Operation.value.ToString();
            else
                InitialOperationNumber = O.Operation.NoOp.value.ToString();

            if (StationTiltModule?._AdjustingStationTilt?.ParametersBasic?._Operation != null)
                FinalOperationNumber = StationTiltModule._AdjustingStationTilt.ParametersBasic._Operation.value.ToString();
            else
                FinalOperationNumber = O.Operation.NoOp.value.ToString();
        }

        private string team;
        public string Team
        {
            get => team;
            set => SetProperty(ref team, value);
        }

        private string theoFilePath;
        public string TheoFilePath
        {
            get => theoFilePath;
            set => SetProperty(ref theoFilePath, value);
        }

        private string initialOperationNumber;
        public string InitialOperationNumber
        {
            get => initialOperationNumber;
            set => SetProperty(ref initialOperationNumber, value);
        }
        public string InitialOperationFile => "Chantier.dat";
        [XmlIgnore]
        public O.Operation InitialOperation { get; private set; }

        private string finalOperationNumber;
        public string FinalOperationNumber
        {
            get => finalOperationNumber;
            set => SetProperty(ref finalOperationNumber, value);
        }
        public string FinalOperationFile => "Chantier.dat";
        [XmlIgnore]
        public O.Operation FinalOperation { get; private set; }

        #region BtnTheo
        private DelegateCommand btnTheo;
        [XmlIgnore]
        public ICommand BtnTheo
        {
            get
            {
                if (btnTheo == null)
                    btnTheo = new DelegateCommand(BtnTheoClick);
                return btnTheo;
            }
        }

        private void BtnTheoClick()
        {
            try
            {
                string filePath = TsuPath.GetFileNameToOpen(
                    MainView,
                    Preferences.Preferences.Instance.Paths.FichierTheo,
                    "",
                    "Geode files (*.dat)|*.dat",
                    R.T_BROWSE_THEORETICAL_FILE);
                if (string.IsNullOrEmpty(filePath))
                    return;

                void Loading()
                {
                    try
                    {
                        TheoFilePath = filePath;
                        if (CheckTheo())
                            LoadTheo();
                    }
                    catch (OperationCanceledException)
                    {
                        MainView?.WaitingForm?.Stop();
                    }
                    catch (Exception ex)
                    {
                        ShowMessageOfBug(ex);
                        MainView?.WaitingForm?.Stop();
                    }
                }

                MainView.ShowProgress(MainView, Loading, 2, string.Format(R.TM_TheoLoad, filePath));
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                MainView?.WaitingForm?.Stop();
            }
        }    
        #endregion

        #region BtnInitialOperation
        private DelegateCommand btnInitialOperation;
        [XmlIgnore]
        public ICommand BtnInitialOperation
        {
            get
            {
                if (btnInitialOperation == null)
                    btnInitialOperation = new DelegateCommand(BtnInitialOperationClick);
                return btnInitialOperation;
            }
        }
        private void BtnInitialOperationClick()
        {
            try
            {
                O.Operation o = SmartModule.OperationManager.SelectOperation();
                if (o == null) return;
                InitialOperationNumber = o.value.ToString();
                InitialOperation = o;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }
        #endregion

        #region BtnFinalOperation
        private DelegateCommand btnFinalOperation;
        [XmlIgnore]
        public ICommand BtnFinalOperation
        {
            get
            {
                if (btnFinalOperation == null)
                    btnFinalOperation = new DelegateCommand(BtnFinalOperationClick);
                return btnFinalOperation;
            }
        }
        private void BtnFinalOperationClick()
        {
            try
            {
                O.Operation o = SmartModule.OperationManager.SelectOperation();
                if (o == null) return;
                FinalOperationNumber = o.value.ToString();
                FinalOperation = o;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }
        #endregion

        #region BtnValidate
        private DelegateCommand btnValidate;
        [XmlIgnore]
        public ICommand BtnValidate
        {
            get
            {
                if (btnValidate == null)
                    btnValidate = new DelegateCommand(BtnValidateClick, BtnValidateCanExecute);
                return btnValidate;
            }
        }
        private bool BtnValidateCanExecute()
        {
            try
            {
                return CheckAdminPage();
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        private void BtnValidateClick()
        {
            try
            {
                if (!CheckAdminPage())
                    return;

                StationTilt.ParametersBasic._Team = Team;
                StationTiltModule._InitStationTilt.ParametersBasic._Operation = InitialOperation;
                StationTiltModule._AdjustingStationTilt.ParametersBasic._Operation = FinalOperation;

                CurrentPage = Page.Components;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }
        #endregion

        #region Error labels

        private string teamError;
        public string TeamError
        {
            get => teamError;
            set => SetProperty(ref teamError, value);
        }

        private string theoError;
        public string TheoError
        {
            get => theoError;
            set => SetProperty(ref theoError, value);
        }

        private string initialOperationError;
        public string InitialOperationError
        {
            get => initialOperationError;
            set => SetProperty(ref initialOperationError, value);
        }

        private string finalOperationError;
        public string FinalOperationError
        {
            get => finalOperationError;
            set => SetProperty(ref finalOperationError, value);
        }

        #endregion

        private static readonly string[] adminPropertiesToCheck = {
            nameof(Team),
            nameof(TheoFilePath),
            nameof(InitialOperationNumber),
            nameof(FinalOperationNumber)
        };

        private void OnChangeAdminPage(object sender, PropertyChangedEventArgs e)
        {
            if (adminPropertiesToCheck.Contains(e.PropertyName))
                CheckAdminPage();
        }


        private bool CheckTeam()
        {
            string value = Team;

            if (value == R.StringSmart_NoTeam)
            {
                TeamError = R.StringSmart_NoTeam;
                return false;
            }

            value = value.Trim().ToUpper();

            if (!Regex.IsMatch(value, @"^[A-Z]+$") || value.Length >= 8)
            {
                TeamError = R.StringSmart_TeamFormat;
                return false;
            }

            Team = value;
            TeamError = "";
            return true;
        }

        private bool CheckOperation(string operationNumberToCheck, out O.Operation operation, out string errorMessage)
        {
            if (string.IsNullOrEmpty(operationNumberToCheck))
            {
                operation = null;
                errorMessage = R.StringSmart_NoOperation;
                return false;
            }

            if (!int.TryParse(operationNumberToCheck, out int operationNumber) || operationNumber <= 0)
            {
                operation = null;
                errorMessage = string.Format(R.StringSmart_OperationFormat, operationNumberToCheck);
                return false;
            }

            operation = O.Manager.GetOperationbyNumber(operationNumber);
            if (operation == null)
            {
                errorMessage = string.Empty;
                return true;
            }

            if (!operation.IsSet)
            {
                operation = null;
                errorMessage = R.StringSmart_NoOperation;
                return false;
            }

            errorMessage = string.Empty;
            return true;
        }

        private bool CheckOperationCoherence()
        {
            if (FinalOperationNumber == InitialOperationNumber)
            {
                InitialOperationError = R.StringSmart_SameOperation;
                FinalOperationError = R.StringSmart_SameOperation;
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool CheckAdminPage()
        {
            bool teamOk = CheckTeam();
            CheckTheo();
            bool initOpOK = CheckOperation(InitialOperationNumber, out O.Operation initialOperation, out string newInitialOperationError);
            InitialOperation = initialOperation;
            InitialOperationError = newInitialOperationError;
            bool finalOpOK = CheckOperation(FinalOperationNumber, out O.Operation finalOperation, out string newFinalOperationError);
            FinalOperation = finalOperation;
            FinalOperationError = newFinalOperationError;
            bool opCoherenceOK;
            if (initOpOK && finalOpOK)
                opCoherenceOK = CheckOperationCoherence();
            else
                // If there is only one operation selected, coherence is ok, so don't check, to avoid overwriting other errors
                opCoherenceOK = true;

            if (teamOk && (initOpOK || finalOpOK) && opCoherenceOK)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool CheckTheo()
        {
            string filePath = TheoFilePath;

            if (filePath == R.StringSmart_NoTheoreticalFile)
            {
                TheoError = R.StringSmart_NoTheoreticalFile;
                return false;
            }

            if (!File.Exists(filePath))
            {
                TheoError = string.Format(R.StringSmart_TheFileDoesNotExist, filePath);
                return false;
            }

            TheoError = string.Empty;
            return true;
        }

        private void LoadTheo()
        {
            EM.Module em = SmartModule._ElementManager;
            FileInfo fileInfo = new FileInfo(TheoFilePath);
            Coordinates.CoordinateSystemsTsunamiTypes type = em.GetProbableType(fileInfo, out bool isDotDat);
            if (type == Coordinates.CoordinateSystemsTsunamiTypes.Unknown)
            {
                if (em.View.AskForCoordinateType(out Coordinates.CoordinateSystemsTsunamiTypes chosenType))
                    type = chosenType;
                else
                {
                    TheoError = R.T_CANCEL_BY_USER;
                    return;
                }
            }
            em.AddElementsFromFile(fileInfo, type, isDotDat, updateView: false);
            Preferences.Preferences.Instance.Paths.FichierTheo = fileInfo.Directory.FullName;
            TheoError = string.Empty;
        }
    }
}