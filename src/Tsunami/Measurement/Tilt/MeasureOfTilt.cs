using System;
using System.Xml.Serialization;
using TSU;
using System.IO;

namespace TSU.Tilt
{
    [Serializable]
    [XmlType(TypeName = "Tilt.Measure")]
    public class Measure : TSU.Common.Measures.Measure
	{
        public override string _Name
        {
            get
            {
                if (this.name != "Unknown")
                    return this.name;
                string name = "";
                if (this._Point != null)
                    name = this._Point._Name;
                else
                    name = this._PointName;
                return $"Tm of {name}";
            }
        }

        /// <summary>
        /// store in Rad
        /// </summary>
        public TSU.DoubleValue _ReadingBeamDirection { get; set; }

        /// <summary>
        /// store in Rad
        /// </summary>
        public TSU.DoubleValue _ReadingOppositeBeam { get; set; }

        /// <summary>
        /// store in Rad
        /// </summary>
        public TSU.DoubleValue _AverageBothDirection { get; set; }

        /// <summary>
        /// store in Rad
        /// </summary>
        public TSU.DoubleValue _Ecart { get; set; }
        /// <summary>
        /// Constructeur
        /// </summary>
        public Measure() : base()
        {
            this._ReadingBeamDirection = new DoubleValue();
            this._ReadingOppositeBeam = new DoubleValue();
            this._AverageBothDirection = new DoubleValue();
            this._Ecart = new DoubleValue();
            this._ReadingBeamDirection.Value = na;
            this._ReadingBeamDirection.Sigma = na;
            this._ReadingOppositeBeam.Value = na;
            this._ReadingOppositeBeam.Sigma = na;
            this._AverageBothDirection.Value = na;
            this._AverageBothDirection.Sigma = na;
            this._Ecart.Value = na;
            this._Ecart.Sigma = na;
            this._Date = System.DateTime.Now;
            enableCommentWithStatus = false;
        }

        /// <summary>
        /// Calcul du tilt moyen et mise � jour de l'�cart
        /// </summary>
        internal void AverageTiltCalculation()
        // Calcul du tilt moyen et mise � jour de l'�cart
        {
            if ((this._ReadingBeamDirection.Value != na)&&(this._ReadingOppositeBeam.Value != na))
            {
            this._AverageBothDirection.Value = (this._ReadingBeamDirection.Value - this._ReadingOppositeBeam.Value) / 2;
            this._Date = System.DateTime.Now;
            }
        }

        public override string ToString()
        {
            return "Roll " + base.ToString();
        }

        /// <summary>
        /// Calcul de l'�cart par rapport � la valeur th�orique
        /// </summary>
        internal void EcartCalculation(double tiltTheo)
        // Calcul de l'�cart par rapport � la valeur th�orique
        {
            if (this._AverageBothDirection.Value!= na)
            {
                this._Ecart.Value = this._AverageBothDirection.Value - tiltTheo;
            }
        }

        /// <summary>
        /// Reset une mesure tilt faisceau
        /// </summary>
        internal void ResetMeasForBeamDirectionReadingDeleted()
        {
            this._ReadingBeamDirection.Value = na;
            this._AverageBothDirection.Value = na;
            this._AverageBothDirection.Sigma = na;
            this._ReadingBeamDirection.Sigma = na;
            this._Ecart.Value = na;
            this._Ecart.Sigma = na;
        }

        /// <summary>
        /// Reset une mesure tilt inverse faisceau
        /// </summary>
        /// <param name="measureNumber"></param>
        internal void ResetMeasForBeamOppositeDirectionReadingDeleted()
        {
            this._ReadingOppositeBeam.Value = na;
            this._AverageBothDirection.Value = na;
            this._AverageBothDirection.Sigma = na;
            this._ReadingOppositeBeam.Sigma = na;
            this._Ecart.Value = na;
            this._Ecart.Sigma = na;
        }

        public override object Clone()
        {
            Measure newMeasure = (Measure)base.Clone();
            newMeasure._ReadingBeamDirection = (DoubleValue)_ReadingBeamDirection.Clone();
            newMeasure._ReadingOppositeBeam = (DoubleValue)_ReadingOppositeBeam.Clone();
            newMeasure._AverageBothDirection = (DoubleValue)_AverageBothDirection.Clone();
            newMeasure._Ecart = (DoubleValue)_Ecart.Clone();
            return newMeasure;
        }
    }

}
