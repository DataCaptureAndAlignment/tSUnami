﻿using System;
using System.Xml.Serialization;
using R = TSU.Properties.Resources;


namespace TSU.Common.Instruments
{
    [XmlType("Theodolite")] // because it is the same name as insturment theoolite
    public class Theodolite : Sensor, IPolarInstrument
    {
        public double sigmaInstrCentering { get; set; }
       
        public Theodolite()
        {

        }
    }
}
