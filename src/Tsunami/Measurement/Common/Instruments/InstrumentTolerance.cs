﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace TSU.Common.Instruments
{
    [Serializable]
    public class InstrumentTolerance
    {
        public enum Types { Same_Face , Opposite_Face , Closure }

        public class Type
        {
            [XmlIgnore]
            public Types type;

            [XmlAttribute]
            public double H_CC { get; set; }

            [XmlAttribute]
            public double V_CC { get; set; }

            [XmlAttribute]
            public double D_mm { get; set; }
        }

        [XmlAttribute]
        public InstrumentTypes InstrumentType { get; set; }

        private Type same_Face;
        public Type Same_Face
        {
            get
            {
                return same_Face;
            }
            set
            {
                same_Face = value;
                same_Face.type = Types.Same_Face;
            } 
        }


        private Type opposite_Face;
        public Type Opposite_Face
        {
            get
            {
                return opposite_Face;
            }
            set
            {
                opposite_Face = value;
                opposite_Face.type = Types.Opposite_Face;
            }
        }

        private Type closure;
        public Type Closure
        {
            get
            {
                return closure;
            }
            set
            {
                closure = value;
                closure.type = Types.Closure;
            }
        }
        public InstrumentTolerance()
        {

        }

        public InstrumentTolerance(InstrumentTypes InstrumentType, 
            double Angular_Cc_Same_Face,double Angular_Cc_Oppisite_Face,double Angular_Cc_Closure,
            double Distance_mm_Same_Face,double Distance_mm_Oppisite_Face,double Distance_mm_Closure)
        {
            this.InstrumentType = InstrumentType;
            this.Same_Face = new Type();
            this.Same_Face.H_CC = Angular_Cc_Same_Face;
            this.Same_Face.D_mm = Distance_mm_Same_Face;
            this.Opposite_Face = new Type();
            this.Opposite_Face.H_CC = Angular_Cc_Oppisite_Face;
            this.Opposite_Face.D_mm = Distance_mm_Oppisite_Face;
            this.Closure = new Type();
            this.Closure.H_CC = Angular_Cc_Closure;
            this.Closure.D_mm = Distance_mm_Closure;
        }
    }
}
