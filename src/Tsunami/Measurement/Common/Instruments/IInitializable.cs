using System;
using System.Collections.Generic;
using System.Text;using R = TSU.Properties.Resources;
using TSU;

namespace TSU.Common.Instruments
{
    public interface IInitializable
	{
        Result InitializeSensor();
	}
}
