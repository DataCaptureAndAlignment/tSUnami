﻿using System;
using System.Xml.Serialization;
using D = TSU.Common.Instruments.Device;

namespace TSU.Common.Instruments
{
    public interface ISensor
    {

    }
    [XmlInclude(typeof(D.TS60.Instrument))]
    [XmlInclude(typeof(Tracker))]
    [XmlInclude(typeof(D.AT40x.Instrument))]
    [XmlInclude(typeof(D.TDA5005.Instrument))]
    [XmlInclude(typeof(D.TCRA1100.Instrument))]
    [XmlInclude(typeof(D.T3000.Instrument))]
    [XmlInclude(typeof(D.TC2002.Instrument))]
    [XmlInclude(typeof(D.Manual.Theodolite.Instrument))]
    [XmlInclude(typeof(D.Simulator.Instrument))]
    [XmlInclude(typeof(D.Manual.Theodolite.T2))]

    [XmlInclude(typeof(D.Manual.Level.Instrument))]
    [XmlInclude(typeof(D.DNA03.Instrument))]
    [XmlInclude(typeof(D.LS15.Instrument))]
    [XmlInclude(typeof(D.WYLER.Instrument))]

    [XmlInclude(typeof(D.Manual.TiltMeter.Instrument))]
    [XmlInclude(typeof(D.Manual.Ecartometer.Instrument))]
    [XmlInclude(typeof(OffsetMeter))]

    [Serializable]
    public class Sensor : Instrument, ISensor
    {
        
        // Methods
        public Sensor()
        {
            this._SerialNumber = "000000";
        }
        //public virtual void CreateView()
        //{ }
        public override object Clone()
        {
            Sensor newAInstrument = (Sensor)this.MemberwiseClone(); // to make a clone of the primitive types fields

            return newAInstrument;
        }
    }
}