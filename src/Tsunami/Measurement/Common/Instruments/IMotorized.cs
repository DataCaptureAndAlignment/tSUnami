using System;
using System.Collections.Generic;
using System.Text;using R = TSU.Properties.Resources;
using TSU;
using TSU.Common.Instruments.Device.AT40x;

namespace TSU.Common.Instruments
{
	public interface IMotorized
	{
		Result MoveTo();

        void Goto();

		void ChangeFace();
        void LockUnLock();

        Result FineAdjust();
	}

    public interface ILiveData
    {
        LiveData LiveData { get; set; }

        void LiveData_Start();
        void LiveData_Stop();

        void LiveData_StartStop();
    }
}
