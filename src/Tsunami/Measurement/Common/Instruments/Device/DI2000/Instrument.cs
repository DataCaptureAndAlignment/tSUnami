﻿using System.Xml.Serialization;

namespace TSU.Common.Instruments.Device.DI2000
{
    [XmlType(TypeName = " DI2000.Instrument")]
    public class Instrument : TotalStation
    {
        public Instrument()
        {
        }
    }

}
