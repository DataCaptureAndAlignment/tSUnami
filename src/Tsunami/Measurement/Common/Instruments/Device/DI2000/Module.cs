﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using TSU.Common.Measures;
using TSU;
using TSU.Common.Instruments.Adapter;
using TSU.ENUM;
using P = TSU.Views.Message.ProgressMessage;
using R = TSU.Properties.Resources;
using TSU.Tools;
using System.Xml.Serialization;
using System.Threading.Tasks;

namespace TSU.Common.Instruments.Device.DI2000
{
    [XmlType(TypeName =" DI2000.Module")]
    public class Module : Instruments.Module
    {
        [XmlIgnore]
        public new Device.DI2000.View View
        {
            get
            {
                return this._TsuView as Device.DI2000.View;
            }
            set
            {
                this._TsuView = value;
            }
        }

        public virtual Reflector.Reflector Reflector { get; set; }
        private List<string> _listPortName;
        private int _listPortIndex;
        private string _portName;
        private string _portNumberAlreadyOpen;
        private int _portHandle;
        private bool isInitialised;
        private bool _portAvailable;

        public Module() : base() { }


        public Module(TSU.Module parentModule, Instruments.Instrument i)
            : base(parentModule)
        {
            this.Instrument = i;
            this._Name = i._Name;
        }
        public override void Initialize()
        {
            base.Initialize();
            _listPortName = GetAllPorts();
            _listPortIndex = 0;
            _portAvailable = false;
            isInitialised = false;
            if (_listPortName.Count > 0) _portAvailable = true;
            if (_portAvailable) Logs.Log.AddEntryAsResult(this, TryNextAvailablePorts());
            _portHandle = -1;
            JustMeasuredData = new Polar.Measure();
            ToBeMeasuredData = new Polar.Measure();
            
        }

        #region InstrumentPolarModule overrides
        
       
        internal override void OnMeasureToDoReceived()
        {
            base.OnMeasureToDoReceived();
            Polar.Measure toBe = (ToBeMeasuredData as Polar.Measure);
        }

        public override bool IsReadyToMeasure()
        {
            if (!this.InstrumentIsOn) throw new Exception("Instrument is Off ?");
            if (this.ParentInstrumentManager.ParentModule is Polar.Station.Module)
                if ((ToBeMeasuredData as Polar.Measure).Distance.Reflector == null || (ToBeMeasuredData as Polar.Measure).Distance.Reflector._Name == R.String_Unknown) throw new Exception(R.T_No_REFLECTOR);

            return true;
        }
        
        

        internal void ShowTemperatureOnInstrument()
        {
            if (this.InstrumentIsOn)
            if (!(T3000_dll.ShowDisplayTemp(_portHandle) == 0)) throw new Exception("Could not show temperature on instrument");
        }

        internal void ShowBatteryOnInstrument()
        {
            if (!(T3000_dll.ShowBattery(_portHandle) == 0)) throw new Exception("Could not show Battery on instrument");
        }

        #endregion

        public Result TryNextAvailablePorts()
        {
            Result result = new Result();
            if (_portAvailable)
            {
                _listPortIndex += 1;
                if (_listPortIndex >= _listPortName.Count) _listPortIndex = 0;
                _portName = GetAllPorts()[_listPortIndex];
                result.Success =true;
                result.AccessToken = "Port " + _portName + " is selected";
            }
            else
	        {
                result.Success =false;
                result.ErrorMessage = "No port Available" ;
	        }
            return result;
        }


        public override bool InstrumentIsOn { get => isConnected; set => isConnected = value; }
        bool isConnected;

        public override Result Connect()
        {
            Result result = new Result();
            if (_portAvailable)
            {
                OpenPortIfNotOpened();
                
                if (T3000_dll.TurnOn(_portHandle) == 0)
                {
                    result.Success = true;
                    result.AccessToken = "The Instrument turned On (handle=" + _portHandle + ")";
                    isConnected = true;
                }
                else
                {
                    result.Success = false;
                    isConnected = false;
                    result.ErrorMessage = "Impossible to Connect";
                }
            }
            else
            {
                result.Success = false;
                isConnected = false;
                result.ErrorMessage = "No port Available";
            }
            return result;
        }
        public override Result Disconnect()
        {
            Result result = new Result();
            T3000_dll.StopAllProcess(_portHandle,1);

            T3000_dll.TurnOff(_portHandle);
            result.Success = true;
            result.AccessToken = "Disconnected ("+_portHandle+")";
            isConnected = false;
            return result;
        }
       
        public override Result InitializeSensor()
        {
            Result result = new Result();
            if (this.InstrumentIsOn)
            {
                int count = 0;
                string message = "";
                if (T3000_dll.SetGRE4(_portHandle) == 0) { message += "Set GR4, "; count += 1; }
                if (T3000_dll.SetDistancePrgm0(_portHandle) == 0) { message += "Set Distance Prgm 0, "; count += 1; }
                if (T3000_dll.TurnOnCompensator(_portHandle) == 0) { message += "Turn On Compensator, "; count += 1; }
                if (T3000_dll.TurnOnCorrectionCalculator(_portHandle) == 0) { message += "Turn On Correction Calculator, "; count += 1; }
                if (T3000_dll.SetAdditionConstantToZero(_portHandle) == 0) { message += "Set Addition Constant To Zero, "; count += 1; }
                if (T3000_dll.SetPpmToZero(_portHandle) == 0) { message += "Set Ppm To Zero, "; count += 1; }
                if (T3000_dll.SetUnitToGrad(_portHandle) == 0) { message += "Set Unit To Grad, "; count += 1; }
                if (T3000_dll.SetDecimalDisplayTo4(_portHandle) == 0) { message += "Set Decimal Display To 4, "; count += 1; }
                if (T3000_dll.SetDistanceUnityTo10Minus4(_portHandle) == 0) { message += "Set Distance Unity To 10e-4, "; count += 1; }
                if (T3000_dll.ShowDisplayHAndV(_portHandle) == 0) { message += "Show Display H And V, "; count += 1; }
                if (T3000_dll.SetFonctionnementContinu(_portHandle) == 0) { message += "Set Fonctionnement Continu, "; count += 1; }
                if (T3000_dll.SetReflectiveTargetMode(_portHandle) == 0) { message += "Set Reflective Target Mode, "; count += 1; }

                if (count == 12)
                {
                    isInitialised = true;
                    result.AccessToken = message + " Initialization completed";
                    result.Success = true;
                }
                else
                {
                    result.ErrorMessage = "Partial Initialization : " + count + "/12";
                    result.Success = false;
                }
            }
            else
            {
                result.ErrorMessage = "Instrument is OFF";
                result.Success = false;
            }
            return result;
        }
        public Result StopAll()
        {
            Result result = new Result();
            if (this.InstrumentIsOn)
            {
                if (T3000_dll.StopAllProcess(_portHandle, 10) == 0)
                {
                    result.AccessToken = "Errors cleaned";
                    result.Success = true;
                }
                else
                {
                    result.ErrorMessage = "Impossible to clean";
                    result.Success = false;
                }
            }
            else
            {
                result.ErrorMessage = "Instrument is OFF";
                result.Success = false;
            }
            return result;

        }

        private void OpenPortIfNotOpened()
        {
            string portNumber = _portName.Substring(3, _portName.Length - 3);
            if (portNumber == _portNumberAlreadyOpen) return;

            // Open port
            if (T3000_dll.OpenPort(portNumber, 2400, 7, 2, 0, ref _portHandle) != -1)
                _portNumberAlreadyOpen = portNumber;
        }

        private Result MeasureAngles(ref float hz, ref float v)
        {
            Result result = new Result();

            if (!isInitialised) return new Result() { Success = false, ErrorMessage = "Not initialized!" };

            if (T3000_dll.GetAngles(_portHandle, ref hz, ref  v) == 0)
            {
                result.AccessToken = string.Format("Hz = {0} gon, V = {1} gon", hz, v);
                result.Success = true;
            }
            else
            {
                result.AccessToken = "Impossible to measure the angle";
                result.Success = false;
            }
            return result;
        }
        private Result MeasureDistance(ref float d)
        {
            Result result = new Result();

            if (!isInitialised) return new Result() { Success = false, ErrorMessage = "Not initialized!" };

            float dummy = 0;
            if (T3000_dll.GetDistance(_portHandle, ref dummy, ref  dummy, ref d) == 0)
            {
                result.AccessToken = string.Format("D = {0} meters",d);
                result.Success = true;
            }
            else
            {
                result.AccessToken = "Impossible to measure the Distance";
                result.Success = false;
            }
            return result;
        }
        private Result MeasureAll( ref float hz, ref float v, ref float d)
        {
            Result result = new Result();

            if (!isInitialised) return new Result() { Success = false,  ErrorMessage = "Not initialized!" };

            if (T3000_dll.GetDistance(_portHandle, ref hz, ref  v, ref d) == 0)
                return new Result() { Success = true, AccessToken = string.Format("Hz = {0} gon, V = {1} gon, D = {2} meters", hz, v, d) };
            else
                return new Result() { Success = false, ErrorMessage = "Impossible to measure Distance" };
            
        }
        private List<string> GetAllPorts()
        {
            List<String> allPorts = new List<String>();
            foreach (String portName in System.IO.Ports.SerialPort.GetPortNames())
            {
                allPorts.Add(portName);
            }
            return allPorts;
        }

        
    }
}
