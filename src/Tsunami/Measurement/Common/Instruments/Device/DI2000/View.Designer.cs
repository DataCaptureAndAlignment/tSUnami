﻿using R = TSU.Properties.Resources;


namespace TSU.Common.Instruments.Device.DI2000
{
    partial class View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (this.Module == null)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Device.TC2002.View));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelLog = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonAll = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripButtonHz = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDist = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonOn = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCOM = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonInit = new System.Windows.Forms.ToolStripButton();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelMeteo = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.panel1.Controls.Add(this.panelLog);
            this.panel1.Controls.Add(this.toolStrip1);
            this.panel1.Location = new System.Drawing.Point(8, 31);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(555, 218);
            this.panel1.TabIndex = 0;
            // 
            // panelLog
            // 
            this.panelLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelLog.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.panelLog.Location = new System.Drawing.Point(11, 8);
            this.panelLog.Name = "panelLog";
            this.panelLog.Size = new System.Drawing.Size(538, 142);
            this.panelLog.TabIndex = 7;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStrip1.BackgroundImage =R.ButtonPictureTC2002_Backgroung;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonAll,
            this.toolStripDropDownButton1,
            this.toolStripButtonHz,
            this.toolStripButtonDist,
            this.toolStripButton3,
            this.toolStripButtonOn,
            this.toolStripButtonCOM,
            this.toolStripButtonInit});
            this.toolStrip1.Location = new System.Drawing.Point(5, 150);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(545, 63);
            this.toolStrip1.TabIndex = 6;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonAll
            // 
            this.toolStripButtonAll.AutoSize = false;
            this.toolStripButtonAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonAll.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAll.Image")));
            this.toolStripButtonAll.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonAll.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripButtonAll.Name = "toolStripButtonAll";
            this.toolStripButtonAll.Size = new System.Drawing.Size(60, 60);
            this.toolStripButtonAll.Text = "Measure";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.Image =R.ButtonPictureTC2002_Reflector;
            this.toolStripDropDownButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripDropDownButton1.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.ShowDropDownArrow = false;
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(52, 60);
            this.toolStripDropDownButton1.Click += new System.EventHandler(this.toolStripDropDownButton1_Click);
            // 
            // toolStripButtonHz
            // 
            this.toolStripButtonHz.AutoSize = false;
            this.toolStripButtonHz.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonHz.Image =R.ButtonPictureTC2002_Hz;
            this.toolStripButtonHz.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonHz.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripButtonHz.Name = "toolStripButtonHz";
            this.toolStripButtonHz.Size = new System.Drawing.Size(60, 60);
            this.toolStripButtonHz.Text = "Measure angles only";
            this.toolStripButtonHz.Click += new System.EventHandler(this.ButtonHzV_Click);
            // 
            // toolStripButtonDist
            // 
            this.toolStripButtonDist.AutoSize = false;
            this.toolStripButtonDist.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonDist.Image =R.ButtonPictureTC2002_DIST;
            this.toolStripButtonDist.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonDist.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripButtonDist.Name = "toolStripButtonDist";
            this.toolStripButtonDist.Size = new System.Drawing.Size(60, 60);
            this.toolStripButtonDist.Text = "Measure distance only";
            this.toolStripButtonDist.Click += new System.EventHandler(this.ButtonDist_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.AutoSize = false;
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image =R.ButtonPictureTC2002_Stop;
            this.toolStripButton3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton3.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(60, 60);
            this.toolStripButton3.Text = R.T_CANCEL;
            this.toolStripButton3.Click += new System.EventHandler(this.ButtonStop_Click);
            // 
            // toolStripButtonOn
            // 
            this.toolStripButtonOn.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonOn.AutoSize = false;
            this.toolStripButtonOn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonOn.Image =R.ButtonPictureTC2002_OnOFF;
            this.toolStripButtonOn.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonOn.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripButtonOn.Name = "toolStripButtonOn";
            this.toolStripButtonOn.Size = new System.Drawing.Size(60, 60);
            this.toolStripButtonOn.Text = "Switch ON/OFF";
            this.toolStripButtonOn.Click += new System.EventHandler(this.buttonPower_Click);
            // 
            // toolStripButtonCOM
            // 
            this.toolStripButtonCOM.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonCOM.AutoSize = false;
            this.toolStripButtonCOM.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonCOM.Image =R.ButtonPictureTC2002_COM;
            this.toolStripButtonCOM.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonCOM.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripButtonCOM.Name = "toolStripButtonCOM";
            this.toolStripButtonCOM.Size = new System.Drawing.Size(60, 60);
            this.toolStripButtonCOM.Text = "Change COM port";
            this.toolStripButtonCOM.Click += new System.EventHandler(this.buttonCom_Click);
            // 
            // toolStripButtonInit
            // 
            this.toolStripButtonInit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonInit.AutoSize = false;
            this.toolStripButtonInit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonInit.Image =R.ButtonPictureTC2002_INIT;
            this.toolStripButtonInit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonInit.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripButtonInit.Name = "toolStripButtonInit";
            this.toolStripButtonInit.Size = new System.Drawing.Size(60, 60);
            this.toolStripButtonInit.Text = "Launch Initialisation of recommended setting";
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.labelTitle.Font = new System.Drawing.Font("Impact", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.labelTitle.Location = new System.Drawing.Point(8, 5);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(111, 23);
            this.labelTitle.TabIndex = 1;
            this.labelTitle.Text = "WILD TC2002";
            // 
            // labelMeteo
            // 
            this.labelMeteo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMeteo.AutoSize = true;
            this.labelMeteo.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
            this.labelMeteo.Location = new System.Drawing.Point(439, 13);
            this.labelMeteo.Name = "labelMeteo";
            this.labelMeteo.Size = new System.Drawing.Size(124, 13);
            this.labelMeteo.TabIndex = 2;
            this.labelMeteo.Text = "T 20°C P 960hPa H 60%";
            // 
            // Tc2002View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.ClientSize = new System.Drawing.Size(571, 257);
            this.Controls.Add(this.labelMeteo);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(255, 175);
            this.Name = "Tc2002View";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.T3000Form_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonAll;
        private System.Windows.Forms.ToolStripButton toolStripButtonHz;
        private System.Windows.Forms.ToolStripButton toolStripButtonDist;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButtonInit;
        private System.Windows.Forms.ToolStripButton toolStripButtonCOM;
        private System.Windows.Forms.ToolStripButton toolStripButtonOn;
        private System.Windows.Forms.Panel panelLog;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.Label labelMeteo;
    }
}

