﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSU.ENUM;
using TSU.Preferences;
using TSU.Common.Instruments.Reflector;


namespace TSU.Common.Instruments.Device.DI2000
{
    public partial class View : Device.View
    {
        private Result result;
        private new Module Module
        {
            get
            {
                return this._Module as Module;
            }
        }
        private Instrument DI2000
        {
            get
            {
                return this.Module.Instrument as Instrument;
            }
        }
        private Reflectors reflectors;

        public View(Instruments.Module instrumentModule)
            : base(instrumentModule)
        {
            InitializeComponent();
            this.ApplyThemeColors();
            InitializeLog();

        }

        private void InitializeLog()
        {
            // 
            // log
            // 
            Preferences.GUI.Theme.ThemeColors colors = Tsunami2.Preferences.Theme.Colors;
            LogView = new Logs.LogView();
            LogView.SetColors(beginningOf: colors.LightForeground,
                             logBack: colors.DarkBackground,
                             dateAndTime: colors.LightForeground,
                             error: colors.Bad,
                             fYI: colors.LightForeground,
                             payAttentionTo: colors.Attention,
                             success: colors.Good,
                             finishOf: colors.LightForeground);
            LogView.ShowDockedFill();
            panelLog.Controls.Add(LogView);
        }

        private void ApplyThemeColors()
        {
            this.panel1.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.panelLog.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStrip1.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripButtonAll.ImageTransparentColor =TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripDropDownButton1.ImageTransparentColor =TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripButtonHz.ImageTransparentColor =TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripButtonDist.ImageTransparentColor =TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripButton3.ImageTransparentColor =TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripButtonOn.ImageTransparentColor =TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripButtonCOM.ImageTransparentColor =TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripButtonInit.ImageTransparentColor =TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.labelTitle.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.labelMeteo.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.Bad;
            this.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;

        }
        private void ButtonReflector_Click(object sender, EventArgs e)
        {
            string name = ((ToolStripMenuItem)sender).Text;
            this.Module.Reflector = reflectors.GetReflectorByTypeNamePlusSerialNumber(name);
        }
        

        private void buttonCom_Click(object sender, EventArgs e)
        {
            result = Module.TryNextAvailablePorts();
            Logs.Log.AddEntryAsResult(this._Module, result);
        }

        private void buttonPower_Click(object sender, EventArgs e)
        {
            if (Module.InstrumentIsOn)
            {
                result = Module.Disconnect();
                Logs.Log.AddEntryAsResult(this._Module, result);
            }
            else
            {
                result = Module.Connect();
                Logs.Log.AddEntryAsResult(this._Module, result);
            }
        }

        private void ButtonHzV_Click(object sender, EventArgs e)
        {
            result = Module.Measure();
            if (result.Success)
            {
                this.Module.ReportMeasurement();
                this.Module.FinishMeasurement();
            }

            this.Module.BeingMeasured = null;
            Logs.Log.AddEntryAsResult(this._Module, result);
        }

        private void ButtonDist_Click(object sender, EventArgs e)
        {
            result = Module.Measure();
            if (result.Success)
            {
                this.Module.ReportMeasurement();
                this.Module.FinishMeasurement();
            }

            this.Module.BeingMeasured = null;
            Logs.Log.AddEntryAsResult(this._Module, result);
        }
        
        
        private void ButtonStop_Click(object sender, EventArgs e)
        {
            result = Module.StopAll();
            Logs.Log.AddEntryAsResult(this._Module, result);
        }

        private void T3000Form_Load(object sender, EventArgs e)
        {

        }

        private void toolStripDropDownButton1_Click(object sender, EventArgs e)
        {
            if (reflectors == null)
            {
                reflectors = new Reflectors(this.Module.Instrument as TotalStation);
                foreach (Reflector.Reflector item in reflectors.ReflectorList)
                {
                    ToolStripMenuItem button = new ToolStripMenuItem();
                    button.Text = item._Name;
                    // button.ToolTipText = "Constante=" + item.Constante + " Diameter=" + item.Diameter;
                    toolStripDropDownButton1.DropDownItems.Add(button);
                    button.Click += new System.EventHandler(this.ButtonReflector_Click);
                }
                this.Module.Reflector = reflectors.ReflectorList[0];
            }

        }
        
    }
}
