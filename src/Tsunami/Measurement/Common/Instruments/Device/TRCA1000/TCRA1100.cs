using TSU.ENUM;
using System.Xml.Serialization;
using TSU;
using TSU.Common.Measures;
using TSU.Common.Elements;

namespace TSU.Common.Instruments.Device.TCRA1100
{
    [XmlType(TypeName = "TCRA1100.Instrument")]
    public class Instrument : TotalStation
    {
        #region variables spécifiques TCRA1100
        [XmlIgnore]
        public double _Battery { get; set; }
        [XmlIgnore]
        public double _Tinterne { get; set; }
        [XmlIgnore]
        public string _PortCom { get; set; }
        [XmlIgnore]
        public Compensator _Compensator { get; set; }
        [XmlIgnore]
        public Angles _AngleCorrection { get; set; }
        [XmlIgnore]
        public int _Face { get; set; }
        [XmlIgnore]
        public bool _ATR { get; set; }
        [XmlIgnore]
        public bool _RedLaser { get; set; }
        [XmlIgnore]
        public bool _UserRedLaserPreference { get; set; }
        [XmlIgnore]
        public bool _Lock { get; set; }
        [XmlIgnore]
        public bool ReflectorLessMode
        { get; set;}
        #endregion

        public Instrument()
        {
            this._Brand = "Leica";
            this.Id = string.Empty;
            //this._LastMeasure = new Management.Measure();
            this._Model = "TCRA1100";
            this._SerialNumber = string.Empty;
            this._PortCom = "COM1";
            this._AngleCorrection = new Angles();
            this._Battery = 0;
            this._Compensator = new Compensator();
            this._InstrumentClass = InstrumentClasses.TACHEOMETRE;
            this._Compensator.longitudinalAxisInclination = -999;
            this._Compensator.tranverseAxisInclination = -999;
            this._Compensator.inclinationAccuracy = -999;
            this._ATR = false;
            this._Lock = false;
            this._Face = 1;
            this._RedLaser = false;
            this._UserRedLaserPreference = true;
            this.ReflectorLessMode = false;
        }
    }
}
