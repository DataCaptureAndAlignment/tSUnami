﻿using System;
using System.Collections.Generic;
using System.Linq;
using TSU.Views;
using TSU.Views.Message;
using R = TSU.Properties.Resources;


namespace TSU.Common.Instruments.Device.TCRA1100
{
    public partial class View : Device.View
    {
        private new Module Module
        {
            get
            {
                return this._Module as Module;
            }
        }

        private Module _Tcra1100Module
        {
            get
            {
                return this._Module as Module;
            }
        }

        private Instrument _Tcra1100
        {
            get
            {
                return this._Tcra1100Module.Instrument as Instrument;
            }
        }
        private System.Windows.Forms.ToolTip toolTip1;
        public View(Instruments.Module instrumentModule)
            : base(instrumentModule)
        {
            InitializeComponent();
            this.ApplyThemeColors();
            InitializeLog();
        }

        private void InitializeLog()
        {
            LogView = new Logs.LogView();
            LogView.ShowDockedFill();
            LogView.SetColors(logBack: panel2.BackColor);
            // show entries from this module only
            LogView.WantedModule = this._Module;
            LogView.UpdateView();
            this.panel2.Controls.Add(this.LogView);
        }

        private void ApplyThemeColors()
        {
            this.label1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.label1.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.panel1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.panel2.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.panel2.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.comboBox_PortCom.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.comboBox_PortCom.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.textBox_numberOfMeas.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.textBox_numberOfMeas.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button_All.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.Button_OnOff.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.label_ComPort.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.label_numberOfMeas.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.button_Rec.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_Distance.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonGoto.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonGotoAll.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonGotoAll.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.buttonGotoAll.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.pictureBox_OnOff.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.labelMeteo.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
            this.labelMeteo.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.pictureBox_Battery.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_face.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_face.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_face.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_ATR.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_ATR.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_ATR.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_ATR.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_Laser.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_Laser.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_Laser.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_TargetType.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_TargetType.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_TargetType.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.label_Bulle.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.label_Bulle.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.pictureBox_bulle.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
        }
        private List<TsuTask> TaskToDo = new List<TsuTask>();
        private void Tcra1100View_Load(object sender, EventArgs e)
        {
            labelMeteo.Text = labelMeteo.Text = string.Format("T° {0}C\r\nP {1}hPa\r\nH {2}%", this.Module.temperature, this.Module.pressure, this.Module.humidity);
            labelMeteo.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
            this.SetAllToolTips();
            ViewTCRAOff();
        }
        private void ViewTCRAOn()
        {
            button_Rec.Visible = false;
            button_Distance.Visible = false; //Comme TS60 pas de mesure distance seule
            button_All.Visible = false;
            buttonGoto.Visible = false;
            buttonGotoAll.Visible = false;
            label_numberOfMeas.Visible = false; //mis dans le module polaire
            label_Bulle.Visible = true; //bulle ne fonctionne pa
            pictureBox_Battery.Visible = true;
            pictureBox_bulle.Visible = true; //bulle ne fonctionne pas
            Timer_check.Enabled = true;
            textBox_numberOfMeas.Visible = false; //mis dans le module polaire
            button_ATR.Visible = true;
            button_face.Visible = true;
            button_Laser.Visible = true;
            button_TargetType.Visible = false;
            Module.InstrumentIsOn = true;
            this.pictureBox_OnOff.Image = R.Tda5005_ON;
            this.pictureBox_bulle.Image = R.Tda5005_Bulle_Bad;
            this.comboBox_PortCom.Visible = false;
            this.label_ComPort.Visible = false;
            timer_ComPort.Enabled = false;
            this.Module.OnMeasureToDoReceived();
        }
        public void AllowGoto()
        {
            buttonGoto.Visible = true;
            buttonGotoAll.Visible = true;
        }
        public void DontAllowGoto()
        {
            buttonGoto.Visible = false;
            buttonGotoAll.Visible = false;
        }
        internal void DontAllowAll()
        {
            button_All.Visible = false;
            button_Rec.Visible = false;
        }
        internal void AllowAll()
        {
            button_All.Visible = true;
            button_Rec.Visible = true;
        }
        private void ViewTCRAOff()
        {
            button_Rec.Visible = false;
            button_Distance.Visible = false;
            button_All.Visible = false;
            buttonGoto.Visible = false;
            buttonGotoAll.Visible = false;
            button_Laser.Visible = false;
            button_TargetType.Visible = false;
            label_numberOfMeas.Visible = false;
            label_Bulle.Visible = false;
            pictureBox_Battery.Visible = false;
            pictureBox_bulle.Visible = false;
            button_face.Visible = false;
            Timer_check.Enabled = false;
            textBox_numberOfMeas.Visible = false;
            button_ATR.Visible = false;
            Module.InstrumentIsOn = false;
            this.pictureBox_OnOff.Image = R.Tda5005_OFF;
            this.pictureBox_bulle.Image = R.Tda5005_Bulle_Bad;
            GetAllPorts();
            this.comboBox_PortCom.Visible = true;
            this.label_ComPort.Visible = true;
            timer_ComPort.Enabled = true;
        }
        /// <summary>
        /// Get the com port available
        /// </summary>
        private void GetAllPorts()
        {
            string portSelected = this.comboBox_PortCom.Text;
            this.comboBox_PortCom.BeginUpdate();
            string[] allPort = this.Module.GetAllPorts();
            // pour éviter le clignotement ajouter et retirer que les ports nécessaires un à un
            for (int i = this.comboBox_PortCom.Items.Count - 1; i >= 0; i--)
            {
                if (!allPort.Contains(this.comboBox_PortCom.Items[i])) this.comboBox_PortCom.Items.RemoveAt(i);
            }
            foreach (string item in allPort)
            {
                if (!this.comboBox_PortCom.Items.Contains(item)) this.comboBox_PortCom.Items.Add(item);
            }
            //this.comboBox_PortCom.Items.AddRange(this.Module.GetAllPorts());
            if (this.comboBox_PortCom.Items.Count > 0)
            {
                if (!this.comboBox_PortCom.Items.Contains(portSelected))
                    this.comboBox_PortCom.Text = this.comboBox_PortCom.Items[0].ToString();
            }
            else this.comboBox_PortCom.Text = "";
            this.comboBox_PortCom.EndUpdate();
        }
        private void BatteryStateImage()
        {
            if (Module.Tcra1100._Battery <= 5.55)
            {
                this.pictureBox_Battery.Image = R.Tda5005_Battery_Bad;
            }
            else if (Module.Tcra1100._Battery > 6)
            {
                this.pictureBox_Battery.Image = R.Tda5005_Battery_Good;
            }
            else
            {
                this.pictureBox_Battery.Image = R.Tda5005_Battery_Average;
            }
        }
        private void UpdateBubble()
        {
            this.label_Bulle.Text = Module.Tcra1100._Compensator.ToString();
            double diffGradBulle = Math.Sqrt(Math.Pow(Module.Tcra1100._Compensator.longitudinalAxisInclination, 2) + Math.Pow(Module.Tcra1100._Compensator.tranverseAxisInclination, 2)) * 200 / Math.PI;
            if (diffGradBulle > 0.002)
            {
                this.pictureBox_bulle.Image = R.Tda5005_Bulle_Bad;
            }
            if (diffGradBulle <= 0.001)
            {
                this.pictureBox_bulle.Image = R.Tda5005_Bulle_Good;
            }
            if ((diffGradBulle <= 0.002) && (diffGradBulle > 0.001))
            {
                this.pictureBox_bulle.Image = R.Tda5005_Bulle_Average;
            }
        }
        private void AtrStateImage()
        {
            if (this.Module.Tcra1100._ATR)
            {
                this.button_ATR.Image = R.Tda5005_ATR_On;
            }
            else
            {
                this.button_ATR.Image = R.Tda5005_ATR_OFF;
            }
        }
        private void FaceStateImage()
        {
            if (this._Tcra1100._Face == 1)
            {
                this.button_face.Image = R.Tda5005_CercleGauche;
            }
            else
            {
                this.button_face.Image = R.Tda5005_CercleDroit;
            }
        }
        private void Button_OnOff_Click(object sender, EventArgs e)
        {
            try
            {
                if (Module.InstrumentIsOn == false)
                    TurnOn();
                else
                    TurnOff();
            }
            catch (Exception ex)
            {
                string titleAndMessage = "Connection/Disconnection failed;" + ex.Message;
                new MessageInput(MessageType.Critical, titleAndMessage).Show();
            }
        }
        private void TurnOff()
        {
            try
            {
                Result closeSuccess = new Result();

                TsuTask taskDeconnection = new TsuTask()
                {
                    ActionName = "Geocom deconnection",
                    estimatedTimeInMilliSec = 1000,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        closeSuccess = Module.Disconnect();
                    },
                    postAction = () =>
                    {
                        Logs.Log.AddEntryAsResult(this._Module, closeSuccess);
                        if (closeSuccess.Success == true)
                        {
                            this.Module.InvokeOnApplicationDispatcher(ViewTCRAOff);
                        }
                        else
                        {
                            Timer_check.Enabled = true;
                            Timer_check.Start();
                        }
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    timeOutAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.ViewTCRAOff();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { taskDeconnection }, "Deconnection");
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(taskDeconnection);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { taskDeconnection }, "Deconnection");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }
        }
        internal void TurnOn()
        {
            try
            {
                Result connecTSUccess = new Result();
                Result iniTSUccess = new Result();
                if (!this.Module.MeteoSet)
                {
                    this.Module.SetMeteoManually();
                    this.UpdateLabelMeteo();
                }
                TsuTask taskConnection = new TsuTask()
                {
                    ActionName = "Connection",
                    estimatedTimeInMilliSec = 3000,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Module.Tcra1100._PortCom = comboBox_PortCom.Text;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        connecTSUccess = Module.Connect();
                    },
                    postAction = () =>
                    {
                        // post action to do in the UI
                        connecTSUccess.MergeString(this._Tcra1100Module.GetSerialNumber());
                        Logs.Log.AddEntryAsResult(this._Module, connecTSUccess);
                        if (connecTSUccess.Success == false)
                        {
                            new MessageInput(MessageType.Critical, R.StringTS60_ProblemDuringInitialization).Show();
                            ViewTCRAOff();
                            TaskToDo.Clear();
                            this.Module.IsBusyCommunicating = false;
                        }
                        else
                        {
                            DoTimerCheckThings();
                            UpdateView();
                        }

                    },
                    cancelAction = () =>
                    {
                        ViewTCRAOff();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    }
                };

                TsuTask taskInit = new TsuTask()
                {
                    ActionName = "Initialisation",
                    estimatedTimeInMilliSec = 300,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Module.Tcra1100._PortCom = comboBox_PortCom.Text;
                    },
                    mainAction = () =>
                    {
                        if (connecTSUccess.Success) iniTSUccess.MergeString(Module.InitializeSensor());
                    },
                    postAction = () =>
                    {
                        if (connecTSUccess.Success)
                        {
                            if (iniTSUccess.Success == true)
                            {
                                ViewTCRAOn();
                                //No need as bubble is not shown
                                //this.Module.MeasureCompensator();
                                this.Module.GetBatteryLevel();
                                this.Module.GetTCRAFace();
                                this.Module.CheckATRLock();
                                this.Module.OnMeasureToDoReceived();
                                BatteryStateImage();
                                FaceStateImage();
                                AtrStateImage();
                                this.TargetLock_StateImage();
                            }
                            Logs.Log.AddEntryAsResult(this._Module, iniTSUccess);
                            if (iniTSUccess.Success == false)
                            {
                                new MessageInput(MessageType.Critical, R.StringTS60_ProblemDuringInitialization).Show();
                                ViewTCRAOff();
                            }
                        }
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        ViewTCRAOff();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(taskConnection);
                    TaskToDo.Add(taskInit);
                    //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { taskConnection, taskInit }, "Connection");
                    RunTasksWithWaitingMessage(new List<TsuTask>() { taskConnection, taskInit }, "Connection");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
                //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { taskConnection, taskInit }, "Connection");
            }
            catch (Exception ex)
            {
                if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                TaskToDo.Clear();
                throw;
            }

        }
        private object SomeOperationAsync(object cancellationToken)
        {
            throw new NotImplementedException();
        }
        private void button_Rec_Click(object sender, EventArgs e)
        {
            this.Module.DistanceWanted = false;
            this.Module.GotoWanted = false;
            TryAction(LaunchMeasurement);
        }
        private void button_All_Click(object sender, EventArgs e)
        {
            this.Module.DistanceWanted = true;
            this.Module.GotoWanted = false;
            TryAction(LaunchMeasurement);
        }
        public override void MeasureAsync()
        {
            base.MeasureAsync(); // do nothing
            TryAction(LaunchMeasurement);

        }
        public void LaunchMeasurement()
        {
            try
            {
                Result r = new Result();
                //   if (this.Module._ToBeMeasureTheodoliteData == null) throw new Exception("Nothing to measure");
                if (this.Module._ToBeMeasureTheodoliteData == null) throw new Exception(R.T_NOTHING_TO_MEASURE);
                int factor = this.Module._ToBeMeasureTheodoliteData.NumberOfMeasureToAverage;
                if (Module._ToBeMeasureTheodoliteData.Face == FaceType.DoubleFace) factor *= 2;

                TsuTask task = new TsuTask()
                {
                    ActionName = "Measuring...",

                    estimatedTimeInMilliSec = 7000 * factor,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        r.MergeString(this.Module.tempResult);
                        r.MergeString(this.Module.MeasureTCRA());
                    },
                    cancelAction = () =>
                    {
                        this.Module.CancelMeasureInProgress();
                        this.Module.CancelMeasureInProgress();
                        TaskToDo.Clear();
                    },
                    postAction = () =>
                    {
                        this.Module.BeingMeasured = null;
                        this.Module.DistanceWanted = true; // by default sot that measure() from polar Module take all.
                        if (r.Success)
                        {
                            this.Module.ReportMeasurement();
                            this.Module.FinishMeasurement();
                            this.Module.tempResult = r;
                        }
                        else
                        {
                            Logs.Log.AddEntryAsResult(this.Module, this.Module.tempResult);
                        }
                        this.AtrStateImage();
                        this.Module.OnMeasureToDoReceived();
                        this.TargetLock_StateImage();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Measure");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
                //if (this.Module.CancelByMessageBeforeMeasure()) throw new CancelException();
                //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Measure");
            }
            catch (Exception ex)
            {
                if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }


        }
        public void LaunchModuleMeasurement()
        {
            try
            {
                Result r = new Result();

                //if (this.Module._ToBeMeasureTheodoliteData == null) throw new Exception("Nothing to measure");
                if (this.Module._ToBeMeasureTheodoliteData == null) throw new Exception(R.T_NOTHING_TO_MEASURE);
                int factor = this.Module._ToBeMeasureTheodoliteData.NumberOfMeasureToAverage;
                if (Module._ToBeMeasureTheodoliteData.Face == FaceType.DoubleFace) factor *= 2;

                TsuTask task = new TsuTask()
                {
                    ActionName = "Measuring...",

                    estimatedTimeInMilliSec = 7000 * factor,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        r.MergeString(this.Module.tempResult);
                        r.MergeString(this.Module.MeasureTCRA());
                    },
                    cancelAction = () =>
                    {
                        this.Module.CancelMeasureInProgress();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    postAction = () =>
                    {
                        this.Module.BeingMeasured = null;
                        this.Module.DistanceWanted = true; // by default sot that measure() from polar Module take all.
                        if (r.Success)
                        {
                            this.Module.tempResult = r;
                        }
                        this.AtrStateImage();
                        this.Module.OnMeasureToDoReceived();
                        this.TargetLock_StateImage();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Measure");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
                //if (this.Module.CancelByMessageBeforeMeasure()) throw new CancelException();
                //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Measure");
            }
            catch (Exception ex)
            {
                if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }


        }
        private void timer_Check_Tick(object sender, EventArgs e)
        {
            if (!this.Module.IsBusyCommunicating)
            {
                Result checkTda = new Result();

                try
                {
                    TsuTask task = new TsuTask()
                    {
                        ActionName = "Timer Check",
                        estimatedTimeInMilliSec = 4,
                        timeOutInSec = 40,
                        preAction = () =>
                        {
                            Timer_check.Stop();
                            Timer_check.Enabled = false;
                            this.Module.CheckAndWaitIfBusy();
                            //this.doingTimerThings = true;
                        },
                        mainAction = () =>
                        {
                            TSU.Debug.WriteInConsole("Timer_check_TCRA_TickMainAction");
                            checkTda.MergeString(this.DoTimerCheckThings());
                        },
                        postAction = () =>
                        {
                            TSU.Debug.WriteInConsole("Timer_check_TCRA_TickPostAction");

                            checkTda.AccessToken = (checkTda.AccessToken as string).Replace("\r\n", ", ");
                            Logs.Log.AddEntryAsResult(this._Module, checkTda);
                            Timer_check.Enabled = true;
                            Timer_check.Start();
                            //TaskToDo.Clear();
                            this.Module.IsBusyCommunicating = false;
                            //if (checkTda.Success)
                            //{
                            //    Timer_check_TDA.Enabled = true;
                            //    Timer_check_TDA.Start();
                            //}
                            //else
                            //{
                            //    // disconnect and reconnect 
                            //    Result r = new Result();
                            //    this.Module.Disconnect();
                            //    r.MergeString(this.Module.Connect());
                            //    Logs.Log.AddEntryAsResult(this._Module, r);
                            //    if (r.Success == false)
                            //    {
                            //        this.ShowMessageOfCritical(R.StringTS60_Connection_Problem);
                            //        this.TurnOff();
                            //        this.Module.InvokeOnApplicationDispatcher(this.ViewTdaOff);
                            //    }
                            //    else
                            //    {
                            //        Timer_check_TDA.Enabled = true;
                            //        Timer_check_TDA.Start();
                            //    }
                            //}
                            //this.doingTimerThings = false;
                            TSU.Debug.WriteInConsole("Timer_check_TCRA_TickDone");
                        },
                        timeOutAction = () =>
                        {
                            Timer_check.Enabled = true;
                            Timer_check.Start();
                            checkTda.AccessToken = (checkTda.AccessToken as string).Replace("\r\n", ", ");
                            Logs.Log.AddEntryAsResult(this._Module, checkTda);
                            TaskToDo.Clear();
                            this.Module.IsBusyCommunicating = false;
                        },
                        ExceptionAction = () =>
                        {
                            Timer_check.Enabled = true;
                            Timer_check.Start();
                            TaskToDo.Clear();
                            this.Module.IsBusyCommunicating = false;
                        },
                    };
                    if (TaskToDo.Count == 0)
                    {
                        //I don't want to block a action made by the timer if done during the timer
                        //TaskToDo.Add(task);
                        this.RunTasksWithoutWaitingMessage(new List<TsuTask>() { task, }, "Updating");
                    }
                }
                catch (Exception ex)
                {
                    if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                    Timer_check.Enabled = true;
                    Timer_check.Start();
                    TaskToDo.Clear();
                    throw;
                }
            }
        }
        /// <summary>
        /// Tache fait lors du tick du timer
        /// </summary>
        /// <returns></returns>
        public Result DoTimerCheckThings()
        {
            Result r = new Result();
            try
            {
                this.DisableButtons();
                r.MergeString(this.Module.CheckConnection());
                if (r.Success)
                {
                    // TOCHECK
                    //this.Module.MeasureReceived(); //why philippe? this should be done when a measure it set in the station module

                    //Measuring compensator switch off the laser during measurement, for now the bubble is not working so no need to measure it.
                    //r.MergeString(this.Module.MeasureCompensator());

                    r.MergeString(this.Module.GetCompensatorReading());
                    r.MergeString(this.Module.GetBatteryLevel());
                    r.MergeString(Module.GetTCRAFace());
                    r.MergeString(this.Module.CheckATRLock());
                    r.Success = true; //if not get current 1 parameter, I don't want to shut down TS60
                    this.UpdateView();

                    // TOCHECK
                    // this.Module.MeasureReceived(); //To check if still have measure to do (avoid having a goto and no measure

                }

                return r;
            }
            catch (Exception)
            {
                r.Success = false;

                return r;
            }
            finally
            {
                this.EnableButtons();
            }
        }

        private void EnableButtons()
        {
            Tsunami2.Properties.InvokeOnApplicationDispatcher(() =>
            {
                this.buttonGoto.Enabled = true;
                this.buttonGotoAll.Enabled = true;
                this.button_All.Enabled = true;
                this.button_ATR.Enabled = true;
                this.button_Distance.Enabled = true;
                this.button_face.Enabled = true;
                this.button_Laser.Enabled = true;
                this.Button_OnOff.Enabled = true;
                this.button_Rec.Enabled = true;
                this.button_TargetType.Enabled = true;
            });
        }

        private void DisableButtons()
        {
            Tsunami2.Properties.InvokeOnApplicationDispatcher(() =>
            {
                this.buttonGoto.Enabled = false;
                this.buttonGotoAll.Enabled = false;
                this.button_All.Enabled = false;
                this.button_ATR.Enabled = false;
                this.button_Distance.Enabled = false;
                this.button_face.Enabled = false;
                this.button_Laser.Enabled = false;
                this.Button_OnOff.Enabled = false;
                this.button_Rec.Enabled = false;
                this.button_TargetType.Enabled = false;
            });
        }

        private void button_ATR_Click(object sender, EventArgs e)
        {
            try
            {
                Result r = new Result();
                TsuTask task = new TsuTask()
                {
                    ActionName = "Set ATR",

                    estimatedTimeInMilliSec = 1000,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        r.MergeString(SwitchATR());
                    },
                    postAction = () =>
                    {
                        Logs.Log.AddEntryAsResult(this._Module, r);
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Set ATR LOCK functions");
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Set ATR LOCK functions");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }

            }
            catch (Exception ex)
            {
                if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }
        }
        /// <summary>
        /// Set the Atr and Lock when pressing the ATR button
        /// </summary>
        private Result SwitchATR()
        {
            Result result = this.Module.SwitchAtr();
            if (result.Success == true)
            {
                this.Module.InvokeOnApplicationDispatcher(this.AtrStateImage);
                this.Module.InvokeOnApplicationDispatcher(() => { button_Rec.Visible = !this.Module.Tcra1100._ATR; });
                this.Module.InvokeOnApplicationDispatcher(() => { button_Distance.Visible = !this.Module.Tcra1100._ATR; });
            }
            return result;
        }
        private void button_face_Click(object sender, EventArgs e)
        {
            try
            {
                Result checkface = new Result();
                TsuTask task = new TsuTask()
                {
                    ActionName = R.T_CHANGING_FACE,
                    estimatedTimeInMilliSec = 2000,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        checkface.MergeString(FaceChangeAction());
                    },
                    postAction = () =>
                    {
                        Logs.Log.AddEntryAsResult(this._Module, checkface);
                        FaceStateImage();
                        AtrStateImage();
                        this.TargetLock_StateImage();
                        this.Module.OnMeasureToDoReceived();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, R.T_CHANGING_FACE);
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, R.T_CHANGING_FACE);
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }
        }


        private Result FaceChangeAction()
        {
            Result r = new Result();
            r.MergeString(Module.InvertFace());
            r.MergeString(Module.GetTCRAFace());
            return r;
        }
        private void PopUpMenu_Load(object sender, EventArgs e)
        {

        }
        private void textBox_numberOfMeas_TextChanged(object sender, EventArgs e)
        {
            int numberOfMeasure;
            if (Int32.TryParse(textBox_numberOfMeas.Text, out numberOfMeasure))
                this.Module._ToBeMeasureTheodoliteData.NumberOfMeasureToAverage = numberOfMeasure;
        }
        private void labelMeteo_Click(object sender, EventArgs e)
        {
            // Change values
            this.Module.SetMeteoManually();
            this.UpdateLabelMeteo();
        }

        public override void UpdateView()
        {
            base.UpdateView();
            this.Module.InvokeOnApplicationDispatcher(BatteryStateImage);
            this.Module.InvokeOnApplicationDispatcher(FaceStateImage);
            this.Module.InvokeOnApplicationDispatcher(AtrStateImage);
            this.Module.InvokeOnApplicationDispatcher(this.TargetLock_StateImage);
            this.Module.InvokeOnApplicationDispatcher(RedLaserStateImage);
            this.Module.InvokeOnApplicationDispatcher(this.UpdateBubble);
        }
        private void UpdateLabelMeteo()
        {
            labelMeteo.Text = string.Format("T° {0}C\r\nP {1}hPa\r\nH {2}%",
                this.Module.temperature.ToString("F1", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")),
                this.Module.pressure.ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")),
                this.Module.humidity.ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
            if (this.Module.MeteoSet) labelMeteo.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Good;
        }
        private void buttonGoto_Click(object sender, EventArgs e)
        {
            this.Module.GotoWanted = false;
            this.Module.MeasureWanted = false;
            this.Goto();
        }
        /// <summary>
        /// Do the goto to next position
        /// </summary>
        internal void Goto()
        {
            try
            {
                Result r = new Result();
                TsuTask task = new TsuTask()
                {
                    ActionName = "Goto...",

                    estimatedTimeInMilliSec = 7000,
                    timeOutInSec = 0,//*30 * factor*,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        this.Module.GotoTCRA();
                        this.Module.DistanceWanted = true;
                        r.MergeString(this.Module.tempResult);
                    },
                    postAction = () =>
                    {
                        Logs.Log.AddEntryAsResult(this.Module, this.Module.tempResult);
                        this.BatteryStateImage();
                        this.AtrStateImage();
                        this.TargetLock_StateImage();
                        this.Module.OnMeasureToDoReceived();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Goto");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
                //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Goto");
            }
            catch (Exception ex)
            {
                if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }

        }
        private void buttonGotoAll_Click(object sender, EventArgs e)
        {
            this.Module.GotoWanted = false;
            this.GotoAll();
        }
        /// <summary>
        /// Do the goto to next position
        /// </summary>
        private void GotoAll()
        {
            try
            {
                Result r = new Result();
                int factor = this.Module._ToBeMeasureTheodoliteData.NumberOfMeasureToAverage;
                if (Module._ToBeMeasureTheodoliteData.Face == FaceType.DoubleFace) factor *= 2;
                factor++;
                TsuTask task = new TsuTask()
                {
                    ActionName = "GotoAll...",

                    estimatedTimeInMilliSec = 7000 * factor,
                    timeOutInSec = 0,//*30 * factor*,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        this.Module.GotoTCRA();
                        this.Module.DistanceWanted = true;
                        r.MergeString(this.Module.tempResult);
                        if (r.Success) { r.MergeString(this.Module.MeasureTCRA()); }
                    },
                    postAction = () =>
                    {
                        this.Module.BeingMeasured = null;
                        this.Module.DistanceWanted = true; // by default sot that measure() from polar Module take all.
                        if (r.Success)
                        {
                            this.Module.ReportMeasurement();
                        }
                        else
                        {
                            Logs.Log.AddEntryAsResult(this.Module, this.Module.tempResult);
                        }
                        this.BatteryStateImage();
                        this.AtrStateImage();
                        this.TargetLock_StateImage();
                        this.Module.OnMeasureToDoReceived();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, R.T_GOTO_AND_DO_MEASUREMENT);
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }
        }
        private void timer_ComPort_Tick(object sender, EventArgs e)
        {
            if (!this.Module.IsBusyCommunicating)
            {
                try
                {
                    TsuTask task = new TsuTask()
                    {
                        ActionName = "Get ComPort",
                        estimatedTimeInMilliSec = 100,
                        timeOutInSec = 1,
                        preAction = () =>
                        {
                            this.Module.CheckAndWaitIfBusy();
                            timer_ComPort.Stop();
                            timer_ComPort.Enabled = false;
                        },
                        mainAction = () =>
                        {
                            TSU.Debug.WriteInConsole("Get ComPort Begin");
                            this.Module.InvokeOnApplicationDispatcher(this.GetAllPorts);
                        },
                        postAction = () =>
                        {
                            timer_ComPort.Enabled = true;
                            timer_ComPort.Start();
                            this.Module.IsBusyCommunicating = false;
                            TSU.Debug.WriteInConsole("Get ComPort end");
                        },
                        timeOutAction = () =>
                        {
                            timer_ComPort.Enabled = true;
                            timer_ComPort.Start();
                            this.Module.IsBusyCommunicating = false;
                            TSU.Debug.WriteInConsole("Get ComPort end with time out");
                        },
                        ExceptionAction = () =>
                        {
                            Timer_check.Enabled = true;
                            Timer_check.Start();
                            TaskToDo.Clear();
                            this.Module.IsBusyCommunicating = false;
                        },
                    };
                    if (TaskToDo.Count == 0)
                    {
                        //I don't want to block a action made by the timer if done during the timer
                        //TaskToDo.Add(task);
                        this.RunTasksWithoutWaitingMessage(new List<TsuTask>() { task, }, "Updating com port");
                    }
                }
                catch (Exception ex)
                {
                    if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                    timer_ComPort.Enabled = true;
                    timer_ComPort.Start();
                    TaskToDo.Clear();
                    throw;
                }
            }
        }
        private void button_TargetType_Click(object sender, EventArgs e)
        {
            try
            {
                if (TaskToDo.Count == 0)
                {
                    Timer_check.Stop();
                    Timer_check.Enabled = false;
                    this.Module.ChangeDefaultReflector();
                    this.ChangeReflector();
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                if (ex is System.OperationCanceledException) ex.Data.Clear();
                else throw;
            }
        }
        /// <summary>
        /// Change le réflecteur dans le TS60
        /// </summary>
        public void ChangeReflector()
        {
            try
            {
                Result r = new Result();
                TsuTask task = new TsuTask()
                {
                    ActionName = "Change Reflector",

                    estimatedTimeInMilliSec = 5000,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        r = this.Module.ChangeReflector();
                    },
                    postAction = () =>
                    {
                        Logs.Log.AddEntryAsResult(this._Module, r);
                        this.AtrStateImage();
                        this.RedLaserStateImage();
                        this.TargetLock_StateImage();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Change Reflector");
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Change Reflector");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }

        }
        private void TargetLock_StateImage()
        {
            if (this.Module.ToBeMeasuredData != null)
            {
                this.button_TargetType.Visible = true;
                switch ((this.Module.ToBeMeasuredData as Polar.Measure).Distance.Reflector._InstrumentType)
                {
                    case InstrumentTypes.CCR1_5:
                        this.button_ATR.Visible = true;
                        this.button_TargetType.BackgroundImage = R.TS60_RRR1_5;
                        break;
                    case InstrumentTypes.CSCOTCH:
                        this.button_ATR.Visible = false;
                        this.button_TargetType.BackgroundImage = R.TS60_Tape;
                        break;
                    case InstrumentTypes.LASER:
                        this.button_ATR.Visible = false;
                        this.button_TargetType.BackgroundImage = R.TS60_RL;
                        break;
                    default:
                        this.button_ATR.Visible = true;
                        this.button_TargetType.BackgroundImage = R.TS60_Unknown_Target;
                        break;
                }
            }
            else
            {
                this.button_TargetType.Visible = false;
            }
        }
        private void button_Laser_Click(object sender, EventArgs e)
        {
            try
            {
                Result r = new Result();
                TsuTask task = new TsuTask()
                {
                    ActionName = "Start/Stop Laser",

                    estimatedTimeInMilliSec = 2000,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        r.MergeString(InverseRedLaser());
                    },
                    postAction = () =>
                    {
                        Logs.Log.AddEntryAsResult(this._Module, r);
                        this.AtrStateImage();
                        this.TargetLock_StateImage();
                        this.RedLaserStateImage();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Set red laser");
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Set red laser");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }
        }
        private Result InverseRedLaser()
        {
            Result result = new Result();
            result = this.Module.SetRedLaserOnOff(!this.Module.Tcra1100._RedLaser);
            this.Module.InvokeOnApplicationDispatcher(RedLaserStateImage);
            return result;
        }
        private void RedLaserStateImage()
        {
            //this.button_Laser.Visible = true;
            this.button_Laser.Image = (this._Tcra1100._RedLaser) ? R.TS60_Laser_ON : R.TS60_Laser_Off;
        }

        private void button_Distance_Click(object sender, EventArgs e)
        {
            this.Module.DoTemporaryDistanceMeasurement();
        }
        /// <summary>
        /// Show a message indicating that the ATR fine adjust has failed
        /// </summary>
        public void MessageATRFailed()
        {
            DsaFlag ShowMsgATRFailed = DsaFlag.GetByNameOrAdd(this.Module.FinalModule.DsaFlags, "ShowMsgATRFailed");
            new MessageInput(MessageType.Warning, R.String_ATRFailed)
            {
                ButtonTexts = new List<string> { R.T_OK + "!" },
                DontShowAgain = ShowMsgATRFailed
            }.Show();
        }
        internal override void SetAllToolTips()
        {

            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            toolTip1.SetToolTip(this.Button_OnOff, R.StringTCRA1101_ToolTip_Init);
            toolTip1.SetToolTip(this.comboBox_PortCom, R.StringTCRA1101_ToolTip_port);
            toolTip1.SetToolTip(this.buttonGotoAll, R.StringTCRA1101_ToolTip_GotoAll);
            toolTip1.SetToolTip(this.buttonGoto, R.StringTCRA1101_ToolTip_Goto);
            toolTip1.SetToolTip(this.button_Distance, R.StringTCRA1101_ToolTip_Dist);
            toolTip1.SetToolTip(this.button_Rec, R.StringTCRA1101_ToolTip_Rec);
            toolTip1.SetToolTip(this.button_All, R.StringTCRA1101_ToolTip_All);
            toolTip1.SetToolTip(this.button_face, R.StringTCRA1101_ToolTip_Face);
            toolTip1.SetToolTip(this.button_ATR, R.StringTCRA1101_ToolTip_Lock);
            toolTip1.SetToolTip(this.button_Laser, R.StringTCRA1101_ToolTip_RedLaser);
            toolTip1.SetToolTip(this.button_TargetType, R.StringTCRA1101_ToolTip_TargetType);
            toolTip1.SetToolTip(this.labelMeteo, R.StringTCRA1101_ToolTip_Weather);
            toolTip1.SetToolTip(this.label_Bulle, R.StringTCRA1101_ToolTip_Bulle);
            toolTip1.SetToolTip(this.pictureBox_bulle, R.StringTCRA1101_ToolTip_Bulle);
            toolTip1.SetToolTip(this.pictureBox_Battery, R.StringTCRA1101_ToolTip_Battery);
            toolTip1.SetToolTip(this.pictureBox_OnOff, R.StringTCRA1101_ToolTip_ONOFF);
        }


        //private void ShowAsConnected()
        //{
        //    this.pictureBox_OnOff.Image = R.Tda5005_ON;
        //    panel3.Visible = true;
        //    this.AtrStateImage();
        //    this.RedLaserStateImage();
        //}
        //private void ShowAsDisconnected()
        //{
        //    this.pictureBox_OnOff.Image = R.Tda5005_OFF;
        //    panel3.Visible = false;
        //}



        //private void Button_Init_Click(object sender, EventArgs e)
        //{
        //    TryAction(Initialise);
        //}

        //public void Initialise()
        //{
        //    Result r = null;

        //    TsuTask t = new TsuTask() { ActionName = "Initialisation", estimatedTimeInMilliSec = 5000, timeOutInSec = 20 };

        //    t.mainAction = delegate
        //    {
        //        r = _Tcra1100Module.InitializeSensor();
        //    };
        //    t.postAction = delegate
        //    {
        //        Logs.Log.AddEntryAsResult(this._Module, r);
        //        if (r.Success)
        //        {
        //            this.pictureInit.Image = R.Tcra_Init;
        //        }
        //    };
        //    RunTasksWithWaitingMessage(t);
        //}

        //private void button_Disconnect_Click(object sender, EventArgs e)
        //{
        //    TryAction(Disconnect);
        //}

        //public void Disconnect()
        //{
        //    if (!this.Module.CheckConnection().Success) ShowAsDisconnected();

        //    Result r = null;

        //    TsuTask t = new TsuTask() { ActionName = "Disconnection", estimatedTimeInMilliSec = 1000, timeOutInSec = 20 };

        //    t.mainAction = delegate
        //    {
        //        r = _Tcra1100Module.Disconnect();
        //    };
        //    t.postAction = delegate
        //    {
        //        Logs.Log.AddEntryAsResult(this._Module, r);
        //        if (r.Success)
        //        {
        //            ShowAsDisconnected();
        //        }
        //    };
        //    RunTasksWithWaitingMessage(t);
        //}

        //private void button_FullInit_Click(object sender, EventArgs e)
        //{
        //    Logs.Log.AddEntryAsResult(this._Module, _Tcra1100Module.Initialize(1, true));
        //}

        //private void button_Distance_Click(object sender, EventArgs e)
        //{
        //    //bool parseOK;
        //    //int numberOfDistance;
        //    //DoubleValue averageSlopeDistance;
        //    //parseOK= Int32.TryParse(comboBox_NumberOfDistance.Text,out numberOfDistance);
        //    //Logs.Log.AddEntryAsResult(this._Module, _Tcra1100Module.GetAverageDistance(numberOfDistance, out averageSlopeDistance));
        //    //Logs.Log.AddEntryAsFYI(this._Module, string.Format("Distance={0}m\r\nEmq Distance={1}\r\n", averageSlopeDistance.Value, averageSlopeDistance.Sigma));
        //}

        //private void button_Rec_Click(object sender, EventArgs e)
        //{
        //    this.Module.DistanceWanted = false;
        //    this.Module.GotoWanted = false;
        //    TryAction(LaunchMeasurement);
        //    //bool parseOK;
        //    //int numberOAngle;
        //    //Angles averageAngle;
        //    //parseOK= Int32.TryParse(comboBox_NumberOfAngle.Text,out numberOAngle);
        //    //Logs.Log.AddEntryAsResult(this._Module, _Tcra1100Module.GetAverageAngle(out averageAngle, numberOAngle));
        //    //Logs.Log.AddEntryAsFYI(this._Module, string.Format("Hz={0}gon\r\nEmq Hz={1}\r\n", averageAngle.Horizontal.Value * 200 / Math.PI, averageAngle.Horizontal.Sigma * 200 / Math.PI));
        //    //Logs.Log.AddEntryAsFYI(this._Module, string.Format("V={0}gon\r\nEmq V={1}\r\n", averageAngle.Vertical.Value * 200 / Math.PI, averageAngle.Vertical.Sigma * 200 / Math.PI));
        //}

        //private void button_All_Click(object sender, EventArgs e)
        //{
        //    //bool parseOK;
        //    //int numberOfMeasure;
        //    //Angles averageAngle;
        //    //DoubleValue averageSlopeDistance;
        //    //parseOK = Int32.TryParse(comboBox_All.Text, out numberOfMeasure);
        //    //Logs.Log.AddEntryAsResult(this._Module, _Tcra1100Module.GetAllMeasurement(numberOfMeasure, out averageSlopeDistance, out averageAngle));
        //    //Logs.Log.AddEntryAsFYI(this._Module, string.Format("Hz={0}gon\r\nEmq Hz={1}\r\n", averageAngle.Horizontal.Value * 200 / Math.PI, averageAngle.Horizontal.Sigma * 200 / Math.PI));
        //    //Logs.Log.AddEntryAsFYI(this._Module, string.Format("V={0}gon\r\nEmq V={1}\r\n", averageAngle.Vertical.Value * 200 / Math.PI, averageAngle.Vertical.Sigma * 200 / Math.PI));
        //    //Logs.Log.AddEntryAsFYI(this._Module, string.Format("Distance={0}m\r\nEmq Distance={1}\r\n", averageSlopeDistance.Value, averageSlopeDistance.Sigma));
        //    this.Module.GotoWanted = false;
        //    this.Module.DistanceWanted = true;
        //    TryAction(LaunchMeasurement);
        //}
        //public override void MeasureAsync()
        //{
        //    base.MeasureAsync(); // do nothing
        //    TryAction(LaunchMeasurement);

        //}
        //private void Button_Connect_Click(object sender, EventArgs e)
        //{
        //    TryAction(Connect);
        //}

        //public void Connect()
        //{
        //    if (this.Module.CheckConnection().Success)
        //    {
        //        TsuTask ts = new TsuTask() { ActionName = "Connection", estimatedTimeInMilliSec = 10000, timeOutInSec = 20 };

        //        ts.mainAction = delegate
        //        {
        //            this.Module.CheckAllowGoto();
        //            this.Module.GetATRStatus();
        //            this.Module.GetTargetLOCKStatus();
        //        };
        //        ts.postAction = delegate
        //        {
        //            ShowAsConnected();
        //        };
        //        RunTasksWithWaitingMessage(ts);
        //        return;
        //    }

        //    ShowMessageOfValidation("TCRA must be OFF;If the instrument is not OFF, please turn it OFF now, before we try to connect");

        //    Result r = null;

        //    TsuTask t = new TsuTask() { ActionName = "Connection", estimatedTimeInMilliSec = 10000, timeOutInSec = 20 };

        //    t.mainAction = delegate
        //    {
        //        r = _Tcra1100Module.Connect();
        //    };
        //    t.postAction = delegate
        //    {
        //        Logs.Log.AddEntryAsResult(this._Module, r);
        //        if (r.Success)
        //        {
        //            ShowAsConnected();
        //            if (R.T_YES == ShowMessageOfChoice("Initialise parameters?;Do you want to do it now?", R.T_YES, R.T_NO))
        //                TryAction(Initialise);
        //            if (!this.Module.MeteoSet)
        //            {
        //                this.Module.SetMeteoManually();
        //                this.UpdateLabelMeteo();
        //            }
        //        }
        //        else
        //            this.Module.Disconnect();

        //    };
        //    t.cancelAction = () =>
        //    {
        //        if (this.Module.CheckConnection().Success)
        //        {
        //            Disconnect();
        //        }
        //    };

        //    RunTasksWithWaitingMessage(t);
        //}
        //private void button_RedLaser_Click(object sender, EventArgs e)
        //{
        //    Result r = new Result();
        //    TsuTask task = new TsuTask()
        //    {
        //        ActionName = "Start/Stop Laser",

        //        estimatedTimeInMilliSec = 2000,
        //        timeOutInSec = 5,
        //        preAction = () =>
        //        {
        //        },
        //        mainAction = () =>
        //        {
        //            r.MergeString(InverseRedLaser());
        //        },
        //        postAction = () =>
        //        {
        //            Logs.Log.AddEntryAsResult(this._Module, r);
        //        }
        //    };
        //    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Set red laser");
        //}
        //internal void RedLaserStateImage()
        //{
        //    if (this.Module.LaserIsOn) this.pictureLaser.Image = R.Tcra1101_Laser_ON;
        //    else this.pictureLaser.Image = R.Tcra1101_Laser_Off;
        //}
        //private Result InverseRedLaser()
        //{
        //    Result r = new Result();
        //    if (Module.LaserIsOn)
        //    {
        //        r.MergeString(_Tcra1100Module.SetRedLaserOnOff(false));
        //        Logs.Log.AddEntryAsResult(this._Module, r);
        //        if (r.Success) Module.LaserIsOn = false;
        //    }
        //    else
        //    {
        //        r.MergeString(r = _Tcra1100Module.SetRedLaserOnOff(true));
        //        Logs.Log.AddEntryAsResult(this._Module, r);
        //        if (r.Success) Module.LaserIsOn = true;
        //    }
        //    this.Module.InvokeOnApplicationDispatcher(RedLaserStateImage);
        //    return r;
        //}
        //private void buttonATR_Click(object sender, EventArgs e)
        //{
        //    Result r = new Result();
        //    TsuTask task = new TsuTask()
        //    {
        //        ActionName = "Start/Stop Laser",

        //        estimatedTimeInMilliSec = 2000,
        //        timeOutInSec = 5,
        //        preAction = () =>
        //        {
        //        },
        //        mainAction = () =>
        //        {
        //            r.MergeString(InverseAtr());
        //        },
        //        postAction = () =>
        //        {
        //            Logs.Log.AddEntryAsResult(this._Module, r);
        //        }
        //    };
        //    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Set Atr");
        //}
        ///// <summary>
        ///// Switch between ATr and manual
        ///// </summary>
        ///// <returns></returns>
        //private Result InverseAtr()
        //{
        //    Result r = new Result();
        //    if (Module.AtrIsOn)
        //    {
        //        r.MergeString(_Tcra1100Module.SetATROnOff(false));
        //        Logs.Log.AddEntryAsResult(this._Module, r);
        //        if (r.Success) Module.AtrIsOn = false;
        //    }
        //    else
        //    {
        //        r.MergeString(_Tcra1100Module.SetATROnOff(true));
        //        Logs.Log.AddEntryAsResult(this._Module, r);
        //        if (r.Success) Module.AtrIsOn = true;
        //    }
        //    this.Module.InvokeOnApplicationDispatcher(AtrStateImage);
        //    return r;
        //}

        ///// <summary>
        ///// Set the correct Atr image
        ///// </summary>
        //internal void AtrStateImage ()
        //{
        //    if (Module.AtrIsOn)
        //        this.pictureATR.Image = R.Tda5005_ATR_On;
        //    else

        //        this.pictureATR.Image = R.Tda5005_ATR_OFF;
        //}

        //private void button_All_Click_1(object sender, EventArgs e)
        //{
        //    this.Module.DistanceWanted = true;
        //    this.Module.GotoWanted = false;
        //    TryAction(LaunchMeasurement);
        //}

        //private void LaunchMeasurement()
        //{
        //    Result r = new Result();
        //    //if (this.Module._ToBeMeasureTheodoliteData == null) throw new Exception("Nothing to measure");
        //    int factor;
        //    if (this.Module._ToBeMeasureTheodoliteData != null)
        //    {
        //        factor = this.Module._ToBeMeasureTheodoliteData.NumberOfMeasureToAverage;
        //        if (Module._ToBeMeasureTheodoliteData.Face == I.FaceType.DoubleFace) factor *= 2;
        //    }
        //    else
        //    {
        //        factor = 6;
        //    }

        //    TsuTask task = new TsuTask()
        //    {
        //        ActionName = "Measuring...",

        //        estimatedTimeInMilliSec = 7000 * factor,
        //        timeOutInSec = 0 * factor,
        //        preAction = () =>
        //        {
        //        },
        //        mainAction = () =>
        //        {
        //            r = this.Module.Measure();
        //        },
        //        cancelAction = () =>
        //        {
        //            this.Module.Cancel();
        //        },
        //        postAction = () =>
        //        {
        //            if (r.Success)
        //            {
        //                this.Module.ReportMeasurement();
        //                this.Module.FinishMeasurement();
        //            }
        //            this.Module._BeingMeasured = null;
        //            this.Module.DistanceWanted = true; // by default sot that measure() from polar Module take all.
        //            this.Module.InvokeOnApplicationDispatcher(this.AtrStateImage);
        //            this.Module.InvokeOnApplicationDispatcher(this.RedLaserStateImage);
        //        }
        //    };
        //    RunTasksWithWaitingMessage(task);
        //}

        //private void labelMeteo_Click(object sender, EventArgs e)
        //{
        //    // Change values
        //    this.Module.SetMeteoManually();
        //    this.UpdateLabelMeteo();
        //}
        //private void UpdateLabelMeteo()
        //{
        //    labelMeteo.Text = string.Format("T° {0}C\r\nP {1}hPa\r\nH {2}%",
        //        this.Module.temperature.ToString("F1", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")),
        //        this.Module.pressure.ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")),
        //        this.Module.humidity.ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
        //    if (this.Module.MeteoSet) labelMeteo.ForeColor = Tsunami2.Preferences.Theme._Colors.Good;
        //}
        //private void buttonGoto_Click(object sender, EventArgs e)
        //{
        //    this.Goto();
        //}
        ///// <summary>
        ///// Do the goto to next position
        ///// </summary>
        //private void Goto()
        //{
        //    Result r = new Result();
        //    TsuTask task = new TsuTask()
        //    {
        //        ActionName = "Goto...",

        //        estimatedTimeInMilliSec = 7000,
        //        timeOutInSec = 0,//*30 * factor*,
        //        preAction = () =>
        //        {
        //        },
        //        mainAction = () =>
        //        {
        //            this.Module.Goto();
        //            this.Module.DistanceWanted = true;
        //            r.MergeString(this.Module.tempResult);
        //        },
        //        cancelAction = () =>
        //        {
        //            this.Module.Cancel();
        //        },
        //        postAction = () =>
        //        {
        //            Logs.Log.AddEntryAsResult(this.Module, this.Module.tempResult);
        //        }
        //    };
        //    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Goto");
        //}
        //internal void ShowReflectorLess()
        //{
        //    this.pictureATR.Image = R.TRCA_Reflectorless;
        //}

        //internal void ShowWithReflector()
        //{
        //    if (Module.AtrIsOn)

        //        this.pictureATR.Image = R.Tda5005_ATR_On;
        //    else
        //        this.pictureATR.Image = R.Tda5005_ATR_OFF;
        //}

        //private void buttonCOM_Click(object sender, EventArgs e)
        //{
        //    TryAction(ChangeToNextAvailableComPort);
        //}

        //private void ChangeToNextAvailableComPort()
        //{
        //    if (Module._InstrumentIsOn) throw new Exception(R.T_INSTRUMENT_IS_ON);
        //    List<string> coms = System.IO.Ports.SerialPort.GetPortNames().ToList();
        //    int index = coms.FindIndex(x => x == _Tcra1100._PortCom);

        //    index++;
        //    if (index > coms.Count - 1)
        //        index = 0;

        //    _Tcra1100._PortCom = coms[index];
        //    System.IO.Ports.SerialPort port = new System.IO.Ports.SerialPort(_Tcra1100._PortCom);
        //    port.Close();
        //    buttonCOM.Text = _Tcra1100._PortCom;
        //}

        //private void buttonGoAll_Click(object sender, EventArgs e)
        //{
        //    //this.Module.DistanceWanted = true;
        //    //this.Module.GotoWanted = true;
        //    //TryAction(LaunchMeasurement);
        //    this.GotoAll();
        //}
        ///// <summary>
        ///// Do the goto to next position
        ///// </summary>
        //private void GotoAll()
        //{
        //    Result r = new Result();
        //    int factor = this.Module._ToBeMeasureTheodoliteData.NumberOfMeasureToAverage;
        //    if (Module._ToBeMeasureTheodoliteData.Face == I.FaceType.DoubleFace) factor *= 2;
        //    factor++;
        //    TsuTask task = new TsuTask()
        //    {
        //        ActionName = "GotoAll...",

        //        estimatedTimeInMilliSec = 7000 * factor,
        //        timeOutInSec = 0,//*30 * factor*,
        //        preAction = () =>
        //        {
        //        },
        //        mainAction = () =>
        //        {
        //            this.Module.Goto();
        //            this.Module.DistanceWanted = true;
        //            r.MergeString(this.Module.tempResult);
        //            if (r.Success) { r.MergeString(this.Module.Measure()); }
        //        },
        //        cancelAction = () =>
        //        {
        //            this.Module.Cancel();
        //        },
        //        postAction = () =>
        //        {
        //            this.Module._BeingMeasured = null;
        //            this.Module.DistanceWanted = true; // by default sot that measure() from polar Module take all.
        //            if (r.Success)
        //            {
        //                this.Module.ReportMeasurement();
        //            }
        //            else
        //            {
        //                Logs.Log.AddEntryAsResult(this.Module, this.Module.tempResult);
        //            }
        //        }
        //    };

        //    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Goto and do measurement");
        //}
    }
}
