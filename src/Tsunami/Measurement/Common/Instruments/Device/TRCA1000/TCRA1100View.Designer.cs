﻿using R = TSU.Properties.Resources;


namespace TSU.Common.Instruments.Device.TCRA1100
{
    partial class View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (this.Module == null)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.comboBox_PortCom = new System.Windows.Forms.ComboBox();
            this.textBox_numberOfMeas = new System.Windows.Forms.TextBox();
            this.button_All = new System.Windows.Forms.Button();
            this.Button_OnOff = new System.Windows.Forms.Button();
            this.label_ComPort = new System.Windows.Forms.Label();
            this.label_numberOfMeas = new System.Windows.Forms.Label();
            this.button_Rec = new System.Windows.Forms.Button();
            this.button_Distance = new System.Windows.Forms.Button();
            this.buttonGoto = new System.Windows.Forms.Button();
            this.buttonGotoAll = new System.Windows.Forms.Button();
            this.pictureBox_OnOff = new System.Windows.Forms.PictureBox();
            this.labelMeteo = new System.Windows.Forms.Label();
            this.pictureBox_Battery = new System.Windows.Forms.PictureBox();
            this.button_face = new System.Windows.Forms.Button();
            this.button_ATR = new System.Windows.Forms.Button();
            this.button_Laser = new System.Windows.Forms.Button();
            this.button_TargetType = new System.Windows.Forms.Button();
            this.label_Bulle = new System.Windows.Forms.Label();
            this.pictureBox_bulle = new System.Windows.Forms.PictureBox();
            this.Timer_check = new System.Windows.Forms.Timer(this.components);
            this.timer_ComPort = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_OnOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Battery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_bulle)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.label1.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Medium", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 34);
            this.label1.TabIndex = 2;
            this.label1.Text = "TCRA1101";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.comboBox_PortCom);
            this.panel1.Controls.Add(this.textBox_numberOfMeas);
            this.panel1.Controls.Add(this.button_All);
            this.panel1.Controls.Add(this.Button_OnOff);
            this.panel1.Controls.Add(this.label_ComPort);
            this.panel1.Controls.Add(this.label_numberOfMeas);
            this.panel1.Controls.Add(this.button_Rec);
            this.panel1.Controls.Add(this.button_Distance);
            this.panel1.Controls.Add(this.buttonGoto);
            this.panel1.Controls.Add(this.buttonGotoAll);
            this.panel1.Location = new System.Drawing.Point(8, 54);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(632, 182);
            this.panel1.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.AutoScroll = true;
            this.panel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel2.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(624, 119);
            this.panel2.TabIndex = 2;
            // 
            // comboBox_PortCom
            // 
            this.comboBox_PortCom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_PortCom.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.comboBox_PortCom.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.comboBox_PortCom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_PortCom.FormattingEnabled = true;
            this.comboBox_PortCom.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4"});
            this.comboBox_PortCom.Location = new System.Drawing.Point(489, 152);
            this.comboBox_PortCom.MaxDropDownItems = 4;
            this.comboBox_PortCom.Name = "comboBox_PortCom";
            this.comboBox_PortCom.Size = new System.Drawing.Size(71, 21);
            this.comboBox_PortCom.TabIndex = 36;
            // 
            // textBox_numberOfMeas
            // 
            this.textBox_numberOfMeas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_numberOfMeas.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.textBox_numberOfMeas.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.textBox_numberOfMeas.Location = new System.Drawing.Point(534, 130);
            this.textBox_numberOfMeas.Name = "textBox_numberOfMeas";
            this.textBox_numberOfMeas.Size = new System.Drawing.Size(26, 20);
            this.textBox_numberOfMeas.TabIndex = 37;
            this.textBox_numberOfMeas.Text = "1";
            this.textBox_numberOfMeas.TextChanged += new System.EventHandler(this.textBox_numberOfMeas_TextChanged);
            // 
            // button_All
            // 
            this.button_All.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_All.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_All.BackgroundImage = global::TSU.Properties.Resources.Tda5005_ALL;
            this.button_All.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_All.Location = new System.Drawing.Point(3, 128);
            this.button_All.Name = "button_All";
            this.button_All.Size = new System.Drawing.Size(69, 46);
            this.button_All.TabIndex = 38;
            this.button_All.UseVisualStyleBackColor = true;
            this.button_All.Click += new System.EventHandler(this.button_All_Click);
            // 
            // Button_OnOff
            // 
            this.Button_OnOff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_OnOff.BackgroundImage = global::TSU.Properties.Resources.Tda5005_OnOff;
            this.Button_OnOff.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.Button_OnOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_OnOff.Location = new System.Drawing.Point(563, 129);
            this.Button_OnOff.Name = "Button_OnOff";
            this.Button_OnOff.Size = new System.Drawing.Size(64, 46);
            this.Button_OnOff.TabIndex = 33;
            this.Button_OnOff.UseVisualStyleBackColor = true;
            this.Button_OnOff.Click += new System.EventHandler(this.Button_OnOff_Click);
            // 
            // label_ComPort
            // 
            this.label_ComPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label_ComPort.AutoSize = true;
            this.label_ComPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ComPort.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.label_ComPort.Location = new System.Drawing.Point(482, 130);
            this.label_ComPort.Name = "label_ComPort";
            this.label_ComPort.Size = new System.Drawing.Size(75, 20);
            this.label_ComPort.TabIndex = 35;
            this.label_ComPort.Text = "Com Port";
            // 
            // label_numberOfMeas
            // 
            this.label_numberOfMeas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label_numberOfMeas.AutoSize = true;
            this.label_numberOfMeas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_numberOfMeas.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.label_numberOfMeas.Location = new System.Drawing.Point(445, 130);
            this.label_numberOfMeas.Name = "label_numberOfMeas";
            this.label_numberOfMeas.Size = new System.Drawing.Size(83, 20);
            this.label_numberOfMeas.TabIndex = 34;
            this.label_numberOfMeas.Text = "# of meas.";
            // 
            // button_Rec
            // 
            this.button_Rec.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_Rec.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_Rec.BackgroundImage = global::TSU.Properties.Resources.Tda5005_HzV;
            this.button_Rec.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Rec.Location = new System.Drawing.Point(78, 128);
            this.button_Rec.Name = "button_Rec";
            this.button_Rec.Size = new System.Drawing.Size(69, 46);
            this.button_Rec.TabIndex = 39;
            this.button_Rec.UseVisualStyleBackColor = false;
            this.button_Rec.Click += new System.EventHandler(this.button_Rec_Click);
            // 
            // button_Distance
            // 
            this.button_Distance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_Distance.BackgroundImage = global::TSU.Properties.Resources.Tda5005_Dist;
            this.button_Distance.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_Distance.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Distance.Location = new System.Drawing.Point(303, 128);
            this.button_Distance.Name = "button_Distance";
            this.button_Distance.Size = new System.Drawing.Size(69, 47);
            this.button_Distance.TabIndex = 42;
            this.button_Distance.UseVisualStyleBackColor = true;
            this.button_Distance.Click += new System.EventHandler(this.button_Distance_Click);
            // 
            // buttonGoto
            // 
            this.buttonGoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonGoto.BackgroundImage = global::TSU.Properties.Resources.Tda5005_Goto;
            this.buttonGoto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonGoto.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonGoto.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.buttonGoto.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.buttonGoto.Location = new System.Drawing.Point(153, 128);
            this.buttonGoto.Name = "buttonGoto";
            this.buttonGoto.Size = new System.Drawing.Size(69, 47);
            this.buttonGoto.TabIndex = 40;
            this.buttonGoto.UseVisualStyleBackColor = true;
            this.buttonGoto.Click += new System.EventHandler(this.buttonGoto_Click);
            // 
            // buttonGotoAll
            // 
            this.buttonGotoAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonGotoAll.BackgroundImage = global::TSU.Properties.Resources.Tda5005_Goto_All;
            this.buttonGotoAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonGotoAll.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonGotoAll.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.buttonGotoAll.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.buttonGotoAll.Location = new System.Drawing.Point(228, 127);
            this.buttonGotoAll.Name = "buttonGotoAll";
            this.buttonGotoAll.Size = new System.Drawing.Size(69, 47);
            this.buttonGotoAll.TabIndex = 41;
            this.buttonGotoAll.UseVisualStyleBackColor = true;
            this.buttonGotoAll.Click += new System.EventHandler(this.buttonGotoAll_Click);
            // 
            // pictureBox_OnOff
            // 
            this.pictureBox_OnOff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox_OnOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox_OnOff.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.pictureBox_OnOff.Image = global::TSU.Properties.Resources.Tda5005_OFF;
            this.pictureBox_OnOff.Location = new System.Drawing.Point(596, 2);
            this.pictureBox_OnOff.Name = "pictureBox_OnOff";
            this.pictureBox_OnOff.Size = new System.Drawing.Size(44, 42);
            this.pictureBox_OnOff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_OnOff.TabIndex = 37;
            this.pictureBox_OnOff.TabStop = false;
            // 
            // labelMeteo
            // 
            this.labelMeteo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMeteo.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
            this.labelMeteo.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.labelMeteo.Location = new System.Drawing.Point(494, 4);
            this.labelMeteo.Name = "labelMeteo";
            this.labelMeteo.Size = new System.Drawing.Size(71, 41);
            this.labelMeteo.TabIndex = 38;
            this.labelMeteo.Text = "T° 20.0C\r\nP 980hPa\r\nH 60%";
            this.labelMeteo.Click += new System.EventHandler(this.labelMeteo_Click);
            // 
            // pictureBox_Battery
            // 
            this.pictureBox_Battery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox_Battery.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox_Battery.Image = global::TSU.Properties.Resources.TS60_Battery_Bad;
            this.pictureBox_Battery.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.pictureBox_Battery.Location = new System.Drawing.Point(571, 4);
            this.pictureBox_Battery.Name = "pictureBox_Battery";
            this.pictureBox_Battery.Size = new System.Drawing.Size(19, 40);
            this.pictureBox_Battery.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Battery.TabIndex = 39;
            this.pictureBox_Battery.TabStop = false;
            // 
            // button_face
            // 
            this.button_face.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_face.FlatAppearance.BorderSize = 0;
            this.button_face.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_face.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_face.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_face.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_face.Image = global::TSU.Properties.Resources.TS60_CercleGauche;
            this.button_face.Location = new System.Drawing.Point(137, 4);
            this.button_face.Name = "button_face";
            this.button_face.Size = new System.Drawing.Size(40, 40);
            this.button_face.TabIndex = 40;
            this.button_face.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button_face.UseVisualStyleBackColor = true;
            this.button_face.Click += new System.EventHandler(this.button_face_Click);
            // 
            // button_ATR
            // 
            this.button_ATR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_ATR.FlatAppearance.BorderSize = 0;
            this.button_ATR.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_ATR.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_ATR.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_ATR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_ATR.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_ATR.Image = global::TSU.Properties.Resources.TS60_LOCK_On;
            this.button_ATR.Location = new System.Drawing.Point(183, 4);
            this.button_ATR.Name = "button_ATR";
            this.button_ATR.Size = new System.Drawing.Size(40, 40);
            this.button_ATR.TabIndex = 15;
            this.button_ATR.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_ATR.UseVisualStyleBackColor = false;
            this.button_ATR.Click += new System.EventHandler(this.button_ATR_Click);
            // 
            // button_Laser
            // 
            this.button_Laser.BackgroundImage = global::TSU.Properties.Resources.TS60_Laser_Off;
            this.button_Laser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Laser.FlatAppearance.BorderSize = 0;
            this.button_Laser.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_Laser.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_Laser.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_Laser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Laser.Location = new System.Drawing.Point(229, 4);
            this.button_Laser.Name = "button_Laser";
            this.button_Laser.Size = new System.Drawing.Size(40, 40);
            this.button_Laser.TabIndex = 32;
            this.button_Laser.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button_Laser.UseVisualStyleBackColor = true;
            this.button_Laser.Click += new System.EventHandler(this.button_Laser_Click);
            // 
            // button_TargetType
            // 
            this.button_TargetType.BackgroundImage = global::TSU.Properties.Resources.TS60_Unknown_Target;
            this.button_TargetType.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_TargetType.FlatAppearance.BorderSize = 0;
            this.button_TargetType.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_TargetType.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_TargetType.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_TargetType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_TargetType.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_TargetType.Location = new System.Drawing.Point(275, 3);
            this.button_TargetType.Name = "button_TargetType";
            this.button_TargetType.Size = new System.Drawing.Size(40, 40);
            this.button_TargetType.TabIndex = 41;
            this.button_TargetType.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_TargetType.UseVisualStyleBackColor = false;
            this.button_TargetType.Click += new System.EventHandler(this.button_TargetType_Click);
            // 
            // label_Bulle
            // 
            this.label_Bulle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_Bulle.AutoSize = true;
            this.label_Bulle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Bulle.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.label_Bulle.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.label_Bulle.Location = new System.Drawing.Point(411, 2);
            this.label_Bulle.Name = "label_Bulle";
            this.label_Bulle.Size = new System.Drawing.Size(26, 13);
            this.label_Bulle.TabIndex = 22;
            this.label_Bulle.Text = "L & T";
            // 
            // pictureBox_bulle
            // 
            this.pictureBox_bulle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox_bulle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox_bulle.Image = global::TSU.Properties.Resources.Tda5005_Bulle_Bad;
            this.pictureBox_bulle.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.pictureBox_bulle.Location = new System.Drawing.Point(363, 3);
            this.pictureBox_bulle.Name = "pictureBox_bulle";
            this.pictureBox_bulle.Size = new System.Drawing.Size(42, 40);
            this.pictureBox_bulle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_bulle.TabIndex = 21;
            this.pictureBox_bulle.TabStop = false;
            // 
            // Timer_check
            // 
            this.Timer_check.Interval = 20000;
            this.Timer_check.Tick += new System.EventHandler(this.timer_Check_Tick);
            // 
            // timer_ComPort
            // 
            this.timer_ComPort.Interval = 2000;
            this.timer_ComPort.Tick += new System.EventHandler(this.timer_ComPort_Tick);
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.ClientSize = new System.Drawing.Size(643, 235);
            this.Controls.Add(this.label_Bulle);
            this.Controls.Add(this.button_TargetType);
            this.Controls.Add(this.pictureBox_bulle);
            this.Controls.Add(this.button_Laser);
            this.Controls.Add(this.button_ATR);
            this.Controls.Add(this.button_face);
            this.Controls.Add(this.pictureBox_Battery);
            this.Controls.Add(this.labelMeteo);
            this.Controls.Add(this.pictureBox_OnOff);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.Name = "View";
            this.Text = "TCRA1100View";
            this.Load += new System.EventHandler(this.Tcra1100View_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_OnOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Battery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_bulle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox_OnOff;
        private System.Windows.Forms.Label labelMeteo;
        private System.Windows.Forms.PictureBox pictureBox_Battery;
        private System.Windows.Forms.Button button_face;
        private System.Windows.Forms.Button button_ATR;
        private System.Windows.Forms.Button button_Laser;
        private System.Windows.Forms.Button button_TargetType;
        private System.Windows.Forms.Label label_Bulle;
        private System.Windows.Forms.PictureBox pictureBox_bulle;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button_Distance;
        private System.Windows.Forms.Button Button_OnOff;
        private System.Windows.Forms.ComboBox comboBox_PortCom;
        public System.Windows.Forms.TextBox textBox_numberOfMeas;
        private System.Windows.Forms.Label label_ComPort;
        private System.Windows.Forms.Button button_All;
        private System.Windows.Forms.Button buttonGotoAll;
        private System.Windows.Forms.Label label_numberOfMeas;
        private System.Windows.Forms.Button button_Rec;
        private System.Windows.Forms.Button buttonGoto;
        private System.Windows.Forms.Timer Timer_check;
        private System.Windows.Forms.Timer timer_ComPort;
    }
}