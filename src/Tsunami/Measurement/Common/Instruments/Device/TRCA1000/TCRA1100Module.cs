using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using TSU.Common.Elements;
using TSU.Common.Instruments.Adapter;
using TSU.Views.Message;
using M = TSU.Common.Measures;
using MMMM = TSU;
using P = TSU.Views.Message.ProgressMessage;
using R = TSU.Properties.Resources;


namespace TSU.Common.Instruments.Device.TCRA1100
{

    [XmlType(TypeName = "TCRA1100.Module")]
    public class Module : PolarModule, IConnectable, IPolarModule, IMotorized
    {

        public Instrument Tcra1100
        {
            get
            {
                return this.Instrument as Instrument;
            }
        }

        [XmlIgnore]
        public Result tempResult = new Result();
        [XmlIgnore]
        internal List<string> listPortName;
        [XmlIgnore]
        private int listPortIndex;
        [XmlIgnore]
        private InstrumentTypes previousReflector = InstrumentTypes.CCR1_5;
        [XmlIgnore]
        public override bool InstrumentIsOn { get => isConnected; set => isConnected = value; }
        [XmlIgnore]
        bool isConnected;
        [XmlIgnore]
        public TSU.Polar.Station.Module StationTheodoliteModule
        {
            get
            {
                return (((this.ParentModule as Manager.Module).ParentModule) as TSU.Polar.Station.Module);
            }
        }
        [XmlIgnore]
        public new Device.TCRA1100.View View
        {
            get
            {
                return this._TsuView as Device.TCRA1100.View;
            }
            set
            {
                this._TsuView = value;
            }
        }
        //public bool allowGoto { get; set; }

        #region Constructeur TCRA1100

        public Module() : base() { }


        public Module(MMMM.Module parentModule, Instruments.Instrument i)
            : base(parentModule)
        {
            this.Instrument = i;

        }

        public override void Initialize()
        {
            base.Initialize();
        }
        #endregion

        #region private Method TCRA1100
        private string ErrorCode(short errorCode) //converti le num�ro de l'erreur renvoy� par la dll en message d'erreur pour le logbook
        {
            string message = "";
            switch (errorCode)
            {
                case 0:
                    message = "";
                    break;
                case 1:
                    message = "GRC_UNDEFINED: Unknown error, result unspecified.";
                    break;
                case 2:
                    message = "GRC_IVPARAM: Invalid parameter detected, Result Unspecified.";
                    break;
                case 3:
                    message = "GRC_IVRESULT: Invalid result.";
                    break;
                case 4:
                    message = "GRC_FATAL: Fatal error.";
                    break;
                case 5:
                    message = "GRC_NOT_IMPL: Not yet implemented.";
                    break;
                case 6:
                    message = "GRC_TIME_OUT: Function execution timed out. Result unspecified.";
                    break;
                case 7:
                    message = "GRC_SET_INCOMPL: Parameter setup for subsystem is incomplete.";
                    break;
                case 8:
                    message = "GRC_ABORT: Function execution has been aborted.";
                    break;
                case 9:
                    message = "GRC_NOMEMORY: Fatal error - not enough memory.";
                    break;
                case 10:
                    message = "GRC_NOINIT: Fatal error - subsystem not initialised.";
                    break;
                case 12:
                    message = "GRC_SHUTDOWN: Subsystem is down.";
                    break;
                case 13:
                    message = "GRC_SYSBUSY: System busy/already in use of another process. Cannot execute function.";
                    break;
                case 14:
                    message = "GRC_RC_HWFAILURE: Fatal error - hardware failure.";
                    break;
                case 15:
                    message = "GRC_RC_ABORT_APPL: Execution of application has been aborted (SHIFT-ESC).";
                    break;
                case 16:
                    message = "GRC_RC_LOW_POWER: Operation aborted - insufficient power supply level.";
                    break;
                case 17:
                    message = " GRC_IVVERSION Invalid version of file, ... ";
                    break;
                case 18:
                    message = " GRC_BATT_EMPTY Battery empty ";
                    break;
                case 20:
                    message = "GRC_NO_EVENT no event pending. ";
                    break;
                case 21:
                    message = "GRC_OUT_OF_TEMP out of temperature range ";
                    break;
                case 22:
                    message = "GRC_INSTRUMENT_TILT instrument tilting out of range";
                    break;
                case 23:
                    message = "GRC_COM_SETTING communication error ";
                    break;
                case 24:
                    message = "GRC_NO_ACTION GRC_TYPE Input 'do no action' ";
                    break;
                case 25:
                    message = "GRC_SLEEP_MODE Instr. run into the sleep mode ";
                    break;
                case 26:
                    message = "GRC_NOTOK Function not successfully completed. ";
                    break;
                case 27:
                    message = "GRC_NA: Not available ";
                    break;
                case 28:
                    message = "GRC_OVERFLOW: Overflow error ";
                    break;
                case 29:
                    message = "GRC_STOPPED System or subsystem has been stopped ";
                    break;
                case 257:
                    message = "GRC_ANG_ERROR: Angles and Inclinations not valid.";
                    break;
                case 258:
                    message = "GRC_ANG_INCL_ERROR: Inclinations not valid.";
                    break;
                case 259:
                    message = "GRC_ANG_BAD_ACC: value accuracy not reached.";
                    break;
                case 260:
                    message = "GRC_ANG_BAD_ANGLE_ACC: angle accuracy not reached.";
                    break;
                case 261:
                    message = "GRC_ANG_BAD_INCLIN_ACC: Inclination accuracy not reached.";
                    break;
                case 266:
                    message = "GRC_ANG_WRITE_PROTECTED: No write access allowed.";
                    break;
                case 267:
                    message = "GRC_ANG_OUT_OF_RANGE: Value out of range.";
                    break;
                case 268:
                    message = "GRC_ANG_IR_OCCURED: Function aborted due to interrupt.";
                    break;
                case 269:
                    message = "GRC_ANG_HZ_MOVED: Hz moved during incline measurement.";
                    break;
                case 270:
                    message = "GRC_ANG_OS_ERROR: Troubles with operation system";
                    break;
                case 271:
                    message = "GRC_ANG_DATA_ERROR: Overflow at parameter values.";
                    break;
                case 272:
                    message = "GRC_ANG_PEAK_CNT_UFL: Too less peak.";
                    break;
                case 273:
                    message = "GRC_GRC_ANG_TIMEOUT: reading timeout.";
                    break;
                case 274:
                    message = "GRC_ANG_TOO_MANY_EXPOS: Too many exposures wanted.";
                    break;
                case 275:
                    message = "GRC_ANG_PIX_CTRL_ERR: Picture height out of range.";
                    break;
                case 276:
                    message = "GRC_ANG_MAX_POS_SKIP: Positive exposure dynamic overflow.";
                    break;
                case 277:
                    message = "GRC_ANG_MAX_NEG_SKIP: Negative exposure dynamic overflow.";
                    break;
                case 278:
                    message = "GRC_ANG_EXP_LIMIT: Exposure time overflow.";
                    break;
                case 279:
                    message = "GRC_ANG_UNDER_EXPOSURE: picture underexposured.";
                    break;
                case 280:
                    message = "GRC_ANG_OVER_EXPOSURE: picture overexposured.";
                    break;
                case 300:
                    message = "GRC_ANG_TMANY_PEAKS: too many peaks detected.";
                    break;
                case 301:
                    message = "GRC_ANG_TLESS_PEAKS: too less peaks detected.";
                    break;
                case 302:
                    message = "GRC_ANG_PEAK_TOO_SLIM: peak too slim.";
                    break;
                case 303:
                    message = "GRC_ANG_PEAK_TOO_WIDE: peak too wide.";
                    break;
                case 304:
                    message = "GRC_ANG_BAD_PEAKDIFF: bad peak difference.";
                    break;
                case 305:
                    message = "GRC_ANG_UNDER_EXP_PICT: too less peak amplitude.";
                    break;
                case 306:
                    message = "GRC_ANG_PEAK_INHOMOGEN: inhomogen peak amplitudes.";
                    break;
                case 307:
                    message = "GRC_ANG_NO_DECOD_POSS: no peak decoding possible.";
                    break;
                case 308:
                    message = "GRC_ANG_UNSTABLE_DECOD: peak decoding not stable.";
                    break;
                case 309:
                    message = "GRC_ANG_TLESS_FPEAKS: too less valid finepeak.";
                    break;
                case 512:
                    message = "GRC_ATA_NOT_READY ATR-System: is not ready.";
                    break;
                case 513:
                    message = "GRC_ATA_NO_RESULT: Result isn't available yet.";
                    break;
                case 514:
                    message = "GRC_ATA_SEVERAL_TARGETS: Several Targets detected.";
                    break;
                case 515:
                    message = "GRC_ATA_BIG_SPOT: Spot is too big for analyse.";
                    break;
                case 516:
                    message = "GRC_ATA_BACKGROUND: Background is too bright.";
                    break;
                case 517:
                    message = "GRC_ATA_NO_TARGETS: No targets detected.";
                    break;
                case 518:
                    message = "GRC_ATA_NOT_ACCURAT: Accuracy worse than asked for.";
                    break;
                case 519:
                    message = "GRC_ATA_SPOT_ON_EDGE: Spot is on the edge of the sensing area.";
                    break;
                case 522:
                    message = "GRC_ATA_BLOOMING: Blooming or spot on edge detected.";
                    break;
                case 523:
                    message = "GRC_ATA_NOT_BUSY ATR: isn't in a continuous mode.";
                    break;
                case 524:
                    message = "GRC_ATA_STRANGE_LIGHT: Not the spot of the own target illuminator.";
                    break;
                case 525:
                    message = "GRC_ATA_V24_FAIL: Communication error to sensor (ATR).";
                    break;
                case 526:
                    message = "GRC_ATA_DECODE_ERROR: Received Arguments cannot be decoded";
                    break;
                case 527:
                    message = "GRC_ATA_HZ_FAIL: No Spot detected in Hz-direction.";
                    break;
                case 528:
                    message = "GRC_ATA_V_FAIL: No Spot detected in V-direction.";
                    break;
                case 529:
                    message = "GRC_ATA_HZ_STRANGE_L: Strange light in Hz-direction.";
                    break;
                case 530:
                    message = "GRC_ATA_V_STRANGE_L: Strange light in V-direction.";
                    break;
                case 531:
                    message = "GRC_ATA_SLDR_TRANSFER_PENDING: On multiple ATA_SLDR_OpenTransfer.";
                    break;
                case 532:
                    message = "GRC_ATA_SLDR_TRANSFER_ILLEGAL: No ATA_SLDR_OpenTransfer happened.";
                    break;
                case 533:
                    message = "GRC_ATA_SLDR_DATA_ERROR: Unexpected data format received.";
                    break;
                case 534:
                    message = "GRC_ATA_SLDR_CHK_SUM_ERROR: Checksum error in transmitted data.";
                    break;
                case 535:
                    message = "GRC_ATA_SLDR_ADDRESS_ERROR: Address out of valid range.";
                    break;
                case 536:
                    message = "GRC_ATA_SLDR_INV_LOADFILE: Firmware file has invalid format.";
                    break;
                case 537:
                    message = "GRC_ATA_SLDR_UNSUPPORTED: Current (loaded) firmware doesn't support upload.";
                    break;
                case 538:
                    message = "GRC_ATA_PS_NOT_READY: PS-System is not ready.";
                    break;
                case 539:
                    message = "GRC_ATA_ATR_SYSTEM_ERR: ATR system error";
                    break;
                case 769:
                    message = "GRC_EDM_SYSTEM_ERR: Fatal EDM sensor error. See for the exact reason the original EDM sensor error number. In the most cases a service problem. ";
                    break;
                case 770:
                    message = "GRC_EDM_INVALID_COMMAND: Invalid command or unknown command, see command syntax. ";
                    break;
                case 771:
                    message = "GRC_EDM_BOOM_ERR: Boomerang error. ";
                    break;
                case 772:
                    message = "GRC_EDM_SIGN_LOW_ERR: Received signal to low, prism to far away, or natural barrier, bad environment, etc. ";
                    break;
                case 773:
                    message = "GRC_EDM_DIL_ERR: obsolete ";
                    break;
                case 774:
                    message = "GRC_EDM_SIGN_HIGH_ERR:Received signal to strong, prism to near, stranger light effect. ";
                    break;
                case 775:
                    message = "GRC_EDM_TIMEOUT: Timeout, measuring time exceeded (signal too weak, beam interrupted,..) ";
                    break;
                case 776:
                    message = "GRC_EDM_FLUKT_ERR:t o much turbulences or distractions ";
                    break;
                case 777:
                    message = "GRC_EDM_FMOT_ERR: filter motor defective ";
                    break;
                case 778:
                    message = "GRC_EDM_DEV_NOT_INSTALLED: Device like EGL, DL is not installed. ";
                    break;
                case 779:
                    message = "GRC_EDM_NOT_FOUND: Search result invalid. For the exact explanation, see in the description of the called function. ";
                    break;
                case 780:
                    message = "GRC_EDM_ERROR_RECEIVED: Communication ok, but an error reported from the EDM sensor. ";
                    break;
                case 781:
                    message = "GRC_EDM_MISSING_SRVPWD: No service password is set. ";
                    break;
                case 782:
                    message = "GRC_EDM_INVALID_ANSWER: Communication ok, but an unexpected answer received. ";
                    break;
                case 783:
                    message = "GRC_EDM_SEND_ERR: Data send error, sending buffer is full. ";
                    break;
                case 784:
                    message = "GRC_EDM_RECEIVE_ERR: Data receive error, like parity buffer overflow. ";
                    break;
                case 785:
                    message = "GRC_EDM_INTERNAL_ERR: Internal EDM subsystem error. ";
                    break;
                case 786:
                    message = "GRC_EDM_BUSY: Sensor is working already, abort current measuring first. ";
                    break;
                case 787:
                    message = "GRC_EDM_NO_MEASACTIVITY: No measurement activity started. ";
                    break;
                case 788:
                    message = "GRC_EDM_CHKSUM_ERR: Calculated checksum, resp. received data wrong (only in binary communication mode possible). ";
                    break;
                case 789:
                    message = "GRC_EDM_INIT_OR_STOP_ERR: During start up or shut down phase an error occured. It is saved in the DEL buffer. ";
                    break;
                case 790:
                    message = "GRC_EDM_SRL_NOT_AVAILABLE: Red laser not available on this sensor HW. ";
                    break;
                case 791:
                    message = "GRC_EDM_MEAS_ABORTED: Measurement will be aborted (will be used for the laser security) ";
                    break;
                case 798:
                    message = "GRC_EDM_SLDR_TRANSFER_PENDING: Multiple OpenTransfer calls. ";
                    break;
                case 799:
                    message = "GRC_EDM_SLDR_TRANSFER_ILLEGAL: No open transfer happened. ";
                    break;
                case 800:
                    message = "GRC_EDM_SLDR_DATA_ERROR: Unexpected data format received. ";
                    break;
                case 801:
                    message = "GRC_EDM_SLDR_CHK_SUM_ERROR: Checksum error in transmitted data. ";
                    break;
                case 802:
                    message = "GRC_EDM_SLDR_ADDR_ERROR: Address out of valid range. ";
                    break;
                case 803:
                    message = "GRC_EDM_SLDR_INV_LOADFILE: Firmware file has invalid format. ";
                    break;
                case 804:
                    message = "GRC_EDM_SLDR_UNSUPPORTED: Current (loaded) firmware doesn't support upload. ";
                    break;
                case 808:
                    message = "GRC_EDM_UNKNOW_ERR: Undocumented error from the EDM sensor, should not occur. ";
                    break;
                case 818:
                    message = "GRC_EDM_DISTRANGE_ERR: Out of distance range (dist too small or large) ";
                    break;
                case 819:
                    message = "GRC_EDM_SIGNTONOISE_ERR: Signal to noise ratio too small ";
                    break;
                case 820:
                    message = "GRC_EDM_NOISEHIGH_ERR: Noise to high ";
                    break;
                case 821:
                    message = "GRC_EDM_PWD_NOTSET: Password is not set ";
                    break;
                case 822:
                    message = "GRC_EDM_ACTION_NO_MORE_VALID: Elapsed time between prepare und start fast measurement for ATR to long ";
                    break;
                case 823:
                    message = "GRC_EDM_MULTRG_ERR: Possibly more than one target (also a sensor error) ";
                    break;
                case 1283:
                    message = "GRC_TMC_NO_FULL_CORRECTION: Warning: measurement without full correction ";
                    break;
                case 1284:
                    message = "GRC_TMC_ACCURACY_GUARANTEE: Info: accuracy can not be guarantee ";
                    break;
                case 1285:
                    message = "GRC_TMC_ANGLE_OK: Warning: only angle measurement valid ";
                    break;
                case 1288:
                    message = "GRC_TMC_ANGLE_NOT_FULL_CORR: Warning: only angle measurement valid but without full correction ";
                    break;
                case 1289:
                    message = "GRC_TMC_ANGLE_NO_ACC_GUARANTY: Info: only angle measurement valid but accuracy can not be guarantee ";
                    break;
                case 1290:
                    message = "GRC_TMC_ANGLE_ERROR: Error: no angle measurement ";
                    break;
                case 1291:
                    message = "GRC_TMC_DIST_PPM: Error: wrong setting of PPM or MM on EDM ";
                    break;
                case 1292:
                    message = "GRC_TMC_DIST_ERROR: Error: distance measurement not done (no aim, etc.) ";
                    break;
                case 1293:
                    message = "GRC_TMC_BUSY: Error: system is busy (no measurement done) ";
                    break;
                case 1294:
                    message = "GRC_TMC_SIGNAL_ERROR: Error: no signal on EDM (only in signal mode) ";
                    break;
                case 2305:
                    message = "GRC_BMM_XFER_PENDING: Loading process already opened ";
                    break;
                case 2306:
                    message = "GRC_BMM_NO_XFER_OPEN: Transfer not opened ";
                    break;
                case 2307:
                    message = "GRC_BMM_UNKNOWN_CHARSET: Unknown character set ";
                    break;
                case 2308:
                    message = "GRC_BMM_NOT_INSTALLED: Display module not present ";
                    break;
                case 2309:
                    message = "GRC_BMM_ALREADY_EXIST: Character set already exists ";
                    break;
                case 2310:
                    message = "GRC_BMM_CANT_DELETE: Character set cannot be deleted ";
                    break;
                case 2311:
                    message = "GRC_BMM_MEM_ERROR: Memory cannot be allocated ";
                    break;
                case 2312:
                    message = "GRC_BMM_CHARSET_USED: Character set still used ";
                    break;
                case 2313:
                    message = "GRC_BMM_CHARSET_SAVED: Charset cannot be deleted or is protected ";
                    break;
                case 2314:
                    message = "GRC_BMM_INVALID_ADR: Attempt to copy a character block outside the allocated memory ";
                    break;
                case 2315:
                    message = "GRC_BMM_CANCELANDADR_ERROR: Error during release of allocated memory ";
                    break;
                case 2316:
                    message = "GRC_BMM_INVALID_SIZE: Number of bytes specified in header does not match the bytes read ";
                    break;
                case 2317:
                    message = "GRC_BMM_CANCELANDINVSIZE_ERROR: Allocated memory could not be released ";
                    break;
                case 2318:
                    message = "GRC_BMM_ALL_GROUP_OCC: Max. number of character sets already loaded ";
                    break;
                case 2319:
                    message = "GRC_BMM_CANT_DEL_LAYERS: Layer cannot be deleted ";
                    break;
                case 2320:
                    message = "GRC_BMM_UNKNOWN_LAYER: Required layer does not exist ";
                    break;
                case 2321:
                    message = "GRC_BMM_INVALID_LAYERLEN: Layer length exceeds maximum ";
                    break;
                case 3072:
                    message = "GRC_COM_ERO: Initiate Extended Runtime Operation (ERO).";
                    break;
                case 3073:
                    message = "GRC_COM_CANT_ENCODE: Cannot encode arguments in client.";
                    break;
                case 3074:
                    message = "GRC_COM_CANT_DECODE:Cannot decode results in client.";
                    break;
                case 3075:
                    message = "GRC_COM_CANT_SEND: Hardware error while sending.";
                    break;
                case 3076:
                    message = "GRC_COM_CANT_RECV: Hardware error while receiving.";
                    break;
                case 3077:
                    message = "GRC_COM_TIMEDOUT: Request timed out.";
                    break;
                case 3078:
                    message = "GRC_COM_WRONG_FORMAT: Packet format error.";
                    break;
                case 3079:
                    message = "GRC_COM_VER_MISMATCH: Version mismatch between client and server.";
                    break;
                case 3080:
                    message = "GRC_COM_CANT_DECODE_REQ: Cannot decode arguments in server.";
                    break;
                case 3081:
                    message = "GRC_COM_PROC_UNAVAIL: Unknown RPC, procedure ID invalid.";
                    break;
                case 3082:
                    message = "GRC_COM_CANT_ENCODE_REP: Cannot encode results in server.";
                    break;
                case 3083:
                    message = "GRC_COM_SYSTEM_ERR: Unspecified generic system error.";
                    break;
                case 3085:
                    message = "GRC_COM_FAILED: Unspecified error.";
                    break;
                case 3086:
                    message = "GRC_COM_NO_BINARY: Binary protocol not available.";
                    break;
                case 3087:
                    message = "GRC_COM_INTR: Call interrupted.";
                    break;
                case 3090:
                    message = "GRC_COM_REQUIRES_8DBITS: Protocol needs 8bit encoded characters.";
                    break;
                case 3093:
                    message = "GRC_COM_TR_ID_MISMATCH: Transaction ID mismatch error.";
                    break;
                case 3094:
                    message = "GRC_COM_NOT_GEOCOM: Protocol not recognisable.";
                    break;
                case 3095:
                    message = "GRC_COM_UNKNOWN_PORT: (WIN) Invalid port address.";
                    break;
                case 3099:
                    message = "GRC_COM_ERO_END: ERO is terminating.";
                    break;
                case 3100:
                    message = "GRC_COM_OVERRUN: Internal error: data buffer overflow.";
                    break;
                case 3101:
                    message = "GRC_COM_SRVR_RX_CHECKSUM_ERR: Invalid checksum on server side received.";
                    break;
                case 3102:
                    message = "GRC_COM_CLNT_RX_CHECKSUM_ERR: Invalid checksum on client side received.";
                    break;
                case 3103:
                    message = "GRC_COM_PORT_NOT_AVAILABLE: (WIN) Port not available.";
                    break;
                case 3104:
                    message = "GRC_COM_PORT_NOT_OPEN: (WIN) Port not opened.";
                    break;
                case 3105:
                    message = "GRC_COM_NO_PARTNER: (WIN) Unable to find TPS.";
                    break;
                case 3106:
                    message = "GRC_COM_ERO_NOT_STARTED: Extended Runtime Operation could not be started.";
                    break;
                case 3107:
                    message = "GRC_COM_CONS_REQ: Attention to send cons requests.";
                    break;
                case 3108:
                    message = "GRC_COM_SRVR_IS_SLEEPING: TPS has gone to sleep. Wait and try again.";
                    break;
                case 3109:
                    message = "GRC_COM_SRVR_IS_OFF: TPS has shut down. Wait and try again.";
                    break;
                case 8704:
                    message = "GRC_AUT_TIMEOUT: Position not reached ";
                    break;
                case 8705:
                    message = "GRC_AUT_DETENT_ERROR: Positioning not possible due to mounted EDM ";
                    break;
                case 8706:
                    message = "GRC_AUT_ANGLE_ERROR: Angle measurement error ";
                    break;
                case 8707:
                    message = "GRC_AUT_MOTOR_ERROR: Motorisation error ";
                    break;
                case 8708:
                    message = "GRC_AUT_INCACC: Position not exactly reached ";
                    break;
                case 8709:
                    message = "GRC_AUT_DEV_ERROR: Deviation measurement error ";
                    break;
                case 8710:
                    message = "GRC_AUT_NO_TARGET: No target detected ";
                    break;
                case 8711:
                    message = "GRC_AUT_MULTIPLE_TARGETS: Multiple target detected ";
                    break;
                case 8712:
                    message = "GRC_AUT_BAD_ENVIRONMENT: Bad environment conditions ";
                    break;
                case 8713:
                    message = "GRC_AUT_DETECTOR_ERROR: Error in target acquisition ";
                    break;
                case 8714:
                    message = "GRC_AUT_NOT_ENABLED: Target acquisition not enabled ";
                    break;
                case 8715:
                    message = "GRC_AUT_CALACC: ATR-Calibration failed ";
                    break;
                case 8716:
                    message = "GRC_AUT_ACCURACY: Target position not exactly reached ";
                    break;
                case 8717:
                    message = "Info: dist. measurement has been started ";
                    break;
                case 8718:
                    message = "GRC_AUT_SUPPLY_TOO_HIGH: external Supply voltage is too high ";
                    break;
                case 8719:
                    message = "GRC_AUT_SUPPLY_TOO_LOW: int. or ext. Supply voltage is too low ";
                    break;
                case 8720:
                    message = "GRC_AUT_NO_WORKING_AREA: working area not set ";
                    break;
                case 8721:
                    message = "GRC_AUT_ARRAY_FULL: power search data array is filled ";
                    break;
                case 8722:
                    message = "GRC_AUT_NO_DATA: no data available ";
                    break;
                case 12544:
                    message = "GRC_KDM_NOT_AVAILABLE KDM device is not available.";
                    break;
                case 13056:
                    message = "GRC_FTR_FILEACCESS: File access error ";
                    break;
                case 13057:
                    message = "GRC_FTR_WRONGFILEBLOCKNUMBER: block number was not the expected one ";
                    break;
                case 13058:
                    message = "GRC_FTR_NOTENOUGHSPACE: not enough space on device to proceed uploading ";
                    break;
                case 13059:
                    message = "GRC_FTR_INVALIDINPUT: Rename of file failed. ";
                    break;
                case 13060:
                    message = "GRC_FTR_MISSINGSETUP: invalid parameter as input ";
                    break;
                default:
                    message = "GRC_Unknown error message";
                    break;
            }
            return message;
        }

        internal Result GetCompensatorReading()
        {
            // seems that the padding of packing is problematic when Inclien structure is received inside the Angle structure, the values are crazy
            // I found that asking for integer only transformed them in array bytes, concat them 2 by 2 and read give the right values...

            GeoCom105_dll.TMC_ANGLE2 TPSAngles = new GeoCom105_dll.TMC_ANGLE2();
            int inclineProg = (int)GeoCom105_dll.TMC_INCLINE_PRG.TMC_AUTO_INC;

            short errorCode = GeoCom105_dll.VB_TMC_GetAngle2(ref TPSAngles, ref inclineProg);

            Result result = new Result();

            if (errorCode == 0)
            {
                result.Success = true;

                // apply trick
                {
                    byte[] binary1 = BitConverter.GetBytes(TPSAngles.Incline._5);
                    byte[] binary2 = BitConverter.GetBytes(TPSAngles.Incline._6);
                    byte[] binary = binary1.Concat(binary2).ToArray();
                    this.Tcra1100._Compensator.tranverseAxisInclination = BitConverter.ToDouble(binary, 0);

                    binary1 = BitConverter.GetBytes(TPSAngles.Incline._7);
                    binary2 = BitConverter.GetBytes(TPSAngles.Incline._8);
                    binary = binary1.Concat(binary2).ToArray();
                    this.Tcra1100._Compensator.longitudinalAxisInclination = BitConverter.ToDouble(binary, 0);

                    binary1 = BitConverter.GetBytes(TPSAngles.Incline._9);
                    binary2 = BitConverter.GetBytes(TPSAngles.Incline._A);
                    binary = binary1.Concat(binary2).ToArray();
                    this.Tcra1100._Compensator.inclinationAccuracy = BitConverter.ToDouble(binary, 0);

                    this.Tcra1100._Compensator.inclineTime = DateTime.Now;
                }

                result.AccessToken = "State Incline switch: " + this.Tcra1100._Compensator.ToString();
            }
            else
            {
                result.Success = false;
                result.ErrorMessage = ErrorCode(errorCode);
                result.AccessToken = "Incline Switch error: " + result.ErrorMessage;
            }
            return result;
        }

        private Result InitializeGeocom() //Initialize Geocom buffers and variables inside GCOM105 dll. Has to be done before opening connection to TDA.
        {
            Result result = new Result();
            try
            {
                short errorCode = GeoCom105_dll.VB_COM_NullProc();
                result.ErrorMessage = ErrorCode(errorCode);

                errorCode = GeoCom105_dll.VB_COM_Init();
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Initialization of GEOCOM OK.";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    //   result.AccessToken = string.Format("Initialization of GEOCOM error: {0}", result.ErrorMessage);
                    result.AccessToken = $"{R.T_INITIALIZATION_OF_GEOCOM_ERROR}: {result.ErrorMessage}";
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                // result.AccessToken = string.Format("Initialization of GEOCOM error: {0}", result.ErrorMessage);
                result.AccessToken = $"{R.T_INITIALIZATION_OF_GEOCOM_ERROR}: {result.ErrorMessage}";
            }
            return result;
        }
        private Result OpenConnection(string portCom) //Connection au TDA en 19200 bauds, choix du port COM1 � 4 � d�finir).
        {
            Result result = new Result();
            try
            {

                int port = 0;
                switch (portCom)
                {
                    case "COM1":
                        port = (int)GeoCom105_dll.COM_PORT.COM_1;
                        break;
                    case "COM2":
                        port = (int)GeoCom105_dll.COM_PORT.COM_2;
                        break;
                    case "COM3":
                        port = (int)GeoCom105_dll.COM_PORT.COM_3;
                        break;
                    case "COM4":
                        port = (int)GeoCom105_dll.COM_PORT.COM_4;
                        break;
                    default:
                        result.Success = false;
                        result.ErrorMessage = "Unknown serial port or serial port number > 4";
                        return result;
                }
                int baudrate = (int)GeoCom105_dll.COM_BAUD_RATE.COM_BAUD_19200;
                GeoCom105_dll.BOOLE falseTrue = GeoCom105_dll.BOOLE.FALSE;
                GeoCom105_dll.VB_COM_SetConnDlgFlag((int)falseTrue);
                short errorCode = GeoCom105_dll.VB_COM_OpenConnection(port, ref baudrate, 2);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = string.Format("Open port {0} OK", portCom);
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = string.Format("Open port {0} error: {1}", portCom, result.ErrorMessage);
                    Logs.Log.AddEntryAsError(this, result.AccessToken.ToString());
                    CloseConnection();
                    EndGeocom();
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = string.Format("Open port {0} error: {1}", portCom, result.ErrorMessage);
                CloseConnection();
                EndGeocom();
            }

            return result;
        }

        private Result CloseConnection() //Fermeture Connection au TCRA 1101.
        {
            Result result = new Result();
            try
            {
                int TCRAOff = (int)GeoCom105_dll.COM_TPS_STOP_MODE.COM_TPS_SHUT_DOWN;
                short errorCode = GeoCom105_dll.VB_COM_SwitchOffTPS(ref TCRAOff);
                errorCode = GeoCom105_dll.VB_COM_CloseConnection();
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Close COM port OK";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = string.Format("Close COM port error: {0}", result.ErrorMessage);
                    EndGeocom();
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = string.Format("Close COM port error: {0}", result.ErrorMessage);
                EndGeocom();
            }
            return result;
        }


        private Result SetInclineSwitchOn() //Met sur ON InclineSwitch
        {
            Result result = new Result();
            try
            {
                int inclineSwitch = (int)GeoCom105_dll.ON_OFF_TYPE.ON;
                short errorCode = GeoCom105_dll.VB_TMC_SetInclineSwitch(ref inclineSwitch);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Turned Incline Switch ON.";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Incline Switch error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Incline Switch error: " + result.ErrorMessage;
            }
            return result;
        }

        //public Result GetInclineSwitchStateFomCarnet4000()
        //{
        //    double controlValue;
        //    const double CONV_RAD_TO_GON = 63.6619772367581;

        //    //DM 10.01.2001
        //    //This function gets the inclination and accuracy of a TCA2003
        //    //and writes them in the member variables all values [mgon]
        //    //controlvalue = 0   succesfully
        //    //controlvalue = -1  the TCA2003 hasn't returned a correct measurement
        //    //controlvalue = -2  Compensator is OFF
        //    //controlvalue = -3  not used
        //    int result;
        //    GeoCom105_dll.TMC_ANGLE angle = new GeoCom105_dll.TMC_ANGLE();
        //    int SwCorr=(int)GeoCom105_dll.ON_OFF_TYPE.OFF;
        //    bool Measure_Ok;
        //    int counter;

        //    //Control if open port exists
        //    result = GeoCom105_dll.VB_TMC_GetInclineSwitch(ref SwCorr);
        //    if (SwCorr == (int)GeoCom105_dll.ON_OFF_TYPE.OFF)
        //    {
        //        controlValue = -2;
        //    }
        //    else
        //    {
        //        int useSencor = 1;// (int)GeoCom105_dll.TMC_INCLINE_PRG.TMC_MEA_INC;
        //        int clear = (int)GeoCom105_dll.TMC_MEASURE_PRG.TMC_CLEAR;
        //        //result = GeoCom105_dll.VB_TMC_DoMeasure(ref clear, ref useSencor);
        //        result = GeoCom105_dll.VB_TMC_GetAngle1(ref angle, ref useSencor);

        //        if (result == 0)
        //        {
        //            double CrossInc = angle.Incline.dCrossIncline * CONV_RAD_TO_GON * 1000;
        //            double LengthInc = angle.Incline.dLengthIncline * CONV_RAD_TO_GON * 1000;
        //            double Accuracy = angle.Incline.dAccuracyIncline * CONV_RAD_TO_GON * 1000;
        //            double LengthInc_norm = (LengthInc * Math.Cos(angle.dHz) - CrossInc * Math.Sin(angle.dHz));
        //            double CrossInc_norm = (LengthInc * Math.Sin(angle.dHz) + CrossInc * Math.Cos(angle.dHz));
        //            controlValue = 0;
        //        }
        //     }
        //    //MsgBox ("In GetBulle(): Len = " & LengthInc & " Cross = " & CrossInc & " Acc = " & Accuracy)
        //    //MsgBox ("In GetBulle(): Len_norm = " & LengthInc_norm & " Cross_norm " & CrossInc_norm)
        //    //MsgBox ("Controlvalue = " & Controlvalue)
        //    return null;
        //}

        internal Result GetInclineSwitchState() //V�rifie �tat InclineSwitch
        {
            Result result = new Result();

            int inclineSwitch = (int)GeoCom105_dll.ON_OFF_TYPE.ON;
            short errorCode = GeoCom105_dll.VB_TMC_GetInclineSwitch(ref inclineSwitch);
            if (errorCode == 0)
            {
                result.Success = true;
                result.AccessToken = "State Incline switch: " + inclineSwitch.ToString();
            }
            else
            {
                result.Success = false;
                result.ErrorMessage = ErrorCode(errorCode);
                result.AccessToken = "Incline Switch error: " + result.ErrorMessage;
            }
            return result;
        }
        private Result SetAngleSwitchOn() //Met sur On Angle Switch
        {
            Result result = new Result();
            try
            {
                GeoCom105_dll.TMC_ANG_SWITCH angleSwitch;
                GeoCom105_dll.ON_OFF_TYPE on = GeoCom105_dll.ON_OFF_TYPE.ON;
                angleSwitch.eCollimationCorr = on;
                angleSwitch.eInclineCorr = on;
                angleSwitch.eStandAxisCorr = on;
                angleSwitch.eTiltAxisCorr = on;
                short errorCode = GeoCom105_dll.VB_TMC_SetAngSwitch(ref angleSwitch);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Turned Angle switch ON";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Angle Switch error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Angle Switch error: " + result.ErrorMessage;
            }
            return result;
        }
        private Result GetAngleSwitchState() //V�rifie �tat Angle Switch
        {
            Result result = new Result();
            try
            {
                GeoCom105_dll.TMC_ANG_SWITCH angleSwitch;
                GeoCom105_dll.ON_OFF_TYPE off = GeoCom105_dll.ON_OFF_TYPE.OFF;
                angleSwitch.eCollimationCorr = off;
                angleSwitch.eInclineCorr = off;
                angleSwitch.eStandAxisCorr = off;
                angleSwitch.eTiltAxisCorr = off;
                short errorCode = GeoCom105_dll.VB_TMC_GetAngSwitch(ref angleSwitch);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = string.Format("State Angle switch Collimation Correction: {0} \r\nState Angle switch Incline Correction: {1} \r\nState Angle switch Standing Axis: {2} \r\nState Angle switch Tilt Axis: {3}", angleSwitch.eCollimationCorr.ToString(), angleSwitch.eInclineCorr.ToString(), angleSwitch.eStandAxisCorr.ToString(), angleSwitch.eTiltAxisCorr.ToString());
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Angle Switch error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Angle Switch error: " + result.ErrorMessage;
            }
            return result;
        }
        private Result GetPPMAtmParameters(out GeoCom105_dll.TMC_ATMOS_TEMPERATURE atmParameters) //Donne les param�tres atmosph�riques actifs dans le TDA
        {
            Result result = new Result();
            try
            {
                atmParameters.dDryTemperature = 0;
                atmParameters.dLambda = 0;
                atmParameters.dPressure = 0;
                atmParameters.dWetTemperature = 0;
                short errorCode = GeoCom105_dll.VB_TMC_GetAtmCorr(ref atmParameters);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = string.Format("Atmospheric PPM dry temperature is {0} �C \r\nAtmospheric PPM wet temperature is {1} �C \r\nAtmospheric PPM wet temperature is {2} mb \r\nAtmospheric PPM wet temperature is {3}�C \r\nEDM wavelength is {4}", Math.Round(atmParameters.dDryTemperature, 2), Math.Round(atmParameters.dWetTemperature, 2), Math.Round(atmParameters.dPressure, 2), Math.Round(atmParameters.dWetTemperature, 1), Math.Round(atmParameters.dLambda, 3));
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Getting PPM atmospheric error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Getting PPM atmospheric error: " + result.ErrorMessage;
                atmParameters.dDryTemperature = 0;
                atmParameters.dLambda = 0;
                atmParameters.dPressure = 0;
                atmParameters.dWetTemperature = 0;
            }
            return result;
        }
        private Result SetPPMAtmToZero() //met � z�ro le PPM atmosph�rique
        {
            Result result = new Result();
            try
            {
                GeoCom105_dll.TMC_ATMOS_TEMPERATURE atmCondition;
                result = GetPPMAtmParameters(out atmCondition); //permet de garder la longueur d'onde par d�faut du TDA
                if (result.Success == false)
                {
                    return result;
                }
                atmCondition.dDryTemperature = 12;
                atmCondition.dWetTemperature = 8.25;
                atmCondition.dPressure = 1013.25;
                short errorCode = GeoCom105_dll.VB_TMC_SetAtmCorr(ref atmCondition);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Atmospheric PPM sets to zero.";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Setting PPM atmospheric error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Setting PPM atmospheric error: " + result.ErrorMessage;
            }
            return result;
        }
        private Result GetRefractionParameters(out GeoCom105_dll.TMC_REFRACTION refractionParameters) //Donne la correction de refraction active
        {
            Result result = new Result();
            try
            {
                refractionParameters.dEarthRadius = 0;
                refractionParameters.dRefractiveScale = 0;
                refractionParameters.eRefON = 0;
                short errorCode = GeoCom105_dll.VB_TMC_GetRefractiveCorr(ref refractionParameters);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = string.Format("Refraction correction is {0}\r\nEarth radius = {1} M \r\nRefractive scale = {2}", refractionParameters.eRefON.ToString(), Math.Round(refractionParameters.dEarthRadius, 0).ToString(), Math.Round(refractionParameters.dRefractiveScale, 2).ToString());
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Refraction correction error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Refraction correction error: " + result.ErrorMessage;
                refractionParameters.dEarthRadius = 0;
                refractionParameters.dRefractiveScale = 0;
                refractionParameters.eRefON = GeoCom105_dll.ON_OFF_TYPE.ON;
            }
            return result;
        }
        private Result KeepTPSAlwaysOnline() //Garde online le TPS sans mise en veille
        {
            Result result = new Result();
            try
            {
                int lowTempOnOff = (int)GeoCom105_dll.ON_OFF_TYPE.OFF;
                int autoPower = (int)GeoCom105_dll.SUP_AUTO_POWER.AUTO_POWER_DISABLED;
                int timeOut = 3600000;
                short errorCode = GeoCom105_dll.VB_SUP_SetConfig(ref lowTempOnOff, ref autoPower, ref timeOut);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "TPS set to keep always online.";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Setting power management error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Setting power management error: " + result.ErrorMessage;
            }
            return result;
        }
        private Result ShowPowerManagementParameters() //Donne les param�tres power management du TPS
        {
            Result result = new Result();
            try
            {
                int lowTempOnOff = (int)GeoCom105_dll.ON_OFF_TYPE.ON;
                int autoPower = (int)GeoCom105_dll.SUP_AUTO_POWER.AUTO_POWER_OFF;
                int timeOut = 0;
                short errorCode = GeoCom105_dll.VB_SUP_GetConfig(ref lowTempOnOff, ref autoPower, ref timeOut);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = string.Format("TPS low temperature control is {0}\r\nTPS auto power mode is {1} \r\nTime out = {2} ms", (GeoCom105_dll.ON_OFF_TYPE)lowTempOnOff, (GeoCom105_dll.SUP_AUTO_POWER)autoPower, timeOut.ToString());
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Setting power management error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Setting power management error: " + result.ErrorMessage;
            }
            return result;
        }
        private Result ShowEDMMode() //Montre le mode EDM actif du TPS
        {
            Result result = new Result();
            try
            {
                int edmMode = (int)GeoCom105_dll.EDM_MODE.EDM_MODE_NOT_USED;
                short errorCode = GeoCom105_dll.VB_TMC_GetEdmMode(ref edmMode);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Active EDM Mode is " + (GeoCom105_dll.EDM_MODE)edmMode;
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
            }
            return result;
        }
        private Result SetCommunicationWaitingTimeToDefault() //Met les temps d'attente comme dans SFB
        {
            Result result = new Result();
            Result resultGlobal = new Result();
            try
            {
                short sendDelay = 20;
                short errorCode = GeoCom105_dll.VB_COM_SetSendDelay(ref sendDelay);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "TPS communication send delay sets to 20 ms";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode) + "\r\n";
                    result.AccessToken = "Set Send Delay error: " + result.ErrorMessage;
                }
                resultGlobal.MergeString(result);
                errorCode = GeoCom105_dll.VB_COM_SetTimeOut(30);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "TPS communication time out sets to 15 ms";
                    result.ErrorMessage = "";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Set time out error: " + result.ErrorMessage;
                }
                resultGlobal.MergeString(result);
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Set time out error: " + result.ErrorMessage;
            }
            return resultGlobal;
        }
        private Result ShowCommunicationTimeOut() //Donne le temps d'attente maximum avant erreur par Geocom
        {
            Result result = new Result();
            try
            {
                short timeOut = 0;
                short errorCode = GeoCom105_dll.VB_COM_GetTimeOut(ref timeOut);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "TPS communication time out = " + timeOut.ToString() + " ms";
                    result.ErrorMessage = "";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Show time out error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Show time out error: " + result.ErrorMessage;
            }
            return result;
        }
        private Result GetOneDistance(ref double slopeDistance, ref Angles angles)
        {
            if (!this.Tcra1100._ATR)
            {
                new MessageInput(MessageType.GoodNews, "Ready to measure distance?;Click OK!")
                {
                    ButtonTexts = new List<string> { R.T_OK + '!' }
                }.Show();
            }

            Result result = new Result();
            result.MergeString(PurgeDistance());
            result.MergeString(LaunchDistanceMeasurement()); result.MergeString(ReadActualDistance(ref slopeDistance, ref angles));

            result.MergeString(PurgeDistance());
            return result;
        }
        private Result ReadActualDistance(ref double slopeDistance, ref Angles angles)

        {
            Result result = new Result();
            try
            {
                GeoCom105_dll.TMC_HZ_V_ANG onlyAngle = new GeoCom105_dll.TMC_HZ_V_ANG();
                int measureIncline = (int)GeoCom105_dll.TMC_INCLINE_PRG.TMC_MEA_INC;
                int waitTime = 5000;
                short errorCode = GeoCom105_dll.VB_TMC_GetSimpleMea(ref waitTime, ref onlyAngle, ref slopeDistance, ref measureIncline);
                if (errorCode == 0)
                {
                    angles.Horizontal.Value = onlyAngle.dHz / Math.PI * 200;
                    angles.Vertical.Value = onlyAngle.dV / Math.PI * 200;
                    Logs.Log.AddEntryAsError(null, $"GetSimpleMea {angles.Horizontal.Value} {angles.Vertical.Value}");

                    result.Success = true;
                    result.AccessToken = "TPS read the actual distance in memory";
                    result.ErrorMessage = "";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Read of distance error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Read of distance error: " + result.ErrorMessage;
                slopeDistance = 0;
            }
            return result;
        }
        private void EmqCalculation(double[] setOfMeasure, out DoubleValue average)
        {
            average = new DoubleValue();
            average.Value = 0;
            average.Sigma = 0;
            for (int i = 0; i < setOfMeasure.Length; i++)
            {
                average.Value += setOfMeasure[i];
            }
            average.Value = average.Value / setOfMeasure.Length;
            for (int i = 0; i < setOfMeasure.Length; i++)
            {
                average.Sigma += Math.Pow(setOfMeasure[i] - average.Value, 2);
            }
            average.Sigma = Math.Sqrt(average.Sigma) / (setOfMeasure.Length - 1);
        }
        private Result PurgeDistance()
        {
            Result result = new Result();
            try
            {
                int clear = (int)GeoCom105_dll.TMC_MEASURE_PRG.TMC_CLEAR;
                int measureIncline = (int)GeoCom105_dll.TMC_INCLINE_PRG.TMC_PLANE_INC;
                short errorCode = GeoCom105_dll.VB_TMC_DoMeasure(ref clear, ref measureIncline);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Purge distance OK";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = string.Format("Purge distance error: {0}", result.ErrorMessage);
                    EndGeocom();
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
            }
            return result;
        }
        private Result LaunchDistanceMeasurement()
        {
            Result result = new Result();
            try
            {
                int def_Dist = (int)GeoCom105_dll.TMC_MEASURE_PRG.TMC_DEF_DIST;
                if (GeoCom105_dll.ModeMesure == GeoCom105_dll.IR_Or_RL.RL)
                {
                    def_Dist = (int)GeoCom105_dll.TMC_MEASURE_PRG.TMC_RED_TRK_DIST;
                }
                int measureIncline = (int)GeoCom105_dll.TMC_INCLINE_PRG.TMC_MEA_INC;
                short errorCode = GeoCom105_dll.VB_TMC_DoMeasure(ref def_Dist, ref measureIncline);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "TPS did a measure of distance";
                    result.ErrorMessage = "";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Measurement of distance error: " + result.ErrorMessage;
                    return result;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Measurement of distance error: " + result.ErrorMessage;
            }
            return result;
        }

        private Result EndGeocom() //Fermeture Geocom.,
        {
            Result result = new Result();
            try
            {
                short errorCode = GeoCom105_dll.VB_COM_End();
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Close Geocom OK";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Close Geocom error: " + result.ErrorMessage.ToString();
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Close Geocom error: " + result.ErrorMessage.ToString();
            }
            return result;
        }
        private Result SetEDMModeToRL() //Met sur le mode EDM sur mesure continue sans reflecteur
        {

            Result result = new Result();
            try
            {
                int edmMode = (int)GeoCom105_dll.EDM_MODE.EDM_CONT_REFLESS;
                short errorCode = GeoCom105_dll.VB_TMC_SetEdmMode(ref edmMode);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "EDM Mode sets to measurement without reflector.";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
                }
                result.MergeString(SetATROnOff(false));
                if (result.Success) this.Tcra1100.ReflectorLessMode = true;
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
            }
            return result;
        }

        #endregion

        #region public method TCRA1100
        public override Result Disconnect() //Fermerture du port COM et de Geocom
        {
            Result result = new Result();
            Result resultGlobal = new Result();
            result = CloseConnection();
            resultGlobal.MergeString(result);
            result = EndGeocom();
            resultGlobal.MergeString(result);
            this.InstrumentIsOn = false;
            return resultGlobal;
        }

        internal void DoTemporaryDistanceMeasurement()
        {
            double d = 0;
            Angles a = new Angles();
            GetOneDistance(ref d, ref a);
            this.TemporaryDistanceAvailableInInstrument = true;
        }

        public string[] GetAllPorts()
        //Recherche des ports s�rie disponibles du PC
        {
            List<String> allPorts = new List<String>();
            foreach (String portName in System.IO.Ports.SerialPort.GetPortNames())
            {
                //evite d'ajouter les ports en double
                if (allPorts.FindIndex(x => x == portName) == -1)
                {
                    ///Tri par ordre
                    int index = 0;
                    int a;
                    bool oka = Int32.TryParse(portName.Substring(3), out a);
                    if (oka)
                    {
                        // N'ajoute que les port jusque COM4 pour la Geocom
                        if (a <= 4)
                        {
                            foreach (String item in allPorts)
                            {
                                int b;
                                bool okb = Int32.TryParse(item.Substring(3), out b);
                                if (oka && okb)
                                {
                                    if (a > b) break;
                                    index++;
                                }
                                else
                                {
                                    break;
                                }

                            }
                            allPorts.Insert(index, portName);
                        }
                    }
                }
            }
            allPorts.Reverse();
            this.listPortName = allPorts;
            ///To fill the combobox in view with all com port available
            string[] arrayPort = new string[allPorts.Count];
            for (int i = 0; i < allPorts.Count; i++)
            {
                arrayPort[i] = allPorts[i];
            }
            return arrayPort;
        }
        internal Result TryConnectOthersPorts()
        {
            Result result = new Result();
            if (InstrumentIsOn == false)
            {
                for (int i = 1; i < this.listPortName.Count; i++)
                {

                    if (InstrumentIsOn == false)
                    {
                        //change de port et essaye de se connecter
                        this.ChangePort();
                        try
                        {
                            result.Success = true;
                            result.MergeString(OpenConnection(this.Tcra1100._PortCom));
                        }
                        catch (Exception ex)
                        {
                            ex.Data.Clear();
                        }
                        if (result.Success)
                        {
                            isConnected = true;
                            //result.MergeString(GetSerialNumber());
                            return result;
                        }
                        else
                        {
                            result.MergeString(new Result() { Success = false, ErrorMessage = R.StringTDA5005_ConnectionFailed + this.Tcra1100._PortCom, });
                            isConnected = false;
                        }
                    }
                }
            }
            else
            {
                //Si le TCRA �tait d�j� ON lors du lancement de la fonction, ne fait rien
                result.MergeString(new Result() { Success = true, ErrorMessage = R.StringTDA5005_AlreadyOn, });
            }
            if (InstrumentIsOn == false)
            //N'a pas trouv� de TCRA sur aucun des ports
            {
                result.MergeString(new Result() { Success = false, ErrorMessage = R.StringTDA5005_CannotFindPortCom, });
            }
            return result;
        }
        public Result ChangePort()
        //Changement du port s�rie
        {
            Result result = new Result();
            try
            {
                listPortIndex += 1;
                if (listPortIndex >= listPortName.Count) listPortIndex = 0;
                if (listPortName.Count > 0)
                {
                    this.Tcra1100._PortCom = listPortName[listPortIndex];
                }
                result.MergeString(new Result() { Success = true, AccessToken = "New port name: " + this.Tcra1100._PortCom, });
            }
            catch (Exception)
            {
                result.MergeString(new Result() { Success = false, ErrorMessage = "Cannot change port", });
            }
            return result;
        }
        public Result CheckConnection() //Teste si connection est active.
        {
            Result result = new Result();
            try
            {
                short errorCode = GeoCom105_dll.VB_COM_NullProc();
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Check connection OK";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Check connection error: " + result.ErrorMessage.ToString();
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Check connection error: " + result.ErrorMessage.ToString();
            }
            return result;
        }
        public Result CorrectionAngleCalculation(ref double CorrHz, ref double CorrV)
        {
            Result result = new Result();
            Result resultGlobal = new Result();
            Angles anglesCorrectionFace1_1 = new Angles();
            Angles anglesCorrectionFace2_1 = new Angles();
            //Angles anglesCorrectionFace1_2 = new Angles();
            //Angles anglesCorrectionFace2_2 = new Angles();
            Angles averageAnglesCorrection = new Angles();
            Compensator compensator = new Compensator();
            result.MergeString(this.GoFace(1));
            result.MergeString(this.FineAdjust());
            result.MergeString(this.MeasureAngleAndCompensator(ref anglesCorrectionFace1_1, ref compensator));
            result.MergeString(this.GoFace(2));
            result.MergeString(this.FineAdjust());
            result.MergeString(this.MeasureAngleAndCompensator(ref anglesCorrectionFace2_1, ref compensator));
            result.MergeString(this.GoFace(1));
            averageAnglesCorrection.Horizontal.Value = (anglesCorrectionFace1_1.Horizontal.Value > Math.PI) ? ((anglesCorrectionFace1_1.Horizontal.Value + anglesCorrectionFace2_1.Horizontal.Value + Math.PI) / 2) : ((anglesCorrectionFace1_1.Horizontal.Value + anglesCorrectionFace2_1.Horizontal.Value - Math.PI) / 2);
            averageAnglesCorrection.Vertical.Value = (anglesCorrectionFace1_1.Vertical.Value - anglesCorrectionFace2_1.Vertical.Value + 2 * Math.PI) / 2;
            CorrHz = averageAnglesCorrection.Horizontal.Value - anglesCorrectionFace1_1.Horizontal.Value;
            CorrV = averageAnglesCorrection.Vertical.Value - anglesCorrectionFace1_1.Vertical.Value;
            return result;
        }
        public Result Initialize(int numberOfIterationForCollimationError, bool checkInitialization) //fait l'initilisation et v�rifie ensuite si l'initialisation est OK.Permet de calculer la correction de collimation � appliquer en face1)
        {
            Result result = new Result();
            result = this.InitializeSensor();
            if (numberOfIterationForCollimationError > 0 && result.Success == true)
            {
                double SumCorrHz = 0;
                double SumCorrV = 0;
                double CorrHz = 0;
                double CorrV = 0;
                for (int i = 0; i < numberOfIterationForCollimationError; i++)
                {
                    result.MergeString(CorrectionAngleCalculation(ref CorrHz, ref CorrV));
                    SumCorrHz += CorrHz;
                    SumCorrV += CorrV;
                }
                this.Tcra1100._AngleCorrection.Horizontal.Value = SumCorrHz / numberOfIterationForCollimationError;
                this.Tcra1100._AngleCorrection.Vertical.Value = SumCorrV / numberOfIterationForCollimationError;
            }
            if (checkInitialization == true && result.Success == true)
            {
                result.MergeString(this.CheckInitialization());
            }
            return result;
        }
        public override Result InitializeSensor() // Initialisation compl�te du TDA avec tous les param�tres habituels par d�faut
        {
            Result result = new Result();
            Result resultGlobal = new Result();
            GeoCom105_dll.ModeMesure = GeoCom105_dll.IR_Or_RL.EDM;
            result = SetCommunicationWaitingTimeToDefault();
            result = SetAngleSwitchOn();
            #region Update resultGlobal or exit if error
            resultGlobal.MergeString(result);
            if (resultGlobal.Success == false) { return resultGlobal; }
            #endregion
            double prismCorrection = 0;
            result = SetPrismCorrectionMM(ref prismCorrection);
            #region Update resultGlobal or exit if error
            resultGlobal.MergeString(result);
            if (resultGlobal.Success == false) { return resultGlobal; }
            #endregion
            result = SetPPMAtmToZero();
            #region Update resultGlobal or exit if error
            resultGlobal.MergeString(result);
            if (resultGlobal.Success == false) { return resultGlobal; }
            #endregion
            result = SetRefractionTozero();
            #region Update resultGlobal or exit if error
            resultGlobal.MergeString(result);
            if (resultGlobal.Success == false) { return resultGlobal; }
            #endregion
            result = SetTargetLOCKOnOff(false);
            #region Update resultGlobal or exit if error
            resultGlobal.MergeString(result);
            if (resultGlobal.Success == false) { return resultGlobal; }
            #endregion
            result = SetEDMModeToStandard();
            #region Update resultGlobal or exit if error
            resultGlobal.MergeString(result);
            if (resultGlobal.Success == false) { return resultGlobal; }
            #endregion
            result = SetRedLaserOnOff(false);
            #region Update resultGlobal or exit if error
            resultGlobal.MergeString(result);
            if (resultGlobal.Success == false) { return resultGlobal; }
            #endregion
            string atrAdjustMode = "Normal_Mode";
            result = SetAtrAdjustMode(atrAdjustMode);
            #region Update resultGlobal or exit if error
            resultGlobal.MergeString(result);
            if (resultGlobal.Success == false) { return resultGlobal; }
            #endregion
            result = SetInclineSwitchOn();
            #region Update resultGlobal or exit if error
            resultGlobal.MergeString(result);
            if (resultGlobal.Success == false) { return resultGlobal; }
            #endregion
            result = SetATROnOff(true);
            #region Update resultGlobal or exit if error
            resultGlobal.MergeString(result);
            if (resultGlobal.Success == false) { return resultGlobal; }
            #endregion
            result = GetBatteryLevel();
            #region Update resultGlobal or exit if error
            resultGlobal.MergeString(result);
            if (resultGlobal.Success == false) { return resultGlobal; }
            #endregion
            result = GetInternalTemperature();
            #region Update resultGlobal or exit if error
            resultGlobal.MergeString(result);
            if (resultGlobal.Success == false) { return resultGlobal; }
            #endregion
            return resultGlobal;
        }
        public Result CheckInitialization() // Donne tous les param�tres actifs du TDA pour v�rifier l'initialisation
        {
            Result result = new Result();
            Result resultGlobal = new Result();
            resultGlobal.Success = true;
            result = CheckConnection();
            resultGlobal.MergeString(result);
            result = GetSerialNumber();
            resultGlobal.MergeString(result);
            result = GetInclineSwitchState();
            resultGlobal.MergeString(result);
            result = GetAngleSwitchState();
            resultGlobal.MergeString(result);
            double prismCorrection = 0;
            result = GetPrismCorrectionMM(out prismCorrection);
            resultGlobal.MergeString(result);
            GeoCom105_dll.TMC_ATMOS_TEMPERATURE atmParameters;
            result = GetPPMAtmParameters(out atmParameters);
            resultGlobal.MergeString(result);
            GeoCom105_dll.TMC_REFRACTION refractionParameters;
            result = GetRefractionParameters(out refractionParameters);
            resultGlobal.MergeString(result);
            result = ShowPowerManagementParameters();
            resultGlobal.MergeString(result);
            result = GetTargetLOCKStatus();
            resultGlobal.MergeString(result);
            result = GetATRStatus();
            resultGlobal.MergeString(result);
            result = ShowEDMMode();
            resultGlobal.MergeString(result);
            string atrAdjustMode = "";
            result = GetAtrAdjustMode(out atrAdjustMode);
            resultGlobal.MergeString(result);
            result = ShowCommunicationTimeOut();
            resultGlobal.MergeString(result);
            result = GetBatteryLevel();
            resultGlobal.MergeString(result);
            result = GetInternalTemperature();
            resultGlobal.MergeString(result);
            return resultGlobal;
        }
        public Result GetSerialNumber() //Donne le num�ro de s�rie TPS
        {
            Result result = new Result();
            try
            {
                int TpsSerialNumber = 0;
                short errorCode = GeoCom105_dll.VB_CSV_GetInstrumentNo(ref TpsSerialNumber);
                if (errorCode == 0)
                {
                    if (this.Tcra1100._SerialNumber != TpsSerialNumber.ToString())
                    {
                        this.Disconnect();
                        isConnected = false;

                        this.StationTheodoliteModule.CheckAndChangeInstrument(TpsSerialNumber.ToString());
                        result.Success = false;
                        result.AccessToken = "TPS SN" + TpsSerialNumber.ToString() + "is not the good one, please reconnect";
                    }
                    else
                    {
                        result.Success = true;
                        result.AccessToken = "TPS serial number is " + TpsSerialNumber.ToString();
                    }
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Get serial number error: " + result.ErrorMessage.ToString();
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Get serial number error: " + result.ErrorMessage.ToString();
            }
            return result;
        }
        public Result GetBatteryLevel()
        {
            Result result = new Result();
            try
            {
                double batteryLevel = 0;
                short errorCode = GeoCom105_dll.VB_CSV_GetVBat(ref batteryLevel);
                this.Tcra1100._Battery = batteryLevel;
                string batteryState = "";
                if (batteryLevel >= 5)
                {
                    batteryState = "Good";
                }
                else
                {
                    batteryState = "Empty";
                }
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = string.Format("Battery Level = {0} is {1}", Math.Round(batteryLevel, 1).ToString(), batteryState);
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Get battery level error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Get battery level error: " + result.ErrorMessage;
            }

            return result;
        }
        public Result GetInternalTemperature()
        {
            Result result = new Result();
            try
            {
                double TpsInternalTemperature = 0;
                short errorCode = GeoCom105_dll.VB_CSV_GetIntTemp(ref TpsInternalTemperature);
                this.Tcra1100._Tinterne = TpsInternalTemperature;
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "TPS internal temperature = " + Math.Round(TpsInternalTemperature, 0).ToString();
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Get TPS internal temperature error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Get TPS internal temperature error: " + result.ErrorMessage;
            }
            return result;
        }
        public Result SetATROnOff(bool ATROn) //Met sur on ou off l'ATR TPS
        {
            Result result = new Result();
            try
            {
                int onOff;
                if (ATROn == true)
                {
                    onOff = (int)GeoCom105_dll.ON_OFF_TYPE.ON;
                }
                else
                {
                    onOff = (int)GeoCom105_dll.ON_OFF_TYPE.OFF;
                }

                short errorCode = GeoCom105_dll.VB_AUT_SetATRStatus(ref onOff);
                if (errorCode == 0)
                {
                    this.Tcra1100._ATR = ATROn;
                    result.Success = true;
                    result.AccessToken = "ATR sets to " + (GeoCom105_dll.ON_OFF_TYPE)onOff;
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Set ATR error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Set ATR error: " + result.ErrorMessage;
            }
            return result;
        }
        public Result SwitchAtr()
        {
            Result resultGlobal = new Result();
            resultGlobal.MergeString(this.GetATRStatus());
            if (resultGlobal.Success)
            {
                resultGlobal.MergeString(this.SetATROnOff(!this.Tcra1100._ATR));
            }
            return resultGlobal;
        }
        public Result GetATRStatus() //Donne le status actif ATR TPS
        {
            Result result = new Result();
            try
            {
                int onOff = (int)GeoCom105_dll.ON_OFF_TYPE.ON;
                short errorCode = GeoCom105_dll.VB_AUT_GetATRStatus(ref onOff);

                if (errorCode == 0)
                {
                    if (onOff == 0)
                    {
                        this.Tcra1100._ATR = false;
                    }
                    else
                    {
                        this.Tcra1100._ATR = true;
                    }
                    result.Success = true;
                    result.AccessToken = "ATR is " + (GeoCom105_dll.ON_OFF_TYPE)onOff;
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Get ATR error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Get ATR error: " + result.ErrorMessage;
            }
            return result;
        }
        public Result InvertFace()
        {
            Logs.Log.AddEntryAsBeginningOf(this, "Changing Face");
            Result result = new Result();
            try
            {
                int posMode = (int)GeoCom105_dll.AUT_POSMODE.AUT_NORMAL;
                int atrMode = (int)GeoCom105_dll.AUT_ATRMODE.AUT_POSITION;
                int bDummy = (int)GeoCom105_dll.BOOLE.FALSE;
                short errorCode = GeoCom105_dll.VB_AUT_ChangeFace4(ref posMode, ref atrMode, ref bDummy);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "TPS Face has changed";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = String.Format("TPS change face error:{0}", result.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = String.Format("TPS change face error:{0}", result.ErrorMessage);
            }
            return result;
        }
        public Result GoFace(int face1Or2) //Change face TPS
        {
            Result result = new Result();
            Result resultGlobal = new Result();
            switch (face1Or2)
            {
                case 1:
                    break;
                case 2:
                    break;
                default:
                    resultGlobal.Success = false;
                    resultGlobal.ErrorMessage = "Error face must be 1 or 2";
                    return resultGlobal;
            }
            resultGlobal.MergeString(GetTCRAFace());
            if (resultGlobal.Success == false) { return resultGlobal; }
            if (this.Tcra1100._Face != face1Or2)
            {
                resultGlobal.MergeString(InvertFace());
                if (resultGlobal.Success) this.Tcra1100._Face = face1Or2;
            }
            else
            {
                resultGlobal.Success = true;
                resultGlobal.AccessToken = string.Format("{0}TPS was already in face {1}", resultGlobal.AccessToken, face1Or2);
            }
            return resultGlobal;
        }
        public Result GetTCRAFace() //Donne face TPS
        {
            Result result = new Result();
            try
            {
                int face = (int)GeoCom105_dll.TMC_FACE.TMC_FACE_1;
                short errorCode = GeoCom105_dll.VB_TMC_GetFace(ref face);
                if (errorCode == 0)
                {
                    this.Tcra1100._Face = (face == 0) ? 1 : 2;
                    result.Success = true;
                    result.AccessToken = "TPS face is " + this.Tcra1100._Face.ToString();
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Get TPS face error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Get TPS face error: " + result.ErrorMessage;
            }
            return result;
        }
        public Result FineAdjust()
        {
            Result result = new Result();
            try
            {
                double dSrchHz = 0.016;
                double dSrchV = 0.016;
                int bDummy = (int)GeoCom105_dll.BOOLE.FALSE;
                short errorCode = 1;
                int i = 0;
                while (errorCode != 0) //Try 3 times to adjust on target if errors.
                {
                    errorCode = GeoCom105_dll.VB_AUT_FineAdjust3(ref dSrchHz, ref dSrchV, ref bDummy);
                    i += 1;
                    if (i > 2)
                    {
                        result.Success = false;
                        result.ErrorMessage = ErrorCode(errorCode);
                        result.AccessToken = string.Format("Target fine adjust error: {0}", result.ErrorMessage);
                        this.InvokeOnApplicationDispatcher(this.View.MessageATRFailed);
                        return result;
                    }
                }
                result.Success = true;
                //  result.AccessToken = "TPS is fine adjusted to target center";
                result.AccessToken = R.T_TPS_IS_FINE_ADJUSTED_TO_TARGET_CENTER;
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = string.Format("Target fine adjust error: {0}", result.ErrorMessage);
            }

            return result;
        }

        public Result MoveBy(double dHz, double dV)
        {
            Result result = new Result();
            try
            {
                if (dHz > 400) dHz -= 400;
                if (dHz < 0) dHz += 400;

                dHz = dHz / 200 * Math.PI;
                dV = dV / 200 * Math.PI;
                Angles actualPosition = new Angles();
                result = MeasureAngle(ref actualPosition);
                if (result.Success == false) { return result; }
                double hz = (actualPosition.Horizontal.Value + dHz);
                double v = (actualPosition.Vertical.Value + dV);
                int posMode = (int)GeoCom105_dll.AUT_POSMODE.AUT_NORMAL;
                int atrMode = (int)GeoCom105_dll.AUT_ATRMODE.AUT_POSITION;
                int bDummy = (int)GeoCom105_dll.BOOLE.FALSE;
                short errorCode = GeoCom105_dll.VB_AUT_MakePositioning4(ref hz, ref v, ref posMode, ref atrMode, ref bDummy);
                if (errorCode == 0)
                {
                    result.Success = true;
                    //   result.AccessToken = "TPS has moved to new position";
                    result.AccessToken = R.T_TPS_HAS_MOVED_TO_NEW_POSITION;
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = string.Format("MoveTo error: {0}", result.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = string.Format("Moveby error: {0}", result.ErrorMessage);
            }

            return result;
        }

        public Result MoveTo(double Hz, double V)
        {
            Result result = new Result();
            try
            {
                int posMode = (int)GeoCom105_dll.AUT_POSMODE.AUT_NORMAL;
                int atrMode = (int)GeoCom105_dll.AUT_ATRMODE.AUT_POSITION;
                int bDummy = (int)GeoCom105_dll.BOOLE.FALSE;
                if (Hz > 400) Hz -= 400;
                if (Hz < 0) Hz += 400;
                Hz = Hz / 200 * Math.PI;
                V = V / 200 * Math.PI;
                short errorCode = GeoCom105_dll.VB_AUT_MakePositioning4(ref Hz, ref V, ref posMode, ref atrMode, ref bDummy);
                if (errorCode == 0)
                {
                    result.Success = true;
                    //    result.AccessToken = "TPS has moved to new position";
                    result.AccessToken = R.T_TPS_HAS_MOVED_TO_NEW_POSITION;
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = string.Format("MoveTo error: {0}", result.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = string.Format("MoveTo error: {0}", result.ErrorMessage);
            }

            return result;
        }

        public Result MeasureAngleAndCompensator(ref Angles measureOfAngles, ref Compensator MeasureOfCompensator)
        {
            Result result = new Result();
            Result resultGlobal = new Result();
            try
            {
                GeoCom105_dll.TMC_ANGLE TPSAngles = new GeoCom105_dll.TMC_ANGLE();
                DoubleValue angleHz = new DoubleValue();
                DoubleValue angleV = new DoubleValue();
                measureOfAngles = new Angles();
                MeasureOfCompensator = new Compensator();
                int inclineProg = (int)GeoCom105_dll.TMC_INCLINE_PRG.TMC_AUTO_INC;
                if (GeoCom105_dll.ModeMesure == GeoCom105_dll.IR_Or_RL.EDM && this.Tcra1100._ATR)
                {
                    resultGlobal.MergeString(FineAdjust());
                }
                if (resultGlobal.Success == false) { return resultGlobal; }

                int errorCode = 0;
                double h = -1;
                double v = -1;
                double sh = -1;
                double sv = -1;

                if (!this.Tcra1100._ATR || !this.DistanceWanted)
                {
                    errorCode = GeoCom105_dll.VB_TMC_GetAngle1(ref TPSAngles, ref inclineProg);
                    h = TPSAngles.dHz;
                    v = TPSAngles.dV;
                    sh = TPSAngles.dAngleAccuracy;
                    sv = sh;
                }
                else
                {
                    int waitingTimeForDistance = 22;
                    double dummyDistance = -1;
                    GeoCom105_dll.TMC_HZ_V_ANG angles = new GeoCom105_dll.TMC_HZ_V_ANG();
                    errorCode = GeoCom105_dll.VB_TMC_GetAngle1(ref TPSAngles, ref inclineProg);
                    errorCode = GeoCom105_dll.VB_TMC_GetSimpleMea(ref waitingTimeForDistance, ref angles, ref dummyDistance, ref inclineProg);

                    h = angles.dHz;
                    v = angles.dV;
                    sh = TPSAngles.dAngleAccuracy;
                    sv = sh;
                }


                if (errorCode == 0)
                {
                    result.Success = true;
                    //   result.AccessToken = "TPS has done a complete angle and compensator measurement.";
                    result.AccessToken = R.T_TPS_HAS_DONE_A_COMPLETE_ANGLE_AND_COMPENSATOR_MEASUREMENT;
                    angleHz.Value = h / Math.PI * 200;
                    angleHz.Sigma = sh / Math.PI * 200;
                    angleV.Value = v / Math.PI * 200;
                    angleV.Sigma = sv / Math.PI * 200;

                    Logs.Log.AddEntryAsError(null, $"FineAdjust-- {angleHz.Value} {angleV.Value}");
                    measureOfAngles.Horizontal = angleHz;
                    measureOfAngles.Vertical = angleV;
                    // measureOfAngles.sigma = TPSAngles.dAngleAccuracy; I did remove the sigma of the two angles together i dont think it make sense? pascal

                    this.Tcra1100._Compensator.inclinationAccuracy = TPSAngles.Incline.dAccuracyIncline;
                    this.Tcra1100._Compensator.longitudinalAxisInclination = TPSAngles.Incline.dLengthIncline;
                    this.Tcra1100._Compensator.tranverseAxisInclination = TPSAngles.Incline.dCrossIncline;
                    this.Tcra1100._Compensator.inclineTime = System.DateTime.Now;
                    MeasureOfCompensator.inclinationAccuracy = TPSAngles.Incline.dAccuracyIncline;
                    MeasureOfCompensator.longitudinalAxisInclination = TPSAngles.Incline.dLengthIncline;
                    MeasureOfCompensator.tranverseAxisInclination = TPSAngles.Incline.dCrossIncline;
                    MeasureOfCompensator.inclineTime = System.DateTime.Now;
                }
                else
                {
                    result.Success = false;
                    // result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = string.Format("TPS angle and compensator measurement error: {0}", result.ErrorMessage);

                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                measureOfAngles = new Angles();
                MeasureOfCompensator = new Compensator();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = string.Format("TPS angle and compensator measurement error: {0}", result.ErrorMessage);
            }

            resultGlobal.MergeString(result);
            return resultGlobal;
        }
        public Result MeasureAngle(ref Angles measureOfAngles)
        {
            if (!this.Tcra1100._ATR)
            {
                new MessageInput(MessageType.GoodNews, "Please aim;then click OK!")
                {
                    ButtonTexts = new List<string> { R.T_OK + '!' }
                }.Show();
            }

            Compensator MeasureOfCompensator = new Compensator();
            Result result = new Result();
            result = MeasureAngleAndCompensator(ref measureOfAngles, ref MeasureOfCompensator);
            return result;
        }
        public Result MeasureCompensator()
        {
            Angles MeasureOfAngles = new Angles();
            Compensator measureOfCompensator = new Compensator();
            Result result = new Result();
            result = MeasureAngleAndCompensator(ref MeasureOfAngles, ref measureOfCompensator);
            return result;
        }
        public Result GetAverageAngle(out Angles averageAngle, int numberOfMeasure)
        {
            Result result = new Result();
            Angles singleAngle = new Angles();
            averageAngle = new Angles();
            if (numberOfMeasure <= 0)
            {
                result.Success = false;
                // result.ErrorMessage = "Cannot measure negative or zero angles";
                // result.AccessToken = "Distance error: Number of measures must be > 0";
                result.ErrorMessage = R.T_CANNOT_MEASURE_NEGATIVE_OR_ZERO_ANGLES;
                result.AccessToken = R.T_DISTANCE_ERROR_NUMBER_OF_MEASURES_MUST_BE;
                return result;
            }
            double[] angleHz = new double[numberOfMeasure];
            double[] angleV = new double[numberOfMeasure];
            for (int i = 0; i < numberOfMeasure; i++)
            {
                result.MergeString(MeasureAngle(ref singleAngle));
                if (result.Success == false) { return result; }
                angleHz[i] = singleAngle.Horizontal.Value;
                angleV[i] = singleAngle.Vertical.Value;
            }
            DoubleValue averageHz;
            EmqCalculation(angleHz, out averageHz);
            averageAngle.Horizontal.Value = averageHz.Value;
            averageAngle.Horizontal.Sigma = averageHz.Sigma;
            DoubleValue averageV;
            EmqCalculation(angleV, out averageV);
            averageAngle.Vertical.Value = averageV.Value;
            averageAngle.Vertical.Sigma = averageV.Sigma;
            return result;

        }
        public Result GetAtrAdjustMode(out string atrAdjustMode) //Montre le mode ATR actif
        {
            Result result = new Result();
            try
            {
                int iAtrAdjustMode = (int)GeoCom105_dll.AUT_ADJMODE.AUT_POINT_MODE;
                short errorCode = GeoCom105_dll.VB_AUT_GetFineAdjustMode(ref iAtrAdjustMode);
                switch ((GeoCom105_dll.AUT_ADJMODE)iAtrAdjustMode)
                {
                    case GeoCom105_dll.AUT_ADJMODE.AUT_NORM_MODE:
                        atrAdjustMode = "Normal_Mode";
                        break;
                    case GeoCom105_dll.AUT_ADJMODE.AUT_POINT_MODE:
                        atrAdjustMode = "Point_Mode";
                        break;
                    default:
                        atrAdjustMode = "";
                        break;
                }
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "ATR fine adjust mode is " + atrAdjustMode;
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Fine Adjust Mode error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Fine Adjust Mode error: " + result.ErrorMessage;
                atrAdjustMode = "";
            }

            return result;
        }
        public Result SetAtrAdjustMode(string atrAdjustMode) //Met le mode ATR sur normal point_mode: prend les points _a courte distance avec dispersion, norm_mode :code 8704 pour courtes distances
        {
            Result result = new Result();
            try
            {
                int iAtrAdjustMode = 0;
                switch (atrAdjustMode)
                {
                    case "Point_Mode":
                        iAtrAdjustMode = (int)GeoCom105_dll.AUT_ADJMODE.AUT_POINT_MODE;
                        break;
                    case "Normal_Mode":
                        iAtrAdjustMode = (int)GeoCom105_dll.AUT_ADJMODE.AUT_NORM_MODE;
                        break;
                    default:
                        result.Success = false;
                        result.ErrorMessage = "Fine Adjust Mode Error: Unknown ATR mode in input";
                        result.AccessToken = "Fine Adjust Mode Error: Unknown ATR mode in input";
                        return result;
                }
                short errorCode = GeoCom105_dll.VB_AUT_SetFineAdjustMode(ref iAtrAdjustMode);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "ATR fine adjust mode sets to " + atrAdjustMode;
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Fine Adjust Mode error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Fine Adjust Mode error: " + result.ErrorMessage;
            }

            return result;
        }
        public Result SetTargetLOCKOnOff(bool LockOn) //Met sur on ou off le LOCK TPS
        {
            Result result = new Result();
            try
            {
                int onOff;
                if (LockOn == true)
                {
                    onOff = (int)GeoCom105_dll.ON_OFF_TYPE.ON;
                }
                else
                {
                    onOff = (int)GeoCom105_dll.ON_OFF_TYPE.OFF;
                }

                short errorCode = GeoCom105_dll.VB_AUT_SetLockStatus(ref onOff);
                if (errorCode == 0)
                {
                    result.Success = true;
                    //result.AccessToken = "Target Lock set to " + ((ON_OFF_TYPE)Convert.ChangeType(onOff, typeof(ON_OFF_TYPE))).ToString() + "\r\n";
                    result.AccessToken = "Target Lock set to " + ((GeoCom105_dll.ON_OFF_TYPE)onOff).ToString();
                    this.Tcra1100._Lock = LockOn;
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Target Lock error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Target Lock error: " + result.ErrorMessage;
            }

            return result;
        }
        public Result GetTargetLOCKStatus() //Donne le status actif LOCK TPS
        {
            Result result = new Result();
            try
            {
                int onOff = (int)GeoCom105_dll.ON_OFF_TYPE.ON;
                short errorCode = GeoCom105_dll.VB_AUT_GetLockStatus(ref onOff);
                if (onOff == 0)
                {
                    this.Tcra1100._Lock = false;
                }
                else
                {
                    this.Tcra1100._Lock = true;
                }
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Target Lock is " + (GeoCom105_dll.ON_OFF_TYPE)onOff;
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Target Lock error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Target Lock error: " + result.ErrorMessage;
            }

            return result;
        }
        public Result SetRefractionTozero() //Desactive la correction de refraction rentrer tous les param�tres sinon probl�me d'affichage de la distance horizontale en mode GSI. (OFF, rayon Terre 6378000, coeff refraction 0.13)
        {
            Result result = new Result();
            try
            {
                GeoCom105_dll.TMC_REFRACTION refractionParameters;
                result = GetRefractionParameters(out refractionParameters);
                if (refractionParameters.eRefON == GeoCom105_dll.ON_OFF_TYPE.ON)
                {
                    //refractionParameters.dEarthRadius = 6378000;
                    //refractionParameters.dRefractiveScale = 0.13;
                    refractionParameters.eRefON = GeoCom105_dll.ON_OFF_TYPE.OFF;
                    short errorCode = GeoCom105_dll.VB_TMC_SetRefractiveCorr(ref refractionParameters);
                    if (errorCode == 0)
                    {
                        result.Success = true;
                        result.AccessToken = "Refraction correction is set to off";
                    }
                    else
                    {
                        result.Success = false;
                        result.ErrorMessage = ErrorCode(errorCode);
                        result.AccessToken = "Refraction correction error: " + result.ErrorMessage.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Refraction correction error: " + result.ErrorMessage.ToString();
            }

            return result;
        }
        public Result SetPrismCorrectionMM(ref double prismCorrection) //Change constante prisme
        {
            Result result = new Result();
            try
            {
                short errorCode = GeoCom105_dll.VB_TMC_SetPrismCorr(ref prismCorrection);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = string.Format("Turned prism correction to {0} MM", Math.Round(prismCorrection, 2).ToString());
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Prism correction error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Prism correction error: " + result.ErrorMessage;
            }

            return result;
        }
        public Result GetPrismCorrectionMM(out double prismCorrection) //Donne constante prisme active
        {
            Result result = new Result();
            try
            {
                prismCorrection = 1000;
                short errorCode = GeoCom105_dll.VB_TMC_GetPrismCorr(ref prismCorrection);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = string.Format("Active prism correction = {0} MM", Math.Round(prismCorrection, 2).ToString());
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Prism correction error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Prism correction error: " + result.ErrorMessage;
                prismCorrection = 1000;
            }
            return result;
        }
        public Result GetAverageDistance(int numberOfDistance, out DoubleValue averageSlopeDistance)
        {
            Result result = new Result();
            averageSlopeDistance = new DoubleValue();
            if (numberOfDistance <= 0)
            {
                averageSlopeDistance.Value = 0;
                averageSlopeDistance.Sigma = 0;
                result.Success = false;
                //   result.ErrorMessage = "Cannot measure negative or zero distance";
                //   result.AccessToken = "Distance error: Number of distance must be > 0";
                result.ErrorMessage = R.T_CANNOT_MEASURE_NEGATIVE_OR_ZERO_DISTANCE;
                result.AccessToken = R.T_DISTANCE_ERROR_NUMBER_OF_MEASURES_MUST_BE;
                return result;
            }
            double[] slopeDistance = new double[numberOfDistance];
            for (int i = 0; i < numberOfDistance; i++)
            {
                Angles dummy = null;
                result.MergeString(GetOneDistance(ref slopeDistance[i], ref dummy));

                if (result.Success == false) { averageSlopeDistance.Value = 0; averageSlopeDistance.Sigma = 0; return result; }
            }
            EmqCalculation(slopeDistance, out averageSlopeDistance);
            return result;
        }

        public Result GetAllMeasurement2(int numberOfMeasure, ref DoubleValue distance, ref Angles angles)
        {
            try
            {
                Result r = new Result();
                r = MeasureAngle(ref angles);
                ///Refait une 2e mesures angles si la premi�re est rat�e
                if (!r.Success)
                {
                    r = new Result();
                    r = MeasureAngle(ref angles);
                }
                if (!r.Success) throw new Exception("Angle acquisition failed");
                if (DistanceWanted)
                {
                    double d = double.NaN;

                    r = GetOneDistance(ref d, ref angles);

                    if (!r.Success) throw new Exception("Distance acquisition failed");
                    distance = new DoubleValue(d, double.NaN);
                }
                else
                    distance = null;
                return new Result() { AccessToken = R.T_OK, Success = true };
            }
            catch (Exception ex)
            {
                throw new Exception("TPS acquisition Failed. " + ex.Message);
            }
        }
        public override Result Connect()
        {
            Result result = new Result();
            if (this.Tcra1100._PortCom != "")
            {
                string portCom = this.Tcra1100._PortCom;
                this.listPortIndex = this.listPortName.FindIndex(x => x == portCom);
                if (listPortIndex == -1)
                {
                    result.Success = false;
                    result.ErrorMessage = R.StringTCRA1101_NoSelectedPortCom;
                    return result;
                }
                try
                {
                    result.MergeString(InitializeGeocom());
                    result.MergeString(OpenConnection(portCom));
                    if (result.Success)
                    {
                        isConnected = true;
                        //result.MergeString(GetSerialNumber());
                    }
                    else
                    {
                        isConnected = false;
                        result.Success = true;
                        this.TryConnectOthersPorts();
                    }
                }
                catch (Exception e)
                {
                    e.Data.Clear();
                    EndGeocom();
                    result.Success = false;
                    isConnected = false;
                    result.MergeString(new Result() { ErrorMessage = R.StringTCRA1101_ConnectionFailed + this.Tcra1100._PortCom });
                }
            }
            else
            {
                result.Success = false;
                result.ErrorMessage = R.StringTCRA1101_NoSelectedPortCom;
            }
            return result;
        }
        public Result MoveTo()
        {
            throw new NotImplementedException();
        }
        public Result SetEDMModeToStandard() //Met sur le mode EDM sur mesure standard
        {
            Result result = new Result();
            try
            {
                int edmMode = (int)GeoCom105_dll.EDM_MODE.EDM_SINGLE_STANDARD;
                short errorCode = GeoCom105_dll.VB_TMC_SetEdmMode(ref edmMode);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "EDM Mode sets to single standard measurement.";
                    this.GetATRStatus();
                    this.GetTargetLOCKStatus();
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
                }
                result.MergeString(SetATROnOff(this.Tcra1100._ATR));

                if (result.Success)
                { this.Tcra1100.ReflectorLessMode = false; }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
            }
            return result;
        }
        public Result SetEDMModeToTape() //Met sur le mode EDM sur mesure sur cible retro
        {
            Result result = new Result();
            try
            {
                int edmMode = (int)GeoCom105_dll.EDM_MODE.EDM_SINGLE_TAPE;
                short errorCode = GeoCom105_dll.VB_TMC_SetEdmMode(ref edmMode);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "EDM Mode sets to single measurement on tape.";
                    this.GetATRStatus();
                    this.GetTargetLOCKStatus();
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
                }
                result.MergeString(SetATROnOff(false));
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
            }

            return result;
        }
        public Result SetToReflectorLess() //Met sur le mode sans reflecteur
        {
            Result result = new Result();
            try
            {
                GeoCom105_dll.ModeMesure = GeoCom105_dll.IR_Or_RL.RL;
                result.MergeString(SetEDMModeToRL());
                this.GetATRStatus();
                this.GetTargetLOCKStatus();
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
            }

            return result;
        }
        public Result SetToWithReflector() //Met sur le mode avec reflecteur
        {
            Result result = new Result();
            try
            {
                GeoCom105_dll.ModeMesure = GeoCom105_dll.IR_Or_RL.EDM;
                result.MergeString(SetEDMModeToStandard());
                this.GetATRStatus();
                this.GetTargetLOCKStatus();
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
            }
            return result;
        }

        public Result SetToWithReflectiveTarget() //Met sur le mode avec reflecteur
        {
            Result result = new Result();
            try
            {
                GeoCom105_dll.ModeMesure = GeoCom105_dll.IR_Or_RL.EDM;
                result.MergeString(SetEDMModeToTape());
                this.GetATRStatus();
                this.GetTargetLOCKStatus();
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
            }
            return result;
        }

        public Result SetRedLaserOnOff(bool On) //Met sur on ou off le laser rouge TPS
        {
            Result result = new Result();
            try
            {
                int onOff = 0;
                //First desactivate laser in case the TCRA1100 has done a auto power off (recomanded by Geocom manual)
                GeoCom105_dll.VB_EDM_Laserpointer(ref onOff);
                if (On == true)
                {
                    onOff = (int)GeoCom105_dll.ON_OFF_TYPE.ON;
                }
                else
                {
                    onOff = (int)GeoCom105_dll.ON_OFF_TYPE.OFF;
                }

                short errorCode = GeoCom105_dll.VB_EDM_Laserpointer(ref onOff);
                if (errorCode == 0)
                {
                    result.Success = true;
                    //result.AccessToken = "Target Lock set to " + ((ON_OFF_TYPE)Convert.ChangeType(onOff, typeof(ON_OFF_TYPE))).ToString() + "\r\n";
                    result.AccessToken = "Red Laser is " + ((GeoCom105_dll.ON_OFF_TYPE)onOff).ToString();
                    switch (onOff)
                    {
                        case 0:
                            this.Tcra1100._RedLaser = false;
                            this.Tcra1100._UserRedLaserPreference = false;
                            break;
                        case 1:
                            this.Tcra1100._RedLaser = true;
                            this.Tcra1100._UserRedLaserPreference = true;
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Red Laser Error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Red Laser Error: " + result.ErrorMessage;
            }

            return result;
        }
        #endregion

        #region PolarModule implementationg (overrideing)

        public override void ChangeFace()
        {
            base.ChangeFace();
            this.InvertFace();
        }
        public override void CheckIncomingMeasure(Polar.Measure m)
        {
            base.CheckIncomingMeasure(m);

        }

        public override FaceType GetFace()
        {
            if (this.GetTCRAFace().Success)
            {
                switch (this.Tcra1100._Face)
                {
                    case 1: return FaceType.Face1;
                    case 2: return FaceType.Face2;
                    default:
                        return FaceType.UnknownFace;
                }
            }
            else
            {
                return FaceType.UnknownFace;
            }
        }

        public override bool IsReadyToMeasure()
        {
            if (!this.InstrumentIsOn) throw new Exception("Instrument is Off?");
            if (this.ParentInstrumentManager.ParentModule is TSU.Polar.Station.Module)
            {
                if (BeingMeasured == null)
                {
                    if (ToBeMeasuredData == null)
                    {
                        MMMM.Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(this.WaitMeasurementDetails);
                    }
                    else
                    {
                        if ((ToBeMeasuredData as Polar.Measure).Distance.Reflector == null || (ToBeMeasuredData as Polar.Measure).Distance.Reflector._Name == R.String_Unknown)
                            throw new Exception(R.T_No_REFLECTOR);

                        //// check measurement mode is ok with the refector
                        //if (this._ToBeMeasureTheodoliteData.Distance.Reflector._InstrumentType == InstrumentTypes.LASER)
                        //{
                        //    if (!this.Tcra1100.ReflectorLessMode)
                        //        this.SetToReflectorLess();
                        //}
                        //else
                        //{
                        //    if (this.Tcra1100.ReflectorLessMode)
                        //        this.SetToWithReflector();
                        //}
                    }
                }
            }
            return true;
        }


        public override Result Measure()
        {
            this.InvokeOnApplicationDispatcher(this.View.LaunchModuleMeasurement);
            return tempResult;
        }
        public Result MeasureTCRA()
        {
            Result r = new Result();
            this.MeasureWanted = true;
            r.MergeString(base.Measure());
            this.MeasureWanted = false;
            return r;
        }
        public override void MeasureLikeThis()
        {
            base.MeasureLikeThis();

            try
            {
                //Force la remise ON du compensateur et de la correction des angles avant chaque mesure pour �viter qu'un utilisateur les d�sactive dans l'instrument.
                this.SetAngleSwitchOn();
                this.SetInclineSwitchOn();
                // 1. Face and goto
                this.ChangeFaceAndDoGoto();

                if (this.IsFirstMesaureOfFirstFace)
                {
                    if (this.CancelByMessageBeforeMeasure())
                    {
                        this.BeingMeasured = null;
                        denyMeasurement = true;
                        throw new CancelException();
                    }
                }

                //MeasureTheodolite m = this._BeingMeasuredTheodoliteData;
                Result result = new Result();
                Angles rawAngles = new Angles();
                DoubleValue rawDistance = new DoubleValue();
                if (this.DistanceWanted)
                    result = this.GetAllMeasurement2(1, ref rawDistance, ref rawAngles);
                else
                {
                    result = this.GetAverageAngle(out rawAngles, 1);

                }

                if (beingMeasured._Status is M.States.Cancel)
                    throw new CancelException();

                //   if (!result.Success) throw new Exception("Could not acquire, " + result.ErrorMessage);
                if (!result.Success) throw new Exception(R.T_COULD_NOT_ACQUIRE + result.ErrorMessage);

                // Convvert to TSU objects
                if (rawAngles.Horizontal.Value < 0) rawAngles.Horizontal.Value = rawAngles.Horizontal.Value + 400;
                if (rawAngles.Horizontal.Value >= 400) rawAngles.Horizontal.Value = rawAngles.Horizontal.Value - 400;

                // if (this._BeingMeasuredTheodoliteData.Face == FaceType.Face2) SurveyCompute.TransformToOppositeFace(rawAngles); only for at?

                Polar.Measure m = this._BeingMeasuredTheodoliteData;
                // Fill the measurementObject
                m._Date = DateTime.Now;
                m.Angles.Raw = rawAngles;
                if (DistanceWanted)
                {
                    m.Distance.Raw = rawDistance;
                    m.Distance.WeatherConditions.dryTemperature = temperature;
                    m.Distance.WeatherConditions.pressure = pressure;
                    m.Distance.WeatherConditions.humidity = (int)humidity;
                }
                else
                {
                    m.Distance.Raw = new DoubleValue(); // to eraser value that can be there from goto need
                }

                this.DoMeasureCorrectionsThenNextMeasurementStep(m);

                //int precisionDigitsA = (TSU.Tsunami2.TsunamiPreferences.Values.GuiPrefs == null) ? 5 :TSU.Tsunami2.TsunamiPreferences.Values.GuiPrefs.NumberOfDecimalForAngles;
                //int precisionDigitsD = (TSU.Tsunami2.TsunamiPreferences.Values.GuiPrefs == null) ? 5 :TSU.Tsunami2.TsunamiPreferences.Values.GuiPrefs.NumberOfDecimalForDistances;

                //string mess = string.Format("Hz={0}gon, Vt={1}gon D={2}m, ext={3}m",
                //    m.Angles.Corrected.Horizontal.ToString(precisionDigitsA),
                //    m.Angles.Corrected.Vertical.ToString(precisionDigitsA),
                //    m.Distance.Corrected.ToString(precisionDigitsD),
                //    m.Extension.ToString(precisionDigitsD)
                //    );

                //Logs.Log.AddEntryAsFinishOf(this, mess);

            }
            catch (CancelException)
            {
                denyMeasurement = true;
                this.StatusChange(string.Format(R.T_CANCEL), P.StepTypes.EndAll);
                this.DeclareMeasurementCancelled();
            }
            catch (Exception e)
            {
                this.JustMeasuredData = this.BeingMeasured;
                //this.LastMessage = "Measurement failed: " + e.Message;
                this.StatusChange(string.Format(this.LastMessage), P.StepTypes.EndAll);
                this.HaveFailed();
                this.State = new InstrumentState(InstrumentState.type.Idle);
                throw new Exception(e.Message);
            }
        }

        //public override void MeasureNexTSUbMeasure()
        //{
        //    base.MeasureNexTSUbMeasure();
        //}

        internal override void OnMeasureToDoReceived()
        {
            base.OnMeasureToDoReceived();

            if (ToBeMeasuredData != null)
            {
                // Check if reflector defined;
                if ((ToBeMeasuredData as Polar.Measure).Distance.Reflector == null || (ToBeMeasuredData as Polar.Measure).Distance.Reflector._Name == R.String_Unknown)
                {
                    Logs.Log.AddEntryAsPayAttentionOf(this, R.T_No_REFLECTOR);
                    this.InvokeOnApplicationDispatcher(this.View.DontAllowAll);
                    this.InvokeOnApplicationDispatcher(this.View.DontAllowGoto);
                    this.View.UpdateView();
                }
                else
                {
                    if (this.previousReflector != (ToBeMeasuredData as Polar.Measure).Distance.Reflector._InstrumentType && this.InstrumentIsOn)
                    {
                        this.InvokeOnApplicationDispatcher(this.View.ChangeReflector);
                    }
                    // Set the number of measure
                    //(this.View as View).Text = (_ToBeMeasuredData as MeasureTheodolite).NumberOfMeasureToAverage.ToString();
                    if (this.InstrumentIsOn) this.InvokeOnApplicationDispatcher(this.View.AllowAll);
                    else this.InvokeOnApplicationDispatcher(this.View.DontAllowAll);
                    // allow goto
                    if ((ToBeMeasuredData as Polar.Measure).HasCorrectedAngles && this.InstrumentIsOn)
                        this.InvokeOnApplicationDispatcher(this.View.AllowGoto);
                    else
                        this.InvokeOnApplicationDispatcher(this.View.DontAllowGoto);

                    if (ToBeMeasuredData._Status is M.States.Good)// this is the way to say 'i want  a measure now' without clicking gotoall button 
                    {
                        if (base.ToBeMeasuredData._Status.PreviousType == M.States.Types.Questionnable)
                            base.ToBeMeasuredData._Status = new M.States.Questionnable();
                        else
                            base.ToBeMeasuredData._Status = new M.States.Unknown();

                        GotoWanted = true;
                        this.View.LaunchMeasurement();
                    }
                }
            }
            else
            {
                this.InvokeOnApplicationDispatcher(this.View.DontAllowAll);
                this.InvokeOnApplicationDispatcher(this.View.DontAllowGoto);
            }
            //// to make it from the UI thread
            //TsunamiPreferences.Values.InvokeOnApplicationDispatcher(() =>
            //{
            //    MeasureTheodolite toBe = (_ToBeMeasuredData as MeasureTheodolite);

            //    if (_InstrumentIsOn)
            //    {

            //        if (toBe.Distance.Reflector == null || toBe.Distance.Reflector._Name == R.String_Unknown)
            //        {
            //            Logs.Log.AddEntryAsPayAttentionOf(this, string.Format("{0} {1}", R.T_No_REFLECTOR, "!!!"));
            //        }
            //        else
            //        {
            //            ChangeModeBasedOnReflector();
            //        }
            //    }
            //    this.CheckAllowGoto();
            //});

        }
        /// <summary>
        /// Change le reflecteur par d�faut de la station
        /// </summary>
        internal void ChangeDefaultReflector()
        {
            if (this.StationTheodoliteModule != null)
            {
                this.StationTheodoliteModule.Reflector(this.StationTheodoliteModule.stationParameters.DefaultMeasure);
            }
        }
        /// <summary>
        /// Change reflector type inside the TS60
        /// </summary>
        /// <returns></returns>
        public Result ChangeReflector()
        {
            Result r = new Result();
            if (ToBeMeasuredData != null)
            {
                this.previousReflector = (ToBeMeasuredData as Polar.Measure).Distance.Reflector._InstrumentType;
                double d = 0;
                double a = 100;
                switch ((ToBeMeasuredData as Polar.Measure).Distance.Reflector._InstrumentType)
                {
                    case InstrumentTypes.CCR1_5:
                        //r.MergeString(this.SetEDMModeToStandard());
                        r.MergeString(this.SetToWithReflector());
                        r.MergeString(this.SetPrismCorrectionMM(ref d));
                        r.MergeString(this.GetPrismCorrectionMM(out a));
                        r.MergeString(this.SetATROnOff(this.Tcra1100._ATR));
                        r.MergeString(this.SetTargetLOCKOnOff(this.Tcra1100._Lock));
                        break;
                    case InstrumentTypes.CSCOTCH:
                        r.MergeString(this.SetToWithReflectiveTarget());
                        //r.MergeString(this.SetToWithReflector());
                        //Check if need 34.4mm or 0mm in case target is in etalo.dat
                        r.MergeString(this.SetPrismCorrectionMM(ref d));
                        r.MergeString(this.GetPrismCorrectionMM(out a));
                        r.MergeString(this.SetRedLaserOnOff(this.Tcra1100._UserRedLaserPreference));
                        break;
                    case InstrumentTypes.LASER:
                        //r.MergeString(this.SetEDMModeToRL());
                        r.MergeString(this.SetToReflectorLess());
                        //Check if need 34.4mm or 0mm in case target is in etalo.dat
                        r.MergeString(this.SetPrismCorrectionMM(ref d));
                        r.MergeString(this.GetPrismCorrectionMM(out a));
                        r.MergeString(this.SetRedLaserOnOff(this.Tcra1100._UserRedLaserPreference));
                        break;
                    default:
                        //r.MergeString(this.SetEDMModeToStandard());
                        r.MergeString(this.SetToWithReflector());
                        r.MergeString(this.SetPrismCorrectionMM(ref d));
                        r.MergeString(this.GetPrismCorrectionMM(out a));
                        r.MergeString(this.SetATROnOff(this.Tcra1100._ATR));
                        r.MergeString(this.SetTargetLOCKOnOff(this.Tcra1100._Lock));
                        break;
                }
            }
            else
            {
                r.Success = false;
            }
            return r;
        }

        //internal void CheckAllowGoto()
        //{
        //    if (this.toBeMeasuredData != null)
        //    {
        //        MeasureTheodolite tobe = this.toBeMeasuredData as MeasureTheodolite;
        //        if (tobe != null)
        //        {
        //            this.allowGoto = tobe.HasCorrectedAngles;
        //        }
        //        else this.allowGoto = false;
        //    }
        //    else this.allowGoto = false;
        //    this.InvokeOnApplicationDispatcher(this.View.AllowGoto);
        //}

        //private void ChangeModeBasedOnReflector()
        //{
        //    MeasureTheodolite toBe = (_ToBeMeasuredData as MeasureTheodolite);

        //    if (toBe.Distance.Reflector._InstrumentType == InstrumentTypes.LASER)
        //    {
        //        Result r = null;
        //        TsuTask t = new TsuTask()
        //        {
        //            mainAction = delegate
        //            {
        //                r = SetToReflectorLess();
        //            },
        //            postAction = delegate
        //            {
        //                Log.AddEntryAsResult(this, r);
        //                if (r.Success) this.InvokeOnApplicationDispatcher(View.ShowReflectorLess);
        //            },
        //            ActionName = "Switch to Reflector Less Mode",
        //            estimatedTimeInMilliSec = 2000
        //        };
        //        View.RunTasksWithWaitingMessage(t);
        //    }
        //    else if (toBe.Distance.Reflector._InstrumentType == InstrumentTypes.CSCOTCH)
        //    {
        //        Result r1 = null;
        //        Result r2 = null;
        //        bool atr = false;
        //        TsuTask t = new TsuTask()
        //        {
        //            mainAction = delegate
        //            {
        //                r1 = SetToWithReflector();
        //                r2 = SetATROnOff(atr);
        //            },
        //            postAction = delegate
        //            {
        //                Log.AddEntryAsResult(this, r1);
        //                if (r1.Success) this.InvokeOnApplicationDispatcher(View.ShowWithReflector);
        //                if (r2.Success) this.InvokeOnApplicationDispatcher(View.AtrStateImage);
        //                this.InvokeOnApplicationDispatcher(View.RedLaserStateImage);
        //            },
        //            ActionName = "Switch to ATR off for reflective target",
        //            estimatedTimeInMilliSec = 2000
        //        };
        //        View.RunTasksWithWaitingMessage(t);
        //    }
        //    else if (toBe.Distance.Reflector._InstrumentType == InstrumentTypes.CHAINE)
        //    {
        //        Result r = null;
        //        TsuTask t = new TsuTask()
        //        {
        //            mainAction = delegate
        //            {
        //                r = SetEDMModeToTape();
        //            },
        //            postAction = delegate
        //            {
        //                Log.AddEntryAsResult(this, r);
        //                if (r.Success) this.InvokeOnApplicationDispatcher(View.ShowWithReflector);
        //                this.InvokeOnApplicationDispatcher(View.AtrStateImage);
        //                this.InvokeOnApplicationDispatcher(View.RedLaserStateImage);
        //            },
        //            ActionName = "Switch to Reflector Less Mode with Measureing Tape",
        //            estimatedTimeInMilliSec = 2000
        //        };
        //        View.RunTasksWithWaitingMessage(t);
        //    }
        //    else
        //    {
        //        Result r = null;
        //        TsuTask t = new TsuTask()
        //        {
        //            mainAction = delegate
        //            {
        //                r = SetToWithReflector();
        //            },
        //            postAction = delegate
        //            {
        //                Log.AddEntryAsResult(this, r);
        //                if (r.Success) this.InvokeOnApplicationDispatcher(View.ShowWithReflector);
        //                this.InvokeOnApplicationDispatcher(View.AtrStateImage);
        //                this.InvokeOnApplicationDispatcher(View.RedLaserStateImage);
        //            },
        //            ActionName = "Switch to 'With Reflector' Mode",
        //            estimatedTimeInMilliSec = 2000
        //        };
        //        View.RunTasksWithWaitingMessage(t);
        //    }
        //}

        //private void SetMeasureReflectorToLaser(MeasureTheodolite m)
        //{
        //    m.Distance.Reflector =TSU.Tsunami2.TsunamiPreferences.Values.Reflectors.Find(x => x._InstrumentType == InstrumentTypes.LASER);
        //}

        //public override void Goto()
        //{
        //    base.Goto();
        //    MeasureTheodolite m = _BeingMeasuredTheodoliteData;
        //    if (m == null) m = _ToBeMeasureTheodoliteData;
        //    if (m == null) throw new Exception(R.T_NOWHERE_TO_GOTO);
        //    if (m.HasCorrectedAngles)
        //    {
        //        this.GetAngleForGoodFaceGoto(m, out Angles a);
        //        Result r = this.MoveTo(a.Horizontal.Value, a.Vertical.Value);
        //        if (!r.Success)
        //            throw new Exception($"{R.T_GOTO_IMPOSSIBLE};{r.ErrorMessage}");
        //    }
        //    else
        //        throw new Exception(R.T_GOTO_IMPOSSIBLE);
        //}
        public override void Goto()
        {
            //if the gotowanted = true, mean that the function has been called by measure in this module 
            if (this.GotoWanted)
                this.GotoTCRA();
            else
                this.InvokeOnApplicationDispatcher(this.View.Goto);
        }
        public void GotoTCRA()
        {
            base.Goto();
            try
            {
                Result r = new Result();
                Polar.Measure m = this._BeingMeasuredTheodoliteData;
                if (m == null) m = this._ToBeMeasureTheodoliteData;
                if (m == null) throw new Exception(R.T_NOWHERE_TO_GOTO);
                this.GetAngleForGoodFaceGoto(m, out Angles a);
                r.MergeString(MoveTo(a.Horizontal.Value, a.Vertical.Value));
                this.tempResult = r;
                if (!this.MeasureWanted) this.Ready();
            }
            catch (Exception ex)
            {
                if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.IsBusyCommunicating = false;
                throw;
            }
        }
        internal override bool Lock()
        {
            return base.Lock();
        }

        public void LockUnLock()
        {
            int onOff = -1;
            short errorCode = GeoCom32_dll.VB_AUT_GetLockStatus(ref onOff);
            if (onOff == 0)
                SetTargetLOCKOnOff(true);
            else
                SetTargetLOCKOnOff(false);
        }

        public Result CheckATRLock()
        {
            Result resultGlobal = new Result();
            resultGlobal.MergeString(this.GetATRStatus());
            resultGlobal.MergeString(this.GetTargetLOCKStatus());
            return resultGlobal;
        }
        internal void CheckAndWaitIfBusy()
        {
            DateTime start = DateTime.Now;
            start = start.AddMilliseconds(3000);
            //this.TimerWaitForCommunication.Enabled = true;
            //this.TimerWaitForCommunication.Start();
            while (DateTime.Now < start) ///Lance le timer et regarde  durant 2 secondes que la communication se lib�re.
            {
                if (!this.IsBusyCommunicating)
                {
                    IsBusyCommunicating = true;
                    //this.TimerWaitForCommunication.Stop();
                    //this.TimerWaitForCommunication.Enabled = true;
                    //timerTry = 0;
                    return;
                }
            }
            throw new Exception(R.String_Exception_InstrumentIsBusy);
        }

        void IConnectable.Refresh()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
