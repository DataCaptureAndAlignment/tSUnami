﻿using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments.Device.TDA5005
{
    partial class View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (this.Module == null)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label_ComPort = new System.Windows.Forms.Label();
            this.buttonGotoAll = new System.Windows.Forms.Button();
            this.buttonGoto = new System.Windows.Forms.Button();
            this.label_numberOfMeas = new System.Windows.Forms.Label();
            this.textBox_numberOfMeas = new System.Windows.Forms.TextBox();
            this.button_All = new System.Windows.Forms.Button();
            this.button_Rec = new System.Windows.Forms.Button();
            this.button_Distance = new System.Windows.Forms.Button();
            this.comboBox_PortCom = new System.Windows.Forms.ComboBox();
            this.Button_OnOff = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelMeteo = new System.Windows.Forms.Label();
            this.button_face = new System.Windows.Forms.Button();
            this.label_Bulle = new System.Windows.Forms.Label();
            this.pictureBox_bulle = new System.Windows.Forms.PictureBox();
            this.pictureBox_Battery = new System.Windows.Forms.PictureBox();
            this.button_ATR = new System.Windows.Forms.Button();
            this.pictureBox_OnOff = new System.Windows.Forms.PictureBox();
            this.Timer_check = new System.Windows.Forms.Timer(this.components);
            this.timer_ComPort = new System.Windows.Forms.Timer(this.components);
            label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_bulle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Battery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_OnOff)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            label1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            label1.Location = new System.Drawing.Point(13, 6);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(66, 19);
            label1.TabIndex = 2;
            label1.Text = "TDA5005";
            label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.panel1.BackgroundImage = global::TSU.Properties.Resources.Tda5005_Background;
            this.panel1.Controls.Add(this.label_ComPort);
            this.panel1.Controls.Add(this.buttonGotoAll);
            this.panel1.Controls.Add(this.buttonGoto);
            this.panel1.Controls.Add(this.label_numberOfMeas);
            this.panel1.Controls.Add(this.textBox_numberOfMeas);
            this.panel1.Controls.Add(this.button_All);
            this.panel1.Controls.Add(this.button_Rec);
            this.panel1.Controls.Add(this.button_Distance);
            this.panel1.Controls.Add(this.comboBox_PortCom);
            this.panel1.Controls.Add(this.Button_OnOff);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(5, 50);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10, 10, 10, 10);
            this.panel1.Size = new System.Drawing.Size(616, 182);
            this.panel1.TabIndex = 3;
            // 
            // label_ComPort
            // 
            this.label_ComPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label_ComPort.AutoSize = true;
            this.label_ComPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ComPort.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.label_ComPort.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.label_ComPort.Location = new System.Drawing.Point(469, 131);
            this.label_ComPort.Name = "label_ComPort";
            this.label_ComPort.Size = new System.Drawing.Size(75, 20);
            this.label_ComPort.TabIndex = 19;
            this.label_ComPort.Text = "Com Port";
            // 
            // buttonGotoAll
            // 
            this.buttonGotoAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonGotoAll.BackgroundImage = global::TSU.Properties.Resources.Tda5005_Goto_All;
            this.buttonGotoAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonGotoAll.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.buttonGotoAll.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.buttonGotoAll.Location = new System.Drawing.Point(230, 129);
            this.buttonGotoAll.Name = "buttonGotoAll";
            this.buttonGotoAll.Size = new System.Drawing.Size(69, 47);
            this.buttonGotoAll.TabIndex = 18;
            this.buttonGotoAll.UseVisualStyleBackColor = true;
            this.buttonGotoAll.Click += new System.EventHandler(this.buttonGotoAll_Click);
            // 
            // buttonGoto
            // 
            this.buttonGoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonGoto.BackgroundImage = global::TSU.Properties.Resources.Tda5005_Goto;
            this.buttonGoto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonGoto.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.buttonGoto.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.buttonGoto.Location = new System.Drawing.Point(155, 130);
            this.buttonGoto.Name = "buttonGoto";
            this.buttonGoto.Size = new System.Drawing.Size(69, 47);
            this.buttonGoto.TabIndex = 17;
            this.buttonGoto.UseVisualStyleBackColor = true;
            this.buttonGoto.Click += new System.EventHandler(this.buttonGoto_Click);
            // 
            // label_numberOfMeas
            // 
            this.label_numberOfMeas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label_numberOfMeas.AutoSize = true;
            this.label_numberOfMeas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_numberOfMeas.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.label_numberOfMeas.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.label_numberOfMeas.Location = new System.Drawing.Point(428, 131);
            this.label_numberOfMeas.Name = "label_numberOfMeas";
            this.label_numberOfMeas.Size = new System.Drawing.Size(83, 20);
            this.label_numberOfMeas.TabIndex = 16;
            this.label_numberOfMeas.Text = "# of meas.";
            // 
            // textBox_numberOfMeas
            // 
            this.textBox_numberOfMeas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_numberOfMeas.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.textBox_numberOfMeas.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.textBox_numberOfMeas.Location = new System.Drawing.Point(517, 131);
            this.textBox_numberOfMeas.Name = "textBox_numberOfMeas";
            this.textBox_numberOfMeas.Size = new System.Drawing.Size(26, 20);
            this.textBox_numberOfMeas.TabIndex = 15;
            this.textBox_numberOfMeas.Text = "1";
            this.textBox_numberOfMeas.TextChanged += new System.EventHandler(this.textBox_numberOfMeas_TextChanged);
            // 
            // button_All
            // 
            this.button_All.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_All.BackgroundImage = global::TSU.Properties.Resources.Tda5005_ALL;
            this.button_All.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_All.Location = new System.Drawing.Point(5, 130);
            this.button_All.Name = "button_All";
            this.button_All.Size = new System.Drawing.Size(69, 46);
            this.button_All.TabIndex = 12;
            this.button_All.UseVisualStyleBackColor = true;
            this.button_All.Click += new System.EventHandler(this.button_All_Click);
            // 
            // button_Rec
            // 
            this.button_Rec.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_Rec.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_Rec.BackgroundImage = global::TSU.Properties.Resources.Tda5005_HzV;
            this.button_Rec.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Rec.Location = new System.Drawing.Point(80, 130);
            this.button_Rec.Name = "button_Rec";
            this.button_Rec.Size = new System.Drawing.Size(69, 46);
            this.button_Rec.TabIndex = 10;
            this.button_Rec.UseVisualStyleBackColor = false;
            this.button_Rec.Click += new System.EventHandler(this.button_Rec_Click);
            // 
            // button_Distance
            // 
            this.button_Distance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_Distance.BackgroundImage = global::TSU.Properties.Resources.Tda5005_Dist;
            this.button_Distance.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Distance.Location = new System.Drawing.Point(305, 129);
            this.button_Distance.Name = "button_Distance";
            this.button_Distance.Size = new System.Drawing.Size(69, 47);
            this.button_Distance.TabIndex = 8;
            this.button_Distance.UseVisualStyleBackColor = true;
            this.button_Distance.Click += new System.EventHandler(this.button_Distance_Click);
            // 
            // comboBox_PortCom
            // 
            this.comboBox_PortCom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_PortCom.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.comboBox_PortCom.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.comboBox_PortCom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_PortCom.FormattingEnabled = true;
            this.comboBox_PortCom.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4"});
            this.comboBox_PortCom.Location = new System.Drawing.Point(473, 155);
            this.comboBox_PortCom.MaxDropDownItems = 4;
            this.comboBox_PortCom.Name = "comboBox_PortCom";
            this.comboBox_PortCom.Size = new System.Drawing.Size(71, 21);
            this.comboBox_PortCom.TabIndex = 4;
            // 
            // Button_OnOff
            // 
            this.Button_OnOff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_OnOff.BackgroundImage = global::TSU.Properties.Resources.Tda5005_OnOff;
            this.Button_OnOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_OnOff.Location = new System.Drawing.Point(549, 131);
            this.Button_OnOff.Name = "Button_OnOff";
            this.Button_OnOff.Size = new System.Drawing.Size(64, 46);
            this.Button_OnOff.TabIndex = 3;
            this.Button_OnOff.UseVisualStyleBackColor = true;
            this.Button_OnOff.Click += new System.EventHandler(this.Button_OnOff_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel2.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.panel2.Location = new System.Drawing.Point(5, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(605, 119);
            this.panel2.TabIndex = 2;
            // 
            // labelMeteo
            // 
            this.labelMeteo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMeteo.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
            this.labelMeteo.Location = new System.Drawing.Point(377, 5);
            this.labelMeteo.Name = "labelMeteo";
            this.labelMeteo.Size = new System.Drawing.Size(71, 41);
            this.labelMeteo.TabIndex = 27;
            this.labelMeteo.Text = "T° 20.0C       P 980hPa         H 60%";
            this.labelMeteo.Click += new System.EventHandler(this.labelMeteo_Click);
            // 
            // button_face
            // 
            this.button_face.AutoSize = true;
            this.button_face.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_face.FlatAppearance.BorderSize = 0;
            this.button_face.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_face.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_face.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_face.Image = global::TSU.Properties.Resources.Tda5005_CercleGauche;
            this.button_face.Location = new System.Drawing.Point(85, 3);
            this.button_face.Name = "button_face";
            this.button_face.Size = new System.Drawing.Size(46, 46);
            this.button_face.TabIndex = 26;
            this.button_face.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button_face.UseVisualStyleBackColor = true;
            this.button_face.Click += new System.EventHandler(this.button_face_Click);
            // 
            // label_Bulle
            // 
            this.label_Bulle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_Bulle.AutoSize = true;
            this.label_Bulle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Bulle.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.label_Bulle.Location = new System.Drawing.Point(502, 6);
            this.label_Bulle.Name = "label_Bulle";
            this.label_Bulle.Size = new System.Drawing.Size(26, 13);
            this.label_Bulle.TabIndex = 20;
            this.label_Bulle.Text = "L & T";
            // 
            // pictureBox_bulle
            // 
            this.pictureBox_bulle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox_bulle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox_bulle.Image = global::TSU.Properties.Resources.Tda5005_Bulle_Bad;
            this.pictureBox_bulle.Location = new System.Drawing.Point(454, 6);
            this.pictureBox_bulle.Name = "pictureBox_bulle";
            this.pictureBox_bulle.Size = new System.Drawing.Size(42, 40);
            this.pictureBox_bulle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_bulle.TabIndex = 19;
            this.pictureBox_bulle.TabStop = false;
            // 
            // pictureBox_Battery
            // 
            this.pictureBox_Battery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox_Battery.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox_Battery.Image = global::TSU.Properties.Resources.Tda5005_Battery_Bad;
            this.pictureBox_Battery.Location = new System.Drawing.Point(556, 6);
            this.pictureBox_Battery.Name = "pictureBox_Battery";
            this.pictureBox_Battery.Size = new System.Drawing.Size(19, 40);
            this.pictureBox_Battery.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Battery.TabIndex = 18;
            this.pictureBox_Battery.TabStop = false;
            // 
            // button_ATR
            // 
            this.button_ATR.AutoSize = true;
            this.button_ATR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_ATR.FlatAppearance.BorderSize = 0;
            this.button_ATR.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_ATR.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_ATR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_ATR.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_ATR.Image = global::TSU.Properties.Resources.Tda5005_ATR_On;
            this.button_ATR.Location = new System.Drawing.Point(134, 1);
            this.button_ATR.Name = "button_ATR";
            this.button_ATR.Size = new System.Drawing.Size(48, 48);
            this.button_ATR.TabIndex = 14;
            this.button_ATR.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_ATR.UseVisualStyleBackColor = false;
            this.button_ATR.Click += new System.EventHandler(this.button_ATR_Click);
            // 
            // pictureBox_OnOff
            // 
            this.pictureBox_OnOff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox_OnOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox_OnOff.Image = global::TSU.Properties.Resources.Tda5005_OFF;
            this.pictureBox_OnOff.Location = new System.Drawing.Point(581, 6);
            this.pictureBox_OnOff.Name = "pictureBox_OnOff";
            this.pictureBox_OnOff.Size = new System.Drawing.Size(40, 40);
            this.pictureBox_OnOff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_OnOff.TabIndex = 17;
            this.pictureBox_OnOff.TabStop = false;
            // 
            // Timer_check
            // 
            this.Timer_check.Interval = 5000;
            this.Timer_check.Tick += new System.EventHandler(this.Timer_check_TDA_Tick);
            // 
            // timer_ComPort
            // 
            this.timer_ComPort.Interval = 2000;
            this.timer_ComPort.Tick += new System.EventHandler(this.timer_ComPort_Tick);
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.ClientSize = new System.Drawing.Size(626, 235);
            this.Controls.Add(this.labelMeteo);
            this.Controls.Add(this.button_face);
            this.Controls.Add(this.panel1);
            this.Controls.Add(label1);
            this.Controls.Add(this.label_Bulle);
            this.Controls.Add(this.pictureBox_OnOff);
            this.Controls.Add(this.pictureBox_bulle);
            this.Controls.Add(this.pictureBox_Battery);
            this.Controls.Add(this.button_ATR);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "View";
            this.Padding = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "Tda5005View";
            this.Load += new System.EventHandler(this.Tda5005View_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_bulle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Battery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_OnOff)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button Button_OnOff;
        private System.Windows.Forms.ComboBox comboBox_PortCom;
        private System.Windows.Forms.Button button_Distance;
        private System.Windows.Forms.Button button_Rec;
        private System.Windows.Forms.Button button_All;
        private System.Windows.Forms.Button button_ATR;
        private System.Windows.Forms.Timer Timer_check;
        public System.Windows.Forms.TextBox textBox_numberOfMeas;
        private System.Windows.Forms.Label label_numberOfMeas;
        private System.Windows.Forms.PictureBox pictureBox_OnOff;
        private System.Windows.Forms.PictureBox pictureBox_bulle;
        private System.Windows.Forms.PictureBox pictureBox_Battery;
        private System.Windows.Forms.Label label_Bulle;
        private System.Windows.Forms.Button button_face;
        private System.Windows.Forms.Label labelMeteo;
        private System.Windows.Forms.Button buttonGoto;
        private System.Windows.Forms.Button buttonGotoAll;
        private System.Windows.Forms.Label label_ComPort;
        private System.Windows.Forms.Timer timer_ComPort;
        private System.Windows.Forms.Label label1;
    }
}