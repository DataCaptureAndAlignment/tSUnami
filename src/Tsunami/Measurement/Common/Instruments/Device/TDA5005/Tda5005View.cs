﻿using System;
using System.Collections.Generic;
using System.Linq;
using TSU.Views;
using TSU.Views.Message;
using R = TSU.Properties.Resources;


namespace TSU.Common.Instruments.Device.TDA5005
{
    public partial class View : Device.View
    {
        private new Module Module
        {
            get
            {
                return this._Module as Module;
            }
        }
        private Instrument _Tda5005
        {
            get
            {
                return this.Module.Instrument as Instrument;
            }
        }
        ///private bool doingTimerThings = false;
        private List<TsuTask> TaskToDo = new List<TsuTask>();
        private TsuBool ShowMsgATRFailed = new TsuBool();
        private System.Windows.Forms.ToolTip toolTip1;

        public View(Instruments.Module instrumentModule)
            : base(instrumentModule)
        {
            InitializeComponent();
            this.ApplyThemeColors();
            comboBox_PortCom.MouseDoubleClick += delegate { System.Diagnostics.Process.Start("devmgmt.msc"); };
            InitializeLog();
        }

        private void InitializeLog()
        {
            LogView = new Logs.LogView();
            LogView.ShowDockedFill();
            this.panel2.Controls.Add(this.LogView);
        }

        private void ApplyThemeColors()
        {
            label1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            label1.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.panel1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.label_ComPort.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.label_ComPort.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.buttonGotoAll.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.buttonGotoAll.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.buttonGoto.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.buttonGoto.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.label_numberOfMeas.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.label_numberOfMeas.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.textBox_numberOfMeas.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.textBox_numberOfMeas.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button_All.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_Rec.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_Distance.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_Rec.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.comboBox_PortCom.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.comboBox_PortCom.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.Button_OnOff.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.panel2.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.labelMeteo.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
            this.button_face.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_face.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_face.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.label_Bulle.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.label_Bulle.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.pictureBox_bulle.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.pictureBox_Battery.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_ATR.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_ATR.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_ATR.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_ATR.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.pictureBox_OnOff.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
        }
        private void Tda5005View_Load(object sender, EventArgs e)
        {
            labelMeteo.Text = labelMeteo.Text = string.Format("T° {0}C\r\nP {1}hPa\r\nH {2}%", this.Module.temperature, this.Module.pressure, this.Module.humidity);
            labelMeteo.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
            this.SetAllToolTips();
            ViewTdaOff();
        }
        private void ViewTdaOn()
        {
            button_Rec.Visible = false;
            button_Distance.Visible = false; //Comme TS60 pas de mesure distance seule
            button_All.Visible = false;
            buttonGoto.Visible = false;
            buttonGotoAll.Visible = false;
            label_numberOfMeas.Visible = false; //mis dans le module polaire
            label_Bulle.Visible = false; //bulle ne fonctionne pa
            pictureBox_Battery.Visible = true;
            pictureBox_bulle.Visible = false; //bulle ne fonctionne pas
            Timer_check.Enabled = true;
            textBox_numberOfMeas.Visible = false; //mis dans le module polaire
            button_ATR.Visible = true;
            button_face.Visible = true;
            Module.InstrumentIsOn = true;
            this.pictureBox_OnOff.Image = R.Tda5005_ON;
            this.pictureBox_bulle.Image = R.Tda5005_Bulle_Bad;
            this.comboBox_PortCom.Visible = false;
            this.label_ComPort.Visible = false;
            timer_ComPort.Enabled = false;
            this.Module.OnMeasureToDoReceived();
        }

        public void AllowGoto()
        {
            buttonGoto.Visible = true;
            buttonGotoAll.Visible = true;
        }

        public void DontAllowGoto()
        {
            buttonGoto.Visible = false;
            buttonGotoAll.Visible = false;
        }
        internal void DontAllowAll()
        {
            button_All.Visible = false;
            button_Rec.Visible = false;
        }
        internal void AllowAll()
        {
            button_All.Visible = true;
            button_Rec.Visible = true;
        }
        private void ViewTdaOff()
        {
            button_Rec.Visible = false;
            button_Distance.Visible = false;
            button_All.Visible = false;
            buttonGoto.Visible = false;
            buttonGotoAll.Visible = false;
            label_numberOfMeas.Visible = false;
            label_Bulle.Visible = false;
            pictureBox_Battery.Visible = false;
            pictureBox_bulle.Visible = false;
            button_face.Visible = false;
            Timer_check.Enabled = false;
            textBox_numberOfMeas.Visible = false;
            button_ATR.Visible = false;
            Module.InstrumentIsOn = false;
            this.pictureBox_OnOff.Image = R.Tda5005_OFF;
            this.pictureBox_bulle.Image = R.Tda5005_Bulle_Bad;
            GetAllPorts();
            this.comboBox_PortCom.Visible = true;
            this.label_ComPort.Visible = true;
            timer_ComPort.Enabled = true;
        }
        /// <summary>
        /// Get the com port available
        /// </summary>
        private void GetAllPorts()
        {
            string portSelected = this.comboBox_PortCom.Text;
            this.comboBox_PortCom.BeginUpdate();
            string[] allPort = this.Module.GetAllPorts();
            // pour éviter le clignotement ajouter et retirer que les ports nécessaires un à un
            for (int i = this.comboBox_PortCom.Items.Count - 1; i >= 0; i--)
            {
                if (!allPort.Contains(this.comboBox_PortCom.Items[i])) this.comboBox_PortCom.Items.RemoveAt(i);
            }
            foreach (string item in allPort)
            {
                if (!this.comboBox_PortCom.Items.Contains(item)) this.comboBox_PortCom.Items.Add(item);
            }
            //this.comboBox_PortCom.Items.AddRange(this.Module.GetAllPorts());
            if (this.comboBox_PortCom.Items.Count > 0)
            {
                if (!this.comboBox_PortCom.Items.Contains(portSelected))
                    this.comboBox_PortCom.Text = this.comboBox_PortCom.Items[0].ToString();
            }
            else this.comboBox_PortCom.Text = "";
            this.comboBox_PortCom.EndUpdate();
        }

        private void BatteryStateImage()
        {
            if (Module.Tda5005._Battery <= 11.4)
            {
                this.pictureBox_Battery.Image = R.Tda5005_Battery_Bad;
            }
            if (Module.Tda5005._Battery > 12.4)
            {
                this.pictureBox_Battery.Image = R.Tda5005_Battery_Good;
            }
            if ((Module.Tda5005._Battery <= 12.4) && (Module.Tda5005._Battery > 11.4))
            {
                this.pictureBox_Battery.Image = R.Tda5005_Battery_Average;
            }
        }
        private void BulleStateImage()
        {
            double diffGradBulle = Math.Sqrt(Math.Pow(Module.Tda5005._Compensator.longitudinalAxisInclination, 2) + Math.Pow(Module.Tda5005._Compensator.tranverseAxisInclination, 2)) * 200 / Math.PI;
            if (diffGradBulle > 0.002)
            {
                this.pictureBox_bulle.Image = R.Tda5005_Bulle_Bad;
            }
            if (diffGradBulle <= 0.001)
            {
                this.pictureBox_bulle.Image = R.Tda5005_Bulle_Good;
            }
            if ((diffGradBulle <= 0.002) && (diffGradBulle > 0.001))
            {
                this.pictureBox_bulle.Image = R.Tda5005_Bulle_Average;
            }
        }
        //private void faceimage(int face)
        //{
        //    if (face==1)
        //    {
        //        this.button_face.Image =R.Tda5005_CercleGauche;
        //    }
        //    else
        //    {
        //        this.button_face.Image =R.Tda5005_CercleDroit;
        //    }
        //}
        private void Button_OnOff_Click(object sender, EventArgs e)
        {
            try
            {
                if (Module.InstrumentIsOn == false)
                    TurnOn();
                else
                    TurnOff();
            }
            catch (Exception ex)
            {
                string titleAndMessage = "Connection/Disconnection failed;" + ex.Message;
                new MessageInput(MessageType.Critical, titleAndMessage).Show();
            }
        }
        private void TurnOff()
        {
            try
            {
                Result closeSuccess = new Result();

                TsuTask taskDeconnection = new TsuTask()
                {
                    ActionName = "Geocom deconnection",
                    estimatedTimeInMilliSec = 1000,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        closeSuccess = Module.Disconnect();
                    },
                    postAction = () =>
                    {
                        Logs.Log.AddEntryAsResult(this._Module, closeSuccess);
                        if (closeSuccess.Success == true)
                        {
                            this.Module.InvokeOnApplicationDispatcher(ViewTdaOff);
                        }
                        else
                        {
                            Timer_check.Enabled = true;
                            Timer_check.Start();
                        }
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    timeOutAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.ViewTdaOff();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { taskDeconnection }, "Deconnection");
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(taskDeconnection);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { taskDeconnection }, "Deconnection");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }
            // Do thing from the UI

        }

        private void TurnOn()
        {
            try
            {
                Result connecTSUccess = new Result();
                Result iniTSUccess = new Result();
                if (!this.Module.MeteoSet)
                {
                    this.Module.SetMeteoManually();
                    this.UpdateLabelMeteo();
                }
                TsuTask taskConnection = new TsuTask()
                {
                    ActionName = "Connection",
                    estimatedTimeInMilliSec = 3000,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Module.Tda5005._PortCom = comboBox_PortCom.Text;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        connecTSUccess = Module.Connect();
                    },
                    postAction = () =>
                    {
                        // post action to do in the UI
                        connecTSUccess.MergeString(Module.GetSerialNumber());
                        Logs.Log.AddEntryAsResult(this._Module, connecTSUccess);
                        if (connecTSUccess.Success == false)
                        {
                            new MessageInput(MessageType.Critical, R.StringTS60_ProblemDuringInitialization).Show();
                            ViewTdaOff();
                            TaskToDo.Clear();
                            this.Module.IsBusyCommunicating = false;
                        }
                    },
                    cancelAction = () =>
                    {
                        ViewTdaOff();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    }
                };

                TsuTask taskInit = new TsuTask()
                {
                    ActionName = "Initialisation",
                    estimatedTimeInMilliSec = 300,
                    preAction = () =>
                    {
                        Module.Tda5005._PortCom = comboBox_PortCom.Text;
                    },
                    mainAction = () =>
                    {
                        if (connecTSUccess.Success) iniTSUccess.MergeString(Module.InitializeSensor());
                    },
                    postAction = () =>
                    {
                        if (connecTSUccess.Success)
                        {
                            if (iniTSUccess.Success == true)
                            {
                                ViewTdaOn();
                                //No need as bubble is not shown
                                //this.Module.MeasureCompensator();
                                this.Module.GetBatteryLevel();
                                this.Module.GetTda5005Face();
                                this.Module.CheckATRLock();
                                this.Module.OnMeasureToDoReceived();
                                BatteryStateImage();
                                FaceStateImage();
                                AtrStateImage();
                            }
                            Logs.Log.AddEntryAsResult(this._Module, iniTSUccess);
                            if (iniTSUccess.Success == false)
                            {
                                new MessageInput(MessageType.Critical, R.StringTS60_ProblemDuringInitialization).Show();
                                ViewTdaOff();
                            }
                        }
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        ViewTdaOff();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(taskConnection);
                    TaskToDo.Add(taskInit);
                    //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { taskConnection, taskInit }, "Connection");
                    RunTasksWithWaitingMessage(new List<TsuTask>() { taskConnection, taskInit }, "Connection");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
                //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { taskConnection, taskInit }, "Connection");
            }
            catch (Exception ex)
            {
                if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                TaskToDo.Clear();
                throw;
            }

        }
        private object SomeOperationAsync(object cancellationToken)
        {
            throw new NotImplementedException();
        }

        private void button_Distance_Click(object sender, EventArgs e)
        {
            //int numberOfDistance=1;
            //DoubleValue averageSlopeDistance;
            //bool parseOK;
            //parseOK = Int32.TryParse(textBox_numberOfMeas.Text, out numberOfDistance);
            //Timer_check_TDA.Enabled = false;
            //Logs.Log.AddEntryAsResult(this._Module, Module.GetAverageDistance(numberOfDistance, out averageSlopeDistance));
            //Logs.Log.AddEntryAsFYI(this._Module, string.Format("Distance={0}m\r\nEmq Distance={1}\r\n", averageSlopeDistance.Value, averageSlopeDistance.Sigma));
            //Timer_check_TDA.Enabled = true;
        }

        private void button_Rec_Click(object sender, EventArgs e)
        {
            //int numberOfAngle=1;
            //Angles averageAngle;
            //bool parseOK;
            //parseOK = Int32.TryParse(textBox_numberOfMeas.Text, out numberOfAngle);
            //Timer_check_TDA.Enabled = false;
            //Logs.Log.AddEntryAsResult(this._Module, Module.GetAverageAngle(out averageAngle, numberOfAngle));
            ////textBox_Bulle_L.Text = string.Format("{0:0.000}",_tda5005._Compensator.longitudinalAxisInclination/Math.PI*200,0.000);
            ////textBox_Bulle_T.Text = string.Format("{0:0.000}", _tda5005._Compensator.tranverseAxisInclination / Math.PI * 200, 0.000);
            //Timer_check_TDA.Enabled = true;
            //Logs.Log.AddEntryAsFYI(this._Module, string.Format("Hz={0}gon\r\nEmq Hz={1}\r\n", averageAngle.Horizontal.Value * 200 / Math.PI, averageAngle.Horizontal.Sigma * 200 / Math.PI));
            //Logs.Log.AddEntryAsFYI(this._Module, string.Format("V={0}gon\r\nEmq V={1}\r\n", averageAngle.Vertical.Value * 200 / Math.PI, averageAngle.Vertical.Sigma * 200 / Math.PI));
            this.Module.DistanceWanted = false;
            this.Module.GotoWanted = false;
            TryAction(LaunchMeasurement);
        }


        private void button_All_Click(object sender, EventArgs e)
        {
            this.Module.DistanceWanted = true;
            this.Module.GotoWanted = false;
            TryAction(LaunchMeasurement);
        }

        public override void MeasureAsync()
        {
            base.MeasureAsync(); // do nothing
            TryAction(LaunchMeasurement);

        }

        public void LaunchMeasurement()
        {
            try
            {
                Result r = new Result();
                if (this.Module._ToBeMeasureTheodoliteData == null) throw new Exception("Nothing to measure");
                int factor = this.Module._ToBeMeasureTheodoliteData.NumberOfMeasureToAverage;
                if (Module._ToBeMeasureTheodoliteData.Face == FaceType.DoubleFace) factor *= 2;

                TsuTask task = new TsuTask()
                {
                    ActionName = "Measuring...",

                    estimatedTimeInMilliSec = 7000 * factor,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        r.MergeString(this.Module.tempResult);
                        r.MergeString(this.Module.MeasureTda());
                    },
                    cancelAction = () =>
                    {
                        this.Module.CancelMeasureInProgress();
                        this.Module.CancelMeasureInProgress();
                        TaskToDo.Clear();
                    },
                    postAction = () =>
                    {
                        this.Module.BeingMeasured = null;
                        this.Module.DistanceWanted = true; // by default sot that measure() from polar Module take all.
                        if (r.Success)
                        {
                            this.Module.ReportMeasurement();
                            this.Module.FinishMeasurement();
                            this.Module.tempResult = r;
                        }
                        else
                        {
                            Logs.Log.AddEntryAsResult(this.Module, this.Module.tempResult);
                        }
                        this.AtrStateImage();
                        this.Module.OnMeasureToDoReceived();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Measure");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
                //if (this.Module.CancelByMessageBeforeMeasure()) throw new CancelException();
                //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Measure");
            }
            catch (Exception ex)
            {
                if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }


        }
        public void LaunchModuleMeasurement()
        {
            try
            {
                Result r = new Result();
                if (this.Module._ToBeMeasureTheodoliteData == null) throw new Exception("Nothing to measure");
                int factor = this.Module._ToBeMeasureTheodoliteData.NumberOfMeasureToAverage;
                if (Module._ToBeMeasureTheodoliteData.Face == FaceType.DoubleFace) factor *= 2;

                TsuTask task = new TsuTask()
                {
                    ActionName = "Measuring...",

                    estimatedTimeInMilliSec = 7000 * factor,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        r.MergeString(this.Module.tempResult);
                        r.MergeString(this.Module.MeasureTda());
                    },
                    cancelAction = () =>
                    {
                        this.Module.CancelMeasureInProgress();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    postAction = () =>
                    {
                        this.Module.BeingMeasured = null;
                        this.Module.DistanceWanted = true; // by default sot that measure() from polar Module take all.
                        if (r.Success)
                        {
                            this.Module.tempResult = r;
                        }
                        this.AtrStateImage();
                        this.Module.OnMeasureToDoReceived();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Measure");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
                //if (this.Module.CancelByMessageBeforeMeasure()) throw new CancelException();
                //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Measure");
            }
            catch (Exception ex)
            {
                if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }


        }
        private void Timer_check_TDA_Tick(object sender, EventArgs e)
        {
            if (!this.Module.IsBusyCommunicating)
            {
                Result checkTda = new Result();

                try
                {
                    TsuTask task = new TsuTask()
                    {
                        ActionName = "Timer Check",
                        estimatedTimeInMilliSec = 4000,
                        timeOutInSec = 4,
                        preAction = () =>
                        {
                            Timer_check.Stop();
                            Timer_check.Enabled = false;
                            this.Module.CheckAndWaitIfBusy();
                            //this.doingTimerThings = true;

                        },
                        mainAction = () =>
                        {
                            TSU.Debug.WriteInConsole("Timer_check_TDA_TickMainAction");
                            checkTda.MergeString(this.DoTimerCheckThings());
                        },
                        postAction = () =>
                        {
                            TSU.Debug.WriteInConsole("Timer_check_TDA_TickPostAction");

                            checkTda.AccessToken = (checkTda.AccessToken as string).Replace("\r\n", ", ");
                            Logs.Log.AddEntryAsResult(this._Module, checkTda);
                            Timer_check.Enabled = true;
                            Timer_check.Start();
                            //TaskToDo.Clear();
                            this.Module.IsBusyCommunicating = false;
                            //if (checkTda.Success)
                            //{
                            //    Timer_check_TDA.Enabled = true;
                            //    Timer_check_TDA.Start();
                            //}
                            //else
                            //{
                            //    // disconnect and reconnect 
                            //    Result r = new Result();
                            //    this.Module.Disconnect();
                            //    r.MergeString(this.Module.Connect());
                            //    Logs.Log.AddEntryAsResult(this._Module, r);
                            //    if (r.Success == false)
                            //    {
                            //        this.ShowMessageOfCritical(R.StringTS60_Connection_Problem);
                            //        this.TurnOff();
                            //        this.Module.InvokeOnApplicationDispatcher(this.ViewTdaOff);
                            //    }
                            //    else
                            //    {
                            //        Timer_check_TDA.Enabled = true;
                            //        Timer_check_TDA.Start();
                            //    }
                            //}
                            //this.doingTimerThings = false;
                            TSU.Debug.WriteInConsole("Timer_check_TDA_TickDone");
                        },
                        timeOutAction = () =>
                        {
                            Timer_check.Enabled = true;
                            Timer_check.Start();
                            checkTda.AccessToken = (checkTda.AccessToken as string).Replace("\r\n", ", ");
                            Logs.Log.AddEntryAsResult(this._Module, checkTda);
                            TaskToDo.Clear();
                            this.Module.IsBusyCommunicating = false;
                        },
                        ExceptionAction = () =>
                        {
                            Timer_check.Enabled = true;
                            Timer_check.Start();
                            TaskToDo.Clear();
                            this.Module.IsBusyCommunicating = false;
                        },
                    };
                    if (TaskToDo.Count == 0)
                    {
                        //I don't want to block a action made by the timer if done during the timer
                        //TaskToDo.Add(task);
                        this.RunTasksWithoutWaitingMessage(new List<TsuTask>() { task, }, "Updating");
                    }
                }
                catch (Exception ex)
                {
                    if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                    Timer_check.Enabled = true;
                    Timer_check.Start();
                    TaskToDo.Clear();
                    throw;
                }
            }
        }
        /// <summary>
        /// Tache fait lors du tick du timer
        /// </summary>
        /// <returns></returns>
        public Result DoTimerCheckThings()
        {
            Result r = new Result();
            r.MergeString(this.Module.CheckConnection());
            if (r.Success)
            {
                this.Module.OnMeasureToDoReceived();
                //for now the bubble is not working so no need to measure compensator.
                //r.MergeString(this.Module.MeasureCompensator());
                r.MergeString(this.Module.GetBatteryLevel());
                r.MergeString(Module.GetTda5005Face());
                r.MergeString(this.Module.CheckATRLock());
                r.Success = true; //if not get current 1 parameter, I don't want to shut down TS60
                this.Module.InvokeOnApplicationDispatcher(BatteryStateImage);
                //this.Module.InvokeOnApplicationDispatcher(BulleStateImage);
                this.Module.InvokeOnApplicationDispatcher(FaceStateImage);
                this.Module.InvokeOnApplicationDispatcher(AtrStateImage);
                this.Module.OnMeasureToDoReceived(); //To check if still have measure to do (avoid having a goto and no measure
                //Logs.Log.AddEntryAsFYI(this._Module, "Connection TDA OK");
            }
            return r;
        }
        private void button_ATR_Click(object sender, EventArgs e)
        {
            try
            {
                //bool atrOnOff;
                //Result result = new Result();
                //Timer_check_TDA.Enabled = false;
                //result.MergeString(Module.GetATRStatus(out atrOnOff));
                //atrOnOff=!atrOnOff;
                //result.MergeString(Module.SetATROn(atrOnOff));
                //Logs.Log.AddEntryAsResult(this._Module, result);
                //if (result.Success==true)       
                //{
                //    if (atrOnOff==true)
                //    {
                //        this.button_ATR.Image =R.Tda5005_ATR_On;
                //    }
                //    else
                //    {
                //        this.button_ATR.Image =R.Tda5005_ATR_OFF;
                //    }
                //}
                //Timer_check_TDA.Enabled = false;

                Result r = new Result();
                TsuTask task = new TsuTask()
                {
                    ActionName = "Set ATR",

                    estimatedTimeInMilliSec = 1000,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        r.MergeString(SwitchATR());
                    },
                    postAction = () =>
                    {
                        Logs.Log.AddEntryAsResult(this._Module, r);
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Set ATR LOCK functions");
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Set ATR LOCK functions");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }

            }
            catch (Exception ex)
            {
                if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }
        }
        /// <summary>
        /// Set the Atr and Lock when pressing the ATR button
        /// </summary>
        private Result SwitchATR()
        {
            Result result = this.Module.SwitchAtr();
            if (result.Success == true)
            {
                this.Module.InvokeOnApplicationDispatcher(this.AtrStateImage);
                this.Module.InvokeOnApplicationDispatcher(() => { button_Rec.Visible = !this.Module.Tda5005._ATR; });
                this.Module.InvokeOnApplicationDispatcher(() => { button_Distance.Visible = !this.Module.Tda5005._ATR; });
            }
            return result;
        }
        private void AtrStateImage()
        {
            if (this.Module.Tda5005._ATR)
            {
                this.button_ATR.Image = R.Tda5005_ATR_On;
            }
            else
            {
                this.button_ATR.Image = R.Tda5005_ATR_OFF;
            }
        }
        private void FaceStateImage()
        {
            if (this._Tda5005._Face == 1)
            {
                this.button_face.Image = R.Tda5005_CercleGauche;
            }
            else
            {
                this.button_face.Image = R.Tda5005_CercleDroit;
            }
        }
        private void button_face_Click(object sender, EventArgs e)
        {
            try
            {
                //Result checkface = new Result();
                //int face = 1;
                //checkface.MergeString(Module.ChangeFace2());
                //checkface.MergeString(Module.GetFace(ref face));
                //Logs.Log.AddEntryAsResult(this._Module, checkface);
                //faceimage(face);

                Result checkface = new Result();
                TsuTask task = new TsuTask()
                {
                    ActionName = R.T_CHANGING_FACE,
                    estimatedTimeInMilliSec = 2000,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        checkface.MergeString(FaceChangeAction());
                    },
                    postAction = () =>
                    {
                        Logs.Log.AddEntryAsResult(this._Module, checkface);
                        FaceStateImage();
                        AtrStateImage();
                        this.Module.OnMeasureToDoReceived();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, R.T_CHANGING_FACE);
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, R.T_CHANGING_FACE);
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }

        }
        private Result FaceChangeAction()
        {
            Result r = new Result();
            r.MergeString(Module.InvertFace());
            r.MergeString(Module.GetTda5005Face());
            return r;
        }
        private void PopUpMenu_Load(object sender, EventArgs e)
        {

        }

        private void textBox_numberOfMeas_TextChanged(object sender, EventArgs e)
        {
            int numberOfMeasure;
            if (Int32.TryParse(textBox_numberOfMeas.Text, out numberOfMeasure))
                this.Module._ToBeMeasureTheodoliteData.NumberOfMeasureToAverage = numberOfMeasure;
        }


        private void labelMeteo_Click(object sender, EventArgs e)
        {
            // Change values
            this.Module.SetMeteoManually();
            this.UpdateLabelMeteo();
        }
        private void UpdateLabelMeteo()
        {
            labelMeteo.Text = string.Format("T° {0}C\r\nP {1}hPa\r\nH {2}%",
                this.Module.temperature.ToString("F1", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")),
                this.Module.pressure.ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")),
                this.Module.humidity.ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
            if (this.Module.MeteoSet) labelMeteo.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Good;
        }
        private void buttonGoto_Click(object sender, EventArgs e)
        {
            this.Module.GotoWanted = false;
            this.Module.MeasureWanted = false;
            this.Goto();
        }
        /// <summary>
        /// Do the goto to next position
        /// </summary>
        internal void Goto()
        {
            try
            {
                Result r = new Result();
                TsuTask task = new TsuTask()
                {
                    ActionName = "Goto...",

                    estimatedTimeInMilliSec = 7000,
                    timeOutInSec = 0,//*30 * factor*,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        this.Module.GotoTda();
                        this.Module.DistanceWanted = true;
                        r.MergeString(this.Module.tempResult);
                    },
                    postAction = () =>
                    {
                        Logs.Log.AddEntryAsResult(this.Module, this.Module.tempResult);
                        this.BatteryStateImage();
                        this.AtrStateImage();
                        this.Module.OnMeasureToDoReceived();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Goto");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
                //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Goto");
            }
            catch (Exception ex)
            {
                if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }

        }

        private void buttonGotoAll_Click(object sender, EventArgs e)
        {
            this.Module.GotoWanted = false;
            this.GotoAll();
        }
        /// <summary>
        /// Do the goto to next position
        /// </summary>
        private void GotoAll()
        {
            try
            {
                Result r = new Result();
                int factor = this.Module._ToBeMeasureTheodoliteData.NumberOfMeasureToAverage;
                if (Module._ToBeMeasureTheodoliteData.Face == FaceType.DoubleFace) factor *= 2;
                factor++;
                TsuTask task = new TsuTask()
                {
                    ActionName = "GotoAll...",

                    estimatedTimeInMilliSec = 7000 * factor,
                    timeOutInSec = 0,//*30 * factor*,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        this.Module.GotoTda();
                        this.Module.DistanceWanted = true;
                        r.MergeString(this.Module.tempResult);
                        if (r.Success) { r.MergeString(this.Module.MeasureTda()); }
                    },
                    postAction = () =>
                    {
                        this.Module.BeingMeasured = null;
                        this.Module.DistanceWanted = true; // by default sot that measure() from polar Module take all.
                        if (r.Success)
                        {
                            this.Module.ReportMeasurement();
                        }
                        else
                        {
                            Logs.Log.AddEntryAsResult(this.Module, this.Module.tempResult);
                        }
                        this.BatteryStateImage();
                        this.AtrStateImage();
                        this.Module.OnMeasureToDoReceived();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, R.T_GOTO_AND_DO_MEASUREMENT);
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }
        }
        /// <summary>
        /// timer to check the comport available
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_ComPort_Tick(object sender, EventArgs e)
        {
            if (!this.Module.IsBusyCommunicating)
            {
                try
                {
                    TsuTask task = new TsuTask()
                    {
                        ActionName = "Get ComPort",
                        estimatedTimeInMilliSec = 100,
                        timeOutInSec = 1,
                        preAction = () =>
                        {
                            this.Module.CheckAndWaitIfBusy();
                            timer_ComPort.Stop();
                            timer_ComPort.Enabled = false;
                        },
                        mainAction = () =>
                        {
                            TSU.Debug.WriteInConsole("Get ComPort Begin");
                            this.Module.InvokeOnApplicationDispatcher(this.GetAllPorts);
                        },
                        postAction = () =>
                        {
                            timer_ComPort.Enabled = true;
                            timer_ComPort.Start();
                            this.Module.IsBusyCommunicating = false;
                            TSU.Debug.WriteInConsole("Get ComPort end");
                        },
                        timeOutAction = () =>
                        {
                            timer_ComPort.Enabled = true;
                            timer_ComPort.Start();
                            this.Module.IsBusyCommunicating = false;
                            TSU.Debug.WriteInConsole("Get ComPort end with time out");
                        },
                        ExceptionAction = () =>
                        {
                            Timer_check.Enabled = true;
                            Timer_check.Start();
                            TaskToDo.Clear();
                            this.Module.IsBusyCommunicating = false;
                        },
                    };
                    if (TaskToDo.Count == 0)
                    {
                        //I don't want to block a action made by the timer if done during the timer
                        //TaskToDo.Add(task);
                        this.RunTasksWithoutWaitingMessage(new List<TsuTask>() { task, }, "Updating com port");
                    }
                }
                catch (Exception ex)
                {
                    if (!(ex.Message == R.String_Exception_InstrumentIsBusy)) this.Module.IsBusyCommunicating = false;
                    timer_ComPort.Enabled = true;
                    timer_ComPort.Start();
                    TaskToDo.Clear();
                    throw;
                }
            }
        }
        /// <summary>
        /// Show a message indicating that the ATR fine adjust has failed
        /// </summary>
        public void MessageATRFailed()
        {
            DsaFlag dsaFlag = DsaFlag.GetByNameOrAdd(this.Module.FinalModule.DsaFlags, "ShowMsgATRFailed");
            new MessageInput(MessageType.Warning, R.String_ATRFailed)
            {
                ButtonTexts = new List<string> { R.T_OK + "!" },
                DontShowAgain = dsaFlag
            }.Show();
        }
        internal override void SetAllToolTips()
        {

            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            toolTip1.SetToolTip(this.Button_OnOff, R.StringTDA5005_ToolTip_Init);
            toolTip1.SetToolTip(this.comboBox_PortCom, R.StringTDA5005_ToolTip_port);
            toolTip1.SetToolTip(this.buttonGotoAll, R.StringTDA5005_ToolTip_GotoAll);
            toolTip1.SetToolTip(this.buttonGoto, R.StringTDA5005_ToolTip_Goto);
            toolTip1.SetToolTip(this.button_Distance, R.StringTDA5005_ToolTip_Dist);
            toolTip1.SetToolTip(this.button_Rec, R.StringTDA5005_ToolTip_Rec);
            toolTip1.SetToolTip(this.button_All, R.StringTDA5005_ToolTip_All);
            toolTip1.SetToolTip(this.button_face, R.StringTDA5005_ToolTip_Face);
            toolTip1.SetToolTip(this.button_ATR, R.StringTDA5005_ToolTip_Lock);
            toolTip1.SetToolTip(this.labelMeteo, R.StringTDA5005_ToolTip_Weather);
            toolTip1.SetToolTip(this.label_Bulle, R.StringTDA5005_ToolTip_Bulle);
            toolTip1.SetToolTip(this.pictureBox_bulle, R.StringTDA5005_ToolTip_Bulle);
            toolTip1.SetToolTip(this.pictureBox_Battery, R.StringTDA5005_ToolTip_Battery);
            toolTip1.SetToolTip(this.pictureBox_OnOff, R.StringTDA5005_ToolTip_ONOFF);
        }
    }
}
