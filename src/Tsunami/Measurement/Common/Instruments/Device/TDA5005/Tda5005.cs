﻿using TSU.ENUM;
using TSU;
using TSU.Common.Measures;
using System.Xml.Serialization;
using G = GeoComClient;
using TSU.Common.Elements;

namespace TSU.Common.Instruments.Device.TDA5005
{
    [XmlType(TypeName = "TDA5005.Instrument")]
    public sealed class Instrument : TotalStation
    {
        [XmlIgnore]
        public double _Battery { get; set; }
        [XmlIgnore]
        public double _Tinterne { get; set; }
        [XmlIgnore]
        public string _PortCom { get; set; }
        [XmlIgnore]
        public Compensator _Compensator { get; set; }
        [XmlIgnore]
        public Angles _AngleCorrection { get; set; }
        [XmlIgnore]
        public int _Face { get; set; }
        [XmlIgnore]
        public bool _ATR { get; set; }
        [XmlIgnore]
        public bool _Lock { get; set; }

        public Instrument()
        {
            this._Brand = "Leica";
            this.Id = string.Empty;
            this._Model = "TDA5005";
            this._SerialNumber = string.Empty;
            this._PortCom = "COM1";
            this._AngleCorrection = new Angles();
            this._Battery = 0;
            this._Compensator = new Compensator();
            this._InstrumentClass = InstrumentClasses.TACHEOMETRE;
            this._Compensator.longitudinalAxisInclination = -999;
            this._Compensator.tranverseAxisInclination = -999;
            this._Compensator.inclinationAccuracy = -999;
            this._ATR = false;
            this._Face = 1;
            this._Lock = false;
        }
    }
}
    

    

 