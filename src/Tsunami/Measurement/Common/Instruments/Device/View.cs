﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TSU.Views;
using TSU.Views.Message;
using M = TSU;
using R = TSU.Properties.Resources;


namespace TSU.Common.Instruments.Device
{
    public class View : TsuView
    {
        internal virtual Module Module
        {
            get
            {
                return this._Module as Module;
            }
            set
            {
                this._Module = value;
            }
        }
        //Height and width of the view to calculate the factorscale
        internal int startingHeight;
        internal int startingWidth;
        internal double na = TSU.Tsunami2.Preferences.Values.na;

        internal Logs.LogView LogView { get; set; }

        internal View()
            : base()
        {
        }

        internal View(M.IModule parentModule)
            : base(parentModule)
        {
        }

        public virtual void MeasureAsync()
        {

        }

        internal bool IsOn
        {
            get
            {
                if (this.Module == null) return false;
                if (this.Module.InstrumentIsOn) return true;

                Logs.Log.AddEntryAsError(this._Module, R.T_INST_OFF);
                return false;
            }
        }

        internal virtual void TryAction(Action action, string message, bool showSuccessInlogs = true, bool showFailureInlog = true, bool showFailureMessage = true)
        {
            this.Refresh();
            try
            {
                if (!IsOn)
                    throw new Exception(R.T352);


                if (showSuccessInlogs) Logs.Log.AddEntryAsBeginningOf(this._Module, message);
                action();

            }

            catch (OutOfMemoryException e)
            {
                TsuView.WarnForOutOfMemory(this.Module, message, e);
            }

            catch (Exception ex)
            {
                string failureMessage = message + " " + R.T354 + ";" + ex.Message;
                if (ex is NullReferenceException)
                {
                    string titleAndMessage = $"{ex.Message}\r\n\r\n{ex.ToString()}";
                    new MessageInput(MessageType.Critical, titleAndMessage).Show();
                }
                if (!(ex is CancelException))
                {
                    if (showFailureInlog) Logs.Log.AddEntryAsError(this._Module, failureMessage);
                    if (showFailureMessage)
                    {
                        new MessageInput(MessageType.Critical, failureMessage).Show();
                    }
                }
                this.IsWaiting = false;
                this.Module.State = new InstrumentState(InstrumentState.type.Idle);
                if (this.WaitingForm != null) this.WaitingForm.EndAll();
            }
        }

        internal virtual void TrySyncActionWithMessage(Action action, string title, string beginningMessage, int stepCount = 1, int totlEstimatedTimeMs = 500, bool allowCancel = true)
        {
            try
            {
                this.WaitingForm = new Views.Message.ProgressMessage(
                    this,
                    title,
                    beginningMessage,
                    stepCount,
                    totlEstimatedTimeMs,
                    ShowCancelButton: allowCancel);
                this.WaitingForm.BeginAstep($"beginning {title}...");

                System.Threading.Thread t = new System.Threading.Thread(
                    () =>
                    {
                        TryAction(action, title, false, true);
                    });
                t.Start();
            }
            catch (NullReferenceException ex)
            {
                string titleAndMessage = $"{ex.Message}\r\n\r\n{ex.ToString()}";
                new MessageInput(MessageType.Critical, titleAndMessage).Show();
                this.Module.State = new InstrumentState(InstrumentState.type.Idle);
                this.WaitingForm.EndAll();
            }
            catch (Exception ex)
            {
                new MessageInput(MessageType.Critical, ex.Message).Show();
                this.Module.State = new InstrumentState(InstrumentState.type.Idle);
                this.WaitingForm.EndAll();
            }
        }

        System.Windows.Forms.Label CoverLabel;
        internal void CoverViewDuringSyncAction(string titleAndDescription)
        {

            CoverLabel = new System.Windows.Forms.Label() { Text = titleAndDescription };
            CoverLabel.Dock = System.Windows.Forms.DockStyle.Fill;

            this.Controls.Add(CoverLabel);
            CoverLabel.BringToFront();
        }

        internal void RemoveCoverViewDuringSyncAction()
        {
            CoverLabel.Hide();
            CoverLabel.Dispose();
        }
        internal virtual void SetAllToolTips() { }

        #region Async Running

        internal void RunTasksWithWaitingMessage(TsuTask task)
        {
            List<TsuTask> tasks = new List<TsuTask>() { task };
            RunTasksWithWaitingMessage(tasks, task.ActionName);
        }

        /// <summary>
        /// Use to run in another thread (async) and action with UI things happening before and after
        /// </summary>
        /// <param name="tasks"></param>
        /// <param name="name"></param>
        internal void RunTasksWithWaitingMessage(List<TsuTask> tasks, string name)
        {

            int totalEstimatedTimeInMilliSeconds = 0;
            var cancelToken = new System.Threading.CancellationTokenSource();
            foreach (TsuTask task in tasks) totalEstimatedTimeInMilliSeconds += task.estimatedTimeInMilliSec;
            if (this.WaitingForm != null) this.WaitingForm.EndAll();
            this.WaitingForm = new Views.Message.ProgressMessage(
                this,
                name,
                "Starting " + name,
                tasks.Count,
                totalEstimatedTimeInMilliSeconds, true,
                () =>
                {
                    cancelToken.Cancel();
                    foreach (TsuTask task in tasks)
                    {
                        if (task.cancelAction != null) task.cancelAction();
                        if (task.timeOutAction != null) task.timeOutAction();
                    }
                }
                );
            try
            {
                RunNextTasks(tasks, cancelToken.Token);
            }
            catch (Exception)
            {
                throw; // should never pass here
            }

        }

        private async void RunNextTasks(List<TsuTask> tasks, System.Threading.CancellationToken cancellationToken)
        {
            if (this.InvokeRequired)
            {
                // Invoke the event handler on the main UI thread
                this.BeginInvoke(new System.Action(() => RunNextTasks(tasks, cancellationToken)));
            }
            else
            {
                try
                {
                    // Do thing from the UI
                    this.WaitingForm.BeginAstep(tasks[0].ActionName);
                    if (tasks[0].preAction != null) tasks[0].preAction();


                    // Define task for theDe connection
                    Task task = Task.Factory.StartNew(() =>
                    {
                        tasks[0].mainAction();
                    });

                    // Run the tasks
                    bool runned;
                    if (tasks[0].timeOutInSec != 0)
                        runned = (await Task.WhenAny(task, Task.Delay(tasks[0].timeOutInSec * 1000, cancellationToken)) == task);
                    else
                        runned = (await Task.WhenAny(task) == task);
                    if (runned)
                    {
                        // Task completed within timeout.
                        // Consider that the task may have faulted or been canceled.
                        // We re-await the task so that any exceptions/cancellation is rethrown.
                        await task;
                        this.WaitingForm.EndCurrentStep();
                        if (tasks[0].postAction != null) tasks[0].postAction();

                        if (tasks.Count > 1)
                        {
                            tasks.RemoveAt(0);
                            if (!cancellationToken.IsCancellationRequested)
                                RunNextTasks(tasks, cancellationToken);
                        }
                        else
                            WaitingForm.EndAll();
                    }
                    else
                    {
                        // timeout/cancellation logic
                        if (!cancellationToken.IsCancellationRequested)
                        {
                            string titleAndMessage = $"{R.T_TIMED_OUT};{R.T_THE_OPERATION__DIDNT_SUCCEED_WITHIN_THE_ALLOWED} {tasks[0].timeOutInSec} seconds";
                            new MessageInput(MessageType.Critical, titleAndMessage).Show();
                        }

                        tasks.Clear();
                    }
                }
                catch (Exception ex)
                {
                    if (tasks.Count > 0)
                    {
                        string titleAndMessage = $"{tasks[0].ActionName} {R.T_FAILED}; {ex.Message}";
                        new MessageInput(MessageType.Critical, titleAndMessage).Show();
                        if (tasks[0].ExceptionAction != null) tasks[0].ExceptionAction();
                    }
                    tasks.Clear(); // so that we know we should close the message windows (<2)
                }

                finally
                {
                    if (tasks.Count == 0)
                        this.WaitingForm.EndAll();
                }
            }

        }
        internal void RunTasksWithoutWaitingMessage(List<TsuTask> tasks, string name)
        {
            int totalEstimatedTimeInMilliSeconds = 0;
            foreach (TsuTask task in tasks)
                totalEstimatedTimeInMilliSeconds += task.estimatedTimeInMilliSec;
            RunNextTasksWithoutWaitingForm(tasks);
        }
        private async void RunNextTasksWithoutWaitingForm(List<TsuTask> tasks)
        {
            try
            {
                tasks[0].preAction?.Invoke();
                // temporary unsubscribing
                //this.log.UnSubscribe(this.log.LogView);

                // Define task for theDe connection
                Task task = Task.Factory.StartNew(() =>
                {
                    //cancellationToken.Register(() =>
                    //throw new OperationCanceledException());
                    tasks[0].mainAction();
                });
                // Run the tasks
                bool runned;
                if (tasks[0].timeOutInSec != 0)
                    runned = (await Task.WhenAny(task, Task.Delay(tasks[0].timeOutInSec * 1000)) == task);
                else
                    runned = (await Task.WhenAny(task) == task);
                if (runned)
                {
                    // Task completed within timeout.
                    // Consider that the task may have faulted or been canceled.
                    // We re-await the task so that any exceptions/cancellation is rethrown.
                    await task;
                    tasks[0].postAction?.Invoke();

                    if (tasks.Count > 1)
                    {
                        tasks.RemoveAt(0);
                        RunNextTasksWithoutWaitingForm(tasks);
                    }
                }
                else
                {
                    // timeout/cancellation logic
                    string titleAndMessage = $"{R.T_TIMED_OUT};{R.T_THE_OPERATION__DIDNT_SUCCEED_WITHIN_THE_ALLOWED} {tasks[0].timeOutInSec} seconds";
                    new MessageInput(MessageType.Critical, titleAndMessage).Show();
                    tasks[0].timeOutAction?.Invoke();
                }
            }
            catch (Exception ex)
            {
                string titleAndMessage = $"{tasks[0].ActionName} {R.T_FAILED}; {ex.Message}";
                new MessageInput(MessageType.Critical, titleAndMessage).Show();
                tasks[0].ExceptionAction?.Invoke();
            }
            finally
            {
                //Removed because it happent offen with tda timer and all the log is treated, so instead i will try ti invoke the log update wth dispatched
                //// RE-subscribing
                //this.log.Subscribe(this.log.LogView);
                //this.log.LogView.UpdateView();
            }
        }

        internal virtual void OnRemovedFromView()
        {
        }

        #endregion
    }
}
