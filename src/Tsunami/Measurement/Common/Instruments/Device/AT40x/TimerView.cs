﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSU.Views.Message;


namespace TSU.Common.Instruments.Device.AT40x
{
    public partial class TimerView : SubView
    {
        public TimerView()
        {
            InitializeComponent();
            this.ApplyThemeColor();
        }

        private void ApplyThemeColor()
        {
            this.panelTimer.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.buttonTimer.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.buttonTimer.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
        }
        public TimerView(AT40x.View parentView)
            : base(parentView)
        {
            this._Name = "Timer panel";
            InitializeComponent();
            this.ParentView = parentView;
        }

        internal Remote Remote
        {
            get
            {
                return (this.ParentView as AT40x.View).Module._remote;
            }
        }
   
        private void datePicker_Enter(object sender, EventArgs e)
        {
            datePicker.MinDate = DateTime.Now;
        }
        private void timePicker_Enter(object sender, EventArgs e)
        {
            timePicker.Value = DateTime.Now.AddMinutes(120);
        }

        internal override void Start()
        {
            base.Start(); // will put the view in the subviewPanel
            timePicker.Value = DateTime.Now.AddMinutes(120);
        }

        private void buttonTimer_Click(object sender, EventArgs e)
        {
            try
            {
                string titleAndMessage = $"{R.T_SENSOR_WILL_TURN_OFF_TO_WAKE_UP_LATER};{R.T_THE_SENSOR_WILL_TURN_OFF_THE_CONTROLLER} {this.datePicker.Value.Date + this.timePicker.Value.TimeOfDay}";
                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL }
                };
                if (mi.Show().TextOfButtonClicked != R.T_CANCEL)

                    Remote.SetWarmingTimer(this.datePicker.Value.Date + this.timePicker.Value.TimeOfDay);
            }
            catch
            {

            }
        }
    }
}
