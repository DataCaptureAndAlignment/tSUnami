﻿using System;

namespace TSU.Common.Instruments.Device.AT40x
{
    public class CompensatorEventArgs : EventArgs
    {
        public Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation InstrumentVerticalisation;
        public CompensatorEventArgs(Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation instrumentVerticalisation)
        {
            InstrumentVerticalisation = instrumentVerticalisation;
        }
    }

    public class ReflectorEventArgs : EventArgs
    {
        public InstrumentTypes Target;

        public ReflectorEventArgs(InstrumentTypes target)
        {
            Target = target;
        }
    }
}
