﻿using EmScon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Xml.Serialization;
using TSU.Common.Measures;
using TSU.Tools;
using TSU.Views.Message;
using P = TSU.Views.Message.ProgressMessage;
using Pr = TSU.Preferences;
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments.Device.AT40x
{
    #region Target

    public class Target
    {
        public InstrumentTypes SelectedAsSuType
        {
            get
            {
                switch (this.selected.targetType)
                {
                    case TSU_TargetType.ES_TT_CornerCube: return InstrumentTypes.CCR1_5;
                    case TSU_TargetType.ES_TT_RFIPrism: return InstrumentTypes.RFI0_5;
                    case TSU_TargetType.ES_TT_RRR05: return InstrumentTypes.RRR0_5;
                    case TSU_TargetType.ES_TT_RRR15: return InstrumentTypes.RRR1_5;
                    case TSU_TargetType.ES_TT_TBR05: return InstrumentTypes.TBR0_5;
                    case TSU_TargetType.ES_TT_Unknown:
                    case TSU_TargetType.ES_TT_RRR0875:
                    case TSU_TargetType.ES_TT_GlassPrism:
                    case TSU_TargetType.ES_TT_BRR05:
                    case TSU_TargetType.ES_TT_BRR15:
                    case TSU_TargetType.ES_TT_AutoCollMirror:
                    case TSU_TargetType.ES_TT_CatsEye:
                    case TSU_TargetType.ES_TT_CCR15: return InstrumentTypes.CCR1_5;
                    case TSU_TargetType.ES_TT_CCR35: return InstrumentTypes.CCR3_5;
                    default: return InstrumentTypes.Unknown;
                }
            }
        }
        #region Structure
        public struct Structure // declaration structure pour réception mesure angle simple
        {
            public int id;
            public string name;

            public TSU_TargetType targetType;
            public ES_ProbeType probeType;

            public double surfaceOffset;
            public int numberOfReflector;
            public int numberOfProbes;
        }
        #endregion

        //public Reflector reflector;
        public Structure selected;

        public bool TryingToLock;

        public IEnumerable<Structure> listBoth => listReflectors.Concat(listProbes);
        public List<Structure> listReflectors { get; set; } = new List<Structure>();
        public List<Structure> listProbes { get; set; } = new List<Structure>();

        public enum TSU_TargetType
        {
            ES_TT_Unknown = 0,
            ES_TT_CornerCube = 1,
            ES_TT_CatsEye = 2,
            ES_TT_GlassPrism = 3,
            ES_TT_RFIPrism = 4,
            ES_TT_RRR15 = 5,
            ES_TT_RRR05 = 6,
            ES_TT_BRR15 = 7,
            ES_TT_BRR05 = 8,
            ES_TT_TBR05 = 9,
            ES_TT_AutoCollMirror = 10,
            ES_TT_RRR0875 = 11,
            ES_TT_CCR15 = 12,
            ES_TT_CCR35 = 13
        }

        #region Fields
        Module At40xModule;
        System.Windows.Forms.Timer timer;
        int timerTick;
        [XmlIgnore]
        internal bool IsSelected;
        #endregion

        #region Constructor
        public Target(Module at40x)
        {
            At40xModule = at40x;
        }
        #endregion

        private readonly ManualResetEvent allReflectorsReceived = new ManualResetEvent(false);
        private readonly ManualResetEvent allProbesReceived = new ManualResetEvent(false);

        internal System.Windows.Forms.Timer ConnectionTimer;
        internal int ConnectionTimerTickNumber;

        internal void GetReflectorsList()
        {
            this.listReflectors.Clear();
            allReflectorsReceived.Reset();
            Debug.WriteInConsole("at40x-SYNC-GetReflectors");
            this.At40xModule._status.isTrackerBusy = true;
            this.At40xModule.sync.GetReflectors();
            // wait for all reflectors to be received in an async way
            if (!allReflectorsReceived.WaitOne(10000, true))
                Debug.WriteInConsole("Timeout on allReflectorsReceived");
            this.At40xModule._status.isTrackerBusy = false;
        }

        internal void GetProbesList()
        {
            this.listProbes.Clear();

            Debug.WriteInConsole("at40x-SYNC-GetProbe");
            this.At40xModule.sync.GetProbe(out int i);

            allProbesReceived.Reset();
            Debug.WriteInConsole("at40x-SYNC-GetProbes");
            this.At40xModule._status.isTrackerBusy = true;
            this.At40xModule.sync.GetProbes();
            // wait for all probes to be received in an async way
            if (!allProbesReceived.WaitOne(10000, true))
                Debug.WriteInConsole("Timeout on allProbesReceived");
            this.At40xModule._status.isTrackerBusy = false;

            this.At40xModule.View.UpdateReflectorPictureList();
        }


        internal void AsyncGetReflectorAndProbe()
        {
            this.listReflectors.Clear();
            this.listProbes.Clear();

            ConnectionTimer = new System.Windows.Forms.Timer() { Interval = 500, Enabled = true };

            ConnectionTimerTickNumber = 0;
            ConnectionTimer.Tick += delegate
            {
                if (!this.At40xModule._status.IsWaiting)
                {
                    ConnectionTimerTickNumber += 1;
                    switch (ConnectionTimerTickNumber)
                    {
                        case 1:

                            Debug.WriteInConsole("at40x-ASYNC-GetReflectors");
                            this.At40xModule.async.GetReflectors();
                            break;
                        case 2:
                            Debug.WriteInConsole("at40x-ASYNC-GetProbes");
                            this.At40xModule.async.GetProbes();
                            break;
                        case 3:
                            this.At40xModule._target.Get();
                            break;
                        case 4:
                            if (this.At40xModule.ToBeMeasuredData != null) this.At40xModule.CheckIncomingMeasure(this.At40xModule.ToBeMeasuredData as Polar.Measure);
                            break;
                        case 5:
                            this.At40xModule.Lock();
                            break;
                        default:
                            ConnectionTimer.Stop();
                            this.At40xModule.View.OnStatusChanged(this, new StatusEventArgs(R.T_GETTING_REFLECTORS_FROM_ADAPTER1, P.StepTypes.Begin));
                            break;
                    }
                }
            };
        }

        #region Reflector
        public static Reflector.Reflector FindReflectorInstanceFromAtType(TSU_TargetType eS_TargetType)
        {
            string modelName = "";
            switch (eS_TargetType)
            {
                case TSU_TargetType.ES_TT_RRR05: modelName = "RRR0.5"; break;
                case TSU_TargetType.ES_TT_RRR15: modelName = "RRR1.5"; break;
                case TSU_TargetType.ES_TT_RFIPrism: modelName = "RFI0.5"; break;
                case TSU_TargetType.ES_TT_CornerCube: modelName = "CCR1.5"; break;
                case TSU_TargetType.ES_TT_CCR15: modelName = "CCR1.5"; break;
                case TSU_TargetType.ES_TT_CCR35: modelName = "CCR3.5"; break;
                case TSU_TargetType.ES_TT_TBR05: modelName = "TBR0.5"; break;
                case TSU_TargetType.ES_TT_AutoCollMirror:
                case TSU_TargetType.ES_TT_BRR05: modelName = "BRR0.5"; break;
                case TSU_TargetType.ES_TT_BRR15: modelName = "BRR0.5"; break;
                case TSU_TargetType.ES_TT_CatsEye:
                case TSU_TargetType.ES_TT_GlassPrism:
                case TSU_TargetType.ES_TT_RRR0875:
                case TSU_TargetType.ES_TT_Unknown:
                default:
                    break;
            }
            return Pr.Preferences.Instance.Instruments.Find(x => x._Model == modelName) as Reflector.Reflector;
        }

        internal void OnGetReflectorsAnswer(object sender, Receiver.GetReflectorsAnswerEventArgs args)
        {
            lock (listReflectors)
            {
                Debug.WriteInConsole($"OnGetReflectorsAnswer, TotalReflectors={args.TotalReflectors}, InternalReflectorId={args.InternalReflectorId}, TargetType={args.TargetType}, SurfaceOffset={args.SurfaceOffset}, ReflectorName={args.ReflectorName}");
                listReflectors.Add(new Structure
                {
                    id = args.InternalReflectorId,
                    name = args.ReflectorName,
                    // cast ES into TSU
                    targetType = (TSU_TargetType)(int)args.TargetType,
                    probeType = ES_ProbeType.ES_PT_None,
                    surfaceOffset = args.SurfaceOffset,
                    numberOfReflector = args.TotalReflectors
                });
                if (listReflectors.Count == args.TotalReflectors)
                {
                    At40xModule.async.RemoveFromInProgressListIfExist(ES_Command.ES_C_GetReflectors);
                    At40xModule.StatusChange(R.T_REFLECTORS_RECEIVED, P.StepTypes.End);
                    allReflectorsReceived.Set();
                    At40xModule._status.isTrackerLockedAndReady = true;
                    // add fake ones:
                    //this.listReflectors.Add(new Structure() { targetType = TSU_TargetType.ES_TT_CCR15, id = 20, surfaceOffset = 0.01905 });
                    // en attente de geode this.listReflectors.Add(new Structure() { targetType = TSU_TargetType.ES_TT_CCR35, id = 21, surfaceOffset = 0.02540 }); 
                }
            }
        }

        /// <summary>
        /// This is raised when a prisme is selected in the instrument (this.async.ReflectorData)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void OnGetReflectorAnswer(object sender, Receiver.GetReflectorAnswerEventArgs e)
        {
            Debug.WriteInConsole($"Begin OnGetReflectorAnswer{e.InternalReflectorId}");
            At40xModule.async.RemoveFromInProgressListIfExist(ES_Command.ES_C_GetReflector);
            // check if the id existing
            selected = listBoth.FirstOrDefault(x => x.id == e.InternalReflectorId);
            TSU_TargetType actualType = this.selected.targetType;
            // check if the prisme is the want selected by user because some are not known and changed for a compatible one
            TSU_TargetType desireType = this.RequestedType;
            bool isFakeType = this.selected.targetType != desireType && desireType != TSU_TargetType.ES_TT_Unknown;

            CheckForReFlectorWithUnusalConstante(this.selected.targetType);



            this.InstrumentType = GetGeodeInstrumentType(actualType, desireType);
            TSU_TargetType typeToShowInPicture;
            string additionalMessage;
            if (isFakeType)
            {
                additionalMessage = $" (compatible with {desireType})";
                typeToShowInPicture = desireType;
            }
            else
            {
                additionalMessage = "";
                typeToShowInPicture = this.selected.targetType;
            }

            //At40xModule.Ready();
            Logs.Log.AddEntryAsFinishOf(this.At40xModule, $"{R.T_NOW_USING_}: {this.selected.name}{additionalMessage}");
            At40xModule.View.SetReflectorPicture(typeToShowInPicture);
            this.At40xModule.ReflectorChange();

            // setting variables
            IsSelected = true;
            At40xModule.View.IsWaiting = false; // flag to stop showing the inprogress message
            At40xModule.StatusChange($"{R.T_SET_TO} {actualType}", P.StepTypes.End);
            At40xModule._status.IsWaiting = false;
            At40xModule._status.isTrackerBusy = false;
            Debug.WriteInConsole($"End OnGetReflectorAnswer");
        }

        bool firstTimeReceiveingReflector = true;
        private void CheckForReFlectorWithUnusalConstante(TSU_TargetType targetType)
        {
            try
            {
                if (firstTimeReceiveingReflector)
                {
                    firstTimeReceiveingReflector = false;
                    if (targetType == TSU_TargetType.ES_TT_RFIPrism || targetType == TSU_TargetType.ES_TT_GlassPrism)
                    {
                        string titleAndMessage = $"{R.T_NOW_USING_} {this.selected.targetType};{R.T_ATTENTION}";
                        new MessageInput(MessageType.Warning, titleAndMessage).Show();
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private InstrumentTypes GetGeodeInstrumentType(TSU_TargetType actualType, TSU_TargetType desireType)
        {
            switch (desireType)
            {
                case TSU_TargetType.ES_TT_CCR35:
                    return InstrumentTypes.CCR3_5;

                case TSU_TargetType.ES_TT_RRR15:
                    return InstrumentTypes.RRR1_5;

                case TSU_TargetType.ES_TT_CCR15:
                    return InstrumentTypes.CCR1_5;

                case TSU_TargetType.ES_TT_CatsEye:
                case TSU_TargetType.ES_TT_GlassPrism:
                case TSU_TargetType.ES_TT_RFIPrism:
                    return InstrumentTypes.RFI0_5;

                case TSU_TargetType.ES_TT_RRR05:
                case TSU_TargetType.ES_TT_BRR05:
                    return InstrumentTypes.RRR0_5;

                case TSU_TargetType.ES_TT_TBR05:
                    return InstrumentTypes.TBR0_5;

                case TSU_TargetType.ES_TT_AutoCollMirror:
                case TSU_TargetType.ES_TT_RRR0875:
                case TSU_TargetType.ES_TT_Unknown:
                case TSU_TargetType.ES_TT_CornerCube:
                case TSU_TargetType.ES_TT_BRR15:
                default:
                    return InstrumentTypes.CCR1_5;
            }
        }

        public TSU_TargetType RequestedType;
        public InstrumentTypes InstrumentType;

        /// <summary>
        /// change reflector in AT controller based on the refector received with the incoming measure
        /// </summary>
        /// <param name="distance"></param>
        /// <returns></returns>
        internal bool IsValidReflectorThenSetIt(MeasureOfDistance distance)
        {
            try
            {
                bool valid = IsValidReflector(distance.Reflector, out TSU_TargetType type);
                if (valid)
                {
                    RequestedType = type;
                    InstrumentType = distance.Reflector._InstrumentType;

                    TSU_TargetType knownType;
                    if (IsKnownFromThisAt(type))
                    {
                        knownType = type;
                    }
                    else
                    {
                        knownType = SwithToRightKnownReflector(type);
                        if (!IsKnownFromThisAt(knownType))
                            throw new Exception($"{type} and {knownType} not supported/known by your AT40x");
                    }


                    if (this.selected.targetType != knownType)
                        if (this.At40xModule.sync != null)
                        {
                            Action inner = () => { this.Set(knownType); };
                            if (this.At40xModule.InstrumentIsOn && !At40xModule.ConnectionInProgress)  // 20200929 add conenctionin progress to avoid problem when renamming a wrong at
                                this.At40xModule.View.TrySyncActionWithMessage(inner, R.T_CHANGE_REFLECTOR, R.T_SWITCHING_TO + " RRR 1.5 inch", 1, 2000);
                        }
                }
                else
                {
                    FindAndAddReflectorToMeasure(distance, this.selected.targetType);
                }
                return valid;
            }
            catch (Exception ex)
            {
                new MessageInput(MessageType.Critical, ex.Message)
                {
                    ButtonTexts = new List<string> { R.T_OK + "!" }
                }.Show();
                return false;
            }

        }

        internal TSU_TargetType SwithToRightKnownReflector(TSU_TargetType type)
        {
            switch (type)
            {
                case TSU_TargetType.ES_TT_GlassPrism:
                case TSU_TargetType.ES_TT_RFIPrism:
                case TSU_TargetType.ES_TT_TBR05:
                    return TSU_TargetType.ES_TT_RFIPrism;

                case TSU_TargetType.ES_TT_BRR05:
                case TSU_TargetType.ES_TT_RRR05:
                    return TSU_TargetType.ES_TT_RRR05;

                case TSU_TargetType.ES_TT_AutoCollMirror:
                case TSU_TargetType.ES_TT_CatsEye:
                    throw new Exception($"{type} not supported yet");

                case TSU_TargetType.ES_TT_RRR15:
                case TSU_TargetType.ES_TT_BRR15:
                case TSU_TargetType.ES_TT_RRR0875:
                case TSU_TargetType.ES_TT_Unknown:
                case TSU_TargetType.ES_TT_CornerCube:
                case TSU_TargetType.ES_TT_CCR15:
                case TSU_TargetType.ES_TT_CCR35:
                default:
                    return TSU_TargetType.ES_TT_RRR15;
            }
        }

        internal TSU_TargetType GetTsuType(Reflector.Reflector reflector)
        {
            IsValidReflector(reflector, out TSU_TargetType type);
            return type;
        }

        private bool IsKnownFromThisAt(TSU_TargetType type)
        {
            return !this.listBoth.Any() || this.listBoth.Any(x => x.targetType == type);
        }

        internal bool IsValidReflector(Reflector.Reflector reflector, out TSU_TargetType type)
        {
            bool valid = true;
            switch (reflector._Model)
            {
                case "CCR3.5":
                    type = TSU_TargetType.ES_TT_CCR35;
                    break;
                case "CCR1.5":
                    type = TSU_TargetType.ES_TT_CCR15;
                    break;
                case "RRR0.5":
                    type = TSU_TargetType.ES_TT_RRR05;
                    break;
                case "RRR1.5":
                    type = TSU_TargetType.ES_TT_RRR15;
                    break;
                case "BRR1.5":
                    type = TSU_TargetType.ES_TT_BRR15;
                    break;
                case "BRR0.5":
                    type = TSU_TargetType.ES_TT_BRR05;
                    break;
                case "RFI0.5":
                    type = TSU_TargetType.ES_TT_RFIPrism;
                    break;
                case "TBR0.5":
                    type = TSU_TargetType.ES_TT_TBR05;
                    break;

                default:
                    type = TSU_TargetType.ES_TT_RRR15;
                    valid = false;
                    break;
            }
            return valid;
        }
        internal void FindAndAddReflectorToMeasure(MeasureOfDistance distance, TSU_TargetType eS_TargetType)
        {
            Reflector.Reflector r = FindReflectorInstanceFromAtType(eS_TargetType);
            if (r != null)
                distance.Reflector = r;
        }

        private void Async_GetReflector()
        {
            At40xModule._status.isTrackerBusy = true;
            Debug.WriteInConsole("at40x-ASYNC-GetReflector");
            At40xModule.async.GetReflector();
        }

        public void GetReflector()
        {
            try
            {
                Async_GetReflector(); // this async so next step is in OnReceiveReflectorData(....)
            }
            catch (Exception)
            {

            }
        }

        #endregion

        #region Probe
        public void GetProbe()
        {
            int id, i;
            Debug.WriteInConsole("at40x-SYNC-GetTipAdapter");
            At40xModule.sync.GetTipAdapter(out id, out i);

            Debug.WriteInConsole("at40x-ASYNC-SetMeasurementMode");
            At40xModule.async.SetMeasurementMode(ES_MeasMode.ES_MM_Stationary);
        }

        internal void OnGetProbesAnswer(object sender, Receiver.GetProbesAnswerEventArgs e)
        {
            Debug.WriteInConsole($"Begin OnGetProbesAnswer ProbeType={e.ProbeType}");

            if (e.ProbeType != ES_ProbeType.ES_PT_None)
            {
                this.listProbes.Add(new Structure
                {
                    id = 100 + e.IProbeID,
                    name = e.ProbeName,
                    targetType = TSU_TargetType.ES_TT_Unknown,
                    probeType = e.ProbeType,
                    surfaceOffset = 0,
                    numberOfProbes = e.IProbesTotal
                });
            }
            if (this.listProbes.Count == e.IProbesTotal)
            {
                this.At40xModule.async.RemoveFromInProgressListIfExist(ES_Command.ES_C_GetReflectors);
                At40xModule.StatusChange(R.T_PROBES_RECEIVED, P.StepTypes.End);
                allProbesReceived.Set();
                //this.At40xModule._status.tracker = ES_TrackerStatus.ES_TS_NotReady;
                //this.At40xModule.Ready();
            }
        }

        internal void OnGetProbeAnswer(object sender, Receiver.GetProbeAnswerEventArgs e)
        {
            int probeID = e.ProbeID;

            if (probeID != 0)
            {
                selected = listProbes.Find(x => x.id == probeID + 100);
                string temp = selected.probeType.ToString();
                string newPrism = temp.Substring(6, temp.Length - 6);
                IsSelected = true;
                Logs.Log.AddEntryAsFinishOf(this.At40xModule, $"{R.T_NOW_USING_PROBE}:" + newPrism);
                At40xModule.View.SetReflectorPicture(selected.probeType);
                At40xModule.View.IsWaiting = false; // flag to stop showingthe inprogress message
                At40xModule._status.IsWaiting = false;
            }
        }

        public void Set(ES_ProbeType probeType)
        {
            try
            {
                if (!At40xModule.InstrumentIsOn) throw new Exception(R.T_INSTRUMENT_IS_OFF);

                Debug.WriteInConsole("at40x-SYNC-SetStatisticMode");
                At40xModule.sync.SetStatisticMode(ES_StatisticMode.ES_SM_Extended, ES_StatisticMode.ES_SM_Extended);
                int id = listProbes.Find(x => x.probeType == probeType).id;
                timer = new System.Windows.Forms.Timer();
                timer.Interval = 5000;
                timer.Tick += new EventHandler(ProbeTick);
                timerTick = 1;
                timer.Start();
                Debug.WriteInConsole("at40x-ASYNC-SetTipAdapter");
                At40xModule.async.SetTipAdapter(id - 100);
            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_IMPOSSIBLE_TO_SET_PROBE}: " + ex.Message, ex);
            }
        }
        public bool GetMeasurementProbeInfo(out ES_ProbeType probeType)
        {
            int iFirmwareMajorVersionNumber;
            int iFirmwareMinorVersionNumber;
            int lSerialNumber;
            int lCompensationIdNumber;
            int lActiveField;
            ES_ProbeConnectionType connectionType;
            int lNumberOfTipAdapters;
            ES_ProbeButtonType probeButtonType;
            int lNumberOfFields;
            bool bHasWideAngleReceiver;
            int lNumberOfTipDataSets;
            int lNumberOfMelodies;
            int lNumberOfLoudnesSteps;
            try
            {

                Debug.WriteInConsole("at40x-SYNC-GetMeasurementProbeInfo");
                At40xModule.sync.GetMeasurementProbeInfo(
                    out iFirmwareMajorVersionNumber,
                    out iFirmwareMinorVersionNumber,
                    out lSerialNumber,
                    out probeType,
                    out lCompensationIdNumber,
                    out lActiveField,
                    out connectionType,
                    out lNumberOfTipAdapters,
                    out probeButtonType,
                    out lNumberOfFields,
                    out bHasWideAngleReceiver,
                    out lNumberOfTipDataSets,
                    out lNumberOfMelodies,
                    out lNumberOfLoudnesSteps);
                return true;
            }
            catch (Exception)
            {
                probeType = ES_ProbeType.ES_PT_None;
                return false;
            }
        }
        private void ProbeTick(object sender, EventArgs e)
        {
            switch (timerTick)
            {
                case 0:
                    timerTick += 1;
                    break;
                case 1:
                    if (At40xModule.View.IsWaiting) // flag to stop showingthe inprogress message
                    {
                        this.GetProbe();
                    }
                    timer.Stop();
                    timer.Dispose();
                    break;

                default:
                    break;
            }
        }
        #endregion

        #region Search & Lock
        public bool Catch(int approxDistance = 20)
        {
            try
            {
                bool shouldFindRefelctor = !At40xModule._status.isTrackerLockedAndReady;

                this.TryingToLock = true;

                At40xModule.StatusChange($"{R.T_LOCKING}...", P.StepTypes.Begin);
                At40xModule.timeSpentMeasuring.Add("sync FindReflector (lock)", TimeSpent.type.Started, DateTime.Now);

                Debug.WriteInConsole("at40x-SYNC-FindReflector");

                if (shouldFindRefelctor)
                {
                    At40xModule._status.isTrackerBusy = true;
                    At40xModule.sync.FindReflector(approxDistance);

                    this.At40xModule._status.isTrackerLockedAndReady = true;
                }

                if (At40xModule._BeingMeasuredTheodoliteData != null)
                    At40xModule.timeSpentMeasuring.Add("sync FindReflector (lock)", TimeSpent.type.Stopped, DateTime.Now);

                // TODO TEST this is added here to see if it remove the "10000003 : invalid result" problem: seems to work was not working with 800ms
                if (false)
                {
                    if (At40xModule._measurement.mode == Module.aT40xMeasurementMode.Standard)
                        Thread.Sleep(1000);
                    if (At40xModule._measurement.mode == Module.aT40xMeasurementMode.Precise)
                        Thread.Sleep(1200);
                }

                At40xModule.StatusChange($"{R.T_LOCKED2}", P.StepTypes.End);

                At40xModule.Ready();
                return true;
            }
            catch (Exception ex)
            {
                At40xModule.StatusChange($"{R.T_NOT} {R.T_LOCKED2}", P.StepTypes.End);

                this.At40xModule._status.isTrackerLockedAndReady = false;
                At40xModule.Ready();
                throw new Exception(R.T_LOCKING_PROBLEM_THE_AT40X_DIDNT_MANAGE_TO_LOCK_ON_REFLECTOR);
            }
        }
        public bool Release()
        {
            try
            {

                At40xModule._status.isTrackerBusy = true;


                this.TryingToLock = false;

                At40xModule.StatusChange($"{R.T_RELEASING_TARGET}...", P.StepTypes.Begin);

                Debug.WriteInConsole("at40x-SYNC-PositionRelativeHV");
                At40xModule.sync.PositionRelativeHV(0.3, 0.3);

                At40xModule.StatusChange($"{R.T_RELEASED_TARGET}", P.StepTypes.End);

                At40xModule._status.isTrackerLockedAndReady = false;
                At40xModule.Ready();
                return true;
            }
            catch (Exception)
            {
                At40xModule._status.isTrackerBusy = false;
                At40xModule.StatusChange($"{R.T_RELEASE_FAILED}", P.StepTypes.End);
                return false;
            }
        }
        internal void PowerSearch()
        {
            try
            {
                At40xModule.StatusChange($"{R.T_SEARCHING}...", P.StepTypes.Begin);

                Debug.WriteInConsole("at40x-SYNC-SetSearchParams");
                At40xModule.sync.SetSearchParams(0.5, 20000);

                Debug.WriteInConsole("at40x-SYNC-FindReflector");
                At40xModule.sync.FindReflector(3);

                Debug.WriteInConsole("at40x-SYNC-SetSearchParams");
                At40xModule.sync.SetSearchParams(0.04, 10000); // minimum time is 10000 !! :(

                At40xModule.StatusChange(R.T_FOUND, P.StepTypes.End);

                At40xModule.Ready();
            }
            catch (Exception)
            {
                At40xModule.StatusChange(R.T_SEARCH_FAILED, P.StepTypes.End);
            }
        }
        #endregion

        // Ask reflector id to at40x
        internal void Get()
        {
            int id;

            Debug.WriteInConsole("at40x-SYNC-GetProbe");
            At40xModule.sync.GetProbe(out id);
            if (id == 0) // no probe
            {
                GetReflector();
            }
            //this.At40xModule.Ready();
        }



        public void Set(TSU_TargetType targetType)
        {
            try
            {
                if (!At40xModule.InstrumentIsOn) throw new Exception($"{R.T_INSTRUMENT_IS_OFF}");
                At40xModule.StatusChange($"{R.T_SETTING} {targetType}", P.StepTypes.Begin);

                Debug.WriteInConsole("at40x-SYNC-SetStatisticMode");
                At40xModule.sync.SetStatisticMode(ES_StatisticMode.ES_SM_Standard, ES_StatisticMode.ES_SM_Standard);
                if (listReflectors.Count == 0) throw new Exception(string.Format(R.T_AT_REFLECTORLISTEMPTY));


                // cast TSU to ES because CCR1.5 and 3.5 doesnt exist for AT
                TSU_TargetType wantedType = targetType;
                if (targetType == TSU_TargetType.ES_TT_CCR15 || targetType == TSU_TargetType.ES_TT_CCR35)
                    wantedType = TSU_TargetType.ES_TT_RRR15;

                if (this.selected.targetType != wantedType)
                {
                    int id = listReflectors.Find(x => x.targetType == wantedType).id;
                    Debug.WriteInConsole("at40x-ASYNC-SetReflector");

                    At40xModule.sync.SetReflector(id);
                    GetReflector(); // async
                }
                else
                {
                    At40xModule.StatusChange($"{R.T_SET_TO} {targetType}", P.StepTypes.End);
                    At40xModule.View.SetReflectorPicture(targetType);
                }


            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_IMPOSSIBLE_TO_SET_REFLECTOR}: " + ex.Message, ex);
            }
        }
    }

    #endregion
}
