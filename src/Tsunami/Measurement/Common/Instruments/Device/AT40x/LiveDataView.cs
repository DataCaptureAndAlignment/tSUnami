﻿using System;
using System.Collections.Generic;
using System.Drawing;
using T = System.Timers;
using System.Windows.Forms;
using TSU.Common.Elements;
using DT = TSU.Polar.GuidedModules.Steps.StakeOut.DisplayTypes;
using L = TSU.Common.Instruments.Device.AT40x.LiveData;
using R = TSU.Properties.Resources;


namespace TSU.Common.Instruments.Device.AT40x
{
    public partial class LiveDataView : SubView
    {
        #region fields and proprerties

        Color goodColor1 = TSU.Tsunami2.Preferences.Theme.Colors.Good;
        Color goodColor2 = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
        Color bofColor1 = TSU.Tsunami2.Preferences.Theme.Colors.Attention;
        Color bofColor2 = TSU.Tsunami2.Preferences.Theme.Colors.Action;
        Color badColor = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
        Color colorOne = TSU.Tsunami2.Preferences.Theme.Colors.DarkBackground;
        Color colorTwo = TSU.Tsunami2.Preferences.Theme.Colors.Background;
        bool showMM = true;



        internal LiveData LiveData
        {
            get
            {
                return (this.ParentView as AT40x.View).Module.LiveData;
            }
        }

        internal AT40x.Module at40xModule
        {

            get
            {
                return (this.ParentView as AT40x.View).Module as AT40x.Module;
            }
        }

        public bool IsVisible
        {
            get
            {
                return this.Parent != null;
            }
        }

        List<Button> ButtonsCoordinateType;



        #endregion

        public LiveDataView(AT40x.View parentView)
            : base(parentView)
        {
            InitializeComponent();
            this.ApplyThemeColors();
            this.ParentView = parentView;
            this._Name = "Live Data panel";
            toolTip1.SetToolTip(this.buttonCCS, R.T_TIP_CCS);
            toolTip1.SetToolTip(this.buttonSU, R.T_TIP_SU);
            toolTip1.SetToolTip(this.buttonPhys, R.T_TIP_PHYS);
            toolTip1.SetToolTip(this.buttonBeam, R.T_CS_BEAM_DETAILS);
            toolTip1.SetToolTip(this.buttonBeamV, R.T_CS_BEAMV_DETAILS);
            toolTip1.SetToolTip(this.button2Station, R.T_TIP_2ST);

            toolTip1.SetToolTip(this.buttonHVD, R.T_TIP_HVD);
            toolTip1.SetToolTip(this.buttonXYZ, R.T_TIP_COORD);
            toolTip1.SetToolTip(this.buttonOffset, R.T_TIP_DIFF);
            toolTip1.SetToolTip(this.buttonDispl, R.T_TIP_Display);
            toolTip1.SetToolTip(this.buttonMMM, R.T_TIP_MMM);
            toolTip1.SetToolTip(this.buttonFindClosest, R.T_TIP_Closest);

            toolTip1.SetToolTip(this.buttonIncrease, R.T_TIP_INCREASE);
            toolTip1.SetToolTip(this.buttonDecrease, R.T_TIP_DECREASE);
            toolTip1.SetToolTip(this.button_Stop, R.T_STOPPED);
            toolTip1.SetToolTip(this.button_Start, R.T_START_LIVE_DATA_IN);


            ButtonsCoordinateType = new List<Button>() { buttonSU, buttonPhys, buttonCCS, button2Station, buttonBeam, buttonBeamV };


            this.LiveData.HasAGoodMeasurement += OnGoodMeasurement;
            this.LiveData.HasARoughMeasurement += OnRoughMeasurement;
            this.LiveData.HasFailedToMeasure += OnFailedToMeasure;
            this.LiveData.HasStarted += OnStart;
            this.LiveData.HasStopped += OnStop;

            colorTimer = new T.Timer()
            {
                Interval = 100
            };
            colorTimer.AutoReset = true;
            colorTimer.Elapsed += OnColorTimerTick;
        }

        private void OnColorTimerTick(object sender, T.ElapsedEventArgs e)
        {
            if (this.InvokeRequired)
            {
                // Invoke the event handler on the main UI thread
                this.BeginInvoke(new Action(() => OnColorTimerTick(sender, e)));
                return;
            }

            int r = currentColor.R;
            int g = currentColor.G;
            int b = currentColor.B;

            if (r < 246) r += 10;
            if (g > 9) g -= 10;
            if (b > 9) b -= 10;

            currentColor = Color.FromArgb(r, g, b);
            ColorizeLabels(currentColor);

            if (r >= 246 && b < 10)
                colorTimer.Stop();
        }

        private void ApplyThemeColors()
        {
            this.label1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.label1.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.label2.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.label2.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.label3.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.label3.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.buttonHVD.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.buttonXYZ.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.buttonOffset.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.buttonCCS.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.buttonSU.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.buttonPhys.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.button2Station.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.buttonBeam.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.buttonIncrease.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.buttonDecrease.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.buttonMMM.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.buttonFindClosest.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.button_Stop.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.buttonDispl.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.buttonBeamV.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.button_Start.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.buttonMap.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
        }

        #region update


        internal void Update(int resolution)
        {
            if (this.InvokeRequired)
            {
                // Invoke the event handler on the main UI thread
                this.BeginInvoke(new System.Action(() => Update(resolution)));
            }
            else
            {
                this.TreatButtons();
                this.TreatLabels(resolution);
                //Texts


                //to force the redraw?
                this.Invalidate();
                this.LiveData.ModificationInProgress = false;
            }

        }


        #endregion

        #region Colors

        private Color currentColor;

        private void ColorizeLabels(Color color)
        {

            this.label1.ForeColor = color;
            this.label2.ForeColor = color;
            this.label3.ForeColor = color;
            currentColor = color;
        }

        private void TreatLabels(int resolution)
        {
            //string[,] ps = GetPrefixesAndSuffixes(LiveData.displayType, LiveData.coordinateType, showMM);

            Coordinates c = LiveData.FinalResults;
            int factor = 1;

            string precisionFormat = "";
            int correctedResolution = resolution;
            string unity = "m";
            if (showMM)
            {
                correctedResolution -= 3;
                factor = 1000;
                unity = "mm";
            }

            var cs = Tsunami2.Properties.CoordinatesSystems.SelectedType;
            var displayType = Tsunami2.Properties.ValueToDisplayType;
            string prefix = "";
            switch (displayType)
            {
                case DT.Offsets:
                    prefix = "o";
                    break;
                case DT.Displacement:
                    prefix = "d";
                    break;
                case DT.Observations:
                case DT.Coordinates:
                default:
                    break;
            }

            switch (displayType)
            {
                case DT.Observations:
                    precisionFormat = "F" + resolution.ToString();
                    string obs1 = TreatOneLine(LiveData.val1, "H", "gon", 1, precisionFormat);
                    string obs2 = TreatOneLine(LiveData.val2, "V", "gon", 1, precisionFormat);
                    precisionFormat = "F" + correctedResolution.ToString();
                    string obs3 = TreatOneLine(LiveData.val3, "D", unity, factor, precisionFormat);
                    UpdateLabels(obs1, obs2, obs3, LiveData?.lastMeasure?._OriginalPoint?._Name ?? string.Empty);
                    break;
                case DT.Offsets:
                case DT.Coordinates:
                case DT.Displacement:
                default:
                    precisionFormat = "F" + correctedResolution.ToString();
                    string labelX = prefix + cs.AxisNames[0];
                    string labelY = prefix + cs.AxisNames[1];
                    string labelZ = prefix + cs.AxisNames[2];
                    double na = Tsunami2.Preferences.Values.na;
                    string x = TreatOneLine(c.X?.Value ?? na, labelX, unity, factor, precisionFormat);
                    string y = TreatOneLine(c.Y?.Value ?? na, labelY, unity, factor, precisionFormat);
                    string z = TreatOneLine(c.Z?.Value ?? na, labelZ, unity, factor, precisionFormat);
                    string name = LiveData?.lastMeasure?._OriginalPoint?._Name ?? string.Empty;
                    UpdateLabels(x, y, z, name);
                    break;
            }

            ResizeLabels();

            if (this.LiveData.StoppingInProgress)
            {
                this.ColorizeLabels(badColor);
                //button_Stop.BackgroundImage = R.At40x_Live_Play;
            }
        }

        private string TreatOneLine(double value, string prefix, string suffix, int factor, string presitionFormat)
        {
            double na = TSU.Tsunami2.Preferences.Values.na;
            int numberOfSpaceToAdd = 4 - prefix.Length;
            for (int i = 0; i < numberOfSpaceToAdd; i++)
            {
                prefix = " " + prefix;
            }
            string valueToShow = (value != na) ? (value * factor).ToString(presitionFormat) : " n-a";
            string minusOrNot = valueToShow[0] == '-' ? "" : " ";

            return $"{prefix} ={minusOrNot}{valueToShow} {suffix}";
        }

        private void TreatButtons()
        {
            if (this.InvokeRequired)
            {
                // Invoke the event handler on the main UI thread
                this.BeginInvoke(new Action(() => TreatButtons()));
            }
            else
            {
                var displayType = Tsunami2.Properties.ValueToDisplayType;
                TreatOneButton(buttonHVD, true, displayType == DT.Observations);
                TreatOneButton(buttonXYZ, true, displayType == DT.Coordinates);
                TreatOneButton(buttonOffset, true, displayType == DT.Offsets);
                TreatOneButton(buttonDispl, true, displayType == DT.Displacement);
                TreatOneButton(buttonFindClosest, LiveData.Point._Coordinates.HasAny, this.LiveData.FindClosest);

                //FindAvailableCoordinateType();
                var cType = TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedType;
                switch (displayType)
                {
                    case DT.Coordinates:
                    case DT.Offsets:
                    case DT.Displacement:
                        //TreatOneButton(buttonCCS, true, cType._Name ==  "CCS");
                        TreatOneButton(buttonSU, true, cType._Name.ToUpper() == "SU");
                        TreatOneButton(buttonPhys, true, cType._Name.ToUpper() == "PHYSICIST");
                        TreatOneButton(buttonBeam, true, cType._Name.ToUpper() == "BEAM");
                        //TreatOneButton(button2Station, true, cType._Name == "ToStation");
                        TreatOneButton(buttonBeamV, true, cType._Name.ToUpper() == "BEAMV");
                        break;
                    case DT.Observations:
                    default:
                        this.buttonCCS.Visible = false;
                        this.buttonSU.Visible = false;
                        this.buttonPhys.Visible = false;
                        this.buttonBeam.Visible = false;
                        this.button2Station.Visible = false;
                        this.buttonBeamV.Visible = false;
                        break;
                }
            }

        }

        //private void FindAvailableCoordinateType()
        //{
        //    int count = 0;

        //    var cType =TSU.Tsunami2.TsunamiPreferences.Values.typeOfCs;
        //    while (count<8)
        //    {
        //        switch (LiveData.coordinateType)
        //        {
        //            case E.CoordinateSystemsTsunamiTypes.CCS:
        //                if (LiveData.Point._Coordinates.HasCcs)
        //                    return;
        //                else
        //                    LiveData.coordinateType = E.CoordinateSystemsTsunamiTypes.PHYS;
        //                break;
        //            case E.CoordinateSystemsTsunamiTypes.PHYS:
        //                if (LiveData.Point._Coordinates.HasPhysicist)
        //                    return;
        //                else
        //                    LiveData.coordinateType = E.CoordinateSystemsTsunamiTypes.SU;
        //                break;
        //            case E.CoordinateSystemsTsunamiTypes.SU:
        //                if (LiveData.Point._Coordinates.HasSu)
        //                    return;
        //                else
        //                    LiveData.coordinateType = E.CoordinateSystemsTsunamiTypes.ToStation;
        //                break;
        //            case E.CoordinateSystemsTsunamiTypes.ToStation:
        //                if (LiveData.Point._Coordinates.HasStationCs)
        //                    return;
        //                else
        //                    LiveData.coordinateType = E.CoordinateSystemsTsunamiTypes.Beam;
        //                break;
        //            case E.CoordinateSystemsTsunamiTypes.Beam:
        //                if (LiveData.Point._Coordinates.HasBeam)
        //                    return;
        //                else
        //                    LiveData.coordinateType = E.CoordinateSystemsTsunamiTypes.BeamV;
        //                break;
        //            case E.CoordinateSystemsTsunamiTypes.BeamV:
        //                if (LiveData.Point._Coordinates.HasBeamV)
        //                    return;
        //                else
        //                    LiveData.coordinateType = E.CoordinateSystemsTsunamiTypes.CCS;
        //                break;
        //            case E.CoordinateSystemsTsunamiTypes.MLA:
        //            case E.CoordinateSystemsTsunamiTypes.UserDefined:
        //            case E.CoordinateSystemsTsunamiTypes.Unknown:
        //            default:
        //                LiveData.coordinateType = E.CoordinateSystemsTsunamiTypes.CCS;
        //                break;
        //        }
        //        count++;
        //    }
        //}

        private void TreatOneButton(Button button, bool visibilityCondition, bool coloredCondition)
        {
            if (this.InvokeRequired)
            {
                // Invoke the event handler on the main UI thread
                this.BeginInvoke(new System.Action(() => TreatOneButton(button, visibilityCondition, coloredCondition)));
            }
            else
            {
                button.Visible = visibilityCondition;
                button.BackColor = coloredCondition ? TSU.Tsunami2.Preferences.Theme.Colors.Object : TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            }
        }

        #endregion

        #region Labels

        internal void UpdateLabels(string s1, string s2, string s3, string s0 = "")
        {
            if (this.InvokeRequired)
            {
                // Invoke the event handler on the main UI thread
                this.BeginInvoke(new System.Action(() => UpdateLabels(s1, s2, s3, s0)));
            }
            else
            {
                this.label0.Text = s0;
                this.label1.Text = s1;
                this.label2.Text = s2;
                this.label3.Text = s3;
            }
        }

        public void ResetLabels(string text = "...")
        {
            UpdateLabels(text, text, text);
            this.Refresh();
        }

        public static class FontCache
        {
            private static readonly Dictionary<string, Font> _fontCache = new Dictionary<string, Font>();

            public static Font GetFont(string fontFamily, float size, FontStyle style)
            {
                string key = $"{fontFamily}-{size}-{style}";

                if (!_fontCache.TryGetValue(key, out Font font))
                {
                    font = new Font(fontFamily, size, style);
                    _fontCache[key] = font;
                }

                return font;
            }

            public static void Dispose()
            {
                foreach (var font in _fontCache.Values)
                {
                    font.Dispose();
                }
                _fontCache.Clear();
            }
        }

        private void ResizeLabels()
        {
            if (this.InvokeRequired)
            {
                // Invoke the event handler on the main UI thread
                this.BeginInvoke(new System.Action(() => ResizeLabels()));
            }
            else
            {
                double factorToReduceInterLineSpace = 1;

                int disponible = this.Height - this.buttonCCS.Height;
                bool pointNameMustBeDisplay = label0.Text != "" && label0.Text != R.String_Unknown;
                int neededLabelCount = pointNameMustBeDisplay ? 4 : 3;
                int disponiblePerLabel = disponible / neededLabelCount;
                disponiblePerLabel = disponiblePerLabel < 2 ? 2 : disponiblePerLabel;

                label0.Visible = pointNameMustBeDisplay;

                label1.Font = FontCache.GetFont(TSU.Tsunami2.Preferences.Theme.Fonts.fontNameMonospaced, disponiblePerLabel, FontStyle.Bold);

                bool ok = false;
                int count = 0;
                while (!ok)
                {
                    var measuredSize = System.Windows.Forms.TextRenderer.MeasureText(label1.Text, label1.Font);
                    int w = measuredSize.Width;
                    int h = measuredSize.Height;
                    label1.Height = h;
                    label1.Width = w;

                    ok = true;

                    int maxWidthAllowed = this.Width - this.buttonCCS.Width * 2;
                    double widthNeededFactor = 1;
                    if (w > maxWidthAllowed)
                    {
                        widthNeededFactor = (double)maxWidthAllowed / w;
                        ok = false;
                    }

                    int maxHeightAllowed = (int)(disponiblePerLabel / factorToReduceInterLineSpace);
                    double heightNeededFactor = 1;
                    if (h > maxHeightAllowed)
                    {
                        heightNeededFactor = (double)maxHeightAllowed / h;
                        ok = false;
                    }

                    if (!ok)
                    {

                        double factorNeeded = Math.Min(widthNeededFactor, heightNeededFactor);
                        double newSize = (int)(label1.Font.Size * factorNeeded * 10) / 10;
                        float size;
                        if (label1.Font.Size < 20)
                        {
                            size = 20;
                            break;
                        }
                        else
                            size = label1.Font.Size - 5f;

                        label1.Font = FontCache.GetFont(label1.Font.FontFamily.ToString(), (float)newSize, label1.Font.Style);
                        label0.Font = FontCache.GetFont(label1.Font.FontFamily.ToString(), (float)newSize / 3 * 2, label1.Font.Style);
                    }

                    count++;
                }

                int spaceToAddBetweenFirstLabels = (int)(label0.Height * factorToReduceInterLineSpace);
                int spaceToAddBetweenLabels = (int)(label1.Height * factorToReduceInterLineSpace);
                if (pointNameMustBeDisplay)
                {
                    label1.Location = new System.Drawing.Point(label0.Location.X, label0.Location.Y + spaceToAddBetweenFirstLabels);
                }
                else
                {
                    label1.Location = new System.Drawing.Point(label0.Location.X, label0.Location.Y);
                }

                label2.Font = label1.Font;
                label2.Location = new System.Drawing.Point(label1.Location.X, label1.Location.Y + spaceToAddBetweenLabels);
                label3.Font = label1.Font;
                label3.Location = new System.Drawing.Point(label1.Location.X, label1.Location.Y + 2 * spaceToAddBetweenLabels);
            }
        }

        #endregion

        #region Measurement

        private void OnStop(object sender, L.LiveDataEventArgs e)
        {
            //this.button_Stop.BackgroundImage = R.At40x_Live_Play;
            ColorizeLabels(this.badColor);
            Logs.Log.AddEntryAsFinishOf(this.At40xModule, "Live Data stopped");
        }

        private void OnStart(object sender, L.LiveDataEventArgs e)
        {
            //this.button_Stop.BackgroundImage = R.At40x_Live_Stop;

        }

        private void OnFailedToMeasure(object sender, L.LiveDataEventArgs e)
        {
            ColorizeLabels(this.badColor);
        }

        T.Timer colorTimer;

        private void OnRoughMeasurement(object sender, L.LiveDataEventArgs e)
        {
            ColorizeLabels((currentColor == bofColor1) ? bofColor2 : bofColor1);
            Update(LiveData.resolution);
        }

        private void OnGoodMeasurement(object sender, L.LiveDataEventArgs e)
        {
            ColorizeLabels(goodColor1);
            colorTimer.Start();
            //ColorizeLabels((currentColor == goodColor1) ? goodColor2 : goodColor1);
            Update(LiveData.resolution);
        }

        internal override void Start()
        {
            base.Start();
            ResetLabels("Press '>' to start");

            //this.button_Stop.BackgroundImage = (LiveData.IsOn) ? R.At40x_Live_Stop : R.At40x_Live_Play;


        }


        internal override void Stop()
        {
            base.Stop();

        }



        #endregion

        #region Behaviour

        //private void LiveDataView_VisibleChanged(object sender, EventArgs e)
        //{
        //    this.setOptimalLabelFontSize();
        //    if (!this.Visible) this.LiveData.Stop();
        //}

        private void LiveDataView_Resize(object sender, EventArgs e)
        {
            ResizeLabels();
        }

        private void buttons_MouseEnter(object sender, EventArgs e)
        {
            (sender as Button).BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Action;
        }

        private void buttons_MouseLeaving(object sender, EventArgs e)
        {
            (sender as Button).BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
        }

        private void InvokeModification()
        {
            if (LiveData.IsOn)
            {
                this.LiveData.ModificationInProgress = true;
                this.ColorizeLabels(badColor);
            }
            //this.button_Stop.BackgroundImage = (LiveData.IsOn) ? R.At40x_Live_Stop : R.At40x_Live_Play;

        }

        #endregion

        #region Button Actions

        // ON OFF
        internal void ButtonStop_Click(object sender, EventArgs e)
        {
            this.At40xModule.View.TrySyncActionWithMessage(() =>
            {
                InvokeModification();
                this.LiveData.StoppingInProgress = true;
                this.ColorizeLabels(badColor);
                //this.button_Stop.BackgroundImage = R.At40x_Live_Play;
                this.LiveData.Stop();

            }, "Stopping Livedata", "Finishing current measurement", 1, 1500, true);

            // why to launch if it is comming from the laucnh already? this is a loop!
            //if (sender is AT40x.View)
            //{
            //    (sender as AT40x.View).LaunchMeasure();
            //}
        }


        private void button_Start_Click(object sender, EventArgs e)
        {
            this.TryAction(() =>
            {
                InvokeModification();
                this.LiveData.StoppingInProgress = false;
                ResetLabels("Starting...");
                //this.button_Stop.BackgroundImage = R.At40x_Live_Stop;
                this.LiveData.Start();
            });
            //onLiveData.OnValue = true;
        }

        // Type
        private void ButtonHVD_Click(object sender, EventArgs e)
        {
            this.showMM = false;
            TSU.Tsunami2.Properties.ValueToDisplayType = DT.Observations;
            TreatButtons();
        }

        private void ButtonXYZ_Click(object sender, EventArgs e)
        {
            this.showMM = false;
            TSU.Tsunami2.Properties.ValueToDisplayType = DT.Coordinates;
            TreatButtons();
        }

        private void ButtonDiff_Click(object sender, EventArgs e)
        {
            this.showMM = true;
            TSU.Tsunami2.Properties.ValueToDisplayType = DT.Offsets;
            InvokeModification();
            TreatButtons();
        }

        private void buttonDispl_Click(object sender, EventArgs e)
        {
            this.showMM = true;
            TSU.Tsunami2.Properties.ValueToDisplayType = DT.Displacement;
            InvokeModification();
            TreatButtons();
        }

        // Options
        private void ButtonMMM_Click(object sender, EventArgs e)
        {
            InvokeModification();
            this.showMM = !this.showMM;
            TreatButtons();
        }

        private void button_FindClosest_Click(object sender, EventArgs e)
        {
            InvokeModification();
            this.LiveData.FindClosest = !this.LiveData.FindClosest;
            this.buttonFindClosest.BackColor = (this.LiveData.FindClosest) ? TSU.Tsunami2.Preferences.Theme.Colors.Highlight : TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            TreatButtons();
        }

        private void ButtonIncrease_Click(object sender, EventArgs e)
        {
            this.TryAction(() =>
            {
                InvokeModification();
                this.UpdateLabels(R.T344, R.T522, "...");

                if (LiveData.resolution < 6) // 6 is the max resolution we 'd like to accept
                {
                    bool change = LiveData.resolution == 3;
                    LiveData.resolution += 1;
                    if (change) this.LiveData.SwitchFromRoughToFastMeasurement();

                }

            });
        }

        private void ButtonDecrease_Click(object sender, EventArgs e)
        {
            this.TryAction(() =>
            {
                InvokeModification();
                this.UpdateLabels(R.T344, R.T522, "...");
                if (LiveData.resolution > 3) // 0 is the min resolution we 'd like to accept
                {
                    bool change = LiveData.resolution == 4;
                    LiveData.resolution -= 1;

                    if (change) this.LiveData.SwitchFromFastToRoughMeasurement();

                }
            });
        }

        // Coordinates
        private void ButtonSU_Click(object sender, EventArgs e)
        {
            InvokeModification();
            TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedType = CoordinateSystem.GetByNameIn("SU");
            TreatButtons();
        }

        private void Button2Station_Click(object sender, EventArgs e)
        {
            InvokeModification();
            TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedType = CoordinateSystem.GetByNameIn("ToStation");
            TreatButtons();
        }

        private void ButtonPhys_Click(object sender, EventArgs e)
        {
            InvokeModification();
            TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedType = CoordinateSystem.GetByNameIn("PHYSICIST");
            TreatButtons();
        }

        private void ButtonCCS_Click(object sender, EventArgs e)
        {
            InvokeModification();
            TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedType = CoordinateSystem.GetByNameIn("CCS");
            TreatButtons();
        }

        private void ButtonBeam_Click(object sender, EventArgs e)
        {
            InvokeModification();
            TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedType = CoordinateSystem.GetByNameIn("Beam");
            TreatButtons();
        }


        #endregion

        private void buttonBeamV_Click(object sender, EventArgs e)
        {
            InvokeModification();
            TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedType = CoordinateSystem.GetByNameIn("BeamV");
            //TreatButtons();
        }

        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);
            var a = this._Name;
            if (this.Visible == false)
            {
                try
                {
                    if (this.Parent.Parent.Parent.Parent.Parent.Parent.Parent.Parent.Parent is TSU.Views.MessageModuleView)
                    {
                        this.LiveData.StopAnyMeasurement();
                    }
                }
                catch (Exception)
                {

                }

            }
        }

        private void buttonMap_Click(object sender, EventArgs e)
        {
            ShowMessageOfImage(R.T_COORDINATES_SYSTEM_MAP, R.T_THANKS, R.CS_Systems);
        }
        /// <summary>
        /// show all CS existing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOthers_Click(object sender, EventArgs e)
        {
            Polar.GuidedModules.Steps.StakeOut.CoordinateButtons.SetAction(null);
            var controls = Polar.GuidedModules.Steps.StakeOut.CoordinateButtons.GetCSTypeButtons();
            controls.Insert(0, Polar.GuidedModules.Steps.StakeOut.CoordinateButtons.GetMapButton());
            this.ShowPopUpSubMenu(Polar.GuidedModules.Steps.StakeOut.CoordinateButtons.GetCSTypeButtons(), "CS live data");
        }
    }
}
