﻿using System;
using System.Windows.Forms;
using TSU.ENUM;
using static TSU.Tools.Conversions.Angles;
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments.Device.AT40x
{
    public partial class JogView : SubView
    {
        public JogView()
        {
            this._Name = "Jog panel";
            InitializeComponent();
            this.ApplyThemeColors();
        }

        public JogView(View parentView)
            : base(parentView)
        {
            InitializeComponent();
            this.ParentView = parentView;
        }

        private void ApplyThemeColors()
        {
            Preferences.GUI.Theme.ThemeColors colors = Tsunami2.Preferences.Theme.Colors;
            this.panelJog.BackColor = colors.Background;
            this.tableLayoutPanel1.BackColor = colors.Background;
            this.middlePictureBox.BackColor = colors.Background;
            this.buttonPlusLeft.BackColor = colors.Background;
            this.buttonPlusRight.BackColor = colors.Background;
            this.buttonPlusUp.BackColor = colors.Background;
            this.buttonPlusDown.BackColor = colors.Background;
            this.buttonPlusStop.BackColor = colors.Background;
            this.textBox1.BackColor = colors.Background;
            this.textBox1.ForeColor = colors.LightForeground;
            this.toolStrip2.BackColor = colors.Background;
            this.toolStripButtonUp.BackColor = colors.Background;
            this.toolStripButtonUp.ImageTransparentColor = colors.Transparent;
            this.toolStripButtonDown.BackColor = colors.Background;
            this.toolStripButtonDown.ImageTransparentColor = colors.Transparent;
            this.toolStripButtonLeft.BackColor = colors.Background;
            this.toolStripButtonLeft.ImageTransparentColor = colors.Transparent;
            this.toolStripButtonRight.BackColor = colors.Background;
            this.toolStripButtonRight.ImageTransparentColor = colors.Transparent;
            this.toolStripButtonStop.BackColor = colors.Background;
            this.toolStripButtonStop.ImageTransparentColor = colors.Transparent;
            this.BackColor = colors.Background;

        }

        internal override void Start()
        {
            base.Start();
            this.textBox1.Focus();
        }

        internal override void Stop()
        {
            base.Stop();

            At40xModule._motor.MoveAt401(movingDirection.None);
            At40xModule.async.RemoveFromInProgressListIfExist(EmScon.ES_Command.ES_C_MoveHV);
            this.textBox1.Text = "Not moving";
            this.middlePictureBox.BackgroundImage = R.At40x_moveNot;
        }

        private void MoveButton_Click(object sender, EventArgs e)
        {
            try
            {
                string direction = "";
                if (sender is Control c)
                    direction = c.Tag.ToString();
                if (sender is ToolStripItem tsi)
                    direction = tsi.Tag.ToString();
                this.textBox1.Text = direction;
                movingDirection movingDirection = Motor.GetDirectionFromText(direction);
                Move(movingDirection);
                this.textBox1.Focus();
            }
            catch (Exception ex)
            {
                throw new Exception(R.T_PROBLEM_WHEN_TRYING_TO_JOG + ex.Message, ex);
            }
        }

        internal void MovebyDirection(movingDirection direction)
        {
            this.textBox1.Text = direction.ToString();
        }

        private void On_TextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox tb = (sender as TextBox);
            if (tb.Parent.Parent.Parent.Parent is JogView jv)
            {
                jv.On_TsuView_KeyDown(sender, e);
            }
        }

        internal override void On_TsuView_KeyDown(object sender, KeyEventArgs e)
        {
            base.On_TsuView_KeyDown(sender, e); // nothing
            MoveBasedOnKey(e.KeyData);
        }

        internal void On_TsuView_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            MoveBasedOnKey(e.KeyData);
        }

        internal void MoveBasedOnKey(Keys key)
        {
            var motor = this.At40xView.Module._motor;

            movingDirection dir = movingDirection.None;
            switch (key)
            {
                case Keys.Right:
                    dir = movingDirection.Right;
                    break;
                case Keys.Left:
                    dir = movingDirection.Left;
                    break;
                case Keys.Up:
                    dir = movingDirection.Up;
                    break;
                case Keys.Down:
                    dir = movingDirection.Down;
                    break;
            }

            Move(dir);
        }

        private void Move(movingDirection dir)
        {
            var motor = this.At40xView.Module._motor;

            if (textBox2.Text.EndsWith(" gon"))
                textBox2.Text = textBox2.Text.Substring(0, textBox2.Text.Length - 3);

            if (double.TryParse(textBox2.Text, out double angle))
            {
                textBox2.Text += " gon";
                motor.MovebyAngle(dir, angle);
            }
            else
                motor.MovebyDirection(dir);
        }
    }
}
