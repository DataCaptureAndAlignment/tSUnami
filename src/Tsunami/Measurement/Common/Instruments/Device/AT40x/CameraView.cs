﻿using System;
using System.Windows.Forms;
using TSU.ENUM;
using TSU.Views;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Common.Instruments.Device.AT40x
{
    public partial class CameraView : SubView
    {

        public AxLTVIDEOLib.AxLTVideo video;

        public CameraView()
        {
            InitializeComponent();
            this.ApplyThemeColors();
        }
        private void ApplyThemeColors()
        {
            this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
        }
        public CameraView(AT40x.View parentView) : base(parentView)
        {
            InitializeComponent();
            this.ParentView = parentView;
            video = new AxLTVIDEOLib.AxLTVideo();
            var vid = video;
            vid.Top = 1;
            vid.Left = 1;

            vid.PreviewKeyDown += new PreviewKeyDownEventHandler(OnCameraPreviewKeyDown);
            vid.VideoClick += new AxLTVIDEOLib._DLTVideoEvents_VideoClickEventHandler(OnCameraVideoClick);


            this.Controls.Add(vid);
            this.SizeChanged += On_Size_Changed;
            this.buttonForFocusWithTabIndex0.GotFocus += ButtonForFocusWithTabIndex0_GotFocus;

            toolStrip2.BringToFront();
            vid.BringToFront();
        }

        private void ButtonForFocusWithTabIndex0_GotFocus(object sender, EventArgs e)
        {
            this.video.Focus();
        }

        private void On_Size_Changed(object sender, EventArgs e)
        {
            var vid = video;
            if (vid == null) return;

            int availableWidth = this.Width - this.toolStrip2.Width;
            double ratio = 1.4;
            if (this.Height * ratio < availableWidth)
            {
                vid.Height = this.Height;
                vid.Width = (int)(vid.Height * ratio);
            }
            else
            {
                vid.Width = availableWidth;
                vid.Height = (int)(vid.Width / ratio);
            }
        }


        internal override void Start()
        {
            base.Start(); // will put the view in the subviewPanel
            if (!At40xModule._camera.isOn)
                ((TsuView)this.At40xView).TryActionWithMessage(StartCamera, "The start of the camera");
            else
                ((TsuView)this.At40xView).TryAction(CameraStop, "The stop of the camera");
        }

        internal override void Stop()
        {
            base.Stop(); // will put the view in the subviewPanel
            ((TsuView)this.At40xView).TryAction(CameraStop, "The stop of the camera");
        }

        #region Movement Camera

        private void OnCameraPreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            try
            {
                string arrowKeyText = e.KeyCode.ToString();
                movingDirection md = Motor.GetDirectionFromText(arrowKeyText);
                At40xModule._motor.MovebyDirection(md);
            }
            catch (Exception ex)
            {
                Logs.Log.AddEntry(this._Module, new Exception(R.T_PROBLEM_WHEN_TRYING_TO_JOG + ex.Message, ex));
            }
        }

        private void OnCameraVideoClick(object sender, AxLTVIDEOLib._DLTVideoEvents_VideoClickEvent e)
        {
            switch (this.At40xModule._circle._selected)
            {
                case EmScon.ES_TrackerFace.ES_TF_Face2:
                    At40xModule._motor.PositionRelativeHV(T.Conversions.Angles.Rad2Gon(-e.deltaHz), T.Conversions.Angles.Rad2Gon(e.deltaVt));
                    break;
                case EmScon.ES_TrackerFace.ES_TF_Unknown:
                case EmScon.ES_TrackerFace.ES_TF_Face1:
                default:
                    At40xModule._motor.PositionRelativeHV(T.Conversions.Angles.Rad2Gon(e.deltaHz), T.Conversions.Angles.Rad2Gon(e.deltaVt));
                    break;
            }

        }

        internal void StartCamera()
        {
            At40xModule._camera.SetParameters(127, 127, 0);
            var vid = video;
            if (vid != null && !At40xModule._camera.isOn)
            {
                At40xModule._camera.Activate();
                vid.Visible = true;
                vid.ServerAddress = At40xModule.IP.ToString();
                vid.StartLiveImage();
            }

            Logs.Log.AddEntryAsFYI(this._Module, "Camera turned ON");
        }
        internal void CameraStop()
        {
            var vid = video;

            if (vid == null)
            {
                At40xModule._camera.isOn = false;
                return;
            }

            try
            {
                vid.StopLiveImage();
                vid.Visible = false;
                At40xModule._camera.isOn = false;
                vid = null;
                Logs.Log.AddEntryAsFYI(this._Module, "Camera turned Off");
            }
            catch (Exception)
            {
                if (At40xModule._camera.isOn)
                    Logs.Log.AddEntryAsPayAttentionOf(this._Module, "Camera didnt respond.");
                At40xModule._camera.isOn = false;
                vid.Visible = false;
            }

        }
        #endregion

        private void toolStripButtonMoreB_Click(object sender, EventArgs e)
        {
            this.At40xModule._camera.ChangeBrightness(50);
        }

        private void toolStripButtonLessB_Click(object sender, EventArgs e)
        {
            this.At40xModule._camera.ChangeBrightness(-50);
        }

        private void toolStripButtonMoreC_Click(object sender, EventArgs e)
        {

            this.At40xModule._camera.ChangeContrast(50);
        }

        private void toolStripButtonLessC_Click(object sender, EventArgs e)
        {

            this.At40xModule._camera.ChangeContrast(-50);
        }
    }
}
