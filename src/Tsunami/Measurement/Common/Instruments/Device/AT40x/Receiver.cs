﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Controls;
using EmScon;

namespace TSU.Common.Instruments.Device.AT40x
{
    internal class Receiver : CESCSAPIReceive
    {
        public Receiver()
        {
        }

        internal event EventHandler<CommandAnswerEventArgs> CommandAnswerEvent;
        protected override void OnCommandAnswer(BasicCommandRT A_0)
        {
            CommandAnswerEvent?.Invoke(this, new CommandAnswerEventArgs(A_0.command, A_0.status));
            if (A_0.status != ES_ResultStatus.ES_RS_AllOK)
                ErrorEvent?.Invoke(this, new CommandAnswerEventArgs(A_0.command, A_0.status));
        }

        internal event EventHandler<CommandAnswerEventArgs> ErrorEvent;
        protected override void OnErrorAnswer(ErrorResponseT A_0)
        {
            ErrorEvent?.Invoke(this, new CommandAnswerEventArgs(A_0.command, A_0.status));
        }

        internal class CommandAnswerEventArgs : EventArgs
        {
            public CommandAnswerEventArgs(ES_Command command, ES_ResultStatus status)
            {
                this.command = command;
                this.status = status;
            }

            public ES_Command command { get; }

            public ES_ResultStatus status { get; }
        }

        internal event EventHandler<SystemStatusChangeEventArgs> SystemStatusChangeEvent;
        protected override void OnSystemStatusChange(SystemStatusChangeT A_0)
        {
            SystemStatusChangeEvent?.Invoke(this, new SystemStatusChangeEventArgs(A_0));
        }
        internal class SystemStatusChangeEventArgs : EventArgs
        {
            public SystemStatusChangeEventArgs(SystemStatusChangeT systemStatusChangeT)
            {
                systemStatusChange = systemStatusChangeT.systemStatusChange;
            }

            public ES_SystemStatusChange systemStatusChange { get; }
        }

        internal event EventHandler<GetReflectorsAnswerEventArgs> GetReflectorsAnswerEvent;
        protected override void OnGetReflectorsAnswer(
            int iTotalReflectors,
            int iInternalReflectorId,
            ES_TargetType targetType,
            double dSurfaceOffset,
            byte[] cReflectorName)
        {
            GetReflectorsAnswerEvent?.Invoke(this, new GetReflectorsAnswerEventArgs(
                iTotalReflectors,
                iInternalReflectorId,
                targetType,
                dSurfaceOffset,
                UnicodeToStringUntilNulChar(cReflectorName)
            ));
        }
        internal class GetReflectorsAnswerEventArgs : EventArgs
        {
            public GetReflectorsAnswerEventArgs(int iTotalReflectors, int iInternalReflectorId, ES_TargetType targetType, double dSurfaceOffset, string cReflectorName)
            {
                TotalReflectors = iTotalReflectors;
                InternalReflectorId = iInternalReflectorId;
                TargetType = targetType;
                SurfaceOffset = dSurfaceOffset;
                ReflectorName = cReflectorName;
            }

            public int TotalReflectors { get; }
            public int InternalReflectorId { get; }
            public ES_TargetType TargetType { get; }
            public double SurfaceOffset { get; }
            public string ReflectorName { get; }
        }

        internal event EventHandler<GetCameraParamsAnswerEventArgs> GetCameraParamsAnswerEvent;
        protected override void OnGetCameraParamsAnswer(CameraParamsDataT A_0)
        {
            GetCameraParamsAnswerEvent?.Invoke(this, new GetCameraParamsAnswerEventArgs(A_0));
        }
        internal class GetCameraParamsAnswerEventArgs : EventArgs
        {
            public GetCameraParamsAnswerEventArgs(CameraParamsDataT cameraParamsData)
            {
                CameraParamsData = cameraParamsData;
            }

            public CameraParamsDataT CameraParamsData { get; }
        }

        internal event EventHandler<GetDirectionsAnswerEventArgs> GetDirectionsAnswerEvent;
        protected override void OnGetDirectionAnswer(double dHzAngle, double dVtAngle)
        {
            GetDirectionsAnswerEvent?.Invoke(this, new GetDirectionsAnswerEventArgs(dHzAngle, dVtAngle));
        }
        internal class GetDirectionsAnswerEventArgs : EventArgs
        {
            public GetDirectionsAnswerEventArgs(double dHzAngle, double dVtAngle)
            {
                DHzAngle = dHzAngle;
                DVtAngle = dVtAngle;
            }

            public double DHzAngle { get; }
            public double DVtAngle { get; }
        }

        internal event EventHandler<GetSystemSoftwareVersionAnswerEventArgs> GetSystemSoftwareVersionAnswerEvent;
        protected override void OnGetSystemSoftwareVersionAnswer(byte[] cSoftwareVersion)
        {
            GetSystemSoftwareVersionAnswerEvent?.Invoke(this, new GetSystemSoftwareVersionAnswerEventArgs(UnicodeToStringUntilNulChar(cSoftwareVersion)));
        }
        internal class GetSystemSoftwareVersionAnswerEventArgs : EventArgs
        {
            public GetSystemSoftwareVersionAnswerEventArgs(string v)
            {
                SystemSoftwareVersion = v;
            }

            public string SystemSoftwareVersion { get; }
        }

        internal event EventHandler<GetEnvironmentParamsAnswerEventArgs> GetEnvironmentParamsAnswerEvent;
        protected override void OnGetEnvironmentParamsAnswer(EnvironmentDataT A_0)
        {
            GetEnvironmentParamsAnswerEvent?.Invoke(this, new GetEnvironmentParamsAnswerEventArgs(A_0));
        }
        internal class GetEnvironmentParamsAnswerEventArgs : EventArgs
        {
            public GetEnvironmentParamsAnswerEventArgs(EnvironmentDataT environmentDataT)
            {
                EnvironmentDataT = environmentDataT;
            }

            public EnvironmentDataT EnvironmentDataT { get; }
        }

        internal event EventHandler<GetSystemStatusAnswerEventArgs> GetSystemStatusAnswerEvent;
        protected override void OnGetSystemStatusAnswer(
            ES_ResultStatus lastResultStatus,
            ES_TrackerProcessorStatus trackerProcessorStatus,
            ES_LaserProcessorStatus laserStatus,
            ES_ADMStatus admStatus,
            ESVersionNumberT esVersionNumber,
            ES_WeatherMonitorStatus weatherMonitorStatus,
            int lFlagsValue,
            int lTrackerSerialNumber)
        {
            GetSystemStatusAnswerEvent?.Invoke(this, new GetSystemStatusAnswerEventArgs(
                lastResultStatus,
                trackerProcessorStatus,
                laserStatus,
                admStatus,
                esVersionNumber,
                weatherMonitorStatus,
                lFlagsValue,
                lTrackerSerialNumber
            ));
        }
        internal class GetSystemStatusAnswerEventArgs : EventArgs
        {
            public GetSystemStatusAnswerEventArgs(
                ES_ResultStatus lastResultStatus,
                ES_TrackerProcessorStatus trackerProcessorStatus,
                ES_LaserProcessorStatus laserStatus,
                ES_ADMStatus admStatus,
                ESVersionNumberT esVersionNumber,
                ES_WeatherMonitorStatus weatherMonitorStatus,
                int lFlagsValue,
                int lTrackerSerialNumber)
            {
                LastResultStatus = lastResultStatus;
                TrackerProcessorStatus = trackerProcessorStatus;
                LaserStatus = laserStatus;
                AdmStatus = admStatus;
                EsVersionNumber = esVersionNumber;
                WeatherMonitorStatus = weatherMonitorStatus;
                LFlagsValue = lFlagsValue;
                LTrackerSerialNumber = lTrackerSerialNumber;
            }

            public ES_ResultStatus LastResultStatus { get; }
            public ES_TrackerProcessorStatus TrackerProcessorStatus { get; }
            public ES_LaserProcessorStatus LaserStatus { get; }
            public ES_ADMStatus AdmStatus { get; }
            public ESVersionNumberT EsVersionNumber { get; }
            public ES_WeatherMonitorStatus WeatherMonitorStatus { get; }
            public int LFlagsValue { get; }
            public int LTrackerSerialNumber { get; }
        }

        internal event EventHandler<GetTrackerStatusAnswerEventArgs> GetTrackerStatusAnswerEvent;
        protected override void OnGetTrackerStatusAnswer(ES_TrackerStatus A_0)
        {
            GetTrackerStatusAnswerEvent?.Invoke(this, new GetTrackerStatusAnswerEventArgs(A_0));
        }
        internal class GetTrackerStatusAnswerEventArgs : EventArgs
        {
            public GetTrackerStatusAnswerEventArgs(ES_TrackerStatus trackerStatus)
            {
                TrackerStatus = trackerStatus;
            }

            public ES_TrackerStatus TrackerStatus { get; }
        }

        internal event EventHandler<NivelMeasurementAnswerEventArgs> NivelMeasurementAnswerEvent;
        protected override void OnNivelMeasurementAnswer(NivelResultT A_0)
        {
            NivelMeasurementAnswerEvent?.Invoke(this, new NivelMeasurementAnswerEventArgs(A_0));
        }
        internal class NivelMeasurementAnswerEventArgs : EventArgs
        {
            public NivelMeasurementAnswerEventArgs(NivelResultT nivelMeasurement)
            {
                NivelMeasurement = nivelMeasurement;
            }

            public NivelResultT NivelMeasurement { get; }
        }

        internal event EventHandler<GetMeasurementStatusInfoAnswerEventArgs> GetMeasurementStatusInfoAnswerEvent;
        protected override void OnGetMeasurementStatusInfoAnswer(int measurementStatusInfo)
        {
            GetMeasurementStatusInfoAnswerEvent?.Invoke(this, new GetMeasurementStatusInfoAnswerEventArgs(measurementStatusInfo));
        }
        internal class GetMeasurementStatusInfoAnswerEventArgs : EventArgs
        {
            public GetMeasurementStatusInfoAnswerEventArgs(int measurementStatusInfo)
            {
                MeasurementStatusInfo = measurementStatusInfo;
            }

            public int MeasurementStatusInfo { get; }
        }

        internal event EventHandler<GetLongSystemParamAnswerEventArgs> GetLongSystemParamAnswerEvent;
        protected override void OnGetLongSystemParamAnswer(int lParameter)
        {
            GetLongSystemParamAnswerEvent?.Invoke(this, new GetLongSystemParamAnswerEventArgs(lParameter));
        }
        internal class GetLongSystemParamAnswerEventArgs : EventArgs
        {
            public GetLongSystemParamAnswerEventArgs(int lParameter)
            {
                LParameter = lParameter;
            }

            public int LParameter { get; }
        }

        internal event EventHandler<GetFaceAnswerEventArgs> GetFaceAnswerEvent;
        protected override void OnGetFaceAnswer(ES_TrackerFace A_0)
        {
            GetFaceAnswerEvent?.Invoke(this, new GetFaceAnswerEventArgs(A_0));
        }
        internal class GetFaceAnswerEventArgs : EventArgs
        {
            public GetFaceAnswerEventArgs(ES_TrackerFace getFaceAnswer)
            {
                GetFaceAnswer = getFaceAnswer;
            }

            public ES_TrackerFace GetFaceAnswer { get; }
        }

        internal event EventHandler<GetMeasurementModeAnswerEventArgs> GetMeasurementModeAnswerEvent;
        protected override void OnGetMeasurementModeAnswer(ES_MeasMode A_0)
        {
            GetMeasurementModeAnswerEvent?.Invoke(this, new GetMeasurementModeAnswerEventArgs(A_0));
        }
        internal class GetMeasurementModeAnswerEventArgs : EventArgs
        {
            public GetMeasurementModeAnswerEventArgs(ES_MeasMode measMode)
            {
                MeasMode = measMode;
            }

            public ES_MeasMode MeasMode { get; }
        }

        internal event EventHandler<GetProbeAnswerEventArgs> GetProbeAnswerEvent;
        protected override void OnGetProbeAnswer(int iProbeID)
        {
            GetProbeAnswerEvent?.Invoke(this, new GetProbeAnswerEventArgs(iProbeID));
        }
        internal class GetProbeAnswerEventArgs : EventArgs
        {
            public GetProbeAnswerEventArgs(int iProbeID)
            {
                ProbeID = iProbeID;
            }

            public int ProbeID { get; }
        }

        internal event EventHandler<GetTipAdapterAnswerEventArgs> GetTipAdapterAnswerEvent;
        protected override void OnGetTipAdapterAnswer(int iTipAdapterID, int iTipAdapterInterface)
        {
            Debug.WriteInConsole($"Receiver.OnGetTipAdapterAnswer");
            GetTipAdapterAnswerEvent?.Invoke(this, new GetTipAdapterAnswerEventArgs(
                iTipAdapterID,
                iTipAdapterInterface
            ));
        }
        internal class GetTipAdapterAnswerEventArgs : EventArgs
        {
            public GetTipAdapterAnswerEventArgs(int iTipAdapterID, int iTipAdapterInterface)
            {
                ITipAdapterID = iTipAdapterID;
                ITipAdapterInterface = iTipAdapterInterface;
            }

            public int ITipAdapterID { get; }
            public int ITipAdapterInterface { get; }
        }

        internal event EventHandler<GetMeasurementProbeInfoAnswerEventArgs> GetMeasurementProbeInfoAnswerEvent;
        protected override void OnGetMeasurementProbeInfoAnswer(
            int iFirmwareMajorVersionNumber,
            int iFirmwareMinorVersionNumber,
            int lSerialNumber,
            ES_ProbeType probeType,
            int lCompensationIdNumber,
            int lActiveField,
            ES_ProbeConnectionType connectionType,
            int lNumberOfTipAdapters,
            ES_ProbeButtonType probeButtonType,
            int lNumberOfFields,
            bool bHasWideAngleReceiver,
            int lNumberOfTipDataSets,
            int lNumberOfMelodies,
            int lNumberOfLoudnesSteps)
        {
            GetMeasurementProbeInfoAnswerEvent?.Invoke(this, new GetMeasurementProbeInfoAnswerEventArgs(
                iFirmwareMajorVersionNumber,
                iFirmwareMinorVersionNumber,
                lSerialNumber,
                probeType,
                lCompensationIdNumber,
                lActiveField,
                connectionType,
                lNumberOfTipAdapters,
                probeButtonType,
                lNumberOfFields,
                bHasWideAngleReceiver,
                lNumberOfTipDataSets,
                lNumberOfMelodies,
                lNumberOfLoudnesSteps
            ));
        }
        internal class GetMeasurementProbeInfoAnswerEventArgs : EventArgs
        {
            public GetMeasurementProbeInfoAnswerEventArgs(
                int iFirmwareMajorVersionNumber,
                int iFirmwareMinorVersionNumber,
                int lSerialNumber,
                ES_ProbeType probeType,
                int lCompensationIdNumber,
                int lActiveField,
                ES_ProbeConnectionType connectionType,
                int lNumberOfTipAdapters,
                ES_ProbeButtonType probeButtonType,
                int lNumberOfFields,
                bool bHasWideAngleReceiver,
                int lNumberOfTipDataSets,
                int lNumberOfMelodies,
                int lNumberOfLoudnesSteps)
            {
                IFirmwareMajorVersionNumber = iFirmwareMajorVersionNumber;
                IFirmwareMinorVersionNumber = iFirmwareMinorVersionNumber;
                LSerialNumber = lSerialNumber;
                ProbeType = probeType;
                LCompensationIdNumber = lCompensationIdNumber;
                LActiveField = lActiveField;
                ConnectionType = connectionType;
                LNumberOfTipAdapters = lNumberOfTipAdapters;
                ProbeButtonType = probeButtonType;
                LNumberOfFields = lNumberOfFields;
                BHasWideAngleReceiver = bHasWideAngleReceiver;
                LNumberOfTipDataSets = lNumberOfTipDataSets;
                LNumberOfMelodies = lNumberOfMelodies;
                LNumberOfLoudnesSteps = lNumberOfLoudnesSteps;
            }

            public int IFirmwareMajorVersionNumber { get; }
            public int IFirmwareMinorVersionNumber { get; }
            public int LSerialNumber { get; }
            public ES_ProbeType ProbeType { get; }
            public int LCompensationIdNumber { get; }
            public int LActiveField { get; }
            public ES_ProbeConnectionType ConnectionType { get; }
            public int LNumberOfTipAdapters { get; }
            public ES_ProbeButtonType ProbeButtonType { get; }
            public int LNumberOfFields { get; }
            public bool BHasWideAngleReceiver { get; }
            public int LNumberOfTipDataSets { get; }
            public int LNumberOfMelodies { get; }
            public int LNumberOfLoudnesSteps { get; }
        }

        internal event EventHandler<GetProbesAnswerEventArgs> GetProbesAnswerEvent;
        protected override void OnGetProbesAnswer(
            int iProbeID,
            int lSerialNumber,
            ES_ProbeType probeType,
            int iNumberOfFields,
            byte[] cProbeName,
            byte[] cProbeComment,
            int iProbesTotal)
        {
            GetProbesAnswerEvent?.Invoke(this, new GetProbesAnswerEventArgs(
                 iProbeID,
                 lSerialNumber,
                 probeType,
                 iNumberOfFields,
                 UnicodeToStringUntilNulChar(cProbeName),
                 UnicodeToStringUntilNulChar(cProbeComment),
                 iProbesTotal
            ));
        }
        internal class GetProbesAnswerEventArgs : EventArgs
        {
            public GetProbesAnswerEventArgs(
                int iProbeID,
                int lSerialNumber,
                ES_ProbeType probeType,
                int iNumberOfFields,
                string probeName,
                string probeComment,
                int iProbesTotal)
            {
                IProbeID = iProbeID;
                LSerialNumber = lSerialNumber;
                ProbeType = probeType;
                INumberOfFields = iNumberOfFields;
                ProbeName = probeName;
                ProbeComment = probeComment;
                IProbesTotal = iProbesTotal;
            }

            public int IProbeID { get; }
            public int LSerialNumber { get; }
            public ES_ProbeType ProbeType { get; }
            public int INumberOfFields { get; }
            public string ProbeName { get; }
            public string ProbeComment { get; }
            public int IProbesTotal { get; }
        }

        internal event EventHandler<GetReflectorAnswerEventArgs> GetReflectorAnswerEvent;
        protected override void OnGetReflectorAnswer(int iInternalReflectorId)
        {
            GetReflectorAnswerEvent?.Invoke(this, new GetReflectorAnswerEventArgs(iInternalReflectorId));
        }
        internal class GetReflectorAnswerEventArgs : EventArgs
        {
            public GetReflectorAnswerEventArgs(int iInternalReflectorId)
            {
                InternalReflectorId = iInternalReflectorId;
            }

            public int InternalReflectorId { get; }
        }

        internal event EventHandler<StationaryProbeMeasurementAnswerEventArgs> StationaryProbeMeasurementAnswerEvent;
        protected override void OnStationaryProbeMeasurementAnswer(ProbeStationaryResultT A_0)
        {
            Debug.WriteInConsole($"Receiver.OnStationaryProbeMeasurementAnswer");
            StationaryProbeMeasurementAnswerEvent?.Invoke(this, new StationaryProbeMeasurementAnswerEventArgs(A_0));
        }
        internal class StationaryProbeMeasurementAnswerEventArgs : EventArgs
        {
            public StationaryProbeMeasurementAnswerEventArgs(ProbeStationaryResultT probeStationaryResult)
            {
                ProbeStationaryResult = probeStationaryResult;
            }

            public ProbeStationaryResultT ProbeStationaryResult { get; }
        }

        internal event EventHandler<SingleMeasurementAnswerEventArgs> SingleMeasurementAnswerEvent;
        protected override void OnSingleMeasurementAnswer(SingleMeasResultT A_0)
        {
            Debug.WriteInConsole($"Receiver.OnSingleMeasurementAnswer");
            Thread.Sleep(100); // for some reason after a precise or standard measurement, the tracker claims to be busy if dont wait a bit
            SingleMeasurementAnswerEvent?.Invoke(this, new SingleMeasurementAnswerEventArgs(A_0));
        }
        internal class SingleMeasurementAnswerEventArgs : EventArgs
        {
            public SingleMeasurementAnswerEventArgs(SingleMeasResultT singleMeasResult)
            {
                SingleMeasResult = singleMeasResult;
            }

            public SingleMeasResultT SingleMeasResult { get; }
        }

        internal event EventHandler<ReflectorPosAnswerEventArgs> ReflectorPosAnswerEvent;
        protected override void OnReflectorPosAnswer(ReflectorPosResultT A_0)
        {
            Debug.WriteInConsole($"Receiver.OnReflectorPosAnswer");
            ReflectorPosAnswerEvent?.Invoke(this, new ReflectorPosAnswerEventArgs(A_0));
        }
        internal class ReflectorPosAnswerEventArgs : EventArgs
        {
            public ReflectorPosAnswerEventArgs(ReflectorPosResultT reflectorPosResult)
            {
                ReflectorPosResult = reflectorPosResult;
            }

            public ReflectorPosResultT ReflectorPosResult { get; }
        }


        internal event EventHandler MoveHVAnswerEvent;
        protected override void OnMoveHVAnswer()
        {
            MoveHVAnswerEvent?.Invoke(this, new EventArgs());
        }

        internal event EventHandler SetReflectorAnswerEvent;
        protected override void OnSetReflectorAnswer()
        {
            SetReflectorAnswerEvent?.Invoke(this, new EventArgs());
        }

        internal class GetCoordinateSystemTypeAnserEventArgs
        {
            public GetCoordinateSystemTypeAnserEventArgs(ES_CoordinateSystemType coordinateSystemType)
            {
                this.CoordinateSystemType = coordinateSystemType;
            }

            public ES_CoordinateSystemType CoordinateSystemType { get; }
        }

        internal event EventHandler<GetCoordinateSystemTypeAnserEventArgs> GetCoordinateSystemTypeAnswerEvent;
        protected override void OnGetCoordinateSystemTypeAnswer(ES_CoordinateSystemType A_0)
        {
            GetCoordinateSystemTypeAnswerEvent?.Invoke(this, new GetCoordinateSystemTypeAnserEventArgs(A_0));
        }

        internal class GetUnitsAnswerEventArgs
        {
            public GetUnitsAnswerEventArgs(SystemUnitsDataT systemUnitsData)
            {
                this.SystemUnitsData = systemUnitsData;
            }

            public SystemUnitsDataT SystemUnitsData { get; }
        }

        internal event EventHandler<GetUnitsAnswerEventArgs> GetUnitsAnswerEvent;
        protected override void OnGetUnitsAnswer(SystemUnitsDataT A_0)
        {
            GetUnitsAnswerEvent?.Invoke(this, new GetUnitsAnswerEventArgs(A_0));
        }

        internal class GetStatisticModeAnswerEventArgs
        {
            public GetStatisticModeAnswerEventArgs(ES_StatisticMode stationaryMeasurements, ES_StatisticMode continuousMeasurements)
            {
                StationaryMeasurements = stationaryMeasurements;
                ContinuousMeasurements = continuousMeasurements;
            }

            public ES_StatisticMode StationaryMeasurements { get; }

            public ES_StatisticMode ContinuousMeasurements { get; }
        }


        internal event EventHandler<GetStatisticModeAnswerEventArgs> GetStatisticModeAnswerEvent;
        protected override void OnGetStatisticModeAnswer(ES_StatisticMode stationaryMeasurements, ES_StatisticMode continuousMeasurements)
        {
            GetStatisticModeAnswerEvent?.Invoke(this, new GetStatisticModeAnswerEventArgs(stationaryMeasurements, continuousMeasurements));
        }

        /// <summary>
        /// To decode the names, we use UTF16 decoding
        /// But the length of the byte array is wrong, the relevant part of the string is terminated by a nul character \u0000
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        internal static string UnicodeToStringUntilNulChar(byte[] bytes)
        {
            string s = Encoding.Unicode.GetString(bytes);
            int end = s.IndexOf('\u0000');
            string ret;
            if (end != -1)
                ret = s.Substring(0, end);
            else
                ret = s;
            return ret;
        }

    }
}
