﻿using TSU.Views;
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments.Device.AT40x
{
    partial class RemoteView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelProbe = new TSU.Views.TsuPanel();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelProbeD = new System.Windows.Forms.Label();
            this.labelProbeC = new System.Windows.Forms.Label();
            this.labelProbeB = new System.Windows.Forms.Label();
            this.labelProbeA = new System.Windows.Forms.Label();
            this.pictureBoxC = new System.Windows.Forms.PictureBox();
            this.pictureBoxB = new System.Windows.Forms.PictureBox();
            this.pictureBoxA = new System.Windows.Forms.PictureBox();
            this.pictureBoxD = new System.Windows.Forms.PictureBox();
            this.labelTips = new System.Windows.Forms.Label();
            this.panelProbe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxD)).BeginInit();
            this.SuspendLayout();
            // 
            // panelProbe
            // 
            this.panelProbe.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.panelProbe.Controls.Add(this.labelTitle);
            this.panelProbe.Controls.Add(this.labelProbeD);
            this.panelProbe.Controls.Add(this.labelProbeC);
            this.panelProbe.Controls.Add(this.labelProbeB);
            this.panelProbe.Controls.Add(this.labelProbeA);
            this.panelProbe.Controls.Add(this.pictureBoxC);
            this.panelProbe.Controls.Add(this.pictureBoxB);
            this.panelProbe.Controls.Add(this.pictureBoxA);
            this.panelProbe.Controls.Add(this.pictureBoxD);
            this.panelProbe.Controls.Add(this.labelTips);
            this.panelProbe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelProbe.Location = new System.Drawing.Point(5, 5);
            this.panelProbe.Margin = new System.Windows.Forms.Padding(10);
            this.panelProbe.Name = "panelProbe";
            this.panelProbe.Padding = new System.Windows.Forms.Padding(10);
            this.panelProbe.Size = new System.Drawing.Size(308, 254);
            this.panelProbe.TabIndex = 20;
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.labelTitle.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
            this.labelTitle.Location = new System.Drawing.Point(13, 10);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(60, 13);
            this.labelTitle.TabIndex = 9;
            this.labelTitle.Text = "Title here...";
            // 
            // labelProbeD
            // 
            this.labelProbeD.AutoSize = true;
            this.labelProbeD.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.labelProbeD.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.labelProbeD.Location = new System.Drawing.Point(115, 205);
            this.labelProbeD.Name = "labelProbeD";
            this.labelProbeD.Size = new System.Drawing.Size(15, 13);
            this.labelProbeD.TabIndex = 4;
            this.labelProbeD.Text = "D";
            this.labelProbeD.Click += new System.EventHandler(this.pictureBoxD_Click);
            // 
            // labelProbeC
            // 
            this.labelProbeC.AutoSize = true;
            this.labelProbeC.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.labelProbeC.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.labelProbeC.Location = new System.Drawing.Point(13, 124);
            this.labelProbeC.Name = "labelProbeC";
            this.labelProbeC.Size = new System.Drawing.Size(14, 13);
            this.labelProbeC.TabIndex = 3;
            this.labelProbeC.Text = "C";
            this.labelProbeC.Click += new System.EventHandler(this.pictureBoxC_Click);
            // 
            // labelProbeB
            // 
            this.labelProbeB.AutoSize = true;
            this.labelProbeB.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.labelProbeB.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.labelProbeB.Location = new System.Drawing.Point(212, 124);
            this.labelProbeB.Name = "labelProbeB";
            this.labelProbeB.Size = new System.Drawing.Size(14, 13);
            this.labelProbeB.TabIndex = 2;
            this.labelProbeB.Text = "B";
            this.labelProbeB.Click += new System.EventHandler(this.pictureBoxB_Click);
            // 
            // labelProbeA
            // 
            this.labelProbeA.AutoSize = true;
            this.labelProbeA.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.labelProbeA.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.labelProbeA.Location = new System.Drawing.Point(127, 51);
            this.labelProbeA.Name = "labelProbeA";
            this.labelProbeA.Size = new System.Drawing.Size(14, 13);
            this.labelProbeA.TabIndex = 1;
            this.labelProbeA.Text = "A";
            this.labelProbeA.Click += new System.EventHandler(this.pictureBoxA_Click);
            // 
            // pictureBoxC
            // 
            this.pictureBoxC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxC.BackgroundImage = global::TSU.Properties.Resources.At40x_ProbeOff;
            this.pictureBoxC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxC.Location = new System.Drawing.Point(46, 93);
            this.pictureBoxC.Name = "pictureBoxC";
            this.pictureBoxC.Size = new System.Drawing.Size(26, 28);
            this.pictureBoxC.TabIndex = 7;
            this.pictureBoxC.TabStop = false;
            this.pictureBoxC.Click += new System.EventHandler(this.pictureBoxC_Click);
            this.pictureBoxC.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxC_MouseDown);
            this.pictureBoxC.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxC_MouseUp);
            // 
            // pictureBoxB
            // 
            this.pictureBoxB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxB.BackgroundImage = global::TSU.Properties.Resources.At40x_ProbeOff;
            this.pictureBoxB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxB.Location = new System.Drawing.Point(149, 174);
            this.pictureBoxB.Name = "pictureBoxB";
            this.pictureBoxB.Size = new System.Drawing.Size(26, 28);
            this.pictureBoxB.TabIndex = 6;
            this.pictureBoxB.TabStop = false;
            this.pictureBoxB.Click += new System.EventHandler(this.pictureBoxB_Click);
            this.pictureBoxB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxB_MouseDown);
            this.pictureBoxB.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxB_MouseUp);
            // 
            // pictureBoxA
            // 
            this.pictureBoxA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxA.BackgroundImage = global::TSU.Properties.Resources.At40x_ProbeOff;
            this.pictureBoxA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxA.Location = new System.Drawing.Point(149, 20);
            this.pictureBoxA.Name = "pictureBoxA";
            this.pictureBoxA.Size = new System.Drawing.Size(26, 28);
            this.pictureBoxA.TabIndex = 5;
            this.pictureBoxA.TabStop = false;
            this.pictureBoxA.Click += new System.EventHandler(this.pictureBoxA_Click);
            this.pictureBoxA.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxA_MouseDown);
            this.pictureBoxA.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxA_MouseUp);
            // 
            // pictureBoxD
            // 
            this.pictureBoxD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxD.BackgroundImage = global::TSU.Properties.Resources.At40x_ProbeOff;
            this.pictureBoxD.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxD.Location = new System.Drawing.Point(239, 93);
            this.pictureBoxD.Name = "pictureBoxD";
            this.pictureBoxD.Size = new System.Drawing.Size(26, 28);
            this.pictureBoxD.TabIndex = 8;
            this.pictureBoxD.TabStop = false;
            this.pictureBoxD.Click += new System.EventHandler(this.pictureBoxD_Click);
            this.pictureBoxD.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxD_MouseDown);
            this.pictureBoxD.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxD_MouseUp);
            // 
            // labelTips
            // 
            this.labelTips.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTips.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.labelTips.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
            this.labelTips.Location = new System.Drawing.Point(13, 218);
            this.labelTips.Name = "labelTips";
            this.labelTips.Size = new System.Drawing.Size(282, 26);
            this.labelTips.TabIndex = 10;
            this.labelTips.Text = "Tips here...";
            this.labelTips.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // RemoteView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 264);
            this.Controls.Add(this.panelProbe);
            this.Name = "RemoteView";
            this.Resize += new System.EventHandler(this.panelProbe_Resize);
            this.panelProbe.ResumeLayout(false);
            this.panelProbe.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxD)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Views.TsuPanel panelProbe;
        private System.Windows.Forms.Label labelProbeD;
        private System.Windows.Forms.Label labelProbeC;
        private System.Windows.Forms.Label labelProbeB;
        private System.Windows.Forms.Label labelProbeA;
        private System.Windows.Forms.PictureBox pictureBoxC;
        private System.Windows.Forms.PictureBox pictureBoxB;
        private System.Windows.Forms.PictureBox pictureBoxA;
        private System.Windows.Forms.PictureBox pictureBoxD;
        private System.Windows.Forms.Label labelTips;
        private System.Windows.Forms.Label labelTitle;
    }
}
