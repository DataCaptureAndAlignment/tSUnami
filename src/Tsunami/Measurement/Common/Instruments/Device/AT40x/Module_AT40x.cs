﻿using EmScon;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Xml.Serialization;
using TSU.Common.Compute;
using TSU.ENUM;
using TSU.Tools;
using TSU.Views.Message;
using static TSU.Common.Instruments.Device.AT40x.Target;
using M = TSU.Common.Measures;
using MMMM = TSU;
using P = TSU.Views.Message.ProgressMessage;
using Pr = TSU.Preferences;
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments.Device.AT40x
{

    [XmlType(TypeName = "AT40x.Module")]
    public class Module : PolarModule, IConnectable, IInitializable, IMotorized, ILiveData
    {
        public bool flagShowFailureMessage = true;



        [XmlIgnore]
        public Reflector.Reflector ActualReflector
        {
            get
            {
                if (_target != null)
                    if (InstrumentIsOn)
                    {
                        //this._target.Get();
                        return FindReflectorInstanceFromAtType(_target.selected.targetType);
                    }
                return null;
            }
        }


        [XmlIgnore]
        public LiveData LiveData { get; set; }

        [XmlIgnore]
        public override bool IsContinuousMeasurementOn
        {
            get
            {
                return LiveData.IsOn;
            }
        }


        [XmlIgnore]
        public Polar.Station StationTheodolite
        {
            get
            {
                return (((ParentModule as Manager.Module).ParentModule) as Polar.Station.Module).StationTheodolite;
            }
        }

        [XmlIgnore]
        public override bool InstrumentIsOn
        {
            get
            {
                return socket != null && socket.Connected;
            }
            set
            {


            }
        }

        [XmlIgnore]
        public override bool ConnectionInProgress { get; set; } = false;

        [XmlIgnore]
        internal new View View
        {
            get
            {
                return _TsuView as View;
            }
            set
            {
                _TsuView = value;
            }
        }

        public Instrument At40x
        {
            get
            {
                return Instrument as Instrument;
            }
            set
            {
                Instrument = value;
            }
        }
        [XmlIgnore]
        public Remote _remote;
        [XmlIgnore]
        public Target _target;
        [XmlIgnore]
        public Camera _camera;
        [XmlIgnore]
        public Motor _motor;
        [XmlIgnore]
        public Status _status;
        [XmlIgnore]
        public Face _circle;
        [XmlIgnore]
        public Ip IP;
        [XmlIgnore]
        public Measurement _measurement;
        //Variable pour ne pas remettre le measure mode à standard si l'utilisateur à sélectionné un autre mode avant de le connecter
        internal bool measModeSetByuser = false;


        public override void Goto()
        {
            base.Goto();
            _motor.Goto();
        }

        internal override void HaveFailed()
        {
            _status.isTrackerBusy = false;
            if (!LiveData.IsOn)
            {
                _status.GetTrackerStatus();
                View.UpdateSystemStatusVisual();
            }
            else
            {
                _status.isTrackerBusy = false;
            }

            base.HaveFailed();
        }

        internal override void Ready()
        {

            _status.isTrackerBusy = false;
            base.Ready();
        }
        /// <summary>
        /// normal mean the current before switching to fast/quicl measure for livedata or quick measure
        /// </summary>
        internal override void SetMeasureModeToNormal()
        {
            if (!LiveData.IsOn)
                _measurement.SetMode();
        }
        internal override void SetMeasureModeToQuick()
        {
            if (!LiveData.IsOn)
            {
                _measurement.SetModeLive();
                if (QuickMeasureOneFaceWanted) _BeingMeasuredTheodoliteData.Face = FaceType.Face1;
                if (QuickMeasureTwoFaceWanted) _BeingMeasuredTheodoliteData.Face = FaceType.DoubleFace;
            }
        }

        DateTime lastFaceCheckTime = DateTime.Now;
        public override FaceType GetFace()
        {
            if ((DateTime.Now - lastFaceCheckTime).TotalMilliseconds > 3000)
            {
                lastFaceCheckTime = DateTime.Now;
                return _circle.Get();
            }
            else
            {
                return _circle._selectedTsu;
            }
        }

        internal override bool Lock()
        {
            base.Lock();
            return _target.Catch();
        }

        public void LockUnLock()
        {
            if (_status.isTrackerLockedAndReady)
                _target.Release();
            else
                _target.Catch();
        }

        public override void CancelMeasureInProgress()
        {
            _measurement.Cancel();
        }

        public override void CheckIncomingMeasure(Polar.Measure m)
        {
            // measure?
            if (m == null)
            {
                View.AllowGoto(false);
                return;
                //throw new Exception(R.W_NoMeas);
            }

            // Known Face?
            if (m.Face == FaceType.UnknownFace) throw new Exception(R.W_NoFaceStrategy);

            // Then ok to set the measure
            toBeMeasuredData = m;

            // Known prisme?
            string prismName;
            if (m.Distance.Reflector != null && m.Distance.Reflector._Name != R.T_MANAGED_BY_AT40X)
            {
                prismName = m.Distance.Reflector._Name; // saved before it change
                if (!this._target.IsValidReflectorThenSetIt(m.Distance))
                    Logs.Log.AddEntryAsPayAttentionOf(
                            this,
                            string.Format(R.T345,
                            prismName,
                            _Name, m.Distance.Reflector._Name));

            }
            else
            {
                prismName = R.String_Unknown;
            }

            // Ready
            if (InstrumentIsOn)
                Logs.Log.AddEntryAsFYI(
                    this,
                    string.Format(
                        R.T348,
                        Instrument?._Name ?? R.T_UNKNOWN_INSTRUMENT,
                        m._Point?._Name ?? R.T_UNKNOWN_POINT,
                        m.Face,
                        m.Extension,
                        prismName
                    )
                );

            View.ShowMeasureDetails();

            // Goto availabel?
            if (m.HasCorrectedAngles)
            {
                View.AllowGoto(true);

            }
            else
                View.AllowGoto(false);
        }

        #region constructor

        public Module()
            : base() { }


        public Module(MMMM.Module parentModule, Instruments.Instrument i)
            : base(parentModule)
        {
            Instrument = i;
            if (Instrument is TotalStation ts)
            {
                TotalStation totalStationCreatedAtStartUpThatContainsEtalonnagesThatAreNotSerialized = Tsunami2.Preferences.Values.Instruments.Find(x => x._Name == Instrument._Name) as TotalStation;
                ts.EtalonnageParameterList = totalStationCreatedAtStartUpThatContainsEtalonnagesThatAreNotSerialized.EtalonnageParameterList;
            }
        }

        public override void Initialize()
        {
            this._Name = "AT40x module";
            this.Utility = R.T351;
            _remote = new Remote(this);
            LiveData = new LiveData(this);
            LiveData.HasStarted += On_LiveData_HasStarted;
            LiveData.HasStopped += On_LiveData_HasStopped;
            _target = new Target(this);
            _camera = new Camera(this);
            _motor = new Motor(this);
            _status = new Status(this);
            _circle = new Face(this);
            IP = new Ip("192.168.0.100");
            _measurement = new Measurement(this);

            async = new Async(this);
            sync = new Sync(this);
            receiver = new Receiver();

            base.Initialize();

            var colors = Tsunami2.Preferences.Theme.Colors;
        }

        private void On_LiveData_HasStopped(object sender, LiveData.LiveDataEventArgs e)
        {
            if (LiveDataStopped != null)
                LiveDataStopped(_target.selected, e);
        }

        private void On_LiveData_HasStarted(object sender, LiveData.LiveDataEventArgs e)
        {
            if (LiveDataStarted != null)
                LiveDataStarted(_target.selected, e);
        }


        #endregion

        private void StartSimulationMeasurement()
        {
            _measurement.IsSimulation = true;
            InstrumentIsOn = true;
            _target.IsSelected = true;

        }


        #region Events 

        [field: XmlIgnore]
        public event EventHandler<CompensatorEventArgs> CompensatorStateChanged;
        public event EventHandler<ReflectorEventArgs> ReflectorChanged;
        public event EventHandler LiveDataStarted;
        public event EventHandler LiveDataStopped;


        public override void ChangeFace()
        {

            this.timeSpentMeasuring.Add("ChangeFace", TimeSpent.type.Started, DateTime.Now);
            base.ChangeFace();
            _circle.Change();
            this.timeSpentMeasuring.Add("ChangeFace", TimeSpent.type.Stopped, DateTime.Now);
        }

        internal override void ChangeVerticalisationState(Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation verticalisationState)
        {
            if (InstrumentIsOn)
            {
                switch (verticalisationState)
                {
                    case Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation.NotVerticalised:
                        TurnOffCompensator();
                        break;
                    case Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation.Unknown:
                    case Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation.Verticalised:
                    default:
                        TurnOnCompensator();
                        break;
                }
            }
        }

        public void CompensatorChanged(Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation instrumentVerticalisation)
        {
            CompensatorStateChanged?.Invoke(_target.selected.name, new CompensatorEventArgs(instrumentVerticalisation));
        }

        public void ReflectorChange()
        {
            if (ToBeMeasuredData != null)
            {
                // we have to check if we really need to update, maybe s it a fake reflector such as ccr to correspond to RRR for AT40x
                TSU_TargetType refletorType = _target.GetTsuType((ToBeMeasuredData as Polar.Measure).Distance.Reflector);
                TSU_TargetType correspondingtype = _target.SwithToRightKnownReflector(refletorType);
                // need to change only if it is different
                if (_target.selected.targetType != correspondingtype)
                {
                    (ToBeMeasuredData as Polar.Measure).Distance.Reflector =
                           FindReflectorInstanceFromAtType(_target.selected.targetType);
                }
            }
            ReflectorChanged?.Invoke(_target.selected, new ReflectorEventArgs(_target.InstrumentType));
        }



        public void SubscribeAt40xEvents(bool unsubscribe = false)
        {
            if (receiver == null)
                return;

            if (true) // un subscribe first !! to avoid to be subscripbed twice.
            {
                receiver.ErrorEvent -= this.OnError;
                receiver.SystemStatusChangeEvent -= this.OnStatusEvent;

                receiver.GetReflectorsAnswerEvent -= _target.OnGetReflectorsAnswer;
                receiver.GetProbesAnswerEvent -= _target.OnGetProbesAnswer;
                receiver.GetReflectorAnswerEvent -= _target.OnGetReflectorAnswer;
                receiver.GetProbeAnswerEvent -= _target.OnGetProbeAnswer;

                receiver.SingleMeasurementAnswerEvent -= _measurement.SingleMeasurementAnswer;
                receiver.StationaryProbeMeasurementAnswerEvent -= _measurement.StationaryProbeMeasurementAnswer;

                receiver.MoveHVAnswerEvent -= _motor.OnMoveHVAnswer;

                Debug.WriteInConsole("Remove OnReflectorPosAnswer from ReflectorPosAnswerEvent");
                receiver.ReflectorPosAnswerEvent -= LiveData.OnReflectorPosAnswer;

                sync.UnsubscribeEvents();
            }

            if (!unsubscribe) // if 'subscribre'
            {
                receiver.ErrorEvent += this.OnError;
                receiver.SystemStatusChangeEvent += this.OnStatusEvent;

                receiver.GetReflectorsAnswerEvent += _target.OnGetReflectorsAnswer;
                receiver.GetProbesAnswerEvent += _target.OnGetProbesAnswer;
                receiver.GetReflectorAnswerEvent += _target.OnGetReflectorAnswer;
                receiver.GetProbeAnswerEvent += _target.OnGetProbeAnswer;

                receiver.SingleMeasurementAnswerEvent += _measurement.SingleMeasurementAnswer;
                receiver.StationaryProbeMeasurementAnswerEvent += _measurement.StationaryProbeMeasurementAnswer;

                receiver.MoveHVAnswerEvent += _motor.OnMoveHVAnswer;

                Debug.WriteInConsole("Add OnReflectorPosAnswer to ReflectorPosAnswerEvent");
                receiver.ReflectorPosAnswerEvent += LiveData.OnReflectorPosAnswer;

                sync.SubscribeEvents();
            }
        }

        internal void OnStatusEvent(object sender, Receiver.SystemStatusChangeEventArgs args)
        {
            ES_SystemStatusChange changeEvent = args.systemStatusChange;
            Debug.WriteInConsole($"OnStatusEvent, changeEvent={changeEvent}");
            switch (changeEvent) // i have commented the real LT events
            {
                // BUTTON 1 or A
                case ES_SystemStatusChange.ES_SSC_ProbeButton1Down:
                    _remote.On_ProbeButton(changeEvent);
                    Logs.Log.AddEntryAsPayAttentionOf(this, "A Down"); break;
                case ES_SystemStatusChange.ES_SSC_ProbeButton1Up:
                    _remote.On_ProbeButton(changeEvent);
                    Logs.Log.AddEntryAsPayAttentionOf(this, "A up"); break;
                case ES_SystemStatusChange.ES_SSC_ProbeButton1DoubleClick:
                    Logs.Log.AddEntryAsPayAttentionOf(this, "A doubled");
                    _remote.On_ProbeButton(changeEvent);
                    break;

                // BUTTON 2 or C
                case ES_SystemStatusChange.ES_SSC_ProbeButton2Down:
                    _remote.On_ProbeButton(changeEvent);
                    Logs.Log.AddEntryAsPayAttentionOf(this, "C Down"); break;
                case ES_SystemStatusChange.ES_SSC_ProbeButton2Up:
                    _remote.On_ProbeButton(changeEvent);
                    Logs.Log.AddEntryAsPayAttentionOf(this, "C up"); break;
                case ES_SystemStatusChange.ES_SSC_ProbeButton2DoubleClick:
                    Logs.Log.AddEntryAsPayAttentionOf(this, "C doubled");
                    _remote.On_ProbeButton(changeEvent);
                    break;

                // BUTTON 3 or B
                case ES_SystemStatusChange.ES_SSC_ProbeButton3Down:
                    _remote.On_ProbeButton(changeEvent);
                    Logs.Log.AddEntryAsPayAttentionOf(this, "B Down"); break;
                case ES_SystemStatusChange.ES_SSC_ProbeButton3Up:
                    _remote.On_ProbeButton(changeEvent);
                    Logs.Log.AddEntryAsPayAttentionOf(this, "B up"); break;
                case ES_SystemStatusChange.ES_SSC_ProbeButton3DoubleClick:
                    _remote.On_ProbeButton(changeEvent);
                    Logs.Log.AddEntryAsPayAttentionOf(this, "B doubled"); break;

                // BUTTON 4 or D
                case ES_SystemStatusChange.ES_SSC_ProbeButton4Down:
                    _remote.On_ProbeButton(changeEvent);
                    Logs.Log.AddEntryAsPayAttentionOf(this, "D Down"); break;
                case ES_SystemStatusChange.ES_SSC_ProbeButton4Up:
                    _remote.On_ProbeButton(changeEvent);
                    Logs.Log.AddEntryAsPayAttentionOf(this, "D up"); break;
                case ES_SystemStatusChange.ES_SSC_ProbeButton4DoubleClick:
                    _remote.On_ProbeButton(changeEvent);
                    Logs.Log.AddEntryAsPayAttentionOf(this, "D doubled"); break;

                // PROBE changed
                case ES_SystemStatusChange.ES_SSC_ProbeChanged:
                    View.On_ES_SSC_ProbeChanged();
                    break;

                case ES_SystemStatusChange.ES_SSC_MeasStatus_Ready:
                    if (!LiveData.IsOn)
                        Logs.Log.AddEntryAsFYI(this, $"Event Status : {ES_SystemStatusChange.ES_SSC_MeasStatus_Ready}");
                    LiveData.OnMeasStatusReady();
                    break;

                default:
                    if (!LiveData.IsOn)
                        Logs.Log.AddEntryAsFYI(this, $"Event Status not treated: {changeEvent}");
                    break;
            }
            View.UpdateSystemStatusVisual();
        }

        DsaOptions flagAutomaticalyRemeasure3TimesOnErrors = DsaOptions.Ask_to;
        int numberOfError = 0;

        private void OnError(object sender, Receiver.CommandAnswerEventArgs err)
        {
            ES_Command command = err.command;
            ES_ResultStatus status = err.status;

            string readableErrorMessage;
            if (int.TryParse(status.ToString(), out int i))
                readableErrorMessage = Errors.GetErrorTextFromNumber(i);
            else
                readableErrorMessage = status.ToString();

            if (command == ES_Command.ES_C_Unknown)
            {
                Logs.Log.AddEntryAsPayAttentionOf(this, $"Error received without source command, status:{status}");
            }
            else if (sync.SyncCallInProgress == command)
            {
                //If the error is from the sync, it will be treated as an exception, we just log it here
                Logs.Log.AddEntryAsError(this, $"Sync error received: {readableErrorMessage} (command:{command})");
                return;
            }
            else
            {
                async.EndAsyncCall(command);
                Logs.Log.AddEntryAsPayAttentionOf(this, $"Async error received, command:{command} status, {status}  ");
            }

            _measurement.timerToCheckIfMeasOrErrorIsReceived.Tag = null;
            _measurement.timerToCheckIfMeasOrErrorIsReceived.Stop();

            if (State.Value == InstrumentState.type.Measuring)
                State = new InstrumentState(InstrumentState.type.Idle);



            // Live Data ???
            if (LiveData.IsOn)
            {
                if (i == 0) return; //dont care is busy
                                    //  Logs.Log.AddEntryAsError(this, $"LiveData received a message of error from the instrument :{message}");
                Logs.Log.AddEntryAsError(this, $"{R.T_LIVEDATA_RECEIVED_A_MESSAGE_OF_ERROR_FROM_THE_INSTRUMENT} {readableErrorMessage}");
                return;
            }

            // Warmed ???
            if (status.ToString() == "ES_RS_WarmedUpStateReached") //ES_RS_WarmedUpStateReached = 10018,
            {
                string titleAndMessage = string.Format("{0};{1}\r\n{2}",
                    R.T_INSTRUMENT_IS_WARM,
                    R.T_A_MESSAGE_FROM_THE_AT40X_HAS_BEEN_RECEIVED_SAYING_THAT_IT_REACHES_THE_OPTIMAL_TEMPERATURE,
                    R.T_FOR_OPTIMAL_RESULTS_YOU_MAY_CONSIDER_TO_INITIALIZE_THE_INSTRUMENT_AND_START_YOU_STATION_FROM_SCRATCH);
                new MessageInput(MessageType.Warning, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_UNDERSTOOD }
                }.Show();
                View.ShowWarmed();
                LastMessage = R.T_INSTRUMENT_IS_WARM;
                return;
            }

            // More explicit message for the one taht happened often
            // if (i == 111) this.LastMessage = $"Tracker not initialized;{message}";
            //  if (i == 113316) this.LastMessage = $"Distance measurement failed;{message}";
            //  if (i == 100003) this.LastMessage = $"Measurement happened to quickily after the locking;{command}:\r\n{message} ";

            LastMessage = readableErrorMessage;
            if (i == 111) LastMessage = $"{R.T_TRACKER_NOT_INITIALIZED};{readableErrorMessage}";
            if (i == 113316) LastMessage = $"{R.T_DISTANCE_MEASUREMENT_FAILED};{readableErrorMessage}";
            if (i == 100003) LastMessage = $"{readableErrorMessage} for {command};{R.T_MEASUREMENT_HAPPENED_TO_QUICKLY_AFTER_THE_LOCKING} ";

            // report error
            JustMeasuredData = _BeingMeasuredTheodoliteData;
            numberOfError++;

            StatusChange(LastMessage, P.StepTypes.EndAll);
            if (command == ES_Command.ES_C_StartMeasurement && !Debug.IsRunningInATest)
            {
                AskIfRemeasure();
            }
            else
                HaveFailed();
        }

        private void AskIfRemeasure()
        {
            if (numberOfError < 3)
            {
                HaveFailed();
                View.PossibleActions(LastMessage, ref flagAutomaticalyRemeasure3TimesOnErrors,
                    R.T_RETRY, () =>
                    {
                        GotoWanted = false;
                        ReMeasure();
                    },
                    MessageType.Warning, neverIsAllowed: false, dontName: R.T_CANCEL, dontAction: null);
            }
            else
            {
                numberOfError = 0;
                string titleAndMessage = $"{LastMessage}\r\n{R.T_TRIED_THREE_TIME}";
                new MessageInput(MessageType.Critical, titleAndMessage).Show();
                HaveFailed();
            }
        }


        #endregion

        #region Connection
        [XmlIgnore]
        internal Socket socket;

        [XmlIgnore]
        internal ReceiverThread receiverThread;

        [XmlIgnore]
        internal Receiver receiver;

        [XmlIgnore]
        public Async async;

        [XmlIgnore]
        public Sync sync;

        private void SocketDisconnect()
        {
            try
            {
                socket?.Disconnect(false);
            }
            catch (Exception)
            {
                //Ignore the exceptions
            }

            try
            {
                socket?.Dispose();
            }
            catch (Exception)
            {
                //Ignore the exceptions
            }

            socket = null;

            try
            {
                receiverThread?.Abort();
            }
            catch (Exception)
            {
                //Ignore the exceptions
            }

            receiverThread = null;
        }

        private IPEndPoint remoteEP;

        private void SocketConnect(string sAddr, int port = 700)
        {
            Debug.WriteInConsole($"Module {Id} is connecting to {sAddr}:{port}");

            // Check the parameters are valid
            IPAddress ipAdd = IPAddress.Parse(sAddr);
            remoteEP = new IPEndPoint(ipAdd, port);

            // Create socket
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(remoteEP);
        }

        internal void SocketReConnect()
        {
            try
            {
                socket?.Disconnect(false);
            }
            catch (Exception)
            {
                //Ignore the exceptions
            }

            try
            {
                socket?.Dispose();
            }
            catch (Exception)
            {
                //Ignore the exceptions
            }

            socket = null;

            // ReCreate socket
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // Reconnect
            socket.Connect(remoteEP);
        }

        public override Result Connect()
        {
            Result result = new Result();

            Logs.Log.AddEntryAsBeginningOf(this, R.T_TRY_TO_CONNECT);

            //Connection au ESCOM
            try
            {
                State = new InstrumentState(InstrumentState.type.Connecting);
                Ip.GetFirstKnownThatReplyToPing(View, At40x._SerialNumber, out IP);

                ConnectAdapter(IP);
                Logs.Log.AddEntryAsSuccess(this, R.T_CONNECTED_TO_ + IP.ToString());

                _measurement.SetParameters();

                if (!GetInitializationStatus()) Logs.Log.AddEntryAsPayAttentionOf(this, R.T_NOT_INITIALIZED);

                WakeUpAdapter();

                result.Success = true;
                State = new InstrumentState(InstrumentState.type.Idle);
            }
            catch (Exception e)
            {
                // test if error is "already connected
                //   if (e.Message == "Object already connected to server\n")
                if (e.Message == $"{R.T_OBJECT_ALREADY_CONNECTED_TO_SERVER}\n")
                {
                    result.Success = true;
                    result.AccessToken = e.Message;
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = R.T_IMPOSSIBLE_TO_CONNECT + e.Message;
                }
            }
            //Get Status
            if (result.Success)
            {

                try
                {
                    //sync.ChangeFace();
                    result.AccessToken += ", " + ConnectionResult();
                }
                catch (Exception e)
                {
                    result.Success = false;
                    result.ErrorMessage = R.T_IMPOSSIBLE_TO_CONNECT + e.Message;

                }
            }
            Logs.Log.AddEntryAsResult(this, result);
            //Return
            return result;
        }

        internal void WakeUpAdapter()
        {
            try
            {
                if (Debugger.IsAttached)
                {
                    Ready();
                    return;
                }
                // Wake up
                double h = 0;
                double v = 0;
                _status.isTrackerLockedAndReady = false;
                _status.isTrackerBusy = true;
                sync.GetDirection(out h, out v);
                if (v > 180 && v < 220)
                {
                    Debug.WriteInConsole("at40x-SYNC-PointLaserHVD");
                    sync.PointLaserHVD(h, 100, 1);

                    Debug.WriteInConsole("at40x-SYNC-GetDirection");
                    sync.GetDirection(out h, out v);
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                _status.isTrackerBusy = false;
            }
        }
        public void ConnectAdapter(Ip ip)
        {
            try
            {
                //Connect to adapter
                State = new InstrumentState(InstrumentState.type.Connecting);
                SocketConnect(ip.ToString());
                State = new InstrumentState(InstrumentState.type.Idle);

                Pr.Preferences.Instance.AddIpToListOfAt401IpAdresses(At40x._SerialNumber, ip.ToString());

                //Start listening
                receiverThread = new ReceiverThread(this);
                receiverThread.Start();
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(string.Format("{0};{1}: {2}", R.T_CONNECTION_FAILED, ex.Message, R.T_YOU_SHOULD_TRY_TO_RESET), ex);
            }
        }

        public void ResetConnection(bool thenTurnOn = true)
        {
            SocketDisconnect();
            Logs.Log.AddEntryAsSuccess(this, R.T_CONNECTION_RESET);
            InstrumentIsOn = false;
            this._camera.isOn = false;
            this.View.cameraView.CameraStop();
            View.actionList.Clear();
            if (thenTurnOn)
                View.TurnOn();
        }

        private string ConnectionResult()
        {
            string s = $"{R.T_CONNECTED_TO_INSTRUMENT} # {_status.lTrackerSerialNumber}" +
                $" {R.T_WITH_ES_VERSION}:  {_status.esVersionNumber.iMajorVersionNumber}" +
                $".{_status.esVersionNumber.iMinorVersionNumber}" +
                $".{_status.esVersionNumber.iBuildNumber}";

            if (_status.lTrackerSerialNumber != 0)
                At40x._SerialNumber = _status.lTrackerSerialNumber.ToString();
            SendMessage(this);

            return s;
        }

        public override Result Disconnect()
        {
            Result result = new Result();
            try
            {
                if (CheckConnection())
                {
                    double h, v;
                    StatusChange($"{R.T_ASKING_VERTICAL_POSITION}...", P.StepTypes.Begin);

                    Debug.WriteInConsole("at40x-SYNC-GetDirection");
                    sync.GetDirection(out h, out v);
                    StatusChange($"...{R.T_RECIEVED}", P.StepTypes.End);

                    StatusChange($"{R.T_GOING_TO_BED}...", P.StepTypes.Begin);
                    if (!(Debugger.IsAttached))
                    {
                        Debug.WriteInConsole("at40x-SYNC-PointLaserHVD");
                        sync.PointLaserHVD(h, 207, 1); // to have the led lghting the buble for the next station
                    }
                    StatusChange($"...{R.T_WENT}", P.StepTypes.End);

                    StatusChange($"{R.T_DISCONNECTING}...", P.StepTypes.Begin);
                    SocketDisconnect();
                    StatusChange($"...{R.T_DISCONNECTED}", P.StepTypes.End);

                    result.Success = true;
                    result.AccessToken = R.T_DISCONNECTED;
                    Logs.Log.AddEntryAsResult(this, result);
                    State = new InstrumentState(InstrumentState.type.Disconnected);
                    StatusChange($"...{R.T_DISCONNECTED}", P.StepTypes.EndAll);
                    _TsuView.UpdateView();
                }
                else
                {
                    StatusChange($"...{R.T_WAS_ALREADY_STOPPED}", P.StepTypes.EndAll);
                    View.ShowStatusAsOff();
                    InstrumentIsOn = false;
                }
            }
            catch (Exception e)
            {
                StatusChange($"...{R.T_STOPPED_DURING_THE_PROCESS}", P.StepTypes.EndAll);
                result.Success = false;
                result.ErrorMessage = $"{R.T_IMPOSSIBLE_TO_DISCONNECT}: " + e.Message;
                Logs.Log.AddEntryAsError(this, $"{R.T_IMPOSSIBLE_TO_DISCONNECT}: " + e.Message);
            }
            return result;
        }


        internal void CheckWhatToMeasure()
        {
            if (toBeMeasuredData != null) return;

            if (LiveData.IsOn)
                _BeingMeasuredTheodoliteData = new Polar.Measure() { Face = FaceType.DoubleFace };
            else
                WaitMeasurementDetails();
        }


        /// <summary>
        /// check conection by actually request something
        /// </summary>
        /// <returns></returns>
        public bool CheckConnection()
        {
            if (socket != null && socket.Connected)
            {
                try
                {

                    Debug.WriteInConsole("at40x-SYNC-GetSystemSoftwareVersion");
                    sync.GetSystemSoftwareVersion(out string dummy);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
                return false;
        }

        #endregion

        #region Initisalisation
        public override Result InitializeSensor()
        {
            Result result = new Result();
            try
            {
                result = _status.GetEnvironmentParameters();

                Debug.WriteInConsole("at40x-SYNC-SetSearchParams");
                sync.SetSearchParams(0.04, 10000);
                InitializeByLeica_Sync();

                result.Success = true;
                result.AccessToken = R.T_INITIALIZED;
            }
            catch (Exception ex)
            {
                result.Success = false;
                string message = $"{R.T_IMPOSSIBLE_TO_INITIALIZE}: " + Errors.TryTogetMessageFromErrorCode(ex.Message);
                result.ErrorMessage = message;
                throw new Exception(message, ex);
            }
            return result;
        }
        public void InitializeByLeica_Sync()
        {
            try
            {
                _status.isTrackerBusy = true;
                StatusChange($"{R.T_AT40X_IS_DOING_HIS_THING}...", P.StepTypes.Begin);
                Logs.Log.AddEntryAsBeginningOf(this, $"{R.T_INITIALISATION_OF_THE_INSTRUMENT}...");

                Debug.WriteInConsole("at40x-SYNC-Initialize");
                sync.Initialize();
                _status.isTrackerBusy = false;
                Logs.Log.AddEntryAsFinishOf(this, $"...{R.T_INITIALIZED}.");
                UpdateView();
                StatusChange($"..{R.T_DONE}", P.StepTypes.End);
            }
            catch (Exception e)
            {
                string message = $"{R.T_IMPOSSIBLE_TO_INITIALIZE}: " + Errors.TryTogetMessageFromErrorCode(e.Message);
                StatusChange($"...{R.T_ERROR}", P.StepTypes.End);
                _status.isTrackerBusy = false;
                throw new Exception(message, e);
            }
        }

        public void InitializeByLeica_Async()
        {
            try
            {
                StatusChange($"{R.T_AT40X_IS_DOING_HIS_THING}...", P.StepTypes.Begin);
                Logs.Log.AddEntryAsBeginningOf(this, $"{R.T_INITIALISATION_OF_THE_INSTRUMENT}...");

                Debug.WriteInConsole("at40x-ASYNC-Initialize");
                async.Initialize();

            }
            catch (Exception ex)
            {
                string message = $"{R.T_IMPOSSIBLE_TO_INITIALIZE}: " + Errors.TryTogetMessageFromErrorCode(ex.Message);
                StatusChange($"...{R.T_ERROR}", P.StepTypes.End);
                throw new Exception(message, ex);
            }
        }

        private bool isInitialized;

        public bool IsInitialized
        {
            get
            {
                return isInitialized;
            }
        }


        public bool GetInitializationStatus()
        {
            if (InstrumentIsOn)
            {
                isInitialized = _status.GetMeasurementStatusInfo(ES_MeasurementStatusInfo.ES_MSI_Initialized);
                return isInitialized;
            }
            else
            {
                isInitialized = false;
                return false;
            }
        }


        #endregion

        #region Remote

        #endregion



        #region Camera


        #endregion



        #region Status


        public override void TurnOnCompensator()
        {
            _status.TurnCompensator(true);
        }


        public override void TurnOffCompensator()
        {
            _status.TurnCompensator(false);
        }


        public class Status
        {
            readonly Module At40xModule;

            public Status(Module at40xModule)
            {
                At40xModule = at40xModule;
                tracker = new ES_TrackerStatus();
            }

            [XmlIgnore]
            public int batteryLevelSensor;

            [XmlIgnore]
            public int batteryLevelController;

            [XmlIgnore]
            public int inclinationSensorState;

            [XmlIgnore]
            public ES_TrackerStatus tracker;

            [XmlIgnore]
            public ES_TrackerStatus trackerLocked;

            [XmlIgnore]
            public ES_ResultStatus lastResultStatus;

            [XmlIgnore]
            public ES_TrackerProcessorStatus trackerProcessorStatus;

            [XmlIgnore]
            public ES_LaserProcessorStatus laserStatus;

            [XmlIgnore]
            public ES_ADMStatus admStatus;

            [XmlIgnore]
            public CESCSAPIReceive.ESVersionNumberT esVersionNumber;

            [XmlIgnore]
            public ES_WeatherMonitorStatus weatherMonitorStatus;

            [XmlIgnore]
            public int lFlagsValue;

            [XmlIgnore]
            public int lTrackerSerialNumber;

            // allow to avoid ES-tracekrStatus type which annoy the debugging
            [XmlIgnore]
            public bool isTrackerBusy
            {
                get
                {
                    return tracker == ES_TrackerStatus.ES_TS_Busy;
                }
                set
                {
                    if (value == true)
                    {
                        tracker = ES_TrackerStatus.ES_TS_Busy;

                        At40xModule.View.pMeasTsu.Image = R.At40x_Red;
                    }
                    else
                    {
                        tracker = ES_TrackerStatus.ES_TS_Ready;
                        At40xModule.View.pMeasTsu.Image = R.At40x_Green;

                    }
                }
            }

            [XmlIgnore]
            public bool isTrackerLockedAndReady

            {
                get
                {
                    return trackerLocked == ES_TrackerStatus.ES_TS_Ready;
                }
                set
                {
                    if (value == false)
                    {
                        trackerLocked = ES_TrackerStatus.ES_TS_NotReady;

                        if (At40xModule._target.TryingToLock)
                        {
                            At40xModule.View.pMeasLock.Image = R.At40x_Blue;
                            //At40xModule.View.buttonLocking.BackgroundImage = R.At40x_Lock_trying;
                        }
                        else
                        {
                            At40xModule.View.pMeasLock.Image = R.At40x_Red;
                            //At40xModule.View.buttonLocking.BackgroundImage = R.At40x_Release;
                        }

                    }
                    else
                    {
                        trackerLocked = ES_TrackerStatus.ES_TS_Ready;
                        At40xModule.View.pMeasLock.Image = R.At40x_Green;
                        //At40xModule.View.buttonLocking.BackgroundImage = R.At40x_Lock;
                    }
                }
            }


            // allow to avoid ES-tracekrStatus type which annoy the debugging
            [XmlIgnore]
            public bool isMeasuring { get; set; }

            public double temperature;
            public double humidity;
            public double pressure;
            public bool IsWaiting;

            internal Result GetEnvironmentParameters()
            {
                Logs.Log.AddEntryAsBeginningOf(At40xModule, $"{R.T_SETTING_UP_PARAMETERS}");
                Result result = new Result();
                try
                {
                    double temp, press, hum;

                    Debug.WriteInConsole("at40x-SYNC-GetEnvironmentParams");
                    At40xModule.sync.GetEnvironmentParams(out temp, out press, out hum);
                    result.Success = true;
                    result.AccessToken =
                        $"{R.T_TEMPERATURE_IS} {temp} °C   " +
                        $"{R.T_PRESSURE_IS} {press} Hpa   " +
                        $"{R.T_HUMIDITY_IS} {hum} %";
                }
                catch (Exception e)
                {
                    result.Success = false;
                    result.ErrorMessage = $"{R.T_IMPOSSIBLE_TO_GET_ENVIRONMENT_PARAMETERS}: {e.Message}";
                }

                Logs.Log.AddEntryAsResult(At40xModule, result);
                return result;
            }  //Temp, press, hum

            internal void GetEnvironmentParameters(out double temp, out double press, out double hum)
            {
                try
                {

                    Debug.WriteInConsole("at40x-SYNC-GetEnvironmentParams");
                    At40xModule.sync.GetEnvironmentParams(out temp, out press, out hum);
                    temperature = temp;
                    pressure = press;
                    humidity = hum;
                }
                catch (Exception ex)
                {
                    temp = 0;
                    press = 0;
                    hum = 0;
                    throw new Exception($"{R.T_IMPOSSIBLE_TO_GET_ENVIRONMENT_PARAMETERS}: {ex.Message}", ex);
                }
            }  //Temp, press, hum
            internal void GetSystemStatus()  // result, Processor, Laser, ADM, Version, WeatherMonitor, SerialNumber
            {
                try
                {
                    Debug.WriteInConsole($"?????? Status asked at {DateTime.Now}");

                    if (At40xModule.InstrumentIsOn && !At40xModule.LiveData.IsOn)
                    {
                        Debug.WriteInConsole("at40x-SYNC-GetSystemStatus");

                        At40xModule.sync.GetSystemStatus(
                            out lastResultStatus,
                            out trackerProcessorStatus,
                            out laserStatus,
                            out admStatus,
                            out esVersionNumber,
                            out weatherMonitorStatus,
                            out lFlagsValue,
                            out lTrackerSerialNumber);
                    }
                    Debug.WriteInConsole($"!!!!!!!Status received at {DateTime.Now}");
                }
                catch (Exception ex)
                {
                    throw new Exception($"{R.T_GET_SYSTEM_STATUS_FAILED}; " + ex.Message, ex);
                }
            }

            public void GetTrackerStatus()
            {
                Debug.WriteInConsole("at40x-SYNC-GetTrackerStatus\r\n" + new StackTrace().ToString());
                At40xModule.sync.GetTrackerStatus(out tracker); // ready, busy, not ready
                switch (tracker)
                {
                    case ES_TrackerStatus.ES_TS_NotReady:
                        isTrackerLockedAndReady = false;
                        break;
                    case ES_TrackerStatus.ES_TS_Busy:
                        isTrackerBusy = true;
                        isTrackerLockedAndReady = false;
                        break;
                    case ES_TrackerStatus.ES_TS_Ready:
                        isTrackerBusy = false;
                        isTrackerLockedAndReady = true;
                        break;
                    case ES_TrackerStatus.ES_TS_6DStatusInvalid:
                        break;
                    default:
                        break;
                }
            }

            public int NivelTolerance;
            public string NivelValues;
            /// <summary>
            /// return a string with the tilt in microRad aroung the X and Y axis of the instrument local cartesian system + outOfTol = 0  for controller showing 0-3 outoftol = 11 for 3-6, outoftol = 2for >6
            /// </summary>
            /// <param name="tolerance"></param>
            /// <param name="outOfTol"></param>
            /// <returns></returns>
            internal string GetNivelMeasurement(out int outOfTol)
            {
                if (inclinationSensorState != 0)
                {
                    ES_NivelStatus status;
                    double xTilt, yTilt, temperature;
                    Debug.WriteInConsole("at40x-SYNC-StartNivelMeasurement");
                    At40xModule.sync.StartNivelMeasurement(out status, out xTilt, out yTilt, out temperature); // mrad and Celcius
                    xTilt = (xTilt * 1000);
                    yTilt = (yTilt * 1000);
                    outOfTol = 0;
                    if (xTilt > 0.2) outOfTol = 1;
                    if (yTilt > 0.2) outOfTol = 1;
                    if (xTilt > 0.4) outOfTol = 2;
                    if (yTilt > 0.4) outOfTol = 2;
                    NivelTolerance = outOfTol;
                    NivelValues = xTilt.ToString("0.0") + "_" + yTilt.ToString("0.0");
                    return NivelValues;
                }
                else
                { // compensator off, no level measure posible
                    outOfTol = 0;
                    Logs.Log.AddEntryAsPayAttentionOf(this.At40xModule, "Reminder: Compensator is Off");
                    return "N/A";
                }
            }
            internal Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation GetInclinationSensorState()
            {

                Debug.WriteInConsole("at40x-SYNC-GetLongSystemParameter in GetInclinationSensorState");
                At40xModule.sync.GetLongSystemParameter(ES_SystemParameter.ES_SP_InclinationSensorState, out inclinationSensorState);
                switch (inclinationSensorState)
                {
                    case 0: return Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation.NotVerticalised;
                    case 2: return Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation.Verticalised;
                    default:
                        return Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation.Unknown;
                }
            }
            internal bool GetMeasurementStatusInfo(ES_MeasurementStatusInfo es_msi)
            {
                int status;

                Debug.WriteInConsole("at40x-SYNC-GetMeasurementStatusInfo");
                At40xModule.sync.GetMeasurementStatusInfo(out status);
                if ((status & (int)es_msi) == (int)es_msi)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            public void GetBatteryLevel()
            {
                try
                {
                    At40xModule.sync.GetLongSystemParameter(ES_SystemParameter.ES_SP_SensorBatteryStatus, out batteryLevelSensor);
                    At40xModule.sync.GetLongSystemParameter(ES_SystemParameter.ES_SP_ControllerBatteryStatus, out batteryLevelController);
                }
                catch (Exception)
                {
                    Logs.Log.AddEntryAsPayAttentionOf(At40xModule, "Battery state changed but level could not be retreived");
                }
            }
            public bool IsSensorBatteryMounted()
            {
                if (GetMeasurementStatusInfo(ES_MeasurementStatusInfo.ES_MSI_SensorBatteryMounted))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public bool IsNivelInWorkingRange;
            internal void CheckNivelInWorkingRange()
            {
                if (GetMeasurementStatusInfo(ES_MeasurementStatusInfo.ES_MSI_NivelInWorkingRange))
                {
                    IsNivelInWorkingRange = true;
                }
                else
                {
                    IsNivelInWorkingRange = false;
                }
            }



            internal void TurnCompensator(bool turnItON)
            {
                // Now remove because it is set from the station and everythime the instrumetn cahnge from one staion to another
                //if (this.At40xModule.StationTheodolite.MeasuresTaken.Count > 0)
                //    throw new Exception(R.T_COMP_TOOLATE);
                if (!At40xModule.InstrumentIsOn) return;

                int state;
                if (turnItON)
                {
                    state = 2;
                }
                else
                {
                    state = 0;
                }

                Debug.WriteInConsole("at40x-SYNC-SetLongSystemParameter");
                this.At40xModule.sync.SetLongSystemParameter(ES_SystemParameter.ES_SP_InclinationSensorState, state);

                At40xModule.CompensatorChanged(GetInclinationSensorState());
            }
        }

        #endregion

        #region Face
        public class Face
        {
            readonly Module At40xModule;

            public Face(Module at40x)
            {
                At40xModule = at40x;
                _selected = ES_TrackerFace.ES_TF_Face1;
            }

            [XmlIgnore]
            public ES_TrackerFace _selected;

            public FaceType _selectedTsu;

            internal void Change()
            {
                try
                {
                    At40xModule.Change();

                    Debug.WriteInConsole("at40x-SYNC-ChangeFace");
                    At40xModule.sync.ChangeFace();
                    switch (_selectedTsu)
                    {
                        case FaceType.Face1:
                            _selectedTsu = FaceType.Face2;
                            break;
                        case FaceType.Face2:
                            _selectedTsu = FaceType.Face1;
                            break;
                        default:
                            _selectedTsu = Get();
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Logs.Log.AddEntry(this.At40xModule, ex);
                }

            }
            internal FaceType Get()
            {
                ES_TrackerFace face;
                Debug.WriteInConsole("at40x-SYNC-GetFace");
                At40xModule.sync.GetFace(out face);
                switch (face)
                {
                    case ES_TrackerFace.ES_TF_Face1:
                        _selectedTsu = FaceType.Face1;
                        return FaceType.Face1;
                    case ES_TrackerFace.ES_TF_Face2:
                        _selectedTsu = FaceType.Face2;
                        return FaceType.Face2;
                    default: return FaceType.UnknownFace;
                }
            }
        }

        #endregion

        #region Ip


        #endregion

        #region Measurement

        public override bool IsReadyToMeasure()
        {
            if (!InstrumentIsOn) throw new Exception(R.T_INSTRUMENT_IS_OFF);
            if (!IsInitialized) throw new Exception(R.T_INSTRUMENT_NOT_INITIALZED);
            if (!_target.IsSelected) throw new Exception(R.T_YOU_SHOULD_SELECT_A_REFLECTOR);

            if (BeingMeasured != null)
            {
                if (BeingMeasured._Status is M.States.Cancel)
                    BeingMeasured = null;
            }

            if (BeingMeasured == null)
            {
                if (ToBeMeasuredData == null)
                {
                    if (LiveData.IsOn)
                    {
                        ToBeMeasuredData = new Polar.Measure() { Face = FaceType.Face1 };

                    }
                    else
                        WaitMeasurementDetails();
                }
                else
                {
                    if (ToBeMeasuredData._Point == null)
                        WaitMeasurementDetails();
                }

                if (ToBeMeasuredData == null)
                    throw new Exception("Nothing to measure");

                // check for managed by AT40x (happen when point created before At connection)
                var distanceMeasurement = (ToBeMeasuredData as Polar.Measure).Distance;

                if (distanceMeasurement.Reflector._Name == R.T_MANAGED_BY_AT40X)
                    distanceMeasurement.Reflector = ActualReflector;

                if (!_target.IsValidReflector(distanceMeasurement.Reflector, out _))
                    throw new Exception(R.T_REFLECTOR_SELECTED_IN_THE_NEXT_POINT_SECTION_IS_UNKNOWN);
            }

            return true;
        }

        public override void PrepareMeasure()
        {
            base.PrepareMeasure();
            WaitingForMeasure = true;
            _BeingMeasuredTheodoliteData.ModeOfMeasure = _measurement.mode.ToString();
        }

        internal override void OnMeasureToDoReceived()
        {
            if (ToBeMeasuredData != null)
            {
                InvokeOnApplicationDispatcher(View.ShowMeasButton);
                if ((ToBeMeasuredData as Polar.Measure).Distance.Reflector == null)
                {
                    (ToBeMeasuredData as Polar.Measure).Distance.Reflector = ActualReflector;
                    Logs.Log.AddEntryAsSuccess(this, $"{R.T_REFLECTOR_SET_TO} {ActualReflector._Name}");
                }
                base.OnMeasureToDoReceived();

                // if (this.LiveData.IsOn) // removed because if not, then a measure in conttrol seected during livedata will be treated a not a cntrol
                //     _ToBeMeasuredData._Status = new MeasureStateUnknown();

                if (ToBeMeasuredData._Status is M.States.Good || _ToBeMeasureTheodoliteData.DirectMeasurementWanted)// this is the way to say 'i want  a measure now' without clicking gotoall button 
                {
                    _ToBeMeasureTheodoliteData.DirectMeasurementWanted = false; // to make sure this behaviour is not reproduce with a remeasurement for example
                    // this.GotoWanted = true; this should be decided previously because sometime we dont want goto
                    View.LaunchMeasure();
                }
            }
            if (ToBeMeasuredData == null && !allowAlwaysButtonMeas == true)
            {
                InvokeOnApplicationDispatcher(View.HideMeasButton);
            }
            else
            {
                InvokeOnApplicationDispatcher(View.ShowMeasButton);
                //CheckIncomingMeasure(this._ToBeMeasuredData as MeasureTheodolite); already di done in the tobemeasuredata property
            }
        }

        internal override void DeclareMeasurementCancelled()
        {
            base.DeclareMeasurementCancelled();
            _measurement.timerToCheckIfMeasOrErrorIsReceived.Tag = null;
            _measurement.timerToCheckIfMeasOrErrorIsReceived.Stop();

            StatusChange(string.Format(R.T_CANCEL), P.StepTypes.EndAll);
        }

        /// <summary>
        /// Do a directionaly random move of 1cm if distance is available or 0.2gon if not.
        /// </summary>
        public override void DoSmallMoveBeforeMeasure()
        {
            Random rnd = new Random();
            int n = rnd.Next(0, 3);

            if (Tsunami2.Properties != null)
                Tsunami2.Properties.UpdateStatus("move");

            // seems that it take time if we move by a given angle, so lets try  by moving with speed
            const bool MovingBySpeed = false;

            if (MovingBySpeed)
            {
                int h;
                int v;

                Debug.WriteInConsole("at40x-SYNC-MoveHV");
                switch (n)
                {
                    case 1: h = -50; v = -50; break;
                    case 2: h = 50; v = -50; break;
                    case 3: h = -50; v = 50; break;
                    default: h = 50; v = 50; break;
                }
                sync.MoveHV(h, v);
                System.Threading.Thread.Sleep(50);
                _motor.StopAt401();
            }
            else
            {
                double angleToMove = 0.2;
                if (_BeingMeasuredOnThisFace != null)
                {
                    if (_BeingMeasuredOnThisFace.Count > 0)
                    {
                        Tsunami2.Properties.UpdateStatus("compute angle to move from");
                        // compute angle to move 1cm at the reflector position
                        angleToMove = Math.Asin(0.005 / _BeingMeasuredOnThisFace[0].Distance.Raw.Value) / Math.PI * 200;
                    }
                }

                switch (n)
                {
                    case 1: _motor.MovebyAngle(movingDirection.Right, angleToMove); break;
                    case 2: _motor.MovebyAngle(movingDirection.Up, angleToMove); break;
                    case 3: _motor.MovebyAngle(movingDirection.Down, angleToMove); break;
                    default: _motor.MovebyAngle(movingDirection.Left, angleToMove); break;
                }
            }
        }

        public override void MeasureLikeThis()
        {

            try
            {
                _measurement.MeasureLikeThis();
            }
            catch (Exception ex)
            {
                if (LiveData.IsOn)
                    Logs.Log.AddEntryAsError(this, ex.Message);
                else
                    throw;
            }
        }

        internal override void ValidateMeasurement(Polar.Measure m)
        {

            _status.isMeasuring = false;

            base.ValidateMeasurement(m);

            ReportMeasurement();
            FinishMeasurement();

            // update status
            _status.GetSystemStatus();
            _status.GetTrackerStatus();
            _status.GetBatteryLevel();
            _status.GetInclinationSensorState();
            View.UpdateMeteo();
            View.UpdateSystemStatusVisual();

            this._camera.RestartOrNot();
        }


        public enum aT40xMeasurementMode
        {
            Standard, Precise, Fast, Outdoor
        }

        public class Measurement
        {
            public aT40xMeasurementMode mode;
            readonly Module Module;
            public Measurement(Module at40x)
            {
                Module = at40x;
                timerToCheckIfMeasOrErrorIsReceived.Stop();
                timerToCheckIfMeasOrErrorIsReceived.Tick += delegate
                {
                    // the tag is set to Now () before the measurement and set to null when measurement or message received
                    // we expect null , if not this means nothing when received by tsunami in the acceptabel time
                    if (timerToCheckIfMeasOrErrorIsReceived != null)
                        if (timerToCheckIfMeasOrErrorIsReceived.Tag != null)
                        {
                            timerToCheckIfMeasOrErrorIsReceived.Stop();
                            Module.DeclareMeasurementCancelled();
                            string titleAndMessage = $"{R.T_NO_ANSWER_FROM_INSTRUMENT};{R.T_MEASURE_HAS_BEEN_CANCELLED}";
                            new MessageInput(MessageType.Critical, titleAndMessage).Show();
                        }
                        else
                        {
                            timerToCheckIfMeasOrErrorIsReceived.Stop();
                        }
                };
            }
            //[XmlIgnore] public int numberOfMeasurement;
            public bool IsSimulation { get; set; }
            private int numerOfTheFakeMeasure;

            public void SetModeLive()
            {
                try
                {
                    HourGlass.Set();

                    Debug.WriteInConsole("at40x-SYNC-SetMeasurementMode");
                    Module.sync.SetMeasurementMode(ES_MeasMode.ES_MM_Fast);
                }
                catch (Exception ex)
                {
                    Logs.Log.AddEntryAsError(Module, $"Cannot start live data: {ex.Message}");
                }
                finally
                {
                    HourGlass.Remove();
                }
                Module.Ready();
            }

            public void SetMode()
            {
                Logs.Log.AddEntryAsBeginningOf(Module, R.T_SETTING_UP_MODE);

                string type;

                Result result = new Result();

                try
                {
                    HourGlass.Set();
                    ES_MeasMode measMode;
                    Debug.WriteInConsole("at40x-SYNC-SetMeasurementMode");
                    switch (mode)
                    {
                        case aT40xMeasurementMode.Fast:
                            measMode = ES_MeasMode.ES_MM_Fast;
                            type = "Set ES_MeasMode.ES_MM_Fast";
                            break;
                        case aT40xMeasurementMode.Outdoor:
                            measMode = ES_MeasMode.ES_MM_Outdoor;
                            type = "Set c.ES_MM_Outdoor";
                            break;
                        case aT40xMeasurementMode.Precise:
                            measMode = ES_MeasMode.ES_MM_Precise;
                            type = "Set ES_MeasMode.ES_MM_Precise";
                            break;
                        default:
                            measMode = ES_MeasMode.ES_MM_Standard;
                            type = "Set ES_MeasMode.ES_MM_Standard";
                            break;
                    }
                    Module.sync.SetMeasurementMode(measMode);

                    Logs.Log.AddEntryAsFinishOf(Module, $"{R.T_PARAMETERS_SET_TO}  '{type}'");

                    Module.StatusChange(R.T_MODE_CHANGED, P.StepTypes.End);
                    Module.Ready();
                }
                catch (Exception ex)
                {
                    throw new Exception($"{R.T_IMPOSSIBLE_TO_SET_THE_MEASUREMENT_MODE_PARAMETERS}: " + ex.Message, ex);
                }
                finally
                {
                    HourGlass.Remove();
                }
            }

            internal void SetParameters()
            {
                Logs.Log.AddEntryAsBeginningOf(Module, R.T_SETTING_UP_PARAMETERS);
                try
                {
                    Debug.WriteInConsole("at40x-SYNC-SetCoordinateSystemType");
                    Module.sync.SetCoordinateSystemType(ES_CoordinateSystemType.ES_CS_SCW);       // Système de coordonnées -> SCW: Spherical ClockWise

                    Debug.WriteInConsole("at40x-SYNC-SetUnits");
                    Module.sync.SetUnits(ES_LengthUnit.ES_LU_Meter, ES_AngleUnit.ES_AU_Gon, ES_TemperatureUnit.ES_TU_Celsius, ES_PressureUnit.ES_PU_HPascal, ES_HumidityUnit.ES_HU_RH);

                    Debug.WriteInConsole("at40x-SYNC-SetStatisticMode");
                    Module.sync.SetStatisticMode(ES_StatisticMode.ES_SM_Standard, ES_StatisticMode.ES_SM_Standard);                                 // Mode statistique

                    Debug.WriteInConsole("at40x-SYNC-SetLongSystemParameter");
                    Module.sync.SetLongSystemParameter(ES_SystemParameter.ES_SP_PowerLockFunctionActive, 1);

                    Logs.Log.AddEntryAsFinishOf(Module, $"{R.T_PARAMETERS_SET_TO} 'Spherical', 'Meter', 'Gon', Celsius', 'hPa', 'Static-Standard'");
                }
                catch (Exception ex)
                {
                    throw new Exception($"{"Impossible to set measurement parameters"}: {ex.Message}", ex);
                }
            }

            internal void GetParameters()
            {
                try
                {
                    Module.sync.GetCoordinateSystemType(out var type);
                    Logs.Log.AddEntryAsFYI(Module, $"CoordinateSystemType is {type}");

                    Module.sync.GetUnits(out var length, out var angle, out var temp, out var press, out var hum);
                    Logs.Log.AddEntryAsFYI(Module, $"Units are : length:{length}, angle:{angle}, letempngth:{temp}, press:{press}, length:{length}, hum:{hum}");

                    Module.sync.GetStatisticMode(out var stationaryMode, out var continuousMode);
                    Logs.Log.AddEntryAsFYI(Module, $"StatisticModes are : stationary:{stationaryMode}, continous:{continuousMode}");
                }
                catch (Exception ex)
                {
                    throw new Exception($"{"Impossible to get measurement parameters"}: {ex.Message}", ex);
                }
            }

            internal void GetSimulation(Polar.Measure m, FaceType faceType)
            {
                Module.StatusChange($"{R.T_LOCKED2} {Module._BeingMeasuredTheodoliteData.Face}", P.StepTypes.End);

                Random rnd = new Random();
                int month = rnd.Next(1, 13);
                m.Angles.Raw.Horizontal.Value = (rnd.Next(0, 3999999)) / 10000.0;
                if (faceType == FaceType.Face2)
                    m.Angles.Raw.Vertical.Value = rnd.Next(2000000, 3999999) / 10000.0;
                else
                    m.Angles.Raw.Vertical.Value = rnd.Next(0, 1999999) / 10000.0;
                m.Distance.Raw.Value = rnd.Next(0, 1000000) / 10000.0;
                m.Angles.Raw.Horizontal.Sigma = (rnd.Next(0, 100)) / 10000.0;
                m.Angles.Raw.Vertical.Sigma = rnd.Next(0, 100) / 10000.0;
                m.Distance.Raw.Sigma = rnd.Next(0, 100) / 10000.0;

                m.Distance.WeatherConditions.dryTemperature = rnd.Next(0, 300) / 10.0;
                m.Distance.WeatherConditions.pressure = rnd.Next(60000, 140000) / 100.0;
                m.Distance.WeatherConditions.humidity = rnd.Next(0, 600) / 10;
                switch (m._Point._Name)
                {
                    case "Point_001":
                        if (faceType == FaceType.Face1)
                        {
                            m.Angles.Raw.Horizontal.Value = 334.18429;
                            m.Angles.Raw.Vertical.Value = 96.92029;
                        }
                        else
                        {
                            m.Angles.Raw.Horizontal.Value = 34.18430;
                            m.Angles.Raw.Vertical.Value = 400 - 96.92030;
                        }
                        m.Distance.Raw.Value = 9.357193;
                        numerOfTheFakeMeasure += 1;
                        break;
                    case "Point_002":
                        if (faceType == FaceType.Face1)
                        {
                            m.Angles.Raw.Horizontal.Value = 127.31244;
                            m.Angles.Raw.Vertical.Value = 105.50056;
                        }
                        else
                        {
                            m.Angles.Raw.Horizontal.Value = 27.31245;
                            m.Angles.Raw.Vertical.Value = 400 - 105.50055;
                        }

                        m.Distance.Raw.Value = 6.095938;
                        numerOfTheFakeMeasure += 1;
                        break;
                    case "Point_003":
                        if (faceType == FaceType.Face1)
                        {
                            m.Angles.Raw.Horizontal.Value = 150.82600;
                            m.Angles.Raw.Vertical.Value = 85.23349;
                        }
                        else
                        {
                            m.Angles.Raw.Horizontal.Value = 50.82601;
                            m.Angles.Raw.Vertical.Value = 400 - 85.23348;
                        }

                        m.Distance.Raw.Value = 3.186488;
                        numerOfTheFakeMeasure += 1;
                        break;
                    case "Point_004":
                        if (faceType == FaceType.Face1)
                        {
                            m.Angles.Raw.Horizontal.Value = 34.18429;
                            m.Angles.Raw.Vertical.Value = 96.92029;
                        }
                        else
                        {
                            m.Angles.Raw.Horizontal.Value = 334.18429;
                            m.Angles.Raw.Vertical.Value = 400 - 96.92032;
                        }

                        m.Distance.Raw.Value = 9.357193;
                        numerOfTheFakeMeasure += 1;
                        break;
                    default:
                        break;
                }
                Module.JustMeasuredData = m;
                BackgroundWorker b = new BackgroundWorker();
                b.WorkerReportsProgress = true;
                b.DoWork += delegate
                    {
                        b.ReportProgress(30);
                        System.Threading.Thread.Sleep(1000);
                        b.ReportProgress(60);
                        System.Threading.Thread.Sleep(1000);
                        b.ReportProgress(90);
                        System.Threading.Thread.Sleep(1000);
                    };
                b.ProgressChanged += delegate (object sender, ProgressChangedEventArgs e)
                {
                    switch (e.ProgressPercentage)
                    {
                        case 30: Module.StartMeasurement(); break;

                        case 60: break;

                        case 90: Module.Lock(); break;
                        default:

                            break;
                    }
                };
                b.RunWorkerCompleted += delegate
                {
                    Module.SendMessage(Module.JustMeasuredData);
                    Module.WaitingForMeasure = false;
                    Module._TsuView.IsWaiting = false;
                    Module.ReportMeasurement();
                    Module.FinishMeasurement();
                };
                b.RunWorkerAsync();

                Module.StatusChange($"{R.T_MEASURED} {Module._BeingMeasuredTheodoliteData.Face}", P.StepTypes.End);
            }

            internal void MeasureLikeThis()
            {
                bool liveDataOn = Module.LiveData.IsOn;
                Module.State = new InstrumentState(InstrumentState.type.Measuring);

                Module._status.isMeasuring = true;

                // removed and move to measure first face to avoid double event for double face : this.Module.StartMeasurement(); // to start the event
                if (!liveDataOn)
                    Logs.Log.AddEntryAsBeginningOf(Module, R.T_MEASURING);
                else
                    Module.BeingMeasured._Status = new M.States.Cancel(Module.BeingMeasured._Status.Type);
                try
                {
                    if (IsSimulation)
                    {
                        GetSimulation(Module._BeingMeasuredTheodoliteData, Module._BeingMeasuredTheodoliteData.Face);
                    }
                    else
                    {
                        // 1. Face and goto
                        Module.ChangeFaceAndDoGoto();

                        if (Module.IsFirstMesaureOfFirstFace)
                        {
                            if (!liveDataOn && Module.CancelByMessageBeforeMeasure())
                            {
                                Module.BeingMeasured = null;
                                throw new CancelException();
                            }
                        }

                        // 2. Catching
                        //bool oldFashion = false;

                        //if (oldFashion)
                        //{
                        //    if (!this.Module._status.isTrackerReady)
                        //    {
                        //        if (!this.Module.Lock())
                        //            throw new Exception($"{R.T_LOCKING_PROBLEM_THE_AT40X_DIDNT_MANAGE_TO_LOCK_ON_REFLECTOR}");
                        //    }
                        //    else
                        //        if (!this.Module.IsContinuousMeasurementOn)
                        //            this.Module.StatusChange($"{R.T_WAS_LOOKED}", P.stepType.End);
                        //}
                        //else
                        {
                            if (Module.IsContinuousMeasurementOn)
                            {

                            }
                            else
                            {
                                if (!Module.Lock())
                                    throw new Exception($"{R.T_LOCKING_PROBLEM_THE_AT40X_DIDNT_MANAGE_TO_LOCK_ON_REFLECTOR}");
                            }
                        }

                        // 3. Measuring
                        if (Module._target.selected.id < 100) // meaning we have a reflector and not a probe selected 
                        {
                            GetStationnaryReflectorMeasure(Module._BeingMeasuredTheodoliteData);
                        }
                        else
                            GetStationaryProbeMeasure(Module._BeingMeasuredTheodoliteData);
                    }
                }

                catch (CancelException)
                {
                    Module.StatusChange(string.Format(R.T_CANCEL), P.StepTypes.EndAll);
                    Module.DeclareMeasurementCancelled();
                }
                catch (AsyncException ex)
                {
                    if (!liveDataOn)
                    {
                        string titleAndMessage = $"{R.T_ERROR};{ex}";
                        new MessageInput(MessageType.Critical, titleAndMessage).Show();
                    }
                }
                catch (Exception ex)
                {
                    Module.LastMessage = R.T_MEASUREMENT_FAILED;
                    Module.StatusChange(string.Format(Module.LastMessage), P.StepTypes.EndAll);

                    if (ex.Message == "[49] \"Start Measurement\" failed  (status:  TRK #111)")
                        Module.LastMessage += $"{R.T_THE_INSTRUMENT_IS_NOT_INITIALIZED}: " + ex.Message;
                    else
                        Module.LastMessage += ", " + ex.Message;

                    Module.JustMeasuredData = Module.BeingMeasured;

                    Module.State = new InstrumentState(InstrumentState.type.Idle);

                    if (Module.LiveData.IsOn || Debug.IsRunningInATest)
                        Logs.Log.AddEntryAsError(Module, Module.LastMessage);
                    else
                        Module.AskIfRemeasure();
                }
            }

            internal System.Windows.Forms.Timer timerToCheckIfMeasOrErrorIsReceived = new System.Windows.Forms.Timer() { Interval = 1000 * 15, Enabled = true };


            internal void GetStationaryProbeMeasure(Polar.Measure m)
            {
                Module.State = new InstrumentState(InstrumentState.type.Measuring);
                ES_MeasMode a;
                Debug.WriteInConsole("at40x-SYNC-GetMeasurementMode");
                Module.sync.GetMeasurementMode(out a);
                Debug.WriteInConsole("at40x-ASYNC-MeasureStationaryProbeData");

                Module.async.StartMeasurement();
            }

            internal void GetStationnaryReflectorMeasure(Polar.Measure m)
            {
                Module.timeSpentMeasuring.Add("async MeasureStationaryPoint", TimeSpent.type.Started, DateTime.Now);
                Module.WaitingForMeasure = true;
                Module.State = new InstrumentState(InstrumentState.type.Measuring);
                if (!Module.IsContinuousMeasurementOn)
                    Module.InvokeMeasureAcquiring();
                int n = Module._BeingMeasuredTheodoliteData.NumberOfMeasureToAverage + 1 - Module.NumberOfMeasureOnThisFace;
                int nn = Module._BeingMeasuredTheodoliteData.NumberOfMeasureToAverage;
                if (!Module.IsContinuousMeasurementOn && m._Point != null)
                    Module.StatusChange($"{R.T_MEASURING} {m._Point._Name} {R.T_WITH} {m.Face}, {R.T_MEASURE} {n} / {nn}", P.StepTypes.Begin);

                timerToCheckIfMeasOrErrorIsReceived.Tag = DateTime.Now;

                Debug.WriteInConsole("at40x-ASYNC-MeasureStationaryPoint");
                Module.async.StartMeasurement();
            }

            internal void StationaryProbeMeasurementAnswer(object sender, Receiver.StationaryProbeMeasurementAnswerEventArgs e)
            {
                Module.async.RemoveFromInProgressListIfExist(ES_Command.ES_C_StartMeasurement);
                Module._status.isMeasuring = false;

                timerToCheckIfMeasOrErrorIsReceived.Tag = null;
                timerToCheckIfMeasOrErrorIsReceived.Stop();

                Module.numberOfError = 0;
                Polar.Measure m = Module._BeingMeasuredTheodoliteData;
                Module.InvokeMeasureAcquiring();
                m._Date = DateTime.Now;

                m.Angles.Raw.Horizontal.Value = e.ProbeStationaryResult.dPosition1;
                m.Angles.Raw.Vertical.Value = e.ProbeStationaryResult.dPosition2;
                m.Distance.Raw.Value = e.ProbeStationaryResult.dPosition3;

                m.Angles.Raw.Horizontal.Sigma = e.ProbeStationaryResult.dStdDevPosition1;
                m.Angles.Raw.Vertical.Sigma = e.ProbeStationaryResult.dStdDevPosition2;
                m.Distance.Raw.Sigma = e.ProbeStationaryResult.dStdDevPosition3;

                m.Distance.WeatherConditions.dryTemperature = e.ProbeStationaryResult.dTemperature;
                m.Distance.WeatherConditions.pressure = e.ProbeStationaryResult.dPressure;
                m.Distance.WeatherConditions.humidity = (int)e.ProbeStationaryResult.dHumidity;

                try
                {
                    Module.DoMeasureCorrectionsThenNextMeasurementStep(m);
                }
                catch (Exception ex)
                {
                    Module.MeasureExceptionTreatment(ex);
                    new MessageInput(MessageType.Critical, ex.Message).Show();
                }
            }

            internal void SingleMeasurementAnswer(object sender, Receiver.SingleMeasurementAnswerEventArgs e)
            {
                Polar.Measure m;
                try
                {
                    // choose where to pur the result
                    
                    if (Module.LiveData.IsOn)
                        m = new Polar.Measure();
                    else
                        m = Module._BeingMeasuredTheodoliteData;

                    bool isCancelled = (m == null) || (m._Status.Type == M.States.Types.Cancel);

                    Module.async.RemoveFromInProgressListIfExist(ES_Command.ES_C_StartMeasurement);

                    if (!(Module.LiveData.IsOn))
                    {
                        string messageAcquisition = $"Acquisition received";
                        if (isCancelled)
                            messageAcquisition += " but cancelled";
                        Logs.Log.AddEntryAsFYI(Module, messageAcquisition);
                    }

                    timerToCheckIfMeasOrErrorIsReceived.Tag = null;
                    timerToCheckIfMeasOrErrorIsReceived.Stop();

                    Module.numberOfError = 0;
                    if (Module._BeingMeasuredTheodoliteData == null)
                    {
                        Logs.Log.AddEntryAsFYI(Module, "Measured received but was cancelled by the user");
                        return;
                    }

                    // Convvert to tsu objects

                    var smr = e.SingleMeasResult;
                    double h = smr.dVal1;
                    double v = smr.dVal2;
                    double d = smr.dVal3;
                    if (h < 0) h = h + 400;
                    if (h >= 400) h = h - 400;

                    if (v > 200)
                    {

                    }


                    // check precisions are in tolérances
                    var tol = Tsunami2.Preferences.Values.InstrumentsTolerances.FirstOrDefault(x => x.InstrumentType.ToString().Contains("AT4"));
                    double alpha_gon = tol.Same_Face.H_CC / 100 / 100;
                    double alpha_rad = alpha_gon / 200 * Math.PI;
                    double tol3 = Math.Sin(alpha_rad) * d;
                    tol3 = tol3 * 3;
                    double tolH = tol.Same_Face.H_CC / 100 / 100;
                    double tolV = tol.Same_Face.V_CC / 100 / 100;
                    double tolD = tol.Same_Face.D_mm / 1000;
                    bool ok3 = Tools.Conversions.Tolerances.Check(value: smr.dStdTotal, tolerance: tol3, out string message3, "mm");
                    bool okH = Tools.Conversions.Tolerances.Check(value: smr.dStd1, tolerance: tolH, out string messageH, "cc");
                    bool okV = Tools.Conversions.Tolerances.Check(value: smr.dStd2, tolerance: tolV, out string messageV, "cc");
                    bool okD = Tools.Conversions.Tolerances.Check(value: smr.dStd3, tolerance: tolD, out string messageD, "mm");
                    bool okAll = ok3 && okH && okV && okD;

                    if (!okAll && !Module.LiveData.IsOn && !isCancelled)
                    {
                        Module._measurement.GetParameters();
                        Module._measurement.SetParameters();

                        string message = $"{R.T_DEVIATION_PROBLEM}";
                        if (!okH) message += $"\r\nsH: {messageH}";
                        if (!okV) message += $"\r\nsV: {messageV}";
                        if (!okD) message += $"\r\nsD: {messageD}";
                        if (!ok3) message += $"\r\ns3D: {message3}";
                        string r = new MessageInput(MessageType.Critical, message)
                        {
                            ButtonTexts = new List<string> { R.T_CONTINUE, R.T_RETRY, R.T_CANCEL }
                        }.Show().TextOfButtonClicked;
                        if (r != R.T_CONTINUE)
                        {
                            Module.LastMessage = R.T_MEASUREMENT_FAILED;
                            Module.StatusChange(string.Format(Module.LastMessage), P.StepTypes.EndAll);
                            Module.HaveFailed();
                            if (r == R.T_RETRY)
                                Module.ReMeasure();
                            return;
                        }
                    }

                    Elements.Angles rawAngles = new Elements.Angles
                    {
                        Horizontal = new DoubleValue(h, smr.dStd1),
                        Vertical = new DoubleValue(smr.dVal2, smr.dStd2)
                    };
                    DoubleValue rawDistance = new DoubleValue(smr.dVal3, smr.dStd3);
                    if (Module._BeingMeasuredTheodoliteData.Face == FaceType.Face2)
                        Survey.TransformToOppositeFace(rawAngles);



                    // Fill the measurementObject
                    m._Date = DateTime.Now;
                    m.Angles.Raw = rawAngles;
                    m.Distance.Raw = rawDistance;
                    m.Distance.WeatherConditions.dryTemperature = smr.dTemperature;
                    m.Distance.WeatherConditions.pressure = smr.dPressure;
                    m.Distance.WeatherConditions.humidity = (int)smr.dHumidity;

                    if (m.Distance.Reflector._Name == R.T_MANAGED_BY_AT40X ||
                        m.Distance.Reflector._Name == R.String_Unknown)
                        m.Distance.Reflector = this.Module.ActualReflector;


                    Module._status.temperature = smr.dTemperature;
                    Module._status.pressure = smr.dPressure;
                    Module._status.humidity = smr.dHumidity;

                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    Module.timeSpentMeasuring.Add("async MeasureStationaryPoint", TimeSpent.type.Stopped, DateTime.Now);
                }

                // check if livedata
                if (Module.LiveData.IsOn)
                {
                    Module.WaitingForMeasure = false;
                    Module._status.isTrackerBusy = false;
                    Module._status.isMeasuring = false;
                    Module.LiveData.OnGoodMeasurement(m);


                    // this.Module.FinishMeasurement();
                    return;
                }
                else
                {
                    try
                    {
                        //if (m._Status.type != Measures.StateType.Cancel) // was maybe here for after live data measure, but then the normal cancel do ot clean the action list
                        Module.DoMeasureCorrectionsThenNextMeasurementStep(m);
                        //else
                        //this.Module.DeclareMeasurementCancelled();
                    }
                    catch (Exception ex)
                    {

                        Module.MeasureExceptionTreatment(ex);
                        Tsunami2.Properties.InvokeOnApplicationDispatcher(() =>
                        {
                            new MessageInput(MessageType.Critical, ex.Message).Show();
                        });
                    }

                    //this.Module.View.UpdateMeteo();
                }

            }

            internal void Cancel()
            {
                if (Module.BeingMeasured != null)
                    Module.BeingMeasured._Status = new M.States.Cancel();
                else
                {
                    string titleAndMessage = $"{R.T_NO_ACQUISITION_IN_PROGRESS};{R.T_YOU_CAN_ONLY_CANCEL_IT_THE_MEASUREMENT_IS_IN_PROCESS}";
                    new MessageInput(MessageType.Warning, titleAndMessage).Show();
                }
            }

            internal void StartStopAndGoMeasurement()
            {
                // set the region croteria to start a measure in stableprobing mode in meters
                Debug.WriteInConsole("at40x-SYNC-SetDoubleSystemParameter");
                Module.sync.SetDoubleSystemParameter(ES_SystemParameter.ES_SP_D_StableProbingCriteriaRegion, 0.005);

                // Specify the criteria of time: how long shoufl i stay stable (in ms)
                Debug.WriteInConsole("at40x-SYNC-SetLongSystemParameter");
                Module.sync.SetLongSystemParameter(ES_SystemParameter.ES_SP_StableProbingCriteriaTime, 500);

                // start the StableProbingTrigger to receive a measure when the reflector respect the  criteria
                Debug.WriteInConsole("at40x-SYNC-SetLongSystemParameter");
                Module.sync.SetLongSystemParameter(ES_SystemParameter.ES_SP_StableProbingTrigger, 1);
            }
        }

        /// <summary>
        /// Override la fonction car les distances At40X ne doivent pas être corrigée
        /// </summary>
        /// <param name="distance"></param>
        internal override void ChooseDistanceCorrectionWanted(M.MeasureOfDistance distance)
        {
            distance.IsCorrectedForEtalonnage = true;
            distance.IsCorrectedForPrismConstanteValue = false;
            distance.IsCorrectedForWeatherConditions = true;
            distance.IsDistanceOutOfRange = false;
        }
        #endregion

        #region Interface implementation
        public Result MoveTo()
        {
            try
            {
                _motor.MovebyDirection(movingDirection.Left);
                return new Result() { Success = true };
            }
            catch (Exception)
            {
                return new Result() { Success = false };
            }
        }

        public Result FineAdjust()
        {
            return null;
        }


        public void SaveMeFromNotWorkingConnection(object sender, EventArgs e)
        {
            WiFi.TryToConnectAutomaticallyWithWifi(ConnectToAT, SN: At40x._SerialNumber, worker: this.View.worker);
        }


        public bool IsConnectedToAT(string ip)
        {
            bool connected = InstrumentIsOn;

            if (connected)
                Logs.Log.AddEntryAsSuccess(null, R.WifiConnected + ip);
            return connected;

        }

        public bool ConnectToAT(string ipAddress, int port)
        {
            bool isConnected = IsConnectedToAT(ipAddress);
            try
            {
                if (!isConnected)
                {
                    Debug.WriteInConsole($"Module {Id} is connecting to {ipAddress}:{port}");
                    ConnectAdapter(new Ip(ipAddress));
                    if (socket.Connected)
                    {
                        Logs.Log.AddEntryAsSuccess(null, R.WifiConnected + ipAddress);
                        isConnected = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0};{1}: {2}", R.T_CONNECTION_FAILED, ex.Message, R.T_YOU_SHOULD_TRY_TO_RESET), ex);
            }

            return isConnected;


        }


        internal void CheckFinalStatus(bool isConnectedToTP, bool isConnectedToAT)
        {
            if (!isConnectedToTP && !isConnectedToAT)
                Logs.Log.AddEntryAsError(null, R.WifiAndIpNotConnected);
            else if (isConnectedToTP && !isConnectedToAT)
                Logs.Log.AddEntryAsError(null, R.IpNotConnected);
            else if (!isConnectedToTP && isConnectedToAT)
                Logs.Log.AddEntryAsError(null, R.WifiNotConnected);
            else
                Logs.Log.AddEntryAsSuccess(null, R.WifiAndIpConnected);
        }

        void ILiveData.LiveData_Start()
        {
            if (View.liveDataView.IsVisible)
                LiveData.Start();
        }

        void ILiveData.LiveData_Stop()
        {
            if (View.liveDataView.IsVisible)
                LiveData.Stop();
        }

        void ILiveData.LiveData_StartStop()
        {
            if (View.liveDataView.IsVisible)
            {
                if (LiveData.IsOn)
                    LiveData.Stop();
                else
                    LiveData.Start();
            }

        }

        void IConnectable.Refresh()
        {
            View.ClearStateAndUpdate();
        }

        public override void Dispose()
        {
            base.Dispose();
            SocketDisconnect();
        }
        /// <summary>
        /// force the target as selected for F-test purpose
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public void SetTargetAsSelected()
        {
            this._target.IsSelected = true;
        }

        #endregion
    }
}
