﻿using TSU.Views;
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments.Device.AT40x
{
    partial class LiveDataView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonHVD = new System.Windows.Forms.Button();
            this.buttonXYZ = new System.Windows.Forms.Button();
            this.buttonOffset = new System.Windows.Forms.Button();
            this.buttonCCS = new System.Windows.Forms.Button();
            this.buttonSU = new System.Windows.Forms.Button();
            this.buttonPhys = new System.Windows.Forms.Button();
            this.button2Station = new System.Windows.Forms.Button();
            this.buttonBeam = new System.Windows.Forms.Button();
            this.buttonIncrease = new System.Windows.Forms.Button();
            this.buttonDecrease = new System.Windows.Forms.Button();
            this.buttonMMM = new System.Windows.Forms.Button();
            this.buttonFindClosest = new System.Windows.Forms.Button();
            this.button_Stop = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.buttonDispl = new System.Windows.Forms.Button();
            this.buttonBeamV = new System.Windows.Forms.Button();
            this.button_Start = new System.Windows.Forms.Button();
            this.buttonMap = new System.Windows.Forms.Button();
            this.buttonOthers = new System.Windows.Forms.Button();
            this.label0 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(53, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Observation 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(50, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Observation 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(53, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Observation 3";
            // 
            // buttonHVD
            // 
            this.buttonHVD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonHVD.BackgroundImage = global::TSU.Properties.Resources.At40x_Live_HDV;
            this.buttonHVD.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonHVD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonHVD.Location = new System.Drawing.Point(0, 125);
            this.buttonHVD.Name = "buttonHVD";
            this.buttonHVD.Size = new System.Drawing.Size(50, 50);
            this.buttonHVD.TabIndex = 34;
            this.buttonHVD.UseVisualStyleBackColor = true;
            this.buttonHVD.Click += new System.EventHandler(this.ButtonHVD_Click);
            this.buttonHVD.MouseEnter += new System.EventHandler(this.buttons_MouseEnter);
            this.buttonHVD.MouseLeave += new System.EventHandler(this.buttons_MouseLeaving);
            // 
            // buttonXYZ
            // 
            this.buttonXYZ.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonXYZ.BackgroundImage = global::TSU.Properties.Resources.At40x_Live_XYZ;
            this.buttonXYZ.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonXYZ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonXYZ.Location = new System.Drawing.Point(0, 175);
            this.buttonXYZ.Name = "buttonXYZ";
            this.buttonXYZ.Size = new System.Drawing.Size(50, 50);
            this.buttonXYZ.TabIndex = 35;
            this.buttonXYZ.UseVisualStyleBackColor = true;
            this.buttonXYZ.Click += new System.EventHandler(this.ButtonXYZ_Click);
            this.buttonXYZ.MouseEnter += new System.EventHandler(this.buttons_MouseEnter);
            this.buttonXYZ.MouseLeave += new System.EventHandler(this.buttons_MouseLeaving);
            // 
            // buttonOffset
            // 
            this.buttonOffset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonOffset.BackgroundImage = global::TSU.Properties.Resources.At40x_Live_Diff;
            this.buttonOffset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonOffset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOffset.Location = new System.Drawing.Point(0, 225);
            this.buttonOffset.Name = "buttonOffset";
            this.buttonOffset.Size = new System.Drawing.Size(50, 50);
            this.buttonOffset.TabIndex = 36;
            this.buttonOffset.UseVisualStyleBackColor = true;
            this.buttonOffset.Click += new System.EventHandler(this.ButtonDiff_Click);
            this.buttonOffset.MouseEnter += new System.EventHandler(this.buttons_MouseEnter);
            this.buttonOffset.MouseLeave += new System.EventHandler(this.buttons_MouseLeaving);
            // 
            // buttonCCS
            // 
            this.buttonCCS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCCS.BackgroundImage = global::TSU.Properties.Resources.At40x_Live_CCS;
            this.buttonCCS.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonCCS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCCS.Location = new System.Drawing.Point(332, 324);
            this.buttonCCS.Name = "buttonCCS";
            this.buttonCCS.Size = new System.Drawing.Size(50, 49);
            this.buttonCCS.TabIndex = 37;
            this.buttonCCS.UseVisualStyleBackColor = true;
            this.buttonCCS.Visible = false;
            this.buttonCCS.Click += new System.EventHandler(this.ButtonCCS_Click);
            this.buttonCCS.MouseEnter += new System.EventHandler(this.buttons_MouseEnter);
            this.buttonCCS.MouseLeave += new System.EventHandler(this.buttons_MouseLeaving);
            // 
            // buttonSU
            // 
            this.buttonSU.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSU.BackgroundImage = global::TSU.Properties.Resources.At40x_Live_SU;
            this.buttonSU.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonSU.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSU.Location = new System.Drawing.Point(23, 325);
            this.buttonSU.Name = "buttonSU";
            this.buttonSU.Size = new System.Drawing.Size(50, 50);
            this.buttonSU.TabIndex = 38;
            this.buttonSU.UseVisualStyleBackColor = true;
            this.buttonSU.Click += new System.EventHandler(this.ButtonSU_Click);
            this.buttonSU.MouseEnter += new System.EventHandler(this.buttons_MouseEnter);
            this.buttonSU.MouseLeave += new System.EventHandler(this.buttons_MouseLeaving);
            // 
            // buttonPhys
            // 
            this.buttonPhys.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonPhys.BackgroundImage = global::TSU.Properties.Resources.At40x_Live_PHY;
            this.buttonPhys.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonPhys.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPhys.Location = new System.Drawing.Point(73, 325);
            this.buttonPhys.Name = "buttonPhys";
            this.buttonPhys.Size = new System.Drawing.Size(50, 50);
            this.buttonPhys.TabIndex = 39;
            this.buttonPhys.UseVisualStyleBackColor = true;
            this.buttonPhys.Click += new System.EventHandler(this.ButtonPhys_Click);
            this.buttonPhys.MouseEnter += new System.EventHandler(this.buttons_MouseEnter);
            this.buttonPhys.MouseLeave += new System.EventHandler(this.buttons_MouseLeaving);
            // 
            // button2Station
            // 
            this.button2Station.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2Station.BackgroundImage = global::TSU.Properties.Resources.At40x_Live_2St;
            this.button2Station.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2Station.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2Station.Location = new System.Drawing.Point(388, 323);
            this.button2Station.Name = "button2Station";
            this.button2Station.Size = new System.Drawing.Size(50, 50);
            this.button2Station.TabIndex = 40;
            this.button2Station.UseVisualStyleBackColor = true;
            this.button2Station.Visible = false;
            this.button2Station.Click += new System.EventHandler(this.Button2Station_Click);
            this.button2Station.MouseEnter += new System.EventHandler(this.buttons_MouseEnter);
            this.button2Station.MouseLeave += new System.EventHandler(this.buttons_MouseLeaving);
            // 
            // buttonBeam
            // 
            this.buttonBeam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBeam.BackgroundImage = global::TSU.Properties.Resources.At40x_Live_Beam;
            this.buttonBeam.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonBeam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBeam.Location = new System.Drawing.Point(123, 325);
            this.buttonBeam.Name = "buttonBeam";
            this.buttonBeam.Size = new System.Drawing.Size(50, 50);
            this.buttonBeam.TabIndex = 41;
            this.buttonBeam.UseVisualStyleBackColor = true;
            this.buttonBeam.Click += new System.EventHandler(this.ButtonBeam_Click);
            this.buttonBeam.MouseEnter += new System.EventHandler(this.buttons_MouseEnter);
            this.buttonBeam.MouseLeave += new System.EventHandler(this.buttons_MouseLeaving);
            // 
            // buttonIncrease
            // 
            this.buttonIncrease.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonIncrease.BackgroundImage = global::TSU.Properties.Resources.At40x_Live_Decimal_increase;
            this.buttonIncrease.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonIncrease.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonIncrease.Location = new System.Drawing.Point(797, 223);
            this.buttonIncrease.Name = "buttonIncrease";
            this.buttonIncrease.Size = new System.Drawing.Size(50, 50);
            this.buttonIncrease.TabIndex = 43;
            this.buttonIncrease.UseVisualStyleBackColor = true;
            this.buttonIncrease.Click += new System.EventHandler(this.ButtonIncrease_Click);
            this.buttonIncrease.MouseEnter += new System.EventHandler(this.buttons_MouseEnter);
            this.buttonIncrease.MouseLeave += new System.EventHandler(this.buttons_MouseLeaving);
            // 
            // buttonDecrease
            // 
            this.buttonDecrease.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDecrease.BackgroundImage = global::TSU.Properties.Resources.At40x_Live_Decimal_decrease;
            this.buttonDecrease.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonDecrease.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDecrease.Location = new System.Drawing.Point(797, 273);
            this.buttonDecrease.Name = "buttonDecrease";
            this.buttonDecrease.Size = new System.Drawing.Size(50, 50);
            this.buttonDecrease.TabIndex = 44;
            this.buttonDecrease.UseVisualStyleBackColor = true;
            this.buttonDecrease.Click += new System.EventHandler(this.ButtonDecrease_Click);
            this.buttonDecrease.MouseEnter += new System.EventHandler(this.buttons_MouseEnter);
            this.buttonDecrease.MouseLeave += new System.EventHandler(this.buttons_MouseLeaving);
            // 
            // buttonMMM
            // 
            this.buttonMMM.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMMM.BackgroundImage = global::TSU.Properties.Resources.At40x_Live_MMM;
            this.buttonMMM.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonMMM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMMM.Location = new System.Drawing.Point(726, 324);
            this.buttonMMM.Name = "buttonMMM";
            this.buttonMMM.Size = new System.Drawing.Size(50, 50);
            this.buttonMMM.TabIndex = 45;
            this.buttonMMM.UseVisualStyleBackColor = true;
            this.buttonMMM.Click += new System.EventHandler(this.ButtonMMM_Click);
            this.buttonMMM.MouseEnter += new System.EventHandler(this.buttons_MouseEnter);
            this.buttonMMM.MouseLeave += new System.EventHandler(this.buttons_MouseLeaving);
            // 
            // buttonFindClosest
            // 
            this.buttonFindClosest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonFindClosest.BackgroundImage = global::TSU.Properties.Resources.At40x_Live_Closest;
            this.buttonFindClosest.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonFindClosest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFindClosest.Location = new System.Drawing.Point(774, 324);
            this.buttonFindClosest.Name = "buttonFindClosest";
            this.buttonFindClosest.Size = new System.Drawing.Size(50, 50);
            this.buttonFindClosest.TabIndex = 46;
            this.buttonFindClosest.UseVisualStyleBackColor = true;
            this.buttonFindClosest.Click += new System.EventHandler(this.button_FindClosest_Click);
            this.buttonFindClosest.MouseEnter += new System.EventHandler(this.buttons_MouseEnter);
            this.buttonFindClosest.MouseLeave += new System.EventHandler(this.buttons_MouseLeaving);
            // 
            // button_Stop
            // 
            this.button_Stop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Stop.BackgroundImage = global::TSU.Properties.Resources.At40x_Live_Stop;
            this.button_Stop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Stop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Stop.Location = new System.Drawing.Point(797, 173);
            this.button_Stop.Name = "button_Stop";
            this.button_Stop.Size = new System.Drawing.Size(50, 50);
            this.button_Stop.TabIndex = 47;
            this.button_Stop.UseVisualStyleBackColor = true;
            this.button_Stop.Click += new System.EventHandler(this.ButtonStop_Click);
            this.button_Stop.MouseEnter += new System.EventHandler(this.buttons_MouseEnter);
            this.button_Stop.MouseLeave += new System.EventHandler(this.buttons_MouseLeaving);
            // 
            // buttonDispl
            // 
            this.buttonDispl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonDispl.BackgroundImage = global::TSU.Properties.Resources.At40x_Live_Disp;
            this.buttonDispl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonDispl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDispl.Location = new System.Drawing.Point(0, 275);
            this.buttonDispl.Name = "buttonDispl";
            this.buttonDispl.Size = new System.Drawing.Size(50, 50);
            this.buttonDispl.TabIndex = 48;
            this.buttonDispl.UseVisualStyleBackColor = true;
            this.buttonDispl.Click += new System.EventHandler(this.buttonDispl_Click);
            this.buttonDispl.MouseEnter += new System.EventHandler(this.buttons_MouseEnter);
            this.buttonDispl.MouseLeave += new System.EventHandler(this.buttons_MouseLeaving);
            // 
            // buttonBeamV
            // 
            this.buttonBeamV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBeamV.BackgroundImage = global::TSU.Properties.Resources.At40x_Live_BeamV;
            this.buttonBeamV.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonBeamV.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBeamV.Location = new System.Drawing.Point(173, 325);
            this.buttonBeamV.Name = "buttonBeamV";
            this.buttonBeamV.Size = new System.Drawing.Size(50, 50);
            this.buttonBeamV.TabIndex = 49;
            this.buttonBeamV.UseVisualStyleBackColor = true;
            this.buttonBeamV.Click += new System.EventHandler(this.buttonBeamV_Click);
            // 
            // button_Start
            // 
            this.button_Start.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Start.BackgroundImage = global::TSU.Properties.Resources.At40x_Live_Play;
            this.button_Start.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Start.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Start.Location = new System.Drawing.Point(797, 122);
            this.button_Start.Name = "button_Start";
            this.button_Start.Size = new System.Drawing.Size(50, 50);
            this.button_Start.TabIndex = 50;
            this.button_Start.UseVisualStyleBackColor = true;
            this.button_Start.Click += new System.EventHandler(this.button_Start_Click);
            this.button_Start.MouseEnter += new System.EventHandler(this.buttons_MouseEnter);
            this.button_Start.MouseLeave += new System.EventHandler(this.buttons_MouseLeaving);
            // 
            // buttonMap
            // 
            this.buttonMap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMap.BackgroundImage = global::TSU.Properties.Resources.At40x_map;
            this.buttonMap.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonMap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMap.Location = new System.Drawing.Point(678, 324);
            this.buttonMap.Name = "buttonMap";
            this.buttonMap.Size = new System.Drawing.Size(50, 50);
            this.buttonMap.TabIndex = 51;
            this.buttonMap.UseVisualStyleBackColor = true;
            this.buttonMap.Click += new System.EventHandler(this.buttonMap_Click);
            // 
            // buttonOthers
            // 
            this.buttonOthers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonOthers.BackgroundImage = global::TSU.Properties.Resources.Add;
            this.buttonOthers.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonOthers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOthers.Location = new System.Drawing.Point(233, 325);
            this.buttonOthers.Name = "buttonOthers";
            this.buttonOthers.Size = new System.Drawing.Size(50, 49);
            this.buttonOthers.TabIndex = 52;
            this.buttonOthers.UseVisualStyleBackColor = true;
            this.buttonOthers.Click += new System.EventHandler(this.buttonOthers_Click);
            // 
            // label0
            // 
            this.label0.AutoSize = true;
            this.label0.BackColor = System.Drawing.Color.Transparent;
            this.label0.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label0.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label0.Location = new System.Drawing.Point(53, 9);
            this.label0.Name = "label0";
            this.label0.Size = new System.Drawing.Size(58, 13);
            this.label0.TabIndex = 53;
            this.label0.Text = "pointName";
            // 
            // LiveDataView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.ClientSize = new System.Drawing.Size(850, 376);
            this.Controls.Add(this.label0);
            this.Controls.Add(this.buttonOthers);
            this.Controls.Add(this.buttonMap);
            this.Controls.Add(this.button_Start);
            this.Controls.Add(this.buttonBeamV);
            this.Controls.Add(this.buttonDispl);
            this.Controls.Add(this.button_Stop);
            this.Controls.Add(this.buttonFindClosest);
            this.Controls.Add(this.buttonMMM);
            this.Controls.Add(this.buttonDecrease);
            this.Controls.Add(this.buttonIncrease);
            this.Controls.Add(this.buttonBeam);
            this.Controls.Add(this.button2Station);
            this.Controls.Add(this.buttonPhys);
            this.Controls.Add(this.buttonSU);
            this.Controls.Add(this.buttonCCS);
            this.Controls.Add(this.buttonOffset);
            this.Controls.Add(this.buttonXYZ);
            this.Controls.Add(this.buttonHVD);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "LiveDataView";
            this.Padding = new System.Windows.Forms.Padding(0);
            this.Resize += new System.EventHandler(this.LiveDataView_Resize);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.buttonHVD, 0);
            this.Controls.SetChildIndex(this.buttonXYZ, 0);
            this.Controls.SetChildIndex(this.buttonOffset, 0);
            this.Controls.SetChildIndex(this.buttonCCS, 0);
            this.Controls.SetChildIndex(this.buttonSU, 0);
            this.Controls.SetChildIndex(this.buttonPhys, 0);
            this.Controls.SetChildIndex(this.button2Station, 0);
            this.Controls.SetChildIndex(this.buttonBeam, 0);
            this.Controls.SetChildIndex(this.buttonIncrease, 0);
            this.Controls.SetChildIndex(this.buttonDecrease, 0);
            this.Controls.SetChildIndex(this.buttonMMM, 0);
            this.Controls.SetChildIndex(this.buttonFindClosest, 0);
            this.Controls.SetChildIndex(this.button_Stop, 0);
            this.Controls.SetChildIndex(this.buttonDispl, 0);
            this.Controls.SetChildIndex(this.buttonBeamV, 0);
            this.Controls.SetChildIndex(this.button_Start, 0);
            this.Controls.SetChildIndex(this.buttonMap, 0);
            this.Controls.SetChildIndex(this.buttonOthers, 0);
            this.Controls.SetChildIndex(this.buttonForFocusWithTabIndex0, 0);
            this.Controls.SetChildIndex(this.label0, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonHVD;
        private System.Windows.Forms.Button buttonXYZ;
        private System.Windows.Forms.Button buttonOffset;
        private System.Windows.Forms.Button buttonCCS;
        private System.Windows.Forms.Button buttonSU;
        private System.Windows.Forms.Button buttonPhys;
        private System.Windows.Forms.Button button2Station;
        private System.Windows.Forms.Button buttonBeam;
        private System.Windows.Forms.Button buttonIncrease;
        private System.Windows.Forms.Button buttonDecrease;
        private System.Windows.Forms.Button buttonMMM;
        private System.Windows.Forms.Button buttonFindClosest;
        private System.Windows.Forms.Button button_Stop;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button buttonDispl;
        private System.Windows.Forms.Button buttonBeamV;
        private System.Windows.Forms.Button button_Start;
        private System.Windows.Forms.Button buttonMap;
        private System.Windows.Forms.Button buttonOthers;
        private System.Windows.Forms.Label label0;
    }
}
