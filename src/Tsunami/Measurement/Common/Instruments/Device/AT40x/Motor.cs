﻿using EmScon;
using System;
using System.Windows.Forms;
using System.Xml.Serialization;
using TSU.Common.Compute;
using TSU.ENUM;
using TSU.Tools;
using TSU.Views.Message;
using M = TSU.Common.Measures;
using P = TSU.Views.Message.ProgressMessage;
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments.Device.AT40x
{
    #region Motor
    public class Motor
    {
        private readonly Module At40xModule;

        [XmlIgnore]
        public int at401SpeedH;

        [XmlIgnore]
        public int at401SpeedV;

        [XmlIgnore]
        public movingDirection moving;

        public Motor(Module at40x)
        {
            At40xModule = at40x;
        }

        /// <summary>
        /// use "Left", Right, Up or Down to move, use it several time to increase speed
        /// </summary>
        /// <param name="direction"></param>
        internal void MovebyDirection(movingDirection direction)
        {
            try
            {
                PictureBox pb = At40xModule.View.jogView.middlePictureBox;
                switch (direction)
                {
                    case movingDirection.Up:
                        if (moving == movingDirection.None)
                        {
                            moving = movingDirection.Up;
                            pb.BackgroundImage = R.At40x_moveup;
                        }
                        if (moving == movingDirection.Up)
                        {
                            MoveAt401(moving);
                        }
                        else
                        {
                            StopAt401();
                        }
                        break;

                    case movingDirection.Down:
                        if (moving == movingDirection.None)
                        {
                            moving = movingDirection.Down;
                            pb.BackgroundImage = R.At40x_movedown;
                        }
                        if (moving == movingDirection.Down)
                        {
                            MoveAt401(moving);
                        }
                        else 
                        { 
                            StopAt401(); 
                        }
                        break;

                    case movingDirection.Left:
                        if (moving == movingDirection.None)
                        {
                            moving = movingDirection.Left;
                            pb.BackgroundImage = R.At40x_moveleft;
                        }
                        if (moving == movingDirection.Left)
                        {
                            MoveAt401(moving);
                        }
                        else
                        {
                            StopAt401();
                        }
                        break;

                    case movingDirection.Right:
                        if (moving == movingDirection.None)
                        {
                            moving = movingDirection.Right;
                            pb.BackgroundImage = R.At40x_moveright;
                        }
                        if (moving == movingDirection.Right)
                        {
                            MoveAt401(moving);
                        }
                        else
                        {
                            StopAt401();
                        }
                        break;

                    default:
                        StopAt401();
                        break;
                }
                At40xModule.View.jogView.MovebyDirection(direction);
            }
            catch (Exception ex)
            {
                Logs.Log.AddEntry(this.At40xModule, ex);
            }
        }

        public static movingDirection GetDirectionFromText(string arrowKeyText)
        {
            switch (arrowKeyText.ToUpper())
            {
                case "UP":
                    return movingDirection.Up;
                case "DOWN":
                    return movingDirection.Down;
                case "LEFT":
                    return movingDirection.Left;
                case "RIGHT":
                    return movingDirection.Right;
                default:
                    return movingDirection.None;
            }
        }

        internal void StopAt401()
        {
            this.At40xModule.async.RemoveFromInProgressListIfExist(ES_Command.ES_C_MoveHV);
            Debug.WriteInConsole("at40x-Motor-StopMove");
            At40xModule.sync.StopMove();
            at401SpeedH = 0;
            at401SpeedV = 0;
            moving = movingDirection.None;
            At40xModule.View.jogView.Stop();
            At40xModule._status.isTrackerLockedAndReady = false;

            At40xModule._status.isTrackerBusy = false;
            At40xModule._circle.Get(); // to make sure  taht if we went on the other side by jog taht the status refelct it right.
            At40xModule.View.UpdateSystemStatusVisual();
        }

        internal void StopIfMoving()
        {
            if (moving != movingDirection.None)
                StopAt401();
        }

        /// <summary>
        /// jog by direction optionnnaly duuring a period of time in millis and with a max speed 0--100
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="durationInMillis"></param>
        /// <param name="maxSpeed"></param>
        internal void MoveAt401(movingDirection direction, int durationInMillis = 0, int maxSpeed = 100, int step = 10)
        {
            try
            {
                At40xModule._status.isTrackerBusy = true;
                At40xModule._target.TryingToLock = false;

                int lDeltaSpeedH = 0;
                int lDeltaSpeedV = 0;


                this.moving = direction;


                switch (direction)
                {
                    case movingDirection.Right:
                        if (at401SpeedH >= 0)
                        {
                            lDeltaSpeedH = step;
                        }
                        else
                        {
                            lDeltaSpeedH = -at401SpeedH;
                        }
                        break;
                    case movingDirection.Left:
                        if (at401SpeedH <= 0)
                        {
                            lDeltaSpeedH = -step;
                        }
                        else
                        {
                            lDeltaSpeedH = -at401SpeedH;
                        }
                        break;
                    case movingDirection.Up:
                        if (at401SpeedV <= 0)
                        {
                            lDeltaSpeedV = -step;
                        }
                        else
                        {
                            lDeltaSpeedV = -at401SpeedV;
                        }
                        break;
                    case movingDirection.Down:
                        if (at401SpeedV >= 0)
                        {
                            lDeltaSpeedV = step;
                        }
                        else
                        {
                            lDeltaSpeedV = -at401SpeedV;
                        }
                        break;
                    default:
                        lDeltaSpeedH = -at401SpeedH;
                        lDeltaSpeedV = -at401SpeedV;
                        break;
                }

                if (this.At40xModule._circle._selected == ES_TrackerFace.ES_TF_Face2)
                {
                    lDeltaSpeedV = -lDeltaSpeedV;
                    if (this.At40xModule._camera.isOn)
                    {
                        lDeltaSpeedH = -lDeltaSpeedH;
                        lDeltaSpeedV = -lDeltaSpeedV;
                    }
                }

                at401SpeedH = StayInARange(at401SpeedH + lDeltaSpeedH, maxSpeed, -maxSpeed);
                at401SpeedV = StayInARange(at401SpeedV + lDeltaSpeedV, maxSpeed, -maxSpeed);

                Debug.WriteInConsole("at40x-Motor-MoveHV");
                if (at401SpeedH != 0)
                    Logs.Log.AddEntryAsBeginningOf(At40xModule, $"Jog H = {at401SpeedH}/100");
                if (at401SpeedV != 0)
                    Logs.Log.AddEntryAsBeginningOf(At40xModule, $"Jog V = {at401SpeedV}/100");
                At40xModule.async.MoveHV(at401SpeedH, at401SpeedV);
                if (durationInMillis > 0) 
                    StopMovingIn(durationInMillis);
            }
            catch (Exception ex)
            {
                Logs.Log.AddEntry(this.At40xModule, ex);
            }
        }

        // removed because in face 2 only vertical direction is inverted not the horizontal one
        //private void CorrectDirectionBasedOnFace(ref movingDirection direction, ES_TrackerFace face)
        //{
        //    if (face == ES_TrackerFace.ES_TF_Face2)
        //    {
        //        switch (direction)
        //        { 
        //            case movingDirection.Up:
        //                direction = movingDirection.Down;
        //                break;
        //            case movingDirection.Down:
        //                direction = movingDirection.Up;
        //                break;
        //            case movingDirection.Left:
        //                direction = movingDirection.Right;
        //                break;
        //            case movingDirection.Right:
        //                direction = movingDirection.Left;
        //                break;
        //            default:
        //            case movingDirection.None:
        //                break;
        //        }
        //    }
        //}

        Timer movingTimer;

        int timerDurationInMills;

        DateTime startingTime;

        private void StopMovingIn(int durationInMills)
        {
            timerDurationInMills = durationInMills;
            startingTime = DateTime.Now;
            movingTimer = new Timer();
            movingTimer.Tick += new EventHandler(CheckIfTimeToStopMoving);
            movingTimer.Interval = 200;
            movingTimer.Start();
        }

        private void CheckIfTimeToStopMoving(object sender, EventArgs e)
        {
            if ((DateTime.Now - startingTime).TotalMilliseconds > timerDurationInMills)
            {
                movingTimer.Stop();
                movingTimer.Enabled = false;
                StopAt401();
            }
        }

        public void MovebyAngle(movingDirection direction, double angle)
        {
            try
            {
                Debug.WriteInConsole($"at40x-Motor-MovebyAngle({direction},{angle}");

                if (Tsunami2.Properties != null)
                    Tsunami2.Properties.UpdateStatus("writting start timing ");
                At40xModule.timeSpentMeasuring.Add("sync PositionRelativeHV (unlock)", TimeSpent.type.Started, DateTime.Now);

                double h;
                double v;
                if (Tsunami2.Properties != null)
                    Tsunami2.Properties.UpdateStatus("PositionRelativeHV");
                switch (direction)
                {
                    case movingDirection.Right:
                        Debug.WriteInConsole("at40x-Motor-PositionRelativeHV (unlock)");
                        h = angle;
                        v = 0;
                        break;

                    case movingDirection.Left:
                        Debug.WriteInConsole("at40x-Motor-PositionRelativeHV");
                        h = -angle;
                        v = 0;
                        break;

                    case movingDirection.Up:
                        Debug.WriteInConsole("at40x-Motor-PositionRelativeHV");
                        h = 0;
                        v = -angle;
                        break;

                    case movingDirection.Down:
                        Debug.WriteInConsole("at40x-Motor-PositionRelativeHV");
                        h = 0;
                        v = angle;
                        break;

                    default:
                        h = 0;
                        v = angle;
                        break;
                }
                At40xModule.sync.PositionRelativeHV(h, v);
                Tsunami2.Properties?.UpdateStatus("writting finish timing ");
                At40xModule.timeSpentMeasuring.Add("sync PositionRelativeHV", TimeSpent.type.Stopped, DateTime.Now);
            }
            catch (Exception ex)
            {
                Logs.Log.AddEntry(this.At40xModule, ex);
            }
        }

        internal int StayInARange(int value, int upperLimit = 100, int lowerLimit = -100)
        {
            if (value > upperLimit)
            {
                value = upperLimit;
            }
            else if (value < lowerLimit)
            {
                value = lowerLimit;
            }
            return value;
        }

        internal void Goto()
        {
            Polar.Measure m = At40xModule._BeingMeasuredTheodoliteData;

            if (m == null)
            {
                m = At40xModule._ToBeMeasureTheodoliteData;
            }
            else
            {
                if (m._Status is M.States.Cancel) // it can happen if the point where in the live data
                    m = At40xModule._ToBeMeasureTheodoliteData;
            }

            if (m == null)
                throw new Exception($"{R.T_DONT_KNOW_WHERE_TO_GO_TO};{R.T_IS_A_POINT_SET_UP_FOR_MEASUREMENT}");

            if (m.HasCorrectedAngles)
                Goto(m);
            else
            {
                Polar.Station s = this.At40xModule.StationTheodolite;
                Elements.Point p = m._Point;

                if (s.Parameters2._IsSetup)
                    Goto(p, s, m.Extension.Value);
            }
            At40xModule.Ready();
        }
        internal void Goto(Polar.Measure m)
        {
            At40xModule.StatusChange($"{R.T_GOING_TO} {m._Point._Name} {R.T_IN} {m.Face}", P.StepTypes.Begin);
            try
            {
                TSU.HourGlass.Set();

                if (m.Angles.Corrected.Horizontal.Value == TSU.Tsunami2.Preferences.Values.na)
                    throw new Exception($"{R.T_GOTO_IMPOSSIBLE}.");

                if (At40xModule.sync != null)
                {

                    this.At40xModule.GetAngleForGoodFaceGoto(m, out TSU.Common.Elements.Angles a);

                    At40xModule.timeSpentMeasuring.Add("sync PointLaserHVD (goto)", TimeSpent.type.Started, DateTime.Now);

                    Debug.WriteInConsole("at40x-Motor-PointLaserHVD");

                    At40xModule._status.isTrackerBusy = true;

                    At40xModule.sync.PointLaserHVD(a.Horizontal.Value, a.Vertical.Value, 1);


                    At40xModule.timeSpentMeasuring.Add("sync PointLaserHVD (goto)", TimeSpent.type.Stopped, DateTime.Now);
                }
                At40xModule.StatusChange($"{R.T_WENT_TO} {m._Point._Name}", P.StepTypes.End);
            }
            catch (Exception ex)
            {

                if (ex.HResult == -2146233088) // "Could not get face;Invoking command \"Get Face\" [98] failed!" we have probably lost connection
                {
                    string titleAndMessage = $"{"No reaction"};{"At40x did not respond, are you connected? Maybe you lost the connection?"}";
                    new MessageInput(MessageType.Warning, titleAndMessage).Show();
                    return;
                }

                if (!this.At40xModule._measurement.IsSimulation)
                    throw new Exception($"{R.T_GOTO_FAILED}:" + ex.Message, ex);

                At40xModule.StatusChange($"{R.T_DIDNT_WENT_TO} {m._Point._Name}", P.StepTypes.End);
            }
            finally
            {
                TSU.HourGlass.Remove();

                At40xModule._status.isTrackerLockedAndReady = false;
                At40xModule._status.isTrackerBusy = false;
            }
        }

        internal void Goto(Elements.Point p, Polar.Station s, double ext)
        {
            try
            {
                At40xModule._status.isTrackerBusy = true;

                if (p == null)
                    throw new Exception($"{R.T_NO_POINT_DEFINED_FOR_THE_GOTO}");

                if (!p._Coordinates.HasAny)
                    throw new Exception($"Point '{p._Name}' {R.T_HAS_NO_COORDINATES}");

                At40xModule.StatusChange($"{R.T_GOING_TO} {p._Name}", P.StepTypes.Begin);

                Polar.Station.Parameters param = s.Parameters2;

                Debug.WriteInConsole("at40x-Motor-SetCoordinateSystemType");
                At40xModule.sync.SetCoordinateSystemType(ES_CoordinateSystemType.ES_CS_RHR);

                // ANGL
                double gisementStationPointVise = Survey.GetBearing(param._StationPoint, p);
                double h = gisementStationPointVise - param.vZero.Value;
                if (h > 400) h -= 400;
                if (h < 0) h += 400;

                // ZEND
                double v = Survey.GetVerticalAngle(param._StationPoint, p);


                Debug.WriteInConsole("at40x-Motor-PointLaserHVD");
                At40xModule.sync.PointLaserHVD(h, v, 1);
                Debug.WriteInConsole("at40x-Motor-SetCoordinateSystemType");
                At40xModule.sync.SetCoordinateSystemType(ES_CoordinateSystemType.ES_CS_SCW);



                At40xModule.StatusChange($"{R.T_WENT_TO} {p._Name}", P.StepTypes.End);
            }
            catch (Exception ex)
            {
                Logs.Log.AddEntry(this.At40xModule, ex);
            }
            finally
            {
                At40xModule._status.isTrackerLockedAndReady = false;
                At40xModule._status.isTrackerBusy = false;
            }
        }
        internal void PositionRelativeHV(double p1, double p2)
        {
            // this vt angle when moving up do no move up as much a the opposite value move down...lets multiply by 1.4
            if (p2 < 0)
                p2 *= 1.4;

            At40xModule._status.isTrackerBusy = true;
            Debug.WriteInConsole("at40x-Motor-PositionRelativeHV");
            At40xModule.sync.PositionRelativeHV(p1, p2);
            At40xModule._status.isTrackerBusy = false;
        }

        internal void OnMoveHVAnswer(object sender, EventArgs e)
        {
            Debug.WriteInConsole("at40x-Motor-OnMoveHVAnswer");
            // To prevent the /!\ ES_C_MoveHV Timed out in 60 sec. /!\
            this.At40xModule.async.RemoveFromInProgressListIfExist(ES_Command.ES_C_MoveHV);
        }
    }





    #endregion
}
