﻿using System;
using System.Linq;
using System.Windows.Forms;
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments.Device.AT40x
{
    partial class JogView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JogView));
            this.panelJog = new TSU.Views.TsuPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.middlePictureBox = new System.Windows.Forms.PictureBox();
            this.buttonPlusLeft = new System.Windows.Forms.Button();
            this.buttonPlusRight = new System.Windows.Forms.Button();
            this.buttonPlusUp = new System.Windows.Forms.Button();
            this.buttonPlusDown = new System.Windows.Forms.Button();
            this.buttonPlusStop = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonUp = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDown = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonLeft = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRight = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonStop = new System.Windows.Forms.ToolStripButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panelJog.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.middlePictureBox)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelJog
            // 
            this.panelJog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.panelJog.Controls.Add(this.tableLayoutPanel1);
            this.panelJog.Controls.Add(this.toolStrip2);
            this.panelJog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelJog.Location = new System.Drawing.Point(5, 5);
            this.panelJog.Name = "panelJog";
            this.panelJog.Size = new System.Drawing.Size(612, 350);
            this.panelJog.TabIndex = 18;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableLayoutPanel1.Controls.Add(this.middlePictureBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.buttonPlusLeft, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.buttonPlusRight, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.buttonPlusUp, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonPlusDown, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.buttonPlusStop, 2, 2);

            Panel p = new Panel();
            p.Controls.Add(this.textBox1);
            p.Controls.Add(this.textBox2);
            this.tableLayoutPanel1.Controls.Add(p, 0, 0);

            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35.03185F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 29.93631F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(612, 285);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // middlePictureBox
            // 
            this.middlePictureBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.middlePictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("middlePictureBox.BackgroundImage")));
            this.middlePictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.middlePictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.middlePictureBox.Location = new System.Drawing.Point(204, 103);
            this.middlePictureBox.Name = "middlePictureBox";
            this.middlePictureBox.Size = new System.Drawing.Size(195, 80);
            this.middlePictureBox.TabIndex = 17;
            this.middlePictureBox.TabStop = false;
            this.middlePictureBox.Tag = "STOP";
            this.middlePictureBox.Click += new System.EventHandler(this.MoveButton_Click);
            // 
            // buttonPlusLeft
            // 
            this.buttonPlusLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonPlusLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPlusLeft.Image = global::TSU.Properties.Resources.At40x_moveleft;
            this.buttonPlusLeft.Location = new System.Drawing.Point(3, 103);
            this.buttonPlusLeft.Name = "buttonPlusLeft";
            this.buttonPlusLeft.Size = new System.Drawing.Size(195, 80);
            this.buttonPlusLeft.TabIndex = 18;
            this.buttonPlusLeft.TabStop = false;
            this.buttonPlusLeft.Tag = "LEFT";
            this.buttonPlusLeft.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.buttonPlusLeft, "Move left or stop if already moving right");
            this.buttonPlusLeft.UseVisualStyleBackColor = true;
            this.buttonPlusLeft.Click += new System.EventHandler(this.MoveButton_Click);
            // 
            // buttonPlusRight
            // 
            this.buttonPlusRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonPlusRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPlusRight.Image = global::TSU.Properties.Resources.At40x_moveright;
            this.buttonPlusRight.Location = new System.Drawing.Point(405, 103);
            this.buttonPlusRight.Name = "buttonPlusRight";
            this.buttonPlusRight.Size = new System.Drawing.Size(204, 80);
            this.buttonPlusRight.TabIndex = 19;
            this.buttonPlusRight.TabStop = false;
            this.buttonPlusRight.Tag = "RIGHT";
            this.buttonPlusRight.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.buttonPlusRight.UseVisualStyleBackColor = true;
            this.buttonPlusRight.Click += new System.EventHandler(this.MoveButton_Click);
            // 
            // buttonPlusUp
            // 
            this.buttonPlusUp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonPlusUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPlusUp.Image = global::TSU.Properties.Resources.At40x_moveup;
            this.buttonPlusUp.Location = new System.Drawing.Point(204, 3);
            this.buttonPlusUp.Name = "buttonPlusUp";
            this.buttonPlusUp.Size = new System.Drawing.Size(195, 94);
            this.buttonPlusUp.TabIndex = 20;
            this.buttonPlusUp.TabStop = false;
            this.buttonPlusUp.Tag = "UP";
            this.buttonPlusUp.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonPlusUp.UseVisualStyleBackColor = true;
            this.buttonPlusUp.Click += new System.EventHandler(this.MoveButton_Click);
            // 
            // buttonPlusDown
            // 
            this.buttonPlusDown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonPlusDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPlusDown.Image = global::TSU.Properties.Resources.At40x_movedown;
            this.buttonPlusDown.Location = new System.Drawing.Point(204, 189);
            this.buttonPlusDown.Name = "buttonPlusDown";
            this.buttonPlusDown.Size = new System.Drawing.Size(195, 93);
            this.buttonPlusDown.TabIndex = 21;
            this.buttonPlusDown.TabStop = false;
            this.buttonPlusDown.Tag = "DOWN";
            this.buttonPlusDown.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonPlusDown.UseVisualStyleBackColor = true;
            this.buttonPlusDown.Click += new System.EventHandler(this.MoveButton_Click);
            // 
            // buttonPlusStop
            // 
            this.buttonPlusStop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonPlusStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPlusStop.Image = global::TSU.Properties.Resources.At40x_moveNot;
            this.buttonPlusStop.Location = new System.Drawing.Point(405, 189);
            this.buttonPlusStop.Name = "buttonPlusStop";
            this.buttonPlusStop.Size = new System.Drawing.Size(204, 93);
            this.buttonPlusStop.TabIndex = 22;
            this.buttonPlusStop.TabStop = false;
            this.buttonPlusStop.Tag = "STOP";
            this.buttonPlusStop.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonPlusStop.UseVisualStyleBackColor = true;
            this.buttonPlusStop.Click += new System.EventHandler(this.MoveButton_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.textBox1.ForeColor = System.Drawing.Color.Snow;
            this.textBox1.Location = new System.Drawing.Point(3, 3);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(76, 20);
            this.textBox1.TabIndex = 23;
            this.textBox1.Text = "Enter here";
            this.toolTip1.SetToolTip(this.textBox1, "Enter here to be able to jog with the keyboard");
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.On_TextBox1_KeyDown);
            // textBox1
            // 
            this.textBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.textBox2.ForeColor = System.Drawing.Color.Snow;
            this.textBox2.Location = new System.Drawing.Point(3, 23);
            this.textBox2.Name = "textBox2";
            this.textBox2.Text = "Move By gon";
            this.textBox2.Size = new System.Drawing.Size(76, 20); 
            this.toolTip1.SetToolTip(this.textBox2, "Enter a value in gon to move by known step");
            this.textBox2.TabIndex = 24;
            // toolStrip2
            // 
            this.toolStrip2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonUp,
            this.toolStripButtonDown,
            this.toolStripButtonLeft,
            this.toolStripButtonRight,
            this.toolStripButtonStop});
            this.toolStrip2.Location = new System.Drawing.Point(0, 285);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Padding = new System.Windows.Forms.Padding(1);
            this.toolStrip2.Size = new System.Drawing.Size(612, 65);
            this.toolStrip2.TabIndex = 15;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripButtonUp
            // 
            this.toolStripButtonUp.AutoSize = false;
            this.toolStripButtonUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.toolStripButtonUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStripButtonUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonUp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonUp.Image")));
            this.toolStripButtonUp.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonUp.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.toolStripButtonUp.Name = "toolStripButtonUp";
            this.toolStripButtonUp.Size = new System.Drawing.Size(60, 60);
            this.toolStripButtonUp.Tag = "UP";
            this.toolStripButtonUp.Text = "toolStripButtonUp";
            this.toolStripButtonUp.ToolTipText = "Move up (several clicks increase the speed)";
            this.toolStripButtonUp.Click += new System.EventHandler(this.MoveButton_Click);
            // 
            // toolStripButtonDown
            // 
            this.toolStripButtonDown.AutoSize = false;
            this.toolStripButtonDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.toolStripButtonDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStripButtonDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonDown.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDown.Image")));
            this.toolStripButtonDown.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonDown.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.toolStripButtonDown.Name = "toolStripButtonDown";
            this.toolStripButtonDown.Size = new System.Drawing.Size(60, 60);
            this.toolStripButtonDown.Tag = "DOWN";
            this.toolStripButtonDown.Text = "toolStripButtonDown";
            this.toolStripButtonDown.ToolTipText = "Move Down";
            this.toolStripButtonDown.Click += new System.EventHandler(this.MoveButton_Click);
            // 
            // toolStripButtonLeft
            // 
            this.toolStripButtonLeft.AutoSize = false;
            this.toolStripButtonLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.toolStripButtonLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStripButtonLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonLeft.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonLeft.Image")));
            this.toolStripButtonLeft.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonLeft.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.toolStripButtonLeft.Name = "toolStripButtonLeft";
            this.toolStripButtonLeft.Size = new System.Drawing.Size(60, 60);
            this.toolStripButtonLeft.Tag = "LEFT";
            this.toolStripButtonLeft.Text = "toolStripButtonLeft";
            this.toolStripButtonLeft.ToolTipText = "Move left";
            this.toolStripButtonLeft.Click += new System.EventHandler(this.MoveButton_Click);
            // 
            // toolStripButtonRight
            // 
            this.toolStripButtonRight.AutoSize = false;
            this.toolStripButtonRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.toolStripButtonRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStripButtonRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRight.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRight.Image")));
            this.toolStripButtonRight.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonRight.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.toolStripButtonRight.Name = "toolStripButtonRight";
            this.toolStripButtonRight.Size = new System.Drawing.Size(60, 60);
            this.toolStripButtonRight.Tag = "RIGHT";
            this.toolStripButtonRight.Text = "toolStripButtonRight";
            this.toolStripButtonRight.ToolTipText = "Move right";
            this.toolStripButtonRight.Click += new System.EventHandler(this.MoveButton_Click);
            // 
            // toolStripButtonStop
            // 
            this.toolStripButtonStop.AutoSize = false;
            this.toolStripButtonStop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.toolStripButtonStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStripButtonStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonStop.Image = global::TSU.Properties.Resources.At40x_moveNot;
            this.toolStripButtonStop.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonStop.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.toolStripButtonStop.Name = "toolStripButtonStop";
            this.toolStripButtonStop.Size = new System.Drawing.Size(60, 60);
            this.toolStripButtonStop.Tag = "STOP";
            this.toolStripButtonStop.Text = "toolStripButtonRight";
            this.toolStripButtonStop.ToolTipText = "Stop any move";
            this.toolStripButtonStop.Click += new System.EventHandler(this.MoveButton_Click);
            // 
            // JogView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 360);
            this.Controls.Add(this.panelJog);
            this.Name = "JogView";
            this.Controls.SetChildIndex(this.panelJog, 0);
            this.Controls.SetChildIndex(this.buttonForFocusWithTabIndex0, 0);
            this.panelJog.ResumeLayout(false);
            this.panelJog.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.middlePictureBox)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);

        }


        #endregion

        private Views.TsuPanel panelJog;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolStripButtonDown;
        private System.Windows.Forms.ToolStripButton toolStripButtonLeft;
        private System.Windows.Forms.ToolStripButton toolStripButtonStop;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.PictureBox middlePictureBox;
        private System.Windows.Forms.Button buttonPlusLeft;
        private System.Windows.Forms.Button buttonPlusRight;
        private System.Windows.Forms.Button buttonPlusUp;
        private System.Windows.Forms.Button buttonPlusDown;
        private System.Windows.Forms.Button buttonPlusStop;
        private System.Windows.Forms.ToolStripButton toolStripButtonUp;
        private System.Windows.Forms.ToolStripButton toolStripButtonRight;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
