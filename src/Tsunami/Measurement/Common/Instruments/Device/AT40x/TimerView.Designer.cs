﻿using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments.Device.AT40x
{
    partial class TimerView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTimer = new TSU.Views.TsuPanel();
            this.buttonTimer = new System.Windows.Forms.Button();
            this.datePicker = new System.Windows.Forms.DateTimePicker();
            this.timePicker = new System.Windows.Forms.DateTimePicker();
            this.panelTimer.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTimer
            // 
            this.panelTimer.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.panelTimer.Controls.Add(this.buttonTimer);
            this.panelTimer.Controls.Add(this.datePicker);
            this.panelTimer.Controls.Add(this.timePicker);
            this.panelTimer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTimer.Location = new System.Drawing.Point(5, 5);
            this.panelTimer.Name = "panelTimer";
            this.panelTimer.Size = new System.Drawing.Size(570, 344);
            this.panelTimer.TabIndex = 21;
            // 
            // buttonTimer
            // 
            this.buttonTimer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonTimer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonTimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTimer.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.buttonTimer.Image =R.At40x_Timer;
            this.buttonTimer.Location = new System.Drawing.Point(17, 131);
            this.buttonTimer.Name = "buttonTimer";
            this.buttonTimer.Size = new System.Drawing.Size(538, 201);
            this.buttonTimer.TabIndex = 2;
            this.buttonTimer.UseVisualStyleBackColor = true;
            this.buttonTimer.Click += new System.EventHandler(this.buttonTimer_Click);
            // 
            // datePicker
            // 
            this.datePicker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.datePicker.CalendarMonthBackground = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.datePicker.CalendarTitleForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.datePicker.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.datePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker.Location = new System.Drawing.Point(3, 11);
            this.datePicker.Name = "datePicker";
            this.datePicker.Size = new System.Drawing.Size(564, 49);
            this.datePicker.TabIndex = 1;
            // 
            // timePicker
            // 
            this.timePicker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.timePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timePicker.Location = new System.Drawing.Point(3, 69);
            this.timePicker.Name = "timePicker";
            this.timePicker.ShowUpDown = true;
            this.timePicker.Size = new System.Drawing.Size(564, 49);
            this.timePicker.TabIndex = 0;
            // 
            // TimerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 354);
            this.Controls.Add(this.panelTimer);
            this.Name = "TimerView";
            this.panelTimer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Views.TsuPanel panelTimer;
        private System.Windows.Forms.Button buttonTimer;
        private System.Windows.Forms.DateTimePicker datePicker;
        private System.Windows.Forms.DateTimePicker timePicker;

    }
}
