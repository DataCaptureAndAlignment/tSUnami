﻿using EmScon;
using System;
using System.Net.Sockets;
using System.Threading;

namespace TSU.Common.Instruments.Device.AT40x
{
    internal class ReceiverThread
    {
        readonly Module _at40x;

        Thread mainThread;

        public ReceiverThread(Module at40x)
        {
            _at40x = at40x;
        }

        public void Start()
        {
            mainThread = new Thread(Receiver);
            mainThread.IsBackground = true;
            mainThread.Name = $"ReceiverThread for module AT40X {_at40x.Guid}";
            mainThread.Start();
        }

        public void Abort()
        {
            mainThread?.Abort();
            mainThread = null;
        }

        private void Receiver()
        {
            try
            {
                byte[] bytes = new byte[128 * 1024];

                CESCSAPIReceive.PacketHeaderT header = new CESCSAPIReceive.PacketHeaderT();

                int headSize = System.Runtime.InteropServices.Marshal.SizeOf(header);

                while (true)
                {
                    int nCounter = 0;
                    int nMissing = 0;
                    int nBytesRead = 0;
                    int nBytesReadTotal = 0;

                    try
                    {
                        // Just Peek the header size
                        int nPeekRead = _at40x.socket.Receive(bytes, 0, headSize, SocketFlags.Peek);

                        if (nPeekRead == headSize)
                        {
                            _at40x.receiver.GetPacketHeader(bytes, out header);

                            do
                            {
                                nCounter++;

                                if (nBytesRead > 0)
                                    nBytesReadTotal += nBytesRead;

                                nMissing = header.lPacketSize - nBytesReadTotal;

                                nBytesRead = _at40x.socket.Receive(bytes, nBytesReadTotal, nMissing, SocketFlags.None);

                                if (nBytesRead < 0)
                                    continue; // error

                                if (nCounter > 64) // emergency exit
                                {
                                    if (nBytesReadTotal <= 0)
                                        continue; // nothing read yet - we can leave safely
                                    else
                                        break;
                                } // if
                            } while (nBytesRead < nMissing);

                            if (nBytesRead > 0)
                                nBytesReadTotal += nBytesRead;

                            if (nBytesReadTotal == header.lPacketSize)
                            {
                                WorkerData wd = new WorkerData(_at40x, bytes, nBytesReadTotal);
                                ThreadPool.QueueUserWorkItem(WorkerMethod, wd);
                            }
                        }
                    }
                    catch (SocketException)
                    {
                        //  Will try to reconnect to the socket, but only once
                        _at40x.SocketReConnect();
                    }
                }
            }
            catch (ThreadAbortException)
            {
                Debug.WriteInConsole($"Receiver thread has been aborted.");
            }
            catch (Exception e)
            {
                Debug.WriteInConsole($"Receive failed, Exception is {e.GetType()} {e}");
            }
        }

        private static void WorkerMethod(object state)
        {
            WorkerData wd = (WorkerData)state;

            try
            {
                string formattedBytes = BitConverter.ToString(wd.Bytes, 0, Math.Min(wd.NBytesReadTotal, 20));
                Debug.WriteInConsole($"WorkerMethod Beginning treatment of a {wd.NBytesReadTotal} byte message beginning with {formattedBytes}");
                wd.At40x.receiver.ReceiveData(wd.Bytes, wd.NBytesReadTotal);
            }
            catch (Exception e)
            {
                Logs.Log.AddEntryAsError(wd.At40x, "Error in the treatment of the response : " + e.Message);
            }
            finally
            {
                Debug.WriteInConsole($"WorkerMethod End treatment");
            }

        }

        private class WorkerData
        {
            public WorkerData(Module at40x, byte[] bytes, int nBytesReadTotal)
            {
                At40x = at40x;
                Bytes = (byte[])bytes.Clone();
                NBytesReadTotal = nBytesReadTotal;
            }

            public byte[] Bytes { get; }
            public int NBytesReadTotal { get; }
            public Module At40x { get; }
        }
    }
}
