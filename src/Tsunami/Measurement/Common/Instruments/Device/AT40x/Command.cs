﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TSU.Common.Instruments.Device.AT40x
{
    internal class Command : EmScon.CESCSAPICommand
    {
        private readonly Module module;

        public Command(Module m)
        {
            module = m;
        }

        public override bool SendPacket(byte[] packetAddress, int packetSize)
        {
            try
            {
                try
                {
                    module.socket.Send(packetAddress, 0, packetSize, SocketFlags.None);
                }
                // Try to reconnect, but only one time
                catch (SocketException)
                {
                    module.SocketReConnect();
                    module.socket.Send(packetAddress, 0, packetSize, SocketFlags.None);
                }
            }
            catch (Exception ex)
            {
                Logs.Log.AddEntryAsPayAttentionOf(module, $"Packet not sent with {packetSize} bytes: {ex.GetType()} {ex.Message}");
            }
            return true;
        }
    }
}
