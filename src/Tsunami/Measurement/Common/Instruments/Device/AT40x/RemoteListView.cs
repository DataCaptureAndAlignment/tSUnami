﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;
using RR = TSU.Common.Instruments.Device.AT40x.Remote;
using TSU.Tools;
using TSU.Common.Instruments.Device.AT40x;

namespace TSU.Common.Instruments.Device.AT40x
{
    public partial class RemoteListView : SubView
    {

        internal Remote Remote
        {
            get
            {
                return (this.ParentView as AT40x.View).Module._remote;
            }
        }

        public RemoteListView()
        {
            InitializeComponent();
        }

        public RemoteListView(AT40x.View parentView)
            : base(parentView)
        {
            this._Name = "Remote panel";
            InitializeComponent();
            this.ParentView = parentView;
            Update(this.Remote.CurrentCommandFromList);
        }



        internal override void Start()
        {
            base.Start(); // will put the view in the subviewPane

            this.Remote.CurrentStrategy = RR.RemoteStrategies.ListCommand;
        }

        internal void RemoteButtonPressed(RR.RemoteButtonList b, RR.RemoteButtonState s)
        {

            //ShowABigLabel(b, s);
            //return;

            // view
            Bitmap bm;
            switch (s)
            {
                case RR.RemoteButtonState.Down: bm = R.At40x_ProbeOn; break;
                default: bm = R.At40x_ProbeOff; break;
            }
        }

        internal void Update(RR.CommandsList command)
        {
            if(InvokeRequired)
            {
                Invoke(new Action(() => {Update(command); }));
                return;
            }

            this.labelProbeA.Text = $"A = {command}";
            this.labelProbeB.Text = $"B = {R.T_NEXT}";
            this.labelProbeC.Text = $"C = {R.T_PREVIOUS}";
            this.labelProbeD.Text = $"D = {command.Next()}";
        }

    }
}
