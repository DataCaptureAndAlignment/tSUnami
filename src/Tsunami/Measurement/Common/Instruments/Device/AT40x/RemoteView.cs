﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;
using RR = TSU.Common.Instruments.Device.AT40x.Remote;


namespace TSU.Common.Instruments.Device.AT40x
{
    public partial class RemoteView : SubView
    {
        #region Refs

        internal Remote Remote
        {
            get
            {
                return (this.ParentView as AT40x.View).Module._remote;
            }
        }

        #endregion

        #region Constructor

        public RemoteView()
        {
            InitializeComponent();
            this.ApplyThemeColor();
        }

        private void ApplyThemeColor()
        {
            this.labelTitle.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.labelTitle.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.Bad;
            this.labelProbeD.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.labelProbeD.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.labelProbeC.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.labelProbeC.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.labelProbeB.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.labelProbeB.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.labelProbeB.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.labelProbeB.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.labelProbeA.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.labelProbeA.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.pictureBoxC.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.pictureBoxB.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.pictureBoxA.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.pictureBoxD.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.labelTips.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.labelTips.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.Bad;
            this.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
        }

        public RemoteView(AT40x.View parentView)
            : base(parentView)
        {
            this._Name = "Remote panel";
            InitializeComponent();
            this.ParentView = parentView;
            this.labelTips.Text = "Click on the button to define different actions";
            this.ChangeTexts();
        }

        #endregion

        #region Events

        private void panelProbe_Resize(object sender, EventArgs e)
        {
            int disponibleV = panelProbe.Height;
            int disponibleH = panelProbe.Width;
            int dispo = Math.Min(disponibleV, disponibleH);
            int dispoPar3 = (dispo) / 3;
            dispoPar3 = dispoPar3 < 5 ? 5 : dispoPar3;

            bool searchGoodFontSize = true;
            while (searchGoodFontSize)
            {
                // font size
                Font f = new Font(TSU.Tsunami2.Preferences.Theme.Fonts.fontNameNice, dispoPar3 / 5, FontStyle.Bold);
                labelProbeA.Font = f; labelProbeB.Font = f; labelProbeC.Font = f; labelProbeD.Font = f;

                // font position
                int halfWidth, halfHeight;
                halfWidth = labelProbeA.Width / 2; halfHeight = labelProbeA.Height / 2;
                labelProbeA.Location = new Point(disponibleH / 2 - halfWidth, disponibleV / 4 - halfHeight);

                halfWidth = labelProbeB.Width / 2; halfHeight = labelProbeB.Height / 2;
                labelProbeB.Location = new Point(disponibleH / 4 * 3 - halfWidth, disponibleV / 2 - halfHeight);

                halfWidth = labelProbeC.Width / 2; halfHeight = labelProbeC.Height / 2;
                labelProbeC.Location = new Point(disponibleH / 4 - halfWidth, disponibleV / 2 - halfHeight);

                halfWidth = labelProbeD.Width / 2; halfHeight = labelProbeD.Height / 2;
                labelProbeD.Location = new Point(disponibleH / 2 - halfWidth, disponibleV / 4 * 3 - halfHeight);

                if (labelProbeB.Width + labelProbeC.Width < disponibleH)
                    searchGoodFontSize = false;
                else
                    dispoPar3 = dispoPar3 - 5;
            }
            // Circle size
            int diameter = dispo / 3;
            pictureBoxA.Height = diameter; pictureBoxA.Width = diameter;
            pictureBoxB.Height = diameter; pictureBoxB.Width = diameter;
            pictureBoxC.Height = diameter; pictureBoxC.Width = diameter;
            pictureBoxD.Height = diameter; pictureBoxD.Width = diameter;

            // Circle position
            int radius = diameter / 2;
            pictureBoxA.Location = new Point(disponibleH / 2 - radius, disponibleV / 4 - radius);
            pictureBoxB.Location = new Point(disponibleH / 4 * 3 - radius, disponibleV / 2 - radius);
            pictureBoxC.Location = new Point(disponibleH / 4 - radius, disponibleV / 2 - radius);
            pictureBoxD.Location = new Point(disponibleH / 2 - radius, disponibleV / 4 * 3 - radius);

           
        }
        private void pictureBoxA_MouseDown(object sender, MouseEventArgs e) { On_RemoteButtonPressed(RR.RemoteButtonList.A, RR.RemoteButtonState.Down); }
        private void pictureBoxA_MouseUp(object sender, MouseEventArgs e) { On_RemoteButtonPressed(RR.RemoteButtonList.A, RR.RemoteButtonState.Up); }
        private void pictureBoxC_MouseDown(object sender, MouseEventArgs e) { On_RemoteButtonPressed(RR.RemoteButtonList.C, RR.RemoteButtonState.Down); }
        private void pictureBoxC_MouseUp(object sender, MouseEventArgs e) { On_RemoteButtonPressed(RR.RemoteButtonList.C, RR.RemoteButtonState.Up); }
        private void pictureBoxD_MouseDown(object sender, MouseEventArgs e) { On_RemoteButtonPressed(RR.RemoteButtonList.D, RR.RemoteButtonState.Down); }
        private void pictureBoxD_MouseUp(object sender, MouseEventArgs e) { On_RemoteButtonPressed(RR.RemoteButtonList.D, RR.RemoteButtonState.Up); }
        private void pictureBoxB_MouseDown(object sender, MouseEventArgs e) { On_RemoteButtonPressed(RR.RemoteButtonList.B, RR.RemoteButtonState.Down); }
        private void pictureBoxB_MouseUp(object sender, MouseEventArgs e) { On_RemoteButtonPressed(RR.RemoteButtonList.B, RR.RemoteButtonState.Up); }

        #endregion

        #region Actions

        public void ChangeTexts()
        {
            List<string> l = Remote.GetText();
            labelTitle.Text = l[0];
            labelProbeA.Text = l[1];
            labelProbeB.Text = l[2];
            labelProbeC.Text = l[3];
            labelProbeD.Text = l[4];
            
            panelProbe_Resize(this, null);
        }

        public  void ChangeTitle(string title)
        {
            labelTitle.Text = title;
        }
        
        internal override void Start()
        {
            base.Start(); // will put the view in the subviewPanel

            // set the button the 4 predefined strategy
            this.Remote.CurrentStrategy = RR.RemoteStrategies.PredefinedCommand;

        }

        internal void On_RemoteButtonPressed(RR.RemoteButtonList b, RR.RemoteButtonState s)
        {
            // view
            Bitmap bm;
            switch (s)
            {
                case RR.RemoteButtonState.Down: bm = R.At40x_ProbeOn; break;
                default: bm = R.At40x_ProbeOff; break;
            }
            switch (b)
            {
                case RR.RemoteButtonList.A: pictureBoxA.BackgroundImage = bm;   break;
                case RR.RemoteButtonList.B: pictureBoxB.BackgroundImage = bm;   break;
                case RR.RemoteButtonList.C: pictureBoxC.BackgroundImage = bm;   break;
                case RR.RemoteButtonList.D: pictureBoxD.BackgroundImage = bm;   break;
                default:break;
            }
        }

        void AssignCommand(ref RR.CommandsList button)
        {
            // assign a command
            BindingSource bs = new BindingSource();
            bs.DataSource = Enum.GetValues(typeof(RR.CommandsList));
            object choice = ShowMessageOfPredefinedInput(
                $"{"Choose a command"};{"Choose a command to execute when this button is pressed"}",
                R.T_OK,
                R.T_CANCEL,
                bs);
            if (choice is RR.CommandsList rcl)
                button = rcl;
            ChangeTexts();
        }

        private void pictureBoxA_Click(object sender, EventArgs e)
        {
            AssignCommand(ref this.Remote.ButtonA1Command);
        }

        private void pictureBoxD_Click(object sender, EventArgs e)
        {
            AssignCommand(ref this.Remote.ButtonD4Command);
        }

        private void pictureBoxB_Click(object sender, EventArgs e)
        {
            AssignCommand(ref this.Remote.ButtonB3Command);
        }

        private void pictureBoxC_Click(object sender, EventArgs e)
        {
            AssignCommand(ref this.Remote.ButtonC2Command);
        }
        #endregion
    }
}

