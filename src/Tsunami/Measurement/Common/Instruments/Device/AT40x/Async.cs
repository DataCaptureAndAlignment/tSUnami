﻿using EmScon;
using System;
using System.Collections.Generic;
using System.Threading;
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments.Device.AT40x
{
    public class Async
    {
        private readonly Module Module;

        public Async(Module module)
        {
            Module = module;
        }

        internal void EndAsyncCall(ES_Command command)
        {
            if (command == ES_Command.ES_C_StartMeasurement)
                this.Module._status.isMeasuring = false;

            // remove this command from the list of in progress commands
            this.RemoveFromInProgressListIfExist(command);
        }

        internal Dictionary<ES_Command, Timer> InProgressActions = new Dictionary<ES_Command, Timer>();

        public event EventHandler AsyncCommandListHasBeenCleared;

        public bool RemoveFromInProgressListIfExist(ES_Command action)
        {
            bool somethingRemoved = false;
            if (InProgressActions.TryGetValue(action, out Timer timer))
            {
                // Destroy the timer, so we don't get a timeout message when launching the same action repeatedly
                timer?.Dispose();
                InProgressActions.Remove(action);
                somethingRemoved = true;
            }

            if (InProgressActions.Count == 0)
                AsyncCommandListHasBeenCleared?.Invoke(this, new EventArgs());

            return somethingRemoved;
        }

        public Timer CreateTimeOutTimer(ES_Command action, int millis)
        {
            if (millis == 0)
                return null;

            Debug.WriteInConsole($"AT40x.Async.CreateTimeOutTimer({action},{millis})");

            return new Timer(Elapsed, new TimeoutState(action, millis), millis, Timeout.Infinite);
        }

        private class TimeoutState
        {
            public ES_Command action;
            public int millis;

            public TimeoutState(ES_Command action, int millis)
            {
                this.action = action;
                this.millis = millis;
            }
        }

        private void Elapsed(object state)
        {
            TimeoutState timeoutState = (TimeoutState)state;

            Debug.WriteInConsole($"AT40x.Async.Elapsed({timeoutState.action},{timeoutState.millis})");

            if (RemoveFromInProgressListIfExist(timeoutState.action))
            {
                bool liveDataIsRunningWell = Module.LiveData.IsOn && Module.LiveData.MeasureCount > 2;
                if (!liveDataIsRunningWell) // because it live data is running there wil  always be a measurement command in the list
                    Logs.Log.AddEntryAsPayAttentionOf(Module, $"{timeoutState.action} {R.T_TIMED_OUT} {R.T_IN} {timeoutState.millis / 1000d} sec.");
            }
        }

        public DateTime LastCallTime;

        public void AddToActionsInProgressAndCreateTimeOutTimer(ES_Command command, int millis = 0)
        {
            if (InProgressActions.ContainsKey(command))
            {
                Logs.Log.AddEntryAsPayAttentionOf(Module, string.Format(R.T_DoubleCommand, command, LastCallTime));
            }
            else
            {
                InProgressActions.Add(command, CreateTimeOutTimer(command, millis));
                LastCallTime = DateTime.Now;
            }
        }

        public void Initialize()
        {
            AddToActionsInProgressAndCreateTimeOutTimer(ES_Command.ES_C_Initialize, 5 * 60 * 1000);
            new Command(Module).Initialize();
        }
        public void GetReflectors()
        {
            AddToActionsInProgressAndCreateTimeOutTimer(ES_Command.ES_C_GetReflectors, 5000);
            new Command(Module).GetReflectors();
        }
        public void GetProbes()
        {
            AddToActionsInProgressAndCreateTimeOutTimer(ES_Command.ES_C_GetProbes, 5000);
            new Command(Module).GetProbes();
        }
        public void GetReflector()
        {
            AddToActionsInProgressAndCreateTimeOutTimer(ES_Command.ES_C_GetReflector, 5000);
            new Command(Module).GetReflector();
        }
        public void SetMeasurementMode(ES_MeasMode mode)
        {
            AddToActionsInProgressAndCreateTimeOutTimer(ES_Command.ES_C_SetMeasurementMode, 5000);
            new Command(Module).SetMeasurementMode(mode);
        }
        public void SetTipAdapter(int id)
        {
            AddToActionsInProgressAndCreateTimeOutTimer(ES_Command.ES_C_SetTipAdapter, 5000);
            new Command(Module).SetTipAdapter(id);
        }

        public void MoveHV(int horizontalSpeed, int verticalSpeed)
        {
            AddToActionsInProgressAndCreateTimeOutTimer(ES_Command.ES_C_MoveHV, 5000);
            new Command(Module).MoveHV(horizontalSpeed, verticalSpeed);
        }

        public void StartMeasurement()
        {
            // On AT403 the previous commands are not always fully precesed, otherwise the measure fails
            Thread.Sleep(100);
            AddToActionsInProgressAndCreateTimeOutTimer(ES_Command.ES_C_StartMeasurement, 5 * 1000);
            new Command(Module).StartMeasurement();
        }

        /// <summary>
        /// clear the list od in progress actions
        /// </summary>
        internal void Clear()
        {
            InProgressActions.Clear();
        }

        internal void DelayAction(Action action)
        {
            void OneTimeSubscription(object sender, EventArgs e)
            {
                Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(action);
                AsyncCommandListHasBeenCleared -= OneTimeSubscription;
            }

            AsyncCommandListHasBeenCleared += OneTimeSubscription;
        }
    }

    public class AsyncException : Exception
    {
        public AsyncException(string message) : base(message)
        {

        }
    }
}
