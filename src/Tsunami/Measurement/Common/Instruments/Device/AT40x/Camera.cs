﻿using System.Collections.Generic;
using EmScon;
using System.Xml.Serialization;
using TSU;
using P = TSU.Views.Message.ProgressMessage;
using R = TSU.Properties.Resources;
using System.Net.NetworkInformation;
using System.Windows.Forms;
using TSU.Views;
using TSU.Views.Message;
using System;

namespace TSU.Common.Instruments.Device.AT40x
{
    public class Camera
    {
        readonly Module _at40x;

        [XmlIgnore]
        public bool isOn;

        public Camera(Module at40x)
        {
            _at40x = at40x;

        }

        DsaFlag TurnToFaceOneWhenCameraUsed = new DsaFlag("TurnToFaceOneWhenCameraUsed", DsaOptions.Ask_to_with_pre_checked_dont_show_again);

        [XmlIgnore]
        public bool CameraWasOnWhenMeasureWasLaunched { get; internal set; } = false;

        //TsuBool DontwarnForCameraInFace2 = new TsuBool(false);
        public void Activate()
        {
            if (_at40x.InstrumentIsOn)
            {
                if (this._at40x._circle._selected == ES_TrackerFace.ES_TF_Face2)
                {
                    string titleAndMessage = $"{R.T_ATTENTION_CAMERA_IN_FACE2};{R.T_DO_YOU_WANT_TO_TURN_TO_FACE_1}? ";
                    MessageInput mi = new MessageInput(MessageType.Warning, titleAndMessage)
                    {
                        ButtonTexts = CreationHelper.GetYesNoButtons(),
                        DontShowAgain = TurnToFaceOneWhenCameraUsed,
                        Sender = "Camera"
                    };
                    if (mi.Show().TextOfButtonClicked == R.T_YES)
                        this._at40x._circle.Change();
                }
                _at40x.StatusChange($"{R.T_ACTIVATING} camera...", P.StepTypes.Begin);

                Debug.WriteInConsole("at40x-SYNC-ActivateCameraView");
                _at40x.sync.ActivateCameraView();
                isOn = true;
                _at40x.StatusChange($"Camera {R.T_ACTIVATED}!", P.StepTypes.End);
                _at40x.Ready();
            }
        }
        public void SetParameters(int contrast, int brightness, int saturation)
        {
            if (this._at40x.InstrumentIsOn)
            {
                Debug.WriteInConsole("at40x-SYNC-SetCameraParams");
                _at40x.sync.SetCameraParams(contrast, brightness, saturation);

                Logs.Log.AddEntryAsFYI(this._at40x, $"Constrast = {contrast}/255, brightness = {brightness}/255, saturation = {saturation}/255.");
            }
        }

        internal int GeCheckRange(int startingValue, int delta)
        {
            int value = startingValue + delta;
            if (value > 255) value = 255;
            if (value < 0) value = 0;
            return value;
        }
        

        internal void ChangeBrightness(int delta)
        {
            _at40x.sync.GetCameraParams(out int contrast, out int brightness, out int saturation);
            brightness = GeCheckRange(brightness, delta);
            SetParameters(contrast, brightness, saturation);
        }

        internal void ChangeContrast(int delta)
        {
            _at40x.sync.GetCameraParams(out int contrast, out int brightness, out int saturation);
            contrast = GeCheckRange(contrast, delta);
            SetParameters(contrast, brightness, saturation);
        }

        internal void ChangeSaturation(int delta)
        {
            _at40x.sync.GetCameraParams(out int contrast, out int brightness, out int saturation);
            saturation = GeCheckRange(saturation, delta);
            SetParameters(contrast, brightness, saturation);
        }
        DsaFlag Flag_RestartCameraAfterMeasurement = new DsaFlag("RestartCamera", DsaOptions.Ask_to_with_pre_checked_dont_show_again);
        internal void RestartOrNot()
        {
            if (isOn)
                return;

            if (!CameraWasOnWhenMeasureWasLaunched)
                return;

            if (Flag_RestartCameraAfterMeasurement.State == DsaOptions.Never)
                return;

            Action action = () => {  this._at40x.View.cameraView.StartCamera(); };

            if (Flag_RestartCameraAfterMeasurement.State == DsaOptions.Always)
                this._at40x.View.cameraView.BeginInvoke(action);
            else
            {
                var r = new MessageInput(MessageType.Critical, "Restart the camera?;THe camera was ON when your launch the measurement, do you want to turn it back ON?")
                {
                    ButtonTexts = new List<string> { R.T_YES, R.T_NO },
                    DontShowAgain = Flag_RestartCameraAfterMeasurement
                }.Show();
                if (r.TextOfButtonClicked == R.T_YES || r.TextOfButtonClicked == R.T_ALWAYS)
                    this._at40x.View.cameraView.BeginInvoke(action);
            }
        }
    }
}
