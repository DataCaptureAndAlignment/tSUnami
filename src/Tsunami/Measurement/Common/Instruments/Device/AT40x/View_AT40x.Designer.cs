﻿using TSU.Common.Instruments.Device;
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments.Device.AT40x
{
    partial class View : Device.View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (this.Module == null)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }
            
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View));
            this.labelModel = new System.Windows.Forms.Label();
            this.panelGlobal = new TSU.Views.TsuPanel();
            this.splitAT_V_SubV = new System.Windows.Forms.SplitContainer();
            this.panelMain = new TSU.Views.TsuPanel();
            this.panelConnected = new System.Windows.Forms.Panel();
            this.buttonRelease2 = new System.Windows.Forms.Button();
            this.buttonInit = new System.Windows.Forms.Button();
            this.buttonShift = new System.Windows.Forms.Button();
            this.buttonPrism = new System.Windows.Forms.Button();
            this.buttonLocking = new System.Windows.Forms.Button();
            this.buttonGoto = new System.Windows.Forms.Button();
            this.buttonMeasure = new System.Windows.Forms.Button();
            this.buttonGotoAll = new System.Windows.Forms.Button();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.panelLog = new TSU.Views.TsuPanel();
            this.panelState = new TSU.Views.TsuPanel();
            this.pMeas = new System.Windows.Forms.PictureBox();
            this.pictureLaserState = new System.Windows.Forms.PictureBox();
            this.pictureLaser = new System.Windows.Forms.PictureBox();
            this.buttonRefreshStatus = new System.Windows.Forms.Button();
            this.pictureNivelState = new System.Windows.Forms.PictureBox();
            this.picture_State_Measure_Mode = new System.Windows.Forms.PictureBox();
            this.labelHumidity = new System.Windows.Forms.Label();
            this.labelPressure = new System.Windows.Forms.Label();
            this.labelTemperatureState = new System.Windows.Forms.Label();
            this.pictureHumidity = new System.Windows.Forms.PictureBox();
            this.picturePressure = new System.Windows.Forms.PictureBox();
            this.pictureTemperature = new System.Windows.Forms.PictureBox();
            this.pictureCompensatorState = new System.Windows.Forms.PictureBox();
            this.pictureCompensator = new System.Windows.Forms.PictureBox();
            this.labelBatterySensorState = new System.Windows.Forms.Label();
            this.labelBatteryControlerState = new System.Windows.Forms.Label();
            this.picture_State_Beam_State = new System.Windows.Forms.PictureBox();
            this.picture_State_Face = new System.Windows.Forms.PictureBox();
            this.pictureInitialisationState = new System.Windows.Forms.PictureBox();
            this.pictureInitialisation = new System.Windows.Forms.PictureBox();
            this.pictureSensorIdealTemperatureState = new System.Windows.Forms.PictureBox();
            this.pictureSensorIdealTemperature = new System.Windows.Forms.PictureBox();
            this.pictureNivel = new System.Windows.Forms.PictureBox();
            this.pictureBatterySensor = new System.Windows.Forms.PictureBox();
            this.pictureBattetyControler = new System.Windows.Forms.PictureBox();
            this.pictureAdmState = new System.Windows.Forms.PictureBox();
            this.pictureAdm = new System.Windows.Forms.PictureBox();
            this.pictureControlerState = new System.Windows.Forms.PictureBox();
            this.picture_Controller = new System.Windows.Forms.PictureBox();
            this.pictureSensorState = new System.Windows.Forms.PictureBox();
            this.pictureSensor = new System.Windows.Forms.PictureBox();
            this.panelCaJoLi = new TSU.Views.TsuPanel();
            this.panelCaJoLiConnected = new System.Windows.Forms.Panel();
            this.buttonRemoteList = new System.Windows.Forms.Button();
            this.buttonTimer = new System.Windows.Forms.Button();
            this.buttonJog = new System.Windows.Forms.Button();
            this.buttonRemote = new System.Windows.Forms.Button();
            this.buttonLiveData = new System.Windows.Forms.Button();
            this.buttonCamera = new System.Windows.Forms.Button();
            this.buttonExtract = new System.Windows.Forms.Button();
            this.panel4SubViews = new TSU.Views.TsuPanel();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pMeasTsu = new System.Windows.Forms.PictureBox();
            this.pMeasLock = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.panelGlobal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitAT_V_SubV)).BeginInit();
            this.splitAT_V_SubV.Panel1.SuspendLayout();
            this.splitAT_V_SubV.Panel2.SuspendLayout();
            this.splitAT_V_SubV.SuspendLayout();
            this.panelMain.SuspendLayout();
            this.panelConnected.SuspendLayout();
            this.panelState.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pMeas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureLaserState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureLaser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureNivelState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture_State_Measure_Mode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureHumidity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picturePressure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureTemperature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureCompensatorState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureCompensator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture_State_Beam_State)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture_State_Face)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureInitialisationState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureInitialisation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureSensorIdealTemperatureState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureSensorIdealTemperature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureNivel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBatterySensor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattetyControler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureAdmState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureAdm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureControlerState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture_Controller)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureSensorState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureSensor)).BeginInit();
            this.panelCaJoLi.SuspendLayout();
            this.panelCaJoLiConnected.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pMeasTsu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pMeasLock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelModel
            // 
            this.labelModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModel.ForeColor = System.Drawing.Color.Black;
            this.labelModel.Location = new System.Drawing.Point(40, 2);
            this.labelModel.Name = "labelModel";
            this.labelModel.Size = new System.Drawing.Size(644, 26);
            this.labelModel.TabIndex = 18;
            this.labelModel.Text = "Absolute Tracker 40X";
            // 
            // panelGlobal
            // 
            this.panelGlobal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelGlobal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.panelGlobal.Controls.Add(this.splitAT_V_SubV);
            this.panelGlobal.Location = new System.Drawing.Point(6, 22);
            this.panelGlobal.Name = "panelGlobal";
            this.panelGlobal.Size = new System.Drawing.Size(946, 376);
            this.panelGlobal.TabIndex = 17;
            // 
            // splitAT_V_SubV
            // 
            this.splitAT_V_SubV.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.splitAT_V_SubV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitAT_V_SubV.Location = new System.Drawing.Point(0, 0);
            this.splitAT_V_SubV.Margin = new System.Windows.Forms.Padding(0);
            this.splitAT_V_SubV.Name = "splitAT_V_SubV";
            // 
            // splitAT_V_SubV.Panel1
            // 
            this.splitAT_V_SubV.Panel1.Controls.Add(this.panelMain);
            this.splitAT_V_SubV.Panel1MinSize = 100;
            // 
            // splitAT_V_SubV.Panel2
            // 
            this.splitAT_V_SubV.Panel2.Controls.Add(this.panelCaJoLi);
            this.splitAT_V_SubV.Panel2MinSize = 0;
            this.splitAT_V_SubV.Size = new System.Drawing.Size(946, 376);
            this.splitAT_V_SubV.SplitterDistance = 541;
            this.splitAT_V_SubV.SplitterWidth = 20;
            this.splitAT_V_SubV.TabIndex = 15;
            this.splitAT_V_SubV.DoubleClick += new System.EventHandler(this.splitContainer1_DoubleClick);
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.panelMain.Controls.Add(this.panelConnected);
            this.panelMain.Controls.Add(this.buttonConnect);
            this.panelMain.Controls.Add(this.panelLog);
            this.panelMain.Controls.Add(this.panelState);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(0, 0);
            this.panelMain.Name = "panelMain";
            this.panelMain.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.panelMain.Size = new System.Drawing.Size(541, 376);
            this.panelMain.TabIndex = 14;
            // 
            // panelConnected
            // 
            this.panelConnected.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelConnected.Controls.Add(this.buttonRelease2);
            this.panelConnected.Controls.Add(this.buttonInit);
            this.panelConnected.Controls.Add(this.buttonShift);
            this.panelConnected.Controls.Add(this.buttonPrism);
            this.panelConnected.Controls.Add(this.buttonLocking);
            this.panelConnected.Controls.Add(this.buttonGoto);
            this.panelConnected.Controls.Add(this.buttonMeasure);
            this.panelConnected.Controls.Add(this.buttonGotoAll);
            this.panelConnected.Location = new System.Drawing.Point(3, 319);
            this.panelConnected.Name = "panelConnected";
            this.panelConnected.Size = new System.Drawing.Size(474, 54);
            this.panelConnected.TabIndex = 37;
            // 
            // buttonRelease2
            // 
            this.buttonRelease2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRelease2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.buttonRelease2.BackgroundImage = global::TSU.Properties.Resources.At40x_Release;
            this.buttonRelease2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonRelease2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRelease2.ImageKey = "(none)";
            this.buttonRelease2.Location = new System.Drawing.Point(214, 2);
            this.buttonRelease2.Name = "buttonRelease2";
            this.buttonRelease2.Size = new System.Drawing.Size(52, 52);
            this.buttonRelease2.TabIndex = 51;
            this.toolTip1.SetToolTip(this.buttonRelease2, global::TSU.Properties.Resources.T_FORCE_TO_UPDATE_THE_STATUS);
            this.buttonRelease2.UseVisualStyleBackColor = false;
            this.buttonRelease2.Click += new System.EventHandler(this.On_Button_Release_Click);
            // 
            // buttonInit
            // 
            this.buttonInit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonInit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.buttonInit.BackgroundImage = global::TSU.Properties.Resources.At40x_Init;
            this.buttonInit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonInit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInit.ImageKey = "(none)";
            this.buttonInit.Location = new System.Drawing.Point(421, 3);
            this.buttonInit.Name = "buttonInit";
            this.buttonInit.Size = new System.Drawing.Size(52, 52);
            this.buttonInit.TabIndex = 44;
            this.toolTip1.SetToolTip(this.buttonInit, global::TSU.Properties.Resources.T_FORCE_TO_UPDATE_THE_STATUS);
            this.buttonInit.UseVisualStyleBackColor = false;
            this.buttonInit.Click += new System.EventHandler(this.On_Button_Initialisation_Click);
            // 
            // buttonShift
            // 
            this.buttonShift.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonShift.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.buttonShift.BackgroundImage = global::TSU.Properties.Resources.At40x_Shift;
            this.buttonShift.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonShift.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShift.ImageKey = "(none)";
            this.buttonShift.Location = new System.Drawing.Point(320, 2);
            this.buttonShift.Name = "buttonShift";
            this.buttonShift.Size = new System.Drawing.Size(52, 52);
            this.buttonShift.TabIndex = 45;
            this.toolTip1.SetToolTip(this.buttonShift, global::TSU.Properties.Resources.T_FORCE_TO_UPDATE_THE_STATUS);
            this.buttonShift.UseVisualStyleBackColor = false;
            this.buttonShift.Click += new System.EventHandler(this.On_ButtonShift_Click);
            // 
            // buttonPrism
            // 
            this.buttonPrism.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonPrism.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.buttonPrism.BackgroundImage = global::TSU.Properties.Resources.At40x_PrismeUnknown;
            this.buttonPrism.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonPrism.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPrism.ImageKey = "(none)";
            this.buttonPrism.Location = new System.Drawing.Point(267, 2);
            this.buttonPrism.Name = "buttonPrism";
            this.buttonPrism.Size = new System.Drawing.Size(52, 52);
            this.buttonPrism.TabIndex = 46;
            this.toolTip1.SetToolTip(this.buttonPrism, global::TSU.Properties.Resources.T_FORCE_TO_UPDATE_THE_STATUS);
            this.buttonPrism.UseVisualStyleBackColor = false;
            this.buttonPrism.Click += new System.EventHandler(this.On_ButtonPrism_Click);
            this.buttonPrism.MouseEnter += new System.EventHandler(this.On_Button_Prismes_MouseEnter);
            // 
            // buttonLocking
            // 
            this.buttonLocking.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonLocking.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.buttonLocking.BackgroundImage = global::TSU.Properties.Resources.At40x_Lock;
            this.buttonLocking.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonLocking.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLocking.ImageKey = "(none)";
            this.buttonLocking.Location = new System.Drawing.Point(161, 2);
            this.buttonLocking.Name = "buttonLocking";
            this.buttonLocking.Size = new System.Drawing.Size(52, 52);
            this.buttonLocking.TabIndex = 47;
            this.toolTip1.SetToolTip(this.buttonLocking, global::TSU.Properties.Resources.T_FORCE_TO_UPDATE_THE_STATUS);
            this.buttonLocking.UseVisualStyleBackColor = false;
            this.buttonLocking.Click += new System.EventHandler(this.On_Button_Lock_Click);
            // 
            // buttonGoto
            // 
            this.buttonGoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonGoto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.buttonGoto.BackgroundImage = global::TSU.Properties.Resources.At40x_Goto;
            this.buttonGoto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonGoto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGoto.ImageKey = "(none)";
            this.buttonGoto.Location = new System.Drawing.Point(108, 2);
            this.buttonGoto.Name = "buttonGoto";
            this.buttonGoto.Size = new System.Drawing.Size(52, 52);
            this.buttonGoto.TabIndex = 48;
            this.toolTip1.SetToolTip(this.buttonGoto, global::TSU.Properties.Resources.T_FORCE_TO_UPDATE_THE_STATUS);
            this.buttonGoto.UseVisualStyleBackColor = false;
            this.buttonGoto.Click += new System.EventHandler(this.On_Button_Goto_Click);
            // 
            // buttonMeasure
            // 
            this.buttonMeasure.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonMeasure.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.buttonMeasure.BackgroundImage = global::TSU.Properties.Resources.At40x_Meas;
            this.buttonMeasure.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMeasure.ImageKey = "(none)";
            this.buttonMeasure.Location = new System.Drawing.Point(55, 2);
            this.buttonMeasure.Name = "buttonMeasure";
            this.buttonMeasure.Size = new System.Drawing.Size(52, 52);
            this.buttonMeasure.TabIndex = 49;
            this.toolTip1.SetToolTip(this.buttonMeasure, global::TSU.Properties.Resources.T_FORCE_TO_UPDATE_THE_STATUS);
            this.buttonMeasure.UseVisualStyleBackColor = false;
            this.buttonMeasure.Click += new System.EventHandler(this.On_Button_Measure_Click);
            // 
            // buttonGotoAll
            // 
            this.buttonGotoAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonGotoAll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.buttonGotoAll.BackgroundImage = global::TSU.Properties.Resources.At40x_GotoAll;
            this.buttonGotoAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonGotoAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGotoAll.ImageKey = "(none)";
            this.buttonGotoAll.Location = new System.Drawing.Point(2, 2);
            this.buttonGotoAll.Name = "buttonGotoAll";
            this.buttonGotoAll.Size = new System.Drawing.Size(52, 52);
            this.buttonGotoAll.TabIndex = 50;
            this.toolTip1.SetToolTip(this.buttonGotoAll, global::TSU.Properties.Resources.T_FORCE_TO_UPDATE_THE_STATUS);
            this.buttonGotoAll.UseVisualStyleBackColor = false;
            this.buttonGotoAll.Click += new System.EventHandler(this.On_Button_GotoAndMeasure_Click);
            // 
            // buttonConnect
            // 
            this.buttonConnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonConnect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.buttonConnect.BackgroundImage = global::TSU.Properties.Resources.At40x_Power;
            this.buttonConnect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonConnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonConnect.Location = new System.Drawing.Point(483, 322);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(52, 52);
            this.buttonConnect.TabIndex = 36;
            this.toolTip1.SetToolTip(this.buttonConnect, global::TSU.Properties.Resources.T_FORCE_TO_UPDATE_THE_STATUS);
            this.buttonConnect.UseVisualStyleBackColor = false;
            this.buttonConnect.MouseDown += new System.Windows.Forms.MouseEventHandler(this.On_Button_Connection_MouseDown);
            // 
            // panelLog
            // 
            this.panelLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelLog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.panelLog.Location = new System.Drawing.Point(8, 53);
            this.panelLog.Name = "panelLog";
            this.panelLog.Size = new System.Drawing.Size(524, 264);
            this.panelLog.TabIndex = 14;
            // 
            // panelState
            // 
            this.panelState.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.panelState.Controls.Add(this.pMeas);
            this.panelState.Controls.Add(this.pictureLaserState);
            this.panelState.Controls.Add(this.pictureLaser);
            this.panelState.Controls.Add(this.buttonRefreshStatus);
            this.panelState.Controls.Add(this.pictureNivelState);
            this.panelState.Controls.Add(this.picture_State_Measure_Mode);
            this.panelState.Controls.Add(this.labelHumidity);
            this.panelState.Controls.Add(this.labelPressure);
            this.panelState.Controls.Add(this.labelTemperatureState);
            this.panelState.Controls.Add(this.pictureHumidity);
            this.panelState.Controls.Add(this.picturePressure);
            this.panelState.Controls.Add(this.pictureTemperature);
            this.panelState.Controls.Add(this.pictureCompensatorState);
            this.panelState.Controls.Add(this.pictureCompensator);
            this.panelState.Controls.Add(this.labelBatterySensorState);
            this.panelState.Controls.Add(this.labelBatteryControlerState);
            this.panelState.Controls.Add(this.picture_State_Beam_State);
            this.panelState.Controls.Add(this.picture_State_Face);
            this.panelState.Controls.Add(this.pictureInitialisationState);
            this.panelState.Controls.Add(this.pictureInitialisation);
            this.panelState.Controls.Add(this.pictureSensorIdealTemperatureState);
            this.panelState.Controls.Add(this.pictureSensorIdealTemperature);
            this.panelState.Controls.Add(this.pictureNivel);
            this.panelState.Controls.Add(this.pictureBatterySensor);
            this.panelState.Controls.Add(this.pictureBattetyControler);
            this.panelState.Controls.Add(this.pictureAdmState);
            this.panelState.Controls.Add(this.pictureAdm);
            this.panelState.Controls.Add(this.pictureControlerState);
            this.panelState.Controls.Add(this.picture_Controller);
            this.panelState.Controls.Add(this.pictureSensorState);
            this.panelState.Controls.Add(this.pictureSensor);
            this.panelState.Location = new System.Drawing.Point(9, 4);
            this.panelState.Name = "panelState";
            this.panelState.Size = new System.Drawing.Size(527, 46);
            this.panelState.TabIndex = 13;
            // 
            // pMeas
            // 
            this.pMeas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pMeas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.pMeas.Image = ((System.Drawing.Image)(resources.GetObject("pMeas.Image")));
            this.pMeas.Location = new System.Drawing.Point(535, 29);
            this.pMeas.Name = "pMeas";
            this.pMeas.Size = new System.Drawing.Size(18, 18);
            this.pMeas.TabIndex = 18;
            this.pMeas.TabStop = false;
            // 
            // pictureLaserState
            // 
            this.pictureLaserState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.pictureLaserState.Image = ((System.Drawing.Image)(resources.GetObject("pictureLaserState.Image")));
            this.pictureLaserState.Location = new System.Drawing.Point(211, 29);
            this.pictureLaserState.Name = "pictureLaserState";
            this.pictureLaserState.Size = new System.Drawing.Size(18, 18);
            this.pictureLaserState.TabIndex = 35;
            this.pictureLaserState.TabStop = false;
            // 
            // pictureLaser
            // 
            this.pictureLaser.Image = global::TSU.Properties.Resources.At40x_Measure;
            this.pictureLaser.Location = new System.Drawing.Point(208, 3);
            this.pictureLaser.Name = "pictureLaser";
            this.pictureLaser.Size = new System.Drawing.Size(24, 24);
            this.pictureLaser.TabIndex = 34;
            this.pictureLaser.TabStop = false;
            // 
            // buttonRefreshStatus
            // 
            this.buttonRefreshStatus.BackColor = System.Drawing.Color.Lime;
            this.buttonRefreshStatus.BackgroundImage = global::TSU.Properties.Resources.Update;
            this.buttonRefreshStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonRefreshStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRefreshStatus.Location = new System.Drawing.Point(370, 3);
            this.buttonRefreshStatus.Name = "buttonRefreshStatus";
            this.buttonRefreshStatus.Size = new System.Drawing.Size(41, 40);
            this.buttonRefreshStatus.TabIndex = 33;
            this.toolTip1.SetToolTip(this.buttonRefreshStatus, "Force to update the status, that often fixes some problems too");
            this.buttonRefreshStatus.UseVisualStyleBackColor = false;
            this.buttonRefreshStatus.Click += new System.EventHandler(this.On_Button_UpdateStatus_Click);
            this.buttonRefreshStatus.MouseHover += new System.EventHandler(this.On_ButtonRefreshStatus_MouseHover);
            // 
            // pictureNivelState
            // 
            this.pictureNivelState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.pictureNivelState.Image = ((System.Drawing.Image)(resources.GetObject("pictureNivelState.Image")));
            this.pictureNivelState.Location = new System.Drawing.Point(138, 29);
            this.pictureNivelState.Name = "pictureNivelState";
            this.pictureNivelState.Size = new System.Drawing.Size(18, 18);
            this.pictureNivelState.TabIndex = 31;
            this.pictureNivelState.TabStop = false;
            // 
            // picture_State_Measure_Mode
            // 
            this.picture_State_Measure_Mode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picture_State_Measure_Mode.Image = global::TSU.Properties.Resources.At40x_Precision_Precise;
            this.picture_State_Measure_Mode.Location = new System.Drawing.Point(396, 3);
            this.picture_State_Measure_Mode.Name = "picture_State_Measure_Mode";
            this.picture_State_Measure_Mode.Size = new System.Drawing.Size(39, 40);
            this.picture_State_Measure_Mode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picture_State_Measure_Mode.TabIndex = 30;
            this.picture_State_Measure_Mode.TabStop = false;
            this.picture_State_Measure_Mode.Click += new System.EventHandler(this.picture_State_Measure_Mode_Click);
            // 
            // labelHumidity
            // 
            this.labelHumidity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.labelHumidity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHumidity.ForeColor = System.Drawing.Color.White;
            this.labelHumidity.Location = new System.Drawing.Point(289, 28);
            this.labelHumidity.Name = "labelHumidity";
            this.labelHumidity.Size = new System.Drawing.Size(30, 16);
            this.labelHumidity.TabIndex = 29;
            this.labelHumidity.Text = "?/?";
            this.labelHumidity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPressure
            // 
            this.labelPressure.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.labelPressure.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPressure.ForeColor = System.Drawing.Color.White;
            this.labelPressure.Location = new System.Drawing.Point(322, 28);
            this.labelPressure.Name = "labelPressure";
            this.labelPressure.Size = new System.Drawing.Size(59, 16);
            this.labelPressure.TabIndex = 28;
            this.labelPressure.Text = "?/?";
            this.labelPressure.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelTemperatureState
            // 
            this.labelTemperatureState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.labelTemperatureState.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTemperatureState.ForeColor = System.Drawing.Color.White;
            this.labelTemperatureState.Location = new System.Drawing.Point(256, 28);
            this.labelTemperatureState.Name = "labelTemperatureState";
            this.labelTemperatureState.Size = new System.Drawing.Size(36, 16);
            this.labelTemperatureState.TabIndex = 27;
            this.labelTemperatureState.Text = "?/?";
            this.labelTemperatureState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureHumidity
            // 
            this.pictureHumidity.Image = ((System.Drawing.Image)(resources.GetObject("pictureHumidity.Image")));
            this.pictureHumidity.InitialImage = null;
            this.pictureHumidity.Location = new System.Drawing.Point(292, 3);
            this.pictureHumidity.Name = "pictureHumidity";
            this.pictureHumidity.Size = new System.Drawing.Size(24, 24);
            this.pictureHumidity.TabIndex = 26;
            this.pictureHumidity.TabStop = false;
            // 
            // picturePressure
            // 
            this.picturePressure.Image = global::TSU.Properties.Resources.At40x_Pressure;
            this.picturePressure.Location = new System.Drawing.Point(323, 3);
            this.picturePressure.Name = "picturePressure";
            this.picturePressure.Size = new System.Drawing.Size(24, 24);
            this.picturePressure.TabIndex = 25;
            this.picturePressure.TabStop = false;
            // 
            // pictureTemperature
            // 
            this.pictureTemperature.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureTemperature.Image = global::TSU.Properties.Resources.At40x_Temperature;
            this.pictureTemperature.Location = new System.Drawing.Point(262, 3);
            this.pictureTemperature.Name = "pictureTemperature";
            this.pictureTemperature.Size = new System.Drawing.Size(24, 24);
            this.pictureTemperature.TabIndex = 24;
            this.pictureTemperature.TabStop = false;
            // 
            // pictureCompensatorState
            // 
            this.pictureCompensatorState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.pictureCompensatorState.Image = ((System.Drawing.Image)(resources.GetObject("pictureCompensatorState.Image")));
            this.pictureCompensatorState.Location = new System.Drawing.Point(113, 29);
            this.pictureCompensatorState.Name = "pictureCompensatorState";
            this.pictureCompensatorState.Size = new System.Drawing.Size(18, 18);
            this.pictureCompensatorState.TabIndex = 23;
            this.pictureCompensatorState.TabStop = false;
            // 
            // pictureCompensator
            // 
            this.pictureCompensator.Image = ((System.Drawing.Image)(resources.GetObject("pictureCompensator.Image")));
            this.pictureCompensator.Location = new System.Drawing.Point(110, 3);
            this.pictureCompensator.Name = "pictureCompensator";
            this.pictureCompensator.Size = new System.Drawing.Size(24, 24);
            this.pictureCompensator.TabIndex = 22;
            this.pictureCompensator.TabStop = false;
            // 
            // labelBatterySensorState
            // 
            this.labelBatterySensorState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.labelBatterySensorState.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBatterySensorState.ForeColor = System.Drawing.Color.Red;
            this.labelBatterySensorState.Location = new System.Drawing.Point(77, 30);
            this.labelBatterySensorState.Name = "labelBatterySensorState";
            this.labelBatterySensorState.Size = new System.Drawing.Size(27, 13);
            this.labelBatterySensorState.TabIndex = 20;
            this.labelBatterySensorState.Text = "%";
            this.labelBatterySensorState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelBatteryControlerState
            // 
            this.labelBatteryControlerState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.labelBatteryControlerState.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBatteryControlerState.ForeColor = System.Drawing.Color.Red;
            this.labelBatteryControlerState.Location = new System.Drawing.Point(52, 30);
            this.labelBatteryControlerState.Name = "labelBatteryControlerState";
            this.labelBatteryControlerState.Size = new System.Drawing.Size(27, 13);
            this.labelBatteryControlerState.TabIndex = 19;
            this.labelBatteryControlerState.Text = "%";
            this.labelBatteryControlerState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picture_State_Beam_State
            // 
            this.picture_State_Beam_State.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picture_State_Beam_State.Image = ((System.Drawing.Image)(resources.GetObject("picture_State_Beam_State.Image")));
            this.picture_State_Beam_State.Location = new System.Drawing.Point(485, 4);
            this.picture_State_Beam_State.Name = "picture_State_Beam_State";
            this.picture_State_Beam_State.Size = new System.Drawing.Size(39, 40);
            this.picture_State_Beam_State.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picture_State_Beam_State.TabIndex = 17;
            this.picture_State_Beam_State.TabStop = false;
            this.picture_State_Beam_State.Click += new System.EventHandler(this.picture_State_Beam_State_Click);
            // 
            // picture_State_Face
            // 
            this.picture_State_Face.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picture_State_Face.Image = ((System.Drawing.Image)(resources.GetObject("picture_State_Face.Image")));
            this.picture_State_Face.Location = new System.Drawing.Point(440, 3);
            this.picture_State_Face.Name = "picture_State_Face";
            this.picture_State_Face.Size = new System.Drawing.Size(40, 40);
            this.picture_State_Face.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picture_State_Face.TabIndex = 16;
            this.picture_State_Face.TabStop = false;
            // 
            // pictureInitialisationState
            // 
            this.pictureInitialisationState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.pictureInitialisationState.Image = ((System.Drawing.Image)(resources.GetObject("pictureInitialisationState.Image")));
            this.pictureInitialisationState.Location = new System.Drawing.Point(186, 29);
            this.pictureInitialisationState.Name = "pictureInitialisationState";
            this.pictureInitialisationState.Size = new System.Drawing.Size(18, 18);
            this.pictureInitialisationState.TabIndex = 15;
            this.pictureInitialisationState.TabStop = false;
            // 
            // pictureInitialisation
            // 
            this.pictureInitialisation.Image = ((System.Drawing.Image)(resources.GetObject("pictureInitialisation.Image")));
            this.pictureInitialisation.Location = new System.Drawing.Point(184, 3);
            this.pictureInitialisation.Name = "pictureInitialisation";
            this.pictureInitialisation.Size = new System.Drawing.Size(24, 24);
            this.pictureInitialisation.TabIndex = 14;
            this.pictureInitialisation.TabStop = false;
            // 
            // pictureSensorIdealTemperatureState
            // 
            this.pictureSensorIdealTemperatureState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.pictureSensorIdealTemperatureState.Image = ((System.Drawing.Image)(resources.GetObject("pictureSensorIdealTemperatureState.Image")));
            this.pictureSensorIdealTemperatureState.Location = new System.Drawing.Point(236, 29);
            this.pictureSensorIdealTemperatureState.Name = "pictureSensorIdealTemperatureState";
            this.pictureSensorIdealTemperatureState.Size = new System.Drawing.Size(18, 18);
            this.pictureSensorIdealTemperatureState.TabIndex = 13;
            this.pictureSensorIdealTemperatureState.TabStop = false;
            // 
            // pictureSensorIdealTemperature
            // 
            this.pictureSensorIdealTemperature.Image = ((System.Drawing.Image)(resources.GetObject("pictureSensorIdealTemperature.Image")));
            this.pictureSensorIdealTemperature.Location = new System.Drawing.Point(232, 3);
            this.pictureSensorIdealTemperature.Name = "pictureSensorIdealTemperature";
            this.pictureSensorIdealTemperature.Size = new System.Drawing.Size(24, 24);
            this.pictureSensorIdealTemperature.TabIndex = 12;
            this.pictureSensorIdealTemperature.TabStop = false;
            // 
            // pictureNivel
            // 
            this.pictureNivel.Image = ((System.Drawing.Image)(resources.GetObject("pictureNivel.Image")));
            this.pictureNivel.Location = new System.Drawing.Point(134, 3);
            this.pictureNivel.Name = "pictureNivel";
            this.pictureNivel.Size = new System.Drawing.Size(24, 24);
            this.pictureNivel.TabIndex = 10;
            this.pictureNivel.TabStop = false;
            // 
            // pictureBatterySensor
            // 
            this.pictureBatterySensor.Image = ((System.Drawing.Image)(resources.GetObject("pictureBatterySensor.Image")));
            this.pictureBatterySensor.Location = new System.Drawing.Point(76, 3);
            this.pictureBatterySensor.Name = "pictureBatterySensor";
            this.pictureBatterySensor.Size = new System.Drawing.Size(24, 24);
            this.pictureBatterySensor.TabIndex = 8;
            this.pictureBatterySensor.TabStop = false;
            // 
            // pictureBattetyControler
            // 
            this.pictureBattetyControler.Image = ((System.Drawing.Image)(resources.GetObject("pictureBattetyControler.Image")));
            this.pictureBattetyControler.Location = new System.Drawing.Point(54, 3);
            this.pictureBattetyControler.Name = "pictureBattetyControler";
            this.pictureBattetyControler.Size = new System.Drawing.Size(24, 24);
            this.pictureBattetyControler.TabIndex = 6;
            this.pictureBattetyControler.TabStop = false;
            // 
            // pictureAdmState
            // 
            this.pictureAdmState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.pictureAdmState.Image = ((System.Drawing.Image)(resources.GetObject("pictureAdmState.Image")));
            this.pictureAdmState.Location = new System.Drawing.Point(162, 29);
            this.pictureAdmState.Name = "pictureAdmState";
            this.pictureAdmState.Size = new System.Drawing.Size(18, 18);
            this.pictureAdmState.TabIndex = 5;
            this.pictureAdmState.TabStop = false;
            // 
            // pictureAdm
            // 
            this.pictureAdm.Image = ((System.Drawing.Image)(resources.GetObject("pictureAdm.Image")));
            this.pictureAdm.Location = new System.Drawing.Point(159, 3);
            this.pictureAdm.Name = "pictureAdm";
            this.pictureAdm.Size = new System.Drawing.Size(24, 24);
            this.pictureAdm.TabIndex = 4;
            this.pictureAdm.TabStop = false;
            // 
            // pictureControlerState
            // 
            this.pictureControlerState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.pictureControlerState.Image = ((System.Drawing.Image)(resources.GetObject("pictureControlerState.Image")));
            this.pictureControlerState.Location = new System.Drawing.Point(6, 29);
            this.pictureControlerState.Name = "pictureControlerState";
            this.pictureControlerState.Size = new System.Drawing.Size(18, 18);
            this.pictureControlerState.TabIndex = 3;
            this.pictureControlerState.TabStop = false;
            // 
            // picture_Controller
            // 
            this.picture_Controller.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.picture_Controller.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picture_Controller.BackgroundImage")));
            this.picture_Controller.Image = ((System.Drawing.Image)(resources.GetObject("picture_Controller.Image")));
            this.picture_Controller.Location = new System.Drawing.Point(3, 3);
            this.picture_Controller.Name = "picture_Controller";
            this.picture_Controller.Size = new System.Drawing.Size(24, 24);
            this.picture_Controller.TabIndex = 2;
            this.picture_Controller.TabStop = false;
            this.picture_Controller.Tag = "Connection Opened";
            this.picture_Controller.MouseEnter += new System.EventHandler(this.ShowToolTip_OnMouseEnter);
            // 
            // pictureSensorState
            // 
            this.pictureSensorState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.pictureSensorState.Image = ((System.Drawing.Image)(resources.GetObject("pictureSensorState.Image")));
            this.pictureSensorState.Location = new System.Drawing.Point(28, 29);
            this.pictureSensorState.Name = "pictureSensorState";
            this.pictureSensorState.Size = new System.Drawing.Size(18, 18);
            this.pictureSensorState.TabIndex = 1;
            this.pictureSensorState.TabStop = false;
            // 
            // pictureSensor
            // 
            this.pictureSensor.Image = ((System.Drawing.Image)(resources.GetObject("pictureSensor.Image")));
            this.pictureSensor.Location = new System.Drawing.Point(25, 3);
            this.pictureSensor.Name = "pictureSensor";
            this.pictureSensor.Size = new System.Drawing.Size(24, 24);
            this.pictureSensor.TabIndex = 0;
            this.pictureSensor.TabStop = false;
            // 
            // panelCaJoLi
            // 
            this.panelCaJoLi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.panelCaJoLi.Controls.Add(this.panelCaJoLiConnected);
            this.panelCaJoLi.Controls.Add(this.buttonExtract);
            this.panelCaJoLi.Controls.Add(this.panel4SubViews);
            this.panelCaJoLi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCaJoLi.Location = new System.Drawing.Point(0, 0);
            this.panelCaJoLi.Margin = new System.Windows.Forms.Padding(0);
            this.panelCaJoLi.Name = "panelCaJoLi";
            this.panelCaJoLi.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.panelCaJoLi.Size = new System.Drawing.Size(385, 376);
            this.panelCaJoLi.TabIndex = 13;
            // 
            // panelCaJoLiConnected
            // 
            this.panelCaJoLiConnected.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelCaJoLiConnected.Controls.Add(this.buttonRemoteList);
            this.panelCaJoLiConnected.Controls.Add(this.buttonTimer);
            this.panelCaJoLiConnected.Controls.Add(this.buttonJog);
            this.panelCaJoLiConnected.Controls.Add(this.buttonRemote);
            this.panelCaJoLiConnected.Controls.Add(this.buttonLiveData);
            this.panelCaJoLiConnected.Controls.Add(this.buttonCamera);
            this.panelCaJoLiConnected.Location = new System.Drawing.Point(3, 319);
            this.panelCaJoLiConnected.Name = "panelCaJoLiConnected";
            this.panelCaJoLiConnected.Size = new System.Drawing.Size(347, 54);
            this.panelCaJoLiConnected.TabIndex = 38;
            // 
            // buttonRemoteList
            // 
            this.buttonRemoteList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRemoteList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.buttonRemoteList.BackgroundImage = global::TSU.Properties.Resources.At40x_ProbeList;
            this.buttonRemoteList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonRemoteList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRemoteList.ImageKey = "(none)";
            this.buttonRemoteList.Location = new System.Drawing.Point(210, 2);
            this.buttonRemoteList.Name = "buttonRemoteList";
            this.buttonRemoteList.Size = new System.Drawing.Size(52, 52);
            this.buttonRemoteList.TabIndex = 54;
            this.toolTip1.SetToolTip(this.buttonRemoteList, global::TSU.Properties.Resources.T_FORCE_TO_UPDATE_THE_STATUS);
            this.buttonRemoteList.UseVisualStyleBackColor = false;
            this.buttonRemoteList.Click += new System.EventHandler(this.On_ButtonRemoteList_Click);
            // 
            // buttonTimer
            // 
            this.buttonTimer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonTimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.buttonTimer.BackgroundImage = global::TSU.Properties.Resources.At40x_Timer;
            this.buttonTimer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonTimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTimer.ImageKey = "(none)";
            this.buttonTimer.Location = new System.Drawing.Point(295, 2);
            this.buttonTimer.Name = "buttonTimer";
            this.buttonTimer.Size = new System.Drawing.Size(52, 52);
            this.buttonTimer.TabIndex = 53;
            this.buttonTimer.UseVisualStyleBackColor = false;
            this.buttonTimer.Click += new System.EventHandler(this.On_Button_Timer_Click);
            // 
            // buttonJog
            // 
            this.buttonJog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonJog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.buttonJog.BackgroundImage = global::TSU.Properties.Resources.At40x_Jog;
            this.buttonJog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonJog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonJog.ImageKey = "(none)";
            this.buttonJog.Location = new System.Drawing.Point(2, 2);
            this.buttonJog.Name = "buttonJog";
            this.buttonJog.Size = new System.Drawing.Size(52, 52);
            this.buttonJog.TabIndex = 49;
            this.toolTip1.SetToolTip(this.buttonJog, global::TSU.Properties.Resources.T_FORCE_TO_UPDATE_THE_STATUS);
            this.buttonJog.UseVisualStyleBackColor = false;
            this.buttonJog.Click += new System.EventHandler(this.On_Button_Jog_Click);
            // 
            // buttonRemote
            // 
            this.buttonRemote.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRemote.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.buttonRemote.BackgroundImage = global::TSU.Properties.Resources.At40x_Probe;
            this.buttonRemote.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonRemote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRemote.ImageKey = "(none)";
            this.buttonRemote.Location = new System.Drawing.Point(158, 2);
            this.buttonRemote.Name = "buttonRemote";
            this.buttonRemote.Size = new System.Drawing.Size(52, 52);
            this.buttonRemote.TabIndex = 52;
            this.toolTip1.SetToolTip(this.buttonRemote, global::TSU.Properties.Resources.T_FORCE_TO_UPDATE_THE_STATUS);
            this.buttonRemote.UseVisualStyleBackColor = false;
            this.buttonRemote.Click += new System.EventHandler(this.On_Button_Remote_Click);
            // 
            // buttonLiveData
            // 
            this.buttonLiveData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonLiveData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.buttonLiveData.BackgroundImage = global::TSU.Properties.Resources.At40x_Live;
            this.buttonLiveData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonLiveData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLiveData.ImageKey = "(none)";
            this.buttonLiveData.Location = new System.Drawing.Point(54, 2);
            this.buttonLiveData.Name = "buttonLiveData";
            this.buttonLiveData.Size = new System.Drawing.Size(52, 52);
            this.buttonLiveData.TabIndex = 51;
            this.toolTip1.SetToolTip(this.buttonLiveData, global::TSU.Properties.Resources.T_FORCE_TO_UPDATE_THE_STATUS);
            this.buttonLiveData.UseVisualStyleBackColor = false;
            this.buttonLiveData.Click += new System.EventHandler(this.On_Button_LiveData_Click);
            // 
            // buttonCamera
            // 
            this.buttonCamera.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCamera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.buttonCamera.BackgroundImage = global::TSU.Properties.Resources.At40x_Camera;
            this.buttonCamera.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonCamera.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCamera.ImageKey = "(none)";
            this.buttonCamera.Location = new System.Drawing.Point(106, 2);
            this.buttonCamera.Name = "buttonCamera";
            this.buttonCamera.Size = new System.Drawing.Size(52, 52);
            this.buttonCamera.TabIndex = 50;
            this.toolTip1.SetToolTip(this.buttonCamera, global::TSU.Properties.Resources.T_FORCE_TO_UPDATE_THE_STATUS);
            this.buttonCamera.UseVisualStyleBackColor = false;
            this.buttonCamera.Click += new System.EventHandler(this.On_Button_Camera_Click);
            // 
            // buttonExtract
            // 
            this.buttonExtract.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExtract.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.buttonExtract.BackgroundImage = global::TSU.Properties.Resources.At40x_Extract;
            this.buttonExtract.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonExtract.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExtract.ImageKey = "(none)";
            this.buttonExtract.Location = new System.Drawing.Point(353, 323);
            this.buttonExtract.Name = "buttonExtract";
            this.buttonExtract.Size = new System.Drawing.Size(28, 50);
            this.buttonExtract.TabIndex = 49;
            this.toolTip1.SetToolTip(this.buttonExtract, global::TSU.Properties.Resources.T_FORCE_TO_UPDATE_THE_STATUS);
            this.buttonExtract.UseVisualStyleBackColor = false;
            this.buttonExtract.Click += new System.EventHandler(this.On_Button_Eject_Click);
            // 
            // panel4SubViews
            // 
            this.panel4SubViews.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4SubViews.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.panel4SubViews.Location = new System.Drawing.Point(2, 0);
            this.panel4SubViews.Name = "panel4SubViews";
            this.panel4SubViews.Size = new System.Drawing.Size(383, 317);
            this.panel4SubViews.TabIndex = 15;
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 23);
            // 
            // pMeasTsu
            // 
            this.pMeasTsu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pMeasTsu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.pMeasTsu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pMeasTsu.Image = ((System.Drawing.Image)(resources.GetObject("pMeasTsu.Image")));
            this.pMeasTsu.Location = new System.Drawing.Point(899, 2);
            this.pMeasTsu.Name = "pMeasTsu";
            this.pMeasTsu.Size = new System.Drawing.Size(18, 18);
            this.pMeasTsu.TabIndex = 36;
            this.pMeasTsu.TabStop = false;
            this.toolTip1.SetToolTip(this.pMeasTsu, "Tracker state for Tsunami");
            // 
            // pMeasLock
            // 
            this.pMeasLock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pMeasLock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.pMeasLock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pMeasLock.Image = ((System.Drawing.Image)(resources.GetObject("pMeasLock.Image")));
            this.pMeasLock.Location = new System.Drawing.Point(877, 2);
            this.pMeasLock.Name = "pMeasLock";
            this.pMeasLock.Size = new System.Drawing.Size(18, 18);
            this.pMeasLock.TabIndex = 37;
            this.pMeasLock.TabStop = false;
            this.toolTip1.SetToolTip(this.pMeasLock, "Tracker look state for Tsunami");
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(-21, 407);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(47, 50);
            this.pictureBox4.TabIndex = 16;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(927, 406);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(47, 50);
            this.pictureBox3.TabIndex = 15;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(926, -30);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(47, 50);
            this.pictureBox2.TabIndex = 14;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-14, -23);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(47, 50);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem2.Text = " ";
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(957, 416);
            this.Controls.Add(this.pMeasLock);
            this.Controls.Add(this.pMeasTsu);
            this.Controls.Add(this.panelGlobal);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.labelModel);
            this.DoubleBuffered = true;
            this.MinimumSize = new System.Drawing.Size(450, 200);
            this.Name = "View";
            this.Padding = new System.Windows.Forms.Padding(2);
            this.Text = "At401";
            this.Load += new System.EventHandler(this.At40xView_Load);
            this.Shown += new System.EventHandler(this.At40xView_Shown);
            this.ResizeBegin += new System.EventHandler(this.At40xView_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.At40xView_ResizeEnd);
            this.Enter += new System.EventHandler(this.View_Enter);
            this.Controls.SetChildIndex(this.labelModel, 0);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            this.Controls.SetChildIndex(this.pictureBox2, 0);
            this.Controls.SetChildIndex(this.pictureBox3, 0);
            this.Controls.SetChildIndex(this.pictureBox4, 0);
            this.Controls.SetChildIndex(this.panelGlobal, 0);
            this.Controls.SetChildIndex(this.pMeasTsu, 0);
            this.Controls.SetChildIndex(this.pMeasLock, 0);
            this.Controls.SetChildIndex(this.buttonForFocusWithTabIndex0, 0);
            this.panelGlobal.ResumeLayout(false);
            this.splitAT_V_SubV.Panel1.ResumeLayout(false);
            this.splitAT_V_SubV.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitAT_V_SubV)).EndInit();
            this.splitAT_V_SubV.ResumeLayout(false);
            this.panelMain.ResumeLayout(false);
            this.panelConnected.ResumeLayout(false);
            this.panelState.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pMeas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureLaserState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureLaser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureNivelState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture_State_Measure_Mode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureHumidity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picturePressure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureTemperature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureCompensatorState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureCompensator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture_State_Beam_State)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture_State_Face)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureInitialisationState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureInitialisation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureSensorIdealTemperatureState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureSensorIdealTemperature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureNivel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBatterySensor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattetyControler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureAdmState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureAdm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureControlerState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture_Controller)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureSensorState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureSensor)).EndInit();
            this.panelCaJoLi.ResumeLayout(false);
            this.panelCaJoLiConnected.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pMeasTsu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pMeasLock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private TSU.Views.TsuPanel panelGlobal;
        private System.Windows.Forms.Label labelModel;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private TSU.Views.TsuPanel panelState;
        private System.Windows.Forms.PictureBox pictureSensorState;
        private System.Windows.Forms.PictureBox pictureSensor;
        private System.Windows.Forms.PictureBox pictureControlerState;
        private System.Windows.Forms.PictureBox picture_Controller;
        private System.Windows.Forms.PictureBox pictureAdmState;
        private System.Windows.Forms.PictureBox pictureAdm;
        private System.Windows.Forms.PictureBox picture_State_Face;
        private System.Windows.Forms.PictureBox pictureInitialisationState;
        private System.Windows.Forms.PictureBox pictureInitialisation;
        private System.Windows.Forms.PictureBox pictureSensorIdealTemperatureState;
        private System.Windows.Forms.PictureBox pictureSensorIdealTemperature;
        private System.Windows.Forms.PictureBox pictureNivel;
        private System.Windows.Forms.PictureBox pictureBatterySensor;
        private System.Windows.Forms.PictureBox pictureBattetyControler;
        private System.Windows.Forms.PictureBox pMeas;
        private System.Windows.Forms.PictureBox picture_State_Beam_State;
        private System.Windows.Forms.Label labelBatteryControlerState;
        private System.Windows.Forms.Label labelBatterySensorState;
        private System.Windows.Forms.PictureBox pictureCompensatorState;
        private System.Windows.Forms.PictureBox pictureCompensator;
        private TSU.Views.TsuPanel panelLog;
        private TSU.Views.TsuPanel panel4SubViews;
        private System.Windows.Forms.PictureBox pictureHumidity;
        private System.Windows.Forms.PictureBox picturePressure;
        private System.Windows.Forms.PictureBox pictureTemperature;
        private System.Windows.Forms.Label labelHumidity;
        private System.Windows.Forms.Label labelPressure;
        private System.Windows.Forms.Label labelTemperatureState;
        private Views.TsuPanel panelMain;
        private System.Windows.Forms.PictureBox picture_State_Measure_Mode;
        internal Views.TsuPanel panelCaJoLi;
        internal System.Windows.Forms.SplitContainer splitAT_V_SubV;
        private System.Windows.Forms.PictureBox pictureNivelState;
        internal System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button buttonRefreshStatus;
        private System.Windows.Forms.PictureBox pictureLaser;
        private System.Windows.Forms.PictureBox pictureLaserState;
        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.Button buttonExtract;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.Panel panelConnected;
        private System.Windows.Forms.Button buttonInit;
        private System.Windows.Forms.Button buttonShift;
        private System.Windows.Forms.Button buttonPrism;
        public System.Windows.Forms.Button buttonLocking;
        private System.Windows.Forms.Button buttonGoto;
        private System.Windows.Forms.Button buttonMeasure;
        private System.Windows.Forms.Button buttonGotoAll;
        private System.Windows.Forms.Panel panelCaJoLiConnected;
        private System.Windows.Forms.Button buttonTimer;
        private System.Windows.Forms.Button buttonJog;
        private System.Windows.Forms.Button buttonRemote;
        private System.Windows.Forms.Button buttonLiveData;
        private System.Windows.Forms.Button buttonCamera;
        public System.Windows.Forms.PictureBox pMeasTsu;
        public System.Windows.Forms.PictureBox pMeasLock;
        private System.Windows.Forms.Button buttonRemoteList;
        public System.Windows.Forms.Button buttonRelease2;
    }
}

