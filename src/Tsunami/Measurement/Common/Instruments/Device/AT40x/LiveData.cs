﻿using EmScon;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Xml.Serialization;
using TSU.Common.Compute;
using TSU.Common.Elements;
using TSU.Polar.GuidedModules.Steps;
using TSU.Views;
using static TSU.Tools.Research;
using DT = TSU.Polar.GuidedModules.Steps.StakeOut.DisplayTypes;
using E = TSU.Common.Elements;
using M = TSU.Common.Measures;
using R = TSU.Properties.Resources;
using Z = TSU.Common.Zones;

namespace TSU.Common.Instruments.Device.AT40x
{

    public class LiveData
    {
        #region enums

        internal enum ValueTypes
        {
            Precise,
            Approximate,
            Old,
        }

        #endregion

        #region propreties

        Module At40xModule;

        public LiveData(Module at40x)
        {
            At40xModule = at40x;
        }

        public bool IsOn
        {
            get
            {
                return GoodMeasurementIsOn || RoughMeasurementIsOn;
            }
        }

        public bool GoodMeasureInProgress;

        public bool GoodMeasurementIsOn;

        public bool RoughMeasurementIsOn;

        public bool StoppingInProgress;
        public bool ModificationInProgress;

        //public DT displayType;

        //public E.Coordinates.CoordinateSystemsTsunamiTypes coordinateType;

        // number of decimal behind the meters
        internal int resolution = 5;

        internal double val1;
        internal double val2;
        internal double val3;


        internal E.Point Point = new E.Point();
        internal E.Coordinates FinalResults = new E.Coordinates();
        internal Polar.Measure lastMeasure = new Polar.Measure();
        internal E.Coordinates lastCoord = new E.Coordinates();



        public bool FindClosest = false;

        internal ValueTypes ValueType;

        internal DateTime lastRunnedTime;

        [XmlIgnore]
        internal Z.Manager ZoneManager
        {
            get
            {
                return At40xModule.ParentInstrumentManager.ParentStationModule.FinalModule.ZoneManager;
            }
        }

        [XmlIgnore]
        public int MeasureCount { get; internal set; }

        #endregion

        #region Events

        public class LiveDataEventArgs : EventArgs
        {
            public LiveData livedata;
            public Polar.Measure Measure;
            public E.Coordinates Coordinates;
        }

        public delegate void LiveDataHandler(object sender, LiveDataEventArgs e);

        public event LiveDataHandler HasStarted;
        public event LiveDataHandler HasStopped;
        public event LiveDataHandler HasFailedToMeasure;
        public event LiveDataHandler HasARoughMeasurement;
        public event LiveDataHandler HasAGoodMeasurement;
        public event LiveDataHandler HasFinishedAGoodMeasurement;
        public event LiveDataHandler LastResultAvailable;


        DsaOptions stateOfShowReflectorAndExtensionBeforeMeasurement;
        public void Started()
        {
            Debug.WriteInConsole("LiveData.Started Beginning");
            DsaFlag temp = DsaFlag.GetByNameOrAdd(At40xModule.FinalModule.DsaFlags, "ShowReflectorAndExtensionBeforeMeasurement");
            stateOfShowReflectorAndExtensionBeforeMeasurement = temp.State;
            DsaFlag.GetByNameOrAdd(At40xModule.FinalModule.DsaFlags, "ShowReflectorAndExtensionBeforeMeasurement").State = DsaOptions.Never; //At40xModule.ShowReflectorAndExtensionBeforeMeasurement = false;
            HasStarted?.Invoke(this, new LiveDataEventArgs() { livedata = this });
            Debug.WriteInConsole("LiveData.Started End");
        }
        private void Stopped()
        {
            Debug.WriteInConsole("LiveData.Stopped Beginning");
            DsaFlag temp = DsaFlag.GetByNameOrAdd(At40xModule.FinalModule.DsaFlags, "ShowReflectorAndExtensionBeforeMeasurement");
            temp.State = stateOfShowReflectorAndExtensionBeforeMeasurement;
            //this.At40xModule._ToBeMeasuredData._Status = new MeasureStateCancel();
            HasStopped?.Invoke(this, new LiveDataEventArgs() { livedata = this });
            Debug.WriteInConsole("LiveData.Stopped End");
        }

        private void FailedToMeasure()
        {
            Debug.WriteInConsole("LiveData.FailedToMeasure Beginning");
            GoodMeasureInProgress = false;

            HasFinishedAGoodMeasurement?.Invoke(this, new LiveDataEventArgs() { livedata = this });
            HasFailedToMeasure?.Invoke(this, new LiveDataEventArgs() { livedata = this });
            Debug.WriteInConsole("LiveData.FailedToMeasure End");
        }

        #endregion


        // triggered by the livedata.ison && ES_SystemStatusChange.ES_SSC_MeasStatus_Ready
        internal void NextLiveMeasure()
        {
            Debug.WriteInConsole("LiveData.NextLiveMeasure Beginning");
            At40xModule.GotoWanted = false;
            At40xModule.denyMeasurement = false;
            try
            {
                if (GoodMeasurementIsOn)
                {
                    if (At40xModule._ToBeMeasureTheodoliteData != null)
                    {
                        if (At40xModule._ToBeMeasureTheodoliteData._Status.GetType() == typeof(M.States.Control)) // Camille is doing a control with livedata on...
                        {
                            return;
                        }
                    }

                    if (At40xModule._status.isTrackerLockedAndReady)
                    {
                        GoodMeasureInProgress = true;
                        At40xModule.Measure();
                    }
                    else
                    {
                        if (!At40xModule.IsInitialized && At40xModule._status.inclinationSensorState == 2)
                            Logs.Log.AddEntryAsError(At40xModule, "At is not initialized");
                    }
                }
            }
            catch (Exception ex)
            {
                Exception deeper = ex;
                while (deeper.InnerException != null)
                {
                    deeper = deeper.InnerException;
                }
                if (deeper is TsuException TSUDeeper)
                {
                    if (TSUDeeper.Id == 00001)
                    {
                        At40xModule.WaitMeasurementDetails();
                        NextLiveMeasure();
                        Debug.WriteInConsole("LiveData.NextLiveMeasure End 1");
                        return;
                    }
                }
                Logs.Log.AddEntryAsError(At40xModule, ex.Message);
            }
            Debug.WriteInConsole("LiveData.NextLiveMeasure End 2");
        }

        internal void OnMeasStatusReady()
        {            
            Debug.WriteInConsole($"LiveData.OnMeasStatusReady Beginning IsOn={IsOn},switchFromFastToRoughMeasurementInProgress={switchFromFastToRoughMeasurementInProgress}");
            // LiveData is on and we are not switching mode
            if (IsOn && !switchFromFastToRoughMeasurementInProgress)
            {
                //if (Debugger.IsAttached) break;
                StartTheRightMeasurement();
            }
            Debug.WriteInConsole("LiveData.OnMeasStatusReady End");
        }

        internal void StartTheRightMeasurement()
        {
            Debug.WriteInConsole("LiveData.StartTheRightMeasurement Beginning");
            if (resolution > 3)
                StartGoodMeasurement();
            else
                StartRoughMeasurementSync();
            Debug.WriteInConsole("LiveData.StartTheRightMeasurement End");

        }

        internal void SwitchFromRoughToFastMeasurement()
        {
            Debug.WriteInConsole("LiveData.SwitchFromRoughToFastMeasurement Beginning");
            StopRoughMeasurementSync();
            StartGoodMeasurement();
            Debug.WriteInConsole("LiveData.SwitchFromRoughToFastMeasurement End");
        }

        LiveDataHandler TodoAtTheEndOfTheActualAcquisition;
        bool switchFromFastToRoughMeasurementInProgress = false;

        internal void SwitchFromFastToRoughMeasurement()
        {
            Debug.WriteInConsole("LiveData.SwitchFromFastToRoughMeasurement Beginning");
            if (GoodMeasureInProgress)
            {
                switchFromFastToRoughMeasurementInProgress = true;

                TodoAtTheEndOfTheActualAcquisition = delegate
                {
                    Debug.WriteInConsole("LiveData.SwitchFromFastToRoughMeasurement delegate Beginning");
                    HasFinishedAGoodMeasurement -= TodoAtTheEndOfTheActualAcquisition;
                    StopGoodMeasurement();
                    StartRoughMeasurementSync();
                    switchFromFastToRoughMeasurementInProgress = false;
                    Debug.WriteInConsole("LiveData.SwitchFromFastToRoughMeasurement delegate End");
                };

                HasFinishedAGoodMeasurement += TodoAtTheEndOfTheActualAcquisition;
            }
            else
            {
                StopGoodMeasurement();
                StartRoughMeasurementSync();
            }
            Debug.WriteInConsole("LiveData.SwitchFromFastToRoughMeasurement End");
        }


        internal void StartRoughMeasurementSync()
        {
            Debug.WriteInConsole("LiveData.StartRoughMeasurementSync Beginning");
            if (At40xModule.InstrumentIsOn && !RoughMeasurementIsOn)
            {
                At40xModule.Lock();
                // ask to send all reflector position (3 time per second)
                TSU.Debug.WriteInConsole("at40x-SYNC-SetLongSystemParameter");
                At40xModule.sync.SetLongSystemParameter(ES_SystemParameter.ES_SP_DisplayReflectorPosition, 1);

                RoughMeasurementIsOn = true;
                Started(); // trigger event
            }
            Debug.WriteInConsole("LiveData.StartRoughMeasurementSync End");
        }

        internal void StartGoodMeasurement()
        {
            Debug.WriteInConsole("LiveData.StartGoodMeasurement Beginning");
            if (RoughMeasurementIsOn) StopRoughMeasurementSync();

            TsuTask t = new TsuTask()
            {
                //   ActionName = "Change Measurement Mode to Fast",
                ActionName = R.T_CHANGE_MEASUREMENT_MODE_TO_FAST,
                mainAction = () =>
                {
                    Debug.WriteInConsole("LiveData.StartGoodMeasurement mainAction Beginning");
                    if (At40xModule.InstrumentIsOn && !GoodMeasurementIsOn)
                    {
                        At40xModule._measurement.SetModeLive();
                        At40xModule.Lock();
                    }
                    Debug.WriteInConsole("LiveData.StartGoodMeasurement mainAction End");
                },
                postAction = () =>
                {
                    Debug.WriteInConsole("LiveData.StartGoodMeasurement postAction Beginning");
                    if (At40xModule._circle._selectedTsu !=  FaceType.Face1) At40xModule.ChangeFace();
                    At40xModule.GotoWanted = false;
                    GoodMeasurementIsOn = true;
                    Started(); // trigger event
                    NextLiveMeasure();
                    Debug.WriteInConsole("LiveData.StartGoodMeasurement postAction End");
                }
            };

            At40xModule.View.RunTasksWithoutWaitingMessage(new List<TsuTask> { t }, "Measure");
            Debug.WriteInConsole("LiveData.StartGoodMeasurement End");
        }

        internal void StopAnyMeasurement()
        {
            Debug.WriteInConsole("LiveData.StopAnyMeasurement Beginning");
            if (GoodMeasurementIsOn) StopGoodMeasurement();

            if (RoughMeasurementIsOn) StopRoughMeasurementSync();
            Debug.WriteInConsole("LiveData.StopAnyMeasurement End");
        }

        internal void StopRoughMeasurementSync()
        {
            Debug.WriteInConsole("LiveData.StopRoughMeasurementSync Beginning");
            try
            {
                TSU.Debug.WriteInConsole("at40x-SYNC-SetLongSystemParameter");
                At40xModule.sync.SetLongSystemParameter(ES_SystemParameter.ES_SP_DisplayReflectorPosition, 0);
                RoughMeasurementIsOn = false;
                Stopped(); // trigger event
            }
            catch (Exception ex)
            {
                RoughMeasurementIsOn = false;
                //   Logs.Log.AddEntryAsError(this.At40xModule, "Cannot stop rough measurement: "+ex.Message);
                Logs.Log.AddEntryAsError(At40xModule, R.T_CANNOT_STOP_ROUGH_MEASUREMENT + ex.Message);
            }
            Debug.WriteInConsole("LiveData.StopRoughMeasurementSync End");
        }

        LiveDataHandler TodoAtTheEndOfTheActualAcquisition2;
        internal void StopGoodMeasurement()
        {
            Debug.WriteInConsole("LiveData.StopGoodMeasurement Beginning");
            Action a = () =>
            {
                Debug.WriteInConsole("LiveData.StopGoodMeasurement a Beginning");
                GoodMeasurementIsOn = false;
                At40xModule._measurement.SetMode(); // restore original mode
                Stopped();
                Debug.WriteInConsole("LiveData.StopGoodMeasurement a Beginning");
            };

            if (GoodMeasureInProgress)
            {
                TodoAtTheEndOfTheActualAcquisition2 = delegate
                {
                    Debug.WriteInConsole("LiveData.StopGoodMeasurement delegate Beginning");
                    HasFinishedAGoodMeasurement -= TodoAtTheEndOfTheActualAcquisition2;
                    HasFinishedAGoodMeasurement = null;
                    a();
                    Debug.WriteInConsole("LiveData.StopGoodMeasurement delegate End");
                };
                HasFinishedAGoodMeasurement += TodoAtTheEndOfTheActualAcquisition2;
            }
            else
            {
                HasFinishedAGoodMeasurement -= TodoAtTheEndOfTheActualAcquisition2;
                if (At40xModule._status.isTrackerLockedAndReady)
                    a();
                else
                    At40xModule.View.actionList.Add(a);
            }
            Debug.WriteInConsole("LiveData.StopGoodMeasurement End");
        }

        public void Stop()
        {
            Debug.WriteInConsole("LiveData.Stop Beginning");
            if (!At40xModule._status.isTrackerBusy)
            {
                GoodMeasurementIsOn = false;
                RoughMeasurementIsOn = false;
                At40xModule.View.WaitingForm.EndAll();
                DeclareLastResultAvailable();

            }
            else
            {
                StopAnyMeasurement();
            }
            if (At40xModule._BeingMeasuredTheodoliteData != null) At40xModule._BeingMeasuredTheodoliteData._Status = new M.States.Cancel();
            //this.At40xModule._ToBeMeasuredData._Status = new MeasureStateUnknown();
            Debug.WriteInConsole("LiveData.Stop End");
        }

        #region measurement


        internal void OnReflectorPosAnswer(object sender, Receiver.ReflectorPosAnswerEventArgs e)
        {
            MeasureCount++;
            Debug.WriteInConsole($"LiveData.OnReflectorPosAnswer Beginning, IsOn={IsOn}, RoughMeasurementIsOn={RoughMeasurementIsOn}");

            if (!IsOn || !RoughMeasurementIsOn)
            {
                At40xModule.sync.SetLongSystemParameter(ES_SystemParameter.ES_SP_DisplayReflectorPosition, 0);
                Debug.WriteInConsole("LiveData.OnReflectorPosAnswer End 1");
                return;
            }
            // slow the process for PLGC and LGC2
            if ((DateTime.UtcNow - lastRunnedTime).TotalMilliseconds > 500)
            {
                lastRunnedTime = DateTime.UtcNow;
                if (IsOn && RoughMeasurementIsOn)
                {
                    val1 = e.ReflectorPosResult.dVal1;
                    val2 = e.ReflectorPosResult.dVal2;
                    val3 = e.ReflectorPosResult.dVal3;
                }
                ValueType = ValueTypes.Approximate;
                CalculateCoordinatesAndOffsets();
                HasARoughMeasurement?.Invoke(this, new LiveDataEventArgs() { livedata = this });
            }
            //this.StartTheRightMeasurement();
            Debug.WriteInConsole("LiveData.OnReflectorPosAnswer End 2");
        }

        internal void OnGoodMeasurement(Polar.Measure m)
        {
            Debug.WriteInConsole($"LiveData.OnGoodMeasurement Beginning, IsOn={IsOn}, StoppingInProgress={StoppingInProgress}");
            MeasureCount++;
            GoodMeasureInProgress = false;
            if (IsOn)
            {

                if (m.Angles.Raw.Vertical.Value > 200)//in case of somehow it measures in face2
                {
                    val1 = m.Angles.Raw.Horizontal.Value + 200;
                    val1 = Survey.Modulo400(val1);
                    val2 = 400 - m.Angles.Raw.Vertical.Value;
                }
                else
                {
                    val1 = m.Angles.Raw.Horizontal.Value;
                    val2 = m.Angles.Raw.Vertical.Value;
                }
                val3 = m.Distance.Raw.Value;

                ValueType = ValueTypes.Precise;
                CalculateCoordinatesAndOffsets();
                HasAGoodMeasurement?.Invoke(this, new LiveDataEventArgs() { livedata = this });
                HasFinishedAGoodMeasurement?.Invoke(this, new LiveDataEventArgs() { livedata = this });
            }
            if (StoppingInProgress)
                Stopped();
            //else
            //StartTheRightMeasurement();
            Debug.WriteInConsole($"LiveData.OnGoodMeasurement End");
        }

        private void CalculateCoordinatesAndOffsets()
        {
            Debug.WriteInConsole("LiveData.CalculateCoordinatesAndOffsets Beginning");
            try
            {
                var displayType = TSU.Tsunami2.Properties.ValueToDisplayType;
                double na = TSU.Tsunami2.Preferences.Values.na;

                if (displayType != DT.Observations)
                {

                    Polar.Measure reconstructedMeasure = GetTemporaryMeasureContainingNewPointAndRelevantOriginalPoint();

                    if (reconstructedMeasure == null)
                        throw new Exception($"{R.T_CANNOT_PROCESS_THE_MEASUREMENT}");

                    FinalResults = StakeOut.Tools.GetRightCoordinates(
                       TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedType._Name, displayType,
                        out string sType)(reconstructedMeasure);

                }
            }
            catch (Exception ex)
            {
                FailedToMeasure();
                FinalResults = new E.Coordinates();
                Logs.Log.AddEntryAsError(At40xModule, ex.Message);
            }
            finally
            {
                if (TSU.Tsunami2.Properties.ValueToDisplayType != DT.Observations)
                {
                    DeclareResultsAvailable(temp, FinalResults);
                }
            }
            Debug.WriteInConsole("LiveData.CalculateCoordinatesAndOffsets End");
        }

        private void DeclareResultsAvailable(Polar.Measure measure, Coordinates coordinates)
        {
            Debug.WriteInConsole("LiveData.DeclareResultsAvailable Beginning");
            lastMeasure = measure;
            lastCoord = coordinates;
            Debug.WriteInConsole("LiveData.DeclareResultsAvailable End");
        }
        private void DeclareLastResultAvailable()
        {
            Debug.WriteInConsole("LiveData.DeclareLastResultAvailable Beginning");
            if (LastResultAvailable != null)
            {
                LastResultAvailable(this, new LiveDataEventArgs() { livedata = this, Measure = lastMeasure, Coordinates = lastCoord });
                LastResultAvailable = null;
            }
            Debug.WriteInConsole("LiveData.DeclareLastResultAvailable End");
        }

        bool simulating = false;
        Polar.Measure temp;
        E.Point previousReferencePoint = null;
        private Polar.Measure GetTemporaryMeasureContainingNewPointAndRelevantOriginalPoint()
        {
            Debug.WriteInConsole("LiveData.GetTemporaryMeasureContainingNewPointAndRelevantOriginalPoint Beginning");
            var temp2 = temp;
            if (simulating)
            {
                Debug.WriteInConsole("LiveData.GetTemporaryMeasureContainingNewPointAndRelevantOriginalPoint End 1");
                return temp2;
            }

            try
            {
                TSU.Polar.Station.Module stm = At40xModule.ParentModule.ParentModule as TSU.Polar.Station.Module;

                if (FindClosest) // based on the last coordinates, move the closest point from the station list to the top
                {
                    if (temp2 != null)
                        MoveClosestPointToTheTopOfTheList(temp2._Point._Coordinates);
                }


                if (At40xModule.ToBeMeasuredData == null)
                {
                    temp2 = new Polar.Measure() { };
                }
                else
                    temp2 = At40xModule.ToBeMeasuredData.Clone() as Polar.Measure;

                // here we should detect if the reference/original point have changed, because we want the pinnedtable to be updated with the last value od the previous point
                {
                    if (temp2._Point != previousReferencePoint)
                        DeclareLastResultAvailable();
                    previousReferencePoint = temp2._Point;
                }

                temp2.Angles = new M.MeasureOfAngles();
                temp2.Distance = new M.MeasureOfDistance();
                temp2.Angles.raw.Horizontal.Value = val1;
                temp2.Angles.raw.Vertical.Value = val2;
                temp2.Distance.Raw.Value = val3;
                temp2._Status = new M.States.Good();
                temp2._Date = DateTime.Now;


                if (temp2._OriginalPoint == null)
                {
                    temp2._OriginalPoint = new E.Point()
                    { 
                        _Name = $"Live-data-point-{temp2._Date:HH-mm-ss-ffffff}", 
                        StorageStatus = Tsunami.StorageStatusTypes.Temp
                    };
                }

                if (temp2._Point == null)
                {
                    temp2._Point = new E.Point()
                    {
                        _Name = $"Live-data-point-{temp2._Date:HH-mm-ss-ffffff}",
                        StorageStatus = Tsunami.StorageStatusTypes.Temp
                    };
                }

                if (temp2.Distance.Reflector._Name == R.String_Unknown)
                {
                    temp2.Distance.Reflector = this.At40xModule.ActualReflector;
                }

                if (Polar.Station.Module.DetermineFullPointLancé(stm.StationTheodolite, stm, temp2, silent: true, deleteLgcFiles: true))
                {
                    temp2.Offsets = Polar.Station.Module.ComputeDifferences(temp2);
                    Point = temp2._Point;
                    Point.Date = DateTime.Now;
                    temp = temp2;
                    Debug.WriteInConsole("LiveData.GetTemporaryMeasureContainingNewPointAndRelevantOriginalPoint End 2");
                    return temp;
                }
                else
                {
                    System.Threading.Thread.Sleep(200);
                    Debug.WriteInConsole("LiveData.GetTemporaryMeasureContainingNewPointAndRelevantOriginalPoint End 3");
                    throw new Exception("Determination of radiated point failed (are you setup?)");
                }
            }
            catch (Exception ex)
            {
                //   throw new Exception(string.Format("{0} {1}", "Failed to compute coordinates", ex.Message));
                Debug.WriteInConsole("LiveData.GetTemporaryMeasureContainingNewPointAndRelevantOriginalPoint End 4");
                throw new Exception($"{R.T_FAILED_TO_COMPUTE_COORDINATES} {ex.Message}", ex);
            }
        }

        private void MoveClosestPointToTheTopOfTheList(E.CoordinatesInAllSystems coordinates)
        {
            Debug.WriteInConsole("LiveData.MoveClosestPointToTheTopOfTheList Beginning");
            if (coordinates != null)
            {
                int closestMeasureIndex = 999;
                int index = 0;
                double smallestOffset = 100000;



                foreach (Polar.Measure item in At40xModule.StationTheodolite.MeasuresToDo)
                {
                    string coordinateTypeToUseForcomparaison = TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedType._Name;
                    E.Coordinates neededCoordinates = null;
                    E.Coordinates measuredCoordinates = null;

                    bool found = false;
                    int count = 0;
                    while (!found)
                    {
                        neededCoordinates = item._OriginalPoint._Coordinates.GetBasedOnType(coordinateTypeToUseForcomparaison);
                        measuredCoordinates = coordinates.GetBasedOnType(coordinateTypeToUseForcomparaison);
                        if (neededCoordinates.AreKnown && measuredCoordinates.AreKnown)
                            found = true;
                        else
                        {
                            coordinateTypeToUseForcomparaison = FinalResults.GetNextPreferedType(coordinateTypeToUseForcomparaison);

                        }
                        count++;
                        if (count > 3) found = true;
                    }

                    if (neededCoordinates != null)
                    {
                        E.Coordinates diff = Analysis.Element.CompareTwoCoordinates(measuredCoordinates, neededCoordinates);
                        double diff3D = Math.Sqrt(Math.Pow(diff.X.Value, 2) + Math.Pow(diff.Y.Value, 2) + Math.Pow(diff.Z.Value, 2));
                        if (diff3D < smallestOffset)
                        {
                            smallestOffset = diff3D;
                            closestMeasureIndex = index;
                        }
                    }
                    index++;
                }

                if (closestMeasureIndex != 999 && closestMeasureIndex != 0)
                {
                    M.Measure item = At40xModule.StationTheodolite.MeasuresToDo[closestMeasureIndex];
                    At40xModule.StationTheodolite.MeasuresToDo.RemoveAt(closestMeasureIndex);
                    At40xModule.StationTheodolite.MeasuresToDo.Insert(0, item);
                    (At40xModule.ParentInstrumentManager.ParentStationModule as TSU.Polar.Station.Module).SendNextMeasureToInstrument();
                    (At40xModule.ParentInstrumentManager.ParentStationModule as TSU.Polar.Station.Module).UpdateView();
                }
            }
            Debug.WriteInConsole("LiveData.MoveClosestPointToTheTopOfTheList End");
        }

        public void Start()
        {
            MeasureCount = 0;
            Debug.WriteInConsole("LiveData.Start Beginning");
            if (!At40xModule.LiveData.IsOn || At40xModule._status.isTrackerLockedAndReady)
            {
                At40xModule.LiveData.ValueType = ValueTypes.Approximate;
                ((TsuView)At40xModule.View).TryAction(
                     StartTheRightMeasurement,
                     //      string.Format("START_LIVE_DATA_IN { 0} mode", this.outputType.ToString()));
                     $"{R.T_START_LIVE_DATA_IN} {TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedType} {R.T_MODE}");
            }
            Debug.WriteInConsole("LiveData.Start End");
        }

        internal void Simulate()
        {
            Debug.WriteInConsole("LiveData.Simulate Beginning");
            simulating = true;
            Timer t = new Timer()
            {
                Enabled = true,
                Interval = 5000,
            };

            t.Tick += delegate
            {
                Debug.WriteInConsole("LiveData.Simulate delegate Beginning");
                Random rnd = new Random();
                temp = new Polar.Measure()
                {
                    Angles = new M.MeasureOfAngles()
                    {
                        Corrected = new Angles() { Horizontal = new DoubleValue(100 + rnd.NextDouble()), Vertical = new DoubleValue(100 + rnd.NextDouble()) },
                        Raw = new Angles() { Horizontal = new DoubleValue(100 + rnd.NextDouble()), Vertical = new DoubleValue(100 + rnd.NextDouble()) }
                    },
                    Distance = new M.MeasureOfDistance()
                    {
                        Corrected = new DoubleValue(3 + 0.001 * rnd.NextDouble()),
                        Raw = new DoubleValue(3 + 0.001 * rnd.NextDouble())
                    }
                };
                CalculateCoordinatesAndOffsets();
                Debug.WriteInConsole("LiveData.Simulate delegate Beginning");
            };
            t.Start();
            Debug.WriteInConsole("LiveData.Simulate End");
        }

        #endregion
    }
}
