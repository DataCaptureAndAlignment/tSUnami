﻿using EmScon;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TSU.ENUM;
using System.Linq;
using TSU.Tools;
using R = TSU.Properties.Resources;
using TSU.Views;
using TSU.Views.Message;
using static TSU.Views.Message.MessageTsu;

namespace TSU.Common.Instruments.Device.AT40x
{
    public class Remote
    {
        Module At40xModule;
        public Strategy _strategy;
        Timer timer;
        RemoteButtonList button;
        bool skipNextAction;
        public enum RemoteButtonList { A, B, C, D }
        public enum RemoteButtonState { Down, Up, Holded, Doubled }

        public class Strategy
        {
            internal Remote _remote;
            public Strategy(Remote remote) { _remote = remote; }
            internal virtual List<string> GetText() { return null; }
            internal virtual void ActionA() { }
            internal virtual void ActionB() { }
            internal virtual void ActionC() { }
            internal virtual void ActionD() { }
        }

        // Management.Measure
        internal class StrategyA : Strategy  
        {
            public StrategyA(Remote remote) : base(remote) { }
            internal override List<string> GetText() { return new List<string> { R.T_MEASURE, R.T_MEASURE, R.T_LOCK, R.T_SKIP, R.T_UNLOCK }; }
            internal override void ActionA() { _remote.At40xModule.GotoWanted = false;  if(_remote.At40xModule.Measure().Success) _remote.At40xModule.ReportMeasurement(); }
            internal override void ActionB() { _remote.At40xModule.Lock(); }
            internal override void ActionC() { _remote.At40xModule._measurement.Cancel(); }
            internal override void ActionD() { _remote.At40xModule._target.Release(); }
        }

        // Jog
        internal class StrategyB : Strategy
        {
            enum AngleFactors { gon_0 = 1, gon_1 = 10, gon_5 = 50, gon_25 = 250, gon_100 = 1000 }
            AngleFactors angleFactor;
            movingDirection direction;
            Timer factorTimer;
            const int MAXIMUM_INTERVAL_IN_MILLIS = 1000;

            public StrategyB(Remote remote) : base(remote)
            {
                factorTimer = new Timer() { Interval = MAXIMUM_INTERVAL_IN_MILLIS };
                factorTimer.Tick += new EventHandler(OnTimeElapsed);
                angleFactor = AngleFactors.gon_0;
            }

            private void OnTimeElapsed(object sender, EventArgs e)
            {
                factorTimer.Stop();
                _remote.At40xModule._motor.MovebyAngle(direction, 1 * (double)angleFactor / 10);
                _remote.At40xModule.View.remoteView.ChangeTexts();
                angleFactor = AngleFactors.gon_0;
            }

            internal override List<string> GetText() { return new List<string> { R.T_JOG, R.T_UP, R.T_RIGHT, R.T_LEFT, R.T_DOWN }; }

            private void IncreaseAngleFactor()
            {
                switch (angleFactor)
                {
                    case AngleFactors.gon_0: angleFactor = AngleFactors.gon_1; break;
                    case AngleFactors.gon_1: angleFactor = AngleFactors.gon_5; break;
                    case AngleFactors.gon_5: angleFactor = AngleFactors.gon_25; break;
                    case AngleFactors.gon_25:
                    case AngleFactors.gon_100:
                    default:
                        angleFactor = AngleFactors.gon_100; break;

                }
                this._remote.At40xModule.View.remoteView.ChangeTitle($"{R.T_WILL_MOVE} {direction} {R.T_BY} {(double)angleFactor / 10} gon");
                factorTimer.Stop();
                factorTimer.Start();
            }
            internal override void ActionA() { this._remote.At40xModule._motor.MovebyDirection(movingDirection.Up); }
            internal override void ActionB() { this._remote.At40xModule._motor.MovebyDirection(movingDirection.Right); }
            internal override void ActionC() { this._remote.At40xModule._motor.MovebyDirection(movingDirection.Left); }
            internal override void ActionD() { this._remote.At40xModule._motor.MovebyDirection(movingDirection.Down); }
        }

        internal class StrategyC : Strategy // Rename
        {
            string name;
            string temp;
            string AllowedChar;
            int positionInTheAllowedChar;
            public StrategyC(Remote remote) : base(remote)
            {
                name = "";
                temp = " < A";
                //            00000000001111111111222222222233333333
                //            01234567890123456789012345678901234567
                AllowedChar = "ABCDEFGHIJKLMNOPQRSTUVWXUZ9876543210._";
                positionInTheAllowedChar = 0;
            }
            internal override List<string> GetText() { return new List<string> { R.T_POINT_NAME, R.T_UP, R.T_DOWN, R.T_BACKSPACE, R.T_VALIDATE }; }
            internal override void ActionA()
            {
                positionInTheAllowedChar++;
                if (positionInTheAllowedChar > AllowedChar.Length - 1) positionInTheAllowedChar = 0;
                char letter = AllowedChar[positionInTheAllowedChar];
                temp = " < " + letter;
            }
            internal override void ActionB()
            {
                positionInTheAllowedChar--;
                if (positionInTheAllowedChar < 0) positionInTheAllowedChar = AllowedChar.Length - 1;
                char letter = AllowedChar[positionInTheAllowedChar];
                temp = " < " + letter;
            }
            internal override void ActionC() { name = name.Substring(0, name.Length - 1); }
            internal override void ActionD()
            {
                name += temp[3];
                temp = " < A";
                positionInTheAllowedChar = 0;
                this._remote.At40xModule.ToBeMeasuredData._Point._Name = name;
            }
        }

        // Jog
        internal class StrategyD : Strategy 
        {
            public StrategyD(Remote remote) : base(remote) { }
            internal override List<string> GetText() { return new List<string> { "LiveData", "Start", "Stop", "Refresh", "Refresh" }; }
            internal override void ActionA() { _remote.At40xModule.LiveData.Start(); }
            internal override void ActionB() { _remote.At40xModule.LiveData.Stop(); }
            internal override void ActionC() { _remote.At40xModule.View.ClearStateAndUpdate(); }
            internal override void ActionD() { _remote.At40xModule.View.ClearStateAndUpdate(); }
        }

        public Remote(Module at40x)
        {
            At40xModule = at40x;
        }

        public enum CommandsList
        {
            Measure,
            OK,
            Cancel,
            Goto_and_measure,
            Goto,
            Lock,
            Unlock,
            Start_Live_Data,
            Stop_Live_Data,
            Control_opening,
            Clear_State_And_Update,
            Jog_up,
            Jog_down,
            Jog_left,
            Jog_right,
        }

        public enum RemoteStrategies
        {
            PredefinedCommand,
            ListCommand,
            None
        }

        public static void LaunchCommand(CommandsList command, Module At40xModule)
        {
            switch (command)
            {
                case CommandsList.Measure:
                    At40xModule.GotoWanted = false;
                    At40xModule.View.LaunchMeasure();
                    break;
                case CommandsList.Goto:
                    At40xModule.Goto();
                    break;
                case CommandsList.Goto_and_measure:
                    At40xModule.GotoWanted = true; 
                    At40xModule.View.LaunchMeasure();
                    break;
                case CommandsList.Lock:
                    At40xModule.Lock();
                    break;
                case CommandsList.Unlock:
                    At40xModule._target.Release();
                    break;
                case CommandsList.Start_Live_Data:
                    At40xModule.LiveData.Start();
                    break;
                case CommandsList.Stop_Live_Data:
                    At40xModule.LiveData.Stop();
                    break;
                case CommandsList.Jog_up:
                    At40xModule._motor.MovebyAngle(movingDirection.Up, 10);
                    break;
                case CommandsList.Jog_right:
                    At40xModule._motor.MovebyAngle(movingDirection.Right, 10);
                    break;
                case CommandsList.Jog_down:
                    At40xModule._motor.MovebyAngle(movingDirection.Down, 10);
                    break;
                case CommandsList.Jog_left:
                    At40xModule._motor.MovebyAngle(movingDirection.Left, 10);
                    break;
                case CommandsList.Control_opening:
                    TryControlOpening(At40xModule);
                    break;
                case CommandsList.Clear_State_And_Update:
                    At40xModule.View.ClearStateAndUpdate();
                    break;
                case CommandsList.OK: TryToClickButtonByText(new List<string>() { "OK", "continue" }); break;
                default:
                case CommandsList.Cancel: TryToCancel(At40xModule); break;

            }
        }

        private static void TryToCancel(Module atModule)
        {
            TryToClickButtonByText(new List<string>() { "CANCEL", "Cancel" });
            if (atModule.beingMeasured != null)
                atModule._measurement.Cancel();
        }

        private static void TryToClickButtonByText(List<string> texts)
        {
            if (Tsunami2.View.InvokeRequired)
            {
                // Invoke the event handler on the main UI thread
                Tsunami2.View.BeginInvoke(new System.Action(() => TryToClickButtonByText(texts)));
            }
            else
            {
                MessageTsu mt = TsunamiView.ExistingMessages.FindLast(x => x.Visible = true);
                if (mt != null)
                {
                    foreach (var text in texts)
                    {
                        Action a = mt.GetButtonClick(text);
                        if (a != null)
                        {
                            a();
                            return;
                        }
                        MessageResponseButton mrb = mt.GetButtonByName(text);
                        if (mrb != null)
                        {
                            mrb.OnClickBeforeClosing();
                            return;
                        }

                        if (FindButtonByTextInForm(mt.Controls, text, out Button b))
                        {
                            mt.PerformButtonClick(b);
                            return;
                        }

                        // seems we have an old message...
                        if (mt.button1.Text.ToUpper() == text.ToUpper()) mt.PerformButtonClick(mt.button1);
                        else if (mt.button2.Text.ToUpper() == text.ToUpper()) mt.PerformButtonClick(mt.button3);
                        else if (mt.button3.Text.ToUpper() == text.ToUpper()) mt.PerformButtonClick(mt.button3);
                    }
                }
            }
        }

        private static bool FindButtonByTextInForm(Control.ControlCollection controls, string text, out Button b)
        {
            b = null;
            foreach (Control item in controls)
            {
                if (item is Button button)
                {
                    if (button.Text.ToUpper() == text.ToUpper())
                    {
                        b = button;
                        return true;
                    }
                }
            }
            foreach (Control item in controls)
            {
                if (FindButtonByTextInForm(item.Controls, text, out Button button))
                {
                    b = button;
                    return true;
                }
            }
            return false;
        }

        private static void TryControlOpening(Module atModule)
        {
            if (atModule.ParentInstrumentManager.ParentStationModule is Polar.Station.Module psm)
            {
                psm.SetNextPointToOpeningPoint();
                atModule.GotoWanted = true;
                atModule.View.LaunchMeasure();
            }
            else
            {
                Logs.Log.AddEntryAsPayAttentionOf(atModule, "Cannot start the control opening, please do it on the computer");
            }
        }


        internal List<string> GetText()
        {
            List<string> l = new List<string>();
            l.Add("Predefined commands");
            l.Add(ButtonA1Command.ToString());
            l.Add(ButtonB3Command.ToString());
            l.Add(ButtonC2Command.ToString());
            l.Add(ButtonD4Command.ToString());
            return l;
        }


        public void RemoteButtonPressed(RemoteButtonList b, RemoteButtonState s)
        {
            // remark: if the button is hold it will be like pressed several time (up and down) so we count how many time it goes trough 'down' beeing already pressed and we skip the same amount of 'up'
            button = b;
            switch (s)
            {
                case RemoteButtonState.Down:
                    timer.Stop();
                    timer.Start();
                    break;
                case RemoteButtonState.Up: // apply button action
                    timer.Stop();
                    if (skipNextAction) // this will avoid to mae the action when lrelease the pressure from the holded button
                    {
                        skipNextAction = false;
                    }
                    else
                    {
                        switch (b)
                        {
                            case RemoteButtonList.A: _strategy.ActionA(); break;
                            case RemoteButtonList.B: _strategy.ActionB(); break;
                            case RemoteButtonList.C: _strategy.ActionC(); break;
                            case RemoteButtonList.D: _strategy.ActionD(); break;
                            default: break;
                        }
                    }
                    break;
                case RemoteButtonState.Holded: // change strategy
                    switch (b)
                    {
                        case RemoteButtonList.A: _strategy = new StrategyA(this); break;
                        case RemoteButtonList.B: _strategy = new StrategyB(this); break;
                        case RemoteButtonList.C: _strategy = new StrategyC(this); break;
                        case RemoteButtonList.D: _strategy = new StrategyD(this); break;
                        default: break;
                    }


                    this.At40xModule.View.remoteView.ChangeTexts();
                    break;
                default:
                    break;
            }
        }



        /// <summary>
        /// WIll check if the button was pressed for more than x secondes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPressedForLongEnough(object sender, EventArgs e)
        {
            this.At40xModule.View.remoteView.On_RemoteButtonPressed(button, RemoteButtonState.Holded);
            skipNextAction = true;
            timer.Stop();
        }

        private void GetButtonIdAndState(ES_SystemStatusChange changeEvent, out RemoteButtonList button, out RemoteButtonState state)
        {
            switch (changeEvent)
            {
                case ES_SystemStatusChange.ES_SSC_ProbeButton1Down:
                    button = RemoteButtonList.A; state = RemoteButtonState.Down; break;
                case ES_SystemStatusChange.ES_SSC_ProbeButton1Up:
                    button = RemoteButtonList.A; state = RemoteButtonState.Up; break;
                case ES_SystemStatusChange.ES_SSC_ProbeButton2Down:
                    button = RemoteButtonList.C; state = RemoteButtonState.Down; break;
                case ES_SystemStatusChange.ES_SSC_ProbeButton2Up:
                    button = RemoteButtonList.C; state = RemoteButtonState.Up; break;
                case ES_SystemStatusChange.ES_SSC_ProbeButton3Down:
                    button = RemoteButtonList.B; state = RemoteButtonState.Down; break;
                case ES_SystemStatusChange.ES_SSC_ProbeButton3Up:
                    button = RemoteButtonList.B; state = RemoteButtonState.Up; break;
                case ES_SystemStatusChange.ES_SSC_ProbeButton4Down:
                    button = RemoteButtonList.D; state = RemoteButtonState.Down; break;
                case ES_SystemStatusChange.ES_SSC_ProbeButton4Up:
                    button = RemoteButtonList.D; state = RemoteButtonState.Up; break;
                case ES_SystemStatusChange.ES_SSC_ProbeButton1DoubleClick:
                    button = RemoteButtonList.A; state = RemoteButtonState.Doubled; break;
                case ES_SystemStatusChange.ES_SSC_ProbeButton2DoubleClick:
                    button = RemoteButtonList.C; state = RemoteButtonState.Doubled; break;
                case ES_SystemStatusChange.ES_SSC_ProbeButton3DoubleClick:
                    button = RemoteButtonList.B; state = RemoteButtonState.Doubled; break;
                case ES_SystemStatusChange.ES_SSC_ProbeButton4DoubleClick:
                    button = RemoteButtonList.D; state = RemoteButtonState.Doubled; break;
                default:
                    button = RemoteButtonList.A; state = RemoteButtonState.Down; break;
            }
        }
        private void TreatCommandList(RemoteButtonList button, RemoteButtonState state)
        {
            CommandsList command = CurrentCommandFromList;
            switch (button)
            {
                case RemoteButtonList.A:
                    // we laucnh the command
                    LaunchCommand(command, this.At40xModule);
                    break;
                case RemoteButtonList.B:
                    // we set the command to the next command in the enum
                    CurrentCommandFromList = command.Next().Next();
                    this.At40xModule.View.remoteListView.Update(CurrentCommandFromList);
                    break;
                case RemoteButtonList.C:
                    // we set the command to the previous command in the enum
                    CurrentCommandFromList = command.Previous().Previous();
                    this.At40xModule.View.remoteListView.Update(CurrentCommandFromList);
                    break;
                case RemoteButtonList.D:
                    // we launch the next command
                    LaunchCommand(command.Next(), this.At40xModule);
                    break;
                default:
                    break;
            }
        }

        internal void On_ProbeButton(ES_SystemStatusChange changeEvent)
        {
            GetButtonIdAndState(changeEvent, out RemoteButtonList buttonPressed, out RemoteButtonState state);

            Debug.WriteInConsole($"changeEvent={changeEvent},button={buttonPressed},state={state},CurrentStrategy={CurrentStrategy}");

            if (state != RemoteButtonState.Down) return;

            switch (CurrentStrategy)
            {
                case RemoteStrategies.PredefinedCommand:
                    LaunchCommand(GetButtonCommand(buttonPressed), At40xModule);
                    break;
                case RemoteStrategies.ListCommand:
                    TreatCommandList(buttonPressed, state);
                    break;
                case RemoteStrategies.None:
                default:
                    break;
            }
        }



        internal RemoteStrategies CurrentStrategy = RemoteStrategies.None;

        internal CommandsList CurrentCommandFromList = CommandsList.Measure;

        internal CommandsList ButtonA1Command = CommandsList.Measure;
        internal CommandsList ButtonC2Command = CommandsList.Goto;
        internal CommandsList ButtonB3Command = CommandsList.Lock;
        internal CommandsList ButtonD4Command = CommandsList.Cancel;


        private CommandsList GetButtonCommand(RemoteButtonList button)
        {
            switch (button)
            {
                case RemoteButtonList.A: return ButtonA1Command;
                case RemoteButtonList.B: return ButtonB3Command;
                case RemoteButtonList.C: return ButtonC2Command;
                case RemoteButtonList.D: return ButtonD4Command;
                default: return CommandsList.Cancel;
            }
        }

        /// <summary>
        /// Will set the time for the lazer to start warming
        /// </summary>
        /// <param name="dateTime"></param>
        public void SetWarmingTimer(DateTime dateTime)
        {
            if (!At40xModule.InstrumentIsOn) throw new Exception($"{R.T_INSTRUMENT_IS_OFF}?");
            double hours = (dateTime - DateTime.Now).TotalHours;
            int iH, iM;
            iH = (int)hours;
            hours = hours - iH;
            double temp = hours / 100 * 6000;
            iM = (int)temp;
            if (iM == 0) iM = 1;

            Debug.WriteInConsole("at40x-SYNC-SetLaserOnTimer");
            At40xModule.sync.SetLaserOnTimer(iH, iM);
            Logs.Log.AddEntryAsSuccess(this.At40xModule, $"{R.T_WAKE_UP_SCHEDULED_IN} {iH} {R.T_HOUR}s {R.T_AND} {iM} {R.T_MINUTE}.");
        }
    }
}
