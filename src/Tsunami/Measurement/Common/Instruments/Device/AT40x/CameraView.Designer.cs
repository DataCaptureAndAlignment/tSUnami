﻿namespace TSU.Common.Instruments.Device.AT40x
{
    partial class CameraView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonMoreB = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonLessB = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonMoreC = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonLessC = new System.Windows.Forms.ToolStripButton();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.Right;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonMoreB,
            this.toolStripButtonLessB,
            this.toolStripButtonMoreC,
            this.toolStripButtonLessC});
            this.toolStrip2.Location = new System.Drawing.Point(377, 5);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Padding = new System.Windows.Forms.Padding(1);
            this.toolStrip2.Size = new System.Drawing.Size(62, 302);
            this.toolStrip2.TabIndex = 16;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripButtonMoreB
            // 
            this.toolStripButtonMoreB.AutoSize = false;
            this.toolStripButtonMoreB.BackColor = System.Drawing.Color.Transparent;
            this.toolStripButtonMoreB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStripButtonMoreB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonMoreB.Image = global::TSU.Properties.Resources.At40x_Camera___Bmore;
            this.toolStripButtonMoreB.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonMoreB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonMoreB.Name = "toolStripButtonMoreB";
            this.toolStripButtonMoreB.Size = new System.Drawing.Size(60, 60);
            this.toolStripButtonMoreB.Click += new System.EventHandler(this.toolStripButtonMoreB_Click);
            // 
            // toolStripButtonLessB
            // 
            this.toolStripButtonLessB.AutoSize = false;
            this.toolStripButtonLessB.BackColor = System.Drawing.Color.Transparent;
            this.toolStripButtonLessB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStripButtonLessB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonLessB.Image = global::TSU.Properties.Resources.At40x_Camera___Bless;
            this.toolStripButtonLessB.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonLessB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLessB.Name = "toolStripButtonLessB";
            this.toolStripButtonLessB.Size = new System.Drawing.Size(60, 60);
            this.toolStripButtonLessB.Click += new System.EventHandler(this.toolStripButtonLessB_Click);
            // 
            // toolStripButtonMoreC
            // 
            this.toolStripButtonMoreC.AutoSize = false;
            this.toolStripButtonMoreC.BackColor = System.Drawing.Color.Transparent;
            this.toolStripButtonMoreC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStripButtonMoreC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonMoreC.Image = global::TSU.Properties.Resources.At40x_Camera___Cmore;
            this.toolStripButtonMoreC.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonMoreC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonMoreC.Name = "toolStripButtonMoreC";
            this.toolStripButtonMoreC.Size = new System.Drawing.Size(60, 60);
            this.toolStripButtonMoreC.Click += new System.EventHandler(this.toolStripButtonMoreC_Click);
            // 
            // toolStripButtonLessC
            // 
            this.toolStripButtonLessC.AutoSize = false;
            this.toolStripButtonLessC.BackColor = System.Drawing.Color.Transparent;
            this.toolStripButtonLessC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStripButtonLessC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonLessC.Image = global::TSU.Properties.Resources.At40x_Camera___Cless;
            this.toolStripButtonLessC.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonLessC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLessC.Name = "toolStripButtonLessC";
            this.toolStripButtonLessC.Size = new System.Drawing.Size(60, 60);
            this.toolStripButtonLessC.Click += new System.EventHandler(this.toolStripButtonLessC_Click);
            // 
            // CameraView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 312);
            this.Controls.Add(this.toolStrip2);
            this.Name = "CameraView";
            this.Controls.SetChildIndex(this.toolStrip2, 0);
            this.Controls.SetChildIndex(this.buttonForFocusWithTabIndex0, 0);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolStripButtonMoreB;
        private System.Windows.Forms.ToolStripButton toolStripButtonLessB;
        private System.Windows.Forms.ToolStripButton toolStripButtonMoreC;
        private System.Windows.Forms.ToolStripButton toolStripButtonLessC;
    }
}
