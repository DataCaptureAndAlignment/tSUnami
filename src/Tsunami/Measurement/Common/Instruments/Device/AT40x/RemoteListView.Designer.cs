﻿using TSU.Views;
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments.Device.AT40x
{
    partial class RemoteListView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelProbe = new TSU.Views.TsuPanel();
            this.labelProbeD = new System.Windows.Forms.Label();
            this.labelProbeC = new System.Windows.Forms.Label();
            this.labelProbeB = new System.Windows.Forms.Label();
            this.labelProbeA = new System.Windows.Forms.Label();
            this.panelProbe.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelProbe
            // 
            this.panelProbe.BackColor = System.Drawing.Color.Transparent;
            this.panelProbe.Controls.Add(this.labelProbeD);
            this.panelProbe.Controls.Add(this.labelProbeC);
            this.panelProbe.Controls.Add(this.labelProbeB);
            this.panelProbe.Controls.Add(this.labelProbeA);
            this.panelProbe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelProbe.Location = new System.Drawing.Point(5, 5);
            this.panelProbe.Margin = new System.Windows.Forms.Padding(10);
            this.panelProbe.Name = "panelProbe";
            this.panelProbe.Padding = new System.Windows.Forms.Padding(10);
            this.panelProbe.Size = new System.Drawing.Size(308, 254);
            this.panelProbe.TabIndex = 20;
            // 
            // labelProbeD
            // 
            this.labelProbeD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelProbeD.BackColor = System.Drawing.Color.Transparent;
            this.labelProbeD.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProbeD.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelProbeD.Location = new System.Drawing.Point(6, 146);
            this.labelProbeD.Name = "labelProbeD";
            this.labelProbeD.Size = new System.Drawing.Size(302, 98);
            this.labelProbeD.TabIndex = 4;
            this.labelProbeD.Text = "D";
            this.labelProbeD.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelProbeC
            // 
            this.labelProbeC.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelProbeC.BackColor = System.Drawing.Color.Transparent;
            this.labelProbeC.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelProbeC.Location = new System.Drawing.Point(16, 118);
            this.labelProbeC.Name = "labelProbeC";
            this.labelProbeC.Size = new System.Drawing.Size(75, 23);
            this.labelProbeC.TabIndex = 3;
            this.labelProbeC.Text = "C";
            this.labelProbeC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelProbeB
            // 
            this.labelProbeB.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelProbeB.BackColor = System.Drawing.Color.Transparent;
            this.labelProbeB.ForeColor = System.Drawing.Color.MistyRose;
            this.labelProbeB.Location = new System.Drawing.Point(227, 123);
            this.labelProbeB.Name = "labelProbeB";
            this.labelProbeB.Size = new System.Drawing.Size(68, 13);
            this.labelProbeB.TabIndex = 2;
            this.labelProbeB.Text = "B";
            this.labelProbeB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelProbeA
            // 
            this.labelProbeA.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelProbeA.BackColor = System.Drawing.Color.Transparent;
            this.labelProbeA.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProbeA.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelProbeA.Location = new System.Drawing.Point(13, 10);
            this.labelProbeA.Name = "labelProbeA";
            this.labelProbeA.Size = new System.Drawing.Size(282, 103);
            this.labelProbeA.TabIndex = 1;
            this.labelProbeA.Text = "A";
            this.labelProbeA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RemoteListView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 264);
            this.Controls.Add(this.panelProbe);
            this.Name = "RemoteListView";
            this.Controls.SetChildIndex(this.panelProbe, 0);
            this.Controls.SetChildIndex(this.buttonForFocusWithTabIndex0, 0);
            this.panelProbe.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Views.TsuPanel panelProbe;
        private System.Windows.Forms.Label labelProbeD;
        private System.Windows.Forms.Label labelProbeB;
        private System.Windows.Forms.Label labelProbeA;
        private System.Windows.Forms.Label labelProbeC;
    }
}
