﻿using EmScon;
using System;
using System.Threading;

namespace TSU.Common.Instruments.Device.AT40x
{
    public class Sync
    {
        readonly Module module;

        #region Command synchronization : memorize the command we send and wait for the matching answer

        private ES_Command syncCallInProgress = ES_Command.ES_C_Unknown;
        private bool dataRequired;
        private bool returnOK;
        private string errorMessage;

        /// <summary>
        /// show if something is running sync if not: unknown
        /// </summary>
        public ES_Command SyncCallInProgress
        {
            get
            {
                return syncCallInProgress;
            }
            set
            {
                if (value != ES_Command.ES_C_Unknown && syncCallInProgress != ES_Command.ES_C_Unknown)
                {
                    throw new AsyncException($"{syncCallInProgress} synchronous call is in progress!, {value} is cancelled");
                }
                else
                {
                    Debug.WriteInConsole($"syncCallInProgress changed from {syncCallInProgress} to {value}");
                    syncCallInProgress = value;
                }
            }
        }

        // Event raised when a response to nameOfSyncCallInProgress is received
        private readonly ManualResetEvent receivedEvent = new ManualResetEvent(false);

        private Command InitCommand(ES_Command esCommand, bool dataRequired)
        {
            Debug.WriteInConsole($"InitCommand command={esCommand}, dataRequired={dataRequired}");

            SyncCallInProgress = esCommand;
            this.dataRequired = dataRequired;
            receivedEvent.Reset();
            return new Command(module);
        }

        private void WaitForAnswer(int millisecondsTimeout = 10000)
        {
            if (receivedEvent.WaitOne(millisecondsTimeout, true))
            {
                Debug.WriteInConsole($"Wait for {syncCallInProgress} is over.");
                if (!returnOK)
                {
                    throw new Exception(errorMessage);
                }
            }
            else
            {
                string message = $"AT40x TimeOut on {syncCallInProgress}";
                Debug.WriteInConsole($"{message}");
                throw new Exception(message);
            }
        }

        private void OnCommandAnswer(object sender, Receiver.CommandAnswerEventArgs e)
        {
            Debug.WriteInConsole($"Received command answer; command={e.command}, result={e.status}, syncCallInProgress={syncCallInProgress}");

            // If the message received is not the answer to the command we are waiting for, ignore it.
            if (e.command != syncCallInProgress)
                return;

            // Error
            if (e.status != ES_ResultStatus.ES_RS_AllOK)
            {
                returnOK = false;
                errorMessage = $"Error {syncCallInProgress} returned {Errors.GetErrorTextFromNumber((int)e.status)}";
                Debug.WriteInConsole(errorMessage);
                receivedEvent.Set();
            }
            else
            {
                returnOK = true;
                errorMessage = string.Empty;
            }

            // If the command has no data, we are ready to continue.
            // Otherwise, the Set() will be done by the data treatment
            if (!dataRequired)
                receivedEvent.Set();
        }

        #endregion

        public Sync(Module module)
        {
            this.module = module;
        }

        public void UnsubscribeEvents()
        {
            module.receiver.CommandAnswerEvent -= OnCommandAnswer;
            module.receiver.GetCameraParamsAnswerEvent -= OnGetCameraParamsAnswer;
            module.receiver.GetDirectionsAnswerEvent -= OnGetDirectionsAnswer;
            module.receiver.GetSystemSoftwareVersionAnswerEvent -= OnGetSystemSoftwareVersionAnswer;
            module.receiver.GetEnvironmentParamsAnswerEvent -= OnGetEnvironmentParamsAnswer;
            module.receiver.GetSystemStatusAnswerEvent -= OnGetSystemStatusAnswer;
            module.receiver.GetTrackerStatusAnswerEvent -= OnGetTrackerStatusAnswer;
            module.receiver.NivelMeasurementAnswerEvent -= OnNivelMeasurementAnswer;
            module.receiver.GetMeasurementStatusInfoAnswerEvent -= OnGetMeasurementStatusInfoAnswer;
            module.receiver.GetLongSystemParamAnswerEvent -= OnGetLongSystemParamAnswer;
            module.receiver.GetFaceAnswerEvent -= OnGetFaceAnswer;
            module.receiver.GetMeasurementModeAnswerEvent -= OnGetMeasurementModeAnswer;
            module.receiver.GetProbeAnswerEvent -= OnGetProbeAnswer;
            module.receiver.GetTipAdapterAnswerEvent -= OnGetTipAdapterAnswer;
            module.receiver.GetMeasurementProbeInfoAnswerEvent -= OnGetMeasurementProbeInfoAnswer;
            module.receiver.GetUnitsAnswerEvent -= OnGetUnitsAnswer;
            module.receiver.GetStatisticModeAnswerEvent -= OnGetStatisticModeAnswer;
            module.receiver.GetCoordinateSystemTypeAnswerEvent -= OnGetCoordinateSystemTypeAnswer;
        }

        public void SubscribeEvents()
        {
            module.receiver.CommandAnswerEvent += OnCommandAnswer;
            module.receiver.GetCameraParamsAnswerEvent += OnGetCameraParamsAnswer;
            module.receiver.GetDirectionsAnswerEvent += OnGetDirectionsAnswer;
            module.receiver.GetSystemSoftwareVersionAnswerEvent += OnGetSystemSoftwareVersionAnswer;
            module.receiver.GetEnvironmentParamsAnswerEvent += OnGetEnvironmentParamsAnswer;
            module.receiver.GetSystemStatusAnswerEvent += OnGetSystemStatusAnswer;
            module.receiver.GetTrackerStatusAnswerEvent += OnGetTrackerStatusAnswer;
            module.receiver.NivelMeasurementAnswerEvent += OnNivelMeasurementAnswer;
            module.receiver.GetMeasurementStatusInfoAnswerEvent += OnGetMeasurementStatusInfoAnswer;
            module.receiver.GetLongSystemParamAnswerEvent += OnGetLongSystemParamAnswer;
            module.receiver.GetFaceAnswerEvent += OnGetFaceAnswer;
            module.receiver.GetMeasurementModeAnswerEvent += OnGetMeasurementModeAnswer;
            module.receiver.GetProbeAnswerEvent += OnGetProbeAnswer;
            module.receiver.GetTipAdapterAnswerEvent += OnGetTipAdapterAnswer;
            module.receiver.GetMeasurementProbeInfoAnswerEvent += OnGetMeasurementProbeInfoAnswer;
            module.receiver.GetUnitsAnswerEvent += OnGetUnitsAnswer;
            module.receiver.GetStatisticModeAnswerEvent += OnGetStatisticModeAnswer;
            module.receiver.GetCoordinateSystemTypeAnswerEvent += OnGetCoordinateSystemTypeAnswer;
        }

        //    if (!myTask.Wait(timeoutMilliseconds, cancellationTokenSource.Token))
        //    {
        //        // Timeout occurred
        //        string message = $"The operation: {actionName} timed out.";
        //        TSU.Debug.WriteInConsole(message);
        //        Logs.Log.AddEntryAsError(this.module, message);
        //        cancellationTokenSource.Cancel();
        //    }
        //    else if (myTask.IsFaulted)
        //    {
        //        // Handle the case where the task encountered an exception
        //        TSU.Debug.WriteInConsole("An error occurred: " + myTask.Exception?.Message);
        //        throw myTask.Exception;
        //    }
        //    else
        //    {
        //        // Task completed successfully
        //        string message = $"The operation: {actionName} completed successfully.";
        //        TSU.Debug.WriteInConsole(message);
        //        Logs.Log.AddEntryAsSuccess(this.module, message);
        //    }
        //}

        public void ActivateCameraView()
        {
            try
            {
                InitCommand(ES_Command.ES_C_ActivateCameraView, false).ActivateCameraView();
                WaitForAnswer(5000);
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not Activate the Camera View;{ex.Message}");
            }
        }

        public void SetCameraParams(int contrast, int brightness, int saturation)
        {
            try
            {
                InitCommand(ES_Command.ES_C_SetCameraParams, false).SetCameraParams(contrast, brightness, saturation);
                WaitForAnswer(5000);
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not Set the Camera parameters;{ex.Message}");
            }
        }

        public void SetReflector(int refID)
        {
            try
            {
                InitCommand(ES_Command.ES_C_SetReflector, false).SetReflector(refID);
                WaitForAnswer(5000);
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not Set the Camera parameters;{ex.Message}");
            }
        }

        public void GetCameraParams(out int contrast, out int brightness, out int saturation)
        {
            try
            {
                InitCommand(ES_Command.ES_C_GetCameraParams, true).GetCameraParams();
                WaitForAnswer();
                contrast = getCameraParamsAnswerEventArgs.CameraParamsData.iContrast;
                brightness = getCameraParamsAnswerEventArgs.CameraParamsData.iBrightness;
                saturation = getCameraParamsAnswerEventArgs.CameraParamsData.iSaturation;
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not Get the Camera Param;{ex.Message}");
            }
        }

        private Receiver.GetCameraParamsAnswerEventArgs getCameraParamsAnswerEventArgs;

        private void OnGetCameraParamsAnswer(object sender, Receiver.GetCameraParamsAnswerEventArgs e)
        {
            getCameraParamsAnswerEventArgs = e;
            if (syncCallInProgress == ES_Command.ES_C_GetCameraParams)
                receivedEvent.Set();
        }

        public void GetDirection(out double h, out double v)
        {
            try
            {
                InitCommand(ES_Command.ES_C_GetDirection, true).GetDirection();
                WaitForAnswer();
                h = getDirectionsAnswerEventArgs.DHzAngle;
                v = getDirectionsAnswerEventArgs.DVtAngle;
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not Get directions;{ex.Message}");
            }
        }

        private Receiver.GetDirectionsAnswerEventArgs getDirectionsAnswerEventArgs;

        private void OnGetDirectionsAnswer(object sender, Receiver.GetDirectionsAnswerEventArgs e)
        {
            getDirectionsAnswerEventArgs = e;
            if (syncCallInProgress == ES_Command.ES_C_GetDirection)
                receivedEvent.Set();
        }

        public void PointLaserHVD(double h, double v, double d)
        {
            try
            {
                InitCommand(ES_Command.ES_C_PointLaserHVD, false).PointLaserHVD(h, v, d);
                WaitForAnswer();
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not point laser at;{ex.Message}");
            }
        }

        public void GetSystemSoftwareVersion(out string v)
        {
            try
            {
                InitCommand(ES_Command.ES_C_GetSystemSoftwareVersion, true).GetSystemSoftwareVersion();
                WaitForAnswer();
                v = getSystemSoftwareVersionAnswerEventArgs.SystemSoftwareVersion;
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not Get software version;{ex.Message}");
            }
        }

        private Receiver.GetSystemSoftwareVersionAnswerEventArgs getSystemSoftwareVersionAnswerEventArgs;

        private void OnGetSystemSoftwareVersionAnswer(object sender, Receiver.GetSystemSoftwareVersionAnswerEventArgs e)
        {
            getSystemSoftwareVersionAnswerEventArgs = e;
            if (syncCallInProgress == ES_Command.ES_C_GetSystemSoftwareVersion)
                receivedEvent.Set();
        }

        public void SetSearchParams(double radius, int timeout)
        {
            try
            {
                InitCommand(ES_Command.ES_C_SetSearchParams, false).SetSearchParams(new CESCSAPICommand.SearchParamsDataT()
                {
                    dSearchRadius = radius,
                    lTimeOut = timeout
                });
                WaitForAnswer();
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not set serach parameters;{ex.Message}");
            }
        }

        public void GetEnvironmentParams(out double temp, out double press, out double hum)
        {
            try
            {
                InitCommand(ES_Command.ES_C_GetEnvironmentParams, true).GetEnvironmentParams();
                WaitForAnswer();
                temp = getEnvironmentParamsAnswerEventArgs.EnvironmentDataT.dTemperature;
                press = getEnvironmentParamsAnswerEventArgs.EnvironmentDataT.dPressure;
                hum = getEnvironmentParamsAnswerEventArgs.EnvironmentDataT.dHumidity;
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not get environnement parameters;{ex.Message}");
            }
        }

        private Receiver.GetEnvironmentParamsAnswerEventArgs getEnvironmentParamsAnswerEventArgs;

        private void OnGetEnvironmentParamsAnswer(object sender, Receiver.GetEnvironmentParamsAnswerEventArgs e)
        {
            getEnvironmentParamsAnswerEventArgs = e;
            if (syncCallInProgress == ES_Command.ES_C_GetEnvironmentParams)
                receivedEvent.Set();
        }

        public void Initialize()
        {
            try
            {
                InitCommand(ES_Command.ES_C_Initialize, false).Initialize();
                WaitForAnswer(5 * 60 * 1000);
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not initialize;{ex.Message}");
            }
        }

        public void GetSystemStatus(
                            out ES_ResultStatus LastResultStatus,
                            out ES_TrackerProcessorStatus TrackerProcessorStatus,
                            out ES_LaserProcessorStatus LaserStatus,
                            out ES_ADMStatus AdmStatus,
                            out CESCSAPIReceive.ESVersionNumberT esVersionNumber,
                            out ES_WeatherMonitorStatus WeatherMonitorStatus,
                            out int lFlagsValue,
                            out int lTrackerSerialNumber)
        {
            try
            {
                InitCommand(ES_Command.ES_C_GetSystemStatus, true).GetSystemStatus();
                WaitForAnswer(5000);
                LastResultStatus = getSystemStatusAnswerEventArgs.LastResultStatus;
                TrackerProcessorStatus = getSystemStatusAnswerEventArgs.TrackerProcessorStatus;
                LaserStatus = getSystemStatusAnswerEventArgs.LaserStatus;
                AdmStatus = getSystemStatusAnswerEventArgs.AdmStatus;
                esVersionNumber = getSystemStatusAnswerEventArgs.EsVersionNumber;
                WeatherMonitorStatus = getSystemStatusAnswerEventArgs.WeatherMonitorStatus;
                lFlagsValue = getSystemStatusAnswerEventArgs.LFlagsValue;
                lTrackerSerialNumber = getSystemStatusAnswerEventArgs.LTrackerSerialNumber;
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not get system status;{ex.Message}");
            }
        }

        private Receiver.GetSystemStatusAnswerEventArgs getSystemStatusAnswerEventArgs;

        private void OnGetSystemStatusAnswer(object sender, Receiver.GetSystemStatusAnswerEventArgs e)
        {
            getSystemStatusAnswerEventArgs = e;
            if (syncCallInProgress == ES_Command.ES_C_GetSystemStatus)
                receivedEvent.Set();
        }

        public void GetTrackerStatus(out ES_TrackerStatus tracker)
        {
            try
            {
                InitCommand(ES_Command.ES_C_GetTrackerStatus, true).GetTrackerStatus();
                WaitForAnswer();
                tracker = getTrackerStatusAnswerEventArgs.TrackerStatus;
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not get tracker status;{ex.Message}");
            }
        }

        private Receiver.GetTrackerStatusAnswerEventArgs getTrackerStatusAnswerEventArgs;

        private void OnGetTrackerStatusAnswer(object sender, Receiver.GetTrackerStatusAnswerEventArgs e)
        {
            getTrackerStatusAnswerEventArgs = e;
            if (syncCallInProgress == ES_Command.ES_C_GetTrackerStatus)
                receivedEvent.Set();
        }

        public void StartNivelMeasurement(
            out ES_NivelStatus status,
            out double xTilt,
            out double yTilt,
            out double temperature)
        {
            try
            {
                InitCommand(ES_Command.ES_C_StartNivelMeasurement, true).StartNivelMeasurement();
                WaitForAnswer();
                status = nivelMeasurementAnswerEventArgs.NivelMeasurement.nivelStatus;
                xTilt = nivelMeasurementAnswerEventArgs.NivelMeasurement.dXTilt;
                yTilt = nivelMeasurementAnswerEventArgs.NivelMeasurement.dYTilt;
                temperature = nivelMeasurementAnswerEventArgs.NivelMeasurement.dNivelTemperature;
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not start nivel measurement;{ex.Message}");
            }
        }

        private Receiver.NivelMeasurementAnswerEventArgs nivelMeasurementAnswerEventArgs;

        private void OnNivelMeasurementAnswer(object sender, Receiver.NivelMeasurementAnswerEventArgs e)
        {
            nivelMeasurementAnswerEventArgs = e;
            if (syncCallInProgress == ES_Command.ES_C_StartNivelMeasurement)
                receivedEvent.Set();
        }

        public void GetMeasurementStatusInfo(out int status)
        {
            try
            {
                InitCommand(ES_Command.ES_C_GetMeasurementStatusInfo, true).GetMeasurementStatusInfo();
                WaitForAnswer();
                status = getMeasurementStatusInfoAnswerEventArgs.MeasurementStatusInfo;
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not get measurement status info;{ex.Message}");
            }
        }

        private Receiver.GetMeasurementStatusInfoAnswerEventArgs getMeasurementStatusInfoAnswerEventArgs;

        private void OnGetMeasurementStatusInfoAnswer(object sender, Receiver.GetMeasurementStatusInfoAnswerEventArgs e)
        {
            getMeasurementStatusInfoAnswerEventArgs = e;
            if (syncCallInProgress == ES_Command.ES_C_GetMeasurementStatusInfo)
                receivedEvent.Set();
        }

        public void GetLongSystemParameter(
            ES_SystemParameter systemParam,
            out int parameterValue)
        {
            try
            {
                Debug.WriteInConsole($"GetLongSystemParameter systemParam={systemParam}");
                InitCommand(ES_Command.ES_C_GetLongSystemParameter, true).GetLongSystemParameter(systemParam);
                WaitForAnswer();
                parameterValue = getLongSystemParamAnswerEventArgs.LParameter;
                Debug.WriteInConsole($"GetLongSystemParameter parameterValue={parameterValue}");
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not get long system parameters;{ex.Message}");
            }
        }

        private Receiver.GetLongSystemParamAnswerEventArgs getLongSystemParamAnswerEventArgs;

        private void OnGetLongSystemParamAnswer(object sender, Receiver.GetLongSystemParamAnswerEventArgs e)
        {
            Debug.WriteInConsole($"OnGetLongSystemParamAnswer LParameter={e.LParameter}");

            getLongSystemParamAnswerEventArgs = e;
            if (syncCallInProgress == ES_Command.ES_C_GetLongSystemParameter)
                receivedEvent.Set();
        }


        public void SetLongSystemParameter(ES_SystemParameter systemparam, int param)
        {
            Debug.WriteInConsole($"SetLongSystemParameter systemparam={systemparam}, param={param}");

            try
            {
                InitCommand(ES_Command.ES_C_SetLongSystemParameter, false).SetLongSystemParameter(systemparam, param);
                WaitForAnswer();
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not set long system parameters;{ex.Message}");
            }
        }
        public void ChangeFace()
        {
            try
            {
                InitCommand(ES_Command.ES_C_ChangeFace, false).ChangeFace();
                WaitForAnswer();
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not change face;{ex.Message}");
            }
        }

        public void GetFace(out ES_TrackerFace face)
        {
            try
            {
                InitCommand(ES_Command.ES_C_GetFace, true).GetFace();
                WaitForAnswer();
                face = getFaceAnswerEventArgs.GetFaceAnswer;
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not get face;{ex.Message}");
            }
        }

        private Receiver.GetFaceAnswerEventArgs getFaceAnswerEventArgs;

        private void OnGetFaceAnswer(object sender, Receiver.GetFaceAnswerEventArgs e)
        {
            getFaceAnswerEventArgs = e;
            if (syncCallInProgress == ES_Command.ES_C_GetFace)
                receivedEvent.Set();
        }

        public void MoveHV(int h, int v)
        {
            try
            {
                InitCommand(ES_Command.ES_C_MoveHV, false).MoveHV(h, v);
                WaitForAnswer();
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not get long system parameters;{ex.Message}");
            }
        }

        public void SetMeasurementMode(ES_MeasMode mode)
        {
            try
            {
                InitCommand(ES_Command.ES_C_SetMeasurementMode, false).SetMeasurementMode(mode);
                WaitForAnswer();
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not set measurement Mode;{ex.Message}");
            }
        }
        public void GetMeasurementMode(out ES_MeasMode mode)
        {
            try
            {
                InitCommand(ES_Command.ES_C_GetMeasurementMode, true).GetMeasurementMode();
                WaitForAnswer();
                mode = getMeasurementModeAnswerEventArgs.MeasMode;
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not get long system parameters;{ex.Message}");
            }
        }

        private Receiver.GetMeasurementModeAnswerEventArgs getMeasurementModeAnswerEventArgs;

        private void OnGetMeasurementModeAnswer(object sender, Receiver.GetMeasurementModeAnswerEventArgs e)
        {
            getMeasurementModeAnswerEventArgs = e;
            if (syncCallInProgress == ES_Command.ES_C_GetMeasurementMode)
                receivedEvent.Set();
        }

        public void SetDoubleSystemParameter(ES_SystemParameter p, double param)
        {
            try
            {
                InitCommand(ES_Command.ES_C_SetDoubleSystemParameter, false).SetDoubleSystemParameter(p, param);
                WaitForAnswer();
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not get double system parameters;{ex.Message}");
            }
        }

        public void GetCoordinateSystemType(out ES_CoordinateSystemType type)
        {
            try
            {
                InitCommand(ES_Command.ES_C_GetCoordinateSystemType, true).GetCoordinateSystemType();
                WaitForAnswer();
                type = getCoordinateSystemTypeAnserEventArgs.CoordinateSystemType;
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not get CS type;{ex.Message}");
            }
        }

        public void SetCoordinateSystemType(ES_CoordinateSystemType type)
        {
            try
            {
                InitCommand(ES_Command.ES_C_SetCoordinateSystemType, false).SetCoordinateSystemType(type);
                WaitForAnswer();
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not set CS type;{ex.Message}");
            }
        }

        public void GetUnits(out ES_LengthUnit length, out ES_AngleUnit angle, out ES_TemperatureUnit temp, out ES_PressureUnit press, out ES_HumidityUnit hum)
        {
            try
            {
                InitCommand(ES_Command.ES_C_GetUnits, true).GetUnits();
                WaitForAnswer();
                length = getUnitsAnswerEventArgs.SystemUnitsData.lenUnitType;
                angle = getUnitsAnswerEventArgs.SystemUnitsData.angUnitType;
                temp = getUnitsAnswerEventArgs.SystemUnitsData.tempUnitType;
                press = getUnitsAnswerEventArgs.SystemUnitsData.pressUnitType;
                hum = getUnitsAnswerEventArgs.SystemUnitsData.humUnitType;
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not get units;{ex.Message}");
            }

        }

        public void SetUnits(ES_LengthUnit length, ES_AngleUnit angle, ES_TemperatureUnit temp, ES_PressureUnit press, ES_HumidityUnit hum)
        {
            try
            {
                InitCommand(ES_Command.ES_C_SetUnits, false).SetUnits(length, angle, temp, press, hum);
                WaitForAnswer();
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not set units;{ex.Message}");
            }
        }

        public void GetStatisticMode(out ES_StatisticMode stationaryMode, out ES_StatisticMode continuousMode)
        {
            try
            {
                InitCommand(ES_Command.ES_C_GetStatisticMode, true).GetStatisticMode();
                WaitForAnswer();
                stationaryMode = getStatisticModeAnswerEventArgs.StationaryMeasurements;
                continuousMode = getStatisticModeAnswerEventArgs.ContinuousMeasurements;
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not get statistic mode;{ex.Message}");
            }
        }

        public void SetStatisticMode(ES_StatisticMode stationaryMode, ES_StatisticMode continuousMode)
        {
            try
            {
                InitCommand(ES_Command.ES_C_SetStatisticMode, false).SetStatisticMode(stationaryMode, continuousMode);
                WaitForAnswer();
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not set statistic mode;{ex.Message}");
            }
        }

        public void SetLaserOnTimer(int iH, int iM)
        {
            try
            {
                InitCommand(ES_Command.ES_C_SetLaserOnTimer, false).SetLaserOnTimer(iH, iM);
                WaitForAnswer();
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not set laser on timer;{ex.Message}");
            }
        }

        public void GetReflectors()
        {
            try
            {
                InitCommand(ES_Command.ES_C_GetReflectors, false).GetReflectors();
                WaitForAnswer();
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not get reflectors (with a S);{ex.Message}");
            }
        }

        public void GetProbe(out int id)
        {
            try
            {
                InitCommand(ES_Command.ES_C_GetProbe, true).GetProbe();
                WaitForAnswer();
                id = getProbeAnswerEventArgs.ProbeID;
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not get probe;{ex.Message}");
            }
        }

        private Receiver.GetProbeAnswerEventArgs getProbeAnswerEventArgs;

        private void OnGetProbeAnswer(object sender, Receiver.GetProbeAnswerEventArgs e)
        {
            getProbeAnswerEventArgs = e;
            if (syncCallInProgress == ES_Command.ES_C_GetProbe)
                receivedEvent.Set();
        }

        public void GetTipAdapter(out int id, out int i)
        {
            try
            {
                InitCommand(ES_Command.ES_C_GetTipAdapter, true).GetTipAdapter();
                WaitForAnswer();
                id = getTipAdapterAnswerEventArgs.ITipAdapterID;
                i = getTipAdapterAnswerEventArgs.ITipAdapterInterface;
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not get tip adapter;{ex.Message}");
            }
        }

        private Receiver.GetTipAdapterAnswerEventArgs getTipAdapterAnswerEventArgs;

        private void OnGetTipAdapterAnswer(object sender, Receiver.GetTipAdapterAnswerEventArgs e)
        {
            getTipAdapterAnswerEventArgs = e;
            if (syncCallInProgress == ES_Command.ES_C_GetTipAdapter)
                receivedEvent.Set();
        }

        public void PositionRelativeHV(double h, double v)
        {
            try
            {
                InitCommand(ES_Command.ES_C_PositionRelativeHV, false).PositionRelativeHV(h, v);
                WaitForAnswer();
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not set position relavtive H V" +
                    $";{ex.Message}");
            }
        }

        /// <summary>
        /// Begin to enumerate the probes
        /// The calls retruns immediately; after this some other events will be trigerred with the data of the different probes
        /// </summary>
        /// <exception cref="Exception"></exception>
        public void GetProbes()
        {
            try
            {
                InitCommand(ES_Command.ES_C_GetProbes, false).GetProbes();
                WaitForAnswer();
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not get probes;{ex.Message}");
            }
        }

        public void FindReflector(double approxDistance)
        {
            try
            {
                InitCommand(ES_Command.ES_C_FindReflector, false).FindReflector(approxDistance);
                WaitForAnswer();
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not  find reflector;{ex.Message}");
            }
        }

        public void StopMove()
        {
            try
            {
                InitCommand(ES_Command.ES_C_MoveHV, false).MoveHV(0,0);
                WaitForAnswer();
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not stop the move;{ex.Message}");
            }
        }

        public void GetMeasurementProbeInfo(
                    out int iFirmwareMajorVersionNumber,
                    out int iFirmwareMinorVersionNumber,
                    out int lSerialNumber,
                    out ES_ProbeType probeType,
                    out int lCompensationIdNumber,
                    out int lActiveField,
                    out ES_ProbeConnectionType connectionType,
                    out int lNumberOfTipAdapters,
                    out ES_ProbeButtonType probeButtonType,
                    out int lNumberOfFields,
                    out bool bHasWideAngleReceiver,
                    out int lNumberOfTipDataSets,
                    out int lNumberOfMelodies,
                    out int lNumberOfLoudnesSteps)
        {
            try
            {
                InitCommand(ES_Command.ES_C_GetMeasurementProbeInfo, true).GetMeasurementProbeInfo();
                WaitForAnswer();
                iFirmwareMajorVersionNumber = getMeasurementProbeInfoAnswerEventArgs.IFirmwareMajorVersionNumber;
                iFirmwareMinorVersionNumber = getMeasurementProbeInfoAnswerEventArgs.IFirmwareMinorVersionNumber;
                lSerialNumber = getMeasurementProbeInfoAnswerEventArgs.LSerialNumber;
                probeType = getMeasurementProbeInfoAnswerEventArgs.ProbeType;
                lCompensationIdNumber = getMeasurementProbeInfoAnswerEventArgs.LCompensationIdNumber;
                lActiveField = getMeasurementProbeInfoAnswerEventArgs.LActiveField;
                connectionType = getMeasurementProbeInfoAnswerEventArgs.ConnectionType;
                lNumberOfTipAdapters = getMeasurementProbeInfoAnswerEventArgs.LNumberOfTipAdapters;
                probeButtonType = getMeasurementProbeInfoAnswerEventArgs.ProbeButtonType;
                lNumberOfFields = getMeasurementProbeInfoAnswerEventArgs.LNumberOfFields;
                bHasWideAngleReceiver = getMeasurementProbeInfoAnswerEventArgs.BHasWideAngleReceiver;
                lNumberOfTipDataSets = getMeasurementProbeInfoAnswerEventArgs.LNumberOfTipDataSets;
                lNumberOfMelodies = getMeasurementProbeInfoAnswerEventArgs.LNumberOfMelodies;
                lNumberOfLoudnesSteps = getMeasurementProbeInfoAnswerEventArgs.LNumberOfLoudnesSteps;
                SyncCallInProgress = ES_Command.ES_C_Unknown;
            }
            catch (Exception ex)
            {
                SyncCallInProgress = ES_Command.ES_C_Unknown;
                throw new Exception($"Could not get probe;{ex.Message}");
            }
        }

        private Receiver.GetMeasurementProbeInfoAnswerEventArgs getMeasurementProbeInfoAnswerEventArgs;

        private void OnGetMeasurementProbeInfoAnswer(object sender, Receiver.GetMeasurementProbeInfoAnswerEventArgs e)
        {
            getMeasurementProbeInfoAnswerEventArgs = e;
            if (syncCallInProgress == ES_Command.ES_C_GetMeasurementProbeInfo)
                receivedEvent.Set();
        }

        private Receiver.GetCoordinateSystemTypeAnserEventArgs getCoordinateSystemTypeAnserEventArgs;

        private void OnGetCoordinateSystemTypeAnswer(object sender, Receiver.GetCoordinateSystemTypeAnserEventArgs e)
        {
            getCoordinateSystemTypeAnserEventArgs = e;
            if (syncCallInProgress == ES_Command.ES_C_GetCoordinateSystemType)
                receivedEvent.Set();
        }

        private Receiver.GetStatisticModeAnswerEventArgs getStatisticModeAnswerEventArgs;
        private void OnGetStatisticModeAnswer(object sender, Receiver.GetStatisticModeAnswerEventArgs e)
        {
            getStatisticModeAnswerEventArgs = e;
            if (syncCallInProgress == ES_Command.ES_C_GetStatisticMode)
                receivedEvent.Set();
        }

        private Receiver.GetUnitsAnswerEventArgs getUnitsAnswerEventArgs;
        private void OnGetUnitsAnswer(object sender, Receiver.GetUnitsAnswerEventArgs e)
        {
            getUnitsAnswerEventArgs = e;
            if (syncCallInProgress == ES_Command.ES_C_GetUnits)
                receivedEvent.Set();
        }

    }
}
