﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace TSU.Common.Instruments.Device.AT40x
{
    public partial class SubView : TSU.Views.TsuView
    {
        public SubView()
        {
            InitializeComponent();
        }

        public SubView(AT40x.View parentView)
        {
            this._Module = parentView._Module;
            InitializeComponent();
            this.ApplyThemeColors();
        }

        private void ApplyThemeColors()
        {
            this.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
        }

        #region refs

        internal AT40x.View At40xView
        {
            get
            {
                return (this.ParentView as AT40x.View);
            }
        }

        internal AT40x.Module At40xModule
        {
            get
            {
                return (this.ParentView._Module as AT40x.Module);
            }
        }

        #endregion

        internal virtual void Start()
        {
            if (this.At40xView.IsOn)
                this.At40xView.SetSubView(this);
        }

        internal virtual void Stop()
        {
            //  Logs.Log.AddEntryAsFinishOf(this._Module, string.Format("{0} Stopped",this._Name));
            Logs.Log.AddEntryAsFinishOf(this._Module, $"{this._Name} {R.T_STOPPED}");
        }
            
        
    }
}
