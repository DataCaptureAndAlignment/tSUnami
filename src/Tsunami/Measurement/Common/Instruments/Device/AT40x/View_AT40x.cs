﻿using EmScon;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSU.Tools;
using TSU.Views;
using TSU.Views.Message;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using Mess = TSU.Views.Message;
using P = TSU.Views.Message.ProgressMessage;
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments.Device.AT40x
{

    public partial class View : Device.View
    {
        #region Fields
        private Color COLOR_GOOD = Tsunami2.Preferences.Theme.Colors.Good;
        private Color COLOR_BAD = Tsunami2.Preferences.Theme.Colors.Bad;
        private Color COLOR_AVERAGE = Tsunami2.Preferences.Theme.Colors.Attention;
        private Color COLOR_NORMAL = Tsunami2.Preferences.Theme.Colors.Highlight;
        private Color COLOR_GRAY = Tsunami2.Preferences.Theme.Colors.Object;

        private int previousSize;
        string NameToShow = "At40x";

        internal LiveDataView liveDataView;
        internal JogView jogView;
        internal RemoteView remoteView;
        internal RemoteListView remoteListView;
        internal TimerView timerView;
        internal CameraView cameraView;
        public new Module Module
        {
            get
            {
                return this._Module as Module;
            }
            set
            {
                this._Module = value;
            }
        }


        #endregion

        #region Constructor
        public View(I.Module instrumentModule)
            : base(instrumentModule)
        {
            // Design
            InitializeComponent();
            this.ApplyThemeColors();
            //TsunamiPreferences.Theme.ApplyTo(this.splitContainer1);
            CreateButtons();
            labelModel.Text = NameToShow;
            panelCaJoLi.Visible = true;
            InitializeLog();
            this.liveDataView = new LiveDataView(this) { Dock = DockStyle.Fill, TopLevel = false };
            this.cameraView = new CameraView(this) { Dock = DockStyle.Fill, TopLevel = false };
            this.jogView = new JogView(this) { Dock = DockStyle.Fill, TopLevel = false };
            this.remoteView = new RemoteView(this) { Dock = DockStyle.Fill, TopLevel = false };
            this.remoteListView = new RemoteListView(this) { Dock = DockStyle.Fill, TopLevel = false };
            this.timerView = new TimerView(this) { Dock = DockStyle.Fill, TopLevel = false };

            //toolTip1.SetToolTip(buttonExtract,"Extract: Will temporary move this view to a message box");
            //toolTip1.SetToolTip(buttonTimer, "AT Timer view");
            //toolTip1.SetToolTip(buttonJog, "Manual Jog view to move the instrument");
            //toolTip1.SetToolTip(buttonRemote, "4 buttons remote view");
            // toolTip1.SetToolTip(buttonCamera, "Camera view");
            //toolTip1.SetToolTip(buttonLiveData, "Live Data View");
            //toolTip1.SetToolTip(buttonGotoAll, " Goto the next point and measure");
            //toolTip1.SetToolTip(buttonMeasure, "Launch Measurement");
            //toolTip1.SetToolTip(buttonGoto, "Move the instrument to point at the first element of the list");
            //toolTip1.SetToolTip(buttonLocking, "Lock / Unlock / Search");
            //toolTip1.SetToolTip(buttonPrism, "Change Reflector");
            //toolTip1.SetToolTip(buttonShift, "ShiftButtons");
            //toolTip1.SetToolTip(buttonJog, "Show the 'jogging' panel");
            //toolTip1.SetToolTip(buttonCamera, "Show the 'Camera' panel");
            //toolTip1.SetToolTip(buttonLiveData, "Show the 'Live Data' panel");
            //toolTip1.SetToolTip(buttonRemote, "Show the 'Remote' panel");
            //toolTip1.SetToolTip(buttonTimer, "Show the 'Automatic start' panel");
            toolTip1.SetToolTip(buttonConnect, "Connect/disconnect/Reset");
            toolTip1.SetToolTip(buttonInit, R.T_INITIALISATION_OF_THE_INSTRUMENT);
            toolTip1.SetToolTip(buttonExtract, R.T_EXTRACT_WILL_TEMPORARY_MOVE_THIS_VIEW_TO_A_MESSAGE_BOX);
            toolTip1.SetToolTip(buttonTimer, R.T_AT_TIMER_VIEW);
            toolTip1.SetToolTip(buttonJog, R.T_MANUAL_JOG_VIEW_TO_MOVE_THE_INSTRUMENT);
            toolTip1.SetToolTip(buttonRemote, R.T_4_BUTTONS_REMOTE_VIEW);
            toolTip1.SetToolTip(buttonCamera, R.T_CAMERA_VIEW);
            toolTip1.SetToolTip(buttonLiveData, R.T_LIVE_DATA_VIEW);
            toolTip1.SetToolTip(buttonGotoAll, R.T_GOTO_THE_NEXT_POINT_AND_MEASURE);
            toolTip1.SetToolTip(buttonMeasure, R.T_LAUNCH_MEASUREMENT);
            toolTip1.SetToolTip(buttonGoto, R.T_MOVE_THE_INSTRUMENT_TO_POINT_AT_THE_FIRST_ELEMENT_OF_THE_LIST);
            toolTip1.SetToolTip(buttonLocking, "Lock / Unlock / Search");
            toolTip1.SetToolTip(buttonPrism, "Change Reflector");
            toolTip1.SetToolTip(buttonShift, "Show other options");
            toolTip1.SetToolTip(buttonJog, R.T_SHOW_THE_JOGGING_PANEL);
            toolTip1.SetToolTip(buttonCamera, R.T_SHOW_THE_CAMERA_PANEL);
            toolTip1.SetToolTip(buttonLiveData, R.T_SHOW_THE_LIVE_DATA_PANEL);
            toolTip1.SetToolTip(buttonRemote, R.T_SHOW_THE_REMOTE_PANEL);
            toolTip1.SetToolTip(buttonTimer, R.T_SHOW_THE_AUTOMATIC_START_PANEL);

            this.remoteView.Start();
            this.ShowDockedFill();

            this.Module.StatusChanged += OnStatusChanged;
        }

        private void ApplyThemeColors()
        {
            if (this.InvokeRequired)
            {
                // Invoke the event handler on the main UI thread
                this.BeginInvoke(new Action(() => ApplyThemeColors()));
            }
            else
            {
                Preferences.GUI.Theme theme = Tsunami2.Preferences.Theme;
                Preferences.GUI.Theme.ThemeColors colors = theme.Colors;
                this.labelModel.ForeColor = colors.NormalFore;
                this.panelGlobal.BackColor = colors.Background;
                theme.ApplyTo(splitAT_V_SubV);
                this.panelMain.BackColor = colors.Background;
                this.buttonInit.BackColor = colors.Background;
                this.buttonShift.BackColor = colors.Background;
                this.buttonPrism.BackColor = colors.Background;
                this.buttonLocking.BackColor = colors.Background;
                this.buttonGoto.BackColor = colors.Background;
                this.buttonMeasure.BackColor = colors.Background;
                this.buttonGotoAll.BackColor = colors.Background;
                this.buttonConnect.BackColor = colors.Background;
                this.panelLog.BackColor = colors.DarkBackground;
                this.panelState.BackColor = colors.Background;
                this.pMeas.BackColor = colors.Background;
                this.pictureLaserState.BackColor = colors.Background;
                this.pictureLaser.BackColor = colors.Background;
                this.buttonRefreshStatus.BackColor = colors.Good;
                this.pictureNivelState.BackColor = colors.Background;
                this.picture_State_Measure_Mode.BackColor = colors.Background;
                this.labelHumidity.BackColor = colors.Background;
                this.labelPressure.BackColor = colors.Background;
                this.labelTemperatureState.BackColor = colors.Background;
                this.pictureHumidity.BackColor = colors.Background;
                this.picturePressure.BackColor = colors.Background;
                this.pictureTemperature.BackColor = colors.Background;
                this.pictureCompensatorState.BackColor = colors.Background;
                this.pictureCompensator.BackColor = colors.Background;
                this.labelBatterySensorState.BackColor = colors.Background;
                this.labelBatteryControlerState.BackColor = colors.Background;
                this.picture_State_Beam_State.BackColor = colors.Background;
                this.picture_State_Face.BackColor = colors.Background;
                this.pictureInitialisationState.BackColor = colors.Background;
                this.pictureInitialisation.BackColor = colors.Background;
                this.pictureSensorIdealTemperatureState.BackColor = colors.Background;
                this.pictureSensorIdealTemperature.BackColor = colors.Background;
                this.pictureNivel.BackColor = colors.Background;
                this.pictureBatterySensor.BackColor = colors.Background;
                this.pictureBattetyControler.BackColor = colors.Background;
                this.pictureAdmState.BackColor = colors.Background;
                this.pictureAdm.BackColor = colors.Background;
                this.pictureControlerState.BackColor = colors.Background;
                this.picture_Controller.BackColor = colors.Background;
                this.pictureSensorState.BackColor = colors.Background;
                this.pictureSensor.BackColor = colors.Background;
                this.panelCaJoLi.BackColor = colors.Background;
                this.panelCaJoLiConnected.BackColor = colors.Background;
                this.buttonTimer.BackColor = colors.Background;
                this.buttonJog.BackColor = colors.Background;
                this.buttonRemote.BackColor = colors.Background;
                this.buttonLiveData.BackColor = colors.Background;
                this.buttonCamera.BackColor = colors.Background;
                this.buttonExtract.BackColor = colors.Background;
                this.panel4SubViews.BackColor = colors.Background;
                this.pMeasTsu.BackColor = colors.Background;
                this.pMeasLock.BackColor = colors.Background;
                this.pictureBox4.BackColor = colors.Background;
                this.pictureBox3.BackColor = colors.Background;
                this.pictureBox2.BackColor = colors.Background;
                this.pictureBox1.BackColor = colors.Background;
                this.toolStripMenuItem2.BackColor = colors.Background;
                this.BackColor = colors.LightBackground;
            }
        }
        /// <summary>
        /// to set the tooltip in the view
        /// </summary>
        internal override void SetAllToolTips()
        {
            //var toolTip1 = this.ToolTip;

            toolTip1.SetToolTip(this.buttonInit, R.String_AT_Tooltip_Init);
            toolTip1.SetToolTip(this.buttonLocking, R.String_AT_Tooltip_Lock);
            toolTip1.SetToolTip(this.buttonConnect, R.String_AT_Tooltip_Connect);
            toolTip1.SetToolTip(this.buttonRefreshStatus, R.String_AT_Tooltip_Refresh_Status);
            toolTip1.SetToolTip(buttonExtract, R.String_AT_Tooltip_Extract);
            toolTip1.SetToolTip(buttonTimer, R.String_AT_Tooltip_Timer);
            toolTip1.SetToolTip(buttonJog, R.String_AT_Tooltip_Jog);
            toolTip1.SetToolTip(buttonRemote, R.String_AT_Tooltip_Remote);
            toolTip1.SetToolTip(buttonCamera, R.String_AT_Tooltip_Camera);
            toolTip1.SetToolTip(buttonLiveData, R.String_AT_Tooltip_LiveData);
            toolTip1.SetToolTip(buttonGotoAll, R.String_AT_Tooltip_GotoAll);
            toolTip1.SetToolTip(buttonMeasure, R.String_AT_Tooltip_Measure);
            toolTip1.SetToolTip(buttonGoto, R.String_AT_Tooltip_Goto);
            toolTip1.SetToolTip(buttonLocking, R.String_AT_Tooltip_LockingStatus);
            toolTip1.SetToolTip(buttonPrism, R.String_AT_Tooltip_Reflector);
            toolTip1.SetToolTip(buttonShift, R.String_AT_Tooltip_Shift);

            toolTip1.SetToolTip(this.pictureLaser, R.String_AT_Tooltip_PicLaser);
            this.toolTip1.SetToolTip(this.picture_State_Measure_Mode, R.String_AT_Tooltip_PicMeasMode);
            this.toolTip1.SetToolTip(this.pictureHumidity, R.String_AT_Tooltip_PicHumidity);
            this.toolTip1.SetToolTip(this.picturePressure, R.String_AT_Tooltip_PicPressure);
            this.toolTip1.SetToolTip(this.pictureTemperature, R.String_AT_Tooltip_PicTemperature);
            this.toolTip1.SetToolTip(this.pictureCompensator, R.String_AT_Tooltip_PicCompensator);
            this.toolTip1.SetToolTip(this.picture_State_Beam_State, R.String_AT_Tooltip_PicBeamState);
            this.toolTip1.SetToolTip(this.picture_State_Face, R.String_AT_Tooltip_PicFace);
            this.toolTip1.SetToolTip(this.pictureInitialisation, R.String_AT_Tooltip_PicInitStatus);
            this.toolTip1.SetToolTip(this.pictureSensorIdealTemperature, R.String_AT_Tooltip_PicLaser);
            this.toolTip1.SetToolTip(this.pictureNivel, R.String_AT_Tooltip_PicNivelStatus);
            this.toolTip1.SetToolTip(this.pictureBatterySensor, R.String_AT_Tooltip_PicBatterySensor);
            this.toolTip1.SetToolTip(this.pictureBattetyControler, R.String_AT_Tooltip_PicBatteryController);
            this.toolTip1.SetToolTip(this.pictureAdm, R.String_AT_Tooltip_PicADMstatus);
            this.toolTip1.SetToolTip(this.picture_Controller, R.String_AT_Tooltip_PicControllerConnection);
            this.toolTip1.SetToolTip(this.pictureSensor, R.String_AT_Tooltip_PicSensor);
        }

        public List<Action> actionList = new List<Action>();

        List<string> actionNameList = new List<string>();


        internal override void TryAction(Action action, string message, bool showSuccessInlogs = true, bool showFailureInlog = true, bool showFailureMessage = true)
        {
            // Si livedata on, we stopped it, do the action and then restart it;

            if (Module._status.isTrackerBusy)
            {
                Debug.WriteInConsole($"Sent to action List: {message}");
                actionList.Add(action);
                actionNameList.Add(message);
                Logs.Log.AddEntryAsPayAttentionOf(this._Module, $"{R.T_ACTION_DELAYED}: {message} {action}, this means several actions are in the pipe, to cancel those actions, please use the refresh button next to the ");
            }
            else
            {
                try
                {
                    action();
                }
                catch (Exception e)
                {
                    string messageFinal;

                    if (e is CancelException)
                    {
                        Logs.Log.AddEntryAsFYI(this.Module, e.ToString());
                    }
                    else
                    {
                        if (e is NullReferenceException)
                        {
                            messageFinal = $"{message};{e.Message}\r\n\r\n{e}";
                        }
                        else
                        {
                            messageFinal = $"{R.T_ERROR};{message} {R.T_FAILED}." + Environment.NewLine + Environment.NewLine +
                                e.Message;
                        }

                        if (e is AsyncException)
                        {
                            new Mess.MessageInput(Mess.MessageType.Warning, messageFinal).Show();
                            Logs.Log.AddEntryAsPayAttentionOf(this.Module, e.ToString());
                        }
                        else
                        {
                            new Mess.MessageInput(Mess.MessageType.Critical, messageFinal).Show();
                            Logs.Log.AddEntryAsError(this.Module, e.ToString());
                        }
                    }

                    if (this.WaitingForm != null)
                    {
                        this.WaitingForm.EndAll();

                    }
                }
            }
        }

        internal void OnStatusChanged(object sender, StatusEventArgs e)
        {
            if (this.InvokeRequired)
            {
                // Invoke the event handler on the main UI thread
                this.BeginInvoke(new Action(() => OnStatusChanged(sender, e)));
                return;
            }

            this.labelModel.Text = NameToShow + " is" + e.StatusAsText;

            if (this.WaitingForm != null)
            {
                Action a = () =>
                {
                    switch (e.StepActiontype)
                    {
                        case P.StepTypes.Begin:
                            if (this.WaitingForm.Visible)
                                this.WaitingForm.BeginAstep(e.StatusAsText);
                            break;
                        case P.StepTypes.End:
                            this.WaitingForm.EndCurrentStep(e.StatusAsText);
                            break;
                        case P.StepTypes.EndAll:
                            this.WaitingForm.EndAll();
                            this.Module.State = new InstrumentState(InstrumentState.type.Idle);
                            break;
                        default:
                            break;
                    }
                };
                Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(a);
            }
        }

        internal void On_InitializationStatusChanged()
        {
            this.TryAction(() => this.UpdateSystemStatus());
        }


        #endregion

        #region design

        private void InitializeLog()
        {
            LogView = new Logs.LogView();
            Preferences.GUI.Theme.ThemeColors colors = Tsunami2.Preferences.Theme.Colors;
            LogView.SetColors(logBack: colors.Background,
                             beginningOf: colors.TextOnDarkBackGround);
            LogView.WantedModule = _Module;
            LogView.ShowDockedFill();
            panelLog.Controls.Add(LogView);
        }

        ToolStripMenuItem buttonCaJoLi;
        ToolStripMenuItem buttonFace;
        ToolStripMenuItem buttonMode;

        ToolStripMenuItem buttonDangereous;
        ToolStripMenuItem buttonInit2;
        ToolStripMenuItem buttonPowerSearch;
        ToolStripMenuItem buttonRelease;
        ToolStripMenuItem buttonLock;

        ToolStripMenuItem buttonTurnOn;
        ToolStripMenuItem buttonTurnOnWithWiFi;
        ToolStripMenuItem buttonTurnOff;
        ToolStripMenuItem buttonReset;

        private void CreateButtons()
        {
            buttonCaJoLi = new ToolStripMenuItem() { BackColor = Tsunami2.Preferences.Theme.Colors.DarkBackground, ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground };
            buttonCaJoLi.Image = R.At40x_CaJoLi;
            buttonCaJoLi.Click += this.ShowHideSubViews;
            buttonCaJoLi.ImageScaling = ToolStripItemImageScaling.None;
            buttonCaJoLi.Text = "Show/Hide Additional panel";

            buttonFace = new ToolStripMenuItem() { BackColor = Tsunami2.Preferences.Theme.Colors.DarkBackground, ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground };
            buttonFace.Image = R.At40x_Face;
            buttonFace.Click += this.On_Button_Face_Click;
            buttonFace.ImageScaling = ToolStripItemImageScaling.None;
            buttonFace.Text = "Change Face";

            buttonMode = new ToolStripMenuItem() { BackColor = Tsunami2.Preferences.Theme.Colors.DarkBackground, ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground };
            buttonMode.Image = R.At40x_MeasureMode;
            buttonMode.ImageScaling = ToolStripItemImageScaling.None;
            buttonMode.Text = "Change the mode of measurement";
            buttonMode.DropDownItems.Add(new ToolStripMenuItem(
                "Precise", R.At40x_Precision_Precise,
                delegate
                {
                    this.Module._measurement.mode = Module.aT40xMeasurementMode.Precise;
                    //   this.TrySyncActionWithMessage(this.Module._measurement.SetMode,"Change to 'precise mode", "Speaking with instrument", 1, 500);
                    this.TrySyncActionWithMessage(
                        this.Module._measurement.SetMode,
                        R.T_CHANGE_TO_PRECISE_MODE,
                        R.T_SPEAKING_WITH_INSTRUMENT,
                        1,
                        500);
                })
            { ImageScaling = ToolStripItemImageScaling.None, BackColor = Tsunami2.Preferences.Theme.Colors.DarkBackground, ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground });
            buttonMode.DropDownItems.Add(new ToolStripMenuItem(
                "Standard", R.At40x_Precision_Standard,
                delegate
                {
                    this.Module._measurement.mode = Module.aT40xMeasurementMode.Standard;
                    //  this.TrySyncActionWithMessage(this.Module._measurement.SetMode, "Change to 'standard' mode", "Speaking with instrument", 1, 500);
                    this.TrySyncActionWithMessage(this.Module._measurement.SetMode, R.T_CHANGE_TO_STANDARD_MODE, R.T_SPEAKING_WITH_INSTRUMENT, 1, 500);
                })
            { ImageScaling = ToolStripItemImageScaling.None, BackColor = Tsunami2.Preferences.Theme.Colors.DarkBackground, ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground });
            buttonMode.DropDownItems.Add(new ToolStripMenuItem(
                "Fast", R.At40x_Precision_Fast,
                delegate
                {
                    this.Module._measurement.mode = Module.aT40xMeasurementMode.Fast;
                    //  this.TrySyncActionWithMessage(this.Module._measurement.SetMode, "Change to 'fast' mode", "Speaking with instrument", 1, 500);
                    this.TrySyncActionWithMessage(this.Module._measurement.SetMode, R.T_CHANGE_TO_FAST_MODE, R.T_SPEAKING_WITH_INSTRUMENT, 1, 500);
                })
            { ImageScaling = ToolStripItemImageScaling.None, BackColor = Tsunami2.Preferences.Theme.Colors.DarkBackground, ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground });
            buttonMode.DropDownItems.Add(new ToolStripMenuItem(
                "Outdoor", R.At40x_Precision_Outdoor,
                delegate
                {
                    this.Module._measurement.mode = Module.aT40xMeasurementMode.Outdoor;
                    // this.TrySyncActionWithMessage(this.Module._measurement.SetMode, "Change to 'outdoor' mode", "Speaking with instrument", 1, 500);
                    this.TrySyncActionWithMessage(this.Module._measurement.SetMode, R.T_CHANGE_TO_OUTDOOR_MODE, R.T_SPEAKING_WITH_INSTRUMENT, 1, 500);
                })
            { ImageScaling = ToolStripItemImageScaling.None, BackColor = Tsunami2.Preferences.Theme.Colors.DarkBackground, ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground });

            buttonDangereous = new ToolStripMenuItem() { BackColor = Tsunami2.Preferences.Theme.Colors.DarkBackground, ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground };
            buttonDangereous.Image = R.At40x_Red;
            buttonDangereous.ImageScaling = ToolStripItemImageScaling.None;
            buttonDangereous.Text = "Dangereous";
            buttonDangereous.DropDownItems.Add(new ToolStripMenuItem(
                "Reset connecton", R.At40x_Controller,
                delegate
                {
                    string titleAndMessage = $"{R.T_RESET_CONNECTION};{R.T_ARE_YOU_SURE_YOUR_WANT_TO_RESET_CONNECTION_YOU_MAY_HAVE_TO_REINITIALISE_THE_INSTRUMENT}";
                    Mess.MessageInput mi = new Mess.MessageInput(Mess.MessageType.Choice, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                    };
                    if (mi.Show().TextOfButtonClicked == R.T_YES)
                        this.Module.ResetConnection();
                })
            { ImageScaling = ToolStripItemImageScaling.None, BackColor = Tsunami2.Preferences.Theme.Colors.DarkBackground, ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground });
            buttonDangereous.DropDownItems.Add(new ToolStripMenuItem(
               "Turn on/off compensator", R.At40x_Compensator,
               delegate
               {
                   this.TryAction(ChangeCompensatorState);
               })
            { ImageScaling = ToolStripItemImageScaling.None, BackColor = Tsunami2.Preferences.Theme.Colors.DarkBackground, ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground });




            buttonInit2 = new ToolStripMenuItem() { BackColor = Tsunami2.Preferences.Theme.Colors.DarkBackground, ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground };
            buttonInit2.Image = R.At40x_Init;
            buttonInit2.Click += this.On_Button_Initialisation_Click;
            buttonInit2.ImageScaling = ToolStripItemImageScaling.None;
            // buttonInit2.Text = "Start the initialisation process";
            buttonInit2.Text = R.T_START_THE_INITIALISATION_PROCESS;

            buttonPowerSearch = new ToolStripMenuItem() { BackColor = Tsunami2.Preferences.Theme.Colors.DarkBackground, ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground };
            buttonPowerSearch.Image = R.At40x_Search;
            buttonPowerSearch.Click += this.On_Button_Search_Click;
            buttonPowerSearch.ImageScaling = ToolStripItemImageScaling.None;
            // buttonPowerSearch.Text = "Search for reflector in a bigger window";
            buttonPowerSearch.Text = R.T_SEARCH_FOR_REFLECTOR_IN_A_BIGGER_WINDOW;

            buttonRelease = new ToolStripMenuItem() { BackColor = Tsunami2.Preferences.Theme.Colors.DarkBackground, ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground };
            buttonRelease.Image = R.At40x_Release;
            buttonRelease.Click += this.On_Button_Release_Click;
            buttonRelease.ImageScaling = ToolStripItemImageScaling.None;
            // buttonRelease.Text = "Release the target";
            buttonRelease.Text = R.T_RELEASE_THE_TARGET;

            buttonLock = new ToolStripMenuItem() { BackColor = Tsunami2.Preferences.Theme.Colors.DarkBackground, ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground };
            buttonLock.Image = R.At40x_Lock;
            buttonLock.Click += this.On_Button_Lock_Click;
            buttonLock.ImageScaling = ToolStripItemImageScaling.None;
            buttonLock.Text = R.T_TRY_TO_LOCK_THE_TARGET;

            buttonTurnOn = new ToolStripMenuItem() { BackColor = Tsunami2.Preferences.Theme.Colors.DarkBackground, ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground };
            buttonTurnOn.Image = R.At40x_PowerOn;
            buttonTurnOn.Click += delegate { TurnOn(); };
            buttonTurnOn.ImageScaling = ToolStripItemImageScaling.None;
            buttonTurnOn.Text = "Turn On";

            buttonTurnOnWithWiFi = new ToolStripMenuItem() { BackColor = Tsunami2.Preferences.Theme.Colors.DarkBackground, ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground };
            buttonTurnOnWithWiFi.Image = R.At40x_Power_Wifi;
            buttonTurnOnWithWiFi.Click += delegate { TurnOnAutomaticallyWithWiFi(); };
            buttonTurnOnWithWiFi.ImageScaling = ToolStripItemImageScaling.None;
            buttonTurnOnWithWiFi.Text = "Try to fix WiFi issues and Connect";

            buttonTurnOff = new ToolStripMenuItem() { BackColor = Tsunami2.Preferences.Theme.Colors.DarkBackground, ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground };
            buttonTurnOff.Image = R.At40x_PowerOff;
            buttonTurnOff.Click += delegate { TurnOff(); };
            buttonTurnOff.ImageScaling = ToolStripItemImageScaling.None;
            buttonTurnOff.Text = "Turn Off";

            buttonReset = new ToolStripMenuItem() { BackColor = Tsunami2.Preferences.Theme.Colors.DarkBackground, ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground };
            buttonReset.Image = R.At40x_Power_reset;
            buttonReset.Click += delegate { ResetConnection(); };
            buttonReset.ImageScaling = ToolStripItemImageScaling.None;
            buttonReset.Text = "Reset connection and Connect";

        }

        internal void On_ES_SSC_MeasurementModeChanged(Module.aT40xMeasurementMode mode)
        {
            switch (mode)
            {
                case Module.aT40xMeasurementMode.Precise:
                    picture_State_Measure_Mode.Image = R.At40x_Precision_Precise;
                    break;
                case Module.aT40xMeasurementMode.Fast:
                    picture_State_Measure_Mode.Image = R.At40x_Precision_Fast;
                    break;
                case Module.aT40xMeasurementMode.Outdoor:
                    picture_State_Measure_Mode.Image = R.At40x_Precision_Outdoor;
                    break;
                default:
                    picture_State_Measure_Mode.Image = R.At40x_Precision_Standard;
                    break;
            }
        }

        internal void On_ES_SSC_ProbeChanged()
        {
            TryAction(this.Module._target.GetProbe, "Getting Selected Probe");
        }

        internal void On_ES_SSC_MeasStatus_Ready()
        {
            pMeas.Image = R.At40x_Green;
            //buttonLocking.BackgroundImage = R.At40x_Lock;
        }

        internal void On_MeasStatus_NotReady()
        {
            //buttonLocking.BackgroundImage = R.At40x_Release;
            pMeas.Image = R.At40x_Red;
        }

        internal void On_MeasStatus_Busy()
        {
            pMeas.Image = R.At40x_Orange;
        }

        internal void On_LaserWarmedUp()
        {
            WarmedUp = true;
            pictureSensorIdealTemperatureState.Image = R.At40x_Green;
            toolTip1.SetToolTip(this.pictureSensorIdealTemperatureState, "Laser warmed up");
        }

        internal void On_IsFace2()
        {

            picture_State_Face.Image = R.At40x_CercleDroit;
            toolTip1.SetToolTip(this.picture_State_Face, "Instrument is in Face 2 position");
        }

        internal void On_IsFace1()
        {
            picture_State_Face.Image = R.At40x_CercleGauche;
            toolTip1.SetToolTip(this.picture_State_Face, "Instrument is in Face 1 position");
        }

        internal void On_ES_SSC_SensorDetected()
        {

            pictureSensorState.Image = R.At40x_Green;
        }

        internal void On_CompensatorStatusChanged()
        {
            TryAction(() => this.UpdateSystemStatus());
        }

        internal void On_ES_SSC_SensorDisconnected()
        {
            pictureSensorState.Image = R.At40x_Red;
        }


        internal void On_ES_SSC_ServerStarted()
        {
            pictureControlerState.Image = R.At40x_Green;
        }

        internal void On_ES_SSC_ServerClosing()
        {
            pictureControlerState.Image = R.At40x_Red;
        }

        private void At40xView_ResizeBegin(object sender, EventArgs e)
        {
            this.SuspendLayout();
        }

        private void At40xView_ResizeEnd(object sender, EventArgs e)
        {
            this.ResumeLayout(false);
            this.PerformLayout();
        }



        #endregion

        #region Event




        private void CreateATimerThatWillSimulateAStatusButtonRefreshClick()
        {
            System.Timers.Timer timer = new System.Timers.Timer() { Interval = 500 };
            timer.Elapsed += delegate
             {
                 timer.Stop();

                 this.buttonRefreshStatus.PerformClick();


                 timer.Dispose();
             };
            Enabled = true;
            timer.Start();
        }

        bool WarmedUp = false;
        internal void ShowWarmed()
        {
            this.pictureSensorIdealTemperatureState.Image = R.At40x_Green;
            WarmedUp = true;
        }

        internal void On_ES_SSC_TPConnectionClosing()
        {
            pictureAdmState.Image = R.At40x_Red;
        }

        public override void OnNext(TsuObject tsuObject)
        {
            dynamic dynamic = tsuObject;
            OnNextBasedOn(dynamic);
        }
        public void OnNextBasedOn(M.Measure m)
        {
            if (m != null)
            {
                if (m._Status.Type == M.States.Types.Failed)
                {
                    Logs.Log.AddEntryAsError(this._Module, m.Comment);
                }
                else
                {
                    //Logs.Log.AddEntryAsSuccess(this._Module, (m as Polar.Measure).Angles.Raw.Horizontal.Value.ToString());
                }
            }

        }

        #endregion

        #region Connection

        private void Lock()
        {
            this.Module.Lock();
        }
        private void Disconnect()
        {
            if (Module.Disconnect().Success)
            {
                TurnLightOff();
                this.Module.SubscribeAt40xEvents(true);
                //this.IsWaiting = false;
            }

        }
        private void TryDisconnect()
        {
            string titleAndMessage = string.Format("{0};{1}", R.T_ARE_YOU_SURE, R.T_DO_YOU_REALLY_WANT_TO_DISCONNECT_THE_INSTRUMENT);
            Mess.MessageInput mi = new Mess.MessageInput(Mess.MessageType.Warning, titleAndMessage)
            {
                ButtonTexts = new List<string> { R.T_NO, R.T_YES }
            };
            if (mi.Show().TextOfButtonClicked == R.T_YES)
                TrySyncActionWithMessage(Disconnect, R.T_DISCONNECTION, R.T_IN_PROGRESS, 3, 6000);
        }
        private void TurnLightOff()
        {
            if (this.InvokeRequired)
            {
                // Invoke the event handler on the main UI thread
                this.BeginInvoke(new Action(() => TurnLightOff()));
            }
            else
            {
                pictureControlerState.Image = R.At40x_Red;
                pictureSensorState.Image = R.At40x_Red;
                pictureCompensatorState.Image = R.At40x_Red;
                pictureNivelState.Image = R.At40x_Red;
                pictureInitialisationState.Image = R.At40x_Red;
                pictureAdmState.Image = R.At40x_Red;
                labelBatteryControlerState.Text = "%"; labelBatteryControlerState.ForeColor = COLOR_BAD;
                labelBatterySensorState.Text = "%"; labelBatterySensorState.ForeColor = COLOR_BAD;
                //this.buttonLocking.BackgroundImage = R.At40x_Release;
                this.pMeasLock.Image = R.At40x_Red;
                this.pMeasTsu.Image = R.At40x_Red;
            }
        }

        #endregion

        #region Initialisation

        private void On_Button_Initialisation_Click(object sender, EventArgs e)
        {
            if (!IsOn) return;

            if (this.Module.GetInitializationStatus())
            {
                string titleAndMessage = $"{R.T_ALREADY_INITIALIZED};{R.T_ARE_YOU_SURE_YOU_WANT_TO_INTIALIZED_THE_TRACKER_AGAIN_IF_YOU_HAVE_STARTED_SOME_MEASUREMENT_YOU_SHOULD_THEN_PROBABLY_RESTART_THE_STATION}";
                Mess.MessageInput mi = new Mess.MessageInput(Mess.MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_NO, R.T_YES },
                };
                if (mi.Show().TextOfButtonClicked != R.T_YES)
                {
                    return;
                }
            }

            // do it sync
            Action innerSync = () =>
            {
                Module._status.isTrackerBusy = true;
                Module.InitializeByLeica_Sync();
                UpdateSystemStatus();  // normaly trigger by the event initchanged.
            };

            // or do it async
            Action innerAsync = () =>
            {
                Module.InitializeByLeica_Async();
            };
            this.TrySyncActionWithMessage(innerSync, "Initialization", "in Progress...", 1, 56000, allowCancel: false);
        }


        #endregion

        #region Reflector

        private void On_Button_RFI_Click(object sender, EventArgs e)
        {
            void inner() { SetReflector(Target.TSU_TargetType.ES_TT_RFIPrism); }
            this.TrySyncActionWithMessage(inner, R.T_CHANGE_REFLECTOR, R.T_SWITCHING_TO + " RFI", 1, 2000);

        }
        private void On_Button_RRR0_5_Click(object sender, EventArgs e)
        {
            Action inner = () => { SetReflector(Target.TSU_TargetType.ES_TT_RRR05); };
            this.TrySyncActionWithMessage(inner, R.T_CHANGE_REFLECTOR, R.T_SWITCHING_TO + " RRR 0.5 inch", 1, 2000);
        }
        private void On_Button_RRR1_5_Click(object sender, EventArgs e)
        {
            Action inner = () => { SetReflector(Target.TSU_TargetType.ES_TT_RRR15); };
            this.TrySyncActionWithMessage(inner, R.T_CHANGE_REFLECTOR, R.T_SWITCHING_TO + " RRR 1.5 inch", 1, 2000);
        }
        private void On_Button_TBR_Click(object sender, EventArgs e)
        {
            Action inner = () => { SetReflector(Target.TSU_TargetType.ES_TT_TBR05); };
            this.TrySyncActionWithMessage(inner, R.T_CHANGE_REFLECTOR, R.T_SWITCHING_TO + " TBR", 1, 2000);
        }
        private void On_Button_BProbe_Click(object sender, EventArgs e)
        {
            Action inner = () => { SetProbe(ES_ProbeType.ES_PT_BProbe); };
            this.TrySyncActionWithMessage(inner, R.T_CHANGE_REFLECTOR, R.T_SWITCHING_TO + " B-Probeh", 1, 2000);
        }
        private void On_Button_CCR1_5_Click(object sender, EventArgs e)
        {
            Action inner = () => { SetReflector(Target.TSU_TargetType.ES_TT_CCR15); };
            this.TrySyncActionWithMessage(inner, R.T_CHANGE_REFLECTOR, R.T_SWITCHING_TO + " CCR 1.5 inch", 1, 2000);
        }
        private void On_Button_CCR3_5_Click(object sender, EventArgs e)
        {
            Action inner = () => { SetReflector(Target.TSU_TargetType.ES_TT_CCR35); };
            this.TrySyncActionWithMessage(inner, R.T_CHANGE_REFLECTOR, R.T_SWITCHING_TO + " CCR 3.5 inch", 1, 2000);
        }


        List<ToolStripMenuItem> buttonPrismes;

        public void UpdateReflectorPictureList()
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action(() => UpdateReflectorPictureList()));
                return;
            }

            if (buttonPrismes == null)
            {
                buttonPrismes = new List<ToolStripMenuItem>();
                foreach (Target.Structure stru in Module._target.listBoth)
                {
                    ToolStripMenuItem button = new ToolStripMenuItem();
                    button.Name = stru.name;
                    switch (stru.targetType)
                    {
                        case Target.TSU_TargetType.ES_TT_RFIPrism:
                            button.Text = "RFI 0.5 inch (r = 6.35 mm)";
                            button.Image = R.At40x_RFI;
                            button.Click += this.On_Button_RFI_Click;
                            break;
                        case Target.TSU_TargetType.ES_TT_RRR05:
                            button.Text = "RRR 0.5 inch (r = 6.35 mm)";
                            button.Image = R.At40x_RRR0_5;
                            button.Click += this.On_Button_RRR0_5_Click;
                            break;
                        case Target.TSU_TargetType.ES_TT_RRR15:
                            button.Text = "RRR 1.5 inch (r = 19.05 mm)";
                            button.Image = R.At40x_RRR1_5;
                            button.Click += this.On_Button_RRR1_5_Click;
                            break;
                        case Target.TSU_TargetType.ES_TT_TBR05:
                            button.Text = "TBR 0.5 inch (r = 6.35 mm)";
                            button.Image = R.At40x_TBR;
                            button.Click += this.On_Button_TBR_Click;
                            break;
                        case Target.TSU_TargetType.ES_TT_Unknown:
                            button.Text = "B-Probe";
                            button.Image = R.At40x_BProbe;
                            button.Click += this.On_Button_BProbe_Click;
                            break;
                        case Target.TSU_TargetType.ES_TT_AutoCollMirror:
                        case Target.TSU_TargetType.ES_TT_BRR05:
                        case Target.TSU_TargetType.ES_TT_BRR15:
                        case Target.TSU_TargetType.ES_TT_CatsEye:
                        case Target.TSU_TargetType.ES_TT_CornerCube:
                        case Target.TSU_TargetType.ES_TT_GlassPrism:
                        default:
                            button.Text = button.Name;
                            button.Image = R.At40x_PrismeUnknown;
                            //button.Click += new System.EventHandler(this.On_Button_BProbe_Click);
                            break;
                    }
                    button.ImageScaling = ToolStripItemImageScaling.None;
                    buttonPrismes.Add(button);
                }

                // add one for the CCR used by ASG and not existing in AT because same as RRR1.5
                ToolStripMenuItem buttonCCR1_5 = new ToolStripMenuItem() { Name = "CCR1.5", Text = "CCR 1.5 inch (r = 19.05 mm)" };
                buttonCCR1_5.Image = R.At40x_CCR1_5;
                buttonCCR1_5.Click += this.On_Button_CCR1_5_Click;
                buttonPrismes.Add(buttonCCR1_5);

                // en attente de geode
                //ToolStripMenuItem buttonCCR3_5 = new ToolStripMenuItem() { Name = "CCR3.5", Text = "CCR 3.5 inch (r = 88.90 mm)" };
                //buttonCCR3_5.Image = R.At40x_CCR3_5;
                //buttonCCR3_5.Click += new System.EventHandler(this.On_Button_CCR3_5_Click);
                //buttonPrismes.Add(buttonCCR3_5);

            }


            switch (Module._target.selected.targetType)
            {
                case Target.TSU_TargetType.ES_TT_RFIPrism:
                    buttonPrism.BackgroundImage = R.At40x_RFI;
                    break;
                case Target.TSU_TargetType.ES_TT_RRR05:
                    buttonPrism.BackgroundImage = R.At40x_RRR0_5;
                    break;
                case Target.TSU_TargetType.ES_TT_RRR15:
                    buttonPrism.BackgroundImage = R.At40x_RRR1_5;
                    break;
                case Target.TSU_TargetType.ES_TT_TBR05:
                    buttonPrism.BackgroundImage = R.At40x_TBR;
                    break;
                case Target.TSU_TargetType.ES_TT_CCR15:
                    buttonPrism.BackgroundImage = R.At40x_CCR1_5;
                    break;
                case Target.TSU_TargetType.ES_TT_CCR35:
                    buttonPrism.BackgroundImage = R.At40x_CCR3_5;
                    break;
                case Target.TSU_TargetType.ES_TT_AutoCollMirror:
                case Target.TSU_TargetType.ES_TT_BRR05:
                case Target.TSU_TargetType.ES_TT_BRR15:
                case Target.TSU_TargetType.ES_TT_CatsEye:
                case Target.TSU_TargetType.ES_TT_CornerCube:
                case Target.TSU_TargetType.ES_TT_GlassPrism:
                case Target.TSU_TargetType.ES_TT_Unknown:
                default:
                    buttonPrism.BackgroundImage = R.At40x_PrismeUnknown;
                    break;
            }
        }

        public void SetReflectorPicture(Target.TSU_TargetType type)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action(() => SetReflectorPicture(type)));
                return;
            }

            switch (type)
            {
                case Target.TSU_TargetType.ES_TT_RRR15:
                    buttonPrism.BackgroundImage = R.At40x_RRR1_5;
                    break;
                case Target.TSU_TargetType.ES_TT_RRR05:
                    buttonPrism.BackgroundImage = R.At40x_RRR0_5;
                    break;
                case Target.TSU_TargetType.ES_TT_RFIPrism:
                    buttonPrism.BackgroundImage = R.At40x_RFI;
                    break;
                case Target.TSU_TargetType.ES_TT_TBR05:
                    buttonPrism.BackgroundImage = R.At40x_TBR;
                    break;
                case Target.TSU_TargetType.ES_TT_CCR15:
                    buttonPrism.BackgroundImage = R.At40x_CCR1_5;
                    break;
                case Target.TSU_TargetType.ES_TT_CCR35:
                    buttonPrism.BackgroundImage = R.At40x_CCR3_5;
                    break;
                case Target.TSU_TargetType.ES_TT_Unknown:
                default:
                    buttonPrism.BackgroundImage = R.At40x_PrismeOther;
                    break;
            }
        }
        public void SetReflectorPicture(ES_ProbeType type)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action(() => SetReflectorPicture(type)));
                return;
            }

            switch (type)
            {
                case ES_ProbeType.ES_PT_BProbe:
                    buttonPrism.BackgroundImage = R.At40x_BProbe;
                    break;
                //case ES_ProbeType.ES_PT_LAS:
                case ES_ProbeType.ES_PT_MachineControlProbe:
                case ES_ProbeType.ES_PT_MachineControlProbeMultiSide:
                case ES_ProbeType.ES_PT_None:
                case ES_ProbeType.ES_PT_Reflector:
                case ES_ProbeType.ES_PT_TCamToTrackerTool:
                //case ES_ProbeType.ES_PT_TMC30E:
                case ES_ProbeType.ES_PT_TProbe:
                case ES_ProbeType.ES_PT_TScan:
                case ES_ProbeType.ES_PT_ZoomArtifactTool:
                default:
                    buttonPrism.BackgroundImage = R.At40x_PrismeUnknown;
                    break;
            }
            this.IsWaiting = false;
        }

        private void On_Button_Prismes_MouseEnter(object sender, EventArgs e)
        {
            //if (IsOn && !Module._liveData.IsOn) 
            //{
            //    //TryAction(Module._target.Get, "Getting Target Infos");
            //    UpdateReflectorPictureList();
            //}
        }
        private void SetReflector(Target.TSU_TargetType targetType)
        {
            Logs.Log.AddEntryAsBeginningOf(this._Module, R.T_CHANGING_REFLECTOR);

            this.Module._target.RequestedType = targetType;
            this.Module._target.InstrumentType = InstrumentTypes.Unknown;
            Module._target.Set(targetType);
        }
        private void SetProbe(ES_ProbeType probeType)
        {
            Logs.Log.AddEntryAsBeginningOf(this._Module, R.T_CHANGING_REFLECTOR);
            Module._target.Set(probeType);
        }
        private void On_Button_Lock_Click(object sender, EventArgs e)
        {
            TrySyncActionWithMessage(() => { Module.Lock(); }, R.T_LOCKING, R.T_IN_PROGRESS);
        }
        private void On_Button_Release_Click(object sender, EventArgs e)
        {
            TrySyncActionWithMessage(() => { Module._target.Release(); }, R.T_RELEASING, R.T_IN_PROGRESS);
        }
        private void On_Button_Search_Click(object sender, EventArgs e)
        {
            TrySyncActionWithMessage(() => { this.Module._target.PowerSearch(); }, R.T_SEARCHING_REFLECTOR, R.T_IN_PROGRESS);
        }

        #endregion

        #region Status

        public ES_TrackerStatus trackerStatus;

        public override void UpdateView()
        {
            if (this.InvokeRequired)
            {
                // Invoke the event handler on the main UI thread
                this.BeginInvoke(new Action(() => UpdateView()));
            }
            else
            {
                TryAction(() => UpdateSystemStatus());
                if (this.Module.toBeMeasuredData == null) // hide goto buttons
                    this.AllowGoto(false);
            }
        }

        private void UpdateLedAndButtonVisiblityBasedOnTrackerState(ES_TrackerProcessorStatus TrackerState)
        {
            panelConnected.Visible = false;
            panelCaJoLiConnected.Visible = false;

            switch (TrackerState)
            {
                case ES_TrackerProcessorStatus.ES_TPS_NoTPFound:
                    pictureControlerState.Image = R.At40x_Red;
                    pictureSensorState.Image = R.At40x_Red;

                    break;
                case ES_TrackerProcessorStatus.ES_TPS_TPFound:
                    pictureControlerState.Image = R.At40x_Orange;
                    pictureSensorState.Image = R.At40x_Orange;
                    break;
                case ES_TrackerProcessorStatus.ES_TPS_Booted:
                    pictureControlerState.Image = R.At40x_Green;
                    pictureSensorState.Image = R.At40x_Green;
                    buttonInit.Visible = true;

                    panelConnected.Visible = true;
                    panelCaJoLiConnected.Visible = true;
                    break;
                case ES_TrackerProcessorStatus.ES_TPS_CompensationSet:
                    pictureControlerState.Image = R.At40x_Green;
                    pictureSensorState.Image = R.At40x_Green;
                    pictureCompensatorState.Image = R.At40x_Green;
                    //pictureInitialisationState.Image = R.At40x_Red;
                    panelConnected.Visible = true;
                    panelCaJoLiConnected.Visible = true;
                    break;
                case ES_TrackerProcessorStatus.ES_TPS_Initialized:
                    pictureControlerState.Image = R.At40x_Green;
                    pictureSensorState.Image = R.At40x_Green;
                    pictureCompensatorState.Image = R.At40x_Green;
                    //pictureInitialisationState.Image = R.At40x_Green;
                    panelConnected.Visible = true;
                    panelCaJoLiConnected.Visible = true;
                    break;
                default:
                    break;
            }

        }

        internal void ShowStatusAsOff()
        {
            if (this.InvokeRequired)
            {
                // Invoke the event handler on the main UI thread
                this.BeginInvoke(new Action(() => ShowStatusAsOff()));
            }
            else
            {
                pictureControlerState.Image = R.At40x_Red;
                pictureCompensatorState.Image = R.At40x_Red;
                pictureInitialisationState.Image = R.At40x_Red;
                pictureSensorState.Image = R.At40x_Red;
                pictureSensorIdealTemperatureState.Image = R.At40x_Red;
                pictureAdmState.Image = R.At40x_Red;
                labelBatterySensorState.Text = "?"; labelBatterySensorState.ForeColor = COLOR_BAD;
                labelBatteryControlerState.Text = "?"; labelBatteryControlerState.ForeColor = COLOR_BAD;
                pictureCompensatorState.Image = R.At40x_Red;
                buttonConnect.BackColor = COLOR_BAD;
                UpdateLedAndButtonVisiblityBasedOnTrackerState(ES_TrackerProcessorStatus.ES_TPS_NoTPFound);
                this.batteryUpdateTimer.Stop();
            }
        }

        Timer batteryUpdateTimer = new Timer() { Interval = 60 * 1000, Enabled = false };

        private void UpdateSystemStatus(bool updateVisual = true)
        {
            if (this.InvokeRequired)
            {
                Debug.WriteInConsole($"UpdateSystemStatus Invoked, updateVisual={updateVisual}");
                // Invoke the event handler on the main UI thread
                this.BeginInvoke(new Action(() => UpdateSystemStatus(updateVisual)));
            }
            else
            {

                this.Refresh();
                Debug.WriteInConsole($"Begin UpdateSystemStatus updateVisual={updateVisual}");
                try
                {
                    if (this.Module.LiveData.IsOn)
                        this.Module.LiveData.Stop();
                    if (!IsOn)
                    {
                        ShowStatusAsOff();
                    }
                    else
                    {
                        // if busy try later
                        if (this.Module._status.isTrackerBusy)
                        {
                            Timer tryAgainTimer = new Timer() { Interval = 1000, Enabled = true };
                            tryAgainTimer.Tick += delegate
                            {
                                tryAgainTimer.Stop();
                                tryAgainTimer.Dispose();
                                UpdateSystemStatus();

                            };
                            return;
                        }

                        buttonConnect.BackColor = COLOR_GRAY;

                        this.SuspendLayout();

                        Module._status.isTrackerBusy = true;
                        Module._status.GetSystemStatus();
                        Module.GetInitializationStatus();
                        Module._status.GetBatteryLevel();
                        Module._status.GetInclinationSensorState();

                        double t, p, h;
                        Module._status.GetEnvironmentParameters(out t, out p, out h);
                        int NivelTol;
                        Module._status.GetNivelMeasurement(out NivelTol);
                        Module._status.CheckNivelInWorkingRange();
                        Module._circle.Get();

                        Logs.Log.AddEntryAsFinishOf(this._Module, "Status updated");

                        Module._status.GetTrackerStatus();

                        if (updateVisual)
                            UpdateSystemStatusVisual();
                    }
                }
                catch (Exception ex)
                {
                    Logs.Log.AddEntryAsError(this._Module, string.Format("Status updated Failed: {0}", "\r\n" + ex.Message));
                }
                finally
                {
                    Module.Ready();

                    this.ResumeLayout();
                }
                Debug.WriteInConsole($"End UpdateSystemStatus");

                this.Refresh();
            }
        }

        private int lastSensorBatteryLevel = 0;
        private int lastControllerBatteryLevel = 0;
        private DateTime lastLevelWarningTime = DateTime.Now - TimeSpan.FromMinutes(3);

        public void UpdateSystemStatusVisual()
        {
            if (this.InvokeRequired)
            {
                // Invoke the event handler on the main UI thread
                this.BeginInvoke(new Action(() => UpdateSystemStatusVisual()));
            }
            else
            {
                try
                {
                    if (IsOn) panelConnected.Visible = true;
                    else
                        panelConnected.Visible = false;
                    switch (this.Module._measurement.mode)
                    {
                        case Module.aT40xMeasurementMode.Standard:
                            this.picture_State_Measure_Mode.Image = R.At40x_Precision_Standard;
                            break;
                        case Module.aT40xMeasurementMode.Precise:
                            this.picture_State_Measure_Mode.Image = R.At40x_Precision_Precise;
                            break;
                        case Module.aT40xMeasurementMode.Fast:
                            this.picture_State_Measure_Mode.Image = R.At40x_Precision_Fast;
                            break;
                        case Module.aT40xMeasurementMode.Outdoor:
                            this.picture_State_Measure_Mode.Image = R.At40x_Precision_Outdoor;
                            break;
                        default:
                            break;
                    }

                    // Tracker
                    UpdateLedAndButtonVisiblityBasedOnTrackerState(Module._status.trackerProcessorStatus);

                    // Temperature
                    if (WarmedUp)
                    {
                        pictureSensorIdealTemperatureState.Image = R.At40x_Green;
                        toolTip1.SetToolTip(pictureSensorIdealTemperatureState, R.T_INSTRUMENT_IS_WARM);
                    }
                    else
                    {
                        pictureSensorIdealTemperatureState.Image = R.At40x_Blue;
                        toolTip1.SetToolTip(pictureSensorIdealTemperatureState, R.T_MESSAGE_NOT_RECEIVED_FROM_INSTRUMENT_BUT);
                    }

                    // Init
                    if (Module.IsInitialized)
                        pictureInitialisationState.Image = R.At40x_Green;
                    else
                        pictureInitialisationState.Image = R.At40x_Red;

                    // Lazer
                    switch (Module._status.laserStatus)
                    {
                        case ES_LaserProcessorStatus.ES_LPS_LCPCommFailed:
                            pictureLaserState.Image = R.At40x_Red;
                            break;
                        case ES_LaserProcessorStatus.ES_LPS_LCPNotAvail:
                            pictureLaserState.Image = R.At40x_Red;
                            break;
                        case ES_LaserProcessorStatus.ES_LPS_LaserHeatingUp:
                            pictureLaserState.Image = R.At40x_Orange;
                            break;
                        case ES_LaserProcessorStatus.ES_LPS_LaserOff:
                            pictureLaserState.Image = R.At40x_Red;
                            break;
                        case ES_LaserProcessorStatus.ES_LPS_LaserReady:
                            pictureLaserState.Image = R.At40x_Green;
                            break;
                        case ES_LaserProcessorStatus.ES_LPS_UnableToStabilize:
                            pictureLaserState.Image = R.At40x_Red;
                            break;
                        default:
                            break;
                    }

                    // ADM
                    switch (Module._status.admStatus)
                    {
                        case ES_ADMStatus.ES_AS_ADMBusy:
                            pictureAdmState.Image = R.At40x_Orange;
                            break;
                        case ES_ADMStatus.ES_AS_ADMCommFailed:
                            pictureAdmState.Image = R.At40x_Red;
                            break;
                        case ES_ADMStatus.ES_AS_ADMReady:
                            pictureAdmState.Image = R.At40x_Green;
                            break;
                        case ES_ADMStatus.ES_AS_HWError:
                            pictureAdmState.Image = R.At40x_Red;
                            break;
                        case ES_ADMStatus.ES_AS_NoADM:
                            pictureAdmState.Image = R.At40x_Red;
                            break;
                        case ES_ADMStatus.ES_AS_NotCompensated:
                            pictureAdmState.Image = R.At40x_Red;
                            break;
                        case ES_ADMStatus.ES_AS_SecurityLockActive:
                            pictureAdmState.Image = R.At40x_Blue;
                            break;
                        default:
                            break;
                    }

                    // battery level
                    {

                        int ls = Module._status.batteryLevelSensor;
                        int lc = Module._status.batteryLevelController;

                        // detect wrong value that are sometime send by instrument if demands are done too quickly (2%)
                        bool problemDetected = false;
                        if (lastSensorBatteryLevel == 0) lastSensorBatteryLevel = ls;
                        if (lastControllerBatteryLevel == 0) lastControllerBatteryLevel = lc;

                        // detect too quick level change
                        if (lastSensorBatteryLevel - ls > 10)
                            problemDetected = true;

                        if (lastControllerBatteryLevel - lc > 10)
                            problemDetected = true;

                        if (!problemDetected)
                        {
                            // sensor
                            if (ls > 30) UpdateOneStatus(labelBatterySensorState, ls.ToString(), COLOR_GOOD);
                            else if (ls > 10) UpdateOneStatus(labelBatterySensorState, ls.ToString(), COLOR_AVERAGE);
                            else if (ls > 0) UpdateOneStatus(labelBatterySensorState, ls.ToString(), COLOR_AVERAGE);
                            else if (ls == -1) UpdateOneStatus(labelBatterySensorState, "NO", COLOR_BAD);
                            else
                            {
                                UpdateOneStatus(labelBatterySensorState, ls.ToString(), COLOR_BAD);
                                if (DateTime.Now - lastLevelWarningTime > TimeSpan.FromMinutes(2))
                                {
                                    lastLevelWarningTime = DateTime.Now;
                                    Logs.Log.AddEntryAsError(this._Module, "Level of the Sensor Battery is " + ls.ToString() + "%");
                                }
                            }

                            // controller
                            if (lc > 30) UpdateOneStatus(labelBatteryControlerState, lc.ToString(), COLOR_GOOD);
                            else if (lc > 10) UpdateOneStatus(labelBatteryControlerState, lc.ToString(), COLOR_AVERAGE);
                            else if (lc == -1) UpdateOneStatus(labelBatteryControlerState, "NO", COLOR_BAD);
                            else
                            {
                                UpdateOneStatus(labelBatteryControlerState, lc.ToString(), COLOR_BAD);
                                if (DateTime.Now - lastLevelWarningTime > TimeSpan.FromMinutes(2))
                                {
                                    lastLevelWarningTime = DateTime.Now;
                                    Logs.Log.AddEntryAsError(this._Module, "Level of the Controller Battery is " + lc.ToString() + "%");
                                }
                            }
                        }
                    }

                    // inclinaison
                    switch (Module._status.inclinationSensorState)
                    {
                        case 0:
                            pictureCompensatorState.Image = R.At40x_Red;
                            pictureNivelState.Image = R.At40x_Red;
                            break;
                        case 1:
                            pictureCompensatorState.Image = R.At40x_Orange;
                            pictureNivelState.Image = R.At40x_Orange;
                            break;
                        case 2:
                            pictureCompensatorState.Image = R.At40x_Green;
                            toolTip1.SetToolTip(this.pictureNivel, Module._status.NivelValues);
                            if (Module._status.IsNivelInWorkingRange)
                            {
                                if (Module._status.NivelTolerance == 0)
                                    pictureNivelState.Image = R.At40x_Green;
                                if (Module._status.NivelTolerance == 1)
                                    pictureNivelState.Image = R.At40x_Orange;
                                if (Module._status.NivelTolerance == 2)
                                    pictureNivelState.Image = R.At40x_Red;
                            }
                            else
                                pictureNivelState.Image = R.At40x_Red;
                            break;
                        default: break;
                    }

                    //if (Module.IsInitialized())
                    //    this.pInit.Image = R.At40x_Green;
                    //else
                    //    this.pInit.Image = R.At40x_Red;

                    NameToShow = Module.At40x._Model + " " + Module._status.lTrackerSerialNumber;
                    labelModel.Text = NameToShow;
                    UpdateMeteo();
                    // face
                    switch (Module._circle._selectedTsu)
                    {
                        case FaceType.Face1:
                            picture_State_Face.Image = R.At40x_CercleGauche;
                            Module._circle._selected = ES_TrackerFace.ES_TF_Face1;
                            Module._circle._selectedTsu = FaceType.Face1;
                            break;
                        case FaceType.Face2:
                            picture_State_Face.Image = R.At40x_CercleDroit;
                            Module._circle._selected = ES_TrackerFace.ES_TF_Face2;
                            Module._circle._selectedTsu = FaceType.Face2;
                            break;
                        default:
                            break;
                    }

                    Refresh();

                }
                catch (Exception ex)
                {
                    Logs.Log.AddEntryAsError(this.Module, "Failed to update visual status: " + ex.Message);
                }

            }
        }

        private void UpdateOneStatus(Label controlState, string value, Color cOLOR_GOOD)
        {
            controlState.Text = value;
            controlState.ForeColor = COLOR_GOOD;
        }



        internal void UpdateMeteo()
        {
            if (this.InvokeRequired)
            {
                // Invoke the event handler on the main UI thread
                this.BeginInvoke(new Action(() => UpdateMeteo()));
                return;
            }
            labelTemperatureState.Text = this.Module._status.temperature.ToString("F1") + "°";
            labelPressure.Text = Math.Round(this.Module._status.pressure).ToString() + "mBar";
            labelHumidity.Text = Math.Round(this.Module._status.humidity).ToString() + "%";
        }


        #endregion

        #region Compensator

        private void ChangeCompensatorState()
        {
            this.Module._status.GetInclinationSensorState();
            if (this.Module._status.inclinationSensorState == 0) // 0 means off, 2 means applying corrections
            {
                this.Module._status.TurnCompensator(true);
            }
            else
            {
                string question = R.Q_Compensator;
                string yes = R.T_YES;
                string no = R.T_NO;
                Mess.MessageInput mi = new Mess.MessageInput(Mess.MessageType.Choice, question)
                {
                    ButtonTexts = new List<string> { no, yes },
                };
                if (mi.Show().TextOfButtonClicked == yes)
                    this.Module._status.TurnCompensator(false);
            }
        }
        #endregion

        #region Measure

        private void On_Button_Measure_Click(object sender, EventArgs e)
        {
            this.Module.QuickMeasureOneFaceWanted = false;
            this.Module.QuickMeasureTwoFaceWanted = false;
            this.Module.GotoWanted = false;
            LaunchMeasure();
        }

        public void On_Button_GotoAndMeasure_Click(object sender, EventArgs e)
        {
            this.Module.QuickMeasureOneFaceWanted = false;
            this.Module.QuickMeasureTwoFaceWanted = false;
            this.Module.GotoWanted = true;
            LaunchMeasure();
        }

        public void LaunchMeasure()
        {
            Action inner = () =>
            {
                if (Module.LiveData.IsOn)
                {
                    Module.View.liveDataView.ButtonStop_Click(this, new EventArgs());
                }
                else
                {
                    if (Module.IsReadyToMeasure())
                    {
                        this.Module.CheckWhatToMeasure();

                        int faceFactor = ((this.Module.toBeMeasuredData as Polar.Measure).Face == FaceType.DoubleFace) ? 2 : 1; // on multiplie par deux si on fait deux faces
                        int NumberOfMeasFactor = (this.Module.toBeMeasuredData as Polar.Measure).NumberOfMeasureToAverage;
                        int stepNumber = (3 * NumberOfMeasFactor) * faceFactor + 1; //3 for {move, lock, measure}
                        int millisPerMeasure;
                        switch (this.Module._measurement.mode)
                        {
                            case Module.aT40xMeasurementMode.Standard: millisPerMeasure = 3000; break;
                            case Module.aT40xMeasurementMode.Precise: millisPerMeasure = 5000; break;
                            case Module.aT40xMeasurementMode.Fast: millisPerMeasure = 1000; break;
                            case Module.aT40xMeasurementMode.Outdoor:
                            default: millisPerMeasure = 500; break;
                        }
                        this.Module._camera.CameraWasOnWhenMeasureWasLaunched = this.Module._camera.isOn;
                        this.cameraView.Stop();
                        this.Module._camera.isOn = false; // it is turn OFF by the measurement

                        System.Threading.Thread.Sleep(500);
                        TrySyncActionWithMessage(() => this.Module.Measure(), R.T_MEASURING, R.T_IN_PROGRESS, stepNumber, stepNumber * millisPerMeasure);
                    }
                }
            };

            // si livedata is On, we switch ot off, but for the meas mode and the measure state to be reset it need 1000ms, so we launch the measurement after 1500ms
            //if (Module._liveData.IsOn)
            //{
            //    this.ShowMessageOfExclamation(R.T_PLEASE_STOP_LIVE_DATA_FIRST);
            //}
            //else
            //{
            //    TryAction(inner, R.T_MEASURE_CHECK, false, true);
            //}
            TryAction(inner, R.T_MEASURE_CHECK, false, true);
        }

        public override void MeasureAsync()
        {
            base.MeasureAsync();
            LaunchMeasure();
        }


        internal override void CancelAction()
        {
            if (this.Module._BeingMeasuredTheodoliteData != null)
                this.Module._BeingMeasuredTheodoliteData._Status = new M.States.Cancel();
        }
        private void ChangeMeasurementMode()
        {
            string question = R.Q_MeasMod;
            string yes = R.T_YES;
            string no = R.T_NO;
            Mess.MessageInput mi = new Mess.MessageInput(Mess.MessageType.Choice, question)
            {
                ButtonTexts = new List<string> { no, yes },
            };
            if (mi.Show().TextOfButtonClicked == yes)
            {
                question = R.Q_WhichMeasMod;
                string Fast = R.t_Fast;
                string Standard = R.t_Standard;
                string Others = R.t_Others;
                Mess.MessageInput mi1 = new Mess.MessageInput(Mess.MessageType.Choice, question)
                {
                    ButtonTexts = new List<string> { Others, Standard, Fast },
                };
                string respond = mi1.Show().TextOfButtonClicked;

                if (respond == Fast)
                    this.Module._measurement.mode = Module.aT40xMeasurementMode.Fast;
                else if (respond == Standard)
                {
                    this.Module._measurement.mode = Module.aT40xMeasurementMode.Standard;
                }
                else if (respond == Others)
                {
                    string outdoor = R.t_Outdoor;
                    string precise = R.t_Precise;
                    Mess.MessageInput mi2 = new Mess.MessageInput(Mess.MessageType.Choice, question)
                    {
                        ButtonTexts = new List<string> { outdoor, precise },
                    };
                    if (mi2.Show().TextOfButtonClicked == outdoor)
                        this.Module._measurement.mode = Module.aT40xMeasurementMode.Outdoor;
                    else
                        this.Module._measurement.mode = Module.aT40xMeasurementMode.Precise;
                }

                this.Module._measurement.SetMode();
            }
        }

        private void On_Button_Connection_MouseDown(object sender, MouseEventArgs e)
        {
            contextMenuStrip.Items.Clear();
            if (Access.IsAdministrator()) // we want the button only if we are because you need admin right to restart the adapter or release/renew the IP config
                contextMenuStrip.Items.Add(buttonTurnOnWithWiFi);

            contextMenuStrip.Items.Add(buttonReset);
            contextMenuStrip.Items.Add(buttonTurnOff);
            contextMenuStrip.Items.Add(buttonTurnOn);

            this.ShowContextMenuAboveControlClicked((Control)sender);
        }

        private void TurnOff()
        {
            if (!Module._measurement.IsSimulation)
            {
                TryDisconnect();
            }
        }

        int stepNumber = 1;
        private void AddConnectionStep(BackgroundWorker worker, Action action, string actionBefore, string actionAfter)
        {
            worker.ReportProgress(stepNumber, new Mess.ProgressMessageState(actionBefore, P.StepTypes.Begin));
            action();
            worker.ReportProgress(stepNumber, new Mess.ProgressMessageState(actionAfter, P.StepTypes.End));
            stepNumber++;
        }

        public BackgroundWorker worker = null;

        public void TurnOn()
        {
            if (this.InvokeRequired)
            {
                // Invoke the event handler on the main UI thread
                this.BeginInvoke(new Action(() => TurnOn()));
                return;
            }
            if (this.Module.CheckConnection())
            {
                string message = $"{R.T_ALREADY} {R.T_CONNECTED_TO_INSTRUMENT}";
                Logs.Log.AddEntryAsPayAttentionOf(this.Module, message);
                return;
            }

            // i use a background worker for the connection beacuse it is synchonous activities
            int stepCount = 13;
            int stepLength = 7000;

            this.SetupBackGroundWorker( R.T_CONNECTING_TO_THE_ADAPTER,
                On_ConnectionStarting,
                ToDoOnProgress: On_ConnectionProgress,
                ToDoWhenFinished: On_ConnectionFinished, stepNumber: stepCount, estimatedTimeInMs: stepLength);
        }

        private void On_ConnectionStarting(object source, DoWorkEventArgs args)
        {

            worker = source as BackgroundWorker;

            try
            {
                this.Module.ConnectionInProgress = true;

                stepNumber = 1;

                // prepare connection
                AddConnectionStep(
                    worker,
                    () => { Ip.GetFirstKnownThatReplyToPing(this, Module.At40x._SerialNumber, out this.Module.IP); },
                    R.T_SEARCH_PIGNABLE,
                    R.T_PINGABLE_FOUND);

                AddConnectionStep(
                        worker,
                        () => { this.Module.ResetConnection(thenTurnOn: false); },
                        R.T_RESET_CONNECTION,
                        R.T_RESET_CONNECTION);

                AddConnectionStep(
                    worker,
                    () => { this.Module.SubscribeAt40xEvents(); },
                    "Subscribe",
                    "Subscribe");

                // connect
                AddConnectionStep(
                    worker,
                    () =>
                    {
                        this.Module.ConnectAdapter(this.Module.IP);
                        Logs.Log.AddEntryAsSuccess(this.Module, R.T_CONNECTED_TO + " " + this.Module.IP.ToString());
                    },
                    R.T_CONNECTING_TO_THE_ADAPTER,
                    R.T_ADAPTER_CONNECTED);

                // check serial number
                AddConnectionStep(worker, this.GetSerialNumberAndShowConnected, "Getting serial number", "Got serial number");

                // set parameters
                AddConnectionStep(worker, this.Module._measurement.SetParameters, R.T_SETTING_UP_MEASUREMENT_PARAMETERS, R.T_MEASUREMENT_PARAMETERS_SET);

                // update init state
                if (!this.Module.GetInitializationStatus()) Logs.Log.AddEntryAsPayAttentionOf(this.Module, R.T_NOT_INITIALIZED);

                AddConnectionStep(worker, () =>
                {
                    // permet de garder le mode de measure sélectionné par l'utilsateur avant connection instrument
                    if (!this.Module.measModeSetByuser)
                    {
                        this.Module._measurement.mode = Module.aT40xMeasurementMode.Fast;
                    }
                    this.Module._measurement.SetMode();
                }, R.T_SETTING_UP_MEASUREMENT_MODE, R.T_MEASUREMENT_MODE_SET);

                // Wakeup
                AddConnectionStep(worker, this.Module.WakeUpAdapter, R.T_WAKING_UP, R.T_WOKE_UP);

                // Refelctors
                AddConnectionStep(worker, this.Module._target.GetReflectorsList, "Reflector list Asked", "Reflector list Asked");

                // Probes
                AddConnectionStep(worker, this.Module._target.GetProbesList, "Probe list Asked", "Probe list Asked");

                // Refelctor
                AddConnectionStep(worker, this.Module._target.Get, "Getting actual target", "Actual target Received");
            }
            catch (CancelException)
            {
                throw new CancelException();
            }
            catch (Exception ex)
            {
                // debugger will stop here beacuse it is not handler inside the background worker, but it will be handle in tsunmi
                throw new InvalidOperationException(string.Format("{0};{1}: {2}", R.T_CONNECTION_FAILED, ex.Message, R.T_YOU_SHOULD_TRY_TO_RESET), ex);
            }
        }

        private void On_ConnectionProgress(object source, ProgressChangedEventArgs args)
        {
            // throw new NotImplementedException();
        }

        private void On_ConnectionFinished(object source, RunWorkerCompletedEventArgs args)
        {
            this.Refresh();
            if (args.Error == null)
            {
                Debug.WriteInConsole("In TurnOn, this.Module.async.TryOrDelayAction(this.Module.DeclareInstrumentConnected);");

                //this.Module.async.DelayAction(this.Module.DeclareInstrumentConnected);
                this.Module.DeclareInstrumentConnected();

                if (IsOn)
                {
                    this.Module.ConnectionInProgress = false;
                    if (this.Module.ToBeMeasuredData == null)
                    {
                        //    TryAction(this.Module.WaitMeasurementDetails, "Asking measurement details");
                        this.AllowGoto(false);
                    }

                    if (this.Module.ToBeMeasuredData != null)
                        this.Module.CheckIncomingMeasure(this.Module.ToBeMeasuredData as Polar.Measure);

                    Debug.WriteInConsole("In TurnOn, this.Module.async.TryOrDelayAction(() => UpdateSystemStatus());");

                    //this.Module.async.DelayAction(() => UpdateSystemStatus());
                    UpdateSystemStatus();

                    Module._status.isTrackerLockedAndReady = false;
                    Module._status.isTrackerBusy = false;

                    TryAction(() => OnStatusChanged(this, new StatusEventArgs("Connected", P.StepTypes.EndAll)), "Closing progress bar");
                }
            }
            else
            {
                this.Module.Disconnect();
            }

            this.Refresh();
        }

        private void TurnOnAutomaticallyWithWiFi()
        {
            // i use a background worker for the connection beacuse it is synchonous activities
            int stepCount = 12;
            int stepLength = 7000;

            this.SetupBackGroundWorker(
                "Connection...",
                delegate (object source, DoWorkEventArgs args)
                {
                    worker = source as BackgroundWorker;

                    try
                    {
                        this.Module.ConnectionInProgress = true;

                        stepNumber = 1;

                        // prepare connection
                        
                        AddConnectionStep(worker, () => { Ip.GetFirstKnownThatReplyToPing(this, Module.At40x._SerialNumber, out this.Module.IP, silent: true, orFirstDefault: true); }, R.T_SEARCH_PIGNABLE, R.T_PINGABLE_FOUND);
                        // MN: AddConnectionStep(worker, () => { this.Module.IP = Ip.GetASingleIpForGivenInstrument(Module.At40x._SerialNumber, Tsunami2.Preferences.Values.At401IpAdresses); }, R.T_SEARCH_PIGNABLE, R.T_PINGABLE_FOUND);

                        AddConnectionStep(worker, () => { this.Module.ResetConnection(thenTurnOn: false); }, R.T_RESET_CONNECTION, R.T_RESET_CONNECTION);

                        AddConnectionStep(worker, () => { this.Module.SubscribeAt40xEvents(); }, "Subscribe", "Subscribe");

                        AddConnectionStep(worker, () => { Tsunami2.Preferences.Values.AdminAndNetworkChecks(); }, "Log admin and Wifi info", "Log admin and Wifi info");// because we want it it to be saved to the log

                        // connect
                        AddConnectionStep(worker, () =>
                        {
                            TSU.WiFi.TryToConnectAutomaticallyWithWifi(this.Module.ConnectToAT, this.Module.IP.ToString(), SN: this.Module.At40x._SerialNumber, worker: worker);
                            Logs.Log.AddEntryAsSuccess(this.Module, R.T_CONNECTED_TO + " " + this.Module.IP.ToString());
                        }, "Checking your network setup", R.T_ADAPTER_CONNECTED);

                        // check serial number
                        AddConnectionStep(worker, this.GetSerialNumberAndShowConnected, "Getting serial number", "Got serial number");

                        // set parameters
                        AddConnectionStep(worker, this.Module._measurement.SetParameters, R.T_SETTING_UP_MEASUREMENT_PARAMETERS, R.T_MEASUREMENT_PARAMETERS_SET);

                        // update init state
                        if (!this.Module.GetInitializationStatus()) Logs.Log.AddEntryAsPayAttentionOf(this.Module, R.T_NOT_INITIALIZED);

                        AddConnectionStep(worker, () =>
                        {
                            // permet de garder le mode de measure sélectionné par l'utilsateur avant connection instrument
                            if (!this.Module.measModeSetByuser)
                            {
                                this.Module._measurement.mode = Module.aT40xMeasurementMode.Fast;
                            }
                            this.Module._measurement.SetMode();
                        }, R.T_SETTING_UP_MEASUREMENT_MODE, R.T_MEASUREMENT_MODE_SET);

                        // Wakeup
                        AddConnectionStep(worker, this.Module.WakeUpAdapter, R.T_WAKING_UP, R.T_WOKE_UP);

                        // Reflectors
                        AddConnectionStep(worker, this.Module._target.GetReflectorsList, "Reflector list Asked", "Reflector list Asked");

                        // Probes
                        AddConnectionStep(worker, this.Module._target.GetProbesList, "Probe list Asked", "Probe list Asked");

                        // Reflector
                        AddConnectionStep(worker, this.Module._target.Get, "Getting actual target", "Actual target Received");
                    }
                    catch (CancelException)
                    {
                        //throw new CancelException();
                    }
                    catch (Exception ex)
                    {
                        ////debugger will stop here because it is not handler inside the background worker, but it will be handle in tsunami
                        throw new InvalidOperationException($"{R.T_CONNECTION_FAILED};{ex.Message}\r\n\r\n{R.T_YOU_SHOULD_TRY_TO_RESET}");
                    }
                },
                ToDoOnProgress: On_ConnectionProgress,
                ToDoWhenFinished: On_ConnectionFinished, stepNumber: stepCount, estimatedTimeInMs: stepLength);
        }


        private void GetSerialNumberAndShowConnected()
        {
            string receivedSerialNumber = "0";
            int count = 0;

            //Compute.Analyse.Time.WaitUntil(() => { return (this.Module._target.allReflectorsReceived == true); }, 200, 3000);

            while (receivedSerialNumber == "0" && count < 3)
            {
                TryAction(() => { this.Module._status.GetSystemStatus(); }, "Updating");
                receivedSerialNumber = this.Module._status.lTrackerSerialNumber.ToString();
                count++;
            }

            // if really dont want t o give something else than 0 then keep the one from user
            if (receivedSerialNumber == "0")
                receivedSerialNumber = this.Module.Instrument._SerialNumber;

            // a serrail number is usualy 6digit, sometime error send a longer number
            if (receivedSerialNumber.Length > 6)
                receivedSerialNumber = this.Module.Instrument._SerialNumber;

            if (receivedSerialNumber != this.Module.Instrument._SerialNumber)
            {
                I.Instrument goodOne = Tsunami2.Preferences.Values.Instruments.Find(x => (x._SerialNumber == receivedSerialNumber && (x._InstrumentClass == InstrumentClasses.LASER_TRACKER)));
                if (goodOne != null)
                {
                    this.Module.Instrument = goodOne;
                    Tsunami2.Preferences.Tsunami.InvokeOnApplicationDispatcher(() =>
                         (this.Module.ParentModule as Manager.Module).ParentStationModule.OnInstrumentChanged(goodOne as Sensor));
                }
                else
                {
                    this.Module.Instrument._SerialNumber = this.Module._status.lTrackerSerialNumber.ToString();
                    this.Module.Instrument._Name = this.Module.Instrument._Model + "_" + this.Module.Instrument._SerialNumber;
                }

                string titleAndMessage = $"Instrument renamed {this.Module.Instrument._SerialNumber}";
                new Mess.MessageInput(Mess.MessageType.Warning, titleAndMessage).Show();
            }

        }

        private void ResetConnection()
        {
            string titleAndMessage = $"{R.T_RESET_CONNECTION}?;{R.T_DO_YOU_WANT_TO_RESET_THE_CONNECTION}";
            Mess.MessageInput mi = new Mess.MessageInput(Mess.MessageType.Critical, titleAndMessage)
            {
                ButtonTexts = new List<string> { R.T_YES, R.T_NO }
            };
            if (R.T_YES == mi.Show().TextOfButtonClicked)
            {
                this.Module.ResetConnection();
                this.ShowStatusAsOff();
            }
        }

        #endregion

        #region Movement
        private void On_Button_Goto_Click(object sender, EventArgs e)
        {
            Action inner = () => { Module.Goto(); };
            //    this.TrySyncActionWithMessage(inner, "Moving to a point", "Moving to a point", 1, 2000);
            this.TrySyncActionWithMessage(inner, R.T_MOVING_TO_A_POINT, R.T_MOVING_TO_A_POINT, 1, 2000);
        }

        private void On_Button_Jog_Click(object sender, EventArgs e)
        {
            // force live data to reset
            this.Module.LiveData.StopAnyMeasurement();

            this.jogView.Start();
            this.Module.LiveData.StopAnyMeasurement();
        }


        internal void AllowGoto(bool enable)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action(() => AllowGoto(enable)));
            }
            else
            {
                this.buttonGoto.Visible = enable;
                this.buttonGotoAll.Visible = enable;
            }
        }

        internal override void On_TsuView_KeyDown(object sender, KeyEventArgs e)
        {
            base.On_TsuView_KeyDown(sender, e);

            if (Module.View.jogView.Visible)
            {
                Module.View.jogView.On_TsuView_KeyDown(sender, e);
            }

            switch (e.KeyData)
            {
                case Keys.M:
                    if (Module.Measure().Success)
                        Module.ReportMeasurement();
                    break;
            }
        }
        #endregion

        #region subViews

        public override string ShowInMessageTsu(string titleAndMessage, string middleButton, Action middleAction,
            string rightButton = null, Action rightAction = null, string leftButton = null, Action leftAction = null,
            Color? background = null, Color? foreground = null, bool showdialog = true, bool fullSize = false,
            DsaFlag flagDont = null, EventHandler onShowingMessage = null)
        {
            this.SetSplitContainerOrientation(Orientation.Vertical, invert: false);
            return base.ShowInMessageTsu(titleAndMessage, middleButton, middleAction, rightButton, rightAction,
                leftButton, leftAction, background, foreground, showdialog, fullSize, flagDont, onShowingMessage);
        }

        internal override void CheckOrientation()
        {
            base.CheckOrientation();

            if (this.Parent.Parent is SplitContainer)
            {
                SplitContainer sc = this.Parent.Parent as SplitContainer;
                this.SetSplitContainerOrientation(sc.Orientation, invert: true);
            }
        }

        internal override Orientation SetSplitContainerOrientation(Orientation orientation, bool invert = false)
        {
            Orientation o = orientation;

            if (invert)
            {
                if (orientation == Orientation.Horizontal)
                    o = Orientation.Vertical;
                else
                    o = Orientation.Horizontal;
            }

            // set new orientation
            this.splitAT_V_SubV.Orientation = o;

            // set half distance of parent, do not work because, the paretn is not sized yet
            //if (this.splitContainer1.Parent != null)
            //this.splitContainer1.SplitterDistance = this.splitContainer1.Parent.Height / 2;
            if (o == Orientation.Horizontal)
                this.splitAT_V_SubV.SplitterDistance = this.Height / 2;
            else
                this.splitAT_V_SubV.SplitterDistance = this.Width * 3 / 5;
            this.Invalidate();
            return o;
        }

        private void ShowHideSubViews(object sender, EventArgs e)
        {
            if (splitAT_V_SubV.Panel2Collapsed)
            {
                splitAT_V_SubV.Panel2Collapsed = false;
            }
            else
            {
                splitAT_V_SubV.Panel2Collapsed = true;
                Module.LiveData.StopRoughMeasurementSync();
            }

            //if (splitContainer1.SplitterDistance < splitContainer1.Size.Width - 20)
            //{

            //    previousSize = splitContainer1.SplitterDistance;
            //    splitContainer1.SplitterDistance = splitContainer1.Size.Width;
            //    panelCaJoLi.Visible = false;
            //    this.Module.LiveData.StopRoughMeasurementSync();
            //}
            //else
            //{
            //    splitContainer1.SplitterDistance = previousSize;
            //    panelCaJoLi.Visible = true;
            //}

        }
        public void SetSubView(SubView s)
        {
            SubView old = null;

            if (this.panel4SubViews.Controls.Count > 0) 
                old = this.panel4SubViews.Controls[0] as SubView;

            if (old != s)
            {
                old?.Stop();
                this.panel4SubViews.Controls.Clear();
                this.panel4SubViews.Controls.Add(s);
                s.Show();
            }
        }

        internal override void OnRemovedFromView()
        {
            base.OnRemovedFromView();
            if (this.Module.LiveData.IsOn)
            {
                this.Module.LiveData.Stop();
                this.Module.Ready();
            }
        }

        private void On_Button_Camera_Click(object sender, EventArgs e)
        {
            cameraView.Start();
        }
        private void On_Button_Remote_Click(object sender, EventArgs e)
        {
            remoteView.Start();
        }
        private void On_ButtonRemoteList_Click(object sender, EventArgs e)
        {
            remoteListView.Start();
        }

        private void On_Button_LiveData_Click(object sender, EventArgs e)
        {
            this.liveDataView.Start();
        }

        private void On_Button_Timer_Click(object sender, EventArgs e)
        {
            timerView.Start();
        }

        #endregion

        #region Movement Face

        public ES_TrackerFace face;
        private void On_Button_Face_Click(object sender, EventArgs e)
        {
            try
            {
                if (!this.Module._measurement.IsSimulation)
                {
                    this.TrySyncActionWithMessage(
                        () =>
                        {
                            this.Invoke(new Action(()=> { 
                                this.WaitingForm.BeginAstep("Moving"); 
                            }));
                            this.Module.ChangeFace();
                            Logs.Log.AddEntryAsSuccess(this._Module, "Turned to " + Module._circle.Get().ToString());
                            this.Invoke(new Action(() => {
                                this.WaitingForm.EndAll();
                            }));
                        },
                        R.T_CHANGE_FACE_AND_TRY_TO_MOVE_TO_POINT,
                        R.T_SPEAKING_WITH_INSTRUMENT,
                        1,
                        5000);
                }
                else if (this.Module._circle._selected == ES_TrackerFace.ES_TF_Face1)
                    this.Module._circle._selected = ES_TrackerFace.ES_TF_Face2;
                else
                    this.Module._circle._selected = ES_TrackerFace.ES_TF_Face1;

            }
            catch (Exception ex)
            {
                Logs.Log.AddEntryAsError(this._Module, "Cannot change face...");
            }

        }

        #endregion


        private void At40xView_Shown(object sender, EventArgs e)
        {
            //Addlog();
        }

        private void At40xView_Load(object sender, EventArgs e)
        {
            // using reflection to allow double buffereing to sow reduce the flickering
            typeof(Panel).InvokeMember("DoubleBuffered",
            BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
            null, this, new object[] { true });
            typeof(Panel).InvokeMember("DoubleBuffered",
            BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
            null, this.panelGlobal, new object[] { true });
            typeof(Panel).InvokeMember("DoubleBuffered",
            BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
            null, this.panelMain, new object[] { true });
            typeof(Panel).InvokeMember("DoubleBuffered",
            BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
            null, this, new object[] { true });
            SetAllToolTips();
        }

        private bool EjectedView = false;

        private void On_Button_Eject_Click(object sender, EventArgs e)
        {
            // Do nothing if there's an ejected view (can happen if you click eject in the ejected view)
            if (EjectedView) return;

            TsuView EjectedViewSupport = null;

            // Called to clean the support
            void CleanEjectedViewSupport()
            {
                EjectedView = false;
                EjectedViewSupport?.Dispose();
                EjectedViewSupport = null;
            }

            try
            {
                EjectedView = true;

                //Create a view and move the existing controls to it
                EjectedViewSupport = new TsuView(Tsunami2.Properties);
                MoveControls(this.splitAT_V_SubV.Panel2.Controls, EjectedViewSupport.Controls);

                void ShowInMessageTsuAndClean()
                {
                    try
                    {
                        //Show the support as messageTsu
                        EjectedViewSupport.ShowInMessageTsu(this.Name,
                            "OK", OnOkButtonInDetachedViewClicked,
                            showdialog: true, fullSize: true);

                        
                        //EjectedViewSupport._SelectionView.FormClosed += (a, b) => CleanEjectedViewSupport();
                    }
                    catch ( Exception ex)
                    {
                        TSU.Logs.Log.AddEntryAsPayAttentionOf(this.Module, ex.Message) ;
                    }
                    finally
                    {
                        EjectedViewSupport.Invoke((Action)CleanEjectedViewSupport);
                    }
                }

                void OnOkButtonInDetachedViewClicked()
                {
                    MoveControls(EjectedViewSupport.Controls, this.splitAT_V_SubV.Panel2.Controls);
                }


                Task.Run(ShowInMessageTsuAndClean);
            }
            catch
            {
                CleanEjectedViewSupport();
            }

        }

        /// <summary>
        /// Move controls from one collection to another, using an intermediate list beacause when you add a control it is removed from the source
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        private void MoveControls(Control.ControlCollection source, Control.ControlCollection destination)
        {
            List<Control> intermediate = new List<Control>();

            foreach (Control item in source)
                intermediate.Add(item);

            foreach (Control item in intermediate)
                destination.Add(item);
        }

        private void View_Enter(object sender, EventArgs e)
        {
            this.Focus();
        }

        internal void ShowMeasureDetails()
        {
            if (this.InvokeRequired)
            {
                // Invoke the event handler on the main UI thread
                this.BeginInvoke(new Action(() => ShowMeasureDetails()));
                return;
            }

            if ((Module.ToBeMeasuredData as Polar.Measure).Distance.Reflector != null)
            {
                this.labelModel.Text = String.Format("{0} : {1}, h={2}, r={3}",
                    Module.Instrument._SerialNumber,
                    Module.ToBeMeasuredData._Point?._Name ?? R.T_UNKNOWN_POINT,
                    Module.ToBeMeasuredData.Extension.Value.ToString(),
                    (Module.ToBeMeasuredData as Polar.Measure).Distance.Reflector._Name);
            }
        }


        public void On_Button_UpdateStatus_Click(object sender, EventArgs e)
        {
            ClearStateAndUpdate();
        }


        public void ClearStateAndUpdate()
        {
            this.Module.async.Clear();
            this.actionList.Clear();
            this.Module._status.isTrackerBusy = true;
            lastLevelWarningTime = DateTime.Now - TimeSpan.FromMinutes(3);
            this.TurnLightOff();
            TryAction(() => UpdateSystemStatus());
        }

        private void ShowToolTip_OnMouseEnter(object sender, EventArgs e)
        {
        }
        internal void ShowMeasButton()
        {
            this.buttonMeasure.Visible = true;
        }
        internal void HideMeasButton()
        {
            this.buttonMeasure.Visible = false;
            //this.UpdateView();
        }

        private void On_ButtonRefreshStatus_MouseHover(object sender, EventArgs e)
        {
            string tipText = "Force to update the status, that often fixes problems too.";


            foreach (var item in this.Module.async.InProgressActions)
            {
                tipText = tipText + "\r\nReset: " + item.ToString();
            }
            toolTip1.SetToolTip(buttonRefreshStatus, tipText);

        }

        private void picture_State_Beam_State_Click(object sender, EventArgs e)
        {
            if (Debug.Debugging)
            {
                if (new Mess.MessageInput(Mess.MessageType.Choice, "Debugging detected; du you want to start the livedata simulator?")
                {
                    ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                }.Show().TextOfButtonClicked == R.T_YES)
                {
                    ShowHideSubViews(this, null);
                    this.liveDataView.Start();
                    this.Module.LiveData.Simulate();
                    return;
                }
            }
        }




        private void On_ButtonPrism_Click(object sender, EventArgs e)
        {
            contextMenuStrip.Items.Clear();
            foreach (var item in buttonPrismes)
            {
                contextMenuStrip.Items.Add(item);
            }
            this.ShowContextMenuAboveControlClicked((Control)sender);
        }

        private void On_ButtonShift_Click(object sender, EventArgs e)
        {
            contextMenuStrip.Items.Clear();
            contextMenuStrip.Items.Add(buttonDangereous);
            contextMenuStrip.Items.Add(buttonCaJoLi);
            contextMenuStrip.Items.Add(buttonFace);
            contextMenuStrip.Items.Add(buttonMode);
            contextMenuStrip.Items.Add(buttonPowerSearch);
            contextMenuStrip.Items.Add(buttonInit2);
            this.ShowContextMenuAboveControlClicked((Control)sender);
        }

        //private void buttonLocking_Click(object sender, EventArgs e)
        //{
        //    contextMenuStrip.Items.Clear();
        //    contextMenuStrip.Items.Add(buttonRelease);
        //    contextMenuStrip.Items.Add(buttonLock);
        //    this.ShowContextMenuAboeControlClicked((Control)sender);
        //}

        private void picture_State_Measure_Mode_Click(object sender, EventArgs e)
        {
            this.TryAction(ChangeMeasurementMode);
        }

        private void splitContainer1_DoubleClick(object sender, EventArgs e)
        {
            this.SetSplitContainerOrientation(splitAT_V_SubV.Orientation, invert: true);
        }
    }
}
