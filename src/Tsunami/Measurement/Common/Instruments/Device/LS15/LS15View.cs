﻿using System;


namespace TSU.Common.Instruments.Device.LS15
{
    public class View : DNA03.View
    {
        public View(IModule module)
             : base(module)
        {
        }

        protected new Module Module
        {
            get
            {
                return this._Module as Module;
            }
        }
        protected new Instrument Instrument
        {
            get
            {
                return this.Module.Instrument as Instrument;
            }
        }


        public override void buttonBatteryLevel_Click(object sender, EventArgs e)
        //Force la vérification du niveau batterie et l'ajoute au logbook
        {
            int battLevel = Module.GetIntBatteryLevel();
            TSU.Debug.WriteInConsole($"BATTERY LEVEL: {battLevel}%");
            this.RefreshBatteryLevelButton(battLevel);
            Logs.Log.AddEntryAsPayAttentionOf(this._Module, "The Battery level is " + battLevel + "%");
        }

        public override void buttonMeasure_Click(object sender, EventArgs e)
        //prise de mesure
        {
            LaunchMeasure();
        }

        public override void buttonPortCom_Click(object sender, EventArgs e)
        //choix du port com
        {
            if (base.Module.InstrumentIsOn == false)
            {
                Result result = new Result();
                Module.GetAllPorts();
                result = Module.ChangePort();
                this.ChangeButtonPortCOMImage(Module.portName);
                Logs.Log.AddEntryAsResult(this._Module, result);
            }
        }
        public override void buttonCancel_Click(object sender, EventArgs e)
        {
            this.TryAction(CancelBackGroundWorker);
        }

    }
}
