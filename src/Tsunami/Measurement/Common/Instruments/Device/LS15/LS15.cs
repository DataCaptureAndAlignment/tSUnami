﻿using System.Xml.Serialization;

namespace TSU.Common.Instruments.Device.LS15
{
    [XmlType(TypeName = "LS15.Instrument")]
    public class Instrument : Level
    {
        public Instrument()
        {
            this._levelType = LevelType.LS15;
        }
    }
}
