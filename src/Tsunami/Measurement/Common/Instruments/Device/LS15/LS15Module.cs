﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Management;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml.Serialization;
using TSU.Common.Measures;
using M = TSU;
using R = TSU.Properties.Resources;


namespace TSU.Common.Instruments.Device.LS15
{
    [XmlType(TypeName = "LS15.Module")]
    public class Module : DNA03.Module, IConnectable
    {
        #region Variables

        protected Instrument LS15
        {
            get
            {
                return this.Instrument as Instrument;
            }
        }
        
        #endregion

        #region Constructeur


        public Module(M.Module parentModule, Instruments.Instrument i)
            : base(parentModule, i)
        {
            this.Instrument = i;
        }

        internal SerialPort measurementPort;
        private List<string> GetAllAvailablePorts()
        {
            List<string> ports = new List<string>();
            try
            {
                // Query WMI for Win32_SerialPort devices
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_SerialPort");

                // Iterate through each serial port found
                foreach (ManagementObject port in searcher.Get())
                {
                    string portName = port["DeviceID"].ToString();
                    string description = port["Description"].ToString();

                    TSU.Debug.WriteInConsole($"COM Port: {portName} ({description})");
                    ports.Add(portName);
                }
            }
            catch (ManagementException ex)
            {
                TSU.Debug.WriteInConsole($"Error retrieving COM port information: {ex.Message}");
            }
            return ports;
        }

        /// <summary>
        /// Initialisation des parmètres DNA, fonction appelée depuis la base MODULE
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
            listPortName = GetAllAvailablePorts();
            foreach (string name in listPortName) { TSU.Debug.WriteInConsole($"PORT NAME: {name}"); }
            //BTdevices = new BluetoothHelper().GetAllBluetoothDevices();
            if (listPortName.Count > 0) {
                portName = listPortName[0];
                if (listPortName.Contains("COM4")) portName = "COM4";
            }
            else portName = "Pas de Port disponible";

        }

        #endregion

        #region functions to communicate to LS15

        public override Result SendReceiveData(string input)
        //Envoi d'un string au DNA03 et réception de sa réponse
        {
            Result result = new Result();
            try
            {
                myPort.DiscardInBuffer();
                myPort.DiscardOutBuffer();
                myPort.ReadTimeout = 7000;
                myPort.WriteTimeout = 7000;
                myPort.Write(input + "\r\n");
                string data = myPort.ReadLine();
                switch (data)
                {
                    case "12032" + "\r":
                        result.ErrorMessage = "Too Dark or poor light ";
                        result.Success = false;
                        break;
                    case "12033" + "\r":
                        result.ErrorMessage = "Too bright ";
                        result.ErrorMessage = R.T_INVALID_COMMAND;
                        result.Success = false;
                        break;
                    case "12034" + "\r":
                        result.ErrorMessage = "Instrument not horizontal. Level up the instrument ";
                        result.Success = false;
                        break;
                    case "12035" + "\r":
                        result.ErrorMessage = "Coarse correlation error, Too much coverage or insufficient code length. ";
                        result.Success = false;
                        break;
                    case "12036" + "\r":
                        result.ErrorMessage = "Fine correlation error. Too much coverage or insufficient code length. ";
                        result.Success = false;
                        break;
                    case "12037" + "\r":
                        result.ErrorMessage = "Distance outside permitted range ";
                        result.Success = false;
                        break;
                    case "12038" + "\r":
                        result.ErrorMessage = "Staff inverted or inverse mode activated ";
                        result.Success = false;
                        break;
                    case "12039" + "\r":
                        result.ErrorMessage = "Bad focusing ";
                        result.Success = false;
                        break;
                    default:
                        result.AccessToken = data;
                        result.Success = true;
                        break;
                }
                return result;
            }
            catch (Exception e)
            {
                e.Data.Clear();
                result.Success = false;
                result.ErrorMessage = "Connection problem";
                return result;
            }
        }

        private Result GetBatteryLevel() { return SendReceiveData("%R1Q,5039:"); } //Communication avec LS15 :demande et réception du niveau de batterie
        private Result StartAutoFocus() { return SendReceiveData("%R1Q,29068:"); }
        private Result GetSerialNumberReq() { return SendReceiveData("%R1Q,5003:"); }
        private Result GetCompass() { return SendReceiveData("%R1Q,29072:"); }


        /// <summary>
        /// Communication avec DNA 03 : demande et réception du numéro de série au DNA03
        /// </summary>
        /// <returns></returns>
        public override Result GetSerialNumber()
        {
            Result request = GetSerialNumberReq();
            Result serialNumber = new Result();

            if (request.Success)
            {
                serialNumber.Success = true;
                string[] parts = request.AccessToken.Split(',');
                if (parts.Length >= 4)
                {
                    serialNumber.AccessToken = parts[3];

                    TSU.Debug.WriteInConsole("Serial Number from the instrument: " + serialNumber);
                    return serialNumber;
                }
            }
            serialNumber.Success = false;
            serialNumber.ErrorMessage = "Serial Number not retrieved";
            return serialNumber;
        }

        /// <summary>
        /// Connection via Bluetooth 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public override Result Connect()
        {
            Result result = new Result();
            try
            {
                myPort = new SerialPort(portName, 115200, Parity.None, 8, StopBits.One);
                myPort.Handshake = Handshake.None;
                if (measurementThread != null) { measurementThread.Abort(); }
                if (!myPort.IsOpen)
                {
                    try
                    {
                        myPort.Open();
                    }
                    catch (Exception ex)
                    {
                        ex.Data.Clear();
                        //if (myPort.IsOpen) myPort.Close();
                    }
                }
                if (TurnOn().Success)
                {
                    InstrumentIsOn = true;
                    result = GetSerialNumber();
                    if (result.Success)
                    {
                        string LSConnectedSN = result.AccessToken;
                        Regex regexObj = new Regex(@"[^\d]");
                        LSConnectedSN = regexObj.Replace(LSConnectedSN, "");
                        //Vérification que le SN du DNA connecté correspond au DNA choisi
                        if (LSConnectedSN == this.LS15._SerialNumber)
                        {
                            this.JustMeasure._InstrumentSN = this.LS15._SerialNumber;
                            result.Success = true;
                            result.AccessToken = "Connected to " + LS15._SerialNumber + " on port " + portName;
                            measurementThread = new Thread(ReadMeasurements);
                        }
                        else
                        {
                            //Fermeture connection si ce n'est pas le bon DNA
                            InstrumentIsOn = false;
                            myPort.Dispose();
                            myPort.Close();
                            result.Success = false;
                            result.ErrorMessage = "LS15 connected is SN " + LSConnectedSN + " on port " + portName + ", not the LS15 SN " + this.LS15._SerialNumber + ". Closing connection";
                            this.stationLevelModule?.CheckAndChangeInstrument(LSConnectedSN);
                        }
                    }
                }
                else
                {
                    myPort.Dispose();
                    myPort.Close();
                    result.Success = false;
                    result.ErrorMessage = "Cannot connect to LS15 SN " + this.LS15._SerialNumber + " on port " + portName;
                }
                return result;
            }
            catch (Exception e)
            {
                e.Data.Clear();
                result.Success = false;
                //InstrumentIsOn = false;
                result.ErrorMessage = R.T_CONNECTION_FAILED_ON_PORT + portName;
            }
            return result;
        }

        public override Result Disconnect()
        //déconnection du LS15
        {
            Result result = new Result();
            try
            {
                if (myPort.IsOpen == true)
                {
                    myPort.Close();
                    measurementThread.Abort();
                }
                InstrumentIsOn = false;
                result.Success = true;
                //   result.AccessToken = "Disconnection succeed";
                result.AccessToken = R.T_DISCONNECTION_SUCCEED;
            }
            catch (Exception e)
            {
                e.Data.Clear();
                result.Success = false;
                //  result.ErrorMessage = "Disconnection Failed";
                result.ErrorMessage = R.T_DISCONNECTION_FAILED;
            }

            return result;
        }

        public override Result TryConnectOthersPorts()
        {
            if (measurementThread != null) { measurementThread.Abort(); }
            Result result = new Result();
            if (InstrumentIsOn == false)
            {
                //In case the dna is connected after starting the module
                this.GetAllPorts();
                int j = this.listPortName.FindIndex(x => x == this.portName);
                if (j != -1) this.listPortIndex = j;
                else this.listPortIndex = 0;
                for (int i = 1; i < this.listPortName.Count; i++)
                {

                    if (InstrumentIsOn == false)
                    {
                        //change de port et essaye de se connecter
                        this.ChangePort();
                        myPort = new SerialPort(portName, 115200, Parity.None, 8, StopBits.One);
                        myPort.Handshake = Handshake.None;
                        if (!myPort.IsOpen)
                        {
                            try
                            {
                                myPort.Open();
                            }
                            catch (Exception ex)
                            {
                                ex.Data.Clear();
                                // if (myPort.IsOpen) myPort.Close();
                            }
                        }
                        if (TurnOn().Success)
                        {
                            InstrumentIsOn = true;
                            result = GetSerialNumber();
                            if (result.Success)
                            {
                                string LSConnectedSN = result.AccessToken;
                                Regex regexObj = new Regex(@"[^\d]");
                                LSConnectedSN = regexObj.Replace(LSConnectedSN, "");
                                //Vérification que le SN du DNA connecté correspond au LS15 choisi
                                if (LSConnectedSN == this.LS15._SerialNumber)
                                {
                                    this.JustMeasure._InstrumentSN = this.LS15._SerialNumber;
                                    result.Success = true;
                                    result.AccessToken = "Connected to " + LS15._SerialNumber + " on port " + portName;
                                    measurementThread = new Thread(ReadMeasurements);
                                }
                                else
                                {
                                    //Fermeture connection si ce n'est pas le bon DNA
                                    InstrumentIsOn = false;
                                    myPort.Dispose();
                                    myPort.Close();
                                    this.stationLevelModule.CheckAndChangeInstrument(LSConnectedSN);
                                    result.Success = false;
                                    //retour ligne si error message n'est pas vide
                                    if (result.ErrorMessage != "") { result.ErrorMessage += "\r\n" + DateTime.Now.ToString("yyyy-mm-dd, h:mm:ss tt") + ": "; }
                                    result.ErrorMessage += "LS15 connected is SN " + LSConnectedSN + " on port " + portName + ", not the LS15 SN " + this.LS15._SerialNumber + ". Closing connection!";
                                }
                            }
                        }
                        else
                        {
                            //n'est pas arrivé à se connecter sur le port choisi -> ferme ce port
                            myPort.Dispose();
                            myPort.Close();
                            result.Success = false;
                            //retour ligne si error message n'est pas vide
                            if (result.ErrorMessage != "") { result.ErrorMessage += "\r\n" + DateTime.Now.ToString("yyyy-mm-dd, h:mm:ss tt") + ": "; }
                            //   result.ErrorMessage += "Connection failed on port " + portName; 
                            result.ErrorMessage += R.T_CONNECTION_FAILED_ON_PORT + portName;
                            InstrumentIsOn = false;
                        }
                    }
                }
            }
            else
            {
                //Si le LS15 était déjà ON lors du lancement de la fonction, ne fait rien
                result.Success = false;
                result.ErrorMessage = "LS15 is already ON";
            }
            if (InstrumentIsOn == false)
            //N'a pas trouvé de DNA03 sur aucun des ports
            {
                result.Success = false;
                //retour ligne si error message n'est pas vide
                if (result.ErrorMessage != "") { result.ErrorMessage += "\r\n" + DateTime.Now.ToString("yyyy-mm-dd, h:mm:ss tt") + ": "; }
                result.ErrorMessage += "Cannot find LS15 SN " + this.LS15._SerialNumber + " on all COM ports";
            }
            return result;
        }

        #endregion

        private void ReadMeasurements()
        {
            while (true)
            {
                // Check if the thread should be paused
                pauseEvent.WaitOne();

                try
                {
                    string data = myPort.ReadLine();
                    TSU.Debug.WriteInConsole("CAPTURED DATA:\n" + data);
                    string[] gsidata = data.Split();
                    bool found = false;
                    

                    foreach (string meas in gsidata) //cant count the last index
                    {
                        // Debug.WriteInConsole(meas);
                        if (meas.StartsWith("33") && !meas.StartsWith("330"))
                        {
                            Debug.WriteInConsole("MEAN MEASUREMENTS FOUND");
                            found = true;
                            break;
                        }
                    }
                
                    if (found)
                    {
                        string[] newGsiData = new string[gsidata.Length - 1];
                        Array.Copy(gsidata, newGsiData, newGsiData.Length);

                        foreach (string meas in newGsiData)
                        {
                            string processedData = "";
                            if (!string.IsNullOrEmpty(meas)) 
                            { 
                                processedData = GSIConverter.ConvertGSI8ToNormalReading(meas, ref this.JustMeasure);
                            }
                            if (processedData.Length > 0)
                            {
                                Debug.WriteInConsole("Measurement received: " + processedData);
                                this.JustMeasure._Date = DateTime.Now;
                                OnNext();
                            }
                            
                        }
                        this.JustMeasure._Compass = double.Parse(GetCompassValue().AccessToken);
                        found = false;
                        TSU.Debug.WriteInConsole($"Compass: {this.JustMeasure._Compass} Distance: {this.JustMeasure._Distance} Height: {this.JustMeasure._RawLevelReading}");
                    }

                }
                catch (TimeoutException)
                {
                    ;
                }
                catch (Exception ex)
                {
                    Debug.WriteInConsole("Error: " + ex.Message);
                }
            }
        }

        public Result GetBearing()
        {
            throw new NotImplementedException();
        }

        public void Refresh()
        {
            throw new NotImplementedException();
        }

        public static double RadiansToGradians(double radians)
        {
            return radians * (200.0 / Math.PI);
        }
        public override Result GetCompassValue()
        {
            Result compass = GetCompass();
            if (compass.Success)
            {
                string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string filePath = Path.Combine(desktopPath, "compassValues.txt");
                string[] parts = compass.AccessToken.Split(',');
                string lastPart = parts[parts.Length - 1];

                // Parse the last part to a double
                double compassValue;
                if (double.TryParse(lastPart, out compassValue))
                {
                    compassValue = RadiansToGradians(compassValue);
                    compass.AccessToken = compassValue.ToString();
                }
            }
            return compass;
        }

        public override int GetIntBatteryLevel()
        {
            Result batteryLevelResult = GetBatteryLevel();
            string batteryReturn;

            // Check if the call was successful
            if (batteryLevelResult.Success) batteryReturn = batteryLevelResult.AccessToken;
            else batteryReturn = "Battery Level could not be read";

            string[] parts = batteryReturn.Split(',');

            if (parts.Length >= 5)
            {
                string valueString = parts[3];

                // Parse the value as an integer
                int.TryParse(valueString, out int value);
                batteryReturn = value.ToString();
                TSU.Debug.WriteInConsole("Battery Level: " + batteryReturn + "%");
                return (value/10)*10;
            }
            return -1;
        }

        public override Result DoSingleMeasure(MeasureOfLevel singleMeas)
        //fait une simple mesure et envoie la mesure
        {
            Result result = new Result();
            result = this.Measure();
            if (result.Success)
            {
                (double levelReading, double dist) = ConvertMeasurementResults(result);
                singleMeas._Date = DateTime.Now;
                double compass = double.Parse(GetCompassValue().AccessToken);
                if (dist != -9999)
                {
                    singleMeas._Distance = dist;
                }
                else
                {
                    this.LS15MeasureFailed();
                    result.Success = false;
                    //   result.AccessToken = "Cannot read distance";
                    result.AccessToken = R.T_CANNOT_READ_DISTANCE;
                }
                if (levelReading != -9999)
                {
                    singleMeas._RawLevelReading = levelReading;
                }
                else
                {
                    this.LS15MeasureFailed();
                    result.Success = false;
                    //   result.AccessToken = "Cannot read level reading";
                    result.AccessToken = R.T_CANNOT_READ_LEVEL_READING;
                }
                if (compass != -9999)
                {
                    singleMeas._Compass = compass;
                    TSU.Debug.WriteInConsole("Compass value retrieved and inserted");
                }
                else
                {
                    this.LS15MeasureFailed();
                    result.Success = false;
                    //   result.AccessToken = "Cannot read level reading";
                    result.AccessToken = "Cannot read level reading.";
                }
            }
            return result;
        }

        private void LS15MeasureFailed()
        //remplace la propriété mesure DNA par les valeurs 9999 pour indiquer que la mesure est ratée
        {
            MeasureFailed();
            this.JustMeasure._Compass = Tsunami2.Preferences.Values.na;
        }

        public class GSIConverter
        {
            public static string ConvertGSI8ToNormalReading(string gsi8Data, ref MeasureOfLevel measure)
            {
                bool validValue = false;
                if (gsi8Data.Length != 15)
                    throw new ArgumentException("Input string length should be 16 characters for GSI8 format.");

                // Extracting relevant parts from the GSI8 string
                string wordIndex = gsi8Data.Substring(0, 3);
                string measurementInfo = gsi8Data.Substring(4, 1);
                string unitInfo = gsi8Data.Substring(5, 1);
                string data = gsi8Data.Substring(6, 8);

                // Mapping unitInfo to corresponding units and number of decimal places
                int decimalPlaces;
                string unit;
                string typeOfData = GetWordIndexDescription(wordIndex);
                if (typeOfData != "Unknown")
                {
                    (decimalPlaces, unit) = GetUnitDecimal(unitInfo, ref validValue);

                    // Parsing data to double and formatting with the appropriate number of decimal places
                    double value = double.Parse(data) / Math.Pow(10, decimalPlaces);
                    string formattedValue = value.ToString($"F{decimalPlaces}");

                    // Constructing the normal reading string
                    if (typeOfData == "Height") measure._RawLevelReading = value;
                    else if (typeOfData == "Distance") measure._Distance = value;
                    string normalReading = typeOfData + ": " + formattedValue + unit;
                    return normalReading;
                }

                return "";
            }

            private static string GetWordIndexDescription(string wordIndex)
            {
                switch (wordIndex.Substring(0, 2))
                {
                    case "33":
                        return "Height";
                    case "32":
                        return "Distance";
                    default:
                        return "Unknown";
                }
            }

            private static (int, string) GetUnitDecimal(string unitInfo, ref bool validValue)
            {
                switch (unitInfo)
                {
                    case "0":
                    case "6":
                    case "8":
                        validValue = true;
                        return (4, "m");
                    case "1":
                    case "7":

                        validValue = true;
                        return (4, "ft");
                    default:
                        validValue = false;
                        return (0, "");
                }
            }
        }
    }
}
