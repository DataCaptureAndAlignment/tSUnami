﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using TSU.Common.Measures;
using TSU.Common.Instruments.Adapter;
using M = TSU.Common.Measures;
using TSU.ENUM;
using P = TSU.Views.Message.ProgressMessage;
using R = TSU.Properties.Resources;
using TSU.Tools;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TSU.Views;
using TSU.Views.Message;


namespace TSU.Common.Instruments.Device.TC2002
{
    [XmlType(TypeName = "TC2002.Module")]
    public class Module : PolarModule, IConnectable
    {
        [XmlIgnore]
        public new View View
        {
            get
            {
                return this._TsuView as View;
            }
            set
            {
                this._TsuView = value;
            }
        }

        public virtual Reflector.Reflector Reflector { get; set; }
        private List<string> _listPortName;
        private int _listPortIndex;
        private string _portName;
        private Dictionary<string, int> ports = new Dictionary<string, int>();
        private int _portHandle = -1;
        private bool isInitialised;
        private bool _portAvailable;

        public Module() : base() { }


        public Module(TSU.Module parentModule, Instruments.Instrument i)
            : base(parentModule)
        {
            this.Instrument = i;
            this._Name = i._Name;
        }
        public override void Initialize()
        {
            base.Initialize();
            _listPortName = GetAllPorts();
            _listPortIndex = 0;
            _portAvailable = false;
            isInitialised = false;
            if (_listPortName.Count > 0) _portAvailable = true;
            if (_portAvailable) Logs.Log.AddEntryAsResult(this, TryNextAvailablePorts());
            JustMeasuredData = new Polar.Measure();
            ToBeMeasuredData = new Polar.Measure();
            
            temperature = 20;// C
            pressure = 960;// hPa
            humidity = 60;// %
        }

        #region InstrumentPolarModule overrides

        public override FaceType GetFace()
        {
            float hz = 0, v=0;
            float d = 0;

            float a = -1;

            try
            {
                Action action = () => {

                    T3000_dll.PurgePort(_portHandle);
                    a = T3000_dll.GetAngles(_portHandle, ref hz, ref v); 
                };
                //TODO
                //unfortunately for some reason, if dont go back to the main thread then the t3000 method somethimes return a 'overflow arithmetic' or -1 with no understandable reasons.... 
                TSU.Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(action);
            }
            catch (Exception ex )
            {
                throw;
            }
            T3000_dll.PurgePort(_portHandle);
            if (a == 0) // no error
            {
                return (v < 200) ? FaceType.Face1 : FaceType.Face2;
            }
            else
            {
                T3000_dll.StopProcess(_portHandle);
                T3000_dll.PurgePort(_portHandle);
                throw new Exception($"{R.T_COULD_NOT_GET_INFORMATION_ABOUT_THE_FACE_POSITION}\r\nTRY AGAIN, sometimes, T3000/TC2002 does not react as tsunami expects it to do");
            }

        }
        public override void ChangeFace()
        {
            bool shouldChangeFace = true;

            int count = 5;

            while (shouldChangeFace)
            {
                string titleAndMessage = $"{R.T_NEXT_FACE};{R.T_PLEASE_TURN_THE_INSTRUMENT_TO_THE_OTHER_FACE_DO_YOUR_POINTING_THEN_CLICK_OK}";
                new MessageInput(MessageType.GoodNews, titleAndMessage).Show();
                shouldChangeFace = !(_BeingMeasuredTheodoliteData.Face == GetFace());
                count--;
                //  if (count < 1) throw new Exception("Face cannot be changed? I quit!");
                if (count < 1) throw new Exception(R.T_FACE_CANNOT_BE_CHANGED_I_QUIT);
            }
        }

        internal void ResetConnectionStuff()
        {
            foreach (var item in ports.Values)
            {
                try
                {
                    T3000_dll.ClosePort(item);
                }
                catch (Exception)
                {

                    throw;
                }
            }
            ports.Clear();
        }

        public override void MeasureLikeThis()
        {
            base.MeasureLikeThis();

            try
            {
                // Face and goto
                if (AnglesWanted)
                    ChangeFaceAndDoGoto();

                if (this.IsFirstMesaureOfFirstFace)
                {
                    if (this.CancelByMessageBeforeMeasure())
                    {
                        this.BeingMeasured = null;
                        throw new CancelException();
                    }
                }

                // Acquire
                float hz = 0, v = 0, d = 0;
                Result result = Acquire(ref hz, ref v, ref d);
                if (!result.Success) throw new Exception(R.T_COULD_NOT_ACQUIRE + result.ErrorMessage);

                // Convvert to TSU objects
                DoubleValue rawDistance;
                if (DistanceWanted)
                    rawDistance = new DoubleValue(d, double.NaN);
                else
                    rawDistance = new DoubleValue();

                TSU.Common.Elements.Angles rawAngles;
                if (AnglesWanted)
                {
                    rawAngles = new TSU.Common.Elements.Angles() { Horizontal = new DoubleValue(hz, double.NaN), Vertical = new DoubleValue(v, double.NaN) };
                    if (rawAngles.Horizontal.Value < 0) rawAngles.Horizontal.Value = rawAngles.Horizontal.Value + 400;
                    if (rawAngles.Horizontal.Value >= 400) rawAngles.Horizontal.Value = rawAngles.Horizontal.Value - 400;
                }
                else
                    rawAngles = new TSU.Common.Elements.Angles();

                if (beingMeasured._Status is M.States.Cancel)
                    throw new CancelException();

                // if (this._BeingMeasuredTheodoliteData.Face == FaceType.Face2) SurveyCompute.TransformToOppositeFace(rawAngles); only for at?

                Polar.Measure m = this._BeingMeasuredTheodoliteData;

                // Fill the measurementObject
                m._Date = DateTime.Now;
                m.Angles.Raw = rawAngles;
                m.Distance.Raw = rawDistance;
                m.Distance.WeatherConditions.dryTemperature = temperature;
                m.Distance.WeatherConditions.pressure = pressure;
                m.Distance.WeatherConditions.humidity = (int)humidity;

                this.DoMeasureCorrectionsThenNextMeasurementStep(m);


                int precisionDigitsA = (TSU.Tsunami2.Preferences.Values.GuiPrefs == null) ? 5 :TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;
                int precisionDigitsD = (TSU.Tsunami2.Preferences.Values.GuiPrefs == null) ? 5 :TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;

                string mess = string.Format("Hz={0}gon, Vt={1}gon D={2}m, ext={3}m",
                    m.Angles.Corrected.Horizontal.ToString(precisionDigitsA),
                    m.Angles.Corrected.Vertical.ToString(precisionDigitsA),
                    m.Distance.Corrected.ToString(precisionDigitsD),
                    m.Extension.ToString(precisionDigitsD)
                    );

                Logs.Log.AddEntryAsFinishOf(this, mess);
            }

            catch (CancelException)
            {
                denyMeasurement = true;
                this.StatusChange(string.Format(R.T_CANCEL), P.StepTypes.EndAll);
                this.DeclareMeasurementCancelled();
            }
            catch (Exception e)
            {
                this.JustMeasuredData = this.BeingMeasured;
                // this.LastMessage = "Measurement failed: " + e.Message;
                this.LastMessage = R.T_MEASUREMENT_FAILED + e.Message;
                this.StatusChange(string.Format(this.LastMessage), P.StepTypes.EndAll);
                this.HaveFailed();
                this.State = new InstrumentState(InstrumentState.type.Idle);
                throw new Exception(e.Message,e);
            }
        }

        private Result MeasureAnglesAndDistanceSeperatly(ref float hz, ref float v, ref float d)
        {
            hz = 100;
            v = 100;
            d = 100;
            return new Result() { Success = true, ErrorMessage = "random value", AccessToken = "Success" };
        }


        internal override void OnMeasureToDoReceived()
        {
            base.OnMeasureToDoReceived();
            Polar.Measure toBe = (ToBeMeasuredData as Polar.Measure);
            if (toBe == null)
            {
                Logs.Log.AddEntryAsPayAttentionOf(this, $"Nothing to Measure");
                return;
            }

            // Check if reflector defined;
            if (toBe.Distance.Reflector == null || toBe.Distance.Reflector._Name == R.String_Unknown)
            {
                if (this.Reflector != null)
                    //     Logs.Log.AddEntryAsPayAttentionOf(this, string.Format("{0} {1}" ,R.T_No_REFLECTOR, this.Reflector +" will be used"));
                    Logs.Log.AddEntryAsPayAttentionOf(this, $"{R.T_No_REFLECTOR} {this.Reflector} {R.T_WILL_BE_USED}");
                else
                    // Logs.Log.AddEntryAsPayAttentionOf(this, string.Format("{0} {1}", R.T_No_REFLECTOR , " and no reflector is selected in the instrument"));
                    Logs.Log.AddEntryAsPayAttentionOf(this, $"{R.T_No_REFLECTOR} {R.T_AND_NO_REFLECTOR_IS_SELECTED_IN_THE_INSTRUMENT}");
            }
            else
            {
                if (this.Reflector != toBe.Distance.Reflector)
                {
                    this.Reflector = toBe.Distance.Reflector;
                    this.View.UpdateReflector(Reflector._Name);
                }
            }

            // allow goto
            if ((ToBeMeasuredData as Polar.Measure).HasCorrectedAngles)
                this.AllowGoto();
        }

        public override bool IsReadyToMeasure()
        {
            if (!this.InstrumentIsOn) throw new Exception("Instrument is Off ?");
            if (ToBeMeasuredData == null) { this.WaitMeasurementDetails(); }
            if (this.ParentInstrumentManager == null) return true; // for test purpose
            if (this.ParentInstrumentManager.ParentModule is TSU.Polar.Station.Module)
                if ((ToBeMeasuredData as Polar.Measure).Distance.Reflector == null || (ToBeMeasuredData as Polar.Measure).Distance.Reflector._Name == R.String_Unknown) throw new Exception(R.T_No_REFLECTOR);

            return true;
        }

        private void AllowGoto()
        {
            //   Log.AddEntryAsFYI(this, string.Format("To point to '{0}', set the Hz to {1} gon and V to {2} gon",
            //       _ToBeMeasureTheodoliteData._OriginalPoint._Name,
            //       _ToBeMeasureTheodoliteData.Angles.Corrected.Horizontal.ToString(4),
            //       _ToBeMeasureTheodoliteData.Angles.Corrected.Vertical.ToString(4)));
            Logs.Log.AddEntryAsFYI(this, $"{R.T_TO_POINT_TO} '{_ToBeMeasureTheodoliteData._OriginalPoint._Name}', {R.T_SET_THE_HZ_TO} {_ToBeMeasureTheodoliteData.Angles.Corrected.Horizontal.ToString(4)} {R.T_GON_AND_V_TO} {_ToBeMeasureTheodoliteData.Angles.Corrected.Vertical.ToString(4)} {R.T_GON}");
        }

        internal void ShowAnglesOnInstrument()
        {
            if (!(T3000_dll.ShowDisplayHAndV(_portHandle) == 0)) throw new Exception("Could not show angles on instrument");
        }

        internal void ShowTemperatureOnInstrument()
        {
            if (this.InstrumentIsOn)
            if (!(T3000_dll.ShowDisplayTemp(_portHandle) == 0)) throw new Exception("Could not show temperature on instrument");
        }

        internal void ShowBatteryOnInstrument()
        {
            if (!(T3000_dll.ShowBattery(_portHandle) == 0)) throw new Exception("Could not show Battery on instrument");
        }

        #endregion

        public Result TryNextAvailablePorts()
        {
            Result result = new Result();
            if (_portAvailable)
            {
                _listPortIndex += 1;
                if (_listPortIndex >= _listPortName.Count) _listPortIndex = 0;
                _portName = GetAllPorts()[_listPortIndex];
                result.Success =true;
                result.AccessToken = "Port " + _portName + " is selected";
            }
            else
	        {
                result.Success =false;
                result.ErrorMessage = "No port Available" ;
	        }
            return result;
        }


        public override bool InstrumentIsOn { get => isConnected; set => isConnected = value; }
        bool isConnected;

        public override Result Connect()
        {
            Result result = new Result();
            if (_portAvailable)
            {
                OpenPortIfNotOpened();
                
                if (T3000_dll.TurnOn(_portHandle) == 0)
                {
                    T3000_dll.SetRS232Standard(_portHandle);
                    result.Success = true;
                    result.AccessToken = "The Instrument turned On (handle=" + _portHandle + ")";
                    isConnected = true;
                }
                else
                {
                    result.Success = false;
                    isConnected = false;
                    result.ErrorMessage = "Impossible to Connect";
                    ports.Remove(_portName.Substring(3, _portName.Length - 3));
                }
            }
            else
            {
                result.Success = false;
                isConnected = false;
                result.ErrorMessage = "No port Available";
            }
            return result;
        }
        public override Result Disconnect()
        {
            Result result = new Result();
            T3000_dll.StopAllProcess(_portHandle,1);

            T3000_dll.TurnOff(_portHandle);
            result.Success = true;
            result.AccessToken = "Disconnected ("+_portHandle+")";
            isConnected = false;
            return result;
        }
       
        public override Result InitializeSensor()
        {
            Result result = new Result();
            if (this.InstrumentIsOn)
            {
                int totalCount = this.Instrument._InstrumentType == InstrumentTypes.TC2002 ? 12 : 11;
                int succesCount = 0;
                string message = "";
                if (T3000_dll.SetGRE4(_portHandle) == 0) { message += "Set GR4, "; succesCount += 1; }
                if (T3000_dll.SetDistancePrgm0(_portHandle) == 0) { message += "Set Distance Prgm 0, "; succesCount += 1; }
                if (T3000_dll.TurnOnCompensator(_portHandle) == 0) { message += "Turn On Compensator, "; succesCount += 1; }
                if (T3000_dll.TurnOnCorrectionCalculator(_portHandle) == 0) { message += "Turn On Correction Calculator, "; succesCount += 1; }
                if (T3000_dll.SetAdditionConstantToZero(_portHandle) == 0) { message += "Set Addition Constant To Zero, "; succesCount += 1; }
                if (T3000_dll.SetPpmToZero(_portHandle) == 0) { message += "Set Ppm To Zero, "; succesCount += 1; }
                if (T3000_dll.SetUnitToGrad(_portHandle) == 0) { message += "Set Unit To Grad, "; succesCount += 1; }
                if (T3000_dll.SetDecimalDisplayTo4(_portHandle) == 0) { message += "Set Decimal Display To 4, "; succesCount += 1; }
                if (T3000_dll.SetDistanceUnityTo10Minus4(_portHandle) == 0) { message += "Set Distance Unity To 10e-4, "; succesCount += 1; }
                if (T3000_dll.ShowDisplayHAndV(_portHandle) == 0) { message += "Show Display H And V, "; succesCount += 1; }
                if (T3000_dll.SetFonctionnementContinu(_portHandle) == 0) { message += "Set Fonctionnement Continu, "; succesCount += 1; }
                if (this.Instrument._InstrumentType == InstrumentTypes.TC2002 && T3000_dll.SetReflectiveTargetMode(_portHandle) == 0) { message += "Set Reflective Target Mode, "; succesCount += 1; }

                if (succesCount == totalCount)
                {
                    isInitialised = true;
                    result.AccessToken = message + " Initialization completed";
                    result.Success = true;
                }
                else
                {
                    result.ErrorMessage = $"Partial Initialization : {succesCount} / {totalCount}";
                    result.Success = false;
                }
            }
            else
            {
                result.ErrorMessage = "Instrument is OFF";
                result.Success = false;
            }
            return result;
        }
        public Result StopAll()
        {
            Result result = new Result();
            if (this.InstrumentIsOn)
            {
                if (T3000_dll.StopAllProcess(_portHandle, 10) == 0)
                {

                    result.AccessToken = "Errors cleaned";
                    result.Success = true;
                    T3000_dll.PurgePort(_portHandle);
                }
                else
                {
                    result.ErrorMessage = "Impossible to clean";
                    result.Success = false;
                }
            }
            else
            {
                result.ErrorMessage = "Instrument is OFF";
                result.Success = false;
            }
            return result;

        }

        private void OpenPortIfNotOpened()
        {
            string portNumber = _portName.Substring(3, _portName.Length - 3);
            if (ports.ContainsKey(portNumber)) return;

            // Open port
            _portHandle = -1;

            System.IO.Ports.SerialPort _serialPort = new System.IO.Ports.SerialPort();
            _serialPort.PortName = _portName;
            _serialPort.Close();


            long r = T3000_dll.OpenPort(portNumber, 2400, 7, 2, 0, ref _portHandle);
            
            if (r != -1)
            {
                if (_portHandle != -1)
                {
                    if (!ports.ContainsKey(portNumber))
                        ports.Add(portNumber, _portHandle);
                    else
                        ports[portNumber] = _portHandle;
                }
                else
                {

                }
            }
        }

        private Result Acquire(ref float hz, ref float v, ref float d)
        {
            Result result = new Result();
            bool tempDistanceWanted = DistanceWanted;

            float a = -1;
            float dummy = 0;
            float tempH = 0, tempV = 0, tempD = 0;

            //T3000_dll.PurgePort(_portHandle);
            //T3000_dll.PurgePort(_portHandle);
            //float a = T3000_dll.GetAngles(_portHandle, ref hz, ref v);
            //T3000_dll.PurgePort(_portHandle);

            Action acquireAngle = () => { T3000_dll.PurgePort(_portHandle); a = T3000_dll.GetAngles(_portHandle, ref tempH, ref tempV); };
            Action actionDistance = () => { T3000_dll.PurgePort(_portHandle); a = T3000_dll.GetDistance(_portHandle, ref dummy, ref dummy, ref tempD); };


            if (!isInitialised) return new Result() { Success = false, ErrorMessage = "Not initialized!" };

            if (AnglesWanted)
            {
                // Angles
                TSU.Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(acquireAngle);
                if (a == -1) return new Result() { Success = false, ErrorMessage = "Impossible to measure the angle" };
            }

            if (DistanceWanted && PauseBetweenAnglesAndDistance)
            {
                // Pause
                var r = new MessageInput(MessageType.Choice, "Continue with distance?")
                {
                    ButtonTexts = new List<string>() { R.T_YES, $"No distance", R.T_CANCEL }
                }.Show();
                if (r.TextOfButtonClicked == R.T_CANCEL)
                    return new Result() { Success = false, ErrorMessage = "" };
                if (r.TextOfButtonClicked != R.T_YES)
                {
                    tempDistanceWanted = false;
                    DistanceWanted = false;
                }
            }

            if (tempDistanceWanted)
            {
                // Distance
                TSU.Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(actionDistance);
                if (a == -1) return new Result() { Success = false, ErrorMessage = "Impossible to acquire the distance" };
            }

            // Result
            string tempAccesToken = "";

            if (AnglesWanted)
            {
                hz = tempH;
                v = tempV;
                tempAccesToken += string.Format("Hz = {0} gon, V = {1} gon", hz, v);

                if (tempDistanceWanted)
                    tempAccesToken += ", ";
            }

            if (tempDistanceWanted)
            {
                d = tempD;
                tempAccesToken += string.Format("D = {0} meters", d);
            }
            else
            {
                d = (float)TSU.Tsunami2.Preferences.Values.na;
            }

            result.AccessToken = tempAccesToken;
            result.Success = true;
            return result;
        }

        public List<string> GetAllPorts()
        {
            List<String> allPorts = new List<String>();
            foreach (String portName in System.IO.Ports.SerialPort.GetPortNames())
            {
                allPorts.Add(portName);
            }
            return allPorts;
        }

        void IConnectable.Refresh()
        {
            throw new NotImplementedException();
        }
    }
}
