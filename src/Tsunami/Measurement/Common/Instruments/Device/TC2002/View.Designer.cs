﻿
using R = TSU.Properties.Resources;


namespace TSU.Common.Instruments.Device.TC2002
{
    partial class View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (this.Module == null)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        internal void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonCom = new System.Windows.Forms.Button();
            this.buttonOnOff = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonInit = new System.Windows.Forms.Button();
            this.buttonCe = new System.Windows.Forms.Button();
            this.buttonDist = new System.Windows.Forms.Button();
            this.buttonHzV = new System.Windows.Forms.Button();
            this.buttonAll = new System.Windows.Forms.Button();
            this.panelLog = new System.Windows.Forms.Panel();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelMeteo = new System.Windows.Forms.Label();
            this.pictureOnOff = new System.Windows.Forms.PictureBox();
            this.checkBoxPauseBetweenAngleAndDistance = new System.Windows.Forms.CheckBox();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureOnOff)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.DimGray;
            this.panel1.Controls.Add(this.buttonCom);
            this.panel1.Controls.Add(this.buttonOnOff);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.panelLog);
            this.panel1.Location = new System.Drawing.Point(8, 31);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(555, 218);
            this.panel1.TabIndex = 0;
            // 
            // buttonCom
            // 
            this.buttonCom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCom.Image = global::TSU.Properties.Resources.ButtonPictureTC2002_COM;
            this.buttonCom.Location = new System.Drawing.Point(441, 156);
            this.buttonCom.Name = "buttonCom";
            this.buttonCom.Size = new System.Drawing.Size(50, 50);
            this.buttonCom.TabIndex = 6;
            this.toolTip2.SetToolTip(this.buttonCom, "Switch COM port");
            this.buttonCom.UseVisualStyleBackColor = true;
            this.buttonCom.Click += new System.EventHandler(this.buttonCom_Click);
            // 
            // buttonOnOff
            // 
            this.buttonOnOff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOnOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOnOff.Image = global::TSU.Properties.Resources.ButtonPictureTC2002_OnOFF;
            this.buttonOnOff.Location = new System.Drawing.Point(497, 156);
            this.buttonOnOff.Name = "buttonOnOff";
            this.buttonOnOff.Size = new System.Drawing.Size(50, 50);
            this.buttonOnOff.TabIndex = 7;
            this.toolTip2.SetToolTip(this.buttonOnOff, "Turn ON and OFF the instrument");
            this.buttonOnOff.UseVisualStyleBackColor = true;
            this.buttonOnOff.MouseDown += new System.Windows.Forms.MouseEventHandler(this.buttonOnOff_MouseDown);
            this.buttonOnOff.MouseUp += new System.Windows.Forms.MouseEventHandler(this.buttonOnOff_MouseUp);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.checkBoxPauseBetweenAngleAndDistance);
            this.panel2.Controls.Add(this.buttonInit);
            this.panel2.Controls.Add(this.buttonCe);
            this.panel2.Controls.Add(this.buttonDist);
            this.panel2.Controls.Add(this.buttonHzV);
            this.panel2.Controls.Add(this.buttonAll);
            this.panel2.Location = new System.Drawing.Point(8, 153);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(422, 57);
            this.panel2.TabIndex = 8;
            // 
            // buttonInit
            // 
            this.buttonInit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonInit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInit.Image = global::TSU.Properties.Resources.ButtonPictureTC2002_INIT;
            this.buttonInit.Location = new System.Drawing.Point(357, 3);
            this.buttonInit.Name = "buttonInit";
            this.buttonInit.Size = new System.Drawing.Size(50, 50);
            this.buttonInit.TabIndex = 10;
            this.toolTip2.SetToolTip(this.buttonInit, "Initialise the instrument");
            this.buttonInit.UseVisualStyleBackColor = true;
            this.buttonInit.Click += new System.EventHandler(this.toolStripButtonInit_Click);
            // 
            // buttonCe
            // 
            this.buttonCe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCe.Image = global::TSU.Properties.Resources.ButtonPictureTC2002_Stop;
            this.buttonCe.Location = new System.Drawing.Point(287, 3);
            this.buttonCe.Name = "buttonCe";
            this.buttonCe.Size = new System.Drawing.Size(50, 50);
            this.buttonCe.TabIndex = 9;
            this.toolTip2.SetToolTip(this.buttonCe, "Cancel all errors");
            this.buttonCe.UseVisualStyleBackColor = true;
            this.buttonCe.Click += new System.EventHandler(this.ButtonStop_Click);
            // 
            // buttonDist
            // 
            this.buttonDist.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDist.Image = global::TSU.Properties.Resources.ButtonPictureTC2002_DIST;
            this.buttonDist.Location = new System.Drawing.Point(181, 4);
            this.buttonDist.Name = "buttonDist";
            this.buttonDist.Size = new System.Drawing.Size(50, 50);
            this.buttonDist.TabIndex = 8;
            this.toolTip2.SetToolTip(this.buttonDist, "acquire distance only");
            this.buttonDist.UseVisualStyleBackColor = true;
            this.buttonDist.Click += new System.EventHandler(this.ButtonDist_Click);
            // 
            // buttonHzV
            // 
            this.buttonHzV.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonHzV.Image = global::TSU.Properties.Resources.ButtonPictureTC2002_Hz;
            this.buttonHzV.Location = new System.Drawing.Point(120, 4);
            this.buttonHzV.Name = "buttonHzV";
            this.buttonHzV.Size = new System.Drawing.Size(50, 50);
            this.buttonHzV.TabIndex = 7;
            this.toolTip2.SetToolTip(this.buttonHzV, "Acquire angles only");
            this.buttonHzV.UseVisualStyleBackColor = true;
            this.buttonHzV.Click += new System.EventHandler(this.ButtonHzV_Click);
            // 
            // buttonAll
            // 
            this.buttonAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAll.Image = global::TSU.Properties.Resources.ButtonPictureTC2002_ALL;
            this.buttonAll.Location = new System.Drawing.Point(3, 3);
            this.buttonAll.Name = "buttonAll";
            this.buttonAll.Size = new System.Drawing.Size(50, 50);
            this.buttonAll.TabIndex = 6;
            this.toolTip2.SetToolTip(this.buttonAll, "Acquire Angles and Distance");
            this.buttonAll.UseVisualStyleBackColor = true;
            this.buttonAll.Click += new System.EventHandler(this.ButtonAll_Click);
            // 
            // panelLog
            // 
            this.panelLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelLog.BackColor = System.Drawing.Color.Maroon;
            this.panelLog.Location = new System.Drawing.Point(4, 8);
            this.panelLog.Name = "panelLog";
            this.panelLog.Size = new System.Drawing.Size(545, 142);
            this.panelLog.TabIndex = 7;
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.BackColor = System.Drawing.Color.Transparent;
            this.labelTitle.Font = new System.Drawing.Font("Impact", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.ForeColor = System.Drawing.Color.White;
            this.labelTitle.Location = new System.Drawing.Point(8, 5);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(111, 23);
            this.labelTitle.TabIndex = 1;
            this.labelTitle.Text = "WILD TC2002";
            // 
            // labelMeteo
            // 
            this.labelMeteo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMeteo.AutoSize = true;
            this.labelMeteo.ForeColor = System.Drawing.Color.Red;
            this.labelMeteo.Location = new System.Drawing.Point(439, 13);
            this.labelMeteo.Name = "labelMeteo";
            this.labelMeteo.Size = new System.Drawing.Size(124, 13);
            this.labelMeteo.TabIndex = 2;
            this.labelMeteo.Text = "T 20°C P 960hPa H 60%";
            this.labelMeteo.Click += new System.EventHandler(this.labelMeteo_Click);
            // 
            // pictureOnOff
            // 
            this.pictureOnOff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureOnOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureOnOff.Image = global::TSU.Properties.Resources.Tda5005_OFF;
            this.pictureOnOff.Location = new System.Drawing.Point(145, 1);
            this.pictureOnOff.Name = "pictureOnOff";
            this.pictureOnOff.Size = new System.Drawing.Size(26, 27);
            this.pictureOnOff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureOnOff.TabIndex = 38;
            this.pictureOnOff.TabStop = false;
            // 
            // checkBoxPauseBetweenAngleAndDistance
            // 
            this.checkBoxPauseBetweenAngleAndDistance.AutoSize = true;
            this.checkBoxPauseBetweenAngleAndDistance.Location = new System.Drawing.Point(59, 22);
            this.checkBoxPauseBetweenAngleAndDistance.Name = "checkBoxPauseBetweenAngleAndDistance";
            this.checkBoxPauseBetweenAngleAndDistance.Size = new System.Drawing.Size(56, 17);
            this.checkBoxPauseBetweenAngleAndDistance.TabIndex = 11;
            this.checkBoxPauseBetweenAngleAndDistance.Text = "Pause";
            this.toolTip2.SetToolTip(this.checkBoxPauseBetweenAngleAndDistance, "Pause between the angles and distance acquisition");
            this.checkBoxPauseBetweenAngleAndDistance.UseVisualStyleBackColor = true;
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkKhaki;
            this.ClientSize = new System.Drawing.Size(571, 257);
            this.Controls.Add(this.pictureOnOff);
            this.Controls.Add(this.labelMeteo);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(255, 175);
            this.Name = "View";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.TC2002Form_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureOnOff)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Panel panelLog;
        private System.Windows.Forms.Label labelMeteo;
        private System.Windows.Forms.Button buttonOnOff;
        private System.Windows.Forms.Button buttonCom;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonInit;
        private System.Windows.Forms.Button buttonCe;
        private System.Windows.Forms.Button buttonDist;
        private System.Windows.Forms.Button buttonHzV;
        private System.Windows.Forms.Button buttonAll;
        private System.Windows.Forms.PictureBox pictureOnOff;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.CheckBox checkBoxPauseBetweenAngleAndDistance;
    }
}

