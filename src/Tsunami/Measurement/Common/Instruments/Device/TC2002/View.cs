﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSU.ENUM;
using TSU.Preferences;
using TSU.Common.Instruments.Reflector;
using TSU.Views.Message;


namespace TSU.Common.Instruments.Device.TC2002
{
    public partial class View : Device.View
    {
        private Result result;
        private new Module Module
        {
            get
            {
                return this._Module as Module;
            }
        }
        private Instrument Tc2002
        {
            get
            {
                return this.Module.Instrument as Instrument;
            }
        }
        private Reflectors reflectors;

        public View(Instruments.Module instrumentModule)
            : base(instrumentModule)
        {
            InitializeComponent();
            this.ApplyThemeColors();
            SetAllToolTips();
            InitializeLog();
        }

        private void InitializeLog()
        {
            // 
            // log
            // 
            LogView = new Logs.LogView();
            Preferences.GUI.Theme.ThemeColors colors = Tsunami2.Preferences.Theme.Colors;
            LogView.SetColors(beginningOf: colors.LightForeground,
                             logBack: colors.DarkBackground,
                             dateAndTime: colors.LightForeground,
                             error: colors.Bad,
                             fYI: colors.LightForeground,
                             payAttentionTo: colors.Attention,
                             success: colors.Good,
                             finishOf: colors.LightForeground);
            // The version before was setting colors twice. Which one is the good one ?
            //logBox.SetColors(logBack: colors.Background,
            //                 beginningOf: colors.TextOnDarkBackGround);
            LogView.ShowDockedFill();
            panelLog.Controls.Add(LogView);
        }

        private void ApplyThemeColors()
        {

        }

        private System.Windows.Forms.ToolTip toolTip1;
        internal override void SetAllToolTips()
        {
             var toolTip1 = this.ToolTip;
            if (toolTip1 != null)
            {
                toolTip1.SetToolTip(this.buttonCom, R.T_CHANGE_COM_PORT);
                toolTip1.SetToolTip(this.buttonCe, R.T_RESET_ERRORS);
                toolTip1.SetToolTip(this.buttonOnOff, R.T_B_TURN_ON_OFF);
                toolTip1.SetToolTip(this.buttonDist, R.StringTS60_ToolTip_Dist);
                toolTip1.SetToolTip(this.buttonHzV, R.StringTS60_ToolTip_Rec);
                toolTip1.SetToolTip(this.buttonAll, R.StringTS60_ToolTip_All);
                toolTip1.SetToolTip(this.labelMeteo, R.StringTS60_ToolTip_Weather);
                toolTip1.SetToolTip(this.pictureOnOff, R.StringTS60_ToolTip_ONOFF);
            }
        }

        private void ButtonReflector_Click(object sender, EventArgs e)
        {
            string name = ((ToolStripMenuItem)sender).Text;
            this.Module.Reflector = reflectors.GetReflectorByTypeNamePlusSerialNumber(name);
            UpdateReflector(name);
        }

        public void UpdateReflector(string name)
        {
            this.Module._ToBeMeasureTheodoliteData.Distance.Reflector = this.Module.Reflector;
            //   Logs.Log.AddEntryAsFinishOf(this.Module, string.Format("{0} Will be used.", this.Module.Reflector._Name));
            Logs.Log.AddEntryAsFinishOf(this.Module, $"{this.Module.Reflector._Name} {R.T_WILL_BE_USED}.");
        }
        internal void buttonCom_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("devmgmt.msc");
            result = Module.TryNextAvailablePorts();
            Logs.Log.AddEntryAsResult(this._Module, result);
        }

        private bool buttonIsDown=false;

        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();

        private void buttonOnOff_MouseDown(object sender, MouseEventArgs e)
        {
            buttonIsDown = true;
            sw.Reset();
            sw.Start();
        }

        internal void buttonOnOff_MouseUp(object sender, MouseEventArgs e)
        {
            buttonIsDown = false;
            sw.Stop();
            if (sw.Elapsed.Seconds > 1)
            {
                this.Module.ResetConnectionStuff();
            }
            else
            {
                Result r = null;

                TsuTask t = new TsuTask() { ActionName = "Connection", estimatedTimeInMilliSec = 10000, timeOutInSec = 7 };
                if (Module.InstrumentIsOn)
                {
                    t.mainAction = delegate
                    {
                        r = Module.Disconnect();
                        Logs.Log.AddEntryAsResult(this._Module, r);
                    };
                    t.postAction = delegate
                    {
                        Logs.Log.AddEntryAsResult(this._Module, r);
                        if (r.Success) ShowAsDisconnected();
                        else this.Module.Disconnect();
                    };
                }
                else
                {
                    t.mainAction = delegate
                    {
                        r = Module.Connect();
                    };
                    t.postAction = delegate
                    {
                        Logs.Log.AddEntryAsResult(this._Module, r);
                        if (r.Success)
                            ShowAsConnected();
                        else
                            this.Module.Disconnect();
                    };
                }
                RunTasksWithWaitingMessage(new List<TsuTask>() { t, CreateInitializationTask() }, "Connection");
            }

        }
        

        private void ShowAsConnected()
        {
            this.pictureOnOff.Image = R.Tda5005_ON;
        }

        private void ShowAsDisconnected()
        {
            this.pictureOnOff.Image = R.Tda5005_OFF;
        }


        internal void ButtonHzV_Click(object sender, EventArgs e)
        {
            try
            {
                this.Module.AnglesWanted = true;
                this.Module.DistanceWanted = false;
                result = Module.Measure();
                if (result.Success)
                {
                    this.Module.ReportMeasurement();
                    this.Module.FinishMeasurement();
                }

                this.Module.BeingMeasured = null;
                Logs.Log.AddEntryAsResult(this._Module, result);
            }
            catch (Exception ex)
            {
                string titleAndMessage = "Problem;" + ex.Message;
                new MessageInput(MessageType.Critical, titleAndMessage).Show();
            }
        }

        internal void ButtonDist_Click(object sender, EventArgs e)
        {
            try
            {
                this.Module.AnglesWanted = false;
                this.Module.PauseBetweenAnglesAndDistance = false;
                this.Module.DistanceWanted = true;
                result = Module.Measure();
                if (result.Success)
                {
                    this.Module.ReportMeasurement();
                    this.Module.FinishMeasurement();
                }

                this.Module.BeingMeasured = null;
                Logs.Log.AddEntryAsResult(this._Module, result);
            }
            catch (Exception ex)
            {
                string titleAndMessage = "Problem;" + ex.Message;
                new MessageInput(MessageType.Critical, titleAndMessage).Show();
            }
        }

        public override void MeasureAsync()
        {
            base.MeasureAsync(); // do nothing
            TryAction(LaunchMeasurement);
        }

        private void LaunchMeasurement()
        {
            Result r = new Result();
            if (this.Module._ToBeMeasureTheodoliteData == null)
            {
                this.Module.WaitMeasurementDetails();
            }

            int factor = this.Module._ToBeMeasureTheodoliteData.NumberOfMeasureToAverage;
            if (Module._ToBeMeasureTheodoliteData.Face == FaceType.DoubleFace) factor *= 2;


            TsuTask task = new TsuTask()
            {
                ActionName = "Measuring...",

                estimatedTimeInMilliSec = 7000 * factor,
                timeOutInSec = 1000,// it is involving human reaction so , it is better nt to use timeout 20 * factor,
                preAction = () =>
                {
                },
                mainAction = () =>
                {
                    result = this.Module.Measure();
                },
                cancelAction = () =>
                {
                    this.Module.CancelMeasureInProgress();
                },
                postAction = () =>
                {
                    Logs.Log.AddEntryAsResult(this._Module, result);
                    if (result.Success)
                    {
                        this.Module.ReportMeasurement();
                        this.Module.FinishMeasurement();
                    }
                    this.Module.BeingMeasured = null;
                }
            };

            RunTasksWithWaitingMessage(new List<TsuTask>() { task }, "Measure");
        }

        internal void ButtonAll_Click(object sender, EventArgs e)
        {
            this.Module.AnglesWanted = true;
            this.Module.PauseBetweenAnglesAndDistance = true;
            this.Module.DistanceWanted = true;
            TryAction(LaunchMeasurement);
        }

        private void ProposeInitialisation()
        {
            this.Module.AnglesWanted = true;
            this.Module.DistanceWanted = true;
            string titleAndMessage = $"{R.T_DO_YOU_WANT_TO_INITIALIZE};{R.T_THIS_WILL_TURN_ON_COMPENSATOR_SET_UNITS_ETC}";
            MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
            {
                ButtonTexts = new List<string> { R.T_INIT, R.T_NO }
            };
            if (mi.Show().TextOfButtonClicked == R.T_INIT)
            {
                toolStripButtonInit_Click(this,  new EventArgs());
            };
        }

        internal void toolStripButtonInit_Click(object sender, EventArgs e)
        {
            RunTasksWithWaitingMessage(new List<TsuTask>() { CreateInitializationTask() }, "Initialisation");
        }

        private TsuTask CreateInitializationTask()
        {
            Result r = new Result();
            return new TsuTask()
            {
                ActionName = "Initialising...",

                estimatedTimeInMilliSec = 3000,
                timeOutInSec = 8,
                preAction = () =>
                {
                },
                mainAction = () =>
                {
                    r = Module.InitializeSensor();
                },
                postAction = () =>
                {
                    Logs.Log.AddEntryAsResult(this._Module, r);
                }
            };
        }

        internal void ButtonStop_Click(object sender, EventArgs e)
        {

            result = Module.StopAll();
            Logs.Log.AddEntryAsResult(this._Module, result);
        }

        private void TC2002Form_Load(object sender, EventArgs e)
        {

        }

        //private void toolStripDropDownButton1_Click(object sender, EventArgs e)
        //{
        //    if (reflectors == null)
        //    {
        //        reflectors = new Reflectors(this.Module.Instrument as TotalStation);
        //        foreach (Reflector item in reflectors.ReflectorList)
        //        {
        //            ToolStripMenuItem button = new ToolStripMenuItem();
        //            button.Text = item._Name;
        //            button.Click += new System.EventHandler(this.ButtonReflector_Click);
        //        }
        //        this.Module.Reflector = reflectors.ReflectorList[0];
        //    }

        //}

        internal void labelMeteo_Click(object sender, EventArgs e)
        {
            this.Module.ShowTemperatureOnInstrument();
            this.Module.SetMeteoManually();
            this.Module.StopAll();
            labelMeteo.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.Good;
            labelMeteo.Text = string.Format("T {0}°C, P {1}hPa, H {2}%", this.Module.temperature, this.Module.pressure, this.Module.humidity);
        }

        
    }
}
