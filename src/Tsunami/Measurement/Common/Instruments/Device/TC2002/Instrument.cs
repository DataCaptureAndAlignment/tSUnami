﻿

using System.Xml.Serialization;

namespace TSU.Common.Instruments.Device.TC2002
{
    [XmlType(TypeName = "TC2002.Instrument")]
    public class Instrument : TotalStation
    {
        public Instrument()
        {
        }
    }
}
