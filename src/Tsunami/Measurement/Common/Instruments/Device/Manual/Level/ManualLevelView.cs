﻿using System;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using TSU.Views;
using T = TSU.Tools;


namespace TSU.Common.Instruments.Device.Manual.Level
{
    public partial class View : Device.View
    {
        private Instrument _ManualLevel
        {
            get
            {
                return (this._Module as Instruments.Module).Instrument as Instrument;
            }
        }
        char decimalSeparator;
        private double previousValue;

        public View(Instruments.Module guiModule)
            : base(guiModule)
        //crèe la vue du manuallevel
        {
            //level._View = this;
            InitializeComponent();
            this.ApplyThemeColors();
            startingHeight = this.Height;
            startingWidth = this.Width;
            decimalSeparator = Convert.ToChar(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
        }
        private void ApplyThemeColors()
        {
            this.button1.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.button1.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button2.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.button2.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button3.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.button3.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button4.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.button4.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button5.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.button5.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button6.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.button6.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button7.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.button7.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button8.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.button8.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button9.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.button9.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button10.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.button10.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button11.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.button11.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button12.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.button12.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button13.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.button13.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.textBox1.BackColor = Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.textBox1.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button14.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.button14.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button15.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            this.button15.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
        }
        private void textBox1_Enter(object sender, EventArgs e)
        //lorsqu'on entre dans la fenêtre textBox1, sélectionne tout
        {
            textBox1.SelectAll();
        }
        private void button_Click(object sender, EventArgs e)
        //événement lorsqu'on clique sur un bouton chiffre
        {
            Add((sender as Button).Text);
        }
        private void Add(string text)
        //fusionne la valeur dans le textbox avec la valeur du bouton appuyé
        {
            try
            {
                if (textBox1.SelectionLength == 0)
                {
                    //permet de garder les zéros après la virgule
                    textBox1.Text = textBox1.Text + text;
                    string zeros = "";
                    if (textBox1.Text.Contains(decimalSeparator))
                    {
                        for (int i = textBox1.TextLength - 1; i >= 0; i--)
                        {
                            if (textBox1.Text[i].ToString() == "0")
                            {
                                zeros = zeros + "0";
                            }
                            else
                            {
                                //ajoute le decimal séparator si 0 juste après la virgule
                                if (textBox1.Text[i] == decimalSeparator)
                                {
                                    zeros = decimalSeparator.ToString() + zeros;
                                }
                                i = 0;
                            }
                        }
                    }
                    textBox1.Text = T.Conversions.Numbers.ToDouble(textBox1.Text).ToString() + zeros;
                }
                else
                {
                    //met dans temp la valeur sélectionnée et y ajoute la valeur du bouton appuyé
                    string temp = textBox1.Text.Remove(textBox1.SelectionStart, textBox1.SelectionLength);
                    temp = temp.Insert(textBox1.SelectionStart, text);
                    textBox1.Text = temp;
                }
            }
            catch (Exception)
            {

            }
        }
        private void delete_Click(object sender, EventArgs e)
        //événement lorsqu'on clique sur le bouton delete
        {
            if (textBox1.SelectionLength == 0)
            //enlève le dernier caractère
            {
                if (textBox1.TextLength > 0)
                {
                    //if (textBox1.SelectionStart < 1) textBox1.SelectionStart = textBox1.TextLength;
                    //    string temp = textBox1.Text.Remove(textBox1.SelectionStart - 1, 1);
                    //textBox1.Text = temp;
                    textBox1.Text = textBox1.Text.Remove(textBox1.TextLength - 1, 1);
                }
            }
            else
            //efface toute la sélection
            {
                textBox1.Text = textBox1.Text.Replace(textBox1.Text.Substring(textBox1.SelectionStart, textBox1.SelectionLength), "");
            }
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        //événement lorsqu'on change le texte dans le textBox1
        {
            try
            {
                if (textBox1.Text != "")
                {
                    if (textBox1.Text[textBox1.Text.Length - 1] != decimalSeparator)
                        previousValue = T.Conversions.Numbers.ToDouble(textBox1.Text);
                }
                else
                {
                    previousValue = 0;
                    textBox1.Text = previousValue.ToString();
                }
            }
            catch (Exception)
            {
                textBox1.Text = previousValue.ToString();
                textBox1.SelectionStart = textBox1.Text.Length;
            }


        }
        private void Separator_Click(object sender, EventArgs e)
        //Evénement lorsqu'on clique sur le bouton des décimales
        {
            if (textBox1.SelectionLength == 0)
            {
                if (!textBox1.Text.Contains(decimalSeparator))
                    textBox1.Text += decimalSeparator;
            }
            else
            {
                string temp = textBox1.Text.Remove(textBox1.SelectionStart, textBox1.SelectionLength);
                temp = temp.Insert(textBox1.SelectionStart, decimalSeparator.ToString());
                textBox1.Text = temp;
            }
        }
        private void Validate_Click(object sender, EventArgs e)
        //validation mesure et envoi de la valeur à l'observateur
        {
            //this.ShowMessageOfValidation("nice", Convert.ToDouble(textBox1.Text).ToString(), R.T_OK);
            double numberEntered = T.Conversions.Numbers.ToDouble(textBox1.Text, true, -9999);
            if (numberEntered != -9999)
            {
                //this._ManualLevel.OnNext(numberEntered/100000);
                Measures.MeasureOfLevel m = new Measures.MeasureOfLevel();
                m._Date = DateTime.Now;
                m._InstrumentSN = this._ManualLevel._SerialNumber;
                m._RawLevelReading = numberEntered / 100000;
                SendMessage(m);
            }
            textBox1.Text = "";
        }
        private void ChangeSigne_Click(object sender, EventArgs e)
        //changement du signe
        {
            previousValue = -previousValue;
            textBox1.Text = previousValue.ToString();
        }
        private void Clear_Click(object sender, EventArgs e)
        //Effacement du contenu du textbox
        {
            previousValue = 0;
            textBox1.Text = previousValue.ToString();
        }

        internal void ChangeScale(float factorScale)
        // change l'echelle d'affichage de la fenêtre
        {
            if (factorScale != 0)
            {
                this.button1.Font = new Font("Impact", 36F * factorScale, FontStyle.Regular, GraphicsUnit.Point, 0);
                this.button2.Font = new Font("Impact", 36F * factorScale, FontStyle.Regular, GraphicsUnit.Point, 0);
                this.button3.Font = new Font("Impact", 36F * factorScale, FontStyle.Regular, GraphicsUnit.Point, 0);
                this.button4.Font = new Font("Impact", 36F * factorScale, FontStyle.Regular, GraphicsUnit.Point, 0);
                this.button5.Font = new Font("Impact", 36F * factorScale, FontStyle.Regular, GraphicsUnit.Point, 0);
                this.button6.Font = new Font("Impact", 36F * factorScale, FontStyle.Regular, GraphicsUnit.Point, 0);
                this.button7.Font = new Font("Impact", 36F * factorScale, FontStyle.Regular, GraphicsUnit.Point, 0);
                this.button8.Font = new Font("Impact", 36F * factorScale, FontStyle.Regular, GraphicsUnit.Point, 0);
                this.button9.Font = new Font("Impact", 36F * factorScale, FontStyle.Regular, GraphicsUnit.Point, 0);
                this.button10.Font = new Font("Impact", 36F * factorScale, FontStyle.Regular, GraphicsUnit.Point, 0);
                this.button11.Font = new Font("Impact", 36F * factorScale, FontStyle.Regular, GraphicsUnit.Point, 0);
                this.button12.Font = new Font("Impact", 18F * factorScale, FontStyle.Regular, GraphicsUnit.Point, 0);
                this.button13.Font = new Font("Impact", 24F * factorScale, FontStyle.Regular, GraphicsUnit.Point, 0);
                this.button14.Font = new Font("Impact", 18F * factorScale, FontStyle.Regular, GraphicsUnit.Point, 0);
                this.button15.Font = new Font("Impact", 18F * factorScale, FontStyle.Regular, GraphicsUnit.Point, 0);
                this.textBox1.Font = new Font("Impact", 36F * factorScale, FontStyle.Regular, GraphicsUnit.Point, 0);
            }
        }
        /// <summary>
        /// n'autorise plus de déplacer le form, mais cause un clignotement de la fenêtre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ManualTiltMeterView_LocationChanged(object sender, EventArgs e)
        {
            this.LocationChanged -= this.ManualTiltMeterView_LocationChanged;
            Location = new Point(0, 0);
            this.LocationChanged += new EventHandler(this.ManualTiltMeterView_LocationChanged);
        }
    }
}
