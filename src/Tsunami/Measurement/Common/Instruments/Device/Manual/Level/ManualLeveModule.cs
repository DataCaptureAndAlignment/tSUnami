﻿using System;
using M = TSU;

using System.Xml.Serialization;

namespace TSU.Common.Instruments.Device.Manual.Level
{

    [Serializable]
    [XmlType(TypeName = "Manual.Level.Module")]
    public class Module : Instruments.Module
    {
        public Module()
        {
            this.Initialize();
        }
        public Module(M.Module parentModule, Instruments.Instrument i)
            : base(parentModule)
        {
            this.Instrument = i;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        
    }
}
