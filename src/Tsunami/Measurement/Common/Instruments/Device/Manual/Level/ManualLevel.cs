﻿using System;
using System.Xml.Serialization;

namespace TSU.Common.Instruments.Device.Manual.Level
{
    [Serializable]
    [XmlType(TypeName = "Manual.Level.Instrument")]
    public class Instrument : Instruments.Level
    {
        public Instrument()
        {
        }
        public Instrument(LevelType levelType)
        {
            this._levelType = levelType;
        }
    }
}
