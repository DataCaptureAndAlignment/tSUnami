﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using T = TSU.Tools;


namespace TSU.Common.Instruments.Device.Manual
{
    public partial class View : Device.View
    {
        internal int numberOfRows = 4;
        internal int numberOfColumns = 5;
        internal int spaceSize = 10;
        internal TSU.Common.Instruments.Device.Manual.Theodolite.Instrument _TheodoliteManuel;
        internal char decimalSeparator;
        internal Control[,] array;
        internal DoubleValue doubleValue; 

        public View()
        {
            InitializeComponent();
            Init();
        }

        public View(DoubleValue d)
        {
            doubleValue = d;
            InitializeComponent();
            Init();
        }

        public View(Module guiModule, TSU.Common.Instruments.Device.Manual.Theodolite.Instrument instrument)
            : base(guiModule)
        {
            _TheodoliteManuel = instrument;
            InitializeComponent();
            Init();
        }
        private void Init()
        {
            decimalSeparator = Convert.ToChar(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
            array = new Control[4, 5];
            array[0, 0] = this.button00;
            array[0, 1] = this.button01;
            array[0, 2] = this.button02;
            array[0, 3] = this.button03;
            array[0, 4] = this.button04;
            array[1, 0] = this.button10;
            array[1, 1] = this.button11;
            array[1, 2] = this.button12;
            array[1, 3] = this.button13;
            array[1, 4] = this.button14;
            array[2, 0] = this.button20;
            array[2, 1] = this.button21;
            array[2, 2] = this.button22;
            array[2, 3] = this.button23;
            array[2, 4] = this.button24;
            array[3, 0] = this.button30;
            array[3, 1] = this.button31;
            array[3, 2] = this.textBox01;
            array[3, 3] = this.button33;
            array[3, 4] = this.button34;

            //if you put "null" the previous button will nbe larger
            // by  default the action is to add the text from the button change it if needed
            SetUpButton(0, 0, "7");
            SetUpButton(0, 1, "8");
            SetUpButton(0, 2, "9");
            SetUpButton(0, 3, "Clr",Clear_Click);
            SetUpButton(0, 4, "Bck",DeleteLastCharacter_Click);
            SetUpButton(1, 0, "4");
            SetUpButton(1, 1, "5");
            SetUpButton(1, 2, "6");
            SetUpButton(1, 3, "*m", Mult10000_Click);
            SetUpButton(1, 4,"/m", Div10000_Click);
            SetUpButton(2, 0, "1");
            SetUpButton(2, 1, "2");
            SetUpButton(2, 2, "3");
            SetUpButton(2, 3, "Valid", Validate_Click);
            SetUpButton(2, 4, "");
            SetUpButton(3, 0, "0");
            SetUpButton(3, 1, ".");
            SetUpButton(3, 2, "0");
            SetUpButton(3, 3, "");
            SetUpButton(3, 4, "");
        }
        private void SetUpButton(int row, int column, string text, EventHandler newAction = null)
        {
            Control c = array[row, column];
                c.Text = text;

            if (!(c is Button)) return;

            if (newAction == null)
                c.Click += buttonWithValue_Click;
            else
                c.Click += newAction;
        }
        private void textBox1_Enter(object sender, EventArgs e)
        {
            textBox01.SelectAll();
        }
        private void buttonWithValue_Click(object sender, EventArgs e)
        {
            Add((sender as Button).Text);
        }
        private void Add(string text)
        {
            try
            {
                if (textBox01.SelectionLength == 0)
                {
                    textBox01.Text =T.Conversions.Numbers.ToDouble(textBox01.Text + text).ToString();
                }
                else
                {
                    string temp = textBox01.Text.Remove(textBox01.SelectionStart, textBox01.SelectionLength);
                    temp = temp.Insert(textBox01.SelectionStart, text);
                    textBox01.Text = temp;
                }
            }
            catch (Exception)
            {

            }
        }
        private void DeleteLastCharacter_Click(object sender, EventArgs e)
        {

        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (textBox01.Text != "")
                {
                    if (textBox01.Text[textBox01.Text.Length - 1] != decimalSeparator)
                        previousValue =T.Conversions.Numbers.ToDouble(textBox01.Text);
                }
                else
                {
                    previousValue = 0;
                    textBox01.Text = previousValue.ToString();
                }
            }
            catch (Exception)
            {
                textBox01.Text = previousValue.ToString();
                textBox01.SelectionStart = textBox01.Text.Length;
            }
            

        }
        private void Separator_Click(object sender, EventArgs e)
        {
            if (textBox01.SelectionLength == 0)
            {
                if (!textBox01.Text.Contains(decimalSeparator))
                    textBox01.Text += decimalSeparator;
            }
            else
            {
                string temp = textBox01.Text.Remove(textBox01.SelectionStart, textBox01.SelectionLength);
                temp = temp.Insert(textBox01.SelectionStart, decimalSeparator.ToString());
                textBox01.Text = temp;
            }
        }
        public void Validate_Click(object sender, EventArgs e)
        {
            Valid();
        }

        private void Valid()
        {
            
            doubleValue.Value =T.Conversions.Numbers.ToDouble(textBox01.Text, true);
            if (this.TopLevel == true)
                this.Hide();
            if (isShownInMessageTsu)
                this._SelectionView.Hide();
        }

        private double previousValue;

        private void Div10000_Click(object sender, EventArgs e)
        {
            previousValue /= 10000;
            textBox01.Text = previousValue.ToString();
        }
        private void Mult10000_Click(object sender, EventArgs e)
        {
            previousValue *= 10000;
            textBox01.Text = previousValue.ToString();
        }
        private void Clear_Click(object sender, EventArgs e)
        {
            previousValue = 0;
            textBox01.Text = previousValue.ToString();
            SelectText();
        }
        private void Manuel_ResizeEnd(object sender, EventArgs e)
        {
            UpdateView();
        }
        public  override void UpdateView()
        {
            int buttonHeight = (this.ClientSize.Height - ((numberOfRows+1) * spaceSize))/numberOfRows;
            int buttonWidth = (this.ClientSize.Width - ((numberOfColumns+1) * spaceSize)) / numberOfColumns;
            for (int i = 0; i < numberOfRows; i++)
            {
                for (int j = 0; j < numberOfColumns; j++)
                {
                    if (array[i, j].Text == "null")
                    {
                        array[i, j].Visible=false;
                        if (array[i, j - 1].Text == "null")
                        {
                            array[i, j-1].Visible = false;
                            array[i, j - 2].Width = 3 * buttonWidth + 2*spaceSize;
                        }
                        else
                            array[i, j - 1].Width = 2 * buttonWidth + spaceSize;
                    }
                    else
                    {
                        array[i, j].Height = buttonHeight;
                        array[i, j].Width = buttonWidth;
                        array[i, j].Top = spaceSize * (i + 1) + buttonHeight * i;
                        array[i, j].Left = spaceSize * (j + 1) + buttonWidth * j;
                        array[i, j].Font =TSU.Tsunami2.Preferences.Theme.Fonts.Normal;
                    }
                } 
            }
        }
       


        private void Manuel_ClientSizeChanged(object sender, EventArgs e)
        {
            if (!(this.resizingInProgress))
                UpdateView();
        }

        private void ManuelView_Shown(object sender, EventArgs e)
        {
            SelectText();
        }

        private void SelectText()
        {
            textBox01.Focus();
            textBox01.SelectAll();
        }

        private void textBox01_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                Valid();
        }


    }
}
