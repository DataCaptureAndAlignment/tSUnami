﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using TSU.Tools;
using T = TSU.Tools;


namespace TSU.Common.Instruments.Device.Manual.Ecartometer
{
    public partial class View : Device.View
    {
        private Instrument _ManualEcartometer
        {
            get
            {
                return this.Module.Instrument as Instrument;
            }
        }
        char decimalSeparator;
        private double previousValue;
        public View(Instruments.Module instrumentModule)
            : base(instrumentModule)
            //crèe la vue du manualEcartometer
        {
            //this._ManualEcartometer = ecartometer;
            //ecartometer._View = this;
            InitializeComponent();
            this.ApplyThemeColors();
            startingHeight = this.Height;
            startingWidth = this.Width;
            decimalSeparator = Convert.ToChar(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
        }
        private void ApplyThemeColors()
        {
            this.button1.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button1.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button2.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button2.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button3.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button3.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button4.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button4.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button5.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button5.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button6.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button6.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button7.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button7.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button8.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button8.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button9.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button9.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button10.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button10.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button11.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button11.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button12.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button12.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button13.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button13.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.textBox1.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.textBox1.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button14.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button14.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button15.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button15.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
        }
        private void textBox1_Enter(object sender, EventArgs e)
            //lorsqu'on entre dans la fenêtre textbox1, sélectionne tout
        {
            textBox1.SelectAll();
        }
        private void button_Click(object sender, EventArgs e)
            //événement lorsqu'on clique sur un bouton chiffre
        {
            Add((sender as Button).Text);
        }
        private void Add(string text)
            //fusionne la valeur dans le textbox avec la valeur du bouton appuyé
        {
            try
            { 
                if (textBox1.SelectionLength == 0)
                {
                    //permet de garder les zéros après la virgule
                    textBox1.Text=textBox1.Text + text;
                    string zeros = "";
                    if (textBox1.Text.Contains(decimalSeparator))
                    {
                        for (int i = textBox1.TextLength - 1; i >= 0; i--)
                        {
                            if (textBox1.Text[i].ToString() == "0")
                            {
                                zeros = zeros+"0";
                            }
                            else
                            {
                                //ajoute le decimal séparator si 0 juste après la virgule
                                if (textBox1.Text[i]==decimalSeparator)
                                {
                                    zeros = decimalSeparator.ToString() + zeros;
                                }
                                i = 0;
                            }
                        }
                    }
                    textBox1.Text =T.Conversions.Numbers.ToDouble(textBox1.Text).ToString() + zeros;
                }
                else
                {
                    //met dans temp la valeur sélectionnée et y ajoute la valeur du bouton appuyé
                    string temp = textBox1.Text.Remove(textBox1.SelectionStart, textBox1.SelectionLength);
                    temp = temp.Insert(textBox1.SelectionStart, text);
                    textBox1.Text = temp;
                }
            }
            catch (Exception)
            {

            }
        }
        private void delete_Click(object sender, EventArgs e)
            //événement lorsqu'on clique sur le bouton delete
        {
            if (textBox1.SelectionLength == 0 )
                //enlève le dernier caractère
            {
                if(textBox1.TextLength > 0)
                {
                    //if (textBox1.SelectionStart < 1) textBox1.SelectionStart = textBox1.TextLength;
                    //    string temp = textBox1.Text.Remove(textBox1.SelectionStart - 1, 1);
                    //textBox1.Text = temp;
                    textBox1.Text = textBox1.Text.Remove(textBox1.TextLength - 1, 1);
                }
            }
            else
                //efface toute la sélection
            {
                textBox1.Text = textBox1.Text.Replace(textBox1.Text.Substring(textBox1.SelectionStart, textBox1.SelectionLength), "");
            }
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
            //événement lorsqu'on change le texte dans le textbox1
        {
            try
            {
                if (textBox1.Text != "")
                {
                    if (textBox1.Text[textBox1.Text.Length - 1] != decimalSeparator)
                        previousValue =T.Conversions.Numbers.ToDouble(textBox1.Text);
                }
                else
                {
                    previousValue = 0;
                    textBox1.Text = previousValue.ToString();
                }
            }
            catch (Exception)
            {
                textBox1.Text = previousValue.ToString();
                textBox1.SelectionStart = textBox1.Text.Length;
            }
            

        }
        private void Separator_Click(object sender, EventArgs e)
            //Evénement lorsqu'on clique sur le bouton des décimales
        {
            if (textBox1.SelectionLength == 0)
            {
                if (!textBox1.Text.Contains(decimalSeparator))
                    textBox1.Text += decimalSeparator;
            }
            else
            {
                string temp = textBox1.Text.Remove(textBox1.SelectionStart, textBox1.SelectionLength);
                temp = temp.Insert(textBox1.SelectionStart, decimalSeparator.ToString());
                textBox1.Text = temp;
            }
        }
        private void Validate_Click(object sender, EventArgs e)
            //validation mesure et envoi de la valeur à l'observateur
        {
            //this.ShowMessageOfValidation("nice", Convert.ToDouble(textBox1.Text).ToString(), R.T_OK);
            double numberEntered = T.Conversions.Numbers.ToDouble(textBox1.Text,true,-9999);
            if (numberEntered!=-9999)
            {
                //this._ManualEcartometer.OnNext(numberEntered);
                TSU.Common.Measures.MeasureOfOffset m = new TSU.Common.Measures.MeasureOfOffset();
                m._Date = DateTime.Now;
                m._InstrumentSN = this._ManualEcartometer._SerialNumber;
                m._RawReading = numberEntered;
                SendMessage(m);
            }
            textBox1.Text = "";
        }
        private void ChangeSigne_Click(object sender, EventArgs e)
        //changement du signe
        {
            this.ChangeSigne();

        }
        /// <summary>
        /// Change le signe de la valeur entrée
        /// </summary>
        private void ChangeSigne()
        // Change le signe de la valeur entrée
        {
            previousValue = -previousValue;
            textBox1.Text = previousValue.ToString();
        }
        private void Clear_Click(object sender, EventArgs e)
        //Effacement du contenu du textbox
        {
            previousValue = 0;
            textBox1.Text = previousValue.ToString();
        }
        /// <summary>
        /// change l'echelle d'affichage de la fenêtre
        /// </summary>
        /// <param name="factorScale"></param>
        internal void ChangeScale(float factorScale)
        // change l'echelle d'affichage de la fenêtre
        {
            if (factorScale != 0)
            {
                this.button1.Font = new System.Drawing.Font("Impact", 36F * factorScale, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.button2.Font = new System.Drawing.Font("Impact", 36F * factorScale, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.button3.Font = new System.Drawing.Font("Impact", 36F * factorScale, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.button4.Font = new System.Drawing.Font("Impact", 36F * factorScale, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.button5.Font = new System.Drawing.Font("Impact", 36F * factorScale, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.button6.Font = new System.Drawing.Font("Impact", 36F * factorScale, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.button7.Font = new System.Drawing.Font("Impact", 36F * factorScale, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.button8.Font = new System.Drawing.Font("Impact", 36F * factorScale, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.button9.Font = new System.Drawing.Font("Impact", 36F * factorScale, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.button10.Font = new System.Drawing.Font("Impact", 36F * factorScale, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.button11.Font = new System.Drawing.Font("Impact", 36F * factorScale, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.button12.Font = new System.Drawing.Font("Impact", 18F * factorScale, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.button13.Font = new System.Drawing.Font("Impact", 24F * factorScale, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.button14.Font = new System.Drawing.Font("Impact", 18F * factorScale, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.button15.Font = new System.Drawing.Font("Impact", 18F * factorScale, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.textBox1.Font = new System.Drawing.Font("Impact", 36F * factorScale, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            }
        }
        /// <summary>
        /// Set Focus on textbox and select all text to be ready for edition
        /// </summary>
        internal void SetFocusToTextbox()
        //Set Focus on textbox and select all text to be ready for edition
        {
            this.textBox1.Focus();
            this.textBox1.SelectAll();
        }
        /// <summary>
        /// n'autorise plus de déplacer le form, mais cause un clignotement de la fenêtre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ManualTiltMeterView_LocationChanged(object sender, EventArgs e)
        {
            this.LocationChanged -= this.ManualTiltMeterView_LocationChanged;
            Location = new Point(0, 0);
            this.LocationChanged += new System.EventHandler(this.ManualTiltMeterView_LocationChanged);
        }

        private void PopUpMenu_Load(object sender, EventArgs e)
        {

        }
    }
}
