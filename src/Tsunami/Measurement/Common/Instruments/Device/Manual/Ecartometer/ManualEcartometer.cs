﻿using System;
using System.Xml.Serialization;

namespace TSU.Common.Instruments.Device.Manual.Ecartometer
{
    [XmlType(TypeName = "Manual.Ecartometer.Instrument")]
    [Serializable]
    public class Instrument : OffsetMeter
    {

    }
}
