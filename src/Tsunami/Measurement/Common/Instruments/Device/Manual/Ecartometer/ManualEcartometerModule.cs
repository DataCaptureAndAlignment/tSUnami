﻿using System;
using System.Xml.Serialization;

namespace TSU.Common.Instruments.Device.Manual.Ecartometer
{
    [Serializable]

    [XmlType(TypeName = "Manual.Ecartometer.Module")]
    public class Module : Instruments.Module
    {
        public Module(TSU.Module parentModule, Instruments.Instrument i)
            : base(parentModule)
        {
            this.Instrument = i;
        }

        public override void Initialize()
        {
            base.Initialize();
        }
        
        public override bool IsReadyToMeasure()
        {
            return true;
        }
    }
}
