﻿namespace TSU.Common.Instruments.Device.Manual
{
    partial class View
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (this.Module == null)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button00 = new System.Windows.Forms.Button();
            this.button01 = new System.Windows.Forms.Button();
            this.button02 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.textBox01 = new System.Windows.Forms.TextBox();
            this.button04 = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.button03 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button20
            // 
            this.button20.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button20.Font = new System.Drawing.Font("Impact", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button20.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button20.Location = new System.Drawing.Point(14, 178);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(100, 77);
            this.button20.TabIndex = 7;
            this.button20.Text = "20";
            this.button20.UseVisualStyleBackColor = true;
            // 
            // button21
            // 
            this.button21.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button21.Font = new System.Drawing.Font("Impact", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button21.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button21.Location = new System.Drawing.Point(120, 178);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(100, 77);
            this.button21.TabIndex = 8;
            this.button21.Text = "21";
            this.button21.UseVisualStyleBackColor = true;
            // 
            // button22
            // 
            this.button22.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button22.Font = new System.Drawing.Font("Impact", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button22.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button22.Location = new System.Drawing.Point(226, 178);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(100, 77);
            this.button22.TabIndex = 9;
            this.button22.Text = "22";
            this.button22.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button10.Font = new System.Drawing.Font("Impact", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button10.Location = new System.Drawing.Point(14, 95);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(100, 77);
            this.button10.TabIndex = 4;
            this.button10.Text = "10";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button11.Font = new System.Drawing.Font("Impact", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button11.Location = new System.Drawing.Point(120, 95);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(100, 77);
            this.button11.TabIndex = 5;
            this.button11.Text = "11";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // button12
            // 
            this.button12.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button12.Font = new System.Drawing.Font("Impact", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button12.Location = new System.Drawing.Point(226, 95);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(100, 77);
            this.button12.TabIndex = 6;
            this.button12.Text = "12";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // button00
            // 
            this.button00.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button00.Font = new System.Drawing.Font("Impact", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button00.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button00.Location = new System.Drawing.Point(14, 12);
            this.button00.Name = "button00";
            this.button00.Size = new System.Drawing.Size(100, 77);
            this.button00.TabIndex = 1;
            this.button00.Text = "00";
            this.button00.UseVisualStyleBackColor = true;
            // 
            // button01
            // 
            this.button01.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button01.Font = new System.Drawing.Font("Impact", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button01.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button01.Location = new System.Drawing.Point(120, 12);
            this.button01.Name = "button01";
            this.button01.Size = new System.Drawing.Size(100, 77);
            this.button01.TabIndex = 2;
            this.button01.Text = "01";
            this.button01.UseVisualStyleBackColor = true;
            // 
            // button02
            // 
            this.button02.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button02.Font = new System.Drawing.Font("Impact", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button02.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button02.Location = new System.Drawing.Point(226, 12);
            this.button02.Name = "button02";
            this.button02.Size = new System.Drawing.Size(100, 77);
            this.button02.TabIndex = 3;
            this.button02.Text = "02";
            this.button02.UseVisualStyleBackColor = true;
            // 
            // button30
            // 
            this.button30.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button30.Font = new System.Drawing.Font("Impact", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button30.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button30.Location = new System.Drawing.Point(14, 264);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(100, 77);
            this.button30.TabIndex = 10;
            this.button30.Text = "30";
            this.button30.UseVisualStyleBackColor = true;
            // 
            // button31
            // 
            this.button31.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button31.Font = new System.Drawing.Font("Impact", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button31.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button31.Location = new System.Drawing.Point(120, 264);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(100, 77);
            this.button31.TabIndex = 11;
            this.button31.Text = "31";
            this.button31.UseVisualStyleBackColor = true;
            // 
            // button13
            // 
            this.button13.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button13.Font = new System.Drawing.Font("Impact", 36F);
            this.button13.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button13.Location = new System.Drawing.Point(332, 95);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(100, 77);
            this.button13.TabIndex = 14;
            this.button13.Text = "13";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // button23
            // 
            this.button23.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button23.Font = new System.Drawing.Font("Impact", 36F);
            this.button23.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button23.Location = new System.Drawing.Point(332, 178);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(100, 77);
            this.button23.TabIndex = 16;
            this.button23.Text = "23";
            this.button23.UseVisualStyleBackColor = true;
            // 
            // textBox01
            // 
            this.textBox01.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.textBox01.Font = new System.Drawing.Font("Impact", 39.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox01.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.textBox01.Location = new System.Drawing.Point(227, 267);
            this.textBox01.Name = "textBox01";
            this.textBox01.Size = new System.Drawing.Size(99, 72);
            this.textBox01.TabIndex = 0;
            this.textBox01.Text = "0";
            this.textBox01.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox01.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox01.Enter += new System.EventHandler(this.textBox1_Enter);
            this.textBox01.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox01_KeyDown);
            // 
            // button04
            // 
            this.button04.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button04.Font = new System.Drawing.Font("Impact", 36F);
            this.button04.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button04.Location = new System.Drawing.Point(439, 12);
            this.button04.Name = "button04";
            this.button04.Size = new System.Drawing.Size(100, 77);
            this.button04.TabIndex = 13;
            this.button04.Text = "04";
            this.button04.UseVisualStyleBackColor = true;
            // 
            // button03
            // 
            this.button03.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button03.Font = new System.Drawing.Font("Impact", 36F);
            this.button03.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button03.Location = new System.Drawing.Point(332, 12);
            this.button03.Name = "button03";
            this.button03.Size = new System.Drawing.Size(100, 77);
            this.button03.TabIndex = 12;
            this.button03.Text = "03";
            this.button03.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button14.Font = new System.Drawing.Font("Impact", 36F);
            this.button14.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button14.Location = new System.Drawing.Point(438, 95);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(100, 77);
            this.button14.TabIndex = 15;
            this.button14.Text = "14";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // button24
            // 
            this.button24.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button24.Font = new System.Drawing.Font("Impact", 36F);
            this.button24.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button24.Location = new System.Drawing.Point(439, 178);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(100, 77);
            this.button24.TabIndex = 17;
            this.button24.Text = "24";
            this.button24.UseVisualStyleBackColor = true;
            // 
            // button33
            // 
            this.button33.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button33.Font = new System.Drawing.Font("Impact", 36F);
            this.button33.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button33.Location = new System.Drawing.Point(332, 264);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(100, 77);
            this.button33.TabIndex = 18;
            this.button33.Text = "33";
            this.button33.UseVisualStyleBackColor = true;
            // 
            // button34
            // 
            this.button34.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button34.Font = new System.Drawing.Font("Impact", 36F);
            this.button34.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button34.Location = new System.Drawing.Point(438, 264);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(100, 77);
            this.button34.TabIndex = 19;
            this.button34.Text = "34";
            this.button34.UseVisualStyleBackColor = true;
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.ClientSize = new System.Drawing.Size(551, 349);
            this.Controls.Add(this.button00);
            this.Controls.Add(this.button34);
            this.Controls.Add(this.button33);
            this.Controls.Add(this.button24);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button03);
            this.Controls.Add(this.button04);
            this.Controls.Add(this.textBox01);
            this.Controls.Add(this.button23);
            this.Controls.Add(this.button30);
            this.Controls.Add(this.button31);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button01);
            this.Controls.Add(this.button02);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button22);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.button20);
            this.Name = "View";
            this.Shown += new System.EventHandler(this.ManuelView_Shown);
            this.ResizeEnd += new System.EventHandler(this.Manuel_ResizeEnd);
            this.ClientSizeChanged += new System.EventHandler(this.Manuel_ClientSizeChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button button20;
        internal System.Windows.Forms.Button button21;
        internal System.Windows.Forms.Button button22;
        internal System.Windows.Forms.Button button10;
        internal System.Windows.Forms.Button button11;
        internal System.Windows.Forms.Button button12;
        internal System.Windows.Forms.Button button00;
        internal System.Windows.Forms.Button button01;
        internal System.Windows.Forms.Button button02;
        internal System.Windows.Forms.Button button30;
        internal System.Windows.Forms.Button button31;
        internal System.Windows.Forms.Button button13;
        internal System.Windows.Forms.Button button23;
        internal System.Windows.Forms.TextBox textBox01;
        internal System.Windows.Forms.Button button04;
        internal System.Windows.Forms.ColorDialog colorDialog1;
        internal System.Windows.Forms.Button button03;
        internal System.Windows.Forms.Button button14;
        internal System.Windows.Forms.Button button24;
        internal System.Windows.Forms.Button button33;
        internal System.Windows.Forms.Button button34;
    }
}
