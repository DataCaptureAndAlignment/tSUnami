﻿using System;
using System.Xml.Serialization;

namespace TSU.Common.Instruments.Device.Manual.TiltMeter
{
    [Serializable]
    [XmlType(TypeName = "Manual.TiltMeter.Instrument")]
    public class Instrument: TSU.Common.Instruments.TiltMeter
    {
        public Instrument()
        {
            
        }
    }
}
