﻿using System;
using System.Xml.Serialization;

namespace TSU.Common.Instruments.Device.Manual.TiltMeter
{
    [Serializable]
    [XmlType(TypeName = "Manual.TiltMeter.Module")]
    public class Module : Instruments.Module
    {
        public Module(TSU.Module parentModule, Instruments.Instrument i)
            : base(parentModule)
        {
            this.Instrument = i;
        }
        public override void Initialize()
        {
            base.Initialize();
        }

    }
}
