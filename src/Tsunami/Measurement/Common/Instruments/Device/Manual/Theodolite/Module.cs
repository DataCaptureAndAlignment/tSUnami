﻿using M = TSU;
using System.Xml.Serialization;
using System;
using TSU.Common.Measures;
using R = TSU.Properties.Resources;

using I = TSU.Common.Instruments;
using TSU.Common.Compute;

namespace TSU.Common.Instruments.Device.Manual.Theodolite
{
    [XmlType(TypeName = "Manual.Theodolite.Module")]
    public class Module : PolarModule
    {
        [XmlIgnore]
        public new ManualTheodoliteView View
        {
            get
            {
                return base._TsuView as ManualTheodoliteView;
            }
            set
            {
                base._TsuView = value;
            }
        }
        public Module() : base() { }

        public Module(M.Module parentModule, Instruments.Instrument i)
            : base(parentModule)
        {
            this.Instrument = i;
            this.ToBeMeasureReceived += delegate (object sender, MeasurementEventArgs e)
            {
                if (toBeMeasuredData != null)
                {
                    PrepareMeasure();
                }
            };
        }
        public override void Initialize()
        {
            base.Initialize();
            this.InstrumentIsOn = true; // manual instrument should always be ON.
        }

        public override void PrepareMeasure()
        {
            base.PrepareMeasure();
            this.OtherFaceWanted = (_ToBeMeasureTheodoliteData.Face == I.FaceType.DoubleFace);
            this.NumberOfMeasureOnThisFace = _BeingMeasuredTheodoliteData.NumberOfMeasureToAverage;
            this._BeingMeasuredOnThisFace = new System.Collections.Generic.List<Polar.Measure>();
            this._AlreadyMeasuredFace = null;

            M.Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(() => View.PrepareForNextAcquisition(_BeingMeasuredTheodoliteData));


        }

        internal override void OnMeasureToDoReceived()
        {
            base.OnMeasureToDoReceived();

            if (this.ToBeMeasuredData != null)
                if (ToBeMeasuredData._Status is Common.Measures.States.Good || _ToBeMeasureTheodoliteData.DirectMeasurementWanted)// this is the way to say 'i want  a measure now' without clicking gotoall button 
                {
                    bool measureNow = _ToBeMeasureTheodoliteData.DirectMeasurementWanted;
                    _ToBeMeasureTheodoliteData.DirectMeasurementWanted = false; // to make sure this behaviour is not reproduce with a remeasurement for example
                    this.GotoWanted = true;
                    if (measureNow)
                        this.MeasureLikeThis();
                }

        }

        internal override void SetDefaultStationParameter(Polar.Station.Parameters stationParameters)
        {
            base.SetDefaultStationParameter(stationParameters);
            stationParameters.DefaultMeasure.Distance.Reflector = Analysis.Instrument.GetReflector("CHAINE");
        }

        public override bool IsReadyToMeasure()
        {
            return true;
        }

        internal void LockUnLock()
        {
            // not applicable
        }

        public override void CheckIncomingMeasure(Polar.Measure m)
        {
            base.CheckIncomingMeasure(m);
            M.Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(() => View.PrepareForNextAcquisition(_BeingMeasuredTheodoliteData));
        }

        internal void ValidateEntry(TSU.DoubleValue doubleValue1, TSU.DoubleValue doubleValue2, TSU.DoubleValue doubleValue3, bool LaunchNextStep)
        {
            if (ToBeMeasuredData == null) // at least the case when reopening a.tsu file that have a prepared measure in the 'next point list'
            {
                this.WaitMeasurementDetails();
                PrepareMeasure();
            }
            //   if (_BeingMeasured == null) throw new Exception("Nothing to measure");
            if (BeingMeasured == null) throw new Exception(R.T_NOTHING_TO_MEASURE);
            if (doubleValue1 == null || doubleValue3 == null || doubleValue2 == null)
            {
                this.HaveFailed();
                throw new System.Exception(R.T220);
            }
            Survey.Modulo400(doubleValue1);
            Survey.Modulo400(doubleValue2);

            // add sigma a priori
            {
                Instruments.Instrument instrument = this.Instrument;
                SigmaForAInstrumentCouple sigma = M.Tsunami2.Preferences.Values.InstrumentSlashTargetCoupleSigmas.Find(
                    x => x.Instrument.ToUpper() == instrument._Model.ToUpper());

                if (sigma == null)
                    sigma = M.Tsunami2.Preferences.Values.InstrumentSlashTargetCoupleSigmas.Find(x => x.Instrument.ToUpper() == "Default" && x.Target.ToUpper() == "Default");
                if (sigma != null)
                {

                    doubleValue1.Sigma = sigma.sigmaAnglCc / 10000;
                    doubleValue2.Sigma = sigma.sigmaZenDCc / 10000;
                    doubleValue3.Sigma = sigma.sigmaDistMm / 1000;
                }
            }


            switch ((_BeingMeasuredTheodoliteData as Polar.Measure).Face)
            {
                case FaceType.Face1:
                    if (doubleValue2.Value > 200)
                        throw new Exception($"{R.T_VERTICAL_ANGLE_IS_BIGGER_THAN_200_GON};{R.T_ARE_YOU_SURE_TO_BE_IN_FACE1_POSITION_AS_ASKED}");
                    break;
                case FaceType.Face2:
                    if (doubleValue2.Value < 200)
                        throw new Exception($"{R.T_VERTICAL_ANGLE_IS_SMALLER_THAN_200_GON};{R.T_ARE_YOU_SURE_TO_BE_IN_FACE2_POSITION_AS_ASKED}");
                    break;
                case FaceType.DoubleFace:
                    if (doubleValue2.Value > 200)
                        _BeingMeasuredTheodoliteData.Face = FaceType.Face2;
                    else
                        _BeingMeasuredTheodoliteData.Face = FaceType.Face1;

                    break;
                case FaceType.Face1Reducted:
                case FaceType.UnknownFace:
                case FaceType.DoubleFaceReducted:
                default:
                    throw new Exception(R.T_FACE_PARAMETER_STRANGE_ACQUISITION_CANCELLED);
            }
            //this.StartMeasurement();

            Polar.Measure m = BeingMeasured as Polar.Measure;
            m._Date = System.DateTime.Now;
            m.Angles.Raw.Horizontal = doubleValue1;
            m.Angles.Raw.Vertical = doubleValue2;
            m.Distance.Raw = doubleValue3;

            if (LaunchNextStep)
                this.DoMeasureCorrectionsThenNextMeasurementStep(m);
        }

        internal override void ValidateMeasurement(Polar.Measure m)
        {
            base.ValidateMeasurement(m);
            this.FinishMeasurement();
            this.ReportMeasurement();
        }

        public override void MeasureLikeThis()
        {
            try
            {
                bool success = false;
                Action a = () =>
                {
                    base.MeasureLikeThis();
                    View.PrepareForNextAcquisition(_BeingMeasuredTheodoliteData);

                    if (this.IsFirstMesaureOfFirstFace)
                    {
                        if (this.CancelByMessageBeforeMeasure())
                        {
                            this.BeingMeasured = null;
                            throw new CancelException();
                        }
                    }

                    if (!(this.ParentInstrumentManager.ParentStationModule.FinalModule is Polar.Module) && !(this.ParentInstrumentManager.ParentStationModule.FinalModule is TSU.Length.Module))
                    {
                        this.View.valid.Visible = false;
                        this.View.cancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left))));
                        this.View.ShowInMessageTsu($"{R.T_ENTER_VALUES};{R.T_ENTER_VALUES}",
                        R.T_VALID, () =>
                            {
                                success = this.View.TryAction(() =>
                                {
                                    ValidateEntry(
                                    this.View.tbAH.Tag as DoubleValue,
                                    this.View.tbAV.Tag as DoubleValue,
                                    this.View.tbD.Tag as DoubleValue,
                                    LaunchNextStep: false); // next step will be launch lower
                                    this.View.valid.Visible = true;
                                    this.View.cancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
| System.Windows.Forms.AnchorStyles.Right)));
                                });
                            },
                        R.T_CANCEL, () =>
                            {
                                CancelMeasureInProgress();
                                this.View.valid.Visible = true;
                                this.View.cancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
| System.Windows.Forms.AnchorStyles.Right)));
                                this.DeclareMeasurementCancelled();
                            });
                    }
                };
                // launch the view in the main UI
                M.Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(a);
                if (!success)
                    throw new CancelException();

                // Launch next sub-measurement
                Polar.Measure m = BeingMeasured as Polar.Measure;
                if (!(this.ParentInstrumentManager.ParentStationModule.FinalModule is Polar.Module))
                {
                    this.DoMeasureCorrectionsThenNextMeasurementStep(m);
                }
            }
            catch (CancelException)
            {
                this.DeclareMeasurementCancelled();
            }
        }
    }
}
