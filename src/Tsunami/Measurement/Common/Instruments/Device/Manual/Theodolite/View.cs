﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TSU.Common.Elements;
using TSU.Common.Measures;
using TSU.Views.Message;
using M = TSU.Common.Measures;
using MMM = TSU;
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments.Device.Manual.Theodolite
{
    public class ManualTheodoliteView : Device.View
    {
        internal TextBox tbAH;
        internal TextBox tbAV;
        private Label label1;
        private Label label2;
        private SplitContainer splitContainer1;
        private TableLayoutPanel TLP_TextBox;
        private TableLayoutPanel TLP_buttons;
        private Panel backPanel;
        internal TextBox tbD;

        internal new Module Module
        {
            get
            {
                return this._Module as Module;
            }
            set
            {
                this._Module = value;
            }
        }

        public ManualTheodoliteView()
            : base()
        {
            InitializeComponent();
            this.ApplyThemeColors();
        }
        private void ApplyThemeColors()
        {
            this.label1.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.label1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.tbAH.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.tbAH.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.tbAV.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.tbAV.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.tbD.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.tbD.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.label2.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.label2.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.splitContainer1.Panel1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.splitContainer1.Panel2.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.backPanel.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;

        }
        public Views.BigButton valid;
        public Views.BigButton cancel;

        public ManualTheodoliteView(MMM.IModule parentModule)
            : base(parentModule)
        {
            InitializeComponent();
            InitializeComponent2();
            // add valid button
            valid = new Views.BigButton(R.T_MT_Valid, R.MessageTsu_OK);
            valid.BigButtonClicked += delegate
            {
                try
                {
                    (this.Module as Module).ValidateEntry(
                        tbAH.Tag as DoubleValue,
                        tbAV.Tag as DoubleValue,
                        tbD.Tag as DoubleValue,
                        LaunchNextStep: true);
                    //PrepareForNextAcquisition(null);
                }
                catch (Exception ex)
                {
                    new MessageInput(MessageType.Critical, ex.Message).Show();
                }

            };
            valid.TabIndex = 20;
            valid.Dock = DockStyle.Top;
            valid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
| System.Windows.Forms.AnchorStyles.Right)));

            // add cancel button
            cancel = new Views.BigButton(R.T_ResetValue, R.Cancel);
            cancel.BigButtonClicked += delegate
            {
                try
                {
                    double na = TSU.Tsunami2.Preferences.Values.na;
                    if (this.Module.BeingMeasured == null) throw new Exception(R.T_NOTHING_TO_MEASURE);
                    this.Module.BeingMeasured._Status = new M.States.Cancel();
                    this.Module.DeclareMeasurementCancelled();
                    this.Module.DoMeasureCorrectionsThenNextMeasurementStep(this.Module.BeingMeasured as Polar.Measure);
                    this.Module.PrepareMeasure();
                }
                catch (Exception ex)
                {
                    new MessageInput(MessageType.Critical, ex.Message).Show();
                }

            };
            cancel.TabIndex = 20;
            cancel.Dock = DockStyle.Top;
            cancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
| System.Windows.Forms.AnchorStyles.Right)));
            TLP_buttons.Controls.Add(cancel);
            TLP_buttons.Controls.Add(valid);
        }

        public override void MeasureAsync()
        {
            base.MeasureAsync(); // do nothing
            TryAction(LaunchMeasurement);

        }

        private void LaunchMeasurement()
        {
            Result r = new Result();
            // if (this.Module._ToBeMeasureTheodoliteData == null) throw new Exception("Nothing to measure");
            if (this.Module._ToBeMeasureTheodoliteData == null) throw new Exception(R.T_NOTHING_TO_MEASURE);

            TsuTask task = new TsuTask()
            {
                ActionName = "Measuring...",

                estimatedTimeInMilliSec = 0,
                timeOutInSec = 0,
                preAction = () =>
                {
                },
                mainAction = () =>
                {
                    r.MergeString(this.Module.Measure());
                },
                cancelAction = () =>
                {
                    this.Module.CancelMeasureInProgress();
                },
                postAction = () =>
                {
                    this.Module.BeingMeasured = null;
                    this.Module.DistanceWanted = true; // by default sot that measure() from polar Module take all.
                    if (r.Success)
                    {
                        // already reporte in overrded version of ValidateMeasurement.
                    }
                    else
                    {
                        Logs.Log.AddEntryAsResult(this.Module, r);
                    }
                }
            };

            this.RunTasksWithoutWaitingMessage(new List<TsuTask>() { task, }, "Measure");

        }

        private void InitializeComponent2()
        {
            //   this.label1.Text = "Waiting for a measure from the station module";
            this.label1.Text = R.T_WAITING_FOR_A_MEASURE_FROM_THE_STATION_MODULE;
            this.label2.Text = "";
            this.tbAH.Text = R.T_MT_EnterAH;
            this.tbAV.Text = R.T_MT_EnterAV;
            this.tbD.Text = R.T_MT_EnterD;
        }
        private void InitializeComponent()
        {
            this.backPanel = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.TLP_TextBox = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.tbAV = new System.Windows.Forms.TextBox();
            this.tbD = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbAH = new System.Windows.Forms.TextBox();
            this.TLP_buttons = new System.Windows.Forms.TableLayoutPanel();
            this.backPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.TLP_TextBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // backPanel
            // 
            this.backPanel.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.backPanel.Controls.Add(this.splitContainer1);
            this.backPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.backPanel.Location = new System.Drawing.Point(5, 5);
            this.backPanel.Name = "backPanel";
            this.backPanel.Padding = new System.Windows.Forms.Padding(5);
            this.backPanel.Size = new System.Drawing.Size(840, 262);
            this.backPanel.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(5, 5);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.TLP_TextBox);
            this.splitContainer1.Panel1MinSize = 175;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.TLP_buttons);
            this.splitContainer1.Panel2MinSize = 75;
            this.splitContainer1.Size = new System.Drawing.Size(830, 252);
            this.splitContainer1.SplitterDistance = 175;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 6;
            // 
            // TLP_TextBox
            // 
            this.TLP_TextBox.ColumnCount = 1;
            this.TLP_TextBox.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TLP_TextBox.Controls.Add(this.label1, 0, 0);
            this.TLP_TextBox.Controls.Add(this.tbAV, 0, 3);
            this.TLP_TextBox.Controls.Add(this.tbD, 0, 4);
            this.TLP_TextBox.Controls.Add(this.label2, 0, 1);
            this.TLP_TextBox.Controls.Add(this.tbAH, 0, 2);
            this.TLP_TextBox.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.TLP_TextBox.Location = new System.Drawing.Point(12, 3);
            this.TLP_TextBox.Name = "TLP_TextBox";
            this.TLP_TextBox.RowCount = 5;
            this.TLP_TextBox.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.TLP_TextBox.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.TLP_TextBox.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.TLP_TextBox.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.TLP_TextBox.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.TLP_TextBox.Size = new System.Drawing.Size(812, 172);
            this.TLP_TextBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Snow;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // tbAV
            // 
            this.tbAV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbAV.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAV.Location = new System.Drawing.Point(0, 105);
            this.tbAV.Margin = new System.Windows.Forms.Padding(0);
            this.tbAV.Name = "tbAV";
            this.tbAV.Size = new System.Drawing.Size(812, 31);
            this.tbAV.TabIndex = 4;
            this.tbAV.Click += new System.EventHandler(this.tb_Click);
            this.tbAV.TextChanged += new System.EventHandler(this.tb_Change);
            this.tbAV.DoubleClick += new System.EventHandler(this.tb_DoubleClick);
            this.tbAV.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_KeyDown);
            // 
            // tbD
            // 
            this.tbD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbD.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbD.Location = new System.Drawing.Point(0, 140);
            this.tbD.Margin = new System.Windows.Forms.Padding(0);
            this.tbD.Name = "tbD";
            this.tbD.Size = new System.Drawing.Size(812, 31);
            this.tbD.TabIndex = 5;
            this.tbD.Click += new System.EventHandler(this.tb_Click);
            this.tbD.TextChanged += new System.EventHandler(this.tb_Change);
            this.tbD.DoubleClick += new System.EventHandler(this.tb_DoubleClick);
            this.tbD.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_KeyDown);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Snow;
            this.label2.Location = new System.Drawing.Point(0, 35);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 35);
            this.label2.TabIndex = 0;
            this.label2.Text = "label2";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // tbAH
            // 
            this.tbAH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbAH.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAH.Location = new System.Drawing.Point(0, 70);
            this.tbAH.Margin = new System.Windows.Forms.Padding(0);
            this.tbAH.Name = "tbAH";
            this.tbAH.Size = new System.Drawing.Size(812, 31);
            this.tbAH.TabIndex = 3;
            this.tbAH.Click += new System.EventHandler(this.tb_Click);
            this.tbAH.TextChanged += new System.EventHandler(this.tb_Change);
            this.tbAH.DoubleClick += new System.EventHandler(this.tb_DoubleClick);
            this.tbAH.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_KeyDown);
            // 
            // TLP_buttons
            // 
            this.TLP_buttons.ColumnCount = 2;
            this.TLP_buttons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TLP_buttons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TLP_buttons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TLP_buttons.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.TLP_buttons.Location = new System.Drawing.Point(0, 0);
            this.TLP_buttons.Name = "TLP_buttons";
            this.TLP_buttons.RowCount = 1;
            this.TLP_buttons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TLP_buttons.Size = new System.Drawing.Size(830, 76);
            this.TLP_buttons.TabIndex = 0;
            // 
            // ManualTheodoliteView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(850, 272);
            this.Controls.Add(this.backPanel);
            this.Margin = new System.Windows.Forms.Padding(48, 22, 48, 22);
            this.MinimumSize = new System.Drawing.Size(0, 270);
            this.Name = "ManualTheodoliteView";
            this.Controls.SetChildIndex(this.backPanel, 0);
            this.Controls.SetChildIndex(this.buttonForFocusWithTabIndex0, 0);
            this.backPanel.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.TLP_TextBox.ResumeLayout(false);
            this.TLP_TextBox.PerformLayout();
            this.ResumeLayout(false);

        }


        private void tb_Click(object sender, EventArgs e)
        {
            string input = (sender as TextBox).Text;
            if (input == R.T_MT_EnterAH || input == R.T_MT_EnterAV || input == R.T_MT_EnterD)
            {
                (sender as TextBox).Text = "";
            }
            //UseCalculator(sender); moved to double click
        }

        private void UseCalculator(object sender)
        {
            DoubleValue d = new DoubleValue();
            Device.Manual.View Calc = new Manual.View(d);

            //if you put text to "null" the previous button will nbe larger
            // by  default the action is to add the text from the button change it if needed by addind an eventhandler parameter
            Calc.array[2, 3].Text = "Valid"; Calc.array[2, 3].Click += Calc.Validate_Click;
            Calc.array[2, 4].Text = "null";
            Calc.array[3, 3].Text = "null";
            Calc.array[3, 4].Text = "null";
            Calc.array[2, 3].TabIndex = 0;
            Calc.ShowDialog(this);

            if (d.Value == TSU.Tsunami2.Preferences.Values.na)
            {
                new MessageInput(MessageType.Critical, R.StringLevel_WrongCellValue).Show();
            }

            (sender as TextBox).Text = d.Value.ToString();
            (sender as TextBox).Tag = d;
            SwitchToNextControl(sender);
        }

        private void SwitchToNextControl(object sender)
        {
            var n = this.GetNextControl(sender as Control, true);
            if (n != null) n.Focus();
        }

        private void tb_Change(object sender, EventArgs e)
        {
            try
            {
                string input = (sender as TextBox).Text;
                (sender as TextBox).Tag = new DoubleValue(Tools.Conversions.Numbers.ToDouble(input, true));

                // could be a special input to automatise the input
                bool containsSpace = input.Contains(" ");
                bool containsDoubleDot = input.Contains(":");

                // all values in one line?
                string[] fields;
                fields = input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (containsSpace && fields.Length >= 3)
                {
                    // remove possible name
                    fields = TSU.Tools.Conversions.Numbers.GetNumerics(fields);
                    if (fields.Length >= 3)
                    {
                        tbAH.Text = fields[0];
                        tbAV.Text = fields[1];
                        tbD.Text = fields[2];
                        (this.Module as Module).ValidateEntry(
                        tbAH.Tag as DoubleValue,
                        tbAV.Tag as DoubleValue,
                        tbD.Tag as DoubleValue,
                        LaunchNextStep: true);
                    }
                }

            }
            catch (Exception)
            {

            }
        }

        private void tb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                (sender as TextBox).Tag = new DoubleValue(Tools.Conversions.Numbers.ToDouble((sender as TextBox).Text, true), 0);
                SwitchToNextControl(sender);
            }

        }

        internal void PrepareForNextAcquisition(Measures.Measure measure)
        {
            Polar.Measure m = (measure as Polar.Measure);

            if (m != null)
            {
                if (this.Module._BeingMeasuredTheodoliteData == null) return;


                int actualNumber = 1;
                actualNumber = m.NumberOfMeasureToAverage + 1 - this.Module.NumberOfMeasureOnThisFace;
                // if double face we can choose to start by any face...
                switch (m.Face)
                {
                    case FaceType.Face1:
                        this.label1.Text = string.Format(R.T_MT_MeasureDescription, this.Module.ToBeMeasuredData.ToString());
                        this.label1.Text.Replace("Corrected", "Expected");
                        this.label2.Text = $"{R.T_MEASURE} {actualNumber} {R.T_OF} {m.NumberOfMeasureToAverage} {R.T_ON} {m.Face.ToString()} :";
                        break;
                    case FaceType.Face2:
                        Polar.Measure temp = this.Module.ToBeMeasuredData.Clone() as Polar.Measure;
                        temp.Angles = temp.Angles.GetOppositeFace();
                        this.label1.Text = string.Format(R.T_MT_MeasureDescription, temp.ToString());
                        this.label1.Text.Replace("Corrected", "Expected");
                        this.label2.Text = $"{R.T_MEASURE} {actualNumber} {R.T_OF} {m.NumberOfMeasureToAverage} {R.T_ON} {m.Face.ToString()} :";
                        //    this.label2.Text = string.Format("Measure {0} of {1} on {2} :", actualNumber, m.NumberOfMeasureToAverage, m.Face.ToString());

                        break;
                    case FaceType.DoubleFace:
                        this.label1.Text = string.Format(R.T_MT_MeasureDescription, m.ToString());
                        this.label1.Text.Replace("Corrected", "Expected");
                        this.label2.Text = $"{R.T_MEASURE} {actualNumber} {R.T_OF} {m.NumberOfMeasureToAverage} {R.T_ON} {R.T_ANY_FACE} :";
                        break;
                    case FaceType.Face1Reducted:
                    case FaceType.DoubleFaceReducted:
                    case FaceType.UnknownFace:
                    default:
                        new MessageInput(MessageType.Critical, R.T_UNKNWON_FACE_TYPE_MEASUREMENT).Show();
                        return;
                }
                this.label1.Text += " (Click here to use nominal observations) (use -999.9 for not available observation).";
                this.label1.Tag = m;
                this.label2.Tag = m;
            }
            else
                InitializeComponent2();

            this.tbAH.Text = R.T_MT_EnterAH; this.tbAH.Tag = null;
            this.tbAV.Text = R.T_MT_EnterAV; this.tbAV.Tag = null;
            this.tbD.Text = R.T_MT_EnterD; this.tbD.Tag = null;
        }


        private void tb_DoubleClick(object sender, EventArgs e)
        {
            UseCalculator(sender);
        }


        private void label1_Click(object sender, EventArgs e)
        {
            UseNominalObservations(sender as Label);
        }


        private void label2_Click(object sender, EventArgs e)
        {
            UseNominalObservations(sender as Label, addSmallError:true);
        }

        private void UseNominalObservations(Label label, bool addSmallError = false)
        {
            var module = this.Module;
            double obs1;
            double obs2;
            double obs3;

            double additionalError = addSmallError? 0.0020: 0.0000;

            if (label.Tag != null && label.Tag is Polar.Measure polarMeasure)
            {
                bool face2 = polarMeasure.Face == FaceType.Face2;
                if (!face2)
                {
                    obs1 = 100;
                    obs2 = 100;
                    obs3 = 10;
                    if (!polarMeasure.Angles.Corrected.Horizontal.IsNa)
                    {
                        obs1 = polarMeasure.Angles.Corrected.Horizontal.Value + additionalError;
                        obs2 = polarMeasure.Angles.Corrected.Vertical.Value - additionalError;
                        obs3 = polarMeasure.Distance.Corrected.Value + additionalError;
                    }
                }
                else
                {
                    obs1 = 300;
                    obs2 = 300;
                    obs3 = 10;
                    if (!polarMeasure.Angles.Corrected.Horizontal.IsNa)
                    {
                        var oppositeAngles = Compute.Survey.TransformToOppositeFace(polarMeasure.Angles.Clone() as MeasureOfAngles);
                        obs1 = oppositeAngles.Corrected.Horizontal.Value + additionalError;
                        obs2 = oppositeAngles.Corrected.Vertical.Value - additionalError;
                        obs3 = polarMeasure.Distance.Corrected.Value + additionalError;
                    }
                }

                this.tbAH.Text = obs1.ToString();
                this.tbAV.Text = obs2.ToString();
                this.tbD.Text = obs3.ToString();
            }
        }

    }

}
