﻿using Microsoft.VisualStudio.TestPlatform.ObjectModel.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents.DocumentStructures;
using System.Windows.Markup;
using System.Windows.Shapes;
using System.Xml.Serialization;
using UnitTestGenerator;
using I = TSU.Common.Instruments;
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments.Device
{
    [Serializable]
    public class Interfaces : TsuObject
    {
        public Interfaces()
        {

        }


        /// <summary>
        /// use by serialisation
        /// </summary>
        [XmlAttribute("Items")]
        public string listOfInterfacesForSerailisation
        {
            get
            {
                string sList= "";
                foreach (var item in Items) sList += item.ToString() + "   ";
                return sList;
            }
            set
            {
                string[] names = value.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var name in names)
                {
                    this.Items.Add(Tsunami2.Preferences.Values.Instruments.Find(x => x._Name == name));
                }
            }
        }

        [XmlIgnore]
        public List<I.Instrument> Items { get; set; } = new List<I.Instrument>();

        [XmlIgnore]
        public int Count
        {
            get
            {
                return Items.Count;
            }
        }

        [XmlAttribute("Name")]
        public override string _Name
        {
            get
            {
                return ToString();
            }
            set
            {

            }
        }
        public override string ToString()
        {
            string name = "";
            foreach (var item in Items)
            {
                name += item._Name + " + ";
            }
            if (name == "")
                name = R.T_NONE;
            else
                name = name.Remove(name.Length - 3, 3);
            return name;
        }


        //return the name of the interface in the asked position (starting from 1)
        public string ToString(int position)
        {
            return Items.Count > position - 1 ? Items[position - 1]._Name : $"Interface{position}";
        }

        public string ToString(int position, char separator)
        {
            var none = "";
            return (Items.Count > position - 1) ?
                $"{Items[position - 1]._InstrumentType,-10};{Items[position - 1]._SerialNumber,-10};" :
                $"{none,-10}{separator}{none,-10}";
        }

        internal static bool AllMissingIn(object obj)
        {
            if (obj == null) return true;
            dynamic dynamicObject = obj;
            return AllMissingIn(dynamicObject);
        }

        internal static bool AllMissingIn(List<Tilt.Station> stations)
        {
            foreach (var station in stations)
            {
                if (station._MeasuresTilt[0].Interfaces.Items.Count > 0)
                    return false;
            }
            return true;
        }

        internal static bool AllMissingIn(TSU.Level.Station station)
        {
            foreach (var measure in station.MeasuresToDo)
            {
                if (measure.Interfaces != null && measure.Interfaces.Items.Count > 0)
                    return false;
            }
            return true;
        }

        internal static bool AllMissingIn(TSU.Polar.Station station)
        {
            foreach (var measure in station.MeasuresToDo)
            {
                if (measure.Interfaces != null && measure.Interfaces.Items.Count > 0)
                    return false;
            }
            return true;
        }

        internal static bool AllMissingIn(TSU.Line.Station station)
        {
            foreach (var measure in station.MeasuresTaken)
            {
                if (measure.Interfaces != null && measure.Interfaces.Items.Count > 0)
                    return false;
            }
            return true;
        }

        internal static void AddUniqueAssemblyToAList(Interfaces is1, List<Interfaces> knownInterfacesAssembly)
        {
            foreach (var is2 in knownInterfacesAssembly)
            {
                if (is1.Items.Count != is2.Items.Count)
                    continue; // next in FOREACH

                bool different = false;
                for (int i = 0; i < is1.Count; i++)
                {
                    if (is1.Items[i] != is2.Items[i])
                        different = true;
                }
                if (!different)
                    return;
            }
            knownInterfacesAssembly.Add(is1);
        }

        internal static bool CopyFromTo(object model, object fan)
        {
            dynamic dynamicModel = model;
            dynamic dynamicFan = fan;
            return CopyFromTo(dynamicModel, dynamicFan);
        }

        internal static void CopyFromTo(List<Tilt.Station> model, List<Tilt.Station> fan)
        {
            // they normaly exist in pair in the same order
            for (int i = 0; i < model.Count; i++)
            {
                fan[i]._MeasuresTilt[0].Interfaces = model[i]._MeasuresTilt[0].Interfaces;
            }
        }
        internal static void CopyFromTo(TSU.Level.Station model, TSU.Level.Station fan)
        {
            var models = model.MeasuresToDo;
            // they normaly exist in pair in the same order
            for (int i = 0; i < models.Count; i++)
            {
                fan.MeasuresToDo[i].Interfaces = models[i].Interfaces;
            }
        }
        internal static void CopyFromTo(TSU.Polar.Station model, TSU.Polar.Station fan)
        {
            var models = model.MeasuresTaken;
            // they normaly exist in pair in the same order
            for (int i = 0; i < models.Count; i++)
            {
                fan.MeasuresToDo[i].Interfaces = models[i].Interfaces;
            }
        }
    }
}
