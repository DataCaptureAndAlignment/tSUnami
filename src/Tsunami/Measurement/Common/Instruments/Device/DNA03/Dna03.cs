﻿using System.Xml.Serialization;

namespace TSU.Common.Instruments.Device.DNA03
{
    [XmlType(TypeName = "DNA03.Instrument")]
    public class Instrument : Level
    {
        public Instrument()
        {
            this._levelType = LevelType.DNA03;
        }
    }
}
