﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;
using System.Xml.Serialization;
using TSU.Common.Measures;
using M = TSU;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Common.Instruments.Device.DNA03
{
    [XmlType(TypeName = "DNA03.Module")]
    public class Module : Instruments.Module, IConnectable
    {
        #region Variables

        public Instrument Dna03
        {
            get
            {
                return this.Instrument as Instrument;
            }
        }

        internal Thread measurementThread;
        internal ManualResetEvent pauseEvent = new ManualResetEvent(true); // Initially, allow the thread to run
        internal bool threadPaused = false;

        private int numberOfMeasurement;
        public int _NumberOfMeasurement
        // nombre de mesure pour faire la moyenne
        {
            get
            {
                return this.numberOfMeasurement;
            }
            set
            {
                this.numberOfMeasurement = value;
            }
        }
        public MeasureOfLevel JustMeasure; // mesure qui sera envoyée au module nivellement
        //public D_H_Reading MeasureData { get; set; }
        internal SerialPort myPort;
        internal List<string> listPortName;
        internal int listPortIndex;
        internal string portName;
        [XmlIgnore]
        public M.Level.Station.Module stationLevelModule
        {
            get
            {
                if (this.ParentModule is Manager.Module instrumentManager 
                    && instrumentManager.ParentModule is M.Level.Station.Module stationModule) 
                    return stationModule;
                return null;
            }
        }

        #endregion

        #region Constructeur

        public override Result Measure()
        {
            return this.GetMeasure();
        }

        public Module(M.Module parentModule, Instruments.Instrument i)
            : base(parentModule)
        {
            this.Instrument = i;
            //this.Initialize(); //already called in the base module MODULE
        }

        /// <summary>
        /// Initialisation des parmètres DNA, fonction appelée depuis la base MODULE
        /// </summary>
        public override void Initialize()
        //Initialisation du DNA03
        {
            base.Initialize();
            this.JustMeasure = new MeasureOfLevel();
            listPortName = GetAllPorts();
            listPortIndex = 0;
            numberOfMeasurement = 3;
            if (listPortName.Count > 0)
            {
                portName = listPortName[listPortIndex];
                myPort = new SerialPort(portName, 9600, Parity.None, 8, StopBits.One);
            }
            else
            {
                portName = "Pas de Port disponible";
            }
        }
        public override Result InitializeSensor()
        //Initialisation avec les paramètres par défaut du DNA03
        {
            Result result = new Result();
            try
            {
                result.MergeString(SetBeepToLoud());
                result.MergeString(SetOutputToRs232());
                result.MergeString(SetUnitToMeters());
                result.MergeString(SetTo5Decimals());
                result.MergeString(SetProtocolOn());
                result.MergeString(SetEarthCurvatuveOff());
                result.MergeString(SetMireToNormalMode());
                result.MergeString(SetToGsi8());
                result.MergeString(SetAutoOff());
                if (result.Success)
                {
                    result.AccessToken = R.T_INITIALIZATION_SUCCEEDED;
                }
            }
            catch (Exception e)
            {
                e.Data.Clear();
                result.Success = false;
                result.ErrorMessage = R.T_INITIALIZATION_FAILED;
            }
            return result;
        }
        //public override void CreateView()
        //    //Creation de la vue DNA 03
        //{
        //    _View = new Dna03Form(this) { TopLevel = false };
        //    _View.ShowDockedFill();
        //}
        #endregion

        #region functions to communicate to DNA
        public virtual Result SendReceiveData(string input)
        //Envoi d'un string au DNA03 et réception de sa réponse
        {
            Result result = new Result();
            try
            {
                myPort.DiscardInBuffer();
                myPort.DiscardOutBuffer();
                myPort.ReadTimeout = 7000;
                myPort.WriteTimeout = 7000;
                myPort.Write(input + "\r\n");
                string data = myPort.ReadLine();
                switch (data)
                {
                    case "@W400" + "\r":
                        //   result.ErrorMessage = "Instrument busy ";
                        result.ErrorMessage = R.T_INSTRUMENT_BUSY;
                        result.Success = false;
                        break;
                    case "@W427" + "\r":
                        // result.ErrorMessage = "Invalid command ";
                        result.ErrorMessage = R.T_INVALID_COMMAND;
                        result.Success = false;
                        break;
                    case "@E458" + "\r":
                        // result.ErrorMessage = "Tilt sensor out of range";
                        result.ErrorMessage = R.T_TILT_SENSOR_OUT_OF_RANGE;
                        result.Success = false;
                        break;
                    case "@E439" + "\r":
                        //  result.ErrorMessage = "Measurement not possible";
                        result.ErrorMessage = R.T_MEASUREMENT_NOT_POSSIBLE;
                        result.Success = false;
                        break;
                    default:
                        result.AccessToken = data;
                        result.Success = true;
                        break;
                }
                return result;
            }
            catch (Exception e)
            {
                e.Data.Clear();
                result.Success = false;
                result.ErrorMessage = "Connection problem";
                return result;
            }
        }

        /// <summary>
        /// Communication avec DNA 03 : demande et réception du numéro de série au DNA03
        /// </summary>
        /// <returns></returns>
        public virtual Result GetSerialNumber()
        {
            Result result = SendReceiveData("GET/I/WI12");
            if (result.Success)
                result.AccessToken = result.AccessToken.ToString().Substring(9, 6);
            return result; 
        } 
        private Result GetBatteryLevel() { return SendReceiveData("CONF/90"); } //Communication avec DNA 03 :demande et réception du niveau de batterie
        public Result TurnOn() { return SendReceiveData("a"); } //Communication avec DNA 03 : POWER on
        public Result TurnOff() { return SendReceiveData("b"); } //Communication avec DNA 03 : POWER off
        private Result Clear() { return SendReceiveData("c"); } //Communication avec DNA 03 : nettoyage buffer
        private Result SetBeepToLoud() { return SendReceiveData("SET/30/2"); } //Communication avec DNA 03
        private Result SetUnitToMeters() { return SendReceiveData("SET/41/0"); } //Communication avec DNA 03
        private Result SetTo5Decimals() { return SendReceiveData("SET/51/5"); } //Communication avec DNA 03
        private Result SetProtocolOn() { return SendReceiveData("SET/75/1"); } //Communication avec DNA 03
        private Result SetEarthCurvatuveOff() { return SendReceiveData("SET/125/0"); } //Communication avec DNA 03
        private Result SetOutputToRs232() { return SendReceiveData("SET/76/1"); } //Communication avec DNA 03
        private Result SetMireToNormalMode() { return SendReceiveData("SET/127/0"); } //Communication avec DNA 03
        private Result SetToGsi8() { return SendReceiveData("SET/137/0"); } //Communication avec DNA 03
        private Result SetAutoOff() { return SendReceiveData("SET/95/0"); } //Communication avec DNA 03
        private Result GetMeasure() { return SendReceiveData("GET/M/WI32/WI330"); } //demande et réception d'une mesure
        public List<string> GetAllPorts()
        //Recherche des ports série disponibles du PC
        {
            List<string> allPorts = new List<string>();
            foreach (string portName in SerialPort.GetPortNames())
            {
                //evite d'ajouter les ports en double
                if (allPorts.FindIndex(x => x == portName) == -1)
                {
                    ///Tri par ordre
                    int index = 0;
                    foreach (string item in allPorts)
                    {
                        int a;
                        int b;
                        bool oka = int.TryParse(portName.Substring(3), out a);
                        bool okb = int.TryParse(item.Substring(3), out b);
                        if (oka && okb)
                        {
                            if (a > b) break;
                            index++;
                        }
                        else
                        {
                            break;
                        }

                    }
                    allPorts.Insert(index, portName);
                }
            }
            allPorts.Reverse();
            return allPorts;
        }
        public Result ChangePort()
        //Changement du port série
        {
            Result result = new Result();
            try
            {
                listPortIndex += 1;
                if (listPortIndex >= listPortName.Count) listPortIndex = 0;
                if (listPortName.Count > 0)
                {
                    portName = listPortName[listPortIndex];
                }
                result.AccessToken = "New port name: " + portName;
                result.Success = true;
            }
            catch (Exception)
            {
                result.Success = false;
                //   result.ErrorMessage = "Cannot change port";
                result.ErrorMessage = R.T_CANNOT_CHANGE_PORT;
            }
            return result;
        }

        public override Result Connect()
        //Connection via le port série au DNA03
        {
            Result result = new Result();
            try
            {
                myPort = new SerialPort(portName, 9600, Parity.None, 8, StopBits.One);
                if (!myPort.IsOpen)
                {
                    try
                    {
                        myPort.Open();
                    }
                    catch (Exception ex)
                    {
                        ex.Data.Clear();
                        //if (myPort.IsOpen) myPort.Close();
                    }
                }
                //myPort.Open();
                if (TurnOn().Success)
                {
                    InstrumentIsOn = true;
                    result = GetSerialNumber();
                    if (result.Success)
                    {
                        string DnaConnectedSN = result.AccessToken;
                        //Vérification que le SN du DNA connecté correspond au DNA choisi
                        if (DnaConnectedSN == this.Dna03._SerialNumber)
                        {
                            this.JustMeasure._InstrumentSN = this.Dna03._SerialNumber;
                            result.Success = true;
                            result.AccessToken = "Connected to " + Dna03._SerialNumber + " on port " + portName;
                        }
                        else
                        {
                            //Fermeture connection si ce n'est pas le bon DNA
                            InstrumentIsOn = false;
                            myPort.Dispose();
                            myPort.Close();
                            result.Success = false;
                            result.ErrorMessage = "Dna03 connected is SN " + DnaConnectedSN + " on port " + portName + ", not the Dna03 SN " + this.Dna03._SerialNumber + ". Closing connection";
                            this.stationLevelModule?.CheckAndChangeInstrument(DnaConnectedSN);
                        }
                    }
                }
                else
                {
                    myPort.Dispose();
                    myPort.Close();
                    result.Success = false;
                    result.ErrorMessage = "Cannot connect to DNA03 SN " + this.Dna03._SerialNumber + " on port " + portName;
                }
                return result;
            }
            catch (Exception e)
            {
                e.Data.Clear();
                result.Success = false;
                InstrumentIsOn = false;
                // result.ErrorMessage = "Connection failed on port "+ portName;
                result.ErrorMessage = R.T_CONNECTION_FAILED_ON_PORT + portName;
            }
            return result;
        }

        public virtual Result TryConnectOthersPorts()
        {
            Result result = new Result();
            if (InstrumentIsOn == false)
            {
                //In case the dna is connected after starting the module
                this.GetAllPorts();
                int j = this.listPortName.FindIndex(x => x == this.portName);
                if (j != -1) this.listPortIndex = j;
                else this.listPortIndex = 0;
                for (int i = 1; i < this.listPortName.Count; i++)
                {

                    if (InstrumentIsOn == false)
                    {
                        //change de port et essaye de se connecter
                        this.ChangePort();
                        myPort = new SerialPort(portName, 9600, Parity.None, 8, StopBits.One);
                        if (!myPort.IsOpen)
                        {
                            try
                            {
                                myPort.Open();
                            }
                            catch (Exception ex)
                            {
                                ex.Data.Clear();
                                // if (myPort.IsOpen) myPort.Close();
                            }
                        }
                        if (TurnOn().Success)
                        {
                            InstrumentIsOn = true;
                            result = GetSerialNumber();
                            if (result.Success)
                            {
                                string DnaConnectedSN = result.AccessToken;
                                //Vérification que le SN du DNA connecté correspond au DNA choisi
                                if (DnaConnectedSN == this.Dna03._SerialNumber)
                                {
                                    this.JustMeasure._InstrumentSN = this.Dna03._SerialNumber;
                                    result.Success = true;
                                    result.AccessToken = "Connected to " + Dna03._SerialNumber + " on port " + portName;
                                }
                                else
                                {
                                    //Fermeture connection si ce n'est pas le bon DNA
                                    InstrumentIsOn = false;
                                    myPort.Dispose();
                                    myPort.Close();
                                    this.stationLevelModule.CheckAndChangeInstrument(DnaConnectedSN);
                                    result.Success = false;
                                    //retour ligne si error message n'est pas vide
                                    if (result.ErrorMessage != "") { result.ErrorMessage += "\r\n" + DateTime.Now.ToString("yyyy-mm-dd, h:mm:ss tt") + ": "; }
                                    result.ErrorMessage += "Dna03 connected is SN " + DnaConnectedSN + " on port " + portName + ", not the Dna03 SN " + this.Dna03._SerialNumber + ". Closing connection!";
                                }
                            }
                        }
                        else
                        {
                            //n'est pas arrivé à se connecter sur le port choisi -> ferme ce port
                            myPort.Dispose();
                            myPort.Close();
                            result.Success = false;
                            //retour ligne si error message n'est pas vide
                            if (result.ErrorMessage != "") { result.ErrorMessage += "\r\n" + DateTime.Now.ToString("yyyy-mm-dd, h:mm:ss tt") + ": "; }
                            //   result.ErrorMessage += "Connection failed on port " + portName; 
                            result.ErrorMessage += R.T_CONNECTION_FAILED_ON_PORT + portName;
                            InstrumentIsOn = false;
                        }
                    }
                }
            }
            else
            {
                //Si le DNA03 était déjà ON lors du lancement de la fonction, ne fait rien
                result.Success = false;
                result.ErrorMessage = "DNA 03 is already ON";
            }
            if (InstrumentIsOn == false)
            //N'a pas trouvé de DNA03 sur aucun des ports
            {
                result.Success = false;
                //retour ligne si error message n'est pas vide
                if (result.ErrorMessage != "") { result.ErrorMessage += "\r\n" + DateTime.Now.ToString("yyyy-mm-dd, h:mm:ss tt") + ": "; }
                result.ErrorMessage += "Cannot find DNA03 SN " + this.Dna03._SerialNumber + " on all COM ports";
            }
            return result;
        }
        //public override Result Management.Measure() 
        //    //Lancement d'une mesure
        //{
        //    Result result = new Result();
        //    result=GetStaticMeasurement(_NumberOfMeasurement);
        //    return result;
        //}
        public override Result Disconnect()
        //déconnection du DNA03
        {
            Result result = new Result();
            try
            {
                if (myPort.IsOpen == true)
                {
                    this.TurnOff();
                    myPort.Close();
                }
                InstrumentIsOn = false;
                result.Success = true;
                //   result.AccessToken = "Disconnection succeed";
                result.AccessToken = R.T_DISCONNECTION_SUCCEED;
            }
            catch (Exception e)
            {
                e.Data.Clear();
                result.Success = false;
                //  result.ErrorMessage = "Disconnection Failed";
                result.ErrorMessage = R.T_DISCONNECTION_FAILED;
            }

            return result;
        }
        public virtual int GetIntBatteryLevel()
        //demande le niveau batterie au DNA et renvoie son niveau de 0 à 100
        {
            Result result = new Result();
            result = GetBatteryLevel();
            int level;
            if (int.TryParse(result.AccessToken.ToString().Substring(7, 2), out level))
            {
                level = level * 10;
            }
            else
            {
                level = 0;
            }
            return level;
        }
        #endregion

        #region reception mesure et envoi mesure à l'observateur du module nivellement
        public virtual Result DoSingleMeasure(MeasureOfLevel singleMeas)
        //fait une simple mesure et envoie la mesure
        {
            Result result = new Result();
            result = this.Measure();
            if (result.Success)
            {
                (double levelReading, double dist) = ConvertMeasurementResults(result);
                singleMeas._Date = DateTime.Now;
                if (dist != -9999)
                {
                    singleMeas._Distance = dist;
                }
                else
                {
                    this.DnaMeasureFailed();
                    result.Success = false;
                    //   result.AccessToken = "Cannot read distance";
                    result.AccessToken = R.T_CANNOT_READ_DISTANCE;
                }
                if (levelReading != -9999)
                {
                    singleMeas._RawLevelReading = levelReading;
                }
                else
                {
                    this.DnaMeasureFailed();
                    result.Success = false;
                    //   result.AccessToken = "Cannot read level reading";
                    result.AccessToken = R.T_CANNOT_READ_LEVEL_READING;
                }
            }
            return result;
        }

        // seperate the single line result from the instrument into an height in millimeters and a distance in meters
        public static (double levelReading, double distance) ConvertMeasurementResults(Result result)
        {
            double defaultValue = -9999;
            double levelReading = defaultValue;
            double distance = defaultValue;
            if (result.Success)
            {
                levelReading = T.Conversions.Numbers.ToDouble(result.AccessToken.ToString().Substring(22, 9), false, defaultValue);
                distance = T.Conversions.Numbers.ToDouble(result.AccessToken.ToString().Substring(6, 9), false, defaultValue);

                if (levelReading != defaultValue)
                    levelReading = levelReading / 100000; //Distance donnée par DNA est au centième de mm

                if (distance != defaultValue)
                    distance = distance / 1000; //Distance donnée par DNA est au mm
            }
            return (levelReading, distance);

        }

        internal Result CalculateAverageMeasure(List<MeasureOfLevel> listMeasure)
        //fait la moyenne d'une liste de plusieurs mesures, calcul l'EMQ pour les lectures niveau et les distances
        {
            Result result = new Result();
            double totalLevelreading = 0;
            double totalDistance = 0;
            double totalCompass = 0;
            foreach (MeasureOfLevel meas in listMeasure)
            {
                totalLevelreading += meas._RawLevelReading;
                totalDistance += meas._Distance;
                totalCompass += meas._Compass;
            }
            if (listMeasure.Count > 0)
            {
                //STocke la mesure dans la propriété this_DnaMeasure
                this.JustMeasure._Date = DateTime.Now;
                this.JustMeasure._RawLevelReading = totalLevelreading / listMeasure.Count;
                this.JustMeasure._Distance = totalDistance / listMeasure.Count;
                this.JustMeasure._Compass = totalCompass / listMeasure.Count;
                double emqHeight = 0;
                double emqDist = 0;
                foreach (MeasureOfLevel meas in listMeasure)
                {
                    emqHeight += Math.Pow(this.JustMeasure._RawLevelReading - meas._RawLevelReading, 2);
                    emqDist += Math.Pow(this.JustMeasure._Distance - meas._Distance, 2);
                }
                this.JustMeasure._EmqDistanceReading = Math.Sqrt(emqDist / (listMeasure.Count - 1));
                this.JustMeasure._RawEmqLevelReading = Math.Sqrt(emqHeight / (listMeasure.Count - 1));
                result.AccessToken = string.Format("Average reading= {0} mm Emq= {1} mm. Average Distance= {2} m Emq = {3} mm. Average Compass = {4} grad."
                    , Math.Round(this.JustMeasure._RawLevelReading * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"))
                    , Math.Round(this.JustMeasure._RawEmqLevelReading * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"))
                    , Math.Round(this.JustMeasure._Distance, 3).ToString("F3", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"))
                    , Math.Round(this.JustMeasure._EmqDistanceReading * 1000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"))
                    , (Math.Round(this.JustMeasure._Compass * 1000, 0) / 1000).ToString("F4", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
            }
            return result;
        }
        private void DnaMeasureFailed()
        //remplace la propriété mesure DNA par les valeurs 9999 pour indiquer que la mesure est ratée
        {
            MeasureFailed();
        }

        internal void MeasureFailed()
        {
            this.JustMeasure._Date = DateTime.MinValue;
            double na = Tsunami2.Preferences.Values.na;
            this.JustMeasure._RawEmqLevelReading = na;
            this.JustMeasure._RawLevelReading = na;
            this.JustMeasure._Distance = na;
            this.JustMeasure._EmqDistanceReading = na;
        }

        internal void OnNext()
        //envoi mesure au module nivellement
        {
            int ie = this.observers.Count;
            SendMessage(this.JustMeasure);
        }

        void IConnectable.Refresh()
        {
            throw new NotImplementedException();
        }
        #endregion

        internal void PauseMeasurementThread()
        {
            pauseEvent.Reset(); // Set the event to non-signaled, pausing the thread
            threadPaused = true;
        }

        internal void ResumeMeasurementThread()
        {
            pauseEvent.Set(); // Set the event to signaled, allowing the thread to resume
        }

        public virtual Result GetCompassValue()
        {
            throw new NotImplementedException();
        }

    }
}
