﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using TSU.Common.Measures;
using TSU.Views.Message;
using M = TSU;
using R = TSU.Properties.Resources;


namespace TSU.Common.Instruments.Device.DNA03
{
    public partial class View : Device.View
    {
        #region variable
        private System.Windows.Forms.ToolTip toolTip1;
        public bool measDone = false;
        protected new Module Module
        {
            get
            {
                return this._Module as Module;
            }
        }
        protected Instrument Instrument
        {
            get
            {
                return this.Module.Instrument as Instrument;
            }
        }
        protected Result toAddInLogbook;
        #endregion
        #region constructeur de la vue
        public View()
        {
            InitializeComponent();
        }

        public View(M.IModule module)
            : base(module)
        {
            InitializeComponent();
            this.ApplyThemeColors();
            InitializeLog();
            //Obligation de forcer la position du splitter au sinon visual studio le déplace et laisse un espace sous la barre des boutons
            this.splitContainer_margin.SplitterDistance = (this.splitContainer_margin.Size.Height - 11);
            toAddInLogbook = new Result();
            this.ModeButtonsOff();
            this.SetAllToolTips();
            timerImageConnecting.Enabled = false;
            picture_Connecting.Visible = false;
            this.ChangeButtonPortCOMImage((module as Module).portName);
            this.RefreshBatteryLevelButton();
            this.RefreshButtonMeasure();
            buttonPortCom.Visible = true;
        }

        private void InitializeLog()
        {
            // 
            // log
            // 
            LogView = new Logs.LogView();
            LogView.SetColors();
            LogView.ShowDockedFill();
            panelLog.Controls.Add(LogView);
        }

        protected void ApplyThemeColors()
        {
            this.buttonMoins.ImageTransparentColor = Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonMoins.BackColor = Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonMeasure.BackColor = Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonMeasure.ImageTransparentColor = Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonPlus.ImageTransparentColor = Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonPlus.BackColor = Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonLoop.BackColor = Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonLoop.ImageTransparentColor = Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonPower.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.buttonPower.ImageTransparentColor = Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonPortCom.BackColor = Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonPortCom.ImageTransparentColor = Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonInit.BackColor = Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonInit.ImageTransparentColor = Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonBatteryLevel.BackColor = Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonBatteryLevel.ImageTransparentColor = Tsunami2.Preferences.Theme.Colors.Transparent;
            this.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
        }
        protected void ChangeButtonPortCOMImage(string portName)
            //change l'image du bouton port com en fonction du port choisi
        {
            
            switch (portName)
            {
                case "COM1":
                    this.buttonPortCom.Image =R.DNA03_Com_1;
                    break;
                case "COM2":
                    this.buttonPortCom.Image =R.DNA03_Com_2;
                    break;
                case "COM3":
                    this.buttonPortCom.Image =R.DNA03_Com_3;
                    break;
                case "COM4":
                    this.buttonPortCom.Image =R.DNA03_Com_4;
                    break;
                case "COM5":
                    this.buttonPortCom.Image =R.DNA03_Com_5;
                    break;
                case "COM6":
                    this.buttonPortCom.Image =R.DNA03_Com_6;
                    break;
                case "COM7":
                    this.buttonPortCom.Image =R.DNA03_Com_7;
                    break;
                case "COM8":
                    this.buttonPortCom.Image =R.DNA03_Com_8;
                    break;
                case "COM9":
                    this.buttonPortCom.Image =R.DNA03_Com_9;
                    break;
                case "COM10":
                    this.buttonPortCom.Image =R.DNA03_Com_10;
                    break;
                case "COM11":
                    this.buttonPortCom.Image =R.DNA03_Com_11;
                    break;
                case "COM12":
                    this.buttonPortCom.Image =R.DNA03_Com_12;
                    break;
                case "COM13":
                    this.buttonPortCom.Image =R.DNA03_Com_13;
                    break;
                case "COM14":
                    this.buttonPortCom.Image =R.DNA03_Com_14;
                    break;
                case "COM15":
                    this.buttonPortCom.Image =R.DNA03_Com_15;
                    break;
                case "COM16":
                    this.buttonPortCom.Image =R.DNA03_Com_16;
                    break;
                case "COM17":
                    this.buttonPortCom.Image =R.DNA03_Com_17;
                    break;
                case "COM18":
                    this.buttonPortCom.Image =R.DNA03_Com_18;
                    break;
                case "COM19":
                    this.buttonPortCom.Image =R.DNA03_Com_19;
                    break;
                case "COM20":
                    this.buttonPortCom.Image =R.DNA03_Com_7;
                    break;
                default:
                    this.buttonPortCom.Image =R.DNA03_Com_X;
                    break;
            }
        }
        #endregion
        #region fonctions
        private void buttonPower_Click(object sender, EventArgs e) 
            //Demarrage DNA, essai connection sur tous les ports, initialization ou déconnection dna03
        {
            this.ModeButtonsOff();
            buttonBar.Visible = false;
            this.picture_Connecting.Visible = true;
            buttonCancel.Visible = true;
            buttonCancel.BringToFront();
            this.backgroundWorker_Connecting.RunWorkerAsync();
        }
        private void buttonInitialisation_Click(object sender, EventArgs e)
            //initialisation du DNA si problème
        {
            //pas possible d'arrêter l'initialisation car trop rapide
            buttonCancel.Visible = false;
            //buttonCancel.BringToFront();
            this.timerDNA03.Enabled = false;
            this.picture_Connecting.Visible = true;
            //lancement du backgroundworker en tâche de fond
            backgroundWorker_Initialization.RunWorkerAsync();
        }
        public virtual void buttonBatteryLevel_Click(object sender, EventArgs e)
            //Force la vérification du niveau batterie et l'ajoute au logbook
        {
            int battLevel = Module.GetIntBatteryLevel();
            this.RefreshBatteryLevelButton(battLevel);
            Logs.Log.AddEntryAsPayAttentionOf(this._Module, "The Battery level is " + battLevel + "%");
        }
        
        public virtual void buttonMeasure_Click(object sender, EventArgs e)
        //prise de mesure
        {
            LaunchMeasure();
        }
        /// <summary>
        /// Lance une mesure par le DNA03
        /// </summary>
        internal void LaunchMeasure()
        {
            buttonCancel.Visible = true;
            buttonCancel.BringToFront();
            this.timerDNA03.Enabled = false;
            this.picture_Connecting.Visible = true;
            this.backgroundWorker_Measuring.RunWorkerAsync();
        }

        private void buttonLoop_Click(object sender, EventArgs e)
            //lancement des mesures continues pour alignement d'un élément
        {
            buttonCancel.Visible = true;
            buttonCancel.BringToFront();
            this.timerDNA03.Enabled = false;
            this.picture_Connecting.Visible = true;
            this.backgroundWorker_Loop.RunWorkerAsync();
        }

        private void buttonCompass_Click(object sender, EventArgs e)
        //lancement des mesures continues pour alignement d'un élément
        {
            buttonCancel.Visible = true;
            buttonCancel.BringToFront();
            this.timerDNA03.Enabled = false;
            this.picture_Connecting.Visible = true;
            this.backgroundWorker_Compass.RunWorkerAsync();
        }
        public virtual void buttonPortCom_Click(object sender, EventArgs e)
            //choix du port com
        {
            if (base.Module.InstrumentIsOn==false)
            {
                Result result = new Result();
                Module.GetAllPorts();
                result = Module.ChangePort();
                this.ChangeButtonPortCOMImage(Module.portName);
                Logs.Log.AddEntryAsResult(this._Module, result);
            } 
        }
        private void buttonMoins_Click(object sender, EventArgs e)
        //diminue le nombre de mesure jusqu'à un min de 1 
        {
            if (Module._NumberOfMeasurement > 1)
            {
                Module._NumberOfMeasurement -= 1;
            }
            else
            {
                Result result = new Result();
                result.Success = false;
                result.ErrorMessage = "1 measure is the minimum";
                Logs.Log.AddEntryAsResult(this._Module, result);
            }
            this.RefreshButtonMeasure();
        }

        private void buttonPlus_Click(object sender, EventArgs e)
        //augmente le nombre de mesure jusqu'à un max de 20
        {
            if (Module._NumberOfMeasurement < 20)
            {
                Module._NumberOfMeasurement += 1; 
            }
            else
            {
                Result result = new Result();
                result.Success = false;
                result.ErrorMessage = "20 measures is the maximum";
                Logs.Log.AddEntryAsResult(this._Module, result);
            }
            this.RefreshButtonMeasure();
        }
        public virtual void buttonCancel_Click(object sender, EventArgs e)
        {
            this.TryAction(CancelBackGroundWorker);
        }
        public void CancelBackGroundWorker()
        {
            //Annule la connection en cours du backgroundworker connecting
            if ((backgroundWorker_Connecting.IsBusy) && (backgroundWorker_Connecting.WorkerSupportsCancellation == true))
            {
                backgroundWorker_Connecting.CancelAsync();
            }
            //Annule la mesure en cours du backgroundworker measuring
            if ((backgroundWorker_Measuring.IsBusy) && (backgroundWorker_Measuring.WorkerSupportsCancellation == true))
            {
                backgroundWorker_Measuring.CancelAsync();
            }
            //Annule la mesure en cours du backgroundworker loop
            if ((backgroundWorker_Loop.IsBusy) && (backgroundWorker_Loop.WorkerSupportsCancellation == true))
            {
                backgroundWorker_Loop.CancelAsync();
            }
        }
        protected void ModeButtonsOff()
            //affiche que certains boutons lorsque DNA est off
        {
            this.buttonBatteryLevel.Visible = false;
            this.buttonInit.Visible = false;
            this.buttonLoop.Visible = false;
            this.buttonMeasure.Visible = false;
            this.buttonPlus.Visible = false;
            this.buttonMoins.Visible = false;
            this.buttonCompass.Visible = false;
        }
        private void ModeButtonsOn()
        //Affiche tous les boutons lorsque le DNA est ON
        {
            this.buttonBatteryLevel.Visible = true;
            if (this.Module.Instrument._Model != "LS15")
                this.buttonInit.Visible = true;
            // desactive la fonction mesure continue
            // this.buttonLoop.Visible = true;
            this.buttonMeasure.Visible = true;
            this.buttonPortCom.Visible = true;
            this.buttonPower.Visible = true;
            this.buttonPlus.Visible = true;
            this.buttonMoins.Visible = true;
        }
        protected void RefreshBatteryLevelButton(int level = 0)
        //rafraichit le bouton battery level avec la bonne image
        {
            //Change l'image en fonction du battery level
            switch (level)
            {
                case 100:
                    this.buttonBatteryLevel.Image =R.DNA03_Batt_100;
                    break;
                case 90:
                    this.buttonBatteryLevel.Image =R.DNA03_Batt_90;
                    break;
                case 80:
                    this.buttonBatteryLevel.Image =R.DNA03_Batt_80;
                    break;
                case 70:
                    this.buttonBatteryLevel.Image =R.DNA03_Batt_70;
                    break;
                case 60:
                    this.buttonBatteryLevel.Image =R.DNA03_Batt_60;
                    break;
                case 50:
                    this.buttonBatteryLevel.Image =R.DNA03_Batt_50;
                    break;
                case 40:
                    this.buttonBatteryLevel.Image =R.DNA03_Batt_40;
                    break;
                case 30:
                    this.buttonBatteryLevel.Image =R.DNA03_Batt_30;
                    break;
                case 20:
                    this.buttonBatteryLevel.Image =R.DNA03_Batt_20;
                    break;
                case 10:
                    this.buttonBatteryLevel.Image =R.DNA03_Batt_10;
                    break;
                case 0:
                    this.buttonBatteryLevel.Image =R.DNA03_Batt_0;
                    break;
                default:
                    this.buttonBatteryLevel.Image =R.DNA03_Batt_0;
                    break;
            }
        }
        protected void RefreshButtonMeasure()
        //rafraichit le bouton mesure avec la bonne image et enable ou disable les boutons + et -
        {
            //desactive le bouton moins si on est à 1 mesure
            if (Module._NumberOfMeasurement == 1)
            {
                this.buttonMoins.Enabled = false;
            }
            else
            {
                this.buttonMoins.Enabled = true;
            }
            //desactive le button plus si on a atteint 20 mesures
            if (Module._NumberOfMeasurement == 20)
            {
                this.buttonPlus.Enabled = false;
            }
            else
            {
                this.buttonPlus.Enabled = true;
            }
            //Change l'image en fonction du nombre de mesures choisis
            switch (Module._NumberOfMeasurement)
            {
                case 1:
                    this.buttonMeasure.Image =R.DNA03_Measure_1;
                    break;
                case 2:
                    this.buttonMeasure.Image =R.DNA03_Measure_2;
                    break;
                case 3:
                    this.buttonMeasure.Image =R.DNA03_Measure_3;
                    break;
                case 4:
                    this.buttonMeasure.Image =R.DNA03_Measure_4;
                    break;
                case 5:
                    this.buttonMeasure.Image =R.DNA03_Measure_5;
                    break;
                case 6:
                    this.buttonMeasure.Image =R.DNA03_Measure_6;
                    break;
                case 7:
                    this.buttonMeasure.Image =R.DNA03_Measure_7;
                    break;
                case 8:
                    this.buttonMeasure.Image =R.DNA03_Measure_8;
                    break;
                case 9:
                    this.buttonMeasure.Image =R.DNA03_Measure_9;
                    break;
                case 10:
                    this.buttonMeasure.Image =R.DNA03_Measure_10;
                    break;
                case 11:
                    this.buttonMeasure.Image =R.DNA03_Measure_11;
                    break;
                case 12:
                    this.buttonMeasure.Image =R.DNA03_Measure_12;
                    break;
                case 13:
                    this.buttonMeasure.Image =R.DNA03_Measure_13;
                    break;
                case 14:
                    this.buttonMeasure.Image =R.DNA03_Measure_14;
                    break;
                case 15:
                    this.buttonMeasure.Image =R.DNA03_Measure_15;
                    break;
                case 16:
                    this.buttonMeasure.Image =R.DNA03_Measure_16;
                    break;
                case 17:
                    this.buttonMeasure.Image =R.DNA03_Measure_17;
                    break;
                case 18:
                    this.buttonMeasure.Image =R.DNA03_Measure_18;
                    break;
                case 19:
                    this.buttonMeasure.Image =R.DNA03_Measure_19;
                    break;
                case 20:
                    this.buttonMeasure.Image =R.DNA03_Measure_20;
                    break;
                default:
                    this.buttonMeasure.Image =R.DNA03_Measure_3;
                    break;
            }
        }
        private void RefreshPowerButton()
        {
            if (base.Module.InstrumentIsOn)
            {
                this.buttonPower.Image = R.DNA03_Power_On;
            }
            else
            {
                this.buttonPower.Image = R.DNA03_Power_Off;
            }
        }
        /// <summary>
        /// n'autorise plus de déplacer le form, mais cause un clignotement de la fenêtre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ManualTiltMeterView_LocationChanged(object sender, EventArgs e)
        {
            this.LocationChanged -= this.ManualTiltMeterView_LocationChanged;
            Location = new Point(0, 0);
            this.LocationChanged += new System.EventHandler(this.ManualTiltMeterView_LocationChanged);
        }
        #endregion
        #region Timer
        private void timerDNA03_Tick(object sender, EventArgs e)
            //action du timer toutes les 5 sec : demande le niveau batterie
        {
            if (base.Module.InstrumentIsOn)
            {
                int battLevel = Module.GetIntBatteryLevel();
                if (battLevel != 0)
                {
                    this.RefreshBatteryLevelButton(battLevel);
                    buttonBatteryLevel.Visible = true;
                }
                else
                {
                    //pas de réponse du DNA -> fermeture connection
                    Module.Disconnect();
                    // Logs.Log.AddEntryAsError(this._Module, "Connection to Dna03 is lost!");
                    Logs.Log.AddEntryAsError(this._Module, R.T_CONNECTION_TO_DNA03_IS_LOST);
                    this.RefreshBatteryLevelButton();
                    this.ModeButtonsOff();
                }
            }
            this.RefreshPowerButton();
        }
        #endregion
        #region BackGroundWorker Connecting
        private void backgroundWorker_Connecting_DoWork(object sender, DoWorkEventArgs e)
            //Tâches à exécuter pour connecter le DNA 03 dans le backGroundworker
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
            if (base.Module.InstrumentIsOn)
            {
                toAddInLogbook.Success = true;
                toAddInLogbook.AccessToken = "Disconnection started...";
                //Déconnection si le DNA est déjà connecté
                worker.ReportProgress(10);
                // Perform a time consuming operation and report progress.
                Thread.Sleep(500);
                this.toAddInLogbook = Module.Disconnect();
                this.ChangeButtonPortCOMImage(Module.portName);
                this.RefreshPowerButton();
                this.RefreshBatteryLevelButton();
                this.RefreshButtonMeasure();
                this.ModeButtonsOff();
                worker.ReportProgress(100);
                // Perform a time consuming operation and report progress.
                Thread.Sleep(500);
                return;
            }
            else
            {
                //Essaye de se connecter sur le port choisi
                toAddInLogbook.Success = true;
                // toAddInLogbook.AccessToken = "Trying to connect Dna03 SN" + Dna03._SerialNumber + " on " + this.Dna03Module.portName + "...";
                toAddInLogbook.AccessToken = "Trying to connect " + this.Module.Instrument._Model + " SN" + this.Module.Instrument._SerialNumber + " on " + this.Module.portName + "...";
                worker.ReportProgress(10);
                // Perform a time consuming operation and report progress.
                Thread.Sleep(500);
                this.toAddInLogbook = Module.Connect();
                if (base.Module.InstrumentIsOn)
                {
                    //Annulation par l'utilisateur
                    if (worker.CancellationPending) { e.Cancel = true; return; }
                    //si connection réussie, worker progress = 50
                    worker.ReportProgress(50);
                    // Perform a time consuming operation and report progress.
                    Thread.Sleep(500);
                }
                else
                {
                    //Annulation par l'utilisateur
                    if (worker.CancellationPending) { e.Cancel = true; return; }
                    //Va essayer de se connecter sur les autres ports
                    worker.ReportProgress(20);
                    // Perform a time consuming operation and report progress.
                    Thread.Sleep(500);
                    toAddInLogbook.Success = true;
                    // toAddInLogbook.AccessToken = "Trying to connect on others ports...";
                    toAddInLogbook.AccessToken = R.T_TRYING_TO_CONNECT_ON_OTHERS_PORTS;
                    //Annulation par l'utilisateur
                    if (worker.CancellationPending) { e.Cancel = true; return; }
                    worker.ReportProgress(21);
                    // Perform a time consuming operation and report progress.
                    Thread.Sleep(500);
                    this.toAddInLogbook = this.Module.TryConnectOthersPorts();
                    //Annulation par l'utilisateur
                    if (worker.CancellationPending) { e.Cancel = true; return; }
                    worker.ReportProgress(50);
                    // Perform a time consuming operation and report progress.
                    Thread.Sleep(500);
                }
                if (base.Module.InstrumentIsOn)
                {
                    //si DNA est ON, on lance l'initialisation            
                    toAddInLogbook.Success = true;
                    toAddInLogbook.AccessToken = "Starting initialization...";
                    //Annulation par l'utilisateur
                    if (worker.CancellationPending) { e.Cancel = true; return; }
                    worker.ReportProgress(51);
                    // Perform a time consuming operation and report progress.
                    Thread.Sleep(500);
                    this.toAddInLogbook = Module.InitializeSensor();
                    //Annulation par l'utilisateur
                    if (worker.CancellationPending) { e.Cancel = true; return; }
                    worker.ReportProgress(80);
                    // Perform a time consuming operation and report progress.
                    Thread.Sleep(500);
                    int battLevel = Module.GetIntBatteryLevel();
                    toAddInLogbook.Success = true;
                    toAddInLogbook.AccessToken = "The Battery level is " + battLevel + "%";
                    this.RefreshBatteryLevelButton(battLevel);
                    this.ChangeButtonPortCOMImage(Module.portName);
                    this.RefreshPowerButton();
                    this.RefreshButtonMeasure();
                    this.ModeButtonsOn();
                    timerDNA03.Enabled = true;
                    //Annulation par l'utilisateur
                    if (worker.CancellationPending) { e.Cancel = true; return; }
                    worker.ReportProgress(100);
                    // Perform a time consuming operation and report progress.
                    Thread.Sleep(500);
                }
                else
                {
                    //Pas possible de faire l'inatialisation car DNA n'est pas ON
                    this.toAddInLogbook.ErrorMessage = "Impossible to connect Instrument SN "+ base.Module.Instrument._SerialNumber;
                    this.toAddInLogbook.Success = false;
                    this.ChangeButtonPortCOMImage(Module.portName);
                    this.RefreshPowerButton();
                    this.RefreshBatteryLevelButton();
                    this.RefreshButtonMeasure();
                    this.ModeButtonsOff();
                    worker.ReportProgress(100);
                    // Perform a time consuming operation and report progress.
                    Thread.Sleep(500);
                }
            }
        }

        private void backgroundWorker_Connecting_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
            //Fin du background worker de la connection au DNA, affiche de nouveau la button bar
        {
            if (e.Cancelled==true)
            {
                this.toAddInLogbook.Success = false;
                this.toAddInLogbook.ErrorMessage = "Canceled by user";
                Logs.Log.AddEntryAsResult(this._Module, toAddInLogbook);
                this.toAddInLogbook = Module.Disconnect();
                Logs.Log.AddEntryAsResult(this._Module, toAddInLogbook);
                this.ChangeButtonPortCOMImage(Module.portName);
                this.RefreshPowerButton();
                this.RefreshBatteryLevelButton();
                this.RefreshButtonMeasure();
                this.ModeButtonsOff();
            }
            buttonBar.Visible = true;
            picture_Connecting.Visible = false;
            buttonCancel.Visible = false;
            if (Module.measurementThread != null && Module.InstrumentIsOn) Module.measurementThread.Start();
        }
        private void backgroundWorker_Connecting_ProgressChanged(object sender, ProgressChangedEventArgs e)
        //progression du background worker connecting : met à jour le logbook
        {
            toAddInLogbook.ErrorMessage = e.ProgressPercentage.ToString() + "% " + this.toAddInLogbook.ErrorMessage;
            toAddInLogbook.AccessToken = e.ProgressPercentage.ToString() + "% " + this.toAddInLogbook.AccessToken;
            Logs.Log.AddEntryAsResult(this._Module, toAddInLogbook);
        }
        #endregion
        #region BackGroundWorker Measuring
        //Mesure statique
        public virtual void backgroundWorker_Measuring_DoWork(object sender, DoWorkEventArgs e)
            //Background worker pour mesure du DNA03
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
            measDone = false;
            if (base.Module.InstrumentIsOn)
            {
                if (Module.measurementThread != null)
                {
                    Module.PauseMeasurementThread();
                    Debug.WriteInConsole("MEASUREMENT THREAD STATE BEFORE MEASURE = " + Module.measurementThread.ThreadState);
                }
                toAddInLogbook.Success = true;
                toAddInLogbook.AccessToken = "Starting Measurement...";
                //Annulation par l'utilisateur
                if (worker.CancellationPending) { e.Cancel = true; return; }
                worker.ReportProgress(1);
                // Perform a time consuming operation and report progress.
                Thread.Sleep(500);

                double percentPerMeasure = 90 / Convert.ToDouble(Module._NumberOfMeasurement);
                List<MeasureOfLevel> listMeasure = new List<MeasureOfLevel>();
                for (int i = 1; i <= Module._NumberOfMeasurement; i++)
                {
                    MeasureOfLevel singleMeas = new MeasureOfLevel();
                    toAddInLogbook = Module.DoSingleMeasure(singleMeas);
                    listMeasure.Add(singleMeas);
                    // if (toAddInLogbook.Success == true) { toAddInLogbook.AccessToken = String.Format("Measurement {0} is successfull.", i); }
                    if (toAddInLogbook.Success == true) { toAddInLogbook.AccessToken = $"{R.T_MEASUREMENT} {i} {R.T_IS_SUCCESSFUL}."; }
                    //Annulation par l'utilisateur
                    if (worker.CancellationPending) { e.Cancel = true; return; }
                    worker.ReportProgress(Convert.ToInt32(i * percentPerMeasure));
                    // Perform a time consuming operation and report progress.
                    Thread.Sleep(500);
                    if (toAddInLogbook.Success==false)
                        //si ne peux pas prendre une mesure, arrête la boucle de mesure
                    {
                        //fin mesure quand problème survient -> message erreur
                        toAddInLogbook.Success = false;
                        toAddInLogbook.ErrorMessage = "Measurement cannot be done";
                        this.Module.JustMeasure._Date = DateTime.Now;
                        this.Module.JustMeasure._RawLevelReading = na;
                        this.Module.JustMeasure._Distance = na;
                        this.Module.JustMeasure._Compass = na;
                        this.Module.JustMeasure._EmqDistanceReading = na;
                        this.Module.JustMeasure._RawEmqLevelReading = na;
                        measDone = false;
                        // Perform a time consuming operation and report progress.
                        Thread.Sleep(500);
                        return;
                    }
                }
                toAddInLogbook = Module.CalculateAverageMeasure(listMeasure);
                    //Annulation par l'utilisateur
                    if (worker.CancellationPending) { e.Cancel = true; return; }
                    worker.ReportProgress(99);
                // Perform a time consuming operation and report progress.
                Thread.Sleep(500);
                toAddInLogbook.Success = true;
                measDone = true;
                TSU.Debug.WriteInConsole($"Compass: {Module.JustMeasure._Compass} Distance: {Module.JustMeasure._Distance} Height: {Module.JustMeasure._RawLevelReading}");
                toAddInLogbook.AccessToken = "Measurement is finished and successfull";
                    worker.ReportProgress(100);
                // Perform a time consuming operation and report progress.
                Thread.Sleep(500);
            }
        }

        private void backgroundWorker_Measuring_ProgressChanged(object sender, ProgressChangedEventArgs e)
            //progression du background worker measuring : met à jour le logbook
        {
            toAddInLogbook.ErrorMessage = e.ProgressPercentage.ToString() + "% " + this.toAddInLogbook.ErrorMessage;
            toAddInLogbook.AccessToken = e.ProgressPercentage.ToString() + "% " + this.toAddInLogbook.AccessToken;
            Logs.Log.AddEntryAsResult(this._Module, toAddInLogbook);
        }

        private void backgroundWorker_Measuring_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
            //fin du background worker mesure DNA, affiche de nouveau le button bar
        {
            //Annulation par l'utilisateur
            if (e.Cancelled == true)
            {
                this.toAddInLogbook.Success = false;
                this.toAddInLogbook.ErrorMessage = "Measurement canceled by user";
                Logs.Log.AddEntryAsResult(this._Module, toAddInLogbook);
            }
            else
            {
                //envoi de la mesure à tous les observateurs
                if (measDone) this.Module.OnNext();
                else
                {
                    new MessageInput(MessageType.Warning, R.StringDNA03_MeasureFailed)
                    {
                        ButtonTexts = new List<string> { R.T_OK + "!" }
                    }.Show();
                }
            }
            this.timerDNA03.Enabled = true;
            this.picture_Connecting.Visible = false;
            buttonCancel.Visible = false;
            if (Module.measurementThread != null)
            {
                Module.ResumeMeasurementThread();
                Debug.WriteInConsole("MEASUREMENT THREAD STATE = " + Module.measurementThread.ThreadState);
            }
        }
        #endregion
        #region BackgroundWorker Initialization
        //Initialisation
        private void backgroundWorker_Initialization_DoWork(object sender, DoWorkEventArgs e)
            //Lancement du backGround worker Initilization lorsqu'on clique sur bouton init
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = false;
            if (base.Module.InstrumentIsOn)
            {
                this.toAddInLogbook.Success = true;
                this.toAddInLogbook.AccessToken = "Initialization...";
                worker.ReportProgress(1);
                // Perform a time consuming operation and report progress.
                Thread.Sleep(500);
                this.toAddInLogbook = Module.InitializeSensor();
                if (toAddInLogbook.Success == false)
                {
                    //Deconnection du DNA03 si initialisation failed
                    worker.ReportProgress(80);
                    // Perform a time consuming operation and report progress.
                    Thread.Sleep(500);
                    this.toAddInLogbook.Success = true;
                    this.toAddInLogbook.AccessToken = "Disconnection of the instrument...";
                    worker.ReportProgress(81);
                    // Perform a time consuming operation and report progress.
                    Thread.Sleep(500);
                    this.toAddInLogbook = Module.Disconnect();
                    worker.ReportProgress(99);
                    // Perform a time consuming operation and report progress.
                    Thread.Sleep(500);
                    this.toAddInLogbook.Success = false;
                    this.toAddInLogbook.AccessToken = "Error during initilization, instrument has been disconnected";
                    worker.ReportProgress(99);
                    // Perform a time consuming operation and report progress.
                    Thread.Sleep(500);

                }
                else
                    //initilisation réussie
                {
                    this.toAddInLogbook.Success = true;
                    this.toAddInLogbook.AccessToken = "Initialization of the instrument is finished and successfull.";
                    worker.ReportProgress(99);
                    // Perform a time consuming operation and report progress.
                    Thread.Sleep(500);
                }
            }
        }

        private void backgroundWorker_Initialization_ProgressChanged(object sender, ProgressChangedEventArgs e)
            //Mise à jour du logbook avec la progression de l'initialisation
        {
            toAddInLogbook.ErrorMessage = e.ProgressPercentage.ToString() + "% " + this.toAddInLogbook.ErrorMessage;
            toAddInLogbook.AccessToken = e.ProgressPercentage.ToString() + "% " + this.toAddInLogbook.AccessToken;
            Logs.Log.AddEntryAsResult(this._Module, toAddInLogbook);
        }

        private void backgroundWorker_Initialization_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
            //fin du backGroundWorker d'initialisation
        {
            if (base.Module.InstrumentIsOn==false)
            {
                //Echec durant l'initialisation DNA03 est OFF
                this.RefreshBatteryLevelButton();
                this.ModeButtonsOff();
            }
            else
            {
                //initilisation réussie
                this.timerDNA03.Enabled = true;
                this.picture_Connecting.Visible = false;
                buttonCancel.Visible = false;  
            }
        }
        #endregion
        #region BackgroundWorker Loop
        //Mesures continues
        private void backgroundWorker_Loop_DoWork(object sender, DoWorkEventArgs e)
            //Lancement des mesures continues dans le background owrker
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
            if (base.Module.InstrumentIsOn)
            {
                //indique que ce sont des mesures continues au station level module view
                this.Module.JustMeasure._ContinuousMeasurement = true;
                toAddInLogbook.Success = true;
                toAddInLogbook.AccessToken = "Starting continuous measurements...";
                //Annulation par l'utilisateur
                if (worker.CancellationPending) { e.Cancel = true; return; }
                worker.ReportProgress(1);
                // Perform a time consuming operation and report progress.
                Thread.Sleep(500);
                int numberOfMeasureDone = 1;
                while (worker.CancellationPending == false)
                {
                    MeasureOfLevel singleMeas = new MeasureOfLevel();
                    //indique que ce sont des mesures continues pour le station level module view
                    singleMeas._ContinuousMeasurement = true;
                    toAddInLogbook = Module.DoSingleMeasure(singleMeas);
                    if (toAddInLogbook.Success == true)
                    {
                        //    toAddInLogbook.AccessToken = String.Format("Measurement {0} is successfull.", numberOfMeasureDone);
                        toAddInLogbook.AccessToken = $"{R.T_MEASUREMENT} {numberOfMeasureDone} {R.T_IS_SUCCESSFUL}.";
                        this.Module.JustMeasure = singleMeas;
                        worker.ReportProgress(1);
                        // Perform a time consuming operation and report progress.
                        Thread.Sleep(500);
                        numberOfMeasureDone++;
                    }
                    else
                    {
                        //   toAddInLogbook.ErrorMessage = String.Format("Measurement just failed");
                        toAddInLogbook.ErrorMessage = $"{R.T_MEASUREMENT_JUST_FAILED}";
                        worker.ReportProgress(1);
                        // Perform a time consuming operation and report progress.
                        Thread.Sleep(500);
                    }
                }


                //worker.ReportProgress(99);
                //// Perform a time consuming operation and report progress.
                //System.Threading.Thread.Sleep(500);
            }
        }

        private void backgroundWorker_Loop_ProgressChanged(object sender, ProgressChangedEventArgs e)
            //progression des mesures continues pour rapport dans le logbook
        {
            Logs.Log.AddEntryAsResult(this._Module, toAddInLogbook);
            //si mesure réussie, envoi de la mesure aux observateurs
            if (toAddInLogbook.Success==true)
            {
                this.Module.OnNext();
            } 
        }

        private void backgroundWorker_Loop_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
            //fin des mesures continues par annulation de l'utilisateur
        {
            //Annulation par l'utilisateur
            //indique que c'est la fin des mesures continues au station level module view
            this.Module.JustMeasure._ContinuousMeasurement = false;
            //envoi de la mesure aux observateurs
            this.Module.OnNext();
            this.toAddInLogbook.Success = false;
            this.toAddInLogbook.ErrorMessage = "Continuous measurements canceled by user and finished";
            Logs.Log.AddEntryAsResult(this._Module, toAddInLogbook);
            this.timerDNA03.Enabled = true;
            this.picture_Connecting.Visible = false;
            buttonCancel.Visible = false;
        }
        #endregion

        private void label_PointName_Click(object sender, EventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
                label_PointName.Text = this.Module.GeTSUbscriberNames();
        }
        #region tooltips
        /// <summary>
        /// Add the tooltips to the buttons
        /// </summary>
        internal override void SetAllToolTips()
        {
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            toolTip1.SetToolTip(this.buttonCancel, R.StringDNA03_ToolTip_Cancel);
            toolTip1.SetToolTip(this.label_PointName, R.StringDNA03_ToolTip_PointName);
            toolTip1.SetToolTip(this.panelLog, R.StringDNA03_ToolTip_PanelLog);
            this.buttonBatteryLevel.ToolTipText = R.StringDNA03_ToolTip_Battery;
            this.buttonInit.ToolTipText = R.StringDNA03_ToolTip_Init;
            this.buttonLoop.ToolTipText = R.StringDNA03_ToolTip_Loop;
            this.buttonMeasure.ToolTipText = R.StringDNA03_ToolTip_Measure;
            this.buttonMoins.ToolTipText = R.StringDNA03_ToolTip_Moins;
            this.buttonPlus.ToolTipText = R.StringDNA03_ToolTip_Plus;
            this.buttonPortCom.ToolTipText = R.StringDNA03_ToolTip_PortCom;
            this.buttonPower.ToolTipText = R.StringDNA03_ToolTip_Power;
        }
        #endregion


        #region BackgroundWorker Compass
        //Mesures continues
        private void backgroundWorker_Compass_DoWork(object sender, DoWorkEventArgs e)
        //Lancement des mesures continues dans le background owrker
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
            if (base.Module.InstrumentIsOn)
            {
                if (Module.measurementThread != null)
                {
                    Module.PauseMeasurementThread();
                    Debug.WriteInConsole("MEASUREMENT THREAD STATE BEFORE MEASURE = " + Module.measurementThread.ThreadState);
                }
                //indique que ce sont des mesures continues au station level module view
                //this.Module.JustMeasure._ContinuousMeasurement = true;
                toAddInLogbook.Success = true;
                toAddInLogbook.AccessToken = "Starting compass measurements...";
                //Annulation par l'utilisateur
                if (worker.CancellationPending) { e.Cancel = true; return; }
                worker.ReportProgress(1);
                // Perform a time consuming operation and report progress.
                Thread.Sleep(500);

                MeasureOfLevel singleMeas = new MeasureOfLevel();
                //indique que ce sont des mesures continues pour le station level module view
                //singleMeas._ContinuousMeasurement = true;
                toAddInLogbook = Module.GetCompassValue();
                if (toAddInLogbook.Success == true)
                {
                    toAddInLogbook.AccessToken = $"Compass Value read and retrieved successfully.";
                    worker.ReportProgress(1);
                    // Perform a time consuming operation and report progress.
                    Thread.Sleep(500);
                }
                else
                {
                    //   toAddInLogbook.ErrorMessage = String.Format("Measurement just failed");
                    toAddInLogbook.ErrorMessage = $"{R.T_MEASUREMENT_JUST_FAILED}";
                    worker.ReportProgress(1);
                    // Perform a time consuming operation and report progress.
                    Thread.Sleep(500);
                }

                //worker.ReportProgress(99);
                //// Perform a time consuming operation and report progress.
                //System.Threading.Thread.Sleep(500);
            }
        }

        private void backgroundWorker_Compass_ProgressChanged(object sender, ProgressChangedEventArgs e)
        //progression des mesures continues pour rapport dans le logbook
        {
            Logs.Log.AddEntryAsResult(this._Module, toAddInLogbook);
            //si mesure réussie, envoi de la mesure aux observateurs
            if (toAddInLogbook.Success == true)
            {
                this.Module.OnNext();
            }
        }

        private void backgroundWorker_Compass_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        //fin des mesures continues par annulation de l'utilisateur
        {
            //Annulation par l'utilisateur
            //indique que c'est la fin des mesures continues au station level module view
            this.Module.JustMeasure._ContinuousMeasurement = false;
            //envoi de la mesure aux observateurs
            this.Module.OnNext();
            this.toAddInLogbook.Success = false;
            this.toAddInLogbook.ErrorMessage = "Compass measurement canceled by user and finished";
            Logs.Log.AddEntryAsResult(this._Module, toAddInLogbook);
            this.timerDNA03.Enabled = true;
            this.picture_Connecting.Visible = false;
            buttonCancel.Visible = false;
            if (Module.measurementThread != null)
            {
                Module.ResumeMeasurementThread();
                Debug.WriteInConsole("MEASUREMENT THREAD STATE = " + Module.measurementThread.ThreadState);
            }
        }
        #endregion
    }
}
