﻿using R = TSU.Properties.Resources;
namespace TSU.Common.Instruments.Device.DNA03
{
    partial class View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (this.Module == null)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        protected void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View));
            this.buttonBar = new System.Windows.Forms.ToolStrip();
            this.buttonMoins = new System.Windows.Forms.ToolStripButton();
            this.buttonMeasure = new System.Windows.Forms.ToolStripButton();
            this.buttonPlus = new System.Windows.Forms.ToolStripButton();
            this.buttonLoop = new System.Windows.Forms.ToolStripButton();
            this.buttonCompass = new System.Windows.Forms.ToolStripButton();
            this.buttonPower = new System.Windows.Forms.ToolStripButton();
            this.buttonPortCom = new System.Windows.Forms.ToolStripButton();
            this.buttonInit = new System.Windows.Forms.ToolStripButton();
            this.buttonBatteryLevel = new System.Windows.Forms.ToolStripButton();
            this.panelLog = new System.Windows.Forms.Panel();
            this.timerDNA03 = new System.Windows.Forms.Timer(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.picture_Connecting = new System.Windows.Forms.PictureBox();
            this.splitContainer_margin = new System.Windows.Forms.SplitContainer();
            this.timerImageConnecting = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorker_Connecting = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker_Measuring = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker_Initialization = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker_Loop = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker_Compass = new System.ComponentModel.BackgroundWorker();
            this.splitContainer_All = new System.Windows.Forms.SplitContainer();
            this.label_PointName = new System.Windows.Forms.Label();
            this.buttonBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture_Connecting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_margin)).BeginInit();
            this.splitContainer_margin.Panel1.SuspendLayout();
            this.splitContainer_margin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_All)).BeginInit();
            this.splitContainer_All.Panel1.SuspendLayout();
            this.splitContainer_All.Panel2.SuspendLayout();
            this.splitContainer_All.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonBar
            // 
            this.buttonBar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonBar.BackgroundImage")));
            this.buttonBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonBar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.buttonBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonMoins,
            this.buttonMeasure,
            this.buttonPlus,
            this.buttonLoop,
            this.buttonCompass,
            this.buttonPower,
            this.buttonPortCom,
            this.buttonInit,
            this.buttonBatteryLevel});
            this.buttonBar.Location = new System.Drawing.Point(0, 0);
            this.buttonBar.MaximumSize = new System.Drawing.Size(0, 70);
            this.buttonBar.MinimumSize = new System.Drawing.Size(490, 70);
            this.buttonBar.Name = "buttonBar";
            this.buttonBar.Padding = new System.Windows.Forms.Padding(1);
            this.buttonBar.Size = new System.Drawing.Size(921, 70);
            this.buttonBar.TabIndex = 12;
            // 
            // buttonMoins
            // 
            this.buttonMoins.AutoSize = false;
            this.buttonMoins.AutoToolTip = false;
            this.buttonMoins.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonMoins.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonMoins.Image = ((System.Drawing.Image)(resources.GetObject("buttonMoins.Image")));
            this.buttonMoins.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.buttonMoins.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
            this.buttonMoins.Name = "buttonMoins";
            this.buttonMoins.Size = new System.Drawing.Size(60, 60);
            this.buttonMoins.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            //  this.buttonMoins.ToolTipText = "Decrease number of measures";
            this.buttonMoins.ToolTipText = R.T_DECREASE_NUMBER_OF_MEASURES;
            this.buttonMoins.Click += new System.EventHandler(this.buttonMoins_Click);
            // 
            // buttonMeasure
            // 
            this.buttonMeasure.AutoSize = false;
            this.buttonMeasure.AutoToolTip = false;
            this.buttonMeasure.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonMeasure.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonMeasure.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonMeasure.Image = ((System.Drawing.Image)(resources.GetObject("buttonMeasure.Image")));
            this.buttonMeasure.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.buttonMeasure.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
            this.buttonMeasure.Name = "buttonMeasure";
            this.buttonMeasure.Size = new System.Drawing.Size(60, 60);
            this.buttonMeasure.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonMeasure.Click += new System.EventHandler(this.buttonMeasure_Click);
            // 
            // buttonPlus
            // 
            this.buttonPlus.AutoSize = false;
            this.buttonPlus.AutoToolTip = false;
            this.buttonPlus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonPlus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonPlus.Image = ((System.Drawing.Image)(resources.GetObject("buttonPlus.Image")));
            this.buttonPlus.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.buttonPlus.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
            this.buttonPlus.Name = "buttonPlus";
            this.buttonPlus.Size = new System.Drawing.Size(60, 60);
            this.buttonPlus.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonPlus.ToolTipText = "Increase number of measures";
            this.buttonPlus.Click += new System.EventHandler(this.buttonPlus_Click);
            // 
            // buttonLoop
            // 
            this.buttonLoop.AutoSize = false;
            this.buttonLoop.AutoToolTip = false;
            this.buttonLoop.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonLoop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonLoop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonLoop.Image = ((System.Drawing.Image)(resources.GetObject("buttonLoop.Image")));
            this.buttonLoop.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.buttonLoop.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
            this.buttonLoop.Name = "buttonLoop";
            this.buttonLoop.Size = new System.Drawing.Size(60, 60);
            this.buttonLoop.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonLoop.Click += new System.EventHandler(this.buttonLoop_Click);
            // 
            // buttonCompass
            // 
            this.buttonCompass.AutoSize = false;
            this.buttonCompass.AutoToolTip = false;
            this.buttonCompass.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonCompass.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonCompass.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonCompass.Image = ((System.Drawing.Image)(resources.GetObject("buttonMeasure.Image")));
            this.buttonCompass.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.buttonCompass.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
            this.buttonCompass.Name = "buttonCompass";
            this.buttonCompass.Size = new System.Drawing.Size(60, 60);
            this.buttonCompass.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonCompass.Click += new System.EventHandler(this.buttonCompass_Click);
            // 
            // buttonPower
            // 
            this.buttonPower.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.buttonPower.AutoSize = false;
            this.buttonPower.AutoToolTip = false;
            this.buttonPower.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonPower.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonPower.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonPower.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.buttonPower.Image = ((System.Drawing.Image)(resources.GetObject("buttonPower.Image")));
            this.buttonPower.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.buttonPower.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
            this.buttonPower.Name = "buttonPower";
            this.buttonPower.Size = new System.Drawing.Size(60, 60);
            this.buttonPower.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonPower.ToolTipText = "Power";
            this.buttonPower.Click += new System.EventHandler(this.buttonPower_Click);
            // 
            // buttonPortCom
            // 
            this.buttonPortCom.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.buttonPortCom.AutoSize = false;
            this.buttonPortCom.AutoToolTip = false;
            this.buttonPortCom.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonPortCom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonPortCom.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonPortCom.Image = ((System.Drawing.Image)(resources.GetObject("buttonPortCom.Image")));
            this.buttonPortCom.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.buttonPortCom.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
            this.buttonPortCom.Name = "buttonPortCom";
            this.buttonPortCom.Size = new System.Drawing.Size(60, 60);
            this.buttonPortCom.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonPortCom.Click += new System.EventHandler(this.buttonPortCom_Click);
            // 
            // buttonInit
            // 
            this.buttonInit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.buttonInit.AutoSize = false;
            this.buttonInit.AutoToolTip = false;
            this.buttonInit.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonInit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonInit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonInit.Image = ((System.Drawing.Image)(resources.GetObject("buttonInit.Image")));
            this.buttonInit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.buttonInit.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
            this.buttonInit.Name = "buttonInit";
            this.buttonInit.Size = new System.Drawing.Size(60, 60);
            this.buttonInit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonInit.Click += new System.EventHandler(this.buttonInitialisation_Click);
            // 
            // buttonBatteryLevel
            // 
            this.buttonBatteryLevel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.buttonBatteryLevel.AutoSize = false;
            this.buttonBatteryLevel.AutoToolTip = false;
            this.buttonBatteryLevel.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.buttonBatteryLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonBatteryLevel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonBatteryLevel.Image = ((System.Drawing.Image)(resources.GetObject("buttonBatteryLevel.Image")));
            this.buttonBatteryLevel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.buttonBatteryLevel.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Highlight;
            this.buttonBatteryLevel.Name = "buttonBatteryLevel";
            this.buttonBatteryLevel.Size = new System.Drawing.Size(60, 60);
            this.buttonBatteryLevel.Click += new System.EventHandler(this.buttonBatteryLevel_Click);
            // 
            // panelLog
            // 
            this.panelLog.AutoSize = true;
            this.panelLog.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelLog.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.DarkBackground;
            this.panelLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLog.Location = new System.Drawing.Point(0, 0);
            this.panelLog.Name = "panelLog";
            this.panelLog.Size = new System.Drawing.Size(921, 99);
            this.panelLog.TabIndex = 10;
            // 
            // timerDNA03
            // 
            this.timerDNA03.Interval = 5000;
            this.timerDNA03.Tick += new System.EventHandler(this.timerDNA03_Tick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(10);
            this.splitContainer1.MinimumSize = new System.Drawing.Size(0, 70);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panelLog);
            this.splitContainer1.Panel1MinSize = 0;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.buttonCancel);
            this.splitContainer1.Panel2.Controls.Add(this.picture_Connecting);
            this.splitContainer1.Panel2.Controls.Add(this.buttonBar);
            this.splitContainer1.Panel2MinSize = 70;
            this.splitContainer1.Size = new System.Drawing.Size(921, 173);
            this.splitContainer1.SplitterDistance = 99;
            this.splitContainer1.TabIndex = 14;
            // 
            // buttonCancel
            // 
            this.buttonCancel.FlatAppearance.BorderSize = 0;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Image = ((System.Drawing.Image)(resources.GetObject("buttonCancel.Image")));
            this.buttonCancel.Location = new System.Drawing.Point(4, 4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(60, 60);
            this.buttonCancel.TabIndex = 13;
            this.buttonCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Visible = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // picture_Connecting
            // 
            this.picture_Connecting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picture_Connecting.Image = ((System.Drawing.Image)(resources.GetObject("picture_Connecting.Image")));
            this.picture_Connecting.Location = new System.Drawing.Point(0, 0);
            this.picture_Connecting.Name = "picture_Connecting";
            this.picture_Connecting.Size = new System.Drawing.Size(921, 70);
            this.picture_Connecting.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picture_Connecting.TabIndex = 11;
            this.picture_Connecting.TabStop = false;
            this.picture_Connecting.Visible = false;
            // 
            // splitContainer_margin
            // 
            this.splitContainer_margin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer_margin.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer_margin.IsSplitterFixed = true;
            this.splitContainer_margin.Location = new System.Drawing.Point(0, 0);
            this.splitContainer_margin.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer_margin.Name = "splitContainer_margin";
            this.splitContainer_margin.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer_margin.Panel1
            // 
            this.splitContainer_margin.Panel1.Controls.Add(this.splitContainer1);
            this.splitContainer_margin.Panel1MinSize = 70;
            this.splitContainer_margin.Panel2MinSize = 10;
            this.splitContainer_margin.Size = new System.Drawing.Size(921, 314);
            this.splitContainer_margin.SplitterDistance = 173;
            this.splitContainer_margin.SplitterWidth = 20;
            this.splitContainer_margin.TabIndex = 15;
            // 
            // timerImageConnecting
            // 
            this.timerImageConnecting.Interval = 1;
            // 
            // backgroundWorker_Connecting
            // 
            this.backgroundWorker_Connecting.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_Connecting_DoWork);
            this.backgroundWorker_Connecting.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_Connecting_ProgressChanged);
            this.backgroundWorker_Connecting.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_Connecting_RunWorkerCompleted);
            // 
            // backgroundWorker_Measuring
            // 
            this.backgroundWorker_Measuring.WorkerReportsProgress = true;
            this.backgroundWorker_Measuring.WorkerSupportsCancellation = true;
            this.backgroundWorker_Measuring.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_Measuring_DoWork);
            this.backgroundWorker_Measuring.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_Measuring_ProgressChanged);
            this.backgroundWorker_Measuring.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_Measuring_RunWorkerCompleted);
            // 
            // backgroundWorker_Initialization
            // 
            this.backgroundWorker_Initialization.WorkerReportsProgress = true;
            this.backgroundWorker_Initialization.WorkerSupportsCancellation = true;
            this.backgroundWorker_Initialization.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_Initialization_DoWork);
            this.backgroundWorker_Initialization.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_Initialization_ProgressChanged);
            this.backgroundWorker_Initialization.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_Initialization_RunWorkerCompleted);
            // 
            // backgroundWorker_Loop
            // 
            this.backgroundWorker_Loop.WorkerReportsProgress = true;
            this.backgroundWorker_Loop.WorkerSupportsCancellation = true;
            this.backgroundWorker_Loop.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_Loop_DoWork);
            this.backgroundWorker_Loop.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_Loop_ProgressChanged);
            this.backgroundWorker_Loop.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_Loop_RunWorkerCompleted);
            // 
            // backgroundWorker_Compass
            // 
            this.backgroundWorker_Compass.WorkerReportsProgress = true;
            this.backgroundWorker_Compass.WorkerSupportsCancellation = true;
            this.backgroundWorker_Compass.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_Compass_DoWork);
            this.backgroundWorker_Compass.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_Compass_ProgressChanged);
            this.backgroundWorker_Compass.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_Compass_RunWorkerCompleted);
            // 
            // splitContainer_All
            // 
            this.splitContainer_All.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer_All.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer_All.IsSplitterFixed = true;
            this.splitContainer_All.Location = new System.Drawing.Point(5, 5);
            this.splitContainer_All.MinimumSize = new System.Drawing.Size(0, 200);
            this.splitContainer_All.Name = "splitContainer_All";
            this.splitContainer_All.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer_All.Panel1
            // 
            this.splitContainer_All.Panel1.Controls.Add(this.label_PointName);
            this.splitContainer_All.Panel1MinSize = 38;
            // 
            // splitContainer_All.Panel2
            // 
            this.splitContainer_All.Panel2.Controls.Add(this.splitContainer_margin);
            this.splitContainer_All.Panel2MinSize = 140;
            this.splitContainer_All.Size = new System.Drawing.Size(921, 353);
            this.splitContainer_All.SplitterDistance = 38;
            this.splitContainer_All.SplitterWidth = 20;
            this.splitContainer_All.TabIndex = 16;
            // 
            // label_PointName
            // 
            this.label_PointName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_PointName.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_PointName.Location = new System.Drawing.Point(0, 0);
            this.label_PointName.Name = "label_PointName";
            this.label_PointName.Size = new System.Drawing.Size(921, 38);
            this.label_PointName.TabIndex = 0;
            this.label_PointName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_PointName.Click += new System.EventHandler(this.label_PointName_Click);
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.ClientSize = new System.Drawing.Size(931, 363);
            this.Controls.Add(this.splitContainer_All);
            this.MinimumSize = new System.Drawing.Size(400, 160);
            this.Name = "View";
            this.ShowIcon = false;
            this.Text = "DNA 03";
            this.LocationChanged += new System.EventHandler(this.ManualTiltMeterView_LocationChanged);
            this.buttonBar.ResumeLayout(false);
            this.buttonBar.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picture_Connecting)).EndInit();
            this.splitContainer_margin.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_margin)).EndInit();
            this.splitContainer_margin.ResumeLayout(false);
            this.splitContainer_All.Panel1.ResumeLayout(false);
            this.splitContainer_All.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_All)).EndInit();
            this.splitContainer_All.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ToolStrip buttonBar;
        public System.Windows.Forms.ToolStripButton buttonBatteryLevel;
        public System.Windows.Forms.ToolStripButton buttonPower;
        public System.Windows.Forms.ToolStripButton buttonInit;
        public System.Windows.Forms.ToolStripButton buttonMeasure;
        public System.Windows.Forms.ToolStripButton buttonLoop;
        public System.Windows.Forms.ToolStripButton buttonCompass;
        protected System.Windows.Forms.Panel panelLog;
        public System.Windows.Forms.Timer timerDNA03;
        private System.Windows.Forms.SplitContainer splitContainer1;
        protected System.Windows.Forms.ToolStripButton buttonPortCom;
        protected System.Windows.Forms.SplitContainer splitContainer_margin;
        protected System.Windows.Forms.PictureBox picture_Connecting;
        protected System.Windows.Forms.Timer timerImageConnecting;
        public System.ComponentModel.BackgroundWorker backgroundWorker_Connecting;
        private System.Windows.Forms.ToolStripButton buttonMoins;
        private System.Windows.Forms.ToolStripButton buttonPlus;
        public System.ComponentModel.BackgroundWorker backgroundWorker_Measuring;
        public System.Windows.Forms.Button buttonCancel;
        public System.ComponentModel.BackgroundWorker backgroundWorker_Initialization;
        private System.ComponentModel.BackgroundWorker backgroundWorker_Loop;
        private System.ComponentModel.BackgroundWorker backgroundWorker_Compass;
        private System.Windows.Forms.SplitContainer splitContainer_All;
        public System.Windows.Forms.Label label_PointName;
    }
}

