﻿using R = TSU.Properties.Resources;


namespace TSU.Common.Instruments.Device.TS60
{
    public partial class SubView : TSU.Views.TsuView
    {
        public SubView()
        {
            InitializeComponent();
        }

        public SubView(TS60.View parentView)
        {
            this._Module = parentView._Module;
            InitializeComponent();
            this.ApplyThemeColors();
        }

        private void ApplyThemeColors()
        {
            this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
        }

        #region refs

        internal TS60.View TS60View
        {
            get
            {
                return (this.ParentView as TS60.View);
            }
        }

        internal TS60.Module TS60Module
        {
            get
            {
                return (this.ParentView._Module as TS60.Module);
            }
        }

        #endregion

        internal virtual void Start()
        {
            if (this.TS60View.IsOn)
                this.TS60View.SetSubView(this);
        }

        internal virtual void Stop()
        {
            //  Logs.Log.AddEntryAsFinishOf(this._Module, string.Format("{0} Stopped",this._Name));
            Logs.Log.AddEntryAsFinishOf(this._Module, $"{this._Name} {R.T_STOPPED}");
        }
            
        
    }
}
