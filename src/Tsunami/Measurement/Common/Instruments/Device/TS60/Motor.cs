﻿using System;
using TSU.ENUM;
using GeoComClient;
using System.Threading;
using System.Linq;

namespace TSU.Common.Instruments.Device.TS60
{
    #region Motor
    internal class Motor : IDisposable
    {
        public Module Module { get; private set; }

        public double SpeedH { get; private set; }

        public double SpeedV { get; private set; }

        public movingDirection Moving { get; private set; }

        public Motor(Module ts60)
        {
            Module = ts60;
        }

        /// <summary>
        /// use "Left", Right, Up or Down to move, use it several time to increase speed
        /// </summary>
        /// <param name="direction"></param>
        internal void MoveByDirection(movingDirection direction)
        {
            try
            {
                switch (direction)
                {
                    case movingDirection.Up:
                        if (Moving == movingDirection.None) { Moving = movingDirection.Up; }
                        if (Moving == movingDirection.Up) { Move(Moving); } else { StopMoving(); }
                        break;
                    case movingDirection.Down:
                        if (Moving == movingDirection.None) { Moving = movingDirection.Down; }
                        if (Moving == movingDirection.Down) { Move(Moving); } else { StopMoving(); }
                        break;
                    case movingDirection.Left:
                        if (Moving == movingDirection.None) { Moving = movingDirection.Left; }
                        if (Moving == movingDirection.Left) { Move(Moving); } else { StopMoving(); }
                        break;
                    case movingDirection.Right:
                        if (Moving == movingDirection.None) { Moving = movingDirection.Right; }
                        if (Moving == movingDirection.Right) { Move(Moving); } else { StopMoving(); }
                        break;
                    default:
                        StopMoving();
                        break;
                }
            }

            catch
            {
                // ignore
            }
        }

        public static movingDirection GetDirectionFromText(string arrowKeyText)
        {
            switch (arrowKeyText.ToUpper())
            {
                case "UP": return movingDirection.Up;
                case "DOWN": return movingDirection.Down;
                case "LEFT": return movingDirection.Left;
                case "RIGHT": return movingDirection.Right;
                default: return movingDirection.None;
            }
        }

        internal void StopMoving()
        {
            Debug.WriteInConsole("TS60-StopMove");
            Module.Ts60._Geocom.MOT_StopController(GeoComTypes.MOT_STOPMODE.MOT_NORMAL);
            Module.Ts60._Geocom.MOT_StartController(GeoComTypes.MOT_MODE.MOT_MANUPOS);
            SpeedH = 0;
            SpeedV = 0;
            Moving = movingDirection.None;            
        }

        internal void StopIfMoving()
        {
            if (Moving != movingDirection.None)
                StopMoving();
        }

        /// <summary>
        /// jog by direction optionnnaly during a period of time in millis and with a max speed 0--100
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="durationInMillis"></param>
        /// <param name="maxSpeed"></param>
        internal void Move(movingDirection direction, int durationInMillis = 0, double maxSpeed = 0.79, double step = 0.01)
        {            
            Module.StopTrackTarget();

            double lDeltaSpeedH = 0;
            double lDeltaSpeedV = 0;

            Moving = direction;

            switch (direction)
            {
                case movingDirection.Right:
                    if (SpeedH >= 0d)
                    {
                        lDeltaSpeedH = step;
                    }
                    else
                    {
                        lDeltaSpeedH = -SpeedH;
                    }
                    break;
                case movingDirection.Left:
                    if (SpeedH <= 0d)
                    {
                        lDeltaSpeedH = -step;
                    }
                    else
                    {
                        lDeltaSpeedH = -SpeedH;
                    }
                    break;
                case movingDirection.Up:
                    if (SpeedV <= 0d)
                    {
                        lDeltaSpeedV = -step;
                    }
                    else
                    {
                        lDeltaSpeedV = -SpeedV;
                    }
                    break;
                case movingDirection.Down:
                    if (SpeedV >= 0d)
                    {
                        lDeltaSpeedV = step;
                    }
                    else
                    {
                        lDeltaSpeedV = -SpeedV;
                    }
                    break;
                default:
                    lDeltaSpeedH = -SpeedH;
                    lDeltaSpeedV = -SpeedV;
                    break;
            }

            // In face 2, invert vAngle only
            if (Module.GetFace() == FaceType.Face2)
                lDeltaSpeedV = -lDeltaSpeedV;

            SpeedH = StayInARange(SpeedH + lDeltaSpeedH, maxSpeed, -maxSpeed);
            SpeedV = StayInARange(SpeedV + lDeltaSpeedV, maxSpeed, -maxSpeed);

            Debug.WriteInConsole("TS60-MoveHV");
            if (SpeedH != 0)
                Logs.Log.AddEntryAsBeginningOf(Module, $"Jog H = {SpeedH}/0.79");
            if (SpeedV != 0)
                Logs.Log.AddEntryAsBeginningOf(Module, $"Jog V = {SpeedV}/0.79");
            SendMoveOrder();

            if (durationInMillis > 0) StopMovingIn(durationInMillis);
        }

        private void SendMoveOrder()
        {
            int retCode = Module.Ts60._Geocom.MOT_StartController(GeoComTypes.MOT_MODE.MOT_OCONST);
            if (retCode != GeoComTypes.GRC_OK)
            {
                string errorMessage = Module.ErrorCode(retCode);
                throw new Exception($"Moving has failed, error in MOT_StartController: {errorMessage}");
            }

            double[] doubleArray = new double[] { SpeedH, SpeedV };
            GeoComTypes.MOT_COM_PAIR refOmega = new GeoComTypes.MOT_COM_PAIR(doubleArray);
            retCode = Module.Ts60._Geocom.MOT_SetVelocity(refOmega);
            if (retCode != GeoComTypes.GRC_OK)
            {
                string errorMessage = Module.ErrorCode(retCode);
                throw new Exception($"Moving has failed, error in MOT_SetVelocity: {errorMessage}");
            }
        }

        Timer movingTimer;

        private void StopMovingIn(int durationInMills)
        {
            //Stop the timer (if it was on)
            movingTimer?.Dispose();
            //Make a new Timer
            movingTimer = new Timer(
                new TimerCallback(TimeToStopMoving),
                null,
                durationInMills,
                Timeout.Infinite);
        }

        private void TimeToStopMoving(object sender)
        {
            movingTimer?.Dispose();
            StopMoving();
        }

        /// <summary>
        /// Move by an angle
        /// </summary>
        /// <param name="hzAngle">Horizontal angle in rads</param>
        /// <param name="vAngle">Vertical angle in rads</param>
        /// <exception cref="Exception"></exception>
        internal void MoveByAngle(double hzAngle, double vAngle)
        {
            StopIfMoving();

            Module.StopTrackTarget();

            // In face 2, invert vAngle only
            if (Module.GetFace() == FaceType.Face2)
                vAngle = -vAngle;

            int retCode = Module.Ts60._Geocom.MOT_StartController(GeoComTypes.MOT_MODE.MOT_OCONST);
            if (retCode != GeoComTypes.GRC_OK)
            {
                string errorMessage = Module.ErrorCode(retCode);
                throw new Exception($"Moving has failed, error in MOT_StartController: {errorMessage}");
            }

            //Time in milliseconds necessary to make the move; we are not going fast because it would reduce accuracy, and we take at least 1s
            double timeForHzAtMaxSpeed = Math.Abs(hzAngle) / 0.25d * 1000d;
            double timeForVAtMaxSpeed = Math.Abs(vAngle) / 0.25d * 1000d;
            double[] times = { timeForHzAtMaxSpeed, timeForVAtMaxSpeed, 1000d };
            double maxTimeInMs = Math.Ceiling(times.Max());
            //Hz and V speed to move by this angle in the time calcuted
            double hzSpeed = hzAngle / maxTimeInMs * 1000d;
            double vSpeed = vAngle / maxTimeInMs * 1000d;
            Debug.WriteInConsole($"Moving at speed hzSpeed={hzSpeed}, vSpeed={vSpeed} during {maxTimeInMs} ms.");
            if (StayInARange(hzSpeed) != hzSpeed || StayInARange(vSpeed) != vSpeed)
                throw new Exception($"Computed speed values are out of range : hzSpeed={hzSpeed}, vSpeed={vSpeed}");

            //Move at the computed velocity for the number of milliseconds computed
            GeoComTypes.MOT_COM_PAIR refOmega = new GeoComTypes.MOT_COM_PAIR(new double[] { hzSpeed, vSpeed });
            retCode = Module.Ts60._Geocom.MOT_SetVelocity(refOmega);
            if (retCode != GeoComTypes.GRC_OK)
            {
                string errorMessage = Module.ErrorCode(retCode);
                throw new Exception($"Moving has failed, error in MOT_SetVelocity: {errorMessage}");
            }
            StopMovingIn((int)maxTimeInMs);
        }

        internal double StayInARange(double value, double upperLimit = 0.79, double lowerLimit = -0.79)
        {
            if (value > upperLimit)
            {
                value = upperLimit;
            }
            else if (value < lowerLimit)
            {
                value = lowerLimit;
            }
            return value;
        }

        #region IDisposable

        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    movingTimer?.Dispose();
                    Module = null;
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable
        //internal void Goto()
        //{
        //    Polar.Measure m = Module._BeingMeasuredTheodoliteData;

        //    if (m == null)
        //    {
        //        m = Module._ToBeMeasureTheodoliteData;
        //    }
        //    else
        //    {
        //        if (m._Status is M.States.Cancel) // it can happen if the point where in the live data
        //            m = Module._ToBeMeasureTheodoliteData;
        //    }

        //    if (m == null) throw new Exception($"{R.T_DONT_KNOW_WHERE_TO_GO_TO};{R.T_IS_A_POINT_SET_UP_FOR_MEASUREMENT}");
        //    if (m.HasCorrectedAngles)
        //        Goto(m);
        //    else
        //    {
        //        Polar.Station s = this.Module.StationTheodolite;
        //        Elements.Point p = m._Point;

        //        if (s.Parameters2._IsSetup)
        //            Goto(p, s, m.Extension.Value);
        //    }
        //    Module.Ready();
        //}
        //internal void Goto(Polar.Measure m)
        //{
        //    Module.StatusChange($"{R.T_GOING_TO} {m._Point._Name} {R.T_IN} {m.Face}", P.StepTypes.Begin);
        //    try
        //    {
        //        TSU.HourGlass.Set();

        //        if (m.Angles.Corrected.Horizontal.Value ==TSU.Tsunami2.Preferences.Values.na) throw new Exception($"{R.T_GOTO_IMPOSSIBLE}.");
        //        if (Module.sync != null)
        //        {

        //            this.Module.GetAngleForGoodFaceGoto(m, out TSU.Common.Elements.Angles a);

        //            Module.timeSpentMeasureing.Add("sync PointLaserHVD", TimeSpent.type.Started, DateTime.Now);

        //            Debug.WriteInConsole("at40x-SYNC-PointLaserHVD");

        //            Module._status.isTrackerBusy = true;

        //            Module.sync.PointLaserHVD(a.Horizontal.Value, a.Vertical.Value, 1);


        //            Module.timeSpentMeasureing.Add("sync PointLaserHVD", TimeSpent.type.Stopped, DateTime.Now);
        //        }
        //        Module.StatusChange($"{R.T_WENT_TO} {m._Point._Name}", P.StepTypes.End);
        //    }
        //    catch (Exception ex)
        //    {

        //        if (ex.HResult == -2146233088) // "Could not get face;Invoking command \"Get Face\" [98] failed!" we have probably lost connection
        //        {
        //            this.Module.View.ShowMessage(Views.MessageTsu.Types.Warning,
        //                $"{"No reaction"};{"Ts60 did not respond, are you connected? Maybe you lost the connection?"}");
        //            return;
        //        }
        //        if (!this.Module._measurement.IsSimulation)
        //            throw new Exception($"{R.T_GOTO_FAILED}:" + ex.Message, ex);
        //        Module.StatusChange($"{R.T_DIDNT_WENT_TO} {m._Point._Name}", P.StepTypes.End);
        //    }
        //    finally
        //    {
        //        TSU.HourGlass.Remove();

        //        Module._status.isTrackerLockedAndReady = false;
        //        Module._status.isTrackerBusy = false;
        //    }
        //}

        //internal void Goto(Elements.Point p, Polar.Station s, double ext)
        //{
        //    try
        //    {

        //        Module._status.isTrackerBusy = true;

        //        if (p == null) throw new Exception($"{R.T_NO_POINT_DEFINED_FOR_THE_GOTO}");
        //        if (!p._Coordinates.HasAny) throw new Exception($"Point '{p._Name}' {R.T_HAS_NO_COORDINATES}");



        //        Module.StatusChange($"{R.T_GOING_TO} {p._Name}", P.StepTypes.Begin);

        //        Polar.Station.Parameters param = s.Parameters2 as Polar.Station.Parameters;

        //        Debug.WriteInConsole("at40x-SYNC-SetCoordinateSystemType");
        //        Module.sync.SetCoordinateSystemType(ES_CoordinateSystemType.ES_CS_RHR);

        //        // ANGL
        //        double gisementStationPointVise = Survey.GetBearing(param._StationPoint, p);
        //        double h = gisementStationPointVise - param.vZero.Value;
        //        if (h > 400) h -= 400;
        //        if (h < 0) h += 400;

        //        // ZEND
        //        double v = Survey.GetVerticalAngle(param._StationPoint, p);


        //        Debug.WriteInConsole("at40x-SYNC-PointLaserHVD");
        //        Module.sync.PointLaserHVD(h, v, 1);
        //        Debug.WriteInConsole("at40x-SYNC-SetCoordinateSystemType");
        //        Module.sync.SetCoordinateSystemType(ES_CoordinateSystemType.ES_CS_SCW);



        //        Module.StatusChange($"{R.T_WENT_TO} {p._Name}", P.StepTypes.End);
        //    }
        //    catch (Exception)
        //    {

        //    }
        //    finally
        //    {
        //        Module._status.isTrackerLockedAndReady = false;
        //        Module._status.isTrackerBusy = false;
        //    }
        //}
        //internal void PositionRelativeHV(double p1, double p2)
        //{
        //    // this vt angle when moving up do no move up as much a the opposite value move down...lets multiply by 1.4
        //    if (p2 < 0)
        //        p2 *= 1.4;

        //    Module._status.isTrackerBusy = true;
        //    Debug.WriteInConsole("at40x-SYNC-PositionRelativeHV");
        //    Module.sync.PositionRelativeHV(p1, p2);
        //    Module._status.isTrackerBusy = false;
        //}
    }





    #endregion
}
