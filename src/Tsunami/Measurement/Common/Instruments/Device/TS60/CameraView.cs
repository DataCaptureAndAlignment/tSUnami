﻿using LibVLCSharp.Shared;
using System;
using System.Windows.Forms;
using TSU.ENUM;
using TSU.Views;
using static GeoComClient.GeoComTypes;

namespace TSU.Common.Instruments.Device.TS60
{
    public partial class CameraView : SubView
    {
        LibVLC _libVLC;
        MediaPlayer _mp;

        public CameraView()
        {
            InitializeComponent();
            this.ApplyThemeColors();
        }

        private void ApplyThemeColors()
        {
            this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
        }

        public CameraView(View parentView) : base(parentView)
        {
            InitializeComponent();
            this.ParentView = parentView;
            if (!DesignMode)
                Core.Initialize();
            _libVLC = new LibVLC();
            _mp = new MediaPlayer(_libVLC);
            videoView.MediaPlayer = _mp;
            _mp.Playing += OnPlay;
        }

        internal override void Start()
        {
            base.Start(); // will put the view in the subviewPanel
            // Alway try to stop the remote video, in case one of the cameras is recording
            ((TsuView)TS60View).TryAction(CameraStop, "The stop of the camera");
            // And start the selected one
            ((TsuView)TS60View).TryAction(StartCamera, "The start of the camera");
        }

        internal override void Stop()
        {
            base.Stop(); // will put the view in the subviewPanel
            ((TsuView)TS60View).TryAction(CameraStop, "The stop of the camera");
        }

        private void StartCamera()
        {
            try
            {
                videoView.Visible = true;
                Result result = TS60Module.SelectedCamera.StartRemoteVideo(3, 100);
                Logs.Log.AddEntryAsResult(_Module, result);
                if (result.Success)
                {
                    using (var media = new Media(_libVLC, new Uri($"rtsp://{TS60Module.IP}/TSCam")))
                    {
                        _mp.Play(media);
                    }
                }
                else
                {
                    videoView.Visible = false;
                }
            }
            catch (Exception)
            {
                videoView.Visible = false;
                Logs.Log.AddEntryAsPayAttentionOf(this._Module, "Error when trying to start the camera.");
            }
        }

        private void CameraStop()
        {
            try
            {

                _mp.SetAdjustInt(VideoAdjustOption.Enable, 0);
                _mp.Stop();
                videoView.Visible = false;
                Logs.Log.AddEntryAsResult(_Module, TS60Module.SelectedCamera.StopRemotevideo());
            }
            catch (Exception ex)
            {
                Logs.Log.AddEntryAsPayAttentionOf(_Module, "Error when trying to stop the camera :" + ex.Message);
            }
        }

        private void toolStripButtonMoreB_Click(object sender, EventArgs e)
        {
            SetVideoAjustOptionByDelta(VideoAdjustOption.Brightness, 0.1f);
        }

        private void toolStripButtonLessB_Click(object sender, EventArgs e)
        {
            SetVideoAjustOptionByDelta(VideoAdjustOption.Brightness, -0.1f);
        }

        private void toolStripButtonMoreC_Click(object sender, EventArgs e)
        {
            SetVideoAjustOptionByDelta(VideoAdjustOption.Contrast, 0.1f);
        }

        private void toolStripButtonLessC_Click(object sender, EventArgs e)
        {
            SetVideoAjustOptionByDelta(VideoAdjustOption.Contrast, -0.1f);
        }

        private void SetVideoAjustOptionByDelta(VideoAdjustOption option, float delta)
        {
            try
            {
                //enable the VideoAjustOption feature
                _mp.SetAdjustInt(VideoAdjustOption.Enable, 1);
                //get the current value
                float value = _mp.AdjustFloat(option);
                //adjust it
                value += delta;
                //and set it
                _mp.SetAdjustFloat(option, value);

                Logs.Log.AddEntryAsFYI(_Module, $"{option} set to {value * 100f:0} %");
            }
            catch (Exception ex)
            {
                Logs.Log.AddEntryAsPayAttentionOf(_Module, "Error when trying to change {option} :" + ex.Message);
            }
        }

        private void CameraView_FormClosed(object sender, FormClosedEventArgs e)
        {
            _mp.SetAdjustInt(VideoAdjustOption.Enable, 0);
            _mp.Stop();
            _mp.Dispose();
            _libVLC.Dispose();
        }

        private void Video_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            string arrowKeyText = e.KeyCode.ToString();
            movingDirection md = Motor.GetDirectionFromText(arrowKeyText);
            TS60Module._motor.MoveByDirection(md);
        }

        private void toolStripButtonSwitchCamera_Click(object sender, EventArgs e)
        {
            ((TsuView)TS60View).TryAction(CameraStop, "The stop of the camera");
            if (TS60Module.SelectedCameraType == CameraType.Overview)
                TS60Module.SelectedCameraType = CameraType.Telescope;
            else
                TS60Module.SelectedCameraType = CameraType.Overview;
            ((TsuView)TS60View).TryAction(StartCamera, "The start of the camera");
        }

        private void toolStripDropDownZoom_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            CAM_ZOOM_FACTOR_TYPE newZoomFactor;
            if (e.ClickedItem == toolStripMenuItem1x)
                newZoomFactor = CAM_ZOOM_FACTOR_TYPE.CAM_ZOOM_1X;
            else if (e.ClickedItem == toolStripMenuItem2x)
                newZoomFactor = CAM_ZOOM_FACTOR_TYPE.CAM_ZOOM_2X;
            else if (e.ClickedItem == toolStripMenuItem4x)
                newZoomFactor = CAM_ZOOM_FACTOR_TYPE.CAM_ZOOM_4X;
            else
                newZoomFactor = CAM_ZOOM_FACTOR_TYPE.CAM_ZOOM_8X;

            Result rs = TS60Module.SelectedCamera.SetZoomFactor(newZoomFactor);
            if (rs.Success)
            {
                Logs.Log.AddEntryAsFYI(this._Module, rs.AccessToken.ToString());
            }
            else
            {
                Logs.Log.AddEntryAsPayAttentionOf(this._Module, rs.AccessToken.ToString());
            }
        }

        private void OnPlay(object sender, EventArgs e)
        {
            // To prevent the control from absorbing Key and Mouse Input
            _mp.EnableKeyInput = false;
            _mp.EnableMouseInput = false;
        }

        private void video_MouseClick(object sender, MouseEventArgs e)
        {
            Result rsGlobal = TS60Module.SelectedCamera.GetFov(out double foVHz, out double foVV);
            Logs.Log.AddEntryAsResult(_Module, rsGlobal);

            if (!rsGlobal.Success)
                return;

            Debug.WriteInConsole($"GetFov foVHz={foVHz}, foVV={foVV}");
            Debug.WriteInConsole($"e.Location={e.Location}");

            // Aspect ratio of the videoView
            double viewHeight = videoView.Size.Height;
            double viewWidth = videoView.Size.Width;
            double viewRatio = viewWidth / viewHeight;
            Debug.WriteInConsole($"viewHeight={viewHeight}, viewWidth={viewWidth}, viewRatio={viewRatio}");

            // Aspect ratio of the video
            uint uIntvideoHeight = 0;
            uint uIntVideoWidth = 0;
            _mp.Size((uint)_mp.VideoTrack, ref uIntVideoWidth, ref uIntvideoHeight);
            double videoHeight = uIntvideoHeight;
            double videoWidth = uIntVideoWidth;
            double videoRatio = videoWidth / videoHeight;
            Debug.WriteInConsole($"videoHeight={videoHeight}, videoWidth={videoWidth}, videoRatio={videoRatio}");

            // Size of the video in the VideoView
            double videoInViewHeight;
            double videoInViewWidth;
            if (viewRatio > videoRatio)
            {
                videoInViewHeight = viewHeight;
                videoInViewWidth = viewWidth * videoRatio / viewRatio ;
            }
            else
            {
                videoInViewHeight = viewHeight * viewRatio / videoRatio;
                videoInViewWidth = viewWidth;
            }
            Debug.WriteInConsole($"videoInViewHeight={videoInViewHeight}, videoInViewWidth={videoInViewWidth}");

            // Position of the click relative to the center, in pixels
            double clickRelToCentX = e.Location.X - (viewWidth / 2d);
            double clickRelToCentY = e.Location.Y - (viewHeight / 2d);
            Debug.WriteInConsole($"clickRelToCentX={clickRelToCentX}, clickRelToCentY={clickRelToCentY}");

            // C
            double hzAngle = clickRelToCentX / videoInViewWidth * foVHz;
            double vAngle = clickRelToCentY / videoInViewHeight * foVV;
            Debug.WriteInConsole($"hzAngle={hzAngle}, vAngle={vAngle}");

            TS60Module._motor.MoveByAngle(hzAngle, vAngle);
        }
    }
}
