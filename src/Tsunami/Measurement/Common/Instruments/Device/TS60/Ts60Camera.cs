﻿using System;
using TSU.Views;
using TSU.Views.Message;
using static GeoComClient.GeoComTypes;

namespace TSU.Common.Instruments.Device.TS60
{
    internal class Camera
    {
        private readonly CAM_ID_TYPE CameraType;

        public string CameraTypeAsString => CameraTypeToString(CameraType);

        public Module Module { get; private set; }

        public Camera(CAM_ID_TYPE cameraType, Module module)
        {
            CameraType = cameraType;
            Module = module;
        }

        internal Result StartRemoteVideo(short nFrameRate, short nBitRate)
        {
            try
            {
                Result r = EnsureCameraIsOnAndReady();
                if (!r.Success)
                    return r;

                // Check the firware version, because the quality is expressed differently in older versions
                int retCode = Module.Ts60._Geocom.CSV_GetSWVersion2(out short nRelease, out short nVersion, out _);
                if (retCode != GRC_OK)
                {
                    string errorMessage = Module.ErrorCode(retCode);
                    return new Result
                    {
                        Success = false,
                        ErrorMessage = errorMessage,
                        AccessToken = $"Could not get firware version on the {CameraTypeAsString} camera, error: {errorMessage}"
                    };
                }

                // Refuse to continue in older versions (to save development time)
                if (nRelease < 5 || (nRelease == 5 && nVersion < 50))
                {
                    string errorMessage = "Please update the TS60 firmware to the latest version to get the best quality for the vidéo";
                    new MessageInput(MessageType.Important, errorMessage).Show();
                }

                retCode = Module.Ts60._Geocom.CAM_StartRemoteVideo(CameraType, nFrameRate, nBitRate);
                if (retCode == GRC_OK)
                {
                    Module.remoteVideoStatus = RemoteVideoStatus.On;
                    return new Result { AccessToken = $"Remote video started on the {CameraTypeAsString} camera" };
                }
                else
                {
                    string errorMessage = Module.ErrorCode(retCode);
                    return new Result
                    {
                        Success = false,
                        ErrorMessage = errorMessage,
                        AccessToken = $"Could not start remote video on the {CameraTypeAsString} camera, error: {errorMessage}"
                    };
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                string errorMessage = Module.ErrorCode(9999);
                return new Result
                {
                    Success = false,
                    ErrorMessage = errorMessage,
                    AccessToken = $"Begin capture on the {CameraTypeAsString} camera error: {errorMessage}"
                };
            }
        }

        private Result EnsureCameraIsOnAndReady()
        {
            Result resultGlobal = new Result();

            var resultReady = IsCameraReady();
            if (resultReady.Success)
            {
                resultGlobal.MergeString(resultReady);
            }
            else
            {
                // Check if the camera is on
                var resultSwitch = GetCameraPowerSwitch(out var status);
                resultGlobal.MergeString(resultSwitch);
                // If it's off, turn it on
                if (resultSwitch.Success && status == ON_OFF_TYPE.OFF)
                {
                    var resultTurnOn = SetCameraPowerSwitch(ON_OFF_TYPE.ON);
                    resultGlobal.MergeString(resultTurnOn);
                }
                // Whether it was already on or we just turned it on, wait until it's ready
                var resultWait = WaitForCameraReady();
                resultGlobal.MergeString(resultWait);
                if (!resultWait.Success)
                {
                    resultGlobal.Success = false;
                    resultGlobal.ErrorMessage = resultGlobal.AccessToken.ToString();
                }
            }

            return resultGlobal;
        }

        private Result IsCameraReady()
        {
            int retCode = Module.Ts60._Geocom.CAM_IsCameraReady(CameraType);
            

            if (retCode == GRC_OK)
            {
                return new Result { AccessToken = $"The {CameraTypeAsString} camera is ready." };
            }
            else
            {
                string errorMessage = Module.ErrorCode(retCode);
                return new Result
                {
                    Success = false,
                    ErrorMessage = errorMessage,
                    AccessToken = $"The {CameraTypeAsString} camera is not ready, error: {errorMessage}"
                };
            }
        }

        private Result WaitForCameraReady()
        {
            int retCode = Module.Ts60._Geocom.CAM_WaitForCameraReady(CameraType, 10000);
            if (retCode == GRC_OK)
            {
                return new Result { AccessToken = $"The {CameraTypeAsString} is ready after waiting" };
            }
            else
            {
                string errorMessage = Module.ErrorCode(retCode);
                return new Result
                {
                    Success = false,
                    ErrorMessage = errorMessage,
                    AccessToken = $"The {CameraTypeAsString} camera is not ready, error: {errorMessage}"
                };
            }
        }

        internal Result GetCameraPowerSwitch(out ON_OFF_TYPE status)
        {
            int retCode = Module.Ts60._Geocom.CAM_GetCameraPowerSwitch(CameraType, out status);
            if (retCode == GRC_OK)
            {
                return new Result { AccessToken = $"The {CameraTypeAsString} camera status is {status}." };
            }
            else
            {
                string errorMessage = Module.ErrorCode(retCode);
                return new Result
                {
                    Success = false,
                    ErrorMessage = errorMessage,
                    AccessToken = $"Could not get the status of the {CameraTypeAsString} camera, error: {errorMessage}"
                };
            }
        }

        internal Result SetCameraPowerSwitch(ON_OFF_TYPE status)
        {
            int retCode = Module.Ts60._Geocom.CAM_SetCameraPowerSwitch(CameraType, status);
            if (retCode == GRC_OK)
            {
                return new Result { AccessToken = $"The {CameraTypeAsString} camera status is now {status}." };
            }
            else
            {
                string errorMessage = Module.ErrorCode(retCode);
                return new Result
                {
                    Success = false,
                    ErrorMessage = errorMessage,
                    AccessToken = $"Could not set the status of the {CameraTypeAsString} camera, error: {errorMessage}"
                };
            }
        }

        internal Result StopRemotevideo()
        {
            int errorCode = Module.Ts60._Geocom.CAM_StopRemoteVideo();
            if (errorCode == GRC_OK)
            {
                Module.remoteVideoStatus = RemoteVideoStatus.Off;
                SetCameraPowerSwitch(ON_OFF_TYPE.OFF);
                return new Result { AccessToken = $"Remote video stopped." };
            }
            else
            {
                string errorMessage = Module.ErrorCode(errorCode);
                return new Result
                {
                    Success = false,
                    ErrorMessage = errorMessage,
                    AccessToken = $"Could not stop remote video, error: {errorMessage}"
                };
            }

            throw new NotImplementedException();
        }

        internal Result SetZoomFactor(CAM_ZOOM_FACTOR_TYPE newZoomFactor)
        {
            int retCode = Module.Ts60._Geocom.CAM_SetZoomFactor(CameraType, newZoomFactor);
            if (retCode == GRC_OK)
            {
                return new Result { AccessToken = $"The zoom factor of the {CameraTypeAsString} camera was set to {ZoomFactorTypeToString(newZoomFactor)}." };
            }
            else
            {
                string errorMessage = Module.ErrorCode(retCode);
                return new Result
                {
                    Success = false,
                    ErrorMessage = errorMessage,
                    AccessToken = $"Could not set the zoom factor of the {CameraTypeAsString} camera to {ZoomFactorTypeToString(newZoomFactor)}, error: {errorMessage}"
                };
            }
        }

        internal Result GetFov(out double rFoVHz, out double rFoVV)
        {
            rFoVHz = 0d;
            rFoVV = 0d;
            CAM_ZOOM_FACTOR_TYPE zoomFactor;

            Result rsGlobal = GetZoomFactor(out zoomFactor);
            if (!rsGlobal.Success)
                return rsGlobal;
            rsGlobal.MergeString(GetFov(zoomFactor, out rFoVHz, out rFoVV));
            return rsGlobal;
        }

        internal Result GetFov(CAM_ZOOM_FACTOR_TYPE zoomFactor, out double rFoVHz, out double rFoVV)
        {
            int retCode = Module.Ts60._Geocom.CAM_GetCameraFoV(CameraType, zoomFactor, out rFoVHz, out rFoVV);
            if (retCode == GRC_OK)
            {
                return new Result { AccessToken = $"The FoV of the {CameraTypeAsString} camera is {rFoVHz} rad HZ, {rFoVV} rad V." };
            }
            else
            {
                string errorMessage = Module.ErrorCode(retCode);
                return new Result
                {
                    Success = false,
                    ErrorMessage = errorMessage,
                    AccessToken = $"Could not get the FoV of the {CameraTypeAsString} camera, error: {errorMessage}"
                };
            }
        }

        internal Result GetZoomFactor(out CAM_ZOOM_FACTOR_TYPE zoomFactor)
        {
            Result r = EnsureCameraIsOnAndReady();
            if (!r.Success)
            {
                zoomFactor = CAM_ZOOM_FACTOR_TYPE.CAM_ZOOM_INVALID;
                return r;
            }

            int retCode = Module.Ts60._Geocom.CAM_GetZoomFactor(CameraType, out zoomFactor);
            if (retCode == GRC_OK)
            {
                return new Result { AccessToken = $"The zoom factor of the {CameraTypeAsString} camera is {ZoomFactorTypeToString(zoomFactor)}." };
            }
            else
            {
                string errorMessage = Module.ErrorCode(retCode);
                return new Result
                {
                    Success = false,
                    ErrorMessage = errorMessage,
                    AccessToken = $"Could not get the zoom factor of the {CameraTypeAsString} camera, error: {errorMessage}"
                };
            }
        }

        internal static string CameraTypeToString(CAM_ID_TYPE cameraType)
        {
            if (cameraType == CAM_ID_TYPE.CAM_ID_OVC)
                return "overview";
            else
                return "telescope";
        }

        internal static string ZoomFactorTypeToString(CAM_ZOOM_FACTOR_TYPE zoomFactorType)
        {
            switch (zoomFactorType)
            {
                case CAM_ZOOM_FACTOR_TYPE.CAM_ZOOM_1X:
                    return "1x";
                case CAM_ZOOM_FACTOR_TYPE.CAM_ZOOM_2X:
                    return "2x";
                case CAM_ZOOM_FACTOR_TYPE.CAM_ZOOM_4X:
                    return "4x";
                case CAM_ZOOM_FACTOR_TYPE.CAM_ZOOM_8X:
                    return "8x";
                default:
                    return "unknown";
            }
        }
    }
}
