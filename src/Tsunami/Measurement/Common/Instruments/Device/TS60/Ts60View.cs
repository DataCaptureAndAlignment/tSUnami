﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TSU.Views;
using TSU.Views.Message;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Common.Instruments.Device.TS60
{
    public partial class View : Device.View
    {
        internal new Module Module
        {
            get
            {
                return this._Module as Module;
            }
        }
        private Instrument _Ts60
        {
            get
            {
                return this.Module.Instrument as Instrument;
            }
        }
        internal List<TsuTask> TaskToDo = new List<TsuTask>();
        //private bool doingTimerThings = false;
        private System.Windows.Forms.ToolTip toolTip1;

        internal JogView jogView;
        internal CameraView cameraView;

        public View(Instruments.Module instrumentModule)
            : base(instrumentModule)
        {
            InitializeComponent();
            this.ApplyThemeColors();
            comboBox_TCP_IP.MouseDoubleClick += delegate { System.Diagnostics.Process.Start("devmgmt.msc"); };
            InitializeLog();
            this.Timer_check.Interval = 20000;
            //this.ShowMessageOfValidation(R.StringTS60_DoCollimation, R.T_OK);
            this.cameraView = new CameraView(this) { Dock = DockStyle.Fill, TopLevel = false };
            this.jogView = new JogView(this) { Dock = DockStyle.Fill, TopLevel = false };
        }

        private void InitializeLog()
        {
            LogView = new Logs.LogView();
            LogView.ShowDockedFill();
            LogView.richTextBox.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.panelLog.Controls.Add(this.LogView);
        }

        private void ApplyThemeColors()
        {
            this.buttonGotoAll.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.buttonGotoAll.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.buttonGotoAll.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.comboBox_PortCom.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.comboBox_PortCom.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.buttonGoto.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.buttonGoto.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.buttonGoto.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.label_numberOfMeas.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.textBox_numberOfMeas.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.textBox_numberOfMeas.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.button_All.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_Rec.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_Distance.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.Button_OnOff.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.panelLog.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.labelMeteo.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
            this.labelMeteo.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_face.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_face.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_face.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.label_Bulle.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.label_Bulle.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.pictureBox_bulle.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.pictureBox_Battery.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_ATR.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_ATR.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_ATR.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.pictureBox_OnOff.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.label1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.label1.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.button_Laser.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_Laser.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_Laser.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_Search.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_Search.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_Search.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_Lock.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_Lock.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_Lock.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.button_TargetType.FlatAppearance.MouseDownBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_TargetType.FlatAppearance.MouseOverBackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.button_TargetType.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            //this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
        }
        private void Ts60View_Load(object sender, EventArgs e)
        {
            this.UpdateLabelMeteo();
            labelMeteo.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
            //log.View.SetColors(
            //    logBack: TSU.Tsunami2.Preferences.Theme.Colors.Background,
            //    fYI: TSU.Tsunami2.Preferences.Theme.Colors.Good);
            this.SetAllToolTips();
            ViewTs60Off();
        }
        public void ViewTs60On()
        {
            button_Rec.Visible = false;
            button_Distance.Visible = false;
            button_All.Visible = true;
            buttonGoto.Visible = false;
            buttonGotoAll.Visible = false;
            button_Lock.Visible = true;
            textBox_numberOfMeas.Visible = false;
            label_numberOfMeas.Visible = false;
            label_Bulle.Visible = true;
            this.label_Bulle.Text = "L = XXX" + "\r\n" + "T = XXX";
            pictureBox_Battery.Visible = true;
            pictureBox_bulle.Visible = true;
            Timer_check.Enabled = true;
            Timer_check.Start();
            button_ATR.Visible = true;
            button_face.Visible = true;
            button_Laser.Visible = true;
            button_Search.Visible = true;
            button_TargetType.Visible = false;
            comboBox_PortCom.Visible = false;
            comboBox_TCP_IP.Visible = false;
            Module.InstrumentIsOn = true;
            this.pictureBox_OnOff.Image = R.TS60_ON;
            this.pictureBox_bulle.Image = R.TS60_Bulle_Bad;
            this.Button_OnOff.BackgroundImage = R.TS60_Close;
            this.Module.OnMeasureToDoReceived();
        }

        internal void AllowAll()
        {
            button_All.Visible = true;
            if (this.Module.Ts60.AtrWanted)
            {
                this.button_Distance.Visible = false;
                this.button_Rec.Visible = false;
            }
            else
            {
                this.button_Distance.Visible = true;
                this.button_Rec.Visible = true;
            }

        }

        //internal void DontAllowAll()
        //{
        //    button_All.Visible = false;
        //    button_Rec.Visible = false;
        //    button_Distance.Visible = false;
        //}

        public void AllowGoto()
        {
            buttonGoto.Visible = true;
            buttonGotoAll.Visible = true;
        }

        public void DontAllowGoto()
        {
            buttonGoto.Visible = false;
            buttonGotoAll.Visible = false;
        }

        public void ViewTs60Off()
        {
            button_Rec.Visible = false;
            button_Distance.Visible = false;
            button_All.Visible = false;
            buttonGoto.Visible = false;
            buttonGotoAll.Visible = false;
            label_numberOfMeas.Visible = false;
            label_Bulle.Visible = false;
            pictureBox_Battery.Visible = false;
            pictureBox_bulle.Visible = false;
            button_face.Visible = false;
            Timer_check.Stop();
            Timer_check.Enabled = false;
            textBox_numberOfMeas.Visible = false;
            button_ATR.Visible = false;
            button_Laser.Visible = false;
            button_Lock.Visible = false;
            button_TargetType.Visible = false;
            button_Search.Visible = false;
            Module.InstrumentIsOn = false;
            comboBox_PortCom.Visible = true;
            comboBox_TCP_IP.Visible = true;
            this.pictureBox_OnOff.Image = R.TS60_OFF;
            this.pictureBox_bulle.Image = R.TS60_Bulle_Bad;
            this.Button_OnOff.BackgroundImage = R.TS60_Connect;
        }
        private void BatteryStateImage()
        {
            if (Module.Ts60._Battery <= 25)
            {
                this.pictureBox_Battery.Image = R.TS60_Battery_Bad;
            }
            if (Module.Ts60._Battery > 75)
            {
                this.pictureBox_Battery.Image = R.TS60_Battery_Good;
            }
            if ((Module.Ts60._Battery <= 75) && (Module.Ts60._Battery > 25))
            {
                this.pictureBox_Battery.Image = R.TS60_Battery_Average;
            }
        }
        private void BulleStateImage()
        {
            double diffGradBulle = Math.Sqrt(Math.Pow(Module.Ts60._Compensator.longitudinalAxisInclination, 2d)
                                            + Math.Pow(Module.Ts60._Compensator.tranverseAxisInclination, 2d))
                                 * 200d
                                 / Math.PI;
            if (diffGradBulle > 0.002)
            {
                this.pictureBox_bulle.Image = R.TS60_Bulle_Bad;
            }
            if (diffGradBulle <= 0.001)
            {
                this.pictureBox_bulle.Image = R.TS60_Bulle_Good;
            }
            if ((diffGradBulle <= 0.002) && (diffGradBulle > 0.001))
            {
                this.pictureBox_bulle.Image = R.TS60_Bulle_Average;
            }
        }
        private void FaceStateImage()
        {
            if (this._Ts60._Face == 1)
            {
                this.button_face.Image = R.TS60_CercleGauche;
            }
            else
            {
                this.button_face.Image = R.TS60_CercleDroit;
            }
        }
        private void Button_OnOff_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Module.InstrumentIsOn && !Module.tcpConnected)
                    TurnOn();
                else
                    TurnOff();
            }
            catch (Exception ex)
            {
                string titleAndMessage = "Connection/Disconnection failed;" + ex.Message;
                new MessageInput(MessageType.Critical, titleAndMessage).Show();
            }
        }

        public void TurnOff()
        {
            try
            {
                // Do thing from the UI
                Result closeSuccess = new Result();

                TsuTask taskDeconnection = new TsuTask()
                {
                    ActionName = "Geocom deconnection",
                    estimatedTimeInMilliSec = 1000,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy(false);
                    },
                    mainAction = () =>
                    {
                        closeSuccess.MergeString(Module.StopTrackTarget());
                        closeSuccess = Module.Disconnect();
                    },
                    postAction = () =>
                    {
                        Logs.Log.AddEntryAsResult(this._Module, closeSuccess);
                        if (closeSuccess.Success)
                        {
                            ViewTs60Off();
                        }
                        else
                        {
                            Timer_check.Enabled = true;
                            Timer_check.Start();
                        }
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    timeOutAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.ViewTs60Off();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                // if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { taskDeconnection }, "Deconnection");
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(taskDeconnection);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { taskDeconnection }, "Deconnection");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != R.String_Exception_InstrumentIsBusy) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }
        }

        public void TurnOn()
        {
            try
            {
                Result connecTSUccess = new Result();
                Result iniTSUccess = new Result();
                String[] nameSplit;
                Char splitChar = '.';
                nameSplit = comboBox_TCP_IP.Text.Split(splitChar);
                if ((nameSplit.Length == 4) && System.Text.RegularExpressions.Regex.IsMatch(comboBox_TCP_IP.Text, @"^[0-9.]+$"))
                {
                    int portCom = T.Conversions.Numbers.ToInt(comboBox_PortCom.Text, -9999);
                    if (portCom != -9999)
                    {
                        this.Module.IP = new Ip(comboBox_TCP_IP.Text);
                        this.Module.Ts60._PortCom = portCom;
                        this.Module.RunWithTimeout(this.Module.CheckCompensation, new TimeSpan(0,0,3));
                        if (!this.Module.MeteoSet)
                        {
                            this.Module.SetMeteoManually();
                            this.UpdateLabelMeteo();
                        }
                        TsuTask taskConnection = new TsuTask()
                        {
                            ActionName = "Connection",
                            estimatedTimeInMilliSec = 300,
                            timeOutInSec = 0,
                            preAction = () =>
                            {
                                Module.IP = new Ip(comboBox_TCP_IP.Text);
                                this.Module.CheckAndWaitIfBusy(false);
                            },
                            mainAction = () =>
                            {
                                connecTSUccess.MergeString(Module.Connect());
                            },
                            postAction = () =>
                            {
                                // post action to do in the UI
                                connecTSUccess.MergeString(Module.GetSerialNumber());
                                Logs.Log.AddEntryAsResult(this._Module, connecTSUccess);
                                if (!connecTSUccess.Success)
                                {
                                    string titleAndMessage = $"{R.StringTS60_ProblemDuringInitialization}. Note that the TS60 is only looking for the tp-link box during start - up, you have to connect the tp - link befre you start the instrument";
                                    new MessageInput(MessageType.Critical, titleAndMessage).Show();

                                    ViewTs60Off();
                                    TaskToDo.Clear();
                                    this.Module.IsBusyCommunicating = false;
                                }
                            },
                            cancelAction = () =>
                            {
                                TaskToDo.Clear();
                                this.Module.IsBusyCommunicating = false;
                            }
                        };
                        if (iniTSUccess.Success)
                        {
                            iniTSUccess = new Result();
                            TsuTask taskInit = new TsuTask()
                            {
                                ActionName = "Initialisation",
                                estimatedTimeInMilliSec = 300,
                                preAction = () =>
                                {
                                    Module.IP = new Ip(comboBox_TCP_IP.Text);
                                },
                                mainAction = () =>
                                {
                                    if (connecTSUccess.Success) iniTSUccess.MergeString(Module.InitializeSensor());
                                },
                                postAction = () =>
                                {
                                    if (connecTSUccess.Success)
                                    {
                                        if (iniTSUccess.Success)
                                        {
                                            this.Module.MeasureCompensator();
                                            ViewTs60On();
                                            BatteryStateImage();
                                            Atr_StateImage();
                                            TargetLock_StateImage();
                                            FaceStateImage();
                                            UpdateBulle();
                                            BulleStateImage();
                                            RedLaserStateImage();
                                            this.Module.OnMeasureToDoReceived();
                                        }
                                        Logs.Log.AddEntryAsResult(this._Module, iniTSUccess);
                                        if (!iniTSUccess.Success)
                                        {
                                            string titleAndMessage = $"{R.StringTS60_ProblemDuringInitialization}";
                                            new MessageInput(MessageType.Critical, titleAndMessage).Show();
                                            ViewTs60Off();
                                        }
                                    }
                                    TaskToDo.Clear();
                                    this.Module.IsBusyCommunicating = false;
                                },
                                ExceptionAction = () =>
                                {
                                    ViewTs60Off();
                                    TaskToDo.Clear();
                                    this.Module.IsBusyCommunicating = false;
                                },
                            };
                            if (TaskToDo.Count == 0)
                            {
                                TaskToDo.Add(taskConnection);
                                TaskToDo.Add(taskInit);
                                //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { taskConnection, taskInit }, "Connection");
                                RunTasksWithWaitingMessage(new List<TsuTask>() { taskConnection, taskInit }, "Connection");
                            }
                            else
                            {
                                MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                                {
                                    ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                                };
                                string respond = mi.Show().TextOfButtonClicked;
                                if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                            }
                        }
                        else
                        {
                            new MessageInput(MessageType.Critical, R.StringTS60_ProblemDuringConnection).Show();
                        }
                    }
                    else
                    {
                        new MessageInput(MessageType.Critical, R.StringTS60_Wrong_ComPort).Show();
                    }
                }
                else
                {
                    new MessageInput(MessageType.Critical, R.StringTS60_Wrong_IP).Show();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != R.String_Exception_InstrumentIsBusy) this.Module.IsBusyCommunicating = false;
                TaskToDo.Clear();
                throw;
            }
        }

        private void button_Distance_Click(object sender, EventArgs e)
        {
            Logs.Log.AddEntryAsSuccess(this.Module, R.T_MEASURING + "...");
            this.Invalidate();
            this.Module.DoTemporaryDistanceMeasurement();
        }

        private void button_Rec_Click(object sender, EventArgs e)
        {
            this.Module.DistanceWanted = false;
            this.Module.GotoWanted = false;
            TryAction(LaunchMeasurement);
        }

        private void button_All_Click(object sender, EventArgs e)
        {
            this.Module.GotoWanted = false;
            this.Module.DistanceWanted = true;
            TryAction(LaunchMeasurement);
        }
        private void buttonGotoAll_Click(object sender, EventArgs e)
        {
            this.Module.GotoWanted = true;
            this.GotoAll();
        }
        public override void MeasureAsync()
        {
            base.MeasureAsync(); // do nothing
            TryAction(LaunchMeasurement);

        }
        /// <summary>
        /// Lance une mesure demandée à l'intérieur de la vue
        /// </summary>
        public void LaunchMeasurement()
        {
            try
            {
                Result r = new Result();
                if (!this.Module.IsReadyToMeasure())
                    throw new Exception("Nothing ready to measure");
                //if (this.Module._ToBeMeasureTheodoliteData == null) throw new Exception("Nothing to measure");
                //this.Module.PrepareMeasure();
                int factor = this.Module._ToBeMeasureTheodoliteData.NumberOfMeasureToAverage;
                if (Module._ToBeMeasureTheodoliteData.Face == FaceType.DoubleFace) factor *= 2;

                TsuTask task = new TsuTask()
                {
                    ActionName = "Measuring...",

                    estimatedTimeInMilliSec = 7000 * factor,
                    timeOutInSec = 0,//*30 * factor*,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        r.MergeString(this.Module.tempResult);
                        r.MergeString(this.Module.MeasureTS60());
                        TaskToDo.Clear();
                    },
                    cancelAction = () =>
                    {
                        this.Module.CancelMeasureInProgress();
                    },
                    postAction = () =>
                    {
                        if (this.Module.reMeasure) return; // if the user selected to remeasure for somereasons (message diff or problem we should not end the measure here
                        this.Module.BeingMeasured = null;
                        this.Module.DistanceWanted = true; // by default sot that measure() from polar Module take all.
                        if (r.Success)
                        {
                            this.Module.ReportMeasurement();
                            this.Module.FinishMeasurement();
                            this.Module.tempResult = r;
                        }
                        else
                        {
                            Logs.Log.AddEntryAsResult(this.Module, this.Module.tempResult);
                        }
                        this.Atr_StateImage();
                        this.TargetLock_StateImage();
                        this.RedLaserStateImage();
                        // will be updated with the timer
                        //this.Module.OnMeasureToDoReceived();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Measure");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != R.String_Exception_InstrumentIsBusy) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }
        }
        /// <summary>
        /// Lance une mesure demandée par un module hors de cette vue et ne fait pas un report measurement et un ajout logbook
        /// </summary>
        public void LaunchModuleMeasurement()
        {
            try
            {
                Result r = new Result();
                //this.Module.PrepareMeasure();
                int factor = this.Module._ToBeMeasureTheodoliteData.NumberOfMeasureToAverage;
                if (Module._ToBeMeasureTheodoliteData.Face == FaceType.DoubleFace) factor *= 2;

                TsuTask task = new TsuTask()
                {
                    ActionName = "Measuring...",

                    estimatedTimeInMilliSec = 7000 * factor,
                    timeOutInSec = 0,//*30 * factor*,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        r.MergeString(this.Module.tempResult);
                        r.MergeString(this.Module.MeasureTS60());
                    },
                    cancelAction = () =>
                    {
                        this.Module.CancelMeasureInProgress();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    postAction = () =>
                    {
                        this.Module.BeingMeasured = null;
                        this.Module.DistanceWanted = true; // by default sot that measure() from polar Module take all.
                        if (r.Success)
                        {
                            this.Module.tempResult = r;
                        }
                        this.Atr_StateImage();
                        this.TargetLock_StateImage();
                        this.RedLaserStateImage();
                        // will be updated with the timer
                        this.Module.OnMeasureToDoReceived();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Measure");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != R.String_Exception_InstrumentIsBusy) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }
        }
        /// <summary>
        /// Do the goto to next position
        /// </summary>
        internal void Goto()
        {
            try
            {
                Result r = new Result();
                TsuTask task = new TsuTask()
                {
                    ActionName = "Goto...",

                    estimatedTimeInMilliSec = 7000,
                    timeOutInSec = 0,//*30 * factor*,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        this.Module.GotoTS60();
                        this.Module.DistanceWanted = true;
                        r.MergeString(this.Module.tempResult);
                    },
                    postAction = () =>
                    {
                        Logs.Log.AddEntryAsResult(this.Module, this.Module.tempResult);
                        this.Atr_StateImage();
                        this.TargetLock_StateImage();
                        this.RedLaserStateImage();
                        // will be updated with the timer
                        this.Module.OnMeasureToDoReceived();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Goto");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != R.String_Exception_InstrumentIsBusy) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }
        }
        /// <summary>
        /// Do the goto to next position
        /// </summary>
        internal void GotoAll()
        {
            try
            {
                Result r = new Result();
                int factor = this.Module._ToBeMeasureTheodoliteData.NumberOfMeasureToAverage;
                if (Module._ToBeMeasureTheodoliteData.Face == FaceType.DoubleFace) factor *= 2;
                factor++;
                TsuTask task = new TsuTask()
                {
                    ActionName = "GotoAll...",

                    estimatedTimeInMilliSec = 7000 * factor,
                    timeOutInSec = 0,//*30 * factor*,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        this.Module.GotoTS60();
                        this.Module.DistanceWanted = true;
                        r.MergeString(this.Module.tempResult);
                        if (r.Success) { r.MergeString(this.Module.MeasureTS60()); }
                    },
                    postAction = () =>
                    {
                        if (r.Success)
                        {
                            this.Module.ReportMeasurement();
                        }
                        else
                        {
                            Logs.Log.AddEntryAsResult(this.Module, this.Module.tempResult);
                        }
                        this.Atr_StateImage();
                        this.TargetLock_StateImage();
                        this.RedLaserStateImage();
                        // will be updated with the timer
                        this.Module.OnMeasureToDoReceived();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, R.T_GOTO_AND_DO_MEASUREMENT);
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != R.String_Exception_InstrumentIsBusy) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }
        }
        private void Timer_check_TS60_Tick(object sender, EventArgs e)
        {
            if (!this.Module.IsBusyCommunicating)
            {
                try
                {
                    Result checkTS60 = new Result();
                    TsuTask task = new TsuTask()
                    {
                        ActionName = "Timer Check",
                        estimatedTimeInMilliSec = 4000,
                        timeOutInSec = 0,
                        preAction = () =>
                        {
                            Timer_check.Stop();
                            Timer_check.Enabled = false;
                            //this.doingTimerThings = true;
                            this.Module.CheckAndWaitIfBusy();
                        },
                        mainAction = () =>
                        {
                            checkTS60.MergeString(this.DoTimerCheckThings());
                        },
                        postAction = () =>
                        {
                            Logs.Log.AddEntryAsResult(this._Module, checkTS60, skipSuccesMessage:true);
                            if (checkTS60.Success)
                            {
                                Timer_check.Enabled = true;
                                Timer_check.Start();
                            }
                            else
                            {
                                // disconnect and reconnect 
                                Result r = new Result();
                                this.Module.Disconnect();
                                r.MergeString(this.Module.Connect());
                                Logs.Log.AddEntryAsResult(this._Module, r);
                                if (r.Success)
                                {
                                    new MessageInput(MessageType.Critical, R.StringTS60_Connection_Problem).Show();
                                    this.TurnOff();
                                }
                                else
                                {
                                    Timer_check.Enabled = true;
                                    Timer_check.Start();
                                }
                            }
                            //this.doingTimerThings = false;
                            TaskToDo.Clear();
                            this.Module.IsBusyCommunicating = false;
                        },
                        ExceptionAction = () =>
                        {
                            Timer_check.Enabled = true;
                            Timer_check.Start();
                            TaskToDo.Clear();
                            this.Module.IsBusyCommunicating = false;
                        },
                    };
                    if (TaskToDo.Count == 0)
                    {
                        //I want action request by user not be blocked by timer
                        //TaskToDo.Add(task);
                        this.RunTasksWithoutWaitingMessage(new List<TsuTask>() { task, }, "Updating");

                    }
                    else
                    {
                        MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                        {
                            ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                        };
                        string respond = mi.Show().TextOfButtonClicked;
                        if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message != R.String_Exception_InstrumentIsBusy) this.Module.IsBusyCommunicating = false;
                    Timer_check.Enabled = true;
                    Timer_check.Start();
                    TaskToDo.Clear();
                    throw;
                }
            }
            //// run in task not to block Tsunami when the TDA is closed by Tsunami itself
            //var task = Task.Run(() => { checkTs60 = Module.CheckConnection()});
            //if (task.Wait(TimeSpan.FromSeconds(5)) || )
            //{
            //    checkTS60 = DoTimerThings(task);
            //}
            //else
            //{
            //    this.ShowMessageOfCritical("Connection Lost");
            //}
        }
        public Result DoTimerCheckThings()
        {
            Result r = new Result();
            r.MergeString(this.Module.CheckConnection());
            if (!r.Success)
            {
                //this.Module.InvokeOnApplicationDispatcher(this.TurnOff);
                Logs.Log.AddEntryAsError(this._Module, R.StringTS60_CheckConnectionFailed);
            }
            else
            {
                r.MergeString(this.Module.MeasureCompensator());
                r.MergeString(this.Module.GetBatteryLevel());
                r.MergeString(this.Module.GetTs60Face());
                r.MergeString(this.Module.GetLockStatus());
                if (this.Module.Ts60.LockStatus == GeoComClient.GeoComTypes.MOT_LOCK_STATUS.MOT_LOCKED_OUT)
                {
                    r.MergeString(this.Module.SetRedLaserOnOff(this.Module.Ts60._UserRedLaserPreference));
                }
                r.Success = true; //if not get current 1 parameter, I don't want to shut down TS60
                this.Module.InvokeOnApplicationDispatcher(this.Atr_StateImage);
                this.Module.InvokeOnApplicationDispatcher(this.TargetLock_StateImage);
                this.Module.InvokeOnApplicationDispatcher(this.UpdateBulle);
                this.Module.InvokeOnApplicationDispatcher(BatteryStateImage);
                this.Module.InvokeOnApplicationDispatcher(BulleStateImage);
                this.Module.InvokeOnApplicationDispatcher(RedLaserStateImage);
                this.Module.InvokeOnApplicationDispatcher(FaceStateImage);
                this.Module.OnMeasureToDoReceived(); //To check if still have measure to do (avoid having a goto and no measure
                //Logs.Log.AddEntryAsFYI(this._Module, "Connection TDA OK");
            }
            return r;
        }

        public void UpdateBulle()
        {
            if (this._Ts60._Compensator.tranverseAxisInclination != -999)
            {
                double LInGon = this._Ts60._Compensator.longitudinalAxisInclination / Math.PI * 200;
                double TInGon = this._Ts60._Compensator.tranverseAxisInclination / Math.PI * 200;
                this.label_Bulle.Text = $"L = {LInGon:0.0000} g\r\nT ={TInGon:0.0000} g";
                if (LInGon <= 0.001 && TInGon <= 0.001)
                {
                    this.pictureBox_bulle.Image = R.TS60_Bulle_Good;
                }
                else
                {
                    if (Math.Abs(LInGon) <= 0.002 && Math.Abs(TInGon) <= 0.001)
                    {
                        this.pictureBox_bulle.Image = R.TS60_Bulle_Average;
                        return;
                    }
                    if (Math.Abs(TInGon) <= 0.002 && Math.Abs(LInGon) <= 0.001)
                    {
                        this.pictureBox_bulle.Image = R.TS60_Bulle_Average;
                        return;
                    }
                    this.pictureBox_bulle.Image = R.TS60_Bulle_Bad;
                }
            }
            else
            {
                this.label_Bulle.Text = "L = XXX" + "\r\n" + "T = XXX";
                this.pictureBox_bulle.Image = R.TS60_Bulle_Bad;
            }
        }
        private void button_ATR_Click(object sender, EventArgs e)
        {
            try
            {
                Result r = new Result();
                TsuTask task = new TsuTask()
                {
                    ActionName = "Set ATR LOCK",

                    estimatedTimeInMilliSec = 1000,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        r.MergeString(SetATRLock());
                    },
                    postAction = () =>
                    {
                        Logs.Log.AddEntryAsResult(this._Module, r);
                        this.Atr_StateImage();
                        this.TargetLock_StateImage();
                        this.RedLaserStateImage();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Set ATR LOCK functions");
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Set ATR LOCK functions");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != R.String_Exception_InstrumentIsBusy) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }
        }
        /// <summary>
        /// Set the Atr and Lock when pressing the ATR button
        /// </summary>
        private Result SetATRLock()
        {
            Result result = this.Module.SwitchATR();
            this.Module.InvokeOnApplicationDispatcher(this.Atr_StateImage);
            this.Module.InvokeOnApplicationDispatcher(this.TargetLock_StateImage);
            return result;
        }

        public void Atr_StateImage()
        {
            if (this.Module.ToBeMeasuredData != null)
            {
                this.button_TargetType.Visible = true;
                switch ((this.Module.ToBeMeasuredData as Polar.Measure).Distance.Reflector._InstrumentType)
                {
                    case InstrumentTypes.CCR1_5:
                        this.button_ATR.Visible = true;
                        this.button_Search.Visible = true;
                        this.button_Lock.Visible = true;
                        this.button_TargetType.BackgroundImage = R.TS60_RRR1_5;
                        break;
                    case InstrumentTypes.CSCOTCH:
                        this.button_Search.Visible = false;
                        this.button_ATR.Visible = false;
                        this.button_Lock.Visible = false;
                        this.button_TargetType.BackgroundImage = R.TS60_Tape;
                        break;
                    case InstrumentTypes.LASER:
                        this.button_Search.Visible = false;
                        this.button_ATR.Visible = false;
                        this.button_Lock.Visible = false;
                        this.button_TargetType.BackgroundImage = R.TS60_RL;
                        break;
                    default:
                        this.button_ATR.Visible = true;
                        this.button_Search.Visible = true;
                        this.button_Lock.Visible = true;
                        this.button_TargetType.BackgroundImage = R.TS60_Unknown_Target;
                        break;
                }
            }
            else
            {
                this.button_TargetType.Visible = false;
            }

            if (this.Module.Ts60.AtrWanted)
            {
                this.button_Distance.Visible = false;
                this.button_Rec.Visible = false;
            }
            else
            {
                this.button_Distance.Visible = true;
                this.button_Rec.Visible = true;
            }

            if (this.Module.Ts60.AtrWanted)
            {
                if(Module.Ts60.LockStatus != GeoComClient.GeoComTypes.MOT_LOCK_STATUS.MOT_LOCKED_OUT)
                    this.button_ATR.Image = R.TS60_LOCK_On;
                else
                    this.button_ATR.Image = R.TS60_ATR_On;
                this.button_Laser.Visible = false;
            }
            else
            {
                this.button_ATR.Image = R.TS60_ATR_OFF;
                this.button_Laser.Visible = true;
            }
        }
        private void TargetLock_StateImage()
        {
            if (this.Module.ToBeMeasuredData != null)
            {
                this.button_TargetType.Visible = true;
                switch ((this.Module.ToBeMeasuredData as Polar.Measure).Distance.Reflector._InstrumentType)
                {
                    case InstrumentTypes.CCR1_5:
                        this.button_ATR.Visible = true;
                        this.button_TargetType.BackgroundImage = R.TS60_RRR1_5;
                        break;
                    case InstrumentTypes.CSCOTCH:
                        this.button_ATR.Visible = false;
                        this.button_TargetType.BackgroundImage = R.TS60_Tape;
                        break;
                    case InstrumentTypes.LASER:
                        this.button_ATR.Visible = false;
                        this.button_TargetType.BackgroundImage = R.TS60_RL;
                        break;
                    default:
                        this.button_ATR.Visible = true;
                        this.button_TargetType.BackgroundImage = R.TS60_Unknown_Target;
                        break;
                }
            }
            else
            {
                this.button_TargetType.Visible = false;
            }

            if(!this.Module.Ts60.LockWanted 
                || this.Module.Ts60.LockStatus == GeoComClient.GeoComTypes.MOT_LOCK_STATUS.MOT_LOCKED_OUT)
                this.button_Lock.Image = R.TS60_NotLocked;
            else if(this.Module.Ts60.LockStatus == GeoComClient.GeoComTypes.MOT_LOCK_STATUS.MOT_LOCKED_IN)
                this.button_Lock.Image = R.TS60_Locked;
            else
                this.button_Lock.Image = R.TS60_Locking;
        }
        private void button_face_Click(object sender, EventArgs e)
        {
            try
            {
                Result checkface = new Result();
                TsuTask task = new TsuTask()
                {
                    ActionName = R.T_CHANGING_FACE,

                    estimatedTimeInMilliSec = 2000,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        checkface.MergeString(FaceChangeAction());
                    },
                    postAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        Logs.Log.AddEntryAsResult(this._Module, checkface);
                        Atr_StateImage();
                        FaceStateImage();
                        RedLaserStateImage();
                        TargetLock_StateImage();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, R.T_CHANGING_FACE);
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, R.T_CHANGING_FACE);
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != R.String_Exception_InstrumentIsBusy) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }

        }

        private Result FaceChangeAction()
        {
            Result r = new Result();
            r.MergeString(Module.InvertFace());
            r.MergeString(Module.GetTs60Face());
            this.Module.InvokeOnApplicationDispatcher(this.FaceStateImage);
            if (this.Module.Ts60.LockStatus == GeoComClient.GeoComTypes.MOT_LOCK_STATUS.MOT_LOCKED_IN)
            {
                this.Module.StartTrackTarget();
            }
            return r;
        }

        private void textBox_numberOfMeas_TextChanged(object sender, EventArgs e)
        {
            int numberOfMeasure = T.Conversions.Numbers.ToInt(textBox_numberOfMeas.Text, -999);
            if (numberOfMeasure > 0)
                this.Module._ToBeMeasureTheodoliteData.NumberOfMeasureToAverage = numberOfMeasure;
            else
            {
                new MessageInput(MessageType.Critical, R.StringTS60_WrongNumberOfMeas).Show();
            }
        }
        private void labelMeteo_Click(object sender, EventArgs e)
        {
            this.Module.SetMeteoManually();
            this.UpdateLabelMeteo();
        }
        private void UpdateLabelMeteo()
        {
            labelMeteo.Text = string.Format("T° {0}C\r\nP {1}hPa\r\nH {2}%", this.Module.temperature.ToString("F1", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")), this.Module.pressure.ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")), this.Module.humidity.ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
            if (this.Module.MeteoSet) labelMeteo.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Good;
        }

        private void buttonGoto_Click(object sender, EventArgs e)
        {
            this.Module.GotoWanted = false;
            this.Module.MeasureWanted = false;
            this.Goto();
        }

        private void button_Lock_Click(object sender, EventArgs e)
        {
            try
            {
                Result r = new Result();
                TsuTask task = new TsuTask()
                {
                    ActionName = "Track target",

                    estimatedTimeInMilliSec = 2000,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        r.MergeString(Module.SwitchLock());
                    },
                    postAction = () =>
                    {
                        Logs.Log.AddEntryAsResult(this._Module, r);
                        Atr_StateImage();
                        RedLaserStateImage();
                        TargetLock_StateImage();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Set ATR LOCK functions");
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Set ATR LOCK functions");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != R.String_Exception_InstrumentIsBusy) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }

        }

        private void button_Laser_Click(object sender, EventArgs e)
        {
            try
            {
                Result r = new Result();
                TsuTask task = new TsuTask()
                {
                    ActionName = "Start/Stop Laser",

                    estimatedTimeInMilliSec = 2000,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        r.MergeString(InverseRedLaser());
                    },
                    postAction = () =>
                    {
                        Logs.Log.AddEntryAsResult(this._Module, r);
                        this.Atr_StateImage();
                        this.TargetLock_StateImage();
                        this.RedLaserStateImage();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Set red laser");
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Set red laser");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != R.String_Exception_InstrumentIsBusy) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }
        }

        private Result InverseRedLaser()
        {
            Result result = this.Module.SetRedLaserOnOff(!this.Module.Ts60._RedLaser);
            this.Module.InvokeOnApplicationDispatcher(RedLaserStateImage);
            return result;
        }

        private void RedLaserStateImage()
        {
            if (this.Module.Ts60.LockStatus == GeoComClient.GeoComTypes.MOT_LOCK_STATUS.MOT_LOCKED_OUT)
            {
                this.button_Laser.Visible = true;
                this.button_Laser.Image = (this._Ts60._RedLaser) ? R.TS60_Laser_ON : R.TS60_Laser_Off;
            }
            else
            {
                this.button_Laser.Visible = false;
            }
        }

        private void button_Search_Click(object sender, EventArgs e)
        {
            try
            {
                Result r = new Result();
                TsuTask task = new TsuTask()
                {
                    ActionName = "Power Search",

                    estimatedTimeInMilliSec = 10000,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        r.MergeString(DoPowerSearch());
                    },
                    postAction = () =>
                    {
                        Logs.Log.AddEntryAsResult(this._Module, r);
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Launch Power Search");
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Launch Power Search");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != R.String_Exception_InstrumentIsBusy) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }

        }
        /// <summary>
        /// Do a power search
        /// </summary>
        /// <returns></returns>
        private Result DoPowerSearch()
        {
            Result result = new Result();
            result.MergeString(this.Module.DoDefaultPowerSearch());
            if (!result.Success)
            {
                this._Module.InvokeOnApplicationDispatcher(AskIfDoFullPowerSearch);
                if (this.Module.tempMsg == R.T_YES)
                {
                    result = new Result();
                    result.MergeString(this.Module.DoFullPowerSearch());
                }
            }
            if (result.Success && this.Module.Ts60.LockWanted)
            {
                result.MergeString(this.Module.StartTrackTarget());
            }
            return result;
        }
        private void AskIfDoFullPowerSearch()
        {
            MessageInput mi = new MessageInput(MessageType.Choice, R.StringTS60_AskIfDoFullPowerSearch)
            {
                ButtonTexts = new List<string> { R.T_YES, R.T_NO }
            };
            this.Module.tempMsg = mi.Show().TextOfButtonClicked;
        }

        /// <summary>
        /// Set all tooltips the controls inside the view.
        /// </summary>
        internal override void SetAllToolTips()
        {

            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            toolTip1.SetToolTip(this.Button_OnOff, R.StringTS60_ToolTip_Init);
            toolTip1.SetToolTip(this.comboBox_PortCom, R.StringTS60_ToolTip_port);
            toolTip1.SetToolTip(this.comboBox_TCP_IP, R.StringTS60_ToolTip_IP);
            toolTip1.SetToolTip(this.buttonGotoAll, R.StringTS60_ToolTip_GotoAll);
            toolTip1.SetToolTip(this.buttonGoto, R.StringTS60_ToolTip_Goto);
            toolTip1.SetToolTip(this.button_Distance, R.StringTS60_ToolTip_Dist);
            toolTip1.SetToolTip(this.button_Rec, R.StringTS60_ToolTip_Rec);
            toolTip1.SetToolTip(this.button_All, R.StringTS60_ToolTip_All);
            toolTip1.SetToolTip(this.button_face, R.StringTS60_ToolTip_Face);
            toolTip1.SetToolTip(this.button_ATR, R.StringTS60_ToolTip_Lock);
            toolTip1.SetToolTip(this.button_Lock, R.StringTS60_ToolTip_TargetLocked);
            toolTip1.SetToolTip(this.button_Search, R.StringTS60_ToolTip_PowerSearch);
            toolTip1.SetToolTip(this.button_Laser, R.StringTS60_ToolTip_RedLaser);
            toolTip1.SetToolTip(this.button_TargetType, R.StringTS60_ToolTip_TargetType);
            toolTip1.SetToolTip(this.labelMeteo, R.StringTS60_ToolTip_Weather);
            toolTip1.SetToolTip(this.label_Bulle, R.StringTS60_ToolTip_Bulle);
            toolTip1.SetToolTip(this.pictureBox_bulle, R.StringTS60_ToolTip_Bulle);
            toolTip1.SetToolTip(this.pictureBox_Battery, R.StringTS60_ToolTip_Battery);
            toolTip1.SetToolTip(this.pictureBox_OnOff, R.StringTS60_ToolTip_ONOFF);
        }
        /// <summary>
        /// Ouvre le reflector manager pour changer le default reflector
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_TargetType_Click(object sender, EventArgs e)
        {
            try
            {
                if (TaskToDo.Count == 0)
                {
                    Timer_check.Stop();
                    Timer_check.Enabled = false;
                    this.Module.ChangeDefaultReflector();
                    this.ChangeReflector();
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                if (ex is System.OperationCanceledException) ex.Data.Clear();
                else throw;
            }
        }
        /// <summary>
        /// Change le réflecteur dans le TS60
        /// </summary>
        public void ChangeReflector()
        {
            try
            {
                Result r = new Result();
                TsuTask task = new TsuTask()
                {
                    ActionName = "Change Reflector",

                    estimatedTimeInMilliSec = 5000,
                    timeOutInSec = 0,
                    preAction = () =>
                    {
                        Timer_check.Stop();
                        Timer_check.Enabled = false;
                        this.Module.CheckAndWaitIfBusy();
                    },
                    mainAction = () =>
                    {
                        r = this.Module.ChangeReflector();
                    },
                    postAction = () =>
                    {
                        Logs.Log.AddEntryAsResult(this._Module, r);
                        this.Atr_StateImage();
                        this.TargetLock_StateImage();
                        this.RedLaserStateImage();
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                    ExceptionAction = () =>
                    {
                        Timer_check.Enabled = true;
                        Timer_check.Start();
                        TaskToDo.Clear();
                        this.Module.IsBusyCommunicating = false;
                    },
                };
                if (TaskToDo.Count == 0)
                {
                    TaskToDo.Add(task);
                    //if (!doingTimerThings) RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Change Reflector");
                    RunTasksWithWaitingMessage(new List<TsuTask>() { task, }, "Change Reflector");
                }
                else
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, R.StringTS60_AnotherTaskInProgress)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.StringTS60_ClearTaskInProgress }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.StringTS60_ClearTaskInProgress) TaskToDo.Clear();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != R.String_Exception_InstrumentIsBusy) this.Module.IsBusyCommunicating = false;
                Timer_check.Enabled = true;
                Timer_check.Start();
                TaskToDo.Clear();
                throw;
            }

        }
        /// <summary>
        /// Show a message indicating that the ATR fine adjust has failed
        /// </summary>
        public void MessageATRFailed()
        {
            DsaFlag dsaFlag = DsaFlag.GetByNameOrAdd(this.Module.FinalModule.DsaFlags, "ShowMsgATRFailed");
            new MessageInput(MessageType.Warning, R.String_ATRFailed)
            {
                ButtonTexts = new List<string> { R.T_OK + "!" },
                DontShowAgain = dsaFlag
            }.Show();
        }

        public void SetSubView(SubView s)
        {
            SubView old = null;
            if (this.panel4SubViews.Controls.Count > 0) old = this.panel4SubViews.Controls[0] as SubView;

            if (old != s)
            {
                old?.Stop();
                this.panel4SubViews.Controls.Clear();
                this.panel4SubViews.Controls.Add(s);
                s.Show();
            }
        }

        private void ButtonCamera_Click(object sender, EventArgs e)
        {
            cameraView.Start();
        }

        private void ButtonJog_Click(object sender, EventArgs e)
        {
            jogView.Start();
        }
    }
}
