﻿namespace TSU.Common.Instruments.Device.TS60
{
    partial class CameraView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CameraView));
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonMoreB = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonLessB = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonMoreC = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonLessC = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSwitchCamera = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownZoom = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItem1x = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2x = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4x = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8x = new System.Windows.Forms.ToolStripMenuItem();
            this.videoView = new LibVLCSharp.WinForms.VideoView();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.toolStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.videoView)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.Right;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonMoreB,
            this.toolStripButtonLessB,
            this.toolStripButtonMoreC,
            this.toolStripButtonLessC,
            this.toolStripButtonSwitchCamera,
            this.toolStripDropDownZoom});
            this.toolStrip2.Location = new System.Drawing.Point(333, 5);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Padding = new System.Windows.Forms.Padding(1);
            this.toolStrip2.Size = new System.Drawing.Size(106, 461);
            this.toolStrip2.TabIndex = 16;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripButtonMoreB
            // 
            this.toolStripButtonMoreB.BackColor = System.Drawing.Color.Transparent;
            this.toolStripButtonMoreB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStripButtonMoreB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonMoreB.Image = global::TSU.Properties.Resources.At40x_Camera___Bmore;
            this.toolStripButtonMoreB.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonMoreB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonMoreB.Name = "toolStripButtonMoreB";
            this.toolStripButtonMoreB.Size = new System.Drawing.Size(101, 52);
            this.toolStripButtonMoreB.Tag = "";
            this.toolStripButtonMoreB.Text = "Increase brigthness";
            this.toolStripButtonMoreB.ToolTipText = "Increase brigthness";
            this.toolStripButtonMoreB.Click += new System.EventHandler(this.toolStripButtonMoreB_Click);
            // 
            // toolStripButtonLessB
            // 
            this.toolStripButtonLessB.BackColor = System.Drawing.Color.Transparent;
            this.toolStripButtonLessB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStripButtonLessB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonLessB.Image = global::TSU.Properties.Resources.At40x_Camera___Bless;
            this.toolStripButtonLessB.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonLessB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLessB.Name = "toolStripButtonLessB";
            this.toolStripButtonLessB.Size = new System.Drawing.Size(101, 52);
            this.toolStripButtonLessB.Tag = "";
            this.toolStripButtonLessB.Text = "Decrease brightness";
            this.toolStripButtonLessB.Click += new System.EventHandler(this.toolStripButtonLessB_Click);
            // 
            // toolStripButtonMoreC
            // 
            this.toolStripButtonMoreC.BackColor = System.Drawing.Color.Transparent;
            this.toolStripButtonMoreC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStripButtonMoreC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonMoreC.Image = global::TSU.Properties.Resources.At40x_Camera___Cmore;
            this.toolStripButtonMoreC.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonMoreC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonMoreC.Name = "toolStripButtonMoreC";
            this.toolStripButtonMoreC.Size = new System.Drawing.Size(101, 52);
            this.toolStripButtonMoreC.Tag = "";
            this.toolStripButtonMoreC.Text = "Increase contrast";
            this.toolStripButtonMoreC.ToolTipText = "Increase contrast";
            this.toolStripButtonMoreC.Click += new System.EventHandler(this.toolStripButtonMoreC_Click);
            // 
            // toolStripButtonLessC
            // 
            this.toolStripButtonLessC.BackColor = System.Drawing.Color.Transparent;
            this.toolStripButtonLessC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStripButtonLessC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonLessC.Image = global::TSU.Properties.Resources.At40x_Camera___Cless;
            this.toolStripButtonLessC.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonLessC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLessC.Name = "toolStripButtonLessC";
            this.toolStripButtonLessC.Size = new System.Drawing.Size(101, 52);
            this.toolStripButtonLessC.Tag = "";
            this.toolStripButtonLessC.Text = "Decrease Contrast";
            this.toolStripButtonLessC.ToolTipText = "Decrease Contrast";
            this.toolStripButtonLessC.Click += new System.EventHandler(this.toolStripButtonLessC_Click);
            // 
            // toolStripButtonSwitchCamera
            // 
            this.toolStripButtonSwitchCamera.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSwitchCamera.Image = global::TSU.Properties.Resources.TS60_OvcOac;
            this.toolStripButtonSwitchCamera.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonSwitchCamera.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSwitchCamera.Name = "toolStripButtonSwitchCamera";
            this.toolStripButtonSwitchCamera.Size = new System.Drawing.Size(101, 79);
            this.toolStripButtonSwitchCamera.Text = "Switch between overview and telescope cameras";
            this.toolStripButtonSwitchCamera.ToolTipText = "Switch between overview and telescope cameras";
            this.toolStripButtonSwitchCamera.Click += new System.EventHandler(this.toolStripButtonSwitchCamera_Click);
            // 
            // toolStripDropDownZoom
            // 
            this.toolStripDropDownZoom.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDropDownZoom.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1x,
            this.toolStripMenuItem2x,
            this.toolStripMenuItem4x,
            this.toolStripMenuItem8x});
            this.toolStripDropDownZoom.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownZoom.Image")));
            this.toolStripDropDownZoom.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownZoom.Name = "toolStripDropDownZoom";
            this.toolStripDropDownZoom.Size = new System.Drawing.Size(101, 20);
            this.toolStripDropDownZoom.Text = "toolStripDropDownZoom";
            this.toolStripDropDownZoom.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStripDropDownZoom_DropDownItemClicked);
            // 
            // toolStripMenuItem1x
            // 
            this.toolStripMenuItem1x.Name = "toolStripMenuItem1x";
            this.toolStripMenuItem1x.Size = new System.Drawing.Size(86, 22);
            this.toolStripMenuItem1x.Text = "1x";
            // 
            // toolStripMenuItem2x
            // 
            this.toolStripMenuItem2x.Name = "toolStripMenuItem2x";
            this.toolStripMenuItem2x.Size = new System.Drawing.Size(86, 22);
            this.toolStripMenuItem2x.Text = "2x";
            // 
            // toolStripMenuItem4x
            // 
            this.toolStripMenuItem4x.Name = "toolStripMenuItem4x";
            this.toolStripMenuItem4x.Size = new System.Drawing.Size(86, 22);
            this.toolStripMenuItem4x.Text = "4x";
            // 
            // toolStripMenuItem8x
            // 
            this.toolStripMenuItem8x.Name = "toolStripMenuItem8x";
            this.toolStripMenuItem8x.Size = new System.Drawing.Size(86, 22);
            this.toolStripMenuItem8x.Text = "8x";
            // 
            // videoView
            // 
            this.videoView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.videoView.BackColor = System.Drawing.Color.Black;
            this.videoView.Location = new System.Drawing.Point(8, 5);
            this.videoView.MediaPlayer = null;
            this.videoView.Name = "videoView";
            this.videoView.Size = new System.Drawing.Size(326, 460);
            this.videoView.TabIndex = 17;
            this.videoView.Text = "videoView1";
            this.videoView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.video_MouseClick);
            this.videoView.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.Video_PreviewKeyDown);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.textBox1.ForeColor = System.Drawing.Color.Snow;
            this.textBox1.Location = new System.Drawing.Point(377, 443);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(59, 20);
            this.textBox1.TabIndex = 25;
            this.textBox1.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.Video_PreviewKeyDown);
            // 
            // CameraView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 471);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.videoView);
            this.Controls.Add(this.toolStrip2);
            this.Name = "CameraView";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CameraView_FormClosed);
            this.Controls.SetChildIndex(this.toolStrip2, 0);
            this.Controls.SetChildIndex(this.buttonForFocusWithTabIndex0, 0);
            this.Controls.SetChildIndex(this.videoView, 0);
            this.Controls.SetChildIndex(this.textBox1, 0);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.videoView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolStripButtonMoreB;
        private System.Windows.Forms.ToolStripButton toolStripButtonLessB;
        private System.Windows.Forms.ToolStripButton toolStripButtonMoreC;
        private System.Windows.Forms.ToolStripButton toolStripButtonLessC;
        private LibVLCSharp.WinForms.VideoView videoView;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStripButton toolStripButtonSwitchCamera;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownZoom;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1x;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2x;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4x;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8x;
    }
}
