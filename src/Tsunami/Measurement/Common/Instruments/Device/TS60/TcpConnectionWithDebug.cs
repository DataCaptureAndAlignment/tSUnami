﻿using GeoComClient;
using System;
using System.Text;

namespace TSU.Common.Instruments.Device.TS60
{
    public class TcpConnectionWithDebug : TcpConnection
    {


        public TcpConnectionWithDebug(string serverIpAddress, int portNumber) : base(serverIpAddress, portNumber, false)
        {
        }

        StringBuilder sbSending;

        protected override void Send(byte[] bytesToSend)
        {
            AddToTraceBuffer("Sent:", bytesToSend, ref sbSending);
            base.Send(bytesToSend);
        }

        StringBuilder sbReceiving;

        protected override byte[] Receive(int length)
        {
            byte[] bytes = base.Receive(length);
            AddToTraceBuffer("Received:", bytes, ref sbReceiving);
            return bytes;
        }

        protected override byte ReceiveSingleByte()
        {
            byte @byte = base.ReceiveSingleByte();
            byte[] bytes = new byte[] { @byte };
            AddToTraceBuffer("Received:", bytes, ref sbReceiving);
            return @byte;
        }

        private void AddToTraceBuffer(string label, byte[] bytes, ref StringBuilder sb)
        {
            BeginTrace(label, ref sb);

            string s = Encoding.ASCII.GetString(bytes);

            //typical end, in block mode
            if (s.EndsWith("\r\n"))
            {
                sb.Append(s.Substring(0, s.Length - 2));
                EndTrace(ref sb);
            }
            //typical end, in byte mode
            else if (s == "\r")
            {
                EndTrace(ref sb);
            }
            else
            {
                sb.Append(s);
            }
        }

        private static void BeginTrace(string label, ref StringBuilder sb)
        {
            if (sb == null)
                sb = new StringBuilder(label);
        }

        private static void EndTrace(ref StringBuilder sb)
        {
            Debug.WriteInConsole(sb.ToString());
            sb = null;
        }
    }
}
