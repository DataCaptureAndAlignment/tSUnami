using EmScon;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TSU.Common.Elements;
using TSU.Views.Message;
using static GeoComClient.GeoComTypes;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using P = TSU.Views.Message.ProgressMessage;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Common.Instruments.Device.TS60
{
    [XmlType(TypeName = "TS60.Module")]
    public sealed partial class Module : PolarModule, IPolarModule, IMotorized, IConnectable
    {
        #region variables sp�cifiques Ts60
        [XmlIgnore]
        public Instrument Ts60
        {
            get
            {
                return Instrument as Instrument;
            }
        }

        [XmlIgnore]
        internal Camera OverviewCamera;

        [XmlIgnore]
        internal Camera TelescopeCamera;

        internal CameraType SelectedCameraType { get; set; }

        [XmlIgnore]
        internal Camera SelectedCamera => SelectedCameraType == CameraType.Overview ? OverviewCamera : TelescopeCamera;

        internal Ip IP;

        [XmlIgnore]
        internal Motor _motor;
        #endregion

        [XmlIgnore]
        public new View View
        {
            get
            {
                return _TsuView as View;
            }
            set
            {
                _TsuView = value;
            }
        }
        /// <summary>
        /// Timer pour �viter connection en m�me temps de diff�rents processus vers le TS60
        /// </summary>
        //[XmlIgnore]
        //private Timer TimerWaitForCommunication = new Timer() { Interval = 10,  };
        //[XmlIgnore]
        //private int timerTry = 0;
        //[XmlIgnore]
        //private bool bookCommunication = false;
        [XmlIgnore]
        private InstrumentTypes previousReflector = InstrumentTypes.CCR1_5;

        [XmlIgnore]
        public bool tcpConnected
        {
            get
            {
                return Ts60._TCPConnection != null && Ts60._TCPConnection.IsOpen;
            }
        }

        [XmlIgnore]
        public Result tempResult = new Result();
        internal RemoteVideoStatus remoteVideoStatus;

        [XmlIgnore]
        public Polar.Station.Module StationTheodoliteModule
        {
            get
            {
                return (ParentModule as Manager.Module).ParentModule as Polar.Station.Module;
            }
        }
        #region Constructeur Ts60
        public Module() : base() { }
        public Module(TSU.Module parentModule, I.Instrument i)
            : base(parentModule)
        {
            Instrument = i;
        }

        public override void Initialize()
        {
            if (this.ParentModule == null) // this allow to not create the view in the Connectivity-tests
            {
                this.CreateViewAutmatically = false;
            }
            base.Initialize();
            temperature = 20;// C
            pressure = 960;// hPa
            humidity = 60;// %
            //TimerWaitForCommunication.Tick += new System.EventHandler(this.TimerWaitForCommunication_Tick);
            OverviewCamera = new Camera(CAM_ID_TYPE.CAM_ID_OVC, this);
            TelescopeCamera = new Camera(CAM_ID_TYPE.CAM_ID_OAC, this);
            SelectedCameraType = CameraType.Overview;
            remoteVideoStatus = RemoteVideoStatus.Unknowm;
            IP = new Ip("192.168.0.100");
            _motor = new Motor(this);
        }
        #endregion

        internal override void OnMeasureToDoReceived()
        {
            base.OnMeasureToDoReceived();
            if (ToBeMeasuredData != null)
            {
                // Check if reflector defined;
                if ((ToBeMeasuredData as Polar.Measure).Distance.Reflector == null || (ToBeMeasuredData as Polar.Measure).Distance.Reflector._Name == R.String_Unknown)
                {
                    Logs.Log.AddEntryAsPayAttentionOf(this, R.T_No_REFLECTOR);
                    //this.InvokeOnApplicationDispatcher(this.View.DontAllowAll); why not to allow we have a way to ask for next measure info rmation : instrumentModule.WaitMeasurementDetails()
                    InvokeOnApplicationDispatcher(View.DontAllowGoto);
                }
                else
                {
                    if (previousReflector != (ToBeMeasuredData as Polar.Measure).Distance.Reflector._InstrumentType && InstrumentIsOn)
                    {
                        InvokeOnApplicationDispatcher(View.ChangeReflector);
                    }
                    //Set the number of measure // to be implemented for TS60
                    //(this.View as Ts60View).textBox_numberOfMeas.Text = (_ToBeMeasuredData as MeasureTheodolite).NumberOfMeasureToAverage.ToString();
                    if (InstrumentIsOn) InvokeOnApplicationDispatcher(View.AllowAll);
                    // allow goto
                    if ((ToBeMeasuredData as Polar.Measure).HasCorrectedAngles && InstrumentIsOn)
                        InvokeOnApplicationDispatcher(View.AllowGoto);
                    else
                        InvokeOnApplicationDispatcher(View.DontAllowGoto);
                }

                if (ToBeMeasuredData._Status is M.States.Good || _ToBeMeasureTheodoliteData.DirectMeasurementWanted)// this is the way to say 'i want  a measure now' without clicking gotoall button 
                {
                    _ToBeMeasureTheodoliteData.DirectMeasurementWanted = false; // to make sure this behaviour is not reproduce with a remeasurement for example
                    View.GotoAll();
                }
            }
            else
            {
                InvokeOnApplicationDispatcher(View.DontAllowGoto);
            }
        }
        /// <summary>
        /// Change reflector type inside the TS60
        /// </summary>
        /// <returns></returns>
        public Result ChangeReflector()
        {
            Result r = new Result();
            if (ToBeMeasuredData is Polar.Measure pm)
            {
                previousReflector = pm.Distance.Reflector._InstrumentType;
                switch (pm.Distance.Reflector._InstrumentType)
                {
                    case InstrumentTypes.CCR1_5:
                        r.MergeString(SetEDMModeToIRPrecise());
                        r.MergeString(SetReflectorTypeToUseReflector());
                        r.MergeString(SetPrismTypeToRRR());
                        r.MergeString(GetPrismCorrectionMM(out _));
                        r.MergeString(SetUserAtrState(true));
                        r.MergeString(SetUserLockState(true));
                        break;
                    case InstrumentTypes.CSCOTCH:
                        r.MergeString(SetEDMModeToTape());
                        r.MergeString(SetReflectorTypeToUseReflector());
                        r.MergeString(SetPrismTypeToTape());
                        r.MergeString(GetPrismCorrectionMM(out _));
                        break;
                    case InstrumentTypes.LASER:
                        r.MergeString(SetEDMModeToRL());
                        r.MergeString(SetReflectorTypeToNotUseReflector());
                        break;
                    default:
                        r.MergeString(SetEDMModeToIRPrecise());
                        r.MergeString(SetReflectorTypeToUseReflector());
                        r.MergeString(SetPrismTypeToRRR());
                        r.MergeString(GetPrismCorrectionMM(out _));
                        r.MergeString(SetUserAtrState(true));
                        r.MergeString(SetUserLockState(true));
                        break;
                }
            }
            return r;
        }

        public override void Goto()
        {
            //if the gotowanted = true, mean that the function has been called by measure in this module 
            if (GotoWanted)
                GotoTS60();
            else
                InvokeOnApplicationDispatcher(View.Goto);

        }

        public void GotoTS60()
        {
            base.Goto();
            try
            {
                Result r = new Result();
                Polar.Measure m = _BeingMeasuredTheodoliteData;
                if (m == null) m = _ToBeMeasureTheodoliteData;
                if (m == null) throw new Exception(R.T_NOWHERE_TO_GOTO);
                GetAngleForGoodFaceGoto(m, out Angles a);
                r.MergeString(MoveTo(a.Horizontal.Value, a.Vertical.Value));
                tempResult = r;
                if (!MeasureWanted) Ready();
            }
            catch (Exception ex)
            {
                if (ex.Message != R.String_Exception_InstrumentIsBusy) IsBusyCommunicating = false;
                throw;
            }
        }
        public override bool IsReadyToMeasure()
        {
            if (!InstrumentIsOn) throw new Exception("Instrument is Off?");
            if (ParentInstrumentManager.ParentModule is Polar.Station.Module)
            {
                if (BeingMeasured == null)
                {
                    if (ToBeMeasuredData == null)
                    {
                        WaitMeasurementDetails();
                    }
                    else
                    {
                        if ((ToBeMeasuredData as Polar.Measure).Distance.Reflector == null || (ToBeMeasuredData as Polar.Measure).Distance.Reflector._Name == R.String_Unknown)
                            throw new Exception(R.T_No_REFLECTOR);
                    }
                }
            }
            return true;
        }

        public override FaceType GetFace()
        {
            if (GetTs60Face().Success)
            {
                switch (Ts60._Face)
                {
                    case 1: return FaceType.Face1;
                    case 2: return FaceType.Face2;
                    default:
                        return FaceType.UnknownFace;
                }
            }
            else
            {
                return FaceType.UnknownFace;
            }
        }
        public override void MeasureLikeThis()
        {
            base.MeasureLikeThis();

            try
            {
                bool restoreVolume = false;

                // Si on est lock�, on coupe le son pour �viter le message "lock perdu"
                if (Ts60.LockStatus != MOT_LOCK_STATUS.MOT_LOCKED_OUT)
                {
                    SetVolume(0);
                    restoreVolume = true;
                }

                // 1. Face and goto
                ChangeFaceAndDoGoto();

                if (IsFirstMesaureOfFirstFace)
                {
                    if (CancelByMessageBeforeMeasure())
                    {
                        BeingMeasured = null;
                        denyMeasurement = true;
                        throw new CancelException();
                    }
                }

                // demande de faire le point� manuel si pas ATR
                if (!Ts60.AtrWanted && Ts60.LockStatus != MOT_LOCK_STATUS.MOT_LOCKED_IN)
                {
                    InvokeOnApplicationDispatcher(MsgPointeManuel);
                }

                //Force la remise ON du compensateur et de la correction des angles avant chaque mesure pour �viter qu'un utilisateur les d�sactive dans l'instrument.
                SetAngleSwitchOn();
                SetInclineSwitchOn();
                DoubleValue rawDistance;
                Angles rawAngles;
                tempResult = new Result();
                if (DistanceWanted)
                {
                    tempResult.MergeString(GetDistanceAndAngle(out rawDistance, out rawAngles, purgeDistance: true));
                    if (!tempResult.Success && rawDistance.Value == 0)
                    {
                        throw new Exception(R.T_COULDNT_MEASURE_DISTANCE);
                    }
                }
                else
                {
                    tempResult.MergeString(GetAverageAngle(out rawAngles, 1));
                    tempResult.MergeString(GetTemporaryDistanceFromInstrument(out rawDistance));
                }

                if (!tempResult.Success)
                {
                    throw new Exception(tempResult.AccessToken);
                }

                if (beingMeasured._Status is M.States.Cancel)
                    throw new CancelException();

                // Convvert to TSU objects
                while (rawAngles.Horizontal.Value < 0)
                    rawAngles.Horizontal.Value += 400d;
                while (rawAngles.Horizontal.Value >= 400d)
                    rawAngles.Horizontal.Value -= 400d;

                // if (this._BeingMeasuredTheodoliteData.Face == FaceType.Face2) SurveyCompute.TransformToOppositeFace(rawAngles); only for at?

                Polar.Measure m = _BeingMeasuredTheodoliteData;

                // Fill the measurementObject
                m._Date = DateTime.Now;
                m.Angles.Raw = rawAngles;

                if (DistanceWanted || TemporaryDistanceAvailableInInstrument)
                {
                    m.Distance.Raw = rawDistance;
                    m.Distance.WeatherConditions.dryTemperature = temperature;
                    m.Distance.WeatherConditions.pressure = pressure;
                    m.Distance.WeatherConditions.humidity = (int)humidity;
                    TemporaryDistanceAvailableInInstrument = false;
                }
                else
                {
                    m.Distance.Raw = new DoubleValue(); // to eraser value that can be there from goto need
                }
                DoMeasureCorrectionsThenNextMeasurementStep(m);

                // Si on �tait lock�, on a coup� le son temporairment, donc on le remet le son
                if (restoreVolume)
                    SetVolume(7);

                IsBusyCommunicating = false;
            }
            catch (CancelException)
            {
                denyMeasurement = true;
                StatusChange(string.Format(R.T_CANCEL), P.StepTypes.EndAll);
                DeclareMeasurementCancelled();
            }
            catch (Exception e)
            {
                //  this.StatusChange(string.Format("Measurement failed"), P.stepType.EndAll);
                StatusChange(string.Format(R.T_MEASUREMENT_FAILED), P.StepTypes.EndAll);
                JustMeasuredData = BeingMeasured;
                HaveFailed();
                State = new InstrumentState(InstrumentState.type.Idle);
                throw new Exception(e.Message);
            }
        }

        internal override void HaveFailed()
        {

            View.TaskToDo.Clear();
            base.HaveFailed();
        }

        /// <summary>
        /// message pour demander de faire le point� manuel
        /// </summary>
        private void MsgPointeManuel()
        {
            MessageInput mi = new MessageInput(MessageType.GoodNews, R.StringTS60_PointeManuel)
            {
                ButtonTexts = new List<string> { R.T_OK + '!' }
            };
            string temp = mi.Show().TextOfButtonClicked;
        }

        #region private Method Ts60
        internal string ErrorCode(int errorCode) //converti le num�ro de l'erreur renvoy� par la dll en message d'erreur pour le logbook
        {
            string message = "";
            switch (errorCode)
            {
                case 0:
                    message = "";
                    break;
                case 1:
                    message = "GRC_UNDEFINED: Unknown error, result unspecified.";
                    break;
                case 2:
                    message = "GRC_IVPARAM: Invalid parameter detected, Result Unspecified.";
                    break;
                case 3:
                    message = "GRC_IVRESULT: Invalid result.";
                    break;
                case 4:
                    message = "GRC_FATAL: Fatal error.";
                    break;
                case 5:
                    message = "GRC_NOT_IMPL: Not yet implemented.";
                    break;
                case 6:
                    message = "GRC_TIME_OUT: Function execution timed out. Result unspecified.";
                    break;
                case 7:
                    message = "GRC_SET_INCOMPL: Parameter setup for subsystem is incomplete.";
                    break;
                case 8:
                    message = "GRC_ABORT: Function execution has been aborted.";
                    break;
                case 9:
                    message = "GRC_NOMEMORY: Fatal error - not enough memory.";
                    break;
                case 10:
                    message = "GRC_NOINIT: Fatal error - subsystem not initialised.";
                    break;
                case 12:
                    message = "GRC_SHUTDOWN: Subsystem is down.";
                    break;
                case 13:
                    message = "GRC_SYSBUSY: System busy/already in use of another process. Cannot execute function.";
                    break;
                case 14:
                    message = "GRC_RC_HWFAILURE: Fatal error - hardware failure.";
                    break;
                case 15:
                    message = "GRC_RC_ABORT_APPL: Execution of application has been aborted (SHIFT-ESC).";
                    break;
                case 16:
                    message = "GRC_RC_LOW_POWER: Power is low. power remaining is about 30 minutes";
                    break;
                case 17:
                    message = " GRC_IVVERSION Invalid version of file, ... ";
                    break;
                case 18:
                    message = " GRC_BATT_EMPTY Battery is nearly empty. Time remaining is less than 1 minute. ";
                    break;
                case 20:
                    message = "GRC_NO_EVENT no event pending. ";
                    break;
                case 21:
                    message = "GRC_OUT_OF_TEMP out of temperature range ";
                    break;
                case 22:
                    message = "GRC_INSTRUMENT_TILT instrument tilting out of range";
                    break;
                case 23:
                    message = "GRC_COM_SETTING communication error ";
                    break;
                case 24:
                    message = "GRC_NO_ACTION GRC_TYPE Input 'do no action' ";
                    break;
                case 25:
                    message = "GRC_SLEEP_MODE Instr. run into the sleep mode ";
                    break;
                case 26:
                    message = "GRC_NOTOK Function not successfully completed. ";
                    break;
                case 27:
                    message = "GRC_NA: Not available ";
                    break;
                case 28:
                    message = "GRC_OVERFLOW: Overflow error ";
                    break;
                case 29:
                    message = "GRC_STOPPED System or subsystem has been stopped ";
                    break;
                case 257:
                    message = "GRC_ANG_ERROR: Angles and Inclinations not valid.";
                    break;
                case 258:
                    message = "GRC_ANG_INCL_ERROR: Inclinations not valid.";
                    break;
                case 259:
                    message = "GRC_ANG_BAD_ACC: value accuracy not reached.";
                    break;
                case 260:
                    message = "GRC_ANG_BAD_ANGLE_ACC: angle accuracy not reached.";
                    break;
                case 261:
                    message = "GRC_ANG_BAD_INCLIN_ACC: Inclination accuracy not reached.";
                    break;
                case 266:
                    message = "GRC_ANG_WRITE_PROTECTED: No write access allowed.";
                    break;
                case 267:
                    message = "GRC_ANG_OUT_OF_RANGE: Value out of range.";
                    break;
                case 268:
                    message = "GRC_ANG_IR_OCCURED: Function aborted due to interrupt.";
                    break;
                case 269:
                    message = "GRC_ANG_HZ_MOVED: Hz moved during incline measurement.";
                    break;
                case 270:
                    message = "GRC_ANG_OS_ERROR: Troubles with operation system";
                    break;
                case 271:
                    message = "GRC_ANG_DATA_ERROR: Overflow at parameter values.";
                    break;
                case 272:
                    message = "GRC_ANG_PEAK_CNT_UFL: Too less peak.";
                    break;
                case 273:
                    message = "GRC_GRC_ANG_TIMEOUT: reading timeout.";
                    break;
                case 274:
                    message = "GRC_ANG_TOO_MANY_EXPOS: Too many exposures wanted.";
                    break;
                case 275:
                    message = "GRC_ANG_PIX_CTRL_ERR: Picture height out of range.";
                    break;
                case 276:
                    message = "GRC_ANG_MAX_POS_SKIP: Positive exposure dynamic overflow.";
                    break;
                case 277:
                    message = "GRC_ANG_MAX_NEG_SKIP: Negative exposure dynamic overflow.";
                    break;
                case 278:
                    message = "GRC_ANG_EXP_LIMIT: Exposure time overflow.";
                    break;
                case 279:
                    message = "GRC_ANG_UNDER_EXPOSURE: picture underexposured.";
                    break;
                case 280:
                    message = "GRC_ANG_OVER_EXPOSURE: picture overexposured.";
                    break;
                case 300:
                    message = "GRC_ANG_TMANY_PEAKS: too many peaks detected.";
                    break;
                case 301:
                    message = "GRC_ANG_TLESS_PEAKS: too less peaks detected.";
                    break;
                case 302:
                    message = "GRC_ANG_PEAK_TOO_SLIM: peak too slim.";
                    break;
                case 303:
                    message = "GRC_ANG_PEAK_TOO_WIDE: peak too wide.";
                    break;
                case 304:
                    message = "GRC_ANG_BAD_PEAKDIFF: bad peak difference.";
                    break;
                case 305:
                    message = "GRC_ANG_UNDER_EXP_PICT: too less peak amplitude.";
                    break;
                case 306:
                    message = "GRC_ANG_PEAK_INHOMOGEN: inhomogen peak amplitudes.";
                    break;
                case 307:
                    message = "GRC_ANG_NO_DECOD_POSS: no peak decoding possible.";
                    break;
                case 308:
                    message = "GRC_ANG_UNSTABLE_DECOD: peak decoding not stable.";
                    break;
                case 309:
                    message = "GRC_ANG_TLESS_FPEAKS: too less valid finepeak.";
                    break;
                case 512:
                    message = "GRC_ATA_NOT_READY ATR-System: is not ready.";
                    break;
                case 513:
                    message = "GRC_ATA_NO_RESULT: Result isn't available yet.";
                    break;
                case 514:
                    message = "GRC_ATA_SEVERAL_TARGETS: Several Targets detected.";
                    break;
                case 515:
                    message = "GRC_ATA_BIG_SPOT: Spot is too big for analyse.";
                    break;
                case 516:
                    message = "GRC_ATA_BACKGROUND: Background is too bright.";
                    break;
                case 517:
                    message = "GRC_ATA_NO_TARGETS: No targets detected.";
                    break;
                case 518:
                    message = "GRC_ATA_NOT_ACCURAT: Accuracy worse than asked for.";
                    break;
                case 519:
                    message = "GRC_ATA_SPOT_ON_EDGE: Spot is on the edge of the sensing area.";
                    break;
                case 522:
                    message = "GRC_ATA_BLOOMING: Blooming or spot on edge detected.";
                    break;
                case 523:
                    message = "GRC_ATA_NOT_BUSY ATR: isn't in a continuous mode.";
                    break;
                case 524:
                    message = "GRC_ATA_STRANGE_LIGHT: Not the spot of the own target illuminator.";
                    break;
                case 525:
                    message = "GRC_ATA_V24_FAIL: Communication error to sensor (ATR).";
                    break;
                case 526:
                    message = "GRC_ATA_DECODE_ERROR: Received Arguments cannot be decoded";
                    break;
                case 527:
                    message = "GRC_ATA_HZ_FAIL: No Spot detected in Hz-direction.";
                    break;
                case 528:
                    message = "GRC_ATA_V_FAIL: No Spot detected in V-direction.";
                    break;
                case 529:
                    message = "GRC_ATA_HZ_STRANGE_L: Strange light in Hz-direction.";
                    break;
                case 530:
                    message = "GRC_ATA_V_STRANGE_L: Strange light in V-direction.";
                    break;
                case 531:
                    message = "GRC_ATA_SLDR_TRANSFER_PENDING: On multiple ATA_SLDR_OpenTransfer.";
                    break;
                case 532:
                    message = "GRC_ATA_SLDR_TRANSFER_ILLEGAL: No ATA_SLDR_OpenTransfer happened.";
                    break;
                case 533:
                    message = "GRC_ATA_SLDR_DATA_ERROR: Unexpected data format received.";
                    break;
                case 534:
                    message = "GRC_ATA_SLDR_CHK_SUM_ERROR: Checksum error in transmitted data.";
                    break;
                case 535:
                    message = "GRC_ATA_SLDR_ADDRESS_ERROR: Address out of valid range.";
                    break;
                case 536:
                    message = "GRC_ATA_SLDR_INV_LOADFILE: Firmware file has invalid format.";
                    break;
                case 537:
                    message = "GRC_ATA_SLDR_UNSUPPORTED: Current (loaded) firmware doesn't support upload.";
                    break;
                case 538:
                    message = "GRC_ATA_PS_NOT_READY: PS-System is not ready.";
                    break;
                case 539:
                    message = "GRC_ATA_ATR_SYSTEM_ERR: ATR system error";
                    break;
                case 769:
                    message = "GRC_EDM_SYSTEM_ERR: Fatal EDM sensor error. See for the exact reason the original EDM sensor error number. In the most cases a service problem. ";
                    break;
                case 770:
                    message = "GRC_EDM_INVALID_COMMAND: Invalid command or unknown command, see command syntax. ";
                    break;
                case 771:
                    message = "GRC_EDM_BOOM_ERR: Boomerang error. ";
                    break;
                case 772:
                    message = "GRC_EDM_SIGN_LOW_ERR: Received signal to low, prism to far away, or natural barrier, bad environment, etc. ";
                    break;
                case 773:
                    message = "GRC_EDM_DIL_ERR: obsolete ";
                    break;
                case 774:
                    message = "GRC_EDM_SIGN_HIGH_ERR:Received signal to strong, prism to near, stranger light effect. ";
                    break;
                case 775:
                    message = "GRC_EDM_TIMEOUT: Timeout, measuring time exceeded (signal too weak, beam interrupted,..) ";
                    break;
                case 776:
                    message = "GRC_EDM_FLUKT_ERR:t o much turbulences or distractions ";
                    break;
                case 777:
                    message = "GRC_EDM_FMOT_ERR: filter motor defective ";
                    break;
                case 778:
                    message = "GRC_EDM_DEV_NOT_INSTALLED: Device like EGL, DL is not installed. ";
                    break;
                case 779:
                    message = "GRC_EDM_NOT_FOUND: Search result invalid. For the exact explanation, see in the description of the called function. ";
                    break;
                case 780:
                    message = "GRC_EDM_ERROR_RECEIVED: Communication ok, but an error reported from the EDM sensor. ";
                    break;
                case 781:
                    message = "GRC_EDM_MISSING_SRVPWD: No service password is set. ";
                    break;
                case 782:
                    message = "GRC_EDM_INVALID_ANSWER: Communication ok, but an unexpected answer received. ";
                    break;
                case 783:
                    message = "GRC_EDM_SEND_ERR: Data send error, sending buffer is full. ";
                    break;
                case 784:
                    message = "GRC_EDM_RECEIVE_ERR: Data receive error, like parity buffer overflow. ";
                    break;
                case 785:
                    message = "GRC_EDM_INTERNAL_ERR: Internal EDM subsystem error. ";
                    break;
                case 786:
                    message = "GRC_EDM_BUSY: Sensor is working already, abort current measuring first. ";
                    break;
                case 787:
                    message = "GRC_EDM_NO_MEASACTIVITY: No measurement activity started. ";
                    break;
                case 788:
                    message = "GRC_EDM_CHKSUM_ERR: Calculated checksum, resp. received data wrong (only in binary communication mode possible). ";
                    break;
                case 789:
                    message = "GRC_EDM_INIT_OR_STOP_ERR: During start up or shut down phase an error occured. It is saved in the DEL buffer. ";
                    break;
                case 790:
                    message = "GRC_EDM_SRL_NOT_AVAILABLE: Red laser not available on this sensor HW. ";
                    break;
                case 791:
                    message = "GRC_EDM_MEAS_ABORTED: Measurement will be aborted (will be used for the laser security) ";
                    break;
                case 798:
                    message = "GRC_EDM_SLDR_TRANSFER_PENDING: Multiple OpenTransfer calls. ";
                    break;
                case 799:
                    message = "GRC_EDM_SLDR_TRANSFER_ILLEGAL: No open transfer happened. ";
                    break;
                case 800:
                    message = "GRC_EDM_SLDR_DATA_ERROR: Unexpected data format received. ";
                    break;
                case 801:
                    message = "GRC_EDM_SLDR_CHK_SUM_ERROR: Checksum error in transmitted data. ";
                    break;
                case 802:
                    message = "GRC_EDM_SLDR_ADDR_ERROR: Address out of valid range. ";
                    break;
                case 803:
                    message = "GRC_EDM_SLDR_INV_LOADFILE: Firmware file has invalid format. ";
                    break;
                case 804:
                    message = "GRC_EDM_SLDR_UNSUPPORTED: Current (loaded) firmware doesn't support upload. ";
                    break;
                case 808:
                    message = "GRC_EDM_UNKNOW_ERR: Undocumented error from the EDM sensor, should not occur. ";
                    break;
                case 818:
                    message = "GRC_EDM_DISTRANGE_ERR: Out of distance range (dist too small or large) ";
                    break;
                case 819:
                    message = "GRC_EDM_SIGNTONOISE_ERR: Signal to noise ratio too small ";
                    break;
                case 820:
                    message = "GRC_EDM_NOISEHIGH_ERR: Noise to high ";
                    break;
                case 821:
                    message = "GRC_EDM_PWD_NOTSET: Password is not set ";
                    break;
                case 822:
                    message = "GRC_EDM_ACTION_NO_MORE_VALID: Elapsed time between prepare und start fast measurement for ATR to long ";
                    break;
                case 823:
                    message = "GRC_EDM_MULTRG_ERR: Possibly more than one target (also a sensor error) ";
                    break;
                case 1283:
                    message = "GRC_TMC_NO_FULL_CORRECTION: Warning: measurement without full correction ";
                    break;
                case 1284:
                    message = "GRC_TMC_ACCURACY_GUARANTEE: Info: accuracy can not be guarantee ";
                    break;
                case 1285:
                    message = "GRC_TMC_ANGLE_OK: Warning: only angle measurement valid ";
                    break;
                case 1288:
                    message = "GRC_TMC_ANGLE_NOT_FULL_CORR: Warning: only angle measurement valid but without full correction ";
                    break;
                case 1289:
                    message = "GRC_TMC_ANGLE_NO_ACC_GUARANTY: Info: only angle measurement valid but accuracy can not be guarantee ";
                    break;
                case 1290:
                    message = "GRC_TMC_ANGLE_ERROR: Error: no angle measurement ";
                    break;
                case 1291:
                    message = "GRC_TMC_DIST_PPM: Error: wrong setting of PPM or MM on EDM ";
                    break;
                case 1292:
                    message = "GRC_TMC_DIST_ERROR: Error: distance measurement not done (no aim, etc.) ";
                    break;
                case 1293:
                    message = "GRC_TMC_BUSY: Error: system is busy (no measurement done) ";
                    break;
                case 1294:
                    message = "GRC_TMC_SIGNAL_ERROR: Error: no signal on EDM (only in signal mode) ";
                    break;
                case 2305:
                    message = "GRC_BMM_XFER_PENDING: Loading process already opened ";
                    break;
                case 2306:
                    message = "GRC_BMM_NO_XFER_OPEN: Transfer not opened ";
                    break;
                case 2307:
                    message = "GRC_BMM_UNKNOWN_CHARSET: Unknown character set ";
                    break;
                case 2308:
                    message = "GRC_BMM_NOT_INSTALLED: Display module not present ";
                    break;
                case 2309:
                    message = "GRC_BMM_ALREADY_EXIST: Character set already exists ";
                    break;
                case 2310:
                    message = "GRC_BMM_CANT_DELETE: Character set cannot be deleted ";
                    break;
                case 2311:
                    message = "GRC_BMM_MEM_ERROR: Memory cannot be allocated ";
                    break;
                case 2312:
                    message = "GRC_BMM_CHARSET_USED: Character set still used ";
                    break;
                case 2313:
                    message = "GRC_BMM_CHARSET_SAVED: Charset cannot be deleted or is protected ";
                    break;
                case 2314:
                    message = "GRC_BMM_INVALID_ADR: Attempt to copy a character block outside the allocated memory ";
                    break;
                case 2315:
                    message = "GRC_BMM_CANCELANDADR_ERROR: Error during release of allocated memory ";
                    break;
                case 2316:
                    message = "GRC_BMM_INVALID_SIZE: Number of bytes specified in header does not match the bytes read ";
                    break;
                case 2317:
                    message = "GRC_BMM_CANCELANDINVSIZE_ERROR: Allocated memory could not be released ";
                    break;
                case 2318:
                    message = "GRC_BMM_ALL_GROUP_OCC: Max. number of character sets already loaded ";
                    break;
                case 2319:
                    message = "GRC_BMM_CANT_DEL_LAYERS: Layer cannot be deleted ";
                    break;
                case 2320:
                    message = "GRC_BMM_UNKNOWN_LAYER: Required layer does not exist ";
                    break;
                case 2321:
                    message = "GRC_BMM_INVALID_LAYERLEN: Layer length exceeds maximum ";
                    break;
                case 3072:
                    message = "GRC_COM_ERO: Initiate Extended Runtime Operation (ERO).";
                    break;
                case 3073:
                    message = "GRC_COM_CANT_ENCODE: Cannot encode arguments in client.";
                    break;
                case 3074:
                    message = "GRC_COM_CANT_DECODE:Cannot decode results in client.";
                    break;
                case 3075:
                    message = "GRC_COM_CANT_SEND: Hardware error while sending.";
                    break;
                case 3076:
                    message = "GRC_COM_CANT_RECV: Hardware error while receiving.";
                    break;
                case 3077:
                    message = "GRC_COM_TIMEDOUT: Request timed out.";
                    break;
                case 3078:
                    message = "GRC_COM_WRONG_FORMAT: Packet format error.";
                    break;
                case 3079:
                    message = "GRC_COM_VER_MISMATCH: Version mismatch between client and server.";
                    break;
                case 3080:
                    message = "GRC_COM_CANT_DECODE_REQ: Cannot decode arguments in server.";
                    break;
                case 3081:
                    message = "GRC_COM_PROC_UNAVAIL: Unknown RPC, procedure ID invalid.";
                    break;
                case 3082:
                    message = "GRC_COM_CANT_ENCODE_REP: Cannot encode results in server.";
                    break;
                case 3083:
                    message = "GRC_COM_SYSTEM_ERR: Unspecified generic system error.";
                    break;
                case 3085:
                    message = "GRC_COM_FAILED: Unspecified error.";
                    break;
                case 3086:
                    message = "GRC_COM_NO_BINARY: Binary protocol not available.";
                    break;
                case 3087:
                    message = "GRC_COM_INTR: Call interrupted.";
                    break;
                case 3090:
                    message = "GRC_COM_REQUIRES_8DBITS: Protocol needs 8bit encoded characters.";
                    break;
                case 3093:
                    message = "GRC_COM_TR_ID_MISMATCH: Transaction ID mismatch error.";
                    break;
                case 3094:
                    message = "GRC_COM_NOT_GEOCOM: Protocol not recognisable.";
                    break;
                case 3095:
                    message = "GRC_COM_UNKNOWN_PORT: (WIN) Invalid port address.";
                    break;
                case 3099:
                    message = "GRC_COM_ERO_END: ERO is terminating.";
                    break;
                case 3100:
                    message = "GRC_COM_OVERRUN: Internal error: data buffer overflow.";
                    break;
                case 3101:
                    message = "GRC_COM_SRVR_RX_CHECKSUM_ERR: Invalid checksum on server side received.";
                    break;
                case 3102:
                    message = "GRC_COM_CLNT_RX_CHECKSUM_ERR: Invalid checksum on client side received.";
                    break;
                case 3103:
                    message = "GRC_COM_PORT_NOT_AVAILABLE: (WIN) Port not available.";
                    break;
                case 3104:
                    message = "GRC_COM_PORT_NOT_OPEN: (WIN) Port not opened.";
                    break;
                case 3105:
                    message = "GRC_COM_NO_PARTNER: (WIN) Unable to find TPS.";
                    break;
                case 3106:
                    message = "GRC_COM_ERO_NOT_STARTED: Extended Runtime Operation could not be started.";
                    break;
                case 3107:
                    message = "GRC_COM_CONS_REQ: Attention to send cons requests.";
                    break;
                case 3108:
                    message = "GRC_COM_SRVR_IS_SLEEPING: TPS has gone to sleep. Wait and try again.";
                    break;
                case 3109:
                    message = "GRC_COM_SRVR_IS_OFF: TPS has shut down. Wait and try again.";
                    break;
                case 8704:
                    message = "GRC_AUT_TIMEOUT: Position not reached ";
                    break;
                case 8705:
                    message = "GRC_AUT_DETENT_ERROR: Positioning not possible due to mounted EDM ";
                    break;
                case 8706:
                    message = "GRC_AUT_ANGLE_ERROR: Angle measurement error ";
                    break;
                case 8707:
                    message = "GRC_AUT_MOTOR_ERROR: Motorisation error ";
                    break;
                case 8708:
                    message = "GRC_AUT_INCACC: Position not exactly reached ";
                    break;
                case 8709:
                    message = "GRC_AUT_DEV_ERROR: Deviation measurement error ";
                    break;
                case 8710:
                    message = "GRC_AUT_NO_TARGET: No target detected ";
                    break;
                case 8711:
                    message = "GRC_AUT_MULTIPLE_TARGETS: Multiple target detected ";
                    break;
                case 8712:
                    message = "GRC_AUT_BAD_ENVIRONMENT: Bad environment conditions ";
                    break;
                case 8713:
                    message = "GRC_AUT_DETECTOR_ERROR: Error in target acquisition ";
                    break;
                case 8714:
                    message = "GRC_AUT_NOT_ENABLED: Target acquisition not enabled ";
                    break;
                case 8715:
                    message = "GRC_AUT_CALACC: ATR-Calibration failed ";
                    break;
                case 8716:
                    message = "GRC_AUT_ACCURACY: Target position not exactly reached ";
                    break;
                case 8717:
                    message = "Info: dist. measurement has been started ";
                    break;
                case 8718:
                    message = "GRC_AUT_SUPPLY_TOO_HIGH: external Supply voltage is too high ";
                    break;
                case 8719:
                    message = "GRC_AUT_SUPPLY_TOO_LOW: int. or ext. Supply voltage is too low ";
                    break;
                case 8720:
                    message = "GRC_AUT_NO_WORKING_AREA: working area not set ";
                    break;
                case 8721:
                    message = "GRC_AUT_ARRAY_FULL: power search data array is filled ";
                    break;
                case 8722:
                    message = "GRC_AUT_NO_DATA: no data available ";
                    break;
                case 12544:
                    message = "GRC_KDM_NOT_AVAILABLE KDM device is not available.";
                    break;
                case 13056:
                    message = "GRC_FTR_FILEACCESS: File access error ";
                    break;
                case 13057:
                    message = "GRC_FTR_WRONGFILEBLOCKNUMBER: block number was not the expected one ";
                    break;
                case 13058:
                    message = "GRC_FTR_NOTENOUGHSPACE: not enough space on device to proceed uploading ";
                    break;
                case 13059:
                    message = "GRC_FTR_INVALIDINPUT: Rename of file failed. ";
                    break;
                case 13060:
                    message = "GRC_FTR_MISSINGSETUP: invalid parameter as input ";
                    break;
                default:
                    message = "GRC_Unknown error message";
                    break;
            }
            return message;
        }

        private Result OpenConnection(string IpAdress, int portCom) //Connection au TS60 par TCP/IP en WLAN).
        {
            Result result = new Result();
            try
            {
                //this.Ts60._TCPConnection = new TcpConnection(IpAdress, portCom, true);
                Ts60._TCPConnection = new TcpConnectionWithDebug(IpAdress, portCom);
                Ts60._TCPConnection.Timeout = new TimeSpan(0, 0, 0, 15, 0);
                Ts60._Geocom.SetCommunicationChannel(Ts60._TCPConnection);
                bool opened = Ts60._TCPConnection.Open();
                int errorCode = 0;
                try
                {
                    errorCode = Ts60._Geocom.COM_NullProc();
                }
                catch (Exception)
                {
                    new MessageInput(MessageType.Critical, "Is the TS60 setup to communicate with RS232 cable instead of Wi-Fi?").Show();
                    errorCode = 23;
                }
                
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = string.Format("Open port {0} on {1} OK", portCom, IpAdress);
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Open port error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Open port error: " + result.ErrorMessage;
            }
            return result;
        }
        private Result SetInclineSwitchOn() //Met sur ON InclineSwitch
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.TMC_SetInclineSwitch(ON_OFF_TYPE.ON);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Turned Incline Switch ON.";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Incline Switch error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Incline Switch error: " + result.ErrorMessage;
            }
            return result;
        }
        private Result GetInclineSwitchState() //V�rifie �tat InclineSwitch
        {
            Result result = new Result();
            try
            {
                ON_OFF_TYPE inclineSwitchOnOff;
                int errorCode = Ts60._Geocom.TMC_GetInclineSwitch(out inclineSwitchOnOff);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "State Incline switch: " + inclineSwitchOnOff.ToString();
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Incline Switch error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Incline Switch error: " + result.ErrorMessage;
            }

            return result;
        }
        /// <summary>
        /// Change le reflecteur par d�faut de la station
        /// </summary>
        internal void ChangeDefaultReflector()
        {
            if (StationTheodoliteModule != null)
            {
                StationTheodoliteModule.Reflector(StationTheodoliteModule.stationParameters.DefaultMeasure);
            }
        }

        private Result SetAngleSwitchOn() //Met sur On Angle Switch
        {
            Result result = new Result();
            try
            {
                TMC_ANG_SWITCH angleSwitch = new TMC_ANG_SWITCH();
                ON_OFF_TYPE on = ON_OFF_TYPE.ON;
                angleSwitch.eCollimationCorr = on;
                angleSwitch.eInclineCorr = on;
                angleSwitch.eStandAxisCorr = on;
                angleSwitch.eTiltAxisCorr = on;
                int errorCode = Ts60._Geocom.TMC_SetAngSwitch(angleSwitch); //GeoComS2K_dll.VB_TMC_SetAngSwitch(ref angleSwitch);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Turned Angle switch ON";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Angle Switch error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Angle Switch error: " + result.ErrorMessage;
            }
            return result;
        }
        private Result GetAngleSwitchState() //V�rifie �tat Angle Switch
        {
            Result result = new Result();
            try
            {
                TMC_ANG_SWITCH angleSwitch = new TMC_ANG_SWITCH();
                ON_OFF_TYPE off = ON_OFF_TYPE.OFF;
                angleSwitch.eCollimationCorr = off;
                angleSwitch.eInclineCorr = off;
                angleSwitch.eStandAxisCorr = off;
                angleSwitch.eTiltAxisCorr = off;
                int errorCode = Ts60._Geocom.TMC_GetAngSwitch(out angleSwitch);
                if (errorCode == 0)
                {
                    result.Success = true;
                    //   result.AccessToken = string.Format("State Angle switch Collimation Correction: {0} \r\nState Angle switch Incline Correction: {1} \r\nState Angle switch Standing Axis: {2} \r\nState Angle switch Tilt Axis: {3}", angleSwitch.eCollimationCorr.ToString(), angleSwitch.eInclineCorr.ToString(), angleSwitch.eStandAxisCorr.ToString(), angleSwitch.eTiltAxisCorr.ToString());
                    result.AccessToken = $"{R.T_STATE_ANGLE_SWITCH_COLLIMATION_CORRECTION}: {angleSwitch.eCollimationCorr} \r\n{R.T_STATE_ANGLE_SWITCH_INCLINE_CORRECTION}: {angleSwitch.eInclineCorr} \r\n{R.T_STATE_ANGLE_SWITCH_STANDING_AXIS}: {angleSwitch.eStandAxisCorr} \r\n{R.T_STATE_ANGLE_SWITCH_TILT_AXIS}: {angleSwitch.eTiltAxisCorr}";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Angle Switch error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Angle Switch error: " + result.ErrorMessage;
            }
            return result;
        }
        private Result GetPPMAtmParameters(out TMC_ATMOS_TEMPERATURE atmParameters) //Donne les param�tres atmosph�riques actifs dans le TDA
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.TMC_GetAtmCorr(out atmParameters);
                if (errorCode == 0)
                {
                    result.Success = true;
                    //    result.AccessToken = string.Format("Atmospheric PPM dry temperature is {0} �C \r\nAtmospheric PPM wet temperature is {1} �C \r\nAtmospheric PPM wet temperature is {2} mb \r\nAtmospheric PPM wet temperature is {3}�C \r\nEDM wavelength is {4}", Math.Round(atmParameters.dDryTemperature, 2), Math.Round(atmParameters.dWetTemperature, 2), Math.Round(atmParameters.dPressure, 2), Math.Round(atmParameters.dWetTemperature, 1), Math.Round(atmParameters.dLambda, 3));
                    result.AccessToken = $"{R.T_ATMOSPHERIC_PPM_DRY_TEMPERATURE_IS} {Math.Round(atmParameters.dDryTemperature, 2)} �C \r\n{R.T_ATMOSPHERIC_PPM_WET_TEMPERATURE_IS} {Math.Round(atmParameters.dWetTemperature, 2)} �C \r\n{R.T_ATMOSPHERIC_PPM_WET_TEMPERATURE_IS} {Math.Round(atmParameters.dPressure, 2)} mb \r\n{R.T_ATMOSPHERIC_PPM_WET_TEMPERATURE_IS} {Math.Round(atmParameters.dWetTemperature, 1)}�C \r\n{R.T_EDM_WAVELENGTH_IS} {Math.Round(atmParameters.dLambda, 3)}";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Getting PPM atmospheric error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Getting PPM atmospheric error: " + result.ErrorMessage;
                atmParameters = new TMC_ATMOS_TEMPERATURE();
            }
            return result;
        }
        private Result SetPPMAtmToZero() //met � z�ro le PPM atmosph�rique
        {
            Result result = new Result();
            try
            {
                result = GetPPMAtmParameters(out TMC_ATMOS_TEMPERATURE atmCondition); //permet de garder la longueur d'onde par d�faut du Ts60
                if (result.Success)
                {
                    return result;
                }
                atmCondition.dDryTemperature = 12;
                atmCondition.dWetTemperature = 8.25;
                atmCondition.dPressure = 1013.25;
                int errorCode = Ts60._Geocom.TMC_SetAtmCorr(atmCondition);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Atmospheric PPM sets to zero.";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Setting PPM atmospheric error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Setting PPM atmospheric error: " + result.ErrorMessage;
            }

            return result;
        }
        private Result GetRefractionParameters(TMC_REFRACTION refractionParameters) //Donne la correction de refraction active
        {
            Result result = new Result();
            try
            {
                refractionParameters.dEarthRadius = 0;
                refractionParameters.dRefractiveScale = 0;
                refractionParameters.eRefOn = ON_OFF_TYPE.ON;
                int errorCode = Ts60._Geocom.TMC_GetRefractiveCorr(out refractionParameters);
                if (errorCode == 0)
                {
                    result.Success = true;
                    //    result.AccessToken = string.Format("Refraction correction is {0}\r\nEarth radius = {1} M \r\nRefractive scale = {2}", refractionParameters.eRefOn.ToString(), Math.Round(refractionParameters.dEarthRadius, 0).ToString(), Math.Round(refractionParameters.dRefractiveScale, 2).ToString());
                    result.AccessToken = $"{R.T_REFRACTION_CORRECTION_IS} {refractionParameters.eRefOn}\r\n{R.T_EARTH_RADIUS} = {Math.Round(refractionParameters.dEarthRadius, 0)} M \r\n{R.T_REFRACTIVE_SCALE} = {Math.Round(refractionParameters.dRefractiveScale, 2)}";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Refraction correction error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Refraction correction error: " + result.ErrorMessage;
            }
            return result;
        }
        private Result ShowPowerManagementParameters() //Donne les param�tres power management du TPS
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.SUP_GetConfig(
                    out ON_OFF_TYPE lowTempOnOff,
                    out SUP_AUTO_POWER autoPower,
                    out int timeOut);
                if (errorCode == 0)
                {
                    result.Success = true;
                    //    result.AccessToken = string.Format("TPS low temperature control is {0}\r\nTPS auto power mode is {1} \r\nTime out = {2} ms", lowTempOnOff.ToString(), autoPower.ToString(), timeOut.ToString());
                    result.AccessToken = $"{R.T_TPS_LOW_TEMPERATURE_CONTROL_IS} {lowTempOnOff}\r\n{R.T_TPS_AUTO_POWER_MODE_IS} {autoPower} \r\n{R.T_TIME_OUT} = {timeOut} ms";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Setting power management error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Setting power management error: " + result.ErrorMessage;
            }
            return result;
        }
        private Result SetEDMModeToIRPrecise() //Met sur le mode EDM sur mesure sp�ciale
        {
            Result result = new Result();
            try
            {
                EDM_MODE edmMode = EDM_MODE.EDM_PRECISE_IR;
                int errorCode = Ts60._Geocom.TMC_SetEdmMode(edmMode);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "EDM Mode sets to IR precise measurement.";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
            }

            return result;
        }
        private Result ShowEDMMode() //Montre le mode EDM actif du TPS
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.TMC_GetEdmMode(out EDM_MODE edmMode);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Active EDM Mode is " + edmMode.ToString();
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
            }

            return result;
        }

        private Result ShowCommunicationTimeOut() //Donne le temps d'attente maximum avant erreur par Geocom
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.COM_NullProc();
                if (errorCode == 0)
                {
                    int timeOut = (int)Ts60._TCPConnection.Timeout.TotalMilliseconds;
                    result.Success = true;
                    result.AccessToken = "TPS communication time out = " + timeOut.ToString() + " ms";
                    result.ErrorMessage = "";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Show time out error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Show time out error: " + result.ErrorMessage;
            }
            return result;
        }


        private Result GetOneDistance(out DoubleValue slopeDistance)
        {
            Result result = GetDistanceAndAngle(out slopeDistance, out Angles dummy);
            return result;
        }

        private Result GetTemporaryDistanceFromInstrument(out DoubleValue slopeDistance)
        {
            Result result = ReadActualObservations(out slopeDistance, out Angles dummy);
            result.Success = true; // because we dont always have a distance reacdy
            result.MergeString(PurgeDistance());
            return result;
        }


        public Result GetDistanceAndAngle(out DoubleValue slopeDistance, out Angles angles, bool purgeDistance = false)
        {
            Result result = new Result();
            result.MergeString(PurgeDistance());
            if (Ts60.AtrWanted)
                result.MergeString(FineAdjust());

            result.MergeString(LaunchDistanceMeasurement());
            result.MergeString(ReadActualObservations(out slopeDistance, out angles));
            TemporaryDistanceAvailableInInstrument = true;
            if (purgeDistance)
            {
                result.MergeString(PurgeDistance()); //removed to be able to do DIST+REC
                TemporaryDistanceAvailableInInstrument = false;
            }

            return result;
        }
        private Result ReadActualObservations(out DoubleValue distance, out Angles angles)
        {
            Result result = new Result();
            angles = new Angles();
            distance = new DoubleValue(0, double.NaN);

            try
            {
                TMC_INCLINE_PRG measureIncline = TMC_INCLINE_PRG.TMC_MEA_INC;
                int waitTime = 20000;

                int errorCode = Ts60._Geocom.TMC_GetSimpleMea(waitTime, out TMC_HZ_V_ANG TMC_angles, out double slopeD, measureIncline);

                angles.Horizontal.Value = TMC_angles.dHz / Math.PI * 200;
                angles.Vertical.Value = TMC_angles.dV / Math.PI * 200;
                distance.Value = slopeD;

                if (errorCode == 0 && distance.Value != 0)
                {
                    result.Success = true;
                    result.AccessToken = "TPS read the actual observations in memory";
                    result.ErrorMessage = "";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Read of observations error: " + result.ErrorMessage;

                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Read of observations error: " + result.ErrorMessage;

            }
            return result;
        }
        private void EmqCalculation(DoubleValue[] setOfMeasure, out DoubleValue average)
        {
            average = new DoubleValue();
            average.Value = 0;
            average.Sigma = 0;

            for (int i = 0; i < setOfMeasure.Length; i++)
            {
                average.Value += setOfMeasure[i].Value;
            }
            average.Value = average.Value / setOfMeasure.Length;

            for (int i = 0; i < setOfMeasure.Length; i++)
            {
                average.Sigma += Math.Pow(setOfMeasure[i].Value - average.Value, 2);

            }
            average.Sigma = Math.Sqrt(average.Sigma) / (setOfMeasure.Length - 1);
        }
        private Result PurgeDistance()
        {
            Result result = new Result();
            try
            {
                TMC_MEASURE_PRG clear = TMC_MEASURE_PRG.TMC_CLEAR;
                TMC_INCLINE_PRG measureIncline = TMC_INCLINE_PRG.TMC_PLANE_INC;
                int errorCode = Ts60._Geocom.TMC_DoMeasure(clear, measureIncline);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Purge distance OK";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = string.Format("Purge distance error: {0}", result.ErrorMessage);
                    //EndGeocom();
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = string.Format("Purge distance error: {0}", result.ErrorMessage);
            }
            return result;
        }
        private Result LaunchDistanceMeasurement()
        {
            Result result = new Result();
            try
            {
                TMC_MEASURE_PRG def_Dist = TMC_MEASURE_PRG.TMC_DEF_DIST;
                TMC_INCLINE_PRG measureIncline = TMC_INCLINE_PRG.TMC_MEA_INC;
                int errorCode = Ts60._Geocom.TMC_DoMeasure(def_Dist, measureIncline);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "TPS did a measure of distance";
                    result.ErrorMessage = "";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Measurement of distance error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Measurement of distance error: " + result.ErrorMessage;
            }
            return result;
        }

        public void LockUnLock()
        {
            SwitchLock();
        }

        private Result CloseConnection() //Fermeture Connection au TDA.
        {
            Result result = new Result();

            //Ne pas �teindre automatiquement TS60
            //if (this.CheckConnection().Success)
            //{
            if (tcpConnected)
                Ts60._TCPConnection.Close();
            result.Success = true;
            result.AccessToken = "Close COM port OK";
            //}
            //else
            //{
            //    result.Success = false;
            //}

            return result;
        }


        //private Result EndGeocom() //Fermeture Geocom. // pas de fermeture Geocom avec TS60
        //{
        //    Result result = new Result();
        //    short errorCode = GeoComS2K_dll.VB_COM_End();
        //    if (errorCode == 0)
        //    {
        //        result.Success = true;
        //        result.AccessToken = "Close Geocom OK";
        //    }
        //    else
        //    {
        //        result.Success = false;
        //        result.ErrorMessage = ErrorCode(errorCode);
        //        result.AccessToken = "Close Geocom error: " + result.ErrorMessage.ToString();
        //    }
        //    return result;
        //}
        private Result SetEDMModeToRL() //Met sur le mode EDM sur mesure sans reflecteur standard
        {
            Result result = new Result();
            try
            {
                EDM_MODE edmMode = EDM_MODE.EDM_SINGLE_SRANGE;
                int errorCode = Ts60._Geocom.TMC_SetEdmMode(edmMode);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "EDM Mode sets to measurement without reflector.";
                    //result.MergeString(this.SetTargetLOCKOnOff(false));
                    result.MergeString(SetUserAtrState(false));
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
                }
                result.MergeString(SetUserAtrState(false));
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
            }
            return result;
        }
        #endregion

        #region public method Ts60
        public override Result Disconnect() //Fermerture du port COM et de Geocom
        {
            //Result result = new Result();
            Result resultGlobal = new Result();

            resultGlobal.MergeString(CloseConnection());
            //result = EndGeocom();
            //resultGlobal.MergeString(result);
            return resultGlobal;
        }



        public Result CheckConnection() //Teste si connection est active.
        {
            Result result = new Result();
            try
            {
                int errorCode = 9999;
                int i = 0;
                Ts60._TCPConnection.Timeout = new TimeSpan(0, 0, 0, 2, 0);
                while (errorCode != 0 && i < 3)
                {
                    errorCode = Ts60._Geocom.COM_NullProc();
                    i++;
                }
                Ts60._TCPConnection.Timeout = new TimeSpan(0, 0, 0, 15, 0);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Check connection OK";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Check connection error: " + result.ErrorMessage.ToString();
                }
            }
            catch (Exception)
            {
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Check connection error: " + result.ErrorMessage.ToString();
            }

            return result;
        }
        public Result CorrectionAngleCalculation(ref double CorrHz, ref double CorrV)
        {
            Result result = new Result();
            //Result resultGlobal = new Result();
            Angles anglesCorrectionFace1_1;
            Angles anglesCorrectionFace2_1;
            //Angles anglesCorrectionFace1_2 = new Angles();
            //Angles anglesCorrectionFace2_2 = new Angles();
            Angles averageAnglesCorrection = new Angles();
            Compensator compensator;
            result.MergeString(GoFace(1));
            result.MergeString(FineAdjust());
            result.MergeString(GetAngles(out anglesCorrectionFace1_1, out compensator));

            result.MergeString(GoFace(2));
            result.MergeString(FineAdjust());
            result.MergeString(GetAngles(out anglesCorrectionFace2_1, out compensator));

            result.MergeString(GoFace(1));
            averageAnglesCorrection.Horizontal.Value = (anglesCorrectionFace1_1.Horizontal.Value > Math.PI) ? ((anglesCorrectionFace1_1.Horizontal.Value + anglesCorrectionFace2_1.Horizontal.Value + Math.PI) / 2) : ((anglesCorrectionFace1_1.Horizontal.Value + anglesCorrectionFace2_1.Horizontal.Value - Math.PI) / 2);
            averageAnglesCorrection.Vertical.Value = (anglesCorrectionFace1_1.Vertical.Value - anglesCorrectionFace2_1.Vertical.Value + 2 * Math.PI) / 2;
            CorrHz = averageAnglesCorrection.Horizontal.Value - anglesCorrectionFace1_1.Horizontal.Value;
            CorrV = averageAnglesCorrection.Vertical.Value - anglesCorrectionFace1_1.Vertical.Value;
            return result;
        }
        public Result Initialize(int numberOfIterationForCollimationError, bool checkInitialization) //fait l'initilisation et v�rifie ensuite si l'initialisation est OK.Permet de calculer la correction de collimation � appliquer en face1)
        {
            Result result = InitializeSensor();
            if (numberOfIterationForCollimationError > 0 && result.Success)
            {
                double SumCorrHz = 0;
                double SumCorrV = 0;
                double CorrHz = 0;
                double CorrV = 0;
                for (int i = 0; i < numberOfIterationForCollimationError; i++)
                {
                    result.MergeString(CorrectionAngleCalculation(ref CorrHz, ref CorrV));
                    SumCorrHz += CorrHz;
                    SumCorrV += CorrV;
                }
                Ts60._AngleCorrection.Horizontal.Value = SumCorrHz / numberOfIterationForCollimationError;
                Ts60._AngleCorrection.Vertical.Value = SumCorrV / numberOfIterationForCollimationError;
            }
            if (checkInitialization && result.Success)
            {
                result.MergeString(CheckInitialization());
            }
            return result;
        }
        public override Result InitializeSensor() // Initialisation compl�te du TDA avec tous les param�tres habituels par d�faut
        {
            Result resultGlobal = new Result();
            resultGlobal.MergeString(SetAngleSwitchOn());
            if (!resultGlobal.Success) { return resultGlobal; }
            resultGlobal.MergeString(SetEDMModeToIRPrecise());
            if (!resultGlobal.Success) { return resultGlobal; }
            resultGlobal.MergeString(SetReflectorTypeToUseReflector());
            if (!resultGlobal.Success) { return resultGlobal; }
            resultGlobal.MergeString(SetPrismTypeToRRR());
            if (!resultGlobal.Success) { return resultGlobal; }
            //double prismCorrection = 0;
            //resultGlobal.MergeString(SetPrismCorrectionMM(ref prismCorrection));
            //if (!resultGlobal.Success) { return resultGlobal; }
            resultGlobal.MergeString(GetPrismCorrectionMM(out _));
            if (!resultGlobal.Success) { return resultGlobal; }
            resultGlobal.MergeString(SetRefractionTozero());
            if (!resultGlobal.Success) { return resultGlobal; }
            resultGlobal.MergeString(SetPPMAtmToZero());
            if (!resultGlobal.Success) { return resultGlobal; }
            resultGlobal.MergeString(SetAtrAdjustMode(AUT_ADJMODE.AUT_NORM_MODE));
            if (!resultGlobal.Success) { return resultGlobal; }
            resultGlobal.MergeString(SetInclineSwitchOn());
            if (!resultGlobal.Success) { return resultGlobal; }
            resultGlobal.MergeString(SetUserAtrState(true));
            if (!resultGlobal.Success) { return resultGlobal; }
            resultGlobal.MergeString(SetUserLockState(false));
            if (!resultGlobal.Success) { return resultGlobal; }
            resultGlobal.MergeString(GetBatteryLevel());
            if (!resultGlobal.Success) { return resultGlobal; }
            resultGlobal.MergeString(GetInternalTemperature());
            if (!resultGlobal.Success) { return resultGlobal; }
            resultGlobal.MergeString(SetRedLaserOnOff(false));
            if (!resultGlobal.Success) { return resultGlobal; }
            resultGlobal.MergeString(SetLockFlyMode(true));
            if (!resultGlobal.Success) { return resultGlobal; }
            resultGlobal.MergeString(GetLockStatus());
            // Initialize the wanted lock state according to the instrument's status
            if (Ts60.LockStatus == MOT_LOCK_STATUS.MOT_LOCKED_IN)
                Ts60.LockWanted = true;
            else
                Ts60.LockWanted = false;
            return resultGlobal;
        }

        public Result CheckInitialization() // Donne tous les param�tres actifs du TDA pour v�rifier l'initialisation
        {
            Result result;
            Result resultGlobal = new Result();
            resultGlobal.Success = true;
            result = CheckConnection();
            resultGlobal.MergeString(result);
            result = GetSerialNumber();
            resultGlobal.MergeString(result);
            result = GetInclineSwitchState();
            resultGlobal.MergeString(result);
            result = GetAngleSwitchState();
            resultGlobal.MergeString(result);
            result = GetPrismCorrectionMM(out _);
            resultGlobal.MergeString(result);
            result = GetPPMAtmParameters(out _);
            resultGlobal.MergeString(result);
            TMC_REFRACTION refractionParameters = new TMC_REFRACTION();
            result = GetRefractionParameters(refractionParameters);
            resultGlobal.MergeString(result);
            result = ShowPowerManagementParameters();
            resultGlobal.MergeString(result);
            result = GetLockStatus();
            resultGlobal.MergeString(result);
            result = ShowEDMMode();
            resultGlobal.MergeString(result);
            result = GetAtrAdjustMode(out _);
            resultGlobal.MergeString(result);
            result = ShowCommunicationTimeOut();
            resultGlobal.MergeString(result);
            result = GetBatteryLevel();
            resultGlobal.MergeString(result);
            result = GetInternalTemperature();
            resultGlobal.MergeString(result);
            return resultGlobal;
        }
        public Result GetSerialNumber() //Donne le num�ro de s�rie TPS
        {
            Result result = new Result();
            try
            {
                int TpsSerialNumber = 0;
                int errorCode = Ts60._Geocom.CSV_GetInstrumentNo(out TpsSerialNumber);
                if (errorCode == 0)
                {
                    if (Ts60._SerialNumber != TpsSerialNumber.ToString())
                    {
                        Disconnect();
                        StationTheodoliteModule.CheckAndChangeInstrument(TpsSerialNumber.ToString());
                        result.Success = false;
                        result.AccessToken = "TPS SN" + TpsSerialNumber.ToString() + "is not the good one, please reconnect";
                    }
                    else
                    {
                        result.Success = true;
                        result.AccessToken = "TPS serial number is " + TpsSerialNumber.ToString();
                    }
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Get serial number error: " + result.ErrorMessage.ToString();
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Get serial number error: " + result.ErrorMessage.ToString();
            }
            return result;
        }
        public Result GetBatteryLevel()
        {
            Result result = new Result();
            try
            {
                ushort batteryLevel;
                CSV_POWER_PATH actualPowerSource;
                CSV_POWER_PATH suggestedPowerSource;
                int errorCode = Ts60._Geocom.CSV_CheckPower(out batteryLevel, out actualPowerSource, out suggestedPowerSource);
                if (errorCode == 0)
                {
                    Ts60._Battery = batteryLevel;
                    string batteryState = "";
                    if (batteryLevel >= 40)
                    {
                        batteryState = "Good";
                    }
                    else
                    {
                        batteryState = "Empty";
                    }
                    result.Success = true;
                    result.AccessToken = string.Format("Battery Level = {0} % is {1}", batteryLevel.ToString(), batteryState);
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Get battery level error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Get battery level error: " + result.ErrorMessage;
            }
            return result;
        }
        public Result GetInternalTemperature()
        {
            Result result = new Result();
            try
            {
                double TpsInternalTemperature;
                int errorCode = Ts60._Geocom.CSV_GetIntTemp(out TpsInternalTemperature);
                Ts60._Tinterne = TpsInternalTemperature;
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "TPS internal temperature = " + Math.Round(TpsInternalTemperature, 0).ToString();
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Get TPS internal temperature error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Get TPS internal temperature error: " + result.ErrorMessage;
            }

            return result;
        }

        public override void ChangeFace()
        {
            base.ChangeFace();

            InvertFace();
        }

        public Result InvertFace()
        {
            Result result = new Result();
            try
            {
                Logs.Log.AddEntryAsBeginningOf(this, "Changing Face");
                AUT_POSMODE posMode = AUT_POSMODE.AUT_PRECISE;
                AUT_ATRMODE atrMode;
                atrMode = Ts60.AtrWanted ? AUT_ATRMODE.AUT_TARGET : AUT_ATRMODE.AUT_POSITION;
                bool bDummy = false;
                int errorCode = Ts60._Geocom.AUT_ChangeFace4(posMode, atrMode, bDummy);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "TPS Face has changed";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = string.Format("TPS change face error:{0}", result.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = string.Format("TPS change face error:{0}", result.ErrorMessage);
            }
            return result;
        }
        public Result GoFace(int face1Or2) //Change face TPS
        {
            //Result result = new Result();
            Result resultGlobal = new Result();
            switch (face1Or2)
            {
                case 1:
                    break;
                case 2:
                    break;
                default:
                    resultGlobal.Success = false;
                    resultGlobal.ErrorMessage = "Error face must be 1 or 2";
                    return resultGlobal;
            }
            resultGlobal.MergeString(GetTs60Face());
            if (!resultGlobal.Success) { return resultGlobal; }
            if (Ts60._Face != face1Or2)
            {
                resultGlobal.MergeString(InvertFace());
                if (resultGlobal.Success) Ts60._Face = face1Or2;
            }
            else
            {
                resultGlobal.Success = true;
                resultGlobal.AccessToken = string.Format("{0}TPS was already in face {1}", resultGlobal.AccessToken, face1Or2);
            }
            return resultGlobal;
        }
        public Result GetTs60Face() //Donne face TPS
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.TMC_GetFace(out TMC_FACE face);

                if (errorCode == 0)
                {
                    Ts60._Face = (face == TMC_FACE.TMC_FACE_1) ? 1 : 2;
                    result.Success = true;
                    result.AccessToken = "TPS face is " + Ts60._Face.ToString();
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Get TPS face error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Get TPS face error: " + result.ErrorMessage;
            }
            return result;
        }
        public Result FineAdjust()
        {
            Result result = new Result();
            try
            {
                double dSrchHz = 0.016;
                double dSrchV = 0.016;
                bool bDummy = false;
                int errorCode = 1;
                int i = 0;
                while (errorCode != 0) //Try 3 times to adjust on target if errors.
                {
                    errorCode = Ts60._Geocom.AUT_FineAdjust3(dSrchHz, dSrchV, bDummy);
                    //double dhgon = dSrchHz / Math.PI * 200;
                    //double dvgon = dSrchV / Math.PI * 200;
                    dSrchHz *= 2;
                    dSrchV *= 2;

                    i += 1;
                    if (i > 2)
                    {
                        result.Success = false;
                        result.ErrorMessage = ErrorCode(errorCode);
                        result.AccessToken = string.Format("Target fine adjust error: {0}", result.ErrorMessage);
                        InvokeOnApplicationDispatcher(View.MessageATRFailed);
                        return result;
                    }
                }
                result.Success = true;
                //   result.AccessToken = "TPS is fine adjusted to target center";
                result.AccessToken = R.T_TPS_IS_FINE_ADJUSTED_TO_TARGET_CENTER;
            }
            catch (Exception)
            {
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = string.Format("Target fine adjust error: {0}", result.ErrorMessage);
            }
            return result;
        }
        public Result MoveTo(double hz_gon, double v_gon)
        {
            Result result = new Result();
            try
            {
                AUT_POSMODE posMode = AUT_POSMODE.AUT_NORMAL;
                AUT_ATRMODE atrMode = AUT_ATRMODE.AUT_POSITION;
                bool bDummy = false;
                if (hz_gon > 400) hz_gon -= 400;
                if (hz_gon < 0) hz_gon += 400;
                double hz_rad = hz_gon / 200 * Math.PI;
                double v_rad = v_gon / 200 * Math.PI;
                int errorCode = Ts60._Geocom.AUT_MakePositioning4(hz_rad, v_rad, posMode, atrMode, bDummy);
                if (errorCode == 0)
                {
                    result.Success = true;
                    //   result.AccessToken = "TPS has moved to new position";
                    result.AccessToken = R.T_TPS_HAS_MOVED_TO_NEW_POSITION;
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = string.Format("MoveTo error: {0}", result.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = string.Format("MoveTo error: {0}", result.ErrorMessage);
            }
            return result;
        }



        /// <summary>
        /// move the instrument by given values in gon
        /// </summary>
        /// <param name="dHz_gon"></param>
        /// <param name="dV_gon"></param>
        /// <returns></returns>
        public Result MoveBy(double dHz_gon, double dV_gon)
        {
            Result result = new Result();
            try
            {

                result = GetAngles(out Angles actualPosition, out Compensator dummy, skipATR: true);

                if (!result.Success) { return result; }
                double hz_gon = actualPosition.Horizontal.Value + dHz_gon;
                double v_gon = actualPosition.Vertical.Value + dV_gon;

                if (dHz_gon > 400) dHz_gon -= 400;
                if (dHz_gon < 0) dHz_gon += 400;

                double hz_rad = Math.Round(hz_gon / 200 * Math.PI, 4);
                double v_rad = Math.Round(v_gon / 200 * Math.PI, 4);

                AUT_POSMODE posMode = AUT_POSMODE.AUT_NORMAL;
                AUT_ATRMODE atrMode = AUT_ATRMODE.AUT_POSITION; // was target but not nice to correct by atr directly
                bool bDummy = false;

                // this crap needs H and V in rad and not dH dV ingon like the name says it
                // + this crap makes the TS60 shaing for 10 seconds after the move

                int errorCode = 0;
                this.View.ShowProgress(this.View, () =>
                { 
                    errorCode = Ts60._Geocom.AUT_MakePositioning4(hz_rad, v_rad, posMode, atrMode, bDummy ); 
                }, 2, "Moving");
                if (errorCode == 0)
                {
                    result.Success = true;
                    //   result.AccessToken = "TPS has moved to new position";
                    result.AccessToken = R.T_TPS_HAS_MOVED_TO_NEW_POSITION;
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = string.Format("Moveby error: {0}", result.ErrorMessage);
                }

            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = string.Format("Moveby error: {0}", result.ErrorMessage);
            }
            finally
            {

            }

            return result;
        }

        /// <summary>
        /// return measured angles in Grades
        /// </summary>
        /// <param name="measureOfAngles"></param>
        /// <param name="MeasureOfCompensator"></param>
        /// <returns></returns>
        public Result GetAngles(out Angles measureOfAngles, out Compensator MeasureOfCompensator, bool skipATR = false)
        {
            Result result = new Result();
            Result resultGlobal = new Result();
            try
            {
                TMC_HZ_V_ANG onlyAngles = new TMC_HZ_V_ANG();
                double hz = -1;
                double v = -1;
                double angleAccuracy = -1;
                double crossInclinaison = -1;
                double lengthInclinaison = -1;
                double InclinaisonAccuracy = -1;

                measureOfAngles = new Angles();
                MeasureOfCompensator = new Compensator();
                TMC_INCLINE_PRG inclineProg = TMC_INCLINE_PRG.TMC_AUTO_INC;

                if (!skipATR && Ts60.AtrWanted)
                {
                    resultGlobal.MergeString(FineAdjust());
                }

                bool error = false;
                int errorCode = 999;
                var task = Task.Run(() =>
                {

                    if (!resultGlobal.Success) { error = true; }

                    TMC_ANGLE TPSAngles;

                    if (!skipATR && Ts60.AtrWanted)
                    {
                        errorCode = Ts60._Geocom.TMC_GetAngle1(out TPSAngles, inclineProg);
                        errorCode = Ts60._Geocom.TMC_GetSimpleMea(10000, out onlyAngles, out double d);
                        hz = onlyAngles.dHz;
                        v = onlyAngles.dV;
                        angleAccuracy = TPSAngles.dAngleAccuracy;
                    }
                    else
                    {
                        errorCode = Ts60._Geocom.TMC_GetAngle1(out TPSAngles, inclineProg);
                        hz = TPSAngles.dHz;
                        v = TPSAngles.dV;
                    }

                    using (StreamWriter writer = new StreamWriter(@"C:\temp\ts60.txt"))
                    {
                        writer.WriteLine($"TMC_GetAngle1 {TPSAngles.dHz / Math.PI * 200} {TPSAngles.dV / Math.PI * 200}");
                        writer.WriteLine($"TMC_GetSimpleMea {onlyAngles.dHz / Math.PI * 200} {onlyAngles.dV / Math.PI * 200}");
                    }

                    //errorCode = this.Ts60._Geocom.TMC_GetFullMeas(waitTime, 
                    //    out hz, out v , out angleAccuracy, 
                    //    out crossInclinaison, out lengthInclinaison, out InclinaisonAccuracy, 
                    //    out slopeDistance, out distanceTime, inclineProg);
                });
                task.Wait(TimeSpan.FromSeconds(200));
                task.Dispose();

                if (error)
                    return resultGlobal;
                Ts60._Geocom.TMC_IfDataAzeCorrError(out bool DataAzeCorrError);
                Ts60._Geocom.TMC_IfDataIncCorrError(out bool DataIncCorrError);

                if (errorCode == 0 && onlyAngles != null)
                {
                    result.Success = true;

                    result.AccessToken = R.T_TPS_HAS_DONE_A_COMPLETE_ANGLE_AND_COMPENSATOR_MEASUREMENT;

                    measureOfAngles.Horizontal.Value = hz / Math.PI * 200;
                    measureOfAngles.Horizontal.Sigma = angleAccuracy / Math.PI * 200;
                    measureOfAngles.Vertical.Value = v / Math.PI * 200;
                    measureOfAngles.Vertical.Sigma = angleAccuracy / Math.PI * 200;

                    Ts60._Compensator.inclinationAccuracy = InclinaisonAccuracy / Math.PI * 200;
                    Ts60._Compensator.longitudinalAxisInclination = lengthInclinaison / Math.PI * 200;
                    Ts60._Compensator.tranverseAxisInclination = crossInclinaison / Math.PI * 200;
                    Ts60._Compensator.inclineTime = DateTime.Now;

                    MeasureOfCompensator.inclinationAccuracy = Ts60._Compensator.inclinationAccuracy;
                    MeasureOfCompensator.longitudinalAxisInclination = Ts60._Compensator.longitudinalAxisInclination;
                    MeasureOfCompensator.tranverseAxisInclination = Ts60._Compensator.tranverseAxisInclination;
                    MeasureOfCompensator.inclineTime = Ts60._Compensator.inclineTime;

                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = string.Format("TPS angle and compensator measurement error: {0}", result.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = string.Format("TPS angle and compensator measurement error: {0}", result.ErrorMessage);
                measureOfAngles = new Angles();
                MeasureOfCompensator = new Compensator();
            }

            resultGlobal.MergeString(result);
            return resultGlobal;
        }


        public Result MeasureCompensator() // ne semble pas envoyer la position de la bulle comme voulu
        {
            Result result = new Result();
            try
            {
                TMC_INCLINE_PRG inclineProg = TMC_INCLINE_PRG.TMC_AUTO_INC;
                // run in task not to block Tsunami when the TS60 is closed by Tsunami itself
                //var task = Task.Run(() => errorCode = this.Ts60._Geocom.TMC_GetAngle1(out TPSAngles, inclineProg));
                //task.Wait(TimeSpan.FromSeconds(2));
                //task.Dispose();
                int errorCode = Ts60._Geocom.TMC_GetAngle1(out TMC_ANGLE TPSAngles, inclineProg);
                if (errorCode == 0 && TPSAngles != null)
                {
                    result.Success = true;
                    result.AccessToken = "TPS has done a compensator measurement.";
                    //permet de filtrer les fausses donn�es
                    //TPSAngles.incline.dCrossIncline = ((TPSAngles.incline.dCrossIncline > 0.01) || (TPSAngles.incline.dCrossIncline < -0.01)) ? this._Compensator.tranverseAxisInclination : TPSAngles.incline.dCrossIncline;
                    //TPSAngles.incline.dLengthIncline = ((TPSAngles.incline.dLengthIncline > 0.01) || (TPSAngles.incline.dLengthIncline < -0.01)) ? this._Compensator.longitudinalAxisInclination : TPSAngles.incline.dLengthIncline;
                    Ts60._Compensator.inclinationAccuracy = TPSAngles.Incline.dAccuracyIncline;
                    Ts60._Compensator.longitudinalAxisInclination = TPSAngles.Incline.dLengthIncline;
                    Ts60._Compensator.tranverseAxisInclination = TPSAngles.Incline.dCrossIncline;
                    Ts60._Compensator.inclineTime = DateTime.Now;
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = string.Format("TPS angle and compensator measurement error: {0}", result.ErrorMessage);
                    Ts60._Compensator.inclinationAccuracy = -999;
                    Ts60._Compensator.longitudinalAxisInclination = -999;
                    Ts60._Compensator.tranverseAxisInclination = -999;
                    Ts60._Compensator.inclineTime = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = string.Format("TPS angle and compensator measurement error: {0}", result.ErrorMessage);
                Ts60._Compensator.inclinationAccuracy = -999;
                Ts60._Compensator.longitudinalAxisInclination = -999;
                Ts60._Compensator.tranverseAxisInclination = -999;
                Ts60._Compensator.inclineTime = DateTime.Now;
            }
            return result;
        }
        public Result GetAverageAngle(out Angles averageAngle, int numberOfMeasure)
        {
            Result result = new Result();
            averageAngle = new Angles();
            if (numberOfMeasure <= 0)
            {
                result.Success = false;
                //   result.ErrorMessage = "Cannot measure negative or zero angles";
                //   result.AccessToken = "Distance error: Number of measures must be > 0";
                result.ErrorMessage = R.T_CANNOT_MEASURE_NEGATIVE_OR_ZERO_ANGLES;
                result.AccessToken = R.T_DISTANCE_ERROR_NUMBER_OF_MEASURES_MUST_BE;
                return result;
            }
            DoubleValue[] angleHz = new DoubleValue[numberOfMeasure];
            DoubleValue[] angleV = new DoubleValue[numberOfMeasure];

            for (int i = 0; i < numberOfMeasure; i++)
            {
                angleHz[i] = new DoubleValue();
                angleV[i] = new DoubleValue();
                result.MergeString(GetAngles(out Angles singleAngle, out _, skipATR: true));


                if (!result.Success) { return result; }
                angleHz[i].Value = singleAngle.Horizontal.Value;
                angleV[i].Value = singleAngle.Vertical.Value;

            }
            DoubleValue averageHz;
            EmqCalculation(angleHz, out averageHz);
            averageAngle.Horizontal.Value = averageHz.Value;
            averageAngle.Horizontal.Sigma = averageHz.Sigma;
            DoubleValue averageV;
            EmqCalculation(angleV, out averageV);
            averageAngle.Vertical.Value = averageV.Value;
            averageAngle.Vertical.Sigma = averageV.Sigma;
            return result;

        }
        public Result GetAtrAdjustMode(out string atrAdjustMode) //Montre le mode ATR actif
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.AUT_GetFineAdjustMode(out AUT_ADJMODE iAtrAdjustMode);
                switch (iAtrAdjustMode)
                {
                    case AUT_ADJMODE.AUT_NORM_MODE:
                        atrAdjustMode = "Normal_Mode";
                        break;
                    case AUT_ADJMODE.AUT_POINT_MODE:
                        atrAdjustMode = "Point_Mode";
                        break;
                    case AUT_ADJMODE.AUT_DEFINE_MODE:
                        atrAdjustMode = "Define_Mode";
                        break;
                    default:
                        atrAdjustMode = "";
                        break;
                }
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "ATR fine adjust mode is " + atrAdjustMode;
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Fine Adjust Mode error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                atrAdjustMode = "";
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Fine Adjust Mode error: " + result.ErrorMessage;
            }

            return result;
        }
        public Result SetAtrAdjustMode(AUT_ADJMODE atrAdjustMode) //Met le mode ATR sur normal point_mode: prend les points _a courte distance avec dispersion, norm_mode :code 8704 pour courtes distances
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.AUT_SetFineAdjustMode(atrAdjustMode);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "ATR fine adjust mode sets to " + atrAdjustMode.ToString();
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Fine Adjust Mode error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Fine Adjust Mode error: " + result.ErrorMessage;
            }

            return result;
        }

        public Result SetVolume(int volume)
        {
            Thread.Sleep(1000);

            int errorCode = Ts60._TCPConnection.SendCommand($"%R1Q,11046:{volume}");

            if (errorCode != 0)
            {
                string errorMessage = ErrorCode(errorCode);
                return new Result()
                {
                    Success = false,
                    ErrorMessage = errorMessage,
                    AccessToken = "SendCommand SetVolume error: " + errorMessage
                };
            }

            Thread.Sleep(1000);

            errorCode = Ts60._TCPConnection.ReceiveResult(out _);

            if (errorCode != 0)
            {
                string errorMessage = ErrorCode(errorCode);
                return new Result()
                {
                    Success = false,
                    ErrorMessage = errorMessage,
                    AccessToken = "ReceiveResult SetVolume error: " + errorMessage
                };
            }

            Thread.Sleep(1000);

            return new Result()
            {
                Success = true,
                ErrorMessage = string.Empty,
                AccessToken = string.Empty
            };
        }
        public Result SetUserLockState(bool lockOn) //Met sur on ou off le LOCK TPS
        {
            Result result = new Result();
            int errorCode;
            try
            {
                // Prevent the "lock lost" message
                bool restoreVolume = false;
                if (Ts60.LockStatus != MOT_LOCK_STATUS.MOT_LOCKED_OUT && !lockOn)
                {
                    SetVolume(0);
                    restoreVolume = true;
                }
                ON_OFF_TYPE onOff = BoolToOnOff(lockOn);
                errorCode = Ts60._Geocom.AUS_SetUserLockState(onOff);
                if (restoreVolume)
                    SetVolume(7);
            }
            catch (Exception)
            {
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Target Lock error: " + result.ErrorMessage;
            }
            return result;
        }

        internal static ON_OFF_TYPE BoolToOnOff(bool lockOn)
        {
            return lockOn ? ON_OFF_TYPE.ON : ON_OFF_TYPE.OFF;
        }

        internal static bool OnOffToBool(ON_OFF_TYPE lockOn)
        {
            return lockOn == ON_OFF_TYPE.ON;
        }
        public Result SetRefractionTozero() //Desactive la correction de refraction rentrer tous les param�tres sinon probl�me d'affichage de la distance horizontale en mode GSI. (OFF, rayon Terre 6378000, coeff refraction 0.13)
        {
            Result result = new Result();
            try
            {
                TMC_REFRACTION refractionParameters = new TMC_REFRACTION();
                refractionParameters.dEarthRadius = 6378000;
                refractionParameters.dRefractiveScale = 0.13;
                refractionParameters.eRefOn = ON_OFF_TYPE.OFF;
                int errorCode = Ts60._Geocom.TMC_SetRefractiveCorr(refractionParameters);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Refraction correction is set to off";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Refraction correction error: " + result.ErrorMessage.ToString();
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Refraction correction error: " + result.ErrorMessage.ToString();
            }

            return result;
        }
        public Result SetPrismCorrectionMM(ref double prismCorrection) //Change constante prisme
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.TMC_SetPrismCorr(prismCorrection);
                if (errorCode == 0)
                {
                    result.Success = true;
                    //   result.AccessToken = string.Format("Turned prism correction to {0} MM", Math.Round(prismCorrection, 2).ToString());
                    result.AccessToken = $"{R.T_TURNED_PRISM_CORRECTION_TO} {Math.Round(prismCorrection, 2)} MM";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Prism correction error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Prism correction error: " + result.ErrorMessage;
            }

            return result;
        }
        public Result GetPrismCorrectionMM(out double prismCorrection) //Donne constante prisme active
        {
            Result result = new Result();
            prismCorrection = 1000;
            try
            {

                int errorCode = Ts60._Geocom.TMC_GetPrismCorr(out prismCorrection);
                if (errorCode == 0)
                {
                    result.Success = true;
                    //   result.AccessToken = string.Format("Active prism correction = {0} MM", Math.Round(prismCorrection, 2).ToString());
                    result.AccessToken = $"{R.T_ACTIVE_PRISM_CORRECTION} = {Math.Round(prismCorrection, 2)} MM";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Prism correction error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Prism correction error: " + result.ErrorMessage;
            }

            return result;
        }
        public Result SetReflectorTypeToUseReflector()
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.BAP_SetTargetType(BAP_TARGET_TYPE.BAP_REFL_USE);
                if (errorCode == 0)
                {
                    result.Success = true;
                    //    result.AccessToken = string.Format("Set to use reflector");
                    result.AccessToken = $"{R.T_SET_TO_USE_REFLECTOR}";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Set to use reflector error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Set to use reflector error: " + result.ErrorMessage;
            }

            return result;
        }
        public Result SetReflectorTypeToNotUseReflector()
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.BAP_SetTargetType(BAP_TARGET_TYPE.BAP_REFL_LESS);
                if (errorCode == 0)
                {
                    result.Success = true;
                    //    result.AccessToken = string.Format("Set to not use reflector");
                    result.AccessToken = $"{R.T_SET_TO_NOT_USE_REFLECTOR}";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Set to not use reflector error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Set to not use reflector error: " + result.ErrorMessage;
            }

            return result;
        }
        public Result SetPrismTypeToRRR()
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.BAP_SetPrismType(BAP_PRISMTYPE.BAP_PRISM_ROUND);
                if (errorCode == 0)
                {
                    result.Success = true;
                    //   result.AccessToken = string.Format("Turned to circular prism");
                    result.AccessToken = $"{R.T_TURNED_TO_CIRCULAR_PRISM}";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Turned to circular prism error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Turned to circular prism error: " + result.ErrorMessage;
            }

            return result;
        }
        public Result SetPrismTypeToTape()
        {
            Result result = new Result();
            int errorCode = Ts60._Geocom.BAP_SetPrismType(BAP_PRISMTYPE.BAP_PRISM_TAPE);
            if (errorCode == 0)
            {
                result.Success = true;
                //    result.AccessToken = string.Format("Turned to tape target");
                result.AccessToken = $"{R.T_TURNED_TO_TAPE_TARGET}";
            }
            else
            {
                result.Success = false;
                result.ErrorMessage = ErrorCode(errorCode);
                result.AccessToken = "Turn to tape target error: " + result.ErrorMessage;
            }
            return result;
        }

        public Result GetAverageDistance(out DoubleValue averageSlopeDistance, int numberOfDistance)
        {
            Result result = new Result();
            averageSlopeDistance = new DoubleValue();
            if (numberOfDistance <= 0)
            {
                averageSlopeDistance.Value = 0;
                averageSlopeDistance.Sigma = 0;

                result.Success = false;
                result.ErrorMessage = R.T_CANNOT_MEASURE_NEGATIVE_OR_ZERO_DISTANCE;
                result.AccessToken = R.T_DISTANCE_ERROR_NUMBER_OF_MEASURES_MUST_BE;

                return result;
            }
            DoubleValue[] slopeDistance = new DoubleValue[numberOfDistance];
            for (int i = 0; i < numberOfDistance; i++)
            {
                result.MergeString(GetOneDistance(out slopeDistance[i]));
                if (!result.Success) { averageSlopeDistance.Value = 0; averageSlopeDistance.Sigma = 0; return result; }
            }
            EmqCalculation(slopeDistance, out averageSlopeDistance);
            return result;
        }

        internal void RunWithTimeout(Action action, TimeSpan timeout)
        {
            Task task = Task.Run(action);
            if (Task.WaitAny(new[] { task }, timeout) == -1)
            {
                Console.WriteLine("Operation timed out.");
                // Handle timeout scenario
            }
            else
            {
                // Task completed within the timeout
                if (task.IsFaulted)
                {
                    // Handle the case where the task threw an exception
                    Console.WriteLine("Operation failed.");
                    // Optionally, rethrow the exception or handle it as needed
                    throw task.Exception ?? new Exception("Unknown exception occurred.");
                }
            }
        }

        /// <summary>
        /// Check if a TS60 compensation measurement has been done by the user today
        /// </summary>
        internal void CheckCompensation()
        {
            try
            {
                string directoryTS60Path = $"\\\\{IP}\\Captivate\\Data\\";
                string directoryTsunami = Tsunami2.Preferences.Values.Paths.MeasureOfTheDay;
                if (Directory.Exists(directoryTS60Path))
                {
                    if (!Directory.Exists(directoryTsunami)) Directory.CreateDirectory(directoryTsunami);
                    string filepathTS60 = directoryTS60Path + @"TS60_COM.TXT";
                    string filepathTsunami = directoryTsunami + @"TS60_COMPENSATION.TXT";
                    string filePathTS60_Copy = directoryTS60Path + T.Conversions.Date.ToDateUpSideDown(DateTime.Now) + @"_TS60_COM.TXT";
                    if (File.Exists(filepathTS60))
                    {
                        if (File.Exists(filepathTsunami)) File.Delete(filepathTsunami);
                        File.Copy(filepathTS60, filepathTsunami);
                        if (File.Exists(filepathTsunami))
                        {
                            string[] textArrayFile = File.ReadAllLines(filepathTsunami);
                            List<string> textListFile = new List<string>();
                            ///Si fichier trop gros, cr�e une copie sur le TS60 et purge le fichier existant
                            if (textArrayFile.Length > 500)
                            {
                                File.Copy(filepathTS60, filePathTS60_Copy);
                            }
                            bool findDate = false;
                            bool stopSearching = false;
                            for (int i = textArrayFile.Length - 1; i >= 0; i--)
                            {
                                string line = textArrayFile[i];
                                ///Purge le fichier et ne garde que les 250 derni�res lignes. Besoin de copier le fichier que si on a fichier de plus de 500 lignes
                                if (textListFile.Count <= 250 && textArrayFile.Length > 500)
                                {
                                    textListFile.Add(line);
                                }
                                else
                                {
                                    ///Arr�te la lecture du fichier si on est au del� des 250 lignes et qu'on a trouv� la date de compensation
                                    if (stopSearching)
                                        break;
                                }
                                if (line.StartsWith("%TSU_DATE") && (i + 1) < textArrayFile.Length && !stopSearching)
                                {
                                    char splitChar = ';';
                                    string[] lineSplitDate = line.Split(splitChar);
                                    string instrumentType = lineSplitDate[1].ToString().Substring(0, 4);
                                    int SN = T.Conversions.Numbers.ToInt(lineSplitDate[2], -9999);
                                    if (instrumentType == "TS60" && SN.ToString() == Ts60._SerialNumber)
                                    {
                                        int year = T.Conversions.Numbers.ToInt(lineSplitDate[3], -9999);
                                        int month = T.Conversions.Numbers.ToInt(lineSplitDate[4], -9999);
                                        int day = T.Conversions.Numbers.ToInt(lineSplitDate[5], -9999);
                                        int hour = T.Conversions.Numbers.ToInt(lineSplitDate[6], -9999);
                                        int minute = T.Conversions.Numbers.ToInt(lineSplitDate[7], -9999);
                                        string lineResult = textArrayFile[i + 1];
                                        string[] lineSplitResult = lineResult.Split(splitChar);
                                        if (lineSplitResult[0] == "%TSU_RESULT")
                                        {
                                            //Ne change la date de compensation que si on n'a pas trouv� la derni�re.
                                            if (year != -9999 && month != -9999 && day != -9999 && hour != -9999 && minute != -9999 && !findDate)
                                            {
                                                Ts60._LastCompensationDate = new DateTime(year, month, day, hour, minute, 0);
                                                findDate = true;
                                            }
                                            ///Recherche toutes les derni�res compensations faites le m�me jour.
                                            if (findDate
                                                && year == Ts60._LastCompensationDate.Year
                                                && Ts60._LastCompensationDate.Month == month
                                                && Ts60._LastCompensationDate.Day == day)
                                            {
                                                if (T.Conversions.Numbers.ToDouble(lineSplitResult[1], false, -9999) != -9999) Ts60._Comp_L = true;
                                                if (T.Conversions.Numbers.ToDouble(lineSplitResult[2], false, -9999) != -9999) Ts60._Comp_T = true;
                                                if (T.Conversions.Numbers.ToDouble(lineSplitResult[3], false, -9999) != -9999) Ts60._Comp_i_indexV = true;
                                                if (T.Conversions.Numbers.ToDouble(lineSplitResult[4], false, -9999) != -9999) Ts60._Comp_c_CollimHz = true;
                                                if (T.Conversions.Numbers.ToDouble(lineSplitResult[5], false, -9999) != -9999) Ts60._Comp_a_AxeA = true;
                                                if (T.Conversions.Numbers.ToDouble(lineSplitResult[6], false, -9999) != -9999) Ts60._Comp_ATRHz = true;
                                                if (T.Conversions.Numbers.ToDouble(lineSplitResult[7], false, -9999) != -9999) Ts60._Comp_ATRV = true;
                                            }
                                            else
                                            {
                                                //Si on avait trouv� la date de derni�re compensation et que la pr�c�dente n'est pas le m�me jour, arr�te la recherche
                                                if (findDate) stopSearching = true;
                                            }
                                        }
                                    }
                                }
                            }
                            if (textArrayFile.Length > 500)
                            {
                                if (File.Exists(filePathTS60_Copy))
                                {
                                    File.Delete(filepathTS60);
                                    textListFile.Reverse();
                                    File.WriteAllLines(filepathTS60, textListFile.ToArray());
                                    if (!File.Exists(filepathTS60))
                                    {
                                        new MessageInput(MessageType.Critical, R.StringTS60_ProblemAccessCompensationFile).Show();
                                    }
                                }
                            }
                        }
                        else
                        {
                            new MessageInput(MessageType.Critical, R.StringTS60_ProblemAccessCompensationFile).Show();
                        }
                    }
                    else
                    {
                        new MessageInput(MessageType.Critical, R.StringTS60_ProblemAccessCompensationFile).Show();
                    }
                }
                else
                {
                    new MessageInput(MessageType.Critical, R.StringTS60_ProblemAccessCompensationFile).Show();
                }
            }
            catch (Exception)
            {
                new MessageInput(MessageType.Critical, R.StringTS60_ProblemAccessCompensationFile).Show();
            }
            finally
            {
                if (Ts60._LastCompensationDate.Year != DateTime.Now.Year
                    || Ts60._LastCompensationDate.Month != DateTime.Now.Month
                    || Ts60._LastCompensationDate.Day != DateTime.Now.Day)
                {
                    ///Affiche un message critique si pas de compensation faite aujourd'hui
                    new MessageInput(MessageType.Critical, R.StringTS60_NoCompensationDoneToday).Show();
                }
                else
                {
                    //Affiche un message d'avertissement si compensation n'est pas compl�te (seul le a n'est pas n�cessaire)
                    if (!Ts60._Comp_T || !Ts60._Comp_L || !Ts60._Comp_ATRHz
                        || !Ts60._Comp_ATRV || !Ts60._Comp_c_CollimHz || !Ts60._Comp_i_indexV)
                    {
                        string compensationNotDone = "";
                        if (!Ts60._Comp_L) compensationNotDone = "L,T,";
                        if (!Ts60._Comp_i_indexV) compensationNotDone += "i,c,";
                        if (!Ts60._Comp_a_AxeA) compensationNotDone += "a,";
                        if (!Ts60._Comp_ATRHz) compensationNotDone += "ATR";
                        string titleAndMessage = string.Format(R.StringTS60_CompensationNotComplete, compensationNotDone);
                        new MessageInput(MessageType.Warning, titleAndMessage).Show();
                    }
                }
            }
        }
        public override Result Connect()
        {
            Result result = new Result();
            result.MergeString(OpenConnection(IP.ToString(), Ts60._PortCom));
            if (result.Success)
            {
                //result.MergeString(GetSerialNumber());
            }
            else
            {
                result.Success = true;
            }
            return result;
        }
        public Result SetToWithReflector() //Met sur le mode avec reflecteur
        {
            Result result = new Result();
            //GeoComS2K_dll.ModeMesure = GeoComS2K_dll.IR_Or_RL.EDM;
            result.MergeString(SetEDMModeToIRPrecise());
            return result;
        }
        public Result SetEDMModeToStandard() //Met sur le mode EDM sur mesure standard
        {
            Result result = new Result();
            try
            {
                EDM_MODE edmMode = EDM_MODE.EDM_SINGLE_STANDARD;
                int errorCode = Ts60._Geocom.TMC_SetEdmMode(edmMode);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "EDM Mode sets to single standard measurement.";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
                }
                result.MergeString(SetUserAtrState(true));
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
            }

            return result;
        }
        public Result SetUserAtrState(bool atrOn) //Met sur on ou off l'ATR TPS
        {
            Result result = new Result();
            try
            {
                ON_OFF_TYPE onOff = BoolToOnOff(atrOn);
                int errorCode = Ts60._Geocom.AUS_SetUserAtrState(onOff);

                if (errorCode == 0)
                {
                    Ts60.AtrWanted = atrOn;
                    result.Success = true;
                    result.AccessToken = "ATR sets to " + onOff.ToString();
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Set ATR error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Set ATR error: " + result.ErrorMessage;
            }

            return result;
        }
        public Result SetEDMModeToTape() //Met sur le mode EDM sur mesure sur cible retro
        {
            Result result = new Result();
            try
            {
                EDM_MODE edmMode = EDM_MODE.EDM_PRECISE_TAPE;
                int errorCode = Ts60._Geocom.TMC_SetEdmMode(edmMode);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "EDM Mode sets to precise measurement on tape.";
                    result.MergeString(SetUserLockState(false));
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
                }
                result.MergeString(SetUserAtrState(false));
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "EDM Mode error: " + result.ErrorMessage;
            }

            return result;
        }
        public Result SetToReflectorLess() //Met sur le mode sans reflecteur
        {
            Result result = new Result();
            //GeoComS2K_dll.ModeMesure = GeoComS2K_dll.IR_Or_RL.RL;
            result.MergeString(SetEDMModeToRL());
            return result;
        }
        public Result SetRedLaserOnOff(bool LaserOnOff) //Met sur on ou off le laser rouge TPS
        {
            Result result = new Result();
            try
            {
                ON_OFF_TYPE onOff = BoolToOnOff(LaserOnOff);

                int errorCode = Ts60._Geocom.EDM_Laserpointer(onOff);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Red Laser is " + onOff.ToString();
                    switch (onOff)
                    {
                        case ON_OFF_TYPE.OFF:
                            Ts60._RedLaser = false;
                            Ts60._UserRedLaserPreference = false;
                            break;
                        case ON_OFF_TYPE.ON:
                            Ts60._RedLaser = true;
                            Ts60._UserRedLaserPreference = true;
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Red Laser Error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Red Laser Error: " + result.ErrorMessage;
            }

            return result;
        }
        /// <summary>
        /// Switch ATR on/off when clicking button in the view
        /// </summary>
        /// <returns></returns>
        public Result SwitchATR()
        {
            Result resultGlobal = new Result();

            //Revert the wanted state
            Ts60.AtrWanted = !Ts60.AtrWanted;

            // If lock is on, don't touch the actual ATR status, as it will override the lock
            if (Ts60.LockStatus == MOT_LOCK_STATUS.MOT_LOCKED_OUT)
                resultGlobal.MergeString(SetUserAtrState(Ts60.AtrWanted));

            // update status
            if (resultGlobal.Success)
                resultGlobal.MergeString(GetLockStatus());

            return resultGlobal;
        }

        /// <summary>
        /// Switch lock on/off when clicking button in the view
        /// </summary>
        /// <returns></returns>
        public Result SwitchLock()
        {
            Result result = new Result();

            // If lock is turned off or lost, try to track the target
            if (!Ts60.LockWanted || Ts60.LockStatus == MOT_LOCK_STATUS.MOT_LOCKED_OUT)
            {
                Ts60.LockWanted = true;
                result.MergeString(StartTrackTarget());
            }
            // If lock is locked in, stop the tracking
            else if (Ts60.LockStatus == MOT_LOCK_STATUS.MOT_LOCKED_IN)
            {
                Ts60.LockWanted = false;
                result.MergeString(StopTrackTarget(true));
                result.MergeString(SetRedLaserOnOff(Ts60._UserRedLaserPreference));
            }
            else
            {
                result.MergeString(new Result()
                {
                    Success = true,
                    AccessToken = "Status is unexpected; doing nothing."
                });
            }

            //Do nothing is status .
            return result;
        }

        public Result GetLockStatus()
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.AUS_GetUserLockState(out ON_OFF_TYPE eState);
                if (errorCode == 0)
                {
                    if (eState == ON_OFF_TYPE.OFF)
                    {
                        Ts60.LockStatus = MOT_LOCK_STATUS.MOT_LOCKED_OUT;
                        result.Success = true;
                        result.AccessToken = $"{R.T_TARGET_LOCK_STATUS_IS} {Ts60.LockStatus} ";
                        return result;
                    }
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Get the user lock status error : " + result.ErrorMessage;
                }

                errorCode = Ts60._Geocom.MOT_ReadLockStatus(out MOT_LOCK_STATUS lockStatus);
                if (errorCode == 0)
                {
                    Ts60.LockStatus = lockStatus;
                    result.Success = true;
                    result.AccessToken = $"{R.T_TARGET_LOCK_STATUS_IS} {lockStatus} ";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Get the target lock status error : " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Get the target lock status exception : " + ex.Message;
            }

            return result;
        }
        /// <summary>
        /// Start to track the target
        /// </summary>
        /// <returns></returns>
        public Result StartTrackTarget()
        {
            Result resultGlobal = new Result();
            try
            {
                resultGlobal.MergeString(SetUserLockState(true));
                if (!resultGlobal.Success) return resultGlobal;
                resultGlobal.MergeString(FineAdjust());
                if (!resultGlobal.Success) return resultGlobal;
                resultGlobal.MergeString(LockIn());
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                Result result = new Result();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Tracking target error: " + result.ErrorMessage;
                resultGlobal.MergeString(result);
            }
            return resultGlobal;
        }

        private Result LockIn()
        {
            Result result = new Result();
            int errorCode = Ts60._Geocom.AUT_LockIn();
            if (errorCode == 0)
            {
                result.Success = true;
                result.AccessToken = "Target locked";
                // Wait because the status needs time to update
                Thread.Sleep(1000);
                result.MergeString(GetLockStatus());
                Ts60._RedLaser = false;
            }
            else
            {
                result.Success = false;
                result.ErrorMessage = ErrorCode(errorCode);
                result.AccessToken = "Tracking target error: " + result.ErrorMessage;
            }

            return result;
        }

        /// <summary>
        /// Stop tracking target
        /// </summary>
        /// <returns></returns>
        public Result StopTrackTarget(bool updateStatus = false)
        {
            Result result = new Result();
            try
            {
                result.MergeString(SetUserLockState(false));
                result.MergeString(SetUserAtrState(Ts60.AtrWanted));
                if (result.Success && updateStatus)
                    result.MergeString(GetLockStatus());
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Tracking target error: " + result.ErrorMessage;
            }
            return result;
        }

        /// <summary>
        /// Set the lock fly mode on off
        /// </summary>
        /// <param name="flyOnOff"></param>
        /// <returns></returns>
        public Result SetLockFlyMode(bool flyOnOff)
        {
            Result result = new Result();
            try
            {
                ON_OFF_TYPE eFlyOnOff = BoolToOnOff(flyOnOff);
                int errorCode = Ts60._Geocom.AUT_SetLockFlyMode(eFlyOnOff);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = (flyOnOff) ? "Lock fly mode is set on" : "Lock fly mode is set off";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Set lock fly mode error: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Set lock fly mode error: " + result.ErrorMessage;
            }
            return result;
        }
        /// <summary>
        /// Get the actual lock fly mode
        /// </summary>
        /// <param name="flyOnOff"></param>
        /// <returns></returns>
        public Result GetLockFlyMode(out bool flyOnOff)
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.AUT_GetLockFlyMode(out ON_OFF_TYPE eFlyOnOff);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = (eFlyOnOff == ON_OFF_TYPE.ON) ? "Lock fly mode is  on" : "Lock fly mode is  off";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Get lock fly mode error: " + result.ErrorMessage;
                }
                flyOnOff = eFlyOnOff == ON_OFF_TYPE.ON;
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Get lock fly mode error: " + result.ErrorMessage;
                flyOnOff = true;
            }
            return result;
        }
        /// <summary>
        /// Get the actual power search area
        /// </summary>
        /// <returns></returns>
        public Result GetPowerSearchArea()
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.AUT_GetSearchArea(out AUT_SEARCH_AREA searchArea);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Actual Power Search Area received";
                    Ts60._PowerSearchArea = searchArea;
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Error Actual Power Search Area not received: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Error Actual Power Search Area not received: " + result.ErrorMessage;
            }
            return result;
        }
        /// <summary>
        /// Set the power search area
        /// </summary>
        /// <returns></returns>
        public Result SetPowerSearchArea()
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.AUT_SetSearchArea(Ts60._PowerSearchArea);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Power Search Area is set";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Error Power Search Area not set: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Error Power Search Area not set: " + result.ErrorMessage;
            }
            return result;
        }
        /// <summary>
        /// Get the actual atr spiral search area
        /// </summary>
        /// <returns></returns>
        public Result GetAtrSearchArea()
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.AUT_GetUserSpiral(out AUT_SEARCH_SPIRAL searchArea);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Actual ATR Search Area received";
                    Ts60._AtrSearchArea = searchArea;
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Error Actual ATR Search Area not received: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Error Actual ATR Search Area not received: " + result.ErrorMessage;
            }
            return result;
        }
        /// <summary>
        /// Set the ATR spiral search area
        /// </summary>
        /// <returns></returns>
        public Result SetAtrSearchArea()
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.AUT_SetUserSpiral(Ts60._AtrSearchArea);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Atr spiral Search Area is set";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Error Atr spiral Search Area not set: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Error Atr spiral Search Area not set: " + result.ErrorMessage;
            }
            return result;
        }
        /// <summary>
        ///  Set the power search distance range
        /// </summary>
        /// <param name="lMin"></param>
        /// <param name="lMax"></param>
        /// <returns></returns>
        public Result SetPowerSearchDistanceRange(int lMin, int lMax)
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.AUT_PS_SetRange(lMin, lMax);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Power Search distance range set to 1-100m";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Error Power Search distance range not set: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Error Power Search distance range not set: " + result.ErrorMessage;
            }
            return result;
        }
        /// <summary>
        /// Enable / Disable the power search window
        /// </summary>
        /// <param name="onOff"></param>
        /// <returns></returns>
        public Result EnableDisablePowerSearchWindow(bool onOff)
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.AUT_PS_EnableRange(onOff);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "Enable / Disable Power Search windows set to" + onOff.ToString();
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Error Enable/Disable Power Search window not set: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Error Enable/Disable Power Search window not set: " + result.ErrorMessage;
            }
            return result;
        }
        /// <summary>
        /// Start power search
        /// </summary>
        /// <returns></returns>
        public Result StartDefaultPowerSearch()
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.AUT_PS_SearchNext(0, true);
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "A Target has been found";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Error during power search execution: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Error during power search execution: " + result.ErrorMessage;
            }
            return result;
        }
        /// <summary>
        /// Start power search
        /// </summary>
        /// <returns></returns>
        public Result StartPowerSearchWindow()
        {
            Result result = new Result();
            try
            {
                int errorCode = Ts60._Geocom.AUT_PS_SearchWindow();
                if (errorCode == 0)
                {
                    result.Success = true;
                    result.AccessToken = "A Target has been found";
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = ErrorCode(errorCode);
                    result.AccessToken = "Error during power search execution: " + result.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Clear();
                result.Success = false;
                result.ErrorMessage = ErrorCode(9999);
                result.AccessToken = "Error during power search execution: " + result.ErrorMessage;
            }
            return result;
        }
        /// <summary>
        /// Do a default power search
        /// </summary>
        /// <returns></returns>
        internal Result DoDefaultPowerSearch()
        {
            Result r = new Result();
            Ts60._TCPConnection.Timeout = new TimeSpan(0, 0, 0, 30, 0);
            r.MergeString(StartDefaultPowerSearch());
            Ts60._TCPConnection.Timeout = new TimeSpan(0, 0, 0, 15, 0);
            return r;
        }
        /// <summary>
        /// Do a full power search
        /// </summary>
        /// <returns></returns>
        internal Result DoFullPowerSearch()
        {
            Result r = new Result();
            Ts60._PowerSearchArea.dCenterHz = 0;
            Ts60._PowerSearchArea.dCenterV = 1.5708;
            Ts60._PowerSearchArea.dRangeHz = 6.28;
            Ts60._PowerSearchArea.dRangeV = 1.57;
            r.MergeString(SetPowerSearchArea());
            //this.GetPowerSearchArea();
            r.MergeString(SetPowerSearchDistanceRange(0, 150));
            r.MergeString(EnableDisablePowerSearchWindow(true));
            Ts60._TCPConnection.Timeout = new TimeSpan(0, 0, 0, 60, 0);
            r.MergeString(StartPowerSearchWindow());
            r.MergeString(EnableDisablePowerSearchWindow(false));
            Ts60._TCPConnection.Timeout = new TimeSpan(0, 0, 0, 15, 0);
            return r;
        }
        public override Result Measure()
        {
            InvokeOnApplicationDispatcher(View.LaunchModuleMeasurement);
            return tempResult;
        }
        public Result MeasureTS60()
        {
            Result r = new Result();
            MeasureWanted = true;
            r.MergeString(base.Measure());
            MeasureWanted = false;
            if (Ts60.LockWanted)
                r.MergeString(StartTrackTarget());
            return r;
        }
        public Result MoveTo()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Check if busy communicating and try several time
        /// </summary>
        internal void CheckAndWaitIfBusy(bool checkForConnection = true)
        {
            DateTime start = DateTime.Now;
            start = start.AddMilliseconds(2000);
            //this.TimerWaitForCommunication.Enabled = true;
            if (checkForConnection && !tcpConnected)
            {
                string s = "Not Connected";
                Logs.Log.AddEntryAsError(this, s);
                throw new Exception(s);
            }

            //this.TimerWaitForCommunication.Start();
            while (DateTime.Now < start) ///Lance le timer et regarde  durant 2 secondes que la communication se lib�re.
            {
                if (!IsBusyCommunicating)
                {
                    IsBusyCommunicating = true;
                    //this.TimerWaitForCommunication.Stop();
                    //this.TimerWaitForCommunication.Enabled = true;
                    //timerTry = 0;
                    return;
                }
            }
            throw new Exception(R.String_Exception_InstrumentIsBusy);
        }
        // <summary>
        /// THis is to acquire a distance and waiting for a REC to save it, the distance is store in the TS60 until a purge distance is done by the next distance measurement
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        internal void DoTemporaryDistanceMeasurement()
        {
            
            GetOneDistance(out DoubleValue dummy);
            if (dummy.Value != Tsunami2.Preferences.Values.na)
            {
                TemporaryDistanceAvailableInInstrument = true;
                Logs.Log.AddEntryAsSuccess(this, R.T_DISTANCE_AVAILABLE);
            }

        }

        void IConnectable.Refresh()
        {
            throw new NotImplementedException();
        }
    }
    #endregion
}




