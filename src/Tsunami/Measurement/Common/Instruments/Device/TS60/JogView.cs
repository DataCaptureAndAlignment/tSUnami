﻿using System;
using R = TSU.Properties.Resources;
using System.Windows.Forms;
using TSU.ENUM;
using System.Collections.Generic;


namespace TSU.Common.Instruments.Device.TS60
{
    public partial class JogView : SubView
    {
        public JogView()
        {
            this._Name = "Jog panel";
            InitializeComponent();
            this.ApplyThemeColors();
        }

        private void ApplyThemeColors()
        {
            this.panelJog.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.tableLayoutPanel1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.pictureBox6.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.buttonPlusLeft.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.buttonPlusRight.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.button2.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.button3.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.buttonPlusStop.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.textBox1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.textBox1.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            this.toolStrip2.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.toolStripButtonUp.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.toolStripButtonUp.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripButtonDown.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.toolStripButtonDown.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripButtonLeft.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.toolStripButtonLeft.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripButtonRight.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.toolStripButtonRight.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.toolStripButtonStop.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.toolStripButtonStop.ImageTransparentColor = TSU.Tsunami2.Preferences.Theme.Colors.Transparent;
            this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;

        }
        public JogView(TS60.View parentView)
            : base(parentView)
        {
            InitializeComponent();
            this.ParentView = parentView;
            
        }

        internal override void Start()
        {
            base.Start();
            this.textBox1.Focus();
        }

        internal override void Stop()
        {
            base.Stop();
            TS60Module._motor.MoveByDirection(ENUM.movingDirection.None);
            this.textBox1.Text = "Not moving";
            this.pictureBox6.BackgroundImage = R.At40x_moveNot;
        }

        private void MoveButton_Click(object sender, EventArgs e)
        {
            try
            {
                string direction = "";
                if (sender is Control) direction = (sender as Control).Tag.ToString();
                if (sender is ToolStripItem) direction = (sender as ToolStripItem).Tag.ToString();
                this.textBox1.Text = direction;
                movingDirection d = Motor.GetDirectionFromText(direction);
                TS60Module._motor.MoveByDirection(d);
                this.textBox1.Focus();
            }
            catch (Exception ex)
            {
                throw new Exception(R.T_PROBLEM_WHEN_TRYING_TO_JOG + ex.Message, ex);
            }
        }

        internal void MovebyDirection(TSU.ENUM.movingDirection direction)
        {
            this.textBox1.Text = direction.ToString();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox tb = (sender as TextBox);
            if (tb.Parent.Parent.Parent is JogView)
            {
                (tb.Parent.Parent.Parent as JogView).On_TsuView_KeyDown(sender, e);
            }
        }

        internal override void On_TsuView_KeyDown(object sender, KeyEventArgs e)
        {
            base.On_TsuView_KeyDown(sender, e); // nothing
            MoveBasedOnKey(e.KeyData);
        }

        internal void MoveBasedOnKey(Keys key)
        {
            switch (key)
            {
                case Keys.Right: this.TS60View.Module._motor.MoveByDirection(movingDirection.Right); break;
                case Keys.Left: this.TS60View.Module._motor.MoveByDirection(movingDirection.Left); break;
                case Keys.Up: this.TS60View.Module._motor.MoveByDirection(movingDirection.Up); break;
                case Keys.Down: this.TS60View.Module._motor.MoveByDirection(movingDirection.Down); break;
            }
        }

        internal void On_TsuView_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            MoveBasedOnKey(e.KeyData);
        }


        private void toolStripButtonByAngle_Click(object sender, EventArgs e)
        {
            Views.Message.MessageInput mi = new Views.Message.MessageInput(Views.Message.MessageType.Choice, R.MOVEBYVALUEOFANGLE)
            {
                Controls = new List<Control>() { 
                    Views.Message.CreationHelper.GetPreparedLabel("dH(gon) ="),
                    Views.Message.CreationHelper.GetPreparedTextBox("200.00000"),
                    Views.Message.CreationHelper.GetPreparedLabel("dV(gon) ="),
                    Views.Message.CreationHelper.GetPreparedTextBox("0.00000")
                },
                ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL }
            };
            var result = mi.Show();
            if (result.TextOfButtonClicked != R.T_CANCEL)
            {
                if (double.TryParse((result.ReturnedControls[1] as TextBox).Text, out double dH))
                {
                    if (double.TryParse((result.ReturnedControls[3] as TextBox).Text, out double dV))
                    {
                        this.TS60Module.MoveBy(dH,dV);
                        return;
                    }
                }
                mi = new Views.Message.MessageInput(Views.Message.MessageType.Critical, R.T_WRONG_VALUE_ENCODED)
                {
                    AsNotification = true,
                };
                mi.Show();
            }
        }
    }
}
