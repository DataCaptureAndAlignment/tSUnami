using System;
using System.Xml.Serialization;
using G = GeoComClient;
using TSU.Common.Elements;

namespace TSU.Common.Instruments.Device.TS60
{
    [XmlType(TypeName = "TS60.Instrument")]
    public class Instrument : TotalStation
    {
        #region variables spécifiques Ts60
        [XmlIgnore]
        public ushort _Battery { get; set; }
        [XmlIgnore]
        public double _Tinterne { get; set; }
        [XmlIgnore]
        public int _PortCom { get; set; }
        [XmlIgnore]
        /// <summary>
        /// Tells if ATR is wanted by user; may differ from the instrumnet status if lock is on
        /// </summary>
        public bool AtrWanted { get; set; }
        /// <summary>
        /// Tells if Lock is wanted by user
        /// </summary>
        [XmlIgnore]
        public bool LockWanted { get; set; }
        /// <summary>
        /// The instrument's lock status
        /// LOCKED_OUT if lock is not wanted or lost
        /// LOCKED_IN if lock has been succesful
        /// </summary>
        [XmlIgnore]
        public G.GeoComTypes.MOT_LOCK_STATUS LockStatus { get; set; }
        [XmlIgnore]
        public G.GeoComTypes.AUT_SEARCH_AREA _PowerSearchArea { get; set; }
        [XmlIgnore]
        public G.GeoComTypes.AUT_SEARCH_SPIRAL _AtrSearchArea { get; set; }
        [XmlIgnore]
        public bool _RedLaser { get; set; }
        [XmlIgnore]
        public bool _UserRedLaserPreference { get; set; }
        [XmlIgnore]
        public int _Face { get; set; }
        [XmlIgnore]
        public Compensator _Compensator { get; set; }
        [XmlIgnore]
        public Angles _AngleCorrection { get; set; }
        [XmlIgnore]
        public G.GeoCom _Geocom { get; set; }
        [XmlIgnore]
        public G.TcpConnection _TCPConnection { get; set; }
        [XmlIgnore]
        public DateTime _LastCompensationDate { get; set; }
        [XmlIgnore]
        public bool _Comp_L { get; set; }
        [XmlIgnore]
        public bool _Comp_T { get; set; }
        [XmlIgnore]
        public bool _Comp_i_indexV { get; set; }
        [XmlIgnore]
        public bool _Comp_c_CollimHz { get; set; }
        [XmlIgnore]
        public bool _Comp_a_AxeA { get; set; }
        [XmlIgnore]
        public bool _Comp_ATRHz { get; set; }
        [XmlIgnore]
        public bool _Comp_ATRV { get; set; }
        #endregion

        public Instrument()
        {
            this._Brand = "Leica";
            this.Id = string.Empty;
            //this._LastMeasure = new Management.Measure();
            this._Model = "TS60";
            this._SerialNumber = string.Empty;
            this._PortCom = 1212;
            this._AngleCorrection = new Angles();
            this._Battery = 0;
            this._Compensator = new Compensator();
            this._Compensator.longitudinalAxisInclination = -999;
            this._Compensator.tranverseAxisInclination = -999;
            this._Compensator.inclinationAccuracy = -999;
            this._InstrumentClass = InstrumentClasses.TACHEOMETRE;
            this._Geocom = new G.GeoCom();
            this.AtrWanted = false;
            this.LockWanted = false;
            this._RedLaser = false;
            this.LockStatus = G.GeoComTypes.MOT_LOCK_STATUS.MOT_LOCKED_OUT;
            this._PowerSearchArea = new G.GeoComTypes.AUT_SEARCH_AREA();
            this._AtrSearchArea = new G.GeoComTypes.AUT_SEARCH_SPIRAL();
            this._Face = 1;
            this._UserRedLaserPreference = true;
            this._LastCompensationDate = DateTime.MinValue;
            this._Comp_L = false;
            this._Comp_T = false;
            this._Comp_i_indexV = false;
            this._Comp_c_CollimHz = false;
            this._Comp_a_AxeA = false;
            this._Comp_ATRHz = false;
            this._Comp_ATRV = false;
        }
    }
}
