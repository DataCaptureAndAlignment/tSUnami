﻿using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments.Device.TS60
{
    partial class View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (this.Module == null)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelMain = new System.Windows.Forms.Panel();
            this.buttonGotoAll = new System.Windows.Forms.Button();
            this.comboBox_PortCom = new System.Windows.Forms.ComboBox();
            this.buttonGoto = new System.Windows.Forms.Button();
            this.label_numberOfMeas = new System.Windows.Forms.Label();
            this.textBox_numberOfMeas = new System.Windows.Forms.TextBox();
            this.button_All = new System.Windows.Forms.Button();
            this.button_Rec = new System.Windows.Forms.Button();
            this.button_Distance = new System.Windows.Forms.Button();
            this.comboBox_TCP_IP = new System.Windows.Forms.ComboBox();
            this.Button_OnOff = new System.Windows.Forms.Button();
            this.panelLog = new System.Windows.Forms.Panel();
            this.labelMeteo = new System.Windows.Forms.Label();
            this.button_face = new System.Windows.Forms.Button();
            this.label_Bulle = new System.Windows.Forms.Label();
            this.pictureBox_bulle = new System.Windows.Forms.PictureBox();
            this.pictureBox_Battery = new System.Windows.Forms.PictureBox();
            this.button_ATR = new System.Windows.Forms.Button();
            this.pictureBox_OnOff = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Timer_check = new System.Windows.Forms.Timer(this.components);
            this.button_Laser = new System.Windows.Forms.Button();
            this.button_Search = new System.Windows.Forms.Button();
            this.button_Lock = new System.Windows.Forms.Button();
            this.button_TargetType = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.buttonJog = new System.Windows.Forms.Button();
            this.buttonCamera = new System.Windows.Forms.Button();
            this.panel4SubViews = new TSU.Views.TsuPanel();
            this.panelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_bulle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Battery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_OnOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMain
            // 
            this.panelMain.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelMain.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.panelMain.Controls.Add(this.buttonGotoAll);
            this.panelMain.Controls.Add(this.comboBox_PortCom);
            this.panelMain.Controls.Add(this.buttonGoto);
            this.panelMain.Controls.Add(this.label_numberOfMeas);
            this.panelMain.Controls.Add(this.textBox_numberOfMeas);
            this.panelMain.Controls.Add(this.button_All);
            this.panelMain.Controls.Add(this.button_Rec);
            this.panelMain.Controls.Add(this.button_Distance);
            this.panelMain.Controls.Add(this.comboBox_TCP_IP);
            this.panelMain.Controls.Add(this.Button_OnOff);
            this.panelMain.Controls.Add(this.panelLog);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(0, 0);
            this.panelMain.Name = "panelMain";
            this.panelMain.Padding = new System.Windows.Forms.Padding(10);
            this.panelMain.Size = new System.Drawing.Size(627, 401);
            this.panelMain.TabIndex = 3;
            // 
            // buttonGotoAll
            // 
            this.buttonGotoAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonGotoAll.BackColor = System.Drawing.Color.Transparent;
            this.buttonGotoAll.BackgroundImage = global::TSU.Properties.Resources.TS60_Goto_All;
            this.buttonGotoAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonGotoAll.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.buttonGotoAll.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonGotoAll.Location = new System.Drawing.Point(230, 349);
            this.buttonGotoAll.Name = "buttonGotoAll";
            this.buttonGotoAll.Size = new System.Drawing.Size(69, 47);
            this.buttonGotoAll.TabIndex = 19;
            this.buttonGotoAll.UseVisualStyleBackColor = true;
            this.buttonGotoAll.Click += new System.EventHandler(this.buttonGotoAll_Click);
            // 
            // comboBox_PortCom
            // 
            this.comboBox_PortCom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_PortCom.BackColor = System.Drawing.Color.Snow;
            this.comboBox_PortCom.ForeColor = System.Drawing.Color.Black;
            this.comboBox_PortCom.FormattingEnabled = true;
            this.comboBox_PortCom.Items.AddRange(new object[] {
            "1212"});
            this.comboBox_PortCom.Location = new System.Drawing.Point(504, 375);
            this.comboBox_PortCom.MaxDropDownItems = 4;
            this.comboBox_PortCom.Name = "comboBox_PortCom";
            this.comboBox_PortCom.Size = new System.Drawing.Size(50, 21);
            this.comboBox_PortCom.TabIndex = 18;
            this.comboBox_PortCom.Text = "1212";
            // 
            // buttonGoto
            // 
            this.buttonGoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonGoto.BackColor = System.Drawing.Color.Transparent;
            this.buttonGoto.BackgroundImage = global::TSU.Properties.Resources.TS60_Goto;
            this.buttonGoto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonGoto.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.buttonGoto.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.buttonGoto.Location = new System.Drawing.Point(155, 349);
            this.buttonGoto.Name = "buttonGoto";
            this.buttonGoto.Size = new System.Drawing.Size(69, 47);
            this.buttonGoto.TabIndex = 17;
            this.buttonGoto.UseVisualStyleBackColor = true;
            this.buttonGoto.Click += new System.EventHandler(this.buttonGoto_Click);
            // 
            // label_numberOfMeas
            // 
            this.label_numberOfMeas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label_numberOfMeas.AutoSize = true;
            this.label_numberOfMeas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_numberOfMeas.ForeColor = System.Drawing.Color.Snow;
            this.label_numberOfMeas.Location = new System.Drawing.Point(428, 350);
            this.label_numberOfMeas.Name = "label_numberOfMeas";
            this.label_numberOfMeas.Size = new System.Drawing.Size(83, 20);
            this.label_numberOfMeas.TabIndex = 16;
            this.label_numberOfMeas.Text = "# of meas.";
            // 
            // textBox_numberOfMeas
            // 
            this.textBox_numberOfMeas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox_numberOfMeas.BackColor = System.Drawing.Color.Snow;
            this.textBox_numberOfMeas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.textBox_numberOfMeas.Location = new System.Drawing.Point(517, 350);
            this.textBox_numberOfMeas.Name = "textBox_numberOfMeas";
            this.textBox_numberOfMeas.Size = new System.Drawing.Size(26, 20);
            this.textBox_numberOfMeas.TabIndex = 15;
            this.textBox_numberOfMeas.Text = "1";
            this.textBox_numberOfMeas.TextChanged += new System.EventHandler(this.textBox_numberOfMeas_TextChanged);
            // 
            // button_All
            // 
            this.button_All.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_All.BackColor = System.Drawing.Color.Transparent;
            this.button_All.BackgroundImage = global::TSU.Properties.Resources.TS60_ALL;
            this.button_All.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_All.Location = new System.Drawing.Point(5, 349);
            this.button_All.Name = "button_All";
            this.button_All.Size = new System.Drawing.Size(69, 46);
            this.button_All.TabIndex = 12;
            this.button_All.UseVisualStyleBackColor = true;
            this.button_All.Click += new System.EventHandler(this.button_All_Click);
            // 
            // button_Rec
            // 
            this.button_Rec.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_Rec.BackColor = System.Drawing.Color.Transparent;
            this.button_Rec.BackgroundImage = global::TSU.Properties.Resources.TS60_HzV;
            this.button_Rec.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Rec.Location = new System.Drawing.Point(80, 349);
            this.button_Rec.Name = "button_Rec";
            this.button_Rec.Size = new System.Drawing.Size(69, 46);
            this.button_Rec.TabIndex = 10;
            this.button_Rec.UseVisualStyleBackColor = false;
            this.button_Rec.Click += new System.EventHandler(this.button_Rec_Click);
            // 
            // button_Distance
            // 
            this.button_Distance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_Distance.BackColor = System.Drawing.Color.Transparent;
            this.button_Distance.BackgroundImage = global::TSU.Properties.Resources.TS60_Dist;
            this.button_Distance.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Distance.Location = new System.Drawing.Point(305, 348);
            this.button_Distance.Name = "button_Distance";
            this.button_Distance.Size = new System.Drawing.Size(69, 47);
            this.button_Distance.TabIndex = 8;
            this.button_Distance.UseVisualStyleBackColor = true;
            this.button_Distance.Click += new System.EventHandler(this.button_Distance_Click);
            // 
            // comboBox_TCP_IP
            // 
            this.comboBox_TCP_IP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_TCP_IP.BackColor = System.Drawing.Color.Snow;
            this.comboBox_TCP_IP.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.comboBox_TCP_IP.FormattingEnabled = true;
            this.comboBox_TCP_IP.Items.AddRange(new object[] {
            "192.168.0.100"});
            this.comboBox_TCP_IP.Location = new System.Drawing.Point(386, 375);
            this.comboBox_TCP_IP.MaxDropDownItems = 4;
            this.comboBox_TCP_IP.Name = "comboBox_TCP_IP";
            this.comboBox_TCP_IP.Size = new System.Drawing.Size(112, 21);
            this.comboBox_TCP_IP.TabIndex = 4;
            this.comboBox_TCP_IP.Text = "192.168.0.100";
            // 
            // Button_OnOff
            // 
            this.Button_OnOff.AccessibleDescription = "";
            this.Button_OnOff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_OnOff.BackColor = System.Drawing.Color.Transparent;
            this.Button_OnOff.BackgroundImage = global::TSU.Properties.Resources.TS60_Connect;
            this.Button_OnOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_OnOff.Location = new System.Drawing.Point(560, 350);
            this.Button_OnOff.Name = "Button_OnOff";
            this.Button_OnOff.Size = new System.Drawing.Size(64, 46);
            this.Button_OnOff.TabIndex = 3;
            this.Button_OnOff.UseVisualStyleBackColor = true;
            this.Button_OnOff.Click += new System.EventHandler(this.Button_OnOff_Click);
            // 
            // panelLog
            // 
            this.panelLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelLog.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelLog.BackColor = System.Drawing.Color.DimGray;
            this.panelLog.Location = new System.Drawing.Point(5, 6);
            this.panelLog.Name = "panelLog";
            this.panelLog.Size = new System.Drawing.Size(616, 338);
            this.panelLog.TabIndex = 2;
            // 
            // labelMeteo
            // 
            this.labelMeteo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMeteo.BackColor = System.Drawing.Color.Transparent;
            this.labelMeteo.ForeColor = System.Drawing.Color.Red;
            this.labelMeteo.Location = new System.Drawing.Point(918, 5);
            this.labelMeteo.Name = "labelMeteo";
            this.labelMeteo.Size = new System.Drawing.Size(71, 41);
            this.labelMeteo.TabIndex = 27;
            this.labelMeteo.Text = "T° 20.0C       P 980hPa         H 60%";
            this.labelMeteo.Click += new System.EventHandler(this.labelMeteo_Click);
            // 
            // button_face
            // 
            this.button_face.BackColor = System.Drawing.Color.Transparent;
            this.button_face.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_face.FlatAppearance.BorderSize = 0;
            this.button_face.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.button_face.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.button_face.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_face.Image = global::TSU.Properties.Resources.TS60_CercleGauche;
            this.button_face.Location = new System.Drawing.Point(85, 4);
            this.button_face.Name = "button_face";
            this.button_face.Size = new System.Drawing.Size(40, 40);
            this.button_face.TabIndex = 26;
            this.button_face.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button_face.UseVisualStyleBackColor = true;
            this.button_face.Click += new System.EventHandler(this.button_face_Click);
            // 
            // label_Bulle
            // 
            this.label_Bulle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_Bulle.AutoSize = true;
            this.label_Bulle.BackColor = System.Drawing.Color.Transparent;
            this.label_Bulle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Bulle.ForeColor = System.Drawing.Color.Snow;
            this.label_Bulle.Location = new System.Drawing.Point(1043, 6);
            this.label_Bulle.Name = "label_Bulle";
            this.label_Bulle.Size = new System.Drawing.Size(26, 13);
            this.label_Bulle.TabIndex = 20;
            this.label_Bulle.Text = "L & T";
            // 
            // pictureBox_bulle
            // 
            this.pictureBox_bulle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox_bulle.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_bulle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox_bulle.Image = global::TSU.Properties.Resources.TS60_Bulle_Bad;
            this.pictureBox_bulle.Location = new System.Drawing.Point(995, 6);
            this.pictureBox_bulle.Name = "pictureBox_bulle";
            this.pictureBox_bulle.Size = new System.Drawing.Size(42, 40);
            this.pictureBox_bulle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_bulle.TabIndex = 19;
            this.pictureBox_bulle.TabStop = false;
            // 
            // pictureBox_Battery
            // 
            this.pictureBox_Battery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox_Battery.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_Battery.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox_Battery.Image = global::TSU.Properties.Resources.TS60_Battery_Bad;
            this.pictureBox_Battery.Location = new System.Drawing.Point(1114, 6);
            this.pictureBox_Battery.Name = "pictureBox_Battery";
            this.pictureBox_Battery.Size = new System.Drawing.Size(19, 40);
            this.pictureBox_Battery.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Battery.TabIndex = 18;
            this.pictureBox_Battery.TabStop = false;
            // 
            // button_ATR
            // 
            this.button_ATR.BackColor = System.Drawing.Color.Transparent;
            this.button_ATR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_ATR.FlatAppearance.BorderSize = 0;
            this.button_ATR.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.button_ATR.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.button_ATR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_ATR.ForeColor = System.Drawing.Color.Transparent;
            this.button_ATR.Image = global::TSU.Properties.Resources.TS60_LOCK_On;
            this.button_ATR.Location = new System.Drawing.Point(131, 4);
            this.button_ATR.Name = "button_ATR";
            this.button_ATR.Size = new System.Drawing.Size(40, 40);
            this.button_ATR.TabIndex = 14;
            this.button_ATR.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_ATR.UseVisualStyleBackColor = false;
            this.button_ATR.Click += new System.EventHandler(this.button_ATR_Click);
            // 
            // pictureBox_OnOff
            // 
            this.pictureBox_OnOff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox_OnOff.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_OnOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox_OnOff.Image = global::TSU.Properties.Resources.TS60_OFF;
            this.pictureBox_OnOff.Location = new System.Drawing.Point(1139, 6);
            this.pictureBox_OnOff.Name = "pictureBox_OnOff";
            this.pictureBox_OnOff.Size = new System.Drawing.Size(40, 40);
            this.pictureBox_OnOff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_OnOff.TabIndex = 17;
            this.pictureBox_OnOff.TabStop = false;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(13, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 27);
            this.label1.TabIndex = 2;
            this.label1.Text = "TS60";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Timer_check
            // 
            this.Timer_check.Interval = 20000;
            this.Timer_check.Tick += new System.EventHandler(this.Timer_check_TS60_Tick);
            // 
            // button_Laser
            // 
            this.button_Laser.BackColor = System.Drawing.Color.Transparent;
            this.button_Laser.BackgroundImage = global::TSU.Properties.Resources.TS60_Laser_Off;
            this.button_Laser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_Laser.FlatAppearance.BorderSize = 0;
            this.button_Laser.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.button_Laser.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.button_Laser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Laser.Location = new System.Drawing.Point(269, 6);
            this.button_Laser.Name = "button_Laser";
            this.button_Laser.Size = new System.Drawing.Size(40, 40);
            this.button_Laser.TabIndex = 28;
            this.button_Laser.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button_Laser.UseVisualStyleBackColor = true;
            this.button_Laser.Click += new System.EventHandler(this.button_Laser_Click);
            // 
            // button_Search
            // 
            this.button_Search.BackColor = System.Drawing.Color.Transparent;
            this.button_Search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_Search.FlatAppearance.BorderSize = 0;
            this.button_Search.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.button_Search.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.button_Search.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Search.Image = global::TSU.Properties.Resources.TS60_Search;
            this.button_Search.Location = new System.Drawing.Point(223, 4);
            this.button_Search.Name = "button_Search";
            this.button_Search.Size = new System.Drawing.Size(40, 40);
            this.button_Search.TabIndex = 29;
            this.button_Search.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button_Search.UseVisualStyleBackColor = true;
            this.button_Search.Click += new System.EventHandler(this.button_Search_Click);
            // 
            // button_Lock
            // 
            this.button_Lock.BackColor = System.Drawing.Color.Transparent;
            this.button_Lock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_Lock.FlatAppearance.BorderSize = 0;
            this.button_Lock.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.button_Lock.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.button_Lock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Lock.ForeColor = System.Drawing.Color.Transparent;
            this.button_Lock.Image = global::TSU.Properties.Resources.TS60_NotLocked;
            this.button_Lock.Location = new System.Drawing.Point(177, 4);
            this.button_Lock.Name = "button_Lock";
            this.button_Lock.Size = new System.Drawing.Size(40, 40);
            this.button_Lock.TabIndex = 30;
            this.button_Lock.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_Lock.UseVisualStyleBackColor = false;
            this.button_Lock.Click += new System.EventHandler(this.button_Lock_Click);
            // 
            // button_TargetType
            // 
            this.button_TargetType.BackColor = System.Drawing.Color.Transparent;
            this.button_TargetType.BackgroundImage = global::TSU.Properties.Resources.TS60_Unknown_Target;
            this.button_TargetType.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_TargetType.FlatAppearance.BorderSize = 0;
            this.button_TargetType.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.button_TargetType.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.button_TargetType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_TargetType.ForeColor = System.Drawing.Color.Transparent;
            this.button_TargetType.Location = new System.Drawing.Point(315, 6);
            this.button_TargetType.Name = "button_TargetType";
            this.button_TargetType.Size = new System.Drawing.Size(40, 40);
            this.button_TargetType.TabIndex = 31;
            this.button_TargetType.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_TargetType.UseVisualStyleBackColor = false;
            this.button_TargetType.Click += new System.EventHandler(this.button_TargetType_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(8, 52);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panelMain);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.splitContainer1.Panel2.Controls.Add(this.buttonJog);
            this.splitContainer1.Panel2.Controls.Add(this.buttonCamera);
            this.splitContainer1.Panel2.Controls.Add(this.panel4SubViews);
            this.splitContainer1.Size = new System.Drawing.Size(1171, 401);
            this.splitContainer1.SplitterDistance = 627;
            this.splitContainer1.TabIndex = 32;
            // 
            // buttonJog
            // 
            this.buttonJog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonJog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.buttonJog.BackgroundImage = global::TSU.Properties.Resources.At40x_Jog;
            this.buttonJog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonJog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonJog.ImageKey = "(none)";
            this.buttonJog.Location = new System.Drawing.Point(5, 350);
            this.buttonJog.Name = "buttonJog";
            this.buttonJog.Size = new System.Drawing.Size(69, 46);
            this.buttonJog.TabIndex = 57;
            this.buttonJog.UseVisualStyleBackColor = false;
            this.buttonJog.Click += new System.EventHandler(this.ButtonJog_Click);
            // 
            // buttonCamera
            // 
            this.buttonCamera.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCamera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.buttonCamera.BackgroundImage = global::TSU.Properties.Resources.At40x_Camera;
            this.buttonCamera.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonCamera.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCamera.ImageKey = "(none)";
            this.buttonCamera.Location = new System.Drawing.Point(80, 350);
            this.buttonCamera.Name = "buttonCamera";
            this.buttonCamera.Size = new System.Drawing.Size(69, 46);
            this.buttonCamera.TabIndex = 58;
            this.buttonCamera.UseVisualStyleBackColor = false;
            this.buttonCamera.Click += new System.EventHandler(this.ButtonCamera_Click);
            // 
            // panel4SubViews
            // 
            this.panel4SubViews.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4SubViews.BackColor = System.Drawing.Color.DimGray;
            this.panel4SubViews.Location = new System.Drawing.Point(5, 9);
            this.panel4SubViews.Name = "panel4SubViews";
            this.panel4SubViews.Size = new System.Drawing.Size(532, 335);
            this.panel4SubViews.TabIndex = 55;
            // 
            // View
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(1184, 460);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.button_TargetType);
            this.Controls.Add(this.button_Search);
            this.Controls.Add(this.button_Lock);
            this.Controls.Add(this.button_Laser);
            this.Controls.Add(this.labelMeteo);
            this.Controls.Add(this.button_face);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_Bulle);
            this.Controls.Add(this.pictureBox_OnOff);
            this.Controls.Add(this.pictureBox_bulle);
            this.Controls.Add(this.pictureBox_Battery);
            this.Controls.Add(this.button_ATR);
            this.Name = "View";
            this.Text = "Ts60View";
            this.Load += new System.EventHandler(this.Ts60View_Load);
            this.Controls.SetChildIndex(this.button_ATR, 0);
            this.Controls.SetChildIndex(this.pictureBox_Battery, 0);
            this.Controls.SetChildIndex(this.pictureBox_bulle, 0);
            this.Controls.SetChildIndex(this.pictureBox_OnOff, 0);
            this.Controls.SetChildIndex(this.label_Bulle, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.button_face, 0);
            this.Controls.SetChildIndex(this.labelMeteo, 0);
            this.Controls.SetChildIndex(this.button_Laser, 0);
            this.Controls.SetChildIndex(this.button_Lock, 0);
            this.Controls.SetChildIndex(this.button_Search, 0);
            this.Controls.SetChildIndex(this.button_TargetType, 0);
            this.Controls.SetChildIndex(this.buttonForFocusWithTabIndex0, 0);
            this.Controls.SetChildIndex(this.splitContainer1, 0);
            this.panelMain.ResumeLayout(false);
            this.panelMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_bulle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Battery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_OnOff)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Panel panelLog;
        private System.Windows.Forms.Button Button_OnOff;
        private System.Windows.Forms.ComboBox comboBox_TCP_IP;
        private System.Windows.Forms.Button button_Distance;
        private System.Windows.Forms.Button button_Rec;
        private System.Windows.Forms.Button button_All;
        private System.Windows.Forms.Button button_ATR;
        private System.Windows.Forms.Timer Timer_check;
        public System.Windows.Forms.TextBox textBox_numberOfMeas;
        private System.Windows.Forms.Label label_numberOfMeas;
        private System.Windows.Forms.PictureBox pictureBox_OnOff;
        private System.Windows.Forms.PictureBox pictureBox_bulle;
        private System.Windows.Forms.PictureBox pictureBox_Battery;
        private System.Windows.Forms.Label label_Bulle;
        private System.Windows.Forms.Button button_face;
        private System.Windows.Forms.Label labelMeteo;
        private System.Windows.Forms.Button buttonGoto;
        private System.Windows.Forms.ComboBox comboBox_PortCom;
        private System.Windows.Forms.Button button_Laser;
        private System.Windows.Forms.Button button_Search;
        private System.Windows.Forms.Button button_Lock;
        private System.Windows.Forms.Button button_TargetType;
        private System.Windows.Forms.Button buttonGotoAll;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button buttonJog;
        private System.Windows.Forms.Button buttonCamera;
        private Views.TsuPanel panel4SubViews;
    }
}