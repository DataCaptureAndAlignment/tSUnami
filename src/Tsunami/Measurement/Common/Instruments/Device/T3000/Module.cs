﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using M = TSU.Common.Measures;
using TSU;
using TSU.Common.Instruments.Adapter;
using TSU.ENUM;
using P = TSU.Views.Message.ProgressMessage;
using R = TSU.Properties.Resources;
using TSU.Tools;
using System.Xml.Serialization;
using System.Threading.Tasks;

namespace TSU.Common.Instruments.Device.T3000
{
    [XmlType(TypeName = "T3000.Module")]
    public class Module : Device.TC2002.Module, IConnectable
    {
        [XmlIgnore]
        public new Device.T3000.View View
        {
            get
            {
                return this._TsuView as View;
            }
            set
            {
                this._TsuView = value;
            }
        }

        public Device.DI2000.Instrument DistanceMeter { get; set; }
        public Device.DI2000.Module DistanceMeterModule { get; set; }

        public Module() : base() { }


        public Module(TSU.Module parentModule, Instruments.Instrument i)
            : base(parentModule, i)
        {
            this.Instrument = i;
            this._Name = i._Name;
        }
    }
}
