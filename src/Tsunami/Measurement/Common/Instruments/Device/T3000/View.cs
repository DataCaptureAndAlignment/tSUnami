﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TSU.Common.Instruments.Device.T3000
{
    public partial class View : TSU.Common.Instruments.Device.TC2002.View
    {
        public View(TSU.Common.Instruments.Module instrumentModule) : base(instrumentModule)
        {
            labelTitle.Text = "T3000";
        }
    }
}
