﻿using System.Xml.Serialization;


namespace TSU.Common.Instruments.Device.T3000
{
    [XmlType(TypeName = "T3000.Instrument")]
    public class Instrument : TotalStation
    {
        public Instrument()
        {
        }
    }

}
