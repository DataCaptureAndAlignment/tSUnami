﻿using System.Xml.Serialization;
using System;
using M = TSU.Common.Measures;
using R = TSU.Properties.Resources;
using I = TSU.Common.Instruments;
using TSU.Common.Compute;

namespace TSU.Common.Instruments.Device.Simulator
{
    [XmlType(TypeName = "Simulator.Module")]
    public class Module : PolarModule
    {
        internal bool bMeasureIsImpossible = false;

        public Module() : base() { }

        public Module(TSU.Module parentModule, Instruments.Instrument i)
            : base(parentModule)
        {
            this.Instrument = i;
            this.ToBeMeasureReceived += delegate (object sender, M.MeasurementEventArgs e)
            {
                if (toBeMeasuredData != null)
                {
                    PrepareMeasure();
                }
            };
        }
        public override void Initialize()
        {
            base.Initialize();
            this.InstrumentIsOn = true; // manual instrument should always be ON.
        }

        public override void PrepareMeasure()
        {
            base.PrepareMeasure();
            this.OtherFaceWanted = (_ToBeMeasureTheodoliteData.Face == I.FaceType.DoubleFace);
            this.NumberOfMeasureOnThisFace = _BeingMeasuredTheodoliteData.NumberOfMeasureToAverage;
            this._BeingMeasuredOnThisFace = new System.Collections.Generic.List<Polar.Measure>();
            this._AlreadyMeasuredFace = null;

        }

        internal override void SetDefaultStationParameter(Polar.Station.Parameters stationParameters)
        {
            base.SetDefaultStationParameter(stationParameters);
            stationParameters.DefaultMeasure.Distance.Reflector = Analysis.Instrument.GetReflector("CHAINE");
        }

        public override bool IsReadyToMeasure()
        {
            return true;
        }

        internal void LockUnLock()
        {
            // not applicable
        }

        public override void CheckIncomingMeasure(Polar.Measure m)
        {
            base.CheckIncomingMeasure(m);
            if (m._Status.Type == M.States.Types.Good)
                Measure();
        }

        internal void ValidateEntry(DoubleValue doubleValue1, DoubleValue doubleValue2, DoubleValue doubleValue3, bool LaunchNextStep)
        {
            if (BeingMeasured == null) throw new Exception(R.T_NOTHING_TO_MEASURE);
            if (doubleValue1 == null || doubleValue3 == null || doubleValue2 == null)
            {
                this.HaveFailed();
                throw new System.Exception(R.T220);
            }
            Survey.Modulo400(doubleValue1);
            Survey.Modulo400(doubleValue2);

            // add sigma a priori
            {
                Instruments.Instrument instrument = this.Instrument;
                SigmaForAInstrumentCouple sigma = TSU.Tsunami2.Preferences.Values.InstrumentSlashTargetCoupleSigmas.Find(
                    x => x.Instrument.ToUpper() == instrument._Model.ToUpper());

                if (sigma == null)
                    sigma = TSU.Tsunami2.Preferences.Values.InstrumentSlashTargetCoupleSigmas.Find(x => x.Instrument.ToUpper() == "Default" && x.Target.ToUpper() == "Default");
                if (sigma != null)
                {
                    doubleValue1.Sigma = sigma.sigmaAnglCc / 10000;
                    doubleValue2.Sigma = sigma.sigmaZenDCc / 10000;
                    doubleValue3.Sigma = sigma.sigmaDistMm / 1000;
                }
            }


            switch ((ToBeMeasuredData as Polar.Measure).Face)
            {
                case FaceType.Face1:
                    if (doubleValue2.Value > 200)
                        throw new Exception($"{R.T_VERTICAL_ANGLE_IS_BIGGER_THAN_200_GON};{R.T_ARE_YOU_SURE_TO_BE_IN_FACE1_POSITION_AS_ASKED}");
                    break;
                case FaceType.Face2:
                    if (doubleValue2.Value < 200)
                        throw new Exception($"{R.T_VERTICAL_ANGLE_IS_SMALLER_THAN_200_GON};{R.T_ARE_YOU_SURE_TO_BE_IN_FACE2_POSITION_AS_ASKED}");
                    break;
                case FaceType.DoubleFace:
                    if (doubleValue2.Value > 200)
                        _BeingMeasuredTheodoliteData.Face = FaceType.Face2;
                    else
                        _BeingMeasuredTheodoliteData.Face = FaceType.Face1;

                    break;
                case FaceType.Face1Reducted:
                case FaceType.UnknownFace:
                case FaceType.DoubleFaceReducted:
                default:
                    throw new Exception(R.T_FACE_PARAMETER_STRANGE_ACQUISITION_CANCELLED);
            }
            //this.StartMeasurement();

            Polar.Measure m = BeingMeasured as Polar.Measure;
            m._Date = System.DateTime.Now;
            m.Angles.Raw.Horizontal = doubleValue1;
            m.Angles.Raw.Vertical = doubleValue2;
            m.Distance.Raw = doubleValue3;

            if (LaunchNextStep)
                this.DoMeasureCorrectionsThenNextMeasurementStep(m);
        }

        internal override void ValidateMeasurement(Polar.Measure m)
        {
            base.ValidateMeasurement(m);
            this.FinishMeasurement();
            this.ReportMeasurement();
        }

        public override void MeasureLikeThis()
        {
            
            try
            {
                bool success = false;
                Action a = () =>
                {
                    if (this.CancelByMessageBeforeMeasure())
                    {
                        this.BeingMeasured = null;
                        throw new CancelException();
                    }


                    base.MeasureLikeThis();

                    Random rnd = new Random(1);
                    DoubleValue o1 = new DoubleValue(rnd.NextDouble() * 400, rnd.NextDouble());
                    DoubleValue o2 = new DoubleValue(rnd.NextDouble() * 200, rnd.NextDouble());
                    DoubleValue o3 = new DoubleValue(rnd.NextDouble() * 20, rnd.NextDouble());
                    if (bMeasureIsImpossible)
                        throw (new SimulatorException("Impossible to measure"));

                    ValidateEntry(o1, o2, o3, LaunchNextStep: false); // next step will be launch lower
                    success = true;
                };
                // launch the view in the main UI
                TSU.Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(a);
                if (!success)
                    throw new CancelException();

                //Launch next submeasurement
                Polar.Measure m = BeingMeasured as Polar.Measure;

                this.DoMeasureCorrectionsThenNextMeasurementStep(m);
            }
            catch (CancelException)
            {
                this.DeclareMeasurementCancelled();
            }
        }
    }
}
