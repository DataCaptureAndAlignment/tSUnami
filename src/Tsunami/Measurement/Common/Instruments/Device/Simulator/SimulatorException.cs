﻿using System;
using System.Runtime.Serialization;

namespace TSU.Common.Instruments.Device.Simulator
{
    [Serializable]
    internal class SimulatorException : Exception
    {
        public SimulatorException()
        {
        }

        public SimulatorException(string message) : base(message)
        {
        }

        public SimulatorException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected SimulatorException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}