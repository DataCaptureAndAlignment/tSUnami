﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TSU.Logs;
using TSU.Views.Message;
using R = TSU.Properties.Resources;
using T = TSU.Tools;


namespace TSU.Common.Instruments.Device.Simulator
{
    public class SimulatorView : Device.View
    {

        internal new Module Module
        {
            get
            {
                return this._Module as Module;
            }
            set
            {
                this._Module = value;
            }
        }

        public SimulatorView()
            : base()
        {
            InitializeComponent();
        }

        public Views.BigButton valid;
        public Views.BigButton impossible;
        public Views.BigButton cancel;

        public SimulatorView(TSU.IModule parentModule)
            : base(parentModule)
        {
            InitializeComponent();
            InitializeComponent2();
            // add valid button
            valid = new Views.BigButton("Get random observations", R.MessageTsu_OK);
            valid.TabIndex = 20;
            valid.Dock = DockStyle.Top;
            valid.Visible = true;
            valid.BigButtonClicked += delegate
            {
                try
                {
                    this.Module.Measure();
                }
                catch (Exception ex)
                {
                    new MessageInput(MessageType.Critical, ex.Message).Show();
                }

            };

            impossible = new Views.BigButton("Measure possible/impossible", R.StatusBad);

            impossible.TabIndex = 21;
            impossible.Dock = DockStyle.Top;
            impossible.Visible = true;

            impossible.BigButtonClicked += delegate
            {
                try
                {
                    var sm = (this.Module as Simulator.Module);
                    sm.bMeasureIsImpossible = !sm.bMeasureIsImpossible;
                }
                catch (Exception ex)
                {
                    new MessageInput(MessageType.Critical, ex.Message).Show();
                }

            };
            InitializeLog();
            this.Controls.Add(valid);
            this.Controls.Add(impossible);
        }

        private void InitializeLog()
        {
            LogView = new Logs.LogView();
            LogView.Dock = DockStyle.Fill;
            this.Controls.Add(LogView);
        }

        public override void MeasureAsync()
        {
            base.MeasureAsync(); // do nothing
            TryAction(LaunchMeasurement);

        }

        private void LaunchMeasurement()
        {
            Result r = new Result();
            // if (this.Module._ToBeMeasureTheodoliteData == null) throw new Exception("Nothing to measure");
            if (this.Module._ToBeMeasureTheodoliteData == null) throw new Exception(R.T_NOTHING_TO_MEASURE);

            TsuTask task = new TsuTask()
            {
                ActionName = "Measuring...",

                estimatedTimeInMilliSec = 0,
                timeOutInSec = 0,
                preAction = () =>
                {
                },
                mainAction = () =>
                {
                    Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(() => r.MergeString(this.Module.Measure()));
                },
                cancelAction = () =>
                {
                    this.Module.CancelMeasureInProgress();
                },
                postAction = () =>
                {
                    this.Module.BeingMeasured = null;
                    this.Module.DistanceWanted = true; // by default sot that measure() from polar Module take all.
                    if (r.Success)
                    {
                        // already reporte in overrded version of ValidateMeasurement.
                    }
                    else
                    {
                        Logs.Log.AddEntryAsResult(this.Module, r);
                    }
                }
            };

            this.RunTasksWithoutWaitingMessage(new List<TsuTask>() { task, }, "Measure");

        }

        private void InitializeComponent2()
        {

        }
        private void InitializeComponent()
        {

            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.ClientSize = new System.Drawing.Size(1940, 1100);
            this.Margin = new System.Windows.Forms.Padding(48, 22, 48, 22);
            this.Name = "ManualTheodoliteView";
            this.Padding = new System.Windows.Forms.Padding(0);
            this.ResumeLayout(false);

        }


        private void tb_Click(object sender, EventArgs e)
        {
            DoubleValue d = new DoubleValue();
            Device.Manual.View Calc = new Manual.View(d);

            //if you put text to "null" the previous button will nbe larger
            // by  default the action is to add the text from the button change it if needed by addind an eventhandler parameter
            Calc.array[2, 3].Text = "Valid"; Calc.array[2, 3].Click += Calc.Validate_Click;
            Calc.array[2, 4].Text = "null";
            Calc.array[3, 3].Text = "null";
            Calc.array[3, 4].Text = "null";
            Calc.array[2, 3].TabIndex = 0;
            Calc.ShowDialog(this);

            if (d.Value == TSU.Tsunami2.Preferences.Values.na)
            {
                new MessageInput(MessageType.Critical, R.StringLevel_WrongCellValue).Show();
            }

            (sender as TextBox).Text = d.Value.ToString();
            (sender as TextBox).Tag = d;
            SwitchToNextControl(sender);
        }

        private void SwitchToNextControl(object sender)
        {
            var n = this.GetNextControl(sender as Control, true);
            if (n != null) n.Focus();
        }

        private void tb_Change(object sender, EventArgs e)
        {
            try
            {

                (sender as TextBox).Tag = new DoubleValue(T.Conversions.Numbers.ToDouble((sender as TextBox).Text, true));
            }
            catch (Exception)
            {

            }
        }

        private void tb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                (sender as TextBox).Tag = new DoubleValue(T.Conversions.Numbers.ToDouble((sender as TextBox).Text, true), 0);
                SwitchToNextControl(sender);
            }
        }
    }
}
