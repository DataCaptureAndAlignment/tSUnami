﻿using MathNet.Numerics.Statistics;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;
using System.Xml.Serialization;
using TSU.Logs;
using TSU.Views.Message;
using Wyler.WyBus;
using M = TSU;
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments.Device.WYLER
{
    [XmlType(TypeName = "WYLER.Module")]
    public class Module : Instruments.Module, IConnectable
    {
        public Instrument WYLER
        {
            get
            {
                return this.Instrument as Instrument;
            }
        }

        public Module(M.Module parentModule, Instruments.Instrument i)
           : base(parentModule)
        {
            this.Instrument = i;
            //this.Initialize(); //already called in the base module MODULE
        }

        public Measures.MeasureOfLevel JustMeasure; // mesure qui sera envoyée au module nivellement
        //public D_H_Reading MeasureData { get; set; }
        internal SerialPort myPort;
        internal List<string> listPortName;
        internal int listPortIndex;
        internal string portName;
        internal DateTime date = DateTime.Now;
        internal WyBus _wyBus;
        internal WyBusMeasuringDevice device = null;
        internal bool wyBusConnected = false;

        const int STD_RANGE_BEGIN = 1; // Range for devices supporting "wybus standard" protocol. Convention for max range: 1 .. 99.
        const int STD_RANGE_END = 20; // It should be <= 99. Set low value to speed up search.
        const int EXT_RANGE_BEGIN = 100; // Range for devices supporting "wybus extended" protocol. Convention for max range: 100 .. 253
        const int EXT_RANGE_END = 110; // It should be <= 253, typically below 120. Set low value (not lower than 100) to speed up search.

        const int SAMPLING_TIME_MS = 100; // in [ms], range 100 ... 8000 ms. It is the sampling interval and integration time of each measurement.

        const int HISTORY_DURATION_MS = 1000; // in [ms], history depth (of GetSampledAngles() and GetSampledTemperatures())

        public override void Initialize()
        {
            base.Initialize();

            Debug.WriteInConsole("Wyler INSTRUMENT");

        }

        public override Result Connect()
        {
            Result connectionResult = new Result();
            _wyBus = new WyBus(true);
            Debug.WriteInConsole($"DLL version: {_wyBus.Version}");
            Debug.WriteInConsole($"{_wyBus.AvailableInterfaces.Count} interfaces found.");

            string selectedInterface = "";
            string interfaceLog = "Search interfaces\r\n";
            interfaceLog += $"_wyBus.AvailableInterfaces.Count={_wyBus.AvailableInterfaces.Count}\r\n";
            for (int i = 0; i < _wyBus.AvailableInterfaces.Count; i++)
            {
                WyBusInterface itf = _wyBus.AvailableInterfaces[i];
                interfaceLog += $"AvailableInterfaces[{i}].Name={itf.Name}\r\n";
                if (itf.Name == "usb://" + this.WYLER._SerialNumber)
                {
                    selectedInterface = itf.Name;
                }
            }
            interfaceLog += "Search devices\r\n";
            _wyBus.ListDevices(
                // To find Zerotronic, Zeromatic and all other devices which are using wybus standard protocol.
                new ScanRange(STD_RANGE_BEGIN, STD_RANGE_END, WyBusProtocolType.Usb, selectedInterface),
                // To find BlueLevel devices and all other devices which are using wybus exteded protocol. 
                new ScanRange(EXT_RANGE_BEGIN, EXT_RANGE_END, WyBusProtocolType.Extended, selectedInterface)
            );
            interfaceLog += $"_wyBus.AvailableMeasuringDevices.Count={_wyBus.AvailableMeasuringDevices.Count}\r\n";

            if (_wyBus.AvailableMeasuringDevices.Count == 1)
            {
                device = _wyBus.AvailableMeasuringDevices[0];
                connectionResult.Success = true;
                connectionResult.AccessToken = $"Successfully connected to {this.WYLER._Name}";
                this.wyBusConnected = true;
            }
            else
            {
                connectionResult.Success = false;
                connectionResult.ErrorMessage = "No Device Found !";
                Log.AddEntryAsPayAttentionOf(this, interfaceLog);
                _wyBus.Dispose();
                _wyBus = null;
            }
            return connectionResult;
        }

        public override Result Disconnect()
        {
            Result connectionResult = new Result();
            if (wyBusConnected)
            {
                _wyBus?.Dispose();
                _wyBus = null;
                connectionResult.Success = true;
                connectionResult.AccessToken = $"Successfully ended the connection to {this.WYLER._Name}";
                this.wyBusConnected = false;
            }
            else
            {
                connectionResult.Success = false;
                connectionResult.ErrorMessage = "No connected Wyler Modules to disconnect";
            }
            return connectionResult;
        }

        /// <summary>
        /// This function is supposed to check the temparature
        /// but currently we only use the connection to Clinotronic S
        /// which doesn't have this feature
        /// </summary>
        /// <returns></returns>
        public double ReadingTemperature()
        {
            if (device == null)
                return -99999.99;

            if(!device.DeviceFeatures.Contains(WyBusDeviceFeature.Temperature))
                return -99999.99;
            
            // Set the sampling interval and start polling the devices.
            TimeSpan samplingTimeSpan = TimeSpan.FromMilliseconds(500);
            TimeSpan memoryTimeSpan = TimeSpan.FromMilliseconds(10000);

            //Get the temperature results
            try
            {
                _wyBus.StartSampling(WyBusValueType.Temperature, samplingTimeSpan, memoryTimeSpan, device);
                // DLL starts polling of sensors connected over RS485/RS232.
            }
            catch (WyBusException)
            {
                Debug.WriteInConsole("Start sampling failed");
                return -99999.99;
            }

            // Wait for the sampling to be completed
            Thread.Sleep(memoryTimeSpan + samplingTimeSpan);

            List<WyBusValue> temperatureValues = device.GetSampledTemperatures();
            foreach (var a in temperatureValues)
                Debug.WriteInConsole($"{a.TimeStamp.ToLocalTime():hh:mm:ss:fff}, temperature:{a.Value}");

            List<double> temperatures = temperatureValues.ConvertAll(a => a.Value);
            (double meanTemperature, double stdDevTemperatures) = Statistics.MeanStandardDeviation(temperatures);
            Debug.WriteInConsole($"meanTemperature: {meanTemperature:0.000}");
            Debug.WriteInConsole($"stdDevTemperatures : {stdDevTemperatures:0.000}");

            return meanTemperature;
        }

        public bool ReadingAngle(out DoubleValue ret)
        {
            ret = new DoubleValue();

            if (device == null)
                return false;

            // Set the sampling interval and start polling the devices.
            TimeSpan samplingTimeSpan = TimeSpan.FromMilliseconds(SAMPLING_TIME_MS);
            TimeSpan memoryTimeSpan = TimeSpan.FromMilliseconds(HISTORY_DURATION_MS);

            try
            {
                _wyBus.StartSampling(WyBusValueType.Angle, samplingTimeSpan, memoryTimeSpan, device);
                // DLL starts polling of sensors connected over RS485/RS232.
            }
            catch (WyBusException)
            {
                Debug.WriteInConsole("Start sampling failed");
                return false;
            }

            // Wait for the sampling to be completed
            Thread.Sleep(memoryTimeSpan + samplingTimeSpan);

            // Get the angle results
            List<WyBusValue> angleValues = device.GetSampledAngles();
            foreach (var a in angleValues)
                Debug.WriteInConsole($"{a.TimeStamp.ToLocalTime():hh:mm:ss:fff}, angle:{a.Value * 1000.0:0.000} mrad");

            List<double> angles = angleValues.ConvertAll(a => a.Value * 1000d);
            Debug.WriteInConsole($"Count: {angles.Count}");

            (double meanAngle, double stdDevAngles) = Statistics.MeanStandardDeviation(angles);
            Debug.WriteInConsole($"meanAngle: {meanAngle:0.000} mrad");
            Debug.WriteInConsole($"stdDevAngles : {stdDevAngles:0.000} mrad");

            ret.Value = meanAngle;
            ret.Sigma = stdDevAngles;

            // Check RMS tolerance
            if (stdDevAngles > 0.1d)
            {
                List<string> buttonTexts = new List<string> { R.T_Reject, R.T_RETRY, R.T_Keep };
                string res = new MessageInput(MessageType.Choice, $"The RMS value {stdDevAngles:0.000} mrad is out of tolerance, what would you like to do with the measure ?")
                {
                    ButtonTexts = buttonTexts
                }.Show().TextOfButtonClicked;
                switch (buttonTexts.IndexOf(res))
                {
                    //Reject
                    case 0: return false;
                    //Retry
                    case 1: return ReadingAngle(out ret);
                    //Keep : continue
                }
            }

            return true;
        }

        public void Refresh()
        {
            throw new NotImplementedException();
        }
    }
}
