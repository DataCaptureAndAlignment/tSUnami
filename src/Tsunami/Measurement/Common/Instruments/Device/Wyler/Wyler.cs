﻿
using System.Xml.Serialization;

namespace TSU.Common.Instruments.Device.WYLER
{
    [XmlType(TypeName = "WYLER.Instrument")]
    public class Instrument : TSU.Common.Instruments.TiltMeter
    {
        public Instrument()
        {

        }
    }
}
