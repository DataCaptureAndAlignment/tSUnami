﻿using System;
using System.Drawing;
using System.Windows.Forms;
using R = TSU.Properties.Resources;


namespace TSU.Common.Instruments.Device.WYLER
{
    public class View : Device.View
    {
        private Button connectButton;
        private Panel panelLog;
        private Button measureButton;
        private Result toAddInLogbook;


        protected new Module Module
        {
            get
            {
                return this._Module as Module;
            }
        }
        protected new Instrument Instrument
        {
            get
            {
                return this.Module.Instrument as Instrument;
            }
        }
        public View()
        {
            InitializeComponent();
        }

        public View(IModule module)
            : base(module)
        {
            Debug.WriteInConsole("ÜSING WYLER VIEW");
            InitializeComponent();
            InitializeLog();
            toAddInLogbook = new Result();
        }

        private void InitializeLog()
        {
            // 
            // log
            // 
            LogView = new Logs.LogView();
            LogView.SetColors();
            LogView.ShowDockedFill();
            panelLog.Controls.Add(LogView);
        }

        private void InitializeComponent()
        {
            this.measureButton = new Button();
            this.connectButton = new Button();
            this.panelLog = new Panel();
            this.SuspendLayout();
            // 
            // PopUpMenu
            // 
            this.PopUpMenu.BackColor = Color.LightBlue;
            this.PopUpMenu.Location = new Point(104, 104);
            // 
            // measureButton
            // 
            this.measureButton.Image = R.At40x_Meas;
            this.measureButton.ImageAlign = ContentAlignment.TopCenter;
            this.measureButton.Location = new Point(49, 50);
            this.measureButton.Name = "measureButton";
            this.measureButton.Size = new Size(124, 70);
            this.measureButton.TabIndex = 2;
            this.measureButton.Text = "Measure";
            this.measureButton.TextAlign = ContentAlignment.BottomCenter;
            this.measureButton.UseVisualStyleBackColor = true;
            this.measureButton.Visible = false;
            this.measureButton.Click += new EventHandler(this.measureButton_Click);
            // 
            // connectButton
            // 
            this.connectButton.Image = R.At40x_PowerOff;
            this.connectButton.ImageAlign = ContentAlignment.TopCenter;
            this.connectButton.Location = new Point(202, 50);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new Size(124, 70);
            this.connectButton.TabIndex = 3;
            this.connectButton.Text = "Connect";
            this.connectButton.TextAlign = ContentAlignment.BottomCenter;
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new EventHandler(this.connectButton_Click);
            // 
            // panelLog
            // 
            this.panelLog.BackColor = Color.FromArgb(48, 48, 48);
            this.panelLog.BackgroundImageLayout = ImageLayout.Zoom;
            this.panelLog.Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.panelLog.ForeColor = Color.DimGray;
            this.panelLog.Location = new Point(358, 8);
            this.panelLog.Name = "panelLog";
            this.panelLog.Size = new Size(771, 142);
            this.panelLog.TabIndex = 4;
            // 
            // View
            // 
            this.AutoScaleDimensions = new SizeF(96F, 96F);
            this.ClientSize = new Size(1476, 158);
            this.Controls.Add(this.panelLog);
            this.Controls.Add(this.connectButton);
            this.Controls.Add(this.measureButton);
            this.Name = "View";
            this.Controls.SetChildIndex(this.buttonForFocusWithTabIndex0, 0);
            this.Controls.SetChildIndex(this.measureButton, 0);
            this.Controls.SetChildIndex(this.connectButton, 0);
            this.Controls.SetChildIndex(this.panelLog, 0);
            this.ResumeLayout(false);

        }

        private void measureButton_Click(object sender, EventArgs e)
        {
            Logs.Log.AddEntryAsFYI(this.Module, "Measuring angle");
            try
            {
                if (!this.Module.ReadingAngle(out DoubleValue dv))
                    return;

                if (this._Module.ParentModule?.ParentModule?.View is Tilt.Station.View tilitStationView)
                {
                    DataGridView dataGridViewMesures = tilitStationView.dataGridViewMesures;

                    int beamsDirectionColumnIndex = dataGridViewMesures.Columns["BeamDirection"].Index;
                    int beamOppositeColumnIndex = dataGridViewMesures.Columns["BeamOpposite"].Index;

                    if (dataGridViewMesures.CurrentCell.ColumnIndex == beamsDirectionColumnIndex
                        || dataGridViewMesures.CurrentCell.ColumnIndex == beamOppositeColumnIndex)
                    {
                        dataGridViewMesures.CurrentCell.Tag = dv;
                        dataGridViewMesures.CurrentCell.Value = dv.Value;
                        Logs.Log.AddEntryAsFYI(this.Module, $"Measured angle: {dv.Value} filled in the form");
                    }
                }
            }
            catch (Exception exception) 
            {
                Logs.Log.AddEntryAsError(this.Module, exception.Message);
            }
            
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            if(!this.Module.wyBusConnected)
            {
                Result connectionResult = this.Module.Connect();
                Logs.Log.AddEntryAsResult(this.Module, connectionResult);
                if (connectionResult.Success)
                {
                    this.measureButton.Visible = true;
                    this.connectButton.Image = R.At40x_PowerOn;
                    this.connectButton.Text = "Disconnect";
                }
            }
            else
            {
                Result disconnectionResult = this.Module.Disconnect();
                Logs.Log.AddEntryAsResult(this.Module, disconnectionResult);
                if (disconnectionResult.Success)
                {
                    this.measureButton.Visible = false;
                    this.connectButton.Image = R.At40x_PowerOff;
                    this.connectButton.Text = "Connect";
                }
            }
        }
    }
}
