﻿using System;
using System.Collections.Generic;

using R = TSU.Properties.Resources;
using System.Xml.Serialization;
using TSU.Common.Compute;

namespace TSU.Common.Instruments
{
    public class TotalStation : Theodolite
    {
        [XmlIgnore] // too much to serialize so lets fill it back when we create the instrumentt module.
        //[XmlElement(ElementName = "TheodoliteEtalonnageParameter")]
        public virtual List<EtalonnageParameter.Theodolite> EtalonnageParameterList { get; set; }

        public TotalStation()
        {
            EtalonnageParameterList = new List<EtalonnageParameter.Theodolite>();
        }

        public void DoDistanceCorrections(Measures.MeasureOfDistance m)
        {
            if (m.Reflector == null || m.Reflector._Name == R.String_Unknown) throw new Exception("No reflector defined");
            Survey.DoDistanceCorrections(m, this);
        }

    }

    
}
