﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TSU.Common.Instruments
{
    public class ConnectionParameters
    {
        [XmlAttribute]
        public string Type { get; set; }
        [XmlAttribute]
        public string SN { get; set; } 
        [XmlAttribute]
        public string SSID { get; set; }
        [XmlAttribute]
        public string IP { get; set; }

        public override string ToString()
        {
            return $"{Type}_{SN} {SSID}:{IP}";
        }
    }
}
