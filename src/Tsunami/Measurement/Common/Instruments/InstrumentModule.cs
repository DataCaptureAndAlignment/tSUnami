﻿using System;
using System.Xml.Serialization;
using TSU.Views.Message;
using M = TSU.Common.Measures;
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments
{
    public abstract class Module : TSU.Manager
    {
        public Instrument Instrument;

        [XmlIgnore]
        public virtual bool InstrumentIsOn { get; set; }
        public virtual bool ConnectionInProgress { get; set; } = false;
        [XmlIgnore]
        public string tempMsg;
        [XmlIgnore]
        public bool tempMsgPriority;

        [XmlIgnore]
        public new Device.View View
        {
            get
            {
                return _TsuView as Device.View;
            }
            set
            {
                _TsuView = value;
            }
        }

        public override string ToString()
        {
            if (Instrument != null)
                return $"IM of {Instrument._Name}";
            else
                return "InstrumentModule Without instrument";
        }

        private InstrumentState state;

        public string LastMessage = "";

        [XmlIgnore]
        internal InstrumentState State
        {
            get
            {
                return state;
            }
            set
            {
                if (value.Value == InstrumentState.type.Measuring && value.Details == "")
                {
                    value.Details = string.Format("Busy measuring {0}", BeingMeasured._Point?._Name ?? R.T_UNKNOWN_POINT);
                }
                state = value;
            }
        }

        [XmlIgnore]
        internal M.Measure toBeMeasuredData;

        [XmlIgnore]
        internal M.Measure beingMeasured;

        /// <summary>
        /// is a clone o the toBeMeasure, taht get modified allow the measurement steps
        /// </summary>
        [XmlIgnore]
        internal virtual M.Measure BeingMeasured
        {
            get
            {
                return beingMeasured;
            }
            set
            {
                beingMeasured = value;
            }
        }

        /// <summary>
        /// clone of being measure taht is not modify during measurement, the one to reuse if the measuremtn fails and need to be remeasured.
        /// </summary>
        [XmlIgnore]
        internal virtual M.Measure BeingMeasuredUnModify { get; set; }

        [XmlIgnore]
        public virtual Manager.Module ParentInstrumentManager
        {
            get
            {
                return ParentModule as Manager.Module;
            }
        }

        [XmlIgnore]
        public virtual M.Measure ToBeMeasuredData
        {
            get
            {
                return toBeMeasuredData;
            }
            set
            {
                toBeMeasuredData = value;
                if (Instrument != null)
                {
                    if (InstrumentIsOn)
                    {
                        Logs.Log.AddEntryAsFYI(this,
                            Instrument._Model + "-  "
                            + Instrument._SerialNumber
                            + " " + R.T331
                            + value._Point._Name);
                    }
                }
            }
        }


        [XmlIgnore]
        public M.Measure JustMeasuredData { get; set; }
        [XmlIgnore]
        public virtual M.WeatherConditions WeatherConditionsStatic { get; set; }
        [XmlIgnore]
        public M.State toBeMeasureState { get; set; }

        #region events


        public delegate bool ConnectionDelegate(string ipAddress, int port);

        public delegate void StatusEventHandler(object sender, StatusEventArgs e);

        [field: XmlIgnore]
        public event M.MeasurementEventHandler MeasurementAvailable;

        public event M.MeasurementEventHandler StartingMeasurement;

        public event M.MeasurementEventHandler FinishingMeasurement;
        public event M.MeasurementEventHandler FinishingAction;
        public event StatusEventHandler StatusChanged;

        public event StatusEventHandler InstrumentConnected;

        public event M.MeasurementEventHandler MeasurementCancelled;
        public event M.MeasurementEventHandler MeasurementFailed;
        public event M.MeasurementEventHandler ToBeMeasureReceived;
        public event M.MeasurementEventHandler WaitingForDetailsOfNextMeasurement;


        public virtual Result Measure()
        {
            try
            {
                // checks
                if (!IsReadyToMeasure()) throw new Exception("Instrument NOT ready");

                PrepareMeasure();
                return null;
            }
            catch (Exception ex)
            {
                State.Value = InstrumentState.type.Idle;
                string message = "Impossible to Measure: " + ex.Message;
                Logs.Log.AddEntryAsError(this, message);
                throw new Exception(message);
            }
        }



        public virtual bool IsReadyToMeasure()
        {
            return false;
        }


        internal virtual void FinishMeasurement()
        {
            if (FinishingMeasurement != null)
                Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(() =>
                {
                    FinishingMeasurement(this, new M.MeasurementEventArgs(this));
                });
        }

        internal virtual void FinishAction()
        {
            if (FinishingAction != null)
                Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(() =>
                {
                    FinishingAction(this, new M.MeasurementEventArgs(this));
                });
        }

        internal virtual void StatusChange(string newStatus, ProgressMessage.StepTypes type)
        {

            if (StatusChanged != null)
                Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(() =>
                {
                    StatusChanged(this, new StatusEventArgs(newStatus, type));
                });
        }

        internal virtual void StartMeasurement()
        {
            if (StartingMeasurement != null)
            {
                Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(() =>
                {
                    StartingMeasurement(this, new M.MeasurementEventArgs(this));
                });
            }
        }

        public virtual void CancelMeasureInProgress()
        {
            if (BeingMeasured != null)
                BeingMeasured._Status = new M.States.Cancel();
            else
            {
                string titleAndMessage = $"{R.T_NO_ACQUISITION_IN_PROGRESS};{R.T_YOU_CAN_ONLY_CANCEL_IT_THE_MEASUREMENT_IS_IN_PROCESS}";
                new MessageInput(MessageType.Warning, titleAndMessage).Show();
            }
        }

        internal virtual void WaitMeasurementDetails()
        {
            if (WaitingForDetailsOfNextMeasurement != null)
                Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(() =>
                {
                    WaitingForDetailsOfNextMeasurement(this, new M.MeasurementEventArgs(this));
                });
        }

        internal virtual void HaveFailed()
        {
            JustMeasuredData = BeingMeasured;
            BeingMeasured = null;

            if (MeasurementFailed != null)
                Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(() =>
                {
                    MeasurementFailed(this, new M.MeasurementEventArgs(this));
                });
        }

        internal virtual void DeclareMeasurementCancelled()
        {

            if (MeasurementCancelled != null)
                Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(() =>
                {
                    MeasurementCancelled(this, new M.MeasurementEventArgs(this));
                });
        }

        internal virtual void OnMeasureToDoReceived()
        {
            if (ToBeMeasureReceived != null)
                Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(() =>
                {
                    ToBeMeasureReceived(this, new M.MeasurementEventArgs(this));
                });

        }
        #endregion

        public virtual void PrepareMeasure()
        {
            if (ToBeMeasuredData == null)
                throw new TsuException(R.T_NOTHING_TO_MEASURE_TOBEMEASUREDDATA_VARIABLE_IS_EMPTY, 00001);

            Polar.Measure clone = (ToBeMeasuredData as Polar.Measure).Clone() as Polar.Measure;
            if (clone._Status is M.States.Good)
            {
                if (clone._Status.PreviousType == M.States.Types.Questionnable)
                    clone._Status = new M.States.Questionnable();
                else
                    clone._Status = new M.States.Unknown();
            }
            if (clone._Status is M.States.Control)
            {

            }

            BeingMeasured = clone;
        }

        public Module()
        {

        }

        /// <summary>
        /// the parentmodule will be subscribedt to the instrumentmodule
        /// </summary>
        /// <param name="parentModule"></param>
        /// <param name="i"></param>
        public Module(TSU.Module parentModule)
            : base(parentModule, R.T404)
        {

        }

        public override void Initialize()
        {
            //_ToBeMeasuredData = new Measure();
            base.Initialize();
        }


        [XmlIgnore]
        public Tools.TimeSpent timeSpentMeasuring = new Tools.TimeSpent();  // list of millis used waiting for instrument answers.


        internal virtual void ValidateMeasurement(Polar.Measure m)
        // must be overrided only by async instrument such as AT40x and manual instruments
        {
            JustMeasuredData = m;
            BeingMeasured = null;

            // If measure is good but that it was precise thata this is questionnable, we put as questionnable, so that the  next module can ask what to do.
            if (m._Status is M.States.Good && toBeMeasureState is M.States.Questionnable) m._Status = new M.States.Questionnable();

            int nAngle = Tsunami2.Preferences.Values.GuiPrefs == null ? 5 : Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;
            int nDist = Tsunami2.Preferences.Values.GuiPrefs == null ? 7 : Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;



            Logs.Log.AddEntryAsFinishOf(this, $"" +
                $"Hz={m.Angles.Corrected.Horizontal.ToString(nAngle)}gon, " +
                $"Vt={m.Angles.Corrected.Vertical.ToString(nAngle)}gon " +
                $"D={m.Distance.Corrected.ToString(nDist)}m, ");

            string measuredtime = $"MEASURED IN {timeSpentMeasuring.DoSynthesis()}";
            //Logs.Log.AddEntryAsFinishOf(this, measuredtime);

            // write time spent
            {
                string path = Tsunami2.Preferences.Values.Paths.MeasureOfTheDay + "Logs\\MeasureTime.txt";
                if (Tsunami2.Preferences.Values.GuiPrefs.LogAT40xMeasurementSpeed.IsTrue)
                    System.IO.File.AppendAllText(path, $"{m._PointName} {measuredtime} ms" + Environment.NewLine);

            }
            SendMessage(JustMeasuredData);
            _TsuView.IsWaiting = false;

            StatusChange(string.Format("Measured"), ProgressMessage.StepTypes.EndAll);

            TSU.Debug.WriteInConsole("TSU.Measure " + string.Format("... '{0}' just measured '{1}'", _Name, JustMeasuredData._Name));
            State = new InstrumentState(InstrumentState.type.Idle);
        }

        internal virtual void ReportMeasurement()
        {
            if (MeasurementAvailable != null)
                Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(() =>
                {
                    MeasurementAvailable(this, new M.MeasurementEventArgs() { JustMeasured = JustMeasuredData, InstrumentModule = this });
                });
        }

        public virtual Result InitializeSensor() { return null; }

        internal void DeclareInstrumentConnected()
        {
            InstrumentConnected?.Invoke(this, new StatusEventArgs("Connected", ProgressMessage.StepTypes.End));
        }

        public virtual Result Connect()
        {

            return null;
        }
        public virtual Result Disconnect() { return null; }

        public override void OnNext(TsuObject tsuObject)
        {
            dynamic dynamic = tsuObject;
            OnNextBasedOn(dynamic);
        }

        private void OnNextBasedOn(M.Measure m)
        {
            // Forwarding to observers
            SendMessage(m);

            string message = _Name + ": " + R.T332 + m._Name + " " + m.GetType().ToString();

        }

        internal void CleanSubscriber()
        {
            MeasurementAvailable = null;
        }


    }
}

namespace TSU.Common.Instruments
{
    public class InstrumentState
    {
        public enum type
        {
            Idle, Disconnected, Connecting, Measuring, Moving
        }

        public type Value { get; set; }
        public string Details { get; set; }

        public InstrumentState(type value, string details = "")
        {
            Value = value;
            if (details == "")
                switch (value)
                {
                    case type.Idle:
                        Details = "Instrument is Idling";
                        break;
                    case type.Disconnected:
                        Details = "Instrument is disconnected";
                        break;
                    case type.Connecting:
                        Details = "Instrument is in the process of connection";
                        break;
                    case type.Measuring:
                        Details = "";
                        break;
                    case type.Moving:
                        Details = "Instrument is moving";
                        break;
                    default:
                        Details = R.T_NOT_AVAILABLE;
                        break;
                }
        }

    }

    #region events args



    public class StatusEventArgs : EventArgs
    {
        public string StatusAsText;
        public ProgressMessage.StepTypes StepActiontype;
        public StatusEventArgs(string status, ProgressMessage.StepTypes stepActiontype)
        {
            StatusAsText = status;
            StepActiontype = stepActiontype;
        }
    }
    #endregion

    public class CancelException : Exception
    {
        public CancelException()
        {

        }
        public CancelException(string message) : base(message)
        {

        }
        public CancelException(string message, Exception e) : base(message, e)
        {

        }
    }
}
