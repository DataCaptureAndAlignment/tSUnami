using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using TSU.ENUM;

using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments
{
    public interface IReflector : IInstrument
    {
        DateTime CalibrationDate { get; set; }

        string ReferenceReflectorSN { get; set; }
        double Constante { get; set; }
        double Diameter { get; set; }
    }
}

namespace TSU.Common.Instruments.Reflector
{
    [Serializable]
    public class Reflector : Instrument, IReflector
    {
        public static Reflector GetbyName(string name)
        {
            return TSU.Tsunami2.Preferences.Values.Reflectors.Find(x => x._Name == name);
        }

        [XmlAttribute]
        public DateTime CalibrationDate { get; set; }
        [XmlAttribute]
        public string ReferenceReflectorSN { get; set; }
        [XmlAttribute]
        public double Constante { get; set; }
        [XmlAttribute]
        public double Diameter { get; set; }
        // Properties ti fil LGC2 input
        [XmlAttribute]
        public bool distCorrectionKnown { get; set; }
        [XmlAttribute]
        public double distCorrectionValue { get; set; }
        [XmlAttribute]
        public double sigmaDCorr { get; set; }
        [XmlAttribute]
        public double sigmaTargetCentering { get; set; }


        // Methods
        public Reflector()
        {
            Diameter = 0;
            Constante = 0;
            distCorrectionKnown = true;
            distCorrectionValue = 0;
            sigmaDCorr = 0;
            sigmaTargetCentering = 0;
            _SerialNumber = R.String_Unknown;
            _Model = R.String_Unknown;
            _Brand = "";
            Id = R.String_Unknown;
            ReferenceReflectorSN = R.String_Unknown;
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override object Clone()
        {
            return MemberwiseClone(); // make a clone of the object, work sif there is no other tye than primitive tyes.
        }

        public bool Equals(Reflector other)
        {
            return
                _SerialNumber == other._SerialNumber &&
                _Model == other._Model &&
                _InstrumentType == other._InstrumentType;
        }
        public override bool Equals(object other)
        {
            if (other is Reflector)
                return Equals((Reflector)other);
            else
                return false;
        }
        //public override int GetHashCode()
        //{
        //    return base.GetHashCode() ^ _InstrumentType.GetHashCode(); ;
        //}
        public Reflector DeepCopy()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Reflector reflectorCopy = new Reflector();
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                reflectorCopy = (Reflector)formatter.Deserialize(stream);
                return reflectorCopy;
            }
        }
    }
}