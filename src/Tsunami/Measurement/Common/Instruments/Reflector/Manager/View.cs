﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using TSU.Views;
using V = TSU.Common.Strategies.Views;
using TSU.Common.Instruments.Manager;
using TSU.Views.Message;


namespace TSU.Common.Instruments.Reflector.Manager
{
    public class View : Instruments.Manager.View
    {

        BigButton ShowCalibratedReflector;

        public View(IModule module)
            : base(module)
        {

            AdaptTopPanelToButtonHeight();
            ShowCalibratedReflector = new BigButton($"{R.T_SHOW_CALIBRATED};{R.T_ONLY_THE_REFLECTOR_THAT_HAVE_A_CALIBRATION_WITH_THE_USED_INSTRUMENT}", R.Reflector, ShowCalibratedReflectors);

        }

        public override List<System.Windows.Forms.Control> GetViewOptionButtons()
        {
            List<System.Windows.Forms.Control> l = base.GetViewOptionButtons();
            l.Add(ShowCalibratedReflector);
            return l;
        }
        internal override void InitializeStrategy()
        {
            AvailableViewStrategies = new List<V.Types>() { V.Types.Tree, V.Types.List };
            SetStrategy(TSU.Tsunami2.Preferences.Values.GuiPrefs.ReflectorViewType);
        }
        internal override void SavePreference(V.Types viewStrategyType)
        {
           TSU.Tsunami2.Preferences.Values.GuiPrefs.ReflectorViewType = viewStrategyType;

            TSU.Tsunami2.Preferences.Values.SaveGuiPrefs();
        }

        private void ShowCalibratedReflectors()
        {
            if (Module.ParentModule is Polar.Station.Module)
            {
                Polar.Station.Module stm = Module.ParentStationModule as Polar.Station.Module;
                if (stm.stationParameters._Instrument is TotalStation)
                {
                    TotalStation ts = stm.stationParameters._Instrument as TotalStation;
                    //TsunamiPreferences.Values
                    Module.SelectableObjects = Module.AllElements.FindAll(x => ts.EtalonnageParameterList.Find(y => y.PrismeRef._Name == x._Name) != null);
                    if (Module.SelectableObjects.Count == 0)
                    {
                        string titleAndMessage = $"{R.T_NO_CALIBRATION_FOUND};{R.T_TSUNAMI_FOUND_NO_CALIBRATION_FOR_THIS_INSTRUMENT} ({ts._Name})";
                        new MessageInput(MessageType.Critical, titleAndMessage).Show();
                    }

                    UpdateView();
                }

                new MessageInput(MessageType.FYI, "For 'total station' only").Show();
            }
        }
    }
}
