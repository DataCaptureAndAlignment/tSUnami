using System;
using System.Collections.Generic;
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments.Reflector
{
    [Serializable]
    public class Reflectors
    {
        public List<Reflector> ReflectorList { get; set; }

        public Reflectors(TotalStation ts)
        {
            ReflectorList = new List<Reflector>();

            if (ts != null)
            {
                foreach (var item in ts.EtalonnageParameterList)
                {
                    ReflectorList.Add(item.PrismeRef);
                }
            }
        }

        public Reflector GetReflectorByTypeNamePlusSerialNumber(string TypeNamePlusSerialNumber)
        {
            foreach (Reflector item in ReflectorList)
            {
                if (item._Name == TypeNamePlusSerialNumber)
                {
                    return item;
                }
            }
            throw new Exception(R.T334 + TypeNamePlusSerialNumber + R.T335);
        }
    }
}
