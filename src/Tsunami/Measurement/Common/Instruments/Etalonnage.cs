﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments
{
    [Serializable]
    public abstract class EtalonnageParameter
    {
        public string instrumentSerialNumber;
        public string instrumentModel;           //T2, T3000, ou TC2002, TC1800, TCA2003, TCRP1201  ou TCRA1101
        public string calibrationDate;
        public double fourrierWaveLength;
        public double fourrierScale;
        public double fourrierA0Constant;
        public double fourrierMaximumDistance;
        public double fourrierMinimumDistance;
        public double[] fourrierCoeff_A;               //coefficients Fourier
        public double[] fourrierCoeff_B;               //coefficients Fourier
        
        public EtalonnageParameter()
        {
            fourrierCoeff_A = new double[50];
            fourrierCoeff_B = new double[50];
        }
        public void FillFourrierCoefficients(List<string> LinesBlock)
        {
            int count = 0;
            foreach (string line2 in LinesBlock)
            {
                string line = Regex.Replace(line2, @"(\s)\s+", "$1"); //remove multi tab or space
                List<string>  lineFields = new List<string>(line.Split(' '));
                fourrierCoeff_A[count] = Convert.ToDouble(lineFields[0], System.Globalization.CultureInfo.InvariantCulture);
                fourrierCoeff_B[count] = Convert.ToDouble(lineFields[1], System.Globalization.CultureInfo.InvariantCulture);
                count++; //added the 01-08-17
            }
        }

        [Serializable]
        [XmlType("EtalonnageParameterTheodolite")] // because it is the same name as insturment theoolite
        public class Theodolite : EtalonnageParameter
        {
            public Theodolite()
                : base()
            {
                PrismeRef = new Reflector.Reflector();
                Commentaire_C = new string[3];
                C_H = new double[3];
                C_V = new double[3];
                C_connun = new bool[3];
                D_Collim = new string[3];
            }
            public string SupportNum;       //numero du support en string
            public string Edm;
            public string num;             //numero du distance-metre en string
            public double Ecart_axe_edm;   //distance entre axes optique et distance-metre
            public double Ecart_axe_chaine;  //distance entre axes optique et point de chainage
            public Reflector.Reflector PrismeRef;        //"PREF1" (concatenation de "PREF" et "1")
            public int Indice_corr_dossier_used;
            public int Indice_corr_dossier_used_ATR;
            public bool Coll_ATR;
            public double Corr_H_ATR;
            public double Corr_V_ATR;
            public string Date_Coll_ATR;
            public string[] Commentaire_C;     //corrections de collim. ver. telle que stockées dans le dossier
            public double[] C_H;               //corrections de collim. hor. telle que stockées dans le dossier
            public double[] C_V;               //corrections de collim. ver. telle que stockées dans le dossier
            public bool[] C_connun;            //corrections de collim. ver. telle que stockées dans le dossier
            public string[] D_Collim;          //date 05/04/1999 mm/jj/yy
            public string str_DI2000_ID;        //ID sur numéro

            public static EtalonnageParameter.Theodolite LoadEtalonnageParameterFromLinesBlock(List<string> LinesBlock, List<Instrument> knownInstrumentList)
            {
                EtalonnageParameter.Theodolite etalonnageParameter = new EtalonnageParameter.Theodolite();
                etalonnageParameter.SetTheodoliteParametersFromBlockLine(LinesBlock, knownInstrumentList);
                return etalonnageParameter;
            }

            public override string ToString()
            {
                return $"Etalonnage Parameter for {PrismeRef} dating from {this.calibrationDate}";
            }

            public void SetTheodoliteParametersFromBlockLine(List<string> LinesBlock, List<Instrument> knownInstrumentList)
            {
                //take first line of the Block, cut it anf fill the parameters
                string line = Regex.Replace(LinesBlock[0].ToString(), @"(\s)\s+", "$1"); //remove multi tab or space
                List<string> lineFields = new List<string>(line.Split(' '));
                this.calibrationDate = lineFields[3];

                string name = lineFields[4] + "_" + lineFields[5];
                //this.PrismeRef = (Reflector)TSUPreferences.Preferences.GeodeInstruments.Find(i => i._Id == name);

                this.PrismeRef = (Reflector.Reflector)knownInstrumentList.Find(i => i.Id == name);
                if (this.PrismeRef == null)
                    //   throw new Exception($"Prism '{name}' from calibration doesnt exist in the instrument list");
                    throw new NoCalibrationException($"Prism '{name}' {R.T_FROM_CALIBRATION_DOESNT_EXIST_IN_THE_INSTRUMENT_LIST}");
                //ENUM.ReflectorTypeName reflectorType;
                //ENUM.ReflectorTypeName.TryParse(s, out reflectorType);
                //this.PrismeRef._ReflectorType = reflectorType;
                double waveLength = Convert.ToDouble(lineFields[6], System.Globalization.CultureInfo.InvariantCulture);

                //this.fourrierWaveLength = waveLength != 0 ?waveLength: 3;// see TSU - 626 edm2003.dat carnet algo bug if longueur d'onde a 0, https://its.cern.ch/jira/browse/TSU-626
                
                this.fourrierScale = Convert.ToDouble(lineFields[7], System.Globalization.CultureInfo.InvariantCulture);
                this.fourrierA0Constant = Convert.ToDouble(lineFields[8], System.Globalization.CultureInfo.InvariantCulture);
                this.fourrierMaximumDistance = Convert.ToDouble(lineFields[10], System.Globalization.CultureInfo.InvariantCulture);
                this.fourrierMinimumDistance = Convert.ToDouble(lineFields[9], System.Globalization.CultureInfo.InvariantCulture);
                LinesBlock.RemoveAt(0);

                FillFourrierCoefficients(LinesBlock);
            }
        }

        public class NoCalibrationException : Exception
        {
            public NoCalibrationException (string message):base(message) { }
        }

        [Serializable]
        [XmlType("EtalonnageParameterOffsetMeter")] // because it is the same name as insturment theoolite
        public class OffsetMeter : EtalonnageParameter
        {
            public OffsetMeter()
                : base()
            {
                
            }
            public static EtalonnageParameter.OffsetMeter LoadEtalonnageParameterFromLinesBlock(List<string> LinesBlock)
            {
                EtalonnageParameter.OffsetMeter etalonnageParameter = new EtalonnageParameter.OffsetMeter();
                etalonnageParameter.SetOffsetMeterParametersFromBlockLine(LinesBlock);
                return etalonnageParameter;
            }

            public void SetOffsetMeterParametersFromBlockLine(List<string> LinesBlock)
            {
                //take first line of the Block, cut it anf fill the parameters
                string line = Regex.Replace(LinesBlock[0].ToString(), @"(\s)\s+", "$1"); //remove multi tab or space
                List<string> lineFields = new List<string>(line.Split(' '));
                this.calibrationDate = lineFields[3];
                double waveLength = Convert.ToDouble(lineFields[4], System.Globalization.CultureInfo.InvariantCulture);
                this.fourrierWaveLength = waveLength != 0 ? waveLength : 3;// see TSU - 626 edm2003.dat carnet algo bug if longueur d'onde a 0, https://its.cern.ch/jira/browse/TSU-626
                this.fourrierScale = Convert.ToDouble(lineFields[5], System.Globalization.CultureInfo.InvariantCulture);
                this.fourrierA0Constant = Convert.ToDouble(lineFields[6], System.Globalization.CultureInfo.InvariantCulture);
                this.fourrierMaximumDistance = Convert.ToDouble(lineFields[8], System.Globalization.CultureInfo.InvariantCulture);
                this.fourrierMinimumDistance = Convert.ToDouble(lineFields[7], System.Globalization.CultureInfo.InvariantCulture);
                LinesBlock.RemoveAt(0);

                //Fill Fourrier coefficients
                int count = 0;
                foreach (string line2 in LinesBlock)
                {
                    line = Regex.Replace(line2, @"(\s)\s+", "$1"); //remove multi tab or space
                    lineFields = new List<string>(line.Split(' '));
                    fourrierCoeff_A[count] = Convert.ToDouble(lineFields[0], System.Globalization.CultureInfo.InvariantCulture);
                    fourrierCoeff_B[count] = Convert.ToDouble(lineFields[1], System.Globalization.CultureInfo.InvariantCulture);
                    count++;
                }
            }
        }
        [Serializable]
        [XmlType("EtalonnageParameterLevel")] 
        public class Level : EtalonnageParameter
        {
            public Level()
                : base()
            {

            }
            public static EtalonnageParameter.Level LoadEtalonnageParameterFromLinesBlock(List<string> LinesBlock)
            {
                /// To implement when etalonnage parameter is available in etalo.dat file 
                EtalonnageParameter.Level etalonnageParameter = null;
                //EtalonnageParameter.Level etalonnageParameter = new EtalonnageParameter.Level();
                //etalonnageParameter.SetLevelParametersFromBlockLine(LinesBlock);
                return etalonnageParameter;
            }

            public void SetLevelParametersFromBlockLine(List<string> LinesBlock)
            {
            /// To implement when etalonnage parameter is available in etalo.dat file 
                //    //take first line of the Block, cut it anf fill the parameters
            //    string line = Regex.Replace(LinesBlock[0].ToString(), @"(\s)\s+", "$1"); //remove multi tab or space
            //    List<string> lineFields = new List<string>(line.Split(' '));
            //    this.calibrationDate = lineFields[3];
            //    this.fourrierWaveLength = Convert.ToDouble(lineFields[4], System.Globalization.CultureInfo.InvariantCulture);
            //    this.fourrierScale = Convert.ToDouble(lineFields[5], System.Globalization.CultureInfo.InvariantCulture);
            //    this.fourrierA0Constant = Convert.ToDouble(lineFields[6], System.Globalization.CultureInfo.InvariantCulture);
            //    this.fourrierMaximumDistance = Convert.ToDouble(lineFields[8], System.Globalization.CultureInfo.InvariantCulture);
            //    this.fourrierMinimumDistance = Convert.ToDouble(lineFields[7], System.Globalization.CultureInfo.InvariantCulture);
            //    LinesBlock.RemoveAt(0);

            //    //Fill Fourrier coefficients
            //    int count = 0;
            //    foreach (string line2 in LinesBlock)
            //    {
            //        line = Regex.Replace(line2, @"(\s)\s+", "$1"); //remove multi tab or space
            //        lineFields = new List<string>(line.Split(' '));
            //        fourrierCoeff_A[count] = Convert.ToDouble(lineFields[0], System.Globalization.CultureInfo.InvariantCulture);
            //        fourrierCoeff_B[count] = Convert.ToDouble(lineFields[1], System.Globalization.CultureInfo.InvariantCulture);
            //        count++;
            //    }
            }
        }



        //public bool GetEtalonnageParameterFromLine(string line)
        //{
        //    bool state = false;
        //    if (line[0] != '%')
        //    {
        //        List<string> lineFields = new List<string>(line.Split(';'));
        //        if (lineFields[0] == "*PR")
        //        {

        //            state = true;
        //        }
        //        else if (lineFields[0] == "*EDMTCA")
        //        {
        //            this.DateEtal = lineFields[1].Trim();
        //            this.support = lineFields[2].Trim(); //temp=support
        //            this.SupportNum = lineFields[3].Trim();
        //            this.Edm = "CHAINE";
        //            this.Edm = lineFields[4].Trim();
        //            this.num = "-9999";
        //            this.num = lineFields[5].Trim();

        //            switch (this.Edm)
        //            {
        //                case "DI2000":
        //                    this.Ecart_axe_edm = 0.06;
        //                    this.Ecart_axe_chaine = 0.1094;
        //                    break;
        //                case "TC2002":
        //                    this.Ecart_axe_edm = 0;
        //                    this.Ecart_axe_chaine = 0.083;
        //                    break;
        //                case "TCA2003":
        //                    this.Ecart_axe_edm = 0;
        //                    this.Ecart_axe_chaine = 0.0915;
        //                    break;
        //                case "TDA5005":
        //                    this.Ecart_axe_edm = 0;
        //                    this.Ecart_axe_chaine = 0.0915;
        //                    break;
        //                default:
        //                    this.Ecart_axe_edm = 0;
        //                    this.Ecart_axe_chaine = 0.033;
        //                    break;
        //            }

        //            this.PrismeRef = lineFields[6].Trim();  //nom du prisme de reférence
        //            this.PrismeRef = this.PrismeRef + lineFields[7].Trim();  //numéro du prisme de reférence
        //            this.long_onde = Convert.ToDouble(lineFields[8].Trim());
        //            this.Echelle = Convert.ToDouble(lineFields[9].Trim());
        //            this.Cste_Prisme_A0 = Convert.ToDouble(lineFields[10].Trim());

        //            //count coefficients 2*10 = 20 or 2*50 = 100
        //            //both versions can be read!
        //            int num_coeff;
        //            num_coeff = (lineFields.Count - 11);
        //            if (num_coeff % 2 != 0)
        //            {
        //                MessageTsu.ShowCritical(
        //                    "Oops",
        //                    Erreur dans la fonction 'VersionCarn_Load_From_EDMTCA2003File'. Le nombre des coefficients dans le fichier 'EDMTCA2003.dat' est fausse! Il y a "
        //                    + num_coeff + " coefficients su la ligne!",
        //                    R.T_OK);
        //            }
        //            else
        //            {
        //                for (int i = 0; i < (num_coeff / 2); i++)
        //                {
        //                    this.Coeff_A[i] = Convert.ToDouble(lineFields[11 + i].Trim());
        //                }
        //                for (int i = 0; i < (num_coeff / 2); i++)
        //                {
        //                    this.Coeff_B[i] = Convert.ToDouble(lineFields[11 + num_coeff / 2 + i].Trim());
        //                }
        //            }
        //        }
        //        else
        //        {
        //            state = false;
        //        }
        //    }
        //    return state;
        //}
    }
}
