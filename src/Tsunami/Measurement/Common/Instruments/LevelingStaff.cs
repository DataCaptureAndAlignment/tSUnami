﻿using System;
using System.Xml;
using System.Xml.Serialization;
using TSU.ENUM;
using System.IO;
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments
{
    public interface ILevelingStaff : IInstrument
    {
        StaffType _StaffType { get; set; }
        DateTime CalibrationDate { get; set; }
        double _VerticalDeviation { get; set; } //Standard deviation of a measured vertical distance [mm]
        double _VerticalPPM { get; set; } //PPM error value for a measured vertical distance [mm]
        double _VerticalDistanceCorrection { get; set; } //Distance correction value for the measurements [m]
        double _VerticalDistanceCorrectionDeviation { get; set; } //Standard deviation of the distance correction value [mm]
        double _VerticalOffset { get; set; } //Vertical offset of the staff [m]
        double _VerticalOffsetDeviation { get; set; } //Standard deviation of the vertical offset of the staff [mm]
        double _SpatialDistanceDeviation { get; set; } //flt: Standard deviation of a measured spatial distance [mm]
        double _SpatialDistancePPM { get; set; } //flt: PPM error value for a measured spatial distance [mm]
        int _IsSpatialDistanceCorrectionUnknown { get; set; } //bol: Identifies if the distance correction value is an "unknown value" to be determined in the adjustment
        double _SpatialDistanceCorrection { get; set; } //flt: Distance correction value for the measurements [m]
        double _SpatialDistanceCorrectionDeviation { get; set; } //flt: Standard deviation of the distance correction value [mm]
        double _SpatialDistanceTargetCenteringDeviation { get; set; } //flt: Standard deviation of the target centering [mm]
        double _SpatialDistanceTargetHeight { get; set; } //flt: Target height [m]
        double _SpatialDistanceTargetHeightdeviation { get; set; } //flt: Standard deviation of the target height [mm]
        double _zeroCorrection { get; set; } //correction for raw reading after zero check
        double _zeroCorrectionReading { get; set; } //reading done during the zero check test
        // Methods
    }

    [XmlType(Namespace = "Instrument")]
    [Serializable]
    public class LevelingStaff : Instrument, ILevelingStaff
    {
        public StaffType _StaffType { get; set; }
        [XmlAttribute]
        public DateTime CalibrationDate { get; set; }
        [XmlAttribute]
        public double _VerticalDeviation { get; set; } //Standard deviation of a measured vertical distance [mm]
        [XmlAttribute]
        public double _VerticalPPM { get; set; } //PPM error value for a measured vertical distance [mm]
        [XmlAttribute]
        public double _VerticalDistanceCorrection { get; set; } //Distance correction value for the measurements [m]
        [XmlAttribute]
        public double _VerticalDistanceCorrectionDeviation { get; set; } //Standard deviation of the distance correction value [mm]
        [XmlAttribute]
        public double _VerticalOffset { get; set; } //Vertical offset of the staff [m]
        [XmlAttribute]
        public double _VerticalOffsetDeviation { get; set; } //Standard deviation of the vertical offset of the staff [mm]
        [XmlAttribute]
        public double _SpatialDistanceDeviation { get; set; } //flt: Standard deviation of a measured spatial distance [mm]
        [XmlAttribute]
        public double _SpatialDistancePPM { get; set; } //flt: PPM error value for a measured spatial distance [mm]
        [XmlAttribute]
        public int _IsSpatialDistanceCorrectionUnknown { get; set; } //bol: Identifies if the distance correction value is an "unknown value" to be determined in the adjustment
        [XmlAttribute]
        public double _SpatialDistanceCorrection { get; set; } //flt: Distance correction value for the measurements [m]
        [XmlAttribute]
        public double _SpatialDistanceCorrectionDeviation { get; set; } //flt: Standard deviation of the distance correction value [mm]
        [XmlAttribute]
        public double _SpatialDistanceTargetCenteringDeviation { get; set; } //flt: Standard deviation of the target centering [mm]
        [XmlAttribute]
        public double _SpatialDistanceTargetHeight { get; set; } //flt: Target height [m]
        [XmlAttribute]
        public double _SpatialDistanceTargetHeightdeviation { get; set; } //flt: Standard deviation of the target height [mm]
        [XmlAttribute]
        public double _zeroCorrection { get; set; } //correction for raw reading after zero check
        [XmlAttribute]
        public double _zeroCorrectionReading { get; set; } //reading done during the zero check test
        //Method

        public LevelingStaff()
        {

        }

        public LevelingStaff(StaffType staffType= StaffType.Unknown)
            //Constructeur leveling staff en fonction type de mire
        {
            switch (staffType)
            {
                case StaffType.GCB70:
                    //mire code barre -> mesure distance au niveau
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "SU";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 500; //5mm par 10m pour le DNA 03 => 500mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.G10:
                    //mire manuelle -> mesure distance au pas précision 50cm
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "SU";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 50000; //500mm par 10m au pas => 50000mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0.5;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.G16:
                    //mire manuelle -> mesure distance au pas précision 50cm
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "SU";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 50000; //500mm par 10m au pas => 50000mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0.5;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.G182:
                    //mire manuelle -> mesure distance au pas précision 50cm
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "SU";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 50000; //500mm par 10m au pas => 50000mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0.5;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.G20:
                    //mire manuelle -> mesure distance au pas précision 50cm
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "SU";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 50000; //500mm par 10m au pas => 50000mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0.5;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.G26:
                    //mire manuelle -> mesure distance au pas précision 50cm
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "SU";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 50000; //500mm par 10m au pas => 50000mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0.5;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.G260:
                    //mire manuelle -> mesure distance au pas précision 50cm
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "SU";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 50000; //500mm par 10m au pas => 50000mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0.5;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.G30:
                    //mire manuelle -> mesure distance au pas précision 50cm
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "SU";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 50000; //500mm par 10m au pas => 50000mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0.5;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.G40:
                    //mire manuelle -> mesure distance au pas précision 50cm
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "SU";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 50000; //500mm par 10m au pas => 50000mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0.5;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.G50:
                    //mire manuelle -> mesure distance au pas précision 50cm
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "SU";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 50000; //500mm par 10m au pas => 50000mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0.5;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.G56:
                    //mire manuelle -> mesure distance au pas précision 50cm
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "SU";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 50000; //500mm par 10m au pas => 50000mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0.5;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.G60:
                    //mire manuelle -> mesure distance au pas précision 50cm
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "SU";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 50000; //500mm par 10m au pas => 50000mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0.5;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.G80:
                    //mire manuelle -> mesure distance au pas précision 50cm
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "SU";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 50000; //500mm par 10m au pas => 50000mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0.5;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.LCBL110:
                    //mire code barre -> mesure distance au niveau
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "Nedo_ou_SU";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 500; //5mm par 10m pour le DNA 03 => 500mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.LCBL190:
                    //mire code barre -> mesure distance au niveau
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "Leica";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 500; //5mm par 10m pour le DNA 03 => 500mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.LCB120:
                    //mire code barre -> mesure distance au niveau
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "Nedo";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 500; //5mm par 10m pour le DNA 03 => 500mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.LCB190:
                    //mire code barre -> mesure distance au niveau
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "Leica";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 500; //5mm par 10m pour le DNA 03 => 500mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.LCB90:
                    //mire code barre -> mesure distance au niveau
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand =R.String_Unknown;
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 500; //5mm par 10m pour le DNA 03 => 500mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.M29:
                    //mire manuelle -> mesure distance au pas précision 50cm
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "SU";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 50000; //500mm par 10m au pas => 50000mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0.5;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.REGLET:
                    //mire manuelle -> mesure distance au pas précision 50cm
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand =R.String_Unknown;
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 50000; //500mm par 10m au pas => 50000mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0.5;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.TAYLOR:
                    //mire manuelle -> mesure distance au pas précision 50cm
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "Taylor-Hobbson";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 50000; //500mm par 10m au pas => 50000mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0.5;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.W180:
                    //mire manuelle -> mesure distance au pas précision 50cm
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "Wild";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 50000; //500mm par 10m au pas => 50000mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0.5;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.W90:
                    //mire manuelle -> mesure distance au pas précision 50cm
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "Wild";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 50000; //500mm par 10m au pas => 50000mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0.5;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.ZCB120:
                    //mire code barre -> mesure distance au niveau
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "Zeiss_Trimble";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 500; //5mm par 10m pour le DNA 03 => 500mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.ZCB190:
                    //mire code barre -> mesure distance au niveau
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "Zeiss";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 500; //5mm par 10m pour le DNA 03 => 500mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.ZCB90:
                    //mire code barre -> mesure distance au niveau
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand = "Zeiss";
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 500; //5mm par 10m pour le DNA 03 => 500mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                case StaffType.Unknown:
                    //mire manuelle -> mesure distance au pas précision 50cm
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand =R.String_Unknown;
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 50000; //500mm par 10m au pas => 50000mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0.5;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
                default:
                    //mire manuelle -> mesure distance au pas précision 50cm
                    this._StaffType = staffType;
                    this.CalibrationDate = System.DateTime.Now;
                    this._Brand =R.String_Unknown;
                    this._Model =R.String_Unknown;
                    this._SerialNumber =R.String_Unknown;
                    this._VerticalDeviation = 0.1;
                    this._VerticalPPM = 0;
                    this._VerticalDistanceCorrection = 0;
                    this._VerticalDistanceCorrectionDeviation = 0;
                    this._VerticalOffset = 0;
                    this._VerticalOffsetDeviation = 0;
                    this._SpatialDistanceCorrection = 0;
                    this._SpatialDistanceCorrectionDeviation = 0;
                    this._SpatialDistancePPM = 50000; //500mm par 10m au pas => 50000mm/km
                    this._SpatialDistanceTargetCenteringDeviation = 0;
                    this._SpatialDistanceTargetHeight = 0;
                    this._SpatialDistanceTargetHeightdeviation = 0;
                    this._SpatialDistanceDeviation = 0.5;
                    this._IsSpatialDistanceCorrectionUnknown = 1;
                    this._zeroCorrection = na;
                    this._zeroCorrectionReading = na;
                    break;
            } 
        }
        public LevelingStaff DeepCopy()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                LevelingStaff staffCopy = new LevelingStaff();
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                staffCopy = (LevelingStaff)formatter.Deserialize(stream);
                return staffCopy;
            }
        }
    }
}
