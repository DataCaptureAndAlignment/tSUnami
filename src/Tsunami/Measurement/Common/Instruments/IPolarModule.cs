﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using TSU.ENUM;
using TSU.Common.Measures;

namespace TSU.Common.Instruments
{
    /// <summary>
    /// Interface to implement for a PolarModule
    /// </summary>
    public interface IPolarModule
    {
        /// <summary>
        /// This is exectuted just before the acquisition start
        /// 
        /// </summary>
        void PrepareMeasure();

        /// <summary>
        /// Start the acquisition in the actual face position (doesnt matter if it is Fceor face2.
        /// </summary>
        void MeasureLikeThis();

        /// <summary>
        /// this return of the instrument is ready for acquisition
        /// </summary>
        /// <returns></returns>
        bool IsReadyToMeasure();

        /// <summary>
        /// this should change the face to the opposite one
        /// </summary>
        void ChangeFace();

        /// <summary>
        /// this is executed when teh tobemeasureData is filled,
        /// it is time there to check if the received measure contain all the instrument need
        /// and for exemeple to check if the goto is possible 
        /// </summary>
        /// <param name="m"></param>
        void CheckIncomingMeasure(Polar.Measure m);

        /// <summary>
        /// turn off the compensator, if one
        /// </summary>
        void TurnOffCompensator();

        /// <summary>
        /// turn On the compensator, if one
        /// </summary>
        void TurnOnCompensator();

        /// <summary>
        /// return the type of the actual face 
        /// </summary>
        /// <returns></returns>
        FaceType GetFace();

        /// <summary>
        /// start a goto to the angles stored in the tobemeasure data
        /// </summary>
        void Goto();
    }
}
