using System;
using System.Collections.Generic;
using System.Text;using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments
{
	public class ReflectorType
	{
        public string Name { get; set; }
        public double? Constante { get; set; }
        public double? Diameter { get; set; }

        public ReflectorType(string name)
        {
            Name = name;
            Constante = 0;
            switch (name)
            {
                case "PBMG26": Diameter = 0.026;
                    break;
                default:
                    break;
            }
        }
	}
}
