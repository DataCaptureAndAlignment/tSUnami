using System;
using System.Collections.Generic;
using System.Text;using R = TSU.Properties.Resources;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace TSU.Common.Instruments

{
    [Serializable]
	public class Compensator
	{
        [XmlAttribute("L")]
        public double longitudinalAxisInclination { get; set; }
        [XmlAttribute("T")]
        public double tranverseAxisInclination { get; set; }
        [XmlAttribute("A")]
        public double inclinationAccuracy { get; set; }
        [XmlAttribute("D")]
        public System.DateTime inclineTime { get; set; }

        public Compensator() //Constructeur
        {
            this.inclinationAccuracy = -999;
            this.inclineTime = System.DateTime.Now;
            this.longitudinalAxisInclination = -999;
            this.tranverseAxisInclination = -999;
        }

        public Compensator Clone()
        {
            Compensator newCompensator = (Compensator)this.MemberwiseClone(); // to make a clone of the primitive types fields
            return newCompensator;
        }


        internal XmlElement WriteForTsunami(XmlDocument xmlDocument, string p)
        {
            XmlElement measure = xmlDocument.CreateElement("Compensator");
            measure.SetAttribute("Longitudinal_Axis", longitudinalAxisInclination.ToString());
            measure.SetAttribute("Tranverse_Axis", tranverseAxisInclination.ToString());
            measure.SetAttribute("Accuracy", inclinationAccuracy.ToString());
            measure.SetAttribute("Time", inclineTime.ToString());
            return measure;
        }

        public override string ToString()
        {
            return $"l: {(longitudinalAxisInclination / Math.PI * 200):0.0000} g\r\nt: {(tranverseAxisInclination / Math.PI * 200):0.0000} g\r\n";
        }
    }
}
