using System;
using System.Collections.Generic;
using System.Text;using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments
{
	[Serializable]
    public class OffsetMeter : Sensor
	{
        public virtual EtalonnageParameter.OffsetMeter EtalonnageParameter { get; set; }
	}
}
