﻿
using R = TSU.Properties.Resources;
using System.Xml.Serialization;
using System;

namespace TSU.Common.Instruments
{
    public interface IPolarInstrument : IInstrument
    {
        double sigmaInstrCentering { get; set; }
    }


    [Serializable]
    public class SigmaForAInstrumentCouple
    {
        [XmlAttribute]
        public string Instrument { get; set; }
        [XmlAttribute]
        public string Target { get; set; }
        [XmlAttribute]
        public string Condition { get; set; }
        [XmlAttribute]
        public double sigmaAnglCc { get; set; }
        [XmlAttribute]
        public double sigmaZenDCc { get; set; }
        [XmlAttribute]
        public double constAnglCc { get; set; }
        [XmlAttribute]
        public double sigmaDistMm { get; set; }
        [XmlAttribute]
        public double ppmDistMm { get; set; }
        [XmlAttribute]
        public double rCenteringMm{ get; set; }

        // methods
        public SigmaForAInstrumentCouple()
        {
            this.Instrument =R.T333;
            this.Target = R.T333;

            this.ppmDistMm = 5;
            this.sigmaAnglCc = 10;
            this.sigmaZenDCc = 10;
            this.sigmaDistMm = 0.1;
            this.rCenteringMm = 0;
        }
        public override string ToString()
        {
            return Condition +" for "+Instrument + " & " + Target + ": sANGL=" + sigmaAnglCc.ToString() + "cc, sZEND=" + sigmaZenDCc.ToString() + "cc, sDIST=" + sigmaDistMm.ToString() + "mm.";
        }

        public object Clone()
        {
            SigmaForAInstrumentCouple clone = (SigmaForAInstrumentCouple)this.MemberwiseClone();
            return clone;
        }
    }
}
