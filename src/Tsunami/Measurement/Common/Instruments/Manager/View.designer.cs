﻿
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments.Manager
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            // let's not dispose as it is a single one used in different final module

            //if (this.buttons != null)
            //    this.buttons.Dispose();
            //this.buttons = null;


            //if (disposing && (components != null))
            //{
            //    components.Dispose();
            //}
            //base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // _PanelBottom
            // 
            this._PanelBottom.Size = new System.Drawing.Size(390, 157);
            // 
            // _PanelTop
            // 
            this._PanelTop.Size = new System.Drawing.Size(390, 63);
            // 
            // InstrumentModuleView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage =R.Instrument;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(400, 230);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "InstrumentModuleView";
            this.Controls.SetChildIndex(this._PanelTop, 0);
            this.Controls.SetChildIndex(this._PanelBottom, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


    }
}
