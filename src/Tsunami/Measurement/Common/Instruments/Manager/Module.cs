using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;
using GeoComClient;
using ManagedNativeWifi;
using TSU.Common.Measures;
using TSU.Views;
using TSU.Views.Message;
using D = TSU.Common.Instruments.Device;
using P = TSU.Preferences;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Common.Instruments.Manager
{
    [Serializable]
    [XmlType(Namespace = "Instruments", TypeName = "Manager")]
    public class Module : TSU.Manager
    {
        #region fields and properties

        [XmlIgnore]
        public Instruments.Module SelectedInstrumentModule { get; set; }

        public List<Instruments.Module> ExistingInstrumentModule { get; set; }

        [XmlIgnore]
        public List<TsuObject> _Instruments
        {
            get
            {
                return AllElements;
            }
            set
            {
                AllElements = value;
            }
        }

        private Instrument _selectedIntrument;
        public Instrument SelectedInstrument
        {
            get
            {
                return _selectedIntrument;
            }
            set
            {
                _selectedIntrument = value;
            }
        }
        [XmlIgnore]
        public Station.Module ParentStationModule
        {
            get
            {
                return ParentModule as Station.Module;
            }
        }

        [XmlIgnore]
        public List<Instrument> SelectableInstruments
        {
            get
            {
                List<Instrument> instruments = new List<Instrument>();
                foreach (var item in SelectableObjects)
                {
                    if (item is Instrument)
                        instruments.Add(item as Instrument);
                }
                return instruments;
            }
        }

        [XmlIgnore]
        public List<Instrument> LastUseSelectableInstruments
        {
            get
            {
                List<Instrument> instruments = new List<Instrument>();

                List<Instrument> selectableInstruments = SelectableInstruments;
                if (selectableInstruments == null || selectableInstruments.Count == 0)
                {
                    selectableInstruments = new List<Instrument>();
                    selectableInstruments.AddRange(AllElements.FindAll(x => ((x as Instrument)._InstrumentClass & tempClass) == (x as Instrument)._InstrumentClass).Cast<Instrument>().ToList());
                }

                foreach (var item in LastUsedInstruments)
                {
                    foreach (var item2 in selectableInstruments)
                    {

                        if (item._Name == item2._Name)
                            instruments.Add(item);
                    }
                }
                return instruments;
            }
        }

        [XmlIgnore]
        public List<Instrument> LastUsedInstruments
        {
            get
            {
                List<Instrument> instruments = new List<Instrument>();
                foreach (var item in TSU.Tsunami2.Preferences.Values.GuiPrefs.LastInstrumenTSUsed)
                {
                    Instrument found = TSU.Tsunami2.Preferences.Values.Instruments.Find(x => x._Name == item.Name);
                    if (found != null)
                        instruments.Add(found);
                }
                return instruments;
            }
        }

        [XmlIgnore]
        public Instrument LastUsedInstrument
        {
            get
            {
                Instrument lastUsed = null;
                List<Instrument> lastUseds = LastUseSelectableInstruments;
                if (lastUseds.Count > 1)
                {
                    lastUsed = lastUseds[lastUseds.Count - 1];
                }
                return lastUsed;
            }
        }


        [XmlIgnore]
        public new View View
        {
            get
            {
                return _TsuView as View;
            }
        }

        //public List<I.InstrumentClasses> ClassesWanted { get; set; }
        //public List<ENUM.InstrumentTypes> TypesWanted { get; set; }
        #endregion

        #region construction

        public Module() : base()
        {
        }
        public Module(TSU.Module module)
            : base(module, R.T325)
        {
        }
        public override void Initialize()
        {
            _Instruments = new List<TsuObject>(); // override it to be able tonot serialize it
            ExistingInstrumentModule = new List<Instruments.Module>();

            AllElements = Tsunami2.Preferences.Values.Instruments.Cast<TsuObject>().ToList();
            base.Initialize();
        }

        public List<TsuObject> GetByClass(InstrumentClasses @class)
        {
            return P.Preferences.Instance.Instruments.Where(x => (x._InstrumentClass & @class) != InstrumentClasses.UNKNOWN).Cast<TsuObject>().ToList();
        }

        public List<TsuObject> GetByClassAndTyps(InstrumentClasses @class, InstrumentTypes @type)
        {
            return P.Preferences.Instance.Instruments.Where(x
                => (x._InstrumentClass & @class) != InstrumentClasses.UNKNOWN
                && (x._InstrumentType == @type)).Cast<TsuObject>().ToList();
        }


        public List<TsuObject> GetByClassesAndTypes(List<InstrumentClasses> classesWanted = null, List<InstrumentTypes> typesWanted = null)
        {
            var selectables = new List<TsuObject>();
            bool isTokeep;
            foreach (IInstrument instrument in P.Preferences.Instance.Instruments)
            {
                // a priori we  keep it
                isTokeep = true;
                // check for wanted classes
                if (classesWanted != null)
                {
                    // a priori we dont keep it
                    isTokeep = false;
                    // unless it is a wanted class or no wanted classes defined
                    foreach (InstrumentClasses c in classesWanted)
                        if (instrument._InstrumentClass == c)
                        {
                            isTokeep = true;
                            break;
                        }
                }

                // if it pass this step then lets check for types
                if (isTokeep)
                {
                    // check for wanted types
                    if (typesWanted != null)
                    {
                        // if there is wanted types defined then we dont want it
                        isTokeep = false;
                        // unless 
                        foreach (InstrumentTypes t in typesWanted)
                            if (instrument._InstrumentType == t)
                            {
                                isTokeep = true;
                                break;
                            }
                    }
                }

                //if we finaly wants it;
                if (isTokeep)
                {
                    if (instrument is ISensor)
                        selectables.Add(instrument as Sensor);
                    else
                    {
                        if (instrument is LevelingStaff)
                        {
                            selectables.Add(instrument as LevelingStaff);
                        }
                        else
                        {
                            selectables.Add(instrument as Reflector.Reflector);
                        }
                    }
                }
            }
            return selectables;
        }

        internal override void ReCreateWhatIsNotSerialized(bool saveSomeMemory = false)
        {
            try
            {
                base.ReCreateWhatIsNotSerialized(saveSomeMemory);
            }
            catch (Exception ex)
            {
                throw new Exception($"Cannot re-create what was not serialized in {_Name}", ex);
            }
        }

        #endregion

        #region Selection


        public void ValidateSelection()
        {
            //this.View._strategy.ValidateSelection();
            if (_SelectedObjects.Count > 0)
            {
                SelectedInstrument = _SelectedObjects[0] as Instrument;
                Instruments.Module rightInstrumentModule = GetTheRightModuleBasedOnInstrument(SelectedInstrument, this);
                if (rightInstrumentModule != null)
                {
                    SelectedInstrumentModule = rightInstrumentModule;
                    if (SelectedInstrument is Sensor) // because we dont want the staff selection to launch  'coupleToInstrument' because id does it for the station module common _InstrumentManager Per default and not to staff manager for explample.
                        ParentStationModule.CoupleToInstrument(SelectedInstrumentModule);
                }
                SendMessage(SelectedInstrument);
            }
            else
            {
                SelectedInstrument = null;
            }
        }
        public override string ToString()
        {
            return $"Imanager for {ParentModule} (insts='{SelectedInstrument}'";
        }

        // this will return the instrumentmodule if it already exist and will decoupled it from the module it is linked previuosly
        public static Instruments.Module DecoupleInstrumentFromAnyModule(TSU.Module module, string instrumentName)
        {
            if (module == null)
            {
                TSU.Debug.WriteInConsole("DecoupleInstrumentFromAnyModule with null module");
                return null;
            }
            TSU.Debug.WriteInConsole("DecoupleInstrumentFromAnyModule " + module._Name);
            Instruments.Module find = null;
            if (module is Station.Module)
            {
                find = (module as Station.Module)._InstrumentManager.SelectedInstrumentModule;
                if (find != null)
                {
                    if (find.Instrument != null)
                    {
                        if (find.Instrument._Name == instrumentName)
                        {
                            (module as Station.Module).DecoupleFromInstrument(find);
                            return find;
                        }
                    }
                    find = null;
                }
            }
            else
            {
                if (module is Guided.Module)
                {
                    foreach (TSU.Module item in module.childModules)
                    {
                        find = DecoupleInstrumentFromAnyModule(item, instrumentName);
                        if (find != null && find.InstrumentIsOn)
                            return find;
                    }
                    //Stations.StationModule sm = (module as Guided.Module)._ActiveStationModule;
                    //if (sm != null)
                    //{
                    //    find = sm._InstrumentManager.SelectedInstrumentModule;
                    //    if (find != null)
                    //    {
                    //        if (find.Instrument != null)
                    //        {
                    //            if (find.Instrument._Name == instrumentName)
                    //            {
                    //                sm.DecoupleFromInstrument(find);
                    //                return find;
                    //            }
                    //            else
                    //                find = null;
                    //        }
                    //    }
                    //}

                }
                else
                {
                    if (module is Guided.Group.Module grGm)
                    {
                        foreach (Guided.Module item in grGm.SubGuidedModules)
                        {
                            find = DecoupleInstrumentFromAnyModule(item, instrumentName);
                            if (find != null && find.InstrumentIsOn)
                                return find;
                        }
                    }
                    else
                    {
                        foreach (TSU.Module item in module.childModules)
                        {
                            find = DecoupleInstrumentFromAnyModule(item, instrumentName);
                            if (find != null && find.InstrumentIsOn)
                                return find;
                        }
                    }
                }
            }
            return find;
        }

        public bool SelectInterfaces(D.Interfaces newInterfaces, D.Interfaces previousInterfaces, int level = 0)
        {
            var selectable = this.GetByClassAndTyps(InstrumentClasses.INTERFACES, InstrumentTypes.WYLER);
            selectable.AddRange(this.GetByClass(InstrumentClasses.INTERFACES));
            var preselected = new List<TsuObject>();
            if (previousInterfaces.Count > level)
            {
                // add the already selected interface on that position
                var previousInterface = previousInterfaces.Items[level];
                if (previousInterface != null)
                    preselected.Add(previousInterface);
            }
            PrepareSelection(selectable, multiSelection: false, preselected);

            this.View.TopLevel = false;
            this.View.Visible = true;
            this.View.UpdateView();

            bool level3Reached = level >= 2;
            string ok = R.T_OK;
            string addmore = R.T_MORE;
            string cancel = R.T_CANCEL;
            var buttons = new List<string>() { ok };
            if (!level3Reached) buttons.Add(addmore);
            buttons.Add(cancel);
            string message = "Choose interface" + $" #{level + 1};"
                + R.T_PREVIOUS + $": {previousInterfaces.ToString()}\r\n"
                + R.T_CURRENT_VALUE + $": {newInterfaces.ToString()}";
            var results = Tsunami2.View.ShowMessageWithFillControl(
                control: this.View,
                type: MessageType.Choice,
                message,
                buttons
                );

            var tbc = results.TextOfButtonClicked;

            Action add = () =>
            {
                var im = ((View)results.ReturnedControls[0]).Module;
                var selectedInterface = im._SelectedObjectInBlue as Instrument;
                if (selectedInterface != null)
                    newInterfaces.Items.Add(selectedInterface);
            };

            // Add more Interfaceq
            if (tbc == "" || tbc == addmore) // null is received if the manager has been closed by a double click on an interface
            {
                add();
                level++;

                if (level3Reached)
                {
                    D.Interfaces.AddUniqueAssemblyToAList(newInterfaces, Tsunami2.Properties.KnownInterfacesAssembly);
                    return true;
                }

                else
                    return SelectInterfaces(newInterfaces, previousInterfaces, level);
            }

            // Add and stop
            if (tbc == ok)
            {
                add();
                D.Interfaces.AddUniqueAssemblyToAList(newInterfaces, Tsunami2.Properties.KnownInterfacesAssembly);
                return true;
            }

            // Cancel
            return false;
        }

        public void PrepareSelection(List<TsuObject> selectables, bool multiSelection, List<TsuObject> preselected = null)
        {
            SetInstrumentSelectableList(selectables);

            _SelectedObjects.Clear();
            if (preselected != null && preselected.Count > 0)
            {
                _SelectedObjects.AddRange(preselected);
                _SelectedObjectInBlue = preselected[0];
            }
            else
                _SelectedObjectInBlue = null;

            this.MultiSelection = multiSelection;
            this.View.Module.MultiSelection = multiSelection;
            this.View.CheckBoxesVisible = multiSelection;

        }

        /// <summary>
        /// first item of the preseelcted will be the one selected in blue
        /// </summary>
        /// <param name="titleAndMessage"></param>
        /// <param name="selectables"></param>
        /// <param name="multiSelection"></param>
        /// <param name="preselected"></param>
        /// <param name="showCancel"></param>
        /// <returns></returns>
        public Instrument SelectInstrument(string titleAndMessage, List<TsuObject> selectables, bool multiSelection, List<TsuObject> preselected = null, bool showCancel = true)
        {
            PrepareSelection(selectables, multiSelection, preselected);

            Instrument i = View.SelectInstrument(this, titleAndMessage, showCancel);

            if (i != null)
            {
                SetInstrument(i);
            }

            return i;
        }

        /// <summary>
        /// Selectionne plusieures mires pour le contr�le du zero mire
        /// </summary>
        /// <param name="titleAndMessage"></param>
        /// <param name="type"></param>
        /// <param name="showCancel"></param>
        /// <returns></returns>
        public List<LevelingStaff> SelectStaffs(string titleAndMessage = null, InstrumentClasses type = InstrumentClasses.UNKNOWN, bool showCancel = true)
        {
            List<TsuObject> selectables = null;
            if (type != InstrumentClasses.UNKNOWN)
                selectables = AllElements.FindAll(x => (x as Instrument)._InstrumentClass == type);
            SetInstrumentSelectableList(selectables);

            List<LevelingStaff> i = View.SelectStaffs(titleAndMessage, showCancel);
            return i;
        }



        public void SetInstrument(Instrument i)
        {
            TSU.Tsunami2.Preferences.Values.GuiPrefs.AddLastInstrumentUsed(i);
            const bool UseOldWay = false;

            if (UseOldWay)
                OldSetInstrument(i);
            else
            {
                if (i == null)
                    return;
                Instruments.Module newInstrumentModule = GetTheRightModuleBasedOnInstrument(i, this);
                if (newInstrumentModule != null)
                {
                    if (SelectedInstrumentModule != null)
                    {
                        ParentStationModule.DecoupleFromInstrument(SelectedInstrumentModule);
                    }

                    SelectedInstrumentModule = newInstrumentModule;
                    SelectedInstrumentModule.ParentModule = this; // very important for livedata that uses instrumentmodule.parent.parent

                    if (i is Sensor sensor)
                    {
                        // Decouple instrument from any coupled module
                        var im = SelectedInstrumentModule;
                        if (im?.Instrument != null)
                        {
                            DecoupleInstrumentFromAnyModule(Tsunami2.Properties, im.Instrument._Name);
                            ParentStationModule._Station.ParametersBasic._Instrument = sensor;
                            ParentStationModule.CoupleToInstrument(im);
                        }
                    }
                }

                SelectedInstrument = i;
                // suscribe to the instrument giving it his own ref
                OnNext(SelectedInstrument);

            }

        }

        InstrumentClasses tempClass = InstrumentClasses.UNKNOWN;

        internal void AddButtonForLastInstrument(List<Control> contextButtons, Station.Module stm, InstrumentClasses ic = InstrumentClasses.UNKNOWN)
        {
            tempClass = ic;
            if (LastUsedInstrument != null)
            {

                Views.BigButton b = new Views.BigButton($"{LastUsedInstrument._Name};{R.T_USE_THE_LAST_INSTRUMENT}", R.Instrument);
                b.BigButtonClicked += delegate { stm.OnInstrumentChanged(LastUsedInstrument as Sensor); };
                contextButtons.Add(b);

            }
        }

        internal void AddButtonToScanForSsidInstrument(List<Control> contextButtons, Station.Module stm, InstrumentClasses ic = InstrumentClasses.UNKNOWN)
        {
            Views.BigButton b = new Views.BigButton($"Scan for instrument access point;Try to find other SSID corresponding to an instrument, it takes 5 seconds", R.Instrument);
            
            b.BigButtonClicked += delegate
            {
                System.Threading.Thread thread = new System.Threading.Thread(new ThreadStart(() =>
                {
                    try
                    {
                        System.Threading.CancellationToken cancellationToken = new System.Threading.CancellationToken(); 
                        NativeWifi.ScanNetworksAsync(TimeSpan.FromSeconds(5), cancellationToken).Wait(cancellationToken);
                        var buttons = GetButtonsForSSIDInstruments(stm, ic);
                        stm.View.Invoke(new Action(() =>
                        {
                            stm.View.ShowPopUpSubMenu(buttons);
                        }));
                        
                    }
                    catch (Exception ex)
                    {
                        var view = Tsunami2.View;
                        if (view.IsHandleCreated)
                            view.Invoke(new Action(() =>
                            {
                                var m = new MessageInput(MessageType.Warning, $"An unexpected error occurred: {ex.Message}").Show();
                            }));
                    }
                }));
                thread.Start();
            };
            b.HasSubButtons = true;
            contextButtons.Add(b);
        }
        internal void AddButtonForSsidInstrument(List<Control> contextButtons, Station.Module stm, InstrumentClasses ic = InstrumentClasses.UNKNOWN)
        {
            tempClass = ic;
            contextButtons.AddRange(GetButtonsForSSIDInstruments(stm, ic));
        }

        internal List<Control> GetButtonsForSSIDInstruments( Station.Module stm, InstrumentClasses ic)
        {
            var buttons = new List<Control>();
            if (GetAvailableSsids("", out var ssids, out var instruments))
            {
                for (int i = 0; i < ssids.Count; i++)
                {
                    var ssid = ssids[i];

                    var instrument = instruments[i];
                    Views.BigButton b = new Views.BigButton($"{instrument._Name};{R.T_FOUND_BEHIND} SSID:'{ssid}'. {R.T_CONNECT_AND_RENEW}", R.Instrument);
                    b.BigButtonClicked += delegate {
                        stm.OnInstrumentChanged(instrument as Sensor);

                        System.Threading.Thread thread = new System.Threading.Thread(new ThreadStart(() =>
                        {
                            try
                            {
                                if (TSU.WiFi.Connect(ssid))
                                {
                                    var ada = WiFi.GetWifiInteraceWithNetsh().FirstOrDefault();
                                    WiFi.RenewIpConfig(ada.InterfaceName);
                                }
                            }
                            catch (Exception ex)
                            {
                                var view = Tsunami2.View;
                                if (view.IsHandleCreated)
                                    Tsunami2.View.Invoke(new Action(() =>
                                    {
                                        var m = new MessageInput(MessageType.Warning, $"An unexpected error occurred: {ex.Message}").Show();
                                    }));
                            }
                        }));
                        thread.Start();
                    };
                    buttons.Add(b);
                }
            }
            return buttons;
        }

        public void OldSetInstrument(Instrument i)
        {
            if (i != null)
            {
                SelectedInstrumentModule = GetTheRightModuleBasedOnInstrument(i, this);
                SelectedInstrument = i;

                // suscribe to the instrument giving it his own ref
                OnNext(SelectedInstrument);
            }
        }


        /// <summary>
        /// Set the selectionnable list in the instrument manager
        /// </summary>
        /// <param name="SelectionnableList"></param>
        internal void SetInstrumentSelectableList(List<TsuObject> SelectionnableList = null)
        {
            if (AllElements.Count == 0)
                AllElements.AddRange(Tsunami2.Preferences.Values.Instruments);
            // Build the selectionnable list
            SelectableObjects.Clear();
            if (SelectionnableList != null)
                SelectableObjects.AddRange(SelectionnableList);
            else
                SelectableObjects.AddRange(AllElements);
        }

        internal override bool IsItemSelectable(TsuObject tsuObject)
        {
            bool isIt = tsuObject is Instrument;
            return isIt;
        }

        /// <summary>
        /// Return module if already existing, or create one
        /// </summary>
        /// <param name="i"></param>
        /// <param name="parentModule"></param>
        /// <returns></returns>
        public static Instruments.Module GetTheRightModuleBasedOnInstrument(Instrument i, Module parentModule)
        {
            var existing = TSU.Tsunami2.Preferences.Values.InstrumentModules;

            // Look in exsiting instrument module
            Instruments.Module im = null;
            foreach (var item in existing)
            {
                if (item.Instrument == i)
                {
                    im = item;
                    if (im.View.IsDisposed)
                    {
                        im.CreateView();
                    }
                }
            }

            // If not existing, create
            bool exist = im != null;
            if (!exist)
            {
                im = GetNewModuleBasedOnInstrument(i, parentModule);
                if (im is null) return null;
                existing.Add(im);
            }
            else
            {
                // int ie = observers.Count;
                DecoupleInstrumentFromAnyModule(Tsunami2.Properties, i._Name);
                // im.Subscribe(this);
            }

            //return
            im.SubscribeLogToEvents();

            return im;
        }

        private static Instruments.Module GetNewModuleBasedOnInstrument(Instrument i, Module parentModule)
        {
            if (i is D.AT40x.Instrument) return new D.AT40x.Module(parentModule, i);
            if (i is D.DNA03.Instrument) return new D.DNA03.Module(parentModule, i);
            if (i is D.LS15.Instrument) return new D.LS15.Module(parentModule, i);
            if (i is D.WYLER.Instrument) return new D.WYLER.Module(parentModule, i);
            if (i is D.Manual.Ecartometer.Instrument) return new D.Manual.Ecartometer.Module(parentModule, i);
            if (i is D.Manual.Level.Instrument) return new D.Manual.Level.Module(parentModule, i);
            if (i is D.Manual.TiltMeter.Instrument) return new D.Manual.TiltMeter.Module(parentModule, i);
            if (i is D.TC2002.Instrument) return new D.TC2002.Module(parentModule, i);
            if (i is D.T3000.Instrument) return new D.T3000.Module(parentModule, i);
            if (i is D.Manual.Theodolite.T2) return new D.Manual.Theodolite.Module(parentModule, i);
            if (i is D.TCRA1100.Instrument) return new D.TCRA1100.Module(parentModule, i);
            if (i is D.TDA5005.Instrument) return new D.TDA5005.Module(parentModule, i);
            if (i is D.Manual.Theodolite.Instrument) return new D.Manual.Theodolite.Module(parentModule, i);
            if (i is D.Simulator.Instrument) return new D.Simulator.Module(parentModule, i);

            if (i is D.TS60.Instrument) return new D.TS60.Module(parentModule, i);

            return null;
        }

        internal void OpenInstrumentFile()
        {
            string filename = TsuPath.GetFileNameToOpen(View,
                System.IO.Path.GetDirectoryName(P.Preferences.Instance.Paths.Instruments),
                "", "Geode files (*.dat)|*.dat|Xml files (*.xml)|*.xml", "Browse Instrument file");

            Action InnedMethod = () =>
            {
                try
                {
                    if (filename != "")
                    {
                        AllElements.Clear();
                        SelectableObjects.Clear();
                        _SelectedObjects.Clear();
                        List<Instrument> l = new List<Instrument>();
                        T.Conversions.FileFormat.FillListFromGeodeInstrumentFile(ref l, filename);
                        AllElements.AddRange(l);
                        _SelectedObjectInBlue = null;
                        _SelectedObjects.Clear();
                        MultiSelection = false;
                        Change();
                        UpdateView();
                    }
                }
                catch (Exception ex)
                {
                    string titleAndMessage = string.Format(R.T272, ex.Source, ex.Message);
                    new MessageInput(MessageType.Critical, titleAndMessage)
                    {
                        Sender = System.Reflection.MethodBase.GetCurrentMethod().Name
                    }.Show();
                }
            };
            View.ShowProgress(View, InnedMethod, 2, string.Format(R.TM_SeqLoad, filename));
        }

        #endregion

        #region Event Observer

        public override void OnNext(TsuObject TsuObject)
        {
            dynamic dynamic = TsuObject;
            OnNextBasedOn(dynamic);
        }
        public void OnNextBasedOn(Sensor i)
        {
            SendMessage(i);
        }
        public void OnNextBasedOn(Reflector.Reflector i)
        {
            SelectedInstrument = i;
            SendMessage(i);
        }
        public void OnNextBasedOn(LevelingStaff i)
        {
            SelectedInstrument = i;
            SendMessage(i);
        }
        public void OnNextBasedOn(Measure m)
        {
            SendMessage(m);
        }

        internal void LinkUnlinkedInstrument()
        {
            if (SelectedInstrumentModule == null && SelectedInstrument != null)
            {
                SetInstrument(SelectedInstrument);
            }
        }

        #endregion

        #region Measurement

        //internal void SetNextMeasure(Management.Measure.Management.Measure measure)
        //{
        //    if (_intrument != null)
        //    {
        //        (this._intrument as Sensor)._ToBeMeasuredData = measure;
        //    }
        //}


        internal void SetNextMeasure(Polar.Measure measure)
        {
            if (SelectedInstrumentModule != null)
            {
                SelectedInstrumentModule.ToBeMeasuredData = measure;
                SelectedInstrumentModule.OnMeasureToDoReceived();
            }

        }

        public static bool GetAvailableSsids(string model, out List<string> ssids , out List<Instrument> instruments)
        {
            instruments = new List<Instrument>();
            ssids = new List<string>();
            List<string> ssidsWithSerialNumbers = GetSSIDs();

            // Filter instruments with model containing "AT40"
            var filteredInstrumets = TSU.Preferences.Preferences.Instance.Instruments.Where(x => x._Model.Contains(model) && x._SerialNumber.Length == 6);

            // Find SSIDs that contain the serial number of AT40x instruments
            foreach (var ssid in ssidsWithSerialNumbers)
            {
                var matchedInstrument = filteredInstrumets.FirstOrDefault(instr => ssid.Contains(instr._SerialNumber));
                if (matchedInstrument != null)
                {
                    ssids.Add(ssid);
                    instruments.Add(matchedInstrument);
                }
            }
            return ssids.Count > 0;
        }

        public static List<string> GetSSIDs()
        {
            var allSsids = TSU.WiFi.GetSSIDs();
            allSsids = allSsids.Distinct().ToList();

            // Filter SSIDs containing exactly six consecutive digits
            var ssidsWithSerialNumbers = allSsids.Where(x => Regex.IsMatch(x, @"\d{6}")).ToList();
            return ssidsWithSerialNumbers;
        }

        internal void Measure()
        {
            const bool useAdvancedInstrumentViewMeasureLauncher = true;
            if (useAdvancedInstrumentViewMeasureLauncher)
            {
                SelectedInstrumentModule.View.MeasureAsync();
                return;
            }


            // can be used with TS60, AT40x and manual theodolite
            if (SelectedInstrumentModule != null)
            {

                Result result = SelectedInstrumentModule.Measure(); // just to laucnh the measurement

                if (result.Success)
                {
                    if (!(SelectedInstrumentModule is D.AT40x.Module || SelectedInstrumentModule is D.Manual.Theodolite.Module))
                    {
                        //if (this.SelectedInstrumentModule.beingMeasured != null)
                        {
                            SelectedInstrumentModule.ReportMeasurement();
                            SelectedInstrumentModule.FinishMeasurement();
                            SelectedInstrumentModule.beingMeasured = null;
                        }
                    }
                }
            }
            // not possible because some instru are async ... //if (this.SelectedInstrumentModule.Measure().Success) this.SelectedInstrumentModule.ReportMeasurement();
            else
                throw new Exception(R.T327);
        }

        #endregion
    }
}

namespace TSU.Common.Instruments
{
    #region exception
    public class InstrumentModuleUnknownException : Exception
    {
        internal InstrumentModuleUnknownException()
        {
        }
    }

    #endregion


}
