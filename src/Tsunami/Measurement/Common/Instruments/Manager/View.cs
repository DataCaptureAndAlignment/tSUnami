﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TSU.Views;
using TSU.Views.Message;
using R = TSU.Properties.Resources;
using V = TSU.Common.Strategies.Views;

namespace TSU.Common.Instruments.Manager
{
    public partial class View : ManagerView, IObserver<TsuObject>
    {
        public new Module Module
        {
            get
            {
                return this._Module as Module;
            }

        }
        //public  List<I.InstrumentClasses> TypesToShow;
        public string ChoosenInstrumentId;

        public IM_Buttons buttons;
        public IM_Buttons Buttons
        {
            get
            {
                if (buttons == null)
                    buttons = new IM_Buttons(this);
                return buttons;
            }
        }

        #region Construction

        public View(IModule module)
            : base(module)
        {
            InitializeComponent();
            this.ApplyThemeColors();

            this.AdaptTopPanelToButtonHeight();
            this.SuspendLayout();
            this.Image = R.Instruments;

            //Initializations
            InitializeMenu();
            this.AutoCheckChildren = false;

            this.currentStrategy.Update();
            this.currentStrategy.Show();

            (_Module as Module).Subscribe(this);


            Opacity = 90;
            Width = 700; Height = 500; StartPosition = FormStartPosition.WindowsDefaultLocation;
            this.ResumeLayout();
        }

        private void ApplyThemeColors()
        {
            this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
        }

        internal override void InitializeStrategy()
        {
            this.AvailableViewStrategies = new List<V.Types>() { V.Types.Tree, V.Types.List };
            this.SetStrategy(TSU.Tsunami2.Preferences.Values.GuiPrefs.InstrumentViewType);
        }

        internal override void SavePreference(V.Types viewStrategyType)
        {
            TSU.Tsunami2.Preferences.Values.GuiPrefs.InstrumentViewType = viewStrategyType;

            TSU.Tsunami2.Preferences.Values.SaveGuiPrefs();
        }

        internal override void InitializeMenu()
        {
            buttons = new IM_Buttons(this);
            //Main Button

            base.InitializeMenu();

            bigbuttonTop.ChangeNameAndDescription(R.T328);
            bigbuttonTop.ChangeImage(R.Instrument);
        }

        internal override void ShowContextMenuGlobal()
        {
            contextButtons = new List<Control>();

            contextButtons.Add(buttons.open);

            this.ShowPopUpMenu(contextButtons);
        }

        public override List<Control> GetViewOptionButtons()
        {
            return new List<Control>() { buttons.ShowAll };
        }

        public class IM_Buttons
        {
            private View View;
            // public BigButton main;
            public BigButton open;
            public BigButton ShowAll;
            public BigButton ShowCalibratedReflector;

            public IM_Buttons(View view)
            {
                View = view;
                // main = new BigButton(R.T328, R.Instrument, view.ShowContextMenuGlobal);
                open = new BigButton(R.T329, R.Open, this.View.Module.OpenInstrumentFile);
                ShowAll = new BigButton($"{R.T_SHOW_ALL};{R.T_SHOW_ALL_INSTRUMENTS}", R.Instruments, View.ShowAll);
            }

            internal void Dispose()
            {
                View = null;

                open?.Dispose();
                open = null;

                ShowAll?.Dispose();
                ShowAll = null;
            }
        }

        private void ShowAll()
        {
            this.Module.SelectableObjects.Clear();
            this.UpdateView();
        }

        #endregion

        #region Actions


        #region selection


        public override void UpdateView()
        {
            this.currentStrategy.Update();
        }

        public override void OnNext(TsuObject TsuObject)
        {
            dynamic dynamic = TsuObject;
            this.OnNextBasedOn(dynamic);
        }

        //private bool IsTypeWanted(Sensor intrument)
        //{
        //    foreach (var typesToShow in TypesToShow)
        //    {
        //        if (intrument._InstrumentClass == typesToShow)
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        internal Instrument SelectInstrument(Module module, string titleAndMessage, bool showCancel = true)
        {
            this._Module = module; // to restablish link that has been modify since we created only one instrument view for all tsunami.

            void HideMessageVIew(object o, V.EventArgs e)
            {
                if (e.ModuleView is ManagerView mv)
                {
                    if (mv.Module._SelectedObjectInBlue is Instrument)
                        e.ModuleView.FindMessageModuleViewInParents()?.Hide();
                    else
                        new MessageInput(MessageType.Warning, R.T_NOTHING_SELECTED).Show();
                }
            }

            //if (SelectionnableList != null)
            //{
            //    this.Module.SelectableObjects.Clear();
            //    this.Module.SelectableObjects.AddRange(SelectionnableList);
            //}
            
            this.currentStrategy.DoubleClick += HideMessageVIew;

            this.currentStrategy.ShowSelectionInButton();
            this.currentStrategy.Update();

            if (showCancel)
                this.ShowInMessageTsu(
                titleAndMessage ?? R.T330,
                "Select", this.ValidateSelection,
                R.T_CANCEL, this.CancelSelection);
            else
                this.ShowInMessageTsu(
                titleAndMessage ?? R.T330,
                "Select", this.ValidateSelection);


            this.currentStrategy.DoubleClick -= HideMessageVIew;

            if (this.Module._SelectedObjects.Count > 0)
                return this.Module._SelectedObjects[0] as Instrument;
            else
                return null;
        }

        internal List<LevelingStaff> SelectStaffs(string titleAndMessage = null, bool showCancel = true)
        {

            void HideMessageVIew(object o, V.EventArgs e)
            {
                if (e.ModuleView.Parent.Parent.Parent is MessageTsu tsu)
                {
                    tsu.Hide();
                }
            }

            this.currentStrategy.DoubleClick += HideMessageVIew;

            this.currentStrategy.ShowSelectionInButton();
            this.currentStrategy.Update();

            if (showCancel)
                this.ShowInMessageTsu(
                titleAndMessage ?? R.T330,
                "Select", this.ValidateSelection,
                R.T_CANCEL, this.CancelSelection);
            else
                this.ShowInMessageTsu(
                titleAndMessage ?? R.T330,
                "Select", this.ValidateSelection);


            this.currentStrategy.DoubleClick -= HideMessageVIew;

            if (this.Module._SelectedObjects.Count > 0)

            {
                List<LevelingStaff> instrumentList = new List<LevelingStaff>();
                foreach (TsuObject item in this.Module._SelectedObjects)
                {
                    instrumentList.Add(item as LevelingStaff);
                }
                return instrumentList;
            }
            else
                return null;
        }
        public void CancelSelection()
        {
            this.Module._SelectedObjects.Clear();
        }

        #endregion

        public override string ShowInMessageTsu(string titleAndMessage, string middleButton, Action middleAction,
            string rightButton = null, Action rightAction = null, string leftButton = null, Action leftAction = null,
            Color? background = null, Color? foreground = null, bool showdialog = true, bool fullSize = false,
            DsaFlag flagDont = null, EventHandler onShowingMessage = null)
        {
            this.Module.AllElements = this.Module._Instruments;
            this.currentStrategy.Update();
            this.currentStrategy.Focus();

            return base.ShowInMessageTsu(titleAndMessage, middleButton, middleAction, rightButton, rightAction,
                leftButton, leftAction, background, foreground, showdialog, fullSize, flagDont, onShowingMessage);
        }

        public override List<string> ListviewColumnHeaderTexts
        {

            get
            {
                return new List<string>() { "Type", " Brand", "Model", "[SN]", "Calibration info" };
            }
            set
            {

            }
        }


        #region Nodes Arrangement

        public override void ArrangeNodes(TreeView treeview)
        {
            treeview.Enabled = false;
            TsuNode ArrangedNode = new TsuNode();
            while (treeview.Nodes.Count > 0)
            {
                TsuNode node = ExtractNodeFromTree(treeview);

                if (node != null)
                {
                    TsuNode mainTypeNode = GetOrCreateMainTypeNode(node, ArrangedNode);
                    TsuNode classNode = GetOrCreateClassNode(node, mainTypeNode);
                    TsuNode modelNode = GetOrCreateModelNode(node, classNode);
                    RenameItemAndInsertInModelNode(node, modelNode);
                }
            }
            SimplifyTreeForSingleItemNodes(ArrangedNode.Nodes);

            AddArrangedNodeToTree(ArrangedNode, treeview);
            treeview.Enabled = true;
        }

        private void SimplifyTreeForSingleItemNodes(TreeNodeCollection collection)
        {
            for (int i = 0; i < collection.Count; i++)
            {
                SimplifyTreeForSingleItemNodes(collection[i].Nodes);
                if (collection[i].Nodes.Count == 1)
                {
                    TreeNode children = collection[i].Nodes[0];
                    children.Text = collection[i].Text + " " + children.Text;
                    collection.RemoveAt(i);
                    collection.Insert(i, children);
                }
            }
        }

        private void AddArrangedNodeToTree(TsuNode ArrangedNode, TreeView treeview)
        {
            foreach (TsuNode item in ArrangedNode.Nodes)
            {
                treeview.Nodes.Add(item);
            }
        }

        private TsuNode ExtractNodeFromTree(TreeView treeview)
        {
            TsuNode node = treeview.Nodes[0] as TsuNode;
            treeview.Nodes.RemoveAt(0);
            return node;
        }

        private void RenameItemAndInsertInModelNode(TsuNode item, TsuNode modelNode)
        {
            if (item.Tag == null) return;
            int j = 0;
            string serial = (item.Tag as IInstrument)._SerialNumber;
            while (modelNode.Nodes.ContainsKey(serial))
            {
                j += 1;
                serial = (item.Tag as IInstrument)._SerialNumber.ToString() + "_" + j.ToString();
            }
            item.Name = serial;
            item.Text = serial;
            modelNode.Nodes.Add(item);
        }

        private TsuNode GetOrCreateModelNode(TsuNode item, TsuNode classNode)
        {
            // creation of the Node "Model" if needed
            if (item.Tag == null) return null;
            String Model = (item.Tag as IInstrument)._Model;
            int index = classNode.Nodes.IndexOfKey(Model);
            if (index == -1)
            {
                TsuNode newOne = new TsuNode();
                newOne.DressAs("Instrument"); // attention .name is change here
                newOne.Name = Model;
                newOne.Text = newOne.Name;
                classNode.Nodes.Add(newOne);
                return newOne;
            }
            else
                return classNode.Nodes[index] as TsuNode;
        }

        private TsuNode GetOrCreateClassNode(TsuNode item, TsuNode mainTypeNode)
        {
            if (item.Tag == null) return null;
            InstrumentClasses classe = (item.Tag as IInstrument)._InstrumentClass;
            int index = mainTypeNode.Nodes.IndexOfKey(classe.ToString());
            if (index == -1)
            {
                TsuNode newOne = new TsuNode();
                newOne.DressAs(classe, "", null);
                mainTypeNode.Nodes.Add(newOne);
                return newOne;
            }
            else
                return mainTypeNode.Nodes[index] as TsuNode;
        }

        private TsuNode GetOrCreateMainTypeNode(TsuNode item, TsuNode tempGlobal)
        {
            string mainType = GetMainType(item);
            int index = tempGlobal.Nodes.IndexOfKey(mainType);
            if (index == -1)
            {
                TsuNode newOne = new TsuNode();
                newOne.DressAs(mainType);
                newOne.Name = mainType;
                newOne.Text = newOne.Name;
                tempGlobal.Nodes.Add(newOne);
                return newOne;
            }
            else
                return tempGlobal.Nodes[index] as TsuNode;
        }

        private string GetMainType(TsuNode item)
        {
            if (item.Tag is ISensor)
                return "Instruments";
            else
                return "Accessories";
        }

        #endregion


        #endregion

    }
}
