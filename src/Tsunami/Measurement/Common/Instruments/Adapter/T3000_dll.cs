using System;
using System.Collections.Generic;
using System.Text;using R = TSU.Properties.Resources;
using System.Runtime.InteropServices;

namespace TSU.Common.Instruments.Adapter
{
    public static class T3000_dll
    {
        #region Wrapper T3000.dll functions

        const string dllPath = @"libs\SuSoft\T3000_3.dll";

        // Ouvre le port s�rie numero portNumber avec parametres
        // Retourne portHandle (-1 si pas OK)
        [DllImport(dllPath, EntryPoint = "Open_port_ud", CallingConvention = CallingConvention.StdCall)]
        public static extern long OpenPort(
                string portNumber,
                int bauds,
                byte byteSize,
                byte parity,
                byte stopBits,
                ref int portHandle);

        // Allume le T3000:Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "T3000on", CallingConvention = CallingConvention.StdCall)]
        public static extern float TurnOn(int portHandle);

        // Eteint le T3000:Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "T3000off", CallingConvention = CallingConvention.StdCall)]
        public static extern float TurnOff(int portHandle);

        // Change le signal sonore du T3000:Retourme 0 si OK / retourne -1 si pas OK:Off=0;Low=1;High=2
        [DllImport(dllPath, EntryPoint = "T3000Beep", CallingConvention = CallingConvention.StdCall)]
        public static extern float Beep(int portHandle, int offLowHigh);

        // Ferme le port s�rie
        [DllImport(dllPath, EntryPoint = "Close_port", CallingConvention = CallingConvention.StdCall)]
        public static extern float ClosePort(int portHandle);

        // Purge les tampons du port s�rie
        [DllImport(dllPath, EntryPoint = "Purge_port", CallingConvention = CallingConvention.StdCall)]
        public static extern void PurgePort(int portHandle);

        // Retourme l 'angle horizontal si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "Angle_hor", CallingConvention = CallingConvention.StdCall)]
        public static extern float GetHorizontalAngle(int portHandle);

        // 'Stoppe le processus en cours sur le T3000:Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "Stop_T3000", CallingConvention = CallingConvention.StdCall)]
        public static extern float StopProcess(int portHandle);

        // 'Stoppe le processus en cours sur le T3000 sans emulation clavier:Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "StopAll_T3000", CallingConvention = CallingConvention.StdCall)]
        public static extern float StopAllProcess(int portHandle, double waitTime);

        // 'Choix d 'unite d'enregistrement GRE4:Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "GRE4", CallingConvention = CallingConvention.StdCall)]
        public static extern float SetGRE4(int portHandle);

        // 'Choix des parametres d'interface RS232standard:Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "RS232Standard", CallingConvention = CallingConvention.StdCall)]
        public static extern float SetRS232Standard(int portHandle);

        // Choix du prgm 0 de mesure de distance:Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "PrgmDist0", CallingConvention = CallingConvention.StdCall)]
        public static extern float SetDistancePrgm0(int portHandle);

        // Mise sous tension du compensateur:Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "Compon", CallingConvention = CallingConvention.StdCall)]
        public static extern float TurnOnCompensator(int portHandle);

        // Activation du calculateur de correction des angles:Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "Calccorrangles", CallingConvention = CallingConvention.StdCall)]
        public static extern float TurnOnCorrectionCalculator(int portHandle);

        // 'Mise � zero de la constante d'addition:Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "Consteadd0", CallingConvention = CallingConvention.StdCall)]
        public static extern float SetAdditionConstantToZero(int portHandle);

        // Mise � zero des ppm:Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "Ppm0", CallingConvention = CallingConvention.StdCall)]
        public static extern float SetPpmToZero(int portHandle);

        // Affichage avec 4 decimales sur le T3000:Retourme 0 si OK / retourne -1 si pas OK 
        [DllImport(dllPath, EntryPoint = "Display4", CallingConvention = CallingConvention.StdCall)]
        public static extern float SetDecimalDisplayTo4(int portHandle);

        // Regle l'unite de longueur du distance-metre sur : 0,0001 m:Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "LenUnit1", CallingConvention = CallingConvention.StdCall)]
        public static extern float SetDistanceUnityTo10Minus4(int portHandle);

        // Mise en fonctionnement continu du T3000: Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "Fonctcontinu", CallingConvention = CallingConvention.StdCall)]
        public static extern float SetFonctionnementContinu(int portHandle);

        // Affiche sur DI2000 le signal retour du distance-m�tre:Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "Signaldist", CallingConvention = CallingConvention.StdCall)]
        public static extern float ShowDistanceFeedbackOnDi2000(int portHandle);

        
        // Retourne la distance brute dans le parametre dist_brute:Retourme 0 si OK / retourne -1 si pas OK
        // dans carnet il est toujours preced� de purge port
        [DllImport(dllPath, EntryPoint = "DistDI2000", CallingConvention = CallingConvention.StdCall)]
        public static extern float GetDistance(int portHandle, ref float horizontalAngle, ref float verticalAngle, ref float rawDistance);

        // Retourne les valeurs d'angles dans les parametres dist_brute angle_h et angle_v:Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "Angles", CallingConvention = CallingConvention.StdCall)]
        public static extern float GetAngles(int portHandle, ref float horizontalAngle, ref float verticalAngle);

        public static float GetAngles2(int portHandle, ref float horizontalAngle, ref float verticalAngle)
        {
            return GetAngles(portHandle, ref horizontalAngle, ref verticalAngle);
        }


        // Affichage sur T3000 des angles en continu:Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "Displaycontinu", CallingConvention = CallingConvention.StdCall)]
        public static extern float ShowDisplayNonStop(int portHandle);

        // Affichage sur T3000 de la temp�rature:Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "DisplayTemp", CallingConvention = CallingConvention.StdCall)]
        public static extern float ShowDisplayTemp(int portHandle);

        // Affichage sur T3000 de la charge batterie:Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "DisplayBatt", CallingConvention = CallingConvention.StdCall)]
        public static extern float ShowBattery(int portHandle);
        
        // Met l 'unit� des angles sur grades:Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "Unitgrades", CallingConvention = CallingConvention.StdCall)]
        public static extern float SetUnitToGrad(int portHandle);

        // Affichage sur T3000 des angles H et V: Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "DisplayHzv", CallingConvention = CallingConvention.StdCall)]
        public static extern float ShowDisplayHAndV(int portHandle);

        // Change la vitesse de communication sur l'instrument (aucun changement pour le port de l'ordinateur):Retourne 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "ChangeVitesseTheo", CallingConvention = CallingConvention.StdCall)]
        public static extern float SetCommunicationSpeed(int portHandle, int speed, float waitTime);

        // Cherche la vitesse de communication de l'instrument (elle ne fait aucun changement):Retourne la vitesse si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "ChercheVitesseTheo", CallingConvention = CallingConvention.StdCall)]
        public static extern float GetCommunicationSpeed(int portHandle);

        // Renvoi le string Wild correspondant au WordIdentifier qu'on a d�mand�:Retourne un String vide (longeur z�ro) si un probl�me / retourne String si OK
        [DllImport(dllPath, EntryPoint = "TransmitData", CallingConvention = CallingConvention.StdCall)]
        public static extern float GetData(int portHandle, int wordIdentifierg, ref string text);

        // mod 50 pour prise de distance sur cible autocollante: Retourme 0 si OK / retourne -1 si pas OK
        [DllImport(dllPath, EntryPoint = "ActiveReflectiveTarget", CallingConvention = CallingConvention.StdCall)]
        public static extern float SetReflectiveTargetMode(int int_PortHandle);

        #endregion
	}
}
