using System;
using System.Collections.Generic;
using System.Text;using R = TSU.Properties.Resources;
using System.Runtime.InteropServices;

namespace TSU.Common.Instruments.Adapter
{
	public class GeoCom105_dll
	{
        #region Declaration structures pour communiquer avec GCOM105.dll

        //public struct AUT_POSTOL //Positioning tolerance for Hz and V in radians. Tolerance must be in range of 1cc to 100cc (1.57079 10-6 rad to 1.57079 10-4 rad) 
        //{
        //    public double[] Tolpar;//= new double [2];
        //}

        public struct TMC_HZ_V_ANG // declaration structure pour r�ception mesure angle simple
        {
            public double dHz, dV;

            public TMC_HZ_V_ANG(double dHz1, double dV1)
            {
                dHz = dHz1; //angle H en radians
                dV = dV1;  //angle V en radians
            }
        }
        public struct TMC_ANG_SWITCH // declaration structure pour mettre ON_OFF correction angulaire TPS
        {
            public ON_OFF_TYPE eInclineCorr, eStandAxisCorr, eCollimationCorr, eTiltAxisCorr; //Inclination correction , Standing axis corr, Collimation error corr, Tilting axis corr
        }
        public struct TMC_ATMOS_TEMPERATURE // declaration structure pour les param�tres correction atmosph�rique
        {
            public double dLambda, dPressure, dDryTemperature, dWetTemperature; //Wave lenght of the EDM transmitter , Atmospheric pressure, Dry temperature, Wet temperature
        }
        public struct TMC_REFRACTION // declaration structure pour les param�tres correction r�fraction
        {
            public ON_OFF_TYPE eRefON; //Refraction Correction ON/OFF
            public double dEarthRadius, dRefractiveScale; //Radius of the earth, Refractive coefficient
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct TMC_INCLINE // declaration structure pour r�ception mesure compensateur
        {
            public double dCrossIncline; //Transverse axis incl. (rad)
            public double dLengthIncline; //Longitudinal axis incl. (rad)
            public double dAccuracyIncline; //Inclination accuracy (rad)
            public int InclineTime; //Moment of measurement (ms) time since powerON   

            public override string ToString()
            {
                return $"l: {(dLengthIncline / Math.PI * 200):0.0000} g\r\nt: {(dCrossIncline / Math.PI * 200):0.0000} g\r\n";
            }
        }

        public struct TMC_INCLINE2 // declaration structure pour r�ception mesure compensateur
        {
            public uint _5;
            public uint _6;
            public uint _7;
            public uint _8;
            public uint _9;
            public uint _A;
            public int _B_InclineTime;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct TMC_ANGLE // declaration structure pour r�ception mesure angle complet
        {
            public double dHz; //Horizontal angle (rad)
            public double dV; //Vertical angle (rad)
            public double dAngleAccuracy; //Accuracy of angles (rad)
            public int AngleTime; //Moment of measurement (ms) time since powerON
            public TMC_INCLINE Incline; //Corresponding inclination
            public TMC_FACE eFace; //Face position of telescope
        }
        
        public struct TMC_ANGLE2 // declaration structure pour r�ception mesure angle complet
        {
            public double dHz; //Horizontal angle (rad)
            public double dV; //Vertical angle (rad)
            public double dAngleAccuracy; //Accuracy of angles (rad)
            public int AngleTime; //Moment of measurement (ms) time since powerON
            public TMC_INCLINE2 Incline; //Corresponding inclination
            public TMC_FACE eFace; //Face position of telescope
        }
        #endregion

        #region Declaration class enum pour communiquer avec GCOM105.dll

        public enum AUT_POSMODE
        {
            AUT_NORMAL = 0,//fast positioning mode
            AUT_PRECISE = 1  //exact positioning mode
        };
        public enum AUT_ATRMODE //Possible modes of the target recognition
        {
            AUT_POSITION = 0, //Positioning to the hz and V angle
            AUT_TARGET = 1  //Positioning to a target in the environment of the hz and V angle
        }         //Positionierungsmodi der ATR
        public enum AUT_ADJMODE //Possible settings of the positioning. Tolerance relating angle or the point accuracy at the fine adjust *****
        {
            AUT_NORM_MODE = 0, //Angle tolerance
            AUT_POINT_MODE = 1, //Point Tolerance
            AUT_DEFINE_MODE = 2 // System independent positioning
                                // tolerance. Set with AUT_SetTol
        }


        public enum BOOLE              // BOOLEan type
        {
            FALSE,
            TRUE
        }

        public enum COM_PORT                             // Communications port selector
        {                                           // Use for PC-geocom only
            COM_1 = 0,                             // COM1 : Windows
            COM_2 = 1,                             //  COM2 : Windows
            COM_3 = 2,                             // COM3: Windows
            COM_4 = 3                                // COM4: Windows   
        }
        public enum COM_BAUD_RATE                            // Used for public customer interface
        {                                       //
            COM_BAUD_38400 = 0,                     //
            COM_BAUD_19200 = 1,                     // Default Baud Rate
            COM_BAUD_9600 = 2,                     //
            COM_BAUD_4800 = 3,                     //
            COM_BAUD_2400 = 4                       //
        }
        public enum COM_FORMAT                             // Geocom format selector
        {
            COM_ASCII = 0,                         //
            COM_BINARY = 1                            //
        }
        public enum COM_TPS_STATUS                            //  Current state of server
        {                                       //
            COM_TPS_OFF = 0,                          //  TPS is off
            COM_TPS_SLEEPING = 1,                     //  TPS is in sleep mode
            COM_TPS_READY = 2,                        //  TPS is on & active
            COM_TPS_UNKNOWN = 3                       //  Unknown/Not initialized because mode isn't supported or signoff is disabled
        };
        public enum COM_TPS_STOP_MODE //Mode arr�t TPS
        {
            COM_TPS_SHUT_DOWN = 0, // Arr�t TPS
            COM_TPS_SLEEP = 1, //Mode sleep
        }
        public enum EDM_MODE //Mode de mesure EDM pour la distance **************
        {
            EDM_MODE_NOT_USED = 0, // Init value
            EDM_SINGLE_TAPE = 1, // Single measurement with tape
            EDM_SINGLE_STANDARD = 2, // Standard single measurement
            EDM_SINGLE_FAST = 3, // Fast single measurement
            EDM_SINGLE_LRANGE = 4, // Long range single measurement
            EDM_SINGLE_SRANGE = 5, // Short range single measurement
            EDM_CONT_STANDARD = 6, // Standard repeated measurement
            EDM_CONT_DYNAMIC = 7, // Dynamic repeated measurement
            EDM_CONT_REFLESS = 8, // Reflectorless repeated measurement
            EDM_CONT_FAST = 9, // Fast repeated measurement
            EDM_AVERAGE_IR = 10, // Standard average measurement
            EDM_AVERAGE_SR = 11, // Short range average measurement
            EDM_AVERAGE_LR = 12 // Long range average measurent
        }
        public enum ON_OFF_TYPE //Met ON OFF les diff�rentes fonctions
        {
            OFF = 0,
            ON = 1
        }
        public enum SUP_AUTO_POWER //Automatic shutdown mechanism for the System       
        {
            AUTO_POWER_DISABLED = 0, //desactivate the mechanism
            AUTO_POWER_SLEEP = 1, //activate sleep mechanism
            AUTO_POWER_OFF = 2 //activate shut down
        }
        public enum TMC_MEASURE_PRG //Programme pour faire les mesures ***
        {
            TMC_STOP = 0, // Stop measurement program
            TMC_DEF_DIST = 1, // Default DIST-measurement
            TMC_TRK_DIST = 2, // Distance-TRK measurement
            TMC_CLEAR = 3, // TMC_STOP and clear data
            TMC_SIGNAL = 4, // Signal measurement (test function)
            TMC_IDLE_TASK = 5,
            TMC_DO_MEASURE = 6, // (Re)start measurement task
            TMC_WORK_TIME = 7,
            TMC_RTRK_DIST = 8, // Distance-TRK measurement
            TMC_MEAS_INCL = 9,
            TMC_RED_TRK_DIST = 10, // Red laser tracking
            TMC_FREQUENCY = 11, // Frequency measurement (test)
            TMC_SUSPEND_DIST_PRG = 12,
            TMC_RESUME_DIST_PRG = 13,
            TMC_NEXT_DEF_DIST = 14,
            TMC_DYNAMIC_TRK_DIST = 15
        }
        public enum TMC_INCLINE_PRG //Programme mesure compensateur ***
        {
            TMC_MEA_INC = 0, //Use sensor (apriori sigma)
            TMC_AUTO_INC = 1, //Automatic mode (sensor/plane)
            TMC_PLANE_INC = 2, //Use plane
            TMC_APRIORI_INC = 3,
            TMC_ADJ_INC = 4,
            TMC_REQUIRE_INC = 5,
        }

        public enum BAP_USER_MEASPRG //Mode programme mesure distance
        {
            SINGLE_REF_STANDARD = 0, // standard single IR distance with reflector
            SINGLE_REF_FAST = 1, // fast single IR distance with reflector
            SINGLE_REF_VISIBLE = 2, // long range distance with reflector (red laser)
            SINGLE_RLESS_VISIBLE = 3, // single RL distance, reflector free (red laser)
            CONT_REF_STANDARD = 4, // tracking IR distance with reflector
            CONT_REF_FAST = 5, // fast tracking IR distance with reflector
            CONT_RLESS_VISIBLE = 6, // Average IR distance with reflector
            AVG_REF_STANDARD = 7, // Average IR distance with reflector
            AVG_REF_VISIBLE = 8, // Average long range dist. with reflector (red)
            AVG_RLESS_VISIBLE = 9 // Average RL distance, reflector free (red laser)
        };

        public enum TMC_FACE //Face I ou II du TPS
        {
            TMC_FACE_1 = 0,
            TMC_FACE_2 = 1
        }
        public enum IR_Or_RL //mesure avec reflecteur ou sans reflecteur
        {
            EDM = 0,
            RL = 1
        }
        public static IR_Or_RL ModeMesure = IR_Or_RL.EDM;
        #endregion

        #region Wrapper des fonctions pour communiquer avec  GCOM105.dll
        // Communication avec TCRA 1100
        //Automation functions
        //Change Face TPS

        const string dllPath = @"libs/Leica/GCOM105.dll";

        [DllImport(dllPath, EntryPoint = "VB_AUT_ChangeFace4")]
        public static extern short VB_AUT_ChangeFace4(ref int posMode, ref int atrMode, ref int bDummy);

        //Change l'�tat du switch ATR prisme
        [DllImport(dllPath, EntryPoint = "VB_AUT_SetATRStatus")]
        public static extern short VB_AUT_SetATRStatus(ref int onOff);

        //Donne l'�tat du switch ATR prisme
        [DllImport(dllPath, EntryPoint = "VB_AUT_GetATRStatus")]
        public static extern short VB_AUT_GetATRStatus(ref int onOff);

        //Change l'�tat du switch LOCK prisme
        [DllImport(dllPath, EntryPoint = "VB_AUT_SetLockStatus")]
        public static extern short VB_AUT_SetLockStatus(ref int onOff);

        //Donne l'�tat du switch LOCK prisme
        [DllImport(dllPath, EntryPoint = "VB_AUT_GetLockStatus")]
        public static extern short VB_AUT_GetLockStatus(ref int onOff);

        //Donne le fine adjustment mode actif
        [DllImport(dllPath, EntryPoint = "VB_AUT_GetFineAdjustMode")]
        public static extern short VB_AUT_GetFineAdjustMode(ref int adjustMode);

        //Change le fine adjustment mode actif
        [DllImport(dllPath, EntryPoint = "VB_AUT_SetFineAdjustMode")]
        public static extern short VB_AUT_SetFineAdjustMode(ref int adjustMode);

        //Automatic target positioning - se positionne sur le centre du prisme
        [DllImport(dllPath, EntryPoint = "VB_AUT_FineAdjust3")]
        public static extern short VB_AUT_FineAdjust3(ref double dSrchHz, ref double dSrchV, ref int bDummy);

        ////Set the positioning tolerance No influence on fine adjust mode
        //[DllImport("LeicaDll\\GCOM105.dll", EntryPoint = "VB_AUT_SetTol")]
        //public static extern short VB_AUT_SetTol(ref AUT_POSTOL tolPar);
        ////Get the positioning tolerance No influence on fine adjust mode
        //[DllImport("LeicaDll\\GCOM105.dll", EntryPoint = "VB_AUT_ReadTol")]
        //public static extern short VB_AUT_ReadTol(ref AUT_POSTOL tolPar);
        //Turns telescope to specified position
        [DllImport(dllPath, EntryPoint = "VB_AUT_MakePositioning4")]
        public static extern short VB_AUT_MakePositioning4(ref double hz, ref double v, ref int posMode, ref int atrMode, ref int bDummy);

        //Communication functions
        //Initialisation Geocom
        [DllImport(dllPath, EntryPoint = "VB_COM_Init")]
        public static extern short VB_COM_Init();

        //Ouverture Port Serie
        [DllImport(dllPath, EntryPoint = "VB_COM_OpenConnection")]
        public static extern short VB_COM_OpenConnection(int port, ref int baud, short retries);
        
        //Teste si communication est active
        [DllImport(dllPath, EntryPoint = "VB_COM_NullProc")]
        public static extern short VB_COM_NullProc();
        
        //Permet de fermer le port COM
        [DllImport(dllPath, EntryPoint = "VB_COM_CloseConnection")]
        public static extern short VB_COM_CloseConnection();
        
        //Ferme Geocom
        [DllImport(dllPath, EntryPoint = "VB_COM_End")]
        public static extern short VB_COM_End();
        
        //Permet de mettre off le TPS
        [DllImport(dllPath, EntryPoint = "VB_COM_SwitchOffTPS")]
        public static extern short VB_COM_SwitchOffTPS(ref int offMode);
        
        //Permet afficher fen�tre pour test de tous les settings possible lors ouverture connection avec TDA, par defaut TRUE
        [DllImport(dllPath, EntryPoint = "VB_COM_SetConnDlgFlag")]
        public static extern short VB_COM_SetConnDlgFlag(int bShow);
        
        //insere un delai avant que le TPS reponde � une requete(en ms)
        [DllImport(dllPath, EntryPoint = "VB_COM_SetSendDelay")]
        public static extern short VB_COM_SetSendDelay(ref short nSendDelay);
        
        //insere un delai pour terminer la derniere RPC avant que geocom ne signale une erreur en s par defaut 5s
        [DllImport(dllPath, EntryPoint = "VB_COM_SetTimeOut")]
        public static extern short VB_COM_SetTimeOut(short timeOut);
        
        //Donne le delai pour terminer la derniere RPC avant que geocom ne signale une erreur en s par defaut 5s
        [DllImport(dllPath, EntryPoint = "VB_COM_GetTimeOut")]
        public static extern short VB_COM_GetTimeOut(ref short timeOut);
        
        //Central service functions
        //Donne le num�ro de s�rie instrument
        [DllImport(dllPath, EntryPoint = "VB_CSV_GetInstrumentNo")]
        public static extern short VB_CSV_GetInstrumentNo(ref int serialNo);
        
        //Get the voltage supply, permet de d�terminer le niveau batterie
        [DllImport(dllPath, EntryPoint = "VB_CSV_GetVBat")]
        public static extern short VB_CSV_GetVBat(ref double vBat); // si inf�rieur � 5.5v batterie vide
        
        //Get internal theodolite temperature
        [DllImport(dllPath, EntryPoint = "VB_CSV_GetIntTemp")]
        public static extern short VB_CSV_GetIntTemp(ref double intTemp);

        //EDM Functions
        //Set Laser pointer ON Off
        [DllImport(dllPath, EntryPoint = "VB_EDM_Laserpointer")]
        public static extern short VB_EDM_Laserpointer(ref int OnOff);

        //Supervisor function
        // Get power management configuration status
        [DllImport(dllPath, EntryPoint = "VB_SUP_GetConfig")]
        public static extern short VB_SUP_GetConfig(ref int lowTempOnOff, ref int autoPower, ref int timeOut);
        
        // Set power management configuration status
        [DllImport(dllPath, EntryPoint = "VB_SUP_SetConfig")]
        public static extern short VB_SUP_SetConfig(ref int lowTempOnOff, ref int autoPower, ref int timeOut);

        //Advanced.Theodolite and measurement calculation
        //Donne �tat du compensateur du TPS
        [DllImport(dllPath, EntryPoint = "VB_TMC_GetInclineSwitch")]
        public static extern short VB_TMC_GetInclineSwitch(ref int swCorr);
        
        //Met sur ON ou OFF le compensateur
        [DllImport(dllPath, EntryPoint = "VB_TMC_SetInclineSwitch")]
        public static extern short VB_TMC_SetInclineSwitch(ref int swCorr);
        
        //Donne �tat correction angulaire TPS
        [DllImport(dllPath, EntryPoint = "VB_TMC_GetAngSwitch")]
        public static extern short VB_TMC_GetAngSwitch(ref TMC_ANG_SWITCH angleswitch);
        
        //Active/d�sactive correction angulaire TPS
        [DllImport(dllPath, EntryPoint = "VB_TMC_SetAngSwitch")]
        public static extern short VB_TMC_SetAngSwitch(ref TMC_ANG_SWITCH angleswitch);
        
        //DOnne constante prisme
        [DllImport(dllPath, EntryPoint = "VB_TMC_GetPrismCorr")]
        public static extern short VB_TMC_GetPrismCorr(ref double prismCorr);
        
        //Change constante prisme
        [DllImport(dllPath, EntryPoint = "VB_TMC_SetPrismCorr")]
        public static extern short VB_TMC_SetPrismCorr(ref double prismCorr);
        
        //Donne les param�tres correction temp�rature actif
        [DllImport(dllPath, EntryPoint = "VB_TMC_GetAtmCorr")]
        public static extern short VB_TMC_GetAtmCorr(ref TMC_ATMOS_TEMPERATURE atmTemperature);
        
        //Change les param�tres correction temp�rature actif
        [DllImport(dllPath, EntryPoint = "VB_TMC_SetAtmCorr")]
        public static extern short VB_TMC_SetAtmCorr(ref TMC_ATMOS_TEMPERATURE atmTemperature);
        
        //Donne les param�tres correction r�fraction actif
        [DllImport(dllPath, EntryPoint = "VB_TMC_GetRefractiveCorr")]
        public static extern short VB_TMC_GetRefractiveCorr(ref TMC_REFRACTION refractParam);
        
        //Change les param�tres correction r�fraction actif !!rentrer tous les param�tres sinon probl�me d'affichage de la distance horizontale en mode GSI. (OFF, rayon Terre 6378000, coeff refraction 0.13)
        [DllImport(dllPath, EntryPoint = "VB_TMC_SetRefractiveCorr")]
        public static extern short VB_TMC_SetRefractiveCorr(ref TMC_REFRACTION refractParam);
        
        //Fait une mesure de distance
        [DllImport(dllPath, EntryPoint = "VB_TMC_DoMeasure")]
        public static extern short VB_TMC_DoMeasure(ref int command, ref int inclineMode);
        
        //Change le mode EDM du TPS
        [DllImport(dllPath, EntryPoint = "VB_TMC_SetEdmMode")]
        public static extern short VB_TMC_SetEdmMode(ref int edmMode);
        
        //Donne le mode EDM du TPS
        [DllImport(dllPath, EntryPoint = "VB_TMC_GetEdmMode")]
        public static extern short VB_TMC_GetEdmMode(ref int edmMode);
        
        //Lit et donne la mesure d'angle H + V et la distance 
        [DllImport(dllPath, EntryPoint = "VB_TMC_GetSimpleMea")]
        public static extern short VB_TMC_GetSimpleMea(ref int waitTime, ref TMC_HZ_V_ANG onlyAngle, ref double slopeDistance, ref int inclineMode);
        
        //Return complete angle measurement 
        [DllImport(dllPath, EntryPoint = "VB_TMC_GetAngle1")]
        public static extern short VB_TMC_GetAngle1(ref TMC_ANGLE angle, ref int inclineMode);


        //Return complete angle measurement 
        [DllImport(dllPath, EntryPoint = "VB_TMC_GetAngle1")]
        public static extern short VB_TMC_GetAngle2(ref TMC_ANGLE2 angle, ref int inclineMode);

        //Orients the theodolite in Hz direction (radians)  
        [DllImport(dllPath, EntryPoint = "VB_TMC_SetOrientation")]
        public static extern short VB_TMC_SetOrientation(ref double hzOrientation);



        // trying to call the c method but no success
        
        //[DllImport(dllPath, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
        //public static extern  short _TMC_GetIncline(int initmode, ref TMC_INCLINE incline);





        //Donne la face active du TPS 
        [DllImport(dllPath, EntryPoint = "VB_TMC_GetFace")]
        public static extern short VB_TMC_GetFace(ref int face);
        [DllImport(dllPath, EntryPoint = "VB_BAP_SetMeasPrg")]
        public static extern short VB_BAP_SetMeasPrg(ref int MeasProg);


        #endregion

    }
}
