﻿using System;
using System.Xml;
using System.Xml.Serialization;
using TSU.Common.Measures;


namespace TSU.Common.Instruments
{
    public interface IInstrument: ITsuObject
    {
        string _Brand { get; set; }
        string _Model { get; set; }
        string _SerialNumber { get; set; }
        InstrumentClasses _InstrumentClass { get; set; }
        InstrumentTypes _InstrumentType { get; set; }
    }

    [XmlInclude(typeof(Sensor))]
    [XmlInclude(typeof(Device.AT40x.Instrument))]
    [XmlInclude(typeof(Reflector.Reflector))]
    [XmlInclude(typeof(LevelingStaff))]
    [XmlType(Namespace = "Instrument")]
    [Serializable]
    public abstract class Instrument: TsuObject, IInstrument
    {


        // Properties
        [XmlAttribute] public virtual string _Brand { get; set; }
        [XmlAttribute] public virtual string _Model { get; set; }
        [XmlAttribute] public virtual string _SerialNumber { get; set; }
        [XmlAttribute] public virtual InstrumentClasses _InstrumentClass { get; set; }
        [XmlAttribute] public virtual InstrumentTypes _InstrumentType { get; set; }
        [XmlIgnore] public double na { get; set; } =TSU.Tsunami2.Preferences.Values.na;
        // Exception
        public class InstrumentUnknownException : Exception
        {
            internal InstrumentUnknownException()
            {
            }
        }

        public override string ToString()
        {
            return this._Name;
        }

    }
}