﻿using System;
using System.Xml;
using System.Xml.Serialization;

using TSU.Common.Measures;
using TSU;
using D = TSU.Common.Instruments.Device;

namespace TSU.Common.Instruments
{
    [XmlInclude(typeof(D.AT40x.Instrument))]

    public abstract  class Tracker : Sensor , IPolarInstrument
    {
        [XmlAttribute]
        public double sigmaInstrCentering { get; set; }
        [XmlAttribute]
        public double sigmaAngl { get; set; }
        [XmlAttribute]
        public double sigmaZenD { get; set; }
        [XmlAttribute]
        public double constAngl { get; set; }
        
        public  Tracker()
        {

        }

        public void DoDistanceCorrections(Measures.MeasureOfDistance measureOfDistance)
        {
            // do nothing : included in the tracker result
        }

        public Result MoveTo()
        {
            throw new NotImplementedException();
        }

        public void Goto()
        {
            throw new NotImplementedException();
        }

        public void ChangeFace()
        {
            throw new NotImplementedException();
        }

        public Result FineAdjust()
        {
            throw new NotImplementedException();
        }
    }
}
