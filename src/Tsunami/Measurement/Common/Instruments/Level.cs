﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using D = TSU.Common.Instruments.Device;

namespace TSU.Common.Instruments
{
    [XmlInclude(typeof(D.Manual.Level.Instrument))]
    [XmlInclude(typeof(D.DNA03.Instrument))]
    [XmlInclude(typeof(D.LS15.Instrument))]
    [Serializable]
    public class Level : Sensor
    {
        public LevelType _levelType { get; set; }

        public EtalonnageParameter.Level EtalonnageParameter { get; set; }

        [XmlAttribute]
        public double _LA1 { get; set; }

        [XmlAttribute]
        public double _LB1 { get; set; }

        [XmlAttribute]
        public double _LA2 { get; set; }

        [XmlAttribute]
        public double _LB2 { get; set; }

        [XmlAttribute]
        public double _ErrorCollim { get; set; }

        [XmlAttribute]
        public double _DistCollim { get; set; }

        [XmlAttribute]
        public bool _IsCollimExported { get; set; }

        [XmlAttribute]
        public DateTime _DateCollim { get; set; }

        public Level()
        {
            this._LA1 = na;
            this._LA2 = na;
            this._LB1 = na;
            this._LB2 = na;
            this._ErrorCollim = na;
            this._DistCollim = na;
            this._IsCollimExported = false;
            this._DateCollim = DateTime.MinValue;
        }

        public Level DeepCopy()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                Level levelCopy = (Level)formatter.Deserialize(stream);
                return levelCopy;
            }
        }

    }
}
