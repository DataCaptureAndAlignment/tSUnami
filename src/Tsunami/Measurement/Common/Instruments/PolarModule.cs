﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using TSU.Common.Compute;
using TSU.Common.Instruments.Device;
using TSU.Common.Instruments.Manager;
using TSU.Preferences;
using TSU.Tools;
using TSU.Views;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using M = TSU.Common.Measures;
using MMMM = TSU;
using P = TSU.Views.Message.ProgressMessage;
using R = TSU.Properties.Resources;

namespace TSU.Common.Instruments
{
    [XmlInclude(typeof(TSU.Common.Instruments.Device.AT40x.Module))]
    [XmlInclude(typeof(TSU.Common.Instruments.Device.TC2002.Module))]
    [XmlInclude(typeof(TSU.Common.Instruments.Device.TDA5005.Module))]
    [XmlInclude(typeof(TSU.Common.Instruments.Device.TCRA1100.Module))]
    [XmlInclude(typeof(TSU.Common.Instruments.Device.TS60.Module))]
    [XmlInclude(typeof(TSU.Common.Instruments.Device.Manual.Theodolite.Module))]

    //TODO : thre is nothing forcing the children polarmodule for example at401module to implemente IPOLARMODULE because it already implemented but the mothe polarmodule.
    // Polar module should containt main methods that calls Ipolarmodule method that are abstract in polar module and forced to be implemented in the children.
    public abstract class PolarModule : Module, IPolarModule
    {
        [XmlIgnore]
        public bool MeteoSet { get; set; }
        [XmlIgnore]
        public double temperature { get; set; } = 20; // °C
        [XmlIgnore]
        public double pressure { get; set; } = 960; // hPA
        [XmlIgnore]
        public double humidity { get; set; } = 60;  // %
        /// <summary>
        /// permet d'éviter que le timer se lance en même temps qu'une mesure
        /// </summary>
        [XmlIgnore]
        public bool IsBusyCommunicating { get; set; } = false;
        [XmlIgnore]
        public virtual bool IsContinuousMeasurementOn
        {
            get
            {
                return false;
            }
        }
        public bool IsFirstMesaureOfFirstFace
        {
            get
            {
                bool isFirstFace = !this.Is2FaceMeasurement;
                bool isFirstMeasureOfTheFace = this._BeingMeasuredOnThisFace.Count == 0;
                return (isFirstFace && isFirstMeasureOfTheFace);
            }
        }
        internal bool TemporaryDistanceAvailableInInstrument { get; set; }

        public PolarModule() { }
        public PolarModule(MMMM.Module parentModule)
            : base(parentModule) { }
        [XmlIgnore]
        public bool OtherFaceWanted;
        [XmlIgnore]
        public bool Is2FaceMeasurement = false;

        [XmlIgnore]
        public bool DistanceWanted = true;

        [XmlIgnore]
        public bool PauseBetweenAnglesAndDistance = true;


        [XmlIgnore]
        public bool AnglesWanted = true;



        public Polar.Measure _ToBeMeasureTheodoliteData
        {
            get
            {
                return toBeMeasuredData as Polar.Measure;
            }
            set
            {
                toBeMeasuredData = value;
            }
        }

        public override M.Measure ToBeMeasuredData
        {
            get
            {
                return toBeMeasuredData;
            }
            set
            {
                Polar.Measure m = value as Polar.Measure;

                if (value != null)
                {
                    CheckIncomingMeasure(m);
                }
                if (value == null)
                {
                    toBeMeasuredData = null;
                    return;
                }


                // done in the checkIncoming measure
                //if (Log != null && this.Instrument != null)
                //{
                //    if (this._InstrumentIsOn)
                //    {
                //        int nDD = (TSU.Tsunami2.TsunamiPreferences.Values.GuiPrefs == null) ? 7 :TSU.Tsunami2.TsunamiPreferences.Values.GuiPrefs.NumberOfDecimalForDistances;
                //        Log.AddEntryAsFYI(this,
                //            this.Instrument._Model + "-"
                //            + this.Instrument._SerialNumber
                //            + R.T331
                //            + m._Point._Name + " With " + m.Distance.Reflector._Name + " on " + m.Extension.ToString(nDD) + "m extension");
                //    }
                //}
            }
        }
        public bool allowAlwaysButtonMeas = true;
        public InstrumentTolerance Tolerance
        {
            get
            {
                if (this.ParentInstrumentManager?.ParentStationModule._Station is Polar.Station ps)
                {
                    return ps.Parameters2.Tolerance;
                }
                if (this.ParentInstrumentManager?.ParentStationModule._Station is Length.Station ls)
                {
                    return ls._Parameters.Tolerance;
                }
                return null;
            }
        }


        public override void Initialize()
        {
            base.Initialize();
            this.MeasureWanted = false;

        }
        public virtual void CheckIncomingMeasure(Polar.Measure m)
        {
            toBeMeasuredData = m;
        }
        [XmlIgnore]
        public Polar.Measure _BeingMeasuredTheodoliteData
        {
            get
            {
                return BeingMeasured as Polar.Measure;
            }
            set
            {
                BeingMeasured = value;
            }
        }
        internal override void OnMeasureToDoReceived()
        {
            base.OnMeasureToDoReceived();
            //if (this._ToBeMeasureTheodoliteData != null)
            //{
            //    this.GotoWanted = this._ToBeMeasureTheodoliteData.GotoWanted;
            //} 

        }
        [XmlIgnore]
        public bool GotoWanted { get; set; }

        public bool N_A_GotoMessageWanted { get; set; } = true;

        public bool MeasureWanted { get; set; }


        [XmlIgnore]
        public bool QuickMeasureOneFaceWanted { get; internal set; }

        [XmlIgnore]
        public bool QuickMeasureTwoFaceWanted { get; internal set; }

        public virtual void Goto()
        {
            Polar.Measure m = _BeingMeasuredTheodoliteData;
            if (m == null) m = _ToBeMeasureTheodoliteData;

            if (m._OriginalPointModifiedInH != null)
                m._OriginalPoint = m._OriginalPointModifiedInH;

            //    if (m == null) throw new Exception("Don't know where to go to;Is a point set up for measurement?");
            if (m == null) throw new Exception($"{R.T_DONT_KNOW_WHERE_TO_GO_TO};{R.T_IS_A_POINT_SET_UP_FOR_MEASUREMENT}");
            if (Moving != null) Moving(this, new M.MeasurementEventArgs(this));
        }

        internal void GetAngleForGoodFaceGoto(Polar.Measure m, out E.Angles angleForGoto)
        {

            angleForGoto = m.Angles.Corrected.Clone() as E.Angles;

            if (angleForGoto.IsNa)
                throw new Exception("No data avaialble to do a 'goto'");
            FaceType faceFromVerticalAngleValue = (m.Angles.Corrected.Vertical.Value > 200) ? FaceType.Face2 : FaceType.Face1;
            FaceType toCompareto = (m.Face == FaceType.DoubleFace) ? this.GetFace() : m.Face;
            if (faceFromVerticalAngleValue != toCompareto) // value are for the not correct circle
                Survey.TransformToOppositeFace(angleForGoto);

        }

        public virtual FaceType GetFace() { return FaceType.UnknownFace; }
        [XmlIgnore]
        internal bool WaitingForMeasure;
        [XmlIgnore]
        internal bool denyMeasurement;
        [XmlIgnore]
        internal bool reMeasure;

        public event EventHandler InstrumentReady;
        internal virtual void Ready()
        {
            if (InstrumentReady != null)
            {
                MMMM.Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(() =>
                {
                    InstrumentReady(this, new EventArgs());
                });
            }
        }
        public override void PrepareMeasure()
        {
            base.PrepareMeasure();
            NumberOfMeasureOnThisFace = _ToBeMeasureTheodoliteData.NumberOfMeasureToAverage;

            _BeingMeasuredTheodoliteData.isQuickMeasure = _BeingMeasuredTheodoliteData._Status.Type == M.States.Types.Control ? false :
                   (this.QuickMeasureOneFaceWanted || this.QuickMeasureTwoFaceWanted);

            if (_BeingMeasuredTheodoliteData._OriginalPointModifiedInH != null)
                _BeingMeasuredTheodoliteData._OriginalPoint = _BeingMeasuredTheodoliteData._OriginalPointModifiedInH;

            // Check if doubleface needed: recently added here because if face was changed in the new message with the measuremetnt Node it was taken not  into account
            OtherFaceWanted = (_BeingMeasuredTheodoliteData.Face == FaceType.DoubleFace) ? true : false;
        }
        public override Result Measure()
        {
            try
            {
                if (!this.IsReadyToMeasure()) throw new Exception("Instrument NOT ready");

                PrepareMeasure();

                Task ignoredAwaitableResult = ApplyDelay();
                
                MeasureFirstFace();

                if (denyMeasurement) this.Ready();
                return new Result()
                {
                    Success = !denyMeasurement,
                    AccessToken = denyMeasurement ? "Measure didn't started" : "Measure started"
                };
            }

            catch (Exception ex)
            {
                throw MeasureExceptionTreatment(ex);
            }
        }

        private async Task ApplyDelay()
        {
            int delayInSec = 5;// this._BeingMeasuredTheodoliteData.Delay;
            if (delayInSec != 0)
            {
                await Task.Delay(delayInSec * 1000);
            }
        }

        internal override void HaveFailed()
        {
            this.IsBusyCommunicating = false;
            base.HaveFailed();
        }

        protected Exception MeasureExceptionTreatment(Exception ex)
        {
            if (ex is CancelException)
            {
                this.State = new InstrumentState(InstrumentState.type.Idle);
                return ex;
            }
            else
            {
                string message = "Impossible to Measure: " + ex.Message;
                this.State = new InstrumentState(InstrumentState.type.Idle);
                Logs.Log.AddEntryAsError(this, message);
                return new Exception(message, ex);
            }
        }

        // return true if the user is prommpted with a starting message and choose to cancel.
        public bool CancelByMessageBeforeMeasure()
        {
            var dsaFlags = this.FinalModule?.DsaFlags;
            DsaFlag HideMessageWithReflectorAndExtensionBeforeMeasurement = DsaFlag.GetByNameOrAdd(dsaFlags, "HideMessageWithReflectorAndExtensionBeforeMeasurement");
            if (HideMessageWithReflectorAndExtensionBeforeMeasurement.State == DsaOptions.Never)
                return false;

            //return ShowMeasureConfirmationMessage(ShowReflectorAndExtensionBeforeMeasurement);

            return ShowMeasureConfirmationMessage2(HideMessageWithReflectorAndExtensionBeforeMeasurement);

        }

        [Obsolete]
        private bool ShowMeasureConfirmationMessage(DsaFlag ShowReflectorAndExtensionBeforeMeasurement)
        {
            // build message
            Polar.Measure mt = _ToBeMeasureTheodoliteData;
            int d = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances - 3;
            if (d < 0) d = 0;
            string cancel = R.T_CANCEL;
            string ext = (mt.Extension.Value * 1000).ToString("F" + d) + " mm";
            string m = $"{R.T_READY_TO_MEASURE};" +
                $"{mt._Point._Name}\r\n" +
                $"On $BOLD${mt.Distance.Reflector._Name}\r\n" +
                $"$REGULAR$ With an Extension of $BOLD${ext}$REGULAR$";
            if (mt._OriginalPoint.SocketCode != null)
            {
                if (mt._OriginalPoint.SocketCode.Id != "0")
                {
                    if (mt._OriginalPoint.SocketCode.DefaultExtensionForPolarMeasurement == mt.Extension.Value)
                        m += $" (Code {mt._OriginalPoint.SocketCode.Id})";
                    else
                        m += $" (Given by user)";
                }
            }

            MessageInput mi = new MessageInput(MessageType.Choice, m)
            {
                ButtonTexts = CreationHelper.GetOKCancelButtons(),
                DontShowAgain = ShowReflectorAndExtensionBeforeMeasurement,
                Sender = this._Name
            };
            return mi.Show().TextOfButtonClicked == R.T_CANCEL;
        }

        private bool ShowMeasureConfirmationMessage2(DsaFlag HideMessageWithReflectorAndExtensionBeforeMeasurement)
        {
            string m = $"{R.T_READY_TO_MEASURE} with the following settings?;You can change values in the treeview";

            Polar.Station.Module stm = (this.FinalModule?._ActiveStationModule as Polar.Station.Module);
            //var measureAsStoreInTheStationModule = stm.MeasureToCome; // not always working if we sen measure to instrument without cahnging the order in the MEasureTodo, such like in GMs
            var measureAsStoreInTheStationModule = stm.LastMeasureSendToInstrument;

            // should be show the prism selected by the AT?
            Reflector.Reflector at40xReflector = null;
            if (this is Device.AT40x.Module at)
                at40xReflector = at.ActualReflector;

            ttv = new TsuTreeView();
            ttv.Height = ttv.ItemHeight * 10;
            ttv.Width = 900;
            ttv.ImageList = TreeNodeImages.ImageListInstance;


            var measure = this.ToBeMeasuredData as Polar.Measure;
            if (at40xReflector != null && measure.Reflector._Name.Contains(R.T_MANAGED_BY_AT40X))
            {
                measure.Reflector = at40xReflector;
            }
            ShowNode(ttv, measureAsStoreInTheStationModule);

            EventHandler<ModuleEventArgs> OnNextPointListUpdated = (source, args) =>
            {
                this.View.WaitingForm.Pause();
                this.ToBeMeasuredData = measureAsStoreInTheStationModule.Clone() as Polar.Measure;
                this.PrepareMeasure();
                this.ChangeFaceAndDoGoto();

                ShowNode(ttv, measureAsStoreInTheStationModule);
                
                this.View.WaitingForm.Resume();
            };

            stm.View.NextPointListUpdated += OnNextPointListUpdated;

            ttv.NodeMouseClick += (sender, e) =>
            {
                // stm.View.usedMeasure = stm.MeasureToCome; // useless because the usedmeasure is set to the TAG of the Clicked Node
                stm.View.treeview1_OnClick(sender, e);
            };

            MessageInput mi = new MessageInput(MessageType.Choice, m)
            {
                ButtonTexts = CreationHelper.GetOKCancelButtons(),
                Controls = new List<Control> { ttv },
                DontShowAgain = HideMessageWithReflectorAndExtensionBeforeMeasurement,
                Sender = this._Name
            };
            using (MessageResult results = mi.Show())
            {
                stm.View.NextPointListUpdated -= OnNextPointListUpdated;
                return results.TextOfButtonClicked == R.T_CANCEL;
            }
        }

        private TsuTreeView ttv;

        private void ShowNode(TsuTreeView ttw, Polar.Measure measure)
        {
            Func<Polar.Measure, TsuNode> GetNode = (measure2) =>
            {
                TsuNode nextPointsNode = new TsuNode();
                nextPointsNode.DetailsType = TsuNode.NodeDetailsType.Special; // this make the list to appear same as the station next point tree
                nextPointsNode.BasedOn(new List<Common.Measures.Measure>() { measure2 });
                return nextPointsNode.Nodes[0] as TsuNode;
            };
            ttw.Nodes.Clear();
            ttw.Nodes.Add(GetNode(measure));
            ttw.ExpandAll();
        }

        [XmlIgnore]
        public int NumberOfMeasureOnThisFace = 1;
        [XmlIgnore]
        internal Polar.Measure _AlreadyMeasuredFace;
        [XmlIgnore]
        internal List<Polar.Measure> _BeingMeasuredOnThisFace;
        public void MeasureFirstFace()
        {
            try
            {
                this.timeSpentMeasuring.Reset();
                if (_BeingMeasuredTheodoliteData == null) throw new Exception($"{R.T_TSUNAMI_IS_NOT_SURE_WHAT_TO_MEASURE};{R.T_IN_POLAR_MODULE_BRING_BACK_SOMETHING_AT_THE_TOP_OF_THE_NEXT_POINT_LIST_IN_GUIDED_MODULE_TRY_TO_CAHNGE_STEP_AND_COME_BACK}");
                NumberOfMeasureOnThisFace = _BeingMeasuredTheodoliteData.NumberOfMeasureToAverage;

                if (_BeingMeasuredTheodoliteData._Status.Type != M.States.Types.Control && (QuickMeasureOneFaceWanted || QuickMeasureTwoFaceWanted))
                {
                    _BeingMeasuredTheodoliteData.NumberOfMeasureToAverage = 1;
                    _BeingMeasuredTheodoliteData.CommentFromTsunami = R.T_QUICK_MEASURE;

                    this.SetMeasureModeToQuick();
                }
                else
                    this.SetMeasureModeToNormal();

                BeingMeasuredUnModify = beingMeasured.Clone() as M.Measure;

                if (!this.IsContinuousMeasurementOn)
                    this.DeclareSubMeasurementStart();

                // Check if doubleface needed
                OtherFaceWanted = (_BeingMeasuredTheodoliteData.Face == FaceType.DoubleFace) ? true : false;
                //this.GetFace()

                this._BeingMeasuredOnThisFace = new List<Polar.Measure>();
                this._AlreadyMeasuredFace = null;
                if (!this.IsContinuousMeasurementOn)
                    this.StartMeasurement(); // to start the event
                Is2FaceMeasurement = false;
                MeasureLikeThis();
            }
            catch (Exception ex)
            {
                denyMeasurement = true;
                new MessageInput(MessageType.Critical, ex.Message) { AsNotification = false }.Show();
            }

        }

        internal virtual void SetMeasureModeToNormal()
        {
            // overrided
        }

        internal virtual void SetMeasureModeToQuick()
        {
            if (QuickMeasureOneFaceWanted) _BeingMeasuredTheodoliteData.Face = FaceType.Face1;
            if (QuickMeasureTwoFaceWanted) _BeingMeasuredTheodoliteData.Face = FaceType.DoubleFace;
        }

        private void DeclareSubMeasurementStart()
        {
            if (this.SubMeasurementStarted != null)
            {
                MMMM.Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(() =>
                {
                    this.SubMeasurementStarted(this, new M.MeasurementEventArgs(this));
                });
            }
        }



        internal void MeasureOtherFace()
        {
            this.Is2FaceMeasurement = true;
            this.OtherFaceWanted = false;
            NumberOfMeasureOnThisFace = _BeingMeasuredTheodoliteData.NumberOfMeasureToAverage;
            _BeingMeasuredTheodoliteData = _BeingMeasuredTheodoliteData.Clone() as Polar.Measure;


            this.DeclareSubMeasurementStart();
            this.ChooseDistanceCorrectionWanted(_BeingMeasuredTheodoliteData.Distance);
            this._BeingMeasuredOnThisFace = new List<Polar.Measure>();

            switch (_BeingMeasuredTheodoliteData.Face)
            {
                case FaceType.Face1:
                    _BeingMeasuredTheodoliteData.Face = FaceType.Face2;
                    break;
                case FaceType.Face2:
                    _BeingMeasuredTheodoliteData.Face = FaceType.Face1;
                    break;
                case FaceType.UnknownFace:
                case FaceType.DoubleFace:
                case FaceType.Face1Reducted:
                case FaceType.DoubleFaceReducted:
                default: break;
            }
            MeasureLikeThis();
        }
        /// <summary>
        /// Indique si la distance doit être corrigée de la météo et de l'étalonnage
        /// la méthode peut-être être overridée dans chaque type de polarmodule
        /// </summary>
        /// <param name="distance"></param>
        internal virtual void ChooseDistanceCorrectionWanted(M.MeasureOfDistance distance)
        {
            distance.IsCorrectedForEtalonnage = false;
            distance.IsCorrectedForPrismConstanteValue = false;
            distance.IsCorrectedForWeatherConditions = false;
            distance.IsDistanceOutOfRange = false;
        }
        internal void DoMeasureCorrections(Polar.Measure m)
        {
            Survey.DoCollimationCorrection(m.Angles);
            if (this.StartingMeasureCorrection != null) StartingMeasureCorrection(this, new M.MeasurementEventArgs(m));
            try
            {
                // Correct distance

                if (DistanceWanted)
                {
                    if (this.Instrument is TotalStation ts)
                    {
                        ts.DoDistanceCorrections(m.Distance);
                        if (m.Distance.IsDistanceOutOfRange)
                        {
                            this.InvokeOnApplicationDispatcher(MsgDistOutOfCalibration);
                            if (this.tempMsg == R.T_BAD) 
                                m._Status = new M.States.Bad();
                            else if (this.tempMsg == R.T_QUESTIONNABLE) 
                                m._Status = new M.States.Questionnable();
                            m.CommentFromTsunami += "Out of calibration range";
                        }
                    }
                }

                if (this.MeasureCorrected != null) MeasureCorrected(this, new M.MeasurementEventArgs(m));
            }
            catch (Exception ex)
            {
                throw new Exception("Distance Correction Failed;" + ex.Message, ex);
            }
        }

        private void MsgDistOutOfCalibration()
        {
            MessageInput mi = new MessageInput(MessageType.Warning, R.StringPolar_DistOutOfCalibration)
            {
                ButtonTexts = new List<string> { R.T_QUESTIONNABLE, R.T_BAD }
            };
            this.tempMsg = mi.Show().TextOfButtonClicked;
        }

        internal virtual bool Lock()
        {
            this.LockingForTarget?.Invoke(this, new M.MeasurementEventArgs(this));
            return true;
        }

        public virtual void TurnOnCompensator()
        {

        }

        public override string ToString()
        {
            string pointmeasured = (this.ToBeMeasuredData != null) ? this.ToBeMeasuredData._PointName : "nothing";
            return $"{base.ToString()} measuring: {pointmeasured}";
        }

        public virtual void TurnOffCompensator()
        {

        }

        public virtual void MeasureLikeThis()
        {
            this.State = new InstrumentState(InstrumentState.type.Measuring);
            Logs.Log.AddEntryAsBeginningOf(this, "Measuring...");
        }

        public virtual void DoSmallMoveBeforeMeasure()
        {
            // nothing by default, must be overrided if you want to force your instrument to move before measurement to insure a new reading of encoders and camera i.e. AT40x
        }

        public virtual void MeasureNextSubMeasure()
        {
            BeingMeasured = BeingMeasured.Clone() as Polar.Measure;

            Tsunami2.Properties?.UpdateStatus("DoSmallMoveBeforeMeasure()");

            if (TSU.Tsunami2.Preferences.Values.GuiPrefs.MoveBetweenSameFaceMeasurements.IsTrue)
                this.DoSmallMoveBeforeMeasure();


            Tsunami2.Properties?.UpdateStatus("ChooseDistanceCorrectionWanted()");

            this.ChooseDistanceCorrectionWanted((BeingMeasured as Polar.Measure).Distance);


            Tsunami2.Properties?.UpdateStatus("DeclareSubMeasurementStart()");

            this.DeclareSubMeasurementStart();


            Tsunami2.Properties?.UpdateStatus("MeasureLikeThis()");
            MeasureLikeThis();
        }

        /// <summary>
        /// return true if a wrong point is detected
        /// </summary>
        /// <returns></returns>
        private bool DetectWrongPoint()
        {
            if (!IsFirstMesaureOfFirstFace
             || toBeMeasuredData is null
             || BeingMeasured._Status is M.States.Cancel
             || toBeMeasuredData._Status is M.States.Questionnable
             || toBeMeasuredData._Status is M.States.Cancel)
                return false;

            double DISTANCE_TOLERANCE_IN_2D = Tsunami2.Preferences.Values.Tolerances.Distance_Horizontal_For_Wrong_Point_mm / 1000; 
            double DISTANCE_TOLERANCE_IN_Z = Tsunami2.Preferences.Values.Tolerances.Distance_Vertical_For_Wrong_Point_mm / 1000; 


            Polar.Measure m = BeingMeasured as Polar.Measure;

            // if no distance we cannot check
            if (m.Distance.Corrected.IsNa)
                return false;

            // known point or quit
            E.Point p = m._OriginalPoint;
            if (p == null
             || p._Coordinates == null
             || !p._Coordinates.HasAny
             || m.SendingStationModule == null)
                return false;

            // get station position or quit
            Polar.Station.Parameters param = (m.SendingStationModule._Station as Polar.Station).Parameters2;
            E.Point sp = param._StationPoint;
            if (sp == null) return false;
            if (!sp._Coordinates.HasAny) return false;
            if (param.Setups.InitialValues.VerticalisationState == Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation.NotVerticalised)
            {
                //TODO: determine how to compute the distance (compare the 3D distances station-ptTheo vs station-ptMeasured ?)
                return false;
            }

            // get measured 2D+1

            double m2D = Math.Abs(Survey.GetHorizontalDistance(m.Angles.Corrected.Vertical.Value, m.Distance.Corrected.Value));
            double mZ = 0;
            bool ihKnown;
            try
            {
                ihKnown = (param._InstrumentHeight != null);
            }
            catch (Exception)
            {
                ihKnown = false;
            }

            if (ihKnown)
                mZ = Math.Abs(Survey.GetVerticalDistance(m.Angles.Corrected.Vertical.Value,
                                                m.Distance.Corrected.Value,
                                                param._InstrumentHeight.Value,
                                                m.Extension.Value));

            // get theo 2D+1
            try
            {
                double t2D = Math.Abs(Survey.GetHorizontalDistance(sp, p));
                double tZ;
                if (sp._Coordinates.HasCcs)
                    tZ = p._Coordinates.Ccs.Z.Value - sp._Coordinates.Ccs.Z.Value;
                else
                    tZ = p._Coordinates.Local.Z.Value - sp._Coordinates.Local.Z.Value;
                tZ = Math.Abs(tZ);

                // Compare measured and theo
                double diff2D = m2D - t2D;
                double diffZ = mZ - tZ;
                bool outOfTol2D = (Math.Abs(diff2D) > DISTANCE_TOLERANCE_IN_2D);
                bool outOfTolZ = (Math.Abs(diffZ) > DISTANCE_TOLERANCE_IN_Z);
                if (!ihKnown)
                    outOfTolZ = false;

                DsaFlag ShowWarningOfDifferenceInHorizontalDistance = DsaFlag.GetByNameOrAdd(this.FinalModule.DsaFlags, "ShowWarningOfDifferenceInHorizontalDistance");

                if (outOfTol2D || outOfTolZ)
                {
                    string message = $"{R.T_BIG_DIFFERENCES_IN_DISTANCES} (station <-> {R.T_POINT});{p.FullNameWithoutZone}?\r\n{R.T_THE_DIFFERENCE_BETWEEN_THE_MEASURED_AND_THE_THEORETICAL_DISTANCES_ARE}: \r\n";
                    if (outOfTol2D)
                        message += $"\t{R.T_HORIZONTAL} (2D):\t({m2D:F3}) - ({t2D:F3})) = {diff2D:F3} m.\r\n";
                    if (outOfTolZ)
                        message += $"\t{R.T_VERTICAL} (Z/H):\t({mZ:F3}) - ({tZ:F3}) = {diffZ:F3} m.\r\n";
                    message += $"\r\n{R.T_DO_YOU_WANT_TO} {R.T_CONTINUE}";

                    return new MessageInput(MessageType.Critical, message)
                    {
                        ButtonTexts = new List<string> { R.T_CONTINUE, R.T_CANCEL },
                        DontShowAgain = ShowWarningOfDifferenceInHorizontalDistance
                    }.Show().TextOfButtonClicked == R.T_CANCEL;
                }
                return false;
            }
            catch
            {
                return false;
            }

        }

        internal virtual void ChangeVerticalisationState(Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation verticalisationState)
        {
            // to be be overrided
        }

        /// <summary>
        /// Decide what is the net step based on number of face and measuremsnt per face
        /// </summary>
        /// <param name="m"></param>
        internal void DoMeasureCorrectionsThenNextMeasurementStep(Polar.Measure m)
        {

            try
            {
                // Correct distances based on etalonnage
                this.timeSpentMeasuring.Add("DoMeasureCorrections", TimeSpent.type.Started, DateTime.Now);
                DoMeasureCorrections(m);
                this.timeSpentMeasuring.Add("DoMeasureCorrections", TimeSpent.type.Stopped, DateTime.Now);

                // Check is good point

                this.timeSpentMeasuring.Add("DetectWrongPoint", TimeSpent.type.Started, DateTime.Now);
                if (DetectWrongPoint())
                    BeingMeasured._Status = new M.States.Cancel();
                this.timeSpentMeasuring.Add("DetectWrongPoint", TimeSpent.type.Stopped, DateTime.Now);

                // cancel?
                if (DeclareCancelledMeasurement()) return;

                denyMeasurement = false;
                this.State = new InstrumentState(InstrumentState.type.Idle);

                // Check if it is time to average the measurement of to continue measure with this face

                
                if (!DoAverageThisFaceMeasurement(ref m)) return;

                // Deny based on 'denyMeasurement'?
                Polar.Measure denied;
                if (DeclareDenialMeasurement())
                {
                    if (reMeasure)
                        this.ReMeasure();
                    return;
                }

                // Measure with other face
                if (DidMeasureOtherFace(m))
                    return;
                else
                    if (!DoAverageOftheTwoFaces(ref m))
                    return;

                // Deny based on 'denyMeasurement'?
                if (DeclareDenialMeasurement())
                {
                    if (reMeasure)
                        this.ReMeasure();
                    return;
                }

                WaitingForMeasure = false;


                this.timeSpentMeasuring.Add("ValidateMeasurement", TimeSpent.type.Started, DateTime.Now);
                this.ValidateMeasurement(m);
                this.timeSpentMeasuring.Add("ValidateMeasurement", TimeSpent.type.Stopped, DateTime.Now);
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal void ReMeasure()
        {
            // must reset the last measure (beingMeasured), because the next points list could have been changed and send a new point to be measured (ToBeMeasureData).
            this.ParentInstrumentManager.SetNextMeasure(BeingMeasuredUnModify as Polar.Measure);

            MMMM.Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(this.View.MeasureAsync);
        }

        public bool DoAverageOftheTwoFaces(ref Polar.Measure m)
        {
            if (this._AlreadyMeasuredFace != null) // antoher face was measure, we have to average and store the 3 values
            {
                Polar.Measure averageOfCircles;
                Polar.Measure diff;
                string message;
                bool outOfTol;
                bool completelyOut = false;
                if (this._AlreadyMeasuredFace.Face == FaceType.Face1 && m.Face == FaceType.Face2)
                {
                    averageOfCircles = Survey.DoFaceAverage(this._AlreadyMeasuredFace, m, Tolerance.Opposite_Face.H_CC, Tolerance.Opposite_Face.V_CC, Tolerance.Opposite_Face.D_mm, out diff, out outOfTol, out message, ref completelyOut);
                    averageOfCircles.AnglesF1 = this._AlreadyMeasuredFace.Angles;
                    averageOfCircles.DistanceF1 = this._AlreadyMeasuredFace.Distance;
                    averageOfCircles.AnglesF2 = m.Angles;
                    averageOfCircles.DistanceF2 = m.Distance;
                }
                else
                    if (this._AlreadyMeasuredFace.Face == FaceType.Face2 && m.Face == FaceType.Face1)
                {
                    averageOfCircles = Survey.DoFaceAverage(m, this._AlreadyMeasuredFace, Tolerance.Opposite_Face.H_CC, Tolerance.Opposite_Face.V_CC, Tolerance.Opposite_Face.D_mm, out diff, out outOfTol, out message, ref completelyOut);
                    averageOfCircles.AnglesF1 = m.Angles;
                    averageOfCircles.DistanceF1 = m.Distance;
                    averageOfCircles.AnglesF2 = this._AlreadyMeasuredFace.Angles;
                    averageOfCircles.DistanceF2 = this._AlreadyMeasuredFace.Distance;
                }
                else
                    //  throw new Exception("Trying to average F1 and F2 but problem of consistance");
                    throw new Exception(R.T_TRYING_TO_AVERAGE_F1_AND_F2_BUT_PROBLEM_OF_CONSISTANCE);
                m = averageOfCircles;

                if (outOfTol)
                {
                    this.tempMsg = message;
                    this.tempMsgPriority = completelyOut;
                    this.InvokeOnApplicationDispatcher(MsgOutOfTolFaces);

                }
            }
            return true;
        }

        private void MsgOutOfTolFaces()
        {
            string message = $"{R.StringPolar_OutOfTolFace1and2} ({BeingMeasured._Point._Name}).\r\n" + this.tempMsg + "\r\n";

            // case of sequencial measuremetn, we dont want to be stopped by message
            if (this._ToBeMeasureTheodoliteData._Status is M.States.Good)
                return;

            string valid = R.T_VALID;
            string deny = R.T_DENY;
            string retry = QuickFixHideRetryIfGuidedControl() ? "" : R.T_RETRY;
            string r;
            if (this.tempMsgPriority)
            {
                string titleAndMessage = $"{R.T_OUTOFTOL} ({BeingMeasured._Point._Name});{message} {R.T_OutOfTolChoice}";
                MessageInput mi = new MessageInput(MessageType.Critical, titleAndMessage)
                {
                    ButtonTexts = new List<string> { retry, valid, deny }
                };
                r = mi.Show().TextOfButtonClicked;
            }
            else
            {
                string titleAndMessage = $"{R.T_OUTOFTOL} ({BeingMeasured._Point._Name});{message} {R.T_OutOfTolChoice}";
                MessageInput mi = new MessageInput(MessageType.Warning, titleAndMessage)
                {
                    ButtonTexts = new List<string> { retry, valid, deny }
                };
                r = mi.Show().TextOfButtonClicked;
            }

            denyMeasurement = (r == deny) || (r == retry);
            reMeasure = (r == retry);

            this.tempMsg = r;
        }

        /// <summary>
        /// return true if rety option should be hidden
        /// </summary>
        /// <returns></returns>
        private bool QuickFixHideRetryIfGuidedControl()
        {
            if (this.beingMeasured != null)
            {
                if (this.beingMeasured._Status is M.States.Control)
                {
                    if (this.ParentInstrumentManager.ParentStationModule.FinalModule is Guided.Module)
                        return true;
                    if (this.ParentInstrumentManager.ParentStationModule.FinalModule is Guided.Group.Module)
                        return true;
                }
            }
            return false;
        }

        private bool DidMeasureOtherFace(Polar.Measure m)
        {
            if (this.OtherFaceWanted)
            {

                // StatusChange(string.Format("First face measurement done"), P.stepType.End);
                StatusChange($"{R.T_FIRST_FACE_MEASUREMENT_DONE}", P.StepTypes.End);
                this._AlreadyMeasuredFace = m;
                MeasureOtherFace();
                return true;
            }
            return false;
        }

        private bool DeclareDenialMeasurement()
        {
            if (denyMeasurement)
            {
                //  StatusChange(string.Format("Denied"), P.stepType.EndAll);
                StatusChange($"{R.T_DENIED}", P.StepTypes.EndAll);
                this.LastMessage = "";
                HaveFailed();
                if (this is Device.Manual.Theodolite.Module) PrepareMeasure();
                return true;
            }
            return false;
        }


        private bool DeclareCancelledMeasurement()
        {
            if (this.BeingMeasured._Status is M.States.Cancel)
            {
                this.State = new InstrumentState(InstrumentState.type.Idle);
                this.SendMessage(this.BeingMeasured);
                StatusChange($"{R.T_DENIED}", P.StepTypes.EndAll);
                this.LastMessage = "";
                HaveFailed();
                if (this is Device.Manual.Theodolite.Module) PrepareMeasure();
                return true;
            }
            return false;
        }

        private bool DoAverageThisFaceMeasurement(ref Polar.Measure m)
        {
            if (_BeingMeasuredTheodoliteData.NumberOfMeasureToAverage > 1)
            {
                NumberOfMeasureOnThisFace -= 1;
                this._BeingMeasuredOnThisFace.Add(m);
                if (NumberOfMeasureOnThisFace > 0)
                {
                    //  StatusChange(string.Format("Sub measurement done"), P.stepType.End);
                    StatusChange($"{R.T_SUB_MEASUREMENT_DONE}", P.StepTypes.End);
                    MeasureNextSubMeasure();
                    return false;
                }
                else
                {
                    this.timeSpentMeasuring.Add("DoAverageThisFaceMeasurement", TimeSpent.type.Started, DateTime.Now);
                    m = Survey.DoAverage(this._BeingMeasuredOnThisFace);
                    this.tempMsg = "";
                    reMeasure = false;
                    this.timeSpentMeasuring.Add("DoAverageThisFaceMeasurement", TimeSpent.type.Stopped, DateTime.Now);
                    if (!Analysis.Theodolite.CheckTolerances(
                        this._BeingMeasuredOnThisFace, m,
                        this.Tolerance.Same_Face.H_CC, this.Tolerance.Same_Face.V_CC, this.Tolerance.Same_Face.D_mm, out this.tempMsg)) // 0 for angle to force use of dist
                    {
                        this.InvokeOnApplicationDispatcher(MsgOutOfToleranceSameFace);
                    }
                }
            }
            return true;
        }



        private void MsgOutOfToleranceSameFace()
        {
            string message;

            if (this._ToBeMeasureTheodoliteData._Status is M.States.Good)
                return;

            message = $"{R.StringPolar_OutOfTolSameFace} ({this._ToBeMeasureTheodoliteData._Point._Name});{R.StringPolar_OutOfTolSameFace_desc} ({_BeingMeasuredTheodoliteData.Face})\r\n{this.tempMsg}{R.T_OutOfTolChoice}";
            string valid = R.T_VALID;
            string deny = R.T_DENY;
            string retry = QuickFixHideRetryIfGuidedControl() ? "" : R.T_RETRY;
            MessageInput mi = new MessageInput(MessageType.Warning, message)
            {
                ButtonTexts = new List<string> { retry, valid, deny }
            };
            string r = mi.Show().TextOfButtonClicked;
            denyMeasurement = (r == deny) || (r == retry);
            reMeasure = (r == retry);
        }


        internal void ChangeFaceAndDoGoto()
        {
            if (!this.IsContinuousMeasurementOn)
                StatusChange($"{R.T_CHANGE_FACE_AND_TRY_TO_MOVE_TO_POINT} {_BeingMeasuredTheodoliteData.Face.ToString()}", P.StepTypes.Begin);
            FaceType currentFace = GetFace();

            // Check if doubleface needed
            if (_BeingMeasuredTheodoliteData.Face == FaceType.UnknownFace) throw new Exception(R.T_INSTRUMENT_FACE_UNKNOWN);
            if (_BeingMeasuredTheodoliteData.Face == FaceType.DoubleFace) _BeingMeasuredTheodoliteData.Face = currentFace;

            // Change Face or not
            bool shouldChangeFace = _BeingMeasuredTheodoliteData.Face != currentFace;

            if (_BeingMeasuredTheodoliteData.HasCorrectedAngles && this.GotoWanted)
            {
                this.Goto();
            }
            else
            {
                DsaFlag MeasureEvenIfGotoWasNotPossible = DsaFlag.GetByNameOrAdd(this.FinalModule?.DsaFlags, "MeasureEvenIfGotoWasNotPossible");
                if ((this.GotoWanted && this.N_A_GotoMessageWanted) && MeasureEvenIfGotoWasNotPossible.State != DsaOptions.Always)
                {
                    string titleAndMessage1 = $"{R.T_GOTO_IMPOSSIBLE};{R.T_NO_AVAILBABLE_ANGLES_FOR_THE_POINT} '{_BeingMeasuredTheodoliteData._Point._Name}'";
                    new MessageInput(MessageType.Warning, titleAndMessage1).Show();

                    var flags = ((Polar.Station.Module)this.ParentModule.ParentModule).DsaFlags;

                    if (MeasureEvenIfGotoWasNotPossible.State == DsaOptions.Never)
                        throw new Exception(R.T_ACQUISITION_CANCELLED);

                    string titleAndMessage = $"{R.T_MEASURE_ANYWAY};{R.T_THE_GOTO_WAS_IMPOSSIBLE_DO_YOU_WANT_TO_MEASURE} '{_BeingMeasuredTheodoliteData._Point._Name}' {R.T_IN_THIS_INSTRUMENT_POSITION}";
                    if (new MessageInput(MessageType.Choice, titleAndMessage)
                    {
                        ButtonTexts = CreationHelper.GetYesNoButtons(),
                        DontShowAgain = MeasureEvenIfGotoWasNotPossible
                    }.Show().TextOfButtonClicked == R.T_NO)

                        throw new Exception(R.T_ACQUISITION_CANCELLED);
                }

                if (shouldChangeFace)
                    ChangeFace();

                if (!this.IsContinuousMeasurementOn)
                    StatusChange(string.Format("Good Face: {0}", _BeingMeasuredTheodoliteData.Face.ToString()), P.StepTypes.End);
            }
        }

        internal override void SubscribeLogToEvents()
        {
            base.SubscribeLogToEvents(); // this will unsubscribe all log beforre t o majke sure there arae not subscribe twice.

            this.ChangingFace += Logs.Log.OnInstrumentChangingFace;
            this.LockingForTarget += Logs.Log.OnInstrumentLocking;
            this.MeasureAcquiring += Logs.Log.OnInstrumentAcquiring;
            this.Moving += Logs.Log.OnInstrumentMoving;
            this.SubMeasurementStarted += Logs.Log.OnSubMeasurementStarted;
            this.StartingMeasureCorrection += Logs.Log.OnStartingMeasureCorrection;
            this.MeasureCorrected += Logs.Log.OnMeasureCorrected;
        }

        internal override void UnSubscribeLogToEvents()
        {
            this.ChangingFace -= Logs.Log.OnInstrumentChangingFace;
            this.LockingForTarget -= Logs.Log.OnInstrumentLocking;
            this.MeasureAcquiring -= Logs.Log.OnInstrumentAcquiring;
            this.Moving -= Logs.Log.OnInstrumentMoving;
            this.SubMeasurementStarted -= Logs.Log.OnSubMeasurementStarted;
            this.StartingMeasureCorrection -= Logs.Log.OnStartingMeasureCorrection;
            this.MeasureCorrected -= Logs.Log.OnMeasureCorrected;
        }

        public event M.MeasurementEventHandler ChangingFace;
        public event M.MeasurementEventHandler LockingForTarget;
        public event M.MeasurementEventHandler Moving;
        public event M.MeasurementEventHandler SubMeasurementStarted;
        public event M.MeasurementEventHandler MeasureAcquiring;
        public event M.MeasurementEventHandler StartingMeasureCorrection;
        public event M.MeasurementEventHandler MeasureCorrected;

        public void InvokeMeasureAcquiring()
        {
            if (this.MeasureAcquiring != null) this.MeasureAcquiring(this, new M.MeasurementEventArgs(this));
        }
        public virtual void ChangeFace()
        {
            if (this.ChangingFace != null) this.ChangingFace(this, new M.MeasurementEventArgs());
        }

        internal void SetMeteoManually()
        {
            string cancel = R.T_CANCEL;
            string respond;

            // temp
            double temperature = this.temperature;
            if (this.View.GeNumericaltValueFromUser(R.StringPolar_EnterTemperature, out temperature, temperature, -30, 60))
                this.temperature = temperature;
            else
                return;

            // pressure
            double pressure = this.pressure;
            if (this.View.GeNumericaltValueFromUser(R.StringPolar_EnterPressure, out pressure, pressure, 500, 1500))
                this.pressure = pressure;
            else
                return;

            // humidity
            double humidity = this.humidity;
            if (this.View.GeNumericaltValueFromUser(R.StringPolar_EnterHumidity, out humidity, humidity, 0, 100))
                this.humidity = humidity;
            else
                return;

            //while (!Int32.TryParse(
            //        (respond = this.View.ShowMessageOfInput(R.StringPolar_EnterHumidity, R.T_OK, cancel, humidity.ToString())),
            //        out humidity))
            //{
            //    if (respond == cancel) break;
            //};
            //if (respond != cancel) this.humidity = humidity;
            //else return;

            MeteoSet = true;
        }

        /// <summary>
        /// to speciafy special default parameters based on type of instrument
        /// </summary>
        /// <param name="stationParameters"></param>
        internal virtual void SetDefaultStationParameter(Polar.Station.Parameters stationParameters)
        {
            if (this is IMotorized)
                stationParameters.DefaultMeasure.Face = FaceType.DoubleFace;
            else
                stationParameters.DefaultMeasure.Face = FaceType.Face1;
        }

    }
}
