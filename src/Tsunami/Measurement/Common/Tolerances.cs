﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSU.Common
{
    public class Tolerances
    {
        public double Alignment_Roll_mrad { get; set; } = 0.1;
        public double Alignment_Pitch_mrad { get; set; } = 0.1;
        public double Alignment_Yaw_mrad { get; set; } = 0.1;
        public double Alignment_Radial_mm { get; set; } = 0.1;
        public double Alignment_Vertical_mm { get; set; } = 0.1;
        public double Alignment_Length_mm { get; set; } = 0.5;

        public double Distance_Horizontal_For_Wrong_Point_mm = 50;

        public double Distance_Vertical_For_Wrong_Point_mm = 50;

        public double LGC_s0_Warning { get; set; } = 3;
        public double LGC_s0_Critical { get; set; } = 5;
    }
}
