﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;using R = TSU.Properties.Resources;
using System.Threading.Tasks;

namespace TSU
{
    public class In3D 
    {
        //// singleton // singleton to make sure to not have mre than one instance;
        //private static In3D instance;
        //private In3D() { }
        //private ModuleView _ModuleView;
        //public static In3D Instance
        //{
        //    get
        //    {
        //        if (instance == null)
        //        {
        //            instance = new In3D();
        //        }
        //        return instance;
        //    }
        //}
        //// constructor
        //protected ModuleView view;
        //public In3D(ModuleView destinationView)
        //{
        //    view = destinationView;
        //    _ModuleView = destinationView;
        //    CreateContextMenuItemCollection();
        //    Initialize3DView();
        //    Show();
        //}
        //// Menu
        //public List<BigButtonMenuItem> ContextMenu { get; set; }
        //public void CreateContextMenuItemCollection()
        //{
        //    this.ContextMenu = new List<BigButtonMenuItem>();
        //    this.ContextMenu.Add(new BigButtonMenuItem("Select All", "Select all points",R.Select, SelectAll));
        //    this.ContextMenu.Add(new BigButtonMenuItem("Deselect All", "Deselect all points",R.Deselect, DeselectAll));
        //    this.ContextMenu.Add(new BigButtonMenuItem("Invert Selection", "Invert the Selection of all the points",R.Invert, InvertSelection));
        //    this.ContextMenu.Add(new BigButtonMenuItem("Tree View", "Switch View to a Tree",R.Tree, _ModuleView.ShowAsTreeView));
        //    this.ContextMenu.Add(new BigButtonMenuItem("List View", "Switch View to a List",R.List, _ModuleView.ShowAsListView));

        //}
        //// update
        //public void Update()
        //{
        //    Initialize3DView();
        //}

        //private RenderWindowControl threeDeeView;
        //private List<vtkActor> points;
        //private vtkPolyDataMapper SphereMapper;
        //private vtkRenderWindow RenderWindow;
        //private vtkRenderer Renderer;
        //private vtkIdFilter idFilter;
        //private void Initialize3DView()
        //{
        //    threeDeeView = new RenderWindowControl();
        //    this._ModuleView._PanelBottom.Controls.Add(threeDeeView);
        //    this._ModuleView.Show();

        //    // get a reference to the renderwindow of our renderWindowControl1
        //    RenderWindow = threeDeeView.RenderWindow;
        //    // get a reference to the renderer
        //    Renderer = RenderWindow.GetRenderers().GetFirstRenderer();
        //    // set background color
        //    Renderer.SetBackground(0.14, 0.14, 0.14);
        //    // Work on the 3d view properties
        //    threeDeeView.Dock = DockStyle.Fill;

        //    this.AddOriginOfLocalFrame();
        //    vtkSphereSource SphereSource = vtkSphereSource.New();
        //    // mapper
        //    SphereMapper = vtkPolyDataMapper.New();
        //    SphereMapper.SetInputConnection(SphereSource.GetOutputPort());
        //    SphereSource.SetRadius(0.05);
        //    foreach (Element.Element item in (this._ModuleView._Module as SimpleModule)._Elements)
        //    {
        //        AddPointActorFrom(item);
        //    }
        //    // ensure all actors are visible (in this example not necessarely needed,
        //    // but in case more than one actor needs to be shown it might be a good idea)
        //    Renderer.ResetCamera();

        //}
        //private void AddOriginOfLocalFrame()
        //{
        //    vtkAxesActor axes = vtkAxesActor.New();
        //    // The axes are positioned with a user transform
        //    vtkTransform transform = vtkTransform.New();
        //    transform.Translate(0.0, 0.0, 0.0);
        //    axes.SetUserTransform(transform);
        //    Renderer.AddActor(axes);
        //}
        //private void OrientedArrow(TSU.Module.Element.Point start, TSU.Module.Element.Point end, Color? c = null)
        //{
        //    //Create an arrow.

        //    vtkArrowSource arrowSource = vtkArrowSource.New();
        //    double[] startPoint = new double[]{
        //            start._Coordinates._Mla.X.Value,
        //            start._Coordinates._Mla.Y.Value,
        //            start._Coordinates._Mla.Z.Value
        //        };
        //    double[] endPoint = new double[]{
        //            end._Coordinates._Mla.X.Value,
        //            end._Coordinates._Mla.Y.Value,
        //            end._Coordinates._Mla.Z.Value
        //        };

        //    // Compute a basis
        //    double[] normalizedX = new double[3];
        //    double[] normalizedY = new double[3];
        //    double[] normalizedZ = new double[3];

        //    // The X axis is a vector from start to end
        //    myMath.Subtract(endPoint, startPoint, ref normalizedX);
        //    double length = myMath.Norm(normalizedX);
        //    myMath.Normalize(ref normalizedX);

        //    // The Z axis is an arbitrary vector cross X
        //    double[] arbitrary = new double[]{
        //            vtkMath.Random(-10,10),
        //            vtkMath.Random(-10,10),
        //            vtkMath.Random(-10,10)
        //         };
        //    myMath.Cross(normalizedX, arbitrary, ref normalizedZ);
        //    myMath.Normalize(ref normalizedZ);
        //    // The Y axis is Z cross X
        //    myMath.Cross(normalizedZ, normalizedX, ref normalizedY);
        //    vtkMatrix4x4 matrix = vtkMatrix4x4.New();

        //    // Create the direction cosine matrix
        //    matrix.Identity();
        //    for (int i = 0; i < 3; i++)
        //    {
        //        matrix.SetElement(i, 0, normalizedX[i]);
        //        matrix.SetElement(i, 1, normalizedY[i]);
        //        matrix.SetElement(i, 2, normalizedZ[i]);
        //    }

        //    // Apply the transforms
        //    vtkTransform transform = vtkTransform.New();
        //    transform.Translate(startPoint[0], startPoint[1], startPoint[2]);
        //    transform.Concatenate(matrix);
        //    transform.Scale(length, length, length);


        //    //Create a mapper and actor for the arrow
        //    vtkPolyDataMapper mapper = vtkPolyDataMapper.New();
        //    vtkActor actor = vtkActor.New();

        //    mapper.SetInputConnection(arrowSource.GetOutputPort());
        //    actor.SetUserMatrix(transform.GetMatrix());

        //    actor.SetMapper(mapper);
        //    actor.GetProperty().SetColor(c.Value.R, c.Value.G, c.Value.B);
        //    Renderer.AddActor(actor);
        //}
        //public class myMath
        //{
        //    public static void Subtract(double[] a, double[] b, ref double[] c)
        //    {
        //        c[0] = a[0] - b[0];
        //        c[1] = a[1] - b[1];
        //        c[2] = a[2] - b[2];
        //    }


        //    public static double Norm(double[] x)
        //    {
        //        return Math.Sqrt(x[0] * x[0] + x[1] * x[1] + x[2] * x[2]);
        //    }


        //    public static void Normalize(ref double[] x)
        //    {
        //        double length = Norm(x);
        //        x[0] /= length;
        //        x[1] /= length;
        //        x[2] /= length;
        //    }

        //    public static void Cross(double[] x, double[] y, ref double[] z)
        //    {
        //        z[0] = (x[1] * y[2]) - (x[2] * y[1]);
        //        z[1] = (x[2] * y[0]) - (x[0] * y[2]);
        //        z[2] = (x[0] * y[1]) - (x[1] * y[0]);
        //    }
        //}
        //private void AddPointActorFrom(Element.Element e)
        //{
        //    if (e is TSU.Module.Element.Point)
        //    {
        //        // actor
        //        vtkActor point = vtkActor.New();
        //        point.SetMapper(SphereMapper);
        //        points = new List<vtkActor>();
        //        point.SetPosition(
        //            (e as TSU.Module.Element.Point)._Coordinates._Mla.X.Value,
        //            (e as TSU.Module.Element.Point)._Coordinates._Mla.Y.Value,
        //            (e as TSU.Module.Element.Point)._Coordinates._Mla.Z.Value
        //            );
        //        points.Add(point);
        //        // add actor to the renderer
        //        Renderer.AddActor(point);

        //        //// filter original point ids 
        //        //idFilter = vtkIdFilter.New();
        //        //idFilter.SetInputConnection(ugrid.GetProducerPort());
        //        //idFilter.PointIdsOn();
        //        //idFilter.Update();

        //        //// points 
        //        //vertexFilter = vtkVertexGlyphFilter.New();
        //        //vertexFilter.SetInputConnection(ugrid.GetProducerPort());
        //        //vertexFilter.Update();
        //        //ptsMapper = vtkPolyDataMapper.New();
        //        //ptsMapper.SetInputConnection(vertexFilter.GetOutputPort());
        //        //ptsMapper.ScalarVisibilityOn();
        //        //ptsActor = vtkActor.New();
        //        //ptsActor.SetMapper(ptsMapper);
        //        //ptsActor.GetProperty().SetPointSize(10.0f);

        //        //// point labels 
        //        //lblMapper = vtkLabeledDataMapper.New();
        //        //lblMapper.SetInputConnection(idFilter.GetOutputPort());
        //        //lblMapper.SetLabelModeToLabelFieldData();
        //        //lblActor = vtkActor2D.New();
        //        //lblActor.SetMapper(lblMapper); 
        //    }
        //    else
        //    {
        //        foreach (Element.Element item in e.elements)
        //        {
        //            AddPointActorFrom(item);
        //        }
        //    }
        //}
        //public List<TsuObject> GetSelectedElements()
        //{
        //    List<TsuObject> l = new List<TsuObject>();
        //    return l;
        //}
        //private void OnkeyDown(object sender, KeyEventArgs e)
        //{
        //    switch (e.KeyCode)
        //    {

        //        case Keys.Return: ValidateSelection();
        //            (sender as Control).TopLevelControl.Hide();
        //            break;
        //        default:
        //            filterBoxView.Focus();
        //            break;
        //    }
        //}

        //private void SelectAll()
        //{
        //}
        //private void DeselectAll()
        //{
        //}
        //private void InvertSelection()
        //{
        //}
        //public void Show()
        //{
        //    view._PanelBottom.Controls.Clear();
        //    view._PanelBottom.Controls.Add(threeDeeView);
        //    Update();
        //}
        //public void ValidateSelection()
        //{
        //    view._Selection = new List<TsuObject>();

        //}
        //public void CancelSelection()
        //{
        //    view._Selection.Clear();
        //}
        //// Events
        //private void OnDragStart(object sender, ItemDragEventArgs e)
        //{

        //    dragedName = ((ListView)sender).FocusedItem.Text;
        //    ((ListView)sender).DoDragDrop(dragedName, DragDropEffects.Copy);
        //}
        //private string dragedName;
        //private void OnDrag(object sender, EventArgs e)
        //{
        //    dragedName = ((ListView)sender).FocusedItem.Text;

        //}
        //private void OnDoubleClick(object sender, EventArgs e)
        //{
        //    ValidateSelection();
        //    if (!view._MultiSelection)
        //        if (view._SelectionView != null)
        //            view._SelectionView.Hide();
        //}
        //// Filter
        //private TextBox filterBoxView;
        //private string _filterDefaultText;
        ////private int _filterPreviousLength;
        //private void UseFilter(object sender, EventArgs e)
        //{

        //}
        //private void CloneAllItemsIntoFilteredList()
        //{

        //}
        //private void EnterFilter(object sender, EventArgs e)
        //{
        //    TextBox t = sender as TextBox;
        //    if (t.Text == _filterDefaultText)
        //    {
        //        t.Text = "";
        //    }
        //    t.Focus();
        //    t.SelectAll();
        //}
        //private void LeaveFilter(object sender, EventArgs e)
        //{
        //    TextBox t = sender as TextBox;
        //    if (t.Text == "") t.Text = _filterDefaultText;
        //}
        //public void InitializeFilterView()
        //{
        //    TextBox tb = new TextBox();
        //    tb.Font =TSU.Tsunami2.TsunamiPreferences.Theme.Fonts.Large;
        //    _filterDefaultText = "Type filter key here...";
        //    tb.Text = _filterDefaultText;
        //    tb.HideSelection = false;
        //    tb.Dock = DockStyle.Top;
        //    tb.Show();
        //    tb.AllowDrop = true;
        //    tb.BringToFront();
        //    tb.TextChanged += new EventHandler(UseFilter);
        //    tb.Enter += new EventHandler(EnterFilter);
        //    tb.Leave += new EventHandler(LeaveFilter);
        //    tb.DragEnter += new DragEventHandler(OnDrop);
        //    tb.KeyDown += new KeyEventHandler(OnkeyDown);
        //    this.filterBoxView = tb;
        //}
        //private void OnDrop(object sender, DragEventArgs e)
        //{
        //    filterBoxView.Text = e.Data.GetData(DataFormats.Text).ToString();
        //    ((TextBox)sender).Focus();
        //}
    }
}
