﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSU.Common.Strategies.Views
{

    public class EventArgs : System.EventArgs
    {
        public Interface Strategy;
        public TSU.Views.ModuleView ModuleView;
        public EventArgs(Interface strategy, TSU.Views.ModuleView moduleView)
        {
            Strategy = strategy;
            ModuleView = moduleView;
        }
    }
}
