﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using TSU;
using TSU.Preferences;
using TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using TSU.Views;
using TSU.ENUM;
using E = TSU.Common.Elements;
using F = TSU.Functionalities;
using M = TSU;
using O = TSU.Common.Operations;
using System.Xml.Serialization;

namespace TSU.Common.Strategies.Views
{
    public abstract class Base
    {
        public Types Type { get; set; } = Types.Unknown;
        
       

        private Interface current;


        [XmlIgnore]
        public ManagerView ManagerView { get; set; }


        public abstract List<TsuObject> GetSelectedElements();

        public void ShowSelectionInButton()
        {
            if (this.ManagerView is TSU.Common.Compute.Compensations.Manager.View cmv)
                if (cmv.IsBocView)
                    return;


            string comment;
            if (this.ManagerView.Module is M.Manager module)
            {
                if (module.MultiSelection)
                {
                    int count = this.ManagerView.Module._SelectedObjects.Count;
                    comment = string.Format("{0} item(s) selected", count);
                    if (count > 0)
                    {
                        if (this.ManagerView.Module._SelectedObjects[0] is E.Element)
                        {
                            List<E.Point> points = new List<E.Point>();
                            foreach (E.Element item in this.ManagerView.Module._SelectedObjects)
                            {
                                foreach (E.Point p in item.GetPoints())
                                {
                                    if (!points.Contains(p)) points.Add(p);
                                }
                            }
                            comment += " (" + points.Count.ToString() + " points)";
                        }
                        if (count == 1)
                            comment += " (" + this.ManagerView.Module._SelectedObjects[0]._Name + ")";
                    }
                }
                else
                {
                    var selected = this.GetSelectedElements();
                    if (selected.Count > 0)
                    {
                        if (selected[0] != null)
                            comment = " (" + selected[0]._Name + ")";
                        else
                            comment = "Nothing selected";
                    }
                    else
                        comment = "Nothing selected";
                }
                if (this.ManagerView.bigbuttonTop != null)
                    this.ManagerView.bigbuttonTop.AddCommentToTitle(comment);
            }
        }

        public event StrategyViewEventHandler DoubleClick;

        public void On_DoubleClick(Interface strategy)
        {
            if (DoubleClick != null)
                DoubleClick(this, new EventArgs(strategy, this.ManagerView));
        }

        //public void UseEditableList()
        //{
        //    this.current = new EditableList(_ModuleView);
        //}

        //public  void Show();
        //{
        //    current.CreateContextMenuItemCollection();
        //    current.Show();
        //}
        public void CloneListOrder(Interface strategy)
        {
            if (strategy is List)
            {
                (this as List).listView.ListViewItemSorter =
                   (strategy as List).listView.ListViewItemSorter;
            }
        }


        [XmlIgnore]
        public List<Control> ContextMenu
        {
            get
            {
                // if (this.strategy.ContextMenu == null) this.strategy.CreateContextMenuItemCollection();
                return this.current.ContextMenu;
            }
            set { }
        }

        

        //public void ShowProblems()
        //{
        //    current.ShowProblems();
        //}

        //public List<TsuObject> GetChecked()
        //{
        //    return current.GetChecked();
        //}

        //public List<TsuObject> GetSelectedInBlue()
        //{
        //    return current.GetSelectedInBlue();
        //}


        //public Size GetFullViewRecommandedSize()
        //{
        //    return current.GetFullViewRecommandedSize();
        //}
    }
}
