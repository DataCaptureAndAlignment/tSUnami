﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using R = TSU.Properties.Resources;
using TSU.Preferences;
using TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using TSU;
using TSU.Views;
using TSU.ENUM;
using E = TSU.Common.Elements;
using F = TSU.Functionalities;
using M = TSU;
using O = TSU.Common.Operations;

namespace TSU.Common.Strategies.Views
{

    public delegate void StrategyViewEventHandler(object sender, EventArgs e);

    public interface Interface
    {
        ManagerView ManagerView { get; set; }
        Types Type { get; set; }
        void Show();
        void Update();
        void Focus();
        Control GetControl();
        void Initialize();
        void ValidateSelection();
        void CancelSelection();
        void ShowSelection();
        List<TsuObject> GetSelectedElements();
        void CreateContextMenuItemCollection();
        List<Control> ContextMenu { get; set; }
        void ShowProblems();
        void SetSelectedInBlue();
        List<TsuObject> GetChecked();
        int GetRecommandedHeight();
        List<TsuObject> GetSelectedInBlue();
        void ShowSelectionInButton();
        void CloneListOrder(Interface strategy);
        void Dispose();
        void DeselectAll();

        event StrategyViewEventHandler DoubleClick;
    }
}
