﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using M = TSU;
using P = TSU.Preferences;
using R = TSU.Properties.Resources;

namespace TSU.Common.Strategies.Views
{
    public class ThreeDimension : Base, Interface
    {
        public Control GetControl()
        {
            return null;
        }

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        public static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        protected TSU.Views.ManagerView ModuleView;
        protected M.Manager Module
        {
            get
            {
                return ModuleView.Module;
            }
        }
        private TSU.IO.TCP.AsynchronousServer AsynchronousServer;

        public void Dispose()
        {
            child = null;
            ModuleView = null;
            if (this._Buttons != null)
            {
                this._Buttons.Dispose();
                this._Buttons = null;
            }
        }


        #region Singleton

        private static ThreeDimension instance;
        internal ThreeDimension() { }
        public static ThreeDimension Instance
        {
            get
            {
                if (instance == null)
                    instance = new ThreeDimension();
                return instance;
            }
        }



        #endregion
        internal Process child;
        internal Buttons _Buttons;
        public ThreeDimension(TSU.Views.ManagerView destinationView)
        {
            ModuleView = destinationView;
            if (instance == null) instance = this;
            _Buttons = new Buttons(this);
        }

        internal void SetupTcp(TSU.Views.ManagerView destinationView)
        {
            ModuleView = destinationView;
            this.Initialize();

        }

        public void DeselectAll()
        {
            throw new NotImplementedException();
        }

        public void Initialize()
        {
            AsynchronousServer = new TSU.IO.TCP.AsynchronousServer();
            TSU.IO.TCP.Data.xmlToSend = TSU.IO.Xml.ToText((this.ModuleView._Module as M.Manager).AllElements);
        }

        public override List<TsuObject> GetSelectedElements()
        {
            List<TreeNode> l = new List<TreeNode>();
            //GetCheckedNodes(treeView.Nodes, l);

            List<TsuObject> l2 = new List<TsuObject>();
            foreach (TreeNode item in l)
            {
                if (item.Tag != null)
                    l2.Add(item.Tag as TsuObject);
            }
            return l2;
        }

        public void ShowProblems()
        {

        }

        public void Focus()
        { }
        public void SetSelectedInBlue()
        {
        }
        public void Show()
        {
            ModuleView._PanelBottom.Controls.Clear();

            if (P.Preferences.Instance.the3dProcess == null)
            {
                if (TSU.Preferences.Preferences.Dependencies.GetByBame("SUGAR3", out Dependencies.Application app))
                {
                    string path = app.Path;

                    ProcessStartInfo startInfo = new ProcessStartInfo(path);
                    startInfo.WindowStyle = ProcessWindowStyle.Maximized;

                    P.Preferences.Instance.the3dProcess = Process.Start(startInfo);

                    System.Threading.Thread.Sleep(1000); // Allow the process to open it's window
                }
            }

            child = P.Preferences.Instance.the3dProcess;

            Action SetFullScr4een = () =>
            {

            };

            SetParent(child.MainWindowHandle, ModuleView._PanelBottom.Handle);
            SetFullScreen(this, null);
            ModuleView._PanelBottom.Layout += SetFullScreen;

            //MoveWindow(child.MainWindowHandle, -5, -30, ModuleView._PanelBottom.Width + 10, ModuleView._PanelBottom.Height + 35, true);
            //Update();
        }

        private void SetFullScreen(object sender, LayoutEventArgs e)
        {
            ShowWindow(this.child.MainWindowHandle, 6);
            ShowWindow(this.child.MainWindowHandle, 3);
        }

        public List<Control> ContextMenu { get; set; }
        public Types Type { get; set; } = Types.In3D;

        public class Buttons
        {
            public ThreeDimension threeDimension;

            public TSU.Views.BigButton ViewChoice;

            public Buttons(ThreeDimension threeDimension)
            {
                this.threeDimension = threeDimension;

                ViewChoice = new TSU.Views.BigButton(R.buttonWindowsStrategy, R.View, threeDimension.ChoiceView);
                ViewChoice.HasSubButtons = true;

            }
            internal void Dispose()
            {
                this.threeDimension = null;
                if (this.ViewChoice != null)
                    this.ViewChoice.Dispose();
                this.ViewChoice = null;
            }


        }


        public void CreateContextMenuItemCollection()
        {
            this.ContextMenu = new List<Control>
                {
                    _Buttons.ViewChoice
                };

            // button special for each type of manager
            this.ContextMenu.AddRange(this.ModuleView.GetViewOptionButtons());
        }

        public void Update()
        {
            this.Show();
        }


        public void ValidateSelection()
        {
            //if (_ModuleView._MultiSelection)
            //{
            //    _ModuleView._Selection = new List<TsuObject>();

            //    foreach (TsuNode item in treeView.Nodes)
            //    {
            //        CompositeElement e = item.GetPointsFromCheckedNodes(false);
            //        foreach (Element.Element item2 in e.elements)
            //        {
            //            _ModuleView._Selection.Add(item2);
            //        }
            //    }
            //}

            //TsuNode n = treeView.SelectedNode as TsuNode;
            //if (n != null)
            //    if (n._fileElementType == ENUM.ElementType.SequenceTH || n._fileElementType == ENUM.ElementType.Point)
            //    {
            //        _ModuleView._Selection.Add(n.Tag as Element.Element);
            //    }
            //    else
            //    {
            //        _ModuleView._Selection.Add(n.Tag as TsuObject);
            //    }
        }
        public void CancelSelection()
        {
            this.ModuleView.Module._SelectedObjects.Clear();
        }
        public List<TsuObject> GetChecked()
        {
            //    List<TsuObject> l = new List<TsuObject>();
            //    foreach (ListViewItem item in this.listView.CheckedItems)
            //        l.Add(item.Tag as TsuObject);
            return null;
        }

        public List<TsuObject> GetSelectedInBlue()
        {
            //    List<TsuObject> l = new List<TsuObject>();
            //    foreach (ListViewItem item in this.listView.SelectedItems)
            //        l.Add(item.Tag as TsuObject);
            return null;
        }


        public void ShowSelection()
        {
            throw new NotImplementedException();
        }

        internal void ChoiceView()
        {
            List<Control> l = new List<Control>();
            if (this.ModuleView.AvailableViewStrategies.Contains(Types.Tree)) l.Add(this.ModuleView._Buttons.TreeView);
            if (this.ModuleView.AvailableViewStrategies.Contains(Types.List)) l.Add(this.ModuleView._Buttons.ListView);
            if (this.ModuleView.AvailableViewStrategies.Contains(Types.In3D)) l.Add(this.ModuleView._Buttons.ThreeDView);
            if (TSU.Tsunami2.Preferences.Values.GuiPrefs.UserType == P.Preferences.UserTypes.Super)
                l.Add(this.ModuleView._Buttons.ShowSelection);
            this.ModuleView.ShowPopUpSubMenu(l, "choice ");
            ModuleView._PanelBottom.Layout -= SetFullScreen;
            ShowWindow(this.child.MainWindowHandle, 0);
        }


        public int GetRecommandedHeight()
        {
            return -1;
        }

    }

    
}
