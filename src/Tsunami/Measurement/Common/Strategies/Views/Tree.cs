﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Timers;
using System.Windows.Forms;
using TSU.Common.Elements;
using TSU.Preferences.GUI;
using TSU.Views;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using EC = TSU.Common.Elements.Composites;
using M = TSU;
using O = TSU.Common.Operations;
using P = TSU.Preferences;
using R = TSU.Properties.Resources;

namespace TSU.Common.Strategies.Views
{
    public class Tree : Base, Interface
    {
        public Control GetControl()
        {
            return this.treeView;
        }

        public Types Type { get; set; } = Types.Tree;

        public bool MultiSelection
        {
            get
            {
                if (this.Module == null)
                    return false;
                return this.Module.MultiSelection;
            }
        }

        public Tree() { }

        //[XmlIgnore]
        //private ManagerView _ModuleView;


        public Tree(ManagerView destinationView)
        {
            this.ManagerView = destinationView;
            this._Buttons = new Buttons(this);
            Initialize();
        }

        public void Dispose()
        {
            if (Module != null && Module.View != null)
                Module.View = null;
            treeView.AfterCheck -= treeView_AfterCheck;

            if (this._Buttons != null)
            {
                this._Buttons.Dispose();
                this._Buttons = null;
            }
            timer?.Dispose();
        }

        public void DeselectAll()
        {
            if (treeView != null && treeView.CheckBoxes)
            {
                foreach (TreeNode item in treeView.Nodes)
                {
                    item.Checked = false;
                }
            }
        }
        public override List<TsuObject> GetSelectedElements()
        {
            List<TreeNode> checkedNodes = new List<TreeNode>();
            List<TsuObject> TsuObjects = new List<TsuObject>();

            if (this.Module.View != null)
            {
                if (MultiSelection) // to avoid to keep selected stuff that are not visible
                {
                    GetCheckedNodes(treeView.Nodes, checkedNodes);

                    foreach (TreeNode item in checkedNodes)
                    {
                        if (item.Tag != null)
                            TsuObjects.Add(item.Tag as TsuObject);
                    }
                }
            }

            var blue = this.Module._SelectedObjectInBlue;
            if (blue != null)
                TsuObjects.Add(blue);
            else
            {
                if (!MultiSelection)
                {
                    if (this.Module._SelectedObjects.Count > 0)
                    {
                        TsuObjects.Add(this.Module._SelectedObjects[0]);
                    }
                }
            }

            return TsuObjects;
        }
        internal void GetCheckedNodes(TreeNodeCollection nodes, List<TreeNode> list)
        {
            foreach (TreeNode node in nodes)
            {
                if (MultiSelection)
                {
                    if (node.Checked)
                        list.Add(node);
                }
                else if (node.IsSelected)
                    list.Add(node);

                GetCheckedNodes(node.Nodes, list);
            }
        }




        #region Fields

        internal Buttons _Buttons;
        public TreeView treeView;
        private TsuNode ghostMainNode;
        //protected ManagerView _ModuleView;
        protected Manager Module
        {
            get
            {
                return ManagerView.Module;
            }
        }

        /// <summary>
        /// This is used not to throw several 'change' event when checking subnodes
        /// </summary>
        bool ToConsiderAsASingleChange;
        public event EventHandler Changed;

        #endregion

        #region Construct



        public List<Control> ContextMenu { get; set; }

        public class Buttons
        {
            public Tree Tree;
            public List<BigButton> all;

            public BigButton Collapse;
            public BigButton Expand;
            public BigButton ViewChoice;
            public BigButton SelectionChoice;
            public BigButton CollapseExpand;
            public BigButton ExpandToPoints;

            public Buttons(Tree tree)
            {
                Tree = tree;

                all = new List<BigButton>();

                Collapse = new BigButton(R.CollapseAll, R.Export, Tree.CollapseAll); all.Add(Collapse);
                Expand = new BigButton(R.T524, R.Export, Tree.ExpandAll); all.Add(Expand);
                ExpandToPoints = new BigButton(R.T523, R.Element_Point, Tree.expandToPoints); all.Add(ExpandToPoints);

                ViewChoice = new BigButton(R.buttonWindowsStrategy, R.View, Tree.ChoiceView); all.Add(ViewChoice);
                ViewChoice.HasSubButtons = true;

                CollapseExpand = new BigButton(R.T_buttonExpandCollapse, R.Export, Tree.CollapseExpand); all.Add(CollapseExpand);
                CollapseExpand.HasSubButtons = true;

                SelectionChoice = new BigButton(R.T_button_Selections, R.Invert, Tree.ChoiceSelection); all.Add(SelectionChoice);
                SelectionChoice.HasSubButtons = true;
            }

            public void Dispose()
            {
                Tree = null;
                if (this.all != null)
                {
                    for (int i = 0; i < all.Count; i++)
                    {
                        this.all[i]?.Dispose();
                        this.all[i] = null;
                    }
                    this.all.Clear();
                    this.all = null;
                }
            }
        }

        //public void CreateContextMenuItemCollection()
        //{
        //    this.ContextMenu = new List<BigButton>();

        //    //this.ContextMenu.Add(_Buttons.Selection);
        //    this.ContextMenu.Add(_Buttons.CollapseExpand);
        //    this.ContextMenu.Add(_Buttons.ViewChoice);
        //}

        public void CreateContextMenuItemCollection()
        {
            this.ContextMenu = new List<Control>();
            var mv = this.ManagerView;

            // View type
            if (mv.AvailableViewStrategies.Contains(Types.Tree)) this.ContextMenu.Add(mv._Buttons.TreeView);
            if (mv.AvailableViewStrategies.Contains(Types.List)) this.ContextMenu.Add(mv._Buttons.ListView);
            if (mv.AvailableViewStrategies.Contains(Types.In3D)) this.ContextMenu.Add(mv._Buttons.ThreeDView);


            // button special for each type of manager
            this.ContextMenu.AddRange(this.ManagerView.GetViewOptionButtons());

            //selection
            if (this.ManagerView is E.Manager.View) this.ContextMenu.Add(this._Buttons.ExpandToPoints);
            this.ContextMenu.Add(this._Buttons.Collapse);
            this.ContextMenu.Add(this._Buttons.Expand);

            // Showselection
            if (TSU.Tsunami2.Preferences.Values.GuiPrefs.UserType == P.Preferences.UserTypes.Super)
                this.ContextMenu.Add(mv._Buttons.ShowSelection);
        }

        internal void ChoiceView()
        {
            List<Control> l = this.Module.View.GetViewOptionButtons();

            var mv = this.ManagerView;

            if (mv.AvailableViewStrategies.Contains(Types.Tree)) l.Add(mv._Buttons.TreeView);
            if (mv.AvailableViewStrategies.Contains(Types.List)) l.Add(mv._Buttons.ListView);
            if (mv.AvailableViewStrategies.Contains(Types.In3D)) l.Add(mv._Buttons.ThreeDView);
            if (TSU.Tsunami2.Preferences.Values.GuiPrefs.UserType == P.Preferences.UserTypes.Super)
                l.Add(mv._Buttons.ShowSelection);
            mv.ShowPopUpSubMenu(l, "choice ");
        }

        internal void CollapseExpand()
        {
            List<Control> l = new List<Control>();
            // exception ;(
            if (this.ManagerView is E.Manager.View) l.Add(this._Buttons.ExpandToPoints);
            l.Add(this._Buttons.Expand);
            l.Add(this._Buttons.Collapse);

            this.ManagerView.ShowPopUpSubMenu(l, "node ");
        }

        internal void ChoiceSelection()
        {
            List<Control> l = new List<Control>();

            var mv = this.ManagerView;


            if (mv.AvailableViewStrategies.Contains(Types.Tree)) l.Add(mv._Buttons.TreeView);
            if (mv.AvailableViewStrategies.Contains(Types.List)) l.Add(mv._Buttons.ListView);
            if (mv.AvailableViewStrategies.Contains(Types.In3D)) l.Add(mv._Buttons.ThreeDView);
            mv.ShowPopUpSubMenu(l, "choice ");
        }

        public void Initialize()
        {
            this.treeView = new TsuTreeView();
            this.treeView.ImageList = TreeNodeImages.ImageListInstance;

            this.treeView.FullRowSelect = true;
            this.treeView.NodeMouseDoubleClick += new TreeNodeMouseClickEventHandler(OnNodeMouseDoubleClick);
            this.treeView.AfterExpand += OnExpand;

            this.treeView.DoubleClick += OnDoubleClick;
            this.treeView.NodeMouseClick += new TreeNodeMouseClickEventHandler(OnNodeMouseClick);
            this.treeView.AfterCheck += treeView_AfterCheck;
            this.treeView.KeyDown += OnTreeViewKeyDown;
            //this.treeView.VisibleChanged+=treeView_VisibleChanged;
            treeView.Show();
        }
        private bool ignoreDoubleClick = false;
        private System.Timers.Timer timer = null;

        private void OnExpand(object sender, TreeViewEventArgs e)
        {
            ignoreDoubleClick = true;
            if (timer is null)
            {
                timer = new System.Timers.Timer();
                timer.Elapsed += OnTimedEvent;
                timer.Interval = 100;
            }
            timer.Start();
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            //after the timer
            ignoreDoubleClick = false;
        }

        private void OnDoubleClick(object sender, System.EventArgs e)
        {
            try
            {
                if (treeView.SelectedNode != null && treeView.SelectedNode.Nodes.Count == 0)
                    this.On_DoubleClick(this);
            }
            catch (Exception)
            {
                // certain endroit pose problem en double click?
            }
        }

        private void OnNodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            // strangely, this event happen on the mouse up when click on the picture or the name of the node, but happened on click down when click the (+)...so we need to check if it is click on the (+) to avoid to aut click on the show option button

            int widthOfThePlusSignToOpenANode = 32;
            Rectangle r = e.Node.Bounds;
            bool clickedIOnThePlus = !r.Contains(e.Location);
            r.X -= widthOfThePlusSignToOpenANode;
            r.Width += widthOfThePlusSignToOpenANode + 2;
            if (r.Contains(e.Location))
            {
                ToConsiderAsASingleChange = true;
                if (MultiSelection)
                {
                    if (this.Module.IsItemSelectable(e.Node.Tag as TsuObject))
                        e.Node.Checked = !e.Node.Checked;
                }
                else
                {
                    // Select the first selectable node if there's one
                    var parent = GetFirstSelectableNode(e.Node);
                    if (parent != null)
                        CheckNode(parent);
                    // case we click a parent Node, we want the pre-selected item to be removed.
                    else
                    {
                        UncheckAllBoxes(this.treeView.Nodes, null);
                        this.Module._SelectedObjects.Clear();
                        this.Module._SelectedObjectInBlue = null;
                        this.Module._SelectedObjects.Clear();
                        this.ShowSelectionInButton();
                    }
                }

                this.ManagerView.Module.Change();
                ToConsiderAsASingleChange = false;
            }

            if (!clickedIOnThePlus)
                this.Module.NodeClick(e);

            TSU.Debug.WriteInConsole("TreeView.NodeMouseClickEvent");

            if (e.Node.Tag == null) return;

            this.ManagerView.DealWithClickedItem(e.Node.Tag);
        }

        /// <summary>
        /// Return the first selectable node, going from node to parent node until we reach as selectable node or the root
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        private TreeNode GetFirstSelectableNode(TreeNode n)
        {
            while (!(n is null))
            {
                if (this.Module.IsItemSelectable(n.Tag as TsuObject))
                {
                    // Found a selectable node
                    return n;
                }

                n = n.Parent;
            }

            // Reached the root without finding a selectable node
            return null;
        }

        // the method will check the node and all the subnodes to make sure the v is visible in the view
        private void CheckNode(TreeNode node)
        {
            if (MultiSelection)
            {
                node.Checked = true;
            }
            else
            {
                treeView.SelectedNode = node;
                node.Checked = true;
            }
                this.ShowSelectionInButton();
            //foreach (TreeNode subNode in node.Nodes)
            //{
            //    CheckNode(subNode);
            //}    
        }

        private void OnTreeViewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.F)
            {
                List<string> buttonTexts = new List<string> { R.T_FIND, R.T_CANCEL };

                MessageTsu.ShowMessageWithTextBox(R.M_SEARCH_MM, buttonTexts, out string buttonClicked, out string textInput, "", false);

                if (buttonClicked == R.T_CANCEL)
                    return;

                TsuNode n = SearchForNodeByName(textInput, treeView.Nodes);
                if (n != null) treeView.SelectedNode = n;

            }

        }

        private TsuNode SearchForNodeByName(string substring, TreeNodeCollection treeNodeCollection)
        {
            foreach (TsuNode item in treeNodeCollection)
            {
                if (item.Text.ToUpper().Contains(substring.ToUpper()))
                    return item;
                else
                {
                    TsuNode n = SearchForNodeByName(substring, item.Nodes);
                    if (n != null) return n;
                }
            }
            return null;
        }


        #endregion

        #region Updating

        public void Update()
        {
            try
            {
                TSU.HourGlass.Set();

                treeView.BeginUpdate();
                treeView.AfterCheck -= treeView_AfterCheck;

                SaveExpansionPattern();

                this.treeView.CheckBoxes = MultiSelection;

                treeView.Nodes.Clear();

                //// for now i show everyhting why not?  Nope if then, hen we cannot choose what to show
                //ElementManager em = this.Module as ElementManager;
                //if (em != null)
                //{
                //    em.SetSelectableToAllElements();
                //}
                AddNodesForSelectableObject();

                this.ManagerView.ArrangeNodes(treeView);

                PreCheckSelectedObjects();

                ApplyExpansionPattern(this.ghostMainNode.Nodes, this.treeView.Nodes);

                treeView.Refresh();
                treeView.EndUpdate();
                this.treeView.Focus();

                treeView.AfterCheck += treeView_AfterCheck;
                TSU.Debug.WriteInConsole(string.Format("updated: {0} selected, {1} selectable",
                    this.Module._SelectedObjects.Count, this.Module.SelectableObjects.Count));

            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_COULD_NOT} {R.T_UPDATE} 'tree view';{ex.Message}", ex);
            }

            finally
            {
                TSU.HourGlass.Remove();
                this.ShowSelectionInButton();
            }
        }

        private void AddNodesForSelectableObject()
        {
            // exception :(
            if (this.ManagerView is O.View)
            {
                O.View v = this.ManagerView as O.View;
                v.Module.OperationTree?.Remove();
                treeView.Nodes.Add(v.Module.OperationTree);

                foreach (TsuNode nod in TsuNode.EnumerateAllNodes(treeView.Nodes))
                {
                    nod.Checked = false;
                }
            }
            else
            {
                if (this.Module.AllElements == null)
                    this.Module.AllElements = new List<TsuObject>();

                var selectables = this.Module.SelectableObjects;
                if (selectables.Count == 0)
                {
                    selectables.AddRange(this.Module.AllElements);
                }

                foreach (TsuObject o in this.Module.AllElements)
                {
                    // if the selectable object it direcly present in the root we shwo it:
                    if (selectables.Contains(o))
                        treeView.Nodes.Add(new TsuNode(o, TsuNode.NodeDetailsType.Detailed));

                    // if it is a composite then we check that it contains at least on of the selectable objects
                    if (o is EC.CompositeElement ce)
                    {
                        foreach (Element selectable in selectables.OfType<Element>())
                        {
                            if (ce.ContainsInTheHierarchy(selectable))
                            {
                                treeView.Nodes.Add(new TsuNode(o, TsuNode.NodeDetailsType.Detailed, selectables));
                                break;
                            }
                        }



                    }
                }

            }
        }

        // will cehck the node that contains object that are selectable and selected
        private void PreCheckSelectedObjects()
        {
            foreach (TsuNode node in TsuNode.EnumerateAllNodes(treeView.Nodes))
            {
                if (this.Module.SelectableObjects.Count==0 || this.Module.SelectableObjects.Contains(node.Tag))
                {
                    if (this.Module._SelectedObjects.Contains(node.Tag))
                    {
                        CheckNode(node);
                    }

                    if (this.Module._SelectedObjectInBlue == node.Tag)
                        CheckNode(node);
                }
            }
        }

        public void Focus()
        {
            this.treeView.Focus();
        }
        public void SetSelectedInBlue()
        {
        }
        #endregion

        #region Showing

        public void Show()
        {
            CreateContextMenuItemCollection();
            ManagerView._PanelBottom.Controls.Clear();
            this.treeView.CheckBoxes = true;
            ManagerView._PanelBottom.Controls.Add(this.treeView);
            treeView.Visible = true;
            this.Update();
        }

        public void ShowSelection()
        {
            if (this.Module._SelectedObjects.Count > 0)
            {
                treeView.Focus();
            }
        }

        #endregion

        #region Selecting

        public void ValidateSelection()
        {
            var mod = this.Module;
            var selectable = mod.SelectableObjects;
            var selection = mod._SelectedObjects;
            bool constraint = selectable.Count > 0;
            bool okToLeave = !constraint; // should be false it nothing is selected
            if (constraint)
            {
                foreach (var selected in selection)
                {
                    if (selectable.Contains(selected))
                    {
                        okToLeave = true;
                    }
                    else
                    {
                        okToLeave = false;
                        break;
                    }
                }
            }
            var parentMessage = this.ManagerView?.FindMessageModuleViewInParents();
            if (parentMessage != null)
                parentMessage.CancelClosing = false;
            if (okToLeave)
            {
                if (!MultiSelection)
                {
                    this.ManagerView?.Hide();
                    this.ManagerView?.FindMessageModuleViewInParents()?.TestClosure();
                }
            }
            else
            {
                new MessageInput(MessageType.Warning, R.T_NOTHING_SELECTED).Show();
                if (parentMessage != null)
                    parentMessage.CancelClosing = true;
            }
        }

        public void CancelSelection()
        {
            ManagerView.Module._SelectedObjects.Clear();
        }
        public List<TsuObject> GetChecked()
        {
            List<TsuObject> l = new List<TsuObject>();
            List<TreeNode> ts = new List<TreeNode>();
            this.GetCheckedNodes(this.treeView.Nodes, ts);
            foreach (TreeNode item in ts)
                if (item.Checked) l.Add(item.Tag as TsuObject);
            return l;
        }

        public List<TsuObject> GetSelectedInBlue()
        {
            List<TsuObject> l = new List<TsuObject>();
            l.Add(this.treeView.SelectedNode.Tag as TsuObject);
            return l;
        }

        #endregion

        #region BoxChecking

        private void UncheckAllBoxes(TreeNodeCollection treeNodeCollection, TsuNode exceptionNode = null)
        {
            foreach (TsuNode item in treeNodeCollection)
            {
                if (item.Checked)
                {
                    if (item != exceptionNode)
                        item.Checked = false;
                }
                UncheckAllBoxes(item.Nodes, exceptionNode);
            }
        }

        private void treeView_AfterCheck(object sender, TreeViewEventArgs e)
        {
            bool thisIsACheckFromAClick = !ToConsiderAsASingleChange;
            if (thisIsACheckFromAClick)
            {
                treeView.BeginUpdate();
                ToConsiderAsASingleChange = true;
            }

            TreatTheNode(e.Node as TsuNode);

            if (thisIsACheckFromAClick)
            {
                ToConsiderAsASingleChange = false;
                this.ShowSelectionInButton();
                //this.Module.SelectedObject = null;
                this.ManagerView.Module.Change();
                treeView.EndUpdate();
                this.treeView.Refresh();
            }
            //TSU.Debug.WriteInConsole(string.Format("node checked: {0}",
            //    e.Node.Name));
        }

        #endregion



        #region Expansion Patterns

        public void ShowProblems()
        {
            foreach (TsuNode item in this.treeView.Nodes)
            {
                ExpandNodeWithMissingInfo(item);
            }
        }

        private void ExpandNodeWithMissingInfo(TsuNode n)
        {
            if (n.State == ENUM.StateType.NotReady)
                n.Expand();
            foreach (TsuNode item in n.Nodes)
            {
                ExpandNodeWithMissingInfo(item);
            }
        }

        private void ExpandAll()
        {
            treeView.BeginUpdate();
            treeView.ExpandAll();
            treeView.EndUpdate();
        }

        private void CollapseAll()
        {
            treeView.BeginUpdate();
            treeView.CollapseAll();
            treeView.EndUpdate();
        }

        private void expandToPoints()
        {
            treeView.BeginUpdate();
            foreach (TsuNode item in treeView.Nodes)
            {
                item.ExpandToPoint();
            }
            treeView.EndUpdate();
        }

        #region Save Expansion state

        private void SaveExpansionPattern()
        {
            ghostMainNode = new TsuNode();
            CopyChildExpansionPattern(ghostMainNode.Nodes, treeView.Nodes);
        }

        /// <summary>
        /// Will copy the isExpanded attribut of the nodes and children
        /// </summary>
        /// <param name="fan"></param>
        /// <param name="model"></param>
        private void CopyChildExpansionPattern(TreeNodeCollection fan, TreeNodeCollection model)
        {
            foreach (TsuNode item in model)
            {
                TsuNode ghostChild = item.Clone() as TsuNode;
                ghostChild.Nodes.Clear();
                CopyChildExpansionPattern(ghostChild.Nodes, item.Nodes);
                fan.Add(ghostChild);
            }
        }

        #endregion

        #region ReApply Expansion state

        /// <summary>
        /// Will apply the pattern of expanded or not nodes from a copy made by .SaveExpandedPattern()
        /// </summary>
        private void ApplyExpansionPattern(TreeNodeCollection model, TreeNodeCollection fan)
        {
            if (model.Count > 0)
            {
                if (model.Count != fan.Count)
                {
                    // this.UncheckAllBoxes(this.treeView.Nodes); // why to uncheck? it this case the prechecck selection in worthless

                    // if there is only one Node, lets open it as there is no other choice
                    if (fan.Count == 1)
                        if (fan[0].GetNodeCount(false) > 0)
                            fan[0].Expand();

                    // if there is a selectedInBlue lets expand to it
                    if ( this.Module._SelectedObjectInBlue !=null)
                        fan[0].Expand();
                    return;
                }
            }

            // Open the nodes if there is only one
            if (this.treeView.Nodes.Count == 1) this.treeView.Nodes[0].Expand();

            // Check childrens
            for (int i = 0; i < model.Count; i++)
            {
                if (model[i].IsExpanded)
                    fan[i].Expand();
                ApplyExpansionPattern(model[i].Nodes, fan[i].Nodes);
            }
        }
        #endregion

        #endregion

        #region Events

        public void Change()
        {
            if (this.Changed != null)
                Changed(this, new System.EventArgs());
        }

        private void OnNodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (ignoreDoubleClick) return;
            var tag = e.Node.Tag;
            this.ManagerView.ShowTsuObject(tag);

            // retarded way to save guipref modification
            if (e.Node.Parent != null)
                if (e.Node.Parent.Tag is Global)
                    M.Tsunami2.Preferences.Values.SaveGuiPrefs();

            if (!MultiSelection && tag!=null)
            {
                ValidateSelection();
            }
        }

        private void TreatTheNode(TsuNode n)
        {
            if (n == null)
                return;

            TsuObject tag = n.Tag as TsuObject;

            if (n.Checked)
            {
                //Cancel the check if the item is not selectable
                if (!this.Module.IsItemSelectable(tag))
                {
                    n.Checked = false;
                    if (n.Parent is TsuNode n2)
                        TreatTheNode(n2);
                    return;
                }

                if (!MultiSelection)
                {
                    UncheckAllBoxes(this.treeView.Nodes, n);
                    this.Module._SelectedObjects.Clear();
                }

                treeView.SelectedNode = n;
                this.Module._SelectedObjectInBlue = tag;

                if (tag != null && !this.Module._SelectedObjects.Contains(n.Tag))
                {
                    this.ManagerView.selectionChanged = true;
                    this.Module.AddSelectedObjects(tag);  // previous: this.Module._SelectedObjects.Add(n.Tag as TsuObject); => to now allow events when add
                }
            }
            else
            {
                if (tag != null)
                {
                    this.ManagerView.selectionChanged = true;
                    this.Module.RemoveSelectedObjects(tag); // previous: this.Module._SelectedObjects.Remove(n.Tag as TsuObject);
                }
            }

            CheckChildren(n);
        }

        private void CheckChildren(TreeNode node)
        {
            if (this.ManagerView.AutoCheckChildren)
            {
                bool wasCollapsedBeforeCheck = !node.IsExpanded;
                foreach (TsuNode item in node.Nodes)
                {
                    if (item.Tag is TsuObject to && this.Module.IsItemSelectable(to))
                        item.Checked = node.Checked;

                    if (!MultiSelection) break;
                }
                if (wasCollapsedBeforeCheck) node.Collapse();
            }
        }

        #endregion

        public int GetRecommandedHeight()
        {
            return -1;
        }
    }

}
