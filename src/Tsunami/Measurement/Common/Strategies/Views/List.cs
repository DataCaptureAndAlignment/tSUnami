﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using TSU.ENUM;
using TSU.Views;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using P = TSU.Preferences;
using R = TSU.Properties.Resources;


namespace TSU.Common.Strategies.Views
{
    public class List : Base, Interface
    {
        public Control GetControl()
        {
            return this.listView;
        }

        public new Types Type { get; set; } = Types.List;
        public bool MultiSelection
        {
            get
            {
                if (this.Module == null)
                    return false;

                return this.Module.MultiSelection;
            }
        }

        internal Buttons _Buttons;

        Timer validateSelectionTimer;
        bool isUpdating;
        protected Manager Module
        {
            get
            {
                return ManagerView.Module;
            }
        }

        #region  constructor

        public List()
        {
        }

        public List(ManagerView destinationView)
        {
            ManagerView = destinationView;
            Initialize();
            //if (instance == null) instance = this; 
            _Buttons = new Buttons(this);
        }

        public void Dispose()
        {
            if (Module != null && Module.View != null)
                Module.View = null;
            if (this._Buttons != null)
            {
                this._Buttons.Dispose();
                this._Buttons = null;
            }
        }


        #endregion

        #region  Menu

        public new List<Control> ContextMenu { get; set; }

        public class Buttons
        {
            public List List;
            public List<BigButton> all;
            public BigButton ViewChoice;
            public BigButton SelectionChoice;
            public BigButton SelectAll;
            public BigButton DeselectAll;
            public BigButton InvertSelection;
            public BigButton DuplicateList;

            public Buttons(List list)
            {
                List = list;

                all = new List<BigButton>();
                SelectAll = new BigButton(R.T527, R.Select, List.SelectAll); all.Add(SelectAll);
                DeselectAll = new BigButton(R.T528, R.Deselect, List.DeselectAll); all.Add(DeselectAll);
                InvertSelection = new BigButton(R.T529, R.Invert, List.InvertSelection); all.Add(InvertSelection);
                DuplicateList = new BigButton($"{R.T_DULICATE_LIST_VIEW};{R.T_THIS_WILL_ALLOW_YOU_TO_HORIZONTALLY_SCROLL_2_IDENTICAL_VIEWS}", R.Duplicate, List.DuplicateList); all.Add(DuplicateList);

                ViewChoice = new BigButton(R.buttonWindowsStrategy, R.View, List.ChoiceView); all.Add(ViewChoice);
                ViewChoice.HasSubButtons = true;
                SelectionChoice = new BigButton(R.T_button_Selections, R.Invert, List.ChoiceSelection); all.Add(SelectionChoice);
                SelectionChoice.HasSubButtons = true;
            }
            internal void Dispose()
            {
                List = null;
                if (this.all != null)
                {
                    for (int i = 0; i < all.Count; i++)
                    {
                        if (this.all[i] != null)
                            this.all[i].Dispose();
                        this.all[i] = null;
                    }
                    this.all.Clear();
                    this.all = null;
                }
            }

        }

        public bool duplicateListViewIsOn = false;
        private void DuplicateList()
        {
            duplicateListViewIsOn = !duplicateListViewIsOn;
            this.Update();
        }

        public void CreateContextMenuItemCollection()
        {
            this.ContextMenu = new List<Control>();

            // View type
            if (this.ManagerView.AvailableViewStrategies.Contains(Types.Tree)) this.ContextMenu.Add(this.ManagerView._Buttons.TreeView);
            if (this.ManagerView.AvailableViewStrategies.Contains(Types.List)) this.ContextMenu.Add(this.ManagerView._Buttons.ListView);
            if (this.ManagerView.AvailableViewStrategies.Contains(Types.In3D)) this.ContextMenu.Add(this.ManagerView._Buttons.ThreeDView);

            // button special for each type of manager
            this.ContextMenu.AddRange(this.ManagerView.GetViewOptionButtons());

            //selection
            if (MultiSelection)
            {
                this.ContextMenu.Add(this._Buttons.SelectAll);
                this.ContextMenu.Add(this._Buttons.DeselectAll);
                this.ContextMenu.Add(this._Buttons.InvertSelection);
                this.ContextMenu.Add(this._Buttons.DuplicateList);
            }

            // show element type for element module
            if (this.Module is E.Manager.Module e && e.View != null && e.View.ElementShownInListType != E.Manager.View.ElementShownInListTypes.Unknown)
                SetSelectable(e);

            // Showselection

            if (Tsunami2.Preferences.Values.GuiPrefs.UserType == P.Preferences.UserTypes.Super)
                this.ContextMenu.Add(this.ManagerView._Buttons.ShowSelection);
        }

        internal void ChoiceView()
        {
            List<Control> l = this.Module.View.GetViewOptionButtons();

            //if (e.View.ElementShownInListType != ElementManagerView.ElementShownInListTypes.Unknown)
            //    SetSelectable(e);


            // put view buttons
            if (this.ManagerView.AvailableViewStrategies.Contains(Types.Tree)) l.Add(this.ManagerView._Buttons.TreeView);
            if (this.ManagerView.AvailableViewStrategies.Contains(Types.List)) l.Add(this.ManagerView._Buttons.ListView);
            if (this.ManagerView.AvailableViewStrategies.Contains(Types.In3D)) l.Add(this.ManagerView._Buttons.ThreeDView);

            if (Tsunami2.Preferences.Values.GuiPrefs.UserType == P.Preferences.UserTypes.Super)
                l.Add(this.ManagerView._Buttons.ShowSelection);
            this.ManagerView.ShowPopUpSubMenu(l, "choice ");
        }


        internal void ChoiceSelection()
        {
            List<Control> l = new List<Control>
            {
                this._Buttons.SelectAll,
                this._Buttons.DeselectAll,
                this._Buttons.InvertSelection,
                this._Buttons.DuplicateList
            };

            this.ManagerView.ShowPopUpSubMenu(l, "Choice ");
        }

        #endregion

        #region  update

        public void Show()
        {
            CreateContextMenuItemCollection();
            ListView lv = this.listView;
            lv.Items.Clear();
            var bottomPanel = ManagerView._PanelBottom;
            bottomPanel.Controls.Clear();
            SplitContainer spt = new SplitContainer() { Orientation = Orientation.Vertical, Dock = DockStyle.Fill };
            Tsunami2.Preferences.Theme.ApplyTo(spt);
            spt.SplitterDistance = 5000;// to be all the way to the right
            spt.Panel1.Controls.Add(lv);

            spt.Panel2.Controls.Add(listView2);
            bottomPanel.Controls.Add(spt);
            //bottomPanel.Controls.Add(lv);
            bottomPanel.Controls.Add(filterBoxView);
            // listView.MultiSelect = (view._MultiSelection) ? true : false;
            lv.MultiSelect = true;

            Update(); // added because if not it oesnt show anything unless you press in the filter after a treeview view


            if (this.filterBoxView.Text != R.T_defaultFilter)
            {
                this.filterBoxView.Focus();
                this.filterBoxView.SelectAll();
            }
        }

        public void ShowProblems()
        {
        }


        public void Update()
        {
            try
            {
                ListView lv = this.listView;
                ListView lv2 = this.listView2;

                HourGlass.Set();

                lv.BeginUpdate();
                lv.Items.Clear();
                UpdateColumnHeaders(lv);

                if (duplicateListViewIsOn)
                {
                    lv2.BeginUpdate();
                    lv2.Items.Clear();
                    UpdateColumnHeaders(lv2);
                }

                if (ManagerView._Module is Manager)
                {
                    Tsunami2.Preferences.Values.ElementManagerPositionCounter = 0;

                    string sel = (this.Module._SelectedObjectInBlue != null) ? this.Module._SelectedObjectInBlue._Name : "null";
                    //TSU.Debug.WriteInConsole(this.ModuleView.Name + " Updating...    there is " + this.Module._SelectedObjects.Count +
                    //    " object precheck and " + sel +
                    //    " is preselected\r\n");

                    // hide or not the listview2
                    {
                        lv2.Visible = duplicateListViewIsOn;
                        SplitContainer sc = null;
                        if (lv.Parent != null && lv.Parent.Parent != null)
                            sc = lv.Parent.Parent as SplitContainer;
                        if (sc != null)
                        {
                            if (duplicateListViewIsOn)
                            {
                                sc.Panel2Collapsed = false;
                                sc.Panel2.Show();

                                sc.SplitterDistance = sc.Width * 3 / 4;
                            }
                            else
                            {
                                sc.Panel2Collapsed = true;
                                sc.Panel2.Hide();
                            }
                        }
                    }
                    isUpdating = true;

                    // save the Top node position
                    object o = null;
                    if (lv.TopItem != null) o = lv.TopItem.Tag;

                    // show or hide checkboxes for multiselection
                    lv.MultiSelect = MultiSelection;
                    lv.CheckBoxes = ManagerView.CheckBoxesVisible;
                    if (duplicateListViewIsOn)
                    {
                        lv2.MultiSelect = MultiSelection;
                        lv2.CheckBoxes = ManagerView.CheckBoxesVisible;
                    }

                    // Clear list
                    this.filteredListViewItems.Clear();
                    

                    // Fill it back
                    TreatPreSelectSelectableObject(out TsuListViewItem blue);

                    // filter It
                    if (filterBoxView.Text == _filterDefaultText)
                        filterBoxView.ForeColor = Tsunami2.Preferences.Theme.Colors.Object;
                    else
                        filterBoxView.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;

                    Filter(this.filterBoxView, null);

                    // Restore top node
                    ListViewItem itemToPutOnTop = null;
                    if (o != null)
                        foreach (ListViewItem item in lv.Items)
                            if (item.Tag == o)
                            {
                                itemToPutOnTop = item;
                                break;
                            }
                    if (itemToPutOnTop != null) lv.TopItem = itemToPutOnTop;

                    // Formatage
                    ColorByOriginGroup();

                    this.Module._SelectedObjects.Clear();
                    foreach (var item in filteredListViewItems)
                    {
                        lv.Items.Add(item);
                        if (duplicateListViewIsOn)
                            lv2.Items.Add(item.Clone() as ListViewItem);
                    }
                    var a = this.Module._SelectedObjects;
                    HideEmptyColumns();

                    // put the selected object in blue
                    {
                        lv.FocusedItem = blue;

                    }

                    lv.EndUpdate();
                    lv.ResumeLayout(true);
                    lv.Visible = true;
                    if (duplicateListViewIsOn)
                    {
                        lv2.EndUpdate();
                        lv2.ResumeLayout(true);
                    }
                    ScrollHorizontal(lv2.Handle, 30000); // all the wayyyy

                    isUpdating = false;

                    //lv.IsUpdating = false;
                    //lv2.IsUpdating = false;

                    sel = (this.Module._SelectedObjectInBlue != null) ? this.Module._SelectedObjectInBlue._Name : "null";
                    TSU.Debug.WriteInConsole(this.ManagerView.Name + " ...Updated,   there is " + this.Module._SelectedObjects.Count +
                        " object precheck and " + sel +
                        " is preselected\r\n");

                    TSU.Debug.WriteInConsole("Listview has " + lv.CheckedItems.Count + " item checked\r\n");
                    lv.Refresh();

                    TSU.Debug.WriteInConsole("Listview has " + lv.CheckedItems.Count + " item checked\r\n");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(R.T532 + ex.Message, ex);
            }
            finally
            {
                HourGlass.Remove();
                this.ShowSelectionInButton();
            }
        } // updated

        #region to scroll horizaontally
        const Int32 LVM_FIRST = 0x1000;
        const Int32 LVM_SCROLL = LVM_FIRST + 20;

        [System.Runtime.InteropServices.DllImport("user32")]
        static extern IntPtr SendMessage(IntPtr Handle, Int32 msg, IntPtr wParam,
        IntPtr lParam);

        private void ScrollHorizontal(IntPtr controlHandle, int pixelsToScroll)
        {
            Debug.WriteInConsole("ScrollHorizontal(IntPtr controlHandle, int pixelsToScroll)");
            SendMessage(controlHandle, LVM_SCROLL, (IntPtr)pixelsToScroll,
            IntPtr.Zero);
        }

        #endregion

        private void ColorByOriginGroup()
        {
            if (this.Module is E.Manager.Module)
            {
                List<string> list = new List<string>();
                foreach (TsuListViewItem item in listView.Items)
                {
                    if (item.State == StateType.NotReady) continue; // to keep the bad in red
                    Element e = item.Tag as Element;
                    if (!e._Origin.Contains(':'))
                    {
                        int index = list.FindIndex(x => x == e._Origin);
                        if (index == -1)
                        {
                            index = list.Count;
                            list.Add(e._Origin);
                        }
                        if (Tsunami2.Preferences.Values.DarkRainBow.Count > index)
                            item.ForeColor = Tsunami2.Preferences.Values.DarkRainBow[index];
                        else
                        {
                            Random rnd = new Random();
                            item.ForeColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
                        }
                    }
                }
            }
        }

        internal virtual void HideEmptyColumns()
        {

            TsuListView lv = this.listView as TsuListView;
            TsuListView lv2 = this.listView2 as TsuListView;

            if (!this.ManagerView.HideEmptyColumns)
            {
                TryAutoResizeColumns(lv, ColumnHeaderAutoResizeStyle.HeaderSize);
                return;
            }

            // to avoid infinite scroll update from one lv to the other
            lv.Scroll -= On_ScrollVertical;
            lv2.Scroll -= On_ScrollVertical;

            TryAutoResizeColumns(lv, ColumnHeaderAutoResizeStyle.ColumnContent); // columns empty will get a width of 25, except if the content is 1 or 2 char it also 25

            lv.Refresh();


            bool[] hiddenColumns = new bool[lv.Columns.Count];

            for (int i = 0; i < lv.Columns.Count; i++)
            {
                hiddenColumns[i] = false;

                if (lv.Columns[i].Width < 26 && lv.Columns[i].Text != "Element")
                    hiddenColumns[i] = true;
            }
            TryAutoResizeColumns(lv, ColumnHeaderAutoResizeStyle.HeaderSize);
            for (int i = 0; i < lv.Columns.Count; i++)
            {
                if (hiddenColumns[i])
                    lv.Columns[i].Width = 0;
            }

            if (duplicateListViewIsOn)
            {
                TryAutoResizeColumns(lv2, ColumnHeaderAutoResizeStyle.ColumnContent); 
                for (int i = 0; i < lv2.Columns.Count; i++)
                {
                    if (hiddenColumns[i])
                        lv2.Columns[i].Width = 0;
                }


                lv2.Scroll += On_ScrollVertical;
            }
            lv.Scroll += On_ScrollVertical;
        }

        private void TryAutoResizeColumns(TsuListView lv, ColumnHeaderAutoResizeStyle columnContent)
        {
            try
            {
                lv.AutoResizeColumns(columnContent);
            }
            catch (Exception)
            {
            }
        }

        public void Focus()
        {
            this.listView.Focus();
        }
        public void SetSelectedInBlue()
        {
            ListView lv = this.listView;
            if (this.Module._SelectedObjectInBlue != null && lv.FocusedItem != null)
            {
                lv.Focus();
                lv.FocusedItem.Selected = true;
            }
        }

        List<object> correctedPreselectedObject;

        /// <summary>
        /// // Check the preselect object from the list
        /// </summary>
        private void TreatPreSelectSelectableObject(out TsuListViewItem selectedInBlueItem)
        {
            int counter1 = 0;
            int counter2 = 0;
            selectedInBlueItem = null;

            // Put all as selectable vased on the view setting
            if (this.Module.SelectableObjects.Count == 0
               && this.Module is E.Manager.Module e
               && !(this is EditableList)
               && e.View != null
               && e.View.ElementShownInListType != E.Manager.View.ElementShownInListTypes.Unknown)
            {
                SetSelectable(e);
            }

            // Put all as selectable is nothing is selecteable
            if (this.Module.SelectableObjects.Count == 0)
            {
                if (this.Module.AllElements == null) this.Module.AllElements = new List<TsuObject>();
                //TODO clean this exception
                List<TsuObject> allElementExceptFileName = this.Module.AllElements.FindAll(x => !(x is TheoreticalElement));
                this.Module.SelectableObjects.AddRange(allElementExceptFileName);
                //this.Module.SelectableObjects.AddRange(this.Module.AllElements);
            }

            CorrectPreselectedList();

            //Sorting the points
            List<E.Point> listOfPoint = this.Module.SelectableObjects.OfType<E.Point>().ToList().OrderBy(point => point.Date).ToList();
            List<TsuObject> otherElementsList = this.Module.SelectableObjects.Where(obj => !(obj is E.Point)).ToList();

            otherElementsList.AddRange(listOfPoint);
            this.Module.SelectableObjects = otherElementsList;


            foreach (var element in this.Module.SelectableObjects)
            {
                TsuListViewItem item = new TsuListViewItem(element);

                if (correctedPreselectedObject.Contains(element))
                {
                    item.Checked = true;
                    counter1++;
                }
                else
                {
                    item.Checked = false;
                    counter2++;
                }
                filteredListViewItems.Add(item);


                if (this.Module._SelectedObjectInBlue == element)
                {
                    selectedInBlueItem = item;
                }
            }


            TSU.Debug.WriteInConsole(this.ManagerView.Name + " has cross-checking " + counter1 + " selected objects and " + counter2 + " not selected object \r\n");
        }

        private void SetSelectable(E.Manager.Module em)
        {
            switch (em.View.ElementShownInListType)
            {

                case E.Manager.View.ElementShownInListTypes.Elements: em.SetSelectableToAllElements(); break;
                case E.Manager.View.ElementShownInListTypes.Points: em.SetSelectableToAllUniquePoint(); break;
                case E.Manager.View.ElementShownInListTypes.Alesages: em.SetSelectableListToAllAlesages(); break;
                case E.Manager.View.ElementShownInListTypes.Magnets: em.SetSelectableToAllMagnets(); break;
                case E.Manager.View.ElementShownInListTypes.Sequences: em.SetSelectableToAllSequence(); break;
                case E.Manager.View.ElementShownInListTypes.SequencesFil: em.SetSelectableToAllSequenceFil(); break;
                case E.Manager.View.ElementShownInListTypes.SequencesTH: em.SetSelectableToAllSequenceTH(); break;
                case E.Manager.View.ElementShownInListTypes.SequencesNiv: em.SetSelectableToAllSequenceNiv(); break;
                case E.Manager.View.ElementShownInListTypes.TheoreticalPoints: em.SetSelectableToAllTheoreticalPoints(); break;
                case E.Manager.View.ElementShownInListTypes.UniquePoints: em.SetSelectableToAllUniquePoint(); break;
                case E.Manager.View.ElementShownInListTypes.Pilier: em.SetSelectableToAllPilier(); break;
                case E.Manager.View.ElementShownInListTypes.Shapes:
                case E.Manager.View.ElementShownInListTypes.Unknown:
                default:
                    break;
            }
        }

        /// <summary>
        /// This will (or not) add children object of the selected one to the corrected list
        /// </summary>
        private void CorrectPreselectedList()
        {
            TSU.Debug.WriteInConsole(this.ManagerView.Name + " is preselecting: " + this.Module._SelectedObjects.Count + " objects \r\n");
            correctedPreselectedObject = new List<object>();
            foreach (var item in this.Module._SelectedObjects)
            {
                correctedPreselectedObject.Add(item);
                if (this.ManagerView.AutoCheckChildren && item is CompositeElement ce)
                {
                    foreach (E.Point point in ce.GetPoints())
                    {
                        correctedPreselectedObject.Add(point);
                    }
                }
            }
        }

        #endregion

        #region ListView



        #region fields

        internal ListView listView;
        internal ListView listView2;
        //private ListView.ListViewItemCollection allListViewItems;
        private readonly List<ListViewItem> filteredListViewItems = new List<ListViewItem>();
        private bool DescendantSorting;
        public void Initialize()
        {
            listView = GetInitializedListView();
            listView.Items.AddRange(filteredListViewItems.ToArray());
            listView.ItemSelectionChanged += On_ItemSelectionChanged;
            listView.ItemCheck += On_ItemCheck;


            listView2 = GetInitializedListView();
            listView2.ItemSelectionChanged += On_ItemSelectionChanged;
            listView2.ItemCheck += On_ItemCheck;
            listView.AllowColumnReorder = true;
            listView2.AllowColumnReorder = true;

            InitializeFilterView();
        }

        bool beingChecked = false;
        private void On_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            Debug.WriteInConsole($"ListView.On_ItemCheck beingChecked={beingChecked} e.Index={e.Index} e.NewValue={e.NewValue} e.CurrentValue={e.CurrentValue}");
            StopReverseCheckTimer();
            ListView lv = (ListView)sender;

            int selectedIndex = e.Index;
            Debug.WriteInConsole($"lv.Items.Count={lv.Items.Count}");
            if (!beingChecked)
            {
                if (lv.Items.Count > selectedIndex)
                {
                    beingChecked = true;
                    TsuListViewItem item = lv.Items[selectedIndex] as TsuListViewItem;
                    item.Choosen = e.NewValue == CheckState.Checked;
                    if (item.Choosen)
                        AddToSelection(item);
                    else
                        RemoveFromSelection(item);
                    ShowSelectionInButton();
                    Module.Change();
                    lv.Refresh();
                }
                beingChecked = false;
            }
        }

        bool selectionReflectionInProgress = false;
        private void On_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            Debug.WriteInConsole($"ListView.On_ItemSelectionChanged isUpdating={isUpdating} selectionReflectionInProgress={selectionReflectionInProgress} e.Index={e.ItemIndex} e.isSelected={e.IsSelected}");
            if (isUpdating) return;
            if (selectionReflectionInProgress) return;

            BeginUpdate();

            ListView lv;
            if (sender == this.listView2)
                lv = this.listView2;
            else
                lv = this.listView;

            int selectedIndex = e.ItemIndex;
            Debug.WriteInConsole($"selectedIndex={selectedIndex} lv.Items.Count={lv.Items.Count}");
            if (lv.Items.Count > selectedIndex)
            {
                selectionReflectionInProgress = true;
                lv.Items[selectedIndex].Selected = e.IsSelected;
                selectionReflectionInProgress = false;
            }

            EndUpdate();
        }

        private ListView GetInitializedListView()
        {
            // Work on the list view properties
            TsuListView lv = new TsuListView();
            validateSelectionTimer = new Timer() { Enabled = false, Interval = 30 };
            validateSelectionTimer.Tick += delegate
            {
                TSU.Debug.WriteInConsole("Listview will trigger 'change' if not updating\r\n");
                if (!isUpdating)
                {
                    this.ValidateSelection();
                    validateSelectionTimer.Stop();
                }
            };
            lv.MultiSelect = true;
            lv.FullRowSelect = true;
            lv.AllowDrop = true;
            lv.ColumnClick += new ColumnClickEventHandler(this.On_ListView_Elements_ColumnClick);
            lv.DoubleClick += new EventHandler(On_ListView_DoubleClick);
            lv.ItemMouseHover += On_ListView_MouseHover;
            lv.MouseDown += new MouseEventHandler(On_ListView_MouseDown);
            lv.MouseUp += new MouseEventHandler(On_ListView_MouseUp);
            lv.MouseMove += new MouseEventHandler(On_ListView_MouseMove);
            lv.DragLeave += new EventHandler(On_ListView_Drag);
            lv.ItemDrag += new ItemDragEventHandler(On_ListView_DragStart);
            lv.KeyDown += new KeyEventHandler(On_ListView_keyDown);
            lv.KeyUp += new KeyEventHandler(On_ListView_keyUp);
            lv.View = View.Details;
            lv.HideSelection = false;

            UpdateColumnHeaders(lv);

            // Create the 2 list of possible item list to put in the list view

            lv.Click += On_ListViewClicked;

            lv.DoubleClick += delegate { this.On_DoubleClick(this); };
            lv.Scroll += On_ScrollVertical;

            lv.MouseWheel += On_MouseWheel; //this fucking event trigger before the scroll is applied, i dont find other way than a timer

            // should disabled not to trigger evetns
            lv.Enabled = false;
            lv.CheckBoxes = true;
            lv.Enabled = true;

            lv.Visible = true;
            lv.MultiSelect = MultiSelection;
            lv.Dock = DockStyle.Fill;

            return lv;
        }

        private void On_ItemSelectionChanged(object sender, System.EventArgs e)
        {
           var lv = (ListView)sender;
            if (lv.SelectedIndices.Count > 1 )
            {
                
            }
        }

        private void UpdateColumnHeaders(ListView lv)
        {

            this.isUpdating = true;
            lv.Columns.Clear();
            // Fill the column Names
            int count = 0;
            foreach (string header in ManagerView.ListviewColumnHeaderTexts)
            {
                HorizontalAlignment align;
                if (header.Contains("["))
                {
                    align = HorizontalAlignment.Right;
                }
                else
                    align = HorizontalAlignment.Left;
                ColumnHeader ch = new ColumnHeader() { Text = header, Width = 1, TextAlign = align, };
                lv.Columns.Add(ch);

                count++;
            }
            this.isUpdating = false;


        }

        Timer mouseWheelTimer;
        private void On_MouseWheel(object sender, MouseEventArgs e)
        {
            Debug.WriteInConsole("ListView.On_MouseWheel");
            if (mouseWheelTimer == null)
                mouseWheelTimer = new Timer() { Enabled = true, Interval = 500 };
            else
                mouseWheelTimer.Start();

            mouseWheelTimer.Tick += delegate
            {
                SynchroniseVerticalScroll(sender as ListView);
                mouseWheelTimer.Stop();
                mouseWheelTimer.Dispose();
            };
        }

        private void On_ScrollVertical(object sender, ScrollEventArgs e)
        {
            Debug.WriteInConsole("ListView.On_ScrollVertical");
            SynchroniseVerticalScroll(sender as ListView);
        }

        private void SynchroniseVerticalScroll(ListView scrolledListView)
        {
            if (!duplicateListViewIsOn) return;

            SynchroniseVerticalScroll(scrolledListView, listView, listView2);

        }

        private void SynchroniseVerticalScroll(ListView scrolledListView, ListView listView, ListView listView2)
        {
            if (listView == null) return;
            if (listView2 == null) return;
            if (scrolledListView.TopItem == null) return;

            BeginUpdate();

            int index = scrolledListView.TopItem.Index;
            listView.TopItem = listView.Items[index];
            listView2.TopItem = listView2.Items[index];

            EndUpdate();
        }

        ListViewItem listViewItemHovered;

        private void On_ListView_MouseHover(object sender, ListViewItemMouseHoverEventArgs e)
        {
            try
            {
                if (keyisdown) return;

                System.Drawing.Point p = e.Item.ListView.PointToClient(Cursor.Position);
                if (!e.Item.SubItems[1].Bounds.Contains(p))
                {
                    this.ManagerView.HidePopupMenu();
                    return;
                }

                if (listViewItemHovered != e.Item)
                {
                    if (Tsunami2.Preferences.Values.GuiPrefs.ShowDescriptionWhenHooveringAListView.IsTrue)
                    {
                        listViewItemHovered = e.Item;
                        string s = "";
                        if (e.Item.Tag is E.Point pt)
                            s = pt.ToString(verbose: true);
                        else
                            s = e.Item.Tag.ToString();
                        var font = Tsunami2.Preferences.Theme.Fonts.Normal;
                        Label l = new Label()
                        {
                            Text = s,
                            ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground,
                            Font = font,
                            //AutoSize = true,
                            Visible = true,
                            //Dock = DockStyle.Top,
                            //Location = new System.Drawing.Point(0, 0),
                        };

                        var preferedSize = TextRenderer.MeasureText(s, font);
                        l.MinimumSize = preferedSize;

                        this.ManagerView.ShowPopUpMenu(new List<Control>() { l }, "EM hoover");
                    }
                }
                else
                {
                    this.ManagerView.HidePopupMenu();
                }
            }
            catch (Exception ex)
            {
                Logs.Log.AddEntry(this.Module, ex);
            }
        }


        #endregion

        #region selection

        public override List<TsuObject> GetSelectedElements()
        {
            ListView lv = this.listView;
            List<TsuObject> l = new List<TsuObject>();
            if (this.MultiSelection)
            {
                foreach (ListViewItem item in lv.CheckedItems)
                    if (item.Tag != null)
                        l.Add(item.Tag as TsuObject);
            }
            else if (lv.SelectedItems.Count > 0)
                l.Add(lv.SelectedItems[0].Tag as TsuObject);

            return l;
        }

        private void BeginUpdate()
        {
            ListView lv = this.listView;
            ListView lv2 = this.listView2;

            lv.BeginUpdate();
            lv2.BeginUpdate();
        }

        private void EndUpdate()
        {
            ListView lv = this.listView;
            ListView lv2 = this.listView2;

            lv.ResumeLayout(true);
            lv2.ResumeLayout(true);
            lv.EndUpdate();
            lv2.EndUpdate();
        }

        private void SelectAll()
        {
            BeginUpdate();
            foreach (ListViewItem item in filteredListViewItems)
            {
                AddToSelection(item);
                item.Checked = true; //@"was true
            }
            EndUpdate();

            this.ShowSelectionInButton();
            this.Module.Change();
        }

        public void DeselectAll()
        {
            BeginUpdate();
            foreach (TsuListViewItem item in filteredListViewItems.Cast<TsuListViewItem>())
            {
                item.Choosen = false;
                RemoveFromSelection(item);
                item.Checked = false;
            }
            EndUpdate();

            this.ShowSelectionInButton();
            this.Module.Change();
        }

        private void InvertSelection()
        {
            BeginUpdate();
            foreach (ListViewItem item in filteredListViewItems)
            {
                if (item.Checked)
                {
                    RemoveFromSelection(item);
                    item.Checked = false;
                }
                else
                {
                    AddToSelection(item);
                    item.Checked = true;
                }

            }
            EndUpdate();
            this.ShowSelectionInButton();
            this.Module.Change();
        }



        #endregion

        #region Events
        bool keyisdown = false;
        private void On_ListView_keyDown(object sender, KeyEventArgs e)
        {
            Debug.WriteInConsole("ListView.On_ListView_keyDown");
            keyisdown = true;
            if (e.Control)
            {
                switch (e.KeyCode)
                {
                    case Keys.A:
                        SelectAll();
                        break;
                    case Keys.D:
                        DeselectAll();
                        break;
                    case Keys.I:
                        InvertSelection();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (e.KeyCode)
                {
                    case Keys.Return:
                        ValidateSelection();
                        break;
                    case Keys.Shift: break;
                    case Keys.ShiftKey: break;
                    case Keys.Control: break;
                    case Keys.ControlKey: break;
                    default:
                        {
                            var t = filterBoxView;
                            if (!t.Focused)
                            {
                                t.Focus();
                                string s = e.KeyCode.ToString();
                                if (s.Length == 1) // make sure it is a char and not BACk DELETE SHIFT etc
                                    t.Text = e.KeyCode.ToString();
                                else
                                    t.Text = "";

                                t.SelectionStart = t.Text.Length;
                                t.SelectionLength = 0;
                            }
                        }
                        break;
                }
            }
        }

        private void On_ListView_keyUp(object sender, KeyEventArgs e)
        {
            Debug.WriteInConsole("ListView.On_ListView_keyUp");
            keyisdown = false;
        }


        //Clicked
        private void On_ListViewClicked(object sender, System.EventArgs e)
        {
            Debug.WriteInConsole("ListView.On_ListViewClicked");
            listView.BeginUpdate();
            listView2.BeginUpdate();
            ListView lv = sender as ListView;
            ListViewItem clickedItem = GetItemFromPoint(lv, Cursor.Position, out int columnIndex);

            Debug.WriteInConsole($"clickedItem == null {clickedItem == null} columnIndex {columnIndex}");

            if (clickedItem == null)
                return;

            Debug.WriteInConsole($"clickedItem.Tag {clickedItem.Tag} clickedItem.Name {clickedItem.Name} clickedItem.Index {clickedItem.Index} clickedItem.Checked {clickedItem.Checked}");

            this.Module.EditByClickingSubItem = true;
            bool subItemEdition = this.Module.EditByClickingSubItem && columnIndex > 0;
            bool clickShouldChangeSelection = !subItemEdition;

            if (clickShouldChangeSelection)
                StartReverseCheckTimer(clickedItem);

            if (subItemEdition)
                this.Module.EditItem(clickedItem.Tag, clickedItem.SubItems[columnIndex].Tag);

            if (Control.ModifierKeys.HasFlag(Keys.Shift))
            {
                int checkedCount = 0;
                foreach (ListViewItem item in lv.SelectedItems)
                {
                    if (item.Checked)
                        checkedCount++;
                }
                bool morechecked = checkedCount > lv.SelectedItems.Count / 2;
                foreach (ListViewItem item in lv.SelectedItems)
                {
                    if (morechecked)
                    {
                        item.Checked = false;
                    }
                    else
                    {
                        item.Checked = true;
                    }
                }
            }
            this.ShowSelectionInButton();
            this.Module.Change();

            listView.EndUpdate();
            listView2.EndUpdate();
        }

        private Timer reverseCheckTimer = null;

        private void StartReverseCheckTimer(ListViewItem clickedItem)
        {
            StopReverseCheckTimer();

            reverseCheckTimer = new Timer() { Enabled = true, Interval = 30 };
            reverseCheckTimer.Start();

            reverseCheckTimer.Tick += delegate
            {
                ReverseCheck(clickedItem);
            };
        }

        private void StopReverseCheckTimer()
        {
            if (reverseCheckTimer != null)
            {
                reverseCheckTimer.Stop();
                reverseCheckTimer.Dispose();
                reverseCheckTimer = null;
            }
        }

        private void ReverseCheck(ListViewItem clickedItem)
        {
            TsuListViewItem item = clickedItem as TsuListViewItem;
            item.Checked = !item.Checked;
        }

        private void RemoveFromSelection(ListViewItem clickedItem)
        {
            TsuObject o = clickedItem.Tag as TsuObject;
            Debug.WriteInConsole($"RemoveFromSelection TagName={o._Name} TagId={o.Id} Contained={this.Module._SelectedObjects.Contains(o)}");
            if (this.Module._SelectedObjects.Contains(o))
            {
                this.ManagerView.selectionChanged = true;
                this.Module._SelectedObjects.Remove(o);
            }
            this.Module._SelectedObjectInBlue = null;
        }

        private void AddToSelection(ListViewItem clickedItem)
        {
            TsuObject o = clickedItem.Tag as TsuObject;
            Debug.WriteInConsole($"AddToSelection TagName={o._Name} TagId={o.Id} Contained={this.Module._SelectedObjects.Contains(o)}");
            if (!this.Module._SelectedObjects.Contains(o))
            {
                this.ManagerView.selectionChanged = true;
                this.Module._SelectedObjects.Add(o);
            }
            this.Module._SelectedObjectInBlue = o;
        }

        private ListViewItem GetItemFromPoint(ListView listView, System.Drawing.Point mousePosition, out int columnIndex)
        {
            // translate the mouse position from screen coordinates to 
            // client coordinates within the given ListView
            System.Drawing.Point localPoint = listView.PointToClient(mousePosition);
            ListViewItem item = listView.GetItemAt(localPoint.X, localPoint.Y);

            ListViewHitTestInfo hitTest = listView.HitTest(localPoint);
            columnIndex = hitTest.Item.SubItems.IndexOf(hitTest.SubItem);

            return item;
        }

        private void On_ListView_Elements_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            Debug.WriteInConsole("ListView.On_ListView_Elements_ColumnClick");
            ListView lv1 = this.listView;
            ListView lv2 = this.listView2;
            TSU.Debug.WriteInConsole("Listview has " + lv1.CheckedItems.Count + " item checked\r\n");
            lv1.ListViewItemSorter = new ListViewItemComparer(e.Column, DescendantSorting);
            lv2.ListViewItemSorter = new ListViewItemComparer(e.Column, DescendantSorting);
            DescendantSorting = !DescendantSorting;
        }

        private void On_ListView_DragStart(object sender, ItemDragEventArgs e)
        {
            Debug.WriteInConsole("ListView.On_ListView_DragStart");
            dragedName = ((ListView)sender).FocusedItem.SubItems[1].Text + "&" + ((ListView)sender).FocusedItem.Text;
            ((ListView)sender).DoDragDrop(dragedName, DragDropEffects.Copy);
        }

        private string dragedName;

        private void On_ListView_Drag(object sender, System.EventArgs e)
        {
            Debug.WriteInConsole("ListView.On_ListView_Drag");
            try
            {
                dragedName = ((ListView)sender).FocusedItem.Text;
            }
            catch (Exception)
            {

            }
        }

        internal virtual void On_ListView_DoubleClick(object sender, System.EventArgs e)
        {
            Debug.WriteInConsole("ListView.On_ListView_DoubleClick");
            ValidateSelection();
            if (!MultiSelection)
            {
                if (ManagerView._SelectionView != null)
                    ManagerView._SelectionView.Hide();

            }
            else
            {
                var lv = (ListView)sender;
                if (lv.FocusedItem != null)
                {
                    if (lv.FocusedItem.Checked)
                    {
                        AddToSelection(lv.FocusedItem);
                    }
                    else
                    {
                        RemoveFromSelection(lv.FocusedItem);
                    }
                }
            }
        }

        internal virtual void On_ListView_MouseDown(object sender, MouseEventArgs e)
        {
            Debug.WriteInConsole("ListView.On_ListView_MouseDown");
        }

        internal virtual void On_ListView_MouseUp(object sender, MouseEventArgs e)
        {
            Debug.WriteInConsole("ListView.On_ListView_MouseUp");

            //ListView listview = sender as ListView;
            //if (listview.Tag == null) return;

            //this.ManagerView.DealWithClickedItem(listview.Tag);
        }

        internal virtual void On_ListView_MouseMove(object sender, MouseEventArgs e)
        {
            //if (mouseDown)
            //{
            //    var point = listView.PointToClient(new System.Drawing.Point(e.X, e.Y));
            //    ListViewItem item = listView.GetItemAt(point.X, point.Y);
            //    for (int i = startingItem.Index; i < item.Index; i++)
            //    {
            //        listView.Items[i].Selected = true;
            //    }
            //}

        }

        #endregion

        #endregion

        #region Selection

        /// <summary>
        /// This Method will fill the selection list with the tsuobject from the checked, selected or new item.
        /// </summary>
        public virtual void ValidateSelection()
        {
            if (!MultiSelection)
            {
                var blues = GetSelectedInBlue();
                if (blues.Count == 0)
                {
                    new MessageInput(MessageType.Warning, R.T_NOTHING_SELECTED).Show();
                    throw new TsuCancelException(R.T_NOTHING_SELECTED);
                }

                this.Module._SelectedObjectInBlue = blues[0];
                this.Module._SelectedObjects = blues;
            }
        }

        public void CancelSelection()
        {
            (this.ManagerView._Module as Manager)._SelectedObjects.Clear();
        }

        public List<TsuObject> GetChecked()
        {
            List<TsuObject> l = new List<TsuObject>();
            foreach (ListViewItem item in this.listView.CheckedItems)
                l.Add(item.Tag as TsuObject);
            return l;
        }

        public List<TsuObject> GetSelectedInBlue()
        {
            List<TsuObject> l = new List<TsuObject>();
            foreach (ListViewItem item in this.listView.SelectedItems)
                l.Add(item.Tag as TsuObject);
            return l;
        }
        #endregion

        #region Filter
        private TextBox filterBoxView;
        private string _filterDefaultText;
        //private int _filterPreviousLength;
        private void Filter(object sender, System.EventArgs e)
        {
            string filterLine = ((TextBox)sender).Text;
            string partAND = "";

            // Function called if we want to do the filtration
            Action DoFiltration = () =>
            {
                List<string> partsOR = partAND.Split('|', '#').ToList();

                List<ListViewItem> tobeRemoved = new List<ListViewItem>();
                foreach (ListViewItem item in filteredListViewItems)
                {
                    if (item.Checked) continue;
                    bool partFound = false;
                    foreach (var part in partsOR)
                    {
                        if (!this.ItemContainsText(item, part))
                            partFound = partFound || false;
                        else partFound = true;
                    }
                    if (!partFound) 
                        tobeRemoved.Add(item);
                }
                foreach (var item in tobeRemoved)
                {
                    filteredListViewItems.Remove(item);
                }
            };

            if (filterLine != _filterDefaultText && filterLine != "")
            {
                TSU.Debug.WriteInConsole(this.ManagerView.Name + " is use the text filter (" + filterLine + ") on " + filteredListViewItems.Count + " objects \r\n");
                List<string> partsAND = filterLine.Split('+', '&').ToList();

                foreach (string item in partsAND)
                {
                    partAND = item;
                    DoFiltration();
                }
            }
        }

        private void EnterFilter(object sender, System.EventArgs e)
        {
            //CloneAllItemsIntoFilteredList();
            TextBox t = sender as TextBox;
            if (t.Text == _filterDefaultText)
            {
                t.Text = "";
            }
            t.Focus();
            t.SelectAll();
        }

        private void LeaveFilter(object sender, System.EventArgs e)
        {

            TextBox t = sender as TextBox;
            if (t.Text == "") t.Text = _filterDefaultText;

            if (t.Text.Length > advice.Length && advice != "")
                if (t.Text.Substring(0, advice.Length) == advice)
                {
                    this.Module.AddElementFromText("Filter", t.Text.Substring(advice.Length));
                    t.Text = _filterDefaultText;
                    advice = "";
                    if (this.Module is E.Manager.Module)
                    {
                        this.Module.SelectableObjects.Clear();
                        (this.Module as E.Manager.Module).SetSelectableToAllPoints();
                    }
                    this.Update();
                }
            t.Select(0, 0);
        }

        public void InitializeFilterView()
        {
            _filterDefaultText = R.T_defaultFilter;
            TextBox tb = new TextBox
            {
                //Parent = this.ModuleView,
                Font = Tsunami2.Preferences.Theme.Fonts.Normal,
                Text = _filterDefaultText,
                HideSelection = false,
                Dock = DockStyle.Top,
                //tb.Show();
                AllowDrop = true
            };
            tb.BringToFront();

            tb.TextChanged += OnFilterChanged;
            tb.Enter += new EventHandler(EnterFilter);
            tb.Leave += new EventHandler(LeaveFilter);
            //tb.DragEnter += new DragEventHandler(On_TextBox_DragEnterp);
            tb.DragDrop += new DragEventHandler(On_TextBox_DragDrop);
            tb.KeyDown += new KeyEventHandler(On_ListView_keyDown);

            tb.KeyDown += new KeyEventHandler(On_TextBox_key_down);
            this.filterBoxView = tb;
            this.filterTimer.Tick += delegate { this.filterTimer.Stop(); this.Update(); };
        }



        private string advice = "";
        private readonly string addingAdvice = "+(adding element i.e. ID X Y Z): ";


        readonly Timer filterTimer = new Timer() { Interval = 800 };
        private void OnFilterChanged(object sender, System.EventArgs e)
        {
            Debug.WriteInConsole("ListView.OnFilterChanged");
            if (advice == addingAdvice) return;
            string token = (sender as TextBox).Text;
            if (token == _filterDefaultText) return;
            // if (token == "") return; why was this? it set then clearing the filter will not show everything

            filterTimer.Stop();
            filterTimer.Start(); // this will launch update in 800ms if filter not reset by new key hit.
        }

        private void On_TextBox_key_down(object sender, KeyEventArgs e)
        {
            Debug.WriteInConsole("ListView.On_TextBox_key_down");
            switch (e.KeyCode)
            {

                case Keys.Return:
                    LeaveFilter(filterBoxView, new System.EventArgs());
                    break;
                case Keys.Add:
                    if (filterBoxView.Text.Length == 0)
                    {
                        advice = addingAdvice;
                        filterBoxView.Text = advice;
                        filterBoxView.SelectionStart = filterBoxView.Text.Length;
                        e.SuppressKeyPress = true;
                    }
                    break;
                default:
                    break;
            }
        }


        public bool ItemContainsText(ListViewItem item, string textSeeked)
        {

            textSeeked = textSeeked.ToLower();

            bool allWordsFound = true;

            foreach (string word in textSeeked.Split('&'))
            {
                bool wordFoundInOneOfTheColumns = false;

                bool wordContainsAnEqual = word.Contains('='); // this mean we want to look ina given column that start with the left part of the equal
                string seeked;
                string beginningOfColumnOfInterest = "";
                if (wordContainsAnEqual)
                {
                    string[] parts = textSeeked.Split('=');
                    beginningOfColumnOfInterest = parts[0].Trim();
                    seeked = parts[1].Trim();
                }
                else
                {
                    seeked = word.Trim();
                }

                for (int i = 0; i < item.SubItems.Count; i++)
                {
                    if (wordContainsAnEqual)
                    {
                        if (listView.Columns[i].Text.Substring(0, beginningOfColumnOfInterest.Length).ToLower() != beginningOfColumnOfInterest.ToLower())
                            continue;
                    }
                    if (item.SubItems[i].Text.ToLower().Contains(seeked))
                        wordFoundInOneOfTheColumns = true;
                }
                allWordsFound = allWordsFound && wordFoundInOneOfTheColumns;

            }
            return allWordsFound;
        }

        private void On_TextBox_DragDrop(object sender, DragEventArgs e)
        {
            Debug.WriteInConsole("ListView.On_TextBox_DragDrop");
            filterBoxView.Text = e.Data.GetData(DataFormats.Text).ToString();
            ((TextBox)sender).Focus();
        }

        public void ShowSelection()
        {
            //foreach (var item in this.selectedListViewItemsIndexes)
            //{
            //    this.listView.Items[item].Selected = true;

            //}
            //this.listView.Focus();
            //this.Update();
        }

        #endregion

        public int GetRecommandedHeight()
        {
            if (listView.Items.Count > 0)
            {
                Rectangle itemRect = listView.GetItemRect(0);
                return 10 + itemRect.Top + (itemRect.Height * listView.Items.Count);
            }
            else
            {
                // Will use the minum size defined in TsuView.ShowInMessageTsu
                return 0;
            }
        }
    }
}
