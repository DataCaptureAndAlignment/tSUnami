﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using TSU.Preferences;
using TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using TSU.Views;
using TSU.ENUM;
using TSU.Tools;
using E = TSU.Common.Elements;
using Z = TSU.Common.Zones;
using F = TSU.Functionalities;
using M = TSU;
using O = TSU.Common.Operations;
using T = TSU.Tools;
using TS = TSU.Common.Compute.Transformation.Systems;

using TSU.Common.Measures;
using TSU.Views.Message;
using R = TSU.Properties.Resources;


namespace TSU.Common.Strategies.Views
{
    public class EditableList : List
    {
        private TSU.Common.Elements.Point pointBeforeModification;
        private TSU.Common.Elements.Point pointBeingModify;

        public EditableList(ManagerView destinationView)
            : base(destinationView)
        {
            pointBeforeModification = this.Module.SelectableObjects[0] as TSU.Common.Elements.Point;
            pointBeingModify = pointBeforeModification.DeepCopy() as TSU.Common.Elements.Point;
            this.Module.SelectableObjects.Clear();

            this.Module.SelectableObjects.Add(pointBeingModify);
        }

        



        internal override void On_ListView_DoubleClick(object sender, System.EventArgs e)
        {
            // do nothing compared to select item in normal ListStrategy
        }


        internal override void On_ListView_MouseDown(object sender, MouseEventArgs e)
        {
            ListView lv = this.listView;
            // get column index
            ListViewHitTestInfo hitTest = lv.HitTest(e.Location);
            
            // Ignore clicks outside of items
            if (hitTest.Item == null || hitTest.SubItem == null || !hitTest.Item.SubItems.Contains(hitTest.SubItem))
                return;

            int columnIndex = hitTest.Item.SubItems.IndexOf(hitTest.SubItem);

            TSU.Common.Elements.Point p = pointBeingModify;
            double d;

            // In the Enum the element over enum=6 are numerical values elements
            string s = lv.Columns[columnIndex].Text;

            bool isSu;

            switch (s)
            {
                case "Name":
                    ModifyName(p);
                    break;
                case "Group":
                    break;
                case "Acc_Zone":
                    break;
                case "Class":
                    break;
                case "Numero":
                    break;
                case "Element":

                    break;
                case "Xccs[m]":
                    d = p._Coordinates.Ccs.X.Value;
                    if (TryToModifyNumericValue(s, ref d))
                    {
                        p._Coordinates.Ccs.X.Value = d;
                        TS.RecomputeCoordinateFromCCS(p);
                    }
                    break;
                case "Yccs[m]":
                    d = p._Coordinates.Ccs.Y.Value;
                    if (TryToModifyNumericValue(s, ref d))
                    {
                        p._Coordinates.Ccs.Y.Value = d;
                        TS.RecomputeCoordinateFromCCS(p);
                    }
                    break;
                case "H[m]":

                    string titleAndMessage = $"{R.T_DANGER};{R.T_CHANGE_THE_H_IS_NOT_RECOMMENDED_BECAUSE_IT_SHOULD_AFFECT_X_AND_Y_BUT_TSUNAMI_WILL_NOT_CHANGE_X_AND_Y_COORDINATES} ";
                    MessageInput mi = new MessageInput(MessageType.Critical, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_DONT, R.T_CHANGE }
                    };
                    if (mi.Show().TextOfButtonClicked == R.T_CHANGE)
                    {
                        d = p._Coordinates.Ccs.Z.Value;
                        if (TryToModifyNumericValue(s, ref d))
                        {
                            p._Coordinates.Ccs.Z.Value = d;
                            TS.RecomputeCoordinateFromCCS(p);
                        }
                    }
                    break;
                case "Xsu[m]":
                    isSu = p._Coordinates.Local == p._Coordinates.Su;
                    d = p._Coordinates.Local.X.Value;
                    if (TryToModifyNumericValue(s, ref d))
                    {
                        p._Coordinates.Su.X.Value = d;
                        if (isSu)
                            TS.RecomputeCoordinateFromSu(p);
                        else
                            TS.RecomputeCoordinateFromMLA(p);
                    }
                    break;
                case "Ysu[m]":
                    isSu = p._Coordinates.Local == p._Coordinates.Su;
                    d = p._Coordinates.Local.Y.Value;
                    if (TryToModifyNumericValue(s, ref d))
                    {
                        p._Coordinates.Su.Y.Value = d;
                        if (isSu)
                            TS.RecomputeCoordinateFromSu(p);
                        else
                            TS.RecomputeCoordinateFromMLA(p);
                    }
                    break;
                case "Zsu[m]":
                    isSu = p._Coordinates.Local == p._Coordinates.Su;
                    d = p._Coordinates.Su.Z.Value;
                    if (TryToModifyNumericValue(s, ref d))
                    {
                        p._Coordinates.Local.Z.Value = d;
                        if (isSu)
                            TS.RecomputeCoordinateFromSu(p);
                        else
                            TS.RecomputeCoordinateFromMLA(p);
                    }
                    break;
                case "Xphy[m]":
                    d = p._Coordinates.Physicist.X.Value;
                    if (TryToModifyNumericValue(s, ref d))
                    {
                        p._Coordinates.Physicist.X.Value = d;
                        TS.RecomputeCoordinateFromPhy(p);
                    }
                    break;
                case "Yphy[m]":
                    d = p._Coordinates.Physicist.Y.Value;

                    if (TryToModifyNumericValue(s, ref d))
                    {
                        p._Coordinates.Physicist.Y.Value = d;
                        TS.RecomputeCoordinateFromPhy(p);
                    }
                    break;
                case "Zphy[m]":
                    d = p._Coordinates.Physicist.Z.Value;
                    if (TryToModifyNumericValue(s, ref d))
                    {
                        p._Coordinates.Physicist.Z.Value = d;
                        TS.RecomputeCoordinateFromPhy(p);
                    }
                    break;
                case "Cumul[m]":
                    break;
                case "Gis[gon]":
                    break;
                case "L":
                    break;
                case "Slope":
                    break;
                case "T":
                    break;
                case "Tilt[m]":
                    break;
                case "TH/NP":
                    switch (p.Type)
                    {
                        case E.Point.Types.Reference:
                            p.Type = E.Point.Types.Nominal;
                            break;
                        case E.Point.Types.Nominal:
                            p.Type = E.Point.Types.NewPoint;
                            break;
                        case E.Point.Types.NewPoint:
                            p.Type = E.Point.Types.Reference;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    if (s.Contains('X') || s.Contains('Y') || s.Contains('Z'))
                    {
                        throw new NotImplementedException("For this coordinate system");
                    }
                    break;
            }
            this.ManagerView.UpdateView();

        }



        public override void ValidateSelection()
        {
            if (pointBeforeModification._Name != pointBeingModify._Name) // special case for the name
            {
                string previousName = pointBeforeModification._Name;
                bool dontShowAgain0 = true;
                bool dontShowAgainAny = true;
                Research.RenameOnePointEverywhere(previousName, ref dontShowAgain0, ref dontShowAgainAny, pointBeingModify._Name);
                pointBeforeModification._Name = previousName;
            }

            // bad copy needed?
            bool makeBadCopy = true;
            if (pointBeforeModification._Coordinates.HasCcs && pointBeforeModification._Coordinates.Ccs.X.Value == 2000)
                makeBadCopy = false;
            else
            {
                if (pointBeforeModification._Coordinates.HasLocal && pointBeforeModification._Coordinates.Su.X.Value == 0)
                    makeBadCopy = false;
            }


            E.Point originalCopy = pointBeforeModification.DeepCopy();

            if (makeBadCopy)
            {
                // create a copy og original in bad state
                originalCopy.State = Element.States.Bad;
                this.Module.AllElements.Add(originalCopy);
            }

            // replace original point by the new one, it shoudl stay the same pointer to be applied in everywhere
            pointBeforeModification.AssignProperties(pointBeingModify);
            pointBeforeModification.State = Element.States.ModifiedByUser;
            this.ManagerView._Module.ParentModule._TsuView.UpdateView();


            foreach (TSU.Common.FinalModule module in Tsunami2.Properties.MeasurementModules)
            {
                module.UpdateTheoCoordinates(originalCopy, pointBeforeModification);
            }

        }
        public void CancelSelection()
        {
        }
        private bool TryToModifyNumericValue(string what, ref double value)
        {
            string cancel = R.T_CANCEL;
            string titleAndMessage = R.T_NEW_VALUE_FOR + what + ";" + R.T_ENTER_A_NEW_VALUE;
            List<string> buttonTexts = new List<string> { R.T_VALIDATE, cancel };

            MessageTsu.ShowMessageWithTextBox(titleAndMessage, buttonTexts, out string buttonClicked, out string textInput, value.ToString());

            if (buttonClicked != cancel)
            {
                value = T.Conversions.Numbers.ToDouble(textInput, true, -9999);
                if (value != -9999)
                    return true;
                else
                    throw new Exception(R.T_THE_VALUE_IS_NOT_NUMERIC);
            }
            return false;
        }

        private void ModifyName(E.Point point)
        {
            string value = point._Name;

            List<E.Point> pointsWithThisName = Research.FindPointsEverywhere(
                point._Name,
                out List<E.Point> allPoints,
                out List<TSU.Module> allModules,
                out List<Common.Station> allStations,
                out List<Measure> allMeasures,
                out string message);


            if (GetNwAlphaNumVAlue(point._Name, ref value, message))
            {
                // check if new point name already exist
                int countExistingPoint = Research.CountPointExistingSomewhere(value);
                if (countExistingPoint > 0)
                {
                    string titleAndMessage = $"{R.T_CANNOT_RENAME};{R.T_NAME_ALREADY_USED} ({countExistingPoint} times)";
                    MessageInput mi = new MessageInput(MessageType.Warning, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_CANCEL, R.T_FORCE}
                    };
                    string r = mi.Show().TextOfButtonClicked;
                    if (r == R.T_CANCEL)
                        return;
                }
                point._Name = value;
            }
        }

        private bool GetNwAlphaNumVAlue(string what, ref string value, string message = "")
        {
            string cancel = R.T_CANCEL;

            string preMessage = "";
            if (message != "")
                preMessage = message + "\r\n";

            string titleAndMessage = preMessage +
                                     $"{R.T_NEW_VALUE_FOR} {what};{R.T_ENTER_A_NEW_VALUE}";
            List<string> buttonTexts = new List<string> { R.T_VALIDATE, cancel };

            MessageTsu.ShowMessageWithTextBox(titleAndMessage, buttonTexts, out string buttonClicked, out string textInput, value.ToString());

            if (buttonClicked != cancel)
            {
                value = textInput.ToUpper();
                return true;
            }
            return false;
        }
    }
}
