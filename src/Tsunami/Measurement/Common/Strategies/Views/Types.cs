﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSU.Common.Strategies.Views
{
    public enum Types
    {
        Tree,
        List,
        EditableList,
        In3D,
        Unknown
    }
}
