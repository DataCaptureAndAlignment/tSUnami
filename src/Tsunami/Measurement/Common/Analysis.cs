﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TSU.Common.Compute;
using TSU.Common.Compute.Transformation;
using TSU.Common.Elements;
using TSU.Common.Instruments.Reflector;
using TSU.Views;
using TSU.Views.Message;
using C = TSU.Common.Elements.Composites;
using E = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using L = TSU.Common.Compute.Compensations.Lgc2;
using M = TSU.Common.Measures;
using R = TSU.Properties.Resources;
using T = TSU.Tools;
using Z = TSU.Common.Zones;

namespace TSU.Common.Analysis
{
    internal static class Names
    {
        internal static List<string> Get4AsgParts(string name)
        {
            List<string> allParts = name.Split('.').ToList();
            List<string> fourParts = new List<string>() { "", "", "", "" };

            // function to get a part and remove it from the list
            string GetApartAndRemove(List<string> parts, int index)
            {
                string part = parts[index];
                parts.Remove(part);
                return part;
            }

            if (allParts.Count < 4)
            {
                for (int i = 3; i > -1; i--) // fill boxes from last to first
                {
                    if (allParts.Count > 0)
                    {
                        fourParts[i] = allParts[allParts.Count - 1].ToUpper();
                        allParts.Remove(allParts[allParts.Count - 1]);
                    }
                    else
                        fourParts[i] = "";
                }
            }
            else
            {
                // POINT is always the last part
                fourParts[3] = GetApartAndRemove(allParts, allParts.Count - 1).ToUpper();

                // ZONE or ACC
                fourParts[0] = GetApartAndRemove(allParts, 0).ToUpper();

                // Beam number?
                if (allParts[0].ToUpper() == "B1" || allParts[0].ToUpper() == "B2")
                {
                    fourParts[0] += "." + GetApartAndRemove(allParts, 0).ToUpper();
                }

                // CLASS
                fourParts[1] = GetApartAndRemove(allParts, 0).ToUpper();

                // NUMERO
                fourParts[2] = "";
                foreach (var item in allParts)
                {
                    if (fourParts[2] != "")
                        fourParts[2] += ".";
                    fourParts[2] += item.ToUpper();
                }
            }
            return fourParts;
        }
    }

    internal static class ReferenceFrames
    {
        #region Reference Frames
        /// <summary>
        /// this will return the reference fram if it is constant or return Unknown if it is variable
        /// </summary>
        /// <param name="points"></param>
        /// <param name="tsunamiTypes"></param>
        /// <returns></returns>
        internal static Coordinates.ReferenceFrames GetIfConstant(List<E.Point> points, Coordinates.CoordinateSystemsTsunamiTypes tsunamiTypes)
        {
            Coordinates.ReferenceFrames rf;
            switch (tsunamiTypes)
            {
                case Coordinates.CoordinateSystemsTsunamiTypes.CCS:
                    rf = points[0]._Coordinates.Ccs.ReferenceFrame;

                    foreach (var item in points)
                        if (item._Coordinates.Ccs.ReferenceFrame != rf) return Coordinates.ReferenceFrames.Unknown;
                    return rf;
                case Coordinates.CoordinateSystemsTsunamiTypes.PHYS:
                case Coordinates.CoordinateSystemsTsunamiTypes.MLA:
                case Coordinates.CoordinateSystemsTsunamiTypes.SU:
                    rf = points[0]._Coordinates.Local.ReferenceFrame;
                    foreach (var item in points)
                        if (item._Coordinates.Local.ReferenceFrame != rf) return Coordinates.ReferenceFrames.Unknown;
                    return rf;
                case Coordinates.CoordinateSystemsTsunamiTypes.UserDefined:
                    rf = points[0]._Coordinates.UserDefined.ReferenceFrame;
                    foreach (var item in points)
                        if (item._Coordinates.UserDefined.ReferenceFrame != rf) return Coordinates.ReferenceFrames.Unknown;
                    return rf;
                case Coordinates.CoordinateSystemsTsunamiTypes.Unknown:
                default:
                    return Coordinates.ReferenceFrames.Unknown;
            }
        }
        #endregion
    }

    internal static class Network
    {
        internal static bool QuickBestGuessAboutAccessibilityOfNetworkPath(string path)
        {
            if (string.IsNullOrEmpty(path)) return false;
            string pathRoot = System.IO.Path.GetPathRoot(path);
            if (string.IsNullOrEmpty(pathRoot)) return false;
            System.Diagnostics.ProcessStartInfo pinfo = new System.Diagnostics.ProcessStartInfo("net", "use")
            {
                CreateNoWindow = true,
                RedirectStandardOutput = true,
                UseShellExecute = false
            };
            string output;
            using (System.Diagnostics.Process p = System.Diagnostics.Process.Start(pinfo))
            {
                output = p.StandardOutput.ReadToEnd();
            }
            foreach (string line in output.Split('\n'))
            {
                if (line.Contains(pathRoot))
                {
                    return true; // shareIsProbablyConnected
                }
            }
            return false;
        }
    }

    #region Color
    internal static class Colors
    {
        public static Color ChooseContrastedColorBetweenBlackAndWhite(Color c)
        {
            var l = 0.2126 * c.R + 0.7152 * c.G + 0.0722 * c.B;
            l /= 255; //normalization
            return l < 0.5 ? Tsunami2.Preferences.Theme.Colors.LightForeground : Tsunami2.Preferences.Theme.Colors.NormalFore;
        }

        internal static Color GetDarkerColor(Color original)
        {
            return ChangeColorLuminosity(original, 0.8);
        }

        internal static Color GetLighterColor(Color original)
        {
            return ChangeColorLuminosity(original, 1.2);
        }

        internal static Color ChangeColorLuminosity(Color original, double factor)
        {
            int R = (int)(original.R * factor);
            int G = (int)(original.G * factor);
            int B = (int)(original.B * factor);
            var tempColor = Color.FromArgb(
                original.A,
                R > 255 ? 255 : R,
                G > 255 ? 255 : G,
                B > 255 ? 255 : B);

            return tempColor;
        }

    }

    #endregion

    internal static class Time
    {
        internal static void WaitUntil(Func<bool> myMethodName, int delayBetweenTrialInMillis = 100, int timeOutInMillis = 2000)
        {
            DateTime dt = DateTime.Now;
            while (!myMethodName())
            {
                if ((DateTime.Now - dt).TotalMilliseconds > timeOutInMillis)
                    return;
                System.Threading.Thread.Sleep(delayBetweenTrialInMillis);
            }
        }
    }


    internal static class Theodolite
    {
        internal static List<M.Measure> Compare(List<M.Measure> l, double H_Tol_CC, double V_Tol_CC, double D_Tol_mm)
        {
            List<M.Measure> diffs = new List<M.Measure>();
            for (int i = 0; i < l.Count; i++)
            {
                M.Measure mRef = l[i].Clone() as M.Measure;
                mRef._Name = "M" + i.ToString();
                mRef._Status = new M.States.Temporary();
                mRef.Tag = l[i];
                diffs.Add(mRef);
                bool dummy = false;
                for (int j = i - 1; j > -1; j--)
                {
                    M.Measure mComp = l[j].Clone() as M.Measure;
                    mComp._Name = "M" + j.ToString();
                    mComp.Tag = l[j];
                    bool overTol = false;
                    M.Measure diff = Survey.GetDifference(
                        mRef as Polar.Measure,
                        mComp as Polar.Measure,
                        H_Tol_CC,
                        V_Tol_CC,
                        D_Tol_mm,
ref overTol, out _, ref dummy);

                    if (overTol)
                        diff._Status = new M.States.Questionnable(); // for out of limit to show in red
                    else
                        diff._Status = new M.States.Good(); // for ok limit to show in black, as all good measures are already in green...
                    diffs.Add(diff);
                }

            }

            //diffs.Reverse();

            return diffs;
        }


        // return a message if the different between the average measure and of the measure from the list is over the tolerance.
        internal static bool CheckTolerances(List<Polar.Measure> measures, Polar.Measure averageMeasure, double H_Tol_CC, double V_Tol_CC, double D_Tol_mm, out string message)
        {
            bool outOfToleranceGeneral = false;
            bool outOfToleranceLocal = false;

            int i = 0;
            // numberdecimal for distances
            int nDD = Tsunami2.Preferences.Values.GuiPrefs == null ? 7 : Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
            // numberdecimal for angles
            int nDA = Tsunami2.Preferences.Values.GuiPrefs == null ? 5 : Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;

            string hzInfo = "Hz: \r\n";
            string vInfo = "V: \r\n";
            string dInfo = "D: \r\n";
            foreach (Polar.Measure item in measures)
            {
                i++;

                // H
                double dH = averageMeasure.Angles.Corrected.Horizontal.Value - item.Angles.Corrected.Horizontal.Value;
                if (dH > 399)
                    dH = -(400 - dH);
                if (dH < -399)
                    dH = (400 + dH);

                double mH = Survey.GetOppositeSide(Math.Abs(dH), item.Distance.Corrected.Value);

                outOfToleranceLocal = H_Tol_CC != 0 && Math.Abs(dH) > H_Tol_CC / 10000 || mH > D_Tol_mm;
                if (outOfToleranceLocal)
                    outOfToleranceGeneral = true;
                var showTol = outOfToleranceLocal ? $"(>{H_Tol_CC})" : $"(<{H_Tol_CC})";
                hzInfo += $"" +
                        $"      M{i} = {item.Angles.Corrected.Horizontal.ToString(nDA, 1) + " gon",-20}" +
                        $"Mean-M{i} = {new DoubleValue(dH, 0).ToString(nDA - 4, 10000) + $" CC {showTol}",15}" +
                        $"\t=> {new DoubleValue(mH, 0).ToString(nDD - 3, 1000) + " mm ",10}\r\n";
                
                // V
                double dV = averageMeasure.Angles.Corrected.Vertical.Value - item.Angles.Corrected.Vertical.Value;
                double mV = Survey.GetVerticalDistance(Math.Abs(dV), averageMeasure.Angles.Corrected.Vertical.Value, item.Distance.Corrected.Value);

                outOfToleranceLocal = V_Tol_CC != 0 && Math.Abs(dV) > V_Tol_CC / 10000 || mV > D_Tol_mm;
                if (outOfToleranceLocal)
                    outOfToleranceGeneral = true;
                showTol = outOfToleranceLocal ? $"(>{V_Tol_CC})" : $"(<{V_Tol_CC})";
                vInfo += $"" +
                    $"      M{i} = {item.Angles.Corrected.Vertical.ToString(nDA, 1) + " gon",-20}" +
                    $"Mean-M{i} = {new DoubleValue(dV, 0).ToString(nDA - 4, 10000) + $" CC {showTol}",15}" +
                    $"\t=> {new DoubleValue(mV, 0).ToString(nDD - 3, 1000) + " mm",10}\r\n";

                //D
                double d = averageMeasure.Distance.Corrected.Value - item.Distance.Corrected.Value;

                outOfToleranceLocal = Math.Abs(d) > D_Tol_mm / 1000;
                if (outOfToleranceLocal)
                    outOfToleranceGeneral = true;
                showTol = outOfToleranceLocal ? $"(>{D_Tol_mm})" : $"(<{D_Tol_mm})";
                dInfo += $"" +
                    $"      M{i} = {item.Distance.Corrected.ToString(nDD, 1) + " m",-20}" +
                    $"Mean-M{i} = {new DoubleValue(d, 0).ToString(nDD - 3, 1000) + $" mm {showTol}",15}\r\n";

            }

            message = hzInfo + $"   Mean = {averageMeasure.Angles.Corrected.Horizontal.ToString(nDA, 1) + " gon",-20}" + "\r\n\r\n"
                + vInfo + $"   Mean = {averageMeasure.Angles.Corrected.Vertical.ToString(nDA, 1) + " gon",-20}" + "\r\n\r\n"
                + dInfo + $"   Mean = {averageMeasure.Distance.Corrected.ToString(nDD, 1) + " m",-20}" + "\r\n\r\n";

            return !outOfToleranceGeneral;
        }


        internal static List<E.Point> GetNotExistingVariablePointsFrom(Polar.Station stationTheodolite, List<E.Point> existingVariablePoints)
        {

            List<E.Point> newVariablePoints = new List<E.Point>();

            // BAD?
            bool badStation = stationTheodolite.ParametersBasic._State is Station.State.Bad;

            // Station point?
            List<E.Point> VariablePointsIncludingFreeStationPoints = new List<E.Point>();
            Polar.Station.Parameters parameters2 = stationTheodolite.Parameters2;
            Polar.Station.Parameters.Setup.Values initialValues = parameters2.Setups.InitialValues;
            if (!initialValues.IsPositionKnown
              && initialValues.VerticalisationState != Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation.NotVerticalised)
            {
                VariablePointsIncludingFreeStationPoints.Add(initialValues.StationPoint);
            }
            VariablePointsIncludingFreeStationPoints.AddRange(stationTheodolite.VariablePoints);

            foreach (E.Point point in VariablePointsIncludingFreeStationPoints)
            {
                if (badStation)
                {
                    if (existingVariablePoints.Concat(newVariablePoints).LastOrDefault(x => x.FullNameWithoutZone == point.FullNameWithoutZone) == null)
                    {
                        E.Point clone = point.Clone() as E.Point;
                        clone.State = E.Element.States.Bad;
                        clone.CommentFromTsunami = $"From Bad Station '{parameters2._StationPoint._Name}'";
                        newVariablePoints.Add(clone);
                    }
                }
                else
                {
                    E.Point existingPoint = existingVariablePoints.Concat(newVariablePoints).LastOrDefault(x => x._Name == point._Name);
                    bool pointExist = existingPoint != null;
                    if (pointExist)
                    {
                        if (existingPoint.State == E.Element.States.Bad)
                            existingPoint.State = E.Element.States.Good;
                    }
                    else
                    {
                        newVariablePoints.Add(point.Clone() as E.Point);
                    }
                }
            }
            return newVariablePoints;
        }

        internal static I.FaceType GetFaceType(List<Polar.Measure> measures)
        {
            SortByFaces(
                measures,
                out List<Polar.Measure> face1Ms,
                out List<Polar.Measure> face2Ms,
                out List<Polar.Measure> dFaceMs,
                out List<Polar.Measure> uFaceMs);

            bool u = uFaceMs.Count > 0;
            int f1Count = face2Ms.Count;
            int f2Count = face1Ms.Count;
            int dFCount = dFaceMs.Count;
            bool f1 = f1Count > 0;
            bool f2 = f2Count > 0;
            bool dF = dFCount > 0;

            if (u) return I.FaceType.UnknownFace;
            if (f1 && !f2 && !dF) return I.FaceType.Face1;
            if (!f1 && f2 && !dF) return I.FaceType.Face2;
            if (!f1 && !f2 && dF) return I.FaceType.DoubleFace;
            if (dF && f1Count != f2Count) return I.FaceType.UnknownFace;

            if (f1Count == f2Count) return I.FaceType.DoubleFace;

            return I.FaceType.UnknownFace;
        }

        // Will return a list with the doublefaced averaged measure and out a bool saying if all measure have been used
        internal static List<Polar.Measure> GetDoubleFaceMeasures(List<Polar.Measure> allTypeOfMeasures, out bool allMeasuresUsed)
        {
            SortByFaces(
                allTypeOfMeasures,
                out List<Polar.Measure> face1Ms,
                out List<Polar.Measure> face2Ms,
                out List<Polar.Measure> dFaceMs,
                out List<Polar.Measure> uFaceMs);

            List<Polar.Measure> newDFaceMs = CreateDFace(face1Ms, face2Ms);

            foreach (Polar.Measure item in newDFaceMs)
            {
                if (dFaceMs.Find(x => x._Point._Name == item._Point._Name && x._Date == item._Date) == null) dFaceMs.Add(item);
            }

            allMeasuresUsed = uFaceMs.Count == 0;

            return dFaceMs;
        }

        //Get different list based on the face of the measures face1only, face2only, doubleface, uknownface
        internal static void SortByFaces(
            List<Polar.Measure> AllMs,
            out List<Polar.Measure> face1Ms,
            out List<Polar.Measure> face2Ms,
            out List<Polar.Measure> dFaceMs,
            out List<Polar.Measure> uFaceMs)
        {
            // create lists
            face1Ms = new List<Polar.Measure>();
            face2Ms = new List<Polar.Measure>();
            dFaceMs = new List<Polar.Measure>();
            uFaceMs = new List<Polar.Measure>();

            // first sort by facetype
            foreach (Polar.Measure item in AllMs)
            {
                switch (item.Face)
                {
                    case I.FaceType.Face1: face1Ms.Add(item); break;
                    case I.FaceType.Face2: face2Ms.Add(item); break;
                    case I.FaceType.DoubleFace: dFaceMs.Add(item); break;
                    default: uFaceMs.Add(item); break;
                }
            }
        }

        // Get a list of doubled face measures from lists of face1only and face2only
        internal static List<Polar.Measure> CreateDFace(List<Polar.Measure> face1Ms, List<Polar.Measure> face2Ms)
        {
            List<Polar.Measure> dFaceMs = new List<Polar.Measure>();

            // look for face 2 based on face1
            foreach (Polar.Measure item in face1Ms)
            {
                List<Polar.Measure> found2s = face2Ms.Where(x => x._Point._Name == item._Point._Name).ToList();
                bool completyOut = false;
                if (found2s.Count == 1)
                    dFaceMs.Add(Survey.DoFaceAverage(item, found2s[0], 10, 10, 0.1, out _, out _, out _, ref completyOut));
                else
                {
                    List<Polar.Measure> found1s = face1Ms.Where(x => x._Point._Name == item._Point._Name).ToList();
                    throw new Exception(string.Format(
                        "{0};{1} {2}, {3} {4} {5} {6} {7}",
                        R.T_FACE_AVERAGE_ERROR,
                        R.T_WHEN_TRYING_TO_AVERAGE_FACE1_AND_FACE2_MEASUREMENT_FOR_THE_POINT,
                        item._Point._Name,
                        R.T_WE_FOUND,
                        found1s.Count,
                        R.T_MEASURE_WITH_FACE1_AND,
                        found2s.Count,
                        R.T_MEASURE_WITH_FACE2_TSUNAMI_ONLY_SUPPORTS_1_TO_1_FOR_NOW));
                }
            }
            return dFaceMs;
        }

        internal static M.TheodoliteRounds CreateRounds(List<Polar.Measure> measures)
        {

            try
            {
                M.TheodoliteRounds rounds = new M.TheodoliteRounds();

                var q = measures.ToLookup(k => k.Origin);

                foreach (var item in q)
                {
                    rounds.Add(new M.TheodoliteRound(item));
                }

                return rounds;
            }
            catch (Exception ex)
            {

                throw new Exception($"{R.T_COULD_NOT_COMPUTE_THE_ROUNDS}: {ex.Message}");
            }
        }

        internal static void FindWrongMeasureInCompensation(Polar.Station station, Common.Compute.Compensations.Strategies.List strategy)
        {
            throw new NotImplementedException();
        }

        // get new instrument and reflectors pairs
        internal static List<L.InstrumentWithUsedReflector> GetNotExistingIntrumentTargetpairFrom(
            Polar.Station stationTheodolite,
            List<L.InstrumentWithUsedReflector> existingPairs)
        {
            List<L.InstrumentWithUsedReflector> newPairs = new List<L.InstrumentWithUsedReflector>();

            L.InstrumentWithUsedReflector used = existingPairs.Concat(newPairs).LastOrDefault(x => x.Instrument._Name == stationTheodolite.Parameters2._Instrument._Name);
            if (used == null)
            {
                used = new L.InstrumentWithUsedReflector()
                {
                    Instrument = stationTheodolite.Parameters2._Instrument as I.IPolarInstrument
                };
                newPairs.Add(used);
            }

            foreach (var reflector in T.Research.GetListOfReflectorUsedIn(stationTheodolite))
            {
                if (!used.Reflectors.Contains(reflector))
                    used.Reflectors.Add(reflector);
            }
            return newPairs;
        }

        internal static List<E.Point> FindVariablePointsWithoutObservations(List<E.Point> points, List<Station> stations)
        {
            double na = Tsunami2.Preferences.Values.na;

            List<E.Point> founds = new List<E.Point>();

            List<M.Measure> allMeasures = new List<M.Measure>();

            foreach (var s in stations)
            {
                if (!(s.ParametersBasic._State is Station.State.Bad))
                {
                    foreach (var m in s.MeasuresTaken)
                    {
                        if (!(m._Status is M.States.Bad))
                        {
                            allMeasures.Add(m);
                        }
                    }
                }
            }


            foreach (var point in points)
            {
                if (stations.Find(
                    x => x is Polar.Station psx
                        && psx.Parameters2._StationPoint != null
                        && psx.Parameters2._StationPoint._Name == point._Name) is Polar.Station ps)
                {
                    if (ps.ParametersBasic._State is Station.State.Bad)
                        founds.Add(point);
                    else continue;
                }


                List<M.Measure> concernedMeasures = allMeasures.FindAll(x => x._Point._Name == point._Name);
                if (concernedMeasures.Count > 0)
                {
                    int hFound = 0;
                    int vFound = 0;
                    int dFound = 0;
                    int rFound = 0;
                    int lFound = 0;
                    foreach (M.Measure m in concernedMeasures)
                    {
                        if (m is Polar.Measure pm)
                        {
                            if (pm.Angles.Corrected.Horizontal != null && pm.Angles.Corrected.Horizontal.Value != na)
                                hFound++;
                            if (pm.Angles.Corrected.Vertical != null && pm.Angles.Corrected.Vertical.Value != na)
                                vFound++;
                            if (pm.Distance.Corrected != null && pm.Distance.Corrected.Value != na)
                                dFound++;
                        }
                        else if (m is M.MeasureOfLevel lm)
                        {
                            if (lm._RawLevelReading != na)
                                vFound++;
                        }
                        else if (m is M.MeasureOfOffset om)
                        {
                            if (om._AverageDeviation != na)
                            {
                                rFound++;
                                lFound++;
                            }
                        }
                    }


                    bool OnePolarOK = hFound >= 1 && vFound >= 1 && dFound >= 1;
                    bool PolarIntersectionOK = hFound >= 2 && vFound >= 2;
                    bool distanceIntersectionOK = dFound >= 3;
                    bool ecartoWithLevellingOK = rFound >= 1 && rFound >= 1 && vFound >= 1;

                    bool enoughObservation = OnePolarOK || PolarIntersectionOK || distanceIntersectionOK || ecartoWithLevellingOK;

                    if (!enoughObservation)
                        founds.Add(point);
                }
                else
                {
                    founds.Add(point);
                }
            }
            return founds;
        }
    }

    public static class Instrument
    {
        internal static List<I.SigmaForAInstrumentCouple> GetAprioriSigma(string isntrumentType, List<I.SigmaForAInstrumentCouple> list)
        {
            List<I.SigmaForAInstrumentCouple> founds = new List<I.SigmaForAInstrumentCouple>();
            foreach (I.SigmaForAInstrumentCouple c in list)
            {
                if (c.Instrument == isntrumentType)
                    founds.Add(c);
            }
            return founds;
        }


        /// <summary>
        /// return the apriori sigma, choosed by reflector from the actual lst in the stationparam define by actual instrument and condition
        /// </summary>
        /// <param name="st"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        internal static I.SigmaForAInstrumentCouple GetAprioriSigma(Polar.Station st, Polar.Measure m)
        {
            List<I.SigmaForAInstrumentCouple> availables = st.Parameters2.AprioriSigmaForTheSelectedInstrumentAndConditon;
            // first check for reflector 
            foreach (I.SigmaForAInstrumentCouple c in availables)
            {
                if (c.Target.ToString() == m.Distance.Reflector._InstrumentType.ToString())
                    return c;
            }

            // else default refletor
            foreach (I.SigmaForAInstrumentCouple c in availables)
            {
                if (c.Target.ToUpper() == "DEFAULT")
                    return c;
            }

            // if not that the most default even not takin care of instrument, let go back to full list from preference
            return Tsunami2.Preferences.Values.InstrumentSlashTargetCoupleSigmas.Find(x => x.Instrument.ToUpper() == "DEFAULT");
        }

        internal static List<I.SigmaForAInstrumentCouple> GetAprioriSigma(string instrumentType, string condition, List<I.SigmaForAInstrumentCouple> couples)
        {
            return couples.FindAll(x => x.Instrument == instrumentType && x.Condition == condition);
        }

        internal static List<string> GetListOfSigmaConditionsAvailableFromAList(List<I.SigmaForAInstrumentCouple> list)
        {
            List<string> founds = new List<string>();
            foreach (I.SigmaForAInstrumentCouple c in list)
            {
                if (!founds.Contains(c.Condition))
                    founds.Add(c.Condition);
            }
            return founds;
        }

        //internal static List<I.SigmaForAInstrumentCouple> GetAprioriSigmaOfType(string condition, List<I.SigmaForAInstrumentCouple> list)
        //{
        //    // get targets
        //    List<string> targets = new List<string>();
        //    foreach (I.SigmaForAInstrumentCouple c in list)
        //    {
        //        if (!targets.Contains(c.Target))
        //            targets.Add(c.Target);
        //    }

        //    List<I.SigmaForAInstrumentCouple> founds = new List<I.SigmaForAInstrumentCouple>();
        //    foreach (string t in targets)
        //    {
        //        bool found = false;
        //        I.SigmaForAInstrumentCouple _default = null;
        //        foreach (I.SigmaForAInstrumentCouple c in list)
        //        {
        //            if (c.Target == t)
        //            {
        //                if (c.Condition == condition)
        //                {
        //                    founds.Add(c);
        //                    found = true;
        //                    break; // si trouvé next target
        //                }
        //                else if (c.Condition.ToUpper()=="DEFAULT")
        //                {
        //                    _default = c;
        //                }
        //            }
        //        }
        //        if (!found && _default != null) // si rien trouvé sauf le default, on le prend
        //            founds.Add(_default);
        //    } 
        //    return founds;
        //}

        internal static Reflector GetReflector(string model)
        {

            return Tsunami2.Preferences.Values.Reflectors.Find(x => x._Model.ToString().ToUpper() == model.ToUpper());
        }

    }

    public static class Element
    {
        internal static SocketCode DetermineCodeToPropose(Polar.Measure measure, Polar.Measure defaultStationMeasure, string newName, out bool fromName, out string message)
        {
            if (SocketCode.GetSmartCodeFromName(newName, out SocketCode socketCode, out string message2))
            {
                message = message2;
                fromName = true;
                return socketCode;
            }
            message = "";
            fromName = false;
            SocketCode codeZero = Tsunami2.Preferences.Values.SocketCodes.Find(x => x.Id == "0");
            if (measure == null) return codeZero;
            if (measure == defaultStationMeasure && defaultStationMeasure._Point.SocketCode != null) return defaultStationMeasure._Point.SocketCode;
            if (measure._OriginalPoint == null) return defaultStationMeasure._Point.SocketCode;
            if (measure._OriginalPoint.SocketCode == null && defaultStationMeasure._Point.SocketCode != null) return defaultStationMeasure._Point.SocketCode;

            if (measure._OriginalPoint.SocketCode.Id == "0")
                return defaultStationMeasure._Point.SocketCode;
            else
                return measure._OriginalPoint.SocketCode;
        }


        internal static bool ContainAPointIn(string pointName, E.Element.States state, List<E.Point> points)
        {
            foreach (E.Point p in points)
            {
                if (p._Name.ToUpper() == pointName.ToUpper()
                    && p.State == state)
                    return true;
            }
            return false;
        }

        internal static bool ContainAPointIn(string pointName, List<E.Point> points)
        {
            foreach (E.Point p in points)
            {
                if (p._Name.ToUpper() == pointName.ToUpper())
                    return true;
            }
            return false;
        }

        internal static E.Point CompareTwoPoints(E.Point refPoint, E.Point compPoint)
        {
            E.Point p = new E.Point(refPoint._Name + "-" + compPoint._Name);
            p._Coordinates.Local = CompareTwoCoordinates(refPoint._Coordinates.Local, compPoint._Coordinates.Local);
            p._Coordinates.Ccs = CompareTwoCoordinates(refPoint._Coordinates.Ccs, compPoint._Coordinates.Ccs);
            p._Coordinates.Su = CompareTwoCoordinates(refPoint._Coordinates.Su, compPoint._Coordinates.Su);
            p._Coordinates.Physicist = CompareTwoCoordinates(refPoint._Coordinates.Physicist, compPoint._Coordinates.Physicist);
            p._Coordinates.Beam = CompareTwoCoordinates(refPoint._Coordinates.Beam, compPoint._Coordinates.Beam);
            p._Coordinates.BeamV = CompareTwoCoordinates(refPoint._Coordinates.BeamV, compPoint._Coordinates.BeamV);
            p._Coordinates.StationCs = CompareTwoCoordinates(refPoint._Coordinates.StationCs, compPoint._Coordinates.StationCs);

            p._Parameters.Cumul = refPoint._Parameters.Cumul - compPoint._Parameters.Cumul;
            return p;
        }

        internal static Coordinates CompareTwoCoordinates(Coordinates coordinates1, Coordinates coordinates2)
        {
            double na = Tsunami2.Preferences.Values.na;
            Coordinates c = new Coordinates();
            c.Set();
            c.SystemName = coordinates1.SystemName;
            c.X.Value = coordinates1.X.Value == na || coordinates2.X.Value == na ? na : coordinates1.X.Value - coordinates2.X.Value;
            c.Y.Value = coordinates1.Y.Value == na || coordinates2.Y.Value == na ? na : coordinates1.Y.Value - coordinates2.Y.Value;
            c.Z.Value = coordinates1.Z.Value == na || coordinates2.Z.Value == na ? na : coordinates1.Z.Value - coordinates2.Z.Value;
            return c;
        }

        internal static List<E.Point> Compare(List<E.Point> l)
        {
            List<E.Point> diffs = new List<E.Point>();
            for (int i = l.Count - 1; i > -1; i--)
            {
                E.Point mRef = l[i].Clone() as E.Point;
                mRef._Origin = "P" + i.ToString();

                for (int j = i - 1; j > -1; j--)
                {
                    E.Point mComp = l[j].Clone() as E.Point;
                    mComp._Origin = "P" + j.ToString();
                    E.Point diff = CompareTwoPoints(mRef, mComp);
                    diff._Name = mRef._Origin + " - " + mComp._Origin;
                    diffs.Add(diff);
                }
                diffs.Add(mRef);
            }

            diffs.Reverse();

            return diffs;
        }


        public static void FindCorrespondence(Views.TsuView view, double tolX = 5, double tolY = 5, double tolZ = 5, string startingFolder = "")
        {
            string tolTitleAndMessage = $"{R.T_CORRESPONDENCE_WITHIN_2_LISTS};" +
                                        $"{R.T_THIS_FUNCTION_WILL_SORT_THE_SECOND_FILE_CONTAINING_ID_XYZ_COORDINATES_BASED_ON_THE_ORDER_OF_THE_FIRST_FILE}.\r\n" +
                                        $"{R.T_PLEASE_ENTER_THE_TOLERANCE_ALONG_X_Y_Z}.\r\n" +
                                        $"{R.T_YOU_WILL_BE_THEN_PROMPT_TO_SELECT_THE_2_FILES_CONTAINING_YOUR_POINTS}.\r\n" +
                                        $"{R.T_THE_SECOND_FILE_WILL_THEN_BE_SORTED}.\r\n" +
                                        $"({R.T_IF_DIFFERENT_CS_MAKE_SURE_THE_FIRST_POINT_OF_THE_SECOND_LIST_EXIST_IN_THE_FIRST_LIST})\r\n";
            string pointInSameCs = R.T_POINTS_ARE_IN_CLOSE_CS;
            string pointInDifferentCs = R.T_POINTS_ARE_IN_DIFFERENT_CS;
            string cancel = R.T_CANCEL;
            List<string> buttonTexts = new List<string> { pointInDifferentCs, pointInSameCs, cancel };
            string initialText = $"tolX = {tolX} mm, tolY = {tolY} mm, tolZ = {tolZ} mm";

            MessageTsu.ShowMessageWithTextBox(tolTitleAndMessage, buttonTexts, out string tolButtonClicked, out string tolTextInput, initialText, false);

            if (tolButtonClicked == cancel) return;

            string[] parts = tolTextInput.Split('=', ',');

            Exception e = new Exception($"{R.T_PLEASE_RESPECT_THE_TOL_AND_MM_FORMAT}: 'tolX = {tolX} mm, tolY = {tolY} mm, tolZ = {tolZ} mm'");

            if (parts.Length != 6) throw e;

            if (!double.TryParse(parts[1].Substring(0, parts[1].Length - 3), out double tolXinMm)) throw e;
            if (!double.TryParse(parts[3].Substring(0, parts[3].Length - 3), out double tolYinMm)) throw e;
            if (!double.TryParse(parts[5].Substring(0, parts[5].Length - 3), out double tolZinMm)) throw e;

            if (startingFolder == "")
                startingFolder = Tsunami2.Preferences.Values.Paths.MeasureOfTheDay;
            // get the first list path
            string firstListPath;
            System.Windows.Forms.OpenFileDialog theDialog = new System.Windows.Forms.OpenFileDialog
            {
                Title = $"{R.T_OPEN_TEXT_FILE}\r\n{R.T_CONTAINING_THE_FIRST_REFERENCE_LIST_OF_POINTS}",
                Filter = "all files|*.*",
                InitialDirectory = startingFolder
            };

            TSU.Common.TsunamiView.ViewsShownAsModal.Add(theDialog);
            DialogResult res = theDialog.ShowDialog();
            TSU.Common.TsunamiView.ViewsShownAsModal.Remove(theDialog);

            if (res == System.Windows.Forms.DialogResult.OK)
                firstListPath = theDialog.FileName;
            else
                return;

            startingFolder = new System.IO.FileInfo(firstListPath).DirectoryName;

            // get the second list path
            string secondListPath;
            theDialog.InitialDirectory = startingFolder;
            theDialog.Title = $"{R.T_OPEN_TEXT_FILE}\r\n{R.T_CONTAINING_THE_SECOND_REFERENCE_LIST_OF_POINTS}";

            TSU.Common.TsunamiView.ViewsShownAsModal.Add(theDialog);
            res = theDialog.ShowDialog();
            TSU.Common.TsunamiView.ViewsShownAsModal.Remove(theDialog);

            if (res == System.Windows.Forms.DialogResult.OK)
                secondListPath = theDialog.FileName;
            else
                return;

            // create the first list
            List<Coordinates> refList = ReadFile(firstListPath);

            // create the 2nd list
            List<Coordinates> mixedList = ReadFile(secondListPath);


            // find correspondence
            List<Coordinates> sortedList;
            int missingMatches;
            List<Coordinates> renammedList;
            List<Coordinates> additionnalPoints = new List<Coordinates>();
            List<Coordinates> notFoundPoints;

            if (tolButtonClicked == pointInSameCs)
                sortedList = FindCorrespondenceBasedOnCoordinates(
                    refList,
                    mixedList,
                    tolXinMm / 1000,
                    tolYinMm / 1000,
                    tolZinMm / 1000,
                    out missingMatches,
                    out renammedList,
                    out additionnalPoints,
                    out notFoundPoints);
            else
                sortedList = FindCorrespondenceBasedOn3dDistances(
                    refList,
                    mixedList,
                    Math.Sqrt(Math.Pow(tolXinMm, 2) + Math.Pow(tolYinMm, 2) + Math.Pow(tolZinMm, 2)) / 1000,
                    out missingMatches,
                    out renammedList,
                    out notFoundPoints);

            // Write files

            string resultPath = secondListPath + ".Sorted.txt";
            WritePointsInFile(resultPath, "%%%% Points found sorted in the order of the first file", sortedList);
            WritePointsInFile(resultPath, "%%%% From mixed list which where not in the ref list", additionnalPoints, append: true);

            string resultPath2 = secondListPath + ".renammed.txt";
            WritePointsInFile(resultPath2, "%%%% Points found renammed by the name the first file", renammedList);
            WritePointsInFile(resultPath2, "%%%% From mixed list which where not in the ref list", additionnalPoints, append: true);
            WritePointsInFile(resultPath2, "%%%% Points Not found. /!\\ those are coordinates from the reference file", notFoundPoints, append: true);


            // show results
            string messageWithLinks =
                    $"{R.T_REFERENCE_FILE}:\t{MessageTsu.GetLink(firstListPath)}\r\n" +
                    $"{R.T_MIXED_FILE}:\t{MessageTsu.GetLink(secondListPath)}\r\n\r\n";

            bool fileOk;
            if (System.IO.File.Exists(resultPath))
            {
                if (missingMatches > 0)
                    messageWithLinks += $"$red${missingMatches} {R.T_POINTS_HAVE_NOT_MATCHES}!$red$\r\n";
                messageWithLinks += $"{R.T_MIXED_FILE_SORTED}:\t{MessageTsu.GetLink(resultPath)}\r\n";
                messageWithLinks += $"{R.T_MIXED_FILE_RENAMMED}:\t{MessageTsu.GetLink(resultPath2)}\r\n";
                fileOk = true;
            }
            else
            {
                messageWithLinks += $"{R.T_OPERATION_FAILED}";
                fileOk = false;
            }

            string buttonClicked;
            string runagain = R.T_RUN_AGAIN;
            if (fileOk)
            {
                if (missingMatches > 0)
                {
                    string titleAndMessage = $"{R.T_RESULTS};{messageWithLinks}";
                    MessageInput mi = new MessageInput(MessageType.Warning, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_OK, runagain },
                    };
                    buttonClicked = mi.Show().TextOfButtonClicked;
                }
                else
                {
                    string titleAndMessage = $"{R.T_RESULTS};{messageWithLinks}";
                    MessageInput mi = new MessageInput(MessageType.GoodNews, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_OK, runagain },
                    };
                    buttonClicked = mi.Show().TextOfButtonClicked;
                }
            }
            else
            {
                string titleAndMessage = $"{R.T_ERROR};{messageWithLinks}";
                MessageInput mi = new MessageInput(MessageType.GoodNews, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_OK, runagain }
                };
                buttonClicked = mi.Show().TextOfButtonClicked;
            }

            if (buttonClicked == runagain)
                FindCorrespondence(view, tolXinMm, tolYinMm, tolZinMm, startingFolder);

        }

        private static List<Coordinates> ReadFile(string path)
        {
            List<Coordinates> points = new List<Coordinates>();
            System.IO.StreamReader sr = new System.IO.StreamReader(path);
            try
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string trimmed = line.TrimStart(' ');
                    trimmed = trimmed.TrimStart('\t');
                    if (trimmed.Length == 0)
                        continue;
                    if (trimmed[0] == '%')
                        continue;
                    points.Add(T.Conversions.FileFormat.IdXYZLineToCoordinates(trimmed,
                        Coordinates.ReferenceFrames.Unknown,
                        Coordinates.CoordinateSystemsTypes._3DCartesian));
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                sr.Close();
                sr.Dispose();
            }

            return points;
        }

        private static void WritePointsInFile(string path, string message, List<Coordinates> points, bool append = false, string format = "IDXYZ")
        {
            System.IO.TextWriter tw = new System.IO.StreamWriter(path, append);
            try
            {
                if (points.Count > 0)
                {
                    tw.WriteLine(message);
                    foreach (var item in points)
                    {
                        tw.WriteLine(item.ToString(format));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Could not write the points in {path}", ex);
            }
            finally
            {
                tw.Close();
                tw.Dispose();
            }
        }

        public static List<Coordinates> FindCorrespondenceBasedOnCoordinates(
            List<Coordinates> refList,
            List<Coordinates> mixedList,
            double tolXinM,
            double tolYinM,
            double tolZinM,
            out int missingMatches,
            out List<Coordinates> renammedList,
            out List<Coordinates> additionalPoints,
            out List<Coordinates> notFoundPoints)
        {
            List<Coordinates> startingMixedList = new List<Coordinates>();
            startingMixedList.AddRange(mixedList);
            List<Coordinates> result = new List<Coordinates>();
            additionalPoints = new List<Coordinates>();
            notFoundPoints = new List<Coordinates>();
            List<Coordinates> foundFromMixedPoints = new List<Coordinates>();
            renammedList = new List<Coordinates>();

            missingMatches = 0;
            foreach (var reference in refList)
            {
                if (reference == null)
                    continue;
                double lastD3D = 1000;
                Coordinates bestFound = null;
                foreach (var item in mixedList)
                {
                    if (item == null)
                        continue;
                    double dX = (reference.X - item.X).Value;
                    double dY = (reference.Y - item.Y).Value;
                    double dZ = (reference.Z - item.Z).Value;
                    if (Math.Abs(dX) < tolXinM
                        && Math.Abs(dY) < tolYinM
                        && Math.Abs(dZ) < tolZinM)
                    {
                        double d3D = Survey.GetSpatialDistance(reference, item);
                        if (d3D < lastD3D)
                        {
                            bestFound = item;
                            lastD3D = d3D;
                        }
                    }
                }
                if (bestFound != null)
                {
                    mixedList.Remove(bestFound);
                    result.Add(bestFound);
                    Coordinates renammedPoint = bestFound.Clone() as Coordinates;
                    renammedPoint._Name = reference._Name;
                    renammedList.Add(renammedPoint);
                    foundFromMixedPoints.Add(bestFound); // keeping count of the one found, to add at teh end those not in the ref list
                }
                else
                {
                    result.Add(new Coordinates() { _Name = reference._Name + "_(Not_found)" });
                    notFoundPoints.Add(reference);
                    missingMatches++;
                }
            }

            // list the point from the mixed loist which have no correspondandce
            foreach (var item in startingMixedList)
            {
                if (!foundFromMixedPoints.Contains(item))
                    additionalPoints.Add(item);
            }

            return result;
        }

        public static List<Coordinates> FindCorrespondenceBasedOn3dDistances(List<Coordinates> refList, List<Coordinates> mixedList,
            double tol3dinM, out int missingMatches, out List<Coordinates> renammedList, out List<Coordinates> notFoundPoints)
        {
            List<Coordinates> result = new List<Coordinates>();
            notFoundPoints = new List<Coordinates>();
            renammedList = new List<Coordinates>();
            missingMatches = 0;
            int numbersOfMissingMatches = 1000;

            for (int k = 0; k < refList.Count; k++)
            {
                List<Coordinates> resultForK = MissingIfRefIs(k, refList, mixedList, tol3dinM, out int missingCountForK, out renammedList);
                if (missingCountForK == 0)
                {
                    missingMatches = 0;
                    return resultForK; // return directly if all matches
                }
                if (missingCountForK < numbersOfMissingMatches)
                {
                    result = resultForK;
                    missingMatches = missingCountForK;
                }
            }

            return result;
        }

        // function comparing 3d distance based on a given point as ref
        private static List<Coordinates> MissingIfRefIs(int referencePointIndex, List<Coordinates> refList, List<Coordinates> mixedList,
            double tol3dinM, out int missingCount, out List<Coordinates> renammedList)
        {
            renammedList = new List<Coordinates>();
            List<double> refDistances = new List<double>();
            List<double> mixedDistances = new List<double>();
            List<Coordinates> resultForK = new List<Coordinates>();
            missingCount = 0;
            // compute ref distances
            Coordinates p1 = refList[referencePointIndex];
            foreach (var reference in refList)
                refDistances.Add(Survey.GetSpatialDistance(p1, reference));

            // compute mixed distances
            Coordinates pm1 = mixedList[0];
            foreach (var item in mixedList)
                mixedDistances.Add(Survey.GetSpatialDistance(pm1, item));

            // compare distances
            for (int i = 0; i < refList.Count; i++)
            {
                double lastDD = 1000;
                Coordinates bestFound = null;
                for (int j = 0; j < mixedList.Count; j++)
                {
                    double dD = Math.Abs(refDistances[i] - mixedDistances[j]);
                    if (dD < tol3dinM && dD < lastDD)
                    {
                        bestFound = mixedList[j];
                        lastDD = dD;
                    }
                }
                if (bestFound != null)
                {
                    resultForK.Add(bestFound);

                    Coordinates renammedPoint = bestFound.Clone() as Coordinates;
                    renammedPoint._Name = refList[i]._Name;
                    renammedList.Add(renammedPoint);
                }
                else
                {
                    resultForK.Add(new Coordinates() { _Name = refList[i]._Name + "_(Not found)" });
                    missingCount++;
                }
            }
            return resultForK;
        }

        /// <summary>
        /// return nex name based on the given prefix, if message is set to "defautl", it will show the porposition and prpose a modification, if set to "", it will return without asking)
        /// </summary>
        /// <param name="prefixName"></param>
        /// <param name="em"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        /// Possible input "Point", "Point.", "Point.###", "Point.001", "LHC.MBX.4R8.E", "LHC.STL.001.", "Point(001)"
        public static class Iteration
        {
            public enum Types
            {
                SharpCharOnly,
                SharpCharOrLastNumberOnly,
                All,
                InBrackets
            }
            private static bool Exist(string name, List<string> existingNames)
            {
                return existingNames.Find(x => x.ToUpper() == name.ToUpper()) != null;
            }

            public static string GetNext(string name, List<string> existingNames, Types type)
            {
                return GetNext(new List<string>() { name }, existingNames, type)[0];
            }

            public static List<string> GetNext(List<string> names, List<string> existingNames, Types type)
            {
                // always replace ## with number
                for (int i = 0; i < names.Count; i++)
                {
                    if (names[i].Contains('#'))
                    {
                        int start = names[i].IndexOf('#');
                        int last = names[i].LastIndexOf('#');
                        int size = last + 1 - start;
                        string prefix = names[i].Substring(0, start);
                        string sufix = names[i].Substring(last + 1, names[i].Length - (last + 1));
                        int count = 1;
                        string tempName = prefix + count.ToString($"D{size}") + sufix;
                        while (existingNames.Contains(tempName))
                        {
                            count++;
                            tempName = prefix + count.ToString($"D{size}") + sufix;
                        }
                        names[i] = tempName;
                    }
                }
                if (type == Types.SharpCharOnly)
                    return names;



                List<string> prefixes = new List<string>();
                List<int> numbers = new List<int>();
                List<int> nls = new List<int>();
                List<string> openBox = new List<string>();
                List<string> closingBox = new List<string>();
                List<string> suffixes = new List<string>();
                List<string> finalDots = new List<string>();

                foreach (var item in names)
                {
                    string prefix = "";
                    string numberAsString = "";
                    int nl;
                    string box = "";
                    string suffix = "";
                    string finalDot = "";


                    string name = item.Replace('#', '0');


                    if (name.Last() == '.')
                    {
                        finalDot = ".";
                        name = name.Substring(0, name.Length - 1);
                    }

                    // separate
                    if (name.Contains('[') && name.Contains(']'))
                    {
                        string[] cut1 = name.Split('[');
                        string[] cut2 = cut1[1].Split(']');

                        prefix = cut1[0];
                        numberAsString = cut2[0];
                        box = "[";
                        suffix = cut2[1];
                    }
                    else
                    {
                        //starting prefix
                        prefix = name;
                        // detect how many char are number
                        int i = 1;
                        while (int.TryParse(prefix.Substring(prefix.Length - 1), out int j))
                        {
                            if (j > -1) // Too avoid to get the '-' as number part for '-2'
                            {
                                numberAsString = prefix.Substring(prefix.Length - 1, 1) + numberAsString;

                                if (prefix.Length < 1) break;
                                i++;
                            }
                            else
                                break;
                            prefix = prefix.Substring(0, prefix.Length - 1);
                            if (prefix.Length < 1) break;
                        }
                    }

                    int.TryParse(numberAsString, out int number);

                    nl = numberAsString.Length;

                    prefixes.Add(prefix);
                    if (numberAsString != "")
                        numbers.Add(number);
                    nls.Add(nl);
                    openBox.Add(box);
                    switch (box)
                    {
                        case "[": closingBox.Add("]"); break;
                        case "(": closingBox.Add(")"); break;
                        default: closingBox.Add(""); break;
                    }
                    suffixes.Add(suffix);
                    finalDots.Add(finalDot);
                }

                int maxN = 0;
                if (numbers.Count != 0)
                    maxN = numbers.Max();
                int maxNl = nls.Max();
                if (maxNl < 1) maxNl = 1;
                string format = "D" + maxNl;

                int AvailableNumber = GetNext_FindAvailableNumber(format, maxNl, prefixes, suffixes, maxN, existingNames, openBox, closingBox, finalDots);

                return GetNext_BuildNames(prefixes, openBox, AvailableNumber, format, closingBox, suffixes, finalDots);
            }

            static private List<string> GetNext_BuildNames(List<string> prefixes, List<string> openBox, int testedNumber, string format, List<string> closingBox, List<string> suffixes2, List<string> finalDots)
            {
                List<string> foundNames = new List<string>();
                for (int i = 0; i < prefixes.Count; i++)
                {
                    foundNames.Add(prefixes[i] + openBox[i] + testedNumber.ToString(format) + closingBox[i] + suffixes2[i] + finalDots[0]);
                }
                return foundNames;
            }


            static private int GetNext_FindAvailableNumber(
                string format, int maxNl,
                List<string> prefixes, List<string> suffixes,
                int maxN, List<string> existingNames,
                List<string> openBox, List<string> closingBox, List<string> finalDots)
            {
                try
                {
                    HourGlass.Set();
                    bool found = false;

                    // Find next name available
                    int testedNumber = (int)Math.Pow(10, maxNl) - 1;
                    if (testedNumber > 1000) testedNumber = 999;

                    while (!found)
                    {
                        testedNumber--;
                        for (int i = 0; i < prefixes.Count; i++)
                        {
                            string nameToTry = prefixes[i] + openBox[i] + testedNumber.ToString(format) + closingBox[i] + suffixes[i] + finalDots[i];
                            if (Exist(nameToTry, existingNames))
                                found = true;
                        }
                        if (testedNumber < 1)
                            found = true;

                        // not lower then the number from the input
                        if (testedNumber < maxN)
                            found = true;
                    }

                    testedNumber++; // we found the existing, +1 is the wanted next number

                    return testedNumber;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    HourGlass.Remove();
                }
            }




            /// <summary>
            /// If message different that "" then will propose to choose name
            /// </summary>
            /// <param name="name"></param>
            /// <param name="em"></param>
            /// <param name="message"></param>
            /// <returns></returns>
            internal static string GetNextWithChoice(string name, E.Manager.Module em, string message = "Default", List<E.Point> additionalPoints = null)
            {
                var allPointNames = new List<string>();
                allPointNames.AddRange(em.GetPointsNames());
                if (additionalPoints != null)
                {
                    foreach (var item in additionalPoints)
                    {
                        allPointNames.Add(item._Name);
                    }
                }
                    
                string s = GetNext(new List<string>() { name }, allPointNames, Types.All)[0];

                System.Windows.Forms.BindingSource b = new System.Windows.Forms.BindingSource
                {
                    DataSource = new List<string>() { s, s + 1 }
                };

                if (message != "")
                {
                    if (message == "Default") message = $"{R.T_DO_YOU_LIKE_THIS_NAME};{R.T_HERE_IS_THE_PROPOSED_NAME_PLEASE_CORRECT_IT_IT_NEEDED}";
                    var r = em.View.ShowMessageOfPredefinedInput(message, R.T_OK, null, b, null);
                    if (r is string t)
                        return t;
                    else
                        return null;
                }
                return s;
            }




            /// <summary>
            /// Iterate the number inside brackets of theodolite sequence to not collude with ElementManagerPoints
            /// </summary>
            /// <param name="sequence"></param>
            /// <param name="elementManager"></param>
            public static void GetNext(C.SequenceTH sequence, E.Manager.Module elementManager)
            {
                GetNext(sequence, elementManager.GetPointsInAllElements());
            }


            /// <summary>
            /// Iterate the number inside brackets of theodolite sequence to not collude with list of points
            /// </summary>
            /// <param name="sequence"></param>
            /// <param name="points"></param>
            public static void GetNext(C.SequenceTH sequence, List<E.Point> points)
            {
                int number = 0;
                bool found = true;

                try
                {
                    HourGlass.Set();

                    while (found)
                    {
                        number++;
                        found = false;
                        foreach (E.Point item in sequence)
                        {
                            string[] parts = item._Name.Split('[');
                            parts = parts[1].Split(']');
                            int length = parts[0].Length;
                            string format = "D" + length;
                            string iteratedName = item._Name.Split('[')[0] + "[" + number.ToString(format) + "]";
                            foreach (E.Point knownPoint in points)
                            {
                                if (knownPoint._Name == iteratedName) found = true;
                            }
                        }
                    }

                    // now number should be one that it not use for any points of the sequence, so lets rename the points from the sequence

                    foreach (E.Point item in sequence)
                    {
                        string afterbracket = item._Name.Split('[')[1];
                        string insidebracket = afterbracket.Split(']')[0];
                        int numberDigits = insidebracket.Length;
                        item._Name = item._Name.Split('[')[0] + "[" + number.ToString("D" + numberDigits) + "]";
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    HourGlass.Remove();
                }
            }
        }

        internal static bool ExistanceProblematicOf(E.Point point, List<E.Point> list, out List<E.Point> existingPoints, out List<E.Point> existingPointsInDifferentGroup)
        {
            existingPointsInDifferentGroup = list.FindAll(
                x =>
                x._Name.ToUpper() == point._Name.ToUpper() &&
                x.State != E.Element.States.Bad
                );

            existingPoints = existingPointsInDifferentGroup.FindAll(
                x =>
                x._Origin.ToUpper() == point._Origin.ToUpper()
                );

            return existingPoints.Count > 0;
        }

        internal static void Have_H_and_Z(List<E.Point> points,
            out List<E.Point> pointsWithoutZ,
            out bool thereIsPointsWithoutH, out bool thereIsPointsWithoutZ)
        {
            thereIsPointsWithoutH = false;
            thereIsPointsWithoutZ = false;
            pointsWithoutZ = new List<E.Point>();
            foreach (var point in points)
            {
                if (!CoordinatesInAllSystems.CoordinatesExistInAGivenSystem("CCS-Z", point._Coordinates.ListOfAllCoordinates, out _))
                {
                    thereIsPointsWithoutZ = true;
                    pointsWithoutZ.Add(point);
                }
                if (!CoordinatesInAllSystems.CoordinatesExistInAGivenSystem("CCS-H", point._Coordinates.ListOfAllCoordinates, out _))
                {
                    thereIsPointsWithoutH = true;
                    pointsWithoutZ.Add(point);
                }
            }
        }

        internal static E.Point FindFirstPointByName(List<E.Point> list, string name)
        {
            if (list == null || list.Count == 0) return null;
            return list.Find(x => x._Name.ToUpper() == name.ToUpper());
        }

        internal static Coordinates FindFirstPointByName(List<Coordinates> list, string name)
        {
            return list.Find(x => x._Name.ToUpper() == name.ToUpper());
        }

        internal static List<E.Element> FindByNameAndOrigins(CloneableList<E.Element> elements, string name, List<string> listOfOriginName)
        {
            return elements.FindAll(x =>
                x._Name.ToUpper() == name.ToUpper() &&
                listOfOriginName.Contains(x._Origin)
                );
        }

        internal static E.Point GetBaricentre(CloneableList<M.Measure> measures, Z.Zone zone = null)
        {
            List<E.Point> points = new List<E.Point>();
            measures.FindAll(x => x._Status is M.States.Good).ToList().ForEach(x => points.Add(x._OriginalPoint));
            return GetBaricentre(points, zone);
        }

        internal static E.Point GetBaricentre(List<Polar.Measure> measures, Z.Zone zone = null)
        {
            List<E.Point> points = new List<E.Point>();
            measures.FindAll(x => x._Status is M.States.Good).ToList().ForEach(x => points.Add(x._OriginalPoint));
            return GetBaricentre(points, zone);
        }

        internal static E.Point GetBaricentre(List<E.Point> points, Z.Zone zone = null)
        {

            // CCS coordinates?
            bool allCCS = true;
            bool allSU = true;
            points.ForEach(x =>
            {
                if (!x._Coordinates.HasCcs) allCCS = false;
                if (!x._Coordinates.HasLocal) allSU = false;
            });

            if (allCCS)
            {
                List<Coordinates> coordinates = new List<Coordinates>();
                points.ForEach(x => coordinates.Add(x._Coordinates.Ccs));
                E.Point barycentre = new E.Point("Barycentre");
                barycentre._Coordinates.Ccs = GetBaricentre(coordinates);
                Systems.ZToH(barycentre, Coordinates.ReferenceSurfaces.RS2K);
                //barycentre._Coordinates.Ccs.ReferenceSurface = E.Coordinates.ReferenceSurfaces.RS2K;
                barycentre._Coordinates.Ccs.ReferenceFrame = Coordinates.ReferenceFrames.CCS;
                barycentre._Coordinates.Ccs.CoordinateSystem = Coordinates.CoordinateSystemsTypes._2DPlusH;
                E.Manager.Module.TryToFillMissingCoordinates(barycentre, Coordinates.ReferenceFrames.CCS, zone);
                return barycentre;
            }

            if (allSU)
            {
                List<Coordinates> coordinates = new List<Coordinates>();
                points.ForEach(x => coordinates.Add(x._Coordinates.Local));
                E.Point barycentre = new E.Point("Barycentre");
                barycentre._Coordinates.Local = GetBaricentre(coordinates);
                if (zone != null)
                {
                    E.Manager.Module.TryToFillMissingCoordinates(barycentre, Coordinates.ReferenceFrames.Su2000Machine, zone);
                }
                return barycentre;
            }

            return null;
        }

        internal static Coordinates GetBaricentre(List<Coordinates> coordinates)
        {
            double sumX = 0;
            double sumY = 0;
            double sumZ = 0;
            int n = 0;
            coordinates.ForEach(x => { sumX += x.X.Value; sumY += x.Y.Value; sumZ += x.Z.Value; n++; });

            return new Coordinates("Barycentre", sumX / n, sumY / n, sumZ / n);
        }

        internal static int GetLongerName(List<E.Point> allPointsInitialiazedInTheInput)
        {
            int max = 0;
            foreach (var item in allPointsInitialiazedInTheInput)
            {
                if (item._Name.Length > max)
                    max = item._Name.Length;
            }
            return max;
        }

        internal static bool IsNameContainedIn(string pointName, CloneableList<E.Point> points)
        {
            return points.Find(x => x._Name == pointName) != null;
        }
    }



    internal static class Module
    {

        internal static List<Polar.Station.Module> GetModules_StationThedolite(TSU.Module t)
        {
            List<Polar.Station.Module> l = new List<Polar.Station.Module>();
            foreach (var item in t.childModules)
            {
                if (item is FinalModule)
                {
                    foreach (var item2 in item.childModules)
                    {
                        if (item2 is Polar.Station.Module)
                        {
                            l.Add(item2 as Polar.Station.Module);
                        }
                    }
                }
            }
            return l;
        }

        internal static List<Station.Module> GetActiveStationModule(TSU.Module m)
        {
            List<Station.Module> activeModules = new List<Station.Module>();

            if (m is FinalModule)
            {
                FinalModule fm = m as FinalModule;
                if (fm._ActiveStationModule != null)
                {
                    activeModules.Add(fm._ActiveStationModule);
                    return activeModules;
                }
            }

            foreach (var subModule in m.childModules)
            {
                activeModules.AddRange(GetActiveStationModule(subModule));
            }

            return activeModules;
        }

        internal static List<Polar.Station.Module> GetModules_SettedUpSTM(Tsunami t)
        {
            List<Polar.Station.Module> l = new List<Polar.Station.Module>();
            foreach (Polar.Station.Module item in GetModules_StationThedolite(t))
            {
                if (item.stationParameters.Setups.ActualState != Polar.Station.Parameters.Setup.States.Done)
                {
                    l.Add(item);
                }
            }
            return l;
        }

        internal static List<Station.Module> FindAllStationModules(TSU.Module parentModule)
        {
            List<Station.Module> sms = new List<Station.Module>();

            foreach (var module in parentModule.childModules)
            {
                dynamic d = module;
                sms.AddRange(FindAllStationModules(d));
            }

            return sms;
        }

        internal static List<Station.Module> FindAllStationModules(FinalModule finalModule)
        {
            return finalModule.StationModules;
        }

        internal static List<Station.Module> FindAllStationModules(Guided.Group.Module groupModule)
        {
            List<Station.Module> sms = new List<Station.Module>();
            foreach (var module in groupModule.SubGuidedModules)
            {
                sms.AddRange(FindAllStationModules(module));
            }
            return sms;
        }
    }

    internal static class Measure
    {
        internal static List<M.Measure> GetByState<T>(List<M.Measure> measures)
        {
            return measures.FindAll(x => x._Status is T);
        }

        internal static Polar.Measure GetLastByState<T>(List<M.Measure> measures)
        {
            return measures.LastOrDefault(x => x._Status is T) as Polar.Measure;
        }

        internal static Polar.Measure GetLastByStateException<T>(List<M.Measure> measures)
        {
            return measures.LastOrDefault(x => !(x._Status is T)) as Polar.Measure;
        }

        internal static List<Polar.Measure> GetPolarByState<T>(List<M.Measure> measures)
        {
            return measures.FindAll(x => x._Status is T).Cast<Polar.Measure>().ToList();
        }

        internal static List<Polar.Measure> GetPolarByState<T1, T2>(List<M.Measure> measures)
        {
            return measures.FindAll(x => x._Status is T1 || x._Status is T2).Cast<Polar.Measure>().ToList();
        }

        internal static List<Polar.Measure> GetByOrigin(List<Polar.Measure> measures, string originName)
        {
            return measures.FindAll(x => x.Origin == originName);
        }

        internal static List<M.Measure> GetOkToCompare(CloneableList<M.Measure> measures, M.Measure measureToCompare)
        {
            return measures.FindAll(x =>
            x._Point._Name == measureToCompare._Point._Name &&
            !(x._Status is M.States.Bad) &&
            x.Origin == measureToCompare.Origin);
        }

        internal static List<Polar.Measure> GetOkToReduce(CloneableList<M.Measure> measures)
        {
            return measures.FindAll(x =>
            x._Status is M.States.Good ||
            x._Status is M.States.Control).Cast<Polar.Measure>().ToList();
        }

        internal static I.FaceType GetFaceType(CloneableList<M.Measure> measuresTaken)
        {
            bool someF1 = false;
            bool someF2 = false;
            foreach (Polar.Measure item in measuresTaken.Cast<Polar.Measure>())
            {
                if (!(item._Status is M.States.Bad))
                {
                    if (item.Face == I.FaceType.Face1)
                    {
                        someF1 = true;
                    }
                    else if (item.Face == I.FaceType.Face2)
                    {
                        someF2 = true;
                    }
                    else
                        return I.FaceType.UnknownFace;

                    if (someF1 & someF2) return I.FaceType.UnknownFace;
                }
            }

            if (someF1 & !someF2)
                return I.FaceType.Face1;
            else if (!someF1 & someF2)
                return I.FaceType.Face2;
            else
                return I.FaceType.UnknownFace;
        }

        internal static bool FindStationIn(TSU.Module mod, M.Measure mes, out List<Station> stations)
        {
            stations = new List<Station>();
            string stationNameFromMeasure = mes.Origin;
            foreach (Station.Module stm in Module.FindAllStationModules(mod))
            {
                if (stm._Station._Name == stationNameFromMeasure)
                    stations.Add(stm._Station);
            }
            return stations.Count > 0;
        }

        internal static List<M.Measure> GetMeasuresByNames(List<M.Measure> measuresTaken, List<E.Point> points)
        {
            List<M.Measure> l = new List<M.Measure>();
            List<string> names = new List<string>();
            points.ForEach(x => names.Add(x._Name.ToUpper()));

            foreach (var item in measuresTaken)
            {
                if (names.Contains(item._Point._Name.ToUpper()))
                    l.Add(item);
            }
            return l;
        }

        internal static Polar.Measure FindByNameAndVerticalValue(List<Polar.Measure> measures, string name, double av)
        {
            return measures.Find(x => x._PointName == name && x.Angles.raw.Vertical.Value == av);
        }

        internal static M.Measure FindByDate(CloneableList<M.Measure> measuresTaken, DateTime date)
        {
            return measuresTaken.Find(x => x._Date == date);
        }

        internal static Polar.Measure FindByName(List<Polar.Measure> measures, string name)
        {
            return measures.Find(x => x._Point._Name == name);
        }

        internal static List<E.Point> GetPointsByGeodeRole(List<M.Measure> measures, IO.SUSoft.Geode.Roles roles)
        {
            List<E.Point> points = new List<E.Point>();
            foreach (var measure in measures)
            {
                if ((roles & measure.GeodeRole) == measure.GeodeRole)
                    points.Add(measure._Point);
            }
            return points;
        }
    }

}
