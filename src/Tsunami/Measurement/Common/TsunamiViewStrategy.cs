﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TSU.Tools;
using TSU.Views;
using TSU.Views.Message;
using R = TSU.Properties.Resources;

namespace TSU.Common
{
    public partial class TsunamiView : ModuleView
    {
        public static List<MessageTsu> ExistingMessages { get; internal set; } = new List<MessageTsu>();

        #region TsunamiViewsStrategy
        // This class is nested in th tsanamiView bevcuse there is nt other it should be use
        public abstract class Strategy
        {
            TsunamiView _TsunamiView;

            protected Strategy(TsunamiView TsunamiView)
            {
                _TsunamiView = TsunamiView;
            }

            public interface ITsunamiViewsStrategy
            {
                TsunamiViewsStrategies type { get; }
                void Reload();
                void Unload();
                void AddView(TsuView view);
                TsuView GetActiveView();
                void MoveToNextActiveView();
            }


            #region Tab
            public class Tab : Strategy, ITsunamiViewsStrategy
            {
                public TsunamiViewsStrategies type
                {
                    get
                    {
                        return TsunamiViewsStrategies.Tab;
                    }
                }

                public Tab(TsunamiView TsunamiView)
                    : base(TsunamiView)
                {
                    var tabControl = new TabControl();
                    //tabControl.DrawMode = TabDrawMode.OwnerDrawFixed;
                    tabControl.Dock = DockStyle.Fill;
                    tabControl.Padding = new System.Drawing.Point(5, 7); // this allow higher header

                    _TsunamiView.tabControl1 = tabControl;
                    if (_TsunamiView.Width > 750 && _TsunamiView.Height > 750)// we assume this is not the tablette
                    {
                        // tablette resolution is 720*1440
                        _TsunamiView.SetSizeForMacro(1038, 720);
                    }
                }

                public TsuView GetActiveView()
                {
                    if (_TsunamiView.tabControl1.SelectedTab.Controls.Count > 0)
                        return _TsunamiView.tabControl1.SelectedTab.Controls[0] as TsuView;
                    else
                        return _TsunamiView.Module.Menu.View;
                }

                public void AddView(TsuView view)
                {
                    view.VisibleChanged -= OnVisibleChanged;
                    TabPage tab = new TabPage(view._Name);
                    view.ShowDockedFill();
                    tab.Controls.Add(view);
                    _TsunamiView.tabControl1.TabPages.Add(tab);
                    _TsunamiView.tabControl1.SelectedIndex = _TsunamiView.tabControl1.TabCount - 1;
                    _TsunamiView.Controls.Add(_TsunamiView.tabControl1);
                    view.VisibleChanged += OnVisibleChanged;
                }

                private void OnVisibleChanged(object sender, EventArgs e)
                {
                    TsuView view = sender as TsuView;
                    Debug.WriteInConsole($"OnVisibleChanged view._Name={view._Name}, view.Visible={view.Visible}, view.Parent is TabPage={view.Parent is TabPage}");
                    // When the user reopens the module, in fact the view is only shown
                    // So if a view we are listening to becomes visible, we check if it is in a tab, and if not we create one
                    if (view.Visible && !(view.Parent is TabPage))
                    {
                        AddView(view);
                    }
                    // When the user closes a module, in fact it's hidden, so we remove the corresponding tab
                    if (!view.Visible && view.Parent is TabPage tab)
                    {
                        tab.Controls.Remove(view);
                        _TsunamiView.tabControl1.TabPages.Remove(tab);
                    }
                }

                public void Reload()
                {
                    this._TsunamiView.Controls.Clear();
                    foreach (TSU.Module item in this._TsunamiView._Module.childModules)
                    {
                        this._TsunamiView.AddView(item._TsuView);
                    }
                }

                void ITsunamiViewsStrategy.MoveToNextActiveView()
                {
                    throw new NotImplementedException();
                }

                void ITsunamiViewsStrategy.Unload()
                {
                    foreach (TSU.Module item in this._TsunamiView._Module.childModules)
                    {
                        item._TsuView.VisibleChanged -= OnVisibleChanged;
                    }
                }
            }
            #endregion

            #region Windows
            public class Windows : Strategy, ITsunamiViewsStrategy
            {
                public TsunamiViewsStrategies type
                {
                    get
                    {
                        return TsunamiViewsStrategies.Windows;
                    }
                }

                public Windows(TsunamiView TsunamiView)
                    : base(TsunamiView)
                {
                }

                public TsuView GetActiveView()
                {
                    if (_TsunamiView.Controls.Count > 1)
                        return _TsunamiView.Controls[0] as TsuView;
                    else
                        return _TsunamiView.Module.Menu.View;
                }

                public void MoveToNextActiveView()
                {
                    CompositeView activeView = GetActiveView() as CompositeView;
                    FinalModule activeModule = activeView.Module as FinalModule;
                    bool foundShowNextOn = false;
                    FinalModule previous = null;
                    var l = _TsunamiView.Module.MeasurementModules;
                    for (int i = l.Count - 1; i > -1; i--)
                    {
                        FinalModule item = l[i];
                        if (foundShowNextOn)
                        {
                            if (previous != null)
                                AddView(previous.View);
                            else
                                AddView(l[0].View);
                            return;
                        }
                        if (item == activeModule)
                            foundShowNextOn = true;
                        else
                            previous = item;
                    }
                    if (previous != null) { AddView(previous.View); }
                }

                public void AddView(TsuView view)
                {
                    if (view is Functionalities.MenuView)
                    {
                        view.ShowDockedLeft();
                    }
                    else
                    {
                        view.ShowDockedFill();
                    }

                    _TsunamiView.Controls.Add(view);
                    view.UpdateView();
                    view.FormBorderStyle = FormBorderStyle.None;
                    view.BringToFront();
                }

                public void Reload()
                {
                    this._TsunamiView.Controls.Clear();
                    foreach (TSU.Module item in this._TsunamiView._Module.childModules)
                    {
                        this._TsunamiView.AddView(item._TsuView);
                    }
                }

                void ITsunamiViewsStrategy.Unload()
                {
                    // Nothing to do
                }
            }
            #endregion

            #region DockableWIndows
            public class DockableWIndows : Strategy, ITsunamiViewsStrategy
            {
                public TsunamiViewsStrategies type
                {
                    get
                    {
                        return TsunamiViewsStrategies.DockableWIndows;
                    }
                }
                Crom.Controls.Docking.DockContainer _dockContainer;
                public DockableWIndows(TsunamiView TsunamiView)
                    : base(TsunamiView)
                {
                    // Setting up the container allwing the dockable style
                    _dockContainer = new Crom.Controls.Docking.DockContainer();
                    _dockContainer.Dock = DockStyle.Fill;
                    _dockContainer.ShowContextMenu += new EventHandler<Crom.Controls.Docking.FormContextMenuEventArgs>(OnShowConext);
                    _dockContainer.FormClosing += new EventHandler<Crom.Controls.Docking.DockableFormClosingEventArgs>(OnFormCLosing);

                    _TsunamiView.Controls.Add(_dockContainer);
                }

                private void OnFormCLosing(object sender, Crom.Controls.Docking.DockableFormClosingEventArgs e)
                {
                    e.Cancel = true;
                    Crom.Controls.Docking.DockableFormInfo info;
                    int count = _dockContainer.Count;
                    for (int i = 0; i < count; i++)
                    {
                        info = _dockContainer.GetFormInfoAt(i);
                        if (info.DockableForm != e.Form) continue;
                        if (info.DockableForm is Functionalities.MenuView)
                        {
                            new MessageInput(MessageType.Warning, R.T464).Show();
                            break;
                        }
                        string Cancel = R.T_CANCEL;
                        MessageInput mi1 = new MessageInput(MessageType.Warning, R.T465)
                        {
                            ButtonTexts = new List<string> { "Close", Cancel }
                        };
                        if (mi1.Show().TextOfButtonClicked != Cancel)
                        {
                            MessageInput mi = new MessageInput(MessageType.Critical, R.T466)
                            {
                                ButtonTexts = new List<string> { "CLOSE IT!", Cancel },
                            };
                            if (mi.Show().TextOfButtonClicked != Cancel) e.Cancel = false;
                        }
                    }

                }

                public TsuView GetActiveView()
                {
                    throw new NotImplementedException();
                }

                private void OnShowConext(object sender, Crom.Controls.Docking.FormContextMenuEventArgs e)
                {
                    e.Form.Hide();
                }

                private Crom.Controls.Docking.DockableFormInfo CreateDockableWindows(TsuView view, Crom.Controls.Docking.zAllowedDock allowed, DockStyle style)
                {
                    // New Windows to contain the view
                    view.FormBorderStyle = FormBorderStyle.None;
                    view.TopLevel = false;
                    view.Show();
                    // Add the docakable vue
                    Crom.Controls.Docking.DockableFormInfo info = _dockContainer.Add(view as Form, Crom.Controls.Docking.zAllowedDock.None, view.Guid);

                    _dockContainer.Show();
                    return info;
                }

                public void AddView(TsuView view)
                {
                    // Treat the view
                    view.ShowDockedFill();

                    // Add ths view as dockable
                    if (view is Functionalities.MenuView)
                        CreateDockableWindows(view, Crom.Controls.Docking.zAllowedDock.Horizontally, DockStyle.Left);
                    else
                        CreateDockableWindows(view, Crom.Controls.Docking.zAllowedDock.All, DockStyle.Fill);
                }

                public void Reload()
                {
                    this._TsunamiView.Controls.Clear();
                    foreach (TSU.Module item in this._TsunamiView._Module.childModules)
                    {

                        this._TsunamiView.AddView(item._TsuView);
                    }
                }

                void ITsunamiViewsStrategy.MoveToNextActiveView()
                {
                    throw new NotImplementedException();
                }

                void ITsunamiViewsStrategy.Unload()
                {
                    throw new NotImplementedException();
                }
            }


            #endregion

            #region DockableWIndows2
            //public class DockableWIndows2 : TsunamiViewsStrategy, ITsunamiViewsStrategy
            //{
            //Crom.Controls.Docking.DockContainer _dockContainer;
            //public DockableWIndows2(TsunamiView TsunamiView)
            //    : base(TsunamiView)
            //{
            //    // Setting up the container allwing the dockable style
            //    _dockContainer = new Crom.Controls.DockContainer();
            //    _dockContainer.Dock = DockStyle.Fill;
            //    _TsunamiView.splitContainer3.Panel1.Controls.Add(_dockContainer);

            //    // Adding the menu
            //    _TsunamiView.menu._View.ShowDockedFill();
            //    CreateDockableWindows(_TsunamiView.menu._View, Crom.Controls.zDockMode.Left);
            //}

            //private void CreateDockableWindows(TsuView view, Crom.Controls.zDockMode side)
            //{
            //    // New Windows to contain the view
            //    Crom.Controls.DockableToolWindow w = new Crom.Controls.DockableToolWindow();
            //    w.HideInsteadClosing = true;
            //    w.Icon = view.Icon;
            //    w.Text = view._Name;
            //    w.MinimumSize = view.MinimumSize;
            //    //w.StyleChanged += new EventHandler(onstyleChange);
            //    //w.Move += new EventHandler(OnMove);
            //    //w.FormBorderStyle = FormBorderStyle.SizableToolWindow;
            //    //w.LocationChanged += new EventHandler(OnMove);
            //    //w.ResizeEnd += new EventHandler(OnResizeEnd);
            //    //w.MouseClick +=  new MouseEventHandler(OnMouseClick);
            //    w.Controls.Add(view);
            //    w.Show();

            //    // Add the docakable vue
            //    _dockContainer.AddToolWindow(w);
            //    _dockContainer.DockToolWindow(w, side);
            //}

            //public void AddView(TsuView view)
            //{
            //    // Treat the view
            //    view.ShowDockedFill();

            //    // Add ths view as dockable
            //    CreateDockableWindows(view, Crom.Controls.zDockMode.Top);
            //}

            //}

            #endregion
        }

        internal void Clean()
        {
            if (this.tabControl1 != null)
            {
                List<TabPage> emptyPages = this.tabControl1.Controls.OfType<TabPage>()
                                                                    .Where(item => item.Controls.Count == 0)
                                                                    .ToList();

                foreach (TabPage item in emptyPages)
                    this.tabControl1.Controls.Remove(item);
            }
        }


        internal Label ShowTopMessage(string message, System.Drawing.Color color)
        {
            Label label = new Label()
            {
                Name = "TopMessage",
                Font = Tsunami2.Preferences.Theme.Fonts.Large,
                Text = message,
                Dock = DockStyle.Top,
                BackColor = color,
                Height = Tsunami2.Preferences.Theme.Fonts.LargeTextSize * 2

            };
            this.Controls.Add(label);
            foreach (var item in Tsunami2.Properties.childModules)
            {
                item.View.UpdateView();

            }

            return label;
        }



        internal void RemoveTopMessage(Label label)
        {
            this.Controls.Remove(label);
        }

        internal void SetSizeForMacro(int h, int w)
        {
            if (this.InvokeRequired)
            {
                // Invoke the event handler on the main UI thread
                Invoke(new Action(() =>
                {
                    SetSizeForMacro(h, w);
                }));
                return;
            }

            this.WindowState = FormWindowState.Normal;
            this.Height = h;
            this.Width = w;

            this.Top = Screen.PrimaryScreen.Bounds.Top; ;
            this.Left = Screen.PrimaryScreen.Bounds.Left; ;
            //Maximise();

            this.Refresh();
            //var wa = System.Windows.Forms.Screen.FromControl(this).WorkingArea;
            Macro.Resolution_Screen_H = this.Height;
            Macro.Resolution_Screen_W = this.Width;
        }

        

        #endregion
    }
}
