﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TSU.Functionalities.ManagementModules;
using MathNet.Numerics.LinearAlgebra;
using TSU.Common.Elements;
using System.Security.Policy;
using R = TSU.Properties.Resources;
using TSU.Common;
using System.Xml.Serialization;
using Microsoft.Office.Interop.Excel;


namespace TSU.Common
{
    public class CoordinateSystem: TsuObject
    {
        [XmlAttribute]
        public string Description { get; set; } = "System added by the user from a transformation matrix";
        [XmlAttribute]
        public Coordinates.CoordinateSystemsTypes type { get; set; } = Coordinates.CoordinateSystemsTypes._3DCartesian;
        [XmlAttribute]
        public Coordinates.ReferenceSurfaces ReferenceSurface { get; set; } = Coordinates.ReferenceSurfaces.RS2K;
        [XmlAttribute]
        public Coordinates.ReferenceFrames ReferenceFrame { get; set; } = Coordinates.ReferenceFrames.Unknown;

        [XmlAttribute]
        public List<string> AxisNames { get; set; } = new List<string>();
        [XmlAttribute]
        public string LongName { get; set; } = "New Frame";

        [XmlIgnore]
        public System.Drawing.Image Image { get; set; } = R.CS_Systems;

        [XmlIgnore]
        public Matrix<double> TransformatonMatrix { get; set; }

        [XmlAttribute]
        public string TransformatonMatrixAsString
        { 
            get
            {
                if (TransformatonMatrix != null)
                    return TransformatonMatrix.ToString();
                else
                    return "";
            }
            set
            {
                string matrixAsString = value;
                if (matrixAsString == "")
                {
                    TransformatonMatrix = null;
                    return;
                }
                //  DenseMatrix 4x3-Double
                //  0.988245 - 0.152043 - 0.0159852
                //  - 0.0134341 - 0.190519    0.981592
                //  - 0.15229 - 0.969838 - 0.190321
                //  1.40109    1.27576     3.20219
                char[] delims = new[] { '\r', '\n' };
                string[] rows = matrixAsString.Split(delims, StringSplitOptions.RemoveEmptyEntries);
                string[,] matrix = new string[4, 3];
                for (int i = 1; i < rows.Length; i++)
                {
                    string[] columns = rows[i].Replace("- ","-").Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                    for     (int j = 0; j < columns.Length; j++)
                    {
                        matrix[i-1,j] = columns[j];
                    }
                }
                TransformatonMatrix = NewFrameCreation_Library.Calculation.MatrixManipulation.GetMatrixFrom(matrix);
            }
        }

        internal static bool ExistingNameIn(string name, List<CoordinateSystem> list)
        {
            return GetByNameIn(name,list) != null;
        }

        public override string ToString()
        {
            return $"{_Name} is {Description}";
        }

        /// <summary>
        /// returen existing CS from a list or from the preferenced list by default
        /// </summary>
        /// <param name="name"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        internal static CoordinateSystem GetByNameIn(string name, List<CoordinateSystem> list = null)
        {
            if (list == null)
                list =TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.AllTypes;
            foreach (CoordinateSystem c in list)
            {
                if (c._Name.ToUpper() == name.ToUpper())
                    return c;
            }
            return null;
        }


        internal static List<CoordinateSystem> GetNativeSystems()
        {
            List<CoordinateSystem> natives = new List<CoordinateSystem>();

            Elements.Coordinates.ReferenceSurfaces surface = Coordinates.ReferenceSurfaces.RS2K;
            natives.Add(new CoordinateSystem()
            {
                _Name = "CCS-H",
                AxisNames = new List<string>() { "Xccs", "Yccs", "H" },
                type = Coordinates.CoordinateSystemsTypes._2DPlusH,
                ReferenceFrame=  Coordinates.ReferenceFrames.CernXYHg00Machine,
                TransformatonMatrix = null,
                LongName= R.T_CS_CERN + " XYH",
                Description = "Non cartesian frame with H being the gravity",
                Image = R.CS_CCS
            });

            natives.Add(new CoordinateSystem()
            {
                _Name = "CCS-Z",
                AxisNames = new List<string>() { "Xccs", "Yccs", "Z" + surface },
                type = Coordinates.CoordinateSystemsTypes._3DCartesian,
                TransformatonMatrix = null,
                LongName = R.T_CS_CERN + " XYZ",
                Description = R.T_CS_CERN_DETAILS,
                Image = R.CS_CCS
            });

            natives.Add(new CoordinateSystem()
            {
                _Name = "MLA",
                AxisNames = new List<string>() { "Xmla", "Ymla", "Zmla" },
                TransformatonMatrix = null,
                LongName = "MLA",
                Description = R.T_CS_LOCAL_DETAILS,
                Image = R.CS_SU
            });

            natives.Add(new CoordinateSystem()
            {
                _Name = "SU",
                AxisNames = new List<string>() { "Xsu", "Ysu", "Zsu" },
                TransformatonMatrix = null,
                LongName = "Survey",
                Description = R.T_CS_LOCAL_DETAILS,
                Image = R.CS_SU
            });

            natives.Add(new CoordinateSystem()
            {
                _Name = "PHYSICIST",
                AxisNames = new List<string>() { "Xphys", "Yphys", "Zphys" },
                TransformatonMatrix = null,
                LongName = R.T_CS_PHYS,
                Description = R.T_CS_PHYS_DETAILS,
                Image = R.CS_Phys
            });

            natives.Add(new CoordinateSystem()
            {
                _Name = "TOSTATION",
                AxisNames = new List<string>() { "Tstn", "Rstn", "Vloc" },
                TransformatonMatrix = null,
                LongName = R.T_CS_STATION,
                Description = R.T_CS_STATION_DETAILS,
                Image = R.CS_Station
            });

            natives.Add(new CoordinateSystem()
            {
                _Name = "BEAM",
                AxisNames = new List<string>() { "Rbeam", "Lbeam", "Hbeam" },
                TransformatonMatrix = null,
                LongName = R.T_CS_BEAM,
                Description = R.T_CS_BEAM_DETAILS,
                Image = R.CS_BEAM
            });

            natives.Add(new CoordinateSystem()
            {
                _Name = "BEAMV",
                AxisNames = new List<string>() { "Rbv", "Lbv", "Hbv" },
                TransformatonMatrix = null,
                LongName = R.T_CS_BEAMV,
                Description = R.T_CS_BEAMV_DETAILS,
                Image = R.CS_BEAMV
            });

            natives.Add(new CoordinateSystem()
            {
                _Name = "USERDEFINED",
                AxisNames = new List<string>() { "Xusr", "Yusr", "Zusr" },
                TransformatonMatrix = null,
                LongName = "User Defined",
                Description = R.String_Unknown
            }); 

            natives.Add(new CoordinateSystem()
            {
                _Name = "TEMPCALCULATION",
                AxisNames = new List<string>() { "Xtmp", "Ytmp", "Ztmp" },
                TransformatonMatrix = null,
                LongName = "Temporary computation",
                Description = R.String_Unknown
            }); 

            return natives;
        }
    }
}