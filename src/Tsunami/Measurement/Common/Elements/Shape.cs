﻿using System.Collections.Generic;
using System.Xml.Serialization;
using TSU;

namespace TSU.Common.Elements
{
    [XmlInclude(typeof(Line3D))]
    [XmlInclude(typeof(Plane))]
    [XmlInclude(typeof(Circle))]
    [XmlInclude(typeof(Sphere))]
    [XmlInclude(typeof(Cylinder))]

    public class ParametricShape : PrimitiveElement
    {
        public Coordinates Vector { get; set; }
        public DoubleValue parameter3 { get; set; }

        public override List<Point> GetPoints()
        {
            return base.GetPoints();
        }
    }
    public class Line3D : ParametricShape
    {
        // represented by an ORIGIN this.coordinates, a VECTOR this.Vector 
        
        /// <summary>
        /// Expressed in Grads
        /// </summary>
        public DoubleValue Bearing { get; set; }

        /// <summary>
        /// Expressed in rad
        /// </summary>
        public DoubleValue Slope { get; set; }

        public Line3D()
        {
            this.fileElementType = ENUM.ElementType.Line;
        }
        public Line3D(Coordinates origin, Coordinates vector):this()
        {
            this._Coordinates.UserDefined = origin;
            this.Vector = vector;
        }
    
        public override List<Point> GetPoints()
        {
            Point p1 = new Point() { _Name = ("Cylinder axis 1 (" + this.Elements[0]._Name) + ")", _Coordinates = this._Coordinates };
            Point p2 = new Point() { _Name = ("Cylinder axis 2 (" + this.Elements[0]._Name) + ")", _Coordinates = this._Coordinates + new CoordinatesInAllSystems() { Su = this.Vector } };
            return new List<Point>() { p1, p2 };
        }
    }
    public class Plane : ParametricShape
    {
        // ax+by+c+d = 0, represented by a normal VECTOR(a,b,c) this.Vector and a independant term this.d 
        public DoubleValue d
        {
            get { return this.parameter3; }
            set { this.parameter3 = value; }
        }
        public Plane()
        {
            this.fileElementType = ENUM.ElementType.Plane;
        }
        public Plane(Coordinates vector, DoubleValue d):this()
        {
            this.Vector = vector;
            this.d = d;
        }

        public override List<Point> GetPoints()
        {
            return base.GetPoints();
        }
    }
    public class Circle : ParametricShape
    {
        // represented by an ORIGIN this.coordinates, a VECTOR this.Vector and a RADIUS
        public DoubleValue Radius
        {
            get { return this.parameter3; }
            set {  this.parameter3 = value; }
        }
        public Circle()
        {
            this.fileElementType = ENUM.ElementType.Circle;
        }
        public Circle(Coordinates origin, Coordinates vector, DoubleValue radius):this()
        {
            this._Coordinates.Local = origin;
            this.Vector = vector;
            this.Radius = radius;
        }

        public override List<Point> GetPoints()
        {
            Point center = new Point() { _Name="Circle Center ("+ this.Elements[0]._Name.Replace('.','_')+")", _Coordinates = this._Coordinates};
            return new List<Point>() { center };
        }
    }
    public class Sphere : ParametricShape
    {
        // represented by an ORIGIN this.coordinates, and a radius
        public DoubleValue Radius
        {
            get { return this.parameter3; }
            set { this.parameter3 = value; }
        }
        public Sphere()
        {
            this.fileElementType = ENUM.ElementType.Sphere;
        }
        public Sphere(Coordinates origin, DoubleValue radius):this()
        {
            this._Coordinates.Local = origin;
            this.Radius = radius;
        }

        public override List<Point> GetPoints()
        {
            Point center = new Point() { _Name = ("Sphere Center (" + this.Elements[0]._Name) + ")", _Coordinates = this._Coordinates };
            return new List<Point>() { center };
        }
    }
    public class Cylinder : ParametricShape
    {
        // represented by an ORIGIN this.coordinates, a VECTOR this.Vector and a RADIUS
        public DoubleValue Radius
        {
            get { return this.parameter3; }
            set {  this.parameter3 = value; }
        }

        public Cylinder()
        {
            this.fileElementType = ENUM.ElementType.Cylinder;
        }
        public Cylinder(Coordinates origin, Coordinates vector, DoubleValue radius):this()
        {
            this._Coordinates.Local = origin;
            this.Vector = vector;
            this.Radius = radius;
        }

        public override List<Point> GetPoints()
        {
            Point center1 = new Point() { _Name = ("Cylinder axis 1 (" + this.Elements[0]._Name) + ")", _Coordinates = this._Coordinates };
            Point center2 = new Point() { _Name = ("Cylinder axis 2 (" + this.Elements[0]._Name) + ")", _Coordinates = this._Coordinates + new CoordinatesInAllSystems() { Su = this.Vector } };
            return new List<Point>() { center1, center2 };
        }
    }
        
}
