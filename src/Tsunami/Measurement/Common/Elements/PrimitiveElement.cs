﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using TSU;

namespace TSU.Common.Elements
{
    [XmlInclude(typeof(Point))]
    [XmlInclude(typeof(ParametricShape))]
    [Serializable()]
    public class PrimitiveElement : Element, ICloneable
    {
        public virtual CoordinatesInAllSystems _Coordinates { get; set; }

        [XmlAttribute]
        public virtual DateTime Date { get; set; }

        public PrimitiveElement()
        {
            _Coordinates = new CoordinatesInAllSystems();
            Date = new DateTime();
        }
        public PrimitiveElement(string name)
            : base(name) 
        {
            _Coordinates = new CoordinatesInAllSystems();
            Date = new DateTime();
        }

        public override IElement GetElementByName(string key)
        {
            if (this._Name == key)
                return this;
            else
                return null;
        }

        public override IEnumerator GetEnumerator()
        {
            return null;
        }

        public override IElement GetChild()
        {
            return null;
        }

        public override List<Point> GetPoints()
        {
            List<Point> l = new List<Point>(); ;
            if (this is Point)
            {
                l.Add(this as Point);
            }
            return l;
        }

        public override List<Composites.Magnet> GetMagnets()
        {
            List<Composites.Magnet> l = new List<Composites.Magnet>(); 
            return l;
        }

        public override List<Point> GetAlesages()
        {
            List<Point> l = new List<Point>(); ;
            if (this.fileElementType == ENUM.ElementType.Alesage)
            {
                l.Add(this as Point);
            }
            return l;
        }

        public override List<Point> GetPiliers()
        {
            List<Point> l = new List<Point>(); ;
            if (this.fileElementType == ENUM.ElementType.Pilier)
            {
                l.Add(this as Point);
            }
            return l;
        }

        public override List<Point> GetPointsATracer()
        {
            List<Point> l = new List<Point>(); ;
            if (this.fileElementType == ENUM.ElementType.PointATracer)
            {
                l.Add(this as Point);
            }
            return l;
        }

        public override object Clone()
        {
            PrimitiveElement newAbstractElement = (PrimitiveElement)this.MemberwiseClone(); // to make a clone of the primitive types fields
            newAbstractElement.Elements = (this.Elements != null) ? (CloneableList<Element>)this.Elements.Clone() : null;
            newAbstractElement._Coordinates = (this._Coordinates != null) ? this._Coordinates.Clone() as CoordinatesInAllSystems:null;
            //newAbstractElement._Coordinates = this._Coordinates.
            return newAbstractElement;
        }
    }
}
