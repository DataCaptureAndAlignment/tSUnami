﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;
using System.ServiceModel;
using System.Windows.Forms;
using TSU.Preferences;
using TSU.Views;
using TSU.Views.Message;

namespace TSU.Common.Elements

{
    [Serializable]
    public class SocketCode: TSU.TsuObject
    {
        [Serializable]
        public class PairForDetection
        {
            [XmlAttribute("keyword")]
            public string KeyWord { get; set; }
            [XmlAttribute("number")]
            public int SocketCodeNumber { get; set; }

            public override string ToString()
            {
                return $"Detection of '{KeyWord}' will trigger code {SocketCodeNumber}";
            }
        }
        public enum DefaultValuesTypes
        {
            PolarInstrumentHeight,
            PolarTargetHeight,
            LevellingTargetHeight,
        }
          

        public static SocketCode GetCodeById(string id)
        {
            try
            {
                return TSU.Tsunami2.Preferences.Values.SocketCodes.Find(x => x.Id == id);
            }
            catch (Exception)
            {

                return null;
            }

        }


        public static SocketCode GetById(string id)
        {
            return TSU.Tsunami2.Preferences.Values.SocketCodes.Find(x => x.Id == id.Trim());
        }

        // base._Id for the string code value "14"
        // base._Name for the string code "Code 14"
        [XmlIgnore]
        public string NameForIH
        {
            get
            {
                return $"{_Name,7}=>{InstrumentHeightForPolarMeasurement * 1000,7:F2}mm: {Description}";
            }
        }
        [XmlIgnore]
        public string NameForPTH
        {
            get
            {
                return $"{_Name,-7}=>{DefaultExtensionForPolarMeasurement*1000,7:F2}mm: {Description}";
            }
        }
        [XmlIgnore]
        public string NameForLTH
        {
            get
            {
                return $"{_Name,-7}=>{DefaultExtensionForLevelingMeasurement * 1000,7:F2}mm: {Description}";
            }
            
        }

        [XmlIgnore]
        public string CodeAndDescription
        {
            get
            {
                return $"{_Name}: {Description}.";
            }
        }

        [XmlAttribute]
        public string Description { get; set; }
        [XmlAttribute("DE4PolarM")]
        public double DefaultExtensionForPolarMeasurement { get; set; }
        [XmlAttribute("IH4PolarM")]
        public double InstrumentHeightForPolarMeasurement { get; set; }
        [XmlAttribute("DE4LevelM")]
        public double DefaultExtensionForLevelingMeasurement { get; set; }
        
        public SocketCode()
        {

        }

        public override string ToString()
        {
            return 
                string.Format("{0}: {1} (Pext={2}m , Pih={3}m, Lext={4}m).",
                _Name, 
                Description,  
                DefaultExtensionForPolarMeasurement.ToString(), 
                InstrumentHeightForPolarMeasurement.ToString(), 
                DefaultExtensionForLevelingMeasurement.ToString());
        
        }

        public override object Clone()
        {
            SocketCode n = (SocketCode)this.MemberwiseClone(); // to make a clone of the primitive types fields
            
            return n;
        }
        /// <summary>
        /// gathering possible codes
        /// </summary>
        /// <returns></returns>
        internal static List<SocketCode> GetCodesPossibleForAPolarStationHeight()
        {
            List<SocketCode> codes = new List<SocketCode>();
            codes.AddRange(TSU.Tsunami2.Preferences.Values.SocketCodes.FindAll(x => x.Id == "0" ||x.InstrumentHeightForPolarMeasurement != 0 || x.Id == "10"));
            return codes;
        }

        internal static bool Ask(Views.TsuView view, List<SocketCode> possibleCodes, SocketCode preselectedCode, 
            string displayMember, out SocketCode receivedCode, out double valueManuallyEncoded, string customMessage = "", bool bProposedFromName=false)
        {
            valueManuallyEncoded = -1;
            receivedCode = null;

            // show message
            string message = customMessage == ""? R.T_SELECT_SOCKET_CODE: customMessage;

            MessageInput mi = new MessageInput(bProposedFromName ? MessageType.GoodNews: MessageType.Choice, message)
            {
                ButtonTexts = new List<string>() { R.T_OK, R.T_CANCEL },
                Controls = CreationHelper.GetSocketCodeComboBox(possibleCodes, preselectedCode, displayMember, null)
            };
            using (var results = mi.Show())
            {
                if (results.TextOfButtonClicked == R.T_CANCEL)
                    return false;

                ComboBox comboReturned = results.ReturnedControls[0] as ComboBox;
                receivedCode = comboReturned.SelectedItem as SocketCode;
                if (receivedCode == null) // seems teh use enter value by hand
                {
                    if (int.TryParse(comboReturned.Text, out int codeNumber)) // looks like a code number, lets check
                    {
                        receivedCode = possibleCodes.Find(x => x.Id == codeNumber.ToString());
                    }
                    else
                    {
                        if (!double.TryParse(comboReturned.Text, out valueManuallyEncoded))
                            throw new Exception("Wrong value!");
                    }
                }
            }


            return true;
        }

        internal static bool GetSmartCodeFromName(string name, out SocketCode proposedCode, out string message)
        {
            proposedCode = null;

            string codeId = "";
            string codeKeyFound = "";

            foreach (var item in Tsunami2.Preferences.Values.SocketCodesPairedWithKeyWords)
            {
                if (name.ToUpper().Contains(item.KeyWord.ToUpper()))
                {
                    codeId = item.SocketCodeNumber.ToString();
                    codeKeyFound = item.KeyWord.ToUpper();
                    break;
                }
            }
            if (codeId == "")
            {
                message = "Couldn't help to find a code.";
                return false;
            }
            proposedCode = SocketCode.GetCodeById(codeId);

            message = $"{R.T_SPECIAL_CODE_PROPOSED};" +
                      $"{R.T_CODE} {codeId} {R.T_IS_PROPOSED_BECAUSE} '{codeKeyFound}' {R.T_HAS_BEEN_DETECTED}";

            return true;
        }
    }
}
