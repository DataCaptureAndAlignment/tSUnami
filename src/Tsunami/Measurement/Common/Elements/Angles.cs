using System;
using System.Collections.Generic;
using System.Text;
using R = TSU.Properties.Resources;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using TSU;
using TSU.ENUM;
using TSU.Common.Compute;

namespace TSU.Common.Elements
{
    public interface IAngles : ICloneable, IEquatable<Angles>
    {
        DoubleValue Horizontal { get; set; }
        DoubleValue Vertical { get; set; }
        bool isFace2 { get; }
    }

    [Serializable]
    public class Angles : IAngles
    {
        [XmlIgnore]
        public bool isFace2
        {
            get
            {
                return (Survey.Modulo400(this.Vertical.Value) > 200);
            }
        }


        public virtual DoubleValue Horizontal { get; set; }

        public virtual DoubleValue Vertical { get; set; }

        [XmlIgnore]
        public bool IsNa
        {
            get
            {
                bool hIsNa = Horizontal.Value == TSU.DoubleValue.Na;
                bool vIsNa = Vertical.Value == TSU.DoubleValue.Na;
                return hIsNa || vIsNa;
            }
        }

        public Angles() //Constructeur
        {
            this.Horizontal = new DoubleValue();
            this.Vertical = new DoubleValue();
        }

        public virtual object Clone()
        {
            Angles newAngles = (Angles)this.MemberwiseClone();
            newAngles.Horizontal = (DoubleValue)this.Horizontal.Clone();

            newAngles.Vertical = (DoubleValue)this.Vertical.Clone();
            return newAngles;
        }

        public bool Equals(Angles other)
        {
            if (other == null)
                return false;

            if (this.Horizontal == other.Horizontal)
                return true;
            else
                return false;
        }

        public override int GetHashCode()
        {
            return this.Horizontal.GetHashCode() + this.Vertical.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("AH {0}({1}), AV {2}({3})gon", this.Horizontal.Value, this.Horizontal.Sigma, this.Vertical.Value, this.Vertical.Sigma);
        }
    }
}
