﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using TSU.Common.Compute.Transformation;
using R = TSU.Properties.Resources;
using TSU.Common.Compute;
using E = TSU.Common.Elements;
using System.Activities.Statements;
using TSU.Views.Message;
using System.Activities.Expressions;

namespace TSU.Common.Elements
{
    [Serializable]
    public class CoordinatesInAllSystems
    {
        public List<Coordinates> ListOfAllCoordinates { get; set; } //= new TsuDictionary<CoordinateSystem, Coordinates>();

        #region Local

        [XmlIgnore]
        public virtual TSU.Common.Elements.Coordinates Local
        {
            get
            {
                if (this.Su.AreKnown) return this.Su;
                if (this.Mla != null) if (this.Mla.AreKnown) return this.Mla; //
                if (this.UserDefined.AreKnown) return this.UserDefined;
                if (this.CoordinatesExistInAGivenSystem("CCS-Z"))
                    return this.GetCoordinatesInASystemByName("CCS-Z");
                var c = new Coordinates();
                c.Set();
                return c;
            }
            set
            {
                this.Su = value;
            }
        }

        [XmlIgnore]
        public bool HasLocal
        {
            get
            {
                return HasSomethingAvailableIn(this.Local);
            }
        }

        #endregion

        public Coordinates GetExistingOrCreateOne(string systemName, Coordinates.CoordinateSystemsTypes type)
        {
            if (ListOfAllCoordinates != null)
            {
                Coordinates c = GetCoordinatesInASystemByName(systemName);
                if (c == null)
                {
                    c = new TSU.Common.Elements.Coordinates() { CoordinateSystem = type, SystemName = systemName };
                    AddOrReplaceCoordinatesInOneSystem(systemName, c);
                }
                if (c.X == null || c.Y == null || c.Z == null)
                    c.Set();

                return c;
            }
            return null;
        }
        public bool AddOrReplaceCoordinatesInOneSystem(string systemName, Coordinates coordinates)
        {
            return CoordinatesInAllSystems.AddOrReplaceCoordinatesInOneSystemOfAGivenList(systemName, coordinates, ListOfAllCoordinates);
        }

        #region Ccs

        private TSU.Common.Elements.Coordinates ccs { get; set; }

        [XmlIgnore]
        public virtual TSU.Common.Elements.Coordinates Ccs
        {
            get
            {
                return GetExistingOrCreateOne("CCS-H", Coordinates.CoordinateSystemsTypes._2DPlusH);
            }
            set
            {
                AddOrReplaceCoordinatesInOneSystem("CCS-H", value);
                // SetOrCreateandSet("CCS-Z", value);
            }
        }



        [XmlIgnore]
        public bool HasCcs
        {
            get
            {
                return HasSomethingAvailableIn(this.Ccs);
                if (this.Ccs != null)
                    if (this.Ccs.X.Value != TSU.Tsunami2.Preferences.Values.na)
                        if (this.Ccs.X.Value != -9999)
                            return true;

                return false;
            }
        }

        #endregion

        #region Physicist

        private TSU.Common.Elements.Coordinates physicist { get; set; }

        [XmlIgnore]
        public virtual TSU.Common.Elements.Coordinates Physicist
        {
            get
            {
                return GetExistingOrCreateOne("PHYSICIST", Coordinates.CoordinateSystemsTypes._3DCartesian);
            }
            set
            {
                AddOrReplaceCoordinatesInOneSystem("PHYSICIST", value);
            }
        }

        [XmlIgnore]
        public bool HasPhysicist
        {
            get
            {
                return HasSomethingAvailableIn(this.Physicist);
            }
        }

        #endregion

        #region BeamV
        private TSU.Common.Elements.Coordinates beamV { get; set; }

        [XmlIgnore]
        public virtual TSU.Common.Elements.Coordinates BeamV
        {
            get
            {
                return GetExistingOrCreateOne("BEAMV", Coordinates.CoordinateSystemsTypes._3DCartesian);
            }
            set
            {
                AddOrReplaceCoordinatesInOneSystem("BEAMV", value);
            }
        }

        [XmlIgnore]
        public bool HasBeamV
        {
            get
            {
                return HasSomethingAvailableIn(this.BeamV);
            }
        }

        private TSU.Common.Elements.Coordinates beam { get; set; }

        [XmlIgnore]
        public virtual TSU.Common.Elements.Coordinates Beam
        {
            get
            {
                return GetExistingOrCreateOne("BEAM", Coordinates.CoordinateSystemsTypes._3DCartesian);
            }
            set
            {
                AddOrReplaceCoordinatesInOneSystem("BEAM", value);
            }
        }

        [XmlIgnore]
        public bool HasBeam
        {
            get
            {
                return HasSomethingAvailableIn(this.Beam);
            }
        }

        private bool HasSomethingAvailableIn(Coordinates coordinates)
        {
            return coordinates != null && coordinates.X != null && coordinates.X.Value != TSU.Tsunami2.Preferences.Values.na && coordinates.X.Value != -9999;
        }


        #endregion

        #region StationCS


        [XmlIgnore]
        public virtual TSU.Common.Elements.Coordinates StationCs
        {
            get
            {
                return GetExistingOrCreateOne("TOSTATION", Coordinates.CoordinateSystemsTypes._3DCartesian);
            }
            set
            {
                AddOrReplaceCoordinatesInOneSystem("TOSTATION", value);
            }
        }
        [XmlIgnore]
        public bool HasStationCs
        {
            get
            {
                return HasSomethingAvailableIn(this.StationCs);
            }
        }

        #endregion

        #region Su

        [XmlIgnore]
        public virtual TSU.Common.Elements.Coordinates Su
        {
            get
            {
                return GetExistingOrCreateOne("SU", Coordinates.CoordinateSystemsTypes._3DCartesian);
            }
            set
            {
                AddOrReplaceCoordinatesInOneSystem("SU", value);
            }
        }
        [XmlIgnore]
        public bool HasSu
        {
            get
            {
                return HasSomethingAvailableIn(this.Su);
            }
        }

        #endregion

        #region MLA

        [XmlIgnore]
        public virtual TSU.Common.Elements.Coordinates Mla
        {
            get
            {
                return GetExistingOrCreateOne("MLA", Coordinates.CoordinateSystemsTypes._3DCartesian);
            }
            set
            {
                AddOrReplaceCoordinatesInOneSystem("MLA", value);
            }
        }

        [XmlIgnore]
        public bool HasMla
        {
            get
            {
                return HasSomethingAvailableIn(this.Mla);
            }
        }

        #endregion

        #region UserDefined


        [XmlIgnore]
        public virtual TSU.Common.Elements.Coordinates UserDefined
        {
            get
            {
                return GetExistingOrCreateOne("USERDEFINED", Coordinates.CoordinateSystemsTypes._3DCartesian);
            }
            set
            {
                AddOrReplaceCoordinatesInOneSystem("USERDEFINED", value);
            }
        }

        [XmlIgnore]
        public bool HasUserDefined
        {
            get
            {
                return HasSomethingAvailableIn(this.UserDefined);
            }
        }

        #endregion

        #region TempCalculation

        private TSU.Common.Elements.Coordinates tempCalculation { get; set; }

        [XmlIgnore]
        public virtual TSU.Common.Elements.Coordinates TempCalculation
        {
            get
            {
                return GetExistingOrCreateOne("TEMPCALCULATION", Coordinates.CoordinateSystemsTypes.Unknown);
            }
            set
            {
                AddOrReplaceCoordinatesInOneSystem("TEMPCALCULATION", value);
            }
        }
        #endregion

        [XmlIgnore]
        public bool HasAny
        {
            get
            {
                return (HasLocal || HasMla || HasSu || HasCcs);
            }
        }

        [XmlAttribute("Approx")]
        public bool AreApproximate = false;

        public CoordinatesInAllSystems()
        {
            this.ListOfAllCoordinates = new List<Coordinates> { };
        }

        public object Clone()
        {
            CoordinatesInAllSystems clone = this.MemberwiseClone() as CoordinatesInAllSystems;

            clone.ListOfAllCoordinates = new List<Coordinates> { };
            foreach (var item in this.ListOfAllCoordinates)
            {
                clone.ListOfAllCoordinates.Add(item.Clone() as Coordinates);
            }
            return clone;
        }

        public static CoordinatesInAllSystems operator +(CoordinatesInAllSystems b, CoordinatesInAllSystems c)
        {
            CoordinatesInAllSystems a = new CoordinatesInAllSystems();
            a.Beam = b.Beam + c.Beam;
            a.BeamV = b.BeamV + c.BeamV;
            a.Ccs = b.Ccs + c.Ccs;
            a.Mla = b.Mla + c.Mla;
            a.Physicist = b.Physicist + c.Physicist;
            a.StationCs = b.StationCs + c.StationCs;
            a.Su = b.Su + c.Su;
            a.TempCalculation = b.TempCalculation + c.TempCalculation;
            a.UserDefined = b.UserDefined + c.UserDefined;
            return a;
        }




        /// <summary>
        /// will try to fill the missing coordinates in a ACC or EXP style need : // EXP style, no need of stationCS and BeamCS, // ACC style we need to step up a temorary zone for MLA and SU tobe Station2PointCS and PHYS tobe BeamCS
        /// </summary>
        /// <param name="measuredPoint"></param>
        public static void FillMissingCoordinates(E.Point stationPoint, E.Point measuredPoint, E.Point theoPoint = null, bool silente = false, Zones.Zone zone = null, TSU.Views.TsuView view = null)
        {
            double na = TSU.Tsunami2.Preferences.Values.na;
            try
            {
                E.Coordinates.ReferenceFrames rF = Tsunami2.Properties.ReferenceFrameOfTheMeasurement;

                if (zone != null)
                    E.Manager.Module.TryToFillMissingCoordinates(measuredPoint, rF, zone);


                if (measuredPoint._Coordinates.HasCcs || !measuredPoint._Coordinates.HasLocal)
                {
                    if (theoPoint != null && theoPoint._Coordinates != null)
                    {
                        if (!theoPoint._Coordinates.HasCcs)
                        {
                            Systems.SuOrCcs2Customs(measuredPoint);
                            return;
                        }
                        Systems.HtoZ(theoPoint, E.Coordinates.GetReferenceSurface(rF));

                        if (stationPoint != null)
                            Survey.ComputeCoordinateInStationCSFromCCS(stationPoint, theoPoint, measuredPoint, rF);

                        //TODO check if 'na' is enough or if we need '0' that will make problem if the accelerator has a bearing of exactly '0'
                        if (theoPoint._Parameters.hasGisement && theoPoint != null)
                            Survey.ComputeCoordinateInBeamCS(theoPoint, measuredPoint, rF);
                    }
                }
                else
                {
                    if (theoPoint != null && theoPoint._Parameters.hasGisement &&
                        measuredPoint._Parameters.hasGisement) // probably a implentation along a line
                        Survey.ComputeCoordinateInBeamCS(theoPoint, measuredPoint, rF);
                    else
                        if (stationPoint != null && theoPoint != null && theoPoint._Coordinates.HasAny)
                        Survey.ComputeCoordinateInStationCSFromSU(stationPoint, theoPoint, measuredPoint, rF);
                }

                // compute coordinates from custom CS
                Systems.SuOrCcs2Customs(measuredPoint);
            }
            catch (Exception ex)
            {
                if (!silente)
                {
                    string s = $"{R.T_IMPOSSIBLE_TO_FILL_THE_OTHER_COORDINATE_SYSTEMS};{ex}";
                    new MessageInput(MessageType.Critical, s).Show();
                }
            }
        }

        /// <summary>
        /// fait une copie mémoire du système de coordonnée
        /// </summary>
        /// <returns></returns>
        public CoordinatesInAllSystems DeepCopy()
        // fait une copie mémoire de la coordonnée
        {
            using (MemoryStream stream = new MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                CoordinatesInAllSystems p = (CoordinatesInAllSystems)formatter.Deserialize(stream);
                return p;
            }
        }

        #region ToString

        public override string ToString()
        {
            return ListToString(showInMm: false);
        }

        private string ListToString(bool showInMm = false, bool displayOnlySelectedSystem = false)
        {
            string s = "";
            foreach (var item in this.ListOfAllCoordinates)
            {
                bool isPreferedSystem = item.SystemName == Tsunami2.Properties.CoordinatesSystems.SelectedType._Name;
                if (item.AreKnown && (!displayOnlySelectedSystem || isPreferedSystem))
                {
                    string coordinates = showInMm ? item.ToString("mm") : item.ToString();
                    if (displayOnlySelectedSystem)
                        s += $"{coordinates}";
                    else
                        s += string.Format("\r\n{0,12}", $"{item.SystemName}:") + string.Format("{0,12}", coordinates);
                }
            }
            //if (Ccs.AreKnown) s += string.Format("\r\n{0,12}","CCS:") + string.Format("{0,12}",Ccs.ToString());
            //if (Mla.AreKnown) s += string.Format("\r\n{0,12}", "MLA:") + string.Format("{0,12}", Mla.ToString()); 
            //if (Su.AreKnown) s += string.Format("\r\n{0,12}", "SU:") + string.Format("{0,12}", Su.ToString()); 
            //if (Physicist.AreKnown) s += string.Format("\r\n{0,12}", "PHYS:") + string.Format("{0,-12}", Physicist.ToString()); 
            //if (Local.AreKnown) s += string.Format("\r\n{0,12}", "LOCAL:") + string.Format("{0,12}", Local.ToString()); 
            return s;
        }

        public string ToString(bool showInMm = false, bool displayOnlySelectedSystem = false)
        {
            return ListToString(showInMm, displayOnlySelectedSystem);
        }

        public string ToString(string param)
        {
            string s = "";
            if (param == "")
            {
                if (Ccs.AreKnown) s += "CCS ";// string.Format("\r\n{0,12}","CCS:") + string.Format("{0,12}",Ccs.ToString());
                if (Mla.AreKnown) s += "MLA ";//  string.Format("\r\n{0,12}", "MLA:") + string.Format("{0,12}", Mla.ToString()); 
                if (Su.AreKnown) s += "SU ";//  string.Format("\r\n{0,12}", "SU:") + string.Format("{0,12}", Su.ToString()); 
                if (Physicist.AreKnown) s += "PHY ";//  string.Format("\r\n{0,12}", "PHYS:") + string.Format("{0,-12}", Physicist.ToString()); 
                if (Local.AreKnown) s += "LOCAL ";//  string.Format("\r\n{0,12}", "LOCAL:") + string.Format("{0,12}", Local.ToString()); 
            }
            if (param == "ALL")
            {
                foreach (var item in this.ListOfAllCoordinates)
                {
                    s += $"\r\n{item.ToString()}";
                }
            }
            return s;
        }

        public enum StringFormat
        {
            ToStationCoordinates,
            BeamCoordinates,
            CcsCoordinates,
            DisplacementLRV,
            PhysCoordinates,
            SuCoordinates,
            BeamStnLrv,
            AvailableOffsetsAsTable,
            AvailableCoordinatesAsTable
        }

        public string ToString(StringFormat format)
        {
            switch (format)
            {
                case StringFormat.ToStationCoordinates: return string.Format("{0:7} : {1}", "2St CS", this.StationCs.ToString());
                case StringFormat.BeamCoordinates: return string.Format("{0:7} : {1}", "BeamCS", this.Physicist.ToString());
                case StringFormat.CcsCoordinates: return string.Format("{0:7} : {1}", "CernCS", this.Ccs.ToString());
                case StringFormat.PhysCoordinates: return string.Format("{0:7} : {1}", "CernCS", this.Physicist.ToString());
                case StringFormat.SuCoordinates: return string.Format("{0:7} : {1}", "Su CS", this.Su.ToString());
                case StringFormat.DisplacementLRV:
                    Coordinates c = new Coordinates("LRV", Physicist.Y.Value, Physicist.X.Value, StationCs.Z.Value);
                    return string.Format("{0:7} : {1}", "DspLRV", c.ToString());
                case StringFormat.AvailableOffsetsAsTable: return GetAvailableOffsetsAsStringTable();
                case StringFormat.AvailableCoordinatesAsTable: return GetAvailableCoordinatesAsStringTable();
                default: return "Wrong format";
            }
        }

        internal static Coordinates GetOrCreateByName(string systemName, List<Coordinates> list)
        {
            Coordinates c = list.Find(x => x.SystemName.ToUpper() == systemName.ToUpper());
            if (c == null)
            {
                c = new Coordinates() { SystemName = systemName };
                list.Add(c);
            }
            return c;
        }

        internal Coordinates GetOrCreateByName(string systemName)
        {
            return GetOrCreateByName(systemName, this.ListOfAllCoordinates);
        }


        internal static bool CoordinatesExistInAGivenSystem(string systemName, List<Coordinates> list, out int index)
        {
            index = 0;
            foreach (var item in list)
            {
                if (item.SystemName.ToUpper() == systemName.ToUpper())
                    return true;
                index++;
            }
            return false;
        }

        /// <summary>
        /// get system like CCS-H CCS-Z PHYSICIST, SU, MLA, USERDEFINED, TEMPORARY, BEAM, BEAMV
        /// </summary>
        /// <param name="systemName"></param>
        /// <returns></returns>
        internal bool CoordinatesExistInAGivenSystem(string systemName)
        {
            return CoordinatesExistInAGivenSystem(systemName, this.ListOfAllCoordinates, out _);
        }

        /// <summary>
        /// get system like CCS-H CCS-Z PHYSICIST, SU, MLA, USERDEFINED, TEMPORARY, BEAM, BEAMV
        /// </summary>
        /// <param name="systemName"></param>
        /// <returns></returns>
        internal Coordinates GetCoordinatesInASystemByName(string systemName)
        {
            return GetCoordinatesInASystemByName(systemName, this.ListOfAllCoordinates);
        }

        /// <summary>
        /// get system like CCS-H CCS-Z PHYSICIST, SU, MLA, USERDEFINED, TEMPORARY, BEAM, BEAMV
        /// </summary>
        /// <param name="systemName"></param>
        /// <returns></returns>
        internal static Coordinates GetCoordinatesInASystemByName(string systemName, List<Coordinates> list)
        {
            Coordinates c = list.Find(x => x.SystemName.ToUpper() == systemName.ToUpper());
            return c;
        }

        /// <summary>
        /// will replace or create the entry in the list
        /// </summary>
        /// <param name="cdName"></param>
        /// <param name="coordinates"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        internal static bool AddOrReplaceCoordinatesInOneSystemOfAGivenList(string systemName, Coordinates coordinates, List<Coordinates> list)
        {
            coordinates.SystemName = systemName;
            if (CoordinatesExistInAGivenSystem(systemName, list, out int index))
            {
                list.RemoveAt(index);
                list.Add(coordinates);
                return false;
            }
            else
            {
                list.Add(coordinates);
                return true;
            }
        }

        private string GetAvailableOffsetsAsStringTable(bool Colored = true)
        {

            string s = "";
            if (HasAny)
            {
                s = $"{R.T_CST_OFFSETS} (M-T):\r\n";
                s += $"{"",-11}{"X[mm]",11}{"Y[mm]",11}{"Z[mm]",11}\r\n";
                foreach (Coordinates c in this.ListOfAllCoordinates)
                {
                    if (c.AreKnown)
                    {
                        s += $"{c.SystemName,-11}{c.X.ToString("m-mm"),11}{c.Y.ToString("m-mm"),11}{c.Z.ToString("m-mm"),11}\r\n";
                    }
                }
            }
            return s;
        }

        private string GetAvailableCoordinatesAsStringTable(bool Colored = true)
        {
            string header = "";
            header = $"{R.T_CST_COORDINATES}:\r\n";
            header += $"{"",-11}{"X[m]",11}{"Y[m]",11}{"Z[m]",11}\r\n";
            string coord = "";
            foreach (Coordinates c in this.ListOfAllCoordinates)
            {
                if (c.AreKnown)
                {
                    coord += $"{c.SystemName,-11}{c.X.ToString("m-m"),11}{c.Y.ToString("m-m"),11}{c.Z.ToString("m-m"),11}\r\n";
                }
            }
            if (coord == "")
                return "";
            return header + coord;
        }

        #endregion

        internal Coordinates GetBasedOnType(string coordinateType)
        {
            switch (coordinateType)
            {
                case "CCS": return this.Ccs;
                case "PHYSICIST": return this.Physicist;
                case "SU": return this.Su;
                case "MLA": return this.Mla;
                case "UserDefined": return this.UserDefined;
                default: return this.Local;
            }
        }

        /// <summary>
        /// will return NFC or PHYS or SU or CCS-H or CCS-Z in previous do not exists, and out its name or 'None'
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        internal Coordinates GetMostLocal(out string systemRetreived)
        {
            Coordinates c;
            systemRetreived = "PHYSICIST"; c = this.GetCoordinatesInASystemByName(systemRetreived); if (c != null) { return c; }
            systemRetreived = "SU"; c = this.GetCoordinatesInASystemByName(systemRetreived); if (c != null) { return c; }
            systemRetreived = "MLA"; c = this.GetCoordinatesInASystemByName(systemRetreived); if (c != null) { return c; }
            systemRetreived = "CCS-H"; c = this.GetCoordinatesInASystemByName(systemRetreived); if (c != null) { return c; }
            systemRetreived = "CCS-Z"; c = this.GetCoordinatesInASystemByName(systemRetreived); if (c != null) { return c; }
            systemRetreived = "None";
            return null;
        }

        public static string GetSystemName(Coordinates.CoordinateSystemsTsunamiTypes type)
        {
            switch (type)
            {
                case Coordinates.CoordinateSystemsTsunamiTypes.CCS: return "CCS-H";
                case Coordinates.CoordinateSystemsTsunamiTypes.PHYS: return "PHYSICIST";
                case Coordinates.CoordinateSystemsTsunamiTypes.SU: return "SU";
                case Coordinates.CoordinateSystemsTsunamiTypes.MLA: return "MLA";
                case Coordinates.CoordinateSystemsTsunamiTypes.UserDefined: return "USERDEFINED";
                case Coordinates.CoordinateSystemsTsunamiTypes.ToStation: return "TOSTATION";
                case Coordinates.CoordinateSystemsTsunamiTypes.Beam: return "BEAM";
                case Coordinates.CoordinateSystemsTsunamiTypes.BeamV: return "BEAMV";
                case Coordinates.CoordinateSystemsTsunamiTypes.CCS_Z: return "CCS-Z";
                case Coordinates.CoordinateSystemsTsunamiTypes.Displacement:
                case Coordinates.CoordinateSystemsTsunamiTypes.Unknown:
                default:
                    return "UNKNOWN";
            }
        }

        /// <summary>
        /// will return PHYS or SU or CCS-H, SU, Phys in previous do not exists, and out its name or 'None'
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        internal Coordinates GetLessLocal(out string systemRetreived)
        {
            Coordinates c;
            systemRetreived = "CCS-H"; c = this.GetCoordinatesInASystemByName(systemRetreived); if (c != null) { return c; }
            systemRetreived = "SU"; c = this.GetCoordinatesInASystemByName(systemRetreived); if (c != null) { return c; }
            systemRetreived = "PHYSICIST"; c = this.GetCoordinatesInASystemByName(systemRetreived); if (c != null) { return c; }
            systemRetreived = "None";
            return new Coordinates();
        }

        internal static CoordinatesInAllSystems ComputeDifferences(CoordinatesInAllSystems measuredCoordinates, CoordinatesInAllSystems theoreticalCoordinates)
        {
            // to store the difference between measured and theo
            E.CoordinatesInAllSystems offsets = new CoordinatesInAllSystems();
            if (theoreticalCoordinates == null)
                return offsets;

            foreach (var measuredCoordinate in measuredCoordinates.ListOfAllCoordinates)
            {
                var currrentSystemName = measuredCoordinate.SystemName;
                if (!measuredCoordinate.AreKnown)
                    continue;

                if (currrentSystemName == "BEAM" || currrentSystemName == "BEAMV")
                {
                    var offset = measuredCoordinate.Clone() as E.Coordinates;
                    offsets.AddOrReplaceCoordinatesInOneSystem(currrentSystemName, offset);
                }
                else if(currrentSystemName == "TOSTATION")
                {
                    var offset = new E.Coordinates("", 0, 0, 0) - measuredCoordinate;
                    offsets.AddOrReplaceCoordinatesInOneSystem(currrentSystemName, offset);
                }
                else
                {
                    var theoreticalCoordinate = theoreticalCoordinates.GetCoordinatesInASystemByName(currrentSystemName);
                    if (theoreticalCoordinate == null)
                        continue;
                    var offset = Common.Analysis.Element.CompareTwoCoordinates(measuredCoordinate, theoreticalCoordinate);
                    if (offset != null)
                        offsets.AddOrReplaceCoordinatesInOneSystem(currrentSystemName, offset);
                }  
            }
            return offsets;
        }
    }
    public static class CoordinatesAllSystemsExtensions
    {
        public static Coordinates GetByFrameName<T>(this T src, string frameName) where T : List<Coordinates>
        {
            return src.Find(x => x.SystemName.ToUpper() == frameName.ToUpper());
        }
    }
}