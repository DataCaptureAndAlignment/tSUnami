﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using TSU.Tools.Conversions;
using static TSU.Polar.GuidedModules.Steps.StakeOut;
using R = TSU.Properties.Resources;

namespace TSU.Common.Elements
{
    //[XmlInclude(typeof(PrimitiveElement))] // permet de serialiser Point car il herite de Primitive element
    [Serializable]
    public class Point : PrimitiveElement, ICloneable, IEquatable<Point>, IComparable<Point>
    {
        /// <summary>
        /// This is use to define how tsunami store the point
        /// </summary>
        [XmlIgnore]
        public Tsunami.StorageStatusTypes StorageStatus
        {
            get
            {
                return (Tsunami2.Properties != null) ? Tsunami2.Properties.GetStorageStatus(this) : Tsunami.StorageStatusTypes.NotStored;
            }
            set
            {
                Tsunami2.Properties?.Store(this, value);
            }
        } 

        [XmlAttribute]
        public virtual string _Point { get; set; }

        public virtual Parameters _Parameters { get; set; }

        /// <summary>
        /// return a zone only if the point is PILIER
        /// </summary>
        [XmlIgnore]
        public string ZoneForSure
        {
            get
            {
                // if last char is '.' then the zone is the first field before any '.': a.b.c.d.e.f. => zone = a, if not zone unknown
                if (_Name != null)
                {
                    if (this.IsPilier)
                    {
                        string[] parts = _Name.Split('.');
                        return parts[0];
                    }
                }
                return R.String_Unknown;
            }
        }

        public static class NameManipulation
        {
            internal static string RemoveZone(string name)
            {
                string[] parts = name.Split('.');
                int startingField = 0;
                if (parts.Length >= 3)
                // this is an ASG name
                {
                    if (parts[1] == "B2")
                        startingField = 2;
                    else
                        startingField = 1;
                }
                var reconstructed = string.Join(".", parts, startingField, parts.Length - startingField);
                return reconstructed;
            }
        }


        /// <summary>
        /// will return the last char after the last point i.e. for LHC.MB.1R1.E, will return "e"            /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="src"></param>
        /// <returns></returns>
        public static string GetPointAkaSocketName(string pointName)
        {
            // Split the string by '.'
            string[] parts = pointName.Split('.');

            // Get the last part
            string lastPart = parts.Length > 0 ? parts[parts.Length - 1] : "";

            return lastPart;
        }

        /// <summary>
        /// return the first part of a name with dotes
        /// </summary>
        [XmlIgnore]
        public string ZoneOrAcc
        {
            get
            {
                // if last char is '.' then the zone is the first field before any '.': a.b.c.d.e.f. => zone = a, if not zone unknown
                if (_Name != null)
                {
                    string[] parts = _Name.Split('.');
                    if (parts.Length > 1)
                        return parts[0];
                }
                return R.String_Unknown;
            }
        }

        [XmlIgnore]
        public string Comment
        {
            get
            {
                string comment = "";
                if (CommentFromUser != "" && CommentFromUser != null)
                {
                    comment += $"user: '{CommentFromUser}'";
                    if (CommentFromTsunami != "")
                        comment += $", ";
                }
                if (CommentFromTsunami != "")
                    comment += $"Tsu: '{CommentFromTsunami}'";

                return comment;
            }
        }

        public string CommentFromUser { get; set; }

        public string CommentFromTsunami { get; set; }

        public virtual SocketCode SocketCode { get; set; }

        /// <summary>
        /// A or O use by geode to detect entrance of magnet?
        /// </summary>
        [XmlAttribute]
        public virtual char SocketType { get; set; } = ' ';

        [XmlAttribute]
        public virtual string SerialNumber { get; set; } = "";

        [XmlAttribute]
        public virtual TSU.ENUM.LgcPointOption LGCFixOption { get; set; }


        [XmlType(TypeName = "Point.Types")]
        public enum Types
        {
            Reference, Nominal, NewPoint
        }

        [XmlAttribute]
        public Types Type { get; set; } = Types.NewPoint;

        private string name;

        [XmlIgnore]
        public string FullNameWithoutZone
        {
            get
            {
                return this._ClassAndNumero + "." + this._Point;
            }
        }


        [XmlIgnore]
        public override string _Name
        {
            get
            {
                if (name == null)
                {
                    if (_Accelerator == R.String_Unknown && _ClassAndNumero == R.String_Unknown + "." + R.String_Unknown)
                        name = _Point;
                    else
                        name = _Accelerator + "." + _ClassAndNumero + "." + _Point;
                }

                return name;
            }
            set
            {
                name = value;
                this.FillPropertiesFrom(name);
                if (this._Coordinates != null)
                {
                    if (this._Coordinates.Local != null) this._Coordinates.Local._Name = name;
                    if (this._Coordinates.Ccs != null) this._Coordinates.Ccs._Name = name;
                    if (this._Coordinates.UserDefined != null) this._Coordinates.UserDefined._Name = name;
                }

            }
        }

        [XmlIgnore]
        public bool IsAlesage
        {
            get
            {
                if (this._Name.Length < 7) return false; //because the minimum name for an alesage is 1.3.5.7
                if (this._Name[this._Name.Length - 1] == '.') return false; // because alesage always finishes with a not '.'
                if (this._Name.Split('.').Length < 4) return false; // beacause alesage always have 4 or 5 or 6 fields.
                return true;
            }
        }

        [XmlIgnore]
        public bool IsPilier
        {
            get
            {
                if (this._Name[this._Name.Length - 1] != '.') return false; // because pilier always finishes with a '.'
                if (this._Name.Length < 6) return false; //because the minimum name for a pilier is 1.3.5.
                if (this._Name.Split('.').Length < 4) return false; // beacause alesage always have 4 or 5 or 6 fields.
                return true;
            }
        }

        [XmlAttribute("RefRoll")]
        public bool IsReferenceForRoll; //To know if a alesage if the reference for the roll instead of the entry

        public Point() : base()
        {
            Initialize();
        }
        public Point(string name) : base(name)
        {
            Initialize();
        }
        public override void Initialize()
        {
            Guid = Guid.NewGuid();
            _Origin = R.String_Unknown;
            _Parameters = new Parameters();
            LGCFixOption = TSU.ENUM.LgcPointOption.POIN;
            this.FillPropertiesFrom(name);
            this.fileElementType = ENUM.ElementType.Point;
            this.IsReferenceForRoll = false;
        }

        public override string ToString()
        {
            var coordinates = this._Coordinates;
            string sCoord = (coordinates != null)? this._Coordinates.ToString(): "No coord";
            return string.Format("{0} : {1}", this._Origin + " - " + this._Name, sCoord);
        }

        public string ToString(bool verbose = true)
        {
            if (!verbose)
                return this.ToString();

            var coordinates = this._Coordinates;
            string sCoord = (coordinates != null) ? this._Coordinates.ToString() : "No coord";

            string details = "";
            details += 
                $"{this._Name}\r\n" +
                $"From: {this._Origin}\r\n" +
                $"{sCoord}\r\n" +
                $"{this._Parameters}\r\n" +
                $"{this.SocketCode}\r\n" +
                $"StorageStatus: {this.StorageStatus}\r\n" +
                $"Role: {this.LGCFixOption}\r\n"
                ;

            return details;
        }

        /// <summary>
        /// "Ls'" for all coordinates
        /// "SU" for local 
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        public string ToString(string format)
        {
            if (format == "Ls")
            {
                return string.Format("{0} : {1}", this._Origin + " - " + this._Name, this._Coordinates.ToString("ALL"));
            }
            if (format == "SU")
            {
                return string.Format("{0} : {1}", this._Origin + " - " + this._Name, this._Coordinates.Local.ToString());
            }

            // else => defautlt
            return this.ToString();
        }


        public string ToString(CoordinatesInAllSystems.StringFormat format)
        {
            string coords = "";
            switch (format)
            {
                case CoordinatesInAllSystems.StringFormat.BeamStnLrv:
                    break;
                default:
                    coords += this._Coordinates.ToString(CoordinatesInAllSystems.StringFormat.BeamCoordinates) + "\r\n";
                    coords += this._Coordinates.ToString(CoordinatesInAllSystems.StringFormat.ToStationCoordinates) + "\r\n";
                    coords += this._Coordinates.ToString(CoordinatesInAllSystems.StringFormat.DisplacementLRV) + "\r\n";
                    break;

            }
            //   return string.Format("{0} from ({1}): \r\n{2}\r\n",
            //      this._Name,
            //      this._Origin,
            //      coords);
            return $"{this._Name} {R.T_FROM} ({this._Origin}): \r\n{coords}\r\n";
        }

        public void FillPropertiesFrom(string name)
        {
            if (name == null) throw new Exception(R.T319);
            this.fileElementType = ENUM.ElementType.Point;
            string[] fields = name.Split('.');

            switch (fields.Length)
            {
                case 3: // not ASG
                    _Accelerator = (Tsunami2.Properties.Zone != null) ? Tsunami2.Properties.Zone._Name : R.String_Unknown;
                    _ClassAndNumero = "";
                    _Point = name;
                    break;
                case 4: // ASG classic 
                    _Accelerator = fields[0];
                    _Zone = fields[0]; //Par défaut la zone est la même que l'accélérateur
                    _ClassAndNumero = fields[1] + "." + fields[2];
                    _Point = fields[3];
                    break;
                case 5: // ASG classic with a question is it a zone.b2 or numero num.num? 
                    int n;
                    //si num.num le field 2 est un numéro dans un acc.b2 le field 2 est la classe donc pas un numéro
                    if (Int32.TryParse(fields[2], out n))
                    {
                        //cas num.num
                        _Accelerator = fields[0]; //Par défaut la zone est la première partie du nom de l'accélérateur
                        _Zone = fields[0]; //Par défaut la zone est la même que l'accélérateur
                        _ClassAndNumero = fields[1] + "." + fields[2] + "." + fields[3];
                        _Point = fields[4];
                    }
                    else
                    {
                        // acc.b2
                        _Accelerator = fields[0] + "." + fields[1]; //Par défaut la zone est la première partie du nom de l'accélérateur
                        _Zone = fields[0]; //Par défaut la zone est la même que l'accélérateur
                        _ClassAndNumero = fields[2] + "." + fields[3];
                        _Point = fields[4];
                    }
                    break;
                case 6: // ASG with acc.b2 and extra dot in class or num 
                    _Accelerator = fields[0] + "." + fields[1];  // acc.b2
                    _Zone = fields[0];
                    _ClassAndNumero = fields[2] + "." + fields[3] + "." + fields[4];
                    _Point = fields[5];
                    break;
                default: // ESA or at least not ASG
                    _Accelerator = Tsunami2.Properties != null ? (Tsunami2.Properties.Zone != null) ? Tsunami2.Properties.Zone._Name : R.String_Unknown : R.String_Unknown;
                    _ClassAndNumero = R.String_Unknown + "." + R.String_Unknown;
                    _Point = name;
                    break;
            }

            //if ((i = name.LastIndexOf(".")) != -1)
            //{
            //    this._Point = name.Substring(i + 1, name.Length - i - 1);
            //    name = name.Substring(0, i);
            //    if ((i = name.LastIndexOf(".")) != -1)
            //    {
            //        this._Numero = name.Substring(i + 1, name.Length - i - 1);
            //        name = name.Substring(0, i);
            //        if ((i = name.LastIndexOf(".")) != -1)
            //        {
            //            this._Class = name.Substring(i + 1, name.Length - i - 1);
            //            name = name.Substring(0, i);
            //            if ((i = name.LastIndexOf(".")) != -1)
            //            {
            //                this._Accelerator = name;
            //                this._Zone = name.Substring(i + 1, name.Length - i - 1);
            //                name = name.Substring(0, i);
            //                if ((i = name.LastIndexOf(".")) != -1)
            //                {
            //                    this._Origin = name;
            //                    this._Zone = this._Origin;
            //                }
            //            }
            //            else
            //            {
            //                this._Zone = name;
            //                this._Accelerator = name;
            //            }
            //        }
            //        else this._Class = name;
            //    }
            //    else this._Numero = name;
            //}
            //else _Point = name;

        }
        public override object Clone()
        {
            Point clone = base.Clone() as Point;
            clone.Guid = Guid.NewGuid();
            return clone;
        }

        public static Point operator -(Point P1, Point P2)
        {
            return Analysis.Element.CompareTwoPoints(P1, P2);
        }

        public static bool operator ==(Point P1, double d)
        {
            bool result = true;
            if (P1._Coordinates.HasCcs) if (P1._Coordinates.Ccs.X.Value != 0 || P1._Coordinates.Ccs.Y.Value != 0 || P1._Coordinates.Ccs.Z.Value != 0) result = false;
            if (P1._Coordinates.HasMla) if (P1._Coordinates.Mla.X.Value != 0 || P1._Coordinates.Mla.Y.Value != 0 || P1._Coordinates.Mla.Z.Value != 0) result = false;
            if (P1._Coordinates.HasSu) if (P1._Coordinates.Su.X.Value != 0 || P1._Coordinates.Su.Y.Value != 0 || P1._Coordinates.Su.Z.Value != 0) result = false;
            if (P1._Coordinates.HasPhysicist) if (P1._Coordinates.Physicist.X.Value != 0 || P1._Coordinates.Physicist.Y.Value != 0 || P1._Coordinates.Physicist.Z.Value != 0) result = false;

            return result;
        }

        public static bool operator !=(Point P1, double d)
        {
            return !(P1 == d);
        }

        public Point DeepCopy()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                Point clone = (Point)formatter.Deserialize(stream);

                clone.Guid = Guid.NewGuid();
                return clone;
            }
        }

        internal void AssignProperties(Point p)
        {
            this.Date = p.Date;
            this.fileElementType = p.fileElementType;
            this.Guid = p.Guid;
            this.Type = p.Type;
            this.LGCFixOption = p.LGCFixOption;
            this.name = p.name;
            this.SerialNumber = p.SerialNumber;
            this.SocketCode = p.SocketCode;
            this.SocketType = p.SocketType;
            this.State = p.State;
            this.Tag = p.Tag;
            this._Accelerator = p._Accelerator;
            this._ClassAndNumero = p._Class;
            this._Coordinates = p._Coordinates;
            this.Id = p.Id;
            this._Name = p._Name;
            this._Origin = p._Origin;
            this._OriginType = p._OriginType;
            this._Parameters = p._Parameters;
            this._Point = p._Point;
            this._Zone = p._Zone;
        }

        public void SetCoordinates(Coordinates c, Coordinates.ReferenceFrames referenceFrame, Coordinates.CoordinateSystemsTypes coordinateSystems = Coordinates.CoordinateSystemsTypes.Unknown) // the referenceFrame should  already be in the coordinate properties but i lieave it at parameter so that we remember taht it is important to give it
        {
            if (c.SystemName == R.String_Unknown) c.SystemName = referenceFrame.ToString();
            switch (referenceFrame)
            {
                case Coordinates.ReferenceFrames.CCS:
                    if (coordinateSystems == Coordinates.CoordinateSystemsTypes._3DCartesian)
                    {
                        this._Coordinates.AddOrReplaceCoordinatesInOneSystem("CCS-Z", c); break;
                    }
                    else
                        this._Coordinates.Ccs = c;
                    break;
                case Coordinates.ReferenceFrames.CernXYHg00Machine:
                case Coordinates.ReferenceFrames.CernXYHg00Topo:
                case Coordinates.ReferenceFrames.CernXYHg1985:
                case Coordinates.ReferenceFrames.CernXYHg85Machine:
                case Coordinates.ReferenceFrames.CERNXYHsSphereSPS:
                    var modifiedCoordinate = new Coordinates() { _Name = c._Name, X = c.X, Y = c.Y, Z = c.Z };
                    this._Coordinates.Ccs = modifiedCoordinate;
                    break;
                case Coordinates.ReferenceFrames.Su1985Machine:
                case Coordinates.ReferenceFrames.Su2000Machine:
                case Coordinates.ReferenceFrames.SuSphere:
                    this._Coordinates.Su = c; break;
                case Coordinates.ReferenceFrames.MLA1985Machine:
                case Coordinates.ReferenceFrames.MLA2000Machine:
                case Coordinates.ReferenceFrames.MLASphere:
                case Coordinates.ReferenceFrames.LA1985:
                case Coordinates.ReferenceFrames.LA2000:
                case Coordinates.ReferenceFrames.LASphere:
                case Coordinates.ReferenceFrames.MLG:
                case Coordinates.ReferenceFrames.MLGSphere:
                case Coordinates.ReferenceFrames.LG:
                case Coordinates.ReferenceFrames.LGSphere:
                    this._Coordinates.Mla = c; break;
                case Coordinates.ReferenceFrames.Phys1985Machine:
                case Coordinates.ReferenceFrames.Phys2000Machine:
                case Coordinates.ReferenceFrames.PhysSphere:
                    this._Coordinates.Physicist = c; break;
                case Coordinates.ReferenceFrames.LAp0:
                case Coordinates.ReferenceFrames.LGp0:
                case Coordinates.ReferenceFrames.CERN_GRF:
                case Coordinates.ReferenceFrames.ITRF97:
                case Coordinates.ReferenceFrames.WGS84:
                case Coordinates.ReferenceFrames.ROMA40:
                case Coordinates.ReferenceFrames.ETRF93:
                case Coordinates.ReferenceFrames.CH1903plus:
                case Coordinates.ReferenceFrames.CernXYHe:
                case Coordinates.ReferenceFrames.CernX0Y0He:
                case Coordinates.ReferenceFrames.CGRFSphere:
                case Coordinates.ReferenceFrames.SwissLV95:
                case Coordinates.ReferenceFrames.SwissLV03:
                case Coordinates.ReferenceFrames.FrenchRGF93Zone5:
                case Coordinates.ReferenceFrames.Lambert93:
                case Coordinates.ReferenceFrames.RGF93:

                default: this._Coordinates.UserDefined = c; break;
            }
        }

        private void GetHypotheticalSystemBasedOnCoordinates(Coordinates c, ref Coordinates.CoordinateSystemsTypes coordinateSystems)
        {
            throw new NotImplementedException();
        }



        // Default comparer for points to sort list of points.
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            Point objAsPoint = obj as Point;
            if (objAsPoint == null) return false;
            else return Equals(objAsPoint);
        }
        public int CompareTo(object obj)
        {
            if (obj == null) return 1;
            Point objAsMeas = obj as Point;
            if (objAsMeas == null) return 1;
            else return CompareTo(objAsMeas);
        }

        public int CompareTo(Point comparePoint)
        {
            // A null value means that this object is greater.
            if (comparePoint == null)
                return 1;
            else
            {
                if (this._Parameters.Cumul == TSU.Tsunami2.Preferences.Values.na) return 0;
                if (comparePoint._Parameters.Cumul == TSU.Tsunami2.Preferences.Values.na) return 1;

                return this._Parameters.Cumul.CompareTo(comparePoint._Parameters.Cumul);
            }
        }
        public override int GetHashCode()
        {
            return this._Parameters.Cumul.GetHashCode();
        }

        internal static List<Point> GetExisitingByPointName(List<Point> pointsWithCoordinates, string name)
        {
            return pointsWithCoordinates.FindAll(x => x._Name == name);
        }

        internal static List<Point> NameContains(string namePart, List<Point> points)
        {
            return points.FindAll(x => x._Name.Contains(namePart));
        }
        internal static Point NameContains_First(string namePart, List<Point> points)
        {
            return points.First(x => x._Name.Contains(namePart));
        }

        internal static Point NameContains_Last(string namePart, List<Point> points)
        {
            return points.LastOrDefault(x => x._Name == namePart);
        }

        public bool Equals(Point other)
        {
            if (other == null) return false;
            return
                (this._Parameters.Cumul == other._Parameters.Cumul)
                &&
                (this._Name == other._Name)
                &&
                //added to not add theoretical point and measure points in the element manager if only theoretical are wanted
                (this._Origin == other._Origin)
                &&
                (this.State == other.State);
        }

        internal static bool ExistIn(string pointName, List<Point> points)
        {
            return points.Find(x => x._Name == pointName) != null;
        }

        internal static bool ExistInWithSameGuid(Point point, List<Point> points)
        {
            return points.Find(x => x.Guid == point.Guid) != null;
        }

        internal static Point GetByeGuid(Guid pointGUID, List<Point> points, out string errorMessage)
        {
            errorMessage = "";

            if (pointGUID == Guid.Empty)
                errorMessage = $"Point with GUID:{pointGUID} not Found (852469)";
                //Logs.Log.AddEntryAsPayAttentionOf(null, errorMessage);

            var founds = points.FindAll(x => x.Guid == pointGUID);

            if (founds.Count == 0)
            {
                errorMessage = $"Point with GUID:{pointGUID} not Found (852369)";
                //Logs.Log.AddEntryAsPayAttentionOf(null, errorMessage);
                return null;
            }

            if (founds.Count > 1)
                throw new Exception($"Multiple Points with same GUID:{pointGUID} found (854369)");

            return founds[0];
        }

        internal static void AddToList(Point point, List<Point> points)
        {
            if (!ExistInWithSameGuid(point, points))
                points.Add(point);
        }

        /// <summary>
        /// based on the guid, a point will be accessed in exsiting in the 2 main list of points saved by tsunami: Tsunami.Points and Tsunami.TempPoints
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        internal static Point GetGlobalPointByGuid(Guid guid)
        {
            if (guid == Guid.Empty)
                return null;

            var founds = Tsunami2.Properties.GetStoredPointById(guid);
            

            if (!founds.Any())
            {
                Logs.Log.AddEntryAsPayAttentionOf(null, $"Point with GUID:{guid} not Found int tsunami points lists(852469)");
                return null;
            }

            if (founds.Count > 1)
                throw new Exception($"Multiple Points with same GUID:{guid} found (854369)");

            return founds[0];
        }

        internal static void SetAsGlobalPoint(Point value, ref Guid guid)
        {
            if (value == null)
            {
                guid = Guid.Empty;
                return;
            }

            guid = value.Guid;

            if (value.Guid == Guid.Empty)
                value.Guid = Guid.NewGuid();

            Tsunami2.Properties.Store(value, value.StorageStatus);
        }

        internal static string GetAssembly(string name)
        {
            string bocIdentificator = "BEAM_";
            if (name.ToUpper().StartsWith(bocIdentificator))
                name = name.Replace(bocIdentificator, "");

            string[] parts = name.Split('.');
            if (parts.Length == 4)
                return parts[0] + "." + parts[1] + "." + parts[2];
            if (parts.Length > 4)
            {
                if (parts[parts.Length - 1].Length == 1) // .E .S .T etc
                    return string.Join(".", parts, 0, parts.Length - 1); // return all previous parts
            }

            return R.String_Unknown;
        }
        /// <summary>
        /// replace the socket part of the name ie.e ".E, .S, .T" by the given one. A.B.C.E => A.B.C.S
        /// </summary>
        /// <param name="name"></param>
        /// <param name="newSocketChar"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        internal static string ReplaceSocketName(string name, string newSocketChar)
        {
            var separator = '.';
            string[] parts = name.Split(separator);
            // Check if there are at least two parts (to avoid index out of range)
            if (parts.Length >= 2)
            {
                // Replace the last part with the new value
                parts[parts.Length - 1] = newSocketChar;

                // Join the parts back together using the separator
                return string.Join(separator.ToString(), parts);
            }
            else
                return name;
        }
    }

    public class PointComparer : IEqualityComparer<Point>
    {
        public bool Equals(Point a, Point b)
        {
            return a._Name == b._Name;
        }

        public int GetHashCode(Point obj)
        {
            return obj._Name.GetHashCode();
        }

    }

    public static class PointExtensions
    {
        public static bool ContainsWithSameName<T>(this T src, Point point) where T : List<Point>
        {
            return src.Find(x => x._Name.ToUpper() == point._Name.ToUpper()) != null;
        }

        public static bool ContainsWithSameName<T>(this T src, string name) where T : List<Point>
        {
            return src.Find(x => x._Name.ToUpper() == name.ToUpper()) != null;
        }

        public static bool AddIfNotExisting<T>(this T src, string name) where T : List<string>
        {
            if (!src.Contains(name))
            {
                src.Add(name);
                return true;
            }
            else return false;
        }

        public static bool AddIfDoNotExistWithSameGUID<T>(this T src, Point point) where T : List<Point>
        {
            if (!src.Contains(point.Guid))
            {
                src.Add(point);
                return true;
            }
            return false;
        }

        public static bool AddIfDoNotExistWithSameName<T>(this T src, Point point) where T : List<Point>
        {
            if (!ContainsWithSameName(src, point))
            {
                src.Add(point);
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void AddRangeIfDoNotExistWithSameName<T>(this T src, List<Point> points) where T : List<Point>
        {
            foreach (var point in points)
            {
                src.AddIfDoNotExistWithSameName(point);
            }
        }

        public static int RemoveAllByName<T>(this T src, List<Point> pointsToRemove) where T : List<Point>
        {
            int foundCount = 0;
            for (int i = src.Count - 1; i >= 0; i--)
            {
                var pointName = src[i]._Name;
                foreach (var point in pointsToRemove)
                {
                    if (pointName == point._Name)
                    {
                        src.RemoveAt(i);
                        foundCount++;
                        break;
                    }
                }
            }
            return foundCount;
        }

        public static bool RemoveAllByName<T>(this T src, string name) where T : List<Point>
        {
            bool found = false;
            for (int i = src.Count - 1; i >= 0; i--)
            {
                if (src[i]._Name == name)
                {
                    src.RemoveAt(i);
                    found = true;
                }
            }
            return found;
        }

        public static bool Contains<T>(this T src, Guid guid) where T : List<Point>
        {
            var founds = src.FindAll(x => x != null && x.Guid == guid);
            return (founds.Count > 0);
        }

        public static bool RemoveByGuid<T>(this T src, Guid guid) where T : List<Point>
        {
            bool found = false;

            for (int i = src.Count - 1; i >= 0; i--)
            {
                if (src[i].Guid == guid)
                {
                    src.RemoveAt(i);
                    found = true;
                }
            }
            return found;
        }

        //// will remove the points marked as TEMP that are not the 10 last ones
        //public static bool CleanTaggedAsTemp<T>(this T src) where T : List<Point>
        //{
        //    if (src.Count < 10)
        //        return false;

        //    bool found = false;
        //    for (int i = src.Count - 10; i >= 0; i--)
        //    {
        //        if (src[i].TagAsTempForCleaning)
        //        {
        //            src.RemoveAt(i);
        //            found = true;
        //        }
        //    }
        //    return found;
        //}

        public static Coordinates GetCoordinatesBasedOn<T>(this T src, string selectedType) where T : Point
        {
            Coordinates c = null;

            if (src._Coordinates.CoordinatesExistInAGivenSystem(selectedType))
                c = src._Coordinates.GetCoordinatesInASystemByName(selectedType);
            else
                c.Set();

            return c;
        }

        public static List<string> GetNames<T>(this T src) where T : List<Point>
        {
            var list = new List<string>();

            foreach (var point in src)
            {
                var name = point._Name;
                if (!list.Contains(name))
                    list.Add(name);
            }

            return list;
        }

        public static List<(string, double)> GetMagnetsNamesAndCumul<T>(this T src) where T : List<Point>
        {
            var helpList = new List<string>();
            var list = new List<(string, double)>();

            foreach (var point in src)
            {
                var name = point._Name;
                if (name.EndsWith("."))
                    continue; // this is a pilar/ref mark.

                var magnetName = name.Substring(0, name.LastIndexOf('.'));

                if (!helpList.Contains(magnetName))
                {
                    helpList.Add(magnetName);
                    list.Add((magnetName, point._Parameters.Cumul));
                }
            }
            return list;
        }

        public static bool GetAllPointsWithEquivalentNames<T>(this T existingPoints, List<Point> pointsToLookFor,
            out List<Point> founds) where T : List<Point>
        {
            founds = new List<Point>();
            foreach (var point in pointsToLookFor)
            {
                foreach (var item in existingPoints)
                {
                    if (item._Name == point._Name)
                    {
                        founds.Add(item);
                    }
                }
            }
            return (founds.Count == pointsToLookFor.Count);
        }

        public static List<Point> GetByAssembly<T>(this T src, string assemblyID) where T : List<Point>
        {
            var list = new List<Point>();
            foreach (var point in src)
            {
                if (point._Name.Contains(assemblyID))
                    if (!list.Contains(point))
                        list.Add(point);
            }
            return list;

        }

        public static List<Point> GetByNameParts<T>(this T src, List<string> nameParts) where T : List<Point>
        {
            var list = new List<Point>();
            foreach (var point in src)
            {
                foreach (var namePart in nameParts)
                {
                    if (point._Name.Contains(namePart))
                        if (!list.Contains(point))
                            list.Add(point);
                }

            }
            return list;

        }

        public static List<Point> RemoveByNameParts<T>(this T src, List<string> nameParts) where T : List<Point>
        {
            var list = new List<Point>();
            foreach (var point in src)
            {
                foreach (var namePart in nameParts)
                {
                    if (!point._Name.Contains(namePart))
                        if (!list.Contains(point))
                            list.Add(point);
                }
            }
            return list;
        }
    }
}
