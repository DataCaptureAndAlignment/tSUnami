using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;
using R = TSU.Properties.Resources;
using T = TSU.Tools;
using TSU.IO.SUSoft;

namespace TSU.Common.Elements
{
    /// <summary>
    /// DEFNISTION DE SYSTEM:
    /// 
    /// Level1: the Coordinates in CCS XYZ
    /// 
    /// Level2: Coordiinates in XYH 
    /// ---here is the limit for the local zone definition----
    /// Level2bis: Coordiinates in MLA by defining the ReferenceSurface of the reference frame (which define the surface i.e. MLA1985Machine force the use of de goeide1985 as referecen surface)
    /// 
    /// Level3: SU define by a 3D rotation apply to the MLA coordiantes(containing only 0 or pi/2)
    /// 
    /// Level4: Phys (userdefine) define by a 3d rotation apply to the SU and and additional rotation around the Ysu axis, it could have been mixed in a single 3drotation but the value of the Ysurotation will have be melt into and we want to keep it visible as this is what is given to us.
    /// </summary>
    /// 
    [Serializable]   
    

    public class Coordinates :  TsuObject , IEquatable<Coordinates> , ICloneable//,IXmlSerializable,
    {
        private const double na = TSU.Preferences.Preferences.NotAvailableValueNa;

        [XmlAttribute("Frame")]
        public string SystemName { get; set; }

        //[XmlAttribute]
        //public virtual ReferenceSurfaces ReferenceSurface { get; set; }

        [XmlAttribute]
        public virtual ReferenceFrames ReferenceFrame { get; set; }
        [XmlAttribute]
        public virtual CoordinateSystemsTypes CoordinateSystem { get; set; }


        [XmlAttribute]
        public virtual List<string> labels
        { 
            get
            {
                var cs = Tsunami2.Properties != null ? Tsunami2.Properties.CoordinatesSystems.GetByName(this.SystemName):null ;
                if (cs != null)
                {
                    return cs.AxisNames;
                }
                else
                    return new List<string>() { "X", "Y", "Z" };
            }
        }
        public virtual DoubleValue X { get; set; }
        public virtual DoubleValue Y { get; set; }
        public virtual DoubleValue Z { get; set; }
        //public virtual DoubleValue H { get; set; }



        public bool AreKnown
        {
            get
            {
                double na =TSU.Tsunami2.Preferences.Values.na;
                if (X == null)
                    return false;
                return !(this.X.Value == na || this.Y.Value == na || (this.Z.Value == na) || this.X.Value == -9999.0);
            }
        }


        private string CreateString(bool showInMm, bool colored = false)
        {
            string s = $"{SystemName,20}: ";

            double tolInMm = (TSU.Tsunami2.Preferences.Values.GuiPrefs == null) ? 0.05 :TSU.Tsunami2.Preferences.Values.Tolerances.Alignment_Radial_mm;
            int precisionDigits = (TSU.Tsunami2.Preferences.Values.GuiPrefs == null) ? 7 :TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
            
            string mOrMm="m";
            int factor = 1;
            if (showInMm)
            {
                factor = 1000;
                mOrMm = "mm";
                precisionDigits = precisionDigits - 3;
            }
        
            if (X != null && X.Value != na)
            {

                string labelX = labels.Count > 0 ? labels[0] : "X";
                string labelY = labels.Count > 1 ? labels[1] : "Y";
                string labelZ = labels.Count > 2 ? labels[2] : "Z";
                string labelH = labels.Count > 3 ? labels[3] : "H";

                int maxLabel = 1;
                foreach (var item in labels)
                {
                    if (item.Length > maxLabel)
                        maxLabel = item.Length;
                }

                Func< string, char, int, string> f = T.Conversions.StringManipulation.AddCharsAfter;

                s += $"{f(labelX,' ', maxLabel)} = {X.ToString(precisionDigits, factor, colored, tolInMm)} {mOrMm}, ";
                s += $"{f(labelY, ' ', maxLabel)} = {Y.ToString(precisionDigits, factor, colored, tolInMm)} {mOrMm}, ";
                if (Math.Abs(Z.Value) != Math.Abs(na))
                    s += $"{f(labelZ, ' ', maxLabel)} = {Z.ToString(precisionDigits, factor, colored, tolInMm)} {mOrMm}";
            }
            else
                s += "Unavailable";
            s += ".";
            return s;
        }


       

        public static Coordinates operator +(Coordinates b, Coordinates c)
        {
            Coordinates a = b.Clone() as Coordinates;
            a.X = b.X + c.X;
            a.Y = b.Y + c.Y;
            a.Z = b.Z + c.Z;
            return a;
        }

        public static Coordinates operator +(Coordinates b, double c)
        {
            Coordinates a = b.Clone() as Coordinates;
            a.X.Value = b.X.Value + c;
            a.Y.Value = b.Y.Value + c;
            a.Z.Value = b.Z.Value + c;
            return a;
        }

        public static Coordinates operator / (Coordinates b, double c)
        {
            Coordinates a = b.Clone() as Coordinates;
            a.X = b.X / c;
            a.Y = b.Y / c;
            a.Z = b.Z / c;
            return a;
        }

        public static Coordinates operator -(Coordinates b, Coordinates c)
        {
            Coordinates a = b.Clone() as Coordinates;
            a.X = b.X- c.X;
            a.Y = b.Y - c.Y;
            a.Z = b.Z -c.Z;
            return a;
        }

        private string cutByPackageOf3Digits(string digits)
        {
            string all = digits.ToString();

            string[] twoPart = all.Split('.');

            List<string> allIntegerParts = new List<string>();
            List<string> allDecimalParts = new List<string>();
            bool done;
            string part;

            // integer part
            done = false;
            part = twoPart[0];
            while (!done)
            {
                if (part.Length > 3)
                {
                    int start = part.Length - 3;
                    allIntegerParts.Insert(0, part.Substring(start, 3));
                    part = part.Remove(start, 3);
                }
                else
                {
                    allIntegerParts.Insert(0, part);
                    done = true;
                }
            }

            // decimal part
            done = false;
            part = twoPart[1];
            while (!done)
            {
                if (part.Length > 3)
                {
                    int start = 0;
                    allDecimalParts.Add(part.Substring(start, 3));
                    part = part.Remove(start, 3);
                }
                else
                {
                    allDecimalParts.Add(part);
                    done = true;
                }
            }

            string result = "";

            foreach (string item in allIntegerParts)
	        {
                result += " " + item;
	        }
            result += ".";

            foreach (string item in allDecimalParts)
            {
                result +=  item + " ";
            }

            return result.Trim();

        }

        public enum ReferenceSurfaces
        {
            Unknown,
            Plane,
            Sphere,
            SurfaceTopo1985,
            Machine1985,
            RS2K
        }
        public enum CoordinateSystemsTypes
        {
            Unknown,
            AutomaticBasedOnReferenceFrame,
            _3DCartesian,
            Geodetic,
            _2DPlusH,
            _2DCartesian,
            GeodeticSphere,
        }
        public enum CoordinateSystemsTsunamiTypes
        {
            Unknown,
            CCS,
            PHYS,
            SU,
            MLA,
            UserDefined,
            ToStation,
            Beam,
            BeamV,
            Displacement, 
            CCS_Z
        }

        // list from csgeo 5.01.00
        //"CCS", "CCS - CERN XYZ"
        //"LAp0", "LAp0"
        //"LGp0", "LGp0"
        //"CGRF", "CERN GRF"
        //"ITRF97", "ITRF97 ep98.5"
        //"WGS84", "WGS84"
        //"ROMA40", "ROMA40"
        //"ETRF93", "ETRF93"
        //"CH1903plus", "CH1903+"
        //"CGRF2", "new CERN GRF"
        //"CernXYHe", "CERN XYHe"
        //"CernX0Y0He", "CERN XoYoHe (Map Transfer)"
        //"CernXYHg00", "CERN XYHg00"
        //"CernXYHg00Topo", "CERN XYHg00Topo"
        //"CernXYHg00Machine", "CERN XYHg (RS2K)"
        //"CernXYHg85", "CERN XYHg1985 (Surface Topo)"
        //"CernXYHg85Machine", "CERN XYHg1985 (LHC)"
        //"CERNXYHsSphereSPS", "CERN XYHs (SPS)"
        //"CGRFSphere", "CERN GRF sphere"
        //"SwissLV95", "Swiss LV95 (ellipsoidal height)"
        //"SwissLV03", "Swiss LV03 (ellipsoidal height)"
        //"FrenchRGF93Zone5", "French RGF93zone5 (ellipsoidal height)"
        //"Lambert93", "French Lambert93"
        //"RGF93", "RGF93"
        //"CHTRF95", "CHTRF95"
        //"MLA1985Machine", "MLAMachine (1985)"
        //"MLA1985H0", "MLAH0 (1985)"
        //"MLA2000Machine", "MLAMachine (2000)"
        //"MLA2000Topo", "MLATopo (2000)"
        //"MLA2000H0", "MLAH0 (2000)"
        //"MLASphere", "MLA (Sphere)"
        //"LA1985Machine", "LAMachine (1985)"
        //"LA1985H0", "LAH0 (1985)"
        //"LA2000Machine", "LAMachine (2000)"
        //"LA2000Topo", "LATopo (2000)"
        //"LA2000H0", "LAH0 (2000)"
        //"LASphere", "LA (Sphere)"
        //"MLGGRS80", "MLG"
        //"MLGSphere", "MLG (Sphere)"
        //"LGGRS80", "LG "
        //"LGSphere", "LG (Sphere)"
        //"LocalRefFrame", "LocalRefFrame (RESERVED)"
        public enum ReferenceFrames
        {
            Unknown ,
            CCS,
            LAp0,
            LGp0 ,
            CERN_GRF ,
            ITRF97 ,
            WGS84 ,
            ROMA40 ,
            ETRF93 ,
            CH1903plus ,
            CGRF2,
            CernXYHe ,
            CernX0Y0He ,
            CernXYHg00 ,
            CernXYHg00Topo ,
            CernXYHg00Machine ,
            CernXYHg1985 ,
            CernXYHg85Machine ,
            CERNXYHsSphereSPS ,
            CGRFSphere ,
            SwissLV95 ,
            SwissLV03 ,
            FrenchRGF93Zone5 ,
            Lambert93 ,
            RGF93 ,
            CHTRF95 ,
            MLA1985Machine ,
            MLA2000Machine ,
            MLASphere ,
            LA1985 ,
            LA2000 ,
            LASphere ,
            MLG ,
            MLGSphere ,
            LG ,
            LGSphere ,
            Su1985Machine,
            Phys1985Machine,
            Su2000Machine,
            Phys2000Machine,
            SuSphere,
            PhysSphere
        }

        public static ReferenceFrames GetTsunamiReferenceFramesBasedOnCsGeoRF(CSGEOdll.ReferenceFrames csGeoRF)
        {
            switch (csGeoRF)
            {
                case CSGEOdll.ReferenceFrames.kCCS: 
                    return ReferenceFrames.CCS;
                case CSGEOdll.ReferenceFrames.kLAp0:
                    return ReferenceFrames.LAp0;
                    
                case CSGEOdll.ReferenceFrames.kLGp0:
                    return ReferenceFrames.LGp0;
                case CSGEOdll.ReferenceFrames.kCGRF:
                    return ReferenceFrames.CERN_GRF;
                case CSGEOdll.ReferenceFrames.kITRF97:
                    return ReferenceFrames.ITRF97;
                case CSGEOdll.ReferenceFrames.kWGS84:
                    return ReferenceFrames.WGS84;
                case CSGEOdll.ReferenceFrames.kROMA40:
                    return ReferenceFrames.ROMA40;
                case CSGEOdll.ReferenceFrames.kETRF93:
                    return ReferenceFrames.ETRF93;
                case CSGEOdll.ReferenceFrames.kCH1903plus:
                    return ReferenceFrames.CH1903plus;
                case CSGEOdll.ReferenceFrames.kCGRF_new:
                    return ReferenceFrames.CGRF2;
                case CSGEOdll.ReferenceFrames.kCCS_new:
                    return ReferenceFrames.CCS;
                case CSGEOdll.ReferenceFrames.kLAp0_new:
                    return ReferenceFrames.LAp0;
                case CSGEOdll.ReferenceFrames.kLGp0_new:
                    return ReferenceFrames.LGp0;
                case CSGEOdll.ReferenceFrames.kITRFin:
                case CSGEOdll.ReferenceFrames.kITRFout:
                    return ReferenceFrames.ITRF97;
                case CSGEOdll.ReferenceFrames.kETRFin:
                case CSGEOdll.ReferenceFrames.kETRFout:
                    return ReferenceFrames.ETRF93;
                case CSGEOdll.ReferenceFrames.kCGRFSphere:
                    return ReferenceFrames.CGRFSphere;
                case CSGEOdll.ReferenceFrames.kCernXYHe:
                    return ReferenceFrames.CernXYHe;
                case CSGEOdll.ReferenceFrames.kCernX0Y0He:
                    return ReferenceFrames.CernX0Y0He;
                case CSGEOdll.ReferenceFrames.kCernXYHg00:
                    return ReferenceFrames.CernXYHg00;
                case CSGEOdll.ReferenceFrames.kCernXYHg00Topo:
                    return ReferenceFrames.CernXYHg00Topo;
                case CSGEOdll.ReferenceFrames.kCernXYHg00Machine:
                    return ReferenceFrames.CernXYHg00Machine;
                case CSGEOdll.ReferenceFrames.kCernXYHg85:
                case CSGEOdll.ReferenceFrames.kCernXYHg85Machine:
                    return ReferenceFrames.CernXYHg85Machine;
                case CSGEOdll.ReferenceFrames.kCERNXYHsSphereSPS:
                    return ReferenceFrames.CERNXYHsSphereSPS;
                case CSGEOdll.ReferenceFrames.kCGRFMercator_eh:
                    return ReferenceFrames.Unknown;
                case CSGEOdll.ReferenceFrames.kSwissLV95_eh:
                    return ReferenceFrames.SwissLV95;
                case CSGEOdll.ReferenceFrames.kSwissLV03_eh:
                    return ReferenceFrames.SwissLV03;
                case CSGEOdll.ReferenceFrames.kSwissLV95_lhn95:
                    return ReferenceFrames.SwissLV95;
                case CSGEOdll.ReferenceFrames.kSwissLV03_ln02:
                    return ReferenceFrames.SwissLV03;
                case CSGEOdll.ReferenceFrames.kSwissLV95_ln02:
                    return ReferenceFrames.SwissLV95;
                case CSGEOdll.ReferenceFrames.kSwissLV03_lhn95:
                    return ReferenceFrames.SwissLV03;
                case CSGEOdll.ReferenceFrames.kCHTRF95:
                    return ReferenceFrames.CHTRF95;
                case CSGEOdll.ReferenceFrames.kFrenchRGF93_CC46_eh:
                    return ReferenceFrames.Unknown;
                case CSGEOdll.ReferenceFrames.kFrenchRGF93_CC46_ign69:
                    return ReferenceFrames.Unknown;
                case CSGEOdll.ReferenceFrames.kLambert93_eh:
                    return ReferenceFrames.Lambert93;
                case CSGEOdll.ReferenceFrames.kLambert93_ign69:
                    return ReferenceFrames.Lambert93;
                case CSGEOdll.ReferenceFrames.kRGF93:
                    return ReferenceFrames.RGF93;
                case CSGEOdll.ReferenceFrames.kMLA1985Machine:
                    return ReferenceFrames.MLA1985Machine;
                case CSGEOdll.ReferenceFrames.kMLA2000Machine:
                    return ReferenceFrames.MLA2000Machine;
                case CSGEOdll.ReferenceFrames.kMLASphere:
                    return ReferenceFrames.MLASphere;
                case CSGEOdll.ReferenceFrames.kMLA2000Topo:
                    return ReferenceFrames.Unknown;
                case CSGEOdll.ReferenceFrames.kMLA2000H0:
                    return ReferenceFrames.Unknown;
                case CSGEOdll.ReferenceFrames.kMLA1985H0:
                    return ReferenceFrames.Unknown;
                case CSGEOdll.ReferenceFrames.kLA1985Machine:
                    return ReferenceFrames.LA1985;
                case CSGEOdll.ReferenceFrames.kLA2000Machine:
                    return ReferenceFrames.LA2000;
                case CSGEOdll.ReferenceFrames.kLASphere:
                    return ReferenceFrames.LASphere;
                case CSGEOdll.ReferenceFrames.kLA2000Topo:
                    return ReferenceFrames.Unknown;
                case CSGEOdll.ReferenceFrames.kLA2000H0:
                    return ReferenceFrames.Unknown;
                case CSGEOdll.ReferenceFrames.kLA1985H0:
                    return ReferenceFrames.Unknown;
                case CSGEOdll.ReferenceFrames.kMLGGRS80:
                    return ReferenceFrames.Unknown;
                case CSGEOdll.ReferenceFrames.kMLGSphere:
                    return ReferenceFrames.Unknown;
                case CSGEOdll.ReferenceFrames.kLGGRS80:
                    return ReferenceFrames.Unknown;
                case CSGEOdll.ReferenceFrames.kLGSphere:
                    return ReferenceFrames.LGSphere;
                default:
                    return ReferenceFrames.Unknown;
            }
        }

        public static CoordinateSystemsTsunamiTypes GetCoordinateTsunamiType(Coordinates.ReferenceFrames ReferenceFrames)
        {
            switch (ReferenceFrames)
            {
                case Coordinates.ReferenceFrames.CCS:
                case Coordinates.ReferenceFrames.CernXYHe:
                case Coordinates.ReferenceFrames.CernX0Y0He:
                case Coordinates.ReferenceFrames.CernXYHg00Machine:
                case Coordinates.ReferenceFrames.CernXYHg00Topo:
                case Coordinates.ReferenceFrames.CernXYHg1985:
                case Coordinates.ReferenceFrames.CernXYHg85Machine:
                case Coordinates.ReferenceFrames.CERNXYHsSphereSPS:
                    return CoordinateSystemsTsunamiTypes.CCS;
                case Coordinates.ReferenceFrames.Su1985Machine:
                case Coordinates.ReferenceFrames.Su2000Machine:
                    return CoordinateSystemsTsunamiTypes.SU;
                case Coordinates.ReferenceFrames.MLA1985Machine:
                case Coordinates.ReferenceFrames.MLA2000Machine:
                case Coordinates.ReferenceFrames.MLASphere:
                case Coordinates.ReferenceFrames.LA1985:
                case Coordinates.ReferenceFrames.LA2000:
                case Coordinates.ReferenceFrames.LASphere:
                case Coordinates.ReferenceFrames.MLG:
                case Coordinates.ReferenceFrames.MLGSphere:
                case Coordinates.ReferenceFrames.LG:
                case Coordinates.ReferenceFrames.LGSphere:
                    return CoordinateSystemsTsunamiTypes.MLA;
                case Coordinates.ReferenceFrames.Phys1985Machine:
                case Coordinates.ReferenceFrames.Phys2000Machine:
                    return CoordinateSystemsTsunamiTypes.PHYS;
                case Coordinates.ReferenceFrames.Unknown:
                case Coordinates.ReferenceFrames.LAp0:
                case Coordinates.ReferenceFrames.LGp0:
                case Coordinates.ReferenceFrames.CERN_GRF:
                case Coordinates.ReferenceFrames.ITRF97:
                case Coordinates.ReferenceFrames.WGS84:
                case Coordinates.ReferenceFrames.ROMA40:
                case Coordinates.ReferenceFrames.ETRF93:
                case Coordinates.ReferenceFrames.CH1903plus:
                case Coordinates.ReferenceFrames.CGRFSphere:
                case Coordinates.ReferenceFrames.SwissLV95:
                case Coordinates.ReferenceFrames.SwissLV03:
                case Coordinates.ReferenceFrames.FrenchRGF93Zone5:
                case Coordinates.ReferenceFrames.Lambert93:
                case Coordinates.ReferenceFrames.RGF93:
                default:
                    return CoordinateSystemsTsunamiTypes.UserDefined;
            }
        }
        public static Coordinates.ReferenceFrames GetXYhSystem(Coordinates.ReferenceSurfaces surface)
        {
            switch (surface)
            {
                case ReferenceSurfaces.Sphere:
                    return ReferenceFrames.CERNXYHsSphereSPS;
                case ReferenceSurfaces.SurfaceTopo1985:
                    return ReferenceFrames.CernXYHg1985;
                case ReferenceSurfaces.Machine1985:
                    return ReferenceFrames.CernXYHg85Machine;
                case ReferenceSurfaces.RS2K:
                    return ReferenceFrames.CernXYHg00Machine;
                    
                case ReferenceSurfaces.Unknown:
                case ReferenceSurfaces.Plane:
                default:
                    return ReferenceFrames.CCS;
            }
        }

        public static Coordinates.ReferenceFrames GetRelativeSystemMlaAndXyh(Coordinates.ReferenceFrames referenceFrames)
        {
            switch (referenceFrames)
            {
                case Coordinates.ReferenceFrames.MLA1985Machine:
                    return Coordinates.ReferenceFrames.CernXYHg85Machine;
                case Coordinates.ReferenceFrames.MLA2000Machine:
                    return Coordinates.ReferenceFrames.CernXYHg00Machine;
                case Coordinates.ReferenceFrames.CernXYHg00Machine:
                    return Coordinates.ReferenceFrames.MLA2000Machine;
                case Coordinates.ReferenceFrames.CernXYHg85Machine:
                    return Coordinates.ReferenceFrames.MLA1985Machine;
                case Coordinates.ReferenceFrames.CERNXYHsSphereSPS:
                    return Coordinates.ReferenceFrames.MLASphere;
                case Coordinates.ReferenceFrames.MLASphere:
                    return Coordinates.ReferenceFrames.CERNXYHsSphereSPS;
                case Coordinates.ReferenceFrames.Unknown:
                case Coordinates.ReferenceFrames.CCS:
                case Coordinates.ReferenceFrames.LAp0:
                case Coordinates.ReferenceFrames.LGp0:
                case Coordinates.ReferenceFrames.CERN_GRF:
                case Coordinates.ReferenceFrames.ITRF97:
                case Coordinates.ReferenceFrames.WGS84:
                case Coordinates.ReferenceFrames.ROMA40:
                case Coordinates.ReferenceFrames.ETRF93:
                case Coordinates.ReferenceFrames.CH1903plus:
                case Coordinates.ReferenceFrames.CernXYHe:
                case Coordinates.ReferenceFrames.CernX0Y0He:
                case Coordinates.ReferenceFrames.CernXYHg00Topo:
                case Coordinates.ReferenceFrames.CernXYHg1985:
                case Coordinates.ReferenceFrames.CGRFSphere:
                case Coordinates.ReferenceFrames.SwissLV95:
                case Coordinates.ReferenceFrames.SwissLV03:
                case Coordinates.ReferenceFrames.FrenchRGF93Zone5:
                case Coordinates.ReferenceFrames.Lambert93:
                case Coordinates.ReferenceFrames.RGF93:
                case Coordinates.ReferenceFrames.LA1985:
                case Coordinates.ReferenceFrames.LA2000:
                case Coordinates.ReferenceFrames.LASphere:
                case Coordinates.ReferenceFrames.MLG:
                case Coordinates.ReferenceFrames.MLGSphere:
                case Coordinates.ReferenceFrames.LG:
                case Coordinates.ReferenceFrames.LGSphere:
                default:
                    return Coordinates.ReferenceFrames.Unknown;
            }
        }

        public static Coordinates.ReferenceSurfaces GetReferenceSurface(Coordinates.ReferenceFrames referenceFrames)
        {
            switch (referenceFrames)
            {
                case ReferenceFrames.CernXYHg00Topo:
                case ReferenceFrames.Su1985Machine:
                case ReferenceFrames.Phys1985Machine:
                case ReferenceFrames.MLA1985Machine:
                case ReferenceFrames.CernXYHg1985:
                case ReferenceFrames.CernXYHg85Machine:
                case ReferenceFrames.LA1985:
                    return ReferenceSurfaces.Machine1985;
                case ReferenceFrames.Su2000Machine:
                case ReferenceFrames.Phys2000Machine:
                case ReferenceFrames.MLA2000Machine:
                case ReferenceFrames.LA2000:
                case ReferenceFrames.CernXYHg00:
                case ReferenceFrames.CernXYHg00Machine:
                case ReferenceFrames.CCS:
                    return ReferenceSurfaces.RS2K;
                case ReferenceFrames.SuSphere:
                case ReferenceFrames.PhysSphere:
                case ReferenceFrames.LGSphere:
                case ReferenceFrames.MLGSphere:
                case ReferenceFrames.LASphere:
                case ReferenceFrames.CERNXYHsSphereSPS:
                case ReferenceFrames.CGRFSphere:
                case ReferenceFrames.MLASphere:
                    return ReferenceSurfaces.Sphere;
                case ReferenceFrames.Unknown:
                case ReferenceFrames.LG:
                case ReferenceFrames.MLG:
                case ReferenceFrames.SwissLV95:
                case ReferenceFrames.SwissLV03:
                case ReferenceFrames.FrenchRGF93Zone5:
                case ReferenceFrames.Lambert93:
                case ReferenceFrames.RGF93:
                case ReferenceFrames.CHTRF95:
                case ReferenceFrames.LAp0:
                case ReferenceFrames.LGp0:
                case ReferenceFrames.CERN_GRF:
                case ReferenceFrames.ITRF97:
                case ReferenceFrames.WGS84:
                case ReferenceFrames.ROMA40:
                case ReferenceFrames.ETRF93:
                case ReferenceFrames.CH1903plus:
                case ReferenceFrames.CGRF2:
                case ReferenceFrames.CernXYHe:
                case ReferenceFrames.CernX0Y0He:
                default:
                    return ReferenceSurfaces.Unknown;
            }
        }

        /// <summary>
        /// this return the prefered type when the given one is not available
        /// </summary>
        /// <param name="coordinateTypeToUseForcomparaison"></param>
        /// <returns></returns>
        internal string GetNextPreferedType(string coordinateTypeToUseForcomparaison)
        {
            switch (coordinateTypeToUseForcomparaison)
            {
                case "CCS": return "SU";
                case "UserDefined":
                case "Displacement":
                case "BeamV":
                case "Beam":
                case "ToStation":
                case "MLA":
                case "SU":
                case "PHYSIICIST":
                case "Unknown":
                default: return "CCS";
            }
        }

        public static Coordinates.ReferenceFrames GetRelativeSystemMlaAndSu(Coordinates.ReferenceFrames referenceFrames)
        {
            switch (referenceFrames)
            {
                case Coordinates.ReferenceFrames.MLA1985Machine:
                    return Coordinates.ReferenceFrames.Su1985Machine;
                case Coordinates.ReferenceFrames.MLA2000Machine:
                    return Coordinates.ReferenceFrames.Su2000Machine;
                case Coordinates.ReferenceFrames.Su1985Machine:
                    return Coordinates.ReferenceFrames.MLA1985Machine;
                case Coordinates.ReferenceFrames.Su2000Machine:
                    return Coordinates.ReferenceFrames.MLA2000Machine;
                default:
                    return Coordinates.ReferenceFrames.Unknown;
            }
        }
        public static bool IsLocalSystem(ReferenceFrames ReferenceFrame)
        {
            switch (ReferenceFrame)
            {
                case ReferenceFrames.MLA1985Machine:
                case ReferenceFrames.MLA2000Machine:
                case ReferenceFrames.MLASphere:
                case ReferenceFrames.LA1985:
                case ReferenceFrames.LA2000:
                case ReferenceFrames.LASphere:
                case ReferenceFrames.MLG:
                case ReferenceFrames.MLGSphere:
                case ReferenceFrames.LG:
                case ReferenceFrames.LGSphere:
                case Coordinates.ReferenceFrames.Su1985Machine:
                case Coordinates.ReferenceFrames.Su2000Machine:
                case Coordinates.ReferenceFrames.Phys1985Machine:
                case Coordinates.ReferenceFrames.Phys2000Machine:
                    return true;
                case ReferenceFrames.CernXYHg00Machine:
                case ReferenceFrames.CernXYHg00Topo:
                case ReferenceFrames.CernXYHg1985:
                case ReferenceFrames.CernXYHg85Machine:
                case ReferenceFrames.CERNXYHsSphereSPS:
                case ReferenceFrames.SwissLV95:
                case ReferenceFrames.SwissLV03:
                case ReferenceFrames.FrenchRGF93Zone5:
                case ReferenceFrames.Lambert93:
                case ReferenceFrames.CCS:
                case ReferenceFrames.LAp0:
                case ReferenceFrames.LGp0:
                case ReferenceFrames.CERN_GRF:
                case ReferenceFrames.ITRF97:
                case ReferenceFrames.WGS84:
                case ReferenceFrames.ROMA40:
                case ReferenceFrames.ETRF93:
                case ReferenceFrames.CH1903plus:
                case ReferenceFrames.CernXYHe:
                case ReferenceFrames.CernX0Y0He:
                case ReferenceFrames.CGRFSphere:
                case ReferenceFrames.RGF93:

                default:
                    return false;
            }
        }

        public Coordinates()
        {
            _Name =R.String_Unknown;
            SystemName =R.String_Unknown;
            SystemName = R.String_Unknown;
            //X = new DoubleValue();
            //Y = new DoubleValue();
            //Z = new DoubleValue();
        }
        public Coordinates(string name, double x = -999.9, double y = -999.9, double z = -999.9) : this()
        {
            this._Name =name;
            Set(x, y, z);
        }
        public Coordinates(string[] s, int startingIndex)
            : this()
        {
            Set(s, startingIndex);
        }
        internal void Set (double x, double y, double z)
        {
            if (X == null) X = new DoubleValue(); X.Value = x;
            if (Y == null) Y = new DoubleValue(); Y.Value = y;
            if (Z == null) Z = new DoubleValue(); Z.Value = z;
        }
        internal void Set(string s, int startingIndexInTheSpaceSeperatorString)
        {
            s = System.Text.RegularExpressions.Regex.Replace(s, @"(\s)\s+", "$1");
            string[] blocks = s.Split(' ');
            Set(blocks, startingIndexInTheSpaceSeperatorString);
        }
        internal void Set(string[] s, int startingIndex)
        {
            Set(
                T.Conversions.Numbers.ToDouble(s[startingIndex]),
                T.Conversions.Numbers.ToDouble(s[startingIndex + 1]),
                T.Conversions.Numbers.ToDouble(s[startingIndex + 2]));
        }
        internal void Set(DoubleValue x, DoubleValue y, DoubleValue z)
        {
            X = x;
            Y = y;
            Z = z;
        }
        

        //// IXmlSerializable
        //public System.Xml.Schema.XmlSchema GetSchema()
        //{

        //    return null;
        //}
        //public void ReadXml(XmlReader reader)
        //{
        //    reader.ReadToFollowing("X"); X.ReadXml(reader);
        //    reader.ReadToFollowing("Y"); Y.ReadXml(reader);
        //    reader.ReadToFollowing("Z"); Z.ReadXml(reader);
        //    reader.ReadToFollowing("H"); H.ReadXml(reader);
        //}
        //public void WriteXml(XmlWriter writer)
        //{
        //    writer.WriteStartElement("X"); X.WriteXml(writer); writer.WriteEndElement();
        //    writer.WriteStartElement("Y"); Y.WriteXml(writer); writer.WriteEndElement();
        //    writer.WriteStartElement("Z"); Z.WriteXml(writer); writer.WriteEndElement();
        //    writer.WriteStartElement("H"); H.WriteXml(writer); writer.WriteEndElement();
        //}

        // IEquatable<Coordinates>
        public bool Equals(Coordinates other)
        {
            return (X == other.X && Y == other.Y && Z == other.Z);
            
        }
        public override bool Equals(object obj)
        {
            if (obj is Coordinates && !ReferenceEquals(obj, null))
                return this.Equals(obj as Coordinates);
            else
                return false;
        }
        //public static bool operator == (Coordinates v1, object v2)
        //{
        //    if (ReferenceEquals(v1, null))
        //        return ReferenceEquals(v2, null);

        //    return v1.Equals(v2);
        //}

        //public static bool operator ==(Coordinates v1, object v2)
        //{
        //    if (!ReferenceEquals(v1, null) && !ReferenceEquals(v2, null))
        //        return v1.Equals(v2);
        //    else
        //        return false;
        //}

        //public static bool operator !=(Coordinates v1, object v2)
        //{
        //    return !(v1 == v2);
        //}

        public override int GetHashCode()
        {
            if (this.X == null) return this.Guid.GetHashCode();
            return this.X.GetHashCode() ^ this.Y.GetHashCode() ^ this.Z.GetHashCode();
        }

        public override object Clone()
        {
            Coordinates clone = (Coordinates)this.MemberwiseClone();
            if (this.X != null)
                clone.X = this.X.Clone() as DoubleValue;
            if (this.Y != null)
                clone.Y = this.Y.Clone() as DoubleValue;
            if (this.Z != null)
                clone.Z = this.Z.Clone() as DoubleValue; 
            
            clone.SystemName = this.SystemName;
            return clone;
        }

        internal static ReferenceFrames GetReferenceFrame(ReferenceSurfaces surface, CoordinateSystemsTypes type, Coordinates.CoordinateSystemsTsunamiTypes TSUType = CoordinateSystemsTsunamiTypes.Unknown)
        {
            switch (surface)
            {
                case ReferenceSurfaces.Sphere:
                    if (type == CoordinateSystemsTypes._3DCartesian)
                    {
                        switch (TSUType)
                        {
                            case CoordinateSystemsTsunamiTypes.PHYS:
                                return ReferenceFrames.PhysSphere;
                            case CoordinateSystemsTsunamiTypes.SU:
                                return ReferenceFrames.SuSphere;
                            default:
                                return ReferenceFrames.MLASphere;
                        }
                    }
                    else
                        return ReferenceFrames.CERNXYHsSphereSPS;
                case ReferenceSurfaces.Machine1985:
                    if (type == CoordinateSystemsTypes._3DCartesian)
                    {
                        switch (TSUType)
                        {
                            case CoordinateSystemsTsunamiTypes.PHYS:
                                return ReferenceFrames.Phys1985Machine;
                            case CoordinateSystemsTsunamiTypes.SU:
                                return ReferenceFrames.Su1985Machine;
                            default:
                                return ReferenceFrames.MLA1985Machine;
                        }
                    }
                    else
                        return ReferenceFrames.CernXYHg85Machine;
                case ReferenceSurfaces.RS2K:
                    if (type == CoordinateSystemsTypes._3DCartesian)
                    {
                        switch (TSUType)
                        {
                            case CoordinateSystemsTsunamiTypes.PHYS:
                                return ReferenceFrames.Phys2000Machine;
                            case CoordinateSystemsTsunamiTypes.SU:
                                return ReferenceFrames.Su2000Machine;
                            case CoordinateSystemsTsunamiTypes.CCS_Z:
                                return ReferenceFrames.CCS;
                            default:
                                return ReferenceFrames.MLA2000Machine;
                        }
                    }
                    else
                        return ReferenceFrames.CernXYHg00Machine;
                    
                case ReferenceSurfaces.Plane:
                case ReferenceSurfaces.SurfaceTopo1985:
                case ReferenceSurfaces.Unknown:
                default:
                    return ReferenceFrames.Unknown;
            }
        }

        internal static CoordinateSystemsTypes GetCoordinatesSystems(CoordinateSystemsTsunamiTypes cstt)
        {
            switch (cstt)
            {
                case CoordinateSystemsTsunamiTypes.CCS:
                    return CoordinateSystemsTypes._2DPlusH;
                case CoordinateSystemsTsunamiTypes.SU:
                case CoordinateSystemsTsunamiTypes.MLA:
                case CoordinateSystemsTsunamiTypes.ToStation:
                case CoordinateSystemsTsunamiTypes.Beam:
                case CoordinateSystemsTsunamiTypes.BeamV:
                case CoordinateSystemsTsunamiTypes.Displacement:
                case CoordinateSystemsTsunamiTypes.PHYS:
                case CoordinateSystemsTsunamiTypes.CCS_Z:
                    return CoordinateSystemsTypes._3DCartesian;
                case CoordinateSystemsTsunamiTypes.UserDefined:
                case CoordinateSystemsTsunamiTypes.Unknown: 
                default:
                    return CoordinateSystemsTypes.Unknown;
            }
        }

        internal static CoordinateSystemsTypes GetCoordinatesSystemsTypeBasedOn(ReferenceFrames referenceFrame)
        {
            switch (referenceFrame)
            {
                case ReferenceFrames.CernXYHg00Machine:
                case ReferenceFrames.CernXYHg00Topo:
                case ReferenceFrames.CernXYHg1985:
                case ReferenceFrames.CernXYHg85Machine:
                case ReferenceFrames.CERNXYHsSphereSPS:
                case ReferenceFrames.SwissLV95:
                case ReferenceFrames.SwissLV03:
                case ReferenceFrames.FrenchRGF93Zone5:
                case ReferenceFrames.Lambert93:
                    return CoordinateSystemsTypes._2DPlusH;
                case ReferenceFrames.CCS:
                case ReferenceFrames.LAp0:
                case ReferenceFrames.LGp0:
                case ReferenceFrames.CERN_GRF:
                case ReferenceFrames.ITRF97:
                case ReferenceFrames.WGS84:
                case ReferenceFrames.ROMA40:
                case ReferenceFrames.ETRF93:
                case ReferenceFrames.CH1903plus:
                case ReferenceFrames.CernXYHe:
                case ReferenceFrames.CernX0Y0He:
                case ReferenceFrames.CGRFSphere:
                case ReferenceFrames.RGF93:
                case ReferenceFrames.MLA1985Machine:
                case ReferenceFrames.MLA2000Machine:
                case ReferenceFrames.MLASphere:
                case ReferenceFrames.LA1985:
                case ReferenceFrames.LA2000:
                case ReferenceFrames.LASphere:
                case ReferenceFrames.MLG:
                case ReferenceFrames.MLGSphere:
                case ReferenceFrames.LG:
                case ReferenceFrames.LGSphere:
                case Coordinates.ReferenceFrames.Su1985Machine:
                case Coordinates.ReferenceFrames.Su2000Machine:
                case Coordinates.ReferenceFrames.Phys1985Machine:
                case Coordinates.ReferenceFrames.Phys2000Machine:
                    return CoordinateSystemsTypes._3DCartesian;
                default:
                    return CoordinateSystemsTypes.Unknown;
            }
        }
        /// <summary>
        /// fait une copie m�moire de la coordonn�e
        /// </summary>
        /// <returns></returns>
        public Coordinates DeepCopy()
        // fait une copie m�moire de la coordonn�e
        {
            using (MemoryStream stream = new MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                Coordinates p = (Coordinates)formatter.Deserialize(stream);
                return p;
            }
        }

        internal static ReferenceFrames CorrespondingReferenceFrame(CoordinateSystemsTsunamiTypes coordinateSystemsTypes, ReferenceSurfaces referenceSurfaces)
        {
            switch (coordinateSystemsTypes)
	        {
                case CoordinateSystemsTsunamiTypes.CCS:
                    switch (referenceSurfaces)
	                {
                        case ReferenceSurfaces.Sphere: return ReferenceFrames.CERNXYHsSphereSPS;
                        case ReferenceSurfaces.Machine1985: return ReferenceFrames.CernXYHg85Machine;
                        case ReferenceSurfaces.RS2K:return ReferenceFrames.CernXYHg00Machine;
                        default: return ReferenceFrames.Unknown;
	                }
                case CoordinateSystemsTsunamiTypes.SU:
                    switch (referenceSurfaces)
                    {
                        case ReferenceSurfaces.Sphere: return ReferenceFrames.SuSphere;
                        case ReferenceSurfaces.Machine1985: return ReferenceFrames.Su1985Machine;
                        case ReferenceSurfaces.RS2K: return ReferenceFrames.Su2000Machine;
                        default: return ReferenceFrames.Unknown;
                    }
                case CoordinateSystemsTsunamiTypes.MLA:
                    switch (referenceSurfaces)
                    {
                        case ReferenceSurfaces.Sphere: return ReferenceFrames.MLASphere;
                        case ReferenceSurfaces.Machine1985: return ReferenceFrames.MLA1985Machine;
                        case ReferenceSurfaces.RS2K: return ReferenceFrames.MLA2000Machine;
                        default: return ReferenceFrames.Unknown;
                    }
                case CoordinateSystemsTsunamiTypes.PHYS:
                    switch (referenceSurfaces)
                    {
                        case ReferenceSurfaces.Sphere: return ReferenceFrames.PhysSphere;
                        case ReferenceSurfaces.Machine1985: return ReferenceFrames.Phys1985Machine;
                        case ReferenceSurfaces.RS2K: return ReferenceFrames.Phys2000Machine;
                        default: return ReferenceFrames.Unknown;
                    }
                default:
                 return ReferenceFrames.Unknown;
	        }
        }

        internal static Coordinates GetZeroZeroZeroWithSameInfoAs(Coordinates beamV)
        {
            Coordinates c = beamV.Clone() as Coordinates;
            c.X = new DoubleValue(0, 0);
            c.Y = new DoubleValue(0, 0);
            c.Z = new DoubleValue(0, 0);
            return c;
        }


        public override string ToString()
        {
            return CreateString(false);
        }

        /// <summary>
        /// "mm" for showing in mm, "IDXYZ" , IDtXtYtZ, else " seperator" to show
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        internal string ToString(string param, int columnSize = 0)
        {
            int precisionDigits = (TSU.Tsunami2.Preferences.Values != null) ? 6 :TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
            string s = "";
            if (columnSize == 0)
            {
                if (param.ToUpper() == "MM")
                    return CreateString(showInMm: true);
                if (param.ToUpper() == "IDXYZ")
                    return $"{this._Name} {this.X.Value} {this.Y.Value} {this.Z.Value}";

                if (param.ToUpper() == "IDTXTYTZ")
                    return $"{this._Name}\t{this.X.Value}\t{this.Y.Value}\t{this.Z.Value}";

                s += X.ToString(precisionDigits) + param;
                s += Y.ToString(precisionDigits) + param;
                s += Z.ToString(precisionDigits) + param;
            }
            else
            {
                int factor = 1;
                if (param.ToUpper() == "MM")
                {
                    factor *= 1000;
                    precisionDigits -= 3;
                }
                
                string formatString = "{0," + columnSize.ToString() + "}";
                s += string.Format(formatString, X.ToString(precisionDigits, factor) );
                s += string.Format(formatString, Y.ToString(precisionDigits, factor) );
                s += string.Format(formatString, Z.ToString(precisionDigits, factor) );
                
            }

            return s;
        }

        internal void Set()
        {
            X = new DoubleValue(); 
            Y = new DoubleValue();
            Z = new DoubleValue();
        }

        internal static ReferenceFrames GetReferenceFrameFromExistingCoordinates(List<Point> points)
        {
            var listOfExistingCoordinateSystem = new List<CoordinateSystem>();
            foreach (var cs in Tsunami2.Properties.CoordinatesSystems.AllTypes)
            {
                bool found = false;
                foreach (var point in points)
                {
                    var c = point._Coordinates.GetCoordinatesInASystemByName(cs._Name);
                    if (c != null && c.AreKnown)
                    {
                        found = true;
                        break;
                    }
                }
                if (found)
                    listOfExistingCoordinateSystem.Add(cs);
            }
            if (listOfExistingCoordinateSystem.Count != 1)
                return ReferenceFrames.Unknown;
            else
            {
                var cs = listOfExistingCoordinateSystem[0];
                return GetReferenceFramesFromCSname(cs);
            }

        }

        private static ReferenceFrames GetReferenceFramesFromCSname(CoordinateSystem cs)
        {
            var name  = cs._Name;
            switch (name)
            {
                case "SU": return ReferenceFrames.Su2000Machine;
                case "PHYS": return ReferenceFrames.Phys2000Machine;
                case "CCS-H": return ReferenceFrames.CernXYHg00Machine;
                default: return ReferenceFrames.Unknown;
            }
        }

        internal static bool IsInGrid(double xold, double yold)
        {
            // X: [-5000,6000]
            // Y: [0,15000]
            bool xInGrid = (-5000 < xold) && (xold < 6000);
            bool yInGrid = (0 < yold) && (yold < 15000);
            bool inGrid = xInGrid && yInGrid;
            return inGrid;
        }
    }
}
