﻿using System;
using System.Collections.Generic;
using TSU.ENUM;
using R = TSU.Properties.Resources;

namespace TSU.Common.Elements.Composites
{
    public class SequenceNiv : Sequence
    {
        public bool Equals(SequenceNiv other)
        {
            return _Name == other._Name;
        }

    }
    public class SequenceNivs : Sequence
    {

        internal override void ReadSequence(System.IO.StreamReader reader, CompositeElement theoriticalElements, string origin, out List<Point> missingPoints)
        {
            missingPoints = new List<Point>();
            try
            {
                string line;
                int lineCount = 1;
                int currentID = -1;
                int readID = 0;
                SequenceNiv seq = null;

                // define the way to name the sequence
                void NameTheSequence()
                {
                    seq._Name = string.Format("[NV{0}] {1} -- {2} ({3}Pts)", currentID, seq.Elements[0]._Name,
                        seq.Elements[seq.Elements.Count - 1]._Name, seq.Elements.Count);
                }

                // For each line of the file
                while ((line = reader.ReadLine()) != null)
                {
                    // Split the line
                    List<string> lineFields = new List<string>(line.Split(';'));

                    // Trim all unwanted spaces
                    for (int i = 0; i < lineFields.Count; i++) lineFields[i] = lineFields[i].Trim();

                    //skip empty line
                    if (lineFields.Count == 1 && lineFields[0] == "") continue;

                    // Check the length
                    if (lineFields.Count != 4) 
                        throw new Exception(string.Format(R.Err_FieldCount, "Nv", 4, origin, lineFields.Count, lineCount));

                    readID = int.Parse(lineFields[0]);

                    if (readID != currentID)
                    {
                        if (seq != null)
                        {
                            NameTheSequence();
                            this.Add(seq);
                        }
                        // Create sequence a new sequence
                        currentID = readID;
                        seq = new SequenceNiv() { _Origin = this._Origin, fileElementType = ElementType.SequenceNiv };
                    }
                    string name = lineFields[1] + "." + lineFields[2];
                    seq.AddFoundOrCreatedPoint(theoriticalElements, name, origin, ref missingPoints);
                    lineCount += 1;
                }

                NameTheSequence();
                this.Add(seq);
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
