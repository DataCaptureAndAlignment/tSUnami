﻿using System;
using System.Collections;
using System.Collections.Generic;
using TSU.ENUM;
using System.Linq;
using TSU;
using System.Xml.Serialization;
using R = TSU.Properties.Resources;
using TSU.Common.Compute;
using TSU.Functionalities.ManagementModules;
using Accord.IO;

namespace TSU.Common.Elements.Composites
{
    [XmlInclude(typeof(FittedShape))]
    [XmlInclude(typeof(Accelerator))]
    [XmlInclude(typeof(ElementClass))]
    [XmlInclude(typeof(Magnet))]
    [XmlInclude(typeof(TheoreticalElement))]

    public class CompositeElement : Element
    {


        #region Declaring

        private Element lastAdded;
        public CompositeElement() : base()
        {
            Elements = new CloneableList<Element>();
        }
        public CompositeElement(string name) : base()
        {
            _Name = name;
            Elements = new CloneableList<Element>();
        }
        #endregion

        #region adding

        public void Add(IElement element, HowToRename renamingOption = HowToRename.FindNext, bool forceOverride = false)
        {
            bool canBeOverride = forceOverride;
            if (!(this is Sequence))
            {
                if (this.ContainsObject(element))
                {
                    canBeOverride = forceOverride;
                    if (element is Point)
                    {
                        if ((element as Point)._Coordinates == null)
                            canBeOverride = true;
                        else
                        {
                            if ((element as Point)._Coordinates.AreApproximate)
                                canBeOverride = true;
                            if (!(element as Point)._Coordinates.HasAny)
                                canBeOverride = true;
                        }
                    }
                    if (!canBeOverride)
                        throw new Exception($"'{element._Name}' {R.T320} '{this._Name}'");
                }

                if (renamingOption == HowToRename.UseSame || canBeOverride)
                {
                    if (canBeOverride)
                    {
                        TurnBadExistingPointWithDifferentRefHandle(element);
                    }
                    else
                    {
                        TurnBadExistingPointWithSameRefHandle(element);
                    }

                }
                else
                {
                    while (this.ContainsName(element))
                    {
                        bool breakWhile = false;
                        switch (renamingOption)
                        {
                            case HowToRename.AddParenthesis:
                                if (!element._Name.Contains('('))
                                    element._Name += "(0)";
                                element._Name = TsuString.FindNextName(element._Name);
                                break;
                            case HowToRename.FindNext:
                            default:
                                element._Name = TsuString.FindNextName(element._Name);
                                break;
                        }
                        if (breakWhile)
                            break;
                    }
                }
            }
            Elements.Add((Element)element);
            lastAdded = (Element)element;
        }

        private void TurnBadExistingPointWithSameRefHandle(IElement element)
        {
            List<Element> existing = Analysis.Element.FindByNameAndOrigins(this.Elements, element._Name, new List<string>() { element._Origin, R.T_CREATED_BY_USER });

            foreach (Element item in existing)
            {
                if (ReferenceEquals(item, element))
                {
                    if (item.State != States.Approximate)
                    {
                        // create a bad copy
                        Point badCopy = item.Clone() as Point;
                        badCopy.State = States.Bad;
                        this.Elements.Add(badCopy);
                    }
                }
            }
        }

        private void TurnBadExistingPointWithDifferentRefHandle(IElement element)
        {
            List<Element> existing = Analysis.Element.FindByNameAndOrigins(this.Elements, element._Name, new List<string>() { element._Origin, R.T_CREATED_BY_USER });

            foreach (Element item in existing)
            {
                if (!ReferenceEquals(item, element))
                {
                    item.State = States.Bad;
                }
            }
        }

        public void AddRange(CloneableList<Element> elements)
        {
            foreach (Element e in elements)
            {
                this.Add(e);
            }
        }

        public void AddRange(List<Element> elements)
        {
            foreach (Element e in elements)
            {
                this.Add(e);
            }
        }

        public void AddRange(CompositeElement elements)
        {
            foreach (Element e in elements)
            {
                this.Add(e);
            }
        }

        public void AddInHierarchy(Point point, HowToRename renamingOption = HowToRename.FindNext)
        {
            CompositeElement elementsDuFichier = this;
            CompositeElement temp;
            temp = GetOrCreateTheHierarchyElement(point._Accelerator, "", ElementType.Accelerator, elementsDuFichier, point._Parameters);
            temp = GetOrCreateTheHierarchyElement(point._Accelerator, point._ClassAndNumero, ElementType.Class, temp, point._Parameters);
            if (point._Point != "") //to not create a magnet for pillars
                temp = GetOrCreateTheHierarchyElement(point._Accelerator, point._ClassAndNumero, ElementType.Numero, temp, point._Parameters);
            temp.Add(point, renamingOption);
        }

        private CompositeElement GetOrCreateTheHierarchyElement(
            string accelerator,
            string classeAndNumero,
            ElementType type,
            CompositeElement parent,
            Parameters parameters
            )
        {
            string name;
            name = accelerator;
            if (classeAndNumero != "")
            {
                name += "." + classeAndNumero;
            }

            if (name == null)
                return parent;
            else
            {
                CompositeElement e = parent.GetElementByName(name) as CompositeElement;
                if (e == null) // then we create a new compositte element with the right .elementType()
                {
                    switch (type)
                    {
                        case ElementType.TheoreticalFile: e = new TheoreticalElement() { _Name = name, _Origin = name }; break;
                        case ElementType.Accelerator: e = new Accelerator() { _Name = name, _Origin = parent._Origin, _Accelerator = accelerator }; break;
                        case ElementType.Class:
                        case ElementType.Numero:
                            e = new Magnet()
                            {
                                _Name = name,
                                _Origin = parent._Origin,
                                _Accelerator = accelerator,
                                _ClassAndNumero = classeAndNumero,
                                _Parameters = parameters
                            };
                            break;
                        case ElementType.Unknown:
                        case ElementType.Point:
                        case ElementType.Cumul:
                        case ElementType.X:
                        case ElementType.Y:
                        case ElementType.Z:
                        case ElementType.H:
                        case ElementType.Length:
                        case ElementType.Tilt:
                        case ElementType.Slope:
                        case ElementType.BeamBearing:
                        case ElementType.SocketCode:
                        case ElementType.SocketType:
                        case ElementType.Date:
                        case ElementType.SerialNumber:
                        case ElementType.Comment:
                        case ElementType.CoordinateSystem:
                        case ElementType.Parameters:
                        case ElementType.File:
                        case ElementType.SequenceTH:
                        case ElementType.Line:
                        case ElementType.Plane:
                        case ElementType.Circle:
                        case ElementType.Sphere:
                        case ElementType.Cylinder:
                        case ElementType.Fit:
                        case ElementType.Bearing:
                        default: e = new CompositeElement() { _Name = name, fileElementType = type }; break;
                    }
                    parent.Add(e);
                }
                return e;
            }
        }

        #endregion

        #region removing
        public void RemoveByName(string name)
        {
            this.Elements.RemoveAll(x => x._Name == name);

        }
        public void RemoveByOrigin(string origin)
        {
            this.Elements.RemoveAll(x => x._Origin == origin);
        }
        public void Remove(Element element)
        {
            this.Elements.Remove(element);
        }
        public override void Remove()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region selecting
        public override IElement GetElementByName(string name)
        {
            IElement e;
            if (this._Name == name)
            {
                e = this;
            }
            else
            {
                e = null;
                foreach (IElement item in this.Elements)
                {
                    e = item.GetElementByName(name);
                    if (e != null)
                    {
                        return e;
                    }
                }
            }
            return e;
        }
        public Element GetLastAdded()
        {
            if (lastAdded != null)
            {
                if (lastAdded is Point)
                    return lastAdded;
                else
                    return (lastAdded as CompositeElement).GetLastAdded();
            }
            else
                return null;
        }
        public override List<Point> GetPoints()
        {
            List<Point> l = new List<Point>(); ;

            foreach (IElement item in this.Elements)
            {
                l.AddRange(item.GetPoints());
            }
            return l;
        }

        internal override List<TsuObject> GetAllElements()
        {
            List<TsuObject> l = new List<TsuObject>();
            foreach (Element item in this.Elements)
            {
                l.Add(item);
                l.AddRange(item.GetAllElements());
            }
            return l;
        }
        public override List<Point> GetAlesages()
        {
            List<Point> l = new List<Point>(); ;

            foreach (Element item in this.Elements)
            {
                l.AddRange(item.GetAlesages());
            }
            return l;
        }

        public override List<Point> GetPiliers()
        {
            List<Point> l = new List<Point>(); ;

            foreach (Element item in this.Elements)
            {
                l.AddRange(item.GetPiliers());
            }
            return l;
        }

        private static List<Magnet> existingMagnets = new List<Magnet>();

        public static List<Magnet> GetMagnets(List<Point> points)
        {
            List<Magnet> listToReturn = new List<Magnet>();

            foreach (var point in points)
            {
                if (!point._Name.ToUpper().Contains("BEAM_"))
                {
                    int lastIndex = point._Name.LastIndexOf('.');
                    if (lastIndex != -1)
                    {
                        string magnetname = point._Name.Substring(0, lastIndex);

                        var magnet = existingMagnets.Find(x => x._Name == magnetname);
                        if (magnet == null)
                        {
                            magnet = new Magnet(magnetname) { _Parameters = point._Parameters.DeepClone() };

                            magnet.Elements.Add(point);
                            existingMagnets.Add(magnet);
                            listToReturn.Add(magnet);
                        }
                        else // addition points in the exisiting magnet
                        {
                            var existingPoint = magnet.Elements.Find(x => x._Name == point._Name);
                            if (existingPoint == null)
                                magnet.Elements.Add(point);

                            if (!listToReturn.Exists(x => x._Name == magnetname))
                                listToReturn.Add(magnet);
                        }
                    }
                }
            }
            return listToReturn;
        }

        public override List<Magnet> GetMagnets()
        {
            List<Magnet> l = new List<Magnet>();
            if (this is Magnet)
            {
                l.Add(this as Magnet);
            }
            foreach (Element item in this.Elements)
            {
                if (item is CompositeElement) l.AddRange((item as CompositeElement).GetMagnets());
            }
            return l;
        }

        public override IEnumerator GetEnumerator()
        {
            if (Elements != null)
            {
                return Elements.GetEnumerator();
            }
            else
            {
                return null;
            }
        }
        public override IElement GetChild(int index)
        {
            if (Elements != null)
            {
                return Elements[index]; ;
            }
            else
            {
                return null;
            }
        }
        internal void Reverse()
        {
            this.Elements.Reverse();
        }
        #endregion

        #region testing
        public bool Equals(CompositeElement other)
        {
            return _Name == other._Name;
        }
        private bool ContainsObject(IElement element)
        {
            foreach (Element item in this.Elements)
                if (ReferenceEquals(element, item)) return true;
            return false;
        }

        internal bool ContainsInTheHierarchy(IElement element)
        {
            foreach (Element item in this.Elements)
            {
                if (ReferenceEquals(element, item)) return true;
                if (item is CompositeElement ce)
                {
                    if (ce.ContainsInTheHierarchy(element))
                        return true;
                }
            }
            return false;
        }


        private bool ContainsName(IElement element)
        {
            foreach (Element item in this.Elements)
            {
                if (item != null)
                {
                    if (item._Name == element._Name)
                        if (item.GetType() == element.GetType())
                            if (item is Point p)
                            {
                                if (p.State != States.Bad)
                                    return true;
                            }
                            else
                                return true;
                }
            }
            return false;
        }
        #endregion

        /// <summary>
        /// Replace the first point with same name in the composite element
        /// </summary>
        /// <param name="point"></param>
        internal void Replace(Point point)
        {
            if (this.ContainsName(point))
            {
                this.Elements[this.Elements.FindIndex(x => x._Name == point._Name)] = point.DeepCopy();
            }
        }

        public override string ToString()
        {
            return this._Name + " is a composite element";
        }

        internal static List<TsuObject> CreateTreeBasedOnList(List<Point> points)
        {
            List<TsuObject> listOfOrigins = new List<TsuObject>();

            foreach (Point point in points)
            {
                Common.Elements.Manager.Module.AddPointInATree(point, origin: point._Origin, hierarchyOrigins: ref listOfOrigins);
            }
            return listOfOrigins;
        }
    }
}
