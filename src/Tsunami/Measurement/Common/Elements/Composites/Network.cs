﻿using System.IO;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;

using TSU.Common.Zones;

namespace TSU.Common.Elements.Composites
{
    
    public class TheoreticalElement : CompositeElement
    {
        public class definition
        {
            [XmlAttribute]
            public Coordinates.CoordinateSystemsTypes InitialCoordinateSystems { get; set; }

            [XmlIgnore]
            public Coordinates.ReferenceFrames ToUseReferenceFrames
            {
                get
                {
                    if (LocalZone == null) return InitialReferenceFrames;

                    switch (InitialReferenceFrames)
                    {
                        case Coordinates.ReferenceFrames.MLA1985Machine:
                        case Coordinates.ReferenceFrames.LA1985:
                        case Coordinates.ReferenceFrames.Su1985Machine:
                        case Coordinates.ReferenceFrames.Phys1985Machine:
                            return Coordinates.ReferenceFrames.CernXYHg85Machine;

                        case Coordinates.ReferenceFrames.MLA2000Machine:
                        case Coordinates.ReferenceFrames.LA2000:
                        case Coordinates.ReferenceFrames.Su2000Machine:
                        case Coordinates.ReferenceFrames.Phys2000Machine:
                            return Coordinates.ReferenceFrames.CernXYHg00Machine;

                        case Coordinates.ReferenceFrames.MLASphere:
                        case Coordinates.ReferenceFrames.LASphere:
                        case Coordinates.ReferenceFrames.MLGSphere:
                        case Coordinates.ReferenceFrames.LGSphere:
                        case Coordinates.ReferenceFrames.SuSphere:
                        case Coordinates.ReferenceFrames.PhysSphere:
                            return Coordinates.ReferenceFrames.CERNXYHsSphereSPS;

                        case Coordinates.ReferenceFrames.MLG:
                        case Coordinates.ReferenceFrames.LG:
                        case Coordinates.ReferenceFrames.LAp0:
                        case Coordinates.ReferenceFrames.LGp0:
                        case Coordinates.ReferenceFrames.Unknown:
                        case Coordinates.ReferenceFrames.CERN_GRF:
                        case Coordinates.ReferenceFrames.ITRF97:
                        case Coordinates.ReferenceFrames.WGS84:
                        case Coordinates.ReferenceFrames.ROMA40:
                        case Coordinates.ReferenceFrames.ETRF93:
                        case Coordinates.ReferenceFrames.CH1903plus:
                        case Coordinates.ReferenceFrames.CGRF2:
                        case Coordinates.ReferenceFrames.CCS:
                        case Coordinates.ReferenceFrames.CernXYHe:
                        case Coordinates.ReferenceFrames.CernX0Y0He:
                        case Coordinates.ReferenceFrames.CernXYHg00:
                        case Coordinates.ReferenceFrames.CernXYHg00Topo:
                        case Coordinates.ReferenceFrames.CernXYHg00Machine:
                        case Coordinates.ReferenceFrames.CernXYHg1985:
                        case Coordinates.ReferenceFrames.CernXYHg85Machine:
                        case Coordinates.ReferenceFrames.CERNXYHsSphereSPS:
                        case Coordinates.ReferenceFrames.CGRFSphere:
                        case Coordinates.ReferenceFrames.SwissLV95:
                        case Coordinates.ReferenceFrames.SwissLV03:
                        case Coordinates.ReferenceFrames.FrenchRGF93Zone5:
                        case Coordinates.ReferenceFrames.Lambert93:
                        case Coordinates.ReferenceFrames.RGF93:
                        case Coordinates.ReferenceFrames.CHTRF95:
                        default:
                            return InitialReferenceFrames;
                    }
                }
            }

            [XmlAttribute]
            public Coordinates.CoordinateSystemsTsunamiTypes InitialTsunamiSystems { get; set; }
            [XmlAttribute]
            public Coordinates.ReferenceFrames InitialReferenceFrames { get; set; }
            public Zone LocalZone { get; set; }

            [XmlIgnore]
            public bool Validity
            {
                get
                {              
                    bool zoneOk = (this.LocalZone != null) ? this.LocalZone.Validity:false ;
                    bool csOk = (this.InitialCoordinateSystems != Coordinates.CoordinateSystemsTypes.Unknown) ? true : false;
                    bool rfOk = (this.InitialReferenceFrames != Coordinates.ReferenceFrames.Unknown) ? true : false;
                    return zoneOk && csOk && rfOk;
                }
            }
        }

        public definition Definition { get; set; }
        public string Comments { get; set; }
        
        public string PointInIdXYZFormat { get; set; }
        public string PointInGeodeFormat { get; set; }
        [XmlIgnore]
        public override bool HasChild  { get; set; }
        [XmlIgnore]
        public string networkFilePath { get; set; }
        private FileInfo _fittingOutputFile;
        [XmlIgnore]
        public FileInfo FittingOutputFile
        {
            get { return _fittingOutputFile ?? (_fittingOutputFile = new FileInfo(networkFilePath)); }
        }
        [XmlIgnore]
        public List<Point> Points
        {
            get {
                return base.GetPoints();
            }
        }
    
        public TheoreticalElement()
        {
            this.Definition = new definition();
            this.fileElementType = ENUM.ElementType.TheoreticalFile;
        }
        public void SetNetworkFilePath(string filePath)
        {
            this.networkFilePath = filePath;
        }

        public override bool Equals(object other)
        {
            if (other is Element e)
                return this._Origin == e._Origin;
            else
                return false;
        }

        public override string ToString()
        {
            return this._Name + " is a theoretical element";
        }
    }
}
