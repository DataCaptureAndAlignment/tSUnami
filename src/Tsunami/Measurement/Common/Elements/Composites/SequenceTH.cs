﻿using System;
using System.Collections.Generic;
using TSU.ENUM;
using R = TSU.Properties.Resources;

namespace TSU.Common.Elements.Composites
{
    public class SequenceTH : Sequence
    {
        public bool Equals(SequenceTH other)
        {
            return _Name == other._Name;
        }
    }
    public class SequenceTHs : Sequence
    {
        //
        // a sequence is composite element with the name of the station and with the station point in position "0" of the elements list
        //
        public override string ToString()
        {
            return $"SeqTH{base.ToString()}";
        }
        internal override void ReadSequence(System.IO.StreamReader reader, CompositeElement theoriticalElements, string origin, out List<Point> missingPoints)
        {
            missingPoints = new List<Point>();
            try
            {
                string line;
                int count = 1;

                SequenceTH seq = null;
                SequenceTH seqs = new SequenceTH();

                // define the way to name the sequence
                Action NameTheSequence = () =>
                {
                    seq._Name = string.Format("[TH{0}] {1} - {2} ({3}Pts)", count.ToString(), seq.Elements[0]._Name, seq.Elements[seq.Elements.Count - 1]._Name, seq.Elements.Count);
                };

                while ((line = reader.ReadLine()) != null)
                {
                    // Split the line
                    List<string> lineFields = new List<string>(line.Split(';'));

                    //remove empty field if existing
                    //lineFields = lineFields.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList();

                    // Trim all unwanted spaces
                    for (int i = 0; i < lineFields.Count; i++) lineFields[i] = lineFields[i].Trim();

                    // Checsk the length
                    if (lineFields.Count % 2 == 0)
                        throw new Exception(R.T324);

                    // Create sequence
                    seq = new SequenceTH() { _Origin = this._Origin, fileElementType = ElementType.SequenceTH };

                    for (int i = 0; i < lineFields.Count - 1; i = i + 2)
                    {
                        string name = lineFields[i] + "." + lineFields[i + 1];
                        List<Point> tempMissingPoints = new List<Point>();
                        seq.AddFoundOrCreatedPoint(theoriticalElements, name, origin, ref tempMissingPoints);
                        missingPoints.AddRange(tempMissingPoints);
                    }
                    NameTheSequence();
                    count += 1;
                    seqs.Add(seq);
                }

                // if not cancel add all read sequences
                this.AddRange(seqs.Elements);
            }
            catch (Exception ex)
            {
                throw new Exception($"Could not read or threat sequence file : {ex.Message}");
            }
        }
    }
}
