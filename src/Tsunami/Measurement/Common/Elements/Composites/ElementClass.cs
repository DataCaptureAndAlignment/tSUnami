﻿
namespace TSU.Common.Elements.Composites
{
    public class ElementClass : CompositeElement
    {
        public ElementClass()
            : base()
        {
            this.fileElementType = ENUM.ElementType.Class;
        }
        public ElementClass(string name)
            : base(name)
        {
            this.fileElementType = ENUM.ElementType.Class;
        }
    }
}
