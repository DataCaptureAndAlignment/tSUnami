﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace TSU.Common.Elements.Composites
{
    public class FittedShape : CompositeElement
    {
        public ParametricShape parametricShape { get; set; }

        public string outputFilePath { get; set; }
        private FileInfo _fittingOutputFile;
        [XmlIgnore]
        public FileInfo FittingOutputFile
        {
            get
            {
                if (_fittingOutputFile != null)
                    return _fittingOutputFile;
                else
                {
                    if (outputFilePath != null)
                        return _fittingOutputFile = new FileInfo(outputFilePath);
                    else
                        return null;
                }
            }
        }

        public List<Coordinates> residuals = new List<Coordinates>();
        public List<Coordinates> projectedPoints = new List<Coordinates>();


        public FittedShape()
        {
            this.fileElementType = ENUM.ElementType.Fit;
        }
        public FittedShape(ParametricShape shape, CompositeElement toBeFit, string outputFilePath)
        {
            // masturbation for a good name
            
            this.parametricShape = shape;
            //this.Add(shape);
            this.AddRange(toBeFit);
            this.outputFilePath = outputFilePath;
            this._Name = FindAName(shape, toBeFit);
            this.fileElementType = ENUM.ElementType.Fit;
        }

        public string FindAName(ParametricShape shape, CompositeElement pointsFitted)
        {
            bool allFromSameGroup = true;
            string groupOfPoint1 = pointsFitted.Elements[0]._Origin;
            foreach (Point item in pointsFitted)
                if (item._Origin != groupOfPoint1) allFromSameGroup = false;
            string name = shape._Name + " from ";
            bool isFirst = true;
            foreach (Point item in pointsFitted)
            {
                if (isFirst)
                    isFirst = false;
                else
                    name += ", ";
                name += item._Name;
                if (!allFromSameGroup) name += " (" + item._Origin + ")";
            }
            if (allFromSameGroup) name += " (" + pointsFitted.Elements[0]._Origin + ")";
            return name;
        }

        public override List<Point> GetPoints()
        {
            return base.GetPoints();
        }
    }
}
