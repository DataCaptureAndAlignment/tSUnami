﻿
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TSU.Common.Elements.Composites
{

    public class Magnet : CompositeElement
    {

        public virtual Parameters _Parameters { get; set; }

        public float? MagneticLength { get; set; }
        public float? HalfDifferenceBetweenTheoriticalAndRealLength { get; set; }
        public string LhcMAgnetName { get; set; }
        public float? MagneticLengthFromPrevious { get; set; }
        public Magnet()
            : base()
        {
            this.fileElementType = ENUM.ElementType.Numero;

        }
        public Magnet(string name)
            : base(name)
        {
            this.fileElementType = ENUM.ElementType.Numero;
        }

        internal static bool GetReferenceAlesageForTilt(out Point entryPoint, List<Point> allMagnetPoints)
        {
            // choisi le pt E comme tilt ref par défaut
            entryPoint = allMagnetPoints.Find(x => x._Point == "E" && x._Origin.Contains(".dat"));
            if (entryPoint != null)
            {
                return true;
            }
            else
            {
                ///Dans le cas ou un aimant n'a pas d'entrée, ajoute le premier point de l'aimant
                if (allMagnetPoints.Count > 0)
                {
                    // pointsSelected.Add(allMagnetPoints[0]); // should be ok, but to make sure:
                    entryPoint = allMagnetPoints.OrderBy(x => x._Parameters.Cumul).First();
                    if (entryPoint != null)
                        return true;
                }
            }
            return false;
        }
    }

    public static class MagnetExtensions
    {
        /// <summary>
        /// retrieve magnets from ths tsunami elements based on the names of the point inside a measure
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="src"></param>
        /// <param name="measures"></param>
        /// <returns></returns>
        public static List<Magnet> GetMagnetFrom<T>(this T src) where T : List<Measures.Measure>
        {
            List<Magnet> magnets = new List<Magnet>();
            foreach (var measure in src)
            {
                string measuredMagnetName = measure._Point._Name.Substring(0, measure._Point._Name.LastIndexOf('.'));
                if (measuredMagnetName.StartsWith("BEAM_"))
                    measuredMagnetName = measuredMagnetName.Substring(5);
                foreach (var item in Tsunami2.Properties.Elements)
                {
                    if (item is CompositeElement ce)
                    {
                        foreach (var magnet in ce.GetMagnets())
                        {
                            if (magnet._Name == measuredMagnetName)
                                if (!magnets.Contains(magnet))
                                    magnets.Add(magnet);
                        }
                    }
                }

            }
            return magnets;
        }

        public static List<Magnet> GetMagnetBeingAlignedFrom<T>(this T src) where T : List<Measures.Measure>
        {
            List<Magnet> magnets = new List<Magnet>();
            foreach (var measure in src)
            {
                if (measure.GeodeRole == IO.SUSoft.Geode.Roles.Poin)
                {
                    string measuredMagnetName = measure._Point._Name.Substring(0, measure._Point._Name.LastIndexOf('.'));
                    foreach (var item in Tsunami2.Properties.Elements)
                    {
                        if (item is CompositeElement ce)
                        {
                            foreach (var magnet in ce.GetMagnets())
                            {
                                if (magnet._Name == measuredMagnetName)
                                    magnets.Add(magnet);
                            }
                        }
                    }
                }

            }
            return magnets;
        }
    }
}
