﻿
namespace TSU.Common.Elements.Composites
{
    public class Accelerator : CompositeElement
    {
        public Accelerator()
            : base()
        {
            this.fileElementType = ENUM.ElementType.Accelerator;
        }

        public Accelerator(string name)
            : base(name)
        {
            this.fileElementType = ENUM.ElementType.Accelerator;
        }
    }
}
