﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using TSU.ENUM;
using R = TSU.Properties.Resources;

namespace TSU.Common.Elements.Composites
{
    [XmlInclude(typeof(SequenceTH))]
    [XmlInclude(typeof(SequenceNiv))]
    [XmlInclude(typeof(SequenceFil))]
    [XmlInclude(typeof(SequenceFils))]
    [XmlInclude(typeof(SequenceNivs))]
    [XmlInclude(typeof(SequenceTHs))]
    public class Sequence : CompositeElement
    {
        internal override List<TsuObject> GetAllElements()
        {
            List<TsuObject> l = new List<TsuObject>();
            foreach (Element item in this.Elements)
            {
                l.Add(item);
                // l.AddRange(item.GetAllElements());
            }
            return l;
        }

        public virtual void FillFromFile(string fullFileName, CompositeElement theoriticalElements, out List<Point> missingPoints)
        {
            this._Name = fullFileName;
            this._Origin = fullFileName;
            this._OriginType = ENUM.ElementType.SequenceFile;
            this.fileElementType = ENUM.ElementType.SequenceFile;
            this.FillFromFullPath(fullFileName, theoriticalElements, out missingPoints);
        }

        private void FillFromFullPath(string fullFileName, CompositeElement theoriticalElements, out List<Point> missingPoints)
        {
            System.IO.StreamReader reader = null;
            try
            {
                string extension = Path.GetExtension(fullFileName);
                if (Path.GetExtension(fullFileName).ToUpper() == ".dat".ToUpper())
                {
                    reader = new System.IO.StreamReader(fullFileName);
                    this.ReadSequence(reader, theoriticalElements, fullFileName, out missingPoints);
                    reader.Close();

                    reader.Dispose();
                }
                else throw new Exception(R.T321);
            }
            catch (Exception ex)
            {
                if (reader != null)
                {
                    reader.Dispose();
                    reader.Close();
                }

                throw new Exception(string.Format(R.T322, ex), ex);
            }
        }


        internal virtual void ReadSequence(System.IO.StreamReader reader, CompositeElement theoriticalElements, string origin, out List<Point> missingPoints)
        {
            throw new NotImplementedException(); // must be override in the right sequence class
        }

        /// <summary>
        /// Seak in a theorietical list of point by name, create a new point if not found, than add the point to sequence element list
        /// </summary>
        /// <param name="theoriticalElements"></param>
        /// <param name="pointNameSeekedFor"></param>
        /// <param name="origin"></param>
        internal void AddFoundOrCreatedPoint(CompositeElement theoriticalElements, string pointNameSeekedFor, string origin, ref List<Point> missingPoints)
        {
            IElement e = null;
            if (theoriticalElements != null)
                e = theoriticalElements.GetElementByName(pointNameSeekedFor);
            if (e != null)
                this.Add(e);
            else
            {
                Point p = new Point()
                {
                    _Name = pointNameSeekedFor,
                    _Origin = origin,
                    fileElementType = ElementType.Point,
                    _OriginType = ElementType.SequenceFile
                };
                missingPoints.Add(p);
                this.Add(p);
            }
        }
    }
}
