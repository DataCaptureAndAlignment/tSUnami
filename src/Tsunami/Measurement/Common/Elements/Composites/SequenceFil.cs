﻿using System;
using System.Collections.Generic;
using TSU.ENUM;
using R = TSU.Properties.Resources;

namespace TSU.Common.Elements.Composites
{
    public class SequenceFil : Sequence
    {
        public bool Equals(SequenceFil other)
        {
            return _Name == other._Name;
        }
    }
    public class SequenceFils : Sequence
    {

        /// <summary>
        /// Creates sub sequence object with the first and last element being the 'ancrages'
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="theoriticalElements"></param>
        /// <param name="origin"></param>
        internal override void ReadSequence(System.IO.StreamReader reader, CompositeElement theoriticalElements, string origin, out List<Point> missingPoints)
        {
            string line;
            int seqCount = 1;
            int lineCount = 1;
            string currentAncrages = "";
            string readAncrages = "";
            string previousSecondAncrage = "";
            SequenceFil seq = null;
            string name = "";

            // define the way to name the sequence
            Action NameTheSequence = () =>
            {
                seq._Name = string.Format("[FL{0}] {1} -- {2} ({3}Pts)", seqCount, seq.Elements[0]._Name, seq.Elements[seq.Elements.Count - 1]._Name, seq.Elements.Count);
            };
            missingPoints = new List<Point>();
            // For each line of the file
            while ((line = reader.ReadLine()) != null)
            {
                // Split the line
                List<string> lineFields = new List<string>(line.Split(';'));

                // Trim all unwanted spaces
                for (int i = 0; i < lineFields.Count; i++) lineFields[i] = lineFields[i].Trim();

                // skip empty line
                if (lineFields.Count == 1) if (lineFields[0] == "") continue;

                // Check the length
                if (lineFields.Count != 7) throw new Exception(string.Format(R.Err_FieldCount, "FL", 7, origin, lineFields.Count, lineCount));

                readAncrages = lineFields[0] + "_" + lineFields[1] + "-" + lineFields[2] + "_" + lineFields[3];

                if (readAncrages != currentAncrages)
                {
                    if (seq != null)
                    {
                        name = previousSecondAncrage;
                        seq.AddFoundOrCreatedPoint(theoriticalElements, name, origin, ref missingPoints);
                        NameTheSequence();
                        this.Add(seq);
                    }
                    // Create sequence a new sequence
                    currentAncrages = readAncrages;
                    seq = new SequenceFil() { _Origin = this._Origin, fileElementType = ElementType.SequenceFil };
                    name = lineFields[0] + "." + lineFields[1];
                    seq.AddFoundOrCreatedPoint(theoriticalElements, name, origin, ref missingPoints);
                    seqCount += 1;
                }
                name = lineFields[4] + "." + lineFields[5];
                seq.AddFoundOrCreatedPoint(theoriticalElements, name, origin, ref missingPoints);
                previousSecondAncrage = lineFields[2] + "." + lineFields[3];
                lineCount += 1;
            }
            // add the last ancrage of the last sequence
            seq.AddFoundOrCreatedPoint(theoriticalElements, previousSecondAncrage, origin, ref missingPoints);

            NameTheSequence();
            this.Add(seq);
        }
    }
}
