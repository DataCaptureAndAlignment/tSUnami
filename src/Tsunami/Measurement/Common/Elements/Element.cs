using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using R = TSU.Properties.Resources;
using System.Xml.Serialization;
using TSU.ENUM;
using TSU;
using TSU.Common.Elements.Composites;

namespace TSU.Common.Elements
{
    // The Element entity is implementing the Design Pattern called "COMPOSITE"

    // Interface Element 
    public interface IElement : ITsuObject, IEnumerator, ICloneable//, IExportable //, IEnumerable remve to use serizalisation
	{
        string _Origin { get; set; }
        ENUM.ElementType fileElementType { get; set; }

        bool IsLeaf();
        //void Add(IElement element);
        //void AddFromTheoriticalFile(string fileName);
        //void Remove();
        IElement GetElementByName(string key);
        IElement GetChild();
        IElement GetChild(int index);
        List<Point> GetPoints();
        List<Magnet> GetMagnets();
    } 

    // Abstract implementation available for inheriters, but not directly instanciable class

    [XmlInclude(typeof(PrimitiveElement))]  //so that the serializer will known what type to expect
    [XmlInclude(typeof(CompositeElement))]
    
    [Serializable()]
    public abstract class Element : TsuObject,  IElement 
    {

        public enum States
        {
            Good,Bad,Questionnable,ModifiedByUser, Approximate, FromStation
        }

        public enum HowToRename
        {
            UseSame, FindNext, AddParenthesis
        }

        [XmlAttribute]
        public States State { get; set; } = States.Good;

        [XmlAttribute]
        public virtual string _Origin { get; set; }
        [XmlAttribute]
        public virtual string _Accelerator { get; set; }
        [XmlAttribute]
        public virtual string _Zone { get; set; }
        [XmlAttribute]
        public virtual string _Class
        {
            get
            {
                if (_ClassAndNumero != null)
                {
                    int last = _ClassAndNumero.LastIndexOf('.');
                    if (last == -1) last = _ClassAndNumero.Length;
                    return _ClassAndNumero.Substring(0, last);
                }
                else
                {
                    return R.String_Unknown;
                }
            }
        }

         [XmlAttribute]
        public virtual string _Numero
        {
            get
            {
                if (_ClassAndNumero != null)
                {
                    int last = _ClassAndNumero.LastIndexOf('.');
                    //if (last == -1) last = 0;
                    return _ClassAndNumero.Substring(last+1);
                }
                else
                {
                return R.String_Unknown;
            }
        }
        }
        
        [XmlAttribute]
        public virtual string _ClassAndNumero { get; set; }


        [XmlAttribute]
        public virtual ENUM.ElementType _OriginType { get; set; }

        

        [XmlAttribute]
        public virtual ElementType fileElementType { get; set; }

        [XmlArray("SubElements")]
        [XmlArrayItem("Element")]
        public virtual CloneableList<Element> Elements { get; set; }

        private bool hasChild;
        [XmlAttribute]
        public virtual bool HasChild
        {
            get
            {
                hasChild = ((Elements != null) && (Elements.Count > 0)) ? true : false;
                return hasChild;
            }
            set
            {
                
            }
        }
        // Methods
        public virtual IEnumerator GetEnumerator() 
        {
            return this;
        }
        public Element():base()
        {
            this.Initialize();
        }
        public Element(string name): base(name)
        {
            this.Initialize();
            
        }
        public virtual void Initialize()
        {
            Guid = Guid.NewGuid();
            _Origin =R.String_Unknown;
            //_Accelerator =R.T003;
            //_Zone =R.T003;
            //_Class =R.T003;
            //_Numero =R.T003;
            //_Point =R.T003;
            Elements = new CloneableList<Element>();
        }
        
        // IEquitable
        public bool Equals(Element other)
        {
            bool TSU = (this as TsuObject).Equals(other);
            bool ori = this._Origin == other._Origin;

            return TSU && ori;
        }
        public override bool Equals(object other)
        {
            if (other is Element)
                return this.Equals((Element)other);
            else
                return false;
        }
        //public override int GetHashCode()
        //{
        //    return
        //        base.GetHashCode();
        //        //_Origin.GetHashCode() ^ 
        //        //_Zone.GetHashCode() ^
        //        //_Class.GetHashCode() ^
        //        //_Numero.GetHashCode() ^
        //        //_Point.GetHashCode();
        //}

        // IClonable
        public static T DeepCopy<T>(T obj)
        {
        using (MemoryStream stream = new MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, obj);
                stream.Position = 0;
 
                return (T)formatter.Deserialize(stream);
            }
        }
        public override object Clone()
        {
            Element newAbstractElement = (Element)this.MemberwiseClone(); // to make a clone of the primitive types fields
            newAbstractElement.Elements = (this.Elements != null) ? (CloneableList<Element>)this.Elements.Clone() : null;
            return newAbstractElement;
        }
        // Hierarchy
        public bool IsLeaf()
        {
            return !HasChild;
        }
        //public virtual void Add(object o)
        //{

        //}
        //public virtual void Add(IElement element) 
        //{

        //}
        public virtual void AddInHierarchy(IElement element) 
        { 
        
        }
        public virtual void AddFromTheoriticalFile(string fileName) 
        { 

        }
        public virtual IElement GetChild()
        {
            return null;
        }
        public virtual IElement GetChild(int index)
        {
            return null;
        }
        internal virtual List<TsuObject> GetAllElements()
        {
            return new List<TsuObject>();
        }
        public virtual List<Point> GetPoints() 
        { 
            return null;
        }
        public virtual List<Magnet> GetMagnets()
        {
            return null;
        }
        public virtual List<Point> GetAlesages()
        {
            return null;
        }
        public virtual List<Point> GetPiliers()
        {
            return null;
        }
        public virtual List<Point> GetPointsATracer()
        {
            return null;
        }
        public virtual void Remove()
        {
            
        }
        public virtual void RemoveElementOfKey(string name)
        {
            this.Elements.Remove(this.Elements.Find(x => x._Name == name));
        }
        public virtual void RemoveChildAt(int index)
        {
            this.Elements.RemoveAt(index);
        }
        
        protected string GetFieldFromEndOfArray(string[] array, int indexFromEnd)
        {
            int index = (array.Length - 1) - indexFromEnd;
            if (array[index] != null)
            {
                return array[index].Trim();
            }
            else
            {
                return R.String_Unknown;
            }
        }
        //public virtual XmlElement WriteForTsunami(XmlDocument xmlDocument)
        //{
        //    XmlElement e = xmlDocument.CreateElement("Elements");
        //    e.SetAttribute("Name", _Name.ToString());
        //    e.SetAttribute("Accelerator", _Accelerator.ToString());
        //    e.SetAttribute("Management.Zone", _Zone.ToString());
        //    e.SetAttribute("Class", _Class.ToString());
        //    e.SetAttribute("Numero", _Numero.ToString());
        //    e.SetAttribute("sPoint", _Point.ToString());
        //    return e;
        //}
        // Inumerator
        private object current;
        public object Current
        {
            set
            {
                current = value;
            }
            get
            {
                return current;
            }
        }
        public bool MoveNext()
        {
            return false;
        }
        public void Reset()
        {
            throw new NotImplementedException();
        }
        public virtual IElement GetElementByName(string key)
        {
            IElement e;
            if (this._Name == key)
            {
                e = this;
            }
            else
            {
                e = null;
                if (this.GetChild() != null)
                {
                    foreach (IElement item in this.Elements)
                    {
                        e = GetElementByName(key);
                    }
                }
                
            }
            return e;
        }

        
        public List<Element> GetAllSubElements()
        {
            List<Element> l = new List<Element>();
            foreach (Element item in this.Elements)
            {
                l.AddRange(item.GetAllSubElements());
            }
            return l;
        }
    }
}
