﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using System.ComponentModel;
using System.Xml.Serialization;
using TSU.Common.Elements.Composites;
using TSU.Views;
using TSU.Views.Message;
using R = TSU.Properties.Resources;
using T = TSU.Tools;
using V = TSU.Common.Strategies.Views;
using Z = TSU.Common.Zones;

namespace TSU.Common.Elements.Manager
{
    public partial class View : ManagerView
    {
        # region Fields and props
        private new Module Module
        {
            get
            {
                return base.Module as Module;
            }
            set
            {
                base.Module = value;
            }
        }

        public bool flagProposeFileAtUpdate = true;

        public EM_Buttons buttons;

        #endregion

        #region Constructor

        /// <summary>
        /// Create the view, link it to the Module and should suscribed the view to the module
        /// </summary>
        /// <param name="module"></param>
        public View(IModule module)
            : base(module) // Taking form already in controllerModuleView
        {
            this.SetModule(module as Module);
            this.Image = R.Element_Aimant;

            InitializeMenu();
            InitializeBackgroundWorker();
            this.AutoCheckChildren = true;

            this.currentStrategy.Update();
            this.currentStrategy.Show();
            this.AdaptTopPanelToButtonHeight();
            //this.Activated += delegate
            //{
            //    if (this.currentStrategy is ViewStrategy.List)
            //};
        }



        #endregion

        #region updates

        public override void UpdateView()
        {
            // seems sometime a null CS is appearing
            CleanTheSelectedCsToShowFromNullEntries();

            base.UpdateView();
            if (flagProposeFileAtUpdate)
                if (this.Module.AllElements.Count == 0)
                    this.Module.ProposeFile();
        }

        private void CleanTheSelectedCsToShowFromNullEntries()
        {
            var list = Tsunami2.Properties.CoordinatesSystems.SelectedTypes;
            int index = list.Count - 1;
            while (index >= 0)
            {
                if (list[index] == null)
                    list.RemoveAt(index);
                index--;
            }
        }

        public override List<string> ListviewColumnHeaderTexts
        {

            get
            {
                string refSurf = "ccs";
                if (this.Module.zone != null)
                {
                    if (this.Module.zone.Ccs2MlaInfo != null)
                    {
                        refSurf = this.Module.zone.Ccs2MlaInfo.ReferenceSurface.ToString();
                    }
                }
                List<string> list = new List<string>()
                {
                    "Name",  //00
                    "Pos.", //01
                    "Type",     //02
                };

                if (TSU.Tsunami2.Preferences.Values.GuiPrefs.HideMachineParameters.IsFalse)
                    list.Add("Cumul[m]");

                list.Add("Group");

                if (TSU.Tsunami2.Preferences.Values.GuiPrefs.HideMachineParameters.IsFalse)
                {
                    list.AddRange(new List<string>() {
                        "TH/NP",        //04
                        "Acc_Zone",     //05
                        "Class",        //06
                        "Numero",       //07
                        "Element",      //08
                    });
                }
                var cs = TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems;
                bool showAll = cs == null ? true : cs.SelectedTypes == null ? true : cs.SelectedTypes.Count == 0;
                var toShow = showAll ? cs.AllTypes : cs.SelectedTypes;
                if (toShow != null)
                {
                    foreach (var item in toShow)
                    {
                        if (item != null)
                            foreach (string name in item.AxisNames)
                            {
                                list.Add(name + "[m]");
                            }

                    }
                }

                if (TSU.Tsunami2.Preferences.Values.GuiPrefs.HideMachineParameters.IsFalse)
                {
                    list.AddRange(new List<string>()
                    {
                        "L",            //28
                        "Gis[gon]",     //29
                        "Slope",        //30
                        "T",            //31
                        "Tilt",         //32
                        "Code",         //33
                    });
                }
                list.AddRange(new List<string>()
                {
                    "State",        //34
                    "Date Time",         //36
                    "Comment",       //37
                });
                return list;
            }
        }

        #endregion

        #region Menus
        internal void ShowComputeMenu()
        {
            List<Control> cm = new List<Control>
            {
                buttons.chaba as BigButton,
                buttons.shapes as BigButton,
                buttons.compare as BigButton,
                buttons.Intersections as BigButton,
                buttons.csGeo as BigButton,
                buttons.newFrameCreation as BigButton,
            };
            this.ShowPopUpSubMenu(cm, "compute ");
        }

        internal void ShowShapesMenu()
        {
            List<Control> cm = new List<Control>
            {
                buttons.ShapesFitting as BigButton,
                buttons.ShapesExport as BigButton,
                buttons.SPSImport as BigButton
            };
            this.ShowPopUpSubSubMenu(cm, "shapes ");
        }

        internal void ShowExportShapesMenu()
        {
            List<Control> cm = new List<Control>
            {
                buttons.SPSExport as BigButton,
                buttons.SPSExportLine as BigButton,
                buttons.SPSExportPlane as BigButton,
                buttons.SPSExportCircle as BigButton,
                buttons.SPSExportSphere as BigButton
            };
            this.ShowPopUpSubSubSubMenu(cm, "shapes ");
        }

        private void ShowExportMenu()
        {
            List<Control> cm = new List<Control>
            {
                buttons.exportToText as BigButton,
                buttons.exportExcel as BigButton,
                buttons.exportXml as BigButton,
            };
            this.ShowPopUpSubMenu(cm, "export ");
        }

        private void ShowCSMenu()
        {
            Polar.GuidedModules.Steps.StakeOut.CoordinateButtons.SetAction(UpdateCSToShow);
            List<Control> cm = new List<Control>();
            foreach (Control control in Polar.GuidedModules.Steps.StakeOut.CoordinateButtons.GetCSTypeButtons(null, true))
            {
                cm.Add(control);
            }
            this.ShowPopUpSubMenu(cm, "CS ");
        }

        internal void ShowNFCMenu()
        {
            List<Control> cm = new List<Control>
            {
                //buttons.nfcCreate as BigButton,
                buttons.nfcOpen as BigButton,
                buttons.nfcSelect as BigButton,
            };
            this.ShowPopUpSubSubMenu(cm, "NFC ");
        }

        private void ShowTypesMenu()
        {
            List<Control> cm = new List<Control>
            {
                buttons.ShowPoints as BigButton,
                buttons.ShowAll as BigButton,
                buttons.ShowMagnets as BigButton,
                buttons.ShowPiliers as BigButton,
                buttons.ShowAlesages as BigButton,
                buttons.ShowHideASG as BigButton,
            };
            this.ShowPopUpSubMenu(cm, "types ");
        }

        internal override void InitializeMenu()
        {
            //Creation of buttons
            buttons = new EM_Buttons(this);

            base.InitializeMenu();
            //Main Button
            bigbuttonTop.ChangeNameAndDescription(R.MM_Element_TD);
            bigbuttonTop.ChangeImage(R.Element_Aimant);


            this.SetContextMenuToGlobal();
        }

        internal override void InitializeStrategy()
        {
            this.AvailableViewStrategies = new List<V.Types>() { V.Types.Tree, V.Types.List };
            this.SetStrategy(TSU.Tsunami2.Preferences.Values.GuiPrefs.ElementViewType);
        }

        internal override void SavePreference(V.Types viewStrategyType)
        {
            TSU.Tsunami2.Preferences.Values.GuiPrefs.ElementViewType = viewStrategyType;
            base.SavePreference(viewStrategyType);
        }

        public enum ElementShownInListTypes
        {
            Elements,
            Points,
            Alesages,
            Magnets,
            Sequences,
            SequencesFil,
            SequencesTH,
            SequencesNiv,
            TheoreticalPoints,
            Pilier,
            UniquePoints,
            Shapes,
            Unknown
        }

        public ElementShownInListTypes ElementShownInListType = ElementShownInListTypes.Unknown;

        private void SetContextMenuToGlobal()
        {
            contextButtons.Clear();
            contextButtons.Add(buttons.importRefenceFile);
            contextButtons.Add(buttons.importNominalFile);

            contextButtons.Add(buttons.sequence);
            contextButtons.Add(buttons.exports);
            contextButtons.Add(buttons.add);
            contextButtons.Add(buttons.removeTree);
            //contextButtons.Add(buttons.ShowZone);
            contextButtons.Add(buttons.ClipBoard);
            contextButtons.Add(buttons.modify);
            contextButtons.Add(buttons.compute);
        }
        internal override void ShowContextMenuGlobal()
        {
            this.SetContextMenuToGlobal();
            this.ShowPopUpMenu(contextButtons);
        }

        public override List<Control> GetViewOptionButtons()
        {
            return new List<Control>()
            {
                buttons.ShowTypes,
                buttons.ShowCSTypes
            };
        }

        public class EM_Buttons
        {
            private View _view;
            public List<BigButton> all;

            public BigButton importRefenceFile;
            public BigButton importNominalFile;

            public BigButton sequence;
            public BigButton exports;
            public BigButton exportExcel;
            public BigButton exportCcs;
            public BigButton exportCcsNP;
            public BigButton exportCcsIdXYH;
            public BigButton exportToText;
            public BigButton exportPhys;
            public BigButton exportXml;

            public BigButton removeTree;
            public BigButton clean;
            public BigButton remove;


            public BigButton add;
            public BigButton modify;
            public BigButton compare;
            public BigButton chaba;

            public BigButton newFrameCreation;
            public BigButton nfcCreate;
            public BigButton nfcOpen;
            public BigButton nfcSelect;

            public BigButton csGeo;
            public BigButton recomputeCoordinates;


            public BigButton shapes;
            public BigButton Intersections;
            public BigButton GeoFit; 
            public BigButton Line;
            public BigButton Plane;
            public BigButton Circle;
            public BigButton Sphere;
            public BigButton Cylinder;

            public BigButton ClipBoard;
            //public BigButton ShowZone;
            public BigButton ShowPoints;
            public BigButton ShowAll;
            public BigButton ShowMagnets;
            public BigButton ShowAlesages;
            public BigButton ShowPiliers;

            public BigButton ShowHideASG;

            public BigButton Expend;
            public BigButton compute;
            public BigButton ShowTypes;
            public BigButton ShowCSTypes;
            public BigButton ShapesFitting;
            public BigButton ShapesExport;
            public BigButton SPSExport;
            public BigButton SPSImport;
            public BigButton SPSExportLine;
            public BigButton SPSExportPlane;
            public BigButton SPSExportCircle;
            public BigButton SPSExportSphere;

            public EM_Buttons(View view)
            {
                _view = view;
                all = new List<BigButton>();
                importRefenceFile = new BigButton(R.T_IMPORT_REFERENCE_FILE, R.Open, () => { _view.Open(Point.Types.Reference); }); all.Add(importRefenceFile);
                importNominalFile = new BigButton(R.T_IMPORT_NOMINAL_FILE, R.Open, () => { _view.Open(Point.Types.Nominal); }); all.Add(importNominalFile);

                sequence = new BigButton(R.T_SEQ_IMPORT_FILE, R.Open, _view.SelectSequenceFile); all.Add(sequence);

                exports = new BigButton($"{R.T_Export};{R.T_EXPORT_ELEMENT}", R.Export, _view.ShowExportMenu, color: null, hasSubButtons: true); all.Add(exports);
                exportExcel = new BigButton(R.T_B_Export2Csv, R.Excel, _view.Module.ExportCsv); all.Add(exportExcel);
                exportToText = new BigButton(R.T_B_ExportToText, R.Element_File, () => { _view.Module.ExportToText(); }); all.Add(exportToText);

                //exportPhys = new BigButton(R.T_B_ExportPhys, R.CS_Phys, _view.Module.ExportPhys); all.Add(exportPhys);
                //exportCcs = new BigButton(R.T_B_ExportCcs, R.CS_CCS, _view.Module.ExportCcs); all.Add(exportCcs);
                //exportCcsNP = new BigButton($"{R.T_B_T_ExportCcsNP};{R.T_B_D_ExportCcsNP}", R.CS_CCS, _view.Module.ExportCcsNP); all.Add(exportCcsNP);
                //exportCcsIdXYH = new BigButton(R.T_B_ExportCcsIxyz, R.CS_CCS, _view.Module.ExportCcsIXYZH); all.Add(exportCcsIdXYH);
                exportXml = new BigButton(R.T_B_ExportXml, R.Element_File, _view.Module.ExportXml); all.Add(exportXml);

                removeTree = new BigButton($"{R.T_REMOVE};{R.T_REMOVE} ", R.Remove, () =>
                {
                    List<Control> cm = new List<Control>
                    {
                        clean,
                        remove
                    };
                    _view.ShowPopUpSubMenu(cm, "removal ");
                }, color: null, hasSubButtons: true); all.Add(removeTree);
                clean = new BigButton($"{R.T_CLEAN};{R.T_CLEAN_DETAILS}", R.Remove, _view.Module.CleanPoints); all.Add(clean);
                remove = new BigButton($"{R.T_REMOVE} {R.T_THEORETICAL_FILES_POINTS};{R.T_IT_WILL_REMOVE_THE_FULL_THEORETICAL_FILE_DEFINED_BY_THE_SELECTED_POINTS}",
                    R.Remove, _view.Module.RemoveTheoreticalFile); all.Add(remove);



                add = new BigButton(R.T_ADD_NEW_POINT_TO_LIST, R.Add, _view.AddNewPoint); all.Add(add);
                ClipBoard = new BigButton(R.T_IMPORT_CLIPBOARD_POINT, R.Add, _view.ImportFromClipBoard); all.Add(ClipBoard);

                modify = new BigButton(R.T286, R.Edit, _view.Edit); all.Add(modify);

                compare = new BigButton(R.T287, R.Instruments, _view.Compare); all.Add(compare);

                compute = new BigButton(R.T288, R.Compute, _view.ShowComputeMenu, color: null, hasSubButtons: true); all.Add(compute);
                //GeoFit = new BigButton(R.T_GEOFIT, R.GeoFit, _view.OpenFittingMenu); all.Add(GeoFit);
                Intersections = new BigButton($"{R.T_INTERSECTIONS};{R.T_TRY_TO_MIX_OBSERVATION_FROM_SEVERAL_STATIONS_TO_COMPUTE_POINTS_WITH_MISSING_COORDINATES}", R.GeoFit, ()=> { _view.Module.Intersect(); }); all.Add(Intersections);

                chaba = new BigButton(R.T_Chaba, R.Chaba, _view.OpenChabaWithSelectedPoint); all.Add(chaba);
                csGeo = new BigButton(R.T290, R.Csgeo, _view.OpenCsgeoWithSelectedPoint); all.Add(csGeo);

                shapes = new BigButton(R.T_Shapes, R.Shapes, _view.ShowShapesMenu, color: null, hasSubButtons: true); all.Add(shapes);

                ShapesFitting = new BigButton(R.T_ShapesMacro, R.Shapes, () => { view.Module.SHAPES_Launch(); }); all.Add(ShapesFitting);
                ShapesExport = new BigButton("Shapes Export", R.Shapes, _view.ShowExportShapesMenu, color: null, hasSubButtons: true); all.Add(ShapesExport);
                SPSExport = new BigButton("Export to Shapes", R.Shapes, () => { view.Module.ExportToShapes(); }); all.Add(SPSExport);
                SPSExportLine = new BigButton("Export to Shapes Best fit Line", R.Shapes, () => { view.Module.ExportToShapesLine(); }); all.Add(SPSExportLine);
                SPSExportPlane = new BigButton("Export to Shapes Best fit Plane", R.Shapes, () => { view.Module.ExportToShapesPlane(); }); all.Add(SPSExportPlane);
                SPSExportCircle = new BigButton("Export to Shapes Best fit Cicle", R.Shapes, () => { view.Module.ExportToShapesCircle(); }); all.Add(SPSExportCircle);
                SPSExportSphere = new BigButton("Export to Shapes Best fit Sphere", R.Shapes, () => { view.Module.ExportToShapesSphere(); }); all.Add(SPSExportSphere);
                SPSImport = new BigButton("Import spsOut file to tSUnami", R.Shapes, () => { view.Module.ImportShapes(); }); all.Add(SPSImport);
                //recomputeCoordinates = new BigButton($"{R.T_RECOMPUTE_POINT_COORDINATES};{R.T_RECOMPUTE_POINT_COORDINATES_DETAILS}", R.Element_Point, _view.OpenCsgeoWithSelectedPoint);//all.Add(csGeo);

                newFrameCreation = new BigButton("New Frame Creation", R.Rotation, _view.ShowNFCMenu, color: null, hasSubButtons: true); all.Add(compute);
                nfcCreate = new BigButton(R.T_NFC_CREATE, R.Rotation, _view.Module.NFC_CreateAFrame); all.Add(nfcCreate);
                nfcSelect = new BigButton("Select a Matrix File;Browse to an existing matrix file in the NFC format, it will be apply to SU if existing or to CCS coordinates", R.Open, _view.Module.NFC_SelectExistingMatrixFile); all.Add(nfcSelect);
                nfcOpen = new BigButton("Open NFC winform app", R.Tsunami_Alpha, _view.Module.NFC_Open); all.Add(nfcOpen);

                
                //Shapes = new BigButton(R.T_Shapes, R.Shapes, () => { view.Module.SHAPES_Launch(); }); all.Add(Shapes);
                Line = new BigButton(R.T292, R.Line1, _view.OpenGeoFitToFitLine); all.Add(Line);
                Plane = new BigButton(R.T293, R.Plane, _view.OpenGeoFitToFitPlane); all.Add(Plane);
                Circle = new BigButton(R.T294, R.Circle, _view.OpenGeoFitToFitCircle); all.Add(Circle);
                Sphere = new BigButton(R.T295, R.Sphere, _view.OpenGeoFitToFitSphere); all.Add(Sphere);
                Cylinder = new BigButton(R.T296, R.Cylinder, _view.OpenGeoFitToFitCylinder); all.Add(Cylinder);

                ShowTypes = new BigButton($"{R.T_SHOW_BY_TYPES};{R.T_ALLOW_YOU_TO_SHOW_ONLY_SPECIAL_TYPE_OF_ELEMENT}", R.Element_Point, _view.ShowTypesMenu, null, true);
                all.Add(ShowTypes);

                ShowCSTypes = new BigButton($"Coordinate Systems;Select the System you want to see in the element manager", R.CS_Systems, _view.ShowCSMenu, null, true);
                all.Add(ShowTypes);


                ShowAll = new BigButton($"{R.T_SHOW_ALL_ELEMENTS};{R.T_SHOW_ALL_TYPE_OF_ELEMENT_DETAILS}", R.Element_Aimant,
                    () => { view.Module.SetSelectableToAllElements(); view.Module.UpdateView(); });
                all.Add(ShowAll);

                ShowPoints = new BigButton($"{R.T_SHOW_POINTS};{R.T_SHOW_POINTS_DETAILS}", R.Element_Point,
                    () => { view.Module.SetSelectableToAllPoints(); ; view.Module.UpdateView(); });
                all.Add(ShowPoints);
                ShowMagnets = new BigButton($"{R.T_SHOW_MAGNETS};{R.T_SHOW_ONLY_MAGNETS_DETAILS}", R.Element_Aimant,
                    () => { view.Module.SetSelectableToAllMagnets(); view.Module.UpdateView(); });
                all.Add(ShowMagnets);
                ShowAlesages = new BigButton($"{R.T_SHOW_ALESAGES_};{R.T_SHOW_ONLY_ALESAGES_DETAILS}", R.Element_PointUnKnown,
                    () => { view.Module.SetSelectableListToAllAlesages(); view.Module.UpdateView(); });
                all.Add(ShowAlesages);
                ShowPiliers = new BigButton($"{R.T_SHOW_PILIERS_};{R.T_SHOW_ONLY_PILIERS_DETAILS}", R.Element_PointKnown,
                   () => { view.Module.SetSelectableToAllPilier(); view.Module.UpdateView(); });
                all.Add(ShowPiliers);
                ShowHideASG = new BigButton($"{"Show/hide machine infos"};{"Concerns 'Divided names', 'code', 'magnet params', etc}"}", R.Element_Aimant,
                   () => { view.ShowHideMachineInfo(); view.Module.UpdateView(); });
                all.Add(ShowHideASG);
            }

            public void ReleaseAll()
            {
                foreach (BigButton item in this.all)
                    item.Available = true;
            }

            public void BlockAll()
            {
                foreach (BigButton item in this.all)
                    item.Available = false;
            }

            public void Release(BigButton buttonToRelease)
            {
                (buttonToRelease).Available = true;
            }

            public void Block(ToolStripItem buttonToBLock)
            {
                (buttonToBLock).Available = false;
            }

            public void BlockAdvanced()
            {
                this.BlockAll();
                this.Release(this.add);
            }
            /// <summary>
            /// Affiche que la liste des boutons voulus dans le big button top
            /// </summary>
            public void ShowOnlyButtons(List<BigButton> buttonsToShow)
            {
                this.HideAllButtons();
                foreach (BigButton item in buttonsToShow)
                {
                    int index = this.all.FindIndex(x => x.BB_Title == item.BB_Title);
                    if (index != -1)
                    {
                        this.all[index].Available = true;
                    }
                }
            }
            /// <summary>
            /// Affiche seulement un bouton dans le big button top
            /// </summary>
            public void ShowOnlyButtons(BigButton buttonToShow)
            {
                List<BigButton> listButtons = new List<BigButton>();
                listButtons.Add(buttonToShow);
                this.ShowOnlyButtons(listButtons);
            }
            /// <summary>
            /// Affiche que la liste des boutons voulus dans le big button top
            /// </summary>
            public void HideOnlyButtons(List<BigButton> buttonsToHide)
            {
                this.ShowAllButtons();
                foreach (BigButton item in buttonsToHide)
                {
                    int index = this.all.FindIndex(x => x.name == item.name);
                    if (index != -1)
                    {
                        this.all[index].Available = false;
                    }
                }
            }
            /// <summary>
            /// Affiche seulement un bouton dans le big button top
            /// </summary>
            public void HideOnlyButtons(BigButton buttonToHide)
            {
                List<BigButton> listButtons = new List<BigButton>
                {
                    buttonToHide
                };
                this.HideOnlyButtons(listButtons);
            }
            /// <summary>
            /// Cache tous les boutons
            /// </summary>
            public void HideAllButtons()
            {
                foreach (BigButton item in this.all)
                {
                    item.Available = false;
                }

            }
            /// <summary>
            /// Rend tous les boutons visible
            /// </summary>
            public void ShowAllButtons()
            {
                foreach (BigButton item in this.all)
                {
                    item.Available = true;
                }
            }

            internal void Dispose()
            {
                _view = null;
                for (int i = 0; i < this.all.Count; i++)
                {
                    all[i].Dispose();
                }
                for (int i = 0; i < this.all.Count; i++)
                {
                    all[i] = null;
                }
                all.Clear();
                all = null;

            }
        }

        private void ShowHideMachineInfo()
        {
            TSU.Tsunami2.Preferences.Values.GuiPrefs.HideMachineParameters.IsTrue = !TSU.Tsunami2.Preferences.Values.GuiPrefs.HideMachineParameters.IsTrue;
        }



        private void UpdateCSToShow()
        {
            var cs = TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems;

            // the preference have just been udated:
            var justSelected = cs.SelectedType;
            if (cs.SelectedTypes.Contains(justSelected))
                cs.SelectedTypes.Remove(justSelected);
            else
            {
                if (justSelected != null)
                    cs.SelectedTypes.Add(justSelected);
            }

            this.UpdateView();
        }




        private void ShowZone()
        {
            Z.Manager m = new Z.Manager(this._Module) { _Name = "Management.Zone Manager" };
            m.SelectableObjects.Add(m.AllElements.Find(x => (x as Zones.Zone)._Name == this.Module.zone._Name));
            Z.Zone z = m.View.SelectOne($"{R.T_ZONE};{R.T_SELECTED_ZONE}");

        }

        private void ImportFromClipBoard()
        {
            string text = Clipboard.GetText();
            string niceDateAndTime = Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now);
            this.Module.AddElementFromText(source: $"ClipBoard import {niceDateAndTime}", text: text);

        }

        private BackgroundWorker backgroundWorker = new BackgroundWorker();

        public void InitializeBackgroundWorker()
        {
            backgroundWorker.DoWork += BackgroundWorker_DoWork;
            backgroundWorker.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted;
        }

        public void RunCommandLineCommand(string command, string filePath, string systemName = null)
        {
            this.Enabled = false; 

            backgroundWorker.RunWorkerAsync(new WorkerArgs { Command = command, FilePath = filePath, SystemName = systemName });
        }

        private void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // Retrieve the arguments from RunWorkerAsync.
            WorkerArgs args = (WorkerArgs)e.Argument;

            ProcessStartInfo processInfo = new ProcessStartInfo
            {
                FileName = "cmd.exe",
                Arguments = "/c " + args.Command,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true
            };

            using (Process process = new Process())
            {
                process.StartInfo = processInfo;
                process.Start();

                string output = process.StandardOutput.ReadToEnd();
                string error = process.StandardError.ReadToEnd();

                process.WaitForExit();

                e.Result = new WorkerResult { Output = output, Error = error, Args = args };
            }
        }

        // This is called on the UI (main) thread after the background thread has completed.
        private void BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Enabled = true;

            // Get the result from the background operation.
            WorkerResult result = (WorkerResult)e.Result;

            Console.WriteLine("Output: " + result.Output);
            Console.WriteLine("Error: " + result.Error);

            // Check if further processing is required.
            if (result.Args.Command.Contains("-r"))
            {
                string filePath = result.Args.FilePath.Replace(".sps", ".spsOut");
                if (!File.Exists(filePath))
                {
                    throw new Exception(filePath + " not found");
                }
                else
                {
                    // Deserialize the JSON and update the UI.
                    string jsonText = File.ReadAllText(filePath);
                    this.Module.DeserializationJsonSPSOut(jsonText, result.Args.SystemName);
                    this.Module.Change();
                    UpdateView();
                }
            }
        }

        public class WorkerArgs
        {
            public string Command { get; set; }
            public string FilePath { get; set; }
            public string SystemName { get; set; }
        }

        public class WorkerResult
        {
            public string Output { get; set; }
            public string Error { get; set; }
            public WorkerArgs Args { get; set; }
        }

        #endregion

        #region Actions

        #region Open & Save

        internal void TryOpen(Point.Types type = Point.Types.Reference)
        {
            TryAction(() => { Open(type); });
        }



        internal bool Open(Point.Types type = Point.Types.Reference)
        {
            string filePath = "";
            try
            {
                filePath = TsuPath.GetFileNameToOpen(this, TSU.Tsunami2.Preferences.Values.Paths.FichierTheo,
                    "",
                    "All files (*.*)|*.*|Reseau files (*.res)|*.res|Geode files (*.dat)|*.dat|Xml files (*.xml)|*.xml|Coordinate files (*.coo)|*.coo",
                //    "Browse theoretical file");
                    R.T_BROWSE_THEORETICAL_FILE);
                if (filePath == "") return false;
                Tsunami2.Properties.BOC_Context.SelectedGeodeFilePaths.AddIfNotExisting(filePath);
                TSU.Tsunami2.Preferences.Values.Paths.FichierTheo = (new FileInfo(filePath)).Directory.FullName;

                FileInfo fileInfo = new FileInfo(filePath);

                Coordinates.CoordinateSystemsTsunamiTypes proposedType = this.Module.GetProbableType(fileInfo, out bool isDotDat);

                if (proposedType == Coordinates.CoordinateSystemsTsunamiTypes.Unknown)
                {
                    if (!AskForCoordinateType(out proposedType))
                        return false;
                }

                this.Module.AddElementsFromFile(fileInfo, proposedType, isDotDat, type);
                return true;
            }
            catch (Exception ex)
            {
                Shell.Run(filePath);
                throw new Exception(string.Format(R.T301 + ";", filePath) + "\r\n" + ex.Message, ex);//"Add Points failed", ex.Source + ": " + ex.Message, R.T_OK, "", "", System.Reflection.MethodBase.GetCurrentMethod().Name));
            }
        }

        internal bool AskForCoordinateTypeString(out string type, List<string> listOfChoices)
        {
            var count = listOfChoices.Count;
            type = string.Empty;
            // if not Cartesian system
            if (count == 0)
            {
                return false;
            }
            // if only one available
            else if (count == 1)
            {
                type = listOfChoices[0];
                return true;
            }
            string cancel = R.T_CANCEL;
            listOfChoices.Add(cancel);
            MessageInput mi = new MessageInput(MessageType.Choice,
                    $"{R.T_COORDINATE_SYSTEM};{R.T_IMPOSSIBLE_TO_KNOWN_WHICH_TYPE_OF_COORDINATES_YOU_WANT_TO_IMPORT_CAN_YOU_SPECIFIED}")
            {
                ButtonTexts = listOfChoices
            };
            var r = mi.Show();

            if (r.TextOfButtonClicked == cancel)
                return false;
            listOfChoices.Remove(cancel);
            foreach (string choice in listOfChoices)
            {
                if (r.TextOfButtonClicked == choice)
                {
                    type = choice;
                    break;
                }
            }
            return true;
        }
        internal bool AskForCoordinateType(out Coordinates.CoordinateSystemsTsunamiTypes type)
        {
            type = Coordinates.CoordinateSystemsTsunamiTypes.Unknown;
            var cancel = R.T_CANCEL;
            var ccsH = Coordinates.CoordinateSystemsTsunamiTypes.CCS;
            var ccsZ = Coordinates.CoordinateSystemsTsunamiTypes.CCS_Z;
            var phys = Coordinates.CoordinateSystemsTsunamiTypes.PHYS;
            var su = Coordinates.CoordinateSystemsTsunamiTypes.SU;
            string titleAndMessage = $"{R.T_COORDINATE_SYSTEM};{R.T_IMPOSSIBLE_TO_KNOWN_WHICH_TYPE_OF_COORDINATES_YOU_WANT_TO_IMPORT_CAN_YOU_SPECIFIED}";
            var r = new MessageInput(MessageType.Choice, titleAndMessage)
            {
                ButtonTexts = new List<string> { ccsH.ToString(), ccsZ.ToString(), su.ToString(), phys.ToString(), cancel }
            }.Show();
            if (r.TextOfButtonClicked == cancel)
                return false;

            if (r.TextOfButtonClicked == ccsH.ToString())
                type = ccsH;
            else
            {
                if (r.TextOfButtonClicked == su.ToString())
                    type = su;
                else
                {
                    if (r.TextOfButtonClicked == phys.ToString())
                        type = phys;
                    else
                    {
                        if (r.TextOfButtonClicked == ccsZ.ToString())
                            type = ccsZ;

                    }
                }
            }
            return true;
        }

        #endregion

        #region Select


    

        public void AddNewPoint()
        {
            Point p;
            CoordinatesInAllSystems cs = null;
            if (this.Module._SelectedObjects.Count == 1)
            {
                if (this.Module._SelectedObjects[0] is Point)
                {
                    cs = ((this.Module._SelectedObjects[0] as Point)._Coordinates) as CoordinatesInAllSystems;
                }
            }

            p = this.Module.AddNewPoint(cs);

            if (p == null) return;

            this.ElementShownInListType = ElementShownInListTypes.Points;
            this.Module.SelectableObjects.Clear();
            this.UpdateView();
        }

        public bool NeedToAskCode(string pointName)
        {
            bool alreadySockets = FirstElementHaveSocket();
            bool noElementYet = this.Module.GetPointsInAllElements().Count == 0;
            bool habe2Dots = pointName.Count(f => f == '.') > 2;
            return alreadySockets || (noElementYet && habe2Dots);
        }


        internal string ShowMessageOfExclamation(object p, string v, string t_CANCEL)
        {
            throw new NotImplementedException();
        }

        private bool FirstElementHaveSocket()
        {
            List<Point> points = this.Module.GetPointsInAllElements();
            if (points.Count == 0) return false; // no point we dont know if they have codes
            points = points.FindAll(x => x.SocketCode != null);
            if (points.Count == 0) return false; // no point we dont know if they have codes
            points = points.FindAll(x => x.SocketCode.Id != "0");
            return points.Count > 0;
        }

        public enum CodeFor
        {
            PolarInstrumentHeight,
            PolarTargetHeight,
            LevellingTargetHeight,
        }

        private void Compare()
        {
            List<Element> l = this.currentStrategy.GetSelectedElements().Cast<Element>().ToList();
            if (l.Count < 2) throw new Exception(R.T303);
            this.Module.Compare(l);
        }

        private void Edit()
        {
            switch (this.Module._SelectedObjects.Count)
            {
                case 0:
                    throw new T.UserException(R.T533);

                case 1:
                    TsuObject o = this.Module._SelectedObjects[0];
                    this.Module.Edit(o as Element);
                    break;

                default:
                    throw new T.UserException(R.T304);
            }
        }

        internal void TrySelectSequenceFile()
        {
            TryAction(SelectSequenceFile);
        }
        private void SelectSequenceFile()
        {
            Module.OpenSequenceFile();
        }

        internal Element SelectOne(string titleAndMessage = null, View.ElementShownInListTypes forcedType = ElementShownInListTypes.Unknown)  //, int numberOfLevelToskipInTheHierarchy = 0
        {
            this.Module.MultiSelection = false;
            this.CheckBoxesVisible = false;
            if (Module._SelectedObjects != null)
            {
                if (Module._SelectedObjects.Count > 1)
                {
                    Module._SelectedObjects.Clear();
                    Module._SelectedObjectInBlue = null;
                }
            }

            if (forcedType == ElementShownInListTypes.Sequences && Module.SelectableObjects.Count == 0)
            {
                this.Module.OpenSequenceFile();
            }
            else
            {
                if (this.AllElements.Count == 0 && forcedType != ElementShownInListTypes.Sequences) this.Open();
            }

            this.UpdateView(); // this will apply the preselection and object hiding

            this.ShowInMessageTsu(titleAndMessage ?? R.T305,
                R.T_SELECT, this.ValidateSelection,
                R.T_CANCEL, this.CancelSelection,
                fullSize: true);

            if (this.Module._SelectedObjects.Count > 0)
                return Module._SelectedObjects[0] as Element;
            else
                return null;
        }


        internal CompositeElement SelectSeveral(string titleAndMessage, string buttonValid, string buttonCancel, int numberOfLevelToskipInTheHierarchy = 0, bool showOnlyPointsForWire = false, bool selectOnlyAlesage = false, string customButtonName = "")
        {
            this.Module.MultiSelection = true;
            this.CheckBoxesVisible = true;
            if (this.AllElements.Count == 0) this.Open();
            if (selectOnlyAlesage) { this.Module.SetSelectableListToAllAlesages(); }
            if (showOnlyPointsForWire) this.Module.SetSelectableToAllUniquePointForAdvanceWire();
            // this.UpdateView(); // has been removed because done also in ShowInMessageTsu()

            string buttonName = customButtonName;
            if (buttonName == "")
                buttonName = R.T_SELECT;

            if (buttonCancel == "")
            {
                this.ShowInMessageTsu(titleAndMessage ?? R.T306,
                    buttonName, this.ValidateSelection,
                    fullSize: true);
            }
            else
            {
                this.ShowInMessageTsu(titleAndMessage ?? R.T306,
                    buttonName, this.ValidateSelection,
                    R.T_CANCEL, this.CancelSelection,
                    fullSize: true);
            }
            if (this.Module._SelectedObjects.Count > 0)
            {
                CompositeElement e = new CompositeElement("Sequence");
                foreach (TsuObject o in this.Module._SelectedObjects)
                {
                    e.Add(o as Element);
                }
                return e;
            }
            else
                return null;
        }

        

        public void EditPoint()
        {
            this._PanelTop.Enabled = false;
            //this.Height = 200;
            this.ShowInMessageTsu(R.T307,
                "Apply", this.ValidateSelection,
                R.T_CANCEL, this.currentStrategy.CancelSelection,
                background: Tsunami2.Preferences.Theme.Colors.Attention,
                foreground: Tsunami2.Preferences.Theme.Colors.NormalFore);
        }

        #endregion

        # region Transformation
        private void OpenChabaWithSelectedPoint()
        {
            List<Point> points = GetSelectedPoints();
            string cancel = R.T_CANCEL;
            string titleAndMessage = R.T308 + points.Count.ToString() + " " + R.T309;
            string initialTextBoxText = T.Conversions.Date.ToDateUpSideDown(DateTime.Now);
            List<string> buttonTexts = new List<string> { R.T_OK, cancel };

            MessageTsu.ShowMessageWithTextBox(titleAndMessage, buttonTexts, out string buttonClicked, out string textInput, initialTextBoxText);

            if (buttonClicked != cancel)
                this.Module.ComputeWithChaba(points, "Chaba_" + textInput);
        }

        private void OpenCsgeoWithSelectedPoint()
        {
            List<Point> points = GetSelectedPoints();
            string cancel = R.T_CANCEL;

            MessageInput mi = new MessageInput(MessageType.Choice, R.T310)
            {
                ButtonTexts = new List<string> { "User" , "MLA", "CCS" }
            };
            string buttonClicked1 = mi.Show().TextOfButtonClicked;

            if (buttonClicked1 != cancel)
            {
                Coordinates.CoordinateSystemsTsunamiTypes cs;
                if (buttonClicked1 == "MLA")
                    cs = Coordinates.CoordinateSystemsTsunamiTypes.MLA;
                else if (buttonClicked1 == "CCS")
                    cs = Coordinates.CoordinateSystemsTsunamiTypes.CCS;
                else
                    cs = Coordinates.CoordinateSystemsTsunamiTypes.UserDefined;

                MessageTsu.ShowMessageWithTextBox(R.T311 + points.Count + R.T312, new List<string> { R.T_OK, cancel },
                    out string buttonClicked2, out string textInput, T.Conversions.Date.ToDateUpSideDown(DateTime.Now));

                if (buttonClicked2 != cancel)
                {
                    string name = "CsGeo_" + textInput;
                    this.Module.OpenCsGeoSurveyPadPluginWithPoints(points, name, cs);
                }
            }
        }

        private List<Point> GetSelectedPoints()
        {
            List<Point> points = new List<Point>();
            foreach (Element item in this.currentStrategy.GetSelectedElements())
            {
                points.AddRange(item.GetPoints());
            }
            return points;
        }

        #endregion

        # region shapesFitting
        internal void OpenFittingMenu()
        {
            List<Control> cm = new List<Control>
            {
                buttons.Line,
                buttons.Plane,
                buttons.Circle,
                buttons.Sphere,
                buttons.Cylinder
            };
            this.ShowPopUpSubMenu(cm, "fitting ");
            //cms.Focus(); // if not force the menu lost teh focus on the first appearance...
        }
        internal void OpenGeoFitToFitLine()
        {
            this.Module.FitLine(GetSelectedPoints());
        }
        internal void OpenGeoFitToFitPlane()
        {
            this.Module.FitPlane(GetSelectedPoints());
        }
        internal void OpenGeoFitToFitCircle()
        {
            this.Module.FitCircle(GetSelectedPoints());
        }
        internal void OpenGeoFitToFitSphere()
        {
            this.Module.FitSphere(GetSelectedPoints());
        }
        internal void OpenGeoFitToFitCylinder()
        {
            this.Module.FitCylinder(GetSelectedPoints());
        }
        #endregion

        # region Ask4

        internal string Ask4WhatToDoWhenPointExist(Point point, CompositeElement ce, Point existingPoint)
        {
            string s;
            string keep = R.T314;
            string rename = R.T315;
            string update = R.T316;

            DsaFlag RenameWhenExist = DsaFlag.GetByNameOrAdd(this.Module.FinalModule.DsaFlags, "RenameWhenExist");

            string titleAndMessage = $"{R.T_POINT_EXIST} {R.T_RENAME_POINT}?";
            if (new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = CreationHelper.GetYesNoButtons(),
                    DontShowAgain = RenameWhenExist
                }.Show().TextOfButtonClicked == R.T_YES)
            {
                Module.RenameAndAddElement(point, ce);
                return "rename";
            }

            DsaFlag UpdateWhenExist = DsaFlag.GetByNameOrAdd(this.Module.FinalModule.DsaFlags, "UpdateWhenExist");

            string titleAndMessage1 = string.Format(R.T313, "\r\nNew coordinates:\r\n" + point.ToString() + "\r\nOld coordinates:\r\n" + existingPoint.ToString() + "\r\nDifferences:\r\n" + (point - existingPoint).ToString());
            if (new MessageInput(MessageType.Choice, titleAndMessage1)
                {
                    ButtonTexts = new List<string> { update, keep },
                    DontShowAgain = UpdateWhenExist
                }.Show().TextOfButtonClicked == update)
            {
                this.Module.UpdateExistingPoint(point, ce);
                return "update";
            }
            else
            {
                this.Module.DoNotAddElement(point, ce);
                return "keep";
            }
        }

        internal Z.Zone Ask4MlaZone()
        {
            throw new NotImplementedException();
        }

        internal bool AskIfCoordinatesWillBeGiven(Point p)
        {
            throw new NotImplementedException();
        }



        internal void Ask4SequenceType(ref bool isTH, ref bool isFil, ref bool isNiv)
        {
            string a = R.T_TH;
            string b = R.T_Niv;
            string c = R.T_Fil;
            MessageInput mi = new MessageInput(MessageType.Choice, R.T_SequenceType)
            {
                ButtonTexts = new List<string> { c , a, b }
            };
            string s = mi.Show().TextOfButtonClicked;
            if (s == a)
                isTH = true;
            else if (s == b)
                isNiv = true;
            else if (s == c) 
                isFil = true;
        }

        #endregion


        #endregion

        #region Events

        public override void OnNext(TsuObject TsuObject)
        {
            dynamic dynamic = TsuObject;
            OnNextBasedOn(dynamic);
        }

        public void OnNextBasedOn(TheoreticalElement t)
        {
            //this.Module.SetSelectableList(this.AllElements);
            //this.UpdateView();
        }

        public void OnNextBasedOn(Sequence t)
        {
            //this.Module.SetSelectableList(this.AllElements);
            //this.UpdateView();
        }

        internal void SetModule(Module module)
        {
            this.Module = module;
        }

        #endregion




    }


}
