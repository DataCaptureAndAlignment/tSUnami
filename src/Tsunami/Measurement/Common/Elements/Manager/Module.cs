using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Windows.Forms;
using System.Xml.Serialization;
using TSU.Common.Compute.Transformation;
using TSU.Common.Elements.Composites;
using TSU.Common.Zones;
using TSU.ENUM;
using TSU.IO.SUSoft;
using TSU.Tools;
using TSU.Views;
using TSU.Views.Message;
using M = TSU;
using NFC = NewFrameCreation_Library;
using P = TSU.Preferences;
using R = TSU.Properties.Resources;
using T = TSU.Tools;
using V = TSU.Common.Strategies.Views;
using SP = TSU.Common.Compute.Shapes;
using SO = TSU.Common.Compute.ShapesOutput;
using LibVLCSharp.Shared;
using System.Diagnostics;
using System.Runtime.Remoting.Contexts;
using TSU.Tools.Conversions;

namespace TSU.Common.Elements.Manager
{
    // How to Element Module
    //
    // This module can be used to acces theoritical point from file type.
    //
    // 1. Create a new instance of the class and give him the reference of the parent module, ex: ElementModule e =new ElementModule(this);
    // 2. Call of the available function of selection,  ex : Point p = e.SelectOnePoint();
    //                                                  ex : CompositeElement p = e.SelectPoints();
    // 

    [XmlType(Namespace = "Elements", TypeName = "Manager")]
    public class Module : M.Manager
    {
        public class TheoreticalFile
        {
            public FileInfo Fullpath { get; set; }

            public TheoreticalFile()
            {

            }

            public TheoreticalFile(FileInfo fullpath)
            {
                Fullpath = fullpath;
            }
        }

        #region  Properties

        //internal ElementType SelectableType;

        public static class Flags
        {
            public static TsuBool WarnSmartCodeProposal = new TsuBool(true);
            public static TsuBool AskForCode = new TsuBool(true);
        }


        public enum sourceFile
        {
            geodeXYZH,
            geodeXYZ,
            geodeXYH,
            IDxyz,
            XML
        }


        public enum currentSetSelectableListType
        {
            setSelectableListToAllAlesages,
            setSelectableToAllPoints,
            setSelectableToAllElements,
            setSelectableToAllUniquePoint,
            setSelectableToAllUniquePointForAdvanceWire,
            setSelectableToAllUniquePointForGuidedOffsetModule,
            setSelectableToAllUniquePointForGuidedModule,
            setSelectableToAllTheoreticalPoints,
            setSelectableToAllPilier,
            setSelectableToAllMagnets,
            setSelectableToAllSequence,
            setSelectableToAllSequenceFil,
            setSelectableToAllSequenceNiv,
            setSelectableToAllSequenceTH
        }
        [XmlIgnore]
        public currentSetSelectableListType _currentSetSelectableListType = currentSetSelectableListType.setSelectableToAllPoints;

        [XmlIgnore]
        public new View View
        {
            get
            {
                // since views are not unique to manager anymore, module need to be relinked at everyusage
                var v = _TsuView as View;
                if (v != null)
                    v.SetModule(this);
                return v;
            }
            set { _TsuView = value; }
        }
        private Element _actualElement;

        [XmlIgnore]
        public List<Point> SelectedPoints
        {
            get
            {
                List<Point> ps = new List<Point>();
                if (_SelectedObjects.Count == 0)
                    foreach (var item in AllElements)
                    {
                        if (item is Point p)
                            ps.Add(p);
                        else if (item is CompositeElement c)
                            ps.AddRange(c.GetPoints());
                    }
                else
                    foreach (var item in _SelectedObjects)
                    {
                        if (item is Point p)
                            ps.Add(p);
                        else if (item is CompositeElement c)
                            ps.AddRange(c.GetPoints());
                    }
                return ps;
            }


        }

        // this return the zone defined for this instance of tsunami in the preferences
        public Zone zone
        {
            get
            {
                return Tsunami2.Properties.Zone;
            }
            set
            {
                Tsunami2.Properties.Zone = value;
            }
        }

        public enum TheoFileFormats
        {
            Geode, Other, Unknown
        }

        public TheoFileFormats TheoFileFormat { get; set; } = TheoFileFormats.Unknown;

        [XmlIgnore]
        public List<TheoreticalElement> filesOpened = new List<TheoreticalElement>();
        #endregion

        #region constructor
        // Constructor
        public Module() : base()
        {

        }
        public Module(M.Module module)
            : base(module, R.T_MNU_ELEMENT)
        {

        }

        public override void Initialize()
        {
            // Instanciation
            AllElements = Tsunami2.Properties.Elements;
            base.Initialize();
        }

        internal override void ReCreateWhatIsNotSerialized(bool saveSomeMemory = false)
        {
            try
            {
                base.ReCreateWhatIsNotSerialized(saveSomeMemory);
                AllElements = Tsunami2.Properties.Elements;
                if (zone != null) View.bigbuttonTop.AddCommentToTitle("(Zone is '" + zone._Name + "')");
            }
            catch (Exception ex)
            {
                throw new Exception($"Cannot re-create what was not serialized in {_Name}", ex);
            }
        }
        #endregion

        # region Adding Elements

        public void AddElement(Element e,
            Coordinates.ReferenceFrames inputReferenceFrames = Coordinates.ReferenceFrames.Unknown,
            Zone localZone = null)
        {
            if (e is Point p)
            {
                if (p._Coordinates.HasCcs)
                {
                    if (p._Coordinates.Ccs.Z == Tsunami2.Preferences.Values.na)
                    {
                        if (localZone != null)
                            Systems.HtoZ(p, localZone.Ccs2MlaInfo.ReferenceSurface);
                        else
                            Systems.HtoZ(p, Coordinates.ReferenceSurfaces.RS2K);
                    }
                }
                p.StorageStatus = Tsunami.StorageStatusTypes.Keep;
            }
            else
                AllElements.Add(e);

            bool isTheoElement = e is TheoreticalElement;
            bool referenceFrameIsknown = inputReferenceFrames != Coordinates.ReferenceFrames.Unknown;
            bool zoneKnown = localZone != null;

            if (!isTheoElement && referenceFrameIsknown && zoneKnown)
            {
                TryToFillMissingCoordinates(e, inputReferenceFrames, localZone);
            }
        }

        /// <summary>
        /// will fill two list with the missing Ccs and Mla coord.
        /// </summary>
        /// <param name="e"></param>
        /// <param name="pointWithMissingCcsCoordinates"></param>
        /// <param name="pointWithMissingMlaCoordinates"></param>
        private static void GetIncompleteCoordinates(Element e,
            List<Point> pointWithMissingCcsCoordinates,
            List<Point> pointWithMissingMlaCoordinates,
            List<Point> pointWithMissingSuCoordinates,
            List<Point> pointWithMissingPhysCoordinates)
        {
            if (e is Point p)
            {
                bool notHasCcs = !p._Coordinates.HasCcs;
                bool notHasMla = !p._Coordinates.HasMla;
                bool notHasSu = !p._Coordinates.HasSu;
                bool notHasPhysicist = !p._Coordinates.HasPhysicist;

                if (notHasCcs && notHasMla && notHasSu && notHasPhysicist)
                    return;
                if (!p._Coordinates.HasCcs) pointWithMissingCcsCoordinates.Add(p);
                if (!p._Coordinates.HasMla) pointWithMissingMlaCoordinates.Add(p);
                if (!p._Coordinates.HasSu) pointWithMissingSuCoordinates.Add(p);
                if (!p._Coordinates.HasPhysicist) pointWithMissingPhysCoordinates.Add(p);
            }
            else
            {
                foreach (Point item in e.GetPoints())
                {
                    GetIncompleteCoordinates(item, pointWithMissingCcsCoordinates, pointWithMissingMlaCoordinates, pointWithMissingSuCoordinates, pointWithMissingPhysCoordinates);
                }
            }
        }

        public Coordinates.CoordinateSystemsTsunamiTypes GetProbableType(FileInfo filePath, out bool dotDat)
        {
            // Extension
            dotDat = filePath.Extension.ToUpper() == ".DAT";
            bool dotXML = filePath.Extension.ToUpper() == ".XML";
            bool dotRes = filePath.Extension.ToUpper() == ".RES";

            // First not commented line
            string allLines = File.ReadAllText(filePath.FullName);
            string[] lines = allLines.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

            string firstLine = "";
            foreach (var item in lines)
            {
                if (item.Trim().Length > 0)
                    if (item.Trim()[0] != '%')
                    {
                        firstLine = item;
                        break;
                    }
            }

            bool semiColomnSeperator = firstLine.Split(';').Length > 4;
            bool spaceSeperator = firstLine.Split(' ').Length > 4;

            // Name analyse
            bool phy = false; // filePath.Name.ToUpper().Contains("PHY"); / removed because it can give problme too often
            bool su = false;  // filePath.Name.ToUpper().Contains("SU");
            bool mla = filePath.Name.ToUpper().Contains("MLA");

            if (TheoFileFormat == TheoFileFormats.Unknown)
                TheoFileFormat = TheoFileFormats.Other;

            // Probable type
            if (dotXML) return Coordinates.CoordinateSystemsTsunamiTypes.UserDefined;
            if (!dotDat && !dotRes && spaceSeperator && su)
            {
                Logs.Log.AddEntryAsPayAttentionOf(this, "Theoretical file extension unknown and 'SU' detected in the name of the file, so the coordinates are imported as SU CS");
                return Coordinates.CoordinateSystemsTsunamiTypes.SU;
            }
            if (!dotDat && !dotRes && spaceSeperator && phy)
            {
                Logs.Log.AddEntryAsPayAttentionOf(this, "Theoretical file extension unknown and 'PHY' detected in the name of the file, so the coordinates are imported as PHY CS");
                return Coordinates.CoordinateSystemsTsunamiTypes.PHYS;
            }
            if (!dotDat && !dotRes && spaceSeperator && mla)
            {
                Logs.Log.AddEntryAsPayAttentionOf(this, "Theoretical file extension unknown and 'MLA' detected in the name of the file, so the coordinates are imported as MLA CS");
                return Coordinates.CoordinateSystemsTsunamiTypes.MLA;
            }

            if (dotDat && semiColomnSeperator)
            {
                TheoFileFormat = TheoFileFormats.Geode;
                return Coordinates.CoordinateSystemsTsunamiTypes.CCS;
            }
            if (dotRes) return Coordinates.CoordinateSystemsTsunamiTypes.SU;

            // Else
            return Coordinates.CoordinateSystemsTsunamiTypes.Unknown;
        }

        internal void RemoveTheoreticalFile()
        {
            List<string> filenamesFromSelectedObjects = new List<string>();
            foreach (var item in SelectedPoints)
            {
                if ((item as Point).Type != Point.Types.NewPoint)
                {
                    string origin = (item as Point)._Origin;
                    if (!filenamesFromSelectedObjects.Contains(origin))
                        filenamesFromSelectedObjects.Add(origin);
                }
            }
            foreach (var item in filenamesFromSelectedObjects)
            {
                Remove(item);
            }


            Change();
            _SelectedObjects.Clear();
            SelectableObjects.Clear();

            SelectableObjects.AddRange(GetPointsInAllElements());

            _SelectedObjectInBlue = null;
            UpdateView();
        }



        internal void NFC_CreateAFrame()
        {
            // create temporary element manager
            Module em = new Module();
            em.Initialize();
            string date = T.Conversions.Date.ToDateUpSideDown(DateTime.Now);
            Matrix<double> newTransformationMatrix = NFC.NewFrame.Create(
                new tsunamiViewForNFC(em),
                $@"c:\data\tsunami\temp\nfc\{date}_");
            //TsunamiPreferences.Values.NfcTransformationMatrices.Add(newTransformationMatrix);

        }

        /// <summary>
        /// export all point in SU or CCS to opened them in NFC
        /// </summary>
        internal void NFC_Open()
        {
            // export file with most local coordiantes available, SU or CCS
            string date = T.Conversions.Date.ToDateUpSideDown(DateTime.Now);
            string folder = $@"{Tsunami2.Preferences.Values.Paths.MeasureOfTheDay}computes\NFC";
            Directory.CreateDirectory(folder);
            string inputPath = $@"{folder}\{date}";
            ExportToText(forcedPath: inputPath, silent: true);

            // run NFC with input as parameterand not wait for it but check with timer if the matrix file appeared
            Preferences.Preferences.Dependencies.GetByBame("NFC", out var app);

            string inputPathWithCs = ""; // it is used as a text file for the NFC inpur than become the NFC output folder for its files
            if (File.Exists(inputPath + ".SU.txt"))
                inputPathWithCs = inputPath + ".SU";
            else
                inputPathWithCs = inputPath + ".CCS-Z";

            System.Diagnostics.Process process = Preferences.Preferences.Dependencies.Run(app, inputPathWithCs + ".txt");

            // wait for the matrix file to appear in the folder with the input name
            string matrixPath = inputPathWithCs + @"\transf_matrix.txt";
            Timer t = new Timer() { Interval = 2000, Enabled = true };
            t.Tick += delegate
            {
                if (File.Exists(matrixPath))
                {
                    t.Stop();
                    MessageInput mi = new MessageInput(MessageType.GoodNews, "NFC matrix found;Do you want to use it now? Do you want a specific name for this CS (minimum 3 letters)?")
                    {
                        ButtonTexts = new List<string> { R.T_YES, R.T_NO },
                        Controls = new List<Control> { CreationHelper.GetPreparedTextBox("ABC") }
                    };
                    using (var r = mi.Show())
                    {
                        if (r.TextOfButtonClicked == R.T_YES)
                        {
                            CoordinateSystem newCS = NFC_UseMatrixFile(matrixPath, (r.ReturnedControls[0] as TextBox).Text);
                            if (NFC_ShowOnlyNfcCs(newCS))
                                UpdateView();
                        }
                    }
                }
            };
            process.Exited += delegate
            {
                t.Stop();
                t.Dispose();
            };
        }

        internal void NFC_SelectExistingMatrixFile()
        {
            string filenameFullPath = TsuPath.GetFileNameToOpen(View,
                P.Preferences.Instance.Paths.FichierTheo,
                "",
                "NFC Text files (*.txt)|*.txt|All files (*.*)|*.*",
                "Browse to a NFC matric file");

            if (filenameFullPath == "") return;

            CoordinateSystem newCS = NFC_UseMatrixFile(filenameFullPath);
            if (NFC_ShowOnlyNfcCs(newCS))
                UpdateView();
        }

        private bool NFC_ShowOnlyNfcCs(CoordinateSystem newCS)
        {
            if (newCS == null) return false;

            var css = Tsunami2.Preferences.Tsunami.CoordinatesSystems;
            //string newFrame = newCS._Name;
            //string current = R.T_CURRENT;
            //string both = R.T_BOTH;
            //var r = new MessageInput(MessageType.GoodNews, R.T_MATRIX_IMPORTED_DO_YOU_WANT_TO_SHOW_THE_COORDINATES_OF_YOUR_POINTS_IN_THIS_NEW_SYSTEM_ONLY_IN_THIS_SYSTEM)
            //{
            //    ButtonTexts = new List<string> { newFrame, current, both}
            //}.Show();

            //bool showNewOne = r.TextOfButtonClicked != current;
            //bool removePreviousSelection = r.TextOfButtonClicked == newFrame;

            //if (removePreviousSelection)
            //    css.SelectedTypes.Clear();

            //if (showNewOne)
            //    css.SelectedTypes.Add(newCS);

            css.SelectedTypes.Clear();
            css.SelectedTypes.Add(newCS);

            var r = new MessageInput(MessageType.GoodNews, R.T_MATRIX_IMPORTED_DO_YOU_WANT_TO_SHOW_THE_COORDINATES_OF_YOUR_POINTS_IN_THIS_NEW_SYSTEM_ONLY_IN_THIS_SYSTEM) { AsNotification = true }.Show();

            return true;
        }

        private CoordinateSystem NFC_UseMatrixFile(string filenameFullPath, string csName = "")
        {
            var css = Tsunami2.Preferences.Tsunami.CoordinatesSystems;

            FileInfo matrixFile = new FileInfo(filenameFullPath);

            Matrix<double> selectedMatrix = NFC.IO.TransformationFile.Read(matrixFile.FullName);

            if (css.AllTypes.Find(x => x._Name == matrixFile.Name) != null)
            {
                new MessageInput(MessageType.Warning, "Matrix already exist;The file name already exist as imported matrix?").Show();
                return null;
            }

            string name = csName != "" ? csName : matrixFile.Name;
            string shortName = name.Substring(0, 3);

            // Create a new CS
            CoordinateSystem newCS = new CoordinateSystem()
            {
                _Name = name,
                LongName = name,
                Description = $"CS defined by the matrix coming from {matrixFile.FullName} and applied to your more local system (su, mla, or ccs)",
                AxisNames = new List<string>() { $"X{shortName}", $"Y{shortName}", $"Z{shortName}" },
                type = Coordinates.CoordinateSystemsTypes._3DCartesian,
                TransformatonMatrix = selectedMatrix
            };

            css.AllTypes.Add(newCS);

            NFC_ApplyTransformation(GetPointsInAllElements(), newCS);

            return newCS;
        }

        /// <summary>
        /// Use NFC library to compute coordinates in the new system and add them to CS dictionnary of the points
        /// </summary>
        /// <param name="points"></param>
        /// <param name="cs"></param>
        public static void NFC_ApplyTransformation(List<Point> points, CoordinateSystem cs)
        {
            foreach (var point in points)
            {
                double[,] coordinates = null;
                if (point._Coordinates.HasSu)
                {
                    coordinates = new double[1, 3] { { point._Coordinates.Su.X.Value, point._Coordinates.Su.Y.Value, point._Coordinates.Su.Z.Value } };
                }
                else if (point._Coordinates.HasCcs)
                {
                    coordinates = new double[1, 3] { { point._Coordinates.Ccs.X.Value, point._Coordinates.Ccs.Y.Value, point._Coordinates.Ccs.Z.Value } };
                }
                else
                {
                    Logs.Log.AddEntryAsPayAttentionOf(null, $"{point._Name} needed SU or CCS coordinates available to apply the {cs._Name} transformation");
                    continue;
                }
                var matrixTransformed = NFC.NewFrame.Apply(cs.TransformatonMatrix, Matrix<double>.Build.DenseOfArray(coordinates), new string[3, 1]);

                bool wasExisting = point._Coordinates.AddOrReplaceCoordinatesInOneSystem(cs._Name,
                    new Coordinates(point._Name, matrixTransformed[0, 0], matrixTransformed[0, 1], matrixTransformed[0, 2])
                    );
            }
        }


        // remove points if theoretical or created by user
        internal void Remove(string filename)
        {
            TsuObject o = AllElements.Find(x => x._Name == filename);
            AllElements.Remove(o);
        }

        public void AddElementsFromFile(FileInfo filePath, Coordinates.CoordinateSystemsTsunamiTypes proposedType,
            bool isDotDat, Point.Types type = Point.Types.Reference, bool updateView = true)
        {
            TheoreticalElement t;

            if (proposedType == Coordinates.CoordinateSystemsTsunamiTypes.UserDefined) // xml knwon everything
            {
                t = IO.TheoreticalFile.TheoreticalElementFromXml(filePath);
            }
            else
            {
                t = IO.TheoreticalFile.TheoreticalElement(View, filePath, proposedType, zone, isDotDat);
            }

            SetPointsType(t, type);

            bool weAreAgree;

            if (t.Definition.LocalZone.Ccs2MlaInfo.Origin == null && t.Definition.LocalZone.Su2PhysInGon == null)
            {
                Tsunami2.Properties.ReferenceFrameOfTheMeasurement = t.Definition.ToUseReferenceFrames;

                weAreAgree = true; // no need of compute

                if (isDotDat)
                {
                    // lets check if all point have Z and/or H.
                    Analysis.Element.Have_H_and_Z(t.GetPoints(),
                        out List<Point> pointsWithoutZ, out bool thereIsPointsWithoutH,
                        out bool thereIsPointsWithoutZ);
                    if (thereIsPointsWithoutH) // pas pr�vu
                    {
                        throw new Exception(R.T_SOME_POINTS_HAVE_NO_H);
                    }
                    if (thereIsPointsWithoutZ) // on les calcul en rs2k
                    {
                        Systems.HtoZ(pointsWithoutZ, Coordinates.GetReferenceSurface(t.Definition.ToUseReferenceFrames));
                    }
                }

            }
            else
            {
                EstablishZoneAndFrameToUseForMeasurement(t);
                weAreAgree = TryFillToCoordinates(t);
            }

            if (weAreAgree)
            {
                AddTheoreticalElement(t, false);
            }
            ApplyCurrentSetSelectableType();
            Change();
            if (updateView)
                UpdateView();
        }

        private void SetPointsType(TheoreticalElement t, Point.Types type)
        {
            foreach (Point e in t.GetPoints())

            {
                e.Type = type;
            }
        }



        /// <summary>
        /// Reapplique le filtre pr�c�dent lors de l'ouverture d'un nouveau fichier
        /// </summary>
        private void ApplyCurrentSetSelectableType()
        {
            switch (_currentSetSelectableListType)
            {
                case currentSetSelectableListType.setSelectableListToAllAlesages:
                    SetSelectableListToAllAlesages();
                    break;
                case currentSetSelectableListType.setSelectableToAllPoints:
                    SetSelectableToAllPoints();
                    break;
                case currentSetSelectableListType.setSelectableToAllElements:
                    SetSelectableToAllElements();
                    break;
                case currentSetSelectableListType.setSelectableToAllUniquePoint:
                    SetSelectableToAllUniquePoint();
                    break;
                case currentSetSelectableListType.setSelectableToAllUniquePointForAdvanceWire:
                    SetSelectableToAllUniquePointForAdvanceWire();
                    break;
                case currentSetSelectableListType.setSelectableToAllUniquePointForGuidedOffsetModule:
                    SetSelectableToAllUniquePointForGuidedOffsetModule();
                    break;
                case currentSetSelectableListType.setSelectableToAllUniquePointForGuidedModule:
                    SetSelectableToAllUniquePointForGuidedModule();
                    break;
                case currentSetSelectableListType.setSelectableToAllTheoreticalPoints:
                    SetSelectableToAllTheoreticalPoints();
                    break;
                case currentSetSelectableListType.setSelectableToAllPilier:
                    SetSelectableToAllPilier();
                    break;
                case currentSetSelectableListType.setSelectableToAllMagnets:
                    SetSelectableToAllMagnets();
                    break;
                case currentSetSelectableListType.setSelectableToAllSequence:
                    SetSelectableToAllSequence();
                    break;
                case currentSetSelectableListType.setSelectableToAllSequenceFil:
                    SetSelectableToAllSequenceFil();
                    break;
                case currentSetSelectableListType.setSelectableToAllSequenceNiv:
                    SetSelectableToAllSequenceNiv();
                    break;
                case currentSetSelectableListType.setSelectableToAllSequenceTH:
                    SetSelectableToAllSequenceTH();
                    break;
                default:
                    break;
            }
        }

        internal void CleanPoints()
        {
            List<Point> points = GetPointsInAllElements();
            List<Point> noDupes = new List<Point>();//    points.Distinct().ToList();
            List<Point> dupes = new List<Point>();
            foreach (var item in points)
            {
                Point existing = noDupes.Find(x => x._Name == item._Name);

                if (existing == null)
                    noDupes.Add(item);
                else
                {
                    if (item.Type != Point.Types.NewPoint)
                    {
                        if (existing.Type != Point.Types.NewPoint) // if newer theo we remove the previous one
                        {
                            noDupes.Remove(existing);
                            dupes.Add(item);
                            noDupes.Add(item);
                        }
                        else
                            dupes.Add(item);

                    }
                    else
                        noDupes.Add(item);
                }
            }
            if (dupes.Count == 0)
            {
                new MessageInput(MessageType.Warning, "No duplicated points found").Show();
            }
            else
            {
                string titleAndMessage = $"Remove {dupes.Count} point from the original {points.Count} points?";
                MessageInput mi = new MessageInput(MessageType.ImportantChoice, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_NO, R.T_YES },
                    TextOfButtonToBeLocked = R.T_YES
                };
                if (mi.Show().TextOfButtonClicked == R.T_YES)
                {
                    foreach (var item in dupes)
                    {
                        RemoveElementByNameAndOrigin(item._Name, item._Origin);
                    }
                }
                UpdateView();
            }
        }

        public void AddTheoreticalElement(TheoreticalElement t, bool setSelectableToAllUniquePoint = true)
        {
            FuseInHierarchy(t, AllElements);

            if (!filesOpened.Contains(t))
                filesOpened.Add(t);
            if (setSelectableToAllUniquePoint) { SetSelectableToAllUniquePoint(); }
            SendMessage(t);
        }

        internal void ExportCsv()
        {
            string path = TsuPath.GetFileNameToSave(Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now) + "_Elements", "csv (*.csv) | *.csv", R.T_SELECT_THE_PLACE_WHERE_TO_EXPORT_YOUR_FILE);
            if (path != "")
            {
                IO.Csv.ToCsv(this, path);
                Shell.Run(path);
            }
        }

        internal void ExportCcs()
        {
            string path = TsuPath.GetFileNameToSave("", string.Format("{0:yyyyMMdd}", DateTime.Now) + "_Elements", "Geode(*.dat) | *.dat", "");
            if (path != "")
            {
                Geode.Export(path, SelectedPoints);
                Shell.Run(path);
            }
        }

        internal void ExportCcsNP()
        {
            string path = TsuPath.GetFileNameToSave("", string.Format("{0:yyyyMMdd}", DateTime.Now) + "_New_points", "Geode(*.dat) | *.dat", "");
            if (path != "")
            {
                Geode.ExportInNewPointFormat(path, SelectedPoints);
                Shell.Run(path);
            }
        }

        internal void ExportPhys()
        {
            string path = TsuPath.GetFileNameToSave("", Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now) + "_Elements", "Physicist(*.phy) | *.phy", "");
            if (path != "")
            {
                SaveInIdxyzPhysicistFormat(path, SelectedPoints);
                Shell.Run(path);
            }
        }

        internal void ExportCcsIXYZH()
        {
            string path = TsuPath.GetFileNameToSave("", Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now) + "_Elements", "XYZH(*.dat) | *.dat", "");
            if (path != "")
            {
                SaveCCSInIdxyzFormat(path, SelectedPoints, zone.Ccs2MlaInfo.ReferenceSurface);
                Shell.Run(path);
            }
        }

        internal void ExportToText(string forcedPath = "", bool silent = false)
        {
            if (ExportToTextAndGetPaths(SelectedPoints, out string message, out string directory, forcedPath) && !silent)
            {
                message += "\r\nPresent in this directory:\r\n";
                message += "\t" + MessageTsu.GetLinkIfExist(directory);
                message += "\r\n";
                new MessageInput(MessageType.GoodNews, message)
                {
                    ButtonTexts = new List<string> { R.T_MERCI }
                }.Show();
            }
        }

        internal static bool ExportToTextAndGetPaths(List<Point> points, out string message, out string directory, string forcedPath = "", bool silent = false)
        {
            message = "";
            directory = "";
            if (SaveInAllFormat(points, out List<string> paths, forcedPath) && !silent)
            {
                directory = new FileInfo(paths[0]).DirectoryName;
                message = "A file for each of the available Coordinate Systems and Formats:\r\n";

                foreach (var item in paths)
                {
                    message += MessageTsu.GetLinkIfExist(item) + "\r\n";
                }
                return true;
            }
            return false;
        }

        internal void ExportXml()
        {
            string path = TsuPath.GetFileNameToSave("", Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now) + "_Elements", "xml (*.xml) | *.xml", "");

            if (path != "")
            {
                TheoreticalElement t = new TheoreticalElement();
                t.Definition.LocalZone = zone;

                foreach (var item in GetElementToExport())
                {
                    if (item is Element)
                    {
                        t.Add(item as Element);
                    }
                }

                t.Definition.InitialCoordinateSystems = Coordinates.CoordinateSystemsTypes.AutomaticBasedOnReferenceFrame;
                t.Definition.InitialReferenceFrames = Coordinates.ReferenceFrames.CCS;
                t.Definition.InitialTsunamiSystems = Coordinates.CoordinateSystemsTsunamiTypes.UserDefined;
                IO.Xml.CreateFile(t, path);
                Shell.Run(path);
            }
        }

        private void FuseInHierarchy(TheoreticalElement t, List<TsuObject> list)
        {
            CompositeElement f = list.Find(x => x._Name == t._Name) as CompositeElement;
            if (f != null)
            {
                foreach (Element item in t.Elements)
                {
                    if (item is CompositeElement c)
                        FuseInHierarchy(c, f);
                }
            }
            else
            {
                list.Add(t);
            }
        }

        private void FuseInHierarchy(CompositeElement n, CompositeElement existing)
        {
            CompositeElement f = existing.Elements.Find(x => x._Name == n._Name) as CompositeElement;
            if (f != null)
            {
                foreach (Element item in n.Elements)
                {
                    if (item is CompositeElement c)
                        FuseInHierarchy(c, f);
                }
            }
            else
            {
                existing.Elements.Add(n);
            }
        }

        internal void SHAPES_Launch()
        {
            List<Point> points = GetPointsInSelectedObjects();
            //  if (points.Count < 2) throw new Exception("Not enough points selected (minimum is 2).");
            if (points.Count < 2) throw new Exception(R.T_NOT_ENOUGH_POINTS_SELECTED_MINIMUM_IS_2);

            List<Coordinates> Coordinates = SelectWantedCoordinates(points);

            string ShapeResultFilePath = string.Format("{0}{1}_{2:hh_mm_ss}.xlsx", Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, "Shapes", DateTime.Now);
            string ShapeMacroPath = GetShapeMacroPath();
            Shapes.Launch(Coordinates, ShapeResultFilePath, ShapeMacroPath);
        }

        private List<SP.Point> GetSPSPointsFromPoints(List<Point> points, out string coordType)
        {
            coordType = "Unknown";
            List<SP.Point> spPoints = new List<SP.Point>();
            List<string> commonSystemNames = new List<string>();
            foreach (Coordinates coord in points[0]._Coordinates.ListOfAllCoordinates)
            {
                commonSystemNames.Add(coord.SystemName);
            }

            if (commonSystemNames.Contains("CCS-H"))
            {
                commonSystemNames.Remove("CCS-H");
            }
            foreach (Point pt in points) commonSystemNames = commonSystemNames.Intersect(pt._Coordinates.ListOfAllCoordinates.Where(c => c.AreKnown).Select(c => c.SystemName)).ToList();

            if (!View.AskForCoordinateTypeString(out string systemName, commonSystemNames)) return spPoints;
            else
            {
                coordType = systemName;
                foreach (Point pt in points)
                {
                    SP.Position position = new SP.Position
                    {
                        isfreex = true,
                        isfreey = true,
                        isfreez = true,
                        sigmax = pt._Coordinates.GetCoordinatesInASystemByName(systemName).X.Sigma,
                        sigmay = pt._Coordinates.GetCoordinatesInASystemByName(systemName).Y.Sigma,
                        sigmaz = pt._Coordinates.GetCoordinatesInASystemByName(systemName).Z.Sigma,
                        x = pt._Coordinates.GetCoordinatesInASystemByName(systemName).X.Value,
                        y = pt._Coordinates.GetCoordinatesInASystemByName(systemName).Y.Value,
                        z = pt._Coordinates.GetCoordinatesInASystemByName(systemName).Z.Value,
                    };
                    SP.Point shapesPoint = new SP.Point
                    {
                        active = true,
                        extraInfos = new SP.ExtraInfos
                        {
                            weight = "0"
                        },
                        headerComment = "",
                        inlineComment = "",
                        name = pt._Name,
                        position = position
                    };

                    spPoints.Add(shapesPoint);
                }
                return spPoints;
            }
        }

        private SP.ShapesObject CreateShapesObject(out string coordType)
        {
            coordType = "Unkown";
            List<Point> points = GetPointsInSelectedObjects();

            //  if (points.Count < 2) throw new Exception("Not enough points selected (minimum is 2).");
            if (points.Count < 2) throw new Exception(R.T_NOT_ENOUGH_POINTS_SELECTED_MINIMUM_IS_2);

            List<SP.Point> spPoints = GetSPSPointsFromPoints(points, out string systemName);
            coordType = systemName;

            SP.RootFrame rootFrame = new SP.RootFrame
            {
                innerFrames = new List<object>(),
                isfreescale = false,
                name = "",
                points = spPoints,
                rotation = new SP.Position
                {
                    isfreex = false,
                    isfreey = false,
                    isfreez = false,
                    sigmax = 0,
                    sigmay = 0,
                    sigmaz = 0,
                    x = 0,
                    y = 0,
                    z = 0
                },
                scale = 1,
                translation = new SP.Position
                {
                    isfreex = false,
                    isfreey = false,
                    isfreez = false,
                    sigmax = 0,
                    sigmay = 0,
                    sigmaz = 0,
                    x = 0,
                    y = 0,
                    z = 0
                },
                BestFits = new List<SP.BestFit>()
            };

            SP.Params params1 = new SP.Params
            {
                coordsys = "k3DCartesian",
                extraInfos = new Dictionary<string, object>(),
                precision = 6
            };

            SP.ShapesObject shapesObject = new SP.ShapesObject
            {
                Params = params1,
                rootFrame = rootFrame,
                title = ""
            };

            return shapesObject;
        }

        internal void ExportToShapes()
        {
            string shapesObjectPath = string.Format("{0}{1}_{2:hh_mm_ss}.sps", M.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, "Shapes", DateTime.Now);

            string shapesJson = JsonSerializer.Serialize(CreateShapesObject(out string systemName), new JsonSerializerOptions
            {
                WriteIndented = true
            });

            File.WriteAllText(shapesObjectPath, shapesJson);

            //TSU.Preferences.Preferences.Dependencies.GetByBame("SURVEYPAD", out Common.Dependencies.Application app);
            TSU.Preferences.Preferences.Dependencies.GetByBame("SURVEYPAD", out Common.Dependencies.Application app);
            //app.Path
            Console.WriteLine(app.Path + " " + shapesObjectPath);
            //this.View.RunCommandLineCommand(app.Path + " " + shapesObjectPath, shapesObjectPath, systemName: systemName);
            this.View.RunCommandLineCommand("\"C:\\Program Files\\SUSoft\\SurveyPad\\1.1.beta_shapes\\SurveyPad.exe\" " + shapesObjectPath, shapesObjectPath, systemName: systemName);

        }

        private void ExportWithBestFit(string typeBestFit)
        {
            string shapesObjectPath = string.Format("{0}{1}_{2:hh_mm_ss}.sps", M.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, "Shapes", DateTime.Now);

            SP.ShapesObject shapesObject = CreateShapesObject(out string systemName);

            SP.BestFit bestFit = new SP.BestFit();
            bestFit.Type = typeBestFit;
            bestFit.Name = string.Format("{0}_{1}", typeBestFit, Date.ToDateUpSideDown(DateTime.Now));
            bestFit.Points = new List<string>();
            foreach (SP.Point point in shapesObject.rootFrame.points)
            {
                bestFit.Points.Add(point.name);
            }
            shapesObject.rootFrame.BestFits.Add(bestFit);

            string shapesJson = JsonSerializer.Serialize(shapesObject, new JsonSerializerOptions
            {
                WriteIndented = true
            });

            File.WriteAllText(shapesObjectPath, shapesJson);

            TSU.Preferences.Preferences.Dependencies.GetByBame("SURVEYPAD", out Common.Dependencies.Application app);

            this.View.RunCommandLineCommand("\"C:\\Program Files\\SUSoft\\SurveyPad\\1.1.beta_shapes\\SurveyPad.exe\" " + shapesObjectPath + " -r", shapesObjectPath, systemName: systemName);
        }
        internal void ExportToShapesLine()
        {
            ExportWithBestFit("Line");
        }
        internal void ExportToShapesPlane()
        {
            ExportWithBestFit("Plane");
        }
        internal void ExportToShapesCircle()
        {
            ExportWithBestFit("Circle");
        }
        internal void ExportToShapesSphere()
        {
            ExportWithBestFit("Sphere");
        }

        internal void ImportShapes()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "SPS Out Files (*.spsOut)|*.spsOut|All files (*.*)|*.*";
            openFileDialog.InitialDirectory = M.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay;
            openFileDialog.Title = "Select an spsOut file";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string selectedFilePath = openFileDialog.FileName;
                TSU.Debug.WriteInConsole("Selected file: " + selectedFilePath);
                string jsonText = File.ReadAllText(selectedFilePath);

                // Deserialize
                SO.ShapesObject shapesObject = JsonSerializer.Deserialize<SO.ShapesObject>(jsonText);

                if (!View.AskForCoordinateType(out Coordinates.CoordinateSystemsTsunamiTypes myType))
                    return;
                else
                {
                    TSU.Debug.WriteInConsole("To Add point conversion from shapesObject");
                    string systemName = CoordinatesInAllSystems.GetSystemName(myType);
                    foreach (var fit in shapesObject.BestFits)
                    {
                        foreach (SO.FListPoint pt in fit.fBestFitParam.fListPoint)
                        {
                            Point point = new Point(pt.fPoint.fName);
                            point._Coordinates.AddOrReplaceCoordinatesInOneSystem(systemName, new Coordinates(pt.fPoint.fName, pt.fPoint.fEstimatedValue.fVector[0],
                                pt.fPoint.fEstimatedValue.fVector[1], pt.fPoint.fEstimatedValue.fVector[2]));
                            CoordinatesInAllSystems.FillMissingCoordinates(null, point, null, silente: true, Tsunami2.Properties.Zone);
                            AddElement(point);
                            TSU.Debug.WriteInConsole(point.ToString());
                        }

                        foreach (SO.FListPoint pt in fit.fBestFitParam.fListPoint)
                        {
                            Point point = new Point(pt.fPoint.fName);
                            point._Coordinates.AddOrReplaceCoordinatesInOneSystem(systemName, new Coordinates(pt.fPoint.fName, pt.fPoint.fEstimatedValue.fVector[0],
                                pt.fPoint.fEstimatedValue.fVector[1], pt.fPoint.fEstimatedValue.fVector[2]));
                            CoordinatesInAllSystems.FillMissingCoordinates(null, point, null, silente: true, Tsunami2.Properties.Zone);
                            AddElement(point);
                            TSU.Debug.WriteInConsole(point.ToString());
                        }
                    }
                    Change();
                    UpdateView();
                }
                DeserializationJsonSPSOut(jsonText);

                Change();
                UpdateView();
            }
            else
            {
                throw new Exception("No File Selected");
            }
        }

        internal void DeserializationJsonSPSOut(string spsOutJson, string systemName = null)
        {
            // Deserialize
            SO.ShapesObject shapesObject = JsonSerializer.Deserialize<SO.ShapesObject>(spsOutJson);

            if (systemName == null)
            {
                if (!View.AskForCoordinateType(out Coordinates.CoordinateSystemsTsunamiTypes myType))
                    return;
                else
                {
                    systemName = CoordinatesInAllSystems.GetSystemName(myType);
                }
            }

            Console.WriteLine("To Add point conversion from shapesObject");

            foreach (var fit in shapesObject.BestFits)
            {
                if (fit.fBestFitParam != null)
                {
                    string fitType = fit.fName.Split('_')[0];
                    Point point;
                    switch (fitType)
                    {
                        case "Line":
                            foreach (SO.FListPoint pt in fit.fBestFitParam.fListPoint)
                            {
                                point = new Point(pt.fPoint.fName);
                                point._Coordinates.AddOrReplaceCoordinatesInOneSystem(systemName, new Coordinates(pt.fPoint.fName, pt.fPoint.fEstimatedValue.fVector[0] + pt.fMinDX.fValue,
                                    pt.fPoint.fEstimatedValue.fVector[1] + pt.fMinDY.fValue, pt.fPoint.fEstimatedValue.fVector[2] + pt.fMinDX.fValue));
                                CoordinatesInAllSystems.FillMissingCoordinates(null, point, null, silente: true, Tsunami2.Properties.Zone);
                                AddElement(fit.fName, point);
                            }
                            break;
                        case "Circle":
                            point = new Point(fit.fName + "_Center");
                            point._Coordinates.AddOrReplaceCoordinatesInOneSystem(systemName, new Coordinates(fit.fName + "_Center",
                                fit.fBestFitParam.fEstParameter.coeffCircleCenter.fEstimatedValue.fVector[0],
                                fit.fBestFitParam.fEstParameter.coeffCircleCenter.fEstimatedValue.fVector[1],
                                fit.fBestFitParam.fEstParameter.coeffCircleCenter.fEstimatedValue.fVector[2]));
                            CoordinatesInAllSystems.FillMissingCoordinates(null, point, null, silente: true, Tsunami2.Properties.Zone);
                            AddElement(fit.fName, point);
                            break;
                    }

                }
            }
        }


        /// <summary>
        /// will return the list of coordinate if all point have them, in preference ccs, su then phys
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        private List<Coordinates> SelectWantedCoordinates(List<Point> points)
        {
            List<Coordinates> Coordinates = new List<Coordinates>();

            bool allHaveCCS = true;
            bool allHavePhy = true;
            bool allHaveSU = true;

            foreach (var item in points)
            {
                if (!item._Coordinates.HasCcs) allHaveCCS = false;
                if (!item._Coordinates.HasPhysicist) allHavePhy = false;
                if (!item._Coordinates.HasSu) allHaveSU = false;
            }

            // string message = "Which type of coordinates?;Several coordinates are available for the selected points, which want do you want to export to shapes?";
            string message = $"{R.T_WHICH_TYPE_OF_COORDINATES};{R.T_SEVERAL_COORDINATES_ARE_AVAILABLE_FOR_THE_SELECTED_POINTS}";
            string respond = "";
            if (allHaveCCS && allHavePhy && allHaveSU)
            {
                MessageInput mi = new MessageInput(MessageType.Choice, message)
                {
                    ButtonTexts = new List<string> { "Physicist", "CCS", "SU" }
                };
                respond = mi.Show().TextOfButtonClicked;
            }
            else if (allHavePhy && allHaveSU)
            {
                MessageInput mi = new MessageInput(MessageType.Choice, message)
                {
                    ButtonTexts = new List<string> { "SU", "Physicist" }
                };
                respond = mi.Show().TextOfButtonClicked;
            }
            else if (allHaveCCS && allHaveSU)
            {
                MessageInput mi = new MessageInput(MessageType.Choice, message)
                {
                    ButtonTexts = new List<string> { "CCS", "SU" }
                };
                respond = mi.Show().TextOfButtonClicked;
            }
            else if (allHaveCCS) respond = "CCS";
            else if (allHaveSU) respond = "SU";

            foreach (var item in points)
            {
                switch (respond)
                {
                    case "CCS":
                        Coordinates c = item._Coordinates.GetCoordinatesInASystemByName("CCS-H");
                        c._Name = item._Name;
                        Coordinates.Add(c);
                        break;
                    case "SU":
                        item._Coordinates.Su._Name = item._Name;
                        Coordinates.Add(item._Coordinates.Su);
                        break;
                    case "Physicist":

                        item._Coordinates.Physicist._Name = item._Name;
                        Coordinates.Add(item._Coordinates.Physicist);
                        break;
                    default:
                        break;
                }
            }



            return Coordinates;
        }

        private string GetShapeMacroPath()
        {
            if (!Preferences.Preferences.Dependencies.GetByBame("SHAPES", out Common.Dependencies.Application app))
                throw new Exception("SHAPES not found");
            return app.Path;
        }

        public string Iterate(string name)
        {
            return Analysis.Element.Iteration.GetNext(new List<string>() { name }, GetPointsNames(), Analysis.Element.Iteration.Types.All)[0];
        }


        private void EstablishZoneAndFrameToUseForMeasurement(TheoreticalElement t)
        {
            zone = zone ?? t.Definition.LocalZone;
            if (zone != null) View.bigbuttonTop.AddCommentToTitle("(Zone is '" + zone._Name + "')");
            Tsunami2.Properties.ReferenceFrameOfTheMeasurement = t.Definition.ToUseReferenceFrames;
        }

        public void OpenSequenceFile()
        {
            Sequence seq;
            string filenameFullPath = TsuPath.GetFileNameToOpen(View,
                P.Preferences.Instance.Paths.FichierTheo,
                "",
                "All files (*.*)|*.*|Reseau files (*.res)|*.res|Geode files (*.dat)|*.dat|Xml files (*.xml)|*.xml",
                R.T_BROWSE_SEQ);

            Action InnedMethod = () =>
            {
                try
                {
                    if (filenameFullPath != "")
                    {
                        // contains?

                        char splitChar = '\\';
                        string[] nameSplit;
                        nameSplit = filenameFullPath.Split(splitChar);
                        string filename = nameSplit[nameSplit.Count() - 1];
                        bool cTH = filename.ToUpper().Contains("TH");
                        bool cFil = filename.ToUpper().Contains("FIL");
                        bool cNiv = filename.ToUpper().Contains("NIV");

                        // If for sur? 
                        bool isTH = cTH & !cFil & !cNiv;
                        bool isFil = !cTH & cFil & !cNiv;
                        bool isNiv = !cTH & !cFil & cNiv;

                        // no one? then ask!
                        if (!isTH && !isFil && !isNiv) View.Ask4SequenceType(ref isTH, ref isFil, ref isNiv);

                        // Create
                        if (isTH)
                            seq = new SequenceTHs();
                        else
                        {
                            if (isFil)
                                seq = new SequenceFils();
                            else
                                seq = new SequenceNivs();
                        }
                        seq._Origin = filenameFullPath;
                        LoadSequenceFromFile(seq, filenameFullPath);
                        
                        _SelectedObjectInBlue = null;
                        _SelectedObjects.Clear();
                        MultiSelection = false;
                        View.ShowAsListView();
                        if (isTH)
                        { SetSelectableToAllSequenceTH(); }
                        if (isNiv)
                        { SetSelectableToAllSequenceNiv(); }
                        if (isFil)
                        { SetSelectableToAllSequenceFil(); }
                        SendMessage(seq);
                        Change();
                        UpdateView();
                    }
                }
                catch (Exception ex)
                {
                    if (ex is OperationCanceledException)
                        return;

                    string titleAndMessage = string.Format(R.T272, ex.Source, ex.Message);
                    new MessageInput(MessageType.Critical, titleAndMessage)
                    {
                        Sender = System.Reflection.MethodBase.GetCurrentMethod().Name
                    }.Show();
                }
            };
            View.ShowProgress(View, InnedMethod, 2, string.Format(R.TM_SeqLoad, filenameFullPath));
        }

        /// <summary>
        /// Fill the sequence from file
        /// If the sequnce has unknown points, ask the user if we conntinue
        /// - If the user chose R.T_CANCEL, throws OperationCanceledException
        /// - If the user chose R.T_NEW_POINTS, and set the StorageStatus to Keep
        /// Renames the sequence if there's one with the same name
        /// </summary>
        /// <param name="seq"></param>
        /// <param name="filenameFullPath"></param>
        /// <exception cref="OperationCanceledException"></exception>
        public void LoadSequenceFromFile(Sequence seq, string filenameFullPath, int forcedAnswer = -1)
        {
            // check if sequence has known points
            seq.FillFromFile(filenameFullPath, GetTheoriticalElementKnown(), out List<Point> missingPoints);
            if (missingPoints.Count > 0)
            {
                string titleAndMessage = $"{missingPoints.Count} {R.T_UNKNOWN_POINTS_IN_THE_SEQUENCES};{Research.GetNFirstToString(10, missingPoints)} {R.T_WITH_NO_COORDINATES}. {R.T_DO_YOU_WANT_TO_USE_THOSE_POINTS_AS_NEW_POINT_WITH_NO_KNOWN_COORDINATES_OR_TO_CANCEL_AND_OPEN_A_THEORETICAL_FILE_FIRST}";
                MessageInput mi = new MessageInput(MessageType.Warning, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_NEW_POINTS, R.T_CANCEL }
                };
                string answer;
                if (forcedAnswer != -1)
                    answer = mi.ButtonTexts[forcedAnswer];
                else
                    answer = mi.Show().TextOfButtonClicked;
                if (answer == R.T_CANCEL)
                {
                    throw new OperationCanceledException();
                }
                foreach (Point p in missingPoints)
                    p.StorageStatus = Tsunami.StorageStatusTypes.Keep;
            }

            //Rename the sequence if the name is already used by another sequence
            var compositeElementsStored = Tsunami2.Properties.CompositeElements;

            while (compositeElementsStored.Contains(seq))
                seq._Name = TsuString.FindNextName(seq._Name);
            compositeElementsStored.Add(seq);
        }

        public bool AskName(string proposedName, out string newName, string message = "", List<string> existingNames = null)
        {
            newName = "";
            if (message == "")
            {
                if (proposedName.Contains('.'))
                    message = $"{R.T_POINT_NAME};{R.T_ENTER_THE_NEW_NAME_OF_THE_POINT}" +
                        $"{R.T_YOU_CAN_INCREASE_THE_NUMBER_OF_FIELDS1}.\r\n" +
                        $"{R.T_YOU_CAN_INCREASE_THE_NUMBER_OF_FIELDS2}.\r\n" +
                        $"{R.T_YOU_CAN_INCREASE_THE_NUMBER_OF_FIELDS3}.\r\n" +
                        $"{R.T_YOU_CAN_INCREASE_THE_NUMBER_OF_FIELDS4}.\r\n";
                else
                    message = R.T302;
            }

            if (existingNames == null)
                existingNames = GetPointsNames();

            MessageInput mi = new MessageInput(MessageType.Choice, message)
            {
                ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL },
                Controls = new List<Control> { CreationHelper.GetTableLayoutForRenamingPoint(proposedName, existingNames) },
                MinWidth = 900
            };
            using (MessageResult result = mi.Show())
            {
                if (result.TextOfButtonClicked == R.T_CANCEL)
                {
                    return false;
                }

                if (result.ReturnedControls.Count == 0)
                    throw new Exception("No object returned from the message");

                newName = ((TableLayoutPanel)result.ReturnedControls[0]).Controls["MergedPointName"].Text.ToUpper();
            }

            while (newName[0] == '.')
            {
                newName = newName.Substring(1, newName.Length - 1);
                if (newName == ".")
                {
                    newName = "";
                    break;
                }
            }

            newName = newName.Trim();
            if (newName == "")
                throw new Exception("Wrong name");

            newName = newName.Replace(" ", "_");

            return true;
        }

        public static bool TryFillToCoordinates(TheoreticalElement t)
        {
            try
            {
                if (t.Definition.Validity)
                {
                    t._OriginType = ElementType.TheoreticalFile;
                    TryToFillMissingCoordinates(t);
                    return ShowNetworkValidity(t);
                }
                return false;
            }
            catch (Exception ex)
            {
                //   throw new Exception("Problem to compute the missing coordinates: " + ex.Message);
                throw new Exception(R.T_PROBLEM_TO_COMPUTE_THE_MISSING_COORDINATES + ex.Message, ex);
            }

        }

        public static void TryToFillMissingCoordinates(Element e, Coordinates.ReferenceFrames inputReferenceFrames, Zone zone)
        {
            // list of point missing coordinates
            List<Point> noCcsPoints = new List<Point>();
            List<Point> noSuPoints = new List<Point>();
            List<Point> noMlaPoints = new List<Point>();
            List<Point> noPhyPoints = new List<Point>();
            GetIncompleteCoordinates(e, noCcsPoints, noMlaPoints, noSuPoints, noPhyPoints);

            if (inputReferenceFrames == Coordinates.ReferenceFrames.Unknown && zone != null)
            {
                if (noCcsPoints.Count > 0 && noSuPoints.Count == 0)
                    inputReferenceFrames = Coordinates.GetReferenceFrame(zone.Ccs2MlaInfo.ReferenceSurface, Coordinates.CoordinateSystemsTypes._2DPlusH, Coordinates.CoordinateSystemsTsunamiTypes.CCS);
                else if (noCcsPoints.Count == 0 && noSuPoints.Count > 0)
                    inputReferenceFrames = Coordinates.GetReferenceFrame(zone.Ccs2MlaInfo.ReferenceSurface, Coordinates.CoordinateSystemsTypes._3DCartesian, Coordinates.CoordinateSystemsTsunamiTypes.SU);
                else if (noPhyPoints.Count > 0 && noSuPoints.Count == 0)
                    inputReferenceFrames = Coordinates.GetReferenceFrame(zone.Ccs2MlaInfo.ReferenceSurface, Coordinates.CoordinateSystemsTypes._3DCartesian, Coordinates.CoordinateSystemsTsunamiTypes.PHYS);
            }

            Systems.TryToFixMissingCoordinates(noCcsPoints, noMlaPoints, noSuPoints, noPhyPoints, inputReferenceFrames, zone);

            // NFC frames?
            foreach (var cs in Tsunami2.Properties.CoordinatesSystems.AllTypes)
            {
                if (cs.TransformatonMatrix != null)
                {
                    var pointsMissing = new List<Point>();
                    foreach (var point in e.GetPoints())
                    {
                        if (point._Coordinates.GetCoordinatesInASystemByName(cs._Name) == null)
                            pointsMissing.Add(point);
                    }
                    NFC_ApplyTransformation(pointsMissing, cs);
                }
            }
        }

        public static void TryToFillMissingCoordinates(TheoreticalElement t)
        {
            TryToFillMissingCoordinates(t, t.Definition.InitialReferenceFrames, t.Definition.LocalZone);
        }


        internal static bool ShowNetworkValidity(CompositeElement n)
        {
            const string ok = "Yes";
            string message = "";
            TheoreticalElement t = n as TheoreticalElement;
            if (t != null)
            {
                message = string.Format(R.T317, (n as TheoreticalElement).Definition.InitialCoordinateSystems.ToString());
                var a = (n as TheoreticalElement).Definition.LocalZone.Ccs2MlaInfo;
                if (a != null && a.Origin != null)
                    message += ", " + string.Format(R.T317b, a.ReferenceSurface.ToString());
            }
            else
                message = R.T318;

            string titleAndMessage = R.T318 + ";" + message;
            MessageInput mi = new MessageInput(MessageType.GoodNews, titleAndMessage)
            {
                ButtonTexts = new List<string> { ok, "Nope!" }
            };
            string respond = mi.Show().TextOfButtonClicked;
            switch (respond)
            {
                case ok:
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// will add a element create from a text containg one or several lines with point in IdXYZ or geode format points
        /// </summary>
        /// <param name="source"></param>
        /// <param name="text"></param>
        internal override void AddElementFromText(string source, string text)
        {
            var result = text.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

            if (!AskForCoordinatesType(out Coordinates.ReferenceFrames rf, out Coordinates.CoordinateSystemsTypes cst))
                return;

            CompositeElement sourceElement = new CompositeElement(source);
            var existingPointNames = new List<string>();
            string errorMessage = "";
            foreach (var line in result)
            {
                string cleanline = System.Text.RegularExpressions.Regex.Replace(line, @"\s+", " ").Trim();

                string[] token;
                if (cleanline.Contains(';'))
                    token = cleanline.Split(new[] { ';' });
                else
                    token = cleanline.Split(new[] { '\t', ' ' });

                if (token.Length > 0 && cleanline != "")
                {
                    Point p;
                    switch (token.Length)
                    {
                        case 4: // Guess is ID, X, Y, Z
                            p = T.Conversions.FileFormat.IdXYZLineToPoint(line, rf, cst);
                            break;
                        case 17: // Guess is geode line : TT41; MBG.411622.E; 638.66403; 2897.77769; 4217.78275; 2390.87270; 391.28529; .570; -.00034; -.056608; 105.1728; 1; A; 23 - MAR - 2016; ; ;
                            p = Geode.ReadLine(cleanline);
                            break;
                        default:
                            errorMessage += $"Line '{cleanline}' not imported. \r\n";
                            continue;
                    }
                    p._Origin = source;
                    p._OriginType = ElementType.Unknown;
                    p.Date = DateTime.Now;

                    // iterate while it exist in this group
                    string pName = p._Name;
                    int counter = 0;
                    while (existingPointNames.Contains(pName.ToUpper()))
                    {
                        counter++;
                        pName = p._Name + $" ({counter})";
                    }
                    p._Name = pName;

                    TryToFillMissingCoordinates(p, Coordinates.ReferenceFrames.CCS, Tsunami2.Properties.Zone);
                    sourceElement.AddInHierarchy(p, Element.HowToRename.AddParenthesis);
                    existingPointNames.Add(p._Name.ToUpper());
                }
            }

            if (errorMessage != "")
                new MessageInput(MessageType.Important, errorMessage).Show();

            AddElement(sourceElement, rf, Tsunami2.Properties.Zone);

            SelectableObjects.AddRange(sourceElement.GetPoints());

            UpdateView();
        }

        private bool AskForCoordinatesType(out Coordinates.ReferenceFrames rf, out Coordinates.CoordinateSystemsTypes cst)
        {
            Coordinates.ReferenceSurfaces refSurf = Coordinates.ReferenceSurfaces.RS2K;
            rf = Coordinates.ReferenceFrames.Unknown;
            cst = Coordinates.CoordinateSystemsTypes.Unknown;

            if (!View.AskForCoordinateType(out Coordinates.CoordinateSystemsTsunamiTypes cstt))
                return false;

            cst = Coordinates.GetCoordinatesSystems(cstt);

            if (zone != null)
            {
                if (zone.Ccs2MlaInfo != null)
                {
                    refSurf = zone.Ccs2MlaInfo.ReferenceSurface;
                }
            }
            rf = Coordinates.GetReferenceFrame(refSurf, cst, cstt);
            return true;
        }

        public Point AddNewUnknownPoint(string name = null, string message = null)
        {
            string cancel = R.T_CANCEL;
            string proposedName = name ?? Analysis.Element.Iteration.GetNextWithChoice(R.String_Unknown, this, "");
            if (!AskName(proposedName, out string newName, R.T302))
                return null;

            // get code
            SocketCode.GetSmartCodeFromName(newName, out SocketCode codeFromName, out string message2);

            if (!SocketCode.Ask(View, Tsunami2.Preferences.Values.SocketCodes, codeFromName, "CodeAndDescription", out SocketCode code, out double dummy))
                return null;

            // create point
            Point p = new Point(newName);
            if (code != null) p.SocketCode = code;


            // identify sender
            TsuObject sender;
            if (ParentModule is FinalModule)
                sender = (ParentModule as FinalModule)._ActiveStationModule;
            else
                sender = ParentModule;


            AddElement(sender, p, AddingStrategy.RenameIfExistInSameGroup);
            return p;
        }

        internal Point AddNewPoint(CoordinatesInAllSystems coordinates = null)
        {
            // identify sender
            TsuObject sender;
            if (ParentModule is FinalModule)
                sender = (ParentModule as FinalModule)._ActiveStationModule;
            else
                sender = this;

            // get point name
            Point LastPoint = GetPointsInAllElements().LastOrDefault();
            string lastName = LastPoint != null ? LastPoint._Name : $"{R.String_Unknown}.{R.String_Unknown}.{R.String_Unknown}.POINT###";
            name = Analysis.Element.Iteration.GetNext(lastName, GetPointsNames(), Analysis.Element.Iteration.Types.All);

            TsuOptionalShowing.Type os = TsuOptionalShowing.Type.DontShowDontShowAgainBox;
            if (!AskName(name, out string newName))
                throw new Exception("Cancelled");

            SocketCode.GetSmartCodeFromName(newName, out SocketCode socketCode, out string message2);

            SocketCode.Ask(View, Tsunami2.Preferences.Values.SocketCodes, socketCode, "CodeAndDescription", out SocketCode receivedCode, out double dummy);

            name = newName;

            Point p = new Point(name) { SocketCode = socketCode };

            // approx ot theo
            string titleAndMessage = $"{R.T_ARE_COORDINATES_THEORETICAL_OR_APPROXIMATE};" +
                                     $"{R.T_IF_CONSIDERED_AS_THEORETICAL_COORDINATE_WILL_NOT_BE_RECALCULATED_BY_TSUNAMI}.";
            MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
            {
                ButtonTexts = new List<string> { R.T_THEORETICAL, R.T_APPROXIMATE }
            };
            string r = mi.Show().TextOfButtonClicked;


            if (coordinates != null)
                p._Coordinates = coordinates.Clone() as CoordinatesInAllSystems;
            else
                GiveCoordinateToNewPoint(p);


            if (r == R.T_THEORETICAL)
            {
                p.Type = Point.Types.Reference;
                // for the edit as it will be cala
                Edit(p);
            }
            else
            {
                p.State = Element.States.Approximate;
                p.Type = Point.Types.NewPoint;
                MessageInput mi1 = new MessageInput(MessageType.Choice, R.T_DO_YOU_WANT_TO_EDIT_THE_POINT_NOW)
                {
                    ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                };
                if (R.T_YES == mi1.Show().TextOfButtonClicked)
                {
                    Edit(p);
                }
            }

            Module module = sender as Module;
            string s = "";
            if (module != null)
                s = (sender as Module).GetNewPointGroupName();
            else
                s = R.String_Unknown;
            AddElement(s, p);
            return p;
        }

        private void GiveCoordinateToNewPoint(Point p)
        {
            string ccs = "CCS";
            string local = "Local";
            if (zone == null)
            {
                //   string r = View.ShowMessageOfChoice("Choose coordinate System;Do you want to put coordinates to '2000, 4000, 300' in CCS or '0, 0, 0' in SU?", ccs, local);
                string titleAndMessage = $"{R.T_CHOOSE_COORDINATE_SYSTEM};{R.T_DO_YOU_WANT_TO_PUT_COORDINATES_TO}";
                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = new List<string> { ccs, local }
                };
                string r = mi.Show().TextOfButtonClicked;
                if (r == ccs)
                {
                    p.SetCoordinates(new Coordinates(p._Name, 2000, 4000, 300), Coordinates.ReferenceFrames.CernXYHg00Machine, Coordinates.CoordinateSystemsTypes._2DPlusH);
                    Systems.RecomputeCoordinateFromCCS(p);
                }
                else
                {
                    p._Coordinates.Su = new Coordinates(p._Name, 0, 0, 0);
                    Systems.RecomputeCoordinateFromSu(p);
                }
            }
            else
            {
                p._Coordinates.Su = new Coordinates(p._Name, 0, 0, 0);
                Systems.RecomputeCoordinateFromSu(p);

            }
        }


        internal void ResetFlags()
        {
            Flags.AskForCode = new TsuBool(true);
            Flags.WarnSmartCodeProposal = new TsuBool(true);
        }


        //Permet lorsqu'on cr�e un point � partir de l'element manager de mettre l'origine � created by user
        internal override string GetNewPointGroupName()
        {
            return R.T_CREATED_BY_USER;
        }

        public enum AddingStrategy
        {
            RenameIfExistInSameGroup,
            ReplaceIfSameGroup,
            Replace,
            DontAddIfExist,
            DontAddIfExistInSameGroup
        }


        /// <summary>
        /// this function will create a composite element with the name of the sender and will add the element to it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="p"></param>
        ///// <param name="avoidLookingForHierarchy"></param>
        /// <param name="renameIfNameExist"></param>
        /// <param name="replaceIfNameExist"></param>
        public void AddElement(TsuObject sender, Point p, AddingStrategy strategy)
        {
            List<Point> existingPoints;
            List<Point> existingInDifferentGroup;
            Analysis.Element.ExistanceProblematicOf(p as Point, GetPointsInAllElements(), out existingPoints, out existingInDifferentGroup);

            foreach (var point in existingInDifferentGroup)
            {
                if (point._Coordinates != null && point._Coordinates.HasAny)
                {
                    if (point.State == Element.States.Approximate)
                    {
                        strategy = AddingStrategy.Replace;
                    }
                }
                else
                    strategy = AddingStrategy.Replace;
            }

            bool renameIfNameExist = false;
            bool replaceIfNameExist = false;

            switch (strategy)
            {
                case AddingStrategy.RenameIfExistInSameGroup:
                    renameIfNameExist = true;
                    break;
                case AddingStrategy.ReplaceIfSameGroup:
                    replaceIfNameExist = true;
                    break;
                case AddingStrategy.DontAddIfExist:
                    break;
                case AddingStrategy.DontAddIfExistInSameGroup:
                    break;

                case AddingStrategy.Replace:
                    replaceIfNameExist = true;
                    break;
                default:
                    break;
            }

            string senderName = sender != null ? sender._Name : "Unknown";
            AddElement(
                senderName,
                p,
                avoidLookingForHierarchy: false,
                renameIfNameExist: renameIfNameExist,
                replaceIfNameExist: replaceIfNameExist);

        }

        /// <summary>
        /// this function will create a composite element with the senderName and will add the element to it
        /// </summary>
        /// <param name="senderName"></param>
        /// <param name="element"></param>
        /// <param name="avoidLookingForHierarchy"></param>
        /// <param name="renameIfNameExist"></param>
        /// <param name="replaceIfNameExist"></param>
        /// <param name="updateView"></param>
        public void AddElement(string senderName, Element element,
            bool avoidLookingForHierarchy = false, bool renameIfNameExist = false, bool replaceIfNameExist = false)
        {
            try
            {
                Element elementAsHierarchy = CreateHierarchyElement(senderName, element, avoidLookingForHierarchy);

                if (element is Point p1)
                {
                    TryToFillMissingCoordinates(p1, Tsunami2.Properties.ReferenceFrameOfTheMeasurement, zone);
                }

                // Adding the point???
                Point existingPoint = PointWithSameNameExist(element, elementAsHierarchy);
                if (existingPoint != null)
                {
                    Point p = element as Point;
                    if (!existingPoint._Coordinates.HasAny)
                        ReplaceExistingPoint(p, elementAsHierarchy as CompositeElement);
                    else
                    {
                        if (renameIfNameExist)
                        {
                            RenameAndAddElement(p, elementAsHierarchy as CompositeElement);
                        }
                        else
                        {
                            //replace if name exist
                            if (replaceIfNameExist)
                            {
                                ReplaceExistingPoint(p, elementAsHierarchy as CompositeElement);
                            }
                            else
                            {
                                if (p - existingPoint != 0)
                                    View.Ask4WhatToDoWhenPointExist(p, elementAsHierarchy as CompositeElement, existingPoint);
                            }
                        }
                    }
                }
                else
                {
                    if (replaceIfNameExist)
                    {
                        (elementAsHierarchy as CompositeElement).Add(element, Element.HowToRename.UseSame, forceOverride: true);
                    }
                    else
                    {
                        (elementAsHierarchy as CompositeElement).Add(element, Element.HowToRename.AddParenthesis);
                    }
                }
                Logs.Log.AddEntryAsSuccess(this, $"Point added: {element.ToString()}");
            }
            catch (Exception ex)
            {
                string titleAndMessage = $"Couldn't add a point to the element manager;{ex.Message}";
                new MessageInput(MessageType.Warning, titleAndMessage).Show();
            }
        }

        public Element CreateHierarchyElement(string senderName, Element element, bool avoidLookingForHierarchy)
        {
            try
            {


                Element elementAsHierarchy = null;
                senderName = senderName.Replace('.', '_');
                // add the sender to the hierarchy
                string HierarchySeparatedWithDots = senderName != "" ? senderName + "." : "";
                HierarchySeparatedWithDots += element._Name;
                var parts = Analysis.Names.Get4AsgParts(element._Name);

                if (!avoidLookingForHierarchy)
                {
                    // remove last ".*"
                    HierarchySeparatedWithDots = HierarchySeparatedWithDots.Substring(0, HierarchySeparatedWithDots.LastIndexOf("."));
                }
                else
                {
                    HierarchySeparatedWithDots = senderName + ".";
                }

                int level = 1;
                string currentName = "";
                while (HierarchySeparatedWithDots != "")
                {
                    int i = HierarchySeparatedWithDots.IndexOf('.');
                    if (i == -1) i = HierarchySeparatedWithDots.Length;
                    string levelName = HierarchySeparatedWithDots.Substring(0, i);
                    if (level == 1) // First level is a TsuObjct of type ELement
                    {
                        // Select composite element if existing
                        elementAsHierarchy = AllElements.Find(x => x._Name == levelName) as Element;
                        // Create it it not existing
                        if (elementAsHierarchy == null)
                        {
                            currentName += levelName;
                            elementAsHierarchy = new CompositeElement(currentName);
                            AddElement(elementAsHierarchy);
                        }
                        // Origin is the Level 1
                        element._Origin = levelName;
                    }
                    else // then we can move throught composite Element
                    {
                        // Select composite element if existing
                        Element temp = elementAsHierarchy.Elements.Find(x => x._Name == currentName);
                        if (currentName != "")
                            currentName += '.';
                        currentName += levelName;
                        if (temp == null) // if not existing, create it, then select it
                        {
                            if (currentName.Split('.').Length - 1 == 2 && !currentName.Contains(".B2."))
                            {
                                temp = new Magnet(currentName);
                                Magnet m = temp as Magnet;
                                m._Accelerator = parts[0];
                                m._Origin = senderName;
                                m._ClassAndNumero = parts[1] + "." + parts[2];

                                m._Parameters = (element as Point)._Parameters;
                            }
                            else
                                temp = new CompositeElement(currentName);
                            (elementAsHierarchy as CompositeElement).Add(temp, Element.HowToRename.UseSame);
                        }
                        elementAsHierarchy = temp;
                    }
                    if (i < HierarchySeparatedWithDots.Length)
                        HierarchySeparatedWithDots = HierarchySeparatedWithDots.Substring(i + 1, HierarchySeparatedWithDots.Length - i - 1);
                    else
                        HierarchySeparatedWithDots = "";
                    level++;
                }
                return elementAsHierarchy;
            }
            catch (Exception ex)
            {
                throw new Exception($"Problem to add {element._Name} in hierarchy with CreateHierarchyElement()", ex);
            }
        }

        public static void AddPointInATree(Point point, string origin, ref List<TsuObject> hierarchyOrigins)
        {
            try
            {
                // Get or create the origin node
                Element currentNode = hierarchyOrigins.Find(x => x._Name == origin) as Element;
                if (currentNode == null)
                {
                    currentNode = new CompositeElement(origin) { _Origin = origin, _OriginType = point._OriginType };
                    hierarchyOrigins.Add(currentNode);
                }

                // 
                var parts = Analysis.Names.Get4AsgParts(point._Name);
                string tempName = "";
                for (int i = 0; i < parts.Count; i++)
                {
                    // Get or create the nodes

                    tempName += tempName == "" ? "" : ".";
                    tempName += parts[i];
                    Element tempNode = currentNode.Elements.Find(x => x._Name == tempName) as Element;
                    if (tempNode == null)
                    {
                        if (i == 2) // we have a magnet
                        {
                            tempNode = new Magnet(tempName) { _Origin = origin, _OriginType = point._OriginType };
                            Magnet m = tempNode as Magnet;
                            m._Accelerator = parts[0];
                            m._ClassAndNumero = parts[1] + "." + parts[2];
                            m._Parameters = (point as Point)._Parameters;
                        }
                        else if (i == parts.Count - 1) // we have the point
                        {
                            currentNode.Elements.Add(point);
                            return;
                        }
                        else
                        {
                            tempNode = new CompositeElement(tempName) { _Origin = origin, _OriginType = point._OriginType };
                        }
                        currentNode.Elements.Add(tempNode);

                    }
                    currentNode = tempNode;
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Problem to add {point._Name} in hierarchy with CreateHierarchyElement()", ex);
            }
        }

        /// <summary>
        /// Replace an existing point in the composite module if name exist
        /// </summary>
        /// <param name="point"></param>
        /// <param name="compositeElement"></param>
        private static void ReplaceExistingPoint(Point point, CompositeElement compositeElement)
        {
            compositeElement.Replace(point);
        }

        private static Point PointWithSameNameExist(Element elementToTest, Element elementContainingOtherElements)
        {
            //Element elementWithSameOrigin = new C.CompositeElement() ;
            //elementWithSameOrigin.Elements.AddRange(elementContainingOtherElements.GetPoints().FindAll(x => x._Origin == elementToTest._Origin));
            //return (elementWithSameOrigin.GetElementByName(elementToTest._Name) != null);
            List<TsuObject> f = elementContainingOtherElements.GetAllElements().FindAll(x => x._Name == elementToTest._Name);
            foreach (Point p in f)
            {
                if (p.State == Element.States.Good)
                    return p;
                if (p.State == Element.States.FromStation)
                    return p;
            }
            return null;
        }

        internal void DoNotAddElement(Point e, CompositeElement c)
        {

        }

        internal static void RenameAndAddElement(Point e, CompositeElement c)
        {
            // Do clone if same ref
            Point p;
            if (ReferenceEquals(c.GetElementByName(e._Name), e))
                p = e.DeepCopy();
            else
                p = e;

            // loop until name doesnt exist
            int j = 1;
            while (PointWithSameNameExist(p, c) != null)
            {
                p._Name = string.Format("{0}({1})", p._Name, j);
            }

            // Finally add the point
            c.Add(p);
        }

        internal void UpdateExistingPoint(Point p, CompositeElement l)
        {
            Point old = l.GetElementByName(p._Name) as Point;

            old._Coordinates = p._Coordinates;
        }
        #endregion

        #region remove element
        public void RemoveElement(TsuObject sender, Element element)
        {
            CompositeElement senderElements = AllElements.Find(x => x._Name == sender._Name) as CompositeElement;
            senderElements.Remove(element);
        }
        public void RemoveElementByName(TsuObject sender, string name)
        {
            CompositeElement senderElements = AllElements.Find(x => x._Name == sender._Name) as CompositeElement;
            senderElements.RemoveByName(name);
        }

        public void RemoveElementByNameAndOrigin(string elementName, string originName)
        {
            foreach (Element item in AllElements)
            {
                if (FindAndRemoveFromByNameAndOrigin(item, elementName, originName))
                    break;
            }
        }

        private bool FindAndRemoveFromByNameAndOrigin(Element e, string elementName, string originName)
        {
            if (e == null) return false;
            if (e.Elements == null) return false;
            var find = e.Elements.Find(x => x._Name == elementName && x._Origin == originName);
            if (find != null)
            {
                e.Elements.Remove(find);
                return true;
            }
            if (e is CompositeElement)
                foreach (Element item in e)
                {
                    if (FindAndRemoveFromByNameAndOrigin(item, elementName, originName))
                        return true;
                }
            return false;
        }

        public void Clear()
        {
            AllElements.Clear();
        }
        public void Clear(TsuObject sender)
        //enl�ve que les �l�ments venant du sender
        {
            if (sender != null)
            {
                AllElements.RemoveAll(x => x._Name.Contains(sender._Name));
            }
        }
        #endregion

        #region Selection
        // Selection
        internal void ValidateSelection()
        {
            View.currentStrategy.ValidateSelection();
            if (_SelectedObjects.Count >= 0)
            {
                CompositeElement e = new CompositeElement("Sequence");
                foreach (TsuObject o in _SelectedObjects)
                {
                    e.Add(o as Element);
                }
                SendMessage(e);
                //foreach (var item in this.SelectedObjects)
                //{
                //    SendMessage(item);

                //}
            }
        }

        internal override bool IsItemSelectable(TsuObject tsuObject)
        {
            return tsuObject is Element;
        }

        /// <summary>
        /// use this to give a list of ref to element that you want to see appeared in the list view, you can also set the property
        /// </summary>
        /// <param name="selectableItemList"></param>
        internal override void SetSelectableList(List<TsuObject> selectableItemList)
        {
            base.SetSelectableList(selectableItemList);
            int i = SelectableObjects.Count();
        }


        internal void SetSelectableListToAllAlesages()
        {
            List<TsuObject> selectable = new List<TsuObject>();
            foreach (Element item in AllElements)
                foreach (Point point in item.GetPoints())
                    if (point.State != Element.States.FromStation)
                        if (point.IsAlesage)
                            selectable.Add(point);

            SetSelectableList(selectable);
            _currentSetSelectableListType = currentSetSelectableListType.setSelectableListToAllAlesages;
        }

        /// <summary>
        /// Set the selectable list to all point
        /// </summary>
        internal void SetSelectableToAllPoints()
        {
            //this.SetSelectableToAllUniquePoint();
            List<TsuObject> selectable = new List<TsuObject>();

            foreach (Element item in Tsunami2.Properties.Points)
            {
                selectable.Add(item);
            }
            SetSelectableList(selectable);
            _currentSetSelectableListType = currentSetSelectableListType.setSelectableToAllPoints;
        }

        internal void SetSelectableToAllElements()
        {
            //this.SetSelectableToAllUniquePoint();

            View.buttons.ShowAllButtons();
            List<TsuObject> selectable = new List<TsuObject>();
            foreach (Element item in Tsunami2.Properties.Points)
            {
                selectable.Add(item);
            }
            foreach (Element item in Tsunami2.Properties.CompositeElements)
            {
                selectable.Add(item);
            }
            SetSelectableList(selectable);
            _currentSetSelectableListType = currentSetSelectableListType.setSelectableToAllElements;
        }



        /// <summary>
        /// Remove the points in double in the selectable list
        /// </summary>
        internal void SetSelectableToAllUniquePoint()
        {
            SetSelectableToAllPoints();
            List<TsuObject> selectableList = new List<TsuObject>();
            if (SelectableObjects != null)
            {
                foreach (var item in SelectableObjects)
                {
                    if (item is Point pt)
                    {
                        if (pt.State == Element.States.FromStation)
                            continue;
                        TsuObject idem = selectableList.Find(x =>
                            (x as Point)._Name == pt._Name &&
                            (x as Point)._Origin == pt._Origin &&
                            pt.State != Element.States.Bad);

                        TsuObject sameName = selectableList.Find(x =>
                            (x as Point)._Name == pt._Name &&
                            (x as Point)._Origin != pt._Origin);


                        if (idem == null)
                        {
                            if (sameName == null)
                            {
                                selectableList.Add(item);
                            }
                            else
                            {
                                if (pt.Type != Point.Types.NewPoint)
                                    selectableList.Add(item);
                            }

                        }
                        else
                        {
                            //differentOriginIsDifferentPoint = true; // this var dissapearedduring refactorisation :/
                        }
                    }
                }
                SetSelectableList(selectableList);
                _currentSetSelectableListType = currentSetSelectableListType.setSelectableToAllUniquePoint;
            }

        }
        /// <summary>
        /// N'ajoute que les alesages et les points piliers qui ne sont pas des nids, cp ou GITL pour les modules avanc�s
        /// Les points mesur�s sont affich�s
        /// </summary>
        internal void SetSelectableToAllUniquePointForAdvanceWire()
        {
            //TODO complete with points not wanted in a wire
            SetSelectableToAllUniquePoint();
            //this.SetSelectableToAllPoints();
            List<TsuObject> selectableList = new List<TsuObject>();
            if (SelectableObjects != null)
            {
                foreach (var item in SelectableObjects)
                {
                    if (item is Point pt)
                    {
                        if (pt.State == Element.States.FromStation)
                            continue;
                        if (pt.fileElementType == ElementType.Pilier)
                        {
                            if (!pt._Name.Contains("NID")
                                && !pt._Name.Contains("CP")
                                && !pt._Name.Contains("GITL")
                                && !pt._Name.Contains("STL")
                                && !pt._Name.Contains("CRAP")
                                && !pt._Name.Contains("RN")
                                && !pt._Name.Contains("RV")
                                && !pt._Name.Contains("NIV"))
                            {
                                selectableList.Add(item);
                            }
                        }
                        if (pt.fileElementType == ElementType.Alesage)
                        {
                            selectableList.Add(item);
                        }
                    }
                }
                SetSelectableList(selectableList);
                _currentSetSelectableListType = currentSetSelectableListType.setSelectableToAllUniquePointForAdvanceWire;
            }

        }
        /// <summary>
        /// N'ajoute que les alesages et les points piliers qui ne sont pas des nids, cp ou GITL
        /// Les points mesur�s NE SONT PAS AFFICHE
        /// </summary>
        internal void SetSelectableToAllUniquePointForGuidedOffsetModule()
        {
            SetSelectableToAllUniquePointForGuidedModule();
            List<TsuObject> selectableList = new List<TsuObject>();
            if (SelectableObjects != null)
            {
                foreach (var item in SelectableObjects)
                {
                    if (item is Point pt)
                    {
                        if (pt.State == Element.States.FromStation)
                            continue;
                        if (pt.fileElementType == ElementType.Pilier)
                        {
                            if (!pt._Name.Contains("NID")
                                && !pt._Name.Contains("CP")
                                && !pt._Name.Contains("GITL")
                                && !pt._Name.Contains("STL")
                                && !pt._Name.Contains("CRAP")
                                && !pt._Name.Contains("RN")
                                && !pt._Name.Contains("RV")
                                && !pt._Name.Contains("NIV"))
                            {
                                selectableList.Add(item);
                            }
                        }
                        if (pt.fileElementType == ElementType.Alesage)
                        {
                            selectableList.Add(item);
                        }
                    }
                }
                SetSelectableList(selectableList);
                _currentSetSelectableListType = currentSetSelectableListType.setSelectableToAllUniquePointForGuidedOffsetModule;
            }

        }
        /// <summary>
        /// S�lectionne les points qui ne proviennent pas d'une station de nivellement ou �carto
        /// </summary>
        internal void SetSelectableToAllUniquePointForGuidedModule()
        {
            List<TsuObject> selectableList = new List<TsuObject>();
            SetSelectableToAllUniquePoint();
            if (SelectableObjects != null)
            {
                foreach (var item in SelectableObjects)
                {
                    if (item is Point pt)
                    {
                        if (pt.State == Element.States.FromStation)
                            continue;
                        //if ((!pt._Origin.Contains("STLINE")) && (!pt._Origin.Contains("STLEV")))
                        //{
                        //    selectableList.Add(item);
                        //}
                        if (pt.Type != Point.Types.NewPoint || pt._Origin == R.T_CREATED_BY_USER) selectableList.Add(item);
                    }
                }
                SetSelectableList(selectableList);
                _currentSetSelectableListType = currentSetSelectableListType.setSelectableToAllUniquePointForGuidedModule;
            }
        }

        /// <summary>
        /// show only the points with the origin from a dat file
        /// </summary>
        /// <param name="pointsOnlyFromDatFile"></param>
        internal void SetSelectableToAllTheoreticalPoints()
        {
            SetSelectableToAllPoints();
            List<TsuObject> selectable = new List<TsuObject>();
            foreach (var item in SelectableObjects)
            {
                if (item is Point)
                {
                    Point point = item as Point;
                    if (point.State == Element.States.FromStation)
                        continue;
                    //if (point._Origin.Contains(".dat"))
                    if (point._OriginType == ElementType.TheoreticalFile)
                    {
                        selectable.Add(point);
                    }
                }
            }
            SetSelectableList(selectable);
            _currentSetSelectableListType = currentSetSelectableListType.setSelectableToAllTheoreticalPoints;
        }

        internal void SetSelectableToAllPilier()
        {
            List<TsuObject> selectable = new List<TsuObject>();

            foreach (Point point in Tsunami2.Properties.Points)
                if (point.State != Element.States.FromStation)
                    if (point.IsPilier)
                        selectable.Add(point);

            SetSelectableList(selectable);
            _currentSetSelectableListType = currentSetSelectableListType.setSelectableToAllPilier;
        }

        internal void SetSelectableToAllMagnets()
        {
            List<TsuObject> selectable = new List<TsuObject>();
            foreach (Element item in CompositeElement.GetMagnets(Tsunami2.Properties.Points))
            {
                if (item.State == Element.States.FromStation
                    && item.GetAlesages().Count == 0)
                    continue;
                selectable.AddRange(item.GetMagnets());
            }
            ///Enleve les piliers qui sont aussi consid�r�s comme des magnets
            selectable.RemoveAll(x => (x as Magnet).GetAlesages().Count == 0);
            SetSelectableList(selectable);
            _currentSetSelectableListType = currentSetSelectableListType.setSelectableToAllMagnets;
        }
        /// <summary>
        /// Set the selectable list to all type of sequence
        /// </summary>
        internal void SetSelectableToAllSequence()
        {
            SetSelectableList(Tsunami2.Properties.FlattenedCompositeElements.OfType<Sequence>().ToList<TsuObject>());
            _currentSetSelectableListType = currentSetSelectableListType.setSelectableToAllSequence;
        }
        /// <summary>
        /// Set the selectable list to all sequence of fil
        /// </summary>
        internal void SetSelectableToAllSequenceFil()
        {
            SetSelectableList(Tsunami2.Properties.FlattenedCompositeElements.OfType<SequenceFil>().ToList<TsuObject>());
            _currentSetSelectableListType = currentSetSelectableListType.setSelectableToAllSequenceFil;
        }
        /// <summary>
        /// Set the selectable list to all sequence of niveau
        /// </summary>
        internal void SetSelectableToAllSequenceNiv()
        {
            SetSelectableList(Tsunami2.Properties.FlattenedCompositeElements.OfType<SequenceNiv>().ToList<TsuObject>());
            _currentSetSelectableListType = currentSetSelectableListType.setSelectableToAllSequenceNiv;
        }
        /// <summary>
        /// Set the selectable list to all sequence tour horizon
        /// </summary>
        internal void SetSelectableToAllSequenceTH()
        {
            var sequences = Tsunami2.Properties.FlattenedCompositeElements.OfType<SequenceTH>().ToList<TsuObject>();
            SetSelectableList(sequences);
            _currentSetSelectableListType = currentSetSelectableListType.setSelectableToAllSequenceTH;
        }
        /// <summary>
        /// select one point (no multi-selection) from 
        /// </summary>
        /// <param name="titleAndMessage"></param>
        /// <returns></returns>
        internal Point SelectOnePoint(string titleAndMessage = null)
        {
            //Tsunami2.Properties.Points.CleanTaggedAsTemp();

            if (titleAndMessage == null) titleAndMessage = $"{R.T_ELEMENTS};{R.T_PLEASE_CHOOSE_ONE_POINT_IN_THE_LIST}";

            SetSelectableToAllUniquePoint();
            View.buttons.HideOnlyButtons(View.buttons.sequence);
            _actualElement = View.SelectOne(titleAndMessage, View.ElementShownInListTypes.UniquePoints);
            //Remove send message, because validateSlection also send the same message to observers
            if (_actualElement != null)
            {
                SendMessage(_actualElement);
                // to send message also to observers
                ValidateSelection();
            }
            View.buttons.ShowAllButtons();
            return (Point)_actualElement;
        }

        public void ProposeFile()
        {
            List<TheoreticalElement> filesFound;
            if (AllElements != null)
                if (AllElements.Count == 0)
                {
                    filesFound = FindOpenedFiles();

                    if (filesFound.Count == 1)
                    {
                        string titleAndMessage = $"{R.T_CONTINUE_WITH_SAME_THEORETICAL_FILE}?;  {R.T_TSUNAMI_FOUND_THIS_FILE}: '{filesFound[0]._Name}' {R.T_IN_ANOTHER_MODULE}, {R.T_DO_YOU_WANT_TO_USE_IT_IN_THIS_MODULE}?";
                        MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                        {
                            ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                        };
                        if (mi.Show().TextOfButtonClicked == R.T_YES)
                        {
                            AddTheoreticalElement(filesFound[0]);
                        }
                    }
                }
        }




        public List<TheoreticalElement> FindOpenedFiles()
        {
            List<TheoreticalElement> filesFound = new List<TheoreticalElement>();
            foreach (var item in Tsunami2.Properties.childModules)
            {
                if (item is FinalModule)
                    foreach (TheoreticalElement entry in (item as FinalModule)._ElementManager.filesOpened)
                        filesFound.Add(entry);
                if (item is Module)
                    foreach (TheoreticalElement entry in (item as Module).filesOpened)
                        filesFound.Add(entry);

            }
            return filesFound;
        }

        internal CompositeElement SelectPoints(string titleAndMessage = null, string buttonValid = "Valid", string buttonCancel = "Cancel", bool selectOnlyPointsForWire = false, bool selectOnlyAlesage = false)
        {

            //Tsunami2.Properties.Points.CleanTaggedAsTemp();

            buttonCancel = buttonCancel == "" ? R.T_CANCEL : buttonCancel;
            buttonValid = buttonValid == "" ? R.T_VALIDATE : buttonValid;
            if (!selectOnlyPointsForWire && !selectOnlyAlesage) SetSelectableToAllUniquePoint();
            //if (selectOnlyAlesage){ this.SetSelectableListToAllAlesages(); }
            CompositeElement e = View.SelectSeveral(titleAndMessage, buttonValid, buttonCancel, 0, selectOnlyPointsForWire, selectOnlyAlesage);
            if (e != null)
            {
                if (e.Elements.Count > 0)
                {
                    if (e.Elements.Count == 1 && e.Elements[0] is CompositeElement)
                    {
                        _actualElement = e.Elements[0];
                    }
                    else
                    {


                        _actualElement = e;
                    }
                    //Remove send message, because validateSlection also send the same message to observers
                    //SendMessage(e);
                }
                else
                    return null;
            }
            else
            {
                return null;
            }
            // to send message also to observers
            ValidateSelection();
            View.buttons.ShowAllButtons();

            // cahnge time to be able to find by last usage
            if (_actualElement != null)
            {
                int count = 1;
                int secondOffset = 0;
                foreach (Element item in _actualElement)
                {
                    if (item is Point)
                    {
                        Point pt = item as Point;
                        if (pt.Date.Hour == 0 && pt.Date.Minute == 0 && pt.Date.Second == 0)
                        {
                            if (count > 999)
                            {
                                count = 0;
                                secondOffset++;
                            }
                            pt.Date = new DateTime(pt.Date.Year, pt.Date.Month, pt.Date.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second + secondOffset, count);
                            count++;
                        }
                    }
                }
            }

            return _actualElement as CompositeElement; ;
        }
        /// <summary>
        /// Select one sequence Fil
        /// </summary>
        /// <param name="titleAndMessage"></param>
        /// <param name="buttonValid"></param>
        /// <param name="buttonCancel"></param>
        /// <param name="filAlreadySelected"></param>
        /// <returns></returns>
        internal SequenceFil SelectOneSequenceFil(string titleAndMessage = null, string buttonValid = "", string buttonCancel = "", SequenceFil filAlreadySelected = null)
        {
            buttonCancel = buttonCancel == "" ? R.T_CANCEL : buttonCancel;
            buttonValid = buttonValid == "" ? R.T_VALIDATE : buttonValid;
            SetSelectableToAllSequenceFil();
            View.buttons.ShowOnlyButtons(View.buttons.sequence);
            View.ShowAsListView();
            _actualElement = View.SelectOne(titleAndMessage);
            // to send message also to observers
            ValidateSelection();
            View.buttons.ShowAllButtons();
            return _actualElement as SequenceFil;
        }
        /// <summary>
        /// Select one sequence niv
        /// </summary>
        /// <param name="titleAndMessage"></param>
        /// <param name="buttonValid"></param>
        /// <param name="buttonCancel"></param>
        /// <param name="filAlreadySelected"></param>
        /// <returns></returns>
        internal SequenceNiv SelectOneSequenceNiv(string titleAndMessage = null, string buttonValid = "", string buttonCancel = "", SequenceNiv filAlreadySelected = null)
        {

            buttonCancel = buttonCancel == "" ? R.T_CANCEL : buttonCancel;
            buttonValid = buttonValid == "" ? R.T_VALIDATE : buttonValid;
            SetSelectableToAllSequenceNiv();
            View.buttons.ShowOnlyButtons(View.buttons.sequence);
            View.ShowAsListView();
            _actualElement = View.SelectOne(titleAndMessage);
            // to send message also to observers
            ValidateSelection();
            View.buttons.ShowAllButtons();
            return _actualElement as SequenceNiv;
        }

        internal SequenceTH SelectOneTheodoliteSequence(string titleAndMessage = null, string buttonValid = "", string buttonCancel = "", SequenceNiv filAlreadySelected = null)
        {

            buttonCancel = buttonCancel == "" ? R.T_CANCEL : buttonCancel;
            buttonValid = buttonValid == "" ? R.T_VALIDATE : buttonValid;
            V.Interface startingStrategy = View.currentStrategy;

            View.ShowAsTreeView();
            SetSelectableToAllSequenceTH();
            if (SelectableObjects.Count > 0)
            {
                _SelectedObjects.Clear();
                _SelectedObjects.Add(SelectableObjects.Last());
            }

            View.buttons.ShowOnlyButtons(View.buttons.sequence);
            View.AutoCheckChildren = false;
            MultiSelection = false;
            _actualElement = View.SelectOne(titleAndMessage, View.ElementShownInListTypes.Sequences);

            // to send message also to observers
            ValidateSelection();

            // Restore previous look
            View.buttons.ShowAllButtons();
            SelectableObjects.Clear();
            View.currentStrategy = startingStrategy;
            return _actualElement as SequenceTH;
        }

        /// <summary>
        /// Allow to select composite element from the list, the integer give is use to hide a level in the hiereachy, I.E. if name is 'LHC.MQ.4R8.E' int= 1 will show 'LHC.MQ.4R8'
        /// </summary>
        /// <param name="titleAndMessage"></param>
        /// <param name="numberOfLevelToskipInTheHierarchy"></param>
        /// <returns></returns>
        internal Magnet SelectOneMagnet(string titleAndMessage = "")
        {
            //Tsunami2.Properties.Points.CleanTaggedAsTemp();


            titleAndMessage = titleAndMessage == "" ? titleAndMessage = $"{R.T_MAGNETS};{R.T_PLEASE_CHOOSE_MAGNET_IN_THE_LIST}" : titleAndMessage;
            SetSelectableToAllMagnets();

            _actualElement = View.SelectOne(titleAndMessage);
            if (_actualElement != null)
                SendMessage(_actualElement);
            return (Magnet)_actualElement;
        }

        internal Magnet SelectMagnets(string titleAndMessage = "", string buttonValid = "", string buttonCancel = "")
        {

            //Tsunami2.Properties.Points.CleanTaggedAsTemp();

            titleAndMessage = titleAndMessage == "" ? titleAndMessage = $"{R.T_MAGNETS};{R.T_PLEASE_CHOOSE_MAGNET_IN_THE_LIST}" : titleAndMessage;
            buttonValid = buttonValid == "" ? buttonValid = R.T_VALIDATE : buttonValid;
            buttonCancel = buttonCancel == "" ? buttonCancel = R.T_CANCEL : buttonCancel;
            MultiSelection = true;
            View.CheckBoxesVisible = true;
            SetSelectableToAllMagnets();
            CompositeElement e = View.SelectSeveral(titleAndMessage, buttonValid, buttonCancel) as CompositeElement;
            if (e != null)
            {
                if (e.Elements.Count > 0)
                {
                    _actualElement = e;
                    SendMessage(e);
                }
                else
                    _actualElement = null;
            }
            else
            {
                _actualElement = null;
            }
            return _actualElement as Magnet;
        }



        public Element GetElementByName(string key)
        {
            Element temp = null;
            foreach (IElement e in AllElements)
            {
                if (e != null)
                    if (e._Name == key)
                        temp = e as Element;
                    else
                        temp = e.GetElementByName(key) as Element;
            }
            return temp;
        }
        public List<Point> GetPointsInAllElements()
        {
            return Tsunami2.Properties.Points;
        }

        public List<string> GetPointsNames()
        {
            List<string> l = new List<string>();
            foreach (Point item in GetPointsInAllElements())
            {
                l.Add(item._Name);
            }
            return l;
        }

        public List<Element> GetAllElements()
        {
            List<Element> l = new List<Element>();
            foreach (Element item in AllElements)
            {
                l.AddRange(item.GetAllSubElements());
            }
            return l;
        }





        public CloneableList<Point> GetPointsInSelectedObjects()
        {
            CloneableList<Point> l = new CloneableList<Point>();
            foreach (TsuObject item in _SelectedObjects)
            {
                if (item is Element)
                {
                    l.AddRange((item as Element).GetPoints());
                }
            }
            return l;
        }
        public CloneableList<CompositeElement> GetCompositeElementsInSelectedObjects()
        {
            CloneableList<CompositeElement> l = new CloneableList<CompositeElement>();
            foreach (TsuObject item in _SelectedObjects)
            {
                if (item is CompositeElement)
                {
                    l.Add(item as CompositeElement);
                }
            }
            return l;
        }
        public CloneableList<Magnet> GetMagnetsInSelectedObjects()
        {
            CloneableList<Magnet> l = new CloneableList<Magnet>();
            foreach (TsuObject item in _SelectedObjects)
            {
                if (item is Magnet)
                {
                    l.Add(item as Magnet);
                }
            }
            return l;
        }
        /// <summary>
        /// Get all the points coming from a theoretical file
        /// </summary>
        /// <returns></returns>
        public List<Point> GetAllTheoreticalPoints()
        {

            List<Point> allPoints = new List<Point>();
            List<Point> theoPoints = new List<Point>();
            theoPoints = GetPointsInAllElements();
            foreach (var item in theoPoints)
            {
                if (item is Point)
                {
                    Point point = item as Point;
                    if (point._OriginType == ElementType.TheoreticalFile)
                    {
                        theoPoints.Add(point);
                    }
                }
            }
            return theoPoints;
        }
        /// <summary>
        /// Get all sequences fil
        /// </summary>
        /// <returns></returns>
        public List<SequenceFil> GetAllSequenceFil()
        {
            List<TsuObject> listTsuObject = new List<TsuObject>();
            List<SequenceFil> allSequenceFil = new List<SequenceFil>();
            listTsuObject = View.AllElements.Where(x => x.GetType() == typeof(SequenceFil)).ToList();
            foreach (var item in listTsuObject)
            {
                if (item is SequenceFil)
                {
                    allSequenceFil.Add(item as SequenceFil);
                }
            }
            return allSequenceFil;
        }
        /// <summary>
        /// Get all sequences level
        /// </summary>
        /// <returns></returns>
        public List<SequenceNiv> GetAllSequenceNiv()
        {
            List<TsuObject> listTsuObject = new List<TsuObject>();
            List<SequenceNiv> allSequenceNiv = new List<SequenceNiv>();
            listTsuObject = View.AllElements.Where(x => x.GetType() == typeof(SequenceNiv)).ToList();
            foreach (var item in listTsuObject)
            {
                if (item is SequenceNiv)
                {
                    allSequenceNiv.Add(item as SequenceNiv);
                }
            }
            return allSequenceNiv;
        }
        public CompositeElement GetTheoriticalElementKnown()
        {
            CompositeElement e = new CompositeElement();
            foreach (TsuObject item in AllElements)
            {
                e.Add(item as Element);
            }
            return e;
        }

        /// <summary>
        /// this method will load the manager with all the coreeponding element from all the module present in his parent module
        /// </summary>
        public override void ShowAllElementIncludedInParentModule()
        {
            base.ShowAllElementIncludedInParentModule();
            AllElements.Clear();
            foreach (M.Module module in ParentModule.childModules)
            {
                if (module is FinalModule)
                    AllElements.AddRange((module as FinalModule)._ElementManager.AllElements);
            }
        }

        #endregion

        #region Edit and compare
        // Point Comparaison
        internal void Edit(Element element)
        {
            if (element is Point)
            {
                Point p = element as Point;
                if (!p._Coordinates.HasAny)
                {
                    p._Coordinates.Local = new Coordinates(p._Name, 0, 0, 0);
                }

            }
            Module editModule = new Module(this);
            editModule.SelectableObjects.Clear();
            editModule.SelectableObjects.Add(element);

            editModule.View.ChangeStrategy(V.Types.EditableList);
            editModule.UpdateView();
            editModule.View.EditPoint();
            View.UpdateView();
        }

        internal void Compare2(List<Element> l)
        {
            int count = l.Count;
            string content = "Point comparison:\r\n\r\n";
            content += "                 Name         Xloc         Yloc         Zloc         Xphy         Yphy         Zphy         Xccs         Yccs         Zccs                \r\n\r\n";
            for (int i = 0; i < count; i++)
            {
                for (int j = i + 1; j < count; j++)
                {
                    Point temp = Analysis.Element.CompareTwoPoints(l[i] as Point, l[j] as Point);
                    content += T.Conversions.FileFormat.ToStringWithAllFrame(l[i] as Point) + " from " + l[i]._Origin + "\r\n";
                    content += T.Conversions.FileFormat.ToStringWithAllFrame(l[j] as Point) + " from " + l[j]._Origin + "\r\n";
                    content += T.Conversions.FileFormat.ToStringWithAllFrame(temp) + "\r\n";
                    content += "\r\n";
                }
            }
            string path = P.Preferences.Instance.Paths.Temporary + T.Conversions.Date.ToDateUpSideDown(DateTime.Now) + ".txt";
            File.WriteAllText(path, content);
            Shell.Run(path);
        }

        internal void Compare(List<Element> l)
        {
            // I want here to allow a special comparaison, if there is more 10 points selected from 2 differents groupes

            if (BigCompare(l))
                return;

            List<Point> diffs = Analysis.Element.Compare(l.Cast<Point>().ToList());

            Module mm = new Module(this);
            mm.AllElements.AddRange(diffs);
            mm.SelectableObjects.Clear();
            mm.SelectableObjects.AddRange(diffs);
            mm.View.UpdateView();
            mm.View.ShowInMessageTsu(R.T_COORDINATE_DIFF, R.T_OK, null);
            foreach (var item in diffs)
            {
                mm.AllElements.Remove(item);
            }
        }

        /// <summary>
        /// special comparaison, if there is more 10 points selected from 2 differents groupes
        /// it can be use for example to compare 2 set of coordinates that do not have the same exact list
        /// it will create a list with all the unique point there value in both groups and the differences.
        /// </summary>
        /// <param name="l"></param>
        /// <returns></returns>
        private bool BigCompare(List<Element> l)
        {
            if (l == null) return false;
            if (l.Count < 20) return false;

            // lets check if there is 2 groups
            bool twoGroupsFound = false;
            string firstGroup = l[0]._Origin;
            foreach (Element e in l)
            {
                if (firstGroup != e._Origin)
                {
                    twoGroupsFound = true;
                    break;
                }
            }
            if (!twoGroupsFound)
                return false;

            // from here we deduct that we want to ask to use if he want to make this special comparison
            MessageInput mi = new MessageInput(MessageType.Choice, "Special comparison?;You choose several points (>20) from 2 differents groups." + "\r\n" +
                                                                   "Tsunami is proposing you to compare the 2 groups to find common names, show a single table with coord. and offsets" + "\r\n" +
                                                                   "")
            {
                ButtonTexts = new List<string> { R.T_YES, R.T_NO }
            };
            string r = mi.Show().TextOfButtonClicked;
            if (r == R.T_NO)
                return false;

            // lets do it
            List<string> uniqueNames = new List<string>();
            List<CompositeElement> groups = new List<CompositeElement>();
            foreach (Element e in l)
            {
                foreach (Point p in e.GetPoints())
                {
                    // look for uniqueness of name
                    string name = p._Name;
                    if (!uniqueNames.Contains(name))
                        uniqueNames.Add(name);

                    CompositeElement currentGroup = null;

                    // check if group exist
                    string groupName = p._Origin;
                    if (groupName.Contains("\\"))
                    {
                        FileInfo fileInfo = new FileInfo(groupName);
                        groupName = fileInfo.Name;

                    }
                    groupName = groupName.Replace(' ', '_');
                    foreach (var group in groups)
                    {
                        if (group._Name == groupName)
                        {
                            currentGroup = group;
                            break;
                        }
                    }

                    // create group
                    if (currentGroup == null)
                    {
                        currentGroup = new CompositeElement(groupName);
                        groups.Add(currentGroup);
                    }

                    // Add item to right group
                    currentGroup.Add(p);
                }
            }

            // we have the list of names and the groups

            // create groups with all the unique points
            List<CompositeElement> filledGroups = new List<CompositeElement>();
            foreach (var group in groups)
            {
                filledGroups.Add(new CompositeElement(group._Name));
            }

            // get existing point from existing groups or create a new one empty
            int numberOfGroups = groups.Count;
            foreach (string name in uniqueNames)
            {
                for (int i = 0; i < numberOfGroups; i++)
                {
                    Point p = null;
                    List<Element> a = GetElementByName(groups[i], name);
                    if (a.Count > 1)
                    {
                        string titleAndMessage = $"{R.T_ATTENTION};Multiple {name} {R.T_IN} {groups[i]._Name}";
                        new MessageInput(MessageType.Warning, titleAndMessage).Show();
                    }

                    if (a.Count > 0)
                        p = a[0] as Point;

                    if (p == null)
                        p = new Point(name);

                    filledGroups[i].Add(p);
                }
            }
            // we all groups with all unique points


            // Create difference
            List<CompositeElement> comparedGroups = new List<CompositeElement>();
            for (int i = 0; i < numberOfGroups - 1; i++) // -1 because the last one as already been compared to all the others
            {
                for (int j = i + 1; j < numberOfGroups; j++)
                {
                    CompositeElement c = new CompositeElement(filledGroups[i]._Name + " - " + filledGroups[j]._Name);
                    for (int k = 0; k < filledGroups[i].Elements.Count; k++)
                    {
                        Point p = Analysis.Element.CompareTwoPoints(filledGroups[i].Elements[k] as Point, filledGroups[j].Elements[k] as Point);
                        c.Add(p);
                    }
                    comparedGroups.Add(c);
                }
            }

            // Create a text files
            int columnSizeForNumeric = 15;
            int columnSizeForId = 30;

            // Headers
            string headersLine = string.Format("{0,-" + columnSizeForId + "}", "Id");
            List<string> headers = new List<string>() { "Xccs", "Yccs", "Zccs", "H", "Xsu", "Ysu", "Zsu", "-" };
            foreach (var g in filledGroups)
            {
                foreach (var h in headers)
                {
                    headersLine += string.Format("{0," + columnSizeForNumeric.ToString() + "}", h + "[m]");
                }
            }
            foreach (var g in comparedGroups)
            {
                foreach (var h in headers)
                {
                    headersLine += string.Format("{0," + columnSizeForNumeric.ToString() + "}", h + "[mm]");
                }
            }

            // Titles
            string titlesLine = string.Format("{0,-" + columnSizeForId + "}", "Created_by_Tsunami");
            int totalLengthOfHeaders = headers.Count * columnSizeForNumeric;

            foreach (var g in filledGroups)
            {
                string s = g._Name;
                for (int i = 0; i < headers.Count - 1; i++) // -1 because the name og the group is use one of teh comuln for excel
                {
                    s += " <"; // adding some symbol seperated with space for excel compatibilt�
                }
                titlesLine += string.Format("{0,-" + totalLengthOfHeaders + "}", s);

            }
            foreach (var g in comparedGroups)
            {
                titlesLine += string.Format("{0,-" + totalLengthOfHeaders + "}", g._Name);
            }

            string content = "";
            content += titlesLine + "\r\n";
            content += headersLine + "\r\n";

            for (int i = 0; i < uniqueNames.Count; i++)
            {
                content += string.Format("{0,-30}", uniqueNames[i]);
                for (int j = 0; j < filledGroups.Count; j++)
                {
                    content += (filledGroups[j].Elements[i] as Point)._Coordinates.Ccs.ToString("", columnSizeForNumeric);
                    content += (filledGroups[j].Elements[i] as Point)._Coordinates.Su.ToString("", columnSizeForNumeric);
                }
                for (int j = 0; j < comparedGroups.Count; j++)
                {
                    content += (comparedGroups[j].Elements[i] as Point)._Coordinates.Ccs.ToString("mm", columnSizeForNumeric);
                    content += (comparedGroups[j].Elements[i] as Point)._Coordinates.Su.ToString("mm", columnSizeForNumeric);
                }
                content += "\r\n";
            }


            // write and open
            string description = "";
            foreach (var item in groups)
            {
                description += item._Name;
                if (item != groups[groups.Count - 1])
                    description += " vs ";
            }
            string path = $"{Tsunami2.Preferences.Values.Paths.Temporary}{T.Conversions.Date.ToDateUpSideDown(DateTime.Now)}.txt";
            File.WriteAllText(path, content);
            Shell.ExecutePathInDialogView(path, description);
            return true;
        }

        private List<Element> GetElementByName(CompositeElement compositeElement, string name)
        {
            return compositeElement.Elements.FindAll(x => x._Name == name);
        }

        #endregion

        #region  Open, export and save

        private List<TsuObject> GetElementToExport()
        {
            List<TsuObject> l;
            if (_SelectedObjects.Count > 0)
                l = _SelectedObjects;
            else
            {
                l = new List<TsuObject>();
                l.AddRange(GetPointsInAllElements());
            }
            return l;
        }



        public void SaveCCSInIdxyzFormat(string path, List<Point> points, Coordinates.ReferenceSurfaces surf)
        {
            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine(R.T276);
                sw.WriteLine($"Z value were determine with the reference surface '{surf}' if not directly imported by the user");
                string group = "";
                foreach (Point item in points)
                {
                    if (item._Origin != group)
                    {
                        sw.WriteLine($"% Tsunami 'group/origin': {item._Origin}");
                    }
                    group = item._Origin;
                    // sw.WriteLine(T.Conversions.FileFormat.ToCCSIdXYZString(item));
                }
                sw.Dispose();
            }
        }

        public static bool SaveInAllFormat(List<Point> points, out List<string> paths, string forcedPath = "")
        {
            paths = new List<string>();

            string commonPath = (forcedPath != "") ? forcedPath : TsuPath.GetFileNameToSave("", Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now) + "_Coordinates", "*.* | *.*", "Common name for all exported files");

            if (commonPath == "")
                return false;

            string path;

            // coordinates from the dictionnary
            foreach (var cs in Tsunami2.Properties.CoordinatesSystems.AllTypes)
            {
                path = commonPath + "." + cs._Name + ".txt";
                bool somethingWritten = false;

                using (StreamWriter sw = new StreamWriter(path))
                {
                    sw.WriteLine(R.T276);
                    string group = "";
                    foreach (Point point in points)
                    {
                        if (point._Origin != group)
                        {
                            sw.WriteLine($"% Tsunami 'group/origin': {point._Origin}");
                        }
                        group = point._Origin;
                        if (T.Conversions.FileFormat.ToIdXYZString(point, cs, out string line))
                        {
                            sw.WriteLine(line);
                            somethingWritten = true;
                        }
                    }
                }
                if (!somethingWritten)
                    File.Delete(path);
                else
                    paths.Add(path);

            }

            // NP point un Geode format
            path = commonPath + ".NP.CCS.txt";
            paths.Add(path);
            Geode.ExportInNewPointFormat(path, points);

            // GEODE FORMAT
            path = commonPath + ".CCS.dat";
            paths.Add(path);
            Geode.Export(path, points);
            return true;
        }
        public void SaveInIdxyzFormat(string path, List<Point> points)
        {
            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine(R.T276);
                string group = "";
                foreach (Point item in points)
                {
                    if (item._Origin != group)
                    {
                        sw.WriteLine($"% Tsunami 'group/origin': {item._Origin}");
                    }
                    group = item._Origin;
                    sw.WriteLine(T.Conversions.FileFormat.ToIdXYZString(item, Coordinates.ReferenceFrames.MLA1985Machine));
                }
                sw.Dispose();
            }
        }


        public void SaveInIdxyzCustomFormat(string path, List<Point> points, ref string fileName)
        {
            foreach (var cs in Tsunami2.Properties.CoordinatesSystems.GetCustoms())
            {
                string correctedPath = path.Replace(".xxx", $".{cs._Name}.txt");
                using (StreamWriter sw = new StreamWriter(correctedPath))
                {
                    sw.WriteLine(R.T276 + $"({cs.LongName} system)");
                    string group = "";
                    foreach (Point item in points)
                    {
                        if (item._Origin != group)
                        {
                            sw.WriteLine($"% Tsunami 'group/origin': {item._Origin}");
                        }
                        group = item._Origin;
                        sw.WriteLine(T.Conversions.FileFormat.ToIdXyzString(item._Name, item._Coordinates.GetCoordinatesInASystemByName(cs._Name)));
                    }
                    sw.Dispose();
                }
            }
        }

        public void SaveInIdxyzPhysicistFormat(string path, List<Point> points)
        {
            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine(R.T276 + "(Physicist system)");
                string group = "";
                foreach (Point item in points)
                {
                    if (item._Origin != group)
                    {
                        sw.WriteLine($"% Tsunami 'group/origin': {item._Origin}");
                    }
                    group = item._Origin;
                    //sw.WriteLine(T.Conversions.FileFormat.ToIdXYZPhysicistString(item));
                }
                sw.Dispose();
            }
        }

        public void SaveInIdxyzCCSFormat(string path, List<Point> points)
        {
            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine(R.T276 + "(CERN coordinate system)");
                string group = "";
                foreach (Point item in points)
                {
                    if (item._Origin != group)
                    {
                        sw.WriteLine($"% Tsunami 'group/origin': {item._Origin}");
                    }
                    group = item._Origin;
                    //sw.WriteLine(T.Conversions.FileFormat.ToIdXyzCcsString(item));
                }
                sw.Dispose();
            }
        }

        #endregion

        #region Coordinates Transformations

        Module elementManagerForPassivepoint;

        internal void ComputeWithChaba(List<Point> points, string chabaName = null)
        {
            if (elementManagerForPassivepoint == null) elementManagerForPassivepoint = new Module(this);
            elementManagerForPassivepoint.SelectableObjects.Clear();
            elementManagerForPassivepoint.SetSelectableToAllPoints();
            Element e = elementManagerForPassivepoint.View.SelectSeveral($"{R.T_SELECT_PASSIVE_POINTS};{R.T_SELECT_THE_POINT_TU_USE_IN_PASSIVE}",
                R.T_OK, R.T_CANCEL, 0, false, false, "Chaba");
            if (e == null) return;

            List<string> createdFileFullPaths;

            List<Point> newPoints = Systems.BasedOnCommonPoints(points, e.GetPoints(), out createdFileFullPaths, chabaName);
            if (newPoints != null && newPoints.Count > 0)
            {
                string cancel = R.T_Reject;
                string message = $"{R.T_CHABA_RESULTS};";
                foreach (var item in createdFileFullPaths)
                {
                    message += MessageTsu.GetLink(item) + "\r\n";
                }
                foreach (var item in newPoints)
                {
                    message += item.ToString("SU") + "\r\n";
                }
                message += R.T_CSGEO_IMPORT;
                MessageInput mi = new MessageInput(MessageType.Choice, message)
                {
                    ButtonTexts = new List<string> { R.T_IMPORT, cancel }
                };
                if (mi.Show().TextOfButtonClicked != cancel)
                {
                    // time to compute in other systems
                    try
                    {
                        Zone z = Tsunami2.Preferences.Tsunami.Zone;

                        CompositeElement c = new CompositeElement();
                        foreach (var item in newPoints)
                        {
                            item.StorageStatus = Tsunami.StorageStatusTypes.Keep;
                            c.Elements.Add(item);
                        }
                        Coordinates.ReferenceSurfaces rs =
                            z != null ? z.Ccs2MlaInfo.ReferenceSurface :
                            Coordinates.ReferenceSurfaces.Plane;
                        Coordinates.ReferenceFrames inputFR = Coordinates.GetReferenceFrame(rs, Coordinates.CoordinateSystemsTypes._3DCartesian, Coordinates.CoordinateSystemsTsunamiTypes.SU);

                        if (inputFR == Coordinates.ReferenceFrames.Unknown)
                        {
                            inputFR = Coordinates.GetReferenceFrameFromExistingCoordinates(newPoints);
                        }

                        TryToFillMissingCoordinates(c, inputFR, z);
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    AllElements.AddRange(newPoints);
                    //Tsunami2.Properties.Points
                    SelectableObjects.AddRange(newPoints);
                    View.UpdateView();
                }
            }
        }
        internal void OpenCsGeoSurveyPadPluginWithPoints(List<Point> points, string name, Coordinates.CoordinateSystemsTsunamiTypes frameType)
        {
            Action<string[]> whatToDoAfterUIClosure = (paths) =>
            {
                string message = "New files available;Those files are results of a CSGEO transformation initiated from tsunami:\r\n";
                foreach (var path in paths)
                {
                    if (Path.GetExtension(path).ToUpper() != ".CSGEO")
                        message += "\t" + $"{MessageTsu.GetLink(path)}{Environment.NewLine}";
                }
                new MessageInput(MessageType.GoodNews, message).Show();
            };
            CsGeo.GUI(points, frameType, whatToDoAfterUIClosure, zone);
        }

        #endregion

        #region Parametric Shapes Fitting
        /// <summary>
        /// Check if the number of point required is reached
        /// </summary>
        internal CompositeElement FitCheck1(List<Point> list, string shapeName, int minPointRequired, int minPointAdviced)
        {
            string s;
            if (list.Count < minPointRequired)
            {
                s = R.T279;
                s = s.Replace("<p1>", minPointRequired.ToString());
                s = s.Replace("<p2>", minPointAdviced.ToString());
                s = s.Replace("<p3>", shapeName);
                throw new Exception(s);
            }
            s = "<p3>";
            s = s.Replace("<p3>", shapeName);
            return T.Conversions.Composite.ToCompositeElement(list, s);
        }

        internal void FitLine(List<Point> list)
        {
            CompositeElement pointsToFit = FitCheck1(list, "Line", 2, 5);
            FittedShape fitted = GeoFit.Line3D(pointsToFit);
            SaveFittedShape(fitted);
        }
        internal void FitPlane(List<Point> list)
        {
            CompositeElement pointsToFit = FitCheck1(list, "Plane", 3, 5);
            FittedShape fitted = GeoFit.Plane(pointsToFit);
            SaveFittedShape(fitted);
        }
        internal void FitCircle(List<Point> list)
        {
            CompositeElement pointsToFit = FitCheck1(list, "Circle", 3, 5);
            FittedShape fitted = GeoFit.Circle(pointsToFit);
            SaveFittedShape(fitted);
        }
        internal void FitSphere(List<Point> list)
        {
            CompositeElement pointsToFit = FitCheck1(list, "Sphere", 4, 7);
            FittedShape fitted = GeoFit.Sphere(pointsToFit);
            SaveFittedShape(fitted);
        }
        internal void FitCylinder(List<Point> list)
        {
            CompositeElement pointsToFit = FitCheck1(list, "Cylinder", 5, 7);
            FittedShape fitted = GeoFit.Cylinder(pointsToFit);
            SaveFittedShape(fitted);
        }

        private void SaveFittedShape(FittedShape fitted)
        {
            AddElement(_Name, fitted, true);
            UpdateView();
        }

        #endregion

        /// <summary>
        /// Return most existing zone from pilier and if no piliers most existing accelerator name
        /// </summary>
        /// <returns></returns>
        internal string GetMostCommonZone()
        {

            Dictionary<string, int> occurencesACC = new Dictionary<string, int>();
            Dictionary<string, int> occurencesZONE = new Dictionary<string, int>();
            foreach (Point item in GetPointsInAllElements())
            {
                if (occurencesACC.Keys.Contains(item._Accelerator))
                {
                    occurencesACC[item._Accelerator]++;
                }
                else
                {
                    occurencesACC.Add(item._Accelerator, 0);
                }

                string zoneforSure = item.ZoneForSure;
                if (zoneforSure != R.String_Unknown)
                {
                    if (occurencesZONE.Keys.Contains(zoneforSure))
                    {
                        occurencesZONE[zoneforSure]++;
                    }
                    else
                    {
                        occurencesZONE.Add(zoneforSure, 0);
                    }
                }
            }

            string s = "";
            int maxZONE = 0;
            foreach (var item in occurencesZONE)
            {
                if (item.Value > maxZONE)
                {
                    maxZONE = item.Value;
                    s = item.Key;
                }
            }

            if (s == "")
            {
                int maxACC = 0;
                foreach (var item in occurencesACC)
                {
                    if (item.Value > maxACC)
                    {
                        maxACC = item.Value;
                        s = item.Key;
                    }
                }
            }

            return s;
        }

        static List<Point> AlreadyAvailableBeamPoints = new List<Point>();

        internal static List<Point> GetTheoreticalPointsWithEquivalentNames(List<Point> models, bool beamPointsOnly = true)
        {
            List<Point> points = new List<Point>();
            var existingGeodeFiles = Tsunami2.Properties.BOC_Context.SelectedGeodeFilePaths;

            var alreadyAvailableBeamPoints = AlreadyAvailableBeamPoints;
            // Get already read beam Points
            if (alreadyAvailableBeamPoints.GetAllPointsWithEquivalentNames(models, out points))
                return points;

            for (int i = existingGeodeFiles.Count - 1; i > -1; i--)
            {
                var geodeFile = existingGeodeFiles[i];
                alreadyAvailableBeamPoints.AddRangeIfDoNotExistWithSameName(GetTheoreticalPoints_FromFilePath(geodeFile, beamPointsOnly: beamPointsOnly));
                if (alreadyAvailableBeamPoints.GetAllPointsWithEquivalentNames(models, out points))
                    return points;
            }

            // Ask for a addition file until we found them?
            while (models.Count - points.Count > 0)
            {
                var geodePathPath = TsuPath.GetFileNameToOpen(null, Tsunami2.Preferences.Values.Paths.Data, "", "dat files (*.dat)|*.dat", "Please browse for theoretical beam points, because they are not found in the tsunami project");
                if (geodePathPath == "")
                    throw new Instruments.CancelException();

                Tsunami2.Properties.BOC_Context.SelectedGeodeFilePaths.Add(geodePathPath);
                alreadyAvailableBeamPoints.AddRangeIfDoNotExistWithSameName(GetTheoreticalPoints_FromFilePath(geodePathPath));
                if (alreadyAvailableBeamPoints.GetAllPointsWithEquivalentNames(models, out points))
                    return points;
            }
            return points;
        }


        /// <summary>
        /// Will get the already available beam point, or will try to find and store them from file path already known 
        /// </summary>
        /// <returns></returns>
        internal static List<Point> GetBeamTheoreticalPoints(string idSuAssembly, string forcedPath = "", bool bMustFound = true)
        {
            List<Point> points = new List<Point>();

            var beamAssemblyPoint = "BEAM_" + idSuAssembly;
            var existingGeodeFiles = Tsunami2.Properties.BOC_Context.SelectedGeodeFilePaths;

            var alreadyAvailableBeamPoints = AlreadyAvailableBeamPoints;

            // already known existing?
            points = Point.NameContains(beamAssemblyPoint, alreadyAvailableBeamPoints);
            if (points.Count > 0)
                return points;

            // Get from forced path?
            if (forcedPath != "")
            {
                alreadyAvailableBeamPoints.AddRange(GetTheoreticalPoints_FromFilePath(forcedPath));
                if (Point.NameContains(beamAssemblyPoint, alreadyAvailableBeamPoints).Count > 0)
                    return points;
            }

            // Get from all known opened geode file?
            for (int i = existingGeodeFiles.Count - 1; i > -1; i--)
            {
                var geodeFile = existingGeodeFiles[i];
                alreadyAvailableBeamPoints.AddRange(GetTheoreticalPoints_FromFilePath(geodeFile));
                points = Point.NameContains(beamAssemblyPoint, alreadyAvailableBeamPoints);
                if (points.Count > 0)
                    return points;
            }

            // Ask for a addition file until we found them?
            while (points.Count == 0)
            {
                forcedPath = TsuPath.GetFileNameToOpen(null, Tsunami2.Preferences.Values.Paths.Data, "", "dat files (*.dat)|*.dat", "Please browse for theoretical beam points, because they are not found in the tsunami project");
                if (forcedPath == "")
                    throw new Instruments.CancelException();

                Tsunami2.Properties.BOC_Context.SelectedGeodeFilePaths.AddIfNotExisting(forcedPath);
                alreadyAvailableBeamPoints.AddRange(GetTheoreticalPoints_FromFilePath(forcedPath));
                points = Point.NameContains(beamAssemblyPoint, alreadyAvailableBeamPoints);
            }
            return points;
        }

        private static List<Point> GetTheoreticalPoints_FromFilePath(string forcedPath, bool beamPointsOnly = true)
        {
            List<Point> points = new List<Point>();
            foreach (var line in File.ReadAllText(forcedPath).Split('\n'))
            {
                if (!line.Contains(';'))
                    continue;
                string cleanBeginningOfLine = line.Replace(" ", "").Replace(";", ".");

                if (!beamPointsOnly || (beamPointsOnly && cleanBeginningOfLine.StartsWith($"BEAM_")))
                {
                    Point p = Geode.ReadLine(line);
                    points.Add(p);
                    AlreadyAvailableBeamPoints.AddIfDoNotExistWithSameName(p);
                }
            }
            return points;
        }

        internal Point FindSelectablePointWithSameNameAndOrigin(Point item)
        {
            foreach (TsuObject x in SelectableObjects)
                if ((x is Point p)
                 && (p._Name == item._Name)
                 && (p._Origin == item._Origin))
                    return p;
            return null;
        }

        internal void Intersect()
        {
            if (Common.Compute.Compensations.Lgc2.ComputeIntersection(out var intersectedPoints))
            {
                string titleAndMessage = $"{R.T_IMPORT} points?;{R.T_IMPORT_POINTS_FROM_THE_INTERSECTIONS_COMPUTE} ?";
                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                };
                if (mi.Show().TextOfButtonClicked == R.T_YES)
                    foreach (var item in intersectedPoints)
                    {
                        this.AddElement(item);
                    }
            }
        }

        internal static void TryFillToCoordinates()
        {
            var c = new CompositeElement();
            c.Elements.AddRange(Tsunami2.Properties.Points);
            c.Elements.AddRange(Tsunami2.Properties.HiddenPoints);
            c.Elements.AddRange(Tsunami2.Properties.TempPoints);

            TryToFillMissingCoordinates(c, Coordinates.ReferenceFrames.Unknown, Tsunami2.Properties.Zone);

        }

    }

}
