﻿using TSU.Common.Elements.Composites;

namespace TSU.Common.Elements.Manager
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            return;
            this.Module.View = null;

            if (this.buttons != null)
                this.buttons.Dispose();
            this.buttons = null;


            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this._PanelBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // _PanelBottom
            // 
            this._PanelBottom.Controls.Add(this.label1);
            this._PanelBottom.Location = new System.Drawing.Point(5, 80);
            this._PanelBottom.Size = new System.Drawing.Size(781, 365);
            // 
            // _PanelTop
            // 
            this._PanelTop.Size = new System.Drawing.Size(781, 75);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Location = new System.Drawing.Point(0, 352);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // ElementManagerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(791, 450);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "ElementManagerView";
            this.Controls.SetChildIndex(this._PanelTop, 0);
            this.Controls.SetChildIndex(this._PanelBottom, 0);
            this._PanelBottom.ResumeLayout(false);
            this._PanelBottom.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.Label label1;

    }
}
