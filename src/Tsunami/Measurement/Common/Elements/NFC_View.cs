﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using R = TSU.Properties.Resources;
using NFC = NewFrameCreation_Library;
using System.Windows.Forms;
using C = TSU.Common.Elements.Composites;
using MathNet.Numerics.LinearAlgebra;
using TSU.Common.Elements.Manager;
using TSU.Views;
using TSU.Views.Message;


namespace TSU.Common.Elements
{

    public static class ListExtensions
    {
        public static bool HaveCcsCoordinates(this List<Point> list)
        {
            foreach (Point p in list)
            {
                if (!p._Coordinates.Ccs.AreKnown)
                    return false;
                
            }
            return true;
        }
    }

    public class tsunamiViewForNFC : NFC.IView
    {
        internal Manager.Module ElementManager;
        public tsunamiViewForNFC()
        {

        }

        public tsunamiViewForNFC(Manager.Module em)
        {
            ElementManager = em;
        }


        public NFC.Axis GetAxisOrder(string text)//, NFC.Axis byDefault = NFC.Axis.X)
        {
            NFC.Axis byDefault = (alreadySelectedAxis == NFC.Axis.Unknown) ? NFC.Axis.X : (alreadySelectedAxis == NFC.Axis.X) ? NFC.Axis.Y : NFC.Axis.X;

            BindingSource bindingSource = new BindingSource();
            bindingSource.DataSource = Enum.GetNames(typeof(NFC.Axis)); ;

            // beurk
            NFC.Axis r = (NFC.Axis)Enum.Parse(typeof(NFC.Axis), (string)TSU.Tsunami2.Preferences.Tsunami.View.ShowMessageOfPredefinedInput(
                $"{text};{"What is the first axis to be define?"}", R.T_OK, "", bindingSource, "_Name", null, false, null, byDefault));

            alreadySelectedAxis = r;
            return r;
        }

        public string GetInputFromUser(string message)
        {
            string titleAndMessage = $"Question:;{message}";

            MessageTsu.ShowMessageWithTextBox(titleAndMessage, null, out _, out string textInput, "", true);

            return textInput;
        }

        NFC.Axis alreadySelectedAxis = NFC.Axis.Unknown;
        NFC.Shapes shape1;
        NFC.Shapes shape2;

        public NFC.Shapes GetLineOrPlane()
        {
            BindingSource bindingSource = new BindingSource();
            bindingSource.DataSource = Enum.GetNames(typeof(NFC.Shapes)); ;
            NFC.Shapes r = (NFC.Shapes)Enum.Parse(typeof(NFC.Shapes), (string)TSU.Tsunami2.Preferences.Tsunami.View.ShowMessageOfPredefinedInput(
                $"{"Shape type"};{"What is the shape to fit?"}", R.T_OK, "", bindingSource));

            if (flagToGuessShape1OrShape2_isItShape1)
            {
                // select points for shape1
                SelectPointsAndWriteFile(r, tempPath +  "shape1.txt",1);
                shape1 = r;
            }
            else
            {
                // select points for shape2
                SelectPointsAndWriteFile(r, tempPath + "shape2.txt",2);
                shape2 = r;

                SelectPointsAndWriteFile( NFC.Shapes.Point, tempPath + "origin.txt",0);
            }

            flagToGuessShape1OrShape2_isItShape1 = false;
            return r;
        }

        private List<Elements.Point> SelectPointsAndWriteFile(NFC.Shapes r, string fileName, int shapeNumber)
        {
            string pathToWrite = fileName;
            string message;

            switch (r)
            {
                case NFC.Shapes.Line:
                    message = $"Choose the points defining the line (shape {shapeNumber});The selection order matters!First point is the origin of the line, the second one gives the direction.";

                    break;
                case NFC.Shapes.Plane:
                    message = $"Choose the points defining the plane (shape {shapeNumber}).";
                    break;
                case NFC.Shapes.Point:

                    message = $"Choose the origin of the system.";
                    break;
                case NFC.Shapes.Unknown:

                    message = $"Choose the points to transform.";
                    break;
                default:
                    message = "Choose points";
                    break;
            }

            List<Elements.Point> points = AskForPoints( shapeNumber, message);
            WriteInFileWithTabs(pathToWrite, points);

            return points;
        }

        public bool Happy()
        {
            string titleAndMessage = $"Happy:;{"Are you happy with this result?"}";
            MessageInput mi = new MessageInput(MessageType.GoodNews, titleAndMessage)
            {
                ButtonTexts = new List<string> { R.T_YES, R.T_NO }
            };
            return R.T_YES ==mi.Show().TextOfButtonClicked;
        }

        
        private void WriteInFileWithTabs(string path, List<Point> selectedPoints)
        {
            string content = "";

            bool useCCS = selectedPoints.HaveCcsCoordinates();
            foreach (Point item in selectedPoints)
            {
                if (useCCS)
                {
                    content += item._Coordinates.Ccs.ToString("IDtXtYtZ") + "\r\n";
                }
                else
                    content += item._Coordinates.Su.ToString("IDtXtYtZ") + "\r\n";
            }

            System.IO.File.WriteAllText(path, content);
        }



        //void AskForOrigin(ref string path)
        //{
        //    //select in element manager and write it in a file 'path'

        //    Point p = ElementManager.SelectOnePoint("Choose the origin of the system;You will have possiblity to project it on one of the next fitted shapes");
        //    if (p == null)
        //        throw new Exception("No origin selected");

        //    WriteInFileWithTabs(path, ElementManager.SelectedPoints);
        //}

        List<Elements.Point> AskForPoints( int nb, string message)
        {
            //select in element manager and write it in a file 'path'

            ElementManager.SelectedPoints.Clear();
            C.CompositeElement c = ElementManager.SelectPoints(message);

            if (c == null)
                throw new Exception("No points selected for the line fitting");
            

            return ElementManager.SelectedPoints;
        }
        //void AskForPlanePoints(ref string path, string nb)
        //{
        //    //select in element manager and write it in a file 'path'
            
        //    ElementManager.SelectedPoints.Clear();
        //    C.CompositeElement c = ElementManager.SelectPoints($"Choose the points defining the plane {nb}.");
            
        //    if (c == null)
        //        throw new Exception("No points selected for the line fitting");
            

        //    WriteInFileWithTabs(path, ElementManager.SelectedPoints);
        //}

        //void AskToTransformPoints(ref string path)
        //{
        //    ElementManager._SelectedObjects.Clear();

        //    C.CompositeElement c = ElementManager.SelectPoints("Choose the points to apply the tranasformation to.");

        //    if (c == null)
        //        throw new Exception("No points selected to apply the transformation");
            

        //    WriteInFileWithTabs(path, ElementManager.SelectedPoints);
        //}

        


        public void Introduction(string tempPath)
        {
            // nothing to do
            alreadySelectedAxis = NFC.Axis.Unknown;
            shape1 = NFC.Shapes.Unknown;
            shape2 = NFC.Shapes.Unknown;
            messages.Clear();
        }

        //NFC.Origins SelectOriginProjection(NFC.Shapes shape1, NFC.Shapes shape2)
        //{
        //    NFC.Origins type1 = shape1 == NFC.Shapes.Line ? NFC.Origins.ProjectionOnLine : NFC.Origins.ProjectionOnPlane;

        //    NFC.Origins type2;
        //    if (shape1 != shape2)
        //    {
        //        type2 = shape2 == NFC.Shapes.Line ? NFC.Origins.ProjectionOnLine : NFC.Origins.ProjectionOnPlane;
        //    }
        //    else
        //    {
        //        type2 = shape2 == NFC.Shapes.Line ? NFC.Origins.ProjectionOnSecondLine : NFC.Origins.ProjectionOnSecondPlane;
        //    }

        //    BindingSource bindingSource = new BindingSource();
        //    bindingSource.DataSource = new List<NFC.Origins>() { type1, type2, NFC.Origins.NoProjection };
        //    string s_r =TSU.Tsunami2.TsunamiPreferences.Tsunami.View.ShowMessageOfPredefinedInput(
        //        $"{"Projection of the Origin"};{"Do you want to  project the origin of the new system on one of the fitted shapes?"}",
        //        R.T_OK, "", bindingSource).ToString();
        //    NFC.Origins r = (NFC.Origins)Enum.Parse(typeof(NFC.Origins),s_r );

        //    return r;
        //}

        public void Pause()
        {
            // nothing to do
        }

        public NFC.Axis PrimaryEqualSecondary(NFC.Axis Primary, NFC.Axis Secondary)
        {
            NFC.Axis r = Secondary;

            while (r==Primary)
            {
                BindingSource bindingSource = new BindingSource();
                bindingSource.DataSource = Enum.GetNames(typeof(NFC.Axis)); ;
                r = (NFC.Axis)Enum.Parse(typeof(NFC.Axis), (string)TSU.Tsunami2.Preferences.Tsunami.View.ShowMessageOfPredefinedInput(
                    $"{"Primary == Secondary"}!;{"Primary and Secondary axes are the same! Change Secondary axis!"}", R.T_OK, "", bindingSource));
                
            }

            return r;
        }

        bool gatherMessages = true;
        List<string> messages = new List<string>();

        public void ShowMessage(string text)
        {
            if (gatherMessages)
            {
                messages.Add(text);
            }
            else
            {
                string titleAndMessage = $"Info:;{text}";
                new MessageInput(MessageType.GoodNews, titleAndMessage).Show();
            }
            
        }

        bool flagToGuessShape1OrShape2_isItShape1;
        string tempPath;

        public  string GetTempPath()
        {
            flagToGuessShape1OrShape2_isItShape1 = true;
            tempPath =TSU.Tsunami2.Preferences.Values.Paths.Temporary;
            return tempPath;
        }

        public NFC.Origins OriginToProject()
        {
            return OriginTo();
        }

        private NFC.Origins OriginTo()
        {
            BindingSource bindingSource = new BindingSource();
            List<NFC.Origins> possibles = new List<NFC.Origins>
            {
                NFC.Origins.NoProjection
            };
            if (shape1 == shape2)
            {
                if (shape1 == NFC.Shapes.Line)
                {
                    possibles.Add(NFC.Origins.ProjectionOnLine);
                    possibles.Add(NFC.Origins.ProjectionOnSecondLine);
                }
                else
                {
                    possibles.Add(NFC.Origins.ProjectionOnPlane);
                    possibles.Add(NFC.Origins.ProjectionOnSecondPlane);
                }
            }
            else
            {
                possibles.Add(NFC.Origins.ProjectionOnLine);
                possibles.Add(NFC.Origins.ProjectionOnPlane);
            }

            bindingSource.DataSource = possibles;
            string s_r =TSU.Tsunami2.Preferences.Tsunami.View.ShowMessageOfPredefinedInput(
                $"{"Projection of the Origin"};{"Do you want to  project the origin of the new system on one of the fitted shapes?"}",
                R.T_OK, "", bindingSource).ToString();
            NFC.Origins r = (NFC.Origins)Enum.Parse(typeof(NFC.Origins), s_r);

            return r;
        }

        public NFC.Origins OriginToProjectPlanes()
        {
            return OriginTo();
        }

        public NFC.Origins OriginToProjectLines()
        {
            return OriginTo();
        }

        // store the list of point to trasnform tobe able to get the name because nfc do not send the name...
        List<Elements.Point> points;

        public bool FileToTransform()
        {
            string message = "";
            if (gatherMessages)
            {
                messages.ForEach(x => message += x + "\r\n");
            }

            string titleAndMessage = $"What's next?:;{message +"Do you want to apply the transforamtion to points?"}";
            MessageInput mi = new MessageInput(MessageType.GoodNews, titleAndMessage)
            {
                ButtonTexts = new List<string> { R.T_YES, R.T_NO }
            };
            bool transform = R.T_YES ==mi.Show().TextOfButtonClicked;

            if (transform)
            {

                points = SelectPointsAndWriteFile(NFC.Shapes.Unknown, tempPath + "to_transform.txt", 01);
            }

            return transform;
        }

        public void UseTransformedResults(Matrix<double> results)
        {
            try
            {
                Manager.Module em = new Manager.Module();
                for (int i = 0; i < points.Count; i++)
                {
                    Point p = points[i];
                    p._Coordinates = new CoordinatesInAllSystems();
                    p._Coordinates.Su.X.Value = results[i, 0];
                    p._Coordinates.Su.Y.Value = results[i, 1];
                    p._Coordinates.Su.Z.Value = results[i, 2];

                }
                em.AllElements = new List<TsuObject>();
                em.AllElements.AddRange(points);
                em.CreateView();
                em.View.ShowInMessageTsu("Points in new system", R.T_OK, () => { em.View.Close(); });
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void ShowResults(NFC.Matrices matrices, string[,] pointNames)
        {
            throw new NotImplementedException();
        }

        public void ShowMatrix(Matrix<double> results)
        {
            throw new NotImplementedException();
        }

        public void WantToTransform(string tempPath)
        {
            throw new NotImplementedException();
        }
    }

}
