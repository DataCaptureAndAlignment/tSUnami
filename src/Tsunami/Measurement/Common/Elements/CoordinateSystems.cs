﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TSU.Common;

namespace TSU.Common.Elements
{
    public class CoordinateSystems
    {
        public CoordinateSystems()
        {

        }

        public CoordinateSystem GetByName(string name)
        {
            foreach (var item in AllTypes)
            {
                if (item._Name == name)
                    return item;
            }
            return null;
        }

        public override string ToString()
        {
            return $"{AllTypes.Count} CS, {AllTypes.Last().ToString()}, ...";
        }

        internal List<CoordinateSystem> GetCustoms()
        {
            var customs = new List<CoordinateSystem>();
            var natives = CoordinateSystem.GetNativeSystems();
            foreach (var item in AllTypes)
            {
                var found = natives.Find(x=>x._Name == item._Name);
                if (found==null)
                    customs.Add(item);
            }
            return customs;
        }

        public List<CoordinateSystem> AllTypes { get; set; }

        #region Single Selected Type

        [XmlAttribute]
        public string sSelectedType { get; set; }


        private CoordinateSystem selectedType;

        [XmlIgnore]
        public CoordinateSystem SelectedType
        {
            get
            {
                if (selectedType == null)
                {
                    if (sSelectedType != null)
                    {
                        selectedType = CoordinateSystem.GetByNameIn(sSelectedType);
                    }
                    else
                        return CoordinateSystem.GetByNameIn("BEAMV");
                }
                return selectedType;
            }
            set
            {
                selectedType = value;
                sSelectedType = value._Name;
            }
        }

        #endregion


        #region several selected types

        [XmlAttribute]
        public List<string> sSelectedTypes { get; set; } = new List<string>();



        private SelectedTypesList selectedTypes = new SelectedTypesList();

        public SelectedTypesList SelectedTypes
        {
            get
            {
                if (selectedTypes == null || selectedTypes.Count==0)
                {
                    if (sSelectedTypes == null || sSelectedTypes.Count == 0)
                    {
                        selectedTypes = new SelectedTypesList(sSelectedTypes, this.AllTypes);
                    }
                }
                return selectedTypes;
            }
            set
            {
                selectedTypes = value;
            }
        }

        public class SelectedTypesList : List<CoordinateSystem>
        {
            public List<string> sSelectedTypes;
            public SelectedTypesList()
            {

            }
            public SelectedTypesList(List<string> sSelectedTypes, List<CoordinateSystem> alltypes)
            {
                this.sSelectedTypes = sSelectedTypes;
                if (sSelectedTypes != null )
                {
                    foreach (var item in sSelectedTypes)
                    {
                        base.Add(CoordinateSystem.GetByNameIn(item, alltypes));
                    }
                }

            }

            public new void Add(CoordinateSystem coordinateSystem)
            {
                sSelectedTypes.Add(coordinateSystem._Name);
                base.Add(coordinateSystem);
            }
            public void AddRange(List<CoordinateSystem> coordinateSystems)
            {
                List<string> list = new List<string>();
                coordinateSystems.ForEach(x => list.Add(x._Name));
                sSelectedTypes.AddRange(list);
                base.AddRange(coordinateSystems);
            }

            public new void Remove(CoordinateSystem coordinateSystem)
            {
                sSelectedTypes.Remove(coordinateSystem._Name);
                base.Remove(coordinateSystem);
            }
            public void RemoveRange(List<CoordinateSystem> coordinateSystems)
            {
                List<string> list = new List<string>();
                foreach (var cs in coordinateSystems)
                {
                    sSelectedTypes.Remove(cs._Name);
                    base.Remove(cs);
                }
            }
            public void Clear()
            {
                sSelectedTypes.Clear();
                base.Clear();
            }
        }

        #endregion
    }
}