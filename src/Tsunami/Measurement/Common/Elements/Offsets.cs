
namespace TSU.Common.Elements
{
	public class Offsets
	{
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
        public float H { get; set; }
        public float R { get; set; }
        public float L { get; set; }
	}
}
