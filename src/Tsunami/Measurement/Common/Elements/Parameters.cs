﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;


namespace TSU.Common.Elements
{
    [Serializable]
    public class Parameters
    {
        [XmlAttribute]
        public double Tilt { get; set; }

        /// <summary>
        /// slope in rad, horizontal is slope =0
        /// </summary>
        [XmlAttribute]
        public double Slope { get; set; }                   // en rad , horizontal is slope =0++

        [XmlIgnore]
        public bool hasSlope
        {
            get
            {
                return (Slope !=TSU.Tsunami2.Preferences.Values.na);
            }
        }

        /// <summary>
        /// Bearing in gon or grad
        /// </summary>
        [XmlAttribute]
        public double GisementFaisceau { get; set; } =TSU.Tsunami2.Preferences.Values.na;      // en gon

        [XmlIgnore]
        public bool hasGisement
        {
            get
            {
                return (GisementFaisceau !=TSU.Tsunami2.Preferences.Values.na);
            }
        }

        [XmlAttribute]
        public double R { get; set; }
        [XmlAttribute]
        public double S { get; set; }
        [XmlAttribute]
        public double Cumul { get; set; }
        [XmlAttribute]
        public double T { get; set; }
        [XmlAttribute]
        public double L { get; set; }

        public Parameters()
        {
            double na =TSU.Tsunami2.Preferences.Values.na;
            Tilt = na;
            Slope = na;
            GisementFaisceau = na;
            R = na;
            S = na;
            Cumul = na;
            T = na;
            L = na;
        }

        public override string ToString()
        {
            double na = TSU.Tsunami2.Preferences.Values.na;
            string s = $"Tilt: {Tilt}\r\n" +
                $"Slope: {Slope} mrad\r\n" +
                $"GisementFaisceau: {GisementFaisceau} gon\r\n" +
                $"R: {R} m\r\n" +
                $"S: {S} m\r\n" +
                $"T: {T} m\r\n" +
                $"L: {L} m\r\n" +
                $"Cumul: {Cumul} m\r\n";
            return s;
        }

        internal XmlElement WriteForTsunami(System.Xml.XmlDocument xmlDocument)
        {
            XmlElement e = xmlDocument.CreateElement("Parameters");
            e.SetAttribute("Tilt", Tilt.ToString());
            e.SetAttribute("Slope", Slope.ToString());
            e.SetAttribute("GisementFaisceau", GisementFaisceau.ToString());
            e.SetAttribute("R", R.ToString());
            e.SetAttribute("S", S.ToString());
            e.SetAttribute("Cumul", Cumul.ToString());
            e.SetAttribute("T", T.ToString());
            e.SetAttribute("L", L.ToString());
            return e;
        }
    }
}
