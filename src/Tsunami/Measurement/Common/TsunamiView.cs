﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TSU.Common.Compute.Compensations;
using TSU.Views;
using TSU.Views.Message;
using M = TSU;
using R = TSU.Properties.Resources;

namespace TSU.Common
{
    public partial class TsunamiView : ModuleView
    {
        /// <summary>
        /// used to know if the notification show should hide themself or not, because during showmodal windows the notification cannot be moused over.
        /// </summary>
        public static List<object> ViewsShownAsModal = new List<object>();
        /// <summary>
        /// used to know if the notification show should hide themself or not, because during showmodal windows the notification cannot be moused over.
        /// </summary>
        public static bool IsShownAsModalInPlay
        {
            get
            {
                return ViewsShownAsModal.Count > 0;
            }
        }
        protected override void OnGotFocus(EventArgs e)
        {
            // do nothing for this vew
        }

        private Strategy.ITsunamiViewsStrategy _strategy;
        public TSU.Splash.ScreenView SplashScreenView;

        public Strategy.ITsunamiViewsStrategy _Strategy
        {
            get { return _strategy; }
            // set { _strategy = value; }
        }
        private TabControl tabControl1;
        public Functionalities.Menu menu
        {
            get
            {
                return (this._Module as Common.Tsunami).Menu;
            }
        }

        public TsuView ActiveView
        {
            get
            {
                return this._Strategy.GetActiveView();
            }
        }

        public new Tsunami Module
        {
            get
            {
                return this._Module as Common.Tsunami;
            }

        }

        public enum TsunamiViewsStrategies
        {
            Tab,
            Windows,
            DockableWIndows
        }
        // methods
        public TsunamiView(M.IModule Tsunami) : base(Tsunami)
        {
            InitializeComponent();
            this.ApplyThemeColors();
            string name = Tsunami != null ? Tsunami._Name : "";
            this._Name = $"{Globals.AppName} - {name}";
            //this._Name =R.T459 + Tsunami._Name;
            this.Text = $"{Globals.AppName} - {Globals.AppNameDescription}";
            this.Icon = R.tSUnami_32;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            // this.Resizable = true;

            // Choose a strategy at the end because it will treat the menu and container
            ChangeStrategy(TsunamiViewsStrategies.Windows);
            this.Click += TsunamiView_Click;

            this.AllowDrop = true;
            this.DragEnter += this.On_DragEnter;
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.On_DragDrop);
            this.DragOver += new System.Windows.Forms.DragEventHandler(this.On_DragOver);

            MainToolTip = new ToolTip()
            {
                Active = true,
            };
        }

        public ToolTip MainToolTip;

        private void ApplyThemeColors()
        {
            this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.Name = R.T463;
        }
        public void FindButton(char firstLetter)
        {
            List<BigButton> buttons = new List<BigButton>();


            foreach (Control item in this.menu.View._PanelTop.Controls)
            {
                if (item is BigButton button)
                {
                    if (button.Title[0] == firstLetter)
                        buttons.Add(button);
                }
            }
            foreach (Control item in this.menu.View._PanelBottom.Controls)
            {
                if (item is BigButton button)
                {
                    if (button.Title[0] == firstLetter)
                        buttons.Add(button);
                }
            }
            if (buttons.Count == 0) return;

            if (buttons.Count == 1)
                buttons[0].Do();
            else
                buttons[0].Focus();
            buttons.Clear();
        }

        private void TsunamiView_Click(object sender, EventArgs e)
        {
        }



        internal void ChangeStrategy(TsunamiViewsStrategies newStrategy)
        {
            _strategy?.Unload();
            switch (newStrategy)
            {
                case TsunamiViewsStrategies.Tab:
                    _strategy = new Strategy.Tab(this);
                    break;
                case TsunamiViewsStrategies.Windows:
                    _strategy = new Strategy.Windows(this);
                    break;
                case TsunamiViewsStrategies.DockableWIndows:
                    _strategy = new Strategy.DockableWIndows(this);
                    break;
                default:
                    break;
            }
            _strategy.Reload();
            this.Text = Globals.AppName + " (" + _strategy.type.ToString() + ")";
        }

        //public void AddOpenedFileNameInTitle(string text)
        //{
        //    this.Text = this.text + " (" + _strategy.type.ToString() + ")" + " - '" +text+"'";
        //}

        public void UpdateTextTitle()
        {
            this.Text = Globals.AppName + " - " + Globals.AppNameDescription;
            this.Text += $" {this.Module.Version} from {this.Module.Date}";
            // this.Text += $" ({_strategy.type.ToString()})";
            if (this.Module.PathOfTheSavedFile != "")
                this.Text += $" - '{this.Module.PathOfTheSavedFile}'";
            if (this.Module.Zone != null)
                this.Text += $" ({this.Module.Zone._Name})";
        }


        private void TsunamiView_FormClosing(object sender, FormClosingEventArgs e)
        {
            bool hideWarnings = Debug.Debugging;
            e.Cancel = !Tsunami2.Properties.Quit(hideWarnings);
        }

        public override void AddView(TsuView view)
        {
            base.AddView(view);
            this._strategy.AddView(view);
            view.Focus();

        }

        public void On_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files == null) return;

            foreach (string file in files)
            {
                string ext = System.IO.Path.GetExtension(file).ToUpper();
                if (ext == ".TSU")
                {
                    RunInADifferentThreadToReleaseTheExplorerWindows(() =>
                    {
                        this.menu.OpenExistingModules(file);
                    });
                }
                else if (ext == ".INP" || ext == ".LGC")
                {
                    RunInADifferentThreadToReleaseTheExplorerWindows(() =>
                    {
                        Lgc2.RunInputWithZone(file);
                    });
                }
                else
                {
                    new MessageInput(MessageType.Warning, "Only support '*.TSU; *.LGC; *.INP' file for the moment").Show();
                }
            }
        }

        private void RunInADifferentThreadToReleaseTheExplorerWindows(Action a)
        {
            System.Windows.Forms.Timer t = new System.Windows.Forms.Timer();
            t.Tick += delegate
            {
                t.Stop();
                t.Dispose();
                M.Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(a);
            };
            t.Start();
        }

        public void On_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        public void On_DragEnter(object sender, DragEventArgs e)
        {
        }
    }


}