﻿namespace TSU.Common
{
    public abstract partial class Station
    {
        partial class View
        {
            /// <summary> 
            /// Required designer variable.
            /// </summary>
            private System.ComponentModel.IContainer components = null;
            /// <summary> 
            /// Clean up any resources being used.
            /// </summary>
            /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }
            #region Component Designer generated code

            /// <summary> 
            /// Required method for Designer support - do not modify 
            /// the contents of this method with the code editor.
            /// </summary>
            private void InitializeComponent()
            {
                this.SuspendLayout();
                // 
                // _PanelBottom
                // 
                this._PanelBottom.Size = new System.Drawing.Size(348, 440);
                // 
                // _PanelTop
                // 
                this._PanelTop.Size = new System.Drawing.Size(348, 63);
                // 
                //TSU.Stations.Theodolite.StationTheodoliteModuleView
                // 
                this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
                this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
                this.ClientSize = new System.Drawing.Size(358, 513);
                this.Name = "StationTheodoliteModuleView";
                this.Controls.SetChildIndex(this._PanelTop, 0);
                this.Controls.SetChildIndex(this._PanelBottom, 0);
                this.ResumeLayout(false);
                this.PerformLayout();

            }

            #endregion
        }
    }
}
