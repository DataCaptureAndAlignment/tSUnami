using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using TSU.Common.Elements;
using TSU.Common.Measures;
using R = TSU.Properties.Resources;

namespace TSU.Common
{
    [XmlInclude(typeof(Polar.Station))]
    [XmlInclude(typeof(Line.Station.Average))]
    [XmlInclude(typeof(Level.Station))]
    [XmlInclude(typeof(Tilt.Station))]
    [XmlInclude(typeof(Length.Station))]

    [Serializable]
    [XmlType(TypeName = "Universal.Station")]
    public abstract partial class Station : TsuObject, IEquatable<Station>, IComparable<Station>
    {
        #region props

        public virtual Parameters ParametersBasic { get; set; }

        /// <summary>
        /// Attention : Except for polar stations this list is recreated in the override of the 'get', so you should only read it and not try to manipulate it.
        /// </summary>
        [XmlIgnore]
        public virtual CloneableList<Measure> MeasuresTaken { get; set; }

        /// <summary>
        /// warning to remove or clear, please use the station.module methods taht clean the point from the tsunami.points list
        /// </summary>
        public CloneableList<Measure> MeasuresToDo { get; set; } // very strange but if this is in last position it will not deserialize

        // public virtual CloneableList<Point> PointsMeasured { get; set; } // changed to avoid to lost link at serialization

        internal CloneableList<Point> pointsMeasured = new CloneableList<Point>();

        [XmlIgnore]
        public virtual ReadOnlyCollection<Point> PointsMeasured
        {
            get
            {
                return new ReadOnlyCollection<Point>(pointsMeasured);
            }
        }

        [XmlIgnore]
        public List<Point> OriginalPointsFromMeasures
        {
            get
            {
                List<Point> l = new List<Point>();
                MeasuresTaken.ForEach(x => { l.Add(x._OriginalPoint); });
                return l;
            }
        }

        public CloneableList<Measure> MeasuresToExport { get; set; }
        [XmlIgnore]
        public double na { get; set; } = Tsunami2.Preferences.Values.na;
        #endregion

        #region Construction

        public Station()
        {
            this.MeasuresTaken = new CloneableList<Measure>();
            //this.PointsMeasured = new CloneableList<Point>();
            this.MeasuresToDo = new CloneableList<Measure>();
            this.MeasuresToExport = new CloneableList<Measure>();
        }

        internal static Station GetOldestOftheNewestOfeachType(List<Station> stations)
        {
            var date = stations.GroupBy(obj => obj.GetType()) // Group by the object type
                    .Select(group => group.OrderByDescending(obj => obj.ParametersBasic._Date).First()) // Select the newest object from each group
                    .OrderByDescending(obj => obj.ParametersBasic._Date).Last();
            return date;
        }

        public virtual string ToString(string param)
        {
            string s = "";
            switch (param)
            {
                case "MANAGEMENT":
                    string date = R.T_DATE + ":";
                    string Operation = R.T_OPERATION + ":";
                    string Instrument = R.T_INSTRUMENT + ":";
                    string Team = R.T_TEAM + ":";
                    s += $"{date,15} {this.ParametersBasic._Date}\r\n";
                    s += $"{Operation,15} {this.ParametersBasic._Operation.value}\r\n";
                    s += $"{Instrument,15} {this.ParametersBasic._Instrument._Model} {this.ParametersBasic._Instrument._SerialNumber}\r\n";
                    s += $"{Team,15} {this.ParametersBasic._Team}\r\n";
                    break;
                default:
                    break;
            }
            return s;
        }

        #endregion

        #region Clone

        public override object Clone()
        {
            // to make a clone of the primitive types fields with a new guid
            Station newStation = (Station)base.Clone(); 
            newStation.ParametersBasic = (Parameters)ParametersBasic.Clone();
            newStation.ParametersBasic._Date = ParametersBasic._Date; //not clonable
            //Link the new Parameters to the new Station
            newStation.ParametersBasic._Station = newStation;
            //useful only for the subclasses that dont override MeasuresTaken.get
            newStation.MeasuresTaken = MeasuresTaken.Clone();
            return newStation;
        }
        public Station DeepCopy()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;

                return (Station)formatter.Deserialize(stream);
            }
        }

        #endregion

        #region IEquatable

        public virtual bool Equals(Station other)
        {
            return
               (this as TsuObject).Equals(other) &&
               this.ParametersBasic._Date == other.ParametersBasic._Date &&
               this.ParametersBasic._Team == other.ParametersBasic._Team;
        }
        public override bool Equals(object other)
        {
            if (other is Station station)
                return this.Equals(station);
            else
                return false;
        }
        public int CompareTo(object obj)
        {
            if (obj == null) return 1;
            if (!(obj is Station objAsMeas)) return 1;
            return CompareTo(objAsMeas);
        }
        public int CompareTo(Station compareStation)
        {
            //// A null value means that this object is greater.
            if (compareStation is Tilt.Station compareTiltStation && this is Tilt.Station thisTiltStation)
                return thisTiltStation.TheoPoint._Parameters.Cumul.CompareTo(compareTiltStation.TheoPoint._Parameters.Cumul);
            else
                return 1;
        }
        //public override int GetHashCode()
        //{
        //    return
        //        base.GetHashCode() ^
        //        ParametersBasic.GetHashCode();
        //}

        #endregion

    }
}
