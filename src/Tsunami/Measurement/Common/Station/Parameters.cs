﻿using System;
using System.Xml.Serialization;
using TSU.Common.Elements;
using TSU.Common.Instruments;
using M = TSU.Common.Measures;
using R = TSU.Properties.Resources;
using P = TSU.Tsunami2.Preferences;
using System.Collections.Generic;
using TSU.Common.Instruments.Device;

namespace TSU.Common
{
    public abstract partial class Station
    {
        [Serializable]
        [XmlInclude(typeof(Polar.Station.Parameters))]
        [XmlInclude(typeof(Level.Station.Parameters))]
        [XmlInclude(typeof(Tilt.Station.Parameters))]
        [XmlInclude(typeof(Line.Station.Parameters))]
        [XmlInclude(typeof(Length.Station.Parameters))]
        [XmlType(TypeName = "Stations.Parameters")]
        public class Parameters //: IXmlSerializable
        {
            public virtual DateTime _Date { get; set; }
            public virtual string team { get; set; }
            public virtual string _Team
            {
                get
                {
                    return team;
                }
                set
                {
                    team = value;
                    if (value != R.String_UnknownTeam)
                        P.Values.GuiPrefs.AddLastTeamtUsed(value);
                }
            }
            public virtual Operations.Operation _Operation { get; set; }
            public virtual bool _IsSetup { get; set; }
            public virtual string _StationName { get; set; }
            public State _State { get; set; }
            public virtual M.Measure DefaultMeasure { get; set; }
            public virtual string _GeodeDatFilePath { get; set; }
            public virtual string _LGCInputFilePath { get; set; }
            public virtual string _LGCOutputFilePath { get; set; }
            public virtual string _LGCSaveFilePath { get; set; }
            public virtual DateTime LastSaved { get; set; }
            public virtual DateTime LastChanged { get; set; }
            [NonSerialized]
            private Sensor instrument;
            public virtual Sensor _Instrument
            {
                get
                {
                    return instrument;
                }
                set
                {
                    instrument = value;
                    if (value != null)
                        P.Values.GuiPrefs.AddLastInstrumentUsed(value);
                }
            }

            [XmlIgnore]
            public virtual Station _Station { get; set; }
            public Parameters()
            {
                _Date = DateTime.Now;
                _Team = R.String_UnknownTeam;
                if (P.Values.GuiPrefs.LastTeamUsed != R.String_UnknownTeam
                    && P.Values.GuiPrefs.LastTeamUsed != null)
                {
                    _Team = P.Values.GuiPrefs.LastTeamUsed;
                }
                this._Operation = Operations.Operation.NoOp;
                _IsSetup = false;
                _State = new State.Opening();
                _Instrument = new Sensor();
                _GeodeDatFilePath = "";
                _LGCInputFilePath = "";
                _LGCOutputFilePath = "";
                _LGCSaveFilePath = "";
                this.LastChanged = DateTime.MinValue;
                this.LastSaved = DateTime.MinValue;
            }
            public Parameters(Station station)
                : this()
            {
                this._Station = station;
            }
            public override string ToString()
            {
                string s = string.Format(R.TS_StationParam,
                    this._Date,
                    this._Instrument._Name,
                    this._Operation.ToString(),
                    this._Team,
                    this._State._Description);
                return s;
            }
            public virtual object Clone()
            {
                Parameters newP = (Parameters)MemberwiseClone(); // to make a clone of the primitive types fields
                newP._State = _State.Clone() as State;
                if (DefaultMeasure != null)
                    newP.DefaultMeasure = (M.Measure)DefaultMeasure.Clone();
                return newP;
            }
            public override int GetHashCode()
            {
                return
                    _Date.GetHashCode() ^ _Team.GetHashCode();
            }
            // IXmlSerializable  the automatic one was not working with _paramers ?
            public virtual System.Xml.Schema.XmlSchema GetSchema()
            {
                return null;
            }
        }
    }
}
