﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSU.ENUM;
using TSU.Views;
using Elements = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using TSU.Common.Measures;
using TSU.Common.Compute.CsvImporter;
using TSU.Common.Compute.Rabot;
using TSU.Tools;
using TSU.Common.Elements.Manager;
using TSU.Views.Message;
using System.Activities.Expressions;
using static System.Net.Mime.MediaTypeNames;
using System.Windows.Controls.Primitives;


namespace TSU.Common
{
    public abstract partial class Station
    {
        public partial class View : CompositeView
        {
            #region Fields
            public new Module Module
            {
                get
                {
                    return this._Module as Module;
                }
            }
            public TsuView instrumentView;
            internal Buttons buttons;
            #endregion

            #region Constructor
            public View()
                : base()
            {
                instrumentView = new TsuView();
            }
            public View(Module modelModule, Orientation splitOrientation, ModuleType? TopMenuType = null)
                : base(modelModule, Orientation.Vertical, ModuleType.StationTheodolite)
            {
                InitializeMenu();
                instrumentView = new TsuView();
            }
            private double na = TSU.Tsunami2.Preferences.Values.na;
            #endregion

            #region  Menus
            private void InitializeMenu()
            {
                //Creation of buttons
                buttons = new Buttons(this);
            }

            protected override void OnGotFocus(EventArgs e)
            {
                Invalidate();
                base.OnGotFocus(e);
            }

            protected override void OnLostFocus(EventArgs e)
            {
                base.OnLostFocus(e);
                Invalidate();
            }

            private void ShowContextMenuOfModule()
            {
                this.contextButtons.Clear();
                this.contextButtons.Add(buttons.OpenStation);
                this.contextButtons.Add(buttons.SaveStation);
                this.ShowPopUpMenu(contextButtons);
            }

            protected override void OnKeyUp(KeyEventArgs e)
            {
                base.OnKeyUp(e);
            }
            private void OpenStation()
            {
                this.Module.OpenFromXml();

            }
            private void SaveStation()
            {
                this.Module.SaveToXml();
            }


            #region Buttons
            internal class Buttons
            {
                private View _view;
                public BigButton Team;
                public BigButton OpenStation;
                public BigButton SaveStation;
                public BigButton Operation;
                public BigButton OperationNew;
                public BigButton Instrument;
                public BigButton TheoFile;
                public BigButton MeasureDetails;
                public BigButton ElementDetails;

                public BigButton ChangeState;
                public BigButton ShowInstrumentModule;

                public Buttons(View view)
                {
                    _view = view;
                    OpenStation = new BigButton(string.Format(R.T_OpenModule, "a station module"), R.Open, _view.OpenStation);
                    SaveStation = new BigButton(string.Format(R.T_SaveModule, this._view.Module._Name), R.Save, _view.SaveStation);
                    Team = new BigButton(R.T165, R.Team, _view.ChangeTeam);
                    Operation = new BigButton(R.T_B_SELECT_OPERATION, R.Operation, _view.ChooseOneOfTheOperations);
                    OperationNew = new BigButton(R.T_B_KNOWN_OPERATION, R.Operation, _view.AddtheNumberOfAnOperation);
                    Instrument = new BigButton(R.T168, R.Instrument, _view.ChangeInstrument);
                    TheoFile = new BigButton(R.T_IMPORT_POINTS_FROM_FILE, R.Element_File, _view.OpenTheoFile);
                   
                    MeasureDetails = new BigButton(R.T_STM_MEASURE_DETAILS, R.Measurement, _view.ShowMeasureModule);
                    ElementDetails = new BigButton(R.T_STM_ELEMENT_MANAGER, R.Element_Aimant, () => { _view.ShowElementModule(); });
                    ShowInstrumentModule = new BigButton(R.STM_B_SIM, R.Instrument, () => { _view.ShowInstrumentModuleAsDialog(); });
                    ChangeState = new BigButton(R.STM_B_ChangeState, R.Status, _view.ShowChangeStationStatusMenu, null, true);

                }
            }

            private void OpenTheoFile()
            {
                Elements.Manager.Module em = this.Module.FinalModule._ElementManager;
                if (em != null)
                {
                    if (em.View.Open())
                    {
                        ShowElementModule(multiSelection:false);
                        UpdateView();
                    }
                }
            }

            

            private void ShowChangeStationStatusMenu()
            {

                List<Control> c = new List<Control>();

                State actual = this.Module._Station.ParametersBasic._State;

                BigButton measure = new BigButton(R.T_MEASURING, R.Measurement, () =>
                {
                    if (this.Module._Station.ParametersBasic._IsSetup)
                        this.Module._Station.ParametersBasic._State = new State.Measuring();
                    else
                        this.Module._Station.ParametersBasic._State = new State.Opening();
                    this.UpdateView();
                }, null, false);

                BigButton good = new BigButton(R.T_GOOD, R.StatusGood, () =>
                {
                    this.Module._Station.ParametersBasic._State = new State.Good(); this.UpdateView();
                }, null, false);

                BigButton bad = new BigButton(R.T_BAD, R.StatusBad, () =>
                {
                    this.Module._Station.ParametersBasic._State = new State.Bad(); this.UpdateView();
                }, null, false);

                if (actual is State.Measuring || actual is State.Opening || actual is State.Closed)
                {
                    c.Add(bad);
                }
                else if (actual is State.Bad || actual is State.Questionnable)
                {
                    c.Add(good);
                    c.Add(measure);

                }
                if (actual is State.Good)
                {
                    c.Add(bad);

                    c.Add(measure);
                }
                this.ShowPopUpSubMenu(c, "ChangeStatus ");
            }

            #endregion

            #region Team
            private void ChangeTeam()
            {
                string cancel = R.T_CANCEL;
                List<string> buttonTexts = new List<string> { R.T_OK, cancel };

                MessageTsu.ShowMessageWithTextBox(R.T199, buttonTexts, out string buttonClicked, out string textInput, TSU.Tsunami2.Preferences.Values.GuiPrefs.LastTeamUsed);

                if (buttonClicked != cancel)
                {
                    string team = System.Text.RegularExpressions.Regex.Replace(textInput.ToUpper(), "[,.!;/:*{}]", "_");
                    this.Module.OnTeamChanged(team);
                    this.UpdateView();
                }
            }
            #endregion

            #region Operation
            private void ChooseOneOfTheOperations()
            {
                this.Module.ChangeOperation();
                this.UpdateView();
            }
            private void AddtheNumberOfAnOperation()
            {
                string cancel = R.T_CANCEL;
                List<string> buttonTexts = new List<string> { R.T_OK, cancel };

                MessageTsu.ShowMessageWithTextBox(R.T197, buttonTexts, out string buttonClicked, out string textInput, "999999");

                if (buttonClicked != cancel)
                {
                    this.Module.OnOperationIdChanged(Convert.ToInt32(textInput));
                    this.UpdateView();
                }
            }
            #endregion

            #region Management.Instrument

            public void SelectInterfaces(I.Device.Interfaces previousInterFaces, TsuObject currenObject, object model = null, object fan = null, Action additionalAction = null)
            {
                List<Control> controls = GetInterfacesButtons(previousInterFaces, currenObject, model, fan, additionalAction);
                if (controls.Count == 1) // if no interface already existing nopoint to show the popup menu.
                {
                    (controls[0] as BigButton).Do();
                }
                else
                {
                    var pop = this.PopUpMenu;
                    pop.ShowSubMenu(controls, "InterfacesChoices");
                }
            }

            internal List<Control> GetInterfacesButtons(I.Device.Interfaces previousInterFaces, TsuObject currenObject, object model = null, object fan = null, Action additionalAction = null)
            {
                // selection interfaces
                var module = this.Module;
                var knowns = Tsunami2.Properties.KnownInterfacesAssembly;

                var controls = new List<Control>();
                controls.Add(GetNewInterfacesButton(previousInterFaces, currenObject, additionalAction));
                if (knowns.Count > 0)
                {
                    foreach (var item in knowns)
                    {
                        controls.Add(new BigButton(item.ToString(), R.Interface, () =>
                        {
                            this.Module.ChangeInterfacesBy(currenObject, item);
                            additionalAction();
                        }));
                    }
                    if (fan != null && model == fan)
                    {
                        if (I.Device.Interfaces.AllMissingIn(fan))
                        {
                            if (!I.Device.Interfaces.AllMissingIn(model))
                            {
                                controls.Add(new BigButton("Copy from Initial/Aller Station", R.InterfacesCopy, () =>
                                {
                                    I.Device.Interfaces.CopyFromTo(model, fan);
                                    additionalAction();
                                }));
                            }
                        }
                    }
                    if (previousInterFaces.Count > 0)
                        controls.Add(new BigButton(R.T_REMOVE, R.Remove, () =>
                        {
                            this.Module.ChangeInterfacesBy(currenObject, new I.Device.Interfaces());
                            UpdateView();
                            additionalAction();
                        }));
                }
                return controls;
            }

            internal Control GetNewInterfacesButton(I.Device.Interfaces previousInterFaces, TsuObject currenObject, Action additionalAction = null)
            {
                return new BigButton("Select different interfaces", R.Interfaces, () =>
                {
                    var newInterFaces = new I.Device.Interfaces();
                    if (this.Module._InstrumentManager.SelectInterfaces(newInterFaces, previousInterFaces))
                    {
                        this.Module.ChangeInterfacesBy(currenObject, newInterFaces);
                        if (additionalAction !=null )
                            additionalAction();
                    }
                });
            }

            private void ChangeInstrument()
            {
                this.Module.OnInstrumentChanged();
                this.UpdateView();
            }

            internal void Measure()
            {
                //this.StationModule._InstrumentManager.View
                TryAction(this.Module._InstrumentManager.Measure);
            }

            internal void ShowInstrumentModuleAsDialog(bool showDialog = false)
            {
                this.Module._InstrumentManager.LinkUnlinkedInstrument();
                this.Module._InstrumentManager.SelectedInstrumentModule.View.ShowInMessageTsu(
                   R.STM_SD_I,
                   R.T_OK,
                   () => { this.Module._InstrumentManager.SelectedInstrumentModule.View.ParentView = null; }, // to know later if it visible or not
                   null, null, null, null, null, null, showDialog);
                ///to show the tool tips in the instrument view
                this.Module._InstrumentManager.SelectedInstrumentModule.View.SetAllToolTips();
            }

            internal virtual void UpdateInstrumentView()
            {
            }
            #endregion

            internal virtual void ShowMeasureModule()
            {
                TryAction(this.Module.ShowMeasureModule);
            }

            internal virtual void ShowElementModule(bool multiSelection = true)
            {
                TryAction(() => { this.Module.ShowElementModule(multiSelection); });
            }


            #endregion

            internal void ChangeInstrumentViewToEmpty()
            {
                TsuView v = new TsuView();
                v.BackgroundImage = R.Instrument;
                v.BackgroundImageLayout = ImageLayout.Zoom;
                v.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                v.ShowDockedFill();

                v.Click += delegate
                {
                    this.contextButtons.Clear();
                    this.contextButtons.Add(buttons.Instrument);
                    this.contextButtons.Add(buttons.TheoFile);
                    if (this.Module._InstrumentManager.SelectedInstrument != null)
                    {
                        I.Instrument i = this.Module._InstrumentManager.SelectedInstrument;
                        this.contextButtons.Add(new BigButton(string.Format("Reload '{0}'", i._Name),
                            R.Instrument,
                            delegate
                            {
                                this.Module.OnInstrumentChanged(i as I.Sensor);
                            }, null, null, false));

                    }
                    this.ShowPopUpMenu(contextButtons);
                };
                v.MouseHover += delegate { v.Cursor = Cursors.Hand; };
                ChangeInstrumentView(v);
            }

            internal void ChangeInstrumentView(TsuView view)
            {
                var split = this.splitContainerStationVsInstrument;
                if (split != null)
                {
                    split.Panel2.Controls.Clear();
                    split.Panel2.Controls.Add(view);
                    instrumentView = view;
                }
            }

        }
    }
}
