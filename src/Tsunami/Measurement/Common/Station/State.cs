﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using R = TSU.Properties.Resources;

namespace TSU.Common
{
    public abstract partial class Station
    {

        #region Here we impliment a "Strategy pattern" for the state of the station
        [Serializable]
        [XmlType(TypeName = "Station.State")]
        [XmlInclude(typeof(Bad))]
        [XmlInclude(typeof(Questionnable))]
        [XmlInclude(typeof(Good))]
        [XmlInclude(typeof(Opening))]
        [XmlInclude(typeof(Measuring))]
        [XmlInclude(typeof(Closed))]
        [XmlInclude(typeof(InstrumentSelection))]
        [XmlInclude(typeof(ElementSelection))]
        [XmlInclude(typeof(MeasureToEnter))]
        [XmlInclude(typeof(LGC2CalculationSuccessfull))]
        [XmlInclude(typeof(LGC2CalculationFailed))]
        [XmlInclude(typeof(SFBcalculationSuccessfull))]
        [XmlInclude(typeof(SFBcalculationFailed))]
        [XmlInclude(typeof(WireSaved))]
        [XmlInclude(typeof(WireReadyToBeSaved))]
        [XmlInclude(typeof(StationLevelSaved))]
        [XmlInclude(typeof(StationTiltSaved))]
        [XmlInclude(typeof(StationLevelReadyToBeSaved))]
        [XmlInclude(typeof(StationTiltReadyToBeSaved))]
        [XmlInclude(typeof(TiltcalculationSuccessfull))]
        [XmlInclude(typeof(TiltcalculationFailed))]
        [XmlInclude(typeof(StationLengthReadyToBeSaved))]
        [XmlInclude(typeof(StationLengthSaved))]

        public abstract class State
        {
            [Serializable]
            public enum StrategyType
            {
                Opening,
                SettingUp,
                Measuring,
                Closing,
                InstrumentSelection,
                ElementSelection,
                MeasureToEnter,
                LGC2CalculationSuccessfull,
                LGC2CalculationFailed,
                SFBcalculationSuccessfull,
                SFBcalculationFailed,
                WireReadyToBeSaved,
                WireSaved,
                StationLevelReadyToBeSaved,
                StationLevelSaved,
                StationTiltSaved,
                StationTiltReadyToBeSaved,
                TiltcalculationSuccessfull,
                TiltcalculationFailed,
                StationLengthReadyToBeSaved,
                StationLengthSaved,
                Bad,
                Questionnable,
                Good
            }


            Station _Station;
            public string _Description { get; set; }
            public State()
            {

            }

            public object Clone()
            {
                State newS = (State)this.MemberwiseClone();
                return newS;
            }


            [Serializable]

            public class Opening : State
            {
                public Opening()
                {
                    _Description = R.T374;
                }
            }
            [Serializable]
            public class Measuring : State
            {
                public Measuring()
                {
                    _Description = R.T375;
                }
            }
            [Serializable]
            public class Closed : State
            {
                public Closed()
                {
                    _Description = R.T_StationClosed;
                }
            }
            [Serializable]
            public class InstrumentSelection : State
            {
                public InstrumentSelection()
                {
                    _Description = R.T377;
                }
            }
            [Serializable]
            public class ElementSelection : State
            {
                public ElementSelection()
                {
                    _Description = R.T378;
                }
            }
            [Serializable]
            public class MeasureToEnter : State
            {
                public MeasureToEnter()
                {
                    _Description = R.String_State_MeasureToEnter;
                }
            }
            [Serializable]
            public class LGC2CalculationSuccessfull : State
            {
                public LGC2CalculationSuccessfull()
                {
                    _Description = R.T379;
                }
            }
            [Serializable]
            public class LGC2CalculationFailed : State
            {
                public LGC2CalculationFailed()
                {
                    _Description = R.T380;
                }
            }
            [Serializable]
            public class SFBcalculationSuccessfull : State
            {
                public SFBcalculationSuccessfull()
                {
                    _Description = R.T381;
                }
            }
            [Serializable]
            public class SFBcalculationFailed : State
            {
                public SFBcalculationFailed()
                {
                    _Description = R.T382;
                }
            }
            [Serializable]
            public class WireSaved : State
            {
                public WireSaved()
                {
                    _Description = R.T383;
                }
            }
            [Serializable]
            public class WireReadyToBeSaved : State
            {
                public WireReadyToBeSaved()
                {
                    _Description = R.T384;
                }
            }
            [Serializable]
            public class StationLevelSaved : State
            {
                public StationLevelSaved()
                {
                    _Description = R.T386;
                }
            }
            [Serializable]
            public class StationLevelReadyToBeSaved : State
            {
                public StationLevelReadyToBeSaved()
                {
                    _Description = R.String_State_LevelReadyToBeSaved;
                }
            }
            [Serializable]
            public class StationLengthSaved : State
            {
                public StationLengthSaved()
                {
                    _Description = R.String_State_LengthStationExported;
                }
            }
            [Serializable]
            public class StationLengthReadyToBeSaved : State
            {
                public StationLengthReadyToBeSaved()
                {
                    _Description = R.String_State_LengthStationReadyToBeExported;
                }
            }
            [Serializable]
            public class StationTiltSaved : State
            {
                public StationTiltSaved()
                {
                    _Description = R.T388;
                }
            }
            [Serializable]
            public class StationTiltReadyToBeSaved : State
            {
                public StationTiltReadyToBeSaved()
                {
                    _Description = R.T390;
                }
            }
            [Serializable]
            public class TiltcalculationSuccessfull : State
            {
                public TiltcalculationSuccessfull()
                {
                    _Description = R.T391;
                }
            }
            [Serializable]
            public class TiltcalculationFailed : State
            {
                public TiltcalculationFailed()
                {
                    _Description = R.T392;
                }
            }

            [Serializable]
            public class Questionnable : State
            {
                public Questionnable()
                {
                    _Description = "Questionnable";
                }
            }

            [Serializable]
            public class Bad : State
            {
                public Bad()
                {
                    _Description = "Bad";
                }
            }

            [Serializable]
            public class Good : State
            {
                public Good()
                {
                    _Description = "Good";
                }
            }
        }

        #endregion
    }
}
