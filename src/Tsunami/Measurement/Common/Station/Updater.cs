﻿using System;
using System.Collections.Generic;
using TSU.Common.Compute.Compensations;
using E = TSU.Common.Elements;
using R = TSU.Properties.Resources;

namespace TSU.Common
{
    public abstract partial class Station
    {
        public static class Updater
        {
            /// <summary>
            /// CLean all coordinates of the point with the name and origin just measured and replace with the given coorindates
            /// </summary>
            /// <param name="station"></param>
            /// <param name="coordinatesList"></param>
            internal static List<E.Point> UpdateMeasuredPointsInStation(Polar.Station station, List<E.Point> newPointsFromCompute, bool isApproximative = false)
            {
                List<E.Point> points = new List<E.Point>();

                if (station.Parameters2._IsSetup) return points;

                foreach (E.Point newPoint in newPointsFromCompute)
                {
                    List<E.Point> tempPoints = new List<E.Point>();

                    // var test2 = Tsunami2.Properties.Points;

                    // here if i put RayPoints than it is not adding the new point measured as a new frame and then new frame doesn work.
                    foreach (var item in station.PointsMeasured)
                    {
                        if (item._Name.Trim() == newPoint._Name.Trim())
                            if (station._Name.Contains(item._Origin) || station._Name.Contains(item._Origin.Replace('.', '_')) || item._Origin == R.String_Unknown || newPoint._Origin == "FAKE")
                                tempPoints.Add(item);
                    }

                    foreach (var oldPoint in tempPoints)
                    {
                        oldPoint._Coordinates = newPoint._Coordinates;

                        if (oldPoint != null)
                        {
                            if (isApproximative)
                            {
                                oldPoint.State = E.Element.States.Approximate;
                            }
                            else
                                oldPoint.State = E.Element.States.Good;
                            oldPoint._Coordinates.AreApproximate = isApproximative;
                        }
                    }
                    points.AddRange(tempPoints);
                }

                return points;
            }

            internal static List<E.Point> UpdateMeasuredPoint(Polar.Station station, List<E.Coordinates> coordinateList, bool isApproximative = false)
            {
                List<E.Point> points = new List<E.Point>();
                if (station.Parameters2._IsSetup) return points;
                foreach (E.Coordinates newCoordinate in coordinateList)
                {
                    List<E.Point> tempPoints = new List<E.Point>();
                    // here if i put RayPoints than it is not adding the new point measured as a new frame and then new frame doesn work.
                    foreach (var item in station.PointsMeasured)
                    {
                        if (item._Name == newCoordinate._Name)
                            if (station._Name.Contains(item._Origin) || station._Name.Contains(item._Origin.Replace('.', '_')) || item._Origin == R.String_Unknown)
                                tempPoints.Add(item);
                    }

                    foreach (var oldPoint in tempPoints)
                    {
                        E.CoordinatesInAllSystems c = oldPoint._Coordinates;

                        if (c == null)
                        {
                            c = new E.CoordinatesInAllSystems();
                            oldPoint._Coordinates = c;
                        }
                        c.Ccs = new E.Coordinates();
                        c.Mla = new E.Coordinates();
                        c.Physicist = new E.Coordinates();
                        c.Su = new E.Coordinates();

                        if (oldPoint != null)
                        {
                            switch (newCoordinate.CoordinateSystem)
                            {
                                case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.AutomaticBasedOnReferenceFrame:
                                    break;
                                case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.Unknown:
                                case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes._3DCartesian:
                                    oldPoint._Coordinates.Local = newCoordinate;
                                    break;
                                case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.Geodetic:
                                    break;
                                case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes._2DPlusH:

                                    oldPoint._Coordinates.Ccs = newCoordinate;
                                    break;
                                case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes._2DCartesian:
                                    break;
                                case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.GeodeticSphere:
                                    break;
                                default:
                                    break;
                            }
                            if (isApproximative)
                            {
                                oldPoint.State = E.Element.States.Approximate;
                            }
                            else
                                oldPoint.State = E.Element.States.Good;
                            oldPoint._Coordinates.AreApproximate = isApproximative;
                        }
                    }
                    points.AddRange(tempPoints);
                }

                return points;
            }

            internal static void UpdateLastMeasuredPoint(Polar.Measure lastMeasure, List<E.Point> pointsList)
            {
                foreach (E.Point p in pointsList)
                {
                    if (lastMeasure._Point._Name.ToUpper() == p._Name.ToUpper())
                    {
                        lastMeasure._Point._Coordinates = p._Coordinates;
                        return;
                    }

                    //E.Point lastPoint = lastMeasure._Point;

                    //E.CoordinatesSystems c = lastPoint._Coordinates;
                    //c.Ccs = new E.Coordinates();
                    //c.Mla = new E.Coordinates();
                    //c.Physicist = new E.Coordinates();
                    //c.Su = new E.Coordinates();

                    //if (lastPoint != null)
                    //{
                    //    switch (newCoordinate.CoordinateSystem)
                    //    {
                    //        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.AutomaticBasedOnReferenceFrame:
                    //            break;
                    //        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.Unknown:
                    //        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes._3DCartesian:
                    //        lastPoint._Coordinates.Local = newCoordinate;
                    //            break;
                    //        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.Geodetic:
                    //            break;
                    //        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes._2DPlusH:

                    //        lastPoint._Coordinates.Ccs = newCoordinate;
                    //            break;
                    //        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes._2DCartesian:
                    //            break;
                    //        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.GeodeticSphere:
                    //            break;
                    //        default:
                    //            break;
                    //    }
                    //}
                }
            }

            internal static void UpdateLastMeasuredPoint(Polar.Measure lastMeasure, List<E.Coordinates> pointsList)
            {
                foreach (E.Coordinates newCoordinate in pointsList)
                {
                    if (lastMeasure._Point._Name == newCoordinate._Name)
                    {
                        E.Point lastPoint = lastMeasure._Point;

                        E.CoordinatesInAllSystems c = lastPoint._Coordinates;
                        c.Ccs = new E.Coordinates();
                        c.Mla = new E.Coordinates();
                        c.Physicist = new E.Coordinates();
                        c.Su = new E.Coordinates();

                        if (lastPoint != null)
                        {
                            switch (newCoordinate.CoordinateSystem)
                            {
                                case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.AutomaticBasedOnReferenceFrame:
                                    break;
                                case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.Unknown:
                                case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes._3DCartesian:
                                    lastPoint._Coordinates.Local = newCoordinate;
                                    break;
                                case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.Geodetic:
                                    break;
                                case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes._2DPlusH:

                                    lastPoint._Coordinates.Ccs = newCoordinate;
                                    break;
                                case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes._2DCartesian:
                                    break;
                                case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.GeodeticSphere:
                                    break;
                                default:
                                    break;
                            }
                        }
                        return;
                    }
                }
            }

            internal static void UpdateStationCoordinates(Polar.Station station, E.Point point)
            {
                station.Parameters2._StationPoint._Coordinates = point._Coordinates;
                //if (coordinate != null)
                //{
                //    switch (coordinate.CoordinateSystem)
                //    {

                //        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.Unknown:
                //        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes._3DCartesian:
                //            station.Parameters2._StationPoint._Coordinates.Local = coordinate;
                //            break;

                //        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes._2DPlusH:
                //            station.Parameters2._StationPoint._Coordinates.Ccs = coordinate;
                //            break;

                //        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes._2DCartesian:
                //        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.GeodeticSphere:
                //        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.AutomaticBasedOnReferenceFrame:
                //        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.Geodetic:
                //        default:
                //            break;
                //    }
                //}
            }

            internal static void UpdateStationCoordinates(Polar.Station station, E.Coordinates coordinate)
            {
                if (coordinate != null)
                {
                    switch (coordinate.CoordinateSystem)
                    {

                        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.Unknown:
                        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes._3DCartesian:
                            station.Parameters2._StationPoint._Coordinates.Local = coordinate;
                            break;

                        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes._2DPlusH:
                            station.Parameters2._StationPoint._Coordinates.Ccs = coordinate;
                            break;

                        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes._2DCartesian:
                        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.GeodeticSphere:
                        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.AutomaticBasedOnReferenceFrame:
                        case TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.Geodetic:
                        default:
                            break;
                    }
                }
            }


            internal static void UpdateStationCoordinates(Polar.Station station, Plgc1 plgc)
            {
                station._Name = "PLGC-" + plgc._Strategy.ToString() + station.Parameters2._StationPoint._Name;
                UpdateStationCoordinates(station, plgc.GetFirstOutputCoordinate());
            }

            public static void UpdatePointsLancésAfterRecompute(Polar.Station station)
            {
                if (station == null) throw new Exception(R.T017);
                if (station.Parameters2.Setups.FinalValues.VariablePoints == null) return;

                foreach (Polar.Measure m in station.Rays)
                {
                    m._Point._Coordinates = station.Parameters2.Setups.FinalValues.VariablePoints.Find(x => x._Name == m._Point._Name)._Coordinates;
                    //if (m._Point._Origin == station._Name)
                    //{
                    //    if (station.Parameters2.Setup.FinalSetupValues.CoordinatesSystemsUsedForCompute == E.Coordinates.CoordinateSystemsTypes._2DPlusH)
                    //    {
                    //        m._Point._Coordinates.Ccs = station.Parameters2.Setup.FinalSetupValues.VariablePoints.Find(x => x._Name == m._Point._Name);
                    //        m._Point._Coordinates.Ccs.FrameName = "CCS";
                    //    }
                    //    else
                    //    {
                    //        m._Point._Coordinates.Su = station.Parameters2.Setup.FinalSetupValues.VariablePoints.Find(x => x._Name == m._Point._Name);
                    //        m._Point._Coordinates.Ccs.FrameName = "SU";
                    //    }
                    //}
                }

                foreach (Polar.Measure m in station.Rays)
                {

                    if (station._Name.Contains(m._Point._Origin)) // contains because somethime the name is not complete yet
                    {
                        m._Point._Coordinates = station.Parameters2.Setups.FinalValues.VariablePoints.Find(x => x._Name == m._Point._Name)._Coordinates;
                        //if (station.Parameters2.Setup.FinalSetupValues.CoordinatesSystemsUsedForCompute == E.Coordinates.CoordinateSystemsTypes._2DPlusH)
                        //{
                        //    m._Point._Coordinates.Ccs = station.Parameters2.Setup.FinalSetupValues.VariablePoints.Find(x => x._Name == m._Point._Name);
                        //    m._Point._Coordinates.Ccs.FrameName = "CCS";
                        //}
                        //else
                        //{
                        //    m._Point._Coordinates.Su = station.Parameters2.Setup.FinalSetupValues.VariablePoints.Find(x => x._Name == m._Point._Name);
                        //    m._Point._Coordinates.Ccs.FrameName = "SU";
                        //}
                        m._Point.State = E.Element.States.Good;
                    }
                }
            }

            public static void UpdateStationCoordinates(Polar.Station station, Polar.Station.Parameters.Setup.Values result)
            {

                station.Parameters2.Setups.FinalValues = result;
                station._Name = result.Strategy.ToString() + "_" + station.Parameters2._StationPoint._Name;
                station.Parameters2._StationPoint._Origin = station._Name;
                UpdateStationCoordinates(station, Analysis.Element.FindFirstPointByName(result.VariablePoints, station.Parameters2._StationPoint._Name));
                foreach (var item in station.PointsMeasured)
                {
                    item._Origin = station._Name;
                }

                //TSU.Debug.ShowPointsInConsole();
                if (station.Parameters2.Setups.FinalValues.StationPoint == null) 
                    station.Parameters2.Setups.FinalValues.StationPoint = station.Parameters2._StationPoint;

                //TSU.Debug.ShowPointsInConsole();
            }

            public static void UpdateStationV0AndHt(Polar.Station station, Polar.Station.Parameters.Setup.Values result)
            {

                station.Parameters2.Setups.FinalValues = result;

            }

            internal static void ChangeStationName(Polar.Station station, E.Manager.Module em, string newName)
            {
                // fix old name
                string old = station._Name;

                // update the station point so its name as well
                station.Parameters2._StationPoint._Name = newName;
                string @new = station._Name;

                foreach (E.Point item in em.GetPointsInAllElements())
                {
                    if (item._Origin == old)
                        item._Origin = @new;
                }
                foreach (E.Composites.CompositeElement item in em.AllElements)
                {
                    if (item._Name == old)
                    {
                        item._Name = @new;
                        item._Origin = @new;
                    }
                }
                foreach (Polar.Measure item in station.MeasuresTaken)
                {
                    if (item.Origin == old)
                        item.Origin = @new;

                    if (item._Point._Origin == old)
                        item._Point._Origin = @new;

                }
            }
        }
    }
}
