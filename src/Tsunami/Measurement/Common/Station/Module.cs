﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Serialization;
using TSU.Common.Elements;
using TSU.Common.Instruments.Device;
using TSU.Common.Instruments;
using TSU.ENUM;
using TSU.Functionalities.ManagementModules;
using TSU.Views;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using EC = TSU.Common.Elements.Composites;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;
using System.IO;

namespace TSU.Common
{
    public abstract partial class Station
    {
        internal void ClearMeasuresToDo()
        {
            MeasuresToDo.Clear();
        }

        [XmlType(TypeName = "Stations.Module")]
        [Serializable]
        public abstract class Module : TSU.Module
        {
            public Station _Station { get; set; }
            [XmlIgnore]
            public double na { get; set; } = TSU.Tsunami2.Preferences.Values.na;
            public M.Manager _MeasureManager { get; set; }

            private I.Manager.Module instrumentManager;
            [XmlIgnore]
            public I.Manager.Module _InstrumentManager
            { 
                get
                {
                    if (instrumentManager == null)
                        instrumentManager = new I.Manager.Module(this);
                    instrumentManager.ParentModule = this;
                    instrumentManager.CreateViewIfNull();
                    instrumentManager.View.currentStrategy.ManagerView.Module = instrumentManager;
                    return instrumentManager;
                }
                set
                {
                    instrumentManager = value;
                }
             }




            [XmlIgnore]
            internal E.Manager.Module ElementModule
            {
                get
                {
                    return (this.ParentModule as FinalModule)._ElementManager;
                }
            }

            [XmlIgnore]
            public abstract ReadOnlyCollection<M.Measure> MeasuresReceived { get; }


            [XmlIgnore]
            public FinalModule FinalModule
            {
                get
                {
                    return this.ParentModule as FinalModule;
                }
            }

            [XmlIgnore]
            public new View View
            {
                get
                {
                    return this._TsuView as View;
                }
            }
            internal TSU.ENUM.SortDirection sortDirection = SortDirection.Dcum;
            [XmlIgnore]
            internal I.Sensor tempInstrument = new I.Sensor();
            #region Events

            /// <summary>
            /// definition of the method that an event should implement
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            /// 
            public delegate void StationEventHandler(object sender, StationEventArgs e);

            #region Handlers

            ///// <summary>
            ///// happens when the station has just been set up.
            ///// </summary>
            //[field: XmlIgnore]
            //public event StationEventHandler Setup;

            /// <summary>
            /// happens after a new measurement
            /// </summary>
            [field: XmlIgnore]
            public event M.MeasurementEventHandler MeasureReceived;

            public event M.MeasurementEventHandler MeasureThreated;




            [field: XmlIgnore]
            public event StationEventHandler NewInstrumentSelected;

            #endregion

            #region actions

            #endregion

            #endregion

            // Construstor
            public Module() : base()
            {
            }
            public Module(TSU.Module parentModule, string nameAndDescription = "", bool createView = true)
                : base(parentModule, nameAndDescription, createView)
            {
            }
            public override void Initialize() // This method is called by the base class "Module" when this object is created
            {
                this._InstrumentManager = new I.Manager.Module(this);
                this._InstrumentManager.Changed += UnHandledManager_Changed;
                this._MeasureManager = new M.Manager(this);
                this._MeasureManager.Changed += UnHandledManager_Changed;
                base.Initialize();
                //this._InstrumentManager.Log = this.Log;
                //this._MeasureManager.Log = this.Log;
            }

            public virtual void LoadInstrumentTypes()
            {
                // overrided
            }

            internal virtual void SavePoint(E.Point p, E.Manager.Module.AddingStrategy strategy = E.Manager.Module.AddingStrategy.ReplaceIfSameGroup)
            {
                this.ElementModule.AddElement(this, p, strategy);
            }

            internal override IEnumerable<System.Windows.Forms.Control> GetOptions()
            {
                // an option for guided module is to be open as Advanced module.
                if (this.FinalModule is Guided.Module)
                {
                    BigButton OpenAsAdvanced = new BigButton($"{R.T_OPEN_IN_ADVANCED_MODULE};{R.T_TO_HELP_YOU_ACCESS_MORE_DETAIL}", R.AdvancedModule);
                    OpenAsAdvanced.BigButtonClicked += delegate
                    {
                        TSU.Module m = null;

                        if (!(this.FinalModule is Guided.Module))
                            throw new Exception(R.T_THIS_IS_ALREADY_IN_A_ADVANCE_MODULE);
                        // theodolite
                        if (this is Polar.Station.Module)
                        {
                            Polar.Module tm = new Polar.Module(TSU.Tsunami2.Preferences.Tsunami, R.T_ADVANCED);
                            tm.Add(this);
                            m = tm;
                        }


                        TSU.Tsunami2.Preferences.Tsunami.Add(m);
                    };
                    yield return OpenAsAdvanced;
                }
            }

            public override string ToString()
            {
                return $"{R.T_STM_DEALING_WITH} '{this._Station.ToString()}'";
            }

            internal override void ReCreateWhatIsNotSerialized(bool saveSomeMemory = false)
            {
                try
                {
                    Parameters stp = this._Station.ParametersBasic;

                    // to avoid to create all views that probably not used
                    if (saveSomeMemory && this.FinalModule._ActiveStationModule != this)
                    {
                        this.CreateViewAutmatically = false; // in the base this will not create the view
                        this._MeasureManager.CreateViewAutmatically = false;
                    }

                    if (this._InstrumentManager == null)
                        this._InstrumentManager = new I.Manager.Module(this);
                    // this._MeasureManager = new MeasureManager(this);
                    //this._MeasureManager.AllElements.AddRange(this._Station.MeasuresTaken);
                    this.LoadInstrumentTypes();
                    stp._Station = this._Station;
                    this._Station.MeasuresTaken.AddRange(this._MeasureManager.AllElements.Cast<M.Measure>().ToList());
                    this._InstrumentManager.Changed += UnHandledManager_Changed;
                    this._MeasureManager.Changed += UnHandledManager_Changed;

                    base.ReCreateWhatIsNotSerialized(saveSomeMemory);

                    this.StopSequenceMeasurement();

                    //this._InstrumentManager.Log = this.Log;
                    this._MeasureManager.ReCreateWhatIsNotSerialized(saveSomeMemory);
                    //this._MeasureManager.Log = this.Log;
                    this._MeasureManager.ParentModule = this;
                    if (this.FinalModule._ActiveStationModule == this)
                    {
                        if (stp._Instrument.Id != null)
                        {
                            I.Instrument i = this._InstrumentManager.AllElements.OfType<I.Instrument>().FirstOrDefault(
                                inst => inst._SerialNumber == stp._Instrument._SerialNumber
                                && (inst._Model == this._Station.ParametersBasic._Instrument._Model));

                            var sensor = i as I.Sensor;
                            if (sensor != null)
                            {
                                this.OnInstrumentChanged(sensor);
                            }
                            else
                            {
                                string titleAndMessage = $"{R.T_UNKNOWN_INSTRUMENT} '{stp._Instrument._Name}', {R.T_PLEASE_ADD_IT_TO_YOUR_INTRUMENTDAT_FILE}.";
                                new MessageInput(MessageType.Critical, titleAndMessage).Show();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"{R.T_CANNOT_RECREATE_WHAT_WAS_NOT_SERIALIZED_IN} {this._Name}", ex);
                }
            }

            private void StopSequenceMeasurement()
            {
                if (this is Polar.Station.Module)
                {
                    if (this._Station.MeasuresToDo.Count > 0)
                        if (this._Station.MeasuresToDo[0]._Status.Type == M.States.Types.Good)
                            this._Station.MeasuresToDo[0]._Status = new M.States.Unknown();
                }
            }

            // Observable IInstrument
            public void SendMessage(I.Sensor value)
            {
                if (observers != null)
                    foreach (var observer in observers)
                    {
                        if (value == null)
                            observer.OnError(new NotImplementedException());
                        else
                            observer.OnNext(value);
                    }
            }

            public virtual void BasedOn(I.Sensor i)
            {
                // old type of event handler baesd on desin pattern , but it is global event than we have to sort it based on the object received
                SendMessage(i);
                // new events handler that is more specific
                if (this.NewInstrumentSelected != null) NewInstrumentSelected(this, new StationEventArgs(this));
            }

            // not used?
            //public void OnNext(IElement e)
            //{
            //    //this._State.DealWithElement(this._StationControllerModule, e);
            //}

            public virtual void OnNext(M.IMeasure value)
            {
                OnMeasureReceived(value as M.Measure);
            }

            public void OnMeasureReceived(M.Measure m)
            {
                if (this.MeasureReceived != null) MeasureReceived(this, new M.MeasurementEventArgs(m));
            }

            public void CheckBocValidity(M.Measure m)
            {
                var results = Tsunami2.Properties.BOC_Context.ExistingResults;
                if (results != null)
                {
                    if (this.ParentModule is FinalModule fm)
                    {
                        Tsunami2.Properties.BOC_Context.OnResultUpdated(Tsunami2.Properties.BOC_Context.ExistingResults);
                    }
                }
            }

            public void OnMeasureThreated(M.Measure m)
            {
                CheckBocValidity(m);
                if (this.MeasureThreated != null) MeasureThreated(this, new M.MeasurementEventArgs(m));
            }

            #region Team
            internal void OnTeamChanged(string respond)
            {
                this._Station.ParametersBasic._Team = respond;
            }

            #endregion

            #region Operation Numbers
            internal void ChangeOperation(O.Operation o)
            {
                if (o != null) this._Station.ParametersBasic._Operation = o;
            }
            /// <summary>
            /// Mise à jour des coordonnées theo si elles sont modifiées dans l'element manager
            /// </summary>
            /// <param name="originalPoint"></param>
            /// <param name="pointModified"></param>
            internal virtual void UpdateTheoCoordinates(E.Point originalPoint, E.Point pointModified)
            {
            }


            //internal void ChangeOperation()
            //{
            //    if (this._Station.ParametersBasic._Operation != null)
            //        this.FinalModule.OperationManager._SelectedObjects.Clear();
            //    this.FinalModule.OperationManager._SelectedObjects.Add(this._Station.ParametersBasic._Operation);
            //    O.Operation o = this.FinalModule.OperationManager.SelectOperation();
            //    if (o != null) this._Station.ParametersBasic._Operation = o;
            //}



            internal void ChangeOperation()
            {
                this.FinalModule.OperationManager._SelectedObjectInBlue = this._Station.ParametersBasic._Operation;
                this.FinalModule.OperationManager._SelectedObjects.Clear();
                this.FinalModule.OperationManager._SelectedObjects.Add(this._Station.ParametersBasic._Operation);
                this.ChangeOperation(this.FinalModule.OperationManager.SelectOperation());
            }

            internal void OnOperationIdChanged(int id)
            {
                O.Operation o = this.FinalModule.OperationManager.AllElements.Find(x => (x as O.Operation).value == id) as O.Operation;
                if (o == null) o = new O.Operation(id, "N/A");

                this._Station.ParametersBasic._Operation = o;
            }
            #endregion

            #region Management.Instrument

            internal virtual void OnInstrumentChanged(I.Sensor i = null)
            {
                if (i == null)
                    _InstrumentManager.SelectInstrument("Select instrument", selectables: null, multiSelection: false, preselected: null, true);
                else
                {
                    ///Changed because make crossthread error when called if instrument sn is not good one
                    this.tempInstrument = i;
                    this.InvokeOnApplicationDispatcher(this.SetInstrument);
                }
            }

            private void SetInstrument()
            {
                if (this._InstrumentManager != null)
                {
                    this._InstrumentManager.SetInstrument(this.tempInstrument);
                }
            }
            internal virtual void CoupleToInstrument(I.Module im)
            {
                DecoupleFromInstrument(im); // first to make sure it is not couple several time
                TSU.Debug.WriteInConsole($"Coupling {this._Name} From Instrument '{im.Instrument._Name}'");
                this._InstrumentManager.SelectedInstrumentModule = im;

                im.MeasurementAvailable += OnMeasureReceived;

                im.FinishingMeasurement += Logs.Log.OnMeasurementFinished;
                im.MeasurementFailed += Logs.Log.OnMeasurementFailed;
                im.MeasurementFailed += On_MeasurementFailed;
                im.MeasurementAvailable += Logs.Log.OnMeasureAvailable;
                im.StartingMeasurement += Logs.Log.StartingMeasurement;
                im.StatusChanged += Logs.Log.OnStatusChanged;
                im.InstrumentConnected += OnInstrumentConnected;

                im.Subscribe(this._InstrumentManager);

                im.UpdateView();
            }

            internal virtual void OnInstrumentConnected(object sender, I.StatusEventArgs e)
            {

            }

            internal void DecoupleFromInstrument()
            {
                if (this._InstrumentManager.SelectedInstrumentModule != null)
                    DecoupleFromInstrument(this._InstrumentManager.SelectedInstrumentModule);
            }

            internal void DecoupleFromInstrument(I.Module im)
            {
                if (im == null) return;
                TSU.Debug.WriteInConsole($"Decoupling  {this._Name} From Instrument '{(im.Instrument != null ? im.Instrument._Name : "nothing")}'");

                im.MeasurementAvailable -= OnMeasureReceived;

                im.FinishingMeasurement -= Logs.Log.OnMeasurementFinished;
                im.MeasurementFailed -= Logs.Log.OnMeasurementFailed;
                im.MeasurementFailed -= On_MeasurementFailed;
                im.MeasurementAvailable -= Logs.Log.OnMeasureAvailable;
                im.StartingMeasurement -= Logs.Log.StartingMeasurement;
                im.StatusChanged -= Logs.Log.OnStatusChanged;

                im.InstrumentConnected -= OnInstrumentConnected;

                im.WaitingForDetailsOfNextMeasurement -= OnMeasurementDetailsAskedByInstrumentModule;

                if (this._InstrumentManager.SelectedInstrumentModule == im)
                {
                    this._InstrumentManager.SelectedInstrumentModule = null;
                    im.toBeMeasuredData = null; // to avoid to have a old measurement or goto from another station into the new one
                    this.View?.ChangeInstrumentViewToEmpty();
                }
                im.UnSubscribe(this._InstrumentManager);
            }

            public override void Dispose()
            {
                this.ParentModule = null;

                if (this._InstrumentManager != null)
                {
                    this.DecoupleFromInstrument();
                    this._InstrumentManager.Dispose();
                }
                this._InstrumentManager = null;

                if (this._MeasureManager != null)
                    this._MeasureManager.Dispose();
                this._MeasureManager = null;

                base.Dispose();
            }


            internal virtual void On_MeasurementFailed(object sender, M.MeasurementEventArgs e)
            {
                // to be override by station type, for example a polar module want to resend the original measure to the instrument if necessary
            }


            /// <summary>
            /// Check that the Serial number of the instrument connected is the same as the instrument chosen and change it if necessary
            /// </summary>
            /// <param name="receivedSerialNumber"></param>
            internal virtual void CheckAndChangeInstrument(string receivedSerialNumber)
            {
                if (receivedSerialNumber != this._InstrumentManager.SelectedInstrument._SerialNumber)
                {
                    I.Instrument goodOne = TSU.Tsunami2.Preferences.Values.Instruments.Find(x => (x._SerialNumber == receivedSerialNumber && (x._InstrumentType == this._InstrumentManager.SelectedInstrument._InstrumentType)));
                    if (goodOne != null)
                    {
                        this._InstrumentManager.SelectedInstrument = goodOne;
                        this.OnInstrumentChanged(goodOne as I.Sensor);
                        this.InvokeOnApplicationDispatcher(MessageInstrumentChanged);
                    }
                    else
                    {
                        this.InvokeOnApplicationDispatcher(MessageNotExistingInstrument);
                    }
                }
            }
            private void MessageInstrumentChanged()
            {
                string titleAndMessage = string.Format(R.String_InstrumentRenamed, this._InstrumentManager.SelectedInstrument._SerialNumber);
                new MessageInput(MessageType.Warning, titleAndMessage).Show();
            }
            private void MessageNotExistingInstrument()
            {
                string titleAndMessage = string.Format(R.String_InstrumentDoesntExist);
                new MessageInput(MessageType.Warning, titleAndMessage).Show();
            }

            #endregion

            #region Management.Measure

            internal void ShowMeasureModule()
            {
                this._MeasureManager.SelectableObjects.Clear();
                this._MeasureManager.MultiSelection = true;
                this._MeasureManager.View.CheckBoxesVisible = true;
                this._MeasureManager.Show();
            }

            internal void ShowElementModule(bool multiSelection = true)
            {
                var em = this.FinalModule._ElementManager;
                em.SelectableObjects.Clear();
                em.MultiSelection = multiSelection;   // was commented but is a problem in polar main element manager
                em.View.CheckBoxesVisible = multiSelection; // was commented but is a problem in polar main element manager
                em.SetSelectableToAllPoints();
                em.View.ShowInMessageTsu(
                $"{R.T_ELEMENTS} {R.T_AVAILABLE_IN_THIS_INSTANCE_OF_TSUNAMI};" +
                $"{R.T_VIEW_OF_ELEMENTS}: " +
                $"{R.T_YOU_CAN_SELECT_ELEMENTS_AND_MAKE_SOME_COMPUTATIONS_AVAILABLE_IN_THE_BUTTON_BELOW} " +
                $"{R.T_BUT_THE_SELECTION_WILL_NOT_BE_USED_IN_THE_STATION} " +
                $"{R.T_TO_SELECT_THE_STATION_POINT_PLEASE_USE} '{R.T_SETUP_PARAMETERS}/{R.T_STATIONNED_POINT}/{R.T_KNOWN_POSITION}'. " +
                $"{R.T_TO_SELECT_THE_NEXT_POINTS_TO_MEASURE_PLEASE_USE} '{R.T_NEXT_POINT_SELECTION}/{R.T_ADD_KNOWN_POINTS}", R.T_OK, null);
            }

            /// <summary>
            /// Trie les points par decum ou par inverse decum
            /// </summary>
            /// <param name="pointsToSort"></param>
            internal void SortPoints(CloneableList<E.Point> pointsToSort)
            {
                pointsToSort.Sort();
                if (this.sortDirection == SortDirection.InvDcum) { pointsToSort.Reverse(); }
            }
            /// <summary>
            /// Trie les points par decum ou par inverse decum
            /// </summary>
            /// <param name="pointsToSort"></param>
            internal void SortPoints(List<E.Point> pointsToSort)
            {
                pointsToSort.Sort();
                if (this.sortDirection == SortDirection.InvDcum) { pointsToSort.Reverse(); }
            }
            /// <summary>
            /// Trie les points par decum ou par inverse decum
            /// </summary>
            /// <param name="pointsToSort"></param>
            internal void SortMagnets(List<EC.Magnet> magnetsToSort)
            {
                magnetsToSort.Sort();
                if (this.sortDirection == SortDirection.InvDcum) { magnetsToSort.Reverse(); }
            }
            /// <summary>
            /// Trie les points par decum ou par inverse decum
            /// </summary>
            /// <param name="pointsToSort"></param>
            internal void SortMagnets(CloneableList<EC.Magnet> magnetsToSort)
            {
                magnetsToSort.Sort();
                if (this.sortDirection == SortDirection.InvDcum) { magnetsToSort.Reverse(); }
            }
            /// <summary>
            /// Met les points du sextant 6 avant ceux du sextant 1
            /// </summary>
            /// <param name="theoPoint"></param>
            internal void SortPointsForSPSOrigin(List<E.Point> pointsList)
            {
                List<E.Point> sextant6 = new List<E.Point>();
                sextant6 = pointsList.FindAll(x => x._Numero.StartsWith("6"));
                sextant6.Sort();
                List<E.Point> sextant1 = new List<E.Point>();
                sextant1 = pointsList.FindAll(x => x._Numero.StartsWith("1"));
                sextant1.Sort();
                List<E.Point> mergeList = new List<E.Point>();
                mergeList.AddRange(sextant6);
                mergeList.AddRange(sextant1);
                foreach (E.Point item in mergeList)
                {
                    pointsList.Remove(item);
                }
                pointsList.AddRange(mergeList);
            }
            /// <summary>
            /// Met les points du sextant 6 avant ceux du sextant 1
            /// </summary>
            internal void SortPointsForSPSOrigin(CloneableList<E.Point> pointsList)
            {
                List<E.Point> sextant6 = new List<E.Point>();
                sextant6 = pointsList.FindAll(x => x._Numero.StartsWith("6"));
                sextant6.Sort();
                List<E.Point> sextant1 = new List<E.Point>();
                sextant1 = pointsList.FindAll(x => x._Numero.StartsWith("1"));
                sextant1.Sort();
                List<E.Point> mergeList = new List<E.Point>();
                mergeList.AddRange(sextant6);
                mergeList.AddRange(sextant1);
                foreach (E.Point item in mergeList)
                {
                    pointsList.Remove(item);
                }
                pointsList.AddRange(mergeList);
            }
            /// <summary>
            /// Met les points du PS ring dans l'ordre
            /// </summary>
            /// <param name="theoPoint"></param>
            internal void SortPointsForPROrigin(List<E.Point> pointsList)
            {
                List<E.Point> part8 = new List<E.Point>();
                part8 = pointsList.FindAll(x => x._Numero.StartsWith("8"));
                part8.Sort();
                List<E.Point> part9 = new List<E.Point>();
                part9 = pointsList.FindAll(x => x._Numero.StartsWith("9"));
                part9.Sort();
                List<E.Point> part00 = new List<E.Point>();
                part00 = pointsList.FindAll(x => x._Numero.StartsWith("00"));
                part00.Sort();
                List<E.Point> part0 = new List<E.Point>();
                part0 = pointsList.FindAll(x => x._Numero.StartsWith("01")
                || x._Numero.StartsWith("02")
                || x._Numero.StartsWith("03")
                || x._Numero.StartsWith("04")
                || x._Numero.StartsWith("05")
                || x._Numero.StartsWith("06")
                || x._Numero.StartsWith("07")
                || x._Numero.StartsWith("08")
                || x._Numero.StartsWith("09"));
                part0.Sort();
                List<E.Point> part1 = new List<E.Point>();
                part1 = pointsList.FindAll(x => x._Numero.StartsWith("1"));
                part1.Sort();
                List<E.Point> mergeList = new List<E.Point>();
                mergeList.AddRange(part8);
                mergeList.AddRange(part9);
                mergeList.AddRange(part00);
                mergeList.AddRange(part0);
                mergeList.AddRange(part1);
                foreach (E.Point item in mergeList)
                {
                    pointsList.Remove(item);
                }
                pointsList.AddRange(mergeList);
            }
            /// <summary>
            /// Met les points du PS ring dans l'ordre
            /// </summary>
            /// <param name="theoPoint"></param>
            internal void SortPointsForPROrigin(CloneableList<E.Point> pointsList)
            {
                List<E.Point> part8 = new List<E.Point>();
                part8 = pointsList.FindAll(x => x._Numero.StartsWith("8"));
                part8.Sort();
                List<E.Point> part9 = new List<E.Point>();
                part9 = pointsList.FindAll(x => x._Numero.StartsWith("9"));
                part9.Sort();
                List<E.Point> part00 = new List<E.Point>();
                part00 = pointsList.FindAll(x => x._Numero.StartsWith("00"));
                part00.Sort();
                List<E.Point> part0 = new List<E.Point>();
                part0 = pointsList.FindAll(x => x._Numero.StartsWith("01")
                || x._Numero.StartsWith("02")
                || x._Numero.StartsWith("03")
                || x._Numero.StartsWith("04")
                || x._Numero.StartsWith("05")
                || x._Numero.StartsWith("06")
                || x._Numero.StartsWith("07")
                || x._Numero.StartsWith("08")
                || x._Numero.StartsWith("09"));
                part0.Sort();
                List<E.Point> part1 = new List<E.Point>();
                part1 = pointsList.FindAll(x => x._Numero.StartsWith("1"));
                part1.Sort();
                List<E.Point> mergeList = new List<E.Point>();
                mergeList.AddRange(part8);
                mergeList.AddRange(part9);
                mergeList.AddRange(part00);
                mergeList.AddRange(part0);
                mergeList.AddRange(part1);
                foreach (E.Point item in mergeList)
                {
                    pointsList.Remove(item);
                }
                pointsList.AddRange(mergeList);
            }
            /// <summary>
            /// Met les points du Booster ring dans l'ordre
            /// </summary>
            /// <param name="theoPoint"></param>
            internal void SortPointsForBROrigin(List<E.Point> pointsList)
            {
                List<E.Point> part1 = new List<E.Point>();
                part1 = pointsList.FindAll(x => x._Numero.StartsWith("1")
                && !x._Numero.Contains("16")
                && !x._Numero.Contains("15")
                && !x._Numero.Contains("14")
                && !x._Numero.Contains("13")
                && !x._Numero.Contains("12")
                && !x._Numero.Contains("11")
                && !x._Numero.Contains("10"));
                part1.Sort();
                List<E.Point> part2 = new List<E.Point>();
                part2 = pointsList.FindAll(x => x._Numero.StartsWith("2"));
                part2.Sort();
                List<E.Point> part3 = new List<E.Point>();
                part3 = pointsList.FindAll(x => x._Numero.StartsWith("3"));
                part3.Sort();
                List<E.Point> part4 = new List<E.Point>();
                part4 = pointsList.FindAll(x => x._Numero.StartsWith("4"));
                part4.Sort();
                List<E.Point> part5 = new List<E.Point>();
                part5 = pointsList.FindAll(x => x._Numero.StartsWith("5"));
                part5.Sort();
                List<E.Point> part6 = new List<E.Point>();
                part6 = pointsList.FindAll(x => x._Numero.StartsWith("6"));
                part6.Sort();
                List<E.Point> part7 = new List<E.Point>();
                part7 = pointsList.FindAll(x => x._Numero.StartsWith("7"));
                part7.Sort();
                List<E.Point> part8 = new List<E.Point>();
                part8 = pointsList.FindAll(x => x._Numero.StartsWith("8"));
                part8.Sort();
                List<E.Point> part9 = new List<E.Point>();
                part9 = pointsList.FindAll(x => x._Numero.StartsWith("9"));
                part9.Sort();
                List<E.Point> part10 = new List<E.Point>();
                part10 = pointsList.FindAll(x => x._Numero.StartsWith("10"));
                part10.Sort();
                List<E.Point> part11 = new List<E.Point>();
                part11 = pointsList.FindAll(x => x._Numero.StartsWith("11"));
                part11.Sort();
                List<E.Point> part12 = new List<E.Point>();
                part12 = pointsList.FindAll(x => x._Numero.StartsWith("12"));
                part12.Sort();
                List<E.Point> part13 = new List<E.Point>();
                part13 = pointsList.FindAll(x => x._Numero.StartsWith("13"));
                part13.Sort();
                List<E.Point> part14 = new List<E.Point>();
                part14 = pointsList.FindAll(x => x._Numero.StartsWith("14"));
                part14.Sort();
                List<E.Point> part15 = new List<E.Point>();
                part15 = pointsList.FindAll(x => x._Numero.StartsWith("15"));
                part15.Sort();
                List<E.Point> part16 = new List<E.Point>();
                part16 = pointsList.FindAll(x => x._Numero.StartsWith("16"));
                part16.Sort();
                List<E.Point> mergeList = new List<E.Point>();
                mergeList.AddRange(part8);
                mergeList.AddRange(part9);
                mergeList.AddRange(part10);
                mergeList.AddRange(part11);
                mergeList.AddRange(part12);
                mergeList.AddRange(part13);
                mergeList.AddRange(part14);
                mergeList.AddRange(part15);
                mergeList.AddRange(part16);
                mergeList.AddRange(part1);
                mergeList.AddRange(part2);
                mergeList.AddRange(part3);
                mergeList.AddRange(part4);
                mergeList.AddRange(part5);
                mergeList.AddRange(part6);
                mergeList.AddRange(part7);
                foreach (E.Point item in mergeList)
                {
                    pointsList.Remove(item);
                }
                pointsList.AddRange(mergeList);
            }
            /// <summary>
            /// Met les points du Booster ring dans l'ordre
            /// </summary>
            /// <param name="theoPoint"></param>
            internal void SortPointsForBROrigin(CloneableList<E.Point> pointsList)
            {
                List<E.Point> part1 = new List<E.Point>();
                part1 = pointsList.FindAll(x => x._Numero.StartsWith("1")
                && !x._Numero.Contains("16")
                && !x._Numero.Contains("15")
                && !x._Numero.Contains("14")
                && !x._Numero.Contains("13")
                && !x._Numero.Contains("12")
                && !x._Numero.Contains("11")
                && !x._Numero.Contains("10"));
                part1.Sort();
                List<E.Point> part2 = new List<E.Point>();
                part2 = pointsList.FindAll(x => x._Numero.StartsWith("2"));
                part2.Sort();
                List<E.Point> part3 = new List<E.Point>();
                part3 = pointsList.FindAll(x => x._Numero.StartsWith("3"));
                part3.Sort();
                List<E.Point> part4 = new List<E.Point>();
                part4 = pointsList.FindAll(x => x._Numero.StartsWith("4"));
                part4.Sort();
                List<E.Point> part5 = new List<E.Point>();
                part5 = pointsList.FindAll(x => x._Numero.StartsWith("5"));
                part5.Sort();
                List<E.Point> part6 = new List<E.Point>();
                part6 = pointsList.FindAll(x => x._Numero.StartsWith("6"));
                part6.Sort();
                List<E.Point> part7 = new List<E.Point>();
                part7 = pointsList.FindAll(x => x._Numero.StartsWith("7"));
                part7.Sort();
                List<E.Point> part8 = new List<E.Point>();
                part8 = pointsList.FindAll(x => x._Numero.StartsWith("8"));
                part8.Sort();
                List<E.Point> part9 = new List<E.Point>();
                part9 = pointsList.FindAll(x => x._Numero.StartsWith("9"));
                part9.Sort();
                List<E.Point> part10 = new List<E.Point>();
                part10 = pointsList.FindAll(x => x._Numero.StartsWith("10"));
                part10.Sort();
                List<E.Point> part11 = new List<E.Point>();
                part11 = pointsList.FindAll(x => x._Numero.StartsWith("11"));
                part11.Sort();
                List<E.Point> part12 = new List<E.Point>();
                part12 = pointsList.FindAll(x => x._Numero.StartsWith("12"));
                part12.Sort();
                List<E.Point> part13 = new List<E.Point>();
                part13 = pointsList.FindAll(x => x._Numero.StartsWith("13"));
                part13.Sort();
                List<E.Point> part14 = new List<E.Point>();
                part14 = pointsList.FindAll(x => x._Numero.StartsWith("14"));
                part14.Sort();
                List<E.Point> part15 = new List<E.Point>();
                part15 = pointsList.FindAll(x => x._Numero.StartsWith("15"));
                part15.Sort();
                List<E.Point> part16 = new List<E.Point>();
                part16 = pointsList.FindAll(x => x._Numero.StartsWith("16"));
                part16.Sort();
                List<E.Point> mergeList = new List<E.Point>();
                mergeList.AddRange(part8);
                mergeList.AddRange(part9);
                mergeList.AddRange(part10);
                mergeList.AddRange(part11);
                mergeList.AddRange(part12);
                mergeList.AddRange(part13);
                mergeList.AddRange(part14);
                mergeList.AddRange(part15);
                mergeList.AddRange(part16);
                mergeList.AddRange(part1);
                mergeList.AddRange(part2);
                mergeList.AddRange(part3);
                mergeList.AddRange(part4);
                mergeList.AddRange(part5);
                mergeList.AddRange(part6);
                mergeList.AddRange(part7);
                foreach (E.Point item in mergeList)
                {
                    pointsList.Remove(item);
                }
                pointsList.AddRange(mergeList);
            }
            /// <summary>
            /// Met les points de AD ring dans l'ordre
            /// </summary>
            /// <param name="theoPoint"></param>
            internal void SortPointsForDROrigin(List<E.Point> pointsList)
            {
                List<E.Point> part0 = new List<E.Point>();
                part0 = pointsList.FindAll(x => x._Numero.StartsWith("0"));
                part0.Sort();
                List<E.Point> part1 = new List<E.Point>();
                part1 = pointsList.FindAll(x => x._Numero.StartsWith("1"));
                part1.Sort();
                List<E.Point> part2 = new List<E.Point>();
                part2 = pointsList.FindAll(x => x._Numero.StartsWith("2"));
                part2.Sort();
                List<E.Point> part3 = new List<E.Point>();
                part3 = pointsList.FindAll(x => x._Numero.StartsWith("3"));
                part3.Sort();
                List<E.Point> part4 = new List<E.Point>();
                part4 = pointsList.FindAll(x => x._Numero.StartsWith("4"));
                part4.Sort();
                List<E.Point> part5 = new List<E.Point>();
                part5 = pointsList.FindAll(x => x._Numero.StartsWith("5"));
                part5.Sort();
                List<E.Point> mergeList = new List<E.Point>();
                mergeList.AddRange(part3);
                mergeList.AddRange(part4);
                mergeList.AddRange(part5);
                mergeList.AddRange(part0);
                mergeList.AddRange(part1);
                mergeList.AddRange(part2);
                foreach (E.Point item in mergeList)
                {
                    pointsList.Remove(item);
                }
                pointsList.AddRange(mergeList);
            }
            /// <summary>
            /// Met les points de AD ring dans l'ordre
            /// </summary>
            /// <param name="theoPoint"></param>
            internal void SortPointsForDROrigin(CloneableList<E.Point> pointsList)
            {
                List<E.Point> part0 = new List<E.Point>();
                part0 = pointsList.FindAll(x => x._Numero.StartsWith("0"));
                part0.Sort();
                List<E.Point> part1 = new List<E.Point>();
                part1 = pointsList.FindAll(x => x._Numero.StartsWith("1"));
                part1.Sort();
                List<E.Point> part2 = new List<E.Point>();
                part2 = pointsList.FindAll(x => x._Numero.StartsWith("2"));
                part2.Sort();
                List<E.Point> part3 = new List<E.Point>();
                part3 = pointsList.FindAll(x => x._Numero.StartsWith("3"));
                part3.Sort();
                List<E.Point> part4 = new List<E.Point>();
                part4 = pointsList.FindAll(x => x._Numero.StartsWith("4"));
                part4.Sort();
                List<E.Point> part5 = new List<E.Point>();
                part5 = pointsList.FindAll(x => x._Numero.StartsWith("5"));
                part5.Sort();
                List<E.Point> mergeList = new List<E.Point>();
                mergeList.AddRange(part3);
                mergeList.AddRange(part4);
                mergeList.AddRange(part5);
                mergeList.AddRange(part0);
                mergeList.AddRange(part1);
                mergeList.AddRange(part2);
                foreach (E.Point item in mergeList)
                {
                    pointsList.Remove(item);
                }
                pointsList.AddRange(mergeList);
            }
            #endregion

            internal virtual void OnMeasurementDetailsAskedByInstrumentModule(object sender, M.MeasurementEventArgs e)
            {
                // to be overrided
            }

            #region Adding and selecting

            public virtual List<Polar.Measure> CreateMeasuresFromElement(E.Element e, M.State state, bool silent = false) { return new List<Polar.Measure>(); }

            #endregion

            internal void ShowInParentModule()
            {
                (this.ParentModule as FinalModule).SetActiveStationModule(this);
            }

            /// <summary>
            /// function to set points to measure is used by Final module when received points or composite element from element manager (On next)
            /// to be override in each station module type : 
            /// StationTiltModule Done
            /// StationLevelModule
            /// StationLineModule
            /// StationTheodoliteModule
            /// </summary>
            /// <param name="elementSelected"></param>
            internal virtual void SetPointsToMeasure(EC.CompositeElement elementSelected)
            {
                // function to is used by Final module when received points or composite element from element manager (On next)
                // to be override in each station module type : 
                //StationTiltModule Done
                //StationLevelModule
                //StationLineModule
                //StationTheodoliteModule
            }

            /// <summary>
            /// change le numéro d'opération dans la station
            /// </summary>
            /// <param name="newOperation"></param>
            internal virtual void ChangeOperationID(O.Operation newOperation)
            {
                ///    to be override in each station module type : 
                /// StationTiltModule Done
                /// StationLevelModule
                /// StationLineModule
                /// StationTheodoliteModule
            }


            #region Open, save and Export

            internal void SaveToXml(string fileName = "")
            {
                if (fileName == "") fileName = this.View.GetSavingName(
                    TSU.Tsunami2.Preferences.Values.Paths.Data,
                    TSU.Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now) + "_" + this._Name,
                    "xml (*.xml)|*.xml");
                if (fileName != "") TSU.IO.Xml.CreateFile(this._Station, fileName);
            }

            internal void OpenFromXml(string fileName = "")
            {
                if (fileName == "") fileName = TsuPath.GetFileNameToOpen(this.View,
                    TSU.Tsunami2.Preferences.Values.Paths.Data + string.Format("{0:yyyyMMdd}", DateTime.Now) + "\\",
                    TSU.Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now) + "_" + this._Name,
                    "xml (*.xml)|*.xml",
                    R.T_BROWSE_SEQ);

                if (fileName != "")
                {
                    object s = TSU.IO.Xml.DeserializeFile(typeof(Polar.Station), fileName);
                    this._Station = s as Station;
                    this._MeasureManager.AllElements.AddRange(this._Station.MeasuresTaken);
                    this.OnInstrumentChanged(this._Station.ParametersBasic._Instrument);
                    this._Station.ParametersBasic._Station = this._Station; // ceci n'est pas serializer pour eviter une boucle infinie de serialisation...
                    this.UpdateView();
                }
            }

            internal void ExportNewPointToATheoreticalFile()
            {
                // TODO
                // very bad way to find the frst tehoretical ifle  name they should be store ans serialise in a string list
                string path = "";
                foreach (var item in ElementModule.AllElements)
                {
                    if (item._Name.ToUpper().Contains(".DAT"))
                    {
                        path = item._Name;
                        break;
                    }
                }
                if (path == "")
                {
                    TsuPath.GetFileNameToOpen(this.View, "", "", "", "");
                }
                IO.SUSoft.Geode.Export(path, GetGoodNewPoints());
                Shell.Run(path);
            }

            private List<E.Point> GetGoodNewPoints()
            {
                var a = this.FinalModule._ElementManager.GetPointsInAllElements();
                return a.FindAll(
                    x => x._Origin == this._Name &&
                    x.State == E.Element.States.Good &&
                    x.Type == E.Point.Types.NewPoint);
            }
            #endregion

            internal virtual void ExportToLGC2()
            {
                throw new NotImplementedException();
            }


            internal virtual void OnMeasureReceived(object sender, M.MeasurementEventArgs e)
            {
                // override in theodolitestation
            }

            internal virtual void ChangeInterfacesBy(TsuObject currenObject, Interfaces newInterFaces)
            {
                throw new NotImplementedException();
            }

            internal void ShowGeodeExportSuccess(string titleAndMessage, string filePath="", string filePath2="")
            {
                if (titleAndMessage == null)
                    titleAndMessage = "Geode Export Successfull;";

                if (filePath != "")
                    titleAndMessage += $"\r\n{MessageTsu.GetLink(filePath)}\r\n";

                if (filePath2 != "")
                    titleAndMessage += $"{MessageTsu.GetLink(filePath2)}\r\n";

                if (filePath != "")
                {
                    string folderPath = new FileInfo(filePath).DirectoryName;
                    titleAndMessage += $"in folder:\r\n{MessageTsu.GetLink(folderPath)}";
                }

                new MessageInput(MessageType.GoodNews, titleAndMessage).Show();
            }
        }

        public class StationEventArgs : EventArgs
        {
            public I.Manager.Module InstrumentManager;
            public Station Station;
            public Station.Module StationModule;

            public StationEventArgs()
            {
            }

            public StationEventArgs(Module guidedModule)
            {
                FillEventArgs(guidedModule);
            }

            private void FillEventArgs(Module module)
            {
                this.StationModule = module;
                this.Station = module._Station;
                this.InstrumentManager = module._InstrumentManager;
            }
        }
    }
}
