using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using TSU.Common.Compute.Compensations;
using TSU.Common.Compute.Rabot;
using TSU.Common.Elements.Composites;
using TSU.Common.Measures;
using TSU.Common.Measures.States;
using TSU.Polar.GuidedModules;
using TSU.Views;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;
using S = TSU.Common.Station;
using Z = TSU.Common.Zones;

using System.Xml.Serialization;
using TSU.Common.Blocks;
using System.Linq;

namespace TSU.Common
{
    // the theodolite module is a composite module composed of several other modules
    // as Element, Management.Measure and Station
    public interface IFinalModule : IModule
    {
        ObservationType ObservationType { get; set; }
        List<DsaFlag> DsaFlags { get; }

        E.Manager.Module _ElementManager { get; set; }
        Z.Manager ZoneManager { get; set; }
        O.Manager OperationManager { get; set; }
        S.Module _ActiveStationModule { get; set; }
        CompositeView CompositeView { get; }
        List<S.Module> StationModules { get; set; }
        CloneableList<E.Element> ElementsTobeMeasured { get; set; }
    }


    [XmlInclude(typeof(TdH))]
    public abstract class FinalModule : Module
    {
        public virtual ObservationType ObservationType { get; set; } = ObservationType.Unknown;
        #region Field  & properties

        public TsuDictionnary<string, Rabot> VerticalRabot = new TsuDictionnary<string, Rabot>();
        public TsuDictionnary<string, Rabot> RadialRabot = new TsuDictionnary<string, Rabot>();

        public List<string> RabotFiles = new List<string>();

        [XmlIgnore]
        public string RabotDisplayType  //used for serialization to stay retro compatible
         {
            get
            {
                return RabotStrategy.ToString().Replace('_', ' ');
            }
            set
            {
                var enumName = value.Replace(' ', '_');
                RabotStrategy = (Strategy)Enum.Parse(typeof(Strategy), enumName);
            }
        }

        public Compute.Rabot.Strategy RabotStrategy = Strategy.Not_Defined;

        public Guid idOfActiveStation;

        [XmlIgnore]
        public double na { get; set; } = Tsunami2.Preferences.Values.na;
        [XmlIgnore]
        public E.Manager.Module _ElementManager { get; set; }

        public virtual List<DsaFlag> DsaFlags
        {
            get => Tsunami2.Properties.DsaFlagForAdvancedModule;
        }

        public string defaultUseNameFromNewStationName { get; set; }

        [XmlIgnore]
        public Z.Manager ZoneManager { get; set; } // to avoid serialisation in the childmodule list

        [XmlIgnore]
        public O.Manager OperationManager { get; set; } // to avoid serialisation in the childmodule list

        [XmlIgnore]
        public S.Module _ActiveStationModule
        {
            get
            {
                return this.StationModules.Find(x => x.Guid == idOfActiveStation);
            }
            set
            {
                idOfActiveStation = value.Guid;
                if (!this.StationModules.Exists(x => x.Guid == idOfActiveStation))
                    this.Add(value);
            }
        }
        [XmlIgnore]
        public CompositeView CompositeView
        {
            get
            {
                return this._TsuView as CompositeView;
            }
        }

        public override string ToString()
        {
            return $"FM containing {this.StationModules.Count} Stations (active='{this._ActiveStationModule}')";
        }

        public virtual List<S.Module> StationModules { get; set; } = new List<S.Module>();
        public List<I.LevelingStaff> staffUsed { get; set; } = new List<I.LevelingStaff>();
        public List<I.Level> levelUsed { get; set; } = new List<I.Level>();
        public DateTime timeStaffZeroCheck { get; set; } = DateTime.MinValue;
        [XmlIgnore]
        public bool isStaffZeroCheckExported = true;
        [XmlIgnore]
        public I.Manager.Module staffModule { get; set; }
        internal override void ReCreateWhatIsNotSerialized(bool saveSomeMemory = false)
        {
            try
            {
                base.ReCreateWhatIsNotSerialized(saveSomeMemory);

                this._ElementManager = new E.Manager.Module(this);
                _ElementManager.ReCreateWhatIsNotSerialized(saveSomeMemory);


                foreach (var item in this.StationModules)
                {
                    if (item == null)
                        continue;
                    item.ParentModule = this;
                    this.childModules.Add(item);
                    if (this.StationModules[StationModules.Count - 1] == item)
                        saveSomeMemory = false;

                    item.ReCreateWhatIsNotSerialized(saveSomeMemory);
                    if (item.CreateViewAutmatically)
                    {
                        item.View.ShowDockedFill();
                        this.View.AddView(item.View);
                    }
                }
                if (this.StationModules.Count != 0)
                {
                    this.SetActiveStationModule(this.StationModules[this.StationModules.Count - 1]);
                }
                this.UpdateAllStaff();
                //Pour reexporter le ctrl du zero mire dans le fichier dat
                if (this.staffUsed.Find(x => x._zeroCorrectionReading != na) != null) this.isStaffZeroCheckExported = false;
                foreach (I.Level item in this.levelUsed)
                {
                    item._IsCollimExported = false;
                }
                this.View.UpdateView();
            }
            catch (Exception ex)
            {
                throw new Exception($"Cannot re-create what was not serialized in {this._Name}", ex);
            }
        }


        //public List<Station> _StationsSequences;
        //public List<StationTilt> _StationTiltSequences { get; set; }

        ///// <summary>
        ///// Cast a Magnet cloneablelist into ElementsToBeMeasured
        ///// </summary>
        ///// <param name="listMagnet"></param>
        //public void MagnetToElement_ElementsTobeMeasured(List<Magnet> listMagnet)
        //{
        //    this.ElementsTobeMeasured = new CloneableList<Element>();
        //    foreach (Magnet item in listMagnet)
        //    {
        //        this.ElementsTobeMeasured.Add(item as Element);
        //    }
        //}
        ///// <summary>
        ///// Cast ElementsToBeMeasured into a Magnet cloneable list
        ///// </summary>
        ///// <returns></returns>
        //public List<Magnet> ElementToMagnet_ElementsToBeMeasured()
        //{
        //    List<Magnet> magnetsList = new List<Magnet>();
        //    foreach (Element item in this.ElementsTobeMeasured)
        //    {
        //        if (item is Magnet)
        //        {
        //            magnetsList.Add(item as Magnet);
        //        }
        //    }
        //    return magnetsList;
        //}

        public virtual CloneableList<E.Element> ElementsTobeMeasured { get; set; }
        public virtual CloneableList<E.Point> PointsToBeMeasured { get; set; }
        public virtual CloneableList<E.Point> PointsToBeAligned { get; set; }
        public virtual List<Magnet> MagnetsToBeAligned { get; set; }

        public virtual CloneableList<E.Point> CalaPoints { get; set; }
        public virtual List<Measure> ReceivedMeasures { get; set; }
        #endregion

        #region Constructor
        protected FinalModule()
            : base()
        {
            this.OperationManager = new O.Manager(this);
            this.ZoneManager = new Z.Manager(this);
        }

        protected FinalModule(IModule parentModule)
            : base(parentModule)
        {

        }

        protected FinalModule(IModule parentModule, string nameAndDescription)
            : base(parentModule, nameAndDescription)
        {

        }

        public override void Dispose()
        {
            this._ElementManager.Changed -= UnHandledManager_Changed;

            this.CompositeView?.Dispose();

            this.OperationManager?.Dispose();
            this.OperationManager = null;

            this.ZoneManager?.Dispose();
            this.ZoneManager = null;

            this._ElementManager?.Dispose();
            this._ElementManager = null;

            for (int i = 0; i < StationModules.Count; i++)
            {
                this.StationModules[i]?.Dispose();
                this.StationModules[i] = null;
            }
            this.StationModules.Clear();

            base.Dispose();
        }


        public override void Initialize() // This method is called by the base class "Module" when this object is created
        {
            base.Initialize();

            this._ElementManager = new E.Manager.Module(this);
            this._ElementManager.Changed += UnHandledManager_Changed;
            this.OperationManager = new O.Manager(this);
            this.ZoneManager = new Z.Manager(this);
            //this._StationsSequences = new List<Station>();
            //this._StationTiltSequences = new List<StationTilt>();

            this.PointsToBeMeasured = new CloneableList<E.Point>();
            this.ElementsTobeMeasured = new CloneableList<E.Element>();
            this.MagnetsToBeAligned = new CloneableList<Magnet>();
            this.PointsToBeAligned = new CloneableList<E.Point>();
            this.CalaPoints = new CloneableList<E.Point>();
            this.ReceivedMeasures = new List<Measure>();
        }


        public virtual void BOC_Compute()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///  Add the element to 'ElementsTobeMeasured' + Add all points in the element to 'PointsToBeMeasured' and create (and add to 'MeasuresToBeTake') measures based on the points (points is cloned)
        /// </summary>
        /// <param name="e"></param>
        internal void AddElementToBeMeasured(E.Element e, bool silent = false)
        {
            if (!ElementsTobeMeasured.Contains(e))
            {
                this.ElementsTobeMeasured.Add(e);
            }

            foreach (E.Point p in e.GetPoints())
            {
                if (!PointsToBeMeasured.Contains(p))
                {
                    PointsToBeMeasured.Add(p);
                    this._ActiveStationModule._Station.MeasuresToDo.InsertRange(0,
                        this._ActiveStationModule.CreateMeasuresFromElement(p, new Unknown(), silent: silent));
                }
            }
        }
        ///// <summary>
        ///// Trie les stations tilts par dcum ou inverse dcum
        ///// </summary>
        //internal void SortTiltStations()
        //{
        //    this._StationTiltSequences.Sort();
        //    if (this._ActiveStationModule.sortDirection == SortDirection.InvDcum) { this._StationTiltSequences.Reverse(); }
        //}
        #endregion

        #region Events




        public virtual void OnNext(I.Sensor i)
        {
        }
        public virtual void OnNext(E.IElement e)
        {
            Logs.Log.AddEntryAsFYI(this, this._Name + R.T063 + e._Name);
        }
        /// <summary>
        /// Add the points received from element manager in active station module
        /// </summary>
        /// <param name="point"></param>
        public void OnNextBasedOn(E.Point point)
        {
            CompositeElement compositeSelected = new CompositeElement();
            compositeSelected.Add(point);
            this._ActiveStationModule.SetPointsToMeasure(compositeSelected);
        }
        public void OnNextBasedOn(O.Operation operationSelected)
        {
            this._ActiveStationModule.ChangeOperationID(operationSelected);
        }
        /// <summary>
        /// �v�nement li� � la r�ception de points de l'Element MOdule Manager et ajout � la s�quence de points
        /// </summary>
        /// <param name="elementsFromElementModule"></param>
        public void OnNextBasedOn(CompositeElement elementsFromElementModule)
        {
            var active = this._ActiveStationModule;
            //lance la fonction pour donner les points � mesurer
            if (elementsFromElementModule != null
               && active != null
               && elementsFromElementModule.Elements.Count > 0)
            {
                if ((elementsFromElementModule.Elements[0] is SequenceFil)
                    && !(elementsFromElementModule is SequenceFils)
                    && (elementsFromElementModule.Elements.Count == 1))
                {
                    //lance la fonction pour integrer les points de la s�quence simple
                    (active as Line.Station.Module).SetSequenceToMeasure(elementsFromElementModule.Elements[0] as SequenceFil);
                }
                else
                {
                    if ((elementsFromElementModule.Elements[0] is SequenceNiv) && !(elementsFromElementModule is SequenceNivs) && (elementsFromElementModule.Elements.Count == 1))
                    {
                        //lance la fonction pour integrer les points de la s�quence simple
                        (active as Level.Station.Module).SetSequenceToMeasure(elementsFromElementModule.Elements[0] as SequenceNiv);
                    }
                    else
                    {
                        active.SetPointsToMeasure(elementsFromElementModule);
                    }
                }
            }
        }

        /// <summary>
        /// set new instrument in station module
        /// </summary>
        /// <param name="instrumentSelected"></param>
        public void OnNextBasedOn(I.Instrument instrumentSelected)
        {
        }

        public virtual void OnNext(S s)
        {
            Logs.Log.AddEntryAsFYI(this, this._Name + R.T063 + s._Name);
        }
        #endregion

        #region Change Station
        internal void SwitchToANewStationModule(S.Module stationModule)
        {
            // export to backup folder for safety reason
            if (this.StationModules.Count > 0)
                ExportForGeodeAndLgcIntoBackupFolder(stationModule);

            // add a new module
            this.Add(stationModule);
            this.SetActiveStationModule(stationModule, false);
        }

        private void ExportForGeodeAndLgcIntoBackupFolder(S.Module stationModule)
        {
            try
            {
                string now = Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now);

                this.ExportToGeode(
                    askForFilePathConfirmation: false,
                    filename: now + "_" + IO.SUSoft.Geode.GetFileName(stationModule._Station.ParametersBasic),
                    folder: Tsunami2.Preferences.Values.Paths.Backups, bShowFile: false
                );

                this.ExportToLGC2(
                    fileName: now + "_" + stationModule._Station._Name,
                    folder: Tsunami2.Preferences.Values.Paths.Backups,
                    showSuccessAndOptions: false
                );
            }
            catch (Exception ex)
            {
                Logs.Log.AddEntryAsError(this, ex.Message);
            }
        }

        public override void Add(Module module)
        {
            base.Add(module);

            if (module is S.Module sm)
                this.StationModules.Add(sm);
        }

        internal virtual void SetActiveStationModule(S.Module stationModule, bool createView = true)
        {
            this._ActiveStationModule = stationModule;
            if (createView && stationModule.View == null) // if is was not deserialized to save memory when there sia huge a file
            {
                stationModule.CreateViewAutmatically = true;
                stationModule.CreateView();
                this._TsuView.AddView(stationModule._TsuView);
            }
           
            this.SetInstrumentInActiveStationModule();
            idOfActiveStation = stationModule.Guid;
            this.View?.UpdateView();
            stationModule.View?.UpdateView();
        }
        #endregion

        //internal string SavingFullPath = "";

        internal virtual void Save(string triggerMessage = "")
        {
            Tsunami2.Properties.Save(triggerMessage: triggerMessage);
        }

        private bool GetFilePath(string fileName, string folder, string filter, out string filePath)
        {
            if (fileName != "" && folder != "")
            {
                filePath = folder + fileName;
            }
            else
            {
                if (Tsunami2.Properties.PathOfTheSavedFile == null || Tsunami2.Properties.PathOfTheSavedFile == "")
                {
                    filePath = this.View.GetSavingName(
                       Tsunami2.Preferences.Values.Paths.Data + string.Format("{0:yyyyMMdd}", DateTime.Now) + "\\",
                        Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now) + "_" + this._Name,
                        filter);
                }
                else
                {
                    string dir = System.IO.Path.GetDirectoryName(Tsunami2.Properties.PathOfTheSavedFile);
                    string originalFileName = dir + "\\" + System.IO.Path.GetFileNameWithoutExtension(Tsunami2.Properties.PathOfTheSavedFile);
                    fileName = originalFileName;
                    int count = 0;
                    while (System.IO.File.Exists(fileName + ".inp"))
                    {
                        count++;
                        fileName = $"{originalFileName} ({count})";
                    }
                    filePath = this.View.GetSavingName(dir, fileName, filter);
                }
            }

            return (filePath != "");
        }

        internal bool ExportToPLGC(string fileName = "", string folder = "", bool showSuccessAndOptions = true)
        {
            if (GetFilePath(fileName, folder, "PLGC input (*.pinp)|*.pinp", out string filePath)
                && Plgc1.Input.CreateBasedOn(this, ref filePath))
            {
                if (showSuccessAndOptions)
                    ShowExportSuccessAndOptions(filePath);
                return true;
            }
            return false;
        }

        public bool ExportToLGC2(string fileName = "", string folder = "", bool showSuccessAndOptions = true)
        {
            if (GetFilePath(fileName, folder, "LGC2 input (*.inp)|*.inp", out string filePath))
            {
                if (IO.SUSoft.Lgc2.ToLgc2(this, filePath, Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances))
                {
                    if (showSuccessAndOptions)
                        ShowExportSuccessAndOptions(filePath);
                    return true;
                }
            }
            return false;
        }

        private void ShowExportSuccessAndOptions(string filePath)
        {
            string titleAndMessage = $"{R.T_INPUT_CREATED};{R.T_INPUT_CREATED_WHAT_IS_NEXT}";
            string respond = new MessageInput(MessageType.GoodNews, titleAndMessage)
            {
                ButtonTexts = new List<string> { R.T_YES, R.T_NO }
            }.Show().TextOfButtonClicked;

            if (respond == R.T_YES)
                Lgc2.RunInputWithZone(filePath, askZoneWhenMissing:false);
        }

        internal List<S> GetListOfStationsClosedOrBad()
        {
            List<S> stations = new List<S>();
            List<S> allStations = new List<S>();

            foreach (S.Module stm in this.StationModules)
            {
                allStations.Add(stm._Station);
            }

            bool badOnes (S x) => x.ParametersBasic._State is S.State.Bad;

            if (allStations.All(badOnes))
            {
                allStations.LastOrDefault(badOnes).ParametersBasic._State = new S.State.Good();
            }

            foreach (S.Module stm in this.StationModules)
            {
                if (stm != null)
                {
                    if (stm._Station.ParametersBasic._State is S.State.Bad
                        && Tsunami2.Preferences.Values.GuiPrefs.ExportBadPolarStationsAsCommentedLines.IsFalse)
                        continue;
                    if (stm._Station.ParametersBasic._Team == R.String_UnknownTeam)
                        throw new Exception(string.Format($"{R.T_MISSIGN_TEAM};{R.T_PLEASE_ENTER_A_TEAM_INITIALS_FOR_IN_ORDER_TO_EXPORT_IT}", stm._Station._Name));

                    // close station if controled
                    if (!(stm._Station.ParametersBasic._State is S.State.Closed || stm._Station.ParametersBasic._State is S.State.Bad)
                        && stm is Polar.Station.Module stm2)
                    {
                        CheckControlOpening(stm2);
                    }

                    stations.Add(stm._Station);
                }
            }
            return stations;

            void CheckControlOpening(Polar.Station.Module stm)
            {
                if (!stm.IsControlled() && this.View != null)
                {
                    string close = R.T_CLOSE + " Station";
                    string @continue = "Ignore";
                    string titleAndMessage = $"{stm._Station._Name} is not controlled;Do you want to close it?";
                    MessageInput mi = new MessageInput(MessageType.Important, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { close, @continue },
                        TextOfButtonToBeLocked = @continue
                    };
                    if (mi.Show().TextOfButtonClicked == close)
                        stm._Station.ParametersBasic._State = new S.State.Closed();
                }
            }
        }

        [XmlIgnore]
        public List<E.Point> ListOfStationPoints
        {
            get
            {
                List<E.Point> list = new List<E.Point>();

                foreach (S.Module item in StationModules)
                {
                    if (item is Polar.Station.Module stm
                        && stm.stationParameters._StationPoint != null)
                        list.Add(stm.stationParameters._StationPoint);
                }
                return list;

            }
        }

        internal bool ExportToGeode(bool askForFilePathConfirmation = false, string filename = "", string folder = "", bool bShowFile = true)
        {
            return TSU.IO.SUSoft.Geode.PolarModuleToGeode(this, askForFilePathConfirmation, ref filename, ref folder, bShowFile);
        }

        /// <summary>
        /// Finish the module and put it in the list of finished module
        /// </summary>
        internal virtual void FinishModule()
        {
            this.Finished = true;
            this._TsuView.Hide();
            if (this.ParentModule is Tsunami)
            {
                (this.ParentModule as Tsunami).Menu.SetAnotherModuleAsActive(this);
                (this.ParentModule as Tsunami).Menu.RefreshOpenedAndFinishedModule();
                this.Save();
            }
            this.UpdateView();
        }
        /// <summary>
        /// mise � jour point qui a �t� modifi� dans autre �l�ment manager
        /// </summary>
        /// <param name="originalPoint"></param>
        /// <param name="pointModified"></param>
        internal virtual void UpdateTheoCoordinates(E.Point originalPoint, E.Point pointModified)
        {
            int indexCala = this.CalaPoints.FindIndex(x => x._Name == originalPoint._Name && x._Origin == originalPoint._Origin);
            if (indexCala != -1)
            {
                E.Point initialPoint = this.CalaPoints[indexCala].DeepCopy();
                this.CalaPoints[indexCala].AssignProperties(pointModified);
                this.CalaPoints[indexCala].LGCFixOption = initialPoint.LGCFixOption;
            }
            int indexPtMes = this.PointsToBeMeasured.FindIndex(x => x._Name == originalPoint._Name && x._Origin == originalPoint._Origin);
            if (indexPtMes != -1)
            {
                E.Point initialPoint = this.PointsToBeMeasured[indexPtMes].DeepCopy();
                this.PointsToBeMeasured[indexPtMes].AssignProperties(pointModified);
                this.PointsToBeMeasured[indexPtMes].LGCFixOption = initialPoint.LGCFixOption;
            }
            int indexPtAlign = this.PointsToBeAligned.FindIndex(x => x._Name == originalPoint._Name && x._Origin == originalPoint._Origin);
            if (indexPtAlign != -1)
            {
                E.Point initialPoint = this.PointsToBeAligned[indexPtAlign].DeepCopy();
                this.PointsToBeAligned[indexPtAlign].AssignProperties(pointModified);
                this.PointsToBeAligned[indexPtAlign].LGCFixOption = initialPoint.LGCFixOption;
            }
            int indexReceivedMeasure = this.ReceivedMeasures.FindIndex(x => x._Point._Name == originalPoint._Name && x._Point._Origin == originalPoint._Origin);
            if (indexReceivedMeasure != -1)
            {
                E.Point initialPoint = this.ReceivedMeasures[indexReceivedMeasure]._Point.DeepCopy();
                this.ReceivedMeasures[indexReceivedMeasure]._Point.AssignProperties(pointModified);
                this.ReceivedMeasures[indexReceivedMeasure]._Point.LGCFixOption = initialPoint.LGCFixOption;
            }
        }
        /// <summary>
        /// Reaffecte l'instrument au stationmodule
        /// </summary>
        internal void SetInstrumentInActiveStationModule()
        {
            try
            {
                HourGlass.Set();
                var active = this._ActiveStationModule;
                if (active != null && active._Station != null)
                {
                    if ((active._Station.ParametersBasic._Instrument._Model != null)
                        && !(active._Station is S)
                        && !(active._Station is Length.Station)
                        && active._InstrumentManager != null)
                    {
                        TsuObject selectedInstrument = active._InstrumentManager.AllElements.Find(x => x._Name == active._Station.ParametersBasic._Instrument._Name);
                        if (selectedInstrument != null)
                        {
                            active._InstrumentManager._SelectedObjects.Clear();
                            active._InstrumentManager.AddSelectedObjects(selectedInstrument);
                            active._InstrumentManager._SelectedObjectInBlue = selectedInstrument;
                            active._InstrumentManager.ValidateSelection();
                            if (active._InstrumentManager.SelectedInstrumentModule is I.PolarModule)
                            {
                                active._InstrumentManager.SetNextMeasure(null);
                            }
                        }
                    }
                    if ((active._Station is S || active._Station is Length.Station)
                        && active._InstrumentManager != null)
                    {
                        TsuObject selectedInstrument = active._InstrumentManager.AllElements.Find(x => x._Name == active._Station.ParametersBasic._Instrument._Name);
                        if (selectedInstrument != null)
                        {
                            active.OnInstrumentChanged(selectedInstrument as I.Sensor);
                            if (active._InstrumentManager.SelectedInstrumentModule is I.PolarModule)
                            {
                                active._InstrumentManager.SetNextMeasure(null);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string titleAndMessage = $"{R.T_COULD_NOT_BRING_THE_INSTRUMENT_VIEW_IN_THE_ACTVE_MODULE};{ex.Message} ";
                new MessageInput(MessageType.Warning, titleAndMessage).Show();
            }
            finally
            {
                HourGlass.Remove();
            }
        }
        /// <summary>
        /// Ajoute une mire dans la liste des mires si elle n'est pas d�j� utilis�e
        /// </summary>
        /// <param name="staff"></param>
        public void AddStaff(I.LevelingStaff staff)
        {
            if (this.ParentModule is Guided.Group.Module gm)
            {
                gm.AddStaff(staff);
            }
            else
            {
                if (this.staffUsed.Find(x => x._Name == staff._Name) == null)
                {
                    this.staffUsed.Add(staff.DeepCopy());
                }
            }
        }
        /// <summary>
        /// Recup�re la mire dans la liste des mires utilis�es
        /// </summary>
        /// <param name="staff"></param>
        /// <returns></returns>
        public I.LevelingStaff GetStaff(I.LevelingStaff staff)
        {
            if (this.ParentModule is Guided.Group.Module)
            {
                return (this.ParentModule as Guided.Group.Module).GetStaff(staff);
            }
            else
            {
                return this.staffUsed.Find(x => x._Name == staff._Name);
            }
        }
        /// <summary>
        /// Recup�re la liste des mires utilis�es
        /// </summary>
        /// <param name="staff"></param>
        /// <returns></returns>
        public List<I.LevelingStaff> GetStaffUsedList()
        {
            if (this.ParentModule is Guided.Group.Module)
            {
                return (this.ParentModule as Guided.Group.Module).GetStaffUsedList();
            }
            else
            {
                return this.staffUsed;
            }
        }
        /// <summary>
        /// Recup�re la liste level utilis� contenant les collimations s�rialis�es
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public List<I.Level> GetLevelUsedList()
        {
            if (this.ParentModule is Guided.Group.Module)
            {
                return (this.ParentModule as Guided.Group.Module).GetLevelUsedList();
            }
            else
            {
                return this.levelUsed;
            }
        }
        /// <summary>
        /// Retourne l'heure du dernier contr�le du z�ro mire
        /// </summary>
        /// <returns></returns>
        public DateTime GettimeStaffZeroCheck()
        {
            if (this.ParentModule is Guided.Group.Module)
            {
                return (this.ParentModule as Guided.Group.Module).GettimeStaffZeroCheck();
            }
            else
            {
                return this.timeStaffZeroCheck;
            }
        }
        /// <summary>
        /// Retourne si le dernier contr�le z�ro mire a �t� export� en dat
        /// </summary>
        /// <returns></returns>
        public bool GetIsStaffZeroCheckExported()
        {
            if (this.ParentModule is Guided.Group.Module)
            {
                return (this.ParentModule as Guided.Group.Module).GetIsStaffZeroCheckExported();
            }
            else
            {
                return this.isStaffZeroCheckExported;
            }
        }
        /// <summary>
        /// set que le dernier contr�le mire a �t� effectu�
        /// </summary>
        /// <returns></returns>
        public void SetIsStaffZeroCheckExported(bool isStaffZeroCheckExported)
        {
            if (this.ParentModule is Guided.Group.Module)
            {
                (this.ParentModule as Guided.Group.Module).SetIsStaffZeroCheckExported(isStaffZeroCheckExported);
            }
            else
            {
                this.isStaffZeroCheckExported = isStaffZeroCheckExported;
            }
        }
        /// <summary>
        /// Met � jour toutes les staffs pour un nouveau z�ro de mire
        /// </summary>
        internal virtual void UpdateAllStaff()
        {
        }

        public override void Restart()
        {
            Start();
        }

        internal virtual List<Measure> GetMeasuresFromClosedStations()
        {
            List<Measure> measures = new List<Measure>();
            foreach (var station in GetListOfStationsClosedOrBad())
            {
                measures.AddRange(station.MeasuresTaken);
            }
            return measures;
        }

        internal virtual List<Measure> GetMeasures()
        {
            List<Measure> measures = new List<Measure>();
            foreach (var station in GetStations())
            {
                measures.AddRange(station.MeasuresTaken);
            }
            // some station type do not use .MeasureTaken
            foreach (var stationModule in this.StationModules)
            {
                measures.AddRange(stationModule.MeasuresReceived);
            }

            return measures;
        }


        internal abstract IEnumerable<S> GetStations();
    }

    public class ModuleEventArgs : EventArgs
    {
        public Module Module;
        public ModuleEventArgs(Module module)
        {
            Module = module;
        }
    }

    public enum ObservationType
    {
        Unknown,
        Polar,
        Ecarto,
        Level,
        Roll,
        Length,
        All
    }

    public static class FinalModuleExtensions
    {
        public static List<FinalModule> GetRollModules<T>(this T src) where T : List<FinalModule>
        {
            return GetByObservationType(src, ObservationType.Roll, addRollModules: false);
        }

        /// <summary>
        /// Get module of given type but with roll modules optionally
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="src"></param>
        /// <param name="observationType"></param>
        /// <returns></returns>
        public static List<FinalModule> GetByObservationType<T>(this T src, ObservationType observationType, bool addRollModules) where T : List<FinalModule>
        {
            var founds = new List<FinalModule>();

            // find module of given observation type
            for (int i = src.Count - 1; i > -1; i--)
            {
                var finalModule = src[i];
                if (finalModule is Guided.Group.Module ggm)
                {
                    var guidedAsFinal = new List<FinalModule>();
                    guidedAsFinal.AddRange(ggm.SubGuidedModules);
                    founds.AddRange(guidedAsFinal.GetByObservationType(observationType, addRollModules: true));
                }
                else
                {
                    if (finalModule.ObservationType == observationType || observationType == ObservationType.All)
                        founds.Add(finalModule);
                }
            }

            if (addRollModules)
            {
                // Add roll modules 
                if (observationType != ObservationType.Roll)
                {
                    for (int i = src.Count - 1; i > -1; i--)
                    {
                        var finalModule = src[i];
                        if (finalModule.ObservationType == ObservationType.Roll)
                            founds.Add(finalModule);
                    }
                }
            }
            return founds;
        }

    }
}
