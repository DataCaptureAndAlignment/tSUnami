using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using TSU.Common;
using TSU.Common.Compute.Rabot;
using TSU.Tools.Exploration;
using TSU.Views;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using F = TSU.Functionalities;
using I = TSU.Common.Instruments;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;
using T = TSU.Tools;
using Z = TSU.Common.Zones;

namespace TSU
{
    public interface IModule
    {
        TsuView _TsuView { get; set; }
        string _Name { get; set; }
        void Start();
        void Finish();
    }

    [XmlInclude(typeof(Tsunami))]
    [XmlInclude(typeof(F.Menu))]
    [XmlInclude(typeof(Explorator))]

    [XmlInclude(typeof(Polar.Module))]
    [XmlInclude(typeof(Line.Module))]
    [XmlInclude(typeof(Tilt.Module))]
    [XmlInclude(typeof(Level.Module))]
    [XmlInclude(typeof(Length.Module))]

    [XmlInclude(typeof(Common.Guided.Module))]
    [XmlInclude(typeof(Common.Guided.Group.Module))]

    [XmlInclude(typeof(FinalModule))]

    [XmlInclude(typeof(Polar.Station.Module))]
    [XmlInclude(typeof(Line.Station.Module))]
    [XmlInclude(typeof(Tilt.Station.Module))]
    [XmlInclude(typeof(Level.Station.Module))]
    [XmlInclude(typeof(Length.Station.Module))]
    [XmlInclude(typeof(Station.Module))]
    [XmlInclude(typeof(E.Manager.Module))]
    [XmlInclude(typeof(Common.Measures.Manager))]
    [XmlInclude(typeof(I.Manager.Module))]
    [XmlInclude(typeof(O.Manager))]
    [XmlInclude(typeof(Z.Manager))]
    [XmlInclude(typeof(I.Device.AT40x.Module))]
    [XmlInclude(typeof(Common.Smart.Module))]
    [XmlInclude(typeof(Tilt.Smart.Module))]
    [XmlInclude(typeof(Level.Smart.Module))]
    [XmlInclude(typeof(Line.Smart.Module))]
    [XmlType(Namespace = "Module")]
    [Serializable]

    public class Module : TsuObject, IModule, IObservable<TsuObject>, IObserver<TsuObject>, IChildItem<Module>//, IXmlSerializable
    {
        #region fields &  properties
        [XmlAttribute]
        public string NameAndDescription;
        [XmlAttribute]
        public string Utility;
        [XmlAttribute]
        public string PathOfTheSavedFile = "";
        [XmlAttribute]
        public bool Finished = false;
        [XmlAttribute("Creation")]
        public DateTime CreatedOn { get; set; }
        [NonSerialized]
        private TsuView TsuView;
        [XmlIgnore]
        public TsuView _TsuView
        {
            get
            {
                return TsuView;
            }
            set
            {
                TsuView = value;
                this.Subscribe(TsuView);
            }
        }

        [XmlIgnore]
        public ModuleView View
        {
            get { return this._TsuView as ModuleView; }
            set { this._TsuView = value; }
        }

        [XmlIgnore]
        public bool CreateUniqueView = true;

        [XmlIgnore]
        public bool CreateViewAutmatically = true;

        [XmlIgnore]
        public FinalModule FinalModule
        {
            get
            {
                Module m = this;
                do
                {
                    if (m is FinalModule fm)
                        return fm;
                    m = m.ParentModule;

                } while (m != null);
                return null;
            }
        }

        #region IChildItem<Parent> Members
        //
        // Here we solve the broken link(after XMLserialization between parent and child 
        //
        [XmlIgnore]
        public Module ParentModule { get; set; }

        Module IChildItem<Module>.Parent
        {
            get
            {
                return this.ParentModule;
            }
            set
            {
                this.ParentModule = value;
            }
        }

        [XmlIgnore]
        public ChildItemCollection<Module, Module> childModules { get; private set; }
        #endregion

        #endregion

        #region  Constructors

        public Module() : base()
        {
            // Instantiation
            observers = new List<IObserver<TsuObject>>();
            this.childModules = new ChildItemCollection<Module, Module>(this);
            this.CreatedOn = DateTime.Now;
            //this.Initialize(); //remove to avoid to have 2 to the child module in the list
        }

        public Module(IModule parentModule, string nameAndDescription = "", bool createView = true) : base()
        {
            // Instantiation
            observers = new List<IObserver<TsuObject>>();

            // choose to create or not the view now
            if (createView)
            {
                Module pm = (parentModule as Module);
                if (pm != null && !(this is I.Module))
                    createView = pm.CreateViewAutmatically;
            }
            this.CreateViewAutmatically = createView;

            this.childModules = new ChildItemCollection<Module, Module>(this);

            // Sharing
            this.ParentModule = parentModule as Module;
            this.CreatedOn = DateTime.Now;
            if (nameAndDescription != "")
            {
                this.NameAndDescription = nameAndDescription;
                this._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(nameAndDescription);
                this.Utility = T.Conversions.StringManipulation.SeparateTitleFromMessage(nameAndDescription);
            }

            this.Initialize();
        }

        public virtual void Initialize()
        {
            CreateView();
            SubscribeWithParent();
        }

        internal string GeTSUbscriberNames()
        {
            string s = "";
            foreach (var item in observers)
            {
                if (s != "")
                    s += " and ";
                if (item is Module)
                    s += (item as TsuObject).ToString();
                else
                    s += (item as TsuView)._Name;
            }
            return s;
        }

        // after deserialisation some stuff are missing because we didnt not want to serailized everything, here is the place to recreate what is missing
        internal virtual void ReCreateWhatIsNotSerialized(bool saveSomeMemory = false)
        {
            try
            {
                if (this.View == null)
                    CreateView();
                SubscribeWithParent();
                //foreach (var item in this.childModules)
                //{
                //    item.ReCreateWhatIsNotSerialized();
                //    this.View.AddView(item.View);
                //}
            }
            catch (Exception ex)
            {
                throw new Exception($"Cannot recreate what was not serialized in '{this._Name}", ex);
            }
        }

        internal void SubscribeWithParent()
        {
            // Automatic suscribing of a parent to his child or of the log to his parent
            this.Subscribe(this.ParentModule);
        }

        internal void CreateViewIfNull()
        {
            if (_TsuView == null)
            {
                CreateViewAutmatically = true;
                CreateView();
            }
        }

        internal void CreateView()
        {
            if (this.ParentModule != null && this.ParentModule.View != null && this.ParentModule.View.InvokeRequired)
            {
                Tsunami2.View.Invoke(new System.Action(() => { CreateView(); }));
                return;
            }

            // create the right view
            Globals.numberCreated[1]++;
            if (CreateViewAutmatically)
            {
                Globals.numberCreated[0]++;

                this._TsuView = Creator.Do(this);
                Globals.createdViews.Add(this._TsuView);
            }
        }

        #endregion

        #region  Methods
        public virtual void Add(Module module)
        {
            if (!(this.childModules.Contains(module)))
            {
                this.childModules.Add(module);
                if (this._TsuView != null && module._TsuView != null)
                {
                    this._TsuView.AddView(module._TsuView);
                    //module._TsuView.UpdateView();
                }

            }
            else
                module.View.BringToFront();
        }
        public virtual void UpdateView()
        {
            if (this._TsuView == null) return;
            if (this._TsuView.InvokeRequired)
            {
                // Using Invoke for synchronous execution
                this._TsuView.Invoke(new System.Action(() => UpdateView()));
            }
            this._TsuView?.UpdateView();
        }

        public event EventHandler<ModuleEventArgs> Started;

        public virtual void Start()
        {
            Started?.Invoke(this, new ModuleEventArgs(this));
        }

        public virtual void Restart()
        {
            
        }


        public virtual void Finish() { }

        #endregion

        #region  Observer-Observable


        internal void UnHandledManager_Changed(object source, ModuleChangeEventArgs args)
        {
            TSU.Debug.WriteInConsole($"{DateTime.Now:HH.mm.ss.ffffff} : TSU.UnHandled '{this._Name}' detected a change in '{args.Module._Name}' but has nothing to do");
        }

        /// <summary>
        /// Need to be override in the module where you want to customize the event 
        /// </summary>
        /// <param name="TsuObject"></param>
        public virtual void OnNext(TsuObject TsuObject)
        {
            dynamic dynamic = TsuObject;
            OnNextBasedOn(dynamic);
        }

        public virtual void OnNextBasedOn(TsuObject TsuObject)
        {
            //string message = string.Format(R.T065, this._Name, TsuObject._Name, TsuObject.GetType().ToString());
            //if (!(this is Log)) Logs.Log.AddEntryAsFYI(this, message);
        }

        internal virtual void SendMessage(TsuObject e)
        {
            int count = 0;
            if (observers != null)
                foreach (var observer in observers)
                {
                    if (e == null)
                        observer.OnError(new NotImplementedException());
                    else
                    {
                        count++;
                        observer.OnNext(e);
                        string message = "TSU.MESSAGE.SENT " + string.Format(R.T066,
                            this._Name,
                            this.GetType().ToString(),
                            observer.GetType().ToString(),
                            e.GetType().ToString(),
                            DateTime.Now.ToString(),
                            count.ToString());
                        TSU.Debug.WriteInConsole(message);
                    }
                    string message2 = "TSU.MESSAGE.SENT " + string.Format("{4}n&gt; &gt; &gt; &gt; &gt; Module '{0}' of type '{1}' Sent '{3}' to observer number '{5}' '{2}'.",
                            this._Name,
                            this.GetType().ToString(),
                            observer.GetType().ToString(),
                            e.GetType().ToString(),
                            DateTime.Now.ToString(),
                            count.ToString());
                    TSU.Debug.WriteInConsole(message2);
                }
        }

        internal virtual void SubscribeLogToEvents()
        {
            UnSubscribeLogToEvents();
            // Tolerance be overrided by modue taht want to show stuff in the main log;
        }

        internal virtual void UnSubscribeLogToEvents()
        {
            // Tolerance be overrided by modue taht want to show stuff in the main log;
        }

        [XmlIgnore]
        internal List<IObserver<TsuObject>> observers;
        public IDisposable Subscribe(IObserver<TsuObject> observer)
        {
            if (observer == null) return null;
            if (!observers.Contains(observer))
            {
                observers.Add(observer);

                Debug.WriteInConsole(string.Format("TSU.SUBSCRIPTION " + R.T067,
                    "",
                    (observer as ITsuObject)._Name,
                    this._Name,
                    observer.GetType().ToString(),
                    this.GetType().ToString(),
                    observers.Count.ToString()));
            }
            return new Unsubscriber(observers, observer);
        }

        public IDisposable UnSubscribe(IObserver<TsuObject> observer)
        {
            if (observer == null) return null;
            if (observers.Contains(observer))
            {
                observers.Remove(observer);
            }
            return new Unsubscriber(observers, observer);
        }



        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public virtual void Dispose()
        {
            this.ParentModule = null;
            this.Tag = null;
            this.observers.Clear();
            this._TsuView?.Dispose();

            foreach (Module item in childModules)
            {
                item?.Dispose();
            }
        }

        private void EndTransmission()
        {
            foreach (var observer in observers.ToArray())
                if (observers.Contains(observer))
                    observer.OnCompleted();
            observers.Clear();
        }
        protected IDisposable unsubscriber;
        internal class Unsubscriber : IDisposable
        {
            private List<IObserver<TsuObject>> _observers;
            private IObserver<TsuObject> _observer;
            public Unsubscriber(List<IObserver<TsuObject>> observers, IObserver<TsuObject> observer)
            {
                this._observers = observers;
                this._observer = observer;
            }
            public void Dispose()
            {
                if (_observer != null && _observers.Contains(_observer))
                    _observers.Remove(_observer);
            }
        }

        #endregion

        #region Events

        [field: XmlIgnore]
        public event EventHandler<ModuleChangeEventArgs> Changed;

        public void Change()
        {
            int id = Debug.GetNextId();
            Debug.WriteInConsole("Begin Change in " + this._Name,id);
            Changed?.Invoke(this, new ModuleChangeEventArgs(this));

            // if this removed the name of selected item dont show in the main button
            //this.UpdateView();
            // but if is there the staintili module.change() tourne a l'infini.
            Debug.WriteInConsole("End Change in " + this._Name, id);
        }

        #region event

        public class MessageEventArgs : EventArgs
        {
            public Module Module;
            public string Message;
        }

        public event EventHandler<MessageEventArgs> MessageBroadcast;

        protected virtual void OnMessageBroadcast(MessageEventArgs e)
        {
            MessageBroadcast?.Invoke(this, e);
        }

        internal void BroadcastMessage(string message)
        {
            Logs.Log.AddEntryAsFYI(this, $"Message Broadcasted: {message}");
            OnMessageBroadcast(new MessageEventArgs() { Message = message, Module = this }); //Raises the Tick event
        }

        #endregion


        #endregion
        /// <summary>
        /// Fonction pour invoquer vue du main thread lorsqu'on est dans un autre thread
        /// </summary>
        /// <param name="action"></param>
        public void InvokeOnApplicationDispatcher(Action action)
        {
            try
            {

                if (Tsunami2.Properties == null)
                {
                    if (Debug.IsRunningInATest)
                    {
                        action();
                        return;
                    }
                    else
                        throw new TsunamiException($"Tsunami.Properties is not yet created, the action '{action}' cannot be invoke");
                }
                if (Tsunami2.Properties.View != null && Tsunami2.Properties.View.IsHandleCreated) // because splash view handle is created bafore the main tsunami view
                    Tsunami2.Properties.View.Invoke(action);
                else if (Tsunami2.Properties.View.SplashScreenView != null && Tsunami2.Properties.View.SplashScreenView.IsHandleCreated) // because splash view handle is created bafore the main tsunami view
                    Tsunami2.Properties.View.SplashScreenView.Invoke(action);
                else
                {
                    if (Debug.IsRunningInATest)//&& action.Method.Name.Contains("HandleMeasureReceivedEvent"))
                    {
                        action();
                    }
                    else
                    {
                        if (Tsunami2.Properties.View.SplashScreenView.InvokeRequired)
                            throw new TsunamiException($"Tsunami.Properties.View.SplashScreenView Handle is not yet created, the action '{action}' cannot be invoke");
                        else
                            action();
                    }
                }
            }
            catch (System.Threading.Tasks.TaskCanceledException ex )
            {
                Logs.Log.AddEntryAsPayAttentionOf(this, ex.Message);
            }
            catch (Common.Instruments.CancelException ex)
            {
                Logs.Log.AddEntryAsPayAttentionOf(this, ex.Message);
            }
            catch (Exception ex)
            {
                if (Tsunami2.Properties.View != null && Tsunami2.Properties.View.IsHandleCreated)
                {
                    Tsunami2.Properties.View.Invoke((MethodInvoker)delegate {
                        var messageInput = new MessageInput(MessageType.Critical, ex.Message + "\r\n" + "\r\n" + ex.StackTrace);
                        messageInput.Show(); // Use ShowDialog if you want it to block until dismissed
                    });
                }
                else
                    MessageBox.Show(ex.Message + "\r\n" + "\r\n" + ex.StackTrace);
            }
        }
        /// <summary>
        /// Define the wanted group name for new points
        /// </summary>
        /// <returns></returns>
        internal virtual string GetNewPointGroupName()
        {
            return this._Name;
        }
    }

    public class ModuleChangeEventArgs : EventArgs
    {
        public List<TsuObject> Elements;
        public Module Module;
        public ModuleChangeEventArgs(Module module)
        {
            this.Module = module;
            if (module is Manager)
                this.Elements = (module as Manager).AllElements;
        }
    }


}
