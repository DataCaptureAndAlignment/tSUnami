﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSU
{
    public class TsuException : Exception
    {
        public int Id;

        public TsuException()
        {
            this.Id = 0;
        }


        public TsuException(string name, int uniqueId)
        : base(name)
        {
            this.Id = uniqueId;
        }
        public TsuException(string message) : base(message)
        {
            this.Id = 0;
        }

        public TsuException(string message, Exception ex) : base(message, ex) { }
    }

    public class TsuCancelException : TsuException
    {
        public TsuCancelException(string message) : base(message) { }

        public TsuCancelException(string message, Exception ex) : base(message, ex) { }
    }
}
