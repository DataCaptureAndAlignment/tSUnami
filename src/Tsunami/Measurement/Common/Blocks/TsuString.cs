﻿using System;

namespace TSU
{
    public class TsuString
    {


        public static string FindNextName(string name)
        {
            try
            {
                string nextName;

                int n;
                if (name[name.Length - 1] == ')')
                {
                    int open = name.LastIndexOf('(');
                    int index;
                    string indexString = name.Substring(open + 1, name.Length - open - 2);
                    if (Int32.TryParse(indexString, out index))
                    {
                        index += 1;
                        nextName = name.Substring(0, open) + "(" + index.ToString() + ")";
                    }
                    else
                        nextName = name + "(1)";
                }
                else
                {
                    int i = 1;
                    int index = 0;
                    while (int.TryParse(name.Substring((name.Length - i), i), out n))
                    {
                        index = n;
                        i += 1;
                    }
                    if (index != 0)
                    {
                        nextName = name.Substring(0, name.Length - i + 1) + (index + 1).ToString();
                    }
                    else
                        nextName = nextName = name + "(1)";
                }
                return nextName;
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot find next name in FindNextName()", ex);
            }
        }
    }
}
