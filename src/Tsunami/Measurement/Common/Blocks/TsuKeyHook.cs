﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using TSU.Common;
using TSU.Views.Message;

namespace TSU
{
    // not used
    public class InterceptKeys
    {
        private const int WH_KEYBOARD_LL = 13;
        private const int WM_KEYDOWN = 0x0100;
        private static LowLevelKeyboardProc _proc = HookCallback;
        private static IntPtr _hookID = IntPtr.Zero;

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        private static IntPtr SetHook(LowLevelKeyboardProc proc)
        {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule)
            {
                return SetWindowsHookEx(WH_KEYBOARD_LL, proc,
                    GetModuleHandle(curModule.ModuleName), 0);
            }
        }

        public static void ListenKeys()
        {
            _hookID = SetHook(_proc);
        }

        public static void StopListenKeys()
        {
            UnhookWindowsHookEx(_hookID);
        }

        private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);

        private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
        {
            IntPtr intPtr = CallNextHookEx(_hookID, nCode, wParam, lParam);
            if (nCode >= 0 && wParam == (IntPtr)WM_KEYDOWN)
            {
                int vkCode = Marshal.ReadInt32(lParam);
                Control focusedControl = FindFocusedControl(TSU.Tsunami2.Preferences.Tsunami.View);

                bool treatTheKeyStroked = true;

                if (focusedControl != null)
                {
                    if (focusedControl is TextBox)
                        treatTheKeyStroked = false;
                    if (focusedControl is ComboBox)
                        treatTheKeyStroked = false;
                    if (focusedControl is DataGridView)
                        treatTheKeyStroked = false;
                }

                if(treatTheKeyStroked)
                    ThreatTheKey((Keys)vkCode);
            }

            return intPtr;
        }

        private static void ThreatTheKey(Keys vkCode)
        {
            try
            {
                if (vkCode == Keys.M)
                {
                    List<Common.Station.Module> sms = Common.Analysis.Module.GetActiveStationModule(TSU.Tsunami2.Preferences.Tsunami);
                    if (sms.Count == 1)
                    {
                        if (sms[0] is TSU.Polar.Station.Module)
                        {
                            (sms[0] as TSU.Polar.Station.Module)._InstrumentManager.SelectedInstrumentModule.Measure();
                        }
                    }
                }
            }
            catch (Exception ex )
            {
                string titleAndMessage = $"Shortcut '{vkCode.ToString()}' failed;{ex.Message} ";
                new MessageInput(MessageType.Warning, titleAndMessage).Show();
            }
            
        }

        
        public static Control FindFocusedControl(Control control)
        {
            var container = control as IContainerControl;
            while (container != null)
            {
                control = container.ActiveControl;
                container = control as IContainerControl;
            }
            return control;
        }
    }
}