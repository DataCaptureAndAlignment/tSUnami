﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;using R = TSU.Properties.Resources;
using System.Threading.Tasks;

namespace TSU
{
    public class TsuTask
    {
        public Action preAction;
        public Action mainAction;
        public Action postAction;
        public Action cancelAction;
        public Action timeOutAction;
        public Action ExceptionAction;
        public int timeOutInSec = 10;
        public int estimatedTimeInMilliSec = 500;
        public string ActionName = "Action";
    }
}
