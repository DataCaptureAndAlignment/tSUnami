﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TSU.Common.Blocks
{
    public class Selectable<T>
    {
        /// <summary>
        /// Manage a selection change
        /// Items removed from the selection are removed from the selectedItems list
        /// Items added to the selection are added at the end of the selectedItems list
        /// So, the items that have been selected longer come first in the list,
        /// which allows sorting by selection order
        /// In addition, the selection changes raise an event
        /// </summary>
        public bool Selected
        {
            get
            {
                if (Item is IEquatable<T> ie)
                    return selectedItems.Any(x => ie.Equals(x));
                else
                    return selectedItems.Contains(Item);
            }
            set
            {
                bool valueBefore = Selected;
                if (value != valueBefore)
                {
                    if (value)
                        selectedItems.Add(Item);
                    else
                        selectedItems.Remove(Item);
                    OnSelectedChanged?.Invoke(this, new EventArgs());
                }
            }
        }

        public static bool IsSelected(Selectable<T> point)
        {
            return point.Selected;
        }

        public static bool IsNotSelected(Selectable<T> point)
        {
            return !point.Selected;
        }

        public T Item { get; private set; }

        public event EventHandler OnSelectedChanged;

        private readonly IList<T> selectedItems;

        public Selectable(IList<T> selectedItems, T item)
        {
            Item = item;
            this.selectedItems = selectedItems;
        }
    }
}
