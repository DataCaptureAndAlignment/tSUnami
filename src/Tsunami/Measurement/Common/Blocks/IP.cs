﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Windows.Forms;
using System.Xml;
using TSU.Common.Instruments;
using TSU.Views;
using TSU.Views.Message;
using R = TSU.Properties.Resources;

namespace TSU
{
    public class Ip
    {
        readonly IPAddress Address;
        public string lastAdded = "";

        public Ip(string address)
        {
            if (!IPAddress.TryParse(address, out IPAddress ip))
                throw new Exception("Not a valid IP adress");
            Address = ip;
        }

        public Ip(IPAddress ip)
        {
            Address = ip;
        }

        public static Ip GetMyIp()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    if (ip.ToString().Contains("192.168."))
                        return new Ip(ip);
                    if (ip.ToString().Contains("169.254."))
                        return new Ip(ip);
                }
            }

            throw new Exception("No IP containing '192.168.x.x' or'169.254.x.x' are available.\r\n" +
                "Please check that the local Wifi is selected or the local cable is plugged?");
        }

        internal bool IsPingable
        {
            get
            {
                return Ping(Address.ToString(), 4);
            }
        }


        internal static int EvaluateIfDuplicated(string ipaddress)
        {
            /* 0 - Invalid
             * 1 - Tentative - duplication evaulation not successfully
               2 - Duplicate
               3 - Deprecated - valid but it will be used by other app
               4 - Preferred - valid
            */

            try
            {
                int fieldValue = 4;

                foreach (NetworkInterface adapter in NetworkInterface.GetAllNetworkInterfaces())
                {
                    IPInterfaceProperties adapterProperties = adapter.GetIPProperties();

                    foreach (UnicastIPAddressInformation uni in adapterProperties.UnicastAddresses)
                    {

                        if (ipaddress == uni.Address.ToString())
                        {
                            fieldValue = (int)uni.DuplicateAddressDetectionState;
                        }
                    }
                }
                return fieldValue;
            }
            catch (Exception)
            { return 0; }
        }

        public static bool Ping(string ipAdress, int maxRetries = 4)
        {
            for (int retries = 0; retries < maxRetries; retries++)
            {
                try
                {
                    using (Ping pingClass = new Ping())
                    {
                        int timeout = 500;
                        PingReply pingReply = pingClass.Send(ipAdress, timeout);
                        if (pingReply.Status == IPStatus.Success)
                            return true;
                    }
                }
                catch (Exception)
                {
                    // Do nothing, we will retry
                }
            }
            return false;
        }

        public override string ToString()
        {
            return this.Address.ToString();
        }

        public string Selected;

        public static List<Ip> GetIpsForGivenInstrument(string InstrumentSerialNumber, Dictionary<string, string> dictionnaryOfSnAndIps)
        {
            List<Ip> founds = new List<Ip>();
            foreach (KeyValuePair<string, string> entry in dictionnaryOfSnAndIps)
            {
                string serialNumber = entry.Key.Substring(0, entry.Key.IndexOf('_'));
                if (serialNumber == InstrumentSerialNumber)
                    if (!founds.Exists((i) => i.ToString() == entry.Value))
                        founds.Add(new Ip(entry.Value));
            }
            founds.Reverse(); // so  that the last added is tried first.
            return founds;
        }

        public static Ip GetASingleIpForGivenInstrument(string InstrumentSerialNumber, Dictionary<string, string> dictionnaryOfSnAndIps)
        {
            Dictionary<string, Ip> founds = new Dictionary<string, Ip>();
            foreach (KeyValuePair<string, string> entry in dictionnaryOfSnAndIps)
            {
                string s = entry.Key.Substring(0, entry.Key.IndexOf('_'));
                if (s == InstrumentSerialNumber)
                    if (!founds.ContainsKey(entry.Value))
                        founds.Add(entry.Value, new Ip(entry.Value));
            }
            switch (founds.Count)
            {
                case 0:
                    return new Ip("192.168.0.100");
                case 1:
                    return founds.Values.First();
                default:
                    throw new Exception("Too much IP defined for this instrument inside your AT40x_IP.xml");
            }
        }


        /// <summary>
        /// WIll propose to select pingable known IPS or to enter one.
        /// </summary>
        internal static bool GetFirstKnownThatReplyToPing(Common.Instruments.Device.AT40x.View view,
                                             string serialNumber,
                                             out Ip defaultIp,
                                             bool silent = false,
                                             bool orFirstDefault = false)
        {
            defaultIp = new Ip("192.168.0.100");

            // Get the known IP
            List<Ip> knowns = GetIpsForGivenInstrument(
                serialNumber,
                Tsunami2.Preferences.Values.At401IpAdresses);

            if (knowns.Count == 0)
                knowns.Add(defaultIp);

            // Check if they are responding to ping
            List<Ip> pingables = new List<Ip>();

            string ipList = "";
            string pingableList = "";
            string proposedIp = "";
            MessageType messageType = MessageType.GoodNews;
            string message;

            Ip myIp = null;

            string buttonLeftText = "";
            string buttonRightText = "";

            bool aSingleAdressFound = false;

            try
            {
                myIp = GetMyIp();

                foreach (Ip item in knowns)
                {
                    // all ip
                    if (ipList != "")
                        ipList += ", ";
                    ipList += item.ToString();

                    // responding ip
                    if (item.IsPingable && item.Address != myIp.Address)
                    {
                        pingables.Add(item);
                        if (pingableList != "")
                            pingableList += ", ";
                        pingableList += item.ToString();
                    }
                }

                switch (pingables.Count)
                {
                    case 0: // not a single knonw adress to respond on ping command
                        proposedIp = "169.254.";
                        messageType = MessageType.Warning;
                        message = $"{R.T_NO_IP_IS_RESPONDING};{R.T_PING_FAILED_ON_THE_FOLLOWING_ADRESS}: '{ipList}'";
                        buttonLeftText = "Ping";
                        buttonRightText = "Save Me";
                        break;
                    case 1:
                        proposedIp = pingables[0].ToString();
                        messageType = MessageType.GoodNews;
                        message = $"{R.T_ONE_IP_IS_RESPONDING};{R.T_PING_SUCCEED_ON_THE_FOLLOWING_ADRESS}: '{proposedIp}'";
                        buttonLeftText = "";
                        aSingleAdressFound = true;
                        break;
                    default:
                        proposedIp = pingables[pingables.Count - 1].Address.ToString();
                        messageType = MessageType.Warning;
                        message = $"{R.T_SEVERAL_IP_ARE_RESPONDING};{R.T_PINGS_SUCCEED_ON_THE_FOLLOWING_ADRESSES}: '{pingableList}'";
                        buttonLeftText = "Ping";
                        break;
                }
            }
            catch (Exception ex)
            {
                proposedIp = "192.168.0.100";
                messageType = MessageType.Critical;
                message = $"{R.T_NO_LOCAL_ADRESS_EXISTING};{ex.Message}";
                buttonLeftText = "Ping";
            }

            if (silent)
            {
                if (pingables.Count != 1)
                {
                    if (!orFirstDefault)
                        throw new Exception("Couldn't get a single proposed IP in silent mode");
                }
                return true;
            }

            bool skipMessage = false;
            if (aSingleAdressFound)
                if (!IsDuplicated(proposedIp))
                    skipMessage = true;

            if (!skipMessage)
            {
                message = $"{message}\r\n({R.T_TESTED_ADRESSES_COME_FROM} {MessageTsu.GetLink(Tsunami2.Preferences.Values.Paths.At40xIp)})";

                MessageInput mi = new MessageInput(messageType, message)
                {
                    ButtonTexts = new List<string> { buttonLeftText, R.T_USE, R.T_CANCEL, buttonRightText },
                    Controls = new List<Control> { CreationHelper.GetPreparedTextBox(proposedIp) }
                };
                using (var r = mi.Show())
                {
                    if (r.TextOfButtonClicked == R.T_CANCEL)
                        throw new CancelException();

                    string address = (r.ReturnedControls[0] as TextBox).Text;
                    if (r.TextOfButtonClicked == buttonLeftText) // should mean "PING"
                    {
                        PingInCmdWindow(address);
                        return false;
                    }
                    else if (r.TextOfButtonClicked == buttonRightText)
                    {
                        view.Module.SaveMeFromNotWorkingConnection(view, new EventArgs());
                        return true;
                    }
                    else
                    {
                        defaultIp = new Ip(address);
                        view.Module._status.IsWaiting = false;
                        return true;
                    }
                }
            }
            else
            {
                defaultIp = new Ip(proposedIp);
                view.Module._status.IsWaiting = false;
                return true;
            }
        }


        internal static void PingInCmdWindow(string address)
        {
            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WindowStyle = ProcessWindowStyle.Normal;
            // the cmd program
            startInfo.FileName = "cmd.exe";
            // set my arguments. date is just a dummy example. the real work isn't use date.
            startInfo.Arguments = $"/k ping {address}";
            //startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;
            process.StartInfo = startInfo;
            process.Start();
        }

        public static bool IsDuplicated(string ipAddress)
        {
            const int validAddressValue = 4;
            const int duplicateAddressValue = 2;
            const int inValidAddressValue = 0;

            var isDuplicated = EvaluateIfDuplicated(ipAddress);
            if (isDuplicated != validAddressValue)
            {
                switch (isDuplicated)
                {
                    case inValidAddressValue:
                        throw new Exception("Invalid IP Adress, cannot evaluate if it is duplicated or not");
                    case duplicateAddressValue:
                        Logs.Log.AddEntryAsError(null, R.IpDuplicated);
                        return true;
                }
            }
            return false;
        }

        public static string GetIPFromXML(XmlDocument xmlDocument, string serialNumber)
        {

            string ipAdress = "";
            foreach (XmlNode node in xmlDocument.DocumentElement.ChildNodes)
            {
                if (node.Attributes["SN"]?.InnerText == serialNumber)
                    ipAdress = node.Attributes["IP"]?.InnerText;
            }

            return ipAdress;

        }


        public static bool PingFromCMD(string ipAdress, bool useShell = false)
        {
            Process p = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = "CMD.exe",
                    Arguments = $"/c ping {ipAdress}",
                    UseShellExecute = useShell,
                    Verb = "runas",
                    RedirectStandardOutput = false,
                    CreateNoWindow = true
                }
            };
            p.Start();
            p.WaitForExit();

            bool pingExpected = p.ExitCode == 0;
            return pingExpected;

        }
    }
}
