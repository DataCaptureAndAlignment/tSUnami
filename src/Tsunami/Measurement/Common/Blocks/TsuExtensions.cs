﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Text;
using TSU.Common;
using TSU.Common.Compute.Compensations.BeamOffsets;
using TSU.Common.Elements;
using TSU.Common.Measures;
using TSU.Polar.GuidedModules.Steps;
using TSU.Views;

namespace TSU.Tools
{
    public static class Extensions
    {
        /// <summary>
        /// extension to be able to get the next ENUM
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="src"></param>
        /// <returns></returns>
        public static T Next<T>(this T src) where T : struct
        {
            if (!typeof(T).IsEnum) throw new ArgumentException(String.Format("Argument {0} is not an Enum", typeof(T).FullName));

            T[] Arr = (T[])Enum.GetValues(src.GetType());
            int j = Array.IndexOf<T>(Arr, src) + 1;
            return (Arr.Length == j) ? Arr[0] : Arr[j];
        }

        public static T Previous<T>(this T src) where T : struct
        {
            if (!typeof(T).IsEnum) throw new ArgumentException(String.Format("Argument {0} is not an Enum", typeof(T).FullName));

            T[] Arr = (T[])Enum.GetValues(src.GetType());
            int j = Array.IndexOf<T>(Arr, src) - 1;
            return (j == -1) ? Arr[Arr.Length - 1] : Arr[j];
        }

        public static void Use<T>(this T item, Action<T> work) // to replace the .with from VB
        {
            work(item);
        }
        /// <summary>
        /// return the oldest
        /// </summary>
        /// <param name="date"></param>
        /// <param name="newDate"></param>
        /// <returns></returns>
        public static DateTime ReplaceIfNewerThan(this DateTime date, DateTime newDate)
        {
            // if newDate is older we replace date by newDate
            if (newDate < date)
                return newDate;
            else
                return date;
        }

        /// <summary>
        /// return the newest
        /// </summary>
        /// <param name="date"></param>
        /// <param name="newDate"></param>
        /// <returns></returns>
        public static DateTime ReplaceIfOlderThan(this DateTime date, DateTime newDate)
        {
            // if newDate is newer we replace date by newDate
            if (newDate < date)
                return newDate;
            else
                return date;
        }
    }
}
