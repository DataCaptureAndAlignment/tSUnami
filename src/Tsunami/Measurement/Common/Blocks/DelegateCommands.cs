﻿using System;
using System.Windows.Input;

namespace TSU.Common.Blocks
{
    internal abstract class DelegateCommandBase : ICommand
    {
        public abstract void Execute(object parameter);

        public abstract bool CanExecute(object parameter);

        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }
    }

    internal class DelegateCommand : DelegateCommandBase
    {
        private readonly Action _action;
        private readonly Func<bool> _canExecute;

        public DelegateCommand(Action action, Func<bool> canExecute = null)
        {
            _action = action ?? throw new ArgumentNullException(nameof(action));
            _canExecute = canExecute;
        }

        public override void Execute(object obj)
        {
            _action?.Invoke();
        }

        public override bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke() ?? true;
        }
    }

    internal class DelegateParameterCommand : DelegateCommandBase
    {
        private readonly Action<object> _action;
        private readonly Func<object, bool> _canExecute;

        public DelegateParameterCommand(Action<object> action, Func<object, bool> canExecute = null)
        {
            _action = action ?? throw new ArgumentNullException(nameof(action));
            _canExecute = canExecute;
        }

        public override void Execute(object parameter)
        {
            _action(parameter);
        }

        public override bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke(parameter) ?? true;
        }
    }
}
