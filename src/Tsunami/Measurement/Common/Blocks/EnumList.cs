﻿using System;
using System.Collections;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace TSU.ENUM
{

    // Advanced.Theodolite
    public enum StationProperties
    {
        All,
        Admin,
        Next,
        NextDefault,
        Date,
        dHStakeOut,
        Team,
        Instrument,
        Operation,
        Operation_Initiale,
        Operation_Reglage,
        DefaultPointName,
        DefaultPointCode,
        NextPoint,
        NextPoints,
        Extension,
        NextReflector,
        InstrumentHeight,
        NextComment,
        NextFace,
        v0,
        Point,
        Orientation,
        Measures,
        Status,
        Measure,
        UnknowNameType,
        Tolerance,
        HStation,
        EmqStation,
        EmqAller,
        EmqRetour,
        TiltTheo,
        PointNameTheo,
        Average_Tilt,
        Emq_Tilt,
        Ecart_Tilt,
        Tilt_Tolerance,
        Theo_Tolerances,
        Theo_Sigma,
        Theo_Sigmas,
        Setup,
        Verticalisation,
        NumberOfMeasurement,
        SocketCode,
        TheoFile,
        Interfaces
    }
    public enum MeasureProperties
    {
        Date,
        Point,
        MeasureState,
        Comment,
        Station,
        Theodolite,
        Angles,
        Distances,
        Raw,
        Corrected,
        Face,
        Face1,
        Face2,
        DoubleFace,
        Horizontal,
        Vertical,
        Value,
        Sigma,
        Beam_Direction,
        Opposite_Beam,
        Average_Both_Direction,
        Mode,
        NumberOfMeasureToAverage,
        Reflector
    }
    
    public enum MeasurementType
    {
        All,
        Dist,
        HzV
    }
    

    // Element
    //[Flags]
    public enum ElementType
    {
        Unknown,
        [XmlEnum("Accelerateur")]
        Accelerator,
        Class,
        Numero,
        [XmlEnum("point")]
        Point,
        Cumul,
        X, 
        Y,
        Z,
        H,
        Length,
        Tilt, 
        Slope,
        BeamBearing,
        SocketCode,
        SocketType,
        Date, 
        SerialNumber,
        Comment,
        CoordinateSystem,
        Parameters,
        File,
        SequenceTH, SequenceFil, SequenceNiv, Sequence,
        Line,
        Plane,
        Circle,
        Sphere,  
        Cylinder,
        Fit,
        Bearing,
        TheoreticalFile,
        SequenceFile,
        Alesage,
        Pilier,
        PointATracer,
        Rotations,
        TheoreticalPoint,
        Translations,
        LineAsPoint,
    }
    public enum ElementViewType
    {
        ListView,
        TreeView
    }
    public enum ElementListViewSubItemList
    {
        Name,   //0
        Type,
        Group,
        Acc_Zone,
        Class,
        Numero,
        Element,  //5
        Xccs,
        Yccs,
        Zccs,
        H,
        Xloc,
        Yloc,
        Zloc,
        Xphy,
        Yphy,
        Zphy,
        Tstn,
        Rstn,
        Vloc,
        Lbeam,
        Rbeam,
        Hbeam,
        Cumul,
        L,
        Gis,
        Slope,
        T,
        Tilt,
        Code,
    }
    
    public enum StationListViewSubItemList
    {
        Name,
        Status,
        Date,
        Point,
        Station,
        Hz,
        V,
        Dist,
        Face,
    }
    // Displacement
    public enum movingDirection
    { 
        None, 
        Up, 
        Down, 
        Left, 
        Right, 
    };

    
    
    // Distance&Reflector
    public enum DistanceMeasurementMode
    {
        WithReflector,
        Reflectorless
    }
    
    
    // Module&Station
    public enum StationType
    {
        Theodolite,
        Levelling,
        Tilt,
        Ecartometry
    }
    public enum ModuleType
    {
        Element,
        Instrument,
        Measurement,
        Theodolite,
        Levelling,
        LengthSetting,
        Tilt,
        Ecartometry,
        MagnetAlignment,
        Station,
        StationTheodolite,
        StationEcarto,
        StationLevelling,
        StationTilt,
        testDll,
        testTDA,
        testInstrument,
        TestModule,
        Splash,
        Quit,
        Exploration,
        Guided,
        Advanced
    }

    // type used at least to select the color of the node, should be sorted by best to worst
    internal enum StateType
    {
        Normal = 0,
        Ready = 1,
        Acceptable = 2,
        NotReady = 3,
        Unknown = 4,
        ToBeFilled = 5,
    }
    //

    public enum ReductionType
    {
        ByAverage,
        ByNumberOfMeasure,
        ByTime,
    }
    // LGC
    public enum LgcGeoide
    {
        OLOC,
        RS2K,
    }
    //public enum Management.Zone
    //{
    //    Alice,
    //    Atlas,
    //    CMS,
    //    LHCb,
    //    Dirac,
    //    NA62_1,
    //    NA62_2,
    //    NA62_3,
    //    NA62_4,
    //    PA4LHC,
    //    PA6LHC
    //}
    //Type de calage point pour fichier LGC
    public enum LgcPointOption
    {
        CALA,
        POIN,
        VXY,
        VXZ,
        VYZ,
        VZ,
        PDOR
    }
    public enum LevelingDirection
    {
        ALLER,
        RETOUR,
        REPRISE
    }
    // Other
    public enum AngularUnit
    {
        Gon, 
        Rad, 
        deg
    }
    
    public enum MenuType
    {
        Advanced,
        Guided,
        All
    }
    public enum CoordinatesType
    {
        CCS,
        SU,
        UserDefined
    }
    /// <summary>
    /// Enum donnant la direction de mesure du tilt
    /// </summary>
    public enum TiltDirection
    {
        Both,
        Beam,
        Opposite  
    }
    /// <summary>
    /// Indique si c'est une mesure init ou de réglage dans les paramètres de la station tilt
    /// </summary>
    public enum TiltInitOrAdjusting
    {
        Initial,
        Adjusting
    }
    public enum TiltViewType
    {
        Treeview,
        DataGridView
    }
    /// <summary>
    /// Sens du tri des points suivant decum ou inverse
    /// </summary>
    public enum SortDirection
    {
        Dcum,
        InvDcum
    }
    /// <summary>
    /// Permet de distinguer le type de module guidé après ouverture d'un fichier de sauvegarde
    /// </summary>
    public enum GuidedModuleType
    {
        NoType,
        Alignment,
        AlignmentEcartometry,
        AlignmentImplantation,
        AlignmentLevelling,
        AlignmentLevellingCheck,
        AlignmentManager,
        AlignmentTilt,
        AlignmentLengthSetting,
        Cheminement,
        Ecartometry,
        Example,
        Implantation,
        LengthSetting,
        PointLance,
        PreAlignment,
        Survey1Face,
        Survey2Face,
        Survey2FaceArray,
        SurveyTilt,
        TourDHorizon,
        EquiDistance
    }
    /// <summary>
    /// Type of message for TSUString
    /// </summary>
    public enum TSUStringMessage
    {
        CoordinateChanged,
    }
    public enum ThemeColorName
    {
        Normal,
        Sick,
        Pink,
        RainBow,
        Dark,
        Light,
        Gray
    }
}
