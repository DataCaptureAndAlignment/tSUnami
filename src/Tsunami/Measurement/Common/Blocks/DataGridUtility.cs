﻿using System;
using System.Windows.Forms;
using System.ComponentModel;
namespace TSU
{
    public class DataGridUtility
    {

        private static ListSortDirection _oldSortOrder;
        private static DataGridViewColumn _oldSortCol;
        private static DataGridViewColumn _colToNotSort;
        /// <summary>
        /// Saves information about sorting column, to be restored later by calling RestoreSorting
        /// on the same DataGridView
        /// </summary>
        /// <param name="grid"></param>
        public static void SaveSorting(DataGridView grid)
        {
            
            TSU.Debug.WriteInConsole($"sorting saved");

            _oldSortOrder = grid.SortOrder == SortOrder.Ascending ?
                ListSortDirection.Ascending : ListSortDirection.Descending;
            if (grid.SortedColumn !=null)
                _oldSortCol = grid.SortedColumn;
        }

        /// <summary>
        /// Restores column sorting to a datagrid. You MUST call this AFTER calling 
        /// SaveSorting on the same DataGridView
        /// </summary>
        /// <param name="grid"></param>
        public static void RestoreSorting(DataGridView grid)
        {

            TSU.Debug.WriteInConsole($"restoring saved");

            if (_oldSortCol != null)
            {
                // pas de tri sur la colonne Dcum pour ne pas perdre le tri manuel par Dcum en nombre
                if (_colToNotSort != null)
                {
                    if (_colToNotSort.Name != _oldSortCol.Name)
                    {
                        DataGridViewColumn newCol = grid.Columns[_oldSortCol.Name];
                        grid.Sort(newCol, _oldSortOrder);
                    }
                }
                else
                {
                    DataGridViewColumn newCol = grid.Columns[_oldSortCol.Name];
                    if(newCol!=null)
                        grid.Sort(newCol, _oldSortOrder);
                }
            }
        }
        public static void SetToDcumSorting(DataGridView grid, int dcumColumnIndex)
        {
            TSU.Debug.WriteInConsole($"Set To Dcum Sorting");
            if (grid.Columns[dcumColumnIndex] != null)
            {
                DataGridViewColumn newCol = grid.Columns[dcumColumnIndex];
                _colToNotSort = newCol;
                grid.Sort(newCol, ListSortDirection.Ascending);
            }
        }
    }
}
