﻿using System;
using System.Xml.Serialization;

namespace TSU
{
    public enum DsaOptions
    {
        Always, Never, Ask_to, Ask_to_with_pre_checked_dont_show_again
    }

    public class TsuBool
    {
        [XmlAttribute("value")]
        public bool IsTrue { get; set; }

        [XmlIgnore]
        public bool IsFalse
        {
            get => !IsTrue;
            set => IsTrue = !value;
        }

        public TsuBool() => IsTrue = true;

        public TsuBool(bool value) => IsTrue = value;

        public void Set(bool value) => IsTrue = value;
    }
}
