﻿using System;
using System.Xml;
using System.Xml.Serialization;

using T = TSU.Tools;

namespace TSU
{
    public interface IDoubleValue : ICloneable, IEquatable<DoubleValue>//, IXmlSerializable
    {
        double Value { get; set; }
        double Sigma { get; set; }
    }

    [Serializable]
    public class DoubleValue : IDoubleValue
    {
        private const double na = TSU.Preferences.Preferences.NotAvailableValueNa;

        private double value;

        [XmlAttribute]
        public virtual double Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = (double.IsNaN(value) || double.IsInfinity(value)) ? na : value;
            }
        }

        private double sigma;

        [XmlAttribute]
        public virtual double Sigma
        {
            get
            {
                return this.sigma;
            }
            set
            {
                this.sigma = (double.IsNaN(value) || double.IsInfinity(value)) ? na : value;
            }
        }

        public DoubleValue()
        {
            Value = na;
            Sigma = na;
        }

        public DoubleValue(double value)
        {
            Value = value;
            Sigma = na;
        }

        public DoubleValue(DoubleValue value)
        {
            Value = value.Value;
            Sigma = value.Sigma;
        }

        public DoubleValue(DoubleValue value, double multiplier)
        {
            if (value.Value != na)
                Value = value.Value * multiplier;
            else
                Value = na;

            if (value.Sigma != na)
                Sigma = value.Sigma * multiplier;
            else
                Sigma = na;
        }

        public DoubleValue(double value, double sigma)
        {
            Value = value;
            Sigma = sigma;
        }

        [XmlIgnore]
        public bool IsNa
        {
            get
            {
                return value == TSU.Tsunami2.Preferences.Values.na;
            }
        }

        [XmlIgnore]
        public static double Na = -999.9;


        #region string manipulation


        public override string ToString()
        {
            return GetValueWithSigmaInParanthesis(this.Value, this.Sigma);
        }

        /// <summary>
        /// use "m-m" to show a value in meter printed in meter with prefered number of visible decimals
        /// or "mm-mm" or "gon-gon" or "gon-cc" "sigma_cc" "sigma_mm" to used prefered number of visible decimals
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public string ToString(string param, bool colored = false, double tolinMM = 0.2, bool showSigma = false, int totalStringLength = 0)
        {
            int a = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;
            int d = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
            if (param == "m-m") return this.ToString(d, factor: 1, colored, tolinMM, showSigma, totalStringLength);
            if (param == "m-mm") return this.ToString((d - 3) < 0 ? 0 : d - 3, factor: 1000, colored, tolinMM, showSigma, totalStringLength);
            if (param == "gon-gon") return this.ToString(a, factor: 1, colored, tolinMM, showSigma, totalStringLength);
            if (param == "gon-cc") return this.ToString((a - 4) < 0 ? 0 : a - 4, factor: 10000, colored, tolinMM, showSigma, totalStringLength);
            if (param == "gon-mrad") return this.ToString((a - 3) < 0 ? 0 : a - 3, factor: 1000 / 200 * Math.PI, colored, tolinMM, showSigma, totalStringLength);
            if (param == "gon-rad") return this.ToString((a - 3) < 0 ? 0 : a - 3, factor: 1 / 200 * Math.PI, colored, tolinMM, showSigma, totalStringLength);
            if (param == "mrad-mrad") return this.ToString((a - 1) < 0 ? 0 : a - 1, factor: 1, colored, tolinMM, showSigma, totalStringLength);

            return GetValueWithSigmaInParanthesis(this.Value, this.Sigma);
        }

        /// <summary>
        /// Return a string with value and sigma like: 100.00010(1) gon
        /// </summary>
        /// <param name="numberOfDecimals"></param>
        /// <returns></returns>
        public string ToString(int numberOfDecimals, double factor = 1, bool colored = false, double tolinMM = 0.2, bool showSigma = false, int totalStringLength = 0)
        {
            string s = T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(this, numberOfDecimals, factor, showSigma);

            if (colored)
            {
                Colorize(s, tolinMM);
            }

            if (totalStringLength != 0)
                s.PadLeft(totalStringLength, ' ');

            return s;
        }

        private string Colorize(string s, double tolinMM)
        {
            if (Math.Abs(Value) > 2 * tolinMM)
                s += $"$red${s}$end$";
            else
            {
                if (Math.Abs(Value) > tolinMM)
                    s += $"$orange${s}$end$";
                else
                    s += $"$green${s}$end$";
            }
            return s;
        }

        /// <summary>
        /// use format: cc gon m mm
        /// </summary>
        /// <param format="format"></param>
        /// <returns></returns>
        internal string GetSigma(string format)
        {
            int a = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;
            int d = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;

            if (this.sigma == na)
                return "n/a";

            switch (format)
            {
                case "gon":
                    return T.Conversions.StringManipulation.WithGivenNumberOfDecimal(this.sigma, a);
                case "cc":
                    return T.Conversions.StringManipulation.WithGivenNumberOfDecimal(this.sigma * 10000, a - 4);
                case "m":
                    return T.Conversions.StringManipulation.WithGivenNumberOfDecimal(this.sigma, d);
                case "mm":
                    return T.Conversions.StringManipulation.WithGivenNumberOfDecimal(this.sigma * 1000, d - 3);
                default:
                    return "N/A";
            }

        }
        public static string GetValueWithSigmaInParanthesis(double inputValue, double inputSigma)
        {
            // will return sigma with a single significative number in parenthis and will truncate tht value to the same digit
            // ie 42.424242 with sigma 0.042 will become 42.42(4)

            double value = inputValue;
            double sigma = inputSigma;
            int numberOfDecimal = 7;
            double factor = 1;

            if (sigma == 0 || sigma == Tsunami2.Preferences.Values.na)
            {
                factor = Math.Pow(10, numberOfDecimal);
                value = Math.Round(value * factor) / factor;
                return value.ToString();
            }
            else
            {
                if (sigma > 1)
                {
                    sigma = Math.Truncate(sigma);
                    numberOfDecimal = 0;
                }
                else
                {
                    int count = 0;
                    double temp = sigma;
                    while (temp < 1)
                    {
                        temp *= 10;
                        count++;
                    }
                    numberOfDecimal = count;
                    factor = Math.Pow(10, numberOfDecimal);
                    sigma = Math.Round(sigma * factor);
                }
                value = Math.Round(value * factor) / factor;
                return $"{value.ToString($"F{numberOfDecimal}")}({sigma})";
            }
        }



        public void ValueFromString(string str)
        {
            if (str == "")
            {
                Value = na;
            }
            else
            {
                //Value = Convert.ToDouble(str);  if this one use and french culture choosen then . and , are exchanged
                Value = T.Conversions.Numbers.ToDouble(str);
            }

        }
        public void SigmaFromString(string str, int divisor = 1)
        {
            if (str == "")
            {
                Sigma = na;
            }
            else
            {
                Sigma = T.Conversions.Numbers.ToDouble(str) / divisor;
            }
        }

        #endregion

        // implementation of ICloneable
        public object Clone()
        {
            return this.MemberwiseClone(); // make a clone of the object, work sif there is no other tye than primitive tyes.

        }

        // implementation of IEquatable<DoubleValue>
        public bool Equals(DoubleValue other)
        {
            return (this.GetHashCode() == other.GetHashCode());
        }
        public override bool Equals(object obj)
        {
            if (obj is DoubleValue && !ReferenceEquals(obj, null))
                return this.Equals(obj as DoubleValue);
            else
                return false;
        }
        public static bool operator ==(DoubleValue v1, object v2)
        {
            if (ReferenceEquals(v1, null) && ReferenceEquals(v2, null)) return true;
            if (!ReferenceEquals(v1, null) && !ReferenceEquals(v2, null))
                return v1.Equals(v2);
            else
                return false;
        }
        public static bool operator !=(DoubleValue v1, object v2)
        {
            if (!ReferenceEquals(v1, null))
                return !(v1 == v2);
            else
                return false;
        }

        public static DoubleValue operator -(DoubleValue v1, DoubleValue v2)
        {
            if (ReferenceEquals(v1, null)) return null;
            if (ReferenceEquals(v2, null)) return null;
            DoubleValue r = new DoubleValue();
            if (v1.IsNa || v2.IsNa)
                r.Value = na;
            else
                r.Value = v1.Value - v2.Value;
            r.Sigma = na;
            return r;
        }

        public override int GetHashCode()
        {
            return this.Value.GetHashCode() ^ this.Sigma.GetHashCode();
        }

        public static DoubleValue operator +(DoubleValue b, DoubleValue c)
        {
            DoubleValue a = new DoubleValue();
            if (b.Value == na || c.Value == na)
                a.Value = na;
            else
                a.Value = b.Value + c.Value;

            if (b.Sigma == na || c.Sigma == na)
                a.Sigma = na;
            else
                a.Sigma = Math.Sqrt(Math.Pow(b.Sigma, 2) + Math.Pow(c.Sigma, 2));

            return a;
        }

        public static DoubleValue operator /(DoubleValue b, double c)
        {
            DoubleValue a = new DoubleValue();
            if (b.Value == na || c == na)
                a.Value = na;
            else
                a.Value = b.Value / c;

            if (b.Sigma == na)
                a.Sigma = na;
            else
                a.Sigma = b.sigma;

            return a;
        }

        // implementation of IXmlSerializable
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }
        public void ReadXml(XmlReader reader)
        {
            Value = double.Parse(reader["Value"]);
            Sigma = double.Parse(reader["Sigma"]);
        }
        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("Value", value.ToString());
            writer.WriteAttributeString("Sigma", sigma.ToString());
        }

        internal DoubleValue Round(int prec)
        {
            DoubleValue a = new DoubleValue();
            a.Value = Math.Round(this.value, prec);
            a.Sigma = Math.Round(this.value, prec);

            return a;
        }

        internal string ToStringInJcgm200Way(int numberOfDecimals)
        {
            return T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(this, numberOfDecimals);
        }
    }

    [Serializable]
    public class DoubleString
    {
        public string Main { get; set; }
        public string Details { get; set; }
    }

    internal static class DoubleValueExtension
    {
        public static bool IsNa(this double value)
        {
            return value == DoubleValue.Na || double.IsNaN(value);
        }
    }
}
