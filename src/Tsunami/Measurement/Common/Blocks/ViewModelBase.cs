﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace TSU.Common.Blocks
{
    public abstract class ViewModelBase : INotifyPropertyChanged, IDisposable
    {
        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
            {
                return false;
            }

            storage = value;

            OnPropertyChanged(propertyName);
            return true;
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region IDisposable with Dispose pattern

        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects)
                }

                // free unmanaged resources (unmanaged objects) and override finalizer
                // set large fields to null
                disposedValue = true;
            }
        }

        ~ViewModelBase()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion

        public virtual void Suspend()
        {
            PropertyChanged = null;
        }

        /// <summary>
        /// Updates a list of ViewModels without resetting the new list, assumming they have the same order
        /// </summary>
        /// <typeparam name="TViewModel">Type of ViewModel used in the list</typeparam>
        /// <typeparam name="TValue">Type of value element</typeparam>
        /// <param name="listToUpdate">Initial list</param>
        /// <param name="newValues">List of new vlaues to apply</param>
        /// <param name="ElementInitializer">Method to call when a new ViewModel must be created</param>
        /// <param name="ElementUpdater">Method to class to update an existing ViewModel</param>
        /// 
        public static void UpdateViewModelList<TViewModel, TValue>(ObservableCollection<TViewModel> listToUpdate,
                                                                   List<TValue> newValues,
                                                                   Func<TValue, TViewModel> ElementInitializer,
                                                                   Action<TViewModel, TValue> ElementUpdater)
            where TViewModel : ViewModelBase
        {
            // If the taget list is longer than the computed one, remove the last elements
            // (should only happen if values were removed since previous update)
            if (listToUpdate.Count > newValues.Count)
                //ObservableCollection doesn't have RemoveRange
                for(int i = listToUpdate.Count - 1; i >= newValues.Count; i--)
                    listToUpdate.RemoveAt(i);
            // Update the existing elements
            for (int i = 0; i < newValues.Count && i < listToUpdate.Count; i++)
                ElementUpdater(listToUpdate[i], newValues[i]);

            // Add the new elements
            // (after init, or if values were added since previous update)
            for (int i = listToUpdate.Count; i < newValues.Count; i++)
                listToUpdate.Add(ElementInitializer(newValues[i]));
        }

        /// <summary>
        /// Updates a list of ViewModels without resetting the new list, with a match criteria
        /// </summary>
        /// <typeparam name="TViewModel">Type of ViewModel used in the list</typeparam>
        /// <typeparam name="TValue">Type of value element</typeparam>
        /// <param name="listToUpdate">Initial list</param>
        /// <param name="newValues">List of new vlaues to apply</param>
        /// <param name="ElementInitializer">Method to call when a new ViewModel must be created</param>
        /// <param name="ElementUpdater">Method to class to update an existing ViewModel</param>
        public static void UpdateViewModelList<TViewModel, TValue>(ObservableCollection<TViewModel> listToUpdate,
                                                                   List<TValue> newValues,
                                                                   Func<TValue, TViewModel> ElementInitializer,
                                                                   Action<TViewModel, TValue> ElementUpdater,
                                                                   Func<TValue, TViewModel, bool> Match)
        {
            // Remove the elements in listToUpdate with no match
            Stack<int> toRemove = new Stack<int>();
            for (int i = 0; i < listToUpdate.Count; i++)
            {
                TViewModel vm = listToUpdate[i];
                if (!newValues.Exists(v => Match(v, vm)))
                {
                    Debug.WriteInConsole($"ViewModel {vm} at index {i} will be removed");
                    toRemove.Push(i);
                }
            }
            foreach (int i in toRemove)
                listToUpdate.RemoveAt(i);

            // Update the existing elements and add the new ones
            foreach (TValue v in newValues)
            {
                //ObservableCollection dosen't have FindIndex
                int index = -1;
                for (int i = 0; i < listToUpdate.Count; i++)
                {
                    if (Match(v, listToUpdate[i]))
                    {
                        index = i;
                        break;
                    }
                }
                if (index == -1)
                {
                    TViewModel newVM = ElementInitializer(v);
                    listToUpdate.Add(newVM);
                    Debug.WriteInConsole($"Created a new ViewModel {newVM} based on {v} at index {listToUpdate.Count - 1}");
                }
                else
                {
                    TViewModel existingVM = listToUpdate[index];
                    string vmBefore = existingVM.ToString();
                    ElementUpdater(existingVM, v);
                    string vmAfter = existingVM.ToString();
                    Debug.WriteInConsole($"Existing ViewModel at index {listToUpdate.Count} was updated based on {v} from {vmBefore} to {vmAfter}.");
                }
            }
        }
    }
}
