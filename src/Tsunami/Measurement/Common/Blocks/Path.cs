﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Forms;
using R = TSU.Properties.Resources;
using TSU.Views;

namespace TSU
{
    public static class TsuPath
    {
        // will return choose path in the view or "" if cancel
        public static string GetFileNameToOpen(TsuView parentView, string initialDirectory, string defautlFileName, string filter, string title)
        {
            title = (title == "") ? R.T_OpenFileLocation : title;
            OpenFileDialog d = new OpenFileDialog();
            d.InitialDirectory = initialDirectory;
            d.Filter = filter;
            d.FilterIndex = 1;
            d.RestoreDirectory = true;
            d.Title = title;
            d.FileName = defautlFileName;

            TSU.Common.TsunamiView.ViewsShownAsModal.Add(d);
            var res = d.ShowDialog();
            TSU.Common.TsunamiView.ViewsShownAsModal.Remove(d);

            if (res == DialogResult.OK) // i removed ShowDialog(parentview) because it can hide the modal and never get it back
                return d.FileName;
            else
                return "";
        }

        // will return choose path in the view or "" if cancel
        public static string GetFileNameToSave(string initialDirectory, string defautlFileName, string filter, string title)
        {
            title = (title == "") ? R.T_SELECT_THE_PLACE_WHERE_TO_EXPORT_YOUR_FILE : title;
            SaveFileDialog d = new SaveFileDialog();
            d.InitialDirectory = initialDirectory;
            d.Filter = filter;
            d.FilterIndex = 1;
            d.RestoreDirectory = true;
            d.Title = title;
            d.FileName = defautlFileName;

            TSU.Common.TsunamiView.ViewsShownAsModal.Add(d);
            var res = d.ShowDialog();
            TSU.Common.TsunamiView.ViewsShownAsModal.Remove(d);

            if (res == DialogResult.OK) return d.FileName; else return ""; // i removed ShowDialog(parentview) because it can hide the modal and never get it back
        }
    }
}
