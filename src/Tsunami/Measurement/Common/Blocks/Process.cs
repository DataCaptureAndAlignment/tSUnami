﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSU
{
    public class TsuProcess : TSU.TsuObject
    {
        public TSU.Common.Dependencies.Application Application { get; set; }
        public System.Diagnostics.Process Process { get; set; }
        public string Input { get; set; }

        public DateTime startingTime { get; set; }

        public TsuProcess()
        {
            startingTime = DateTime.Now;
        }

        public void Kill()
        {
            if (Process == null) return;
            if (Process.HasExited) return;
            Process.Kill();
        }

        public override string ToString()
        {
            return $"{Application._Name} {Input}";
        }
    }
}
