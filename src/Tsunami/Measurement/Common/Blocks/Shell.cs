﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using TSU.Views;
using System.Diagnostics;
using System.Windows.Forms;
using System.Threading.Tasks;
using D = TSU.Common.Dependencies;
using System.ComponentModel;
using MathNet.Numerics.Optimization.TrustRegion;
using TSU.Common;
using System.IO;
using TSU.Views.Message;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace TSU
{
    public static class Shell
    {
        internal static void RunInDialogView(string pathToExecute, string appName="", string cancelButtonText="", object module=null, bool needElevation=false)
        {
            if (appName == "") appName = pathToExecute;
            if (cancelButtonText == "") cancelButtonText =R.T_CANCEL;

            D.MessageView dialog = new D.MessageView(pathToExecute, string.Format(R.T_ShellRuns, appName, pathToExecute));
            dialog.AdjustToScreenSize(true);
            dialog.Shown += delegate(object sender, EventArgs e)
            {
                dialog.StartProcess(null, needElevation);
            };

            TSU.Common.TsunamiView.ViewsShownAsModal.Add(dialog);
            dialog.ShowDialog();
            TSU.Common.TsunamiView.ViewsShownAsModal.Remove(dialog);

            dialog.ShowDialog();
        }

        internal static void RunInDialogView(string appPath, string arguments, string appName = "", string cancelButtonText = "", object module = null, bool needElevation = false)
        {
            if (appName == "") appName = arguments;
            if (cancelButtonText == "") cancelButtonText = R.T_CANCEL;


            D.MessageView dialog = new D.MessageView ("commandLine", string.Format( R.T_ShellRuns, appName, arguments));
            dialog.AdjustToScreenSize(true);
            dialog.Shown += delegate (object sender, EventArgs e)
            {
                dialog.StartProcess(appPath.Replace(@"\\", @"\"), arguments.Replace(@"\\", @"\"));
            };

            TSU.Common.TsunamiView.ViewsShownAsModal.Add(dialog);
            dialog.ShowDialog();
            TSU.Common.TsunamiView.ViewsShownAsModal.Remove(dialog);
        }

        internal static void ExecutePathInDialogView(string pathToExecute, string appName = "",
            string cancelButtonText = "", Module module = null, bool needElevation = false, MessageType type = MessageType.FYI, bool launchSurveyPadPlugin = false, bool waitProcessEnd = true)
        {
            // to avoid view in UNIT TESTS:
            if (Tsunami2.View == null)
                return;
            bool dontUseTsunamiView = true;

            if (dontUseTsunamiView)
            {
                System.Diagnostics.Process process = new Process();
                TSU.Preferences.Preferences.Dependencies.GetByBame("SURVEYPAD", out Common.Dependencies.Application app);

                string pathToUse = pathToExecute;
                string argR = "";
                if (launchSurveyPadPlugin && (pathToExecute.ToUpper().EndsWith(".INP") || pathToExecute.ToUpper().EndsWith(".CHABA")))
                    argR = "-r";

                // in the past we were only opening the res in a notepad but now we want to see it in surveypad with the Gramma active so,
                // we need to launch the input .inp instead of the .res and the res will show up with gramma
                if (launchSurveyPadPlugin && pathToUse.ToUpper().EndsWith(".RES"))
                {
                    argR = "";
                    string extension = new FileInfo(pathToUse).Extension;
                    pathToUse = pathToUse.Replace(extension, ".inp");
                }

                string arguments = $"\"{pathToUse}\" {argR}";
                process.StartInfo = new System.Diagnostics.ProcessStartInfo(app.Path, arguments) { UseShellExecute = true };
                 process.Start();
                if (waitProcessEnd)
                {
                    int waitTimeInmillis = TSU.Debug.IsRunningInATest ? 1000 : 5 * 60 * 1000;
                    if (module == null)
                        module = Tsunami2.View.Module;
                    TsuView view = module.View;
                    view.ShowProgress(view, ()=> process.WaitForExit(waitTimeInmillis), 2, $"Waiting for something;Waiting for {pathToExecute} to be closed") ;
                }
            }
            else
            {
                if (appName == "") appName = pathToExecute;
                if (cancelButtonText == "") cancelButtonText = R.T_CANCEL;

                D.MessageView dialog = new D.MessageView(pathToExecute, $"You are editing '{pathToExecute}', Tsunami wait for the changes to read them, please SAVE the file when modifications are done", type);
                dialog.AdjustToScreenSize(true);

                dialog.Load += delegate (object sender, EventArgs e) { dialog.StartPadProcess(null, needElevation); };
                //dialog.Shown += delegate (object sender, EventArgs e) { dialog.StartNotePadProcess(null, needElevation); };

                TSU.Common.TsunamiView.ViewsShownAsModal.Add(dialog);
                dialog.ShowDialog();
                TSU.Common.TsunamiView.ViewsShownAsModal.Remove(dialog);
            }
        }

        internal static void RunWithWaitingMessage(TsuView senderView, List<string> pathsToExecute, string appName = "", string cancelButtonText = "")
        {
            // Checks
            if (pathsToExecute == null) return;
            if (pathsToExecute.Count == 0) return;

            if (appName == "")
            {
                if (pathsToExecute.Count > 1)
                    appName = "Several applications";
                else
                    appName = pathsToExecute[0];
            }
            if (cancelButtonText == "") cancelButtonText =R.T_CANCEL;


            Views.Message.ProgressMessage c = senderView.WaitingForm;

            c = new Views.Message.ProgressMessage(senderView, R.T_TSUNAMI_IS_RUNNING_THINGS_FOR_YOU, R.T_PROGRESS, pathsToExecute.Count, 1000, true,null, cancelButtonText);

            foreach (var item in pathsToExecute)
	        {
                // Check if cancelled
                if (c.IsCancelled) break;

                //bool fini = false;
                //bool interupt = false;


                c.BeginAstep($"'{ item}' {R.T_ISARE_RUNNING};{R.T_TSUNAMI_IS_WAITING_FOR} '{ item}' {R.T_TO_FINISH_BEFORE_IT_CAN_CONTINUE}");
                Process p = Run(item, true);

                if (p != null)
                    p.Exited += delegate {  c.EndCurrentStep(); };
                else
                    c.EndCurrentStep();
                
	        }
            c.EndAll();
        }

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        public static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);


        /// <summary>
        /// Show the process inside a panel, but you have to close the process yourself (you can do it by making the panel.visible to false.
        /// </summary>
        /// <param name="p"></param>
        /// <param name="pathToExecute"></param>
        /// <param name="theProcessRestartAutomatically"></param>
        /// <param name="executable"></param>
        public static Process RunInPanel(Panel p, string pathToExecute, bool theProcessRestartAutomatically = false, string executable="")
        {
            try
            {
                System.Diagnostics.ProcessStartInfo startInfo;
                if (executable=="")
                    startInfo = new System.Diagnostics.ProcessStartInfo() { UseShellExecute = true, FileName = pathToExecute, };
                else
                    startInfo = new System.Diagnostics.ProcessStartInfo(executable, pathToExecute);

                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
                //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                //startInfo.Verb = "runas";
                System.Diagnostics.Process process = System.Diagnostics.Process.Start(startInfo);

                process.EnableRaisingEvents = true;
                //process.WaitForInputIdle();
                

                //if (theProcessRestartAutomatically)
                    System.Threading.Thread.Sleep(100); // Allow the process to open it's window

                // handle process
                IntPtr motherHandle = p.Handle;
                IntPtr childHandle = new IntPtr();


                if (theProcessRestartAutomatically) // get the process anme wait for need thread to start and get its hand
                {
                    string name = process.ProcessName;

                    System.Diagnostics.Process[] processes = System.Diagnostics.Process.GetProcessesByName(name);
                    if (processes.Length >= 1)
                    {
                        process.Refresh();
                        childHandle = processes[0].MainWindowHandle;
                    }
                    else
                        foreach (System.Diagnostics.Process pross in processes)
                        {
                            childHandle = pross.MainWindowHandle;
                        }
                }
                else
                {
                    childHandle = process.MainWindowHandle;
                }

                //EmbedProcess(process, motherHandle);

                p.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
                p.Visible = true;


                Action SetFullScreen = () =>
                {
                    ShowWindow(process.MainWindowHandle, 6);
                    ShowWindow(process.MainWindowHandle, 3);
                };

                if (childHandle != null)
                {
                    SetParent(childHandle, motherHandle);
                    SetFullScreen();
                    p.Layout += delegate
                    {
                        SetFullScreen();
                    };
                    //p.VisibleChanged += delegate
                    //{
                    //    if (p.Visible == false)
                    //        process.CloseMainWindow();
                    //};
                }
                return process;
            }
            catch (Exception ex)
            {
                string titleAndMessage = "OOPS;"+ex.Message;
                new MessageInput(MessageType.Critical, titleAndMessage).Show();
                return null;
            }
        }


        [DllImport("user32.dll", SetLastError = true)]
        private static extern uint GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern uint SetWindowLong(IntPtr hWnd, int nIndex, uint dwNewLong);

        private const int GWL_STYLE = -16;
        private const uint WS_VISIBLE = 0x10000000;
        private const uint WS_CHILD = 0x40000000;

        /// <summary>
        /// Embeds a process's main window into a specified parent handle.
        /// </summary>
        /// <param name="process">The process to embed.</param>
        /// <param name="parentHandle">The handle of the parent control.</param>
        /// <returns>True if embedding is successful, false otherwise.</returns>
        public static bool EmbedProcess(Process process, IntPtr parentHandle)
        {
            try
            {
                if (process == null || process.HasExited)
                    throw new InvalidOperationException("The process is invalid or has already exited.");

                // Wait for the process to initialize its main window
                //process.WaitForInputIdle();
                System.Threading.Thread.Sleep(100);

                // Get the handle of the process's main window
                IntPtr processHandle = process.MainWindowHandle;
                if (processHandle == IntPtr.Zero)
                    throw new InvalidOperationException("The process does not have a valid main window.");

                // Set the process window as a child of the specified parent handle
                SetParent(processHandle, parentHandle);

                // Modify the process window style to behave as a child
                uint style = GetWindowLong(processHandle, GWL_STYLE);
                SetWindowLong(processHandle, GWL_STYLE, (style | WS_CHILD) & ~WS_VISIBLE);

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to embed process: {ex.Message}");
                return false;
            }
        }

        public static System.Diagnostics.Process Run(string pathToExecute, bool wait = false, string arguments = "")
        {
            try
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo = new System.Diagnostics.ProcessStartInfo() { UseShellExecute = true,  FileName = pathToExecute };
                if (arguments != "")
                    process.StartInfo.Arguments = arguments;
                process.Start();
                process.EnableRaisingEvents = true;

                if (wait) process.WaitForExit();
                return process;
            }
            catch (Exception ex)
            {
                Logs.Log.AddEntryAsError(null, $"Could'nt execute '{pathToExecute}';{ex.Message}");
                return null;
            }
            
        }

        public static void Run(List<string> pathsToExecute, bool wait = false)
        {
            try
            {
                foreach (var item in pathsToExecute)
                {
                    Run(item, wait);
                }
            }
            catch (Exception )
            {

            }

        }

        //public static void RunInNotepad(string pathToExecute, bool wait = false)
        //{
        //    System.Diagnostics.Process process = new System.Diagnostics.Process();
        //    process.StartInfo = new System.Diagnostics.ProcessStartInfo("notepad.exe", pathToExecute);
        //    Tsunami2.Properties.View.ShowProgress(
        //       Tsunami2.Properties.View,
        //        delegate
        //        {
        //            process.Start();
        //            if (wait) process.WaitForExit();
        //        },
        //        1000,
        //        string.Format(R.T_M_WAITING_PROCESS, pathToExecute),
        //        R.T_CANCEL,
        //        delegate
        //        {
        //            process.CloseMainWindow();
        //        }
        //        );
        //    //Tsunami2.Properties.TsunamiView.ShowProgress(
        //    //    null,
        //    //    null);
        //}
    }
}
