﻿using System;

namespace TSU
{
    public class Result : ICloneable
    {
        public bool Success { get; set; }

        public string AccessToken { get; set; }

        public string ErrorMessage { get; set; }

        public Result()
        {
            Success = true;
            ErrorMessage = string.Empty;
            AccessToken = null;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }

        public void MergeString(Result resultToMerge)
        {
            AccessToken = FormatString(AccessToken) + FormatString(resultToMerge.AccessToken);
            ErrorMessage = FormatString(ErrorMessage) + FormatString(resultToMerge.ErrorMessage);
            Success = Success && resultToMerge.Success;
        }

        public override string ToString()
        {
            string info = Success ? $"Success: {AccessToken}" : $"Error; {ErrorMessage}";
            return info;
        }

        /// <summary>
        /// Returns either string.Empty (if input is null or enpty) or a string finishing with \n
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private static string FormatString(string input)
        {
            if (string.IsNullOrEmpty(input))
                return string.Empty;
            else if (input.EndsWith("\n"))
                return input;
            else
                return input + "\r\n";
        }
    }
}
