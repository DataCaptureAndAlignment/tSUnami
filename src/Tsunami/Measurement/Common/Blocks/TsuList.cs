﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace TSU
{
    public interface ICloneable<T> : ICloneable where T : ICloneable<T>
    {
        new T Clone();
    }

    // Creation of a List that can be clonable and have special methods
    [Serializable]
    public class CloneableList<T> : List<T>, ICloneable<CloneableList<T>> where T : ICloneable
    {
        public List<T> GetList()
        {
            List<T> l = new List<T>();
            foreach (var item in this)
            {
                l.Add(item);
            }
            return l;
        }
        public CloneableList<T> Clone()
        {
            var result = new CloneableList<T>();
            result.AddRange(this.Select(item => (T)item.Clone()));
            return result;
        }

        object ICloneable.Clone()
        {
            return ((ICloneable<CloneableList<T>>)this).Clone();
        }

        public override string ToString()
        {
            string s = "";
            bool first = true;
            foreach (var item in this)
            {
                if (!first) s += "\r\n";
                s += item.ToString();
                first = false;
            }
            return s;
        }

        internal void MoveUp(T item)
        {
            int oldIndex = this.LastIndexOf(item);
            if (oldIndex > 0)
            {
                this.RemoveAt(oldIndex);
                this.Insert(oldIndex - 1, item);
            }
        }

        internal void MoveDown(T item)
        {
            int oldIndex = this.LastIndexOf(item);
            if (oldIndex + 1 < this.Count)
            {
                this.RemoveAt(oldIndex);
                this.Insert(oldIndex + 1, item);
            }
        }

        internal CloneableList<T> ReverseCopy()
        {
            CloneableList<T> reversed = new CloneableList<T>();
            for (int i = this.Count - 1; i >= 0; i--)
            {
                reversed.Add(this[i]);
            }
            return reversed;
        }
    }

    [Serializable]
    public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, IXmlSerializable
    {
        #region IXmlSerializable Members

        public SerializableDictionary()
        {

        }
        public SerializableDictionary(SerializationInfo info, StreamingContext context)
        {

        }
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
            XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

            bool wasEmpty = reader.IsEmptyElement;
            reader.Read();

            if (wasEmpty)
                return;

            while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
            {
                reader.ReadStartElement("item");

                reader.ReadStartElement("key");
                TKey key = (TKey)keySerializer.Deserialize(reader);
                reader.ReadEndElement();

                reader.ReadStartElement("value");
                TValue value = (TValue)valueSerializer.Deserialize(reader);
                reader.ReadEndElement();

                this.Add(key, value);

                reader.ReadEndElement();
                reader.MoveToContent();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
            XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

            foreach (TKey key in this.Keys)
            {
                writer.WriteStartElement("item");

                writer.WriteStartElement("key");
                keySerializer.Serialize(writer, key);
                writer.WriteEndElement();

                writer.WriteStartElement("value");
                TValue value = this[key];
                valueSerializer.Serialize(writer, value);
                writer.WriteEndElement();

                writer.WriteEndElement();
            }
        }
        #endregion
    }
}
