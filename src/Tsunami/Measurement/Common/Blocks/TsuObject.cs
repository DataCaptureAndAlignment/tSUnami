﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using TSU.Common.Instruments;
using TSU.Common.Instruments.Reflector;
using TSU.Common.Measures;
using TSU.Logs;
using Operations = TSU.Common.Operations;
using R = TSU.Properties.Resources;
using Zones = TSU.Common.Zones;

namespace TSU
{
    public interface ITsuObject
    {
        string _Name { get; set; }
        Guid Guid { get; set; }
        object Tag { get; set; }
    }

    [XmlInclude(typeof(Measure))]
    [XmlInclude(typeof(Sensor))]
    [XmlInclude(typeof(Element))]
    [XmlInclude(typeof(Instrument))]
    [XmlInclude(typeof(LogEntry))]
    [Serializable]

    public class TsuDouble : TsuObject
    {
        public double Value;
    }

    [XmlInclude(typeof(Zones.Zone))]
    [XmlInclude(typeof(Measure))]
    [XmlInclude(typeof(Sensor))]
    [XmlInclude(typeof(Element))]
    [XmlInclude(typeof(CompositeElement))]
    [XmlInclude(typeof(Instrument))]
    [XmlInclude(typeof(Common.Instruments.Device.Interfaces))]
    [XmlInclude(typeof(LevelingStaff))]
    [XmlInclude(typeof(Reflector))]
    [XmlInclude(typeof(LogEntry))]
    [XmlInclude(typeof(Operations.Manager))]
    [XmlInclude(typeof(Zones.Manager))]
    [XmlInclude(typeof(SocketCode))]
    [XmlInclude(typeof(Common.Instruments.Device.AT40x.Module))]
    [Serializable]

    public abstract class TsuObject : ICloneable, IEquatable<TsuObject>, ITsuObject
    {
        protected string name;

        [XmlAttribute("Name")]
        public virtual string _Name
        {
            get => name;
            set => name = value;
        }

        [XmlAttribute("Id")]
        public virtual string Id { get; set; }

        [XmlAttribute("GUID")]
        public virtual Guid Guid { get; set; }

        [XmlIgnore]
        public object Tag { get; set; }

        protected TsuObject()
        {
            _Name = R.String_Unknown;
            Guid = Guid.NewGuid();
        }

        protected TsuObject(string name)
        {
            _Name = name;
            Guid = Guid.NewGuid();
        }

        internal virtual IEnumerable<System.Windows.Forms.Control> GetOptions()
        {
            yield break;
        }

        // IEquatable
        public virtual bool Equals(TsuObject other)
        {
            return _Name == other._Name && Guid == other.Guid;
        }

        public override bool Equals(object obj)
        {
            // other could be a reference type, the is operator will return false if null
            if (obj is TsuObject to)
                return Equals(to);
            else
                return false;
        }

        public override int GetHashCode()
        {
            return Guid.GetHashCode();
        }

        // ICloneable
        public virtual object Clone()
        {
            TsuObject newTsuOject = (TsuObject)MemberwiseClone();
            //Keep the GUID Globally Unique
            newTsuOject.Guid = Guid.NewGuid();
            //We let the subclasses the choice to Clone the Tag or not, as we don't know if it's a good idea.
            return newTsuOject;
        }
    }
}
