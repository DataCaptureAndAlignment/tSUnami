﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using static TSU.Common.Elements.Manager.Module;

namespace TSU
{
    [Serializable]
    public class DsaFlag
    {
        [XmlIgnore]
        public Module stm;

        [XmlAttribute]
        public DsaOptions State;

        [XmlAttribute]
        public string Name;

        public DsaFlag()
        {
            // for xml serialisation
        }

        public DsaFlag(string name, DsaOptions state)
        {
            this.Name = name;
            this.State = state;
        }

        [XmlIgnore]
        public bool IsTrue
        {
            get
            {
                return this.State != DsaOptions.Never;
            }
        }

        public bool IsFalse
        {
            get
            {
                return this.State != DsaOptions.Always;
            }
        }

        public override string ToString()
        {
            string s = this.State.ToString();
            foreach (char c in this.Name)
            {
                if (char.IsUpper(c))
                    s += " ";
                s += c;
            }
            return s;
        }
        public static bool IsSetToAlways(List<DsaFlag> flags, string name)
        {
            return GetByNameOrAdd(flags, name).State == DsaOptions.Always;
        }


        internal static bool IsAskToByName(List<DsaFlag> flags, string name)
        {
            DsaFlag dsa = GetByNameOrAdd(flags, name);
            return dsa.State == DsaOptions.Ask_to || dsa.State == DsaOptions.Ask_to_with_pre_checked_dont_show_again;
        }

        public static bool IsSetToNever(List<DsaFlag> flags, string name)
        {
            return GetByNameOrAdd(flags, name).State == DsaOptions.Never;
        }

        internal static DsaFlag GetByNameOrAdd(List<DsaFlag> flags, string name)
        {
            if (flags == null) // we are running from a unit test
            {
                return new DsaFlag(name, DsaOptions.Never);
            }
            
            foreach (DsaFlag flag in flags)
                    if (flag.Name == name)
                        return flag;
            // if not existing we create it 
            DsaFlag newFlag = new DsaFlag(name, DsaOptions.Ask_to);
            flags?.Add(newFlag);
            return newFlag;
        }

    }
}
