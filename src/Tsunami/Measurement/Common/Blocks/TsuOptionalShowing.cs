﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSU
{
    public class TsuOptionalShowing
    {
        [Flags]
        public enum Type
        {
            None = 0,
            DontShowDontShowAgainBox = 1 << 0,
            ShowBoxChecked = 1 << 1,
            ShowBoxUnchecked = 1 << 2,
            AlwaysDo = 1 << 3,
            NeverDo = 1 << 4,
        }

        public static bool IsBoxNeeded(Type type)
        {
            Type boxIsNeeded = Type.ShowBoxChecked | Type.ShowBoxUnchecked;
            return ((type & boxIsNeeded) == boxIsNeeded);
        }

        public static bool IsShowingNeeded(Type type)
        {
            Type showingIsNeeded = Type.DontShowDontShowAgainBox | Type.ShowBoxChecked | Type.ShowBoxUnchecked;
            bool needed = showingIsNeeded.HasFlag(type);
            return needed;
        }

        public static TsuBool GetDontShowAgainTsuBool(Type type)
        {
            switch (type)
            {
                case Type.ShowBoxChecked:
                    return new TsuBool(true);
                case Type.ShowBoxUnchecked:
                    return new TsuBool(false);
                case Type.AlwaysDo:
                case Type.NeverDo:
                case Type.DontShowDontShowAgainBox:
                default:
                    return null;
            }
        }

        internal static void Update(ref Type showingOption, TsuBool dontshowagain, bool alwaysDo)
        {
            if (dontshowagain == null) return; // ...without update

            if (dontshowagain.IsFalse) return; // ...without update

            if (alwaysDo)
                showingOption = Type.AlwaysDo;
            else
                showingOption = Type.NeverDo;
        }
    }
}
