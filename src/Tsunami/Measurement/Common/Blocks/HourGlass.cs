﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TSU
{
    public static class HourGlass
    {
        static int count = 0;

        public static void Set()
        {
            Cursor.Current = Cursors.WaitCursor;
            System.Windows.Forms.Application.UseWaitCursor = true;
            count++;
            // TSU.Debug.WriteInConsole($"count {count} +++++++++++++++++++++++++++++++++++++++++++++++");
        }

        public static bool IsSet
        {
            get
            {
                return count > 0;
            }
        }

        public static void Remove()
        {
            count--;
            TSU.Debug.WriteInConsole($"count {count} -----------------------------------------------");
            if (count < 1)
            {
                Cursor.Current = Cursors.Default;
                System.Windows.Forms.Application.UseWaitCursor = false;
            }
        }
    }
}
