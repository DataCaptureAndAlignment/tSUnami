﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using E = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using O = TSU.Common.Operations;
using V = TSU.Common.Strategies.Views;
using Z = TSU.Common.Zones;

namespace TSU
{
    [Serializable]
    [XmlInclude(typeof(E.Manager.Module))]
    [XmlInclude(typeof(M.Manager))]
    [XmlInclude(typeof(I.Manager.Module))]
    [XmlInclude(typeof(O.Manager))]
    [XmlInclude(typeof(Z.Manager))]
    [XmlInclude(typeof(TSU.Common.Instruments.Device.AT40x.Module))]
    [XmlType(Namespace = "TSU", TypeName = "Manager")]
    public class Manager : Module
    {

        #region Properties and fields
        /// <summary>
        /// You can specify which object is selected if one. It is used at least for the view, the PreSelectedObjects will be checked but the SelectedObject will be selected in blue.
        /// </summary>
        [XmlIgnore]
        public TsuObject _SelectedObjectInBlue
        {
            get { return this.selectedObjectInBlue; }
            set
            {
               
                this.selectedObjectInBlue = value; // was 'if (value != null) this.selectedObjectInBlue = value;' how can I remove the selection if null not accepted
            }

        }
        private TsuObject selectedObjectInBlue;

        private List<TsuObject> selectedObjects = new List<TsuObject>();

        /// <summary>
        /// Will set the list object that are checked (selected) in the manager, if will also set the first object of list as the selected (in blue) one.
        /// </summary>
        [XmlIgnore]
        public List<TsuObject> _SelectedObjects
        {
            get { return this.selectedObjects; }
            set
            {
                if (value != null && value.Count > 0)
                    this._SelectedObjectInBlue = value[0];
                if (value != null)
                    this.selectedObjects = value;
            }
        }
        private List<TsuObject> elements;

        public List<TsuObject> AllElements
        {
            get { return this.elements; }
            set { this.elements = value; }
        }

        /// <summary>
        /// Those are the objects that will be shown
        /// </summary>
        /// 
        [XmlIgnore]
        public List<TsuObject> SelectableObjects { get; set; } = new List<TsuObject>();
        // PreSelectedObjects property, has been removed. manager show automaticaly checked or select the list of object in SelectedObject

        public override string ToString()
        {
            return $"Managing:  {this.AllElements.Count} objects ({this._SelectedObjects.Count} selected)";
        }

        [XmlIgnore]
        public new TSU.Views.ManagerView View
        {
            get { return this._TsuView as TSU.Views.ManagerView; }
            set { this._TsuView = value; }
        }


        public bool isMainManager { get; set; }

        public bool MultiSelection = true;
        internal bool AutoCheckChildren;


        public List<V.Types> AvailableViewStrategies;
        internal bool EditByClickingSubItem;

        #endregion

        #region Construction


        public Manager()
            : base()
        {

        }
        public Manager(Module parentModule, string nameAndDescription)
            : base(parentModule, nameAndDescription)
        {

        }

        public override void Dispose()
        {
            this.ParentModule = null;
            base.Dispose();
        }



        public override void Initialize()
        {
            // Instanciation
            this._SelectedObjects = new List<TsuObject>();
            this.SelectableObjects = new List<TsuObject>();
            if (this.ParentModule is Manager) // ugly way to catch if we deal with the point editor
                CreateUniqueView = true;
            base.Initialize();
        }

        internal virtual void AddElementFromText(string source, string text)
        {

        }

        internal override void ReCreateWhatIsNotSerialized(bool saveSomeMemory = false)
        {
            try
            {
                this._SelectedObjects = new List<TsuObject>();
                this.SelectableObjects = new List<TsuObject>();
                base.ReCreateWhatIsNotSerialized(saveSomeMemory);
            }
            catch (Exception ex)
            {
                throw new Exception($"Cannot re-create what was not serialized in {this._Name}", ex);
            }
        }

        #endregion

        #region Events

        public class ManagerEventArgs : EventArgs
        {
            public Manager Manager;
            public List<TsuObject> SelectedObjects;
            public List<TsuObject> JustAddedObjects;
            public List<TsuObject> JustRemovedObjects;
            public object Tag;
            public ManagerEventArgs()
            {
            }

            public ManagerEventArgs(Manager manager)
            {
                this.Manager = manager;
                this.SelectedObjects = manager._SelectedObjects;
            }

        }

        public event EventHandler<ManagerEventArgs> SelectionChanged;
        public event EventHandler<ManagerEventArgs> NodeClicked;

        public virtual void NodeClick(System.Windows.Forms.TreeNodeMouseClickEventArgs e)
        {
            if (NodeClicked != null)
                NodeClicked(
                    this,
                    new ManagerEventArgs()
                    {
                        Manager = this,
                        Tag = e
                    });

        }

        #endregion

        public override void UpdateView()
        {

            if (this.View != null)
                this.View.Module = this;
            base.UpdateView();
        }
        /// <summary>
        /// Will tell if the object is ok to be selected
        /// </summary>
        /// <param name="tsuObject"></param>
        internal virtual bool IsItemSelectable(TsuObject tsuObject)
        {
            return true;
        }

        /// <summary>
        /// Build the selectionnable list (put all object if parameter is null) use this to give a list of ref to element that you want to see appeared in the list view, you can also set the property
        /// </summary>
        /// <param name="selectableItemList"></param>
        internal virtual void SetSelectableList(List<TsuObject> selectableItemList)
        {
            this.SelectableObjects.Clear();
            if (selectableItemList != null)
                this.SelectableObjects.AddRange(selectableItemList);
            else
                this.SelectableObjects.AddRange(this.AllElements);
        }
        /// <summary>
        /// this method will load the manager with all the coreeponding element from all the module present in his parent module
        /// </summary>
        public virtual void ShowAllElementIncludedInParentModule()
        {

        }
        /// <summary>
        /// add a new object to the list of selected object
        /// </summary>
        /// <param name="item"></param>
        public virtual void AddSelectedObjects(TsuObject newitem)
        {
            if (newitem != null)
            {
                this._SelectedObjects.Add(newitem);
            }
            if (SelectionChanged != null)
                SelectionChanged(
                    this,
                    new ManagerEventArgs(this)
                    {
                        SelectedObjects = this._SelectedObjects,
                        JustAddedObjects = new List<TsuObject>() { newitem }
                    });

        }

        /// <summary>
        /// remove from the list of selected object
        /// </summary>
        /// <param name="item"></param>
        public virtual void RemoveSelectedObjects(TsuObject newitem)
        {
            if (newitem != null)
            {
                this._SelectedObjects.Remove(newitem);
            }
            if (SelectionChanged != null)
                SelectionChanged(
                    this,
                    new ManagerEventArgs(this)
                    {
                        SelectedObjects = this._SelectedObjects,
                        JustRemovedObjects = new List<TsuObject>() { newitem }
                    });

        }


        /// <summary>
        /// add a new list of objects to the list of selected object
        /// </summary>
        /// <param name="item"></param>
        public virtual void AddSelectedObjects(List<TsuObject> newListItem)
        {
            foreach (TsuObject item in newListItem)
            {
                if (item != null)
                {
                    this._SelectedObjects.Add(item);
                }
            }
            if (SelectionChanged != null)
                SelectionChanged(
                    this,
                    new ManagerEventArgs(this)
                    {
                        SelectedObjects = this._SelectedObjects,
                        JustAddedObjects = newListItem
                    });
        }


        /// <summary>
        /// Enlève l'object de la la liste des sélectionnable et de la liste des sélectionnés
        /// </summary>
        /// <param name="itemToRemove"></param>
        public virtual void RemoveSelectableObjects(TsuObject itemToRemove)
        {
            if (itemToRemove != null)
            {
                if (this.SelectableObjects.Contains(itemToRemove))
                {
                    this.SelectableObjects.Remove(itemToRemove);
                    if (this.selectedObjects.Contains(itemToRemove)) this.selectedObjects.Remove(itemToRemove);
                }
            }
            if (SelectionChanged != null)
                SelectionChanged(
                    this,
                    new ManagerEventArgs(this)
                    {
                        SelectedObjects = this._SelectedObjects,
                        JustRemovedObjects = new List<TsuObject>() { itemToRemove }
                    });
        }

        public virtual void EditItem(object tag1, object tag2)
        {
            // to be override in managers
        }
    }



}
