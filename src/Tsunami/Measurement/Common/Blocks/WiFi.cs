﻿using ManagedNativeWifi;
using Microsoft.Win32;
using System;
using System.Activities;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using TSU.Views.Message;
using P = TSU.Views.Message.ProgressMessage;
using R = TSU.Properties.Resources;

namespace TSU
{
    public static class WiFi
    {
        public static void TryToConnectAutomaticallyWithWifi(Common.Instruments.Module.ConnectionDelegate connectionMethod, string address = "192.168.0.100", int port = 700, string SN = "", BackgroundWorker worker = null)
        {
            int attempt = 0;
            do
            {
                // try it simple first
                worker?.ReportProgress(2, new ProgressMessageState($"Pinging '{address}'...", P.StepTypes.Begin));
                if (Ip.Ping(address, 4))
                {
                    worker?.ReportProgress(2, new ProgressMessageState($"Ping successfull => Connecting to AT '{address}'...", P.StepTypes.Begin));
                    if (connectionMethod(address, port))
                    {
                        return;
                    }
                }

                // check everything
                //Try to find adapters with command netsh interface show interface
                // for the moment we just work with the first one
                worker?.ReportProgress(2, new ProgressMessageState($"Ping failed => Getting Wifi Adapter...", P.StepTypes.Begin));
                var ada = GetWifiInteraceWithNetsh().FirstOrDefault();
                if (ada == null)
                    throw new Exception(R.WifiNoAdapter);


                //Interface is disabled by admin
                worker?.ReportProgress(2, new ProgressMessageState($"Wifi Adapter found => Checking for Admin Rights...", P.StepTypes.Begin));
                if (!ada.AdminStateEnabled)
                {
                    if (Tools.Access.IsAdministrator())
                    {
                        worker?.ReportProgress(2, new ProgressMessageState($"You are admin => Enabling Adapter...", P.StepTypes.Begin));
                        EnableAdapterAsAdmin(ada.InterfaceName);
                        System.Threading.Thread.Sleep(5000);
                        continue;
                    }
                    else
                        throw new Exception(R.WifiNoAdmin);
                }

                // Get all the corresponding objects

                worker?.ReportProgress(2, new ProgressMessageState($"Getting network interface...", P.StepTypes.Begin));
                var ni = GetNetworkInterface(ada.InterfaceName);
                var guid = Guid.Parse(ni.Id);
                var ici = GetInterfaceConnectionInfo(guid);

                //Wifi is turned off
                if (!ici.IsRadioOn)
                {
                    worker?.ReportProgress(2, new ProgressMessageState($"Wifi is turned OFF => Turning ON ...", P.StepTypes.Begin));
                    TurnOn(ici.Id);
                    System.Threading.Thread.Sleep(5000);
                    continue;
                }

                string ssid = SN;
                worker?.ReportProgress(2, new ProgressMessageState($"Checking for '{ssid}' availability ...", P.StepTypes.Begin));
                if (!IsSSIDAvailable(ssid))
                {
                    throw new Exception(R.WifiNoDevice);
                }

                worker?.ReportProgress(2, new ProgressMessageState($"'{ssid}' available => Connected? ...", P.StepTypes.Begin));
                if (!IsConnected(ssid))
                {
                    worker?.ReportProgress(2, new ProgressMessageState($"'{ssid}' available => not connected => connecting...", P.StepTypes.Begin));
                    if (Connect(ssid))
                    {
                        System.Threading.Thread.Sleep(5000);
                        continue;
                    }
                }

                if (!IsProfileExist(ssid))
                {
                    worker?.ReportProgress(2, new ProgressMessageState($"'{ssid}' profile do not exist => Profile creation...", P.StepTypes.Begin));
                    if (SetProfile(ssid))
                    {
                        System.Threading.Thread.Sleep(5000);
                        continue;
                    }
                }

                worker?.ReportProgress(2, new ProgressMessageState($"Checking for duplicate IP...", P.StepTypes.Begin));
                if (Ip.IsDuplicated(address))
                    throw new Exception(R.IpDuplicated);

                worker?.ReportProgress(2, new ProgressMessageState($"Checking if DHCP is activated...", P.StepTypes.Begin));
                if (!CheckDHCPMode(ada.InterfaceName))
                {
                    string titleAndMessage = string.Format(R.EnableDHCP);
                    MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.T_OK)
                    {
                        worker?.ReportProgress(2, new ProgressMessageState($"Enabling DHCP...", P.StepTypes.Begin));
                        EnableAutoDHCP(ada.InterfaceName);
                        System.Threading.Thread.Sleep(5000);
                        continue;
                    }
                }

                if (!CheckDNSMode(ada.InterfaceName))
                {
                    worker?.ReportProgress(2, new ProgressMessageState($"Enabling AUTO DNS...", P.StepTypes.Begin));
                    EnableAutoDNS(ada.InterfaceName);
                    System.Threading.Thread.Sleep(5000);
                    continue;
                }

                if (attempt == 0)
                {
                    worker?.ReportProgress(2, new ProgressMessageState($"Releasing and Renewing IP config...", P.StepTypes.Begin));
                    ReleaseAndRenewIpConfig(ada.InterfaceName);
                    //RenewIpConfig(ada.InterfaceName);
                    System.Threading.Thread.Sleep(7000);
                }
                else if (attempt == 1)
                {
                    DisableAndRenableAdapterAsAdmin(ada.InterfaceName);
                    System.Threading.Thread.Sleep(10000);
                }

                attempt++;
            } while (attempt <= 3);
            throw new Exception("Couldn't help with the connection");
        }


        public static bool IsAdapterEnabled(string interfaceDevice, bool test = false)
        {
            // Check if wifi adpater is enabled/disabled
            // interfaceDevice for Windows7 is "Wireless Network Connection"
            // interfaceDevice for Windows10 is "Wi-Fi"

            try
            {
                var enabledWifiInterface = NativeWifi.EnumerateInterfaceConnections()
                                            .Where(x => x.Description.ToLower().Contains(interfaceDevice.ToLower()));

                bool connected = enabledWifiInterface.Any() && !string.IsNullOrWhiteSpace(interfaceDevice);

                string state = connected ? "Enabled" : "DISABLED";
                if (!test)
                    Logs.Log.AddEntryAsFYI(null, $"{interfaceDevice} is {state}");
                return connected;
            }
            catch (Exception)
            { return false; }
        }

        public static bool IsAdapterEnabled(NetworkInterface inf, bool test = false)
        {
            // Check if wifi adpater is enabled/disabled
            try
            {
                bool connected;
                if (inf is null)
                {
                    connected = false;
                }
                else
                {
                    Guid id = Guid.Parse(inf.Id);
                    connected = NativeWifi.EnumerateInterfaceConnections().Any(x => x.Id == id);
                }

                if (!test)
                {
                    string state = connected ? "Enabled" : "DISABLED";
                    Logs.Log.AddEntryAsFYI(null, $"{inf.Description} is {state}");
                }

                return connected;
            }
            catch (Exception)
            { return false; }
        }

        public static bool DisableAndRenableAdapterAsAdmin(string interfaceName, bool test = false)
        {
            try
            {
                if (!test)
                {
                    string titleAndMessage = $"Tsunami cannot connect; We will try disabling and renabling the {interfaceName} adapter, you have to accept the following elevation request from Windows";
                    if (new MessageInput(MessageType.Warning, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL }
                    }.Show().TextOfButtonClicked == R.T_CANCEL)
                        return false;
                    Logs.Log.AddEntryAsFYI(null, $"Disabling and renabling the {interfaceName} adapter");
                }

                string disableCommand = $"netsh interface set interface \"{interfaceName}\" disable";
                string enableCommand = $"netsh interface set interface \"{interfaceName}\" enable";

                return RunProcess($"/C {disableCommand} && {enableCommand}");

            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool ReleaseAndRenewIpConfig(string interfaceName, bool test = false)
        {
            try
            {
                if (!test)
                {
                    string titleAndMessage = $"Tsunami cannot connect; We will try release and renew the address of the {interfaceName} adapter, you have to accept the following elevation request from Windows";
                    if (new MessageInput(MessageType.Warning, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL }
                    }.Show().TextOfButtonClicked == R.T_CANCEL)
                        return false;
                    Logs.Log.AddEntryAsFYI(null, $"Releasing and renewing IP for the {interfaceName} adapter");
                }

                string releaseCommand = $"ipconfig /release \"{interfaceName}\"";
                string renewCommand = $"ipconfig /renew \"{interfaceName}\"";

                return RunProcess($"/C {releaseCommand} && {renewCommand}", false);

            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool RenewIpConfig(string interfaceName)
        {
            try
            {
                string renewCommand = $"ipconfig /renew \"{interfaceName}\"";

                return RunProcess($"/C {renewCommand}", UseShellExecute: false);

            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool EnableAdapterAsAdmin(string interfaceName, bool test = false)
        {
            try
            {
                if (!test)
                {
                    string titleAndMessage = $"{interfaceName} is disable;You have to accept the following elevation request from Windows";
                    new MessageInput(MessageType.Warning, titleAndMessage).Show();
                    Logs.Log.AddEntryAsFYI(null, $"Enabling the {interfaceName} adapter");
                }
                string enableCommand = $"netsh interface set interface \"{interfaceName}\" enable";

                return RunProcess($" /C {enableCommand}");
            }
            catch (Exception)
            { return false; }
        }

        public static bool IsConnected(string ssid, bool test = false)
        {
            // Check if network with given SSID is connected
            try
            {
                var connectedNets = NativeWifi.EnumerateConnectedNetworkSsids()
                                            .Where(x => x.ToString().Contains(ssid));

                bool connected = connectedNets.Any();

                string state = connected ? "Connected" : "Not Connected";
                if (!test)
                    Logs.Log.AddEntryAsFYI(null, $"{state} to {ssid} AP");
                return connected;
            }
            catch (Exception)
            { return false; }
        }

        public static bool Connect(string ssid, bool test = false)
        {
            // Connect network with given SSID
            try
            {
                bool isConnected = false;
                AvailableNetworkPack availableNet = GetFirstNetContaining(ssid);

                if (availableNet != null && !string.IsNullOrEmpty(availableNet.ProfileName))
                {
                    isConnected = NativeWifi.ConnectNetwork(
                                                interfaceId: availableNet.Interface.Id,
                                                profileName: availableNet.ProfileName,
                                                bssType: availableNet.BssType);
                }
                if (isConnected && !test)
                    Logs.Log.AddEntryAsSuccess(null, R.WifiConnected + ssid + " AP");
                return isConnected;
            }
            catch (Exception)
            { return false; }
        }

        public static void Disconnect(string ssid, bool test = false)
        {
            AvailableNetworkPack availableNet = GetFirstNetContaining(ssid);

            if (availableNet != null && !string.IsNullOrEmpty(availableNet.ProfileName))
            {
                NativeWifi.DisconnectNetwork(interfaceId: availableNet.Interface.Id);
            }
        }

        internal static bool UpdateAfterEnable(string ssid)
        {
            Logs.Log.AddEntryAsFinishOf(null, R.WifiUpdate);
            WaitForSsidAvailibity(ssid);
            return Connect(ssid);
        }

        public static AvailableNetworkPack GetFirstNetContaining(string ssid)
        {
            string value = ssid.ToLower();

            bool TestNet(AvailableNetworkPack x) => x.Ssid.ToString().ToLower().Contains(value);

            return NativeWifi.EnumerateAvailableNetworks().FirstOrDefault(TestNet);
        }

        public static bool SetProfile(string ssid, bool test = false)
        {
            bool isSet = false;

            AvailableNetworkPack availableNet = GetFirstNetContaining(ssid);

            if (availableNet != null)
            {
                string profileName = availableNet.ProfileName;
                if (string.IsNullOrEmpty(profileName))
                    profileName = availableNet.Ssid.ToString();

                try
                {
                    string path = CreateXMLForProfile(profileName);
                    isSet = CreateProfileBasedOnXML(path, ssid, test);

                    if (!test)
                        Logs.Log.AddEntryAsFYI(null, $"Setting Profile for {ssid}");
                }
                catch (Exception)
                {
                    return false;
                }
            }

            return isSet;
        }

        public static string CreateXMLForProfile(string profileName)
        {
            byte[] bytes = Encoding.Default.GetBytes(profileName);
            var hexProfileName = BitConverter.ToString(bytes);
            hexProfileName = hexProfileName.Replace("-", "");
            string path = "profile_xml.xml";

            string profileXML = string.Format("<?xml version=\"1.0\"?>" +
                                                    "<WLANProfile xmlns = \"http://www.microsoft.com/networking/WLAN/profile/v1\">" +
                                                    "<name>{0}</name>" +
                                                    "<SSIDConfig>" +
                                                    "<SSID>" +
                                                    "<hex>{1}</hex>" +
                                                    "<name>{0}</name>" +
                                                    "</SSID>" +
                                                    "</SSIDConfig>" +
                                                    "<connectionType>ESS</connectionType>" +
                                                    "<connectionMode>auto</connectionMode>" +
                                                    "<MSM>" +
                                                    "<security>" +
                                                    "<authEncryption>" +
                                                    "<authentication>open</authentication>" +
                                                    "<encryption>none</encryption>" +
                                                    "<useOneX>false</useOneX>" +
                                                    "</authEncryption>" +
                                                    "</security>" +
                                                    "</MSM>" +
                                                    "</WLANProfile>", profileName, hexProfileName);

            File.WriteAllText(path, profileXML);
            return path;
        }

        public static bool CreateProfileBasedOnXML(string pathXML, string ssid, bool test = false)
        {
            bool isSet = RunProcess($"/C netsh wlan add profile filename=\"{pathXML}\"", false);

            File.Delete(pathXML);
            return isSet;
        }

        public static void WaitForSsidAvailibity(string ssid)
        {
            var avNet = false;
            var count = 0;
            Stopwatch sw = Stopwatch.StartNew();
            do
            {
                AvailableNetworkPack availableNet = GetFirstNetContaining(ssid);
                avNet = availableNet != null;
                if (count >= 10000)
                    break;
                count++;
            }
            while (!avNet);

            sw.Stop();
        }

        public static void Debug_ListNetworkInterfaces(Action<string> action)
        {
            NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();
            for (int i = 0; i < interfaces.Length; i++)
            {
                NetworkInterface inf = interfaces[i];
                action($"NetworkInterface.GetAllNetworkInterfaces()[{i}]: Description={inf.Description} Id={inf.Id} IsReceiveOnly={inf.IsReceiveOnly} Name={inf.Name} NetworkInterfaceType={inf.NetworkInterfaceType} OperationalStatus={inf.OperationalStatus}");
            }
        }

        public static void Debug_ListWifiInterfaces(Action<string> action)
        {
            var interfaces = NativeWifi.EnumerateInterfaceConnections().ToList();
            for (int i = 0; i < interfaces.Count; i++)
            {
                InterfaceConnectionInfo inf = interfaces[i];
                action($"NativeWifi.EnumerateInterfaceConnections().ToList()[{i}]: ConnectionMode={inf.ConnectionMode} Description={inf.Description} Id={inf.Id} IsConnected={inf.IsConnected} IsRadioOn={inf.IsRadioOn} ProfileName={inf.ProfileName} State={inf.State}");
            }
        }

        public static void Debug_ListSSIDs(Action<string> action)
        {
            var ssids = NativeWifi.EnumerateAvailableNetworkSsids().ToList();
            for (int i = 0; i < ssids.Count; i++)
            {
                var ssid = ssids[i];
                action($"NativeWifi.EnumerateAvailableNetworkSsids().ToList()[{i}]: {ssid}");
            }
            ssids = NativeWifi.EnumerateConnectedNetworkSsids().ToList();
            for (int i = 0; i < ssids.Count; i++)
            {
                var ssid = ssids[i];
                action($"NativeWifi.EnumerateConnectedNetworkSsids().ToList()[{i}]: {ssid}");
            }
        }

        public static string GetFirstWirelessInterfaceUpName() => GetFirstWirelessInterfaceUp()?.Name;

        /// <summary>
        /// Get name of the first wifi interface up
        /// </summary>
        /// <returns></returns>
        public static NetworkInterface GetFirstWirelessInterfaceUp()
        {
            return Array.Find(
                NetworkInterface.GetAllNetworkInterfaces(),
            // Type of interface - Wireless80211 - the network interface uses a wireless LAN connection
                testedAdapter => (testedAdapter.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
                              && (testedAdapter.OperationalStatus == OperationalStatus.Up));
        }

        public static NetworkInterface GetNetworkInterface(string name)
        {
            return Array.Find(
                NetworkInterface.GetAllNetworkInterfaces(),
                testedAdapter => testedAdapter.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
        }

        public static NetworkInterface GetNetworkInterface(Guid guid)
        {
            return Array.Find(
                NetworkInterface.GetAllNetworkInterfaces(),
                testedAdapter => Guid.Parse(testedAdapter.Id).Equals(guid));
        }

        public static InterfaceConnectionInfo GetInterfaceConnectionInfo(Guid guid)
        {
            return NativeWifi.EnumerateInterfaceConnections()
                             .FirstOrDefault(ici => ici.Id.Equals(guid));
        }


        internal static bool ReConnectionAfterChange(string ssid, string errorMeassage)
        {
            Logs.Log.AddEntryAsFYI(null, errorMeassage);
            return Connect(ssid);
        }

        public static bool TurnOn(string interfaceDevice, bool test = false)
        {
            // Turn on wifi interface
            try
            {
                Guid id = NativeWifi.EnumerateInterfaces()
                                    .FirstOrDefault(x => x.Description.Contains(interfaceDevice))
                                    .Id;
                return TurnOn(id, test);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool TurnOn(Guid id, bool test = false)
        {
            try
            {
                if (!test)
                    Logs.Log.AddEntryAsFYI(null, R.WifiTurnOff);
                var interfaceRadio = NativeWifi.GetInterfaceRadio(id);
                return NativeWifi.TurnOnInterfaceRadio(interfaceRadio.Id);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool GetAvailableSsids(string model, out List<string> ssids, out List<Common.Instruments.Instrument> instruments)
        {
            return Common.Instruments.Manager.Module.GetAvailableSsids(model, out ssids, out instruments);
        }

        public static List<string> GetSSIDs()
        {
            var list = new List<string>();

            try
            {
                // Retrieve all available networks
                var availableNetworks = NativeWifi.EnumerateAvailableNetworks();

                // Iterate through each network and print the SSID
                foreach (var network in availableNetworks)
                {
                    list.Add(network.Ssid.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");
            }
            return list;
        }


        public static bool IsTurnOn(string interfaceDevice, bool test = false)
        {
            // Check if wifi interface is switch-on/off
            try
            {
                var connectedWifiInterface = NativeWifi.EnumerateInterfaces()
                                                       .FirstOrDefault(x => x.Description.Contains(interfaceDevice));

                var interfaceRadio = NativeWifi.GetInterfaceRadio(connectedWifiInterface.Id);

                bool isTurnOn = interfaceRadio.RadioSets.Any(x => x.SoftwareOn == false);

                string state = isTurnOn ? "ON" : "OFF";
                if (!test)
                    Logs.Log.AddEntryAsFYI(null, $"{interfaceDevice} is {state}");
                return isTurnOn;
            }
            catch (Exception)
            { return false; }



        }

        internal static bool IsSSIDAvailable(string ssid)
        {
            try
            {
                AvailableNetworkPack availableNet = GetFirstNetContaining(ssid);
                bool available = availableNet != null;
                return available;
            }
            catch (Exception)
            { return false; }

        }

        public static bool IsProfileExist(string ssid, bool test = false)
        {
            // Check if profile exists
            // Profile - wifi
            AvailableNetworkPack availableNet = GetFirstNetContaining(ssid);

            bool isSetProfile = availableNet != null && !string.IsNullOrEmpty(availableNet.ProfileName);
            string state = isSetProfile ? "EXIST" : "DO NOT EXIST";
            if (!test)
                Logs.Log.AddEntryAsFYI(null, $"Profil for {ssid} {state}");
            return isSetProfile;
        }

        public static bool CheckDHCPMode(string interfaceName, bool test = false)
        {

            // Check if IP assignment is Automatic (DHCP) - It works only if the wifi is connected !!!! (windows limitation)
            bool isAuto = IsDhcpEnabled(interfaceName, test);

            var numConnects = NativeWifi.EnumerateInterfaceConnections().Count();
            bool isDHCPAuto = (numConnects != 0) && isAuto;


            return isDHCPAuto;

        }


        internal static bool CheckDNSMode(string interfaceName)
        {

            // Check if DNS server is Automatic 
            bool isDNSAuto = IsDNSAutoEnabled(interfaceName);

            return isDNSAuto;

        }

        public static bool IsDhcpEnabled(string interfaceName, bool test = false)
        {
            if (!test)
                Logs.Log.AddEntryAsFYI(null, $"Checking DHCP status for {interfaceName} ");
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            NetworkInterface wifi = Array.Find(nics, x => x.Name == interfaceName);
            if (wifi == null)
            {
                if (!test)
                    Logs.Log.AddEntryAsFYI(null, $"No Network Interface matching {interfaceName} ");
                return false;
            }
            bool isEnabled = wifi.GetIPProperties()?.GetIPv4Properties()?.IsDhcpEnabled ?? false;

            return isEnabled;

        }

        public static bool EnableAutoDHCP(string interfaceName, bool test = false)
        {
            try
            {
                if (!test)
                    Logs.Log.AddEntryAsFYI(null, $"Enabling the IP assignment to Automatic DHCP mode for {interfaceName} ");
                string enableCommand = $"netsh interface ip set address \"{interfaceName}\" dhcp";

                Process p = new Process()
                {
                    StartInfo = new ProcessStartInfo()
                    {
                        FileName = "CMD.exe",
                        Arguments = $" /C {enableCommand}",
                        UseShellExecute = true,
                        Verb = "runas",
                        RedirectStandardOutput = false,
                        CreateNoWindow = true
                    }
                };
                p.Start();
                p.WaitForExit();
                return true;


            }
            catch (Exception)
            { return false; }
        }

        public static bool IsDNSAutoEnabled(string interfaceName, bool test = false)
        {
            if (!test)
                Logs.Log.AddEntryAsFYI(null, $"Checking DNS server assignment for {interfaceName} ");
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            NetworkInterface wifi = Array.Find(nics, x => x.Name == interfaceName);
            if (wifi == null)
            {
                if (!test)
                    Logs.Log.AddEntryAsFYI(null, $"No Network Interface matching {interfaceName} ");
                return false;
            }

            //bool isEnabled = wifi.GetIPProperties().IsDnsEnabled; --> // It should work but all the time is false

            string path = "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters\\Interfaces\\" + wifi.Id;
            string ns = (string)Registry.GetValue(path, "NameServer", null);
            bool isEnabled = string.IsNullOrEmpty(ns);
            return isEnabled;

        }
        public static bool EnableAutoDNS(string interfaceName, bool test = false)
        {

            try
            {
                if (!test)
                    Logs.Log.AddEntryAsFYI(null, $"Enabling DNS server assignment to automatic for {interfaceName} ");
                string enableCommand = $"netsh interface ip set dnsservers name= \"{interfaceName}\" source=dhcp";

                return RunProcess($" /C {enableCommand}");

            }
            catch (Exception)
            { return false; }

        }

        public static List<string> GetListOfInstrumentForTest(string path)
        {
            List<string> deviceList = new List<string>();
            XmlDocument doc = new XmlDocument();
            doc.Load(path);
            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                deviceList.Add(node.Attributes["SSID"]?.InnerText);
            }
            return deviceList;
        }

        public static IEnumerable<AvailableNetworkPack> GetListOfActiveInstrument(string path)
        {
            List<string> deviceName = GetListOfInstrumentForTest(path);
            var availableNet = NativeWifi.EnumerateAvailableNetworks().ToList();
            var ATx_Active = availableNet.Where(x => deviceName.Exists(y => y.ToLower() == x.ProfileName.ToLower()));
            return ATx_Active;
        }

        public static string GetInstrumentSSID(XmlDocument doc, string serialNumber)
        {

            string ssid = "";
            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                if (node.Attributes["SN"]?.InnerText == serialNumber)
                {
                    ssid = node.Attributes["SSID"]?.InnerText;

                }
            }
            return ssid;
        }

        public static string GetInstrumentID(XmlDocument doc, string serialNumber)
        {

            string id = "";
            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                if (node.Attributes["SN"]?.InnerText == serialNumber)
                {
                    id = node.Attributes["Type"]?.InnerText + "_" + node.Attributes["SN"]?.InnerText;
                }
            }
            return id;
        }

        public static bool RunProcess(string argument, bool UseShellExecute = true)
        {
            try
            {
                Process p = new Process()
                {
                    StartInfo = new ProcessStartInfo()
                    {
                        FileName = "CMD.exe",
                        Arguments = argument,
                        UseShellExecute = UseShellExecute,
                        Verb = "runas",
                        RedirectStandardOutput = false,
                        CreateNoWindow = true
                    }
                };
                p.Start();
                p.WaitForExit();
                bool isValid = p.ExitCode == 0;
                return isValid;

            }
            catch (Exception)
            { 
                return false; 
            }



        }

        private static readonly Lazy<Regex> regexForGetWifiInteraceWithNetsh = new Lazy<Regex>(
            () => new Regex("([a-z]+) +([a-z]+) +([a-z]+) +(.*?wi[\\- ]*fi.*)", RegexOptions.IgnoreCase)
        );

        public static IEnumerable<NetshInterface> GetWifiInteraceWithNetsh()
        {
            Process p;
            try
            {
                p = new Process()
                {
                    StartInfo = new ProcessStartInfo()
                    {
                        FileName = "powershell.exe",
                        UseShellExecute = false,
                        Verb = "runas",
                        RedirectStandardOutput = true,
                        RedirectStandardInput = true,
                        CreateNoWindow = true
                    }
                };

                p.Start();
            }
            catch (FileNotFoundException)
            {
                throw new TsuException("powershell.exe not found, did LGC delete your windows PATH?");
            }
            catch (Exception)
            {
                throw;
            }

            p.StandardInput.WriteLine("$UsersCulture = (Get-Culture).Name");
            p.StandardInput.WriteLine("Set-Culture en-Us");
            p.StandardInput.WriteLine("netsh interface show interface");
            p.StandardInput.WriteLine("Set-Culture $UsersCulture");
            p.StandardInput.WriteLine("exit");
            p.WaitForExit();
            string output = p.StandardOutput.ReadToEnd();

            foreach (string s in output.Split('\r', '\n'))
            {
                Match match = regexForGetWifiInteraceWithNetsh.Value.Match(s);
                if (match.Success && match.Groups.Count > 4)
                    yield return new NetshInterface(match);
            }
        }

        public class NetshInterface
        {
            public bool AdminStateEnabled { get; }
            public bool StateConnected { get; }
            public string InterfaceName { get; }

            public NetshInterface(Match match)
            {
                AdminStateEnabled = match.Groups[1].Value.Trim() == "Enabled";
                StateConnected = match.Groups[2].Value.Trim() == "Connected";
                InterfaceName = match.Groups[4].Value.Trim();
            }
        }
    }
}
