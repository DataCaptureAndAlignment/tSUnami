﻿
using R = TSU.Properties.Resources;

namespace TSU.Common
{
    partial class TsunamiView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TsunamiView));
            this.SuspendLayout();
            // 
            // _PanelBottom
            // 
            resources.ApplyResources(this._PanelBottom, "_PanelBottom");
            // 
            // _PanelTop
            // 
            resources.ApplyResources(this._PanelTop, "_PanelTop");
            // 
            // TsunamiView
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            resources.ApplyResources(this, "$this");
            this.Name = "TsunamiView";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TsunamiView_FormClosing);
            this.Controls.SetChildIndex(this._PanelTop, 0);
            this.Controls.SetChildIndex(this._PanelBottom, 0);
            this.ResumeLayout(false);
        }

        #endregion
    }
}

