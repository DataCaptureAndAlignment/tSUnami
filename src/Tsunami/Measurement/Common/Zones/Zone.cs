﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;
using TSU.Common.Elements;
using TSU.Common.Compute.Transformation;

namespace TSU.Common.Zones
{
    [XmlType(TypeName = "Zone")]
    public class Zone : TsuObject
    {
        public class Ccs2Mla
        {
            [XmlAttribute]
            public string OriginName { get; set; }

            [XmlAttribute]
            public Coordinates.ReferenceSurfaces ReferenceSurface { get; set; }
            public Coordinates Origin { get; set; }

            /// <summary>
            /// This represent the bearing in CCS (XYZ) of the 3Dline that the projection will be parallel to the local Y axis 
            /// </summary>
            [XmlElement("BeamBearingInCcsInGon")]
            public DoubleValue Bearing { get; set; }

            /// <summary>
            /// This represent the slope in CCS (XYZ) of the 3Dline that the projection will be parallel to the local Y axis 
            /// </summary>
            [XmlElement("BeamSlopeInCcsInRad")]
            public DoubleValue Slope { get; set; }
            
            [XmlAttribute]
            public double SumToAvoidUnawareModification { get; set; }
        }

        [XmlAttribute]
        public string Source { get; set; }

        [XmlAttribute]
        public DateTime Date { get; set; }

        public Ccs2Mla Ccs2MlaInfo { get; set; }

        private string pathSource;

        [XmlIgnore]
        public string PathSource {
            get
            {
                if (pathSource == "")
                    return "???";
                else
                    return pathSource;
            }

            set
            {
                pathSource = value;
            }
        }

        public Rotation3D Mla2SuInGon { get; set; }
        public Rotation3D Su2PhysInGon { get; set; }
        public Translation3D Su2PhysInM { get; set; }
        public Rotation BeamInclinaisonInRad { get; set; }

        [XmlIgnore]
        public bool Validity
        { 
            get
            {
                bool oriOk = false;
                if (this.Ccs2MlaInfo != null)
                {
                    if (this.Ccs2MlaInfo.Origin != null)
                    {
                        var info = this.Ccs2MlaInfo;
                        double sum = info.Origin.X.Value + info.Origin.Y.Value + info.Origin.Z.Value + info.Bearing.Value + info.Slope.Value;
                        oriOk = (Math.Round(sum, 6) == Math.Round(Ccs2MlaInfo.SumToAvoidUnawareModification, 6));
                    }
                    else
                        oriOk = true;
                }
                else
                    oriOk = true;


                bool rot1Ok = (this.Su2PhysInGon != null) ? this.Su2PhysInGon.Validity : true;
                bool rot2Ok = (this.Mla2SuInGon != null) ? this.Mla2SuInGon.Validity : true;
                bool rot3Ok = (this.BeamInclinaisonInRad != null) ? this.BeamInclinaisonInRad.Validity : true;
                return oriOk && rot2Ok && rot1Ok && rot3Ok;
            }
        }

        public Zone()
        {

        }

        public Zone(string name,Coordinates origin,   DoubleValue bearing, DoubleValue slope,
            Rotation3D rotationsBetweenMlaAndSurvey =null, Rotation3D rotationsBetweenSurveyAndPhysicistSystems =null)
        {
            this._Name = name;
            this.Ccs2MlaInfo = new Ccs2Mla();
            this.Ccs2MlaInfo.Origin = origin;
            this.Ccs2MlaInfo.Bearing = bearing;
            this.Ccs2MlaInfo.Slope = slope;
            this.Mla2SuInGon = (rotationsBetweenMlaAndSurvey == null) ? new Rotation3D() : rotationsBetweenMlaAndSurvey;
            this.Su2PhysInGon = (rotationsBetweenSurveyAndPhysicistSystems == null) ? new Rotation3D() : rotationsBetweenSurveyAndPhysicistSystems;
        }

        public override string ToString()
        {
            return $"{this._Name} " +
                $"{ ( Date != null ? $"from {Date.ToString("yyyy-MM-dd")}": "" ) } " +
                $"in file '{PathSource}' " +
                $"{ ( Ccs2MlaInfo != null ? $"(reference surface: {Ccs2MlaInfo.ReferenceSurface.ToString()})" : "" ) }";
        }

        public static Zone FixDefinition(TSU.Views.TsuView view, System.IO.FileInfo filePath)
        {
            // 3: Ask zone to fill missing coordinates
            Manager m = new Manager(view._Module) { _Name = "Management.Zone Manager" };
            foreach (var zone in TSU.Tsunami2.Preferences.Values.Zones)
            {
                if (filePath.FullName.Contains(zone._Name))
                    m._SelectedObjectInBlue = zone;
            }

            Zone z = m.View.SelectOne($"{R.T_ZONE_SELECTION}; { R.T_LETS_SELECT_OR_CREATE_A_NEW_ZONE_TO_BE_ABLE_TO_TRANSFORME_THE_GLOBAL_COORDINATES_INTO_LOCAL_ONES}");
            m.Dispose();
            return z;

        }
    }
}
