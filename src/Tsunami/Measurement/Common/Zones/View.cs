﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using TSU.ENUM;
using TSU.Views;
using V = TSU.Common.Strategies.Views;
using TSU.Common.Instruments;
using TSU.Common.Measures;
using TSU.Views.Message;


namespace TSU.Common.Zones
{
    public partial class View : ManagerView
    {
        // Fields
        private Manager zoneManager
        {
            get
            {
                return this._Module as Manager;
            }
        }
        //Constructor
        public View(TSU.IModule module)
            : base(module) // Taking form already in controllerModuleView
        {
            this._Name =R.T367;
            this.InitializeComponent();
            this.ApplyThemeColors();
            this.AdaptTopPanelToButtonHeight();
            this.InitializeMenu();
            this.AutoCheckChildren = true;
            this.CheckBoxesVisible = false;

            this.currentStrategy.Update();
            this.currentStrategy.Show();

            this.Image = R.Element_Zone;
            
        }
        private void ApplyThemeColors()
        {

        }
        internal override void InitializeMenu()
        {
            base.InitializeMenu();
            bigbuttonTop.ChangeNameAndDescription(R.T_LOCAL_ZONE);
            bigbuttonTop.ChangeImage(R.Module);
        }


        internal override void InitializeStrategy()
        {
            this.AvailableViewStrategies = new List<V.Types>() { V.Types.Tree };
            this.SetStrategy(TSU.Tsunami2.Preferences.Values.GuiPrefs.ZoneViewType);
        }
        
        internal override void SavePreference(V.Types viewStrategyType)
        {
           TSU.Tsunami2.Preferences.Values.GuiPrefs.ZoneViewType = viewStrategyType;
            base.SavePreference(viewStrategyType);
        }

        internal override void ShowContextMenuGlobal()
        {
            contextButtons = new List<Control>(); 
            this.ShowPopUpMenu(contextButtons);
        }
        // Strategy
        // define an interface for different strategy of showing the elements
        public override void UpdateView()
        {
            base.UpdateView();
            this.currentStrategy.Update();
            //this.TryAction(this.UpdateView);
        }


        public void ValidateSelection()
        {
            this.TryAction(this.currentStrategy.ValidateSelection);
        }

        public override List<string> ListviewColumnHeaderTexts
        {

            get
            {
                List<string> l = new List<string>();
                l.Add("Name");
                l.Add("Mla2SuInGon.RotX [gon]");
                l.Add("Mla2SuInGon.RotY [gon]");
                l.Add("Mla2SuInGon.RotZ [gon]");
                l.Add("Origin Name");
                l.Add("Origin.X [m]");
                l.Add("Origin.Y [m]");
                l.Add("Origin.Z [m]");
                l.Add("Origin.Bearing [gon]");
                l.Add("Origin.Slope [gon]");
                l.Add("Su2Phys RotX [gon]");
                l.Add("Su2Phys RotY [gon]");
                l.Add("Su2Phys RotZ [gon]");
                l.Add("Su2Phys TX [m]");
                l.Add("Su2Phys TY [m]");
                l.Add("Su2Phys TZ [m]");

                l.Add("Beam Inclinaison [Rad]");

                return l;
            }
            set { }
        }


        public Zone SelectOne(string titleAndMessage = null)
        {
            this.Module.MultiSelection = false;

            this.CheckBoxesVisible = false;

            
            this.UpdateView();

            void HideMessageVIew(object o, V.EventArgs e)
            {
                if (e.ModuleView.Parent.Parent.Parent is MessageTsu)
                {
                    (e.ModuleView.Parent.Parent.Parent as MessageTsu).Hide();
                }
            };


            this.currentStrategy.DoubleClick += HideMessageVIew;

            this.ShowInMessageTsu(
                titleAndMessage ??R.T369,
                "Select", CheckAndValidate,
                R.T_CANCEL, this.currentStrategy.CancelSelection,
                null, null,
                null, null);


            this.currentStrategy.DoubleClick -= HideMessageVIew;

            if (this.Module._SelectedObjects.Count > 0)
                return this.Module._SelectedObjects[0] as Zone;
            else
                return null;
        }

        private void CheckAndValidate()
        {
            if (this.Module._SelectedObjects.Count == 1)
            {
                _SelectionView.CancelClosing = false;
                this.ValidateSelection();
            }
            else
            {
                _SelectionView.CancelClosing = true;
                new MessageInput(MessageType.Warning, "Choose a zone or click 'cancel'").Show();
            }
        }
    }
    
}
