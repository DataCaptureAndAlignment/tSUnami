﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;
using TSU;
using P = TSU.Preferences;
using M = TSU;

namespace TSU.Common.Zones
{
    [XmlType(Namespace = "Zones", TypeName = "Manager")]
    public class Manager : M.Manager    
    {
        public Zones.Zone selectedZone;

        public TSU.Common.Elements.Coordinates.ReferenceSurfaces selectedReferenceSurfaces;

        [XmlIgnore]
        public new Zones.View View
        {
            get
            {
                return this._TsuView as Zones.View;
            }
        }

        [XmlIgnore]
        public List<Zones.Zone> SelectedZones
        {
            get
            {
                return this._SelectedObjects.Cast<Zones.Zone>().ToList();
            }
        }

        public Manager()
        {

        }

        public Manager(M.Module parentModule)
            : base(parentModule,R.T365)
        {

        }

        public override void Initialize()
        {
            this.AllElements = new List<TsuObject>();
            base.Initialize();
            MultiSelection = false;

            foreach (Zone item in P.Preferences.Instance.Zones)
	        {
                this.AllElements.Add(item);
	        }

            this.UpdateView();
        }


        internal override bool IsItemSelectable(TsuObject tsuObject)
        {
            return (tsuObject is Zone);
        }
    }
}
