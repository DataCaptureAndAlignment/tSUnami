using System;
using System.Collections.Generic;
using System.Text;
using R = TSU.Properties.Resources;
using System.IO;
using System.Xml.Serialization;
using TSU.Views;
using System.Linq;
using T = TSU.Tools;
using TSU.Common;
using I = TSU.Common.Instruments;
using TSU.Common.Instruments.Reflector;
using TSU.Common.Compute;
using TSU.Common.Compute.Compensations.Strategies;
using System.Runtime.CompilerServices;
using TSU.Views.Message;

namespace TSU.Common.Measures
{
    [XmlType(Namespace = "Measures", TypeName = "Manager")]
    public class Manager : TSU.Manager
    {
        public Manager()
            : base()
        {

        }

        [XmlIgnore]
        public Station.Module StationModule
        {
            get
            {
                return this.ParentModule as Common.Station.Module;
            }
            set
            {
                this.ParentModule = value;
            }
        }

        public new View View
        {
            get
            {
                return this._TsuView as View;
            }
        }

        public Manager(Module module)
            : base(module, R.T355)
        {

        }

        public override void Initialize()
        {
            this.AllElements = new List<TsuObject>();
            base.Initialize();
        }


        // IObservable<TsuObject>, IObserver<TsuObject>
        public override void OnNext(TsuObject TsuObject)
        {
            dynamic dynamic = TsuObject;
            OnNextBasedOn(dynamic);
        }
        public void OnNextBasedOn(Measure value)
        {
            ((View)_TsuView).UpdateView(value);
        }

        private static void AddText(FileStream fs, string value)
        {
            byte[] info = new UTF8Encoding(true).GetBytes(value);
            fs.Write(info, 0, info.Length);
        }
        internal void DoReduction()
        {
            List<Polar.Measure> toBeReductedList = new List<Polar.Measure>();
            foreach (Polar.Measure m in this.AllElements) toBeReductedList.Add(m);
            Survey.DoReduction(toBeReductedList);
        }

        internal override void ReCreateWhatIsNotSerialized(bool saveSomeMemory = false)
        {
            try
            {
                base.ReCreateWhatIsNotSerialized(saveSomeMemory);

                this._SelectedObjects = new List<TsuObject>();
                this.SelectableObjects = new List<TsuObject>();
                this._TsuView = new View(this);
            }
            catch (Exception ex)
            {
                throw new Exception($"Cannot re-create what was not serialized in {this._Name}", ex);
            }
        }

        public void SaveToXml(string fileName = null)
        {
            string filepath = this.View.GetSavingName(
                TSU.Tsunami2.Preferences.Values.Paths.Data + string.Format("{0:yyyyMMdd}", DateTime.Now) + "\\",
                TSU.Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now) + "_" + this._Name,
                "All files (*.*)|*.*|Geode (*.dat)|*.dat|Exp (*.res)|*.res|xml (*.xml)|*.xml");

            if (filepath != "")
            {
                TSU.IO.Xml.CreateFile(this, fileName);
                Shell.Run(fileName);
            }
        }
        internal bool ExportToGeode()
        {
            if (this.ParentModule is Common.Station.Module)
            {
                // to forget what was selected during th einfield compute
                return IO.SUSoft.Geode.ToGeode((object)this.StationModule._Station, out _);
            }
            else
            {
                return IO.SUSoft.Geode.ToGeode(this.AllElements, out _);
            }
        }
        internal void ExportToPLGC()
        {
            //Compute.PLGC.Input.Create(this._Elements);
        }
        internal void ExportToLGC1()
        {
            //Compute.LGC.Input.Create(this._Elements);
        }
        internal void ExportToLGC2()
        {
            if (this.ParentModule is Common.Station.Module)
            {
                // to forget what was selected during th einfield compute
                this.StationModule.ExportToLGC2();
            }
            else
            {
                IO.SUSoft.Lgc2.ToLgc2(this._SelectedObjects, "");
            }
        }

        internal void Show(
            string picture = "m",
            string titleAndMessage = null,
            string validate = "OK",
            string cancel = null)
        {
            titleAndMessage = titleAndMessage ?? R.T357;
            ManagerView m = this._TsuView as ManagerView;
            m.UpdateView();
            m.ShowInMessageTsu(
                titleAndMessage ?? R.T358,
                validate, null,
                cancel, null,
                null, null,
                null, null);
        }



        internal void ShowAllMeasuresIncludedInParentModule()
        {
            this.AllElements.Clear();
            List<Common.Station.Module> sms = Analysis.Module.FindAllStationModules(this.ParentModule);

            string titleAndMessage = "Trying to aggregate measures;This is experimental, It will aggregate the measures available in the manager of each station module, some modules can use other variables to store measures";
            new MessageInput(MessageType.Warning, titleAndMessage).Show();

            foreach (Station.Module sm in sms)
            {
                foreach (Measure m in sm._Station.MeasuresTaken)
                {
                    m.SendingStationModule = sm;
                    this.AllElements.Add(m);
                }
            }
            this.SelectableObjects.Clear(); // To show all
        }

        internal override bool IsItemSelectable(TsuObject tsuObject)
        {
            return tsuObject is Measure;
        }

        internal void Export2Csv()
        {
            string path = TsuPath.GetFileNameToSave(TSU.Tsunami2.Preferences.Values.Paths.Measures, "Measures", "csv (*.csv) | *.csv", R.T_SELECT_THE_PLACE_WHERE_TO_EXPORT_YOUR_FILE);
            if (path != "")
            {

                TSU.IO.Csv.ToCsv(this, path);
                Shell.Run(path);
            }
        }

        [field: XmlIgnore]
        public event MeasurementEventHandler MeasureStateChange;

        internal static void ChangeMeasureStatus(TsuView view, List<TsuObject> list, State state, bool silent = false)
        {
            foreach (Measure item in list)
            {
                Measure measureToModify = item;
                if (item.Tag != null)
                    if (item.Tag is Measure)
                        measureToModify = item.Tag as Polar.Measure;
                measureToModify._Status = state;
                if (state is States.Bad)
                {
                    if (measureToModify.GeodeRole != IO.SUSoft.Geode.Roles.Unknown) // if the station is not setup yet, the role will be defined based on the status when Strategy.SetDefaultMeasureSelection(AllElements);
                         measureToModify.GeodeRole = IO.SUSoft.Geode.Roles.Unused;
                    measureToModify._Point.State = Elements.Element.States.Bad;
                }
                else if (state is States.Good)
                {
                    if (measureToModify.GeodeRole != IO.SUSoft.Geode.Roles.Unknown) // if the station is not setup yet, the role will be defined based on the status when Strategy.SetDefaultMeasureSelection(AllElements);
                        measureToModify.GeodeRole = measureToModify._OriginalPoint.Type == Elements.Point.Types.Reference ? IO.SUSoft.Geode.Roles.Cala : IO.SUSoft.Geode.Roles.Poin;
                    measureToModify._Point.State = Elements.Element.States.Good;
                }

                var module = view._Module as TSU.Common.Measures.Manager;
                if (module?.MeasureStateChange != null) module.MeasureStateChange(module, new MeasurementEventArgs(item));
            }

            

            view.UpdateView();
            if (!silent)
            {
                string titleAndMessage = $"{list.Count} {R.T_CHANGES}; {R.T_STATE_OF} {list.Count} {R.T_MEASURES_HAVE_BEEN_CHANGED_TO} '{state._Name}'";
                new MessageInput(MessageType.GoodNews, titleAndMessage).Show();
            }
        }


        /// <summary>
        /// modify the Extension of the given measure, or the measure in the tag if one, create a modified copy and keep the orinal as bad with a comment
        /// </summary>
        /// <param name="m"></param>
        /// <param name="newExtension"></param>
        internal void ModifyExtension(Measure measureFromView, DoubleValue newExtension, bool recompute = false)
        {
            Measure measureToModify = measureFromView;
            if (measureFromView.Tag != null)
                if (measureFromView.Tag is Measure)
                    measureToModify = measureFromView.Tag as Measure;

            Measure clone = measureToModify.Clone() as Measure;
            clone._Point = measureToModify._Point.Clone() as Elements.Point;
            clone.Guid = Guid.NewGuid();
            string titleAndMessage = $"{R.T_WILL_YOU_EXPORT_FOR_GEODE_OR_LGC};{R.T_WHEN_YOU_CHANGE_A_MEASURE_A_UNMODIFIED}.";
            string buttonMiddle = $"{R.T_QUESTIONNABLE}\r\n({R.T_FOR} Geode)";
            string buttonRight = $"{R.T_BAD}\r\n({R.T_FOR} LGC)";
            MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
            {
                ButtonTexts = new List<string> { buttonMiddle, buttonRight }
            };
            string r = mi.Show().TextOfButtonClicked;
            if (r.Contains(R.T_BAD))
                clone._Status = new States.Bad();
            else
                clone._Status = new States.Questionnable();
            clone.CommentFromTsunami = string.Format(" Ext: {0} => {1}", measureToModify.Extension.ToString(), newExtension.ToString());
            measureToModify.Extension = newExtension;
            measureFromView.Extension = newExtension;

            if (recompute) RecomputePoint(measureToModify);
            AddBadCloneBeforeGoodOne(measureFromView, measureToModify, clone);

            this.UpdateView();
        }

        private void AddBadCloneBeforeGoodOne(Measure measureFormTheView, Measure oldMeasure, Measure newMeasure)
        {
            int indexOld;

            Station.Module sm = GetBestStationModule(measureFormTheView);
            indexOld = sm._Station.MeasuresTaken.LastIndexOf(oldMeasure);
            sm._Station.MeasuresTaken.Insert(indexOld, newMeasure);

            indexOld = this.AllElements.LastIndexOf(measureFormTheView);
            this.AllElements.Insert(indexOld, newMeasure);
            this.SelectableObjects.Add(newMeasure);
        }

        /// <summary>
        /// modify the reflector of the given measure, or the measure in the tag if one,create a modified copy and keep the orinal as bad with a comment
        /// </summary>
        /// <param name="m"></param>
        /// <param name="newReflector"></param>
        internal void ModifyReflector(Polar.Measure measureFromView, Reflector newReflector, bool recompute = false)
        {
            Polar.Measure oldMeasure = measureFromView;
            if (measureFromView.Tag != null)
                if (measureFromView.Tag is Measure)
                    oldMeasure = measureFromView.Tag as Polar.Measure;
            Polar.Measure newMeasure = oldMeasure.Clone() as Polar.Measure;
            oldMeasure._Status = new States.Bad();
            oldMeasure.CommentFromTsunami = string.Format(" Rflc: {0} => {1}", newMeasure.Distance.Reflector.ToString(), newReflector.ToString());
            newMeasure.Distance.Reflector = newReflector;

            CorrectDistanceAfterReflectorChanged(newMeasure as Polar.Measure);

            if (recompute)
                RecomputePoint(newMeasure);

            AddBadCloneBeforeGoodOne(measureFromView, oldMeasure, newMeasure: newMeasure);

            this.UpdateView();

        }

        internal void CreatePointFrom(Polar.Measure measureTheodolite)
        {
            if (this.StationModule is TSU.Polar.Station.Module stm)
            {
                stm.CreatePointFromMeasure(measureTheodolite);
            }
        }

        private void RecomputePoint(Measure m)
        {
            try
            {
                if (m._Point != m._OriginalPoint)
                {
                    // createing a bad clone of the old point
                    Elements.Point old = m._Point;
                    m._Point = m._Point.Clone() as Elements.Point;
                    old.State = Elements.Element.States.Bad;
                    Station.Module sm = GetBestStationModule(m);
                    Polar.Station.Module stm = sm as Polar.Station.Module;
                    Polar.Station.Module.DetermineFullPointLancé(stm.StationTheodolite, stm, m as Polar.Measure, true);
                    sm.SavePoint(m._Point);

                    new MessageInput(MessageType.GoodNews, $"'{m._Point._Name}' point recomputed and previous turn to bad state").Show();
                }
            }
            catch (Exception ex)
            {
                new MessageInput(MessageType.Critical, $"'{m._Point._Name}' point couldn't be recompute;{ex.Message}").Show();
            }
        }

        private void CorrectDistanceAfterReflectorChanged(Polar.Measure m)
        {
            var na = Tsunami2.Preferences.Values.na;
            Instruments.TotalStation i = (GetBestStationModule(m)._InstrumentManager.SelectedInstrument as Instruments.TotalStation);
            if (i != null)
            {

                bool df1 = m.DistanceF1 != null && m.DistanceF1.Raw.Value != na;
                bool df2 = m.DistanceF2 != null && m.DistanceF2.Raw.Value != na;
                if (df1) i.DoDistanceCorrections(m.DistanceF1);
                if (df2) i.DoDistanceCorrections(m.DistanceF2);
                if (df1 && df2)
                    (GetBestStationModule(m)._InstrumentManager.SelectedInstrumentModule as Instruments.PolarModule).DoAverageOftheTwoFaces(ref m);
                else
                    i.DoDistanceCorrections(m.Distance);
            }
        }

        internal void ModifyComment(Measure m, string comment)
        {
            if (m.Tag != null)
                if (m.Tag is Measure)
                    m = m.Tag as Polar.Measure;
            m.CommentFromUser = comment;

            this.UpdateView();
        }



        internal void Compare(List<Measure> l)
        {
            List<Measure> diffs = Analysis.Theodolite.Compare(l, 10, 10, TSU.Tsunami2.Preferences.Values.Tolerances.Alignment_Vertical_mm);

            Manager mm = new Manager(this);
            mm.AllElements.AddRange(diffs);
            mm.View.UpdateView();
            mm.View.ShowInMessageTsu("Measure differences", R.T_OK, null);
        }

        internal List<Measure> Select(string titleAndMessage = "", string buttonValid = "", string buttonCancel = "")
        {
            titleAndMessage = (titleAndMessage == "") ? titleAndMessage = $"{R.T_SELECT};{R.T_SELECT_MEASURES}" : titleAndMessage;
            buttonValid = (buttonValid == "") ? buttonValid = R.T_VALIDATE : buttonValid;
            buttonCancel = (buttonCancel == "") ? buttonCancel = R.T_CANCEL : buttonCancel;
            this.MultiSelection = true;
            this.View.CheckBoxesVisible = true;

            // Cancel
            if (this.View.ShowInMessageTsu(titleAndMessage, buttonValid, null, buttonCancel) == buttonCancel) return null;


            return this._SelectedObjects.Cast<Measure>().ToList();
        }

        /// <summary>
        /// Returns the StationModule from the Measure if possible,
        /// if not returns the stationModule from the parent Module,
        /// if not returns null
        /// </summary>
        /// <param name="m">measure</param>
        private Station.Module GetBestStationModule(Measure m)
        {
            if (m.SendingStationModule != null)
                return m.SendingStationModule;
            return this.StationModule;
        }
    }



    public delegate void MeasurementEventHandler(object sender, MeasurementEventArgs e);

    public class MeasurementEventArgs : EventArgs
    {
        public I.Module InstrumentModule;
        public Measure BeingMeasured;
        public Measure ToBeMeasured;
        public Measure JustMeasured;
        public Measure Measure;
        public bool Goto;

        public MeasurementEventArgs()
        {

        }

        public MeasurementEventArgs(I.Module InstrumentModule)
        {
            FillEventArgs(InstrumentModule);
        }

        public MeasurementEventArgs(Measures.Measure measure)
        {
            Measure = measure;
            BeingMeasured = measure;
        }

        private void FillEventArgs(I.Module InstrumentModule)
        {
            this.InstrumentModule = InstrumentModule;
            this.BeingMeasured = InstrumentModule.BeingMeasured;
            this.ToBeMeasured = InstrumentModule.ToBeMeasuredData;
            this.JustMeasured = InstrumentModule.JustMeasuredData;
        }
    }
}
