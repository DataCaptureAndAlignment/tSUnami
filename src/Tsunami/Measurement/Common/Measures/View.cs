﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TSU.Common.Instruments.Reflector;
using TSU.Tools.Conversions;
using TSU.Views;
using TSU.Views.Message;
using static TSU.Common.Instruments.Device.AT40x.Module;
using M = TSU;
using R = TSU.Properties.Resources;
using V = TSU.Common.Strategies.Views;

namespace TSU.Common.Measures
{
    public partial class View : ManagerView
    {
        TreeView treeView;
        public new Manager Module
        {
            get
            {
                return this._Module as Manager;
            }

        }
        MessageModuleView _MeasureSelectionView;
        public MM_Buttons buttons;
        public View(M.IModule module) : base(module)
        {
            InitializeComponent();
            this.ApplyThemeColors();
            this.Image = R.Measurement;
            InitializeMenu();

            this.AutoCheckChildren = true;

            this.currentStrategy.Update();
            this.currentStrategy.Show();

            this.AdaptTopPanelToButtonHeight();

        }
        private void ApplyThemeColors()
        {

        }
        private void AddBottomTreeview(Control destinationControl)
        {
            treeView = new TreeView() { Dock = DockStyle.Fill, ItemHeight = 16 };

            ImageList myImageList = new ImageList();
            myImageList.ImageSize = new Size(16, 16);
            myImageList.Images.Add(R.Element_Point);       // 0
            myImageList.Images.Add(R.Element_CS);       // 1

            // Assign the ImageList to the TreeView.
            treeView.ImageList = myImageList;

            destinationControl.Controls.Add(treeView);
        }
        // Menu


        internal override void InitializeMenu()
        {
            //Creation of buttons
            buttons = new MM_Buttons(this);
            base.InitializeMenu();
            bigbuttonTop.ChangeNameAndDescription(R.T_B_MeasureMain);
            bigbuttonTop.ChangeImage(R.Measurement);

        }



        internal override void InitializeStrategy()
        {
            this.AvailableViewStrategies = new List<V.Types>() { V.Types.List, V.Types.Tree };
            this.SetStrategy(M.Tsunami2.Preferences.Values.GuiPrefs.MeasureViewType);
        }

        internal override void SavePreference(V.Types viewStrategyType)
        {
            TSU.Tsunami2.Preferences.Values.GuiPrefs.MeasureViewType = viewStrategyType;
            base.SavePreference(viewStrategyType);
        }

        private void SetContextMenuToGlobal()
        {
            contextButtons.Clear();

            contextButtons.Add(buttons.status);

            contextButtons.Add(buttons.compare);
            contextButtons.Add(buttons.CorrectExtension);
            contextButtons.Add(buttons.CorrectComment);
            if (Module.AllElements.Count > 0)
                if (Module.AllElements[0] is Polar.Measure)
                    contextButtons.Add(buttons.CorrectReflector);

            if (this.Module.StationModule is TSU.Polar.Station.Module stm)
                if (stm.stationParameters._IsSetup == true)
                {
                    contextButtons.Add(buttons.CreatePointFromMeasure);
                }

            // contextButtons.Add(buttons.save);
            contextButtons.Add(buttons.export);
            // contextButtons.Add(buttons.Reduction);
        }

        internal override void ShowContextMenuGlobal()
        {
            this.SetContextMenuToGlobal();
            this.ShowPopUpMenu(contextButtons);
        }

        private void CloseMenuStrip(object sender, EventArgs e)
        {
            (sender as Control).Hide();
        }
        // Actions
        private void SaveMeasureInXml()
        {
            this.Module.SaveToXml();
        }

        private void ChangeMeasureStatus()
        {
            this.ShowPopUpSubMenu(new List<Control>() { buttons.statusGood, buttons.statusQuestionnable, buttons.statusBad, buttons.statusControl }, "status");
        }

        private void Export()
        {
            TsuPopUpMenu m = new TsuPopUpMenu(this, "Export ");
            List<Control> controls = new List<Control>();
            controls.Add(buttons.lgc2);
            controls.Add(buttons.geode);
            m.ShowSubMenu(controls, "export");
        }


        private void ChangeMeasureStatusToQuestionnable()
        {
            Manager.ChangeMeasureStatus(this, currentStrategy.GetSelectedElements(), new States.Questionnable());
        }
        private void ChangeMeasureStatusToControl()
        {
            Manager.ChangeMeasureStatus(this, currentStrategy.GetSelectedElements(), new States.Control());
        }
        private void ChangeMeasureStatusToBad()
        {
            Manager.ChangeMeasureStatus(this, currentStrategy.GetSelectedElements(), new States.Bad());
        }
        private void ChangeMeasureStatusToGood()
        {
            Manager.ChangeMeasureStatus(this, currentStrategy.GetSelectedElements(), new States.Good());
        }
        private void ChangeMeasureStatusToPrevious()
        {
            Manager.ChangeMeasureStatus(this, currentStrategy.GetSelectedElements(), null);
        }

        public void ValidateSelection()
        {

        }

        // Update
        public override void UpdateView()
        {
            if (this.Module.isMainManager)
                this.Module.ShowAllMeasuresIncludedInParentModule();
            this.currentStrategy.Update();
        }
        public void UpdateView(TreeNode tn)
        {
            UpdateTreeView(tn);
        }
        public void UpdateView(IMeasure m)
        {
            //PopulateTreeView(m, treeView);
            PopulateCustomTreeView(m, treeView);
        }
        private void PopulateCustomTreeView(IMeasure m, TreeView treeView)
        {

        }
        public virtual void UpdateTreeView(TreeNode node)
        {
            //this.treeView.Nodes[1].Remove();
            //this.treeView.Nodes.Insert(1, this._MeasureModule._ParentModule.GetNextPointNode());
            //this.treeView.Nodes[1].Expand();
        }
        public override List<string> ListviewColumnHeaderTexts
        {

            get
            {
                List<string> l = new List<string>()
                {
                    "Point",
                    "Status",
                    "Date",
                    "Face",
                    "Hz[gon]",
                    "V[gon]",
                    "Dist[m]",
                    "Dhor[m]",
                    "Ext[mm]",
                    "Rflctr",
                    "Ref/Cala",
                    "Comment",
                    "Station",
                    "Hum[%]",
                    "DryTemp[°C]",
                    "Press.[mBar]"
                };
                return l;
            }
            set
            {

            }
        }
        public class MM_Buttons
        {
            private View View;

            public List<BigButton> all;
            public BigButton save;
            public BigButton compare;
            public BigButton geode;
            public BigButton plgc;
            public BigButton lgc1;
            public BigButton lgc2;
            public BigButton Reduction;
            public BigButton CorrectExtension;
            public BigButton CorrectComment;
            public BigButton CorrectReflector;
            public BigButton csv;
            public BigButton export;
            public BigButton status;
            public BigButton statusGood;
            public BigButton statusPrevious;
            public BigButton statusUnknown;
            public BigButton statusControl;
            public BigButton statusQuestionnable;
            public BigButton statusBad;
            public BigButton CreatePointFromMeasure;
            public BigButton main;

            public MM_Buttons(View view)
            {
                all = new List<BigButton>();
                View = view;
                main = new BigButton(R.T_B_MeasureMain, R.Measurement, View.ShowContextMenuGlobal); all.Add(main);
                save = new BigButton(R.T_B_MeasureSave, R.Save, View.SaveMeasureInXml); all.Add(save);
                compare = compare = new BigButton(R.T287, R.Instruments, View.Compare); all.Add(compare);
                geode = new BigButton(R.T_B_MeasureExport2Geode, R.Geode, () => View.Module.ExportToGeode()); all.Add(geode);
                plgc = new BigButton(R.T_B_MeasureExport2Plgc, R.PLGC, View.Module.ExportToPLGC); all.Add(plgc);
                lgc1 = new BigButton(R.T_B_MeasureExport2LGC1, R.LGC1, View.Module.ExportToLGC1); all.Add(lgc1);
                lgc2 = new BigButton(R.T_B_MeasureExportLGC2, R.Lgc, View.Module.ExportToLGC2); all.Add(lgc2);
                Reduction = new BigButton(R.T_B_MeasureDoReduction, R.Export, View.Module.DoReduction); all.Add(Reduction);
                export = new BigButton(R.T_B_MeasureExport, R.Export, View.Export); all.Add(export);

                csv = new BigButton(R.T_B_MeasureExport2Csv, R.Export, View.Module.Export2Csv); all.Add(csv);
                status = new BigButton(R.T_B_MeasureChangeStatus, R.Status, View.ChangeMeasureStatus, hasSubButtons:true); all.Add(status);

                statusPrevious = new BigButton(R.buttonStatusGood, R.StatusGood, View.ChangeMeasureStatusToPrevious); all.Add(statusPrevious);
                statusGood = new BigButton(R.buttonStatusGood, R.StatusGood, View.ChangeMeasureStatusToGood); all.Add(statusGood);
                statusControl = new BigButton(R.buttonStatusControl, R.StatusControl, View.ChangeMeasureStatusToControl); all.Add(statusControl);
                statusQuestionnable = new BigButton(R.buttonStatusQuestionnable, R.StatusQuestionnable, View.ChangeMeasureStatusToQuestionnable); all.Add(statusQuestionnable);
                statusUnknown = new BigButton(R.buttonStatusControl, R.StatusQuestionnable, View.ChangeMeasureStatusToQuestionnable); all.Add(statusUnknown);
                statusBad = new BigButton(R.butttonStatusBad, R.StatusBad, View.ChangeMeasureStatusToBad); all.Add(statusBad);
                CorrectExtension = new BigButton(R.T_B_CORRECT_EXTENSION, R.Rallonge, View.ModifyExtension); all.Add(CorrectExtension);
                CorrectReflector = new BigButton(R.T_B_CORRECT_REFLECTOR, R.Reflector, View.ModifyReflector); all.Add(CorrectReflector);
                CorrectComment = new BigButton(R.StringTilt_Comment, R.Comment, View.ModifyComment); all.Add(CorrectComment);

                CreatePointFromMeasure = new BigButton($"{R.T_CREATE_A_POINT_FROM_THIS_MEASURE};{R.T_USE_THE_PARENT_STATION_MODULE_TO_COMPUTE_THE_POINT_COORDINATES_FROM_THE_OBSERVATIONS}", R.Element_Point, View.CreatePointFromMeasure); all.Add(CreatePointFromMeasure);
            }

            internal void Dispose()
            {
                View = null;
                for (int i = 0; i < this.all.Count; i++)
                {
                    if (all[i] != null)
                        all[i].Dispose();
                }
                for (int i = 0; i < this.all.Count; i++)
                {
                    all[i] = null;
                }
                all.Clear();
                all = null;
            }

        }

        private void CreatePointFromMeasure()
        {
            if (CheckThatOnlyOneMeasureIsSelected(out Measure meas))
            {
                this.Module.CreatePointFrom(meas as Polar.Measure);
            }

        }

        private bool CheckThatOnlyOneMeasureIsSelected(out Measure usedMeasure)
        {
            List<Measure> usedMeasures = GetSelectedMeasures();

            if (usedMeasures.Count == 1)
            {
                usedMeasure = usedMeasures[0];
                return true;
            }
            else
            {
                new MessageInput(MessageType.Critical, R.T_YOU_MUST_SELECT_ONE_MEASURE_AT_A_TIME).Show();
                usedMeasure = null;
                return false;
            }
        }

        private List<Measure> GetSelectedMeasures()
        {
            return currentStrategy.GetSelectedElements().OfType<Measure>().ToHashSet().ToList();
        }

        private void ModifyComment()
        {
            if (!CheckThatOnlyOneMeasureIsSelected(out Measure usedMeasure)) 
                return;

            // Ask
            string cancel = R.T_CANCEL;
            string titleAndMessage = $"{R.T_WHAT_IS_YOUR_COMMENT};{R.T_PLEASE_INSERT_YOUR_COMMENT_BELOW}";
            List<string> buttonTexts = new List<string> { R.T_OK, cancel };

            MessageTsu.ShowMessageWithTextBox(titleAndMessage, buttonTexts, out string buttonClicked, out string textInput, usedMeasure.CommentFromUser, true);

            if (buttonClicked == cancel)
                return;

            // Do
            this.Module.ModifyComment(usedMeasure, textInput);
        }

        internal void ModifyExtension()
        {
            if (!CheckThatOnlyOneMeasureIsSelected(out Measure usedMeasure)) return;

            bool recompute = false;
            if (usedMeasure.SendingStationModule is TSU.Polar.Station.Module psm)
            { 
                recompute = psm.StationTheodolite.Parameters2._IsSetup;
            }
            else if (this.Module.StationModule is TSU.Polar.Station.Module psm2)
            {
                recompute = psm2.StationTheodolite.Parameters2._IsSetup;
            }

            // Ask
            string cancel = R.T_CANCEL;
            string titleAndMessage = string.Format(R.T_ENTER_EXTENSION_M, usedMeasure.Extension);
            List<string> buttonTexts = new List<string> { R.T_OK, cancel };

            MessageTsu.ShowMessageWithTextBox(titleAndMessage, buttonTexts, out string buttonClicked, out string textInput, usedMeasure.Extension.Value.ToString());

            if (buttonClicked == cancel)
                return;

            // Do
            if (Double.TryParse(textInput, out double newExtension))
            {
                this.Module.ModifyExtension(usedMeasure, new DoubleValue(newExtension, 0), recompute);
            }
            else
            {
                new MessageInput(MessageType.Critical, R.StringLevel_WrongCellValue).Show();
            }
        }



        internal void ModifyReflector()
        {
            Measure usedMeasure = null;

            bool recompute = false;
            if (this.Module.StationModule is TSU.Polar.Station.Module)
            {
                recompute = (this.Module.StationModule as TSU.Polar.Station.Module).StationTheodolite.Parameters2._IsSetup;
            }

            if (!CheckThatOnlyOneMeasureIsSelected(out usedMeasure)) return;

            string cancel = R.T_CANCEL;

            // special case for at because we dont do correction in Tsunami, but at do the correction itself
            if (Analysis.Measure.FindStationIn(Tsunami2.Properties, usedMeasure, out List<Station> stations))
            {
                foreach (Common.Station station in stations)
                {
                    if (station.ParametersBasic._Instrument._Model.Contains("AT4"))
                    {
                        string titleAndMessage = $"{R.T_NOT_AVAILABLE_FOR_AT40X};{R.T_THE_AT40X_IS_CORRECTING_THE_DISTANCES_BY_ITSELF_SO_TSUNAMI_CANNOT_DO_IT}";
                        new MessageInput(MessageType.Warning, titleAndMessage).Show();
                        return;
                    }
                }
            }

            // ask with warning if we really do it
            MessageInput mi = new MessageInput(MessageType.Choice, R.T_B_CORRECT_REFLECTOR)
            {
                ButtonTexts = new List<string> { "Change", cancel }
            };
            if (mi.Show().TextOfButtonClicked != cancel)
            {
                // TODO
                // get the reflector manager from stationmodule not nice
                Reflector r = null;
                if (this.Module.StationModule != null)
                {
                    r = (this.Module.StationModule as TSU.Polar.Station.Module)
                        .reflectorManager.SelectInstrument(R.T153, selectables: null, multiSelection: false, preselected: null) as Reflector;
                }
                else
                {
                    r = TSU.Polar.Station.Module.CreateReflectorManager(null).SelectInstrument(R.T153, selectables: null, multiSelection: false, preselected: null) as Reflector;
                }
                if (r != null)
                {
                    this.Module.ModifyReflector(usedMeasure as Polar.Measure, r, recompute);
                    UpdateView();
                }
            }

        }

        internal void Compare()
        {
            List<Measure> l = GetSelectedMeasures();
            if (l.Count < 2) throw new Exception(R.T303);
            this.Module.Compare(l);
        }
    }
}
