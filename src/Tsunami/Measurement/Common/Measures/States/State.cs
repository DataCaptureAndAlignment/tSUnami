using System;
using System.Collections.Generic;
using System.Text;

using System.Xml;
using System.Xml.Serialization;

namespace TSU.Common.Measures
{
	public interface IMeasureState :  ICloneable
	{
        String _Name { get; set; }
        States.Types Type { get; set; }
	}

    [XmlInclude(typeof(States.Unknown))]
    [XmlInclude(typeof(States.Temporary))]
    [XmlInclude(typeof(States.Good))]
    [XmlInclude(typeof(States.Control))]
    [XmlInclude(typeof(States.Failed))]
    [XmlInclude(typeof(States.Bad))]
    [XmlInclude(typeof(States.Questionnable))]
    [XmlInclude(typeof(States.Cancel))]

    [Serializable]
    [XmlType(Namespace = "Measures", TypeName = "State")]
    public class State : IMeasureState
    {
        public static State GetStateByName(string name)
        {
            switch (name)
            {
                case "Unknown": return new Measures.States.Unknown();
                case "Temporary": return new Measures.States.Temporary();
                case "Good": return new Measures.States.Good();
                case "Control": return new Measures.States.Control();
                case "Failed": return new Measures.States.Failed();
                case "Bad": return new Measures.States.Bad();
                case "Questionnable": return new Measures.States.Questionnable();
                case "Cancel": return new Measures.States.Cancel();
                default:
                    return new Measures.States.Unknown();
            }
        }

        public States.Types Type
        { get; set; }
        public States.Types PreviousType { get; set; } 
        public String _Name { get; set; }

        public State()
        {
            Type = States.Types.Unknown;
            _Name = "Unknow";
        }

        public State(States.Types previousStateType):this()
        {
            PreviousType = previousStateType;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public State GetPreviousState()
        {
            switch (this.PreviousType)
            {
                case States.Types.Good:
                    return new States.Good(this.Type);
                case States.Types.Bad:
                    return new States.Good(this.Type);
                case States.Types.Questionnable:
                    return new States.Questionnable(this.Type);
                case States.Types.Control:
                    return new States.Control(this.Type);
                case States.Types.Temporary:
                    return new States.Temporary(this.Type);
                case States.Types.Cancel:
                    return new States.Cancel(this.Type);
                case States.Types.Replace:
                case States.Types.ControlFromOtherRound:
                case States.Types.GoodFromOtherRound:
                case States.Types.Previous:
                case States.Types.Failed:
                case States.Types.Unknown:
                default:
                    return new State(this.Type);
            }
        }
        
    }
    
}
