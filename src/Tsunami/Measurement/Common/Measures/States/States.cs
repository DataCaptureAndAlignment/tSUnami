﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using M = TSU.Common.Measures;
using System.Xml.Serialization;
using R = TSU.Properties.Resources;

namespace TSU.Common.Measures.States
{
    [Serializable]

    [XmlType(Namespace = "Measures.States", TypeName = "Good")]
    public class Good : State
    {
        public Good()
        {
            Type = Types.Good;
            _Name = "Good";
        }
        public Good(Types previousStateType) : this()
        {

            PreviousType = previousStateType;
        }

    }


    [Serializable]
    [XmlType(Namespace = "Measures.States", TypeName = "Control")]
    public class Control : State
    {
        public Control()
        {
            Type = Types.Control;
            _Name = "Control";
        }

        public Control(Types previousStateType) : this()
        {
            PreviousType = previousStateType;
        }
    }



    [Serializable]
    [XmlType(Namespace = "Measures.States", TypeName = "Failed")]
    public class Failed : State
    {
        public Failed()
        {
            Type = Types.Failed;
            _Name = "Failed";
        }
    }

    [Serializable]
    [XmlType(Namespace = "Measures.States", TypeName = "Replaced")]
    public class Replaced : State
    {
        public Replaced()
        {
            Type = Types.Replace;
            _Name = "Failed";
        }
        public Replaced(Types previousStateType) : this()
        {
            PreviousType = previousStateType;
        }
    }

    [Serializable]
    [XmlType(Namespace = "Measures.States", TypeName = "Bad")]
    public class Bad : State
    {
        public Bad()
        {
            Type = Types.Bad;
            _Name = "Bad";
        }
        public Bad(Types previousStateType) : this()
        {
            PreviousType = previousStateType;
        }
    }

    [Serializable]
    [XmlType(Namespace = "Measures.States", TypeName = "Unknown")]
    public class Unknown : State
    {
        public Unknown()
        {
            Type = Types.Unknown;
            _Name = R.String_Unknown;
        }
    }

    [Serializable]
    [XmlType(Namespace = "Measures.States", TypeName = "Questionnable")]
    public class Questionnable : State
    {
        public Questionnable()
        {
            Type = Types.Questionnable;
            _Name = "Questionnable";
        }
        public Questionnable(Types previousStateType) : this()
        {
            PreviousType = previousStateType;
        }
    }

    [Serializable]
    [XmlType(Namespace = "Measures.States", TypeName = "Temporary")]
    public class Temporary : State
    {
        public Temporary()
        {
            Type = Types.Temporary;
            _Name = "Temporary";
        }
        public Temporary(Types previousStateType) : this()
        {
            PreviousType = previousStateType;
        }
    }

    [Serializable]
    [XmlType(Namespace = "Measures.States", TypeName = "Cancel")]
    public class Cancel : State
    {
        public Cancel()
        {
            Type = Types.Cancel;
            _Name = R.T_CANCEL;
        }
        public Cancel(Types previousStateType) : this()
        {
            PreviousType = previousStateType;
        }
    }

    [Flags]
    [XmlType(Namespace = "Measures.States", TypeName = "Types")]
    public enum Types
    {
        Good = 1,
        Failed = 2,
        Bad = 4,
        Unknown = 8,
        Questionnable = 16,
        Temporary = 32,
        Cancel = 64,
        Replace = 128,
        Control = 256, ControlFromOtherRound = 512, GoodFromOtherRound = 1024,
        Previous = 2048
    }
}
