﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Threading.Tasks;
using TSU.ENUM;
using R = TSU.Properties.Resources;


namespace TSU.Common.Measures
{
    [Serializable]
    public class Extension : DoubleValue
    {
        public override string ToString()
        {
            string s;
            int d =TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
            switch (TSU.Tsunami2.Preferences.Values.GuiPrefs.UnitForExtension)
            {
                case TSU.Preferences.Preferences.DistanceUnit.mm:
                    d -= 3;
                    if (d < 0) d = 0;
                    s = this.ToString(d, 1000) + " mm";
                    break;
                case TSU.Preferences.Preferences.DistanceUnit.m:
                default:
                    s = this.ToString(d) + " m";
                    break;
            }
            return s;
        }
    }
}
