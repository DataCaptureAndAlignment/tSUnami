using System;
using R = TSU.Properties.Resources;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using TSU.Common.Elements;
using TSU.IO.SUSoft;
using System.Net.Sockets;
using MathNet.Numerics.Statistics;
using System.CodeDom;
using TSU.Common.Instruments.Device;

namespace TSU.Common.Measures
{


    public interface IMeasure : ITsuObject, ICloneable
    {
        [XmlAttribute]
        DateTime _Date { get; set; }
        [XmlAttribute]
        string _PointName { get; }// usefull for a simplier serialisation
        Point _Point { get; set; }
        [XmlAttribute]
        Point _OriginalPoint { get; set; }
        State _Status { get; set; }
        DoubleValue Extension { get; set; }
        bool isQuickMeasure { get; set; }
        string Origin { get; set; }
        [XmlAttribute]
        string Comment { get; }

    }

    [XmlInclude(typeof(Polar.Measure))]
    [XmlInclude(typeof(MeasureOfDistance))]
    [XmlInclude(typeof(Tilt.Measure))]
    [XmlInclude(typeof(MeasureOfLevel))]
    [XmlInclude(typeof(MeasureOfOffset))]
    [XmlInclude(typeof(Tilt.Measure))]
    [Serializable]
    [XmlType(Namespace = "TSU", TypeName = "Measure")]
    public class Measure : TsuObject, IMeasure, IEquatable<Measure>
    {
        // properties
        [XmlAttribute]
        public virtual DateTime _Date { get; set; }
        public virtual State _Status { get; set; }

        [XmlAttribute]
        public bool IsFake { get; set; } = false;

        public virtual DoubleValue Extension { get; set; }

        public Interfaces Interfaces { get; set; } = new Interfaces();

        public Geode.Roles GeodeRole { get; set; }

        [XmlIgnore]
        public bool IsGood
        {
            get
            {
                return this._Status is States.Good;
            }
            set
            {
                if (value is false) return;
                States.Types previous = this._Status.Type;
                this._Status = new States.Good(previous);
            }
        }

        [XmlIgnore]
        public bool IsControl
        {
            get
            {
                return this._Status is States.Control;
            }
            set
            {
                if (value is false) return;
                States.Types previous = this._Status.Type;
                this._Status = new States.Control(previous);
            }
        }

        public bool IsObsolete { get; set; } = false;

        public bool IsLiveData { get; set; } = false;

        [XmlAttribute]
        public virtual string Comment
        {
            get
            {
                string comment = "";

                // from User
                if (CommentFromUser != "")
                {
                    if (CommentFromUser != R.T_NO_COMMENT)
                    {
                        comment += $"user: '{CommentFromUser}'";
                        if (CommentFromTsunami != "")
                            comment += $", ";
                    }
                }

                // from TSU
                string TSU = CommentFromTsunami;
                if (enableCommentWithStatus)
                {
                    if (CommentFromTsunami != "")
                        TSU += $", ";
                    TSU += this._Status._Name + " " + CommentPrecisionFromTsunami;
                }
                if (TSU != "")
                    comment += $"TSU: '{TSU}'";

                return comment;
            }
        }
        public string CommentFromUser { get; set; }
        public string CommentFromTsunami { get; set; }
        public string CommentPrecisionFromTsunami { get; set; }

        public bool isQuickMeasure { get; set; } = false;
        public bool enableCommentWithStatus { get; set; } = true;

        [XmlAttribute]
        private string pointName;
        public virtual string _PointName
        {
            get
            {
                if (_Point != null && _Point._Name != R.String_Unknown)
                {
                    pointName = _Point._Name;
                    return pointName;
                }
                else
                {
                    if (pointName == "" || pointName == null)
                        return R.String_Unknown;
                    else
                        return pointName;
                }
            }
        }

        [XmlIgnore]
        public Common.Station.Module SendingStationModule;
        [XmlIgnore]
        public double na = TSU.Tsunami2.Preferences.Values.na;
        //[XmlIgnore] 
        //private Point point;
        //private Guid pointGUID;
        //public Guid PointGUID
        //{
        //    get
        //    {
        //        if (pointGUID != Guid.Empty)
        //            return pointGUID;

        //        if (point != null)
        //            pointGUID = point.Id;

        [XmlAttribute]
        public Guid PointGUID;

        // removed: PW says it is not a problem to go thorhought the full list of point each time,
        // If needed we can switch it to a hash list based on the GUID
        // the problem with the private field is a point is created in a constructor before deserailiszation the private field is already set

        // private Point point;

        [XmlIgnore]
        public virtual Point _Point   //virtual for mockup
        {
            get
            {
                return Point.GetGlobalPointByGuid(guid: PointGUID);
            }
            set
            {
                if (this._Point != null)
                    TSU.Debug.WriteInConsole($"._Point replacement in {this._Name}: from \r\n{this._Point._Name} by \r\n{value._Name}");
                Point.SetAsGlobalPoint(value, guid: ref PointGUID);
            }
        }

        [XmlAttribute]
        public Guid OriginalPointGUID;
        [XmlIgnore]
        public virtual Point _OriginalPoint
        {
            get
            {
                return Point.GetGlobalPointByGuid(guid: OriginalPointGUID);
            }
            set
            {
                //if (this._OriginalPoint != null)
                //    TSU.Debug.WriteInConsole($"._OriginalPoint replacement in {this._Name}: from \r\n{this._OriginalPoint._Name} by \r\n{value._Name}");
                Point.SetAsGlobalPoint(value, guid: ref OriginalPointGUID);
            }
        }

        [XmlIgnore]
        public virtual Point _OriginalPointModifiedInH { get; set; } //virtual for mockup

        [XmlAttribute]
        public virtual string _InstrumentSN { get; set; } // usefull for a simplier serialisation

        [XmlAttribute]
        public virtual string Origin { get; set; }
        //  Methods
        public Measure()
        {
            //point = new Point(); removed because when openign TSU the point shoudl be null so taht we know we have to link it with the pint in elements list of Tsunami
            _Date = DateTime.Now;
            _Status = new States.Unknown();
            CommentFromTsunami = String.Empty;
            CommentFromUser = String.Empty;
        }

        public override string ToString()
        {
            if (this._PointName != "Unknown")
            {
                string comment = (this.Comment == "") ? "" : "with the comment '" + this.Comment + "'";
                //   return string.Format("Measurement of '{0}' (state {3}), taken on '{1}' {2}", this._PointName, this._Date, comment, this._Status._Name);
                return $"{R.T_MEASUREMENT_OF} '{this._PointName}'({R.T_STATE} {this._Status._Name}), {R.T_TAKEN_ON} '{this._Date}' {comment}";
            }
            else
                return "";
        }

        public virtual string ToString_List_Diff_Poin(bool returnHeader = false)
        {
            return "NotImplementedException for your type Offsets Measure";
        }

        public override object Clone()
        {
            Measure clone = (Measure)base.Clone(); // to make a clone of the primitive types fields and get a GUID
            clone._Date = this._Date; // Not clonable in the framework[KnownType(typeof(D))] 
            if (this._Point != null)
                clone._Point = (Point)this._Point.DeepCopy();
            //clone._OriginalPoint = (Point)this._OriginalPoint.DeepCopy();
            //clone._OriginalPointModifiedInH = (Point)this._OriginalPointModifiedInH.DeepCopy();

            clone._Status = this._Status.Clone() as State;

            return clone;
        }
        // IEquatable
        public virtual bool Equals(Measure other)
        {
            return
               (this as TsuObject).Equals(other) &&
               this._Date == other._Date &&
               this._PointName == other._PointName;
        }
        public override bool Equals(object other)
        {
            if (other is Measure)
                return this.Equals((Measure)other);
            else
                return false;
        }

        // based on station point, measure number and point param will choose the default Geode Role
        public static Geode.Roles GetDefaultGeodeRole(Polar.Measure m, Polar.Station st)
        {
            if (m._Status.Type == States.Types.Control)
                return Geode.Roles.Unused;

            // setup done
            if (st.ParametersBasic._IsSetup)
            {
                return Geode.Roles.Poin;
            }

            // taking references

            // coordinate available
            if (m._OriginalPoint != null && m._OriginalPoint._Coordinates != null && m._OriginalPoint._Coordinates.HasAny)
            {
                // new points should not be used as cala, but point
                if (m._OriginalPoint.State == Element.States.Approximate)
                    return Geode.Roles.Poin;

                // bearing available
                if (m._OriginalPoint._Parameters.hasGisement)
                {
                    if (st.Parameters2.Setups.InitialValues.IsPositionKnown)
                        return Geode.Roles.Radi;
                    else
                    {
                        foreach (Polar.Measure item in st.MeasuresTaken)
                        {
                            if (item._Status.Type == States.Types.Good)
                                if (item.GeodeRole == Geode.Roles.Cala)
                                    return Geode.Roles.Radi;

                        }
                    }
                }

                return Geode.Roles.Cala;
            }

            return Geode.Roles.Poin;
        }

        public override int GetHashCode()
        {
            return
                base.GetHashCode() ^
                _Date.GetHashCode();
            //_PointName.GetHashCode();
        }

        // IXmlSerializable implementation
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public bool IsActive()
        {
            if (this._Status.GetType() == typeof(States.Bad))
                return false;
            if (this._Status.GetType() == typeof(States.Failed))
                return false;
            return true;
        }

        internal virtual string ToString(string p)
        {
            throw new NotImplementedException();

        }
    }

    public static class MeasureExtensions
    {
        public static Measure FindLastByPointNameAndType<T>(this T src, string socketName, Type type) where T : List<Measure>
        {
            for (int i = src.Count - 1; i >= 0; i--)
            {
                var measure = src[i];
                if (measure.GetType() == type)
                {
                    if (measure._Point._Name == socketName)
                        return (measure);
                }
            }
            return null;
        }
        public static Measure FindLastByAssemblyNameAndType<T>(this T src, string socketName, Type type) where T : List<Measure>
        {
            foreach (var measure in src)
            {
                if (measure.GetType() == type)
                {
                    if (measure._Point._Name.Contains(socketName))
                        return (measure);
                }
            }
            return null;
        }
    }
}
