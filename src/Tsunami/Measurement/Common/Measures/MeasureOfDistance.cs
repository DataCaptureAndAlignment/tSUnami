using System;
using System.Collections.Generic;
using System.Text;
using R = TSU.Properties.Resources;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using TSU.ENUM;
using TSU.Common.Instruments.Device;
using TSU.Tools;
using TSU.Common.Instruments.Reflector;


namespace TSU.Common.Measures
{
    public interface IMeasureOfDistanceMeter : IMeasure
    {
        MeasureOfDistance Distance { get; set; }
    }

    public class MeasureOfDistanceMeter : Measure, IMeasureOfDistanceMeter
    {
        public MeasureOfDistance Distance { get; set; }
    }

    public interface IMeasureOfDistance : ICloneable
    {
        ENUM.DistanceMeasurementMode MeasurementMode { get; set; }

        Reflector Reflector { get; set; }
        WeatherConditions WeatherConditions { get; set; }
        bool IsCorrectedForWeatherConditions { get; set; }
        bool IsCorrectedForPrismConstanteValue { get; set; } //Constante is included in calibration
        bool IsCorrectedForEtalonnage { get; set; }
        bool IsDistanceOutOfRange { get; set; }
        DoubleValue Raw { get; set; }
        DoubleValue Corrected { get; set; }
        
    }

    [Serializable]
	public class MeasureOfDistance : Measure, IMeasureOfDistance
	{
        private DoubleValue raw;
        public virtual DoubleValue Raw 
        {
            get
            {
                return raw;
            }
            set
            {
                corrected = new DoubleValue();
                raw = value;
            }
        }
        internal DoubleValue corrected;

        /// <summary>
        /// Value corrected with calibration and / or weather corrections
        /// </summary>
        public virtual DoubleValue Corrected
        {
            get
            {
                if (corrected.Value ==TSU.Tsunami2.Preferences.Values.na)
                {
                    corrected.Value = raw.Value;
                    corrected.Sigma = raw.Sigma;
                }
                return corrected;
            }
            set
            {
                corrected = value;
            }
        }

        [XmlIgnore]
        internal DoubleValue distTheoHoriz;
        [XmlIgnore]
        internal DoubleValue distMesHoriz;
        [XmlIgnore]
        internal DoubleValue distAvgMove;
        [XmlIgnore]
        internal DoubleValue distOffsetDiffTheoMeas;
        [XmlAttribute]
        public virtual ENUM.DistanceMeasurementMode MeasurementMode { get; set; }
        [XmlAttribute]
        public virtual bool IsCorrectedForWeatherConditions { get; set; }
        [XmlAttribute]
        public virtual bool IsCorrectedForPrismConstanteValue { get; set; } //Constante is included in calibration
        [XmlAttribute]
        public virtual bool IsCorrectedForEtalonnage { get; set; }
        
        [XmlAttribute]
        public virtual bool IsDistanceOutOfRange { get; set; }

        public virtual string ReflectorID { get; set; }
        private Reflector reflector;
        [XmlIgnore]
        public virtual Reflector Reflector
        {
            get
            {
                if (reflector != null)
                {
                    if (!(reflector._Name == R.String_Unknown && ReflectorID != R.String_Unknown ))
                        return reflector;
                }

                
                if (ReflectorID != "" || ReflectorID != null)
                    return TSU.Tsunami2.Preferences.Values.Instruments.Find(x => x._Name == ReflectorID) as Reflector;

                return null;
            }
            set
            {
                if (value != null) {
                    reflector = value;
                    ReflectorID = value._Name;
                }
            }
        }
        public virtual WeatherConditions WeatherConditions { get; set; }
        // methods
        public MeasureOfDistance()
        {
            Raw = new DoubleValue();
            Corrected = new DoubleValue();
            Reflector = new Reflector();
            WeatherConditions = new WeatherConditions();
            IsCorrectedForWeatherConditions = false;
            IsCorrectedForEtalonnage = false;
            IsDistanceOutOfRange = false;
            IsCorrectedForPrismConstanteValue = false;
            distTheoHoriz = new DoubleValue();
            distMesHoriz = new DoubleValue();
            distAvgMove = new DoubleValue();
            distOffsetDiffTheoMeas = new DoubleValue();
        }

        public override string ToString()
        {
            return "Length " + base.ToString();
        }
        public override object Clone()
        {
            MeasureOfDistance newMeasure = (MeasureOfDistance)this.MemberwiseClone();
            newMeasure.Raw = (DoubleValue)Raw.Clone();
            newMeasure.Corrected = (DoubleValue)Corrected.Clone();
            if (Reflector!= null)
                newMeasure.Reflector = (Reflector)Reflector.Clone();
            newMeasure.WeatherConditions = (WeatherConditions)WeatherConditions.Clone();
            return newMeasure;
        }
        public virtual bool IsFullyCorrected()
        {
            return IsCorrectedForWeatherConditions && IsCorrectedForPrismConstanteValue && IsCorrectedForEtalonnage;
        }

        public MeasureOfDistance DeepCopy()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;

                return (MeasureOfDistance)formatter.Deserialize(stream);
            }
        }
        /// <summary>
        /// Say that the distance is not corrected
        /// </summary>
        internal void Clean()
        {
            this.IsCorrectedForEtalonnage = false;
            this.IsCorrectedForPrismConstanteValue = false;
            this.IsCorrectedForWeatherConditions = false;
            this.IsDistanceOutOfRange = false;
        }
        /// <summary>
        /// Say that the distance is corrected (for laser tracker)
        /// </summary>
        internal void UnClean()
        {
            this.IsCorrectedForEtalonnage = true;
            this.IsCorrectedForPrismConstanteValue = true;
            this.IsCorrectedForWeatherConditions = true;
            this.IsDistanceOutOfRange = true;
        }
    }
}
