using System;
using System.Collections.Generic;
using System.Text;
using R = TSU.Properties.Resources;
using System.Xml;
using System.Xml.Serialization;


namespace TSU.Common.Measures
{

    public interface IWeatherConditions
    {
        double dryTemperature { get; set; } // Celsius
        double WetTemperature { get; set; } // Celsius
        double pressure { get; set; } // mBar
        int humidity { get; set; } // %
    }
    [Serializable]
    public class WeatherConditions : IWeatherConditions
    {
        [XmlAttribute]
        public virtual double dryTemperature { get; set; } = 20; // Celsius
        [XmlAttribute]
        public virtual double WetTemperature { get; set; } =TSU.Tsunami2.Preferences.Values.na; // Celsius
        [XmlAttribute]
        public virtual double pressure { get; set; } = 1013; // mBar
        [XmlAttribute]
        public virtual int humidity { get; set; } = 60; // %

        public WeatherConditions()
        {

        }

        public object Clone()
        {
            return this.MemberwiseClone(); // make a clone of the object, work sif there is no other tye than primitive tyes.
        }

    }
}
