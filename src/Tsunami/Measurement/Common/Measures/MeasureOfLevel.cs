using System;
using System.Collections.Generic;
using System.Text;using R = TSU.Properties.Resources;
using System.IO;
using TSU.Common.Instruments;
using TSU.Common.Instruments.Device;
using TSU.Common.Compute.Compensations.Strategies;
using TSU.Common.Compute.Compensations.BeamOffsets;

namespace TSU.Common.Measures
{
	[Serializable]
    public class MeasureOfLevel : Measure, IEquatable<MeasureOfLevel>, IComparable<MeasureOfLevel>
	{
        public override string _Name
        {
            get
            {
                if (this.name != "Unknown")
                    return this.name;
                string name = "";
                if (this._Point != null)
                    name = this._Point._Name;
                else
                    name = this._PointName;
                return $"Lm of {name}";
            }
        }
        public double _Extension { get; set; }
        public double _RawLevelReading { get; set; }
        public double _CorrLevelReadingForZero { get; set; }
        public double _CorrLevelReadingForEtalonnage { get; set; }
        public double _RawEmqLevelReading { get; set; }
        public double _Distance { get; set; }
        public double _EmqDistanceReading { get; set; }
        public double _TheoReading { get; set; }
        public double _Residual { get; set; }
        public double _EcartAller { get; set; }
        public double _ResidualAller { get; set; }
        public double _EcartRetour { get; set; }
        public double _ResidualRetour { get; set; }
        public double _Compass { get;
            set; }
        public double _Hmes { get; set; }
        public bool _ContinuousMeasurement { get; set; }

        public override string ToString()
        {
            return "Level " + base.ToString();
        }
        public LevelingStaff _staff { get; set; }

        public MeasureOfLevel()
        {
           //9999 indique qu'il n'y a pas de valeur dans la variable
            _RawLevelReading = na;
            _RawEmqLevelReading = na;
            _CorrLevelReadingForEtalonnage = na;
            _CorrLevelReadingForZero = na;
            _Extension = 0;
            _Distance = na;
            _EmqDistanceReading = na;
            _Hmes = na;
            _TheoReading = na;
            _Residual = na;
            _EcartAller = na;
            _ResidualAller = na;
            _EcartRetour = na;
            _ResidualRetour = na;
            _Compass = na;
            _ContinuousMeasurement = false;
            _Date = DateTime.MinValue;
            _staff = new LevelingStaff();
            enableCommentWithStatus = false;
        }
        public MeasureOfLevel DeepCopy()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                MeasureOfLevel measOfLevelCopy = new MeasureOfLevel();
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                measOfLevelCopy = (MeasureOfLevel)formatter.Deserialize(stream);
                measOfLevelCopy._staff = this._staff?.DeepCopy();
                return measOfLevelCopy;
            }
        }

        internal void SelectDefaultExtension()
        {
            
            if (this._Point.SocketCode == null) this._Point.SocketCode = new Elements.SocketCode() { Id = "0" };
            this._Extension = this._Point.SocketCode.DefaultExtensionForLevelingMeasurement;
            if(this._Point.SocketCode.Id=="1")
            {
                this._Extension = 0.07;
            }
            if (this._Point.SocketCode.Id == "16")
            {
                this._Extension = -0.07;
            }
        }
        // Default comparer for Management.Measure of level to sort the list.
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            MeasureOfLevel objAsMeas = obj as MeasureOfLevel;
            if (objAsMeas == null) return false;
            else return Equals(objAsMeas);
        }
        public int CompareTo (object obj)
        {
            if (obj == null) return 1;
            MeasureOfLevel objAsMeas = obj as MeasureOfLevel;
            if (objAsMeas == null) return 1;
            else return CompareTo(objAsMeas);
        }
        public int CompareTo(MeasureOfLevel compareMeas)
        {
            // A null value means that this object is greater.
            if (compareMeas == null)
                return 1;

            else
                return this._Point._Parameters.Cumul.CompareTo(compareMeas._Point._Parameters.Cumul);
        }
        public override int GetHashCode()
        {
            return this._Point._Parameters.Cumul.GetHashCode();
        }
        public bool Equals(MeasureOfLevel other)
        {
            if (other == null) return false;
            return (this._PointName==other._PointName);
        }
    }
    public static class LevelMeasureExtensions
    {
        public static bool ContainsWithSamePointName<T>(this T src, MeasureOfLevel measure) where T : List<MeasureOfLevel>
        {
            return src.Find(x => x._Point._Name.ToUpper() == measure._Point._Name.ToUpper()) != null;
        }
        public static bool ContainsWithSamePointName2<T>(this T src, MeasureOfLevel measure) where T : List<MeasureWithStatus>
        {
            List < MeasureOfLevel > list = new List < MeasureOfLevel >();
            foreach ( var item in src)
            {
                list.Add(item.Measure as MeasureOfLevel);
            }
            return list.ContainsWithSamePointName(measure);
        }
    }
}
