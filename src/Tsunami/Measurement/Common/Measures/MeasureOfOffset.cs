using System;
using System.Collections.Generic;
using System.IO;
using TSU.Common.Elements;

namespace TSU.Common.Measures
{
    [Serializable]
    public class MeasureOfOffset : Measure, ICloneable, IEquatable<MeasureOfOffset>, IComparable<MeasureOfOffset>
    {
        public override string _Name
        {
            get
            {
                if (this.name != "Unknown")
                    return this.name;
                string name;
                if (this._Point != null)
                    name = this._Point._Name;
                else
                    name = this._PointName;
                return $"Om of {name}";
            }
        }
        public double _RawReading { get; set; }
        public double _CorrectedReading { get; set; }
        public double _ReductedReading { get; set; }
        public double _TheoreticalReading { get; set; }
        public double _TheoreticalDeviation { get; set; }
        public double _AverageDeviation { get; set; }
        public double _AverageTheoreticalReading { get; set; }

        public bool _IsFakeRawReading = false;

        /// <summary>
        /// When the measure is the average of several readings, this list contains the different readings
        /// </summary>
        public List<SubReading> SubReadings { get; set; }

        /// <summary>
        /// When the measure is the average of several readings, we compute the standard deviation (sigma)
        /// </summary>
        public double _StandardDeviation { get; set; }

        [Serializable]
        public class SubReading
        {
            public double Reading { get; set; }
            public bool Use { get; set; }

            public SubReading()
            {
                Reading = double.NaN;
                Use = true;
            }

            public SubReading(double reading, bool use)
            {
                Reading = reading;
                Use = use;
            }
        }

        public MeasureOfOffset()
        {
            this._Point = new Point();
            this._RawReading = na;
            this._CorrectedReading = na;
            this._ReductedReading = na;
            this._TheoreticalReading = na;
            this._TheoreticalDeviation = na;
            this._AverageDeviation = na;
            this._AverageTheoreticalReading = na;
            enableCommentWithStatus = false;
        }

        public override string ToString()
        {
            return "Ecarto " + base.ToString();
        }

        public override object Clone()
        {
            MeasureOfOffset newAbstractElement = (MeasureOfOffset)this.MemberwiseClone(); // to make a clone of the primitive types fields
            newAbstractElement._Point.DeepCopy();
            //newAbstractElement._Point = (this._Point != null) ? (TSU.Tool.CloneableList<TSU.Module.Element.Point>)this._Point.Clone() : null;
            return newAbstractElement;
        }

        public MeasureOfOffset DeepCopy()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                MeasureOfOffset m = (MeasureOfOffset)formatter.Deserialize(stream);
                m._Point.StorageStatus = Tsunami.StorageStatusTypes.Keep; // Tsunami2.Properties.Points.AddIfDoNotExistWithSameGUID(m._Point);
                return m;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is MeasureOfOffset objAsMeas)
                return Equals(objAsMeas);
            else
                return false;
        }

        public bool Equals(MeasureOfOffset other)
        {
            if (other == null) return false;
            return (this._PointName == other._PointName);
        }

        public override int GetHashCode()
        {
            return this._PointName.GetHashCode();
        }

        public int CompareTo(object obj)
        {
            if (obj is MeasureOfOffset objAsMeas)
                return CompareTo(objAsMeas);
            else
                return 1;
        }

        /// <summary>
        /// Default comparer for Measure of level to sort the list.
        /// </summary>
        /// <param name="compareMeas"></param>
        /// <returns></returns>
        public int CompareTo(MeasureOfOffset compareMeas)
        {
            // A null value means that this object is greater.
            if (compareMeas == null)
                return 1;
            else
                return this._Point._Parameters.Cumul.CompareTo(compareMeas._Point._Parameters.Cumul);
        }
    }
}
