﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TSU.ENUM;

namespace TSU.Common.Measures
{
    [XmlInclude(typeof(Round<Measure>))]
    [XmlInclude(typeof(TheodoliteRound))]
    public class Round<T> where T : Measure
    {
        public int Number { get; set; }

        public Round()
        {
            Measures = new List<T>();
            State = States.Types.Unknown ;
        }

        public Round(List<T> measures)
        {
            Measures = measures;
            State = Measures.All(m => m._Status is States.Bad) ? States.Types.Bad : States.Types.Good;
        }

        public Round(IEnumerable<T> measures)
        {
            Measures = new List<T>(measures);
            State = Measures.All(m => m._Status is States.Bad) ? States.Types.Bad : States.Types.Good;
        }
        
        public List<T> Measures { get; }
        public List<T> Spreads { get; set; }

        public States.Types State { get;  set; }

        public string Name {
            get
            {
                if (Measures.Count > 0)
                    return Measures[0].Origin;
                else
                    return "No Measure";
            }
        }
        
        public override string ToString()
        {
            return string.Format("{0} round of {1} measures called {2}", State, Measures.Count, Name);
        }
    }

    


    [XmlInclude(typeof(TheodoliteRounds))]
    public class Rounds<T, U> : List<T> where T : Round<U> where U : Measure
    { 
        public Rounds() { }
    }

    
}
