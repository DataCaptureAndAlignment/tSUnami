using System;
using System.Collections.Generic;
using System.Text;
using R = TSU.Properties.Resources;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using TSU.Common.Instruments;
using M = TSU.Common.Measures;
using TSU.Common.Elements;
using TSU.Common.Compute;

namespace TSU.Common.Measures
{
    public interface IMeasureOfAngles : ICloneable
    {
        Angles Raw { get; set; }
        Compensator Compensator { get; set; }
        Angles Corrected { get; set; }
        Result CorrectForCollimation();
    }

    [XmlRoot("XmlDocRoot")]
    [Serializable]
    public class MeasureOfAngles : IMeasureOfAngles
    {
        public Angles raw;
        // Setting in the raw value is erasing automaticaly
        [XmlIgnore]
        public virtual Angles Raw
        {
            get
            {
                return raw;
            }
            set
            {
                corrected = new Angles();
                raw = value;
            }
        }
        [XmlElement("Compensator Inclinaison")]
        public virtual Compensator Compensator { get; set; }

        public Angles corrected;
        [XmlIgnore]
        public virtual Angles Corrected
        {
            get
            {
                if (corrected.Horizontal.Value ==TSU.Tsunami2.Preferences.Values.na)
                {
                    corrected = this.Raw.Clone() as Angles;
                    return corrected;
                }
                else
                    return corrected;
            }
            set
            {
                corrected = value;
            }
        }

        public MeasureOfAngles()
        {
            Raw = new Angles();
            Compensator = new Compensator();
            Corrected = new Angles();
        }

        public virtual Result CorrectForCollimation()
        {
            throw new NotImplementedException();
        }


        public M.States.Types HorizontalState { get; set; } = M.States.Types.Good;
        public M.States.Types VerticalState { get; set; } = M.States.Types.Good;

        public virtual object Clone()
        {
            MeasureOfAngles newMeasure; // call clone from measure
            newMeasure = (MeasureOfAngles)this.MemberwiseClone(); // to make a clone of the primitive types fields
            newMeasure.Raw = (Angles)Raw.Clone();
            newMeasure.Corrected = (Angles)Corrected.Clone();
            newMeasure.Compensator = (Compensator)Compensator.Clone();

            return newMeasure;
        }

        public override int GetHashCode()
        {
            int correctedAngle = this.Corrected.GetHashCode();
            return correctedAngle;
        }

        public override string ToString()
        {
            return string.Format("Corrected {0}, Raw {1}", this.Corrected, this.Raw);
        }

        internal MeasureOfAngles GetOppositeFace()
        {
            MeasureOfAngles m = new MeasureOfAngles();
            m.raw = Survey.TransformToOppositeFace(this.raw);
            m.corrected = Survey.TransformToOppositeFace(this.corrected);
            return m;
        }
    }
}
