﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using I = TSU.Common.Instruments;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TSU.ENUM;
using TSU.Common.Compute;

namespace TSU.Common.Measures
{

    [XmlType(Namespace = "TSU.Measure.Theodolite", TypeName = "Round")]
    public class TheodoliteRound : Round<Polar.Measure>
    {
        public TheodoliteRound() : base() { }


        public TheodoliteRound(List<Polar.Measure> measures) : base(measures)
        {
        }

        public TheodoliteRound(IEnumerable<Polar.Measure> measures) : base(measures)
        {
        }

        internal Polar.Measure OpeningMeasure
        {
            get
            {
                    return GoodMeasures.FirstOrDefault();
            }
        }
        internal Polar.Measure OpeningMeasureTransformedToFace1
        {
            get
            {
                return GoodMeasuresTransformedToFace1.FirstOrDefault();
            }
        }

        internal Polar.Measure ControlMeasure
        {
            get
            {
                return MeasuresOfControl.LastOrDefault(x => x._Status is States.Control );
            }
        }

        internal Polar.Measure ControlMeasureTransformedToFace1
        {
            get
            {
                return MeasuresOfControlTransformedToFace1.LastOrDefault(x => x._Status is States.Control);
            }
        }

        internal List<string> PointNames
        {
            get
            {
                List<string> l = new List<string>();
                foreach (Polar.Measure item in this.GoodMeasures)
                {
                    if (!(l.Contains(item._Point._Name))) l.Add(item._Point._Name);
                }
                return l;
            }
        }

       

        /// <summary>
        /// Are always given only the good and temporary measure
        /// </summary>
        internal List<Polar.Measure> GoodMeasures
        {
            get
            {
                List<Polar.Measure> goods = new List<Polar.Measure>();
                goods.AddRange(this.Measures.FindAll(x => (x._Status is States.Good)  || (x._Status is States.Temporary)));
                return goods;
            }
        }

        /// <summary>
        /// return a new list of clones transformed in face1
        /// </summary>
        internal List<Polar.Measure> GoodMeasuresTransformedToFace1
        {
            get
            {
                List<Polar.Measure> goods = new List<Polar.Measure>();
                foreach (var item in GoodMeasures)
                {
                    Polar.Measure clone = item.GetNiceClone();
                    if (this.Face == I.FaceType.Face2)
                    {
                        clone.Angles = Survey.TransformToOppositeFace(clone.Angles);
                    }
                    goods.Add(clone);
                }
                return goods;
            }
        }

        internal List<Polar.Measure> MeasuresOfControl
        {
            get
            {
                List<Polar.Measure> goods = new List<Polar.Measure>();
                goods.AddRange(this.Measures.FindAll(x => (x._Status is States.Control) ));
                return goods;
            }
        }

        internal List<Polar.Measure> MeasuresOfControlTransformedToFace1
        {
            get
            {
                List<Polar.Measure> goods = new List<Polar.Measure>();
                foreach (var item in MeasuresOfControl)
                {
                    Polar.Measure clone = item.GetNiceClone();
                    if (this.Face == I.FaceType.Face2)
                    {
                        clone.Angles = Survey.TransformToOppositeFace(clone.Angles);
                    }
                    goods.Add(clone);
                }
                return goods;

            }
        }

        internal List<Polar.Measure> ReductedMeasures
        {
            get
            {
                try
                {
                    //   if (ControlMeasure == null) throw new Exception("No closure found");
                    if (ControlMeasure == null) throw new Exception(R.T_NO_CLOSURE_FOUND);

                    Polar.Measure clone;
                    double opening = this.OpeningMeasureTransformedToFace1.Angles.Corrected.Horizontal.Value;
                    double openingRaw = this.OpeningMeasureTransformedToFace1.Angles.Corrected.Horizontal.Value;
                    double closure = Closure;
                    double goodMeasureCount = GoodMeasures.Count;

                    List<Polar.Measure> ReductedMeasures = new List<Polar.Measure>();

                    int i = 0;
                    foreach (var item in GoodMeasuresTransformedToFace1)
                    {
                        clone = item.GetNiceClone();
                        
                        // Change Comments
                        {
                            clone.CommentFromTsunami = String.Format("Round {0} ({1})", this.Number, clone.Face.ToString());
                        }

                        clone.Angles.Corrected.Horizontal.Value += (closure * i / goodMeasureCount);
                        clone.Angles.Corrected.Horizontal.Value -= opening;

                        if (clone._Point._Name != this.OpeningMeasure._Point._Name) // to avoid to have a '400.0000' if the opening is measured twice
                            clone.Angles.Corrected.Horizontal.Value = Survey.Modulo400(clone.Angles.Corrected.Horizontal.Value);

                        clone.Angles.Raw.Horizontal.Value += (closure * i / goodMeasureCount);
                        clone.Angles.Raw.Horizontal.Value -= openingRaw;
                        if (clone._Point._Name != this.OpeningMeasure._Point._Name) // to avoid to have a '400.0000' if the opening is measured twice
                            clone.Angles.Raw.Horizontal.Value = Survey.Modulo400(clone.Angles.Raw.Horizontal.Value);
                        
                        ReductedMeasures.Add(clone);
                        i++;
                    }

                    clone = this.ControlMeasureTransformedToFace1.GetNiceClone();
                    clone.Angles.Corrected.Horizontal.Value -= opening;

                    clone.Angles.Raw.Horizontal.Value -= openingRaw;


                    ReductedMeasures.Add(clone);

                    return ReductedMeasures;
                }
                catch (Exception ex)
                {
                    throw new Exception($"{R.T_IMPOSSIBLE_TO_REDUCE_THE_MEASURE_OF} '{this.Name}'\r\n{ex.Message}", ex);
                }
            } 
        }


       

        internal double Closure
        {
            get
            {
                return ControlMeasure.Angles.Corrected.Horizontal.Value - OpeningMeasure.Angles.Corrected.Horizontal.Value;
            }
        }

        internal I.FaceType Face
        {
            get
            {
                if (Measures.Count == 0) return I.FaceType.UnknownFace;
                I.FaceType first = Measures[0].Face;
                foreach (var item in Measures)
                {
                    if (item.Face != first) return I.FaceType.UnknownFace;
                }
                var a = Measures.FindAll(x => (x._Name == ""));
                return first;
            }
        }
        public override string ToString()
        {
            return base.ToString() + string.Format("({0})", Face);
        }
    }


    public class TheodoliteRounds : Rounds<TheodoliteRound, Polar.Measure>
    {
        public TheodoliteRounds() { }

        public TheodoliteRound LastValidRound
        {
            get
            {
                return GoodOnes.LastOrDefault();
            }
        }

        public List<TheodoliteRound> OfFace2
        {
            get
            { 
                return GoodOnes.FindAll(x => x.Face == I.FaceType.Face2);
            } 
        }

        public List<TheodoliteRound> OfFace1
        {
            get
            {
                return GoodOnes.FindAll(x => x.Face == I.FaceType.Face1);
            }
        }

        public List<Polar.Measure> AverageOfFace1
        {
            get
            {
                try
                {
                    if (OfFace1.Count > 0)
                        return GetAverage(OfFace1);
                    else
                        return null;
                }
                catch (Exception ex)
                {
                    throw new Exception($"{R.T_IMPOSSIBLE_AVERAGE_THE_FACE_1_ROUNDS}\r\n{ex.Message}", ex);
                }
            }
        }

        public List<Polar.Measure> AverageOfFace2
        {
            get
            {
                try
                {
                    if (OfFace2.Count > 0)
                        return GetAverage(OfFace2);
                    else
                        return null;
                }
                catch (Exception ex )
                {
                    throw new Exception($"{R.T_IMPOSSIBLE_AVERAGE_THE_FACE_2_ROUNDS}\r\n{ex.Message}", ex);
                }
            }
        }

        private List<Polar.Measure> GetAverage(List<TheodoliteRound> roundsToAverage)
        {
            try
            {
                List<Polar.Measure> averages = new List<Polar.Measure>();
                List<Polar.Measure> l;

                foreach (var name in this.CommonPointNames)
                {
                    l = new List<Polar.Measure>();

                    foreach (var goodRound in roundsToAverage)
                        l.AddRange(goodRound.ReductedMeasures.FindAll(x => (x._Point._Name == name) && (x._Status is States.Good)));
                    
                        averages.Add(Survey.DoAverage(l));
                }

                l = new List<Polar.Measure>();

                foreach (var goodRound in roundsToAverage)
                    l.Add(goodRound.ReductedMeasures.Last(x=>x._Status is States.Control));

                averages.Add(Survey.DoAverage(l));

                return averages;
            }
            catch (Exception ex )
            {
                throw new Exception($"{R.T_IMPOSSIBLE_AVERAGE_SOME_ROUNDS}\r\n{ex.Message}", ex);
            }
        }

        internal List<string> CommonPointNames
        {
            get
            {
                if (this.GoodOnes.Count == 0) return new List<string>();

                var CommonList = this.GoodOnes[0].PointNames;
                foreach (TheodoliteRound item in this.GoodOnes)
                {
                    CommonList = item.PointNames.Intersect(CommonList).ToList() ;
                }
                return CommonList;
            }
        }

        // return the average and fill the spread to the average in each rounds
        public List<Polar.Measure> Average
        {
            get
            {
                try
                {
                    List<Polar.Measure> average = new List<Polar.Measure>();
                    List<Polar.Measure> af1 = AverageOfFace1;
                    List<Polar.Measure> af2 = AverageOfFace2;

                    if (af1 != null && af2 != null)
                    {
                        // to avoid to compute everytime lets store it temporary
                        List<Polar.Measure> l;

                        foreach (var name in this.CommonPointNames)
                        {
                            l = new List<Polar.Measure>();

                            l.AddRange(af1.FindAll(x => (x._Point._Name == name) && (x._Status is States.Good)));
                            l.AddRange(af2.FindAll(x => (x._Point._Name == name) && (x._Status is States.Good)));

                            average.Add(Survey.DoAverage(l));
                        }

                        l = new List<Polar.Measure>();
                        l.Add(af1.Last());
                        l.Add(af2.Last());
                        average.Add(Survey.DoAverage(l));

                        return average;
                    }
                    else if (af1 != null)
                    {
                        return af1;
                    }
                    else if (af2 != null)
                    {
                        return af2;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {

                    throw new Exception($"{R.T_CANNOT_AVERAGE_THE_MEASURES}\r\n{ex.Message}", ex);
                }
            }
        }

        internal void ComputeAndFillSpreadsToAverage(List<Polar.Measure> Averages)
        {
            foreach (var item in this)
            {
                item.Spreads = new List<Polar.Measure>();
                foreach (var m in item.Measures)
                {
                    Polar.Measure spread = new Polar.Measure();
                    Polar.Measure average = Averages.Find(x => x._PointName == m._PointName);
                    spread._Point = m._Point;
                    spread._Status = average._Status;
                    spread.Angles.Corrected.Horizontal.Value = m.Angles.Corrected.Horizontal.Value - average.Angles.Corrected.Horizontal.Value;
                    spread.Angles.Corrected.Vertical.Value = m.Angles.Corrected.Vertical.Value - average.Angles.Corrected.Vertical.Value;
                    var doubleValue = new DoubleValue(m.Distance.Corrected.Value - average.Distance.Corrected.Value);
                    spread.Distance.Corrected = doubleValue;
                    item.Spreads.Add(spread);
                }
            }
        }

        internal List<TheodoliteRound> GoodOnes
        {
            get
            {
                List<TheodoliteRound> goods = new List<TheodoliteRound>();
                goods.AddRange(this.FindAll(x => (x.State != States.Types.Bad) ));
                return goods;
            }
        }

        internal TheodoliteRounds BadOnes
        {
            get
            {
                TheodoliteRounds bads = new TheodoliteRounds();
                bads.AddRange(this.FindAll(x => (x.State == States.Types.Bad)));
                return bads;
            }
        }
        
        internal bool EqualNumberOfFace
        {
            get
            {
                return OfFace1.Count  == OfFace2.Count;
            }
        }

        public override string ToString()
        {
            string s = "";
            int i = 1;
            this.ForEach(x => { s += string.Format("{0} {1}: {2}\r\n", "Round", i, x.ToString()); i++; });
            return s;
        }
        

        public  string ToString(bool colored)
        {
            string s = "";
            int i = 1;
            this.ForEach(x => { s += string.Format("{0} {1}: {2}\r\n", "Round", i, (x.State== States.Types.Bad)? "$red$" + x.ToString() + "$end$" : "$green$" + x.ToString() + "$end$"); i++; });
            return s;
        }
    }

}
