﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using TSU.Common.Compute.Compensations;
using TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using TSU.Common.Instruments.Adapter;
using TSU.Common.Instruments.Device;
using TSU.Logs;
using TSU.Preferences;
using TSU.Views;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using F = TSU.Functionalities;
using M = TSU;
using R = TSU.Properties.Resources;
using T = TSU.Tools;
using Z = TSU.Common.Zones;

namespace TSU.Common
{
    public class Tsunami : Module
    {
        public enum VersionTypes
        {
            Normal,
            Beta,
            Alpha,
            Gamma
        }

        [XmlAttribute("DisplayType")]
        public TSU.Polar.GuidedModules.Steps.StakeOut.DisplayTypes ValueToDisplayType { get; set; } = TSU.Polar.GuidedModules.Steps.StakeOut.DisplayTypes.Displacement;

        public List<DsaFlag> SerializedDsaFlag;

        [XmlIgnore]
        public List<DsaFlag> DsaFlag
        {
            get
            {
                if (SerializedDsaFlag == null || SerializedDsaFlag.Count == 0)
                    SerializedDsaFlag = Polar.Module.DsaFlags_CreatesForAdvancedModule();
                return SerializedDsaFlag;
            }
        }

        public List<DsaFlag> SerializedDsaFlagForAdvancedModule;

        [XmlIgnore]
        public List<DsaFlag> DsaFlagForAdvancedModule
        {
            get
            {
                if (SerializedDsaFlagForAdvancedModule == null || SerializedDsaFlagForAdvancedModule.Count == 0)
                    SerializedDsaFlagForAdvancedModule = Polar.Module.DsaFlags_CreatesForAdvancedModule();
                return SerializedDsaFlagForAdvancedModule;
            }
        }
        public List<DsaFlag> SerializedDsaFlagForGuidedModule;

        [XmlIgnore]
        public List<DsaFlag> DsaFlagForGuidedModule
        {
            get
            {
                if (SerializedDsaFlagForGuidedModule == null || SerializedDsaFlagForGuidedModule.Count == 0)
                    SerializedDsaFlagForGuidedModule = Polar.Module.DsaFlags_CreatesForAdvancedModule();
                return SerializedDsaFlagForGuidedModule;
            }
        }
        private CoordinateSystems coordinatesSystems;
        public CoordinateSystems CoordinatesSystems
        {
            get
            {
                if (coordinatesSystems == null)
                {
                    coordinatesSystems = new CoordinateSystems();
                    coordinatesSystems.AllTypes = CoordinateSystem.GetNativeSystems();

                }
                return coordinatesSystems;
            }
            set
            {
                coordinatesSystems = value;
            }
        }

        [XmlAttribute]
        public Preferences.Preferences.SupportedLanguages Language { get; set; } = Preferences.Preferences.SupportedLanguages.english;
        [XmlAttribute]
        public int tempNumberOfLoadedItem = 0;

        // 
        //Tsunami is Module with child module 
        //
        [XmlIgnore]
        public new TsunamiView View
        {
            get
            {
                if (_TsuView != null)
                    return _TsuView as TsunamiView;
                else
                    return new TsunamiView(new Tsunami());
            }
            set
            {
                _TsuView = value;
            }
        }
        [XmlIgnore]
        public F.Menu Menu
        {
            get
            {
                var menu = childModules.SingleOrDefault(x => x.GetType() == typeof(F.Menu));
                if (menu != null)
                    return menu as F.Menu;
                else
                    return null;
            }
            set
            {
                if (!childModules.Any(x => x.GetType() == typeof(F.Menu)))
                    Add(value);
                else
                    throw new Exception(R.T457 + _Name);
            }
        }

        private T.Gadgets.Module gadgets;

        [XmlIgnore]
        internal T.Gadgets.Module Gadgets
        {
            get
            {
                if (gadgets == null)
                {
                    gadgets = new TSU.Tools.Gadgets.Module(Tsunami2.Properties);
                    Log.AddEntryAsFYI(this, "Gadget Module Creation");
                }
                return gadgets;
            }
            set
            {
                gadgets = value;
            }
        }

        [XmlAttribute]
        public string Version { get; set; }

        [XmlIgnore]
        public bool IsBeta
        {
            get
            {
                return Version.ToUpper().Contains("B");
            }
        }

        [XmlIgnore]
        public bool IsAlpha
        {
            get
            {
                return Version.ToUpper().Contains("A");
            }
        }

        [XmlAttribute]
        public string Date { get; set; }


        private List<TsuObject> elements;

        [XmlIgnore]
        public List<TsuObject> Elements
        {
            get
            {
                var points = Points;
                if (elements == null && points != null)
                {
                    elements = CompositeElement.CreateTreeBasedOnList(points);
                    elements.AddRange(CompositeElements);
                }
                return elements;
            }
            set
            {
                elements = value;
            }
        }

        /// <summary>
        /// Used to define if the point should be stored in tsunami.Points, .TempPoints or .HiddenPoint
        /// The hidden ones will not be visible in the element manager but will be accessible via Measure.Point
        /// The temp ones will possibly be cleaned or not stored in the tsu.
        /// </summary>
        public enum StorageStatusTypes
        {
            /// <summary>
            /// will be stored as 'Kept' if not existing in other categories
            /// </summary>
            MustBeDefined,

            /// <summary>
            /// will be serialized and visible in the element manager by the user
            /// </summary>
            Keep,

            /// <summary>
            /// will be serialized but not visible in the element manager by the user
            /// </summary>
            Hidden,

            /// <summary>
            /// will NOT be serialized but not visible in the element manager by the user
            /// </summary>
            Temp,

            /// <summary>
            /// is not stored at all
            /// </summary>
            NotStored,

            /// <summary>
            /// will be stored as 'Kept' if not existing in other categories
            /// </summary>
            Any,
        }

        public List<CompositeElement> CompositeElements { get; set; } = new List<CompositeElement>();

        public IEnumerable<TsuObject> FlattenedCompositeElements
        {
            get
            {
                foreach (var item in CompositeElements)
                {
                    yield return item;
                    foreach (var v in item.GetAllElements())
                        yield return v;
                }
            }
        }

        [XmlIgnore]
        public List<E.Point> TempPoints { get; set; } = new List<E.Point>();

        public List<E.Point> Points { get; set; } = new List<E.Point>();

        public List<E.Point> HiddenPoints { get; set; } = new List<E.Point>();


        /// <summary>
        /// will store the point in different lists based on the storage status
        /// </summary>
        /// <param name="point"></param>
        /// <exception cref="NotImplementedException"></exception>
        internal void Store(Point point, StorageStatusTypes status)
        {
            var info = $"Store '{point._Origin}'-'{point._Name}' with status: '{status}'";

            if (status == StorageStatusTypes.MustBeDefined)
            {
                new MessageInput(MessageType.Bug, $"Trying to {info}");
            }

            var keepPoints = Points;
            var hiddenPoints = HiddenPoints;
            var tempPoints = TempPoints;
            switch (status)
            {
                case StorageStatusTypes.Hidden:
                    keepPoints.RemoveByGuid(point.Guid);
                    tempPoints.RemoveByGuid(point.Guid);
                    hiddenPoints.AddIfDoNotExistWithSameGUID(point);
                    break;
                case StorageStatusTypes.NotStored:
                case StorageStatusTypes.Temp:
                    if (keepPoints.Contains(point.Guid)) // oops, we want to beable to cahnge storage strategy, but there is a lot of workflow that try to put to temp something previously kept, like polar controle opening that make the previous point created disappearing
                        return;
                    if (hiddenPoints.Contains(point.Guid))
                        return;
                    tempPoints.AddIfDoNotExistWithSameGUID(point);
                    break;
                case StorageStatusTypes.Any:
                    bool existInTemp = tempPoints.Any(x => x._Name == point._Name && x.Guid == point.Guid);
                    bool existInHidden = hiddenPoints.Any(x => x._Name == point._Name && x.Guid == point.Guid);
                    if (!existInTemp && !existInHidden)
                        keepPoints.AddIfDoNotExistWithSameGUID(point);
                    break;
                case StorageStatusTypes.Keep:
                default:
                    keepPoints.AddIfDoNotExistWithSameGUID(point);
                    tempPoints.RemoveByGuid(point.Guid);
                    hiddenPoints.RemoveByGuid(point.Guid);
                    break;
            }
        }

        internal StorageStatusTypes GetStorageStatus(Point point)
        {
            if (Points.Contains(point)) return StorageStatusTypes.Keep;
            if (HiddenPoints.Contains(point)) return StorageStatusTypes.Hidden;
            if (TempPoints.Contains(point)) return StorageStatusTypes.Temp;
            return StorageStatusTypes.NotStored;
        }
        internal List<Point> GetStoredPointById(Guid guid)
        {
            var points = Points;
            var hidden = HiddenPoints;
            var tempPoints = TempPoints;

            var founds = points.FindAll(x => x != null && x.Guid == guid);

            if (!founds.Any())
                founds = hidden.FindAll(x => x != null && x.Guid == guid);

            if (!founds.Any())
                founds = tempPoints.FindAll(x => x!=null && x.Guid == guid);
            return founds;
        }

        public List<FinalModule> MeasurementModules { get; set; }

        private Z.Zone zone { get; set; }
        public Z.Zone Zone
        {
            get
            {
                return zone;
            }
            set
            {
                zone = value;
                if (View != null)
                    View.UpdateTextTitle();
            }
        }
        [XmlAttribute("REF")]
        public Coordinates.ReferenceFrames ReferenceFrameOfTheMeasurement { get; set; }

        public TSU.Common.Compute.Compensations.BeamOffsets.Context BOC_Context { get; set; } = new TSU.Common.Compute.Compensations.BeamOffsets.Context();

        public List<Interfaces> KnownInterfacesAssembly { get; private set; } = new List<Interfaces>();

        [XmlIgnore]
        public bool IsRunningForFunctionalTest { get; set; } = false;

        [XmlIgnore]
        public Random Rnd { get; set; } = new Random(DateTime.Now.Millisecond);

        public Tsunami() { }
        public Tsunami(Module parentModule)
            : base(parentModule)
        {
            this.CoordinatesSystems = new CoordinateSystems();
            this.CoordinatesSystems.AllTypes = CoordinateSystem.GetNativeSystems();
        }

        // used to run test at start
        internal void Test_with_preferences()
        {
            PointNameReplacement pnr = new PointNameReplacement();
            TSU.IO.Xml.CreateFile(pnr, "replacements.xml");
            return;

            //Compute.NewFrameCreation.Creation.Do(new View());
            T.Gadgets.Module m = new T.Gadgets.Module(null);
            m.RebuildLGC2InputFromPointLancéFiles();

            if (System.Diagnostics.Debugger.IsAttached)
            {
                MessageInput mi = new MessageInput(MessageType.Choice, "verbose?")
                {
                    ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                };
                if (R.T_YES == mi.Show().TextOfButtonClicked)
                    M.Tsunami2.Preferences.Theme.Verbose = true;
            }
            return;
        }

        public override void Initialize()
        {
            base._Name = Globals.AppName;

            base.Initialize();
            MeasurementModules = new List<FinalModule>();

            TimeSpanForLogFileCreation = new TimeSpan();
        }

        public TimeSpan TimeSpanForLogFileCreation;

        public event EventHandler<ModuleEventArgs> SubModuleAdded;
        public event EventHandler<ModuleEventArgs> SubModuleAdding;






        public override void Add(Module module)
        {
            SubModuleAdding?.Invoke(this, new ModuleEventArgs(module));

            base.Add(module);
            if (module is FinalModule final) // those are also final modueles || module is M.Guided.Group.Module || module is M.Guided.Module)
            {
                MeasurementModules.Add(final);
                Menu._activeMeasurementModule = module;
                Menu.RefreshOpenedAndFinishedModule();
            }



            if (SubModuleAdded != null) SubModuleAdded(this, new ModuleEventArgs(module));

            // Write log
            Logs.Log.AddEntryAsFYI(this, "Module " + module._Name + " Started.");
        }
        public void Remove(Module module)
        {
            if (!(module is F.Menu))
            {
                childModules.Remove(module);
                module._TsuView.Hide();

                // refresh menu

                Menu.RefreshOpenedAndFinishedModule();

                // Write log
                Logs.Log.AddEntryAsFYI(this, "Module " + module._Name + " Removed.");
            }
        }

        public static void Load(Common.Tsunami incomingTsunami, bool saveSomeMemory = false)
        {
            try
            {
                Common.Tsunami currentTsunami = Tsunami2.Properties;

                if (incomingTsunami.Version == null)
                    incomingTsunami.Version = R.T_PRE_1_0_XX;

                currentTsunami.PathOfTheSavedFile = incomingTsunami.PathOfTheSavedFile;

                if (incomingTsunami.Version != currentTsunami.Version)
                {
                    string backUpPath = TSU.IO.Backup.Create(incomingTsunami.PathOfTheSavedFile, $"version {incomingTsunami.Version}", keepOriginalPlace: true);

                    //currentTsunami.PathOfTheSavedFile = System.IO.Path.GetDirectoryName(incomingTsunami.PathOfTheSavedFile) + "//" + System.IO.Path.GetFileNameWithoutExtension(incomingTsunami.PathOfTheSavedFile) + "(" + currentTsunami.Version + ")" + ".TSU";
                    string titleAndMessage = $"{R.T_DIFFERENT_VERSION};" + $"{R.T_THE_VERSION_OF} '{incomingTsunami.PathOfTheSavedFile}' {R.T_IS} '{incomingTsunami.Version}'\r\n" +
                                             $"{R.T_TSUNAMI_HAVE_CREATED_A_COPY_OF_THE_FILE_WITH_A_TEMPTATIVE_OF_OPENING} ('{backUpPath}')\r\n" +
                                             $"{R.T_THERE_IS_NO_WARRANTY_THAT_ALL_INFORMATION_WILL_PRESENT}\r\n" +
                                             $"{R.T_YOU_SHOULD_CONSIDER_TO_OPEN_THIS_FILE_WITH_THE_VERSION_THAT_CREATED_IT}.";
                    new MessageInput(MessageType.GoodNews, titleAndMessage).Show();
                }
                currentTsunami.View.UpdateTextTitle(); // was currentTsunami.View.AddOpenedFileNameInTitle(currentTsunami.pathOfTheSavedFile);
                currentTsunami.Zone = incomingTsunami.Zone;
                currentTsunami.SerializedDsaFlagForAdvancedModule = incomingTsunami.SerializedDsaFlagForAdvancedModule;
                currentTsunami.SerializedDsaFlagForGuidedModule = incomingTsunami.SerializedDsaFlagForGuidedModule;
                currentTsunami.BOC_Context = incomingTsunami.BOC_Context;

                currentTsunami.Zone = incomingTsunami.Zone;
                currentTsunami.CoordinatesSystems = incomingTsunami.CoordinatesSystems;
                currentTsunami.ReferenceFrameOfTheMeasurement = incomingTsunami.ReferenceFrameOfTheMeasurement;
                currentTsunami.Elements = incomingTsunami.Elements;
                currentTsunami.Points = incomingTsunami.Points;
                currentTsunami.CompositeElements = incomingTsunami.CompositeElements;
                currentTsunami.HiddenPoints = incomingTsunami.HiddenPoints;

                foreach (M.Module item in incomingTsunami.MeasurementModules)
                {
                    Tsunami2.Properties.Menu.BroadcastMessage($"Restarting {item._Name}");
                    RestartModule(item, saveSomeMemory);
                }
                currentTsunami.Save(currentTsunami.PathOfTheSavedFile, "after loading"); // force the path to avoid to have the give q name when the we open and existing one and the taht teh guipref 'ASK for tsu path when nothing is set'
            }
            catch (global::System.Exception ex)
            {

                throw new Exception($"Cannot load 'Tsunami'", ex);
            }
        }

        public static void RestartModule(M.Module module, bool saveSomeMemory = false, bool addToTsunami = true)
        {
            try
            {
                module.ReCreateWhatIsNotSerialized(saveSomeMemory);
                if (addToTsunami) Tsunami2.Properties.Add(module);

                module.Restart();
            }
            catch (Exception ex)
            {
                throw new Exception($"Cannot restart the module '{module._Name}", ex);
            }
        }

        // will close or return false if closing is canceled
        public bool Quit(bool skipWarning = false, bool ExitOnlyTheTsunamiThread = false)
        {
            string respond = "";
            Thread CompressingThread = null;
            string pathOfTsu = M.Tsunami2.Preferences.Tsunami.PathOfTheSavedFile;
            if (pathOfTsu != null)
            {
                if (pathOfTsu != "")
                {
                    FileInfo fi = new FileInfo(pathOfTsu);
                    DirectoryInfo di = new DirectoryInfo(fi.DirectoryName);

                    while (respond != R.T_Quit)
                    {
                        // skip or ask confirmation
                        if (skipWarning)
                            respond = R.T_Quit;
                        else
                        {
                            string titleAndMessage = $"{R.T_Quitting}\r\n{R.T_COPY_FOLDER_TO_CLIPBOARD}";
                            MessageInput mi = new MessageInput(MessageType.ImportantChoice, titleAndMessage)
                            {
                                ButtonTexts = new List<string> { R.T_Quit, R.T_MORE, R.T_CANCEL },
                            };
                            respond = mi.Show().TextOfButtonClicked;
                        }

                        // if cancel
                        if (respond == R.T_CANCEL) return false;

                        // if more options

                        if (respond == R.T_MORE)
                        {
                            string titleAndMessage = $"{R.T_T_MORE_BEFORE_QUIT};{R.T_D_MORE_BEFORE_QUIT}";
                            MessageInput mi = new MessageInput(MessageType.ImportantChoice, titleAndMessage)
                            {
                                ButtonTexts = new List<string> { R.T_OPEN_MEASURE_FOLDER, R.T_COMPRESS_YOUR_FOLDER, R.T_CANCEL },
                            };
                            respond = mi.Show().TextOfButtonClicked;

                            // if cancel
                            if (respond == R.T_CANCEL) continue;

                            if (respond == R.T_COMPRESS_YOUR_FOLDER)
                            {

                                using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile())
                                {
                                    zip.CompressionLevel = Ionic.Zlib.CompressionLevel.BestSpeed;
                                    zip.AddDirectory(di.FullName);
                                    zip.Save($"{di.FullName}.zip");
                                }
                                System.Diagnostics.Process.Start(new DirectoryInfo(M.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay).Parent.FullName);
                                Quit(skipWarning: true);
                                return false;
                            }
                            else
                            {
                                System.Diagnostics.Process.Start(M.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay);
                                Quit(skipWarning: true);
                                return false;
                            }
                        }
                    }
                    new MessageInput(MessageType.GoodNews, "Compressing your compute folder").Show();
                    CompressingThread = CompressComputeFolderASync();
                }
            }

            new MessageInput(MessageType.GoodNews, "Quitting").Show();

            if (ExitOnlyTheTsunamiThread) // to avoid to close the test environnement
                this.View.Invoke(new Action(() => System.Windows.Forms.Application.ExitThread()));
            else
            {
                TSU.Debug.MouseAndKeyboardMonitoring_Unsubscribe();
                if (TSU.Debug.Screenshots.CreateOrNotAVideoInAThread(out Thread videoThread))
                    videoThread.Join(5 * 60 * 1000); // wait 5 minutes max for the video to be created

                if (CompressingThread != null)
                    CompressingThread.Join(1 * 60 * 1000);

                Environment.Exit(Environment.ExitCode);
            }
            return true;
        }

        private Thread CompressComputeFolderASync()
        {
            System.Threading.Thread thread = new Thread(CompressComputeFolder);
            thread.Start();
            return thread;
        }

        private void CompressComputeFolder()
        {
            if (Debug.IsRunningInATest)
                return;
            string folderToCompressPath = "";
            try
            {
                string pathOfTsu = M.Tsunami2.Preferences.Tsunami.PathOfTheSavedFile;
                FileInfo fi = new FileInfo(pathOfTsu);
                DirectoryInfo di = new DirectoryInfo(fi.DirectoryName);

                folderToCompressPath = di.FullName + "\\computes";
                if (!Directory.Exists(folderToCompressPath))
                    return;

                string nameOFTheZip = $"{di.FullName}\\{T.Conversions.Date.ToDateUpSideDown(DateTime.Now)}, Computes.zip";

                // open existing (if 2nd time) or create zip
                Ionic.Zip.ZipFile zip;
                if (File.Exists(nameOFTheZip))
                    zip = Ionic.Zip.ZipFile.Read(nameOFTheZip);
                else
                {
                    zip = new Ionic.Zip.ZipFile();
                    zip.Name = nameOFTheZip;
                }

                // add new computes
                using (zip)
                {
                    zip.CompressionLevel = Ionic.Zlib.CompressionLevel.BestSpeed;
                    zip.IgnoreDuplicateFiles = true;
                    zip.AddDirectory(folderToCompressPath);
                    zip.Save();
                }

                // delete the non compressed folder
                try
                {
                    Directory.Delete(folderToCompressPath, recursive: true);
                }
                catch (Exception)
                {
                    Logs.Log.AddEntryAsError(this, "Could not delete the 'compute' folder after compression");
                }
            }
            catch (Exception)
            {
                Logs.Log.AddEntryAsError(this, $"Could not Compress the {folderToCompressPath} folder");
            }
        }

        internal void ChangeViewStrategy()
        {
            switch (View._Strategy.type)
            {
                case TsunamiView.TsunamiViewsStrategies.Tab:
                    View.ChangeStrategy(TsunamiView.TsunamiViewsStrategies.Windows);
                    break;
                case TsunamiView.TsunamiViewsStrategies.Windows:
                    View.ChangeStrategy(TsunamiView.TsunamiViewsStrategies.Tab);
                    break;
                case TsunamiView.TsunamiViewsStrategies.DockableWIndows:
                    View.ChangeStrategy(TsunamiView.TsunamiViewsStrategies.Windows);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// this check fot TSUT extention, if so the template is copied with default name in the folderof the day
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string MakeCopyIfThisIsATemplate(string fileName)
        {
            FileInfo fi = new FileInfo(fileName);
            if (fi.Extension.ToUpper() == ".TSUT")
            {
                PathOfTheSavedFile = "";
                string proposedPath = GetProposedPath();
                PathOfTheSavedFile = proposedPath;
                View.UpdateTextTitle();
                File.Copy(fileName, proposedPath);
                fileName = proposedPath;
            }
            return fileName;
        }


        public string GetProposedPath(bool TemplateFolder = false)
        {
            if (PathOfTheSavedFile != "" && !TemplateFolder) return PathOfTheSavedFile;

            // directory
            string proposedDirectory = TemplateFolder ? M.Tsunami2.Preferences.Values.Paths.Templates : M.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay;

            if (ParentModule is Guided.Group.Module GrGm)
            {
                if (GrGm.savePath != "")
                    proposedDirectory = GrGm.savePath;
            }

            // filename
            string proposedName = TSU.Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now);
            if (MeasurementModules.Count > 0)
            {
                foreach (var item in MeasurementModules)
                {
                    proposedName += ", " + item._Name;
                }
            }

            if (Zone != null)
                proposedName += ", " + Zone._Name;

            string extension = TemplateFolder ? ".tsut" : ".tsu";
            return proposedDirectory + proposedName + extension;
        }


        [XmlIgnore]
        private bool lastSavingFailed = false;

        internal void SaveAs(string proposedPath = "", string triggerMessage = "")
        {
            if (proposedPath == "") proposedPath = GetProposedPath();

            string fullPath = View.GetSavingName(
                Path.GetDirectoryName(proposedPath),
                Path.GetFileName(proposedPath),
                "Tsu File (*.tsu)|*.tsu|Tsunami Template (*.tsut)|*.tsut|XML file (*.xml)|*.xml");

            if (fullPath != "")
            {
                if (triggerMessage == "")
                    triggerMessage = "Manual Save As";

                Save(fullPath, triggerMessage);

                if (new FileInfo(fullPath).Extension.ToUpper() == ".TSUT")
                {
                    MessageInput mi = new MessageInput(MessageType.Choice, "Continue to modify template?")
                    {
                        ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                    };
                    bool continueWithTemplate = mi.Show().TextOfButtonClicked == R.T_YES;
                    if (!continueWithTemplate)
                    {
                        string newpath = MakeCopyIfThisIsATemplate(fullPath);
                        Save(newpath, "TemplateToTsu");
                    }
                }
            }
        }

        internal void SaveAsTemplate(string proposedPath = "", string triggerMessage = "")
        {
            if (proposedPath == "")
            {
                proposedPath = GetProposedPath(TemplateFolder: true);
            }

            string fullPath = View.GetSavingName(
                Path.GetDirectoryName(proposedPath),
                Path.GetFileName(proposedPath),
                "Template (*.tsut)|*.tsut");

            if (fullPath != "")
            {
                if (triggerMessage == "")
                    triggerMessage = "Manual Save As";

                Save(fullPath, triggerMessage);

                if (new FileInfo(fullPath).Extension.ToUpper() == ".TSUT")
                {
                    MessageInput mi = new MessageInput(MessageType.Choice, "Continue to modify template?")
                    {
                        ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                    };
                    bool continueWithTemplate = mi.Show().TextOfButtonClicked == R.T_YES;
                    if (!continueWithTemplate)
                    {
                        string newpath = MakeCopyIfThisIsATemplate(fullPath);
                        Save(newpath, "TemplateToTsu");
                    }
                }
            }
        }

        internal void Save(string path = "", string triggerMessage = "")
        {
            void WriteFile(Tsunami tsunami, string forcedPath, string message)
            {
                try
                {
                    Logs.Log.AddEntryAsBeginningOf(tsunami, "saving to .tsu asked");
                    if (tsunami.ParentModule is Guided.Group.Module grGm)
                    {
                        IO.Tsu.Create(grGm, forcedPath, message);
                        grGm.savePath = forcedPath;
                    }
                    else
                    {
                        IO.Tsu.Create(tsunami, forcedPath, message);
                    }
                    tsunami.lastSavingFailed = false;
                    tsunami.PathOfTheSavedFile = forcedPath;
                }
                catch (OutOfMemoryException e)
                {
                    TsuView.WarnForOutOfMemory(tsunami, R.String_Saving_ProblemDuringSavingModule, e);
                }
                catch (Exception ex)
                {
                    if (tsunami.lastSavingFailed)
                    {
                        string titleAndMessage = R.String_Saving_ProblemDuringSavingModule + "\r\n" + ex.Message;
                        new MessageInput(MessageType.Warning, titleAndMessage).Show();
                    }

                    tsunami.lastSavingFailed = true;
                }
            }
            bool GetRightPath(Tsunami tsunami, string forcedPath, string message, out string returnedPath)
            {
                returnedPath = forcedPath;
                // if path not forced and never asked
                if (forcedPath == "" &&
                    Tsunami2.Properties.PathOfTheSavedFile == "" &&
                    M.Tsunami2.Preferences.Values.GuiPrefs.AskForTsuFileNameAndLocation.IsTrue)
                {
                    string titleAndMessage = $"{R.T_TIME_TO_SAVE};{R.T_TSUNAMI_WILL_AUTOMATICALLY}";
                    new MessageInput(MessageType.GoodNews, titleAndMessage).Show();
                    tsunami.SaveAs("", message);
                    return false;
                }
                if (path == "")
                    returnedPath = GetProposedPath();
                return true;
            }
            void Do(Tsunami tsunami, string forcedPath, string message = "")
            {
                Logs.Log.AddEntryAsBeginningOf(tsunami, $"Saving triggered by '{triggerMessage}'");

                if (GetRightPath(tsunami, forcedPath, message, out string finalPath))
                {
                    WriteFile(tsunami, finalPath, message);
                    Logs.Log.AddEntryAsFinishOf(tsunami, $"Saved '{message}'");
                }
            }

            try
            {
                Do(this, path, triggerMessage);
            }
            catch (Exception ex)
            {
                string titleAndMessage = $"Not Saved;Saving was triggered by '{triggerMessage}' but : {ex.Message}";
                new MessageInput(MessageType.Critical, titleAndMessage).Show();
            }
        }

        public event EventHandler SavingFinished;
        public void DeclareSavingFinished()
        {
            SavingFinished?.Invoke(this, new EventArgs());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        public void SynchronousSave(string path = "", string triggerMessage = "")
        {
            ManualResetEvent saved = new ManualResetEvent(false);
            void OnSaved(object sender, EventArgs e) => saved.Set();
            SavingFinished += OnSaved;
            Save(path, triggerMessage);
            saved.WaitOne(60000);
            SavingFinished -= OnSaved;
        }


        internal void Export(IO.Types exportType, string path)
        {
            switch (exportType)
            {
                case IO.Types.Geode:
                    IO.SUSoft.Geode.ToGeode(MeasurementModules, out _);
                    break;
                case IO.Types.LGC2:
                    IO.SUSoft.Lgc2.ToLgc2(MeasurementModules, path, M.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances);
                    break;
                case IO.Types.csv:
                    IO.Csv.ToCsv(MeasurementModules, path);
                    break;
                default:
                    break;
            }
            if (File.Exists(path))
            {
                DateTime creationTime = File.GetLastWriteTime(path);
                DateTime now = DateTime.Now;
                if (creationTime.AddSeconds(3) > now)
                    Shell.Run(path);
            }

        }

        internal void UpdateStatus(string v)
        {
            if (Menu != null)
            {
                Action a = () =>
                {
                    Debug.WriteInConsole("Status updated to " + v);
                    Menu.View.infoLabel.Text = v;
                    Menu.View.infoLabel.Invalidate();
                    Menu.View.infoLabel.Update();
                    Menu.View.infoLabel.Refresh();
                    System.Windows.Forms.Application.DoEvents();
                };
                M.Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(a);
            }
        }

        public override void Restart()
        {
            var r = new MessageInput(MessageType.Choice, $"{R.T_SAVE}?;{R.T_SAVE_MODIFICATIONS_SINCE_THE_LAST_MEASUREMENT_}") { ButtonTexts = new List<string> { R.T_YES, R.T_NO } }.Show();

            if (r.TextOfButtonClicked == R.T_YES)
            {
                Tsunami2.Properties.View.Hide();
                Tsunami2.Properties.Save("", "before restart");
                Tsunami2.Properties.SavingFinished += On_Saving_Finished_Restart;
            }
            else
            {
                Tsunami2.Properties.View.Hide();
                On_Saving_Finished_Restart(null, null);
            }
        }

        private void On_Saving_Finished_Restart(object sender, EventArgs e)
        {
            Tsunami2.Properties.SavingFinished -= On_Saving_Finished_Restart;
            string tsuPath = this.PathOfTheSavedFile;
            if (System.IO.File.Exists(tsuPath))
            {
                // we need to use the same tsunami version.
                Process p = new Process();
                p.StartInfo.FileName = Application.ExecutablePath;
                p.StartInfo.Arguments = "\"" + tsuPath + "\"";
                p.StartInfo.UseShellExecute = true;
                p.Start();
                //Shell.Run(Application.ExecutablePath + " " +tsuPath, wait: false);
            }
            else
                Application.Restart();

            Environment.Exit(0);
        }

        internal void On_ZoneChanged(object sender, Manager.ManagerEventArgs e)
        {
            bool validate = false;
            if (e.JustAddedObjects != null)
            {
                if (e.JustAddedObjects.Count == 1)
                {
                    Z.Zone newZ = e.JustAddedObjects[0] as Z.Zone;
                    Z.Zone oldZ = Tsunami2.Properties.Zone;
                    if (newZ != oldZ)
                    {
                        if (oldZ == null)
                        {
                            validate = true;
                        }
                        else
                        {
                            if (Tsunami2.Properties.Elements.Count > 0)
                            {
                                new MessageInput(MessageType.Warning, "A zone is already selected and you have points in element manager;Situation not implemented yet").Show();
                            }
                            else
                                validate = true;
                        }
                    }


                    if (validate)
                    {
                        string titleAndMessage = $"{R.T_NEW_ZONE}!;{R.T_ZONE_USE} ({newZ.ToString()} ?";
                        MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                        {
                            ButtonTexts = new List<string> { newZ._Name, R.T_CANCEL }
                        };
                        if (mi.Show().TextOfButtonClicked != R.T_CANCEL)
                        {
                            Tsunami2.Properties.Zone = newZ;
                            Common.Elements.Manager.Module.TryFillToCoordinates();
                        }
                    }
                }
            }
        }

        private async void DoWork()
        {
            await RunT3000Async();
        }

        int _portHandle;


        public static readonly ManualResetEvent TsunamiExists = new ManualResetEvent(false);
        public static readonly ManualResetEvent TsuLoaded = new ManualResetEvent(false);

        private Task<int> RunT3000Async()
        {
            float hz = 0;
            float v = 0;
            float d = 0;

            //create a task completion source
            //the type of the result value must be the same
            //as the type in the returning Task
            TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
            Task.Run(() =>
            {
                Action a = () =>
                {
                    //TSU.Common.Instruments.Adapter.T3000_dll.PurgePort(_portHandle);
                    if (T3000_dll.GetAngles(_portHandle, ref hz, ref v) == -1) throw new Exception();
                    //TSU.Common.Instruments.Adapter.T3000_dll.PurgePort(_portHandle);
                    //TSU.Common.Instruments.Adapter.T3000_dll.PurgePort(_portHandle);
                    if (T3000_dll.GetDistance(_portHandle, ref hz, ref v, ref d) == -1) throw new Exception();
                    //TSU.Common.Instruments.Adapter.T3000_dll.PurgePort(_portHandle);
                };

                for (int i = 0; i < 10; i++)
                {
                    a();
                }

                tcs.SetResult(3);
            });
            //return the Task
            return tcs.Task;
        }

        public static void ShowHelp()
        {
            M.Shell.Run(M.Tsunami2.Preferences.Values.Paths.LocalHelp);
            //if (CheckForInternetConnection() == true)
            System.Diagnostics.Process.Start("https://readthedocs.web.cern.ch/pages/viewpage.action?pageId=22152983");
            //else
            //M.Shell.Run(M.Tsunami2.Preferences.Values.Paths.LocalHelp);
            new MessageInput(MessageType.FYI, "Several items have been opened;2 local pdf file and a page of ReadTheDocs in your browser").Show();
        }

        public void CreateMenu()
        {
            Menu = new F.Menu(this);
        }

        internal List<Station> GetStations()
        {
            List<Station> stations = new List<Station>();

            Action<Station> addOrNot = (s) =>
            {
                if (!stations.Contains(s))
                    stations.Add(s);
            };

            foreach (var item in MeasurementModules)
            {
                if (item is Guided.Group.Module ggm)
                {
                    foreach (var item2 in ggm.SubGuidedModules)
                    {
                        if (item2 is Guided.Module gm)
                        {
                            foreach (var item3 in gm.StationModules)
                            {
                                addOrNot(item3._Station);
                            }
                        }
                    }
                }
                else if (item is Guided.Module gm)
                {
                    foreach (var item2 in gm.StationModules)
                    {
                        addOrNot(item2._Station);
                    }
                }
                else if (item is FinalModule fm)
                {
                    foreach (var item2 in fm.StationModules)
                    {
                        addOrNot(item2._Station);
                    }
                }
            }
            return stations;
        }

        internal List<Operations.Operation> GetOperationNumbers()
        {
            List<Operations.Operation> operations = new List<Operations.Operation>();

            Action<Operations.Operation> addOrNot = (o) =>
            {
                if (o.value == 999999)
                    return;
                if (!operations.Contains(o))
                    operations.Add(o);
            };

            foreach (var item in GetStations())
            {
                addOrNot(item.ParametersBasic._Operation);
            }
            return operations;
        }
    }

    public class TsunamiException : Exception
    {
        public TsunamiException(string message) : base(message)
        {

        }
    }
}
