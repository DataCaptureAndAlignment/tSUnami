using System;
using R = TSU.Properties.Resources;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;
using TSU.Tools;
using TSU.ENUM;
using TSU.Preferences;
using M = TSU;
using Z = TSU.Common.Zones;
using F = TSU.Functionalities;
using C = TSU.Common.Elements.Composites;

using TSU.Common.Zones;
using TSU.IO;
using TSU.Views.Message;

namespace TSU.Common.Dependencies.Manager
{
    // How to Element Module
    //
    // This module can be used to acces theoritical point from file type.
    //
    // 1. Create a new instance of the class and give him the reference of the parent module, ex: ElementModule e =new ElementModule(this);
    // 2. Call of the available function of selection,  ex : Point p = e.SelectOnePoint();
    //                                                  ex : CompositeElement p = e.SelectPoints();
    // 

    public class Module : M.Manager
    {

        #region  Properties

        public enum sourceFile
        {
            geodeXYZH,
            geodeXYZ,
            geodeXYH,
            IDxyz,
            XML
        }
        [XmlIgnore]
        public new View View
        {
            get { return _TsuView as View; }
            set { _TsuView = value; }
        }
        public Zone zone { get; set; }

        internal bool cancel = false;

        #endregion

        #region constructor

        public Module() : base()
        {

        }

        public Module(M.Module module)
            : base(module, R.T_MNU_Depenencies)
        {
            Refresh();
        }



        private void CheckInstalledApplication()
        {
            Application Installed = new Application(R.T_SU_SOTFWARE_ALREADY_INSTALLED, "X.X.X", "", "", "");

            List<DirectoryInfo> dirsx86 = new List<DirectoryInfo>();
            List<DirectoryInfo> dirsx64 = new List<DirectoryInfo>();
            bool is64bit = Directory.Exists(@"c:\program files (x86)");
            if (is64bit)
            {
                if (Directory.Exists(@"c:\Program Files (x86)\Susoft"))
                    dirsx86.AddRange(new DirectoryInfo(@"c:\Program Files (x86)\Susoft").EnumerateDirectories());
                if (Directory.Exists(@"c:\Program Files\Susoft"))
                    dirsx64.AddRange(new DirectoryInfo(@"c:\Program Files\Susoft").EnumerateDirectories());
            }
            else
                if (Directory.Exists(@"c:\Program Files\Susoft"))
                dirsx86.AddRange(new DirectoryInfo(@"c:\Program Files\Susoft").EnumerateDirectories());

            foreach (DirectoryInfo appDir in dirsx86)
            {
                foreach (DirectoryInfo versionDir in appDir.EnumerateDirectories())
                {
                    string uninstallerPath = versionDir.FullName + @"\uninstall.exe";
                    if (!File.Exists(uninstallerPath)) uninstallerPath = "";
                    string applicationPath = versionDir.FullName + @"\" + appDir.Name + ".exe";
                    if (!File.Exists(applicationPath))
                    {
                        applicationPath = "";
                    }
                    Installed.Add(new Application(appDir.Name, versionDir.Name, "", uninstallerPath, applicationPath));

                }
            }
            foreach (DirectoryInfo appDir in dirsx64)
            {
                foreach (DirectoryInfo versionDir in appDir.EnumerateDirectories())
                {
                    string uninstallerPath = versionDir.FullName + @"\Uninstall.exe";
                    if (!File.Exists(uninstallerPath)) uninstallerPath = "";
                    string applicationPath = versionDir.FullName + @"\" + appDir.Name + ".exe";
                    if (!File.Exists(applicationPath)) applicationPath = "";

                    Installed.Add(new Application(appDir.Name, versionDir.Name, "", uninstallerPath, applicationPath));
                }
            }
            AllElements.Add(Installed);
            SelectableObjects.Clear(); // if clean then will show everything
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        #endregion

        #region Actions

        internal void UnInstallMissingDependencies()
        {
            if (_SelectedObjects.Count > 3)
                if (!View.AskForConfirmation(R.Q_Confirm_Alot_Uninstallation)) return;

            foreach (Application item in _SelectedObjects)
            {
                if (cancel)
                {
                    cancel = false;
                    return;
                }
                if (item != null)
                {
                    if (item.ApplicationFileInfo != null)
                    {
                        if (item.ApplicationFileInfo.Directory.Exists)
                        {
                            FileInfo[] fis = item.ApplicationFileInfo.Directory.GetFiles("*uninstall*");
                            if (fis.Length > 0)
                            {
                                string uninstaller = fis[0].FullName;
                                if (View.AskForConfirmation(string.Format(R.Q_Confirm_Uninstallation, item._Name + " v" + item.Version)))
                                    Shell.RunWithWaitingMessage(View, new List<string>() { fis[0].FullName });
                            }
                            else
                            {
                                string titleAndMessage = string.Format(R.T_NO_INSTALLER, item._Name);
                                new MessageInput(MessageType.Warning, titleAndMessage).Show();
                            }
                        }
                    }

                    if (item.ApplicationFileInfo == null && item.InstallerFileInfo != null)
                    {
                        if (item.InstallerFileInfo.Directory.Exists)
                        {
                            FileInfo[] fis = item.InstallerFileInfo.Directory.GetFiles("*uninstall*");
                            if (fis.Length > 0)
                            {
                                string uninstaller = fis[0].FullName;
                                if (View.AskForConfirmation(string.Format(R.Q_Confirm_Uninstallation, item._Name + " v" + item.Version)))
                                    Shell.RunWithWaitingMessage(View, new List<string>() { fis[0].FullName });
                            }
                            else
                            {
                                string titleAndMessage = string.Format(R.T_NO_INSTALLER, item._Name);
                                new MessageInput(MessageType.Warning, titleAndMessage).Show();
                            }
                        }
                    }
                }
            }
            Refresh();
            UpdateView();
        }


        internal void Refresh()
        {

            if (View.WaitingForm == null) View.WaitingForm = new Views.Message.ProgressMessage(View, R.T_REFRESHING, $"{R.T_STARTING}...", 3, 2000, false);
            View.WaitingForm.Show();
            AllElements.Clear();
            //View.WaitingForm.BeginAstep($"{R.T_LOADING_SUSOFT_RECOMMANDATION_FILE}...");
            //OpenSuSoftRecommandationFile();
            View.WaitingForm.BeginAstep($"{R.T_LOADING_TSUNAMI_DEPENDENCIES_FILE}...");
            OpenTsunamiDependenciesFile();
            View.WaitingForm.BeginAstep($"{R.T_CHECKING_INSTALLED_APPLICATIONS}...");
            CheckInstalledApplication();

            SelectableObjects.Clear(); // if clean then will show everything
            View.WaitingForm.Stop();
            _SelectedObjects.Clear();
            View.UpdateView();
        }

        internal void InstallSelectedDependencies()
        {
            List<Application> apps = new List<Application>();
            foreach (Application item in _SelectedObjects)
                if (item is Application) apps.Add(item as Application);
            apps.Reverse(); // so that dependencies are installed before the app that need it.
            Install(apps);
        }


        private void Install(List<Application> apps)
        {
            List<string> installersPath = new List<string>();
            foreach (Application item in apps)
            {
                if (item.ApplicationFileInfo == null) continue;
                if (cancel)
                {
                    cancel = false;
                    return;
                }
                if (!File.Exists(item.ApplicationFileInfo.FullName))
                {
                    if (File.Exists(item.InstallerFileInfo.FullName))
                    {
                        installersPath.Add(item.InstallerFileInfo.FullName);
                    }
                    else
                    {
                        string newPath = View.Ask4InstallerPath(item);
                        if (newPath != "")
                            installersPath.Add(item.InstallerFileInfo.FullName);
                    }
                }
            }

            Shell.RunWithWaitingMessage(View, installersPath);
            Refresh();
            UpdateView();
        }

        #endregion

        #region Selection
        // Selection
        internal void ValidateSelection()
        {
            View.ValidateSelection();
            if (_SelectedObjects.Count > 0)
            {
                Application e = new Application();
                foreach (TsuObject o in _SelectedObjects)
                {
                    e.Add(o as Application);
                }
                SendMessage(e);
                //foreach (var item in this.SelectedObjects)
                //{
                //    SendMessage(item);

                //}
            }
        }

        #endregion

        #region  Open, export and save

        internal void OpenSuSoftRecommandationFile()
        {
            try
            {
                string dfsPath = @"\\cern.ch\dfs\Support\SurveyingEng\Computing";
                if (!TSU.Tsunami2.Preferences.Values.DfsAvailable)
                    throw new Exception(string.Format("{0};{1}{2}\r\n{3}",
                        R.T_DFS_FILES_UNAVAILABLE,
                        $"{R.T_THE_FOLLOWING_PATH_IS_INACCESSIBLE}: ",
                        dfsPath,
                        $"{R.T_IF_YOU_WANT_TSUNAMI_TO} 'c:\temp' {R.T_FOLDER}."));


                string fullPath = @"\\cern.ch\dfs\Support\SurveyingEng\Computing\Software\Install\Susoft\SUSoftDependencies.xml";
                if (!File.Exists(fullPath)) fullPath = View.Ask4TheoriticalFilePath(fullPath);

                if (fullPath != "")
                {
                    Application a = OpenAFile(fullPath);

                    a.StoredApplicationPath = "";

                    if (a != null)
                    {
                        AllElements.Add(a);

                    }
                }
            }
            catch (Exception ex)
            {
                string titleAndMessage = string.Format("{0};{1}",
                    R.T_LOADING_OF_RECOMMENDATIONS_FAILED,
                    ex.Message);
                new MessageInput(MessageType.Critical, titleAndMessage).Show();
            }
        }
        private void OpenTsunamiDependenciesFile()
        {
            string fullPath =TSU.Tsunami2.Preferences.Values.Paths.Dependencies;
            if (!File.Exists(fullPath)) fullPath = View.Ask4TheoriticalFilePath(fullPath);
            Application a = OpenAFile(fullPath);
            if (a != null)
                AllElements.Add(a);
        }

        internal Application OpenAFile(string fullPath)
        {
            if (fullPath == "") return null;
            return Xml.DeserializeFile(typeof(Application), fullPath) as Application;
        }

        #endregion
    }
}
