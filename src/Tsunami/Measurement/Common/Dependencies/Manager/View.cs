﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using TSU.ENUM;
using TSU.Views;
using TSU.Common.Elements.Composites;
using TSU.Common.Instruments;
using TSU.Common.Measures;

using V = TSU.Common.Strategies.Views;
using TSU.Common.Zones;
using TSU.Views.Message;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TrackBar;
using System.Threading;


namespace TSU.Common.Dependencies.Manager
{
    public partial class View : ManagerView
    {
        # region Fields and props
        private new Module Module
        {
            get
            {
                return base.Module as Module;
            }
            set
            {
                base.Module = value;
            }
        }

        public Buttons buttons;

        
        #endregion

        # region Constructor

        /// <summary>
        /// Create the view, link it to the Module and should suscribed the view to the module
        /// </summary>
        /// <param name="module"></param>
        public View(IModule module)
            : base(module) // Taking form already in controllerModuleView
        {
            Module = module as Module;
            this.Image = R.Susoft;
            //_Selection = new List<ObjectTSU>();
            //Initializations
            InitializeComponent();
            this.ApplyThemeColors();
            InitializeMenu();

            this.AdaptTopPanelToButtonHeight();
            this.CheckBoxesVisible = true;
            this.AutoCheckChildren = true;
            
            this.currentStrategy.Update();
            this.currentStrategy.Show();

        }
        private void ApplyThemeColors()
        {
            this.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
        }
        #endregion

        // Strategy
        // define an interface for different strategy of showing the elements
        public override void UpdateView()
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action(() => { UpdateView(); }));
                return;
            }
            base.UpdateView();
            this.currentStrategy.Update();
        }

        internal override void InitializeStrategy()
        {
            this.AvailableViewStrategies = new List<V.Types>() {  V.Types.Tree };
            this.SetStrategy(V.Types.Tree);
        }

        # region updates

        public override List<string> ListviewColumnHeaderTexts
        {

            get
            {
                return new List<string>() { "Name" };
            }
            set { }
        }

        #endregion
        

        #region Menus

        internal override void InitializeMenu()
        {
           // Creation of buttons
            buttons = new Buttons(this);
            
            base.InitializeMenu();
            bigbuttonTop.ChangeNameAndDescription(R.T_DM_title);
            bigbuttonTop.ChangeImage(R.Susoft);
        }
        
        private void SetContextMenuToGlobal()
        {
            contextButtons.Clear();
            contextButtons.Add(buttons.Install);
            contextButtons.Add(buttons.UnInstall);
            //contextButtons.Add(buttons.open);
            contextButtons.Add(buttons.ShowProblem);
            contextButtons.Add(buttons.Refresh);
            contextButtons.Add(buttons.Quit);
        }
        internal override void ShowContextMenuGlobal()
        {
            this.SetContextMenuToGlobal();
            this.ShowPopUpMenu(contextButtons);
        }

        public new class Buttons
        {
            private View _view;
            public List<BigButton> all;
            public BigButton Install;
            public BigButton InstallAll;
            public BigButton Refresh;
            public BigButton Quit;

            public BigButton UnInstall;
            public BigButton ShowProblem;
            public Buttons(View view)
            {
                _view = view;
                all = new List<BigButton>();

                Refresh = new BigButton($"{R.T_REFRESH};{R.T_RELOAD_FILES_AND_CHECK_DEPENDENCIES}",
                   R.DNA03_Loop, _view.Module.Refresh); all.Add(Install);

                Install = new BigButton(R.T_B_InstallMissingDep,
                   R.Save, _view.Module.InstallSelectedDependencies); all.Add(Install);
                
                UnInstall = new BigButton(R.buttonUnInstallMissingDep,
                   R.Cancel, _view.Module.UnInstallMissingDependencies); all.Add(UnInstall);

                ShowProblem = new BigButton(R.ShowMissingInfo,
                   R.Search, _view.ShowProblems); all.Add(ShowProblem);

                Quit = new BigButton(
                   R.T_B_Close,
                   R.MessageTsu_Problem, this._view.Quit,TSU.Tsunami2.Preferences.Theme.Colors.Bad);
            }

            public void ReleaseAll()
            {
                foreach (BigButton item in this.all)
                    item.Available = true;
            }

           

            public void BlockAll()
            {
                foreach (BigButton item in this.all)
                    item.Available = false;
            }

            public void Release(BigButton buttonToRelease)
            {
                (buttonToRelease).Available = true;
            }

            public void Block(BigButton buttonToBLock)
            {
                (buttonToBLock ).Available = false;
            }

            public void BlockAdvanced()
            {
                this.BlockAll();
            }
        }

        private void Quit()
        {
            Tsunami2.Properties.Remove(this.Module);
        }

        #endregion

        #region Actions

        internal void ShowProblems()
        {
            this.currentStrategy.ShowProblems();
        }

        # region Open & Save

        internal void Open()
        {
            string filePath = this.Ask4TheoriticalFilePath("");
            if (filePath == "") return;
            FileInfo fileInfo = new FileInfo(filePath);
            try
            {
                string extension = fileInfo.Extension.ToUpper();
                string fileName = fileInfo.Name.ToUpper();
            }
            catch (Exception ex)
            {
                Shell.Run(filePath);
                throw new Exception(string.Format(R.T301, filePath) +"\r\n"+ ex.Message);//"Add Points failed", ex.Source + ": " + ex.Message, R.T_OK, "", "", System.Reflection.MethodBase.GetCurrentMethod().Name));
            }
        }

        #endregion

        # region Select


        #endregion

        # region Ask4

        internal string Ask4TheoriticalFilePath(string path)
        {
            string yes =R.T_YES;
            string no =R.T_NO;
            string respond = yes;
            if (path!="")
            { 
                string question = string.Format(R.Err_depFile, path);
                MessageInput mi = new MessageInput(MessageType.Choice, question)
                {
                    ButtonTexts = new List<string> { yes, no }
                };
                respond = mi.Show().TextOfButtonClicked;
            }
            if (respond == yes)
            {
                return TsuPath.GetFileNameToOpen(this, TSU.Tsunami2.Preferences.Values.Paths.Data, "", "Xml files (*.xml)|*.xml", "Browse dependencies reference file");
            }
            else
                return "";
        }

        internal string Ask4InstallerPath(Application item)
        {
            string browse =R.T_Browse;
            string cancel =R.T_CANCEL;
            string question = string.Format(R.Err_Installer, item._Name + " v" +item.Version);
            MessageInput mi = new MessageInput(MessageType.Choice, question)
            {
                ButtonTexts = new List<string> { browse, cancel }
            };
            if (mi.Show().TextOfButtonClicked == browse)
            {
                return TsuPath.GetFileNameToOpen(this, @"\\cern.ch\dfs\Support\SurveyingEng\Computing\Software",
                    "", "Executable (*.*)|*.*", 
                    string.Format(R.Title_Installer, item._Name + " v" + item.Version));
            }
            else
                return "";
        }

        internal bool AskForConfirmation(string question)
        {
            string yes =R.T_YES;
            string no =R.T_NO;
            MessageInput mi = new MessageInput(MessageType.Choice, question)
            {
                ButtonTexts = new List<string> { yes, no }
            };
            return (mi.Show().TextOfButtonClicked == yes);
        }

        #endregion

        #endregion

        #region Events

        public override void OnNext(TsuObject TsuObject)
        {
            dynamic dynamic = TsuObject;
            OnNextBasedOn(dynamic);
        }
        

        internal override void CancelAction()
        {
            this.Module.cancel = true;
        }
        #endregion

    }
}
