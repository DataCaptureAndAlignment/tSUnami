﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;

using System.Xml.Serialization;


namespace TSU.Common.Dependencies
{

    public class Application : TSU.TsuObject
    {
       

        public class VersionNumbers
        {
            public string Major { get; set; }
            public string Minor { get; set; }
            public string Build { get; set; }
            public string Full
            {
                get
                {
                    return Major.ToString() + "." + Minor.ToString() + "." + Build;
                }
            }

            public VersionNumbers()
            {
                this.Major = "1";
                this.Minor = "0";
                this.Build = "0";
            }

            public VersionNumbers(string version)
            {
                if (version != "")
                {
                    this.Major = "0";
                    this.Minor = "0";
                    this.Build = "0";
                    string[] parts = version.Split('.');
                    string a;
                    if (parts.Length >2)
                    {
                        this.Major = parts[0];
                        this.Minor = parts[1];
                        this.Build = parts[2];
                    }
                }
            }

            public override string ToString()
            {
                return this.Major.ToString() +'.'+ this.Minor.ToString() +'.'+ this.Build;
            }

            public string ToString(string param)
            {
                if( param == "MinorWithOneChar")
                    return this.Major.ToString() + '.' + this.Minor.ToString() + '.' + this.Build;

                return this.ToString();
            }
        }


        [XmlAttribute("Vital")]
        public bool Vital { get; set; }

        [XmlAttribute("V")]
        public string Version
        {
            get
            {
                return this._VersionNumbers.ToString();
            }
            set
            {
                this._VersionNumbers = new VersionNumbers(value);
            }
        }

        public string VersionMinorOnOneChar
        {
            get
            {
                return this._VersionNumbers.ToString("MinorWithOneChar");
            }
            set
            {
                this._VersionNumbers = new VersionNumbers(value);
            }
        }

        [XmlIgnore]
        public VersionNumbers _VersionNumbers { get; set; }

        /// <summary>
        /// some key word can be included in the path: 
        ///     {SUSoft} = dfs/computing/software/install/susoft/AppName/Version/
        ///     {program files} c:\program files((x86) or not)/AppName/Version/
        ///     {application} path of the application itself
        /// </summary>
        [XmlAttribute("InstallerPath")]
        public string StoredInstallerPath;

        [XmlAttribute("ApplicationPath")]
        public string StoredApplicationPath;

        [XmlIgnore]
        public string Path
        {
            get { return this.CorrectPath(StoredApplicationPath); }
        }

        [XmlIgnore]
        public string InstallerPath
        {
            get { return this.CorrectPath(StoredInstallerPath); }
        }

        [XmlIgnore]
        public System.IO.FileInfo InstallerFileInfo
        {
            get
            {
                if (StoredInstallerPath != "")
                {
                    string realPath = this.CorrectPath(StoredInstallerPath);
                    return new System.IO.FileInfo(realPath);
                }
                else
                    return null;
            }
        }

        [XmlIgnore]
        public System.IO.FileInfo ApplicationFileInfo
        {
            get
            {
                if (StoredApplicationPath != "")
                    return new System.IO.FileInfo(this.CorrectPath(StoredApplicationPath));
                else
                    return null;
            }
        }

        [XmlIgnore]
        public bool IsInstalled
        {
            get
            {
                string correctedPath = this.CorrectPath(this.StoredApplicationPath);
                if (System.IO.File.Exists(correctedPath))
                    return  true;
                else
                return false;
            }
        }

        [XmlAttribute]
        public string StringDate;

        [XmlIgnore]
        public DateTime Date;

        public List<Application> Dependencies;

        public Application ()
        {
            this._Name = "A SUSoft";
            this._VersionNumbers = new VersionNumbers();
            Dependencies = new List<Application>();
        }

        public Application(string name, string version, string date, string installerPath, string applicationPath)
        {
            this._Name = name;
            this.StringDate = date;
            this._VersionNumbers = new VersionNumbers(version);
            this.Dependencies = new List<Application>();
            this.StoredInstallerPath = installerPath;
            this.StoredApplicationPath = applicationPath;
        }

        public override string ToString()
        {
            return this._Name + " v" + this.Version;
        }

        public void Add(Application a)
        {
            this.Dependencies.Add(a);
        }

        /// <summary>
        /// to replace key words such as {susoft}, etc
        /// </summary>
        /// <param name="pathWithKeyWords"></param>
        /// <returns></returns>
        public string CorrectPath (string pathWithKeyWords)
        {
            string corrected = pathWithKeyWords;
            if (pathWithKeyWords != "")
            {
                if (pathWithKeyWords.ToUpper().Contains("{SUSOFT}"))
                {
                    corrected = pathWithKeyWords.ToUpper().Replace("{SUSOFT}",
                        @"\\cern.ch\dfs\Support\SurveyingEng\Computing\Software\Install\SUSOFT\" + this._Name + @"\" + this.Version);
                }

                if (pathWithKeyWords.ToUpper().Contains("{SUTEST}"))
                {
                    corrected = pathWithKeyWords.ToUpper().Replace("{SUTEST}",
                        @"\\cern.ch\dfs\Support\SurveyingEng\Computing\Software\Test\" + this._Name + @"\" + this.Version);
                }

                if (pathWithKeyWords.ToUpper().Contains("{PROGRAM FILES}"))
                {
                    string pathWithoutKeyWords = "";
                    pathWithoutKeyWords = pathWithKeyWords.ToUpper().Replace("{PROGRAM FILES}",
                    @"C:\Program Files\SUSoft\" + this._Name + @"\" + this.Version);

                    // check with one char minor version
                    string minor = this._VersionNumbers.Minor;
                    if (System.IO.File.Exists(pathWithoutKeyWords) == false)
                        pathWithoutKeyWords = pathWithoutKeyWords.ToUpper().Replace($".{minor}.", $".{minor}.");

                    // check in x86 folder (minor1char)
                    if (System.IO.File.Exists(pathWithoutKeyWords) == false)
                        pathWithoutKeyWords = pathWithoutKeyWords.ToUpper().Replace(@"C:\PROGRAM FILES", @"C:\Program Files (x86)");

                    // check in x86 folder (minor2char)
                    if (System.IO.File.Exists(pathWithoutKeyWords) == false)
                        pathWithoutKeyWords = pathWithoutKeyWords.ToUpper().Replace($".{minor}.", $".{minor}.");

                    corrected = pathWithoutKeyWords;
                }
                else if (pathWithKeyWords.ToUpper().Contains("{APPLICATION}"))
                { 
                    corrected = pathWithKeyWords.ToUpper().Replace(@"{APPLICATION}", AppDomain.CurrentDomain.BaseDirectory);
                }
            }

            if (corrected.ToUpper().Contains(@"\\CERN.CH"))
            {

                if (!TSU.Tsunami2.Preferences.Values.DfsAvailable) // if no dfs available move the looking folder to c:temp
                    corrected = corrected.Replace(
                    @"\\cern.ch\dfs\Support\SurveyingEng\Computing\Software",
                    @"c:\temp" );
            }
            return corrected;
        }
    }
}
