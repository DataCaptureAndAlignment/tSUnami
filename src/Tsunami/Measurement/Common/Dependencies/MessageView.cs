﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;
using TSU.Views;
using TSU.IO;
using TSU.Views.Message;


namespace TSU.Common.Dependencies
{
    internal class MessageView : MessageTsu
    {
        internal string FilePath;
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hwnd, int nCmdShow);

        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        public static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);

        public System.Diagnostics.Process Process;

        //[System.Runtime.InteropServices.DllImport("user32.dll")]
        //private static extern int ShowWindow(IntPtr hwnd, int nCmdShow);

        //[System.Runtime.InteropServices.DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        //static extern IntPtr SendMessage(IntPtr hWnd, Int32 Msg, Int32 wParam, Int32 lParam);

        Timer _timer;
        public MessageView(string filePath, string titleAndMessage, MessageType type = MessageType.FYI )
            : base(null,TSU.Tsunami2.Preferences.Theme.Colors.Object,TSU.Tsunami2.Preferences.Theme.Colors.NormalFore, "P", "Running a dependency", titleAndMessage, "Close", "Open File Outside Tsunami", "Open File Location", "", filePath, false)
        {
            if (type != MessageType.FYI)
                this.Colorize(type, Color.Transparent);
            FilePath = filePath;
            this.Height = (int)(TSU.Tsunami2.Preferences.Values.WorkingArea.Height * 0.8);
            this.Width = (int)(TSU.Tsunami2.Preferences.Values.WorkingArea.Width * 0.8);
            this.panel1.BringToFront();
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Opacity = 1;
        }

        

        internal void StartProcess(string filePath=null, bool needElevation=false)
        {
            if (filePath == null) filePath = FilePath;
            System.Diagnostics.Process process = new Process();
            process.StartInfo = new System.Diagnostics.ProcessStartInfo() { UseShellExecute = true, FileName = filePath, };
            Start(process,needElevation);
        }
        

        internal void StartProcess(string appPath, string commandLine, bool needElevation = false)
        {
            System.Diagnostics.Process process = new Process();
            process.StartInfo = new System.Diagnostics.ProcessStartInfo(appPath, commandLine) { };
            Start(process, needElevation);
        }


        internal void StartPadProcess(string filePath = null, bool needElevation = false)
        {
            if (filePath == null) filePath = FilePath;

            System.Diagnostics.Process process = new Process();
            TSU.Preferences.Preferences.Dependencies.GetByBame("SURVEYPAD", out Common.Dependencies.Application app);

            if (filePath.ToUpper().EndsWith(".INP"))
                filePath = filePath + " -r";

            process.StartInfo = new System.Diagnostics.ProcessStartInfo(app.Path, filePath) { UseShellExecute = true };
            Start(process,needElevation);
        }

        internal void Start(System.Diagnostics.Process process, bool needElevation = false)
        {
            try
            {
                process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Minimized;

                Process = System.Diagnostics.Process.Start(process.StartInfo);

                Process.EnableRaisingEvents = true;
                Process.WaitForInputIdle();

                System.Threading.Thread.Sleep(300); // Allow the process to open it's window


                // handle process
                IntPtr motherHandle = this.panel1.Handle;
                IntPtr childHandle = new IntPtr();

                if (needElevation) // get the process anme wait for need thread to start and get its hand
                {
                    string name = Process.ProcessName;

                    Process[] processes = Process.GetProcessesByName(name);
                    if (processes.Length >= 1)
                    {
                        Process.Refresh();
                        childHandle = processes[0].MainWindowHandle;
                    }
                    else
                        foreach (Process p in processes)
                        {
                            childHandle = p.MainWindowHandle;
                        }
                }
                else
                {
                    childHandle = Process.MainWindowHandle;
                }


                ShowWindow(childHandle, 3);

                this.panel1.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.DarkBackground;
                this.panel1.Visible = true;

                if (childHandle != null) SetParent(childHandle, motherHandle);
                this.button1.Click += CloseProcess;
                this.button2.Click += OpenOutsideTsunami;
                this.button3.Click += OpenFileLocation;
                this.panel1.SizeChanged += SendMessageToProcessToGoMaximized;

                this._timer = new System.Windows.Forms.Timer();
                this._timer.Interval = 100;
                this._timer.Tick += OnTick;
                this._timer.Start();
                this.SendMessageToProcessToGoMaximized(this, null);
            }
            catch (Exception ex)
            {
                new MessageInput(MessageType.Critical, ex.Message).Show();
            }
        }


        // Will close the messageView if the process has been closed
        private void OnTick(object sender, EventArgs e)
        {
            if (Process != null && Process.HasExited)
            {
                this.Close();
                this._timer.Dispose();
            }
        }
        
        private void CloseProcess(object sender, EventArgs e)
        {
            Process.EnableRaisingEvents = true;
            Process.CloseMainWindow();
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // buttonLeft
            // 
            this.button3.Location = new System.Drawing.Point(950, 937);
            this.button3.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.button3.MaximumSize = new System.Drawing.Size(315, 0);
            this.button3.MinimumSize = new System.Drawing.Size(315, 142);
            this.button3.Size = new System.Drawing.Size(315, 142);
            // 
            // buttonRight
            // 
            this.button2.Location = new System.Drawing.Point(1388, 937);
            this.button2.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.button2.MaximumSize = new System.Drawing.Size(315, 0);
            this.button2.MinimumSize = new System.Drawing.Size(315, 142);
            this.button2.Size = new System.Drawing.Size(315, 142);
            // 
            // buttonMiddle
            // 
            this.button1.Location = new System.Drawing.Point(1168, 938);
            this.button1.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.button1.MaximumSize = new System.Drawing.Size(315, 474);
            this.button1.MinimumSize = new System.Drawing.Size(315, 142);
            this.button1.Size = new System.Drawing.Size(315, 142);
            // 
            // panel1
            // 
            this.panel1.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.panel1.Size = new System.Drawing.Size(1580, 751);
            this.panel1.Visible = true;
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(18, 869);
            this.textBox.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.textBox.Size = new System.Drawing.Size(1578, 53);
            // 
            // MessageBy
            // 
            this.MessageBy.Location = new System.Drawing.Point(14, 1035);
            this.MessageBy.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.MessageBy.Size = new System.Drawing.Size(1534, 26);
            // 
            // MessageView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.ClientSize = new System.Drawing.Size(1616, 1060);
            this.Location = new System.Drawing.Point(0, 0);
            this.Margin = new System.Windows.Forms.Padding(9, 12, 9, 12);
            this.MinimumSize = new System.Drawing.Size(1012, 474);
            this.Name = "MessageView";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.button2, 0);
            this.Controls.SetChildIndex(this.button3, 0);
            this.Controls.SetChildIndex(this.labelPicture, 0);
            this.Controls.SetChildIndex(this.MessageBy, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.textBox, 0);
            this.ResumeLayout(false);
            this.PerformLayout();
            this.ApplyThemeColors();
        }
        private void ApplyThemeColors()
        {

        }
        private void SendMessageToProcessToGoMaximized(object sender, EventArgs e)
        {
            if (!MoveWindow(Process.MainWindowHandle, 0, 0, this.panel1.Width + 0, this.panel1.Height + 0, true))
            {
                //throw new Win32Exception();
            }
        }
        /// <summary>
        /// Run filePath outside Tsunami
        /// This will close the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenOutsideTsunami(object sender, EventArgs e)
        {
            Process.EnableRaisingEvents = true;
            Process.CloseMainWindow();
            TSU.Shell.Run(this.FilePath);
        }
        /// <summary>
        /// Open filePath in explorer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenFileLocation(object sender, EventArgs e)
        {
            int index = this.FilePath.LastIndexOf("\\");
            string fileLocation = this.FilePath.Remove(index);
            TSU.Shell.Run(fileLocation);
        }
    }
}
