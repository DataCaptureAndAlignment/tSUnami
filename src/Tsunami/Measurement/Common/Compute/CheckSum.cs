﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using TSU.Common.Compute.Transformation;

namespace TSU.Common.Compute
{

    public static class CheckSum
    {
        public static double Get(Rotation3D i)
        {
            double sum = i.RotX + i.RotY + i.RotZ;
            return Math.Round(sum, 15);
        }
        public static double Get(Rotation i)
        {
            double sum = i.Rot;
            return Math.Round(sum, 15);
        }
        public static double Get(Translation3D i)
        {
            double sum = i.AlongX + i.AlongY + i.AlongZ;
            return Math.Round(sum, 15);
        }
    }
}
