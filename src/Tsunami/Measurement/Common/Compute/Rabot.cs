﻿using CsvHelper.Configuration;
using System;
namespace TSU.Common.Compute.Rabot
{
    public enum Strategy
    {
        Not_Defined = 0,
        Expected_Offset = 1,
        Relative_Displacement = 2,
    }
    public class Rabot
    {
        public string Name { get; set; }
        public double? RabotDCum { get; set; }
        public double? DCum { get; set; }
        public double? Smooth { get; set; }
        public double? Rough { get; set; }
        public double? RoughMinusSmooth { get; set; }
        public double? Displacement { get; set; }
        public double? SmoothTol { get; set; }
        public double? SmoothPlusTol { get; set; }
        public string Element { get; set; }
        public string PointToMove { get; set; }
        public DateTime? OffsetDate { get; set; }
        public string OffsetComment { get; set; }

        public sealed class RabotMap : ClassMap<Rabot>
        {

            public RabotMap()
            {
                Map(m => m.Name).Name("Name");
                Map(m => m.RabotDCum).Name("Rabot DCum");
                Map(m => m.DCum).Name("DCum");
                Map(m => m.Smooth).Name("Smooth");
                Map(m => m.Rough).Name("Rough");
                Map(m => m.RoughMinusSmooth).Name("Rough - Smooth");
                Map(m => m.Displacement).Name("Displacement");
                Map(m => m.SmoothTol).Name("Smooth-tol");
                Map(m => m.SmoothPlusTol).Name("Smooth+tol");
                Map(m => m.Element).Name("Element");
                Map(m => m.PointToMove).Name("Point to move");
                Map(m => m.OffsetDate).Name("Offset date").TypeConverterOption.Format("dd.MM.yyyy");
                Map(m => m.OffsetComment).Name("Offset comment");
            }
        }
    }

}
