﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using E = TSU.Common.Elements;
using Z = TSU.Common.Zones;
using M = MathNet.Numerics.LinearAlgebra;
using R = TSU.Properties.Resources;
using TSU.Common.Compute.Transformation;

namespace TSU.Common.Compute
{
    public static class LinearRegression
    {
        /// Find the least squares linear fit. y = m * x + b
        /// Return the total error.
        public static double FindLinearLeastSquaresFit(
            List<E.Coordinates> points, out double m, out double b)
        {
            /// Perform the calculation.
            /// Find the values S1, Sx, Sy, Sxx, and Sxy.
            double S1 = points.Count;
            double Sx = 0;
            double Sy = 0;
            double Sxx = 0;
            double Sxy = 0;
            foreach (E.Coordinates pt in points)
            {
                Sx += pt.X.Value;
                Sy += pt.Y.Value;
                Sxx += pt.X.Value * pt.X.Value;
                Sxy += pt.X.Value * pt.Y.Value;
            }

            // Solve for m and b.
            m = (Sxy * S1 - Sx * Sy) / (Sxx * S1 - Sx * Sx);
            b = (Sxy * Sx - Sy * Sxx) / (Sx * Sx - S1 * Sxx);

            return Math.Sqrt(ErrorSquared(points, m, b));
        }

        /// Return the error squared.
        public static double ErrorSquared(List<E.Coordinates> points,
            double m, double b)
        {
            double total = 0;
            foreach (E.Coordinates pt in points)
            {
                double dy = pt.Y.Value - (m * pt.X.Value + b);
                total += dy * dy;
            }
            return total;
        }

        /// <summary>
        /// Compute the bearing, slope and projected points, and return the projection of the first point of the list awith bearing ans slope inside there parameters
        /// </summary>
        /// <param name="pointCloud"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static E.Composites.FittedShape Get3dLine(List<E.Point> pointCloud, out string message, Z.Zone zone)
        {
            message = "";
            E.Composites.FittedShape line3D;

            line3D = new E.Composites.FittedShape();
            line3D.Elements.AddRange(pointCloud);
            ComputeBearingAndSlope3D(line3D);

            // write this on the disk
            //Export.Xml.CreateFile(line3D,TSU.Tsunami2.TsunamiPreferences.Values.Paths.OfTheDay + line3D.parametricShape._Name.Replace('+', '_')+".xml");

            // set message
            if (pointCloud.Count > 2)
            {
                foreach (var item in line3D.residuals)
                {
                    message += string.Format("{0}: rX={1:F2}mm rY={2:F2}mm rZ={3:F2}mm", item._Name, item.X.Value, item.Y.Value, item.Z.Value);
                }
            }
            else
            {
                //   message += "No residuals available";
                message += R.T_NO_RESIDUALS_AVAILABLE;
            }

            return line3D;
        }

        private static void ComputeBearingAndSlope1D(List<E.Coordinates> coordinates, out double bearing, out double s)
        {
            //Compute the bearing in the XY plan
            FindLinearLeastSquaresFit(coordinates, out double m, out double b);
            E.Coordinates p1 = new E.Coordinates();
            p1.Set();
            p1.X = new DoubleValue(0, 0);
            p1.Y = new DoubleValue(m * p1.X.Value + b, 0);

            E.Coordinates p2 = new E.Coordinates();
            p2.Set();
            p2.X = new DoubleValue(10, 0);
            p2.Y = new DoubleValue(m * p2.X.Value + b, 0);

            bearing = Survey.GetBearing(p1, p2);

            List<E.Coordinates> rotatedCoordinates = new List<E.Coordinates>();
            foreach (var item in coordinates)
            {
                rotatedCoordinates.Add(Systems.Rotate3DAroundOrigin(item, 0, 0, bearing, ENUM.AngularUnit.Gon));
            }

            // COmpute the slope in the XZ plan
            SwitchAxis(rotatedCoordinates);

            FindLinearLeastSquaresFit(rotatedCoordinates, out m, out b);

            s = Math.Atan(m);
        }

        private static void ComputeBearingAndSlope3D(List<E.Coordinates> coordinates, out E.Coordinates projectionOfFirstPoint, out double bearing, out double s, out string message)
        {
            ComputeLineParametersWithNormalEquations(coordinates, out bearing, out double va, out List<E.Coordinates> proj, out double[] dist, out List<E.Coordinates> res);

            // message = "Distances to the fitted line:" + "\r\n";
            message = R.T_DISTANCES_TO_THE_FITTED_LINE + "\r\n";
            for (int i = 0; i < dist.Length; i++)
            {
                message += string.Format("{0}: {1} mm", coordinates[i]._Name, dist[i]) + "\r\n";
            }
            s = (100 - va) / 200 * Math.PI;

            projectionOfFirstPoint = proj[0];
        }

        private static void ComputeBearingAndSlope3D(E.Composites.FittedShape line3D)
        {
            Z.Zone zone = Tsunami2.Properties == null ? null : Tsunami2.Properties.Zone;
            if (zone == null)
            {
                zone = new Z.Zone() { Ccs2MlaInfo = new Z.Zone.Ccs2Mla() { ReferenceSurface = E.Coordinates.ReferenceSurfaces.RS2K } };
            }

            // set the method to get and out the rigth coordinates
            List<E.Coordinates> coordinates = new List<E.Coordinates>();
            Func<E.Point, E.Coordinates> GetCoordinates;
            Action<E.Point, E.Coordinates> PutCoordinates;
            E.Point firstPoint = (line3D.Elements[0] as E.Point).Clone() as E.Point;
            if (firstPoint._Coordinates.CoordinatesExistInAGivenSystem("CCS-Z"))
            {
                GetCoordinates = (p) =>
                {
                    return p._Coordinates.GetCoordinatesInASystemByName("CCS-Z").Clone() as E.Coordinates;
                };
                PutCoordinates = (p, c) =>
                {
                    p._Coordinates.AddOrReplaceCoordinatesInOneSystem("CCS-Z", c);
                    Systems.ZToH(p, zone.Ccs2MlaInfo.ReferenceSurface);

                    List<E.Point> pointsWithoutCoord = new List<E.Point>() { p };
                    List<E.Point> pointsWithCoord = new List<E.Point>();

                    E.Coordinates.ReferenceFrames rf = E.Coordinates.GetXYhSystem(zone.Ccs2MlaInfo.ReferenceSurface);

                    Systems.TryToFixMissingCoordinates(pointsWithCoord, pointsWithoutCoord, pointsWithoutCoord, pointsWithoutCoord, rf, zone);
                };
            }
            else if (firstPoint._Coordinates.HasLocal)
            {
                GetCoordinates = (p) => { return p._Coordinates.Local; };
                PutCoordinates = (p, c) =>
                {
                    p._Coordinates.Local = c;
                    List<E.Point> pointsWithoutCoord = new List<E.Point>() { p };
                    List<E.Point> pointsWithCoord = new List<E.Point>();

                    E.Coordinates.ReferenceFrames rf = E.Coordinates.GetXYhSystem(zone.Ccs2MlaInfo.ReferenceSurface);
                    Systems.TryToFixMissingCoordinates(pointsWithoutCoord, pointsWithoutCoord, pointsWithCoord, pointsWithoutCoord, rf, zone);
                };
            }
            else
            {
                throw new Exception($"{R.T_LINE_COMPUTATION_PROBLEM};{R.T_LINE_CALCULATION_WITH_THE_COORDINATES_AVAILABLE_ARE_NOT_YET_IMPLEMENTED}");
            }

            // Take the coordiantes
            foreach (var item in line3D.Elements)
            {
                coordinates.Add(GetCoordinates(item as E.Point) as E.Coordinates);
            }

            // Compute
            // if (coordinates.Count < 2) throw new Exception(string.Format("{0};{1}", R.T_LINE_COMPUTATION_PROBLEM, R.T_NOT_ENOUGH_POINTS));
            if (coordinates.Count < 2) throw new Exception($"{R.T_LINE_COMPUTATION_PROBLEM};{R.T_NOT_ENOUGH_POINTS}");
            double bearingInGrad = 0;
            double verticalAngleInGrad = 0;
            if (coordinates.Count > 2)
            {
                //   throw new Exception(string.Format("{0};{1}", R.T_LINE_COMPUTATION_PROBLEM, R.T_TOO_MUCH_POINTS_MORE_THAN_2_NOT_IMPELMENTED_YET));
                throw new Exception($"{R.T_LINE_COMPUTATION_PROBLEM};{R.T_TOO_MUCH_POINTS_MORE_THAN_2_NOT_IMPELMENTED_YET}");
                ComputeLineParametersWithNormalEquations(coordinates, out bearingInGrad, out verticalAngleInGrad, out line3D.projectedPoints, out double[] dist, out line3D.residuals);
            }
            else
            {
                line3D.projectedPoints = coordinates;
                E.Coordinates zeros = new E.Coordinates("", 0, 0, 0);
                line3D.residuals = new List<E.Coordinates>() { zeros, zeros };
                bearingInGrad = Compute.Survey.GetBearing(coordinates[0], coordinates[1]);
                verticalAngleInGrad = (Math.PI / 2 - Compute.Survey.GetSlope(coordinates[0], coordinates[1])) / Math.PI * 200;
            }
            E.Point origin = new E.Point();
            PutCoordinates(origin, line3D.projectedPoints[0]);

            // Fill parametric line
            E.Line3D parametricLine = new E.Line3D();
            parametricLine._Name = string.Format("Line_{0:hh}h{0:mm}({1}_{2}Pts)", DateTime.Now, line3D.Elements[0]._Name, line3D.Elements.Count);
            parametricLine._Coordinates = origin._Coordinates;
            parametricLine.Bearing = new DoubleValue(bearingInGrad, 0);
            parametricLine.Slope = new DoubleValue((100 - verticalAngleInGrad) / 200 * Math.PI, 0);

            // put it in the fitted shape
            line3D._Name = parametricLine._Name;
            line3D.parametricShape = parametricLine;
        }

        private static void SwitchAxis(List<E.Coordinates> coordinates)
        {
            foreach (var item in coordinates)
            {
                DoubleValue temp = item.X;
                item.X = item.Y;
                item.Y = item.Z;

                item.Z = temp;
            }
        }

        [Obsolete] // because do not give same results as SHAPES
        /// <summary>
        /// Not working, a fit a line 2 points is giving residuals.... and all resulsts are different tahn shapes
        /// Maths from Konstantinos Nikolitsas 
        /// </summary>
        /// <param name="inputCoordinates"></param>
        /// <param name="bearingInGrads"></param>
        /// <param name="verticalAngleInGrads"></param>
        /// <param name="projectedCoordinatesInM"></param>
        /// <param name="distancesInMM"></param>
        /// <param name="residualsInMM"></param>
        public static void ComputeLineParametersWithNormalEquations(
            List<E.Coordinates> inputCoordinates,
            out double bearingInGrads,
            out double verticalAngleInGrads,
            out List<E.Coordinates> projectedCoordinatesInM,
            out double[] distancesInMM,
            out List<E.Coordinates> residualsInMM)
        {
            projectedCoordinatesInM = new List<E.Coordinates>();
            distancesInMM = new double[inputCoordinates.Count];
            residualsInMM = new List<E.Coordinates>();

            // Fill matrix
            M.Matrix<double> xyz = M.Matrix<double>.Build.Dense(inputCoordinates.Count, 3);
            for (int i = 0; i < inputCoordinates.Count; i++)
            {
                xyz[i, 0] = inputCoordinates[i].X.Value;
                xyz[i, 1] = inputCoordinates[i].Y.Value;
                xyz[i, 2] = inputCoordinates[i].Z.Value;
            }

            //[lines, column]=size(xyz);
            int lines = xyz.RowCount;
            int columns = xyz.ColumnCount;

            M.Matrix<double> A = M.Matrix<double>.Build.Dense(2 * lines, 4, 0); ////  4 column, 2* n lines 
            M.Matrix<double> DL = M.Matrix<double>.Build.Dense(2 * lines, 1, 0); //  1 column, 2* n lines 

            for (int i = 0; i < 2 * lines; i++)
            {
                if (i < lines)
                {
                    A[i, 0] = xyz[i, 0];
                    A[i, 2] = 1;
                    DL[i, 0] = xyz[i, 1];
                }
                else if (i >= lines)
                {
                    A[i, 0] = 0;
                    A[i, 2] = 0;
                    DL[i, 0] = xyz[i - lines, 2];
                }
                if (i < lines)
                {
                    A[i, 1] = 0;
                    A[i, 3] = 0;
                }
                else if (i >= lines)
                {
                    A[i, 1] = xyz[i - lines, 0];
                    A[i, 3] = 1;
                }
            }

            var N = A.Transpose() * A;

            var x = N.Inverse() * A.Transpose() * DL;

            var u = A * x - DL;

            double so;
            if (lines > 2)
                so = Math.Sqrt((u.Transpose() * u / (2 * lines - 4))[0, 0]);
            else
                so = 0;

            var Vx = Math.Pow(so, 2) * N.Inverse();
            var Vl = Math.Pow(so, 2) * A * N.Inverse() * A.Transpose();

            double x2 = 4;
            double y2 = x[0, 0] * x2 + x[2, 0];
            double z2 = x[1, 0] * x2 + x[3, 0];

            double x22 = 10;
            double y22 = x[0, 0] * x22 + x[2, 0];
            double z22 = x[1, 0] * x22 + x[3, 0];

            double d = Math.Sqrt(Math.Pow(x22 - x2, 2) + Math.Pow(y22 - y2, 2) + Math.Pow(z22 - z2, 2));

            double l = (x22 - x2) / d;
            double m = (y22 - y2) / d;
            double n = (z22 - z2) / d;

            bearingInGrads = 200 * Math.Acos(m) / Math.PI; // grads
            verticalAngleInGrads = 200 * Math.Acos(n) / Math.PI; // grads

            var final = M.Matrix<double>.Build.Dense(lines, 10, 0);
            //for i = 1 : lines
            for (int i = 0; i < lines; i++)
            {
                var v1 = M.Vector<double>.Build.DenseOfArray(new double[] { x2, y2, z2 });
                var v2 = M.Vector<double>.Build.DenseOfArray(new double[] { x22, y22, z22 });
                var v3 = M.Vector<double>.Build.DenseOfArray(new double[] { xyz[i, 0], xyz[i, 1], xyz[i, 2] });
                var v1v3 = v3 - v1;
                var v1v2 = v2 - v1;

                var proj = v1 + v1v3.DotProduct(v1v2) / v1v2.DotProduct(v1v2) * v1v2;
                var diafora = proj - v3;
                var distance = Math.Sqrt(Math.Pow(diafora[0], 2) + Math.Pow(diafora[1], 2) + Math.Pow(diafora[2], 2));

                projectedCoordinatesInM.Add(new E.Coordinates(inputCoordinates[i]._Name, proj[0], proj[1], proj[2]));
                distancesInMM[i] = distance * 1000;
                residualsInMM.Add(new E.Coordinates(inputCoordinates[i]._Name, diafora[0] * 1000, diafora[1] * 1000, diafora[2] * 1000));
            }
        }
    }
}
