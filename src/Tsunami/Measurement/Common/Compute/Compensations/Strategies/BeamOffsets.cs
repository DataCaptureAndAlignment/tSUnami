﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSU.Common.Compute.Compensations.Strategies
{
    [Serializable]
    public class BeamOffsets : TSU.Common.Compute.Compensations.Strategies.Common
    {
        public BeamOffsets()
        {
            _strategy = TSU.Common.Compute.Compensations.Strategies.List.FreeStation_LGC2;
        }
        public BeamOffsets(Polar.Station.Module module)
            : base(module)
        {
            _strategy = TSU.Common.Compute.Compensations.Strategies.List.FreeStation_LGC2;
        }

        public override void Create_LGC2_Input(Polar.Station station, string fileName, Lgc2 lgc2, string title = "",  bool temporaryCompute = false)
        {
            throw new NotImplementedException();
        }

        public override void Create_PLGC1_Input(Polar.Station station, string fileName, Plgc1 plgc1, string title = "")
        {
            throw new NotImplementedException();
        }

        public override bool Update()
        {
            throw new NotImplementedException();
        }

        internal override void CheckGeoid()
        {
            throw new NotImplementedException();
        }

        internal override void ChooseMeasureForApproximateCompute()
        {
            throw new NotImplementedException();
        }

        internal override void ChooseMeasureForCompensation()
        {
            throw new NotImplementedException();
        }

        internal override void Compensate()
        {
            throw new NotImplementedException();
        }

        internal override void ComputeApproximateSetup()
        {
            throw new NotImplementedException();
        }

        internal override void FindErrors()
        {
            throw new NotImplementedException();
        }

        internal override string GetConfidenceEllipsoid1Sigma()
        {
            throw new NotImplementedException();
        }

        internal override string GetSetupLGC2Details()
        {
            throw new NotImplementedException();
        }

        internal override void SetDefaultMeasureSelection(List<TsuObject> allElements)
        {
            throw new NotImplementedException();
        }
    }

}