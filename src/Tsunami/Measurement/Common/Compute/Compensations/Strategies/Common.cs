﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Windows.Forms;
using System.Xml.Serialization;
using TSU.Common.Analysis;
using TSU.Views;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using M = TSU.Common.Measures;
using R = TSU.Properties.Resources;
using TSU.IO.SUSoft;
using System.Threading;
using TSU.Common.Elements.Composites;
using System.IO;
using System.Diagnostics;
using TSU.Common.Compute.Transformation;
using System.Collections;
using System.Windows.Shapes;
using System.Data.SqlTypes;

namespace TSU.Common.Compute.Compensations.Strategies
{
    public enum List
    {
        TourDHorizon,
        FreeStation,
        OrientatedOnly,
        NoComputation,
        Local,
        PointLancé,
        Tour_DHorizon_LGC2,
        FreeStation_LGC2,
        FreeRadi_LGC2,
        OrientatedOnly_LGC2,
        NoComputation2,
        Local_LGC2,
        PointLancé_LGC2,
        Intersections,
        PointLancés_LGC2,
        FuturComputation,
        GotoForStationSettedUp,
        AllCala,
        NonVerticalizedSetup,
        NonVerticalizedPointLancé,
        NonVerticalizedGoto,
    }

    public enum FileEditionOptions
    {
        HideAll,
        ShowAll,
        ShowOutputOnly,
        Ask
    }

    // this is a signature to create a variable containing a method that will give the right coordinates between CCS or MLA
    public delegate E.Coordinates GetTheRightCoordinates(E.Point point);

    // this is a signature to create a variable containing a method that will give the right datum option *oloc *rs2k...
    public delegate string GetTheRightOption();

    //[XmlInclude(typeof(TSU.Advanced.Theodolite.Polar.Compensations.List.FreeStation_LGC2))]
    //[XmlInclude(typeof(TSU.Advanced.Theodolite.Polar.Compensations.List.PointLancéLGC2))]
    //[XmlInclude(typeof(TSU.Advanced.Theodolite.Polar.Compensations.List.Orientation_LGC2))]
    //[XmlInclude(typeof(TSU.Advanced.Theodolite.Polar.Compensations.List.NewFrame_LGC2))]
    //[XmlInclude(typeof(TSU.Advanced.Theodolite.Polar.Compensations.List.AllCala))]
    [Serializable]
    public abstract class Common
    {
        internal static int CriticalSigma0 = 10;
        internal static int ProblematicSigma0 = 3;

        internal FileEditionOptions FileShowingStrategy;
        internal List _strategy;

        [XmlIgnore]
        internal Polar.Station.Module _Module;

        [XmlIgnore]
        internal Polar.Station station
        {
            get
            {
                return this._Module?.StationTheodolite;
            }
        }

        [XmlIgnore]
        internal Polar.Station.Parameters param
        {
            get
            {
                return this._Module?.stationParameters;
            }
        }


        internal Polar.Station.Parameters.Setup.Values CompensationResults;

        internal bool editInput;
        internal bool editInputWithSP;
        internal bool seeOutput;

        internal Frame frameForInstrument = null;

        public Common()
        {
        }

        public Common(Polar.Station.Module module)
        {
            if (module != null)
            {
                _Module = module;
            }
        }

        public static Common GetStrategy(List type, Polar.Station.Module module)
        {
            switch (type)
            {
                case List.TourDHorizon: return null;
                case List.FreeStation:
                case List.FreeStation_LGC2:
                case List.FreeRadi_LGC2: return new Polar.Compensations.FreeStation_LGC2(module);
                case List.OrientatedOnly:
                case List.OrientatedOnly_LGC2: return new Polar.Compensations.Orientation_LGC2(module);
                case List.Local_LGC2:
                case List.Local: return new Polar.Compensations.NewFrame_LGC2(module);
                case List.PointLancé:
                case List.PointLancé_LGC2: return new Polar.Compensations.PointLancéLGC2(module);
                case List.PointLancés_LGC2: return new Polar.Compensations.PointLancésLGC2(module);
                case List.GotoForStationSettedUp: return new Polar.Compensations.GotoForStationSettedUp(module);
                case List.AllCala: return new Polar.Compensations.AllCala(module);
                case List.NonVerticalizedSetup: return new Polar.Compensations.NonVerticalized.Setup(module);
                case List.NonVerticalizedPointLancé: return new Polar.Compensations.NonVerticalized.PointLancé(module);
                case List.NonVerticalizedGoto: return new Polar.Compensations.NonVerticalized.Goto(module);
                case List.Tour_DHorizon_LGC2:
                case List.FuturComputation: throw new Exception($"No computation strategy implemented for {type}");
                case List.NoComputation2:
                case List.NoComputation:
                default: throw new Exception($"No computation strategy set for {type}");
            }
        }
        public void DispatchPointsBasedOnGeodeRole(Station station, string refSurfOption, List<E.Point> cala, List<E.Point> vz, List<E.Point> vxy, List<E.Point> poin)
        {
            foreach (var item in station.MeasuresTaken)
            {
                E.Point point = GetPointWithCoordinates(item, refSurfOption);
                switch (item.GeodeRole)
                {
                    case Geode.Roles.Cala:
                        cala.Add(point);
                        break;
                    case Geode.Roles.Radi:
                        vxy.Add(point);
                        break;
                    case Geode.Roles.Poin:
                        poin.Add(point);
                        break;
                    case Geode.Roles.Unused:
                    case Geode.Roles.Unknown:
                    default:
                        break;
                }
            }
        }

        internal virtual string RenameFile(string fileName)
        {
            string r;
            if (fileName == "")
            {
                r = Tsunami2.Preferences.Values.Paths.MeasureOfTheDay;
                r += @"\computes\PLGC\";
                System.IO.Directory.CreateDirectory(r);
                r += Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now);
            }
            else
                r = fileName;
            return r;
        }


        internal E.Point GetPointWithCoordinates(M.Measure mesure, string refSurfOption)
        {

            try
            {
                E.Point o = mesure._OriginalPoint;
                E.CoordinatesInAllSystems oc = o?._Coordinates;
                E.Point n = mesure._Point;
                E.CoordinatesInAllSystems nc = n?._Coordinates;


                if (refSurfOption.ToUpper() == "*OLOC")
                {
                    if (oc != null && oc.HasLocal && !oc.AreApproximate) return o;
                    if (n != null && nc != null && nc.HasLocal && !nc.AreApproximate) return n;
                    if (oc != null && oc.HasLocal) return o;
                    if (nc != null && nc.HasLocal) return n;
                    return o;
                }
                else
                {
                    if (oc != null && oc.HasCcs && !oc.AreApproximate) return o;
                    if (n != null && nc != null && nc.HasCcs && !nc.AreApproximate) return n;
                    if (oc != null && oc.HasCcs) return o;
                    if (nc != null && nc.HasCcs) return n;
                    return o;
                }
            }
            catch (Exception)
            {
                if (mesure != null)
                    return mesure._OriginalPoint;
                return new E.Point();
            }

        }



        // This should run and LGC with new station coordinates fixed as well as v0 and the rest in POIN
        internal void ComputeRefPointsFromSetUpToHave_dRdLdZ()
        {
            try
            {
                if (station.MeasuresTaken[0]._OriginalPoint._Coordinates == null)
                    return;

                if (!station.MeasuresTaken[0]._OriginalPoint._Coordinates.HasCcs)
                    return;

                if (station.Parameters2.Setups.BestValues == null)
                    return;

                if (station.MeasuresTaken.Where(x => (x as Polar.Measure)._OriginalPoint._Parameters.hasGisement).Count() <= 0)
                    return;

                if (station.Parameters2.Setups.BestValues.VerticalisationState == Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation.NotVerticalised)
                    return;

                // create a fake station 
                Polar.Station fakeStation = station.Clone() as Polar.Station;
                //fakeStation._Name = "Fake4dLdRdZ_"+ fakeStation._Name;
                fakeStation.Parameters2.Setups.InitialValues.CompensationStrategyType = List.PointLancés_LGC2;
                fakeStation.Parameters2.Setups.InitialValues.IsPositionKnown = true;

                //Freestation?
                E.Point stp = Element.FindFirstPointByName(lgc2._Result.VariablePoints, fakeStation.Parameters2.Setups.InitialValues.StationPoint._Name);
                if (stp == null)
                    stp = station.Parameters2.Setups.InitialValues.StationPoint; // it was probably a know station
                fakeStation.Parameters2._StationPoint = stp;
                fakeStation.Parameters2.Setups.BestValues.vZero = lgc2._Result.vZero;

                Polar.Compensations.PointLancésLGC2 c = new Polar.Compensations.PointLancésLGC2(this._Module);

                // remove measure that are not calculable
                List<M.Measure> measuresToRemove = new List<M.Measure>();
                foreach (Polar.Measure item in fakeStation.MeasuresTaken.OfType<Polar.Measure>())
                {
                    M.Measure cm = Measure.FindByName(this.CompensationResults.Measures, item._Point._Name);
                    if (cm == null || !(item.UsedAsReference && item._OriginalPoint._Parameters.hasGisement))
                        measuresToRemove.Add(item);
                }
                foreach (var item in measuresToRemove)
                {
                    fakeStation.MeasuresTaken.Remove(item);
                }

                // restore
                foreach (Polar.Measure pm in fakeStation.MeasuresTaken)
                {
                    pm.Angles.HorizontalState = M.States.Types.Good; 
                    pm.Angles.VerticalState = M.States.Types.Good;
                    pm.Distance._Status = new M.States.Good();

                }

                // compute
                {
                    // temporary exchange stations
                    Polar.Station previous = this.station;
                    try
                    {
                        this._Module.StationTheodolite = fakeStation;
                        c.Compute2(silent: true, autoValidation: true, deleteLgcFiles: false);
                    }
                    catch (Exception ex)
                    {
                        new MessageInput(MessageType.Warning, $"dR dL dZ computation failed: {ex}").Show();
                        throw;
                    }
                    finally
                    {
                        this._Module.StationTheodolite = previous;
                    }
                }



                foreach (var m in station.MeasuresTaken) // real measures
                {
                    Polar.Measure rm = m as Polar.Measure;
                    Polar.Measure fm = Measure.FindByDate(fakeStation.MeasuresTaken, rm._Date) as Polar.Measure;

                    if (fm != null) // was computed
                    {
                        if (fm.UsedAsReference && fm._OriginalPoint._Parameters.hasGisement)
                        {
                            Survey.ComputeCoordinateInBeamCS(fm._OriginalPoint, fm._Point, Tsunami2.Properties.ReferenceFrameOfTheMeasurement);
                            //rm._Point._Coordinates.BeamV = fm._Point._Coordinates.BeamV;
                            //rm._Point._Coordinates.Beam = fm._Point._Coordinates.Beam;

                            // we also need to fill offset because it is what is shown in the alignement datagrid
                            fm._OriginalPoint._Coordinates.BeamV = new E.Coordinates(fm._Point._Name, 0, 0, 0);
                            fm._OriginalPoint._Coordinates.Beam = new E.Coordinates(fm._Point._Name, 0, 0, 0);

                            rm.Offsets = Polar.Station.Module.ComputeDifferences(fm);
                        }
                    }
                    else
                    {
                        //rm._Point._Coordinates.BeamV = null; // why to change the real point?
                        //rm._Point._Coordinates.Beam = null;
                        rm.Offsets = null;
                    }
                }
            }
            catch (Exception ex)
            {
                foreach (var m in station.MeasuresTaken) // real measures
                {
                    Polar.Measure rm = m as Polar.Measure;
                    //rm._Point._Coordinates.BeamV = null; // why to change the real point?
                    //rm._Point._Coordinates.Beam = null;
                    rm.Offsets = null;
                }
                Logs.Log.AddEntryAsPayAttentionOf(this._Module, $"{R.T_THE_COMPUTATION_OF_THE_DL_DR_DZ_FAILED}, {R.T_IF_YOU_DONT_MIND_ABOUT}, {ex}");
            }

        }

        internal abstract void SetDefaultMeasureSelection(List<TsuObject> allElements);


        public void TurnToGoodAndMarkAsToBeUpdated(List<E.Point> updatedPoints)
        {
            TempPointsToBeUpdated.Clear();
            TempPointsToBeUpdatedStrategy = E.Manager.Module.AddingStrategy.RenameIfExistInSameGroup;

            foreach (var item in updatedPoints)
            {
                item.State = E.Element.States.Good;
                item._Origin = station._Name;

                TempPointsToBeUpdated.Add(item);
                TempPointsToBeUpdatedStrategy = E.Manager.Module.AddingStrategy.ReplaceIfSameGroup;
            }
        }

        public List<E.Point> TempPointsToBeUpdated = new List<E.Point>();
        public E.Manager.Module.AddingStrategy TempPointsToBeUpdatedStrategy = E.Manager.Module.AddingStrategy.RenameIfExistInSameGroup;

        ///// <summary>
        ///// Launch the compute, bool param to specify if list of measure need to be edited
        ///// </summary>
        ///// <param name="editInputMeasureList"></param>
        ///// <returns></returns>
        //public abstract bool Compute(bool editInputMeasureList = false, bool showLog = true);

        public abstract bool Update();

        [XmlIgnore]
        public GetTheRightCoordinates getTheRightCoordinates;
        [XmlIgnore]
        public GetTheRightOption getTheRightOption;
        [XmlIgnore]
        public E.Coordinates.CoordinateSystemsTypes coordianteSystemType;

        internal bool CheckIfGeoidalComputeIsPossible(Polar.Station station, Common forcedStretegy = null)
        {
            bool useGoide = true;

            if (forcedStretegy == null) forcedStretegy = station.Parameters2.Setups.InitialValues.CompensationStrategy;

            // add all varailbe and reference point, we not using pointmeasured list because this on doenst know if a bad measured has been removed
            List<E.Point> l = new List<E.Point>();

            if (forcedStretegy is Polar.Compensations.PointLancéLGC2)
            {
                if (station.Parameters2._StationPoint != null)
                    l.Add(station.Parameters2._StationPoint);
            }
            else
            {
                //l.AddRange(station.VariablePoints); // why to check the variable if thery have to be computed anyway?
                l.AddRange(station.ReferencePoints);
            }

            if (l.Count == 0)
            {
                if (station.Parameters2 != null &&
                    station.Parameters2._StationPoint != null &&
                    station.Parameters2._StationPoint._Coordinates != null &&
                    station.Parameters2._StationPoint._Coordinates.HasCcs)
                    useGoide = true;
                else
                    useGoide = false;
            }
            else
            {
                foreach (E.Point item in l)
                {
                    if (item._Coordinates != null
                        && !item._Coordinates.HasCcs
                        && item._Coordinates.HasLocal)
                        // if (item._Origin != station._Name) // removed because i dont why it is here, and whith this local compute fail because it want to use the geoide
                        useGoide = false;
                }
            }

            if (this is Polar.Compensations.NewFrame_LGC2)
                useGoide = false;

            // getTheRightOption
            if (useGoide)
            {
                E.Coordinates.ReferenceSurfaces surface = l.Count > 0 ?
                    E.Coordinates.GetReferenceSurface(l[0]._Coordinates.Ccs.ReferenceFrame) :
                    E.Coordinates.GetReferenceSurface(station.Parameters2._StationPoint._Coordinates.Ccs.ReferenceFrame);
                switch (surface)
                {
                    case E.Coordinates.ReferenceSurfaces.Sphere:
                        getTheRightOption = Lgc2.Input.InputLine.OptionSphere;
                        break;
                    case E.Coordinates.ReferenceSurfaces.Machine1985:
                        getTheRightOption = Lgc2.Input.InputLine.OptionLep;
                        break;
                    default:
                        getTheRightOption = Lgc2.Input.InputLine.OptionRS2K;
                        break;
                }
            }
            else
                getTheRightOption = Lgc2.Input.InputLine.OptionOloc;

            // getTheRightCoordinates
            if (useGoide)
            {
                coordianteSystemType = E.Coordinates.CoordinateSystemsTypes._2DPlusH;
                getTheRightCoordinates = GetCcsCoordinates;
            }
            else
            {
                coordianteSystemType = E.Coordinates.CoordinateSystemsTypes._3DCartesian;
                getTheRightCoordinates = GetLocalCoordinates;
            }
            return useGoide;
        }

        private static E.Coordinates GetLocalCoordinates(E.Point point)
        {
            if (point._Coordinates != null && point._Coordinates.Local.AreKnown)
                return point._Coordinates.Local.Clone() as E.Coordinates;
            else
                return new E.Coordinates(point._Name);
        }

        private static E.Coordinates GetCcsCoordinates(E.Point point)
        {
            if (point._Coordinates != null && point._Coordinates.Ccs.AreKnown)
                return point._Coordinates.Ccs.Clone() as E.Coordinates;
            else
                return new E.Coordinates(point._Name);
        }

        public abstract void Create_LGC2_Input(Polar.Station station, string fileName, Lgc2 lgc2, string title = "", bool TemporaryCompute = false);
        public abstract void Create_PLGC1_Input(Polar.Station station, string fileName, Plgc1 plgc1, string title = "");

        [XmlIgnore]
        public Plgc1 plgc;

        //DsaFlag flag_UseBarycenter = new DsaFlag("Barycenter", DsaOptions.Ask_to);

        // Will try to compute approximate coordinates with plgc or by tsunami if not enough info .
        internal bool ComputeApproximativePosition(bool updateStationCoord, bool updateMeasuredPoints, out string message)
        {
            message = "";
            bool success = false;
            bool stationAlreadyhaveTemporarySetup = station.Parameters2.Setups.ActualState != Polar.Station.Parameters.Setup.States.Done;
            if (!stationAlreadyhaveTemporarySetup)
            {
                // if only 2 good measure, we don't use plgc because it will ask unnecessary question about the station position

                List<Polar.Measure> exploitableMeasures;

                if (updateStationCoord)
                    exploitableMeasures = GetExploitableMeasures(station);
                else
                {
                    exploitableMeasures = new List<Polar.Measure>();
                    exploitableMeasures.AddRange(station.Rays);
                    exploitableMeasures.AddRange(station.References);
                }

                if (exploitableMeasures.Count < 2 && updateStationCoord) throw new Exception($"{R.T_NOT_ENOUGH_ACTIVE_MEASURES};{R.T_COMPUTE_OF_APPROXIMATED_COORDINATES_FAILED}\r\n{R.T_YOU_NEED_A_MINIMUM_OF_2_MEASURES}");

                bool Use_PLGC = Tsunami2.Preferences.Values.UsePLGC;
                if (this._strategy == List.FreeStation_LGC2 && exploitableMeasures.Count < 3)
                {
                    Use_PLGC = false;
                    message += $"{R.T_NOT_ENOUGH_USABLE_MEASURES_TO_COMPUTE_THE_APPROXIMATE_COORDINATES_WITH_PLGC}.\r\n";
                }

                if (Use_PLGC)
                {
                    try
                    {
                        plgc = new Plgc1(station, this, this.getTheRightCoordinates, this.getTheRightOption, this.coordianteSystemType);

                        bool runned = plgc.Run(editInput, seeOutput);

                        if (runned)
                        {
                            message += R.T_PLGC_WAS_USED_AND_RAN_SUCCESSFULLY;
                            if (updateStationCoord)
                                Station.Updater.UpdateStationCoordinates(station, plgc);
                            if (updateMeasuredPoints)
                                Station.Updater.UpdateMeasuredPoint(station, plgc.GetOutputCoordinates(), isApproximative: true);
                        }
                        else
                            message += R.T_PLGC_WAS_USED_BUT_THE_COMPUTE_FAILED;
                        success = runned;
                    }
                    catch (Exception ex)
                    {
                        success = false;
                        message += $"{R.T_PLGC_WAS_USED_BUT_THE_COMPUTE_FAILED} : {ex.Message}";
                    }
                }

                // if plgc cannot be used or failed then try with barycenter
                if (!success)
                {
                    bool temp = false;
                    string message2;
                    bool found = Survey.FindApproximatesCoordinates(station, exploitableMeasures, ref temp, out message2);
                    if (found)
                    {
                        message += R.T_TSUNAMI_COMPUTATION_BECAUSE_IMPOSSIBLE_WITH_PLGC_RAN_SUCESSFULLY + "\r\n" + message2;

                        // PLGC is computing the variable points as well, for the moment we have only the station,inb oloc we can use -999.9 with geode, it need to be in the grid
                        List<E.Coordinates> coordinates = new List<E.Coordinates>();
                        E.Point baryCentrePoint = Element.GetBaricentre(station.References, Tsunami2.Properties.Zone);
                        E.Coordinates baryCentreCoord;
                        if (baryCentrePoint._Coordinates.HasCcs)
                            baryCentreCoord = baryCentrePoint._Coordinates.Ccs;
                        else
                            baryCentreCoord = baryCentrePoint._Coordinates.Su;

                        foreach (var item in station.VariablePoints)
                        {
                            E.Coordinates c = baryCentreCoord.Clone() as E.Coordinates;
                            c._Name = item._Name;
                            coordinates.Add(c);
                        }
                        Station.Updater.UpdateMeasuredPoint(station, coordinates, isApproximative: true);
                    }
                    else
                    {
                        message += R.T_TSUNAMI_COMPUTATION_BECAUSE_IMPOSSIBLE_WITH_PLGC_FAILED + "\r\n" + message2;

                    }
                    success = found;
                }
            }
            else
            {
                message += R.T_THE_APPROXIMATED_COORDINATES_WERE_ALREADY_KNOWN;
                success = true;
            }

            if (!success)
            {
                string message2 = $"{R.T_CONTINUE_WITH_LGC}\r\r{message}";
                MessageInput mi = new MessageInput(MessageType.Critical, message2)
                {
                    ButtonTexts = CreationHelper.GetYesNoButtons(),
                    //DontShowAgain = flag_UseBarycenter,
                    Sender = this.ToString()
                };
                var r = mi.Show();

                if (r.TextOfButtonClicked == R.T_NO)
                    success = false;
                else
                    success = true;
            }
            return success;
        }

        private List<Polar.Measure> GetExploitableMeasures(Polar.Station station)
        {
            // Good only
            List<Polar.Measure> measures = new List<Polar.Measure>();
            foreach (Polar.Measure item in station.MeasuresTaken.OfType<Polar.Measure>())
            {
                if (item._Status.GetType() == typeof(M.States.Good))
                {
                    if ((item.Angles.Corrected.Horizontal.Sigma < 200 || double.IsNaN(item.Angles.Corrected.Horizontal.Sigma))
                        && item._OriginalPoint._Coordinates.HasAny
                        && measures.Find(x => x._OriginalPoint._Name == item._OriginalPoint._Name) == null)
                        measures.Add(item);
                }
            }
            return measures;
        }

        private void ChangeStatusOfRemovedMeasures()
        {
            if (this.removedFromCompensation == null) return;
            if (this.removedFromCompensation.Count > 0)
            {
                string bad = $"{R.T_YES}, '{R.T_BAD}'";
                string quest = $"{R.T_YES}, '{R.T_QUESTIONNABLE}'";
                string no = $"{R.T_NO}, '{R.T_GOOD}'";
                string titleAndMessage = $"{R.T_CHANGE_STATUS_OF_PROBLEMATIC_MEASURE};{R.T_THE_COMPUTE_SUCCEED_AFTER_YOU_REMOVED_SOME_MEASUREMENT_DO_YOU_WANT_CHANGE_THE_STATUS_OF_THISTHOSE_MEASURE}";
                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = new List<string> { no , bad, quest }
                };
                string respond = mi.Show().TextOfButtonClicked;

                foreach (var item in this.removedFromCompensation)
                {
                    Polar.Measure found = this.station.MeasuresTaken.Find(x => x._Date == item._Date) as Polar.Measure;
                    if (found != null)
                    {
                        if (respond == bad) found._Status = new M.States.Bad(found._Status.Type);
                        else if (respond == quest) found._Status = new M.States.Questionnable(found._Status.Type);
                    }
                }
            }
        }

        private void ChangeStatusOfAddedMeasures()
        {
            if (this.removedFromCompensation == null) return;
            if (this.removedFromCompensation.Count > 0)
            {
                string bad = $"{R.T_YES}, '{R.T_BAD}'";
                string quest = $"{R.T_YES}, '{R.T_QUESTIONNABLE}'";
                string no = $"{R.T_NO}, '{R.T_GOOD}'";
                string titleAndMessage = $"{R.T_CHANGE_STATUS_OF_PROBLEMATIC_MEASURE};{R.T_THE_COMPUTE_SUCCEED_AFTER_YOU_REMOVED_SOME_MEASUREMENT_DO_YOU_WANT_CHANGE_THE_STATUS_OF_THISTHOSE_MEASURE}";
                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = new List<string> { no , bad, quest }
                };
                string respond = mi.Show().TextOfButtonClicked;

                foreach (var item in this.removedFromCompensation)
                {
                    Polar.Measure found = this.station.MeasuresTaken.Find(x => x._Date == item._Date) as Polar.Measure;
                    if (found != null)
                    {
                        if (respond == bad) found._Status = new M.States.Bad(found._Status.Type);
                        else if (respond == quest) found._Status = new M.States.Questionnable(found._Status.Type);
                    }
                }
            }
        }

        List<Polar.Measure> removedFromCompensation;


        //internal void EditInputMeasureList()
        //{
        //    // Change the station available measurements
        //    //StationTheodolite tempCopy = station.Clone() as StationTheodolite;
        //    removedFromCompensation = new List<M.MeasureTheodolite>();
        //    M.MeasureManager m = new M.MeasureManager(null);
        //    m.AllElements.AddRange(station.MeasuresTaken);
        //    m.View.MultiSelection = true;
        //    m.View.CheckBoxesVisible = true;
        //    m.SelectableObjects.AddRange(m.AllElements.FindAll(x => (x as M.MeasureTheodolite).Angles.Corrected.Horizontal.Sigma != 200 && !((x as M.MeasureTheodolite)._Status is M.States.MeasureStateControl) && !((x as M.MeasureTheodolite)._Status is M.States.MeasureStateBad)));
        //    m._SelectedObjects.AddRange(m.SelectableObjects.FindAll(x => ((x as M.Measure)._Status is M.States.MeasureStateGood) && (x as M.MeasureTheodolite).UsedAsReference && (x as M.MeasureTheodolite)._OriginalPoint._Coordinates.HasAny));
        //    m.Show("m", R.T_UNSELECT_MEASURE_THAT_YOU_DONT_WANT_TO_USE_IN_THE_COMPENSATION);
        //    foreach (var item in m.SelectableObjects)
        //    {
        //        if (m._SelectedObjects.Contains(item))
        //        {
        //            (item as M.MeasureTheodolite).UsedAsReference = true;
        //        }
        //        else
        //        {
        //            removedFromCompensation.Add(item as M.MeasureTheodolite);
        //            (item as M.MeasureTheodolite)._Status = new M.States.MeasureStateBad((item as M.MeasureTheodolite)._Status.type);

        //            (item as M.MeasureTheodolite).UsedAsReference = false;
        //        }
        //    }
        //}

        [XmlIgnore]
        public Lgc2 lgc2;

        internal Polar.Station.Parameters.Setup.Values RunLGC2(bool showLog = true)
        {
            lgc2 = new Lgc2(station, this, getTheRightCoordinates, getTheRightOption, coordianteSystemType);


            bool runned = lgc2.Run(editInput, seeOutput, showLog);

            if (!runned)
            {
                lgc2._Result.Verified = false;
                lgc2._Result.Ready = false;
                return lgc2._Result;
            }


            //ParseOutPutAsTheodoliteResults(lgc2._Output);
            ParseOutPutAsTheodoliteResultsAsJSON(lgc2._Output);

            // speaxcial case when we run all calal to find errors
            if (this._strategy == List.AllCala) return null;

            lgc2._Result.Strategy = lgc2._Strategy;
            if (this.FileShowingStrategy != FileEditionOptions.HideAll)
            {
                string valid = R.T_VALID;
                string message = R.T_C_VALID_ST_SETUP + "\r\n\r\nSigmaZero = " + lgc2._Result.SigmaZero.ToString("F5");
                MessageInput mi = new MessageInput(MessageType.Choice, message)
                {
                    ButtonTexts = new List<string> { valid, R.T_CANCEL }
                };
                lgc2._Result.Verified = (mi.Show().TextOfButtonClicked == valid);

                if (!lgc2._Result.Verified)
                {
                    lgc2._Result.Ready = false;
                    return lgc2._Result;
                }
                else
                    RenameInputFile(lgc2._FileName);
            }
            lgc2._Result.Ready = true;
            lgc2._Result.StationPoint = this.station.Parameters2._StationPoint;
            return lgc2._Result;
        }

        internal Polar.Station.Parameters.Setup.Values RunLGC(bool showLog = true, bool showSigmaO = true)
        {

            lgc2 = new Lgc2(station, this, getTheRightCoordinates, getTheRightOption, coordianteSystemType);

            bool runned = lgc2.Run(editInput, seeOutput, showLog);

            if (!runned)
            {
                lgc2._Result.Verified = false;
                lgc2._Result.Ready = false;
                return lgc2._Result;
            }

            this.ParseOutPutAsTheodoliteResultsAsJSON(lgc2._Output);

            // speaxcial case when we run all calal to find errors
            if (this._strategy == List.AllCala) return null;

            lgc2._Result.Strategy = lgc2._Strategy;

            var stParam = this.station.Parameters2;
            var stPoint = stParam._StationPoint;


            lgc2._Result.StationPointGUID = stPoint.Guid;
            lgc2._Result.Ready = true;
            lgc2._Result.LgcInputContent = lgc2._Input.asText.ToString();
            lgc2._Result.LgcOutputContent = String.Join("\r\n", lgc2._Output._TextOutput.ToArray());
            lgc2._Result.Lgc2ProjectPath = lgc2._FileName;

            return lgc2._Result;
        }

        private void RenameInputFile(string path)
        {
            string dir = System.IO.Path.GetDirectoryName(path);
            string[] filePaths = System.IO.Directory.GetFiles(dir, System.IO.Path.GetFileName(path) + "*", System.IO.SearchOption.AllDirectories);
            string newDir = dir + @"\User Validated Setup";
            System.IO.Directory.CreateDirectory(newDir);
            foreach (var item in filePaths)
                System.IO.File.Move(item, newDir + "\\" + System.IO.Path.GetFileName(item));

        }

        public virtual void ParseOutPutAsTheodoliteResultsAsJSON(Lgc2.Output output)
        {
            string fileName = output.OutputFilePath.ToUpper().Replace(".RES", ".JSON");

            // var test2 = Tsunami2.Properties.Points;
            try
            {
                //string fileName = "C:\\Users\\malgorza\\cernbox\\WINDOWS\\Desktop\\JSON\\example2.json";
                JsonElement jsonFile = Lgc2.Output.ParserJSON.ReadJSONFile(fileName);
                if (lgc2 == null)
                    lgc2 = output.lgc2;

                Lgc2.Output.OutputJSON.SetHeader(jsonFile, output, lgc2);
                Lgc2.Output.OutputJSON.SetSigma0(jsonFile, output, lgc2);
                try
                {
                    Lgc2.Output.OutputJSON.SetInstrumentProperties(jsonFile, output, frameForInstrument?._Name);
                    Lgc2.Output.OutputJSON.SetAH(jsonFile, output, frameForInstrument?._Name);
                    Lgc2.Output.OutputJSON.SetAV(jsonFile, output, frameForInstrument?._Name);
                }
                catch (Exception ex)
                {
                    throw new Exception($"{R.T_CANNOT_PARSE_THE_LGC2_OUTPUT_FILE} for the polar part: '{MessageTsu.GetLink(fileName)}'", ex);
                }

                Lgc2.Output.OutputJSON.SetD(jsonFile, output, frameForInstrument?._Name);
                output._Result.VariablePoints = Lgc2.Output.OutputJSON.GetVariablePoints(jsonFile);
            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_CANNOT_PARSE_THE_LGC2_OUTPUT_FILE}: '{MessageTsu.GetLink(fileName)}'", ex);
            }
        }

        internal void ManageFileEdition()
        {
            switch (FileShowingStrategy)
            {
                case FileEditionOptions.HideAll:
                    editInput = false;
                    seeOutput = false;
                    break;
                case FileEditionOptions.ShowAll:
                    editInput = true;
                    seeOutput = true;
                    break;
                case FileEditionOptions.ShowOutputOnly:
                    editInput = false;
                    seeOutput = true;
                    break;
                default:

                    this._Module.View.AskIfInputEditionIsWanted(ref editInput, ref seeOutput);
                    break;
            }

        }


        internal bool Silent;
        public bool Compute2(bool silent = false, bool autoValidation = false, bool deleteLgcFiles = false)
        {
            try
            {
                this.Silent = silent;
                //this.station = station;

                // Compute
                CompensationResults = null;

                if (!Silent)
                {
                    LaunchView();
                }
                else
                {
                    while (CompensationResults == null)
                    {
                        allComputeFilesPath.Clear();

                        //SetDefaultMeasureSelection(this.manager.AllElements);
                        CheckGeoid();
                        ManageFileEdition();
                        ChooseMeasureForApproximateCompute();
                        ComputeApproximateSetup();
                        ChooseMeasureForCompensation();
                        Compensate();
                        if (autoValidation) ValidateSetup();
                    }
                }
                if (deleteLgcFiles)
                {
                    this.DeleteAllComputationFiles();
                }
                return true;
            }

            catch (Exception ex)
            {
                throw new Exception($"{R.T_COMPENSATION} {this._strategy} {R.T_FAILED}", ex);
            }
        }

        internal List<string> allComputeFilesPath = new List<string>();


        public void SaveFilesPath(string fileName)
        {
            System.IO.FileInfo fi = new System.IO.FileInfo(fileName);
            string pathOfDIrectory = fi.Directory.FullName;
            string fileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(fi.FullName);

            allComputeFilesPath.AddRange(System.IO.Directory.GetFiles(pathOfDIrectory, fileNameWithoutExtension + ".*"));
        }

        private void DeleteAllComputationFiles()
        {
            try
            {
                foreach (string f in allComputeFilesPath)
                {
                    System.IO.File.Delete(f);
                }
            }
            catch (Exception)
            {

            }
        }

        Manager.Module manager;
        internal void LaunchView()
        {
            try
            {
                manager = new Manager.Module(this._Module);
                manager.MultiSelection = true;
                manager.View.CheckBoxesVisible = true;
                foreach (var m in station.MeasuresTaken)
                {
                    manager.AllElements.Add(new CompensationItem()
                    {
                        _Name = m._Point._Name,
                        Measure = m,

                    });
                }

                manager.Station = station;
                manager.Strategy = this;

                string respond = manager.View.ShowView(ConfirmSigmaZero, MakeFirstCompute, ResetTemporary);

                if (respond == R.T_VALID)
                {
                    ValidateSetup();
                }
                else
                {
                    CancelSetup();
                }

                manager.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_COMPENSATION_MANAGER_LAUNCH_FAILED};{ex.Message}", ex);
            }
        }

        private void MakeFirstCompute(object sender, EventArgs e)
        {
            manager.Update(editFiles: false);
        }

        private void ConfirmSigmaZero()
        {
            if (manager.View.selectionChanged)
            {
                manager.Update(false);
            }

            string yes = R.T_YES;
            string no = R.T_NO;

            if (lgc2._Result.SigmaZero > 1)
            {
                string message = $"{R.T_ARE_YOU_SURE};{R.T_DO_YOU_REALLY_VALIDATE_THE_STATION_SETUP_WITH_A_SIGMA0_OF} $BOLD${lgc2._Result.SigmaZero.ToString("F2")}";
                string r = "";
                if (lgc2._Result.SigmaZero > CriticalSigma0)
                {
                    MessageInput mi = new MessageInput(MessageType.Critical, message)
                    {
                        ButtonTexts = new List<string> { yes, no }
                    };
                    r = mi.Show().TextOfButtonClicked;
                }
                else
                {
                    if (lgc2._Result.SigmaZero > ProblematicSigma0)
                    {
                        MessageInput mi = new MessageInput(MessageType.Warning, message)
                        {
                            ButtonTexts = new List<string> { yes, no }
                        };
                        r = mi.Show().TextOfButtonClicked;
                    }
                    else
                    {
                        MessageInput mi = new MessageInput(MessageType.GoodNews, message)
                        {
                            ButtonTexts = new List<string> { yes, no }
                        };
                        r = mi.Show().TextOfButtonClicked;
                    }
                }
                
                if (r == no)
                    manager.View._SelectionView.CancelClosing = true;
                else
                    manager.View._SelectionView.CancelClosing = false;
            }
        }

        private void ResetTemporary()
        {
            station.Parameters2.Setups.ProposedValues = null;
        }

        internal abstract void CheckGeoid();
        internal abstract void ChooseMeasureForApproximateCompute();
        internal abstract void ComputeApproximateSetup();
        internal abstract void ChooseMeasureForCompensation();
        internal abstract void Compensate();
        internal abstract void FindErrors();
        internal abstract string GetSetupLGC2Details();

        internal string GetSigmaComment()
        {
            if (this.CompensationResults == null)
                //   return "No results available";
                return R.T_NO_RESULTS_AVAILABLE;
            if (this.CompensationResults.SigmaZero == Tsunami2.Preferences.Values.na)
                return "Compute failed";
            if (this.CompensationResults.SigmaZero == 0)
                //   return "No redundancy!";
                return R.T_NO_REDUNDANCY;
            if (this.CompensationResults.SigmaZero > CriticalSigma0)
                //   return "You probably have a point naming issue, or a wrong Extension or fixed points are approximated";
                return R.T_YOU_PROBABLY_HAVE_A_POINT_NAMING_ISSUE_OR;
            if (this.CompensationResults.SigmaZero > ProblematicSigma0)
                //   return "Weightings may be too optimistic or fixed points are poorly known";
                return R.T_WEIGHTINGS_MAY_BE_TOO_OPTIMISTIC_OR_FIXED_POINTS_ARE_POORLY_KNOWN;
            else
                return "Acceptable";
        }

        internal string GetSigma()
        {
            if (this.CompensationResults == null)
                return "sigma0 not available";
            else
                return $"{R.T_Sigma0is} $BOLD${CompensationResults.SigmaZero.ToString("F2")}$END$";
        }

        internal abstract string GetConfidenceEllipsoid1Sigma();


        internal void ValidateSetup()
        {
            TSU.Debug.WriteInConsole("Begin ValidateSetup");
            lgc2._Result.Verified = true;
            RenameInputFile(lgc2._FileName);

            ChangeStatusOfRemovedMeasures();
            ChangeStatusOfAddedMeasures();
            this.UpdateMeasureGeodeRoles();

            this.Update();
            TSU.Debug.WriteInConsole("End ValidateSetup");
        }

        private void UpdateMeasureGeodeRoles()
        {
            if (this.manager?.AllElements == null)
                return;
            var compensationItems = this.manager.AllElements;
            foreach (CompensationItem item in compensationItems)
            {
                item.MeasureTheodolite.GeodeRole = item.GeodeRole;
            }
        }

        internal virtual void CancelSetup()
        {
            TSU.Debug.WriteInConsole("Begin CancelSetup");
            E.Manager.Module em = this._Module.ElementManager;
            bool stationIsClosed = this._Module.IsClosed;
            Polar.Station.View view = this._Module.View;

            if (!Polar.Station.Module.CheckStationState(station.Parameters2, stationIsClosed, view)) return;
            station.Parameters2.Setups.ProposedValues = null;
            station.Parameters2.Setups.FinalValues = null;
            station.Parameters2._IsSetup = false;
            em.GetPointsInAllElements().FindAll(x => x._Origin == station._Name && x.State != E.Element.States.Bad).ForEach(x => { x._Coordinates = new E.CoordinatesInAllSystems(); });
            foreach (E.Point x in station.PointsMeasured.Where(x => x._Origin == station._Name))
            {
                x._Coordinates = new E.CoordinatesInAllSystems();
            }
            station.MeasuresTaken.FindAll(x => (x as Polar.Measure)._OriginalPoint._Origin == station._Name).ForEach(x =>
            {
                (x as Polar.Measure)._OriginalPoint._Coordinates = new E.CoordinatesInAllSystems();
            });
            station.MeasuresTaken.ForEach(x =>
            {
                (x as Polar.Measure).GeodeRole = IO.SUSoft.Geode.Roles.Unknown;
                (x as Polar.Measure).Angles.HorizontalState = M.States.Types.Good;
                (x as Polar.Measure).Angles.VerticalState = M.States.Types.Good;
                (x as Polar.Measure).Distance._Status = new M.States.Good();
            });

            em.RemoveElementByNameAndOrigin(station.Parameters2._StationPoint._Name, station._Name);

            E.Point p = station.Parameters2._StationPoint;

            string freeSuffix = Polar.Compensations.FreeStation_LGC2.FreeStationSuffix;
            string newName = p._Name;
            bool wasAknownPointComputeAsFreeStation = false;
            if (p._Name.EndsWith(freeSuffix))
            {
                newName = p._Name.Substring(0, p._Name.Length - 2);
                wasAknownPointComputeAsFreeStation = true;
            }
            if (p._Name.EndsWith(freeSuffix + "."))
            {
                newName = p._Name.Substring(0, p._Name.Length - 3) + ".";
                wasAknownPointComputeAsFreeStation = true;
            }

            Station.Updater.ChangeStationName(station, em, newName);
            // this.StationTheodolite.Parameters2._StationPoint = null; // because if known point from theo, it was change to the point with station name as origin

            if (wasAknownPointComputeAsFreeStation) // the coordinated were lost we have to bring them back
            {
                List<E.Point> theoStationPoints = em.GetPointsInAllElements().FindAll(x => x._Name == newName);
                if (theoStationPoints.Count == 1)
                    station.Parameters2._StationPoint = theoStationPoints[0];
                else // if not point with this name or several we remove the staion point and the user shoudl put uit back himsself...
                    station.Parameters2._StationPoint = null;
            }
            TSU.Debug.WriteInConsole("End CancelSetup");
        }

        internal static Polar.Station.Parameters.Setup.Values Run(Polar.Station.Parameters.Setup.Values trialSetup)
        {
            try
            {
                var module = trialSetup.Module;
                var strategy = GetStrategy(trialSetup.CompensationStrategyType, module);
                Lgc2 lgc2 = new Lgc2(module.StationTheodolite, strategy, createInput:false);
                strategy.Create_LGC2_Input(module.StationTheodolite, "", lgc2, TemporaryCompute: trialSetup.TemporaryCompute);

                if (lgc2.Run(showInput: false, showOutput: false, showLog: false))
                {
                    //strategy.ParseOutPutAsTheodoliteResults(lgc2._Output);
                    strategy.ParseOutPutAsTheodoliteResultsAsJSON(lgc2._Output);


                    trialSetup = Polar.Station.Parameters.Setup.Values.Merge(trialSetup, lgc2._Output._Result);
                }
                else
                    trialSetup.Ready = false;
                return trialSetup;
            }
            catch (Exception ex)
            {
                throw new Exception("Problem running a trial setup with LGC2", ex);
            }
        }

        public Dictionary<string, Dictionary<string, Ellipses.Ellipse>> Points_Accelerator_Ellipses = new Dictionary<string, Dictionary<string, Ellipses.Ellipse>>();

        /// <summary>
        /// LGC provides error ellipdoid along the CCS axis, but the user would rather see them along the axis of the aligned Magnet(s) and sotre them in Strategy.RotatedEllipsoids
        /// </summary>
        internal void ComputeRotatedEllipsoid()
        {
            Points_Accelerator_Ellipses = Compute.Transformation.Ellipses.Compute(this.station, this.lgc2._FileName);
        }
    }
}
