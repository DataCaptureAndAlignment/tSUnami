﻿using MathNet.Numerics;
using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using TSU.Common.Instruments;
using TSU.ENUM;
using TSU.Views;
using TSU.Views.Message;
using static TSU.Common.Compute.Compensations.Frame.FrameMeasurement;
using static TSU.Polar.GuidedModules.Steps.StakeOut.Tools;
using R = TSU.Properties.Resources;

namespace TSU.Common.Compute.Compensations.BeamOffsets
{
    public static partial class WorkFlow
    {
        public static Results Compute(Module module, List<FinalModule> Modules, string idSuAssembly, ObservationType observationType, bool forceSetup = false)
        {
            try
            {
                OffsetComputedInAllSystems.Reset();

                // Gather relevant Modules 
                var modulesWithRoll = Modules.GetByObservationType(observationType, addRollModules: true);

                // Gather relevant measures
                DateTime expirationDate = forceSetup ? DateTime.MinValue : Gathering.GetDateOfLastCompute(modulesWithRoll, idSuAssembly, observationType);

                Gathering.GetDataFrom(modulesWithRoll, idSuAssembly, expirationDate, out GatheredMeasures GatheredMeasures);

                // Original Frames
                Frame originalFrames = Frame.FromGeode_GetOriginalFrames(idSuAssembly);
                Frame originalFramesCopy = null;
                try
                {
                    originalFramesCopy = originalFrames.Clone() as Frame;
                    Checks.ObsAndFrameAreCompatible(originalFramesCopy, GatheredMeasures, out string addtionalMeasurement, out _);
                    if (addtionalMeasurement != "")
                        new MessageInput(MessageType.GoodNews, addtionalMeasurement).Show();
                }
                catch (Exception ex)
                {
                    new MessageInput(MessageType.Warning, ex.Message).Show();
                }

                // Existing parameters
                var existingBocParameters = Tsunami2.Properties.BOC_Context.ExistingAssemblyParameters;

                var results = ComputeCoordinatesAndRoll(module, observationType, GatheredMeasures, idSuAssembly, originalFramesCopy, existingBocParameters, forceSetup);


                if (results.OK)
                {
                    Tsunami2.Properties.BOC_Context.ExistingResults.Add(results);
                    Tsunami2.Properties.BOC_Context.AddObsoleteMeasures(GatheredMeasures);
                    Tsunami2.Properties.Save();
                    ComputeOffset_Async(results);
                }
                else
                {
                    OffsetComputedInAllSystems.Set();
                }
                return results;
            }
            catch (CancelException ex)
            {
                OffsetComputedInAllSystems.Set();
                throw new Exception("Cancelled");
            }
            catch (TsuException ex)
            {
                OffsetComputedInAllSystems.Set();
                throw;
            }
            catch (Exception ex)
            {
                OffsetComputedInAllSystems.Set();
                throw new Exception("Automatic BOC failed", ex);
            }
        }

        private static void ComputeOffset_Async(Results results)
        {
            if (results.Points != null && results.Points.Count > 0)
            {
                var theoreticalPoints = Elements.Manager.Module.GetTheoreticalPointsWithEquivalentNames(results.Points, beamPointsOnly: false);

                {
                    string message = $"Computing offsets...;for {results.AssemblyId} using:\r\n";
                    var mes = results.Frame.Measurements;
                    foreach (var roll in mes.Inclinaisons)
                        if (roll.Used)
                            message += $"Roll on {roll.Point}\r\n";
                    foreach (var obs in mes.ObsXyzs)
                        if (obs.Used)
                            message += $"{results.ObservationType} obs on {obs.Point}\r\n";
                    new MessageInput(MessageType.GoodNews, message).Show();
                }

                ThreadPool.QueueUserWorkItem((state) =>
                {
                    // Perform your heavy work here
                    results.Offsets.AddRange(GetOffsets(results.Points, theoreticalPoints, stationPoint: null));
                    OffsetComputedInAllSystems.Set();
                });
            }
        }

        private static Results ComputeCoordinatesAndRoll(Module moduleLauchingTheComputation, ObservationType observationType, GatheredMeasures gatheredMeasures, string idSuAssembly,
            Frame originalFrames, List<Parameters> existingBocParameters, bool forceSetup = false)
        {
            try
            {
                var parameters = existingBocParameters.GetByAssemblyId(idSuAssembly);
                bool paramsExist = (parameters != null);
                bool trySmooth = false; // Smooth means default paramters (sigma) and all measures pass the criteria and no manual setup is necessary

                if (gatheredMeasures.CountOfNotRollMeasurement(idSuAssembly) == 0)
                    throw new TsuException($"No measurement for '{idSuAssembly}'");

                if (paramsExist)
                {
                    bool wasSmooth = !parameters.ManuallySet;

                    if (Checks.IfAllMeasuredAsBefore(idSuAssembly, gatheredMeasures, observationType, parameters,
                                                        out bool hasMissingMeasures, out string missingMeasureMessage,
                                                        out bool hasExtraMeasures, out string extraMeasureMessage))
                        trySmooth = false;
                    else if (wasSmooth)
                        trySmooth = true;
                    else
                    {
                        forceSetup = true;
                        if (hasExtraMeasures)
                            new MessageInput(MessageType.GoodNews, $"New points not previously measured;{extraMeasureMessage}").Show();
                    }

                    if (hasMissingMeasures)
                    {
                        new MessageInput(MessageType.Warning, $"Missing points previously measured;{missingMeasureMessage}").Show();
                        forceSetup = true;
                    }
                }
                else
                {
                    trySmooth = true;
                }

                if (forceSetup || trySmooth)
                    return DetermineHowAndComputeCoordinates(moduleLauchingTheComputation, existingBocParameters, observationType,
                        gatheredMeasures, idSuAssembly, originalFrames,
                        forceSetup: forceSetup, trySmooth: trySmooth);
                else
                    return ComputeCoordinatesWithGivenParameters(moduleLauchingTheComputation, observationType, gatheredMeasures, idSuAssembly, originalFrames, parameters);
            }
            catch (TsuException ex)
            {
                throw new TsuException($"Beam offsets computation for assembly '{idSuAssembly}' failed", ex);
            }
            catch (Exception ex)
            {
                throw new Exception($"Beam offsets computation for assembly '{idSuAssembly}' failed", ex);
            }
        }
        public static ManualResetEvent OffsetComputedInAllSystems = new ManualResetEvent(false);
        public static List<(string, CoordinatesInAllSystems)> GetOffsets(List<Point> newPoints, List<Point> theoreticalPoints, Point stationPoint)
        {
            List<(string, CoordinatesInAllSystems)> offsets = new List<(string, CoordinatesInAllSystems)>();
            foreach (Point newPoint in newPoints)
            {
                //find the theoric
                Point theoP = theoreticalPoints.Find(x => x._Name == newPoint._Name);
                if (theoP != null)
                {
                    if (!theoP._Parameters.hasGisement || !theoP._Parameters.hasSlope)
                        throw new Exception($"No slope or bearing for '{theoP._Name}';This is needed to compute the Beam offsets");

                    CoordinatesInAllSystems.FillMissingCoordinates(stationPoint, newPoint, theoP, silente: true, Tsunami2.Preferences.Tsunami.Zone, view: null);

                    offsets.Add((newPoint._Name, CoordinatesInAllSystems.ComputeDifferences(newPoint._Coordinates, theoP._Coordinates)));
                }
            }
            return offsets;
        }

        private static Results ComputeCoordinatesWithGivenParameters(Module module, ObservationType observationType,
            GatheredMeasures gatheredMeasures, string idSuAssembly, Frame allOriginalFramesInRootFrame, Parameters parameters)
        {
            Results results = null;
            bool weAreOk = false;
            try
            {
                Checks.ChecksIfEnoughToCompute(idSuAssembly, gatheredMeasures, observationType, out bool computedRoll);

                Parameters.UpdateBasedOnRollMeasure(parameters, gatheredMeasures.Roll);
                results = ComputeInLgc2CoordinatesAndRoll(idSuAssembly, gatheredMeasures, parameters, allOriginalFramesInRootFrame);
                bool residualsOk = Checks.ControlResiduals(results.Frame, out double newYsigma, out bool bSParamWasOut);
                bool orientationOk = Checks.ControlAdjustedFrameOrientation(results.Frame);
                weAreOk = Checks.ControlAdjustedFrameOrientation(results.Frame) && Checks.ControlSigma0andSigmaRY(results);
            }

            catch (TsuCancelException ex)
            {
                // no measure at all
                weAreOk = true;
                results = new Results() { OK = false };
            }
            catch (BocException ex)
            {
                if (ex.Id == 454545 || ex.Id == 454546)
                {
                    string titleAndMessage = $"{ex.Message};" +
                                             $"You have less sockets measured than during the previous computation.\r\n" +
                                             $"Do you want to edit the BOC setup?";
                    var r = new MessageInput(MessageType.Important, titleAndMessage)
                    {
                        ButtonTexts = CreationHelper.GetYesNoButtons()
                    }.Show();

                    if (r.TextOfButtonClicked == R.T_NO)
                    {
                        weAreOk = true;
                        results = new Results() { OK = false };
                        return results;
                    }
                }
                else
                {
                    string titleAndMessage = $"{ex.Message}";
                    new MessageInput(MessageType.Important, titleAndMessage).Show();
                }
                // not enough measure

                weAreOk = false;
                results = new Results() { Parameters = parameters, Frame = allOriginalFramesInRootFrame, OK = false };
            }
            catch (Exception ex)
            {
                string titleAndMessage = $"{idSuAssembly} BO computation failed;BO computation failed with previous parameters";
                new MessageInput(MessageType.Important, titleAndMessage).Show();
                weAreOk = false;
            }

            if (!weAreOk)
                Setup(module, idSuAssembly, gatheredMeasures, InitialResults: results, allOriginalFramesInRootFrame, out results);

            return results;
        }

        private static void RemovePointsFromGivenAssembly(List<Point> points, string assembly)
        {
            foreach (var point in points)
            {
                if (point._Name.Contains(assembly))
                    points.Remove(point);
            }
        }

        private static Results DetermineHowAndComputeCoordinates(
            Module module,
            List<Parameters> existingParameters,
            ObservationType observationType,
            GatheredMeasures gatheredMeasures,
            string idSuAssembly,
            Frame allOriginalFramesInRootFrame,
            bool forceSetup = false,
            bool trySmooth = false)
        {
            Results results = null;
            Parameters tempParameters = null;
            try
            {
                // Check feasibility
                Checks.ChecksIfEnoughToCompute(idSuAssembly, gatheredMeasures, observationType, out bool isComputedRoll);

                // Get/Create and update parameters
                Parameters initialParams = Parameters.GetExistingOrCreateDefaultOne(existingParameters, idSuAssembly, gatheredMeasures,
                    isComputedRoll, out bool parametersWereExisting, allOriginalFramesInRootFrame, checkNumberOfSockets: trySmooth);

                tempParameters = initialParams.Clone() as Parameters;
                Parameters.UpdateBasedOnRollMeasure(tempParameters, gatheredMeasures.Roll);
                Parameters.UpdateBasedOnOtherMeasures(tempParameters, gatheredMeasures);

                Results results1 = null;
                Results results2 = null;
                if (parametersWereExisting && !trySmooth)
                {
                    results = ComputeInLgc2CoordinatesAndRoll(idSuAssembly, gatheredMeasures, tempParameters, allOriginalFramesInRootFrame);
                }
                else
                {
                    // first compute
                    tempParameters = Parameters.ModifySY(initialParams, newYsigma: 0.010);
                    results1 = ComputeInLgc2CoordinatesAndRoll(idSuAssembly,
                    gatheredMeasures, tempParameters, allOriginalFramesInRootFrame);

                    // if check not passed or manually validated, compute stops
                    bool residualsOk = Checks.ControlResiduals(results1.Frame, out double newYsigma, out bool bSParamWasOut);
                    bool orientationOk = Checks.ControlAdjustedFrameOrientation(results1.Frame);
                    tempParameters = Parameters.ModifySY(tempParameters, newYsigma: newYsigma < 0.0002 ? newYsigma = 0.0002 : newYsigma);
                    if (!residualsOk || !orientationOk) // deny changes
                    {
                        //tempParameters = initialParams;
                        results1.Parameters = tempParameters;
                        results = results1;
                        throw new BocException("Residuals or orientation not OK");
                    }

                    if (bSParamWasOut) throw new BocException("Compute #1: controls failed"); // if S>7mm then no compute 2 but customization


                    // second compute
                    results2 = ComputeInLgc2CoordinatesAndRoll(idSuAssembly, gatheredMeasures, tempParameters, allOriginalFramesInRootFrame);


                    // if already good no need for customization
                    if (Checks.ControlAdjustedFrameOrientation(results2.Frame) && Checks.ControlSigma0andSigmaRY(results2))
                    {
                        results = results2;
                    }
                    else
                    {
                        forceSetup = true;
                        results = results1;
                    }

                    // we save this result1 or 2 as the default one
                    Tsunami2.Properties.BOC_Context.ExistingAssemblyDefaultParameters.ReplaceExisting(results.Parameters);
                }
            }
            catch (TsuCancelException ex)
            {
                // no measure at all
                results = new Results() { OK = false };
            }
            catch (BocException ex)
            {
                // not enough measure
                string titleAndMessage = $"{ex.Message}";
                new MessageInput(MessageType.Important, titleAndMessage).Show();
                if (results == null)
                    results = new Results() { OK = false };
                else
                    results.OK = false;
                forceSetup = true;
            }
            catch (Exception ex)
            {
                if (results == null)
                    results = new Results()
                    {
                        OK = false,
                        Parameters = tempParameters
                    };
                forceSetup = true;
            }
            if (forceSetup && results?.Parameters != null)
            { 
                if (!Setup(module, idSuAssembly, gatheredMeasures, InitialResults: results, allOriginalFramesInRootFrame, out results))
                {
                    results.OK = false;
                    return results; // deny changes
                }
            }
            return results;
        }

        private static DoubleValue Get_dRollFromRY(Frame.Parameters.Parameter rY)
        {
            return new DoubleValue(
                rY.Value / 200 * Math.PI * 1000,
                rY.Sigma / 200 * Math.PI * 1000
                );
        }

        private static bool Setup(Module module, string idSuAssembly, GatheredMeasures measures,
            Results InitialResults, Frame framesFileContent,
            out Results results)
        {
            results = null;

            // clone previous results to possibly change them if later validated by user
            Results tempResults = (InitialResults != null) ? InitialResults.Clone() as Results : new Results();

            // Select parameter to use
            if (tempResults.Parameters == null)
                tempResults.Parameters = Parameters.GetExistingOrDefaultOne(idSuAssembly);

            Results ValidatedResults = null;

            List<CompensationItem> CompensationItems = Setup_CreateCompensationItems(measures, tempResults.Parameters);
            //tempResults.Parameters = Parameters.UpdateBasedOnCompensationItem(CompensationItems, tempResults.Parameters);

            Compensations.Manager.Module cm = null;
            Lgc2 lgc2 = new Lgc2() { _Input = new Lgc2.Input(), _Output = new Lgc2.Output() }; //for compatibility with manager common button

            Action<bool> Compensation = (editInput) =>
            {
                bool recomputeWithInitialParameters = (tempResults == null) ? true : tempResults.RecomputeWithInitialParameters;
                if (recomputeWithInitialParameters)
                {
                    if (tempResults != null) tempResults.RecomputeWithInitialParameters = false;
                    CompensationItems.UpdateFromParameters(tempResults.Parameters);
                }
                CompensationItems.UpdateFromParameters(tempResults.Parameters);
                //tempResults.Parameters = Parameters.UpdateBasedOnCompensationItem(CompensationItems, tempResults.Parameters);

                bool computeAnyWay = true;
                if (!Checks.MinimumRequireIsPresent(tempResults.Parameters, measures, out string message, out bool lookLikeTiltMeasuredWithLevelling))
                {
                    new MessageInput(MessageType.Critical, message).Show();
                    computeAnyWay = false;
                    if (cm.Tag is Results res)
                        res.OK = false;
                }

                if (computeAnyWay)
                {
                    try
                    {
                        var temp2Results = ComputeInLgc2CoordinatesAndRoll(idSuAssembly, measures,
                            tempResults.Parameters, framesFileContent, editInput);
                        lgc2._FileName = tempResults.LgcProjectPath;
                        lgc2._Input.lgc2 = lgc2;
                        lgc2._Output.lgc2 = lgc2;
                        tempResults = temp2Results;
                        CompensationItem.UpdateMeasures(measures, CompensationItems);
                        CompensationItem.UpdateFromFrame(CompensationItems, tempResults.Frame);
                        cm.Tag = tempResults;
                    }
                    catch (Exception ex)
                    {
                        new MessageInput(MessageType.Critical, ex.Message).Show();
                        tempResults.OK = false;
                    }


                    // to insure that the news composite element create from compute only are visible:
                    cm.AllElements.Clear();
                    cm.SelectableObjects.Clear();
                    cm.AllElements.AddRange(CompensationItems);
                }
                cm.UpdateView();
            };
            Action Validate = () =>
            {
                string message = "Keep?;Keep this setup?";
                System.Drawing.Color color = System.Drawing.Color.Transparent;
                if (cm.View.Tag is Tuple<string, System.Drawing.Color> c)
                {
                    message = $"Keep this setup?;{c.Item1}";
                    color = c.Item2;
                }

                var answer = new MessageInput(MessageType.Important, message)
                {
                    ButtonTexts = new List<string>() { R.T_OK, R.T_Reject },
                    Color = color
                }.Show();

                if (answer.TextOfButtonClicked == R.T_OK)
                {
                    tempResults.OK = true;
                    tempResults.Parameters.ManuallySet = true;
                }
                else
                    tempResults.OK = false;
                ValidatedResults = tempResults;
                // CompensationItem.UseToUpdateBOParameters(CompensationItems, tempParameters);
            };
            Action Cancel = () =>
            {
                tempResults.OK = false;
                ValidatedResults = tempResults;
            };
            Action<object, object> EditItem = (compenasationIitem, property) =>
            {
                if (compenasationIitem is CompensationItem frameItem)
                {
                    if (frameItem.StateAndMeasure == null)
                        return;

                    if (property is bool isUsed) // bool is only used for the frameItem.Used
                    {
                        var meas = frameItem.StateAndMeasure.Measure;
                        if (meas._Status.Type == Measures.States.Types.Bad)
                        {
                            if (new MessageInput(MessageType.Choice, "Turn measure to 'Good'?")
                            {
                                ButtonTexts = new List<string>() { R.T_YES, R.T_NO }
                            }.Show().TextOfButtonClicked == R.T_NO)
                                return;

                            meas._Status = new Measures.States.Good(Measures.States.Types.Bad);
                        }
                        else if (meas._Status.Type == Measures.States.Types.Good && meas._Status.PreviousType == Measures.States.Types.Bad)
                        {
                            if (new MessageInput(MessageType.Choice, "Turn back measure to 'Bad'?")
                            {
                                ButtonTexts = new List<string>() { R.T_YES, R.T_NO }
                            }.Show().TextOfButtonClicked == R.T_NO)
                                return;

                            meas._Status = new Measures.States.Bad();
                        }

                        frameItem.Used = !isUsed;
                        frameItem.StateAndMeasure.IsUsed = !isUsed;
                        //if (frameItem.ObsXyz!=null)
                        //    frameItem.ObsXyz.Used = frameItem.Used;
                        //if (frameItem.Incly != null) 
                        //    frameItem.Incly.Used = frameItem.Used;
                    }
                    if (property is Frame.FrameMeasurement.Observation obsxyz) // only sigma are the double values to be modified
                    {
                        if (DoubleValue.ReferenceEquals(obsxyz, frameItem.ObsXyz.X))
                        {
                            if (cm.View.AskNumericalValue("Sigma X/R;Enter the new value in mm", frameItem.ObsXyz.X.Sigma * 1000, out double newValue))
                            {
                                frameItem.ObsXyz.X.Sigma = newValue / 1000;
                                cm.UpdateView();
                            }
                        }
                        else if (DoubleValue.ReferenceEquals(obsxyz, frameItem.ObsXyz.Y))
                        {
                            if (cm.View.AskNumericalValue("Sigma Y/S;Enter the new value in mm", frameItem.ObsXyz.Y.Sigma * 1000, out double newValue))
                            {
                                frameItem.ObsXyz.Y.Sigma = newValue / 1000;
                                cm.UpdateView();
                            }
                        }
                        else if (DoubleValue.ReferenceEquals(obsxyz, frameItem.ObsXyz.Z))
                        {
                            if (cm.View.AskNumericalValue("Sigma Z/T;Enter the new value in mm", frameItem.ObsXyz.Z.Sigma * 1000, out double newValue))
                            {
                                frameItem.ObsXyz.Z.Sigma = newValue / 1000;
                                cm.UpdateView();
                            }
                        }
                    }
                }
                tempResults.Parameters = Parameters.UpdateBasedOnCompensationItem(CompensationItems, tempResults.Parameters);
            };

            // Setup the BOC manager
            bool isAlreadyACompensatationManger;
            if (module is Compensations.Manager.Module cmm)
            {
                cm = cmm;
                isAlreadyACompensatationManger = true;
            }
            else
            {
                cm = new Compensations.Manager.Module(module);
                isAlreadyACompensatationManger = false;
            }

            cm.IsBeamOffsetsFrameCompensation = true;
            cm.Tag = tempResults;
            cm.Compensate_Action = Compensation;
            cm.EditItem_Action = EditItem;

            cm.Validate_Action = Validate;
            cm.Strategy = new Strategies.BeamOffsets() { lgc2 = lgc2 };
            cm.AllElements.AddRange(CompensationItems);
            cm.View.HideEmptyColumns = false;
            cm.MultiSelection = false;
            cm.Update(); // this force the first compensation
            cm.UpdateView();

            string r;
            if (isAlreadyACompensatationManger)
            {
                cm.Compensate_Action(false);
                cm.View.ChangeStrategy(Common.Strategies.Views.Types.List);
                Validate();
                cm.UpdateView();
                r = R.T_OK;
            }
            else
            {
                var type = measures.GetObservationType();
                r = cm.View.ShowInMessageTsu($"Setup of Beam points offsets computation for '{type} type' observations",
                    R.T_OK, Validate, R.T_CANCEL, Cancel);
            }

            bool manuallySetupValidated = r == R.T_OK;

            results = ValidatedResults;

            if (manuallySetupValidated)
            {
                // this will get the latest ones or null if it was cancelled

                results.OK = true;
                Tsunami2.Properties.BOC_Context.ExistingAssemblyParameters.ReplaceExisting(results.Parameters);
                Tsunami2.Properties.Save();
            }
            else
                results.OK = false;

            return manuallySetupValidated;
        }

        private static List<CompensationItem> Setup_CreateCompensationItems(GatheredMeasures measures, Parameters clonedInputParameters)
        {
            List<CompensationItem> CompensationItems = new List<CompensationItem>();
            foreach (var stationsAndMeasures in measures.Polar)
            {
                foreach (var stateAndMeasure in stationsAndMeasures.MeasuresWithStatus)
                {
                    var obsXyz = clonedInputParameters.GetOneObsXyz(stateAndMeasure.Measure._Point._Name);

                    bool usedAccordingToTheMeasure = stateAndMeasure.IsUsed;
                    bool usedAccordingToTheParameters = obsXyz.Used; 
                    bool used = usedAccordingToTheParameters ? usedAccordingToTheMeasure: false;

                    CompensationItems.Add(new CompensationItem()
                    {
                        Used = used,
                        StateAndMeasure = stateAndMeasure,
                        ObsXyz = obsXyz
                    });
                }
            }
            foreach (var stationsAndMeasures in measures.Ecarto)
            {
                foreach (var stateAndMeasure in stationsAndMeasures.Item2)
                {
                    var obsXyz = clonedInputParameters.GetOneOrDefaultObsXyz(stateAndMeasure.Measure._Point._Name);

                    bool usedAccordingToTheMeasure = stateAndMeasure.IsUsed;
                    bool usedAccordingToTheParameters = obsXyz.Used;
                    bool used = usedAccordingToTheParameters ? usedAccordingToTheMeasure : false;

                    CompensationItems.Add(new CompensationItem()
                    {
                        Used = used,
                        StateAndMeasure = stateAndMeasure,
                        ObsXyz = obsXyz,
                    }); ;
                }
            }
            foreach (var stationAndMeasures in measures.Level)
            {
                foreach (var stateAndMeasure in stationAndMeasures.Item2)
                {
                    var obsXyz = clonedInputParameters.GetOneOrDefaultObsXyz(stateAndMeasure.Measure._Point._Name);

                    bool usedAccordingToTheMeasure = stateAndMeasure.IsUsed;
                    bool usedAccordingToTheParameters = obsXyz.Used;
                    bool used = usedAccordingToTheParameters ? usedAccordingToTheMeasure : false;

                    CompensationItems.Add(new CompensationItem()
                    {
                        Used = used,
                        StateAndMeasure = stateAndMeasure,
                        ObsXyz = obsXyz,
                    }); ;
                }
            }
            foreach (var stateAndMeasure in measures.Roll)
            {
                var incly = clonedInputParameters.GetOneOrDefaultIncly(stateAndMeasure.Measure._Point._Name);

                bool usedAccordingToTheMeasure = stateAndMeasure.IsUsed;
                bool usedAccordingToTheParameters = incly.Used;
                bool used = usedAccordingToTheParameters ? usedAccordingToTheMeasure : false;

                CompensationItems.Add(new CompensationItem()
                {
                    Used = used,
                    StateAndMeasure = stateAndMeasure,
                    Incly = incly,
                    _Name = stateAndMeasure.Measure._Point._Name,
                }); ;
            }
            return CompensationItems;
        }

        private static Results ComputeInLgc2CoordinatesAndRoll(string idSuAssembly,
            GatheredMeasures measures, Parameters parameters, Frame allOriginalAssemblyFrames,
            bool editInput = false, string computationName = "")
        {
            if (computationName != "") computationName = ", " + computationName;
            Frame originalAssemblyFrame = Frame.GetOriginalAssemblyFrame(idSuAssembly, parameters, allOriginalAssemblyFrames);

            Frame modifiedFrame = Frame.ModifyObsxyzOfAllFramesBasedOnParameters(idSuAssembly, originalAssemblyFrame, parameters);
            modifiedFrame = Frame.ModifyInclyOfAllFrameBasedOnParameters(idSuAssembly, modifiedFrame, parameters);


            string inputContent = Lgc2.Input.BOC_Create(measures, modifiedFrame, out string proposedName);

            string folderPath = $@"{Tsunami2.Preferences.Values.Paths.MeasureOfTheDay}computes\(P)LGC\BOC\";
            if (!Directory.Exists(folderPath)) Directory.CreateDirectory(folderPath);

            string lgcProjectPath = $@"{folderPath}{Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now)} {computationName} {proposedName}";
            lgcProjectPath = lgcProjectPath.Trim();

            string lgcInputPath = lgcProjectPath + ".INP";
            System.IO.File.WriteAllText(lgcInputPath, inputContent);
            if (editInput)
            {
                TSU.Shell.ExecutePathInDialogView(lgcInputPath, "SurveyPad");
            }

            if (Lgc2.RunInput(lgcInputPath, out _, reRunWithEditionIfFailed: false))
            {
                //if (Frame.Results.GetFrameResultsFromJson(lgcPath + ".JSON", idSuAssembly);
                Frame frames = new Frame() { _Name = modifiedFrame.DeeperFrame._Name };
                Frame.FromJson_GetFrameResults(lgcProjectPath + ".JSON", frameName: modifiedFrame.DeeperFrame._Name, ref frames, out double sigma0, out List<Point> points);

                Results results = new Results();
                results.ObservationType = measures.GetObservationType();
                results.LgcProjectPath = lgcProjectPath;
                results.Sigma0 = sigma0;

                results.Points = points.GetByNameParts(new List<string>() { idSuAssembly, "BEAM_" });

                results.Points = points.RemoveByNameParts(new List<string>() { "STLEV_" });
                results.dRoll = Get_dRollFromRY(frames.DeeperFrame.EstimatedParameters.RY);
                results.OK = true;
                results.Date = DateTime.Now;
                results.Frame = frames;
                results.Parameters = parameters;
                results.AssemblyId = idSuAssembly;
                return results;
            }
            else
                throw new BocException($"LGC2 compensation with frame for {idSuAssembly} did run into a problem");

        }

        internal static class Buttons
        {
            internal static BigButton GetMenuButton(Action action)
            {
                return new BigButton("Beam Offsets;Compute, setup the compute, ...", R.BOC, action, color: null, hasSubButtons: true);
            }

            internal static BigButton GetComputeButton(Action action)
            {
                BigButton button = new BigButton("Compute Beam Offsets", R.BOC_COMPUTE);
                button.BigButtonClicked += delegate
                {
                    action();
                };
                button.HasSubButtons = false;
                return button;
            }

            internal static BigButton GetSetupButton(Action action)
            {
                BigButton button = new BigButton("Setup Beam Offsets compute", R.BOC___SETUP);
                button.BigButtonClicked += delegate
                {
                    action();
                };
                button.HasSubButtons = false;
                return button;
            }
        }

        internal static List<string> DetermineAssemblyIds(List<Magnet> magnetsToBeAligned)
        {
            List<string> ids = new List<string>();
            foreach (var item in magnetsToBeAligned)
            {
                string input = item._Name;
                //int lastIndex = input.LastIndexOf('.');
                //string result = input.Substring(0, lastIndex);
                if (!ids.Contains(input))
                {
                    ids.Add(input);
                }
            }

            if (ids.Count == 0)
                throw new TsuException("No magnets to aligned;May be your selected points where used as resection references?");
            return ids;
        }

        internal static string ResultsToString(string name, List<(string, CoordinatesInAllSystems)> coords, DoubleValue roll)
        {
            string message = $"{name} BOC:;";

            foreach (var item in coords)
            {
                if (item.Item1.StartsWith("BEAM_"))
                {
                    message += item.Item1 + "\t";
                    message += item.Item2.ToString(showInMm: true, displayOnlySelectedSystem: true) + "\r\n";
                }
            }
            message += $"{name}: {new DoubleValue(roll / 1000).ToString("m-mm")} mRad\r\n";

            return message;
        }

    }
    public class BocException : TsuException
    {
        public BocException(string message) : base(message) { }

        public BocException(string message, Exception ex) : base(message, ex) { }
    }
}
