﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Xml.Serialization;
using TSU.Common.Measures;
using TSU.Views;
using TSU.Views.Message;
using static TSU.Common.Compute.Compensations.Frame;
using static TSU.Common.Compute.Compensations.Frame.FrameMeasurement;

namespace TSU.Common.Compute.Compensations.BeamOffsets
{
    [XmlType(TypeName = "BeamOffsets.Parameters")]
    public class Parameters : ICloneable
    {
        public string IdSuAssembly = "";
        public List<Frame.FrameMeasurement.ObsXYZ> PointsToUseAsOBSXYZ = new List<Frame.FrameMeasurement.ObsXYZ>();
        public List<Frame.FrameMeasurement.Incly> Inclys = new List<Frame.FrameMeasurement.Incly>();
        //public Frame OrigninalAssemblyFrame = null;
        public bool ManuallySet = false;
        public Guid Guid = Guid.NewGuid();

        public Parameters()
        {
        }

        public Frame.FrameMeasurement.ObsXYZ GetOneObsXyz(string pointName)
        {
            return PointsToUseAsOBSXYZ.Find(x => x.Point == pointName);
        }

        public Frame.FrameMeasurement.ObsXYZ GetOneOrDefaultObsXyz(string pointName)
        {
            var obsxyz = GetOneObsXyz(pointName);
            if (obsxyz == null)
            {
                obsxyz = new FrameMeasurement.ObsXYZ()
                {
                    Used = true,
                    Point = pointName,
                };
            }
            return obsxyz;
        }

        public Frame.FrameMeasurement.Incly GetOneIncly(string pointName)
        {
            return Inclys.Find(x => x.Point == pointName);
        }

        public Frame.FrameMeasurement.Incly GetOneOrDefaultIncly(string pointName)
        {
            var incly = GetOneIncly(pointName);
            if (incly == null)
            {
                incly = new FrameMeasurement.Incly()
                {
                    Point = pointName,
                    Used = true,
                };
            }
            return incly;
        }

        public object Clone()
        {
            Parameters clone = this.MemberwiseClone() as Parameters;
            clone.PointsToUseAsOBSXYZ = new List<Frame.FrameMeasurement.ObsXYZ>(); this.PointsToUseAsOBSXYZ.ForEach(x => { clone.PointsToUseAsOBSXYZ.Add(x.Clone() as Frame.FrameMeasurement.ObsXYZ); });
            clone.Guid = Guid.NewGuid();
            return clone;
        }



        internal static Parameters DetermineDefaultOnes(string idSuAssembly, GatheredMeasures GatheredMeasures,
            bool computedRoll, Frame rootWithAllTheoreticalFrames)
        {
            Parameters parameters = new Parameters() { IdSuAssembly = idSuAssembly };
            //parameters.OrigninalAssemblyFrame = Frame.GetAssemblyFrameByName(rootWithAllTheoreticalFrames, idSuAssembly);
            Frame lastFrame = Frame.GetAssemblyLastFrameByName(rootWithAllTheoreticalFrames, idSuAssembly);

            foreach (var obsXyz in lastFrame.Measurements.ObsXyzs)
            {
                obsXyz.Used = GatheredMeasures.ContainsGoodRollMeasureFor(obsXyz.Point);
                parameters.PointsToUseAsOBSXYZ.Add(obsXyz);
            }

            foreach (var incly in lastFrame.Measurements.Inclinaisons)
            {
                incly.Used = GatheredMeasures.ContainsGoodRollMeasureFor(incly.Point);
                parameters.Inclys.Add(incly);
            }

            return parameters;
        }

        
        internal static Parameters DetermineDefaultOnes_old(string idSuAssembly, GatheredMeasures GatheredMeasures,
            bool computedRoll, Frame rootWithAllTheoreticalFrames)
        {
            Parameters p = new Parameters() { IdSuAssembly = idSuAssembly };
            //p.OrigninalAssemblyFrame = Frame.GetAssemblyFrameByName(rootWithAllTheoreticalFrames, idSuAssembly);
            Frame lastFrame = Frame.GetAssemblyLastFrameByName(rootWithAllTheoreticalFrames, idSuAssembly);

            foreach (var item in GatheredMeasures.Polar)
            {
                foreach (MeasureWithStatus items in item.Item2)
                {
                    bool used = items.IsUsed;
                    var m = items.Measure;
                    if (m._Status is Measures.States.Good || m._Status is Measures.States.Questionnable)
                    {
                        FrameMeasurement.ObsXYZ obsXyz = lastFrame.Measurements.ObsXyzs.Find(x => x.Point == m._Point._Name);
                        if (obsXyz == null)
                            MessageTsu.ShowMessage(new Exception($"Cannot get the OBSXYZ from the theoretical frame for '{m._Point._Name}'"));
                        else
                            p.PointsToUseAsOBSXYZ.Add(obsXyz);
                    }
                }
            }
            foreach (var item in GatheredMeasures.Ecarto)
            {
                foreach (MeasureWithStatus items in item.Item2)
                {
                    bool used = items.IsUsed;
                    var m = items.Measure;
                    if (m._Status is Measures.States.Good || m._Status is Measures.States.Questionnable)
                    {
                        FrameMeasurement.ObsXYZ obsXyz = lastFrame.Measurements.ObsXyzs.Find(x => x.Point == m._Point._Name);
                        if (obsXyz == null)
                            MessageTsu.ShowMessage(new Exception($"Cannot get the OBSXYZ from the theoretical frame for '{m._Point._Name}'"));
                        else
                            p.PointsToUseAsOBSXYZ.Add(obsXyz);
                    }
                }
            }
            foreach (var item in GatheredMeasures.Level)
            {
                foreach (MeasureWithStatus items in item.Item2)
                {
                    bool used = items.IsUsed;
                    var m = items.Measure;
                    if (!(m._Status is Measures.States.Bad))
                    {
                        FrameMeasurement.ObsXYZ obsXyz = lastFrame.Measurements.ObsXyzs.Find(x => x.Point == m._Point._Name);
                        if (obsXyz == null)
                            MessageTsu.ShowMessage(new Exception($"Cannot get the OBSXYZ from the theoretical frame for '{m._Point._Name}'"));
                        else
                            p.PointsToUseAsOBSXYZ.Add(obsXyz);
                    }
                }
            }
            foreach (var items in GatheredMeasures.Roll)
            {
                bool used = items.IsUsed;
                var m = items.Measure;
                if (m._Status is Measures.States.Good || m._Status is Measures.States.Questionnable)
                {
                    FrameMeasurement.Incly incly = lastFrame.Measurements.Inclinaisons.Find(x => x.Point == m._Point._Name);
                    if (incly == null)
                        MessageTsu.ShowMessage(new Exception($"Cannot get the INCLYS from the theoretical frame for '{m._Point._Name}'"));
                    else
                    {
                        p.Inclys.Add(incly);
                    }
                }
            }
            return p;
        }

        internal static Parameters GetByName(List<Parameters> existingParameters, string idSuAssembly)
        {
            return existingParameters.FindLast(x => x.IdSuAssembly == idSuAssembly);
        }

        internal static Parameters GetExistingOrDefaultOne(string idSuAssembly)
        {
            var found = Tsunami2.Properties.BOC_Context.ExistingAssemblyParameters.GetByAssemblyId(idSuAssembly);
            if (found == null)
                found = Tsunami2.Properties.BOC_Context.ExistingAssemblyDefaultParameters.GetByAssemblyId(idSuAssembly);

            return found;
        }

        internal static Parameters GetExistingOrCreateDefaultOne(List<Parameters> existingParameters, string idSuAssembly,
            GatheredMeasures GatheredMeasures,
            bool computedRoll, out bool wasExisting, Frame rootWithTheoFrame, bool checkNumberOfSockets)
        {
            var existing = existingParameters.GetByAssemblyId(idSuAssembly);
            wasExisting = existing != null;
            if (wasExisting)
            {
                if (!checkNumberOfSockets)
                    return existing;
            }

            var defaultParameters = Parameters.DetermineDefaultOnes(idSuAssembly, GatheredMeasures, computedRoll, rootWithTheoFrame);


            Tsunami2.Properties.BOC_Context.ExistingAssemblyDefaultParameters.ReplaceExisting(defaultParameters);
            return defaultParameters;
        }


        internal static Parameters ModifySY(Parameters param, double newYsigma)
        {
            Parameters p = param.Clone() as Parameters;
            foreach (var item in p.PointsToUseAsOBSXYZ)
            {
                item.Y.Sigma = newYsigma;
            }
            return p;
        }



        internal static void ReplaceExisting(List<Parameters> existingParameters, List<Parameters> newParametersSet)
        {
            foreach (var newParameters in newParametersSet)
                existingParameters.ReplaceExisting(newParameters);
        }

        public override string ToString()
        {
            int obsXyzCount = 0;
            int usedObsXyzCount = 0;
            int inclyCount = 0;
            int usedInclyCount = 0;
            foreach (var item in this.PointsToUseAsOBSXYZ)
            {
                obsXyzCount++;
                if (item.Used)
                    usedObsXyzCount++;
            }
            foreach (var item in this.Inclys)
            {
                inclyCount++;
                if (item.Used)
                    usedInclyCount++;
            }
            return $"BOC parameters for {this.IdSuAssembly}, {usedObsXyzCount}/{obsXyzCount} obsxyz, {usedInclyCount}/{inclyCount} inclys.";
        }


        internal static void UpdateBasedOnRollMeasure(Parameters parameters, List<MeasureWithStatus> rollMeasures)
        {
            foreach (var rm in rollMeasures)
            {
                bool used = rm.IsUsed;
                var measure = rm.Measure as Tilt.Measure;
                var incly = parameters.GetOneIncly(rm.Measure._Point._Name);
                if (incly != null)
                {
                    incly.Used = used;
                    incly.Instrument = measure._InstrumentSN;
                    incly.Roll = new Frame.FrameMeasurement.Observation() { Value = measure._AverageBothDirection.Value };
                    incly.dRoll = new Frame.FrameMeasurement.Observation() { Value = measure._Ecart.Value };
                }
                else
                    parameters.Inclys.Add(new Frame.FrameMeasurement.Incly()
                    {
                        Used = used,
                        Instrument = measure._InstrumentSN,
                        Point = measure._Point._Name,
                        Roll = new Frame.FrameMeasurement.Observation() { Value = measure._AverageBothDirection.Value },
                        dRoll = new Frame.FrameMeasurement.Observation() { Value = measure._Ecart.Value }
                    });
            }

            if (rollMeasures.Count == 0)
            {
                foreach (var incly in parameters.Inclys)
                {
                    incly.Used = false;
                }
            }
        }

        /// <summary>
        /// modify the incly and obs based on the selected measures
        /// </summary>
        /// <param name="compensationItems"></param>
        /// <param name="parameters"></param>
        internal static Parameters UpdateBasedOnCompensationItem(List<CompensationItem> compensationItems, Parameters initialParameters)
        {
            if (initialParameters == null)
                return null;

            Parameters parameters = initialParameters?.Clone() as Parameters;

            // First Create Parameters if they don't exist
            CreateMissingParametersBasedOnCompensationItems(compensationItems, parameters);

            // Update existing
            UpdateParametersBasedOnCompensationItems(compensationItems, parameters);

            return parameters;
        }

        private static void UpdateParametersBasedOnCompensationItems(List<CompensationItem> compensationItems, Parameters parameters)
        {
            // .Inlcys
            foreach (var parameter in parameters.Inclys)
            {
                var compensationItem = compensationItems.GetInclyByPointName(parameter.Point);
                parameter.Used = compensationItems.ContainsAUsedTiltMeasureOfPointNamed(parameter.Point, out Tilt.Measure measure);
                if (parameter.Used)
                {
                    parameter.Instrument = measure._InstrumentSN;
                    parameter.Roll = new Frame.FrameMeasurement.Observation() { Value = measure._AverageBothDirection.Value, Sigma = compensationItem.Incly.Roll.Sigma };
                    parameter.dRoll = new Frame.FrameMeasurement.Observation() { Value = measure._Ecart.Value, Sigma = compensationItem.Incly.Roll.Sigma };
                }
            }

            var na = Tsunami2.Preferences.Values.na;
            // .OBSXYZs
            foreach (var parameter in parameters.PointsToUseAsOBSXYZ)
            {
                var compensationItem = compensationItems.GetObsXyzByPointName(parameter.Point);
                parameter.Used = compensationItems.ContainsAUsedNoneTiltMeasureOfPointNamed(parameter.Point, out Measures.Measure measure);

                if (compensationItem != null)
                {
                    parameter.X.Sigma = compensationItem?.ObsXyz?.X?.Sigma ?? 1; // 1m
                    parameter.Y.Sigma = compensationItem?.ObsXyz?.Y?.Sigma ?? 1;
                    parameter.Z.Sigma = compensationItem?.ObsXyz?.Z?.Sigma ?? 1;
                }
            }
        }

        private static void CreateMissingParametersBasedOnCompensationItems(List<CompensationItem> compensationItems, Parameters parameters)
        {
            // paramaters.Inlcys
            foreach (var item in compensationItems.GetUsedTiltMeasures())
            {
                if (parameters.GetOneIncly(item._Point._Name) == null)
                {
                    parameters.Inclys.Add(new Frame.FrameMeasurement.Incly()
                    {
                        Used = true,
                        Instrument = item._InstrumentSN,
                        Point = item._Point._Name,
                        Roll = new Frame.FrameMeasurement.Observation() { Value = item._AverageBothDirection.Value },
                        dRoll = new Frame.FrameMeasurement.Observation() { Value = item._Ecart.Value }
                    });
                }
            }

            // .OBSXYZ
            foreach (var item in compensationItems.GetUsedNotTiltMeasures())
            {
                var obsXYZ = parameters?.GetOneObsXyz(item._Point._Name);
                if (obsXYZ == null)
                {
                    parameters.PointsToUseAsOBSXYZ.Add(new Frame.FrameMeasurement.ObsXYZ()
                    {
                        Used = true,
                        Point = item._Point._Name,

                    });
                }
            }
        }

        internal static void UpdateBasedOnOtherMeasures(Parameters tempParameters, GatheredMeasures gatheredMeasures)
        {
            // Iterate over PointsToUseAsOBSXYZ in tempParameters
            foreach (var obsxyz in tempParameters.PointsToUseAsOBSXYZ)
            {
                bool pointExistsInPolar = false;
                bool pointExistsInEcarto = false;
                bool pointExistsInLevel = false;

                // Check in Polar measurements
                foreach (var (station, measuresWithStatus) in gatheredMeasures.Polar)
                {
                    if (measuresWithStatus.Any(m => m.Measure._Point._Name == obsxyz.Point && m.Measure._Status.Type == Measures.States.Types.Good))
                    {
                        pointExistsInPolar = true;
                        break;
                    }
                }

                // Check in Ecarto measurements
                foreach (var (station, measuresWithStatus) in gatheredMeasures.Ecarto)
                {
                    if (measuresWithStatus.Any(m => m.Measure._Point._Name == obsxyz.Point && m.Measure._Status.Type == Measures.States.Types.Good))
                    {
                        pointExistsInEcarto = true;
                        break;
                    }
                }

                // Check in Level measurements
                foreach (var (station, measuresWithStatus) in gatheredMeasures.Level)
                {
                    if (measuresWithStatus.Any(m => m.Measure._Point._Name == obsxyz.Point && m.Measure._Status.Type == Measures.States.Types.Good))
                    {
                        pointExistsInLevel = true;
                        break;
                    }
                }

                // Set obsxyz.Used to false if point doesn't exist in any of the measures
                if (!pointExistsInPolar && !pointExistsInEcarto && !pointExistsInLevel)
                {
                    obsxyz.Used = false;
                }
            }
        }

    }
    public static class ParametersExtensions
    {
        public static void RemoveByAssemblyId<T>(this T src, string assemblyId) where T : List<Parameters>
        {
            for (int i = src.Count - 1; i >= 0; i--)
            {
                if (src[i].IdSuAssembly.ToUpper() == assemblyId.ToUpper())
                    src.RemoveAt(i);
            }
        }

        public static void ReplaceExisting<T>(this T src, Parameters parameter) where T : List<Parameters>
        {
            var existing = src.GetByAssemblyId(parameter.IdSuAssembly);
            if (existing != null)
                src.Remove(existing);
            src.Add(parameter);
        }

        public static Parameters GetByAssemblyId<T>(this T existingParameters, string idSuAssembly) where T : List<Parameters>
        {
            Parameters parameters = null;
            // Check if existing one
            foreach (Parameters item in existingParameters)
            {
                if (item.IdSuAssembly == idSuAssembly)
                {
                    parameters = item; break;
                }
            }
            return parameters;
        }

        public static bool Exist<T>(this T existingParameters, string idSuAssembly, Parameters parameters) where T : List<Parameters>
        {
            parameters = null;
            // Check if existing one
            foreach (Parameters item in existingParameters)
            {
                if (item.IdSuAssembly == idSuAssembly)
                {
                    parameters = item; break;
                }
            }
            return parameters != null;
        }
    }
}
