﻿using System;
using System.Collections.Generic;
using System.Linq;
using TSU.Common.Elements;
using TSU.Common.Measures;
using static TSU.Common.Compute.Compensations.Frame;
using R = TSU.Properties.Resources;
using TSU.Views;
using TSU.Common.Elements.Composites;
using System.IO;
using TSU.Common.Measures.States;
using TSU.Views.Message;
using System.Xml.Serialization;
using System.Windows.Forms;

namespace TSU.Common.Compute.Compensations.BeamOffsets
{
    public class GatheredMeasures
    {
        public DateTime DateTime { get; set; }
        public List<(Polar.Station Station, List<MeasureWithStatus> MeasuresWithStatus)> Polar;
        public List<(Line.Station.Average Station, List<MeasureWithStatus> MeasuresWithStatus)> Ecarto;
        public List<(Level.Station Station, List<MeasureWithStatus> MeasuresWithStatus)> Level;
        public List<MeasureWithStatus> Roll;

        public GatheredMeasures()
        {

        }

        internal ObservationType GetObservationType()
        {
            bool polar = Polar.Count > 0;
            bool ecarto = Ecarto.Count > 0;
            bool level = Level.Count > 0;

            if (polar && !ecarto && !level) return ObservationType.Polar;
            if (!polar && ecarto && !level) return ObservationType.Ecarto;
            if (!polar && !ecarto && level) return ObservationType.Level;
            if (!polar && !ecarto && !level) return ObservationType.Unknown;
            return ObservationType.All;
        }

        internal int CountPolarMeasuredAlesage(string idSuAssembly)
        {
            List<string> uniquePointNames = new List<string>();
            foreach (var polar in Polar)
            {
                foreach (MeasureWithStatus pm in polar.Item2)
                {
                    if (pm.Measure._Point._Name.ToUpper().Contains(idSuAssembly) && !(pm.Measure._Status is Bad))
                    {
                        if (!uniquePointNames.Contains(pm.Measure._Point._Name))
                            uniquePointNames.Add(pm.Measure._Point._Name);
                    }

                }
            }
            return uniquePointNames.Count;
        }

        public int CountLevelMeasuredAlesage(string idSuAssembly, out bool alesageSandTFound)
        {
            List<string> uniquePointNames = new List<string>();
            bool sFound = false;
            bool tFound = false;
            foreach (var meas in Level)
            {
                foreach (MeasureWithStatus pm in meas.Item2)
                {
                    if (pm.Measure._Point._Name.ToUpper().Contains(idSuAssembly) && !(pm.Measure._Status is Bad))
                    {
                        if (!uniquePointNames.Contains(pm.Measure._Point._Name))
                            uniquePointNames.Add(pm.Measure._Point._Name);
                    }
                    if (pm.Measure._Point._Name.ToUpper().EndsWith(".S"))
                        sFound = true;
                    if (pm.Measure._Point._Name.ToUpper().EndsWith(".T"))
                        tFound = true;

                }
            }
            alesageSandTFound = sFound && tFound;
            return uniquePointNames.Count;
        }
        internal bool Contains(ObservationType observationType, string pointName)
        {
            switch (observationType)
            {
                case ObservationType.Polar:
                    foreach (var polar in this.Polar)
                        foreach (var measure in polar.Item2)
                            if (measure.Measure._Point._Name == pointName)
                                if (measure.Measure._Status.Type != Types.Bad)
                                    return true;
                    break;
                case ObservationType.Ecarto:
                    foreach (var ecarto in this.Ecarto)
                        foreach (var measure in ecarto.Item2)
                            if (measure.Measure._Point._Name == pointName)
                                if (measure.Measure._Status.Type != Types.Bad)
                                    return true;
                    break;
                case ObservationType.Level:
                    foreach (var level in this.Level)
                        foreach (var measure in level.Item2)
                            if (measure.Measure._Point._Name == pointName)
                                if (measure.Measure._Status.Type != Types.Bad)
                                    return true;
                    break;
                case ObservationType.Roll:
                    foreach (var measure in this.Roll)
                        if (measure.Measure._Point._Name == pointName)
                            if (measure.Measure._Status.Type != Types.Bad)
                                return true;
                    break;
                case ObservationType.Length:
                    break;
                case ObservationType.All:
                case ObservationType.Unknown:
                default:
                    foreach (var polar in this.Polar)
                        foreach (var measure in polar.Item2)
                            if (measure.Measure._Point._Name == pointName)
                                if (measure.Measure._Status.Type != Types.Bad)
                                    return true;
                    foreach (var ecarto in this.Ecarto)
                        foreach (var measure in ecarto.Item2)
                            if (measure.Measure._Point._Name == pointName)
                                if (measure.Measure._Status.Type != Types.Bad)
                                    return true;
                    foreach (var level in this.Level)
                        foreach (var measure in level.Item2)
                            if (measure.Measure._Point._Name == pointName)
                                if (measure.Measure._Status.Type != Types.Bad)
                                    return true;
                    foreach (var measure in this.Roll)
                        if (measure.Measure._Point._Name == pointName)
                            if (measure.Measure._Status.Type != Types.Bad)
                                return true;
                    return false;
            }
            return false;
        }

        public int CountEcartoMeasuredAlesage(string idSuAssembly)
        {
            List<string> uniquePointNames = new List<string>();
            foreach (var ecarto in Ecarto)
            {
                foreach (MeasureWithStatus pm in ecarto.Item2)
                {
                    if (pm.Measure._Point._Name.ToUpper().Contains(idSuAssembly) && !(pm.Measure._Status is Bad))
                    {
                        if (!uniquePointNames.Contains(pm.Measure._Point._Name))
                            uniquePointNames.Add(pm.Measure._Point._Name);
                    }

                }
            }
            return uniquePointNames.Count;
        }

        internal int CountOfNotRollMeasurement(string idSuAssembly)
        {
            int polarCount = CountPolarMeasuredAlesage(idSuAssembly);
            int levelCount = CountLevelMeasuredAlesage(idSuAssembly, out _);
            int ecartoCount = CountEcartoMeasuredAlesage(idSuAssembly);
            return polarCount + levelCount + ecartoCount;
        }

        internal int CountRollMeasurement(string idSuAssembly)
        {
            return Roll.FindAll(x => x.Measure._Point._Name.ToUpper().Contains(idSuAssembly) && !(x.Measure._Status is Bad)).Count;
        }

        internal void ChangeStateInStateAndMeasure(Measure measureToFind, bool newState)
        {
            bool found = false;

            // Look in Polar
            for (int i = 0; i < this.Polar.Count - 1; i++)
            {
                for (int j = 0; j < this.Polar[j].Item2.Count - 1; j++)
                {
                    if (this.Polar[i].Item2[j].Measure == measureToFind)
                    {
                        this.Polar[i].Item2[j] = new MeasureWithStatus(newState, this.Polar[i].Item2[j].Measure);
                        return;
                    }
                }
            }

            // Look in Level
            for (int i = 0; i < this.Level.Count - 1; i++)
            {
                for (int j = 0; j < this.Level[j].Item2.Count - 1; j++)
                {
                    if (this.Level[i].Item2[j].Measure == measureToFind)
                    {
                        this.Level[i].Item2[j] = new MeasureWithStatus(newState, this.Level[i].Item2[j].Measure);
                        return;
                    }
                }
            }

            // Look in Ecarto
            for (int i = 0; i < this.Ecarto.Count - 1; i++)
            {
                for (int j = 0; j < this.Ecarto[j].Item2.Count - 1; j++)
                {
                    if (this.Ecarto[i].Item2[j].Measure == measureToFind)
                    {
                        this.Ecarto[i].Item2[j] = new MeasureWithStatus(newState, this.Ecarto[i].Item2[j].Measure);
                        return;
                    }
                }
            }

            // Look in Roll
            for (int j = 0; j < this.Roll.Count - 1; j++)
            {
                if (this.Roll[j].Measure == measureToFind)
                {
                    this.Roll[j] = new MeasureWithStatus(newState, this.Roll[j].Measure);
                    return;
                }
            }
        }

        internal bool ContainsGoodRollMeasureFor(string pointName)
        {
            var allGoodMeasures = new List<Measure>();
            foreach (var item in Polar)
            {
                foreach (var measuresWithStatus in item.MeasuresWithStatus)
                {
                    var measure = measuresWithStatus.Measure;
                    if (measure._Status.Type != Types.Bad && measure._PointName == pointName)
                        allGoodMeasures.Add(measure);
                }
            }
            foreach (var item in Level)
            {
                foreach (var measuresWithStatus in item.MeasuresWithStatus)
                {
                    var measure = measuresWithStatus.Measure;
                    if (measure._Status.Type != Types.Bad && measure._PointName == pointName)
                        allGoodMeasures.Add(measure);
                }
            }
            foreach (var item in Ecarto)
            {
                foreach (var measuresWithStatus in item.MeasuresWithStatus)
                {
                    var measure = measuresWithStatus.Measure;
                    if (measure._Status.Type != Types.Bad && measure._PointName == pointName)
                        allGoodMeasures.Add(measure);
                }
            }
            return allGoodMeasures.Count > 0;
        }

        internal IEnumerable<string> GetPoints(ObservationType observationType)
        {

            var points = new List<string>();

            switch (observationType)
            {
                case ObservationType.Polar:
                    foreach (var polar in Polar)
                        foreach (var s_m in polar.MeasuresWithStatus)
                            if (s_m.IsUsed)
                                points.Add(s_m.Measure._Point._Name); // Assuming the point name is here
                    break;
                case ObservationType.Ecarto:
                    foreach (var ecarto in Ecarto)
                        foreach (var s_m in ecarto.MeasuresWithStatus)
                            if (s_m.IsUsed && s_m.Measure is MeasureOfOffset om)
                                points.Add(om._Point._Name); // Assuming the point name is here
                    break;
                case ObservationType.Level:
                    foreach (var level in Level)
                        foreach (var s_m in level.MeasuresWithStatus)
                            if (s_m.IsUsed && s_m.Measure is MeasureOfLevel lm)
                                points.Add(lm._Name); // Assuming the point name is here
                    break;

                case ObservationType.Roll:
                    foreach (var roll in Roll)
                        if (roll.IsUsed && roll.Measure is Tilt.Measure tm)
                            points.Add(tm._Point._Name); // Assuming the point name is here
                    break;
                case ObservationType.All:
                    foreach (var polar in Polar)
                        foreach (var s_m in polar.MeasuresWithStatus)
                            if (s_m.IsUsed)
                                points.Add(s_m.Measure._Point._Name); // Assuming the point name is here
                    foreach (var ecarto in Ecarto)
                        foreach (var s_m in ecarto.MeasuresWithStatus)
                            if (s_m.IsUsed && s_m.Measure is MeasureOfOffset om)
                                points.Add(om._Point._Name); // Assuming the point name is here
                    foreach (var level in Level)
                        foreach (var s_m in level.MeasuresWithStatus)
                            if (s_m.IsUsed && s_m.Measure is MeasureOfLevel lm)
                                points.Add(lm._Name); // Assuming the point name is here
                    foreach (var roll in Roll)
                        if (roll.IsUsed && roll.Measure is Tilt.Measure tm)
                            points.Add(tm._Point._Name); // Assuming the point name is here
                    break;

                default:
                    throw new ArgumentException($"Unknown ObservationType: {observationType}");
            }

            return points.Distinct().ToList(); // Ensure points are unique

        }
    }
    public class MeasureWithStatus
    {
        public bool IsUsed = false;
        public Measure Measure = null;
        public MeasureWithStatus()
        {

        }
        public MeasureWithStatus(bool isUsed, Measure measure)
        {
            IsUsed = isUsed;
            Measure = measure;
        }
        public override string ToString()
        {
            string used = IsUsed ? "Used" : "Not used";
            return $"{used} {Measure}";
        }
    }
    public static class Gathering
    {
        public static void GetDataFrom(
            List<FinalModule> modules,
            string idsSuAssembly,
            DateTime expirationDate,
            out GatheredMeasures allMeasures)
        {
            List<(Polar.Station, List<MeasureWithStatus>)> tempPolar = new List<(Polar.Station, List<MeasureWithStatus>)>();
            List<(MeasureWithStatus, Polar.Station)> tempPolarMeasure = new List<(MeasureWithStatus, Polar.Station)>();

            List<(Line.Station.Average, List<MeasureWithStatus>)> tempLine = new List<(Line.Station.Average, List<MeasureWithStatus>)>();
            List<(MeasureWithStatus, Line.Station.Average)> tempLineMeasure = new List<(MeasureWithStatus, Line.Station.Average)>();

            List<(Level.Station, List<MeasureWithStatus>)> tempLevel = new List<(Level.Station, List<MeasureWithStatus>)>();
            List<(MeasureWithStatus, Level.Station)> tempLevelMeasure = new List<(MeasureWithStatus, Level.Station)>();

            List<MeasureWithStatus> tempRollMeasures = new List<MeasureWithStatus>();

            Action<FinalModule> gatherAllMeasures = (fm) =>
            {
                var measures = fm.GetMeasures();

                foreach (var measure in measures)
                {
                    bool pointNull = measure._Point == null;
                    bool isBeamPoint = measure._Point._Name.ToUpper().Contains("BEAM_"); // beacuse sometime there is fake measures to show beam points offsets 
                    bool rightAssemblyId = measure._Point._Name.ToUpper().Contains(idsSuAssembly.ToUpper() + ".");

                    if (pointNull || isBeamPoint || !rightAssemblyId)
                        continue; // we dont want it, let's continue with next measure

                    if (measure is Polar.Measure pm)
                        tempPolarMeasure.Add((new MeasureWithStatus(pm._Status.Type != Types.Bad, pm), fm._ActiveStationModule._Station as Polar.Station));
                    else if (measure is MeasureOfOffset om)
                    {
                        Line.Station.Average ln = null;
                        // difficult to find the station because there is several wires in an 'average' wire station
                        if (fm._ActiveStationModule._Station is Line.Station.Average lsa)
                        {
                            foreach (var item in lsa._ListStationLine)
                            {
                                if (item.MeasuresTaken.Contains(measure))
                                    ln = lsa;
                            }
                        }
                        tempLineMeasure.Add((new MeasureWithStatus(om._Status.Type != Types.Bad, om), ln));
                    }
                    else if (measure is MeasureOfLevel lm)
                    {
                        tempLevelMeasure.Add((new MeasureWithStatus(lm._Status.Type != Types.Bad, lm), fm._ActiveStationModule._Station as Level.Station));
                    }
                    else if (measure is Tilt.Measure tm)
                        tempRollMeasures.Add(new MeasureWithStatus(tm._Status.Type != Types.Bad, tm));
                }
            };

            // gather all measures of the assemblies
            tempPolarMeasure.Clear();
            foreach (FinalModule fm in modules)
            {
                if (fm is Guided.Group.Module ggm)
                    foreach (Guided.Module sgm in ggm.SubGuidedModules)
                        gatherAllMeasures(sgm);
                else
                    gatherAllMeasures(fm);
            }

            // keep only most recent
            tempPolarMeasure = tempPolarMeasure
                .Where(tuple => tuple.Item1.Measure._Date >= expirationDate) // Filter based on the expiration date
                .GroupBy(tuple => tuple.Item1.Measure._Name) // Group by name
                .SelectMany(group => group.OrderByDescending(tuple => tuple.Item1.Measure._Date).Take(1)) // Select the most recent tuple from each group
                .OrderByDescending(tuple => tuple.Item1.Measure._Date) // Sort the tuples in descending order based on the date
                .ToList();
            // lets go from (As,Bs) to (B, List<A>)
            var groupedTuplesPolar = tempPolarMeasure
                .GroupBy(tuple => tuple.Item2) // Group by B
                .Select(group => (group.Key, group.Select(item => item.Item1).ToList())) // Create tuples (B, List<A>)
                .ToList();

            tempLineMeasure = tempLineMeasure
                .Where(tuple => tuple.Item1.Measure._Date >= expirationDate) // Filter based on the expiration date
                .GroupBy(tuple => tuple.Item1.Measure._Name) // Group by name
                .SelectMany(group => group.OrderByDescending(tuple => tuple.Item1.Measure._Date).Take(1)) // Select the most recent tuple from each group
                .OrderByDescending(tuple => tuple.Item1.Measure._Date) // Sort the tuples in descending order based on the date
                .ToList();
            // lets go from (As,Bs) to (B, List<A>)
            var groupedTuplesLine = tempLineMeasure
                .GroupBy(tuple => tuple.Item2) // Group by B
                .Select(group => (group.Key, group.Select(item => item.Item1).ToList())) // Create tuples (B, List<A>)
                .ToList();

            tempLevelMeasure = tempLevelMeasure
                .Where(tuple => tuple.Item1.Measure._Date >= expirationDate) // Filter based on the expiration date
                .GroupBy(tuple => tuple.Item1.Measure._Name) // Group by name
                .SelectMany(group => group.OrderByDescending(tuple => tuple.Item1.Measure._Date).Take(1)) // Select the most recent tuple from each group
                .OrderByDescending(tuple => tuple.Item1.Measure._Date) // Sort the tuples in descending order based on the date
                .ToList();
            // lets go from (As,Bs) to (B, List<A>)
            var groupedTuplesLevel = tempLevelMeasure
                .GroupBy(tuple => tuple.Item2) // Group by B
                .Select(group => (group.Key, group.Select(item => item.Item1).ToList())) // Create tuples (B, List<A>)
                .ToList();

            tempRollMeasures = tempRollMeasures
                //.Where(obj => obj._Date >= expirationDate) // Filter based on the expiration date , roll never obsolete according to camille in tsu-3620
                .GroupBy(obj => obj.Measure._Name)
                .Select(group => group.OrderByDescending(obj => obj.Measure._Date).First())
                .ToList();

            allMeasures = new GatheredMeasures()
            {
                DateTime = expirationDate,
                Polar = groupedTuplesPolar,
                Ecarto = groupedTuplesLine,
                Level = groupedTuplesLevel,
                Roll = tempRollMeasures
            };
        }

        internal static string AddObservationInExistingInput(string existingInputContent, List<(Polar.Station, List<Polar.Measure>)> polarMeasures)
        {
            string modifiedInputContent = existingInputContent;
            foreach (var items in polarMeasures)
            {
                foreach (var item in items.Item2)
                {
                    modifiedInputContent = "";

                    string dummy = "";
                    string angl = "";
                    string zend = "";
                    string dist = "";
                    int countOfGood = 0;
                    List<Point> initializedPoints = new List<Point>() { item._Point };

                    Lgc2.Input.InputLine.AddPolarMeasure(item, new Polar.Station(), new Lgc2() { _Input = new Lgc2.Input() }, initializedPoints,
                        ref dummy, "", useObservationSigma: false, true, null, 32,
                        ref angl, ref zend, ref dist, ref countOfGood, space: " ", tab: "   ",
                        2000);

                    foreach (var line in existingInputContent.Split('\n'))
                    {
                        string cleanLine = line.Replace("\r", "");
                        modifiedInputContent += cleanLine + "\r\n";
                        if (cleanLine.TrimStart().StartsWith("*ANGL "))
                            modifiedInputContent += angl;
                        if (cleanLine.TrimStart().StartsWith("*ZEND "))
                            modifiedInputContent += zend;
                        if (cleanLine.TrimStart().StartsWith("*DIST "))
                            modifiedInputContent += dist;
                    }
                    existingInputContent = modifiedInputContent;
                }
            }
            return modifiedInputContent;
        }

        public static DateTime GetDateOfLastStationOfEachType(List<FinalModule> Modules)
        {
            List<Station> stations = new List<Station>();
            foreach (var module in Modules)
            {
                stations.AddRange(module.GetStations());
            }

            // Retrieve the newest object from each type
            Station newestObject = Station.GetOldestOftheNewestOfeachType(stations);

            return newestObject.ParametersBasic._Date;
        }

        public static DateTime GetDateOfLastCompute(List<FinalModule> modules, string idSuAssembly, ObservationType observationType)
        {
            // there is guiPref that allow to always compute even with expired measurements
            if (Tsunami2.Preferences.Values.GuiPrefs.BocMeasuresAreNeverExpired.IsTrue)
                return DateTime.MinValue;

            // if we are running in unit test we want to compute anyway
            if (Globals.UnitTestDetector.RunningInTest)
                return DateTime.MinValue;

            List<Station> stations = new List<Station>();
            DateTime mostRecent = DateTime.MinValue;


            foreach (var module in modules)
            {
                var lastResult = Tsunami2.Properties.BOC_Context.ExistingResults.GetLastByObservationType(observationType, idSuAssembly);
                if (lastResult != null)
                {
                    if (lastResult.Date > mostRecent)
                        mostRecent = lastResult.Date;
                }
            }

            if (mostRecent == DateTime.MinValue)
                mostRecent = GetDateOfLastStationOfEachType(modules).Add(new TimeSpan(0, -2, 0)); // remove 2 minutes because first station of level exist after the measurement when you choose an ref point;

            return mostRecent;
        }
    }

}
