﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TSU.Common.Compute.Compensations.BeamOffsets
{
    public class Context
    {
        public List<Parameters> ExistingAssemblyParameters { get; set; } = new List<Parameters> { };
        public List<Parameters> ExistingAssemblyDefaultParameters { get; set; } = new List<Parameters> { };

        //[XmlElement(ElementName = "ALLBocOriginalFrames")]
        //private Frame ForSerialization_BOC_ALLOriginalFrames;

        //[XmlIgnore] // we can ignore cause everyhing usefull is in the boc parameter and the original frame that can be retreive via the BOC_Context.SelectedGeodeFilePaths => annoying when the files get separted
        public Frame AlreadyAvailableOriginalFrames { get; set; }

        private List<string> selectedGeodeFilePaths;

        public List<Results> ExistingResults { get; set; } = new List<Results>();

        public List<string> SelectedGeodeFilePaths
        {
            get
            {
                if (selectedGeodeFilePaths == null)
                    selectedGeodeFilePaths = new List<string>();
                return

            selectedGeodeFilePaths;
            }
            set
            {
                selectedGeodeFilePaths = value;
            }

        }

        [XmlIgnore]
        public List<Guid> ObsoleteMeasures = new List<Guid>();

        public event Results.EventHandler ResultsUpdated;

        public void OnResultUpdated(List<Results> results)
        {
            if (ResultsUpdated != null)
                ResultsUpdated(this, new Results.EventArgs()
                {
                    Results = results
                });
        }

        internal void AddObsoleteMeasures(GatheredMeasures gatheredMeasures)
        {
            foreach (var measWithStatus in gatheredMeasures.Roll)
            {
                ObsoleteMeasures.Insert(0, measWithStatus.Measure.Guid);
            }

            foreach (var type in gatheredMeasures.Polar)
            {
                foreach (var measWithStatus in type.Item2)
                {
                    ObsoleteMeasures.Insert(0, measWithStatus.Measure.Guid);
                }
            }

            foreach (var type in gatheredMeasures.Level)
            {
                foreach (var measWithStatus in type.Item2)
                {
                    ObsoleteMeasures.Insert(0, measWithStatus.Measure.Guid);
                }
            }

            foreach (var type in gatheredMeasures.Ecarto)
            {
                foreach (var measWithStatus in type.Item2)
                {
                    ObsoleteMeasures.Insert(0, measWithStatus.Measure.Guid);
                }
            }
        }
    }
}
