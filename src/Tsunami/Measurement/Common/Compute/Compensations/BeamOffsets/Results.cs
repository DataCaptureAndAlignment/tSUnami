﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TSU.Common.Elements;
using TSU.Tools.Conversions;
using TSU.Views;
using TSU.Views.Message;
using static TSU.Polar.GuidedModules.Steps.StakeOut.Tools;
using static TSU.Tilt.TiltStationToGeode;

namespace TSU.Common.Compute.Compensations.BeamOffsets
{

    public class Results : TsuObject
    {
        [XmlAttribute]
        public string AssemblyId = "";

        [XmlAttribute]
        public ObservationType ObservationType { get; set; } = ObservationType.Unknown;

        [XmlAttribute]
        public bool Obsolete { get; set; } = false;
        [XmlAttribute]
        public bool OK { get; set; } = false;
        [XmlAttribute]
        public string LgcProjectPath;
        public Frame Frame { get; set; }
        public List<Point> Points = new List<Point>();
        public List<(string, CoordinatesInAllSystems)> Offsets = new List<(string, CoordinatesInAllSystems)>();
        public DoubleValue dRoll = new DoubleValue();
        public Parameters Parameters { get; set; }

        public bool RecomputeWithInitialParameters = false;

        [XmlIgnore]
        public List<Point> BeamPoints
        {
            get
            {
                List<Point> founds = new List<Point>();
                foreach (var item in Points)
                {
                    if (item._Name.ToUpper().Contains("BEAM_"))
                        founds.Add(item);
                }
                return founds;
            }
        }

        [XmlIgnore]
        public List<Point> ComputedPoints
        {
            get
            {
                List<Point> founds = new List<Point>();
                foreach (var item in Points)
                {
                    if (Frame != null)
                        if (Frame.DeeperFrame.Measurements.ObsXyzs.LastOrDefault(x=> x.Point == item._Name && x.Used == true) == null)
                            founds.Add(item);
                }
                return founds;
            }
        }

        [XmlAttribute]
        public double Sigma0 { get; set; }

        [XmlAttribute]
        public DateTime Date { get; set; }

        public Results()
        {
            // parameterless constructor necessary for deserialization
        }

        public void AddOrReplace(List<Point> points)
        {
            var existing = this.BeamPoints;
            foreach (var point in points)
            {
                for (int i = existing.Count - 1; i >= 0; i--)
                {
                    if (existing[i]._Name == point._Name)
                        existing.RemoveAt(i);
                }
                existing.Add(point);
            }
        }
        public void Add(Point point)
        {
            Points.Add(point);
        }

        public override object Clone()
        {
            Results clone = this.MemberwiseClone() as Results;
            clone.Guid = new Guid();
            if (clone.Parameters !=null)
                clone.Parameters = this.Parameters.Clone() as Parameters;

            if (clone.Frame != null) 
                clone.Frame = this.Frame.Clone() as Frame;

            if (clone.Points != null) 
                clone.Points = new List<Point>();
            return clone;
        }

        internal List<Point> GetPointsBasedOn(List<string> assemblyIds)
        {
            List<Point> points = new List<Point>();

            foreach (string assemblyId in assemblyIds)
            {
                foreach (var item in BeamPoints)
                {
                    if (item._Name.ToUpper().Contains(assemblyId.ToUpper()))
                        points.Add(item);
                }

            }
            return points;
        }


        /// <summary>
        /// use "4LEVEL" "4ECARTO" or "4ALL"
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public string ToString(string v)
        {
            if (!this.OK)
                return "";

            string message = "";

            if (v == "4LEVEL")
            {
                foreach (var item in this.Offsets)
                {
                    message += $"{item.Item1}: Hbeam = {item.Item2.Beam.Z.ToString("m-mm")} mm \r\n";
                }
                if (this.dRoll != null) // Roll could be measured with only level measurements
                    message += $"Diff(Roll): {this.dRoll} mrad\r\n";
            }
            else if (v == "4ECARTO")
            {
                foreach (var item in this.Offsets)
                {
                    message += $"{item.Item1}: Rbeam = {item.Item2.Beam.X.ToString("m-mm")} mm \r\n";
                }
            }
            else
            {
                foreach (var item in this.Offsets)
                {
                    if (this.ComputedPoints.LastOrDefault(x=>x._Name == item.Item1)!=null)
                        message += $"{item.Item1} {item.Item2.Beam.ToString("mm")} mm \r\n";
                }
                message += $"Diff(Roll): {this.dRoll} mrad\r\n";
            }

            message += $"Check LGC details: \r\n\t{MessageTsu.GetLink(this.LgcProjectPath + ".INP")}\r\n";

            return $"Offsets;{message}";
        }

        internal List<(string, CoordinatesInAllSystems)> GetOffsetsByAssembly(string magnetName)
        {
            List<(string, CoordinatesInAllSystems)> founds = new List<(string, CoordinatesInAllSystems)>();
            foreach (var offset in Offsets)
            {
                var pointName = offset.Item1;
                if (pointName.Contains(magnetName))
                {
                    founds.Add(offset);
                }
            }
            return founds;
        }

        public override string ToString()
        {
            string obsolete = this.Obsolete ? "Obsolete " : "";
            return $"{obsolete} BOC results: {ObservationType} {Date}";
        }

        public delegate void EventHandler(object sender, EventArgs e);

        public class EventArgs : System.EventArgs
        {
            public List<Common.Compute.Compensations.BeamOffsets.Results> Results;

            public EventArgs()
            {

            }
        }
    }






    public static class ResultsExtensions
    {
        /// <summary>
        /// return a list of Beam point with SU assembly Ids and obsolete state
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="src"></param>
        /// <param name="assemblyIds"></param>
        /// <returns></returns>
        public static List<(string, bool, Point, DateTime date)> GetBeamPointsBasedOn<T>(this T src, List<string> assemblyIds) where T : List<Results>
        {
            List<(string, bool, Point, DateTime)> points = new List<(string, bool, Point, DateTime)>();

            var bocResults = src;
            var tempExistingNameList = new List<string>();
            foreach (string assemblyId in assemblyIds)
            {
                for (int i = bocResults.Count - 1; i >= 0; i--)
                {
                    var result = src[i];
                    if (result.AssemblyId == assemblyId)
                    {
                        foreach (var item in result.BeamPoints)
                        {
                            if (!tempExistingNameList.Contains(item._Name))
                                points.Add((assemblyId, result.Obsolete, item, result.Date));
                            tempExistingNameList.Add(item._Name);
                        }
                    }
                }
            }
            return points;
        }


        public static bool ContainsDRollFor<T>(this T src, string magnetName, out DoubleValue dRoll) where T : List<Results>
        {
            dRoll = new DoubleValue();
            if (src == null)
                return false;

            for (int i = src.Count - 1; i > -1; i--)
            {
                var result = src[i];
                if (result.AssemblyId == magnetName)
                {
                    if (result.dRoll.Value != dRoll.Value)
                    {
                        dRoll = result.dRoll;
                        return true;
                    }
                }
            }

            return false;
        }

        public static List<string> GetNames<T>(this T src) where T : List<Results>
        {
            var list = new List<string>();

            foreach (var results in src)
            {
                foreach (var point in results.BeamPoints)
                {
                    var name = point._Name;
                    if (!list.Contains(name))
                        list.Add(name);
                }

            }

            return list;
        }

        public static bool MakeObsolete<T>(this T src, List<string> assemblyIds) where T : List<Results>
        {
            foreach (string assemblyId in assemblyIds)
            {
                foreach (var result in src)
                {
                    if (result.AssemblyId == assemblyId)
                    {
                        result.Obsolete = true;
                        return true;
                    }
                }
            }
            return false;
        }
        public static Results GetLastByObservationType<T>(this T src, ObservationType observationType, string idSuAssembly) where T : List<Results>
        {
            for (int i = src.Count - 1; i >= 0; i--)
            {
                if (src[i].ObservationType == observationType)
                    if (src[i].AssemblyId == idSuAssembly)
                        return src[i];
            }
            return null;
        }
        public static List<Results> GetByObservationType<T>(this T src, ObservationType observationType) where T : List<Results>
        {
            var founds = new List<Results>();
            for (int i = src.Count - 1; i >= 0; i--)
            {
                if (src[i].ObservationType == observationType)
                    founds.Add(src[i]);
            }
            return founds;
        }

        public static Results GetLastByAssembly<T>(this T src, string assemblyId) where T : List<Results>
        {
            for (int i = src.Count - 1; i >= 0; i--)
            {
                if (src[i].AssemblyId == assemblyId)
                    return src[i];
            }
            return null;
        }

        public static List<Point> GetSocketsFromLastResultByAssemblyId<T>(this T src, string assemblyId) where T : List<Results>
        {
            var founds = new List<Point>();
            var lastResult = src.GetLastByAssembly(assemblyId);
            if (lastResult != null)
            {
                foreach (var point in lastResult.Points)
                {
                    if (!point._Name.Contains("BEAM_"))
                        founds.Add(point);
                }
            }
            return founds;
        }

        public static Results GetLastByObservationType<T>(this T src, string assemblyId, ObservationType observationType) where T : List<Results>
        {
            for (int i = src.Count - 1; i >= 0; i--)
            {
                var result = src[i];
                if (result.ObservationType == observationType && result.AssemblyId == assemblyId)
                    return result;
            }
            return null;
        }


        public static DoubleValue GetDRoll<T>(this T src, string assemblyId, ObservationType observationType) where T : List<Results>
        {
            var results = src as List<Results>;
            foreach (var result in results)
            {
                if (result.AssemblyId == assemblyId)
                {
                    if (result.ObservationType == observationType)
                    {
                        return result.dRoll;
                    }
                }
            }
            return null;
        }

        public static Coordinates GetOffset<T>(this T src, string pointName, ObservationType observationType, string csName, out bool obsolete) where T : List<Results>
        {
            // Get the last
            for (int i = src.Count - 1; i >= 0; i--)
            {
                // Result of given type
                var result = src[i];
                if (result.ObservationType == observationType)
                {
                    // find the right offset
                    foreach (var offset in result.Offsets)
                    {
                        var offsetPointName = offset.Item1;
                        if (offsetPointName == pointName)
                        {
                            // return the given CS
                            var coordinates = offset.Item2;
                            obsolete = result.Obsolete;
                            return coordinates.GetCoordinatesInASystemByName(csName);
                        }
                    }
                }
            }
            obsolete = true;
            return null;
        }
    }
}
