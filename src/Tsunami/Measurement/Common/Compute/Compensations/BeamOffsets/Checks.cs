﻿using System;
using System.Collections.Generic;
using System.Linq;
using TSU.Common.Elements;
using TSU.Common.Measures;
using TSU.Common.Measures.States;
using TSU.ENUM;
using TSU.Views;
using TSU.Views.Message;
using static TSU.Common.Compute.Compensations.Frame;
using static TSU.Common.Compute.Compensations.Frame.FrameMeasurement;
using static TSU.Level.Compute.StationToStationComparison;
using R = TSU.Properties.Resources;


namespace TSU.Common.Compute.Compensations.BeamOffsets
{
    public static class Checks
    {
        /// <summary>
        /// check if we have stored boc result concern by the last measurement and turn it obsolete if it is the case and return if something has been turned to obsolete.
        /// </summary>
        /// <param name="lastMeasure"></param>
        /// <param name="BOC_Results"></param>
        public static List<BeamOffsets.Results> Validity(Measures.Measure lastMeasure, List<BeamOffsets.Results> BOC_Results = null)
        {

            // load golabl results if not set
            if (BOC_Results == null)
                BOC_Results = Tsunami2.Properties.BOC_Context.ExistingResults;

            List<BeamOffsets.Results> haveChanged = new List<Results>();
            string assemblyIdOfThelastMeasure = Point.GetAssembly(lastMeasure._Point._Name);
            foreach (var result in BOC_Results)
            {
                if (!result.Obsolete)
                {
                    if (result.AssemblyId == assemblyIdOfThelastMeasure)
                    {
                        bool obsolete = false;
                        if (lastMeasure is Tilt.Measure)
                            obsolete = true;
                        else if (lastMeasure is Polar.Measure)
                        {
                            if (result.ObservationType == ObservationType.Polar || result.ObservationType == ObservationType.All)
                                obsolete = true;
                        }
                        else if (lastMeasure is MeasureOfOffset)
                        {
                            if (result.ObservationType == ObservationType.Ecarto || result.ObservationType == ObservationType.All)
                                obsolete = true;
                        }
                        else if (lastMeasure is MeasureOfLevel)
                        {
                            if (result.ObservationType == ObservationType.Level || result.ObservationType == ObservationType.All)
                                obsolete = true;
                        }
                        else if (lastMeasure is MeasureOfDistance)
                        {
                            if (result.ObservationType == ObservationType.Length || result.ObservationType == ObservationType.All)
                                obsolete = true;
                        }

                        if (obsolete)
                        {
                            result.Obsolete = true;
                            haveChanged.Add(result);
                        }
                    }
                }
            }
            return haveChanged;
        }
        private static bool AskToConfirmRotations(double rX, double rY, double rZ)
        {
            string message = R.T_BO_ROT_WRONG;
            if (new MessageInput(MessageType.Warning, message)
                {
                    ButtonTexts = CreationHelper.GetOKCancelButtons()
                }.Show().TextOfButtonClicked == R.T_CANCEL)
                return false;
            return true;
        }
        internal static bool MinimumRequireIsPresent(Parameters param, GatheredMeasures measures, out string message, out bool lookLikeTiltMeasuredWithLevelling)
        {
            lookLikeTiltMeasuredWithLevelling = false;
            message = "";
            if (param == null)
            {
                message = "Lost parameters, close and restart the BOC ";
                return false;
            }
            int numberOfObsXYZ = Frame.FrameMeasurement.ObsXYZ.GetUsedOnes(param.PointsToUseAsOBSXYZ).Count;
            int numberOfEcarto = 0;
            int numberOfLevel = 0;
            int numberOfPolar = 0;
            foreach (var stationsAndMeasures in measures.Ecarto)
            {
                foreach (var stateAndMeasure in stationsAndMeasures.MeasuresWithStatus)
                {
                    if (stateAndMeasure.IsUsed) numberOfEcarto++;
                }
            }
            foreach (var stationsAndMeasures in measures.Level)
            {
                foreach (var stateAndMeasure in stationsAndMeasures.MeasuresWithStatus)
                {
                    if (stateAndMeasure.IsUsed) numberOfLevel++;
                }
            }
            foreach (var stationsAndMeasures in measures.Polar)
            {
                foreach (var stateAndMeasure in stationsAndMeasures.MeasuresWithStatus)
                {
                    if (stateAndMeasure.IsUsed) numberOfPolar++;
                }
            }
            int numberOfIncly = param.Inclys.FindAll(x => x.Used == true).Count();
            lookLikeTiltMeasuredWithLevelling = numberOfObsXYZ == 2 && numberOfIncly == 0;
            if (numberOfEcarto + numberOfPolar + numberOfLevel + numberOfIncly < 3)
            {
                message = $"{R.T_NOT_ENOUGH_ACTIVE_MEASURES};#ECARTO={numberOfEcarto} + #LEVEL={numberOfLevel} + #POLAR={numberOfPolar} + #INCLY={numberOfIncly} < 3";
                return false;
            }
            else
            {
                message = $"{R.T_GOOD}";
                return true;
            }
        }

        internal static bool AskToConfirmResiduals(double sigmaYnew, FrameMeasurement frameMeasurement)
        {
            string message = R.T_BO_RESIDUALS;
            message += ";" + $"sYnew = {Math.Round(sigmaYnew * 1000)} mm. The acceptable limit is 7 mm.";
            if (new MessageInput(MessageType.Warning, message)
            {
                ButtonTexts = new List<string>() { "Edit compute", R.T_CANCEL }
            }.Show().TextOfButtonClicked == R.T_CANCEL)
                throw new TsuCancelException("sY out of tolerance");
            return false;
        }

        internal static bool ChecksIfEnoughToCompute(string idSuAssembly, GatheredMeasures measures, ObservationType observationType, out bool computedRoll)
        {
            int numberOfRollMeasureOnAlesage = measures.CountRollMeasurement(idSuAssembly); 
            bool rollMeasured = numberOfRollMeasureOnAlesage > 0;
            computedRoll = !rollMeasured;

            int numberOfPolarMeasureOnAlesage = measures.CountPolarMeasuredAlesage(idSuAssembly);
            int numberOfLevelMeasureOnAlesage = measures.CountLevelMeasuredAlesage(idSuAssembly,  out bool alesageSandTFound);
            int numberOfEcartoMeasureOnAlesage = measures.CountEcartoMeasuredAlesage(idSuAssembly);

            // check for any minimum requirements
            if (rollMeasured && numberOfPolarMeasureOnAlesage >= 2) return true;
            if (rollMeasured && numberOfLevelMeasureOnAlesage >= 2) return true;
            if (rollMeasured && numberOfEcartoMeasureOnAlesage >= 2) return true;

            if (numberOfPolarMeasureOnAlesage >= 3) return true;
            if (numberOfLevelMeasureOnAlesage >= 2 && alesageSandTFound) return true;

            if (numberOfPolarMeasureOnAlesage + numberOfLevelMeasureOnAlesage + numberOfEcartoMeasureOnAlesage + numberOfRollMeasureOnAlesage == 0)
            {
                var message = $"No measure for '{idSuAssembly}'";
                new MessageInput(MessageType.Warning, message).Show();
                throw new TsuCancelException(message);
            }

            // not enough measurement!
            string specificMessage = "";
            string withRollYouNeed2 = $"With Roll measured, you need '2' sockets measured after {measures.DateTime}, you have";
            string withRollYouNeed3 = $"With no Roll measured, you need '3' sockets measured after {measures.DateTime}, you have";
            switch (observationType)
            {
                case ObservationType.All:
                case ObservationType.Polar:
                    if (rollMeasured)
                        specificMessage = $"{withRollYouNeed2} '{numberOfPolarMeasureOnAlesage}'";
                    else
                        specificMessage = $"{withRollYouNeed3} '{numberOfPolarMeasureOnAlesage}'";
                    break;
                case ObservationType.Ecarto:
                    if (rollMeasured)
                        specificMessage = $"{withRollYouNeed2} '{numberOfEcartoMeasureOnAlesage}'";
                    else
                        specificMessage = $"With no measure of Roll, roll or beam offsets computation is not adviced";
                    break;
                case ObservationType.Level:
                    if (rollMeasured)
                        specificMessage = $"{withRollYouNeed2} '{numberOfLevelMeasureOnAlesage}'";
                    else
                    {
                        if (alesageSandTFound) 
                            specificMessage = $"{withRollYouNeed2} '{numberOfLevelMeasureOnAlesage}'";
                        else
                            specificMessage = $"With no measure of Roll, you need 'S' and 'T' sockets measured after {measures.DateTime}";
                    }
                    break;
                case ObservationType.Unknown:
                case ObservationType.Roll:
                case ObservationType.Length:
                default:
                    specificMessage = "Boc not possible with the observations available...";
                    break;
            }
            throw new BocException(
                $"'{idSuAssembly}'   B.O.C./'Roll Compute'  KO;" +
                $"{R.T_BO_NO_ROLL} or to compute the beam points of '{idSuAssembly}' with {observationType} observations:\r\n" +
                $"{specificMessage}");
        }

        internal static bool ControlAdjustedFrameOrientation(Frame frame)
        {
            double RX = frame.EstimatedParameters.RX.Value;
            double RY = frame.EstimatedParameters.RY.Value;
            double RZ = frame.EstimatedParameters.RZ.Value;

            bool RX_KO = (50 > RY) && (RY > 350);
            bool RY_KO = (50 > RY) && (RY > 350);
            bool RZ_KO = (50 > RY) && (RY > 350);

            if (RY_KO && (RX_KO && RZ_KO))
                return AskToConfirmRotations(RX, RY, RZ);
            else
                return true;
        }

        internal static bool ControlSigma0andSigmaRY(Results results)
        {
            bool sigma0_OK = results.Sigma0 < 1.5;
            bool sigmaRY_OK = results.Frame.DeeperFrame.EstimatedParameters.RY.Sigma < 0.0060; // 60 cc
            return sigma0_OK && sigmaRY_OK;
        }

        internal static bool ControlResiduals(Frame frame, out double newYsigma, out bool wasOut)
        {
            newYsigma = 0;
            foreach (var item in frame.DeeperFrame.Measurements.ObsXyzs)
            {
                newYsigma += Math.Pow(item.Y.Residual, 2);
            }
            newYsigma /= frame.DeeperFrame.Measurements.ObsXyzs.Count - 1;
            newYsigma = Math.Sqrt(newYsigma);
            bool residualOK = newYsigma < 0.007; // 07 mm
            wasOut = !residualOK;
            if (residualOK)

                return true;
            else
                return AskToConfirmResiduals(newYsigma, frame.DeeperFrame.Measurements);
        }

        internal static bool IfAllMeasuredAsBefore(string idSuAssembly, GatheredMeasures gatheredMeasures, ObservationType observationType, Parameters parameters, out bool missingMeasures, out string missingMessage, out bool extraMeasures, out string extraMessage)
        {
            // Initialize out parameters
            missingMessage = "";
            extraMessage = "";
            missingMeasures = false;
            extraMeasures = false;

            var expectedPoints = new HashSet<string>();

            // Collect the points that are marked as 'Used' in PointsToUseAsOBSXYZ and Inclys
            foreach (var obsxyz in parameters.PointsToUseAsOBSXYZ)
            {
                if (obsxyz.Used)
                    expectedPoints.Add(obsxyz.Point);
            }

            foreach (var incly in parameters.Inclys)
            {
                if (incly.Used)
                    expectedPoints.Add(incly.Point);
            }

            // Check if all expected points have gathered measures
            var missingPoints = new List<string>();
            foreach (var pointName in expectedPoints)
            {
                if (!gatheredMeasures.Contains(observationType, pointName))
                {
                    missingPoints.Add(pointName);
                    missingMeasures = true; // Flag missing measures
                }
            }

            if (missingMeasures)
            {
                missingMessage = $"Missing {observationType} observations for the following points:\r\n" + string.Join("\r\n", missingPoints);
            }

            // Collect gathered measures that are not in the expected points list
            var extraPoints = gatheredMeasures.GetPoints(observationType)
                .Where(measuredPoint => !expectedPoints.Contains(measuredPoint))
                .ToList();

            if (extraPoints.Any())
            {
                extraMessage = $"Extra gathered measures found:\r\n" + string.Join("\r\n", extraPoints);
                extraMeasures = true; // Flag extra measures
            }

            // Return true if no missing or extra measures were found
            return !missingMeasures && !extraMeasures;
        }
        
        internal static bool IfAllMeasuredAsBefore_old (string idSuAssembly, GatheredMeasures gatheredMeasures, ObservationType observationType, Parameters parameters, out string message)
        {
            message = "";
            var expectedPoints = new HashSet<string>();

            // Collect the points that are marked as 'Used' in PointsToUseAsOBSXYZ and Inclys
            foreach (var obsxyz in parameters.PointsToUseAsOBSXYZ)
            {
                if (obsxyz.Used)
                    expectedPoints.Add(obsxyz.Point);
            }

            foreach (var incly in parameters.Inclys)
            {
                if (incly.Used)
                    expectedPoints.Add(incly.Point);
            }

            // Check if all expected points have gathered measures
            foreach (var pointName in expectedPoints)
            {
                if (!gatheredMeasures.Contains(observationType, pointName))
                {
                    message = $"Missing {observationType} observation for {pointName}";
                    return false;
                }
            }

            // Collect gathered measures that are not in the expected points list
            var extraMeasures = gatheredMeasures.GetPoints(observationType)
                .Where(measuredPoint => !expectedPoints.Contains(measuredPoint))
                .ToList();

            if (extraMeasures.Any())
            {
                string extraPoints = string.Join("\r\n", extraMeasures);
                message = $"Extra gathered measures found:\r\n{extraPoints}";
                return false;
            }

            return true;
        }


        internal static void ObsAndFrameAreCompatible(
            Frame frame, GatheredMeasures gatheredMeasures, 
            out string additionalPoints,
            out string missingPoints
            )
        {
            additionalPoints = "";
            missingPoints = "";

            if (frame == null)
                return;
            if ( gatheredMeasures.Roll.Count > frame.DeeperFrame.Measurements.Inclinaisons.Count)
                throw new TsuException("Frame not compatible;Frame not compatible with available observations, no *Incly in frame");

            var polarMeasures = new List<Measure>();
            foreach (var polar in gatheredMeasures.Polar)
            {
                foreach (var s_m in polar.MeasuresWithStatus)
                {
                    if (s_m.IsUsed)
                        if (!polarMeasures.Exists(x=> x._Name == s_m.Measure._Name))
                    polarMeasures.Add(s_m.Measure);
                }
            }

            // Collect used obsXYZ points from originalFrames
            var usedObsXyzNames = frame.DeeperFrame.Measurements.ObsXyzs
                .Where(x => x.Used)
                .Select(x => x.Point)
                .ToHashSet();

            // Find incompatible polar measurements by comparing with usedObsXyzNames
            var additionalMeasures = polarMeasures
                .Where(measure => !usedObsXyzNames.Contains(measure._Point._Name))
                .Select(measure => measure._Point._Name)
                .ToList();

            // Find missing measures in usedObsXyzNames that are not in polarMeasures
            var missingMeasures = usedObsXyzNames
                .Where(obsXyzName => !polarMeasures.Any(measure => measure._Point._Name == obsXyzName))
                .ToList();

            // Throw exceptions for both additional and missing measures
            if (additionalMeasures.Any() || missingMeasures.Any())
            {
                additionalPoints = additionalMeasures.Any() ? $"Additional points: {string.Join(", ", additionalMeasures)}" : string.Empty;
                missingPoints = missingMeasures.Any() ? $"Missing points: {string.Join(", ", missingMeasures)}" : string.Empty;

                if (!string.IsNullOrEmpty(additionalPoints)) additionalPoints += $";More polar measurements than available obsXYZ in the frame. {additionalPoints}";
                if (!string.IsNullOrEmpty(missingPoints)) missingPoints += $";Some obsXYZ points are missing corresponding polar measurements. {missingPoints}";
            }
        }
    }

}
