﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TSU.Common.Compute.Compensations.BeamOffsets
{
    public class CompensationItem : TsuObject
    {
        public CompensationItem()
        {
            // parameterless constructor necessary for deserialization
        }

        public MeasureWithStatus StateAndMeasure { get; set; }
        public Frame.FrameMeasurement.ObsXYZ ObsXyz { get; set; }
        public Frame.FrameMeasurement.Incly Incly { get; set; }

        [XmlIgnore]
        public bool Used { get; set; }
        //{
        //    get
        //    {
        //        bool obsUsed = ObsXyz == null ? false : ObsXyz.Used;
        //        bool inclyUsed = Incly == null ? false : Incly.Used;
        //        return obsUsed || inclyUsed;
        //    }
        //    internal set
        //    {
        //        if (ObsXyz != null) ObsXyz.Used = value;
        //        if (Incly != null) Incly.Used = value;
        //    }
        //}

        /// <summary>
        /// Update the residuals for each measurement used + create or update a compensationItems to show the computed Tilt
        /// </summary>
        /// <param name="compensationItems"></param>
        /// <param name="frame"></param>
        internal static void UpdateFromFrame(List<CompensationItem> compensationItems, Frame frame)
        {
            CompensationItem purelyComputeItem = null;
            foreach (BeamOffsets.CompensationItem item in compensationItems)
            {
                // Computed roll item
                if (item.Incly != null && item.Incly.IsAResultFromComputation)
                {
                    purelyComputeItem = item;
                }
                // Polar Item
                else if (item.StateAndMeasure.Measure is Polar.Measure)
                {
                    if (item.ObsXyz == null)
                        item.ObsXyz = new Frame.FrameMeasurement.ObsXYZ();
                    var obs = frame.Measurements.GetObsXyzByName(item.ObsXyz.Point);
                    if (obs != null)
                    {
                        item.ObsXyz.X.Residual = frame.Measurements.GetObsXyzByName(item.ObsXyz.Point).X.Residual;
                        item.ObsXyz.Y.Residual = frame.Measurements.GetObsXyzByName(item.ObsXyz.Point).Y.Residual;
                        item.ObsXyz.Z.Residual = frame.Measurements.GetObsXyzByName(item.ObsXyz.Point).Z.Residual;
                    }
                }
                // Ecarto Item
                else if (item.StateAndMeasure.Measure is Measures.MeasureOfOffset)
                {
                    if (item.ObsXyz == null)
                        item.ObsXyz = new Frame.FrameMeasurement.ObsXYZ();
                    var obs = frame.Measurements.GetObsXyzByName(item.ObsXyz.Point);
                    if (obs != null)
                    {
                        item.ObsXyz.X.Residual = frame.Measurements.GetObsXyzByName(item.ObsXyz.Point).X.Residual;
                        item.ObsXyz.Y.Residual = frame.Measurements.GetObsXyzByName(item.ObsXyz.Point).Y.Residual;
                        item.ObsXyz.Z.Residual = frame.Measurements.GetObsXyzByName(item.ObsXyz.Point).Z.Residual;
                    }
                }
                // Levelling Item
                else if (item.StateAndMeasure.Measure is Measures.MeasureOfLevel)
                {
                    if (item.ObsXyz == null)
                        item.ObsXyz = new Frame.FrameMeasurement.ObsXYZ();
                    var obs = frame.Measurements.GetObsXyzByName(item.ObsXyz.Point);
                    if (obs != null)
                    {
                        item.ObsXyz.X.Residual = frame.Measurements.GetObsXyzByName(item.ObsXyz.Point).X.Residual;
                        item.ObsXyz.Y.Residual = frame.Measurements.GetObsXyzByName(item.ObsXyz.Point).Y.Residual;
                        item.ObsXyz.Z.Residual = frame.Measurements.GetObsXyzByName(item.ObsXyz.Point).Z.Residual;
                    }
                }
                // Tilt item
                else if (item.StateAndMeasure.Measure is Tilt.Measure)
                {
                    if (item.Incly == null)
                        item.Incly = new Frame.FrameMeasurement.Incly();

                    var incly = frame.Measurements.GetInclyByName(item.Incly.Point);
                    if (incly != null)
                    {
                        item.Incly.Roll.Value = incly.Roll.Value;
                        item.Incly.Roll.Sigma = incly.Roll.Sigma;
                    }
                }
            }
            // Remove purelyComputeItem and create a new one create a compensation item to reflect the compute dRoll which is the RY of the deeperFrame
            if (purelyComputeItem != null)
                compensationItems.Remove(purelyComputeItem);
            var dRoll = new Frame.FrameMeasurement.Observation()
            {
                Value = frame.DeeperFrame.EstimatedParameters.RY.Value,
                Sigma = frame.DeeperFrame.EstimatedParameters.RY.Sigma,
            };
            var compensationItem = new CompensationItem()
            {
                Incly = new Frame.FrameMeasurement.Incly() {  Point = Frame.GetAssemblyName(frame), dRoll = dRoll, IsAResultFromComputation = true },
            };
            compensationItems.Add(compensationItem);
        }



        public override string ToString()
        {
            if (this.StateAndMeasure!=null && this.StateAndMeasure.Measure != null)
                return $"Used={Used} - {this.StateAndMeasure.Measure._Name}";
            else
                return "-";
        }

        internal static void UseToUpdateBOParameters(List<CompensationItem> compensationItems, Parameters parameters)
        {
            parameters.PointsToUseAsOBSXYZ.Clear();

            foreach (CompensationItem item in compensationItems)
            {
                if (item.Used)
                {
                    parameters.PointsToUseAsOBSXYZ.Add(item.ObsXyz);
                }
            }
        }

        /// <summary>
        /// we have to update the used state of the measures based on what have been changed in the compensation.items
        /// </summary>
        /// <param name="measures"></param>
        /// <param name="compensationItems"></param>
        /// <exception cref="NotImplementedException"></exception>
        internal static void UpdateMeasures(GatheredMeasures measures, List<CompensationItem> compensationItems)
        {
            foreach (var compensationItem in compensationItems)
            {
                //measures.ChangeStateInStateAndMeasure(compensationItem.StateAndMeasure.Measure, compensationItem.Used);
            }
        }
    }

    public static class CompensationItemExtensions
    {
        public static void UpdateFromParameters<T>(this T src, Parameters parameters) where T : List<CompensationItem>
        {
            if (parameters == null)
                return;
            foreach (CompensationItem item in src)
            {
                if (item.ObsXyz != null)
                    item.ObsXyz = parameters.GetOneObsXyz(item.StateAndMeasure.Measure._Point._Name);
                else if (item.Incly == null || item.Incly.IsAResultFromComputation)
                    continue;
                else
                    item.Incly = parameters.GetOneIncly(item.StateAndMeasure.Measure._Point._Name);
            }
        }

        public static bool ContainsAUsedTiltMeasureOfPointNamed<T>(this T src, string pointName, out Tilt.Measure measure) where T : List<CompensationItem>
        {
            var foundItem = src.Find(x => x.Used == true && x.StateAndMeasure.Measure is Tilt.Measure && x.StateAndMeasure.Measure._Point._Name == pointName);

            if (foundItem != null)
            {
                measure = foundItem.StateAndMeasure.Measure as Tilt.Measure;
                if (measure != null)
                    return true;
            }
            measure = null;
            return false;
        }

        public static CompensationItem GetInclyByPointName<T>(this T src, string pointName) where T : List<CompensationItem>
        {
            var foundItem = src.Find(x => x.StateAndMeasure != null && x.StateAndMeasure.Measure is Tilt.Measure && x.StateAndMeasure.Measure._Point._Name == pointName);
            return foundItem;
        }

        public static CompensationItem GetObsXyzByPointName<T>(this T src, string pointName) where T : List<CompensationItem>
        {
            var foundItem = src.Find(x => x.StateAndMeasure != null && !(x.StateAndMeasure.Measure is Tilt.Measure) && x.StateAndMeasure.Measure._Point._Name == pointName);
            return foundItem;
        }

        public static bool ContainsAUsedNoneTiltMeasureOfPointNamed<T>(this T src, string pointName, out Measures.Measure measure) where T : List<CompensationItem>
        {
            var foundItem = src.Find(x => x.Used == true && !(x.StateAndMeasure.Measure is Tilt.Measure) && x.StateAndMeasure.Measure._Point._Name == pointName);
            if (foundItem != null)
            {
                measure = foundItem.StateAndMeasure.Measure;
                if (measure != null)
                    return true;
            }
            measure = null;
            return false;
        }

        public static List<Tilt.Measure> GetUsedTiltMeasures<T>(this T src) where T : List<CompensationItem>
        {
            List<Tilt.Measure> measures = new List<Tilt.Measure>();
            var founds = src.FindAll(x => x.Used == true && x.StateAndMeasure.Measure is Tilt.Measure);
            foreach (var item in founds)
            {
                measures.Add(item.StateAndMeasure.Measure as Tilt.Measure);
            }
            return measures;
        }

        public static List<Measures.Measure> GetUsedNotTiltMeasures<T>(this T src) where T : List<CompensationItem>
        {
            List<Measures.Measure> measures = new List<Measures.Measure>();
            var founds = src.FindAll(x => x.Used == true 
                                       && !(x.StateAndMeasure.Measure == null) 
                                       && !(x.StateAndMeasure.Measure is Tilt.Measure));
            foreach (var item in founds)
            {
                measures.Add(item.StateAndMeasure.Measure as Measures.Measure);
            }
            return measures;
        }
    }
}
