using System;
using System.Xml.Serialization;
using TSU.IO.SUSoft;
using M = TSU.Common.Measures;
using P = TSU.Polar;
using R = TSU.Properties.Resources;

namespace TSU.Common.Compute.Compensations
{

    public interface ICompensation : ITsuObject, ICloneable
    {
        M.Measure Measure { get; set; }
        DoubleValue V0 { get; set; }
        DoubleValue Zt { get; set; }
        DoubleValue dL { get; set; }
        DoubleValue dR { get; set; }
        DoubleValue dZ { get; set; }

        M.Measure LgcResults { get; set; }
    }

    [Serializable]
    public class CompensationItem : TsuObject, ICompensation, IEquatable<CompensationItem>
    {
        private M.Measure measure;
        public M.Measure Measure
        {
            get
            {
                return measure;
            }

            set
            {
                measure = value;
                if (measure is P.Measure pm)
                {
                    distanceDisabled = pm.Distance._Status is M.States.Bad;
                    aHDisabled = pm.Angles.HorizontalState == M.States.Types.Bad;
                    aVDisabled = pm.Angles.VerticalState == M.States.Types.Bad;
                }
            }
        }

        [XmlIgnore]
        public P.Measure MeasureTheodolite
        {
            get
            {
                return measure as P.Measure;
            }
        }

        public DoubleValue V0 { get; set; }
        public DoubleValue Zt { get; set; }
        public DoubleValue dL { get; set; }
        public DoubleValue dR { get; set; }
        public DoubleValue dZ { get; set; }

        [XmlIgnore]
        public bool UseAsReference
        {
            get
            {
                return MeasureTheodolite.UsedAsReference;
            }
        }

        [XmlIgnore]
        public Geode.Roles GeodeRole
        {
            get
            {
                return MeasureTheodolite.GeodeRole;
            }
        }

        public M.Measure LgcResults { get; set; }

        private bool distanceDisabled = false;
        public bool DistanceDisabled
        {
            get => distanceDisabled;
            set
            {
                distanceDisabled = value;
                if (Measure is P.Measure pm)
                    if (distanceDisabled)
                        pm.Distance._Status = new M.States.Bad();
                    else
                        pm.Distance._Status = new M.States.Good();
            }
        }

        private bool aHDisabled = false;
        public bool AHDisabled
        {
            get => aHDisabled;
            set
            {
                aHDisabled = value;
                if (Measure is P.Measure pm)
                    pm.Angles.HorizontalState = aHDisabled ? M.States.Types.Bad : M.States.Types.Good;
            }
        }

        private bool aVDisabled = false;
        public bool AVDisabled
        {
            get => aVDisabled;
            set
            {
                aVDisabled = value;
                if (Measure is P.Measure pm)
                    pm.Angles.VerticalState = aVDisabled ? M.States.Types.Bad : M.States.Types.Good;
            }
        }


        // properties
        public CompensationItem()
        {

        }

        public override object Clone()
        {
            CompensationItem newItem = (CompensationItem)MemberwiseClone(); // to make a clone of the primitive types fields

            return newItem;
        }
        // IEquatable
        public virtual bool Equals(CompensationItem other)
        {
            return (this as TsuObject).Equals(other)
                && Measure == other.Measure;
        }
        public override bool Equals(object other)
        {
            if (other is CompensationItem ci)
                return Equals(ci);
            else
                return false;
        }

        //public override int GetHashCode()
        //{
        //    return
        //        base.GetHashCode() ^
        //        V0.GetHashCode() ^
        //        Measure.GetHashCode();
        //}

        public override string ToString()
        {
            // return string.Format("Measure of {0} in {1} state", this.Measure._PointName, this.State);
            string type = UseAsReference ? "CALA" : "POIN";
            return $"{R.T_MEASURE_OF} {Measure._PointName} {R.T_IS} {type}";
        }

        internal void Clean()
        {
            LgcResults = null;
            V0 = new DoubleValue();
            Zt = new DoubleValue();
            dL = new DoubleValue();
            dR = new DoubleValue();
            dZ = new DoubleValue();
        }
    }
}
