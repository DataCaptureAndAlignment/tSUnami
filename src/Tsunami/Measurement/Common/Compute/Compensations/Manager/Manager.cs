using System;
using System.Collections.Generic;
using System.Linq;
using TSU.Common.Compute.Compensations.BeamOffsets;
using TSU.Common.Elements.Composites;
using TSU.IO.SUSoft;
using TSU.Views;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using NV = TSU.Polar.Compensations.NonVerticalized;
using R = TSU.Properties.Resources;

namespace TSU.Common.Compute.Compensations.Manager
{
    public class Module : TSU.Manager
    {
        public Station Station { get; set; }

        public Strategies.Common Strategy { get; set; }

        public bool IsBeamOffsetsFrameCompensation { get; set; } = false;

        public new View View
        {
            get
            {
                return _TsuView as View;
            }
        }

        public bool IsAllMeasuresInInknownMode
        {
            get
            {
                foreach (var item in AllElements)
                {
                    if (item is CompensationItem i)
                    {
                        if (i.Measure._Status.Type == M.States.Types.Control)
                            continue; // we dont consider the controls
                        if (i.Measure.GeodeRole != Geode.Roles.Unknown)
                            return false;
                    }
                }

                return true;
            }
        }

        public override void EditItem(object tag1, object tag2)
        {
            if (EditItem_Action != null)
            {
                EditItem_Action(tag1, tag2);
                UpdateComputeOrNotAndUpdate();
                UpdateView();
            }
            else if (tag1 is CompensationItem item
                  && item.MeasureTheodolite is Polar.Measure pm)
            {
                //List<System.Windows.Forms.Control> cs = new List<System.Windows.Forms.Control>();
                if (tag2 is Geode.Roles gr && gr == pm.GeodeRole)
                {
                    void a(Geode.Roles newRole)
                    {
                        pm.GeodeRole = newRole;
                        if (newRole == Geode.Roles.Radi)
                        {
                            ChangeToRadi(item);
                        }
                        else if (newRole == Geode.Roles.Poin)
                        {
                            item.AHDisabled = false;
                            item.AVDisabled = false;
                            item.DistanceDisabled = false;
                        }
                        UpdateComputeOrNotAndUpdate();
                    }
                    this.View.ShowRoleChoicesAndAct(a);
                }

                if (tag2 is M.State stt && stt == pm._Status)
                {
                    void a(M.State newState)
                    {
                        newState.PreviousType = pm._Status.Type;
                        pm._Status = newState;
                        this.UpdateView();
                    }
                    this.View.ShowStateChoicesAndAct(a);
                }

                if (tag2 is I.Reflector.Reflector rf
                 && rf == pm.Distance.Reflector)
                {
                    var rm = new I.Reflector.Manager.Module(this.FinalModule._ActiveStationModule);
                    rm.View.ShowAsTreeView();
                    var newReflector = rm.SelectInstrument("Choose another reflector", selectables: rm.GetByClass(I.InstrumentClasses.PRISME), multiSelection:false);
                    if (newReflector is I.Reflector.Reflector r)
                    {
                        pm.Distance.Reflector = r;
                        UpdateComputeOrNotAndUpdate();
                    }
                }

                if (tag2 is DoubleValue dv)
                {
                    if (dv == pm.Extension)
                    {
                        if (Polar.Station.View.AskExtension(this.View, dv, pm, true, out string respond) && double.TryParse(respond, out double value))
                        {
                            pm.Extension.Value = value / 1000;
                            UpdateComputeOrNotAndUpdate();
                        }
                    }
                    else if (item.LgcResults is Polar.Measure rpm)
                    {
                        void a(string obs)
                        {
                            if (obs =="H")
                                item.AHDisabled = !item.AHDisabled;
                            else if (obs == "V")
                                item.AVDisabled = !item.AVDisabled;
                            else if (obs == "D")
                                item.DistanceDisabled = !item.DistanceDisabled;
                            if (item.AHDisabled && item.AVDisabled && item.DistanceDisabled)
                            {
                                item.MeasureTheodolite.GeodeRole = Geode.Roles.Unused;
                            }
                            UpdateComputeOrNotAndUpdate();
                        }
                        this.View.ShowDeactivationChoicesAndAct(a);
                    }
                }

                if (tag2 == "COMMENT")
                {
                    List<string> buttonTexts = new List<string> { R.T_OK, R.T_CANCEL };

                    MessageTsu.ShowMessageWithTextBox(R.T201, buttonTexts, out string buttonClicked, out string textInput, pm.CommentFromUser);

                    if (buttonClicked != R.T_CANCEL)
                    {
                        pm.CommentFromUser = textInput;
                        UpdateView();
                    }
                }
            }
        }

        private void UpdateComputeOrNotAndUpdate()
        {
            if (DsaFlag.GetByNameOrAdd(Tsunami2.Properties.DsaFlagForAdvancedModule, "UpdateResultAfterEachChangeInCompensationManager").IsTrue)
                this.View.RunCompute();
            else
                this.UpdateView();
        }

        public bool IsAllMeasuresInVariableMode
        {
            get
            {
                foreach (var item in AllElements)
                {
                    if (item is CompensationItem i)
                    {
                        if (i.Measure._Status.Type == M.States.Types.Control)
                            continue; // we dont consider the controls
                        if (i.Measure.GeodeRole != Geode.Roles.Poin)
                            return false;
                    }
                }

                return true;
            }
        }

        public Module()
            : base()
        {

        }

        public Module(TSU.Module module)
            : base(module, "Compensation manager")
        {

        }

        public override void Initialize()
        {
            AllElements = new List<TsuObject>();
            base.Initialize();
        }


        // IObservable<TsuObject>, IObserver<TsuObject>
        public override void OnNext(TsuObject TsuObject)
        {
        }

        //private static void AddText(FileStream fs, string value)
        //{
        //    byte[] info = new UTF8Encoding(true).GetBytes(value);
        //    fs.Write(info, 0, info.Length);
        //}

        public void SaveToXml(string fileName = null)
        {
            string filepath = View.GetSavingName(
              Tsunami2.Preferences.Values.Paths.Data + string.Format("{0:yyyyMMdd}", DateTime.Now) + "\\",
               Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now) + "_" + base._Name,
               "All files (*.*)|*.*|Geode (*.dat)|*.dat|Exp (*.res)|*.res|xml (*.xml)|*.xml");

            if (filepath != "")
            {
                IO.Xml.CreateFile(this, fileName);
                Shell.Run(fileName);
            }
        }

        internal void Show(
            string picture = "m",
            string titleAndMessage = null,
            string validate = "OK",
            string cancel = null)
        {
            titleAndMessage = titleAndMessage ?? R.T357;
            ManagerView m = _TsuView as ManagerView;
            m.UpdateView();
            m.ShowInMessageTsu(
                titleAndMessage ?? R.T358,
                validate, null,
                cancel, null,
                null, null,
                null, null);
        }

        internal void ShowAllMeasuresIncludedInParentModule()
        {
            AllElements.Clear();
            foreach (TSU.Module module in ParentModule.childModules)
            {
                if (module is FinalModule)
                {
                    var sm = (module as FinalModule)._ActiveStationModule;
                    if (sm != null)
                    {
                        var mm = sm._MeasureManager;
                        if (mm != null)
                            AllElements.AddRange(mm.AllElements);
                    }
                }
            }
        }

        internal override bool IsItemSelectable(TsuObject tsuObject)
        {
            return tsuObject is CompensationItem;
        }

        public Action<bool> Compensate_Action { get; set; } = null;

        public Action<object, object> EditItem_Action { get; set; } = null;

        public Action Validate_Action { get; set; } = null;

        public void Validate()
        {
            Validate_Action?.Invoke();
        }

        //public Frame FrameFromTag
        //{
        //    get
        //    {
        //        if (Tag == null) return null;
        //        Tuple<Frame, Frame.Parameters> tag = (Tuple<Frame, Frame.Parameters>)Tag;
        //        return tag.Item1;
        //    }
        //}

        //public Frame.Parameters FrameParamFromTag
        //{
        //    get
        //    {
        //        if (Tag == null) return null;
        //        Tuple<Frame, Frame.Parameters> tag = (Tuple<Frame, Frame.Parameters>)Tag;
        //        return tag.Item2;
        //    }
        //}

        internal void Update(bool editFiles = false)
        {
            if (this.Strategy == null)
                return;

            if (Compensate_Action != null)
            {
                Compensate_Action(editFiles);
                UpdateView();
                return;
            }

            View.selectionChanged = false;
            Strategy.editInput = editFiles;

            // treat selection
            {
                // if nothing yet selected
                if (IsAllMeasuresInInknownMode || IsAllMeasuresInVariableMode)
                {
                    Strategy.SetDefaultMeasureSelection(AllElements);
                }
            }

            Strategy.CheckGeoid();
            Strategy.ComputeApproximateSetup();
             
            Strategy.Compensate();

            // var test2 = Tsunami2.Properties.Points;
            Dictionary<string, List<(string element, double Sx, double Sy, double Sz)>> rotatedEllipsoids = null;

            if (Strategy.lgc2._Runned)
            {
                Strategy.station.Parameters2.Setups.ProposedValues = Strategy.lgc2._Result;
                Strategy.ComputeRotatedEllipsoid();

                
                    Strategy.ComputeRefPointsFromSetUpToHave_dRdLdZ();
            }

            // Reset values

            foreach (CompensationItem item in AllElements.OfType<CompensationItem>())
            {
                item.Clean();
            }

            if (Strategy.CompensationResults.Ready)
            {
                List<CompensationItem> goodItems = new List<CompensationItem>();
                foreach (CompensationItem item in AllElements.OfType<CompensationItem>())
                {
                    ComputeV0(item);
                    ComputeZt(item);
                    Compute_dL_dR_dZ(item);

                    // another list to get the residaul from lgcoutput in the right order (specailly if several time the same points name)
                    if (item.UseAsReference)
                    {
                        goodItems.Add(item);
                    }
                }

                if (editFiles) // more difficult because we dont know in advanced how many stuff to expect
                {
                    foreach (var m in Strategy.CompensationResults.Measures)
                    {
                        foreach (CompensationItem item in AllElements.OfType<CompensationItem>())
                        {
                            if (item.Measure._Point._Name == m._Point._Name)
                                item.LgcResults = m;
                            else
                                item.LgcResults = null;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < goodItems.Count; i++)
                    {
                        CompensationItem item = goodItems[i];
                        string wantedID = (item.MeasureTheodolite.IdForLgc).ToString();
                        item.LgcResults = Strategy.CompensationResults.Measures.Find(m => m.Id == wantedID);
                    }
                }
                ShowResultsInMessage();
            }
            UpdateView();
        }

        //private void AddGeodeRole()
        //{
        //    // Assign GEODE ROLE
        //    {
        //        // point with bearing will be used as radi
        //        // if no cala the first radi selected will be the cala
        //        bool calaFound = false;
        //        View.additionalMessageForRadi = "";
        //        Polar.Measure firstRadi = null;
        //        foreach (CompensationItem item in AllElements)
        //        {
        //            if (_SelectedObjects.Contains(item))
        //            {
        //                // if ccs coord and no bearing then cala
        //                if (!calaFound)
        //                    calaFound = !item.Measure._OriginalPoint._Parameters.hasGisement && !item.Measure._OriginalPoint._Coordinates.HasCcs;

        //                // new point measure during setup
        //                if (!item.Measure._OriginalPoint._Coordinates.HasAny)
        //                    item.MeasureTheodolite.GeodeRole = Geode.Roles.P_as_Ray;
        //                else
        //                // point used as ref, but cala or radi?
        //                {
        //                    if (item.Measure._OriginalPoint._Parameters.hasGisement)
        //                    {
        //                        item.MeasureTheodolite.GeodeRole = Geode.Roles.C_as_Radi;
        //                        View.additionalMessageForRadi += "Some of the selected references have a beam bearing, on those points the *RADI option will be used. ";
        //                        if (firstRadi == null)
        //                            firstRadi = item.MeasureTheodolite;
        //                    }
        //                    else
        //                        item.MeasureTheodolite.GeodeRole = Geode.Roles.R_as_Cala;
        //                }
        //            }
        //            else // not use as cala or radi
        //            {
        //                item.MeasureTheodolite.GeodeRole = Geode.Roles.P_as_Ray;
        //            }
        //        }

        //        // si aucun cala, le premier radi doit �tre transform� en CALA
        //        if (!calaFound)
        //        {
        //            if (firstRadi != null)
        //            {
        //                firstRadi.GeodeRole = Geode.Roles.R_as_Cala;
        //                View.additionalMessageForRadi += "The first point selected will be used as *CALA (first of the list by default). ";
        //            }
        //        }
        //    }
        //}

        private void ShowResultsInMessage()
        {
            double s0 = Strategy.CompensationResults.SigmaZero;

            string s = $"{R.T_LGC_RESULT};";
            s += $"{Strategy.GetSigma()} ({Strategy.GetSigmaComment()})\r\n";

            if (!(Strategy is NV.Setup))
            {
                s += $"{GetV0PerMeasure()}\r\n";
                s += $"\t{Strategy.CompensationResults.GetV0Information()}";
            }

            s += $"{Strategy.GetConfidenceEllipsoid1Sigma()}\r\n\r\n";

            // add link to files
            {
                if (Strategy.lgc2 != null)
                {
                    s += $"RES: {MessageTsu.GetLinkIfExist(Strategy.lgc2._Output.OutputFilePath)}\r\n";
                    s += $"ERR: {MessageTsu.GetLinkIfExist(Strategy.lgc2._Output._ErrorFilePath)}\r\n";
                    s += $"LOG: {MessageTsu.GetLinkIfExist(Strategy.lgc2._Output.LogPath)}\r\n";
                    s += $"INP: {MessageTsu.GetLinkIfExist(Strategy.lgc2._Input.FileName)}\r\n";
                }
                if (Strategy.plgc != null)
                {
                    s += $"POUT: {MessageTsu.GetLinkIfExist(Strategy.plgc.OutputFileName)}\r\n";
                    s += $"PERR: {MessageTsu.GetLinkIfExist(Strategy.plgc.ErrorFileName)}\r\n";
                    s += $"PINP: {MessageTsu.GetLinkIfExist(Strategy.plgc.InputFileName)}\r\n";
                    s += "\r\n";
                }
            }

            MessageType type;
            var tol = Tsunami2.Preferences.Values.Tolerances;
            if (s0 < tol.LGC_s0_Warning && s0 > -tol.LGC_s0_Warning)
                type = MessageType.GoodNews;
            else if (s0 < tol.LGC_s0_Critical && s0 > -tol.LGC_s0_Critical)
                type = MessageType.Warning;
            else
                type = MessageType.Critical;

            new MessageInput(type, s)
            {
                ButtonTexts = new List<string> { R.T_OK + "!" }
            }.Show();
        }


        private void ComputeZt(CompensationItem item)
        {
            item.Zt = Survey.ZtOneMeasure(
                Strategy.getTheRightCoordinates(Strategy.CompensationResults.StationPoint),
                Strategy.getTheRightCoordinates(item.Measure._OriginalPoint),
                item.Measure.Extension,
                (item.Measure as Polar.Measure).Angles.Corrected.Vertical,
                (item.Measure as Polar.Measure).Distance.Corrected);
        }

        private void ComputeV0(CompensationItem item)
        {
            item.V0 = Survey.VZeroOneMeasure(
                Strategy.getTheRightCoordinates(Strategy.CompensationResults.StationPoint),
                Strategy.getTheRightCoordinates(item.Measure._OriginalPoint),
                (item.Measure as Polar.Measure).Angles.Corrected.Horizontal);

        }

        private void Compute_dL_dR_dZ(CompensationItem item)
        {
            try
            {
                if (!item.UseAsReference)
                {
                    item.dR = new DoubleValue();
                    item.dL = new DoubleValue();
                    item.dZ = new DoubleValue();
                }
                else
                {
                    if (item.MeasureTheodolite.Offsets != null)
                    {
                        E.Coordinates beamV = null;
                        beamV = item.MeasureTheodolite.Offsets.BeamV;

                        if (beamV.AreKnown)
                        {
                            item.dR = beamV.X;
                            item.dL = beamV.Y;
                            item.dZ = beamV.Z;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Log.AddEntryAsPayAttentionOf(this, $"{R.T_THE_COMPUTATION_OF_THE_DL_DR_DZ_FAILED}, {R.T_IF_YOU_DONT_MIND_ABOUT}, {ex.Message}");
            }
        }

        private string GetV0PerMeasure()
        {

            int dec = Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;

            string s = "\r\nOrientation:\r\n";

            foreach (CompensationItem item in AllElements.OfType<CompensationItem>())
            {
                if (item.V0 != null && item.UseAsReference)
                    s += string.Format("{0,50}", "V0(" + item._Name + ")") + $" =\t{item.V0.ToString(dec)} gon\r\n";
            }
            return s;
        }

        internal void ChangeExtention(List<CompensationItem> l, DoubleValue value)
        {
            foreach (var i in l)
            {
                i.Measure.Extension = value;
            }
            UpdateView();
        }

        internal void DisableEnableDistance(List<CompensationItem> l)
        {
            foreach (var i in l)
                i.DistanceDisabled = !i.DistanceDisabled;
            UpdateView();
        }

        internal void ChangeToCala(List<CompensationItem> l)
        {
            foreach (CompensationItem i in l)
            {
                i.Measure.GeodeRole = Geode.Roles.Cala;
            }
            UpdateView();
        }
        internal void ChangeToPoin(List<CompensationItem> l)
        {
            foreach (CompensationItem i in l)
            {
                i.Measure.GeodeRole = Geode.Roles.Poin;
            }
            UpdateView();
        }

        internal void ChangeToRadi(List<CompensationItem> l)
        {
            foreach (CompensationItem i in l)
            {
                ChangeToRadi(i);
            }
            UpdateView();
        }

        internal void ChangeToRadi(CompensationItem i)
        {
            // check if bearing available
            if (i.Measure._OriginalPoint._Parameters.hasGisement)
                i.Measure.GeodeRole = Geode.Roles.Radi;
            else
            {
                string useCala = $"{R.T_USE} CALA {R.T_INSTEAD}";
                string titleAndMessage = $"{R.T_no_bearing} {R.T_FOR} {i.Measure._OriginalPoint._Name};{R.T_ENTER_A_VALUE} [gon]";
                List<string> buttonTexts = new List<string> { R.T_Apply, R.T_CANCEL };

                MessageTsu.ShowMessageWithTextBox(titleAndMessage, buttonTexts, out string buttonClicked, out string textInput, useCala);

                if (buttonClicked == R.T_CANCEL)
                    return;

                if (textInput == useCala)
                    i.Measure.GeodeRole = Geode.Roles.Cala;
                else
                {
                    if (double.TryParse(textInput, out double bearing))
                    {
                        i.Measure._OriginalPoint._Parameters.GisementFaisceau = bearing;
                        i.Measure.GeodeRole = Geode.Roles.Radi;
                    }
                    else
                        throw new Exception(R.T_WRONG_VALUE);
                }
            }
        }

        internal void ChangeToNotUsed(List<CompensationItem> l)
        {
            foreach (CompensationItem i in l)
            {
                i.Measure.GeodeRole = Geode.Roles.Unused;
            }
            UpdateView();
        }

        internal void DisableEnableAH(List<CompensationItem> l)
        {
            foreach (var i in l)
                i.AHDisabled = !i.AHDisabled;
            UpdateView();
        }

        internal void DisableEnableAV(List<CompensationItem> l)
        {
            foreach (var i in l)
                i.AVDisabled = ! i.AVDisabled;
            UpdateView();
        }

        Magnet lastMagnet;

        internal void GatherMeasurementsOfPreviousAssembly()
        {
            if (lastMagnet != null)
                GatherMeasurementsOfAssembly(lastMagnet);
            else
            {
                new MessageInput(MessageType.FYI, "No previous assembly avaialble").Show();
                GatherMeasurementsOfSelectedAssembly();
            }
        }

        internal void GatherMeasurementsOfAssembly(Magnet magnet)
        {
            Tsunami project = Tsunami2.Properties;
            if (magnet != null)
            {
                Results r = WorkFlow.Compute(this, project.MeasurementModules, magnet._Name, ObservationType.All, forceSetup: true);
                string message = WorkFlow.ResultsToString(magnet._Name, r.Offsets, r.dRoll);
                new MessageInput(MessageType.FYI, message).Show();
            }
        }


        internal void GatherMeasurementsOfSelectedAssembly()
        {
            Tsunami project = Tsunami2.Properties;
            E.Manager.Module emm = new E.Manager.Module(project)
            {
                AllElements = new List<TsuObject>()
            };
            emm.AllElements.AddRange(project.Elements);
            var magnet = emm.SelectOneMagnet();
            lastMagnet = magnet;
            GatherMeasurementsOfAssembly(magnet);
        }

        internal void EraseParametersOfSelectedAssembly()
        {
            if (this.View.GetParametersToErase(out Parameters p))
                Tsunami2.Properties.BOC_Context.ExistingAssemblyParameters.Remove(p);
        }

        internal void ResetBocParameters()
        {
            if (this.Tag is Results results)
            {
                // geth initial parameters
                var defaultExisting = Tsunami2.Properties.BOC_Context.ExistingAssemblyDefaultParameters.GetByAssemblyId(results.AssemblyId);
                results.Parameters = defaultExisting;
                results.RecomputeWithInitialParameters = true;
                Compensate_Action(false);
            }
        }
    }
}
