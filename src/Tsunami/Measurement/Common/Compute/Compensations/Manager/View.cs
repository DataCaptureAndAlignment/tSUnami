﻿using Accord.IO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TSU.Common.Strategies.Views;
using TSU.IO.SUSoft;
using TSU.Views;
using TSU.Views.Message;
using BOC = TSU.Common.Compute.Compensations.BeamOffsets;
using M = TSU.Common.Measures;
using P = TSU.Preferences;
using R = TSU.Properties.Resources;
using T = TSU.Tools;
using V = TSU.Common.Strategies.Views;

namespace TSU.Common.Compute.Compensations.Manager
{
    public partial class View : ManagerView
    {
        TreeView treeView;
        new Module Module;
        MessageModuleView _MeasureSelectionView;
        public MM_Buttons buttons;
        public View(TSU.IModule module) : base(module)
        {
            InitializeComponent();
            this.ApplyThemeColors();
            Module = module as Module;
            this.Image = R.Compute;
            InitializeMenu();

            this.AdaptTopPanelToButtonHeight();

            this.AutoCheckChildren = true;
        }

        private void ApplyThemeColors()
        {

        }
        private void AddBottomTreeview(Control destinationControl)
        {
            treeView = new TreeView() { Dock = DockStyle.Fill, ItemHeight = 16 };

            ImageList myImageList = new ImageList();
            myImageList.ImageSize = new System.Drawing.Size(16, 16);


            // Assign the ImageList to the TreeView.
            treeView.ImageList = myImageList;

            destinationControl.Controls.Add(treeView);
        }


        public override List<System.Windows.Forms.Control> GetViewOptionButtons()
        {
            Color b = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            Color f = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;

            List<System.Windows.Forms.Control> l = base.GetViewOptionButtons();


            if (DsaFlag.GetByNameOrAdd(Tsunami2.Properties.DsaFlagForAdvancedModule, "UpdateResultAfterEachChangeInCompensationManager").IsTrue)
                this.buttons.AutoCompute.HighLightWithColor();
            else
                this.buttons.AutoCompute.RestoreColor();

            l.Add(this.buttons.AutoCompute);

            if (!this.IsBeamOffsetsFrameCompensation)
            {
                l.Add(this.buttons.ChangeMeasureState);
                l.Add(this.buttons.ChangeRoles);
                l.Add(this.buttons.ChangeExtention);
                l.Add(this.buttons.DisableObservation);
            }

            return l;
        }

        internal override void InitializeStrategy()
        {
            this.AvailableViewStrategies = new List<V.Types>() { V.Types.List };

            this.SetStrategy(V.Types.List);
        }

        // Menu
        internal override void InitializeMenu()
        {
            //Creation of buttons
            buttons = new MM_Buttons(this);

            base.InitializeMenu();
            bigbuttonTop.ChangeNameAndDescription($"{R.T_STATION_SETUP};{R.T_ACCES_PLGC_AND_LGC_FILE}");
            bigbuttonTop.ChangeImage(R.Compute);
        }

        public bool IsBeamOffsetsFrameCompensation
        {
            get
            {
                if (this.Module != null)
                    return this.Module.IsBeamOffsetsFrameCompensation;
                else
                    return false;
            }
        }

        public override List<string> ListviewColumnHeaderTexts
        {

            get
            {
                if (IsBeamOffsetsFrameCompensation)
                    return new List<string>() {
                        "Point Name", "Type", "State", "Extension [mm]", "Used", "rR [mm]", "rS [mm]", "rT [mm]", "dRoll [mrad]", "sR [mm]", "sS [mm]", "sT [mm]", "sRoll [mrad]"};
                else
                    return new List<string>() {
                        "Point Name", "State", "Role", "Reflector", "Distance [m]", "Extension [mm]", "Time","v0 [gon]", "Zinst/Zt [m]", "rAH [cc]", "rAV [cc]", "rD [mm]", "dR [mm]", "dL [mm]", "dZ [mm]", "Comment" };
            }
            set { }
        }


        private void SetContextMenuToGlobal()
        {

            Color f = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            Color b = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;

            contextButtons.Clear();
            contextButtons.Add(buttons.Compute);
            bool isBocManager = Module.IsBeamOffsetsFrameCompensation;
            bool isMainManager = Module.isMainManager || Module.FinalModule == null;
            bool compensationInProgress = Module.AllElements.Count > 0;
            if (isBocManager && (isMainManager || compensationInProgress))
            {
                contextButtons.Add(buttons.GatherMeasurementOnAnAssembyForBOC);
                contextButtons.Add(buttons.GatherMeasurementOfPreviousAeemblyForBOC);
                contextButtons.Add(buttons.EraseParametersOnAnAssembyForBOC);
                contextButtons.Add(buttons.Validate);
            }
            //contextButtons.Add(buttons.EditAndCompute);
            if (compensationInProgress)
                contextButtons.Add(buttons.ResetBOCParameters);
            contextButtons.Add(buttons.EditInSurveyPad);
            contextButtons.Add(buttons.ViewInSurveyPad);

        }

        internal override void ShowContextMenuGlobal()
        {
            this.SetContextMenuToGlobal();
            this.ShowPopUpMenu(contextButtons);
        }

        //private void CloseMenuStrip(object sender, EventArgs e)
        //{
        //    (sender as Control).Hide();
        //}


        public bool IsBocView
        {
            get
            {
                return this.Module.Tag is Compensations.BeamOffsets.Results;
            }
        }

        // Update
        public override void UpdateView()
        {
            this.currentStrategy.Update();

            if (this.IsBocView)
            {
                if (this.Module.isMainManager)
                    if (this.currentStrategy is List listStategy)
                        this._PanelBottom.Controls.Add(listStategy.listView);
                UpdateBigButtonBasedOnBOCResult(this.bigbuttonTop, this.Module.Tag as BOC.Results);
            }
            else if (this.Module.Strategy?.CompensationResults != null)
            {
                Image image;
                double s0 = this.Module.Strategy.CompensationResults.SigmaZero;
                if (s0 < 2 && s0 > -2)
                    image = R.ComputeOK;
                else
                    image = R.ComputeKO;

                this.bigbuttonTop.SetAttributes($"{R.T_STATION_SETUP};{R.T_ACTUAL_VALUES}: {this.Module.Strategy.GetSetupLGC2Details()}", image);
                this.currentStrategy.ShowSelectionInButton();
            }
            this.Refresh();
        }

        private void UpdateBigButtonBasedOnBOCResult(BigButton bigbuttonTop, BOC.Results results)
        {
            string text = $"B.O.C setup: ";
            Color color;
            if (!results.OK || results.Frame?.DeeperFrame?.EstimatedParameters == null)
            {
                text += "Invalid compute";
                color = Tsunami2.Preferences.Theme.Colors.Bad;
            }
            else
            {

                // check s0 < 1.5
                double s0 = results.Sigma0;
                text += $"sigma0 ";
                bool sigmaZeroOk = s0 < 1.5;
                if (sigmaZeroOk)
                    text += $"OK ({s0:0.0} < 1.5)";
                else
                    text += $"KO ({s0:0.0} >= 1.5)";

                // check RY < 200cc
                double rY_mrad = results.Frame.DeeperFrame.EstimatedParameters.RY.Value / 200 * Math.PI * 1000;
                text += $", RY = {rY_mrad:0.0}mrad";

                // show sRY
                double sRY_mrad = results.Frame.DeeperFrame.EstimatedParameters.RY.Sigma / 200 * Math.PI * 1000;
                text += $", sRY ";
                bool sRYOk = sRY_mrad < 0.1;
                if (sRYOk)
                    text += $"OK ({sRY_mrad:0.0} < 0.1mrad)";
                else
                    text += $"KO ({sRY_mrad:0.0} >= 0.1mrad).";

                color = sigmaZeroOk && sRYOk ? Tsunami2.Preferences.Theme.Colors.Good : Tsunami2.Preferences.Theme.Colors.Bad;
            }
            // Mod button
            this.bigbuttonTop.ChangeNameAndDescription(text);
            this.bigbuttonTop.HighLightWithColor(color);
            this.Tag = new Tuple<string, System.Drawing.Color>(text, color);
        }

        public class MM_Buttons
        {
            private View View;

            //public BigButton main;

            public BigButton Compute;

            public BigButton GatherMeasurementOnAnAssembyForBOC;
            public BigButton GatherMeasurementOfPreviousAeemblyForBOC;
            public BigButton EraseParametersOnAnAssembyForBOC;
            public BigButton AutoCompute;
            //public BigButton EditAndCompute;
            public BigButton EditInSurveyPad;
            public BigButton ResetBOCParameters;
            public BigButton Validate;
            public BigButton ViewInSurveyPad;

            public BigButton ShowPlgcInput;
            public BigButton ShowLgcInput;
            public BigButton ShowPlgcError;
            public BigButton ShowPlgcOutput;
            public BigButton ShowLgcOutput;
            public BigButton ShowLgcLog;
            public BigButton ShowPlgcFiles;

            public BigButton ChangeExtention;

            public BigButton DisableObservation;
            public BigButton DisableDistance;
            public BigButton DisableAH;
            public BigButton DisableAV;
            public BigButton ChangeMeasureState;

            public BigButton ChangeRoles;
            public BigButton ChangeToRadi;
            public BigButton ChangeToCala;
            public BigButton ChangeToPoin;
            public BigButton ChangeToNotUsed;

            public MM_Buttons(View view)
            {
                View = view;
                //main = new BigButton($"{R.T_STATION_SETUP};{R.T_ACCES_PLGC_AND_LGC_FILE}", R.Compute, View.ShowContextMenuGlobal);

                Compute = new BigButton($"{R.T_COMPENSATE};{R.T_RUN_THE_COMPUTATION_WITH_SELECTED_ROLESSURES}", R.BOC_COMPUTE, View.RunCompute);
                GatherMeasurementOnAnAssembyForBOC = new BigButton($"{"Select Assembly and Gather measurements"};{"Look for last measurement in opened modules}"}", R.BOC___SETUP, View.Module.GatherMeasurementsOfSelectedAssembly);
                EraseParametersOnAnAssembyForBOC = new BigButton($"{"Select Assembly and erase user parameters"};{"You will have to revalidate the BOC setup"}", R.Delete_Ancrage, View.Module.EraseParametersOfSelectedAssembly);
                GatherMeasurementOfPreviousAeemblyForBOC = new BigButton($"{"Gather measurements"};{"Look for last measurement in opened modules}"}", R.Update, View.Module.GatherMeasurementsOfPreviousAssembly);

                AutoCompute = new BigButton($"Auto-{R.T_COMPENSATE};{R.T_RUN_THE_COMPUTATION_WITH_SELECTED_ROLESSURES}, everytime you change a value", R.Compute, View.ChangeAutoCompute);
                //EditAndCompute = new BigButton($"{R.T_EDIT_INPUTS_AND_RUN_LGC};{R.T_INPUT_FILES_WILL_BE_SHOWN_SO_THAT_YOU_CAN_MODIFY_THEM_BEFORE_THE_COMPUTATIONS}\r\n", R.Edit, View.EditAndRunCompute);
                ResetBOCParameters = new BigButton($"Reset current assembly parameters;Restart the current computation with default parameters", R.Update, View.Module.ResetBocParameters);
                Validate = new BigButton($"Validate;Parameters used for this compute will be used for next computation", R.ComputeOK, View.Module.Validate);

                EditInSurveyPad = new BigButton($"Edit and compute in SurveyPad;{R.T_RUN_SURVEYPAD_TO_EDIT_THE_LGC_INPUT_IF_INSTALLED}", R.SurveyPad, View.EditAndRunInSurveyPad);
                ViewInSurveyPad = new BigButton($"Show files in SurveyPad;{R.T_RUN_SURVEYPAD_TO_EDIT_THE_LGC_INPUT_IF_INSTALLED}", R.SurveyPad, View.ShowInSurveyPad);


                ShowPlgcFiles = new BigButton(R.T_SHOW_PLGC_OR_LGC_FILES, R.Element_File, View.ShowFilesMenu);
                ShowPlgcInput = new BigButton(R.T_SHOW_PLGC_INPUT, R.PLGC, View.ShowPLGCInput);
                ShowPlgcOutput = new BigButton(R.T_SHOW_PLGC_OUTPUT, R.PLGC, View.ShowPLGOutput);
                ShowPlgcError = new BigButton(R.T_SHOW_PLGC_ERROR_FILE, R.PLGC, View.ShowPLGError);

                ShowLgcInput = new BigButton(R.T_SHOW_LGC_INPUT, R.LgcInput, View.ShowLGCInput);
                ShowLgcOutput = new BigButton(R.T_SHOW_LGC_OUTPUT, R.LgcOutput, View.ShowLGCOutput);
                ShowLgcLog = new BigButton(R.T_SHOW_LOG_FILE, R.LgcLog, View.ShowLGCLog);


                ChangeExtention = new BigButton(R.T_CHANGE_EXTENTION, R.Rallonge, View.ChangeExtention);

                DisableObservation = new BigButton($"{"Disable parts of observations"};{"Disable AH, AV, or D"}", R.Distance, View.DisableObservation, null, true);
                DisableDistance = new BigButton($"{R.T_DISABLEENABLE_DISTANCE}", R.Distance, View.DisableDistance);
                DisableAH = new BigButton($"{R.T_DISABLEENABLE_AH}", R.AngleH, View.DisableAH);
                DisableAV = new BigButton($"{R.T_DISABLEENABLE_AV}", R.AngleV, View.DisableAV);

                ChangeMeasureState = new BigButton($"{R.T_B_MeasureChangeStatus}", R.Status, View.ChangeMeasurementsState, null, true);

                ChangeRoles = new BigButton($"{"Change Roles"}", R.Radi, View.ChangeRoles, null, true);
                ChangeToRadi = new BigButton($"*RADI;{R.T_RADI_ROLE_DETAILS}", R.Radi, View.ChangeToRadi);
                ChangeToCala = new BigButton($"*CALA;{R.T_CALA_ROLE_DETAILS}", R.Cala, View.ChangeToCala);
                ChangeToPoin= new BigButton($"*POIN;{R.T_POIN_ROLE_DETAILS}", R.Poin, View.ChangeToPoin);
                ChangeToNotUsed = new BigButton($"Not Used;{R.T_UNUSED_ROLE_DETAILS}", R.Unused, View.ChangeToNotUsed);

            }
        }



        private void DisableObservation()
        {
            this.ShowPopUpSubMenu(new List<Control>()
            {
                buttons.DisableAH, buttons.DisableAV, buttons.DisableDistance
            });
        }

        private void ChangeRoles()
        {
            this.ShowPopUpSubMenu(new List<Control>()
            {
                buttons.ChangeToCala, buttons.ChangeToPoin, buttons.ChangeToNotUsed, buttons.ChangeToRadi
            });
        }

        private void ChangeAutoCompute()
        {
            DsaFlag dsaflag = DsaFlag.GetByNameOrAdd(Tsunami2.Properties.DsaFlagForAdvancedModule, "UpdateResultAfterEachChangeInCompensationManager");
            if (dsaflag.State == DsaOptions.Always)
                dsaflag.State = DsaOptions.Never;
            else
                dsaflag.State = DsaOptions.Always;
        }

        private void DisableAV()
        {
            this.Module.DisableEnableAV(this.Module._SelectedObjects.Cast<CompensationItem>().ToList());
        }

        private void DisableAH()
        {
            this.Module.DisableEnableAH(this.Module._SelectedObjects.Cast<CompensationItem>().ToList());
        }

        private void DisableDistance()
        {
            this.Module.DisableEnableDistance(this.Module._SelectedObjects.Cast<CompensationItem>().ToList());
        }

        private void ChangeExtention()
        {
            foreach (CompensationItem i in this.Module._SelectedObjects)
            {
                bool useMM = (TSU.Tsunami2.Preferences.Values.GuiPrefs.UnitForExtension == P.Preferences.DistanceUnit.mm);
                string value;
                if (TSU.Polar.Station.View.AskExtension(this, i.Measure.Extension, i.Measure as Polar.Measure, useMM, out value))
                {
                    DoubleValue rall = T.Conversions.Numbers.ToDoubleValue(value);
                    if (useMM) rall.Value /= 1000;
                    this.Module.ChangeExtention(new List<CompensationItem>() { i }, rall);
                }
            }
        }

        private void ChangeMeasurementsState()
        {
            Action<M.State> a = (newState) =>
            {
                List<TsuObject> _SelectedObjects = this.Module._SelectedObjects.ToList();
                foreach (CompensationItem i in _SelectedObjects)
                {
                    if (newState == null)
                    {
                        newState = i.Measure._Status.GetPreviousState();
                    }
                    else
                    {
                        newState.PreviousType = i.Measure._Status.Type;
                    }
                    M.Manager.ChangeMeasureStatus(this, new List<TsuObject>() { i.MeasureTheodolite }, newState, silent: true);
                }

                this.UpdateView();
            };
            this.ShowStateChoicesAndAct(a);
        }

        internal void ShowDeactivationChoicesAndAct(Action<string> a)
        {
            this.ShowPopUpMenu(new List<Control>()
            {
            new BigButton($"{R.T_DISABLEENABLE_AH}", R.AngleH, ()=> {a("H"); }),
            new BigButton($"{R.T_DISABLEENABLE_AV}", R.AngleV, () => { a("V"); }),
            new BigButton($"{R.T_DISABLEENABLE_DISTANCE}", R.Distance, () => { a("D"); }),
            }, "activationObs");
        }

        internal void ShowRoleChoicesAndAct(Action<Geode.Roles> a)
        {
            this.ShowPopUpMenu(new List<Control>()
            {
            new BigButton($"*RADI;{R.T_RADI_ROLE_DETAILS}", R.Radi, ()=> {a(Geode.Roles.Radi); }),
            new BigButton($"*CALA;{R.T_CALA_ROLE_DETAILS}", R.Cala, () => { a(Geode.Roles.Cala); }),
            new BigButton($"*POIN;{R.T_POIN_ROLE_DETAILS}", R.Poin, () => { a(Geode.Roles.Poin); }),
            new BigButton($"Not Used;{R.T_UNUSED_ROLE_DETAILS}", R.Unused, () => { a(Geode.Roles.Unused); })
            }, "roles");
        }

        internal void ShowStateChoicesAndAct(Action<M.State> a)
        {
            this.ShowPopUpSubMenu(new List<Control>()
            {
                new BigButton(R.buttonStatusGood, R.StatusGood, ()=> { a(new M.States.Good()); }),
                new BigButton(R.butttonStatusBad, R.StatusBad, ()=> { a(new M.States.Bad()); }),
                new BigButton(R.buttonStatusControl, R.StatusControl, ()=> { a(new M.States.Control()); }),
                new BigButton(R.buttonStatusQuestionnable, R.StatusQuestionnable, ()=> { a(new M.States.Questionnable()); }),
                new BigButton(R.buttonStatusPrevious, R.Horizontal, ()=> { a(null); }),
            }, "state");
        }

        private void ChangeToCala()
        {
            this.Module.ChangeToCala(this.Module._SelectedObjects.Cast<CompensationItem>().ToList());
            this.currentStrategy.DeselectAll();
        }
        private void ChangeToPoin()
        {
            this.Module.ChangeToPoin(this.Module._SelectedObjects.Cast<CompensationItem>().ToList());
            this.currentStrategy.DeselectAll();
        }
        private void ChangeToRadi()
        {
            this.Module.ChangeToRadi(this.Module._SelectedObjects.Cast<CompensationItem>().ToList());
            this.currentStrategy.DeselectAll();
        }
        private void ChangeToNotUsed()
        {
            this.Module.ChangeToNotUsed(this.Module._SelectedObjects.Cast<CompensationItem>().ToList());
            this.currentStrategy.DeselectAll();
        }
        private void ShowFilesMenu()
        {
            if (Module.Strategy == null)
                return;
            List<Control> l = new List<Control>();
            if (Module.Strategy.lgc2 != null)
            {
                if (System.IO.File.Exists(this.Module.Strategy.lgc2._FileName + ".res")) l.Add(buttons.ShowLgcOutput);
                if (System.IO.File.Exists(this.Module.Strategy.lgc2._FileName + ".log")) l.Add(buttons.ShowLgcLog);
                if (System.IO.File.Exists(this.Module.Strategy.lgc2._FileName + ".inp")) l.Add(buttons.ShowLgcInput);
            }
            if (Module.Strategy.plgc != null)
            {
                if (System.IO.File.Exists(Plgc1._FileName + ".perr")) l.Add(buttons.ShowPlgcError);
                if (System.IO.File.Exists(Plgc1._FileName + ".pout")) l.Add(buttons.ShowPlgcOutput);
                if (System.IO.File.Exists(Plgc1._FileName + ".pinp")) l.Add(buttons.ShowPlgcInput);
            }
            this.ShowPopUpMenu(l, "file menu ");
        }

        private void EditAndRunCompute()
        {
            this.Module.Update(editFiles: true);
        }


        private void EditAndRunInSurveyPad()
        {
            TSU.Tsunami2.Preferences.Values.TemporaryFileOpeningMethod = P.Preferences.FileOpeningMethods.SurveyPad;
            this.Module.Update(editFiles: true);
            TSU.Tsunami2.Preferences.Values.TemporaryFileOpeningMethod = P.Preferences.FileOpeningMethods.NotDefined;
        }

        private void ShowInSurveyPad()
        {
            TSU.Tsunami2.Preferences.Values.TemporaryFileOpeningMethod = P.Preferences.FileOpeningMethods.SurveyPad;
            //this.Module.Update(editFiles: true);
            Shell.ExecutePathInDialogView(this.Module.Strategy.lgc2._FileName + ".INP", "SurveyPad", R.T_CANCEL, launchSurveyPadPlugin: true, waitProcessEnd: false);
            TSU.Tsunami2.Preferences.Values.TemporaryFileOpeningMethod = P.Preferences.FileOpeningMethods.NotDefined;
        }

        private void ShowLGCOutput()
        {
            this.Module.Strategy.lgc2._Output.ShowFile();
        }

        private void ShowPLGOutput()
        {
            this.Module.Strategy.plgc.ShowOutput();
        }

        private void ShowPLGError()
        {
            this.Module.Strategy.plgc.ShowErrorFile();
        }

        private void ShowLGCInput()
        {
            this.Module.Strategy.lgc2._Input.ShowInput(ForceSurveyPad: true); ;
        }

        private void ShowLGCLog()
        {
            this.Module.Strategy.lgc2._Output.EditLGC2LogFile();
        }

        private void ShowPLGCInput()
        {
            this.Module.Strategy.plgc.ShowInput();
        }

        internal void RunCompute()
        {
            bool dontEditFiles = false;
            this.Module.Update(dontEditFiles);
        }

        internal string additionalMessageForRadi = "";

        internal string ShowView(Action confirmSigmaZero, EventHandler makeFirstCompute, Action resetTemporary)
        {
            try
            {
                string respond = this.ShowInMessageTsu(
                    $"{R.T_COMPENSATION};{R.T_SELECTED_WHAT_YOU_WANT_AND_SEE_THE_EFFECT_ON_THE_RESULT}\r\n{additionalMessageForRadi}",
                    leftButton: R.T_VALID, leftAction: confirmSigmaZero,
                    middleButton: R.T_CANCEL, middleAction: resetTemporary,
                    onShowingMessage: makeFirstCompute);

                return respond;
            }
            catch (Exception ex)
            {
                new MessageInput(MessageType.Warning, ex.Message).Show();
                return R.T_CANCEL;
            }

        }

        internal bool AskNumericalValue(string message, double value, out double newValue)
        {
            newValue = 0;
            MessageInput mi = new MessageInput(MessageType.Choice, message)
            {
                ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL },
                Controls = new List<Control> { CreationHelper.GetPreparedTextBox(value.ToString()) }
            };
            using (var r = mi.Show())
            {
                if (r.TextOfButtonClicked == R.T_CANCEL)
                    return false;
                else
                    return double.TryParse(r.ReturnedControls[0].Text, out newValue);
            }
        }

        internal bool GetParametersToErase(out BeamOffsets.Parameters value)
        {
            value = null;

            BindingSource bs = new BindingSource() { DataSource = Tsunami2.Properties.BOC_Context.ExistingAssemblyParameters };
            ComboBox comboBox = CreationHelper.GetComboBox(bs);
            comboBox.DropDownStyle = ComboBoxStyle.DropDownList;

            MessageInput mi = new MessageInput(MessageType.Important, "Select parameters to be deleted;/!\\ there is no way back")
            {
                ButtonTexts = CreationHelper.GetOKCancelButtons(),
                Controls = new List<Control> { comboBox }
            };
            using (MessageResult res = mi.Show())
            {
                if (res.TextOfButtonClicked == R.T_OK)
                {
                    value = comboBox.SelectedItem as BOC.Parameters;
                    return true;
                }
                return false;
            }
        }
    }
}
