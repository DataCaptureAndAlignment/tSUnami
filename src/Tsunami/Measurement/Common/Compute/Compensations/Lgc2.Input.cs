﻿using EmScon;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Windows.Media;
using TSU.Common.Analysis;
using TSU.Common.Compute.Compensations.BeamOffsets;
using TSU.Common.Elements;
using TSU.Common.Instruments.Reflector;
using TSU.Common.Measures;
using TSU.ENUM;
using TSU.IO.SUSoft;
using E = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using P = TSU.Preferences;
using R = TSU.Properties.Resources;
using S = TSU.Tools.Conversions.StringManipulation;

namespace TSU.Common.Compute.Compensations
{
    public partial class Lgc2
    {
        internal class InstrumentWithUsedReflector
        {
            internal I.IPolarInstrument Instrument;
            internal List<Reflector> Reflectors = new List<Reflector>();
        }

        public class Input
        {
            internal Lgc2 lgc2;
            internal List<Point> CALAs = new List<Point>();
            internal List<Point> POINs = new List<Point>();
            internal List<Point> VXYs = new List<Point>();
            public string asText;

            private const string offsetFolder = "Offset";
            private const string levelFolder = "Level";

            internal List<Point> ALLpoints
            {
                get
                {
                    List<Point> all = new List<Point>();
                    all.AddRange(CALAs);
                    all.AddRange(POINs);
                    all.AddRange(VXYs);
                    return all;
                }
            }

            internal List<Point> CalaPointsFromFile
            {
                get
                {
                    List<Point> points = new List<Point>();
                    var lines = File.ReadLines(lgc2._FileName + FileFormat.INP);
                    bool read = false;
                    bool oloc = false;

                    foreach (var line in lines)
                    {
                        string trimmedLine = line.Trim();
                        if (trimmedLine.Length == 0) continue;
                        if (trimmedLine.Substring(0, 1) == "%") continue;

                        // OLOC or Geoid?
                        if (trimmedLine.Substring(0, 3) == "*OL") oloc = true;

                        bool keyLine = trimmedLine.Substring(0, 1) == "*";
                        bool calaLine = trimmedLine.Substring(0, 3) == "*CA";


                        if (read)
                        {

                            if (keyLine) return points;
                            string temp = Regex.Replace(trimmedLine.Trim(), @"\s+", " ");
                            string[] parts = temp.Split(' ');
                            if (parts.Length >= 4)
                            {
                                Coordinates c = new Coordinates(parts, 1);
                                Point p = new Point
                                {
                                    _Name = parts[0]
                                };
                                if (oloc)
                                {
                                    p._Coordinates.Su = c;
                                }
                                else
                                {
                                    p._Coordinates.Ccs = c;
                                }
                                points.Add(p);
                            }
                        }
                        if (calaLine) read = true;
                    }
                    return points;
                }
            }

            public string FileName
            {
                get
                {
                    return lgc2._FileName + FileFormat.INP;
                }
            }

            public Input()
            {
            }

            public Input(Lgc2 lgc2, string fileName)
            {
                lgc2.TreatFileName(fileName);
                this.lgc2 = lgc2;
            }

            #region Theodolite Inputs



            public string CreateInputFile(string content)
            {
                try
                {
                    content = content.Replace("%insert_here\r\n", "");
                    lgc2._Input.asText = content;
                    var path = lgc2._FileName + FileFormat.INP;
                    File.WriteAllText(path, content);
                    return path;
                }
                catch (Exception ex)
                {
                    throw new Exception($"Cannot Create input file {lgc2._FileName}", ex);
                }
            }

            //private Polar.Measure GetRoughComputedObservations(Point p1, Point p2, double v0, double ht, double Extension, Strategies.GetTheRightCoordinates getTheRightCoordinates)
            //{
            //    Coordinates c1 = getTheRightCoordinates(p1);
            //    Coordinates c2 = getTheRightCoordinates(p2);
            //    double bearing = Survey.GetBearing(c1, c2);
            //    double av = Survey.GetVerticalAngle(c1, c2, Extension, ht);
            //    double dhor = Survey.GetHorizontalDistance(c1, c2);
            //    double ah = bearing - v0;
            //    return
            //        new Polar.Measure()
            //        {
            //            Angles = new MeasureOfAngles()
            //            {
            //                Corrected = new E.Angles()
            //                {
            //                    Horizontal = new DoubleValue(ah, 100),
            //                    Vertical = new DoubleValue(av, 100)
            //                }
            //            },
            //            Distance = new MeasureOfDistance()
            //            {
            //                Corrected = new DoubleValue(dhor, 1000)
            //            }
            //        };
            //}

            internal static string BOC_Create(GatheredMeasures gMeasures, Frame modifiedFrame, out string proposedName)
            {
                TSU.Debug.WriteInConsole("((BOC))   Create LGC input");
                string content = "";
                proposedName = "";
                // Point Variables ajnd calage
                List<Point> cala = null;
                List<Point> poin = null;
                List<Point> vXY = null;
                List<Point> vZ = null;

                List<M.Measure> allMeasures = null;
                List<Station> stations = null;
                List<InstrumentWithUsedReflector> InstrumentWithUsedReflectors = null;


                // merge from stations
                try
                {
                    content += InputLine.Titr($"Input created by tsunami for BOC {DateTime.Now}");

                    // Refence surface
                    content += "*RS2K" + newLine;
                    content += InputLine.OptionFaut();
                    content += InputLine.OptionCreatePunchHFile();
                    content += InputLine.OptionJSON(true);
                    content += InputLine.Precision(7);
                    content += InputLine.Instr();

                    BOC_GetCalaAndVariablePoints(gMeasures.Polar, gMeasures.Ecarto, gMeasures.Level, out cala, out poin, out vXY, out vZ, out stations, out allMeasures, out InstrumentWithUsedReflectors);


                    foreach (var item in InstrumentWithUsedReflectors)
                    {
                        content += InputLine.Polar(item.Instrument, item.Reflectors,
                            new DoubleValue(0.0, 0.0), new DoubleValue(0.0, 0.0));
                    }

                    content += InputLine.Inclinometers(gMeasures.Roll);
                    foreach (var item in gMeasures.Ecarto)
                    {
                        foreach (var s in item.Item1._ListStationLine)
                        {
                            content += InputLine.OffsetInstrument(s);
                        }
                    }
                    content += InputLine.EDMTheo();

                }
                catch (Exception ex)
                {
                    throw new Exception($"{R.FindCALAPt} {newLine} {ex}", ex);
                }

                CloneableList<M.Measure> measures = new CloneableList<M.Measure>();
                measures.AddRange(allMeasures);
                content += InputLine.AddCalaPoints(cala, delegate (Point p) { return p._Coordinates.Ccs; }, out Coordinates barycenter, measures);

                // lets check if all *poin have some measure available, if not we have to comment them
                List<Point> pointToBeCommented = Theodolite.FindVariablePointsWithoutObservations(poin, stations);

                content += InputLine.AddVariablePoints(poin, delegate (Point p) { return p._Coordinates.Ccs; }, ref cala, "*POIN", pointToBeCommented, barycenter);

                content += InputLine.AddVariableInXY(vXY, "CCS-H");

                content += InputLine.AddVariableInZ(vZ, "CCS-H");

                try
                {
                    // Mesures par stations
                    foreach (var stationsAndMeasures in gMeasures.Polar)
                    {
                        proposedName += "Polar ";
                        Polar.Station s = stationsAndMeasures.Item1;


                        // in case a bad measure is manually selected to be used, we have to turn in questionnable or it will be commented in the LGCi
                        //for (int i = 0; i < measures.Count)

                            content += InputLine.Tstn(s, instrumentHeightKnown: true);
                    }

                    foreach (var stationsAndMeasures in gMeasures.Ecarto)
                    {
                        proposedName += "Ecarto ";
                        var station = stationsAndMeasures.Item1;
                        var statesAndMeasures = stationsAndMeasures.Item2;
                        foreach (var sl in station._ListStationLine)
                        {
                            content += InputLine.EchoCorrected(sl, statesAndMeasures);
                            content += InputLine.DsptTheoOffset(sl, statesAndMeasures);
                        }
                    }

                    foreach (var stationsAndMeasures in gMeasures.Level)
                    {
                        proposedName += "Level ";
                        var s = stationsAndMeasures.Item1;
                        var statesAndConcernedMeasures = stationsAndMeasures.Item2;

                        // specific here cause the input line method wants a station and not a list of measure
                        // so we clone the orinal station and remove all variable points that are not from the assembly
                        var clone = s.Clone() as Level.Station;
                        var measuresToRemove = new List<MeasureOfLevel>();
                        foreach (var measure in clone._MeasureOfLevel)
                        {
                            if (measure._Point.LGCFixOption == LgcPointOption.VZ || measure._Point.LGCFixOption == LgcPointOption.POIN)
                            {
                                if (!statesAndConcernedMeasures.ContainsWithSamePointName2(measure))
                                    measuresToRemove.Add(measure);
                            }
                        }
                        foreach (var useless in measuresToRemove)
                        {
                            clone._MeasureOfLevel.Remove(useless);
                        }
                        // we need to turn the unused as bad so we need clone
                        foreach (var stateAndMeasure in statesAndConcernedMeasures)
                        {
                            if (stateAndMeasure.IsUsed == false) // unused
                            {
                                clone._MeasureOfLevel.Remove(stateAndMeasure.Measure as MeasureOfLevel);
                                var cloneMeasuredTurnedToBad = stateAndMeasure.Measure.Clone() as MeasureOfLevel;
                                cloneMeasuredTurnedToBad._Status = new Measures.States.Bad();
                                clone._MeasureOfLevel.Add(cloneMeasuredTurnedToBad);
                            }
                        }

                        content += InputLine.AddDVerFromStation(clone); // method from DEP
                    }

                    content += Frame.ToLGC_CreateInput(modifiedFrame, cala, vXY, vZ, ref proposedName);

                    content += InputLine.End();

                    content = content.Replace("%insert_here\r\n", "");
                }
                catch (Exception ex)
                {
                    throw new Exception($"{R.FindMeas} {newLine} {ex}", ex);
                }
                return content;
            }

            private static void BOC_GetCalaAndVariablePoints(
                List<(Polar.Station, List<MeasureWithStatus>)> polarStationsAndMeasures,
                List<(Line.Station.Average, List<MeasureWithStatus>)> lineStationsAndMeasures,
                List<(Level.Station, List<MeasureWithStatus>)> levelStationsAndMeasures,
                out List<Point> cala,
                out List<Point> poin,
                out List<Point> vXY,
                out List<Point> vZ,
                out List<Station> stations,
                out List<M.Measure> allMeasures,
                out List<InstrumentWithUsedReflector> InstrumentWithUsedReflectors)
            {
                cala = new List<Point>();
                poin = new List<Point>();
                vZ = new List<Point>();
                vXY = new List<Point>();

                stations = new List<Station>();
                allMeasures = new List<M.Measure>();

                InstrumentWithUsedReflectors = new List<InstrumentWithUsedReflector>();

                //Polar
                foreach (var item in polarStationsAndMeasures)
                {
                    // *POIN
                    Polar.Station ps = item.Item1;

                    stations.Add(ps);
                    if (ps.MeasuresTaken.Count == 0) continue;

                    ps.MeasuresToExport.Clear();
                    item.Item1.Parameters2.GetGhostPointForV0(out Point GhostPoint, out Polar.Measure GhostMeasure, ((Polar.Measure)ps.MeasuresTaken[0]).Distance.Reflector, bLocal: false);
                    ps.MeasuresToExport.Add(GhostMeasure);

                    var items = item.Item2;
                    foreach (var stateAndMeasure in items)
                    {
                        var measureToexport = stateAndMeasure.Measure;
                        if (stateAndMeasure.IsUsed == false)
                        {
                            measureToexport = measureToexport.Clone() as Polar.Measure;
                            measureToexport._Status = new Measures.States.Bad();
                        }

                        ps.MeasuresToExport.Add(measureToexport);
                    }

                    object a = Tsunami2.Properties;

                    InstrumentWithUsedReflectors = InstrumentWithUsedReflectors.Concat(
                        Theodolite.GetNotExistingIntrumentTargetpairFrom(ps, InstrumentWithUsedReflectors)).ToList();

                    foreach (var stateAndMeasure in items)
                    {
                        if (stateAndMeasure.IsUsed)
                            poin.Add(stateAndMeasure.Measure._Point);

                        // Measures
                        allMeasures.Add(stateAndMeasure.Measure);
                    }

                    // *CALA
                    cala.Add(item.Item1.Parameters2._StationPoint);
                    cala.Add(GhostPoint);
                }

                // ecarto
                foreach (var stationAndMeasures in lineStationsAndMeasures)
                {
                    Line.Station.Average station = stationAndMeasures.Item1;
                    stations.Add(station);
                    var statesAndMeasures = stationAndMeasures.Item2;
                    foreach (var stateAndMeasure in statesAndMeasures)
                    {
                        bool used = stateAndMeasure.IsUsed;
                        var m = stateAndMeasure.Measure;
                        // *VXY
                        if (!poin.ContainsWithSameName(m._Point))
                            vXY.AddIfDoNotExistWithSameName(m._Point);

                        // Measures
                        allMeasures.Add(m);
                    }
                    // *CALA
                    foreach (var stationLine in stationAndMeasures.Item1._ListStationLine)
                    {
                        cala.AddIfDoNotExistWithSameName(stationLine._Anchor1._Point);
                        cala.AddIfDoNotExistWithSameName(stationLine._Anchor2._Point);
                    }
                }

                // level
                foreach (var stationAndMeasures in levelStationsAndMeasures)
                {
                    Level.Station station = stationAndMeasures.Item1;
                    stations.Add(station);
                    var statesAndMeasures = stationAndMeasures.Item2;

                    // add in *CALA : all point of reference not yet in *cala
                    foreach (var measure in station._MeasureOfLevel)
                    {
                        if (measure._Point.LGCFixOption == LgcPointOption.CALA || measure._Point.LGCFixOption == LgcPointOption.VXY)
                        {
                            cala.AddIfDoNotExistWithSameName(measure._Point);
                            allMeasures.Add(measure);
                        }
                    }

                    // add in *VZ the point measured taht are not yet in *poin or *vz
                    // in the mean time we have to measure the barycenter of the measured point so taht we have a X and Y not too wrng for the station point
                    double X = 0;
                    double Y = 0;
                    int numberOfMeasuredPoint = stationAndMeasures.Item2.Count;
                    foreach (var stateAndMeasure in statesAndMeasures)
                    {
                        var measure = stateAndMeasure.Measure;
                        bool notBadMeasure = measure._Status.Type != M.States.Types.Bad;
                        bool correctPoint = !poin.ContainsWithSameName(measure._Point);
                        bool notAlreadyInVzList = !vZ.ContainsWithSameName(measure._Point);
                        if (notBadMeasure && correctPoint && notAlreadyInVzList)
                            vZ.Add(measure._Point);

                        allMeasures.Add(measure);

                        X += measure._Point._Coordinates.Ccs.X.Value;
                        Y += measure._Point._Coordinates.Ccs.Y.Value;
                    }
                    X /= numberOfMeasuredPoint;
                    Y /= numberOfMeasuredPoint;

                    // add station point to *vZ
                    if (!poin.ContainsWithSameName(station._Parameters._StationName) && !vZ.ContainsWithSameName(station._Parameters._StationName))
                    {
                        vZ.Add(new Point()
                        {
                            _Name = station._Name,
                            _Coordinates = new CoordinatesInAllSystems()
                            {
                                Ccs = new Coordinates()
                                {
                                    X = new DoubleValue(X),
                                    Y = new DoubleValue(Y),
                                    Z = new DoubleValue(station._Parameters._HStation),
                                }
                            }
                        });
                    }
                }

                // check if *VXY+*VZ should become *POIN
                var toSwitch = new List<Point>();
                foreach (var item in vZ)
                {
                    if (vXY.ContainsWithSameName(item._Name))
                    {
                        toSwitch.AddIfDoNotExistWithSameName(item);
                    }
                }
                foreach (var item in toSwitch)
                {
                    poin.AddIfDoNotExistWithSameName(item);
                    vXY.RemoveAllByName(item._Name);
                    vZ.RemoveAllByName(item._Name);
                }
            }

            public void CreateFromSeveralStations(List<Station.Module> stms, List<Point> points, string title = "", int forcedPrecision = 7, bool consiLibre = false)
            {
                string content = "";



                // Point Variables ajnd calage
                List<Point> cala = lgc2._Input.CALAs;
                List<Point> poin = lgc2._Input.POINs;
                List<Point> vXY = lgc2._Input.VXYs;
                List<Point> vZ = new List<Point>();
                List<M.Measure> AllMeasures = new List<M.Measure>();
                List<Station> stations = new List<Station>();

                SeveralStations_CreateHeader(stms, ref title, forcedPrecision, ref content, ref poin, ref vXY, AllMeasures, stations, consiLibre);
                lgc2._Input.POINs = poin; // because .concat lost the ref.

                
                Coordinates barycenter = SeveralStations_AddCala(ref content, stms, points, cala, poin, vXY, AllMeasures, consiLibre);

                SeveralStations_AddVariablePoints(ref content, ref cala, poin, vXY, stations, barycenter, consiLibre);
                content = SeveralStations_AddStations(stms, content, barycenter);

                content += InputLine.End();

                try
                {
                    CreateInputFile(content);
                }
                catch (Exception ex)
                {
                    throw new Exception($"{R.WriteAllText} {newLine} {ex}", ex);
                }
            }

            public void CreateForIntersection(List<Station.Module> stms, List<Point> pointsWithNoCoordinates, string title = "", int forcedPrecision = 7)
            {
                string content = "";

                // Point Variables ajnd calage
                List<Point> cala = lgc2._Input.CALAs;
                List<Point> poin = lgc2._Input.POINs;
                List<Point> vXY = lgc2._Input.VXYs;
                List<M.Measure> AllMeasures = new List<M.Measure>();
                List<Station> stations = new List<Station>();

                SeveralStations_CreateHeader(stms, ref title, forcedPrecision, ref content, ref poin, ref vXY, AllMeasures, stations);

                Coordinates barycenter;
                // CALA is Station Points + Ghosts
                {
                    foreach (Polar.Station.Module pstm in stms)
                    {
                        var stationPoint = pstm.stationParameters._StationPoint;
                        cala.Add(stationPoint);

                        var ghost = stationPoint.Clone() as Point;
                        ghost._Name = "GHOST_" + ghost._Name;
                        var coordinates = lgc2.getTheRightCoordinates(ghost);
                        if (ghost._Coordinates.Ccs.Y.Value == coordinates.Y.Value) // so sad...
                        {
                            ghost._Coordinates.Ccs.Y.Value += 1;
                        }
                        else
                            ghost._Coordinates.Su.Y.Value += 1;
                        cala.Add(ghost);
                    }
                    content += InputLine.AddCalaPoints(cala, lgc2.getTheRightCoordinates, out barycenter, null);
                }

                // POIN are the point to intersect
                {
                    poin.AddRange(pointsWithNoCoordinates);
                    content += InputLine.AddVariablePoints(poin, lgc2.getTheRightCoordinates, ref cala, "*POIN", pointToBeCommented: null, barycenter);
                }
                content = SeveralStations_AddStations(stms, content, barycenter, restrictedMeasures: pointsWithNoCoordinates, addGhostObservation: true);

                content += InputLine.End();

                try
                {
                    CreateInputFile(content);
                }
                catch (Exception ex)
                {
                    throw new Exception($"{R.WriteAllText} {newLine} {ex}", ex);
                }
            }

            private string SeveralStations_AddStations(List<Station.Module> stms, string content, Coordinates barycenter, List<Point> restrictedMeasures = null, bool addGhostObservation = false)
            {
                try
                {
                    // Mesures par stations
                    foreach (Station.Module item in stms)
                    {
                        if (item is Polar.Station.Module stm)
                        {
                            Polar.Station st = stm.StationTheodolite;
                            Polar.Station.Parameters.Setup.Values initialValues = st.Parameters2.Setups.InitialValues;

                            Frame frameForStation;
                            if (initialValues.VerticalisationState == Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation.NotVerticalised)
                            {
                                var stationCoordinates = barycenter;
                                if (st.Parameters2.Setups.BestValues?.Verified != null)
                                {
                                    var coord = lgc2.getTheRightCoordinates(st.Parameters2.Setups.BestValues.StationPoint);

                                    stationCoordinates = coord;
                                }
                                frameForStation = Polar.Compensations.NonVerticalized.Setup.GetFrameForSetup(st, stationCoordinates);
                            }
                            else
                                frameForStation = null;

                            // if (stm.stationParameters._State is Stations.Station.State.Bad) continue; we want them with "% "
                            string temp = InputLine.Tstn(
                                st,
                                initialValues.IsInstrumentHeightKnown == Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight.Known,
                                UseOnlyLastMeasure: false,
                                useObservationSigma: false,
                                useDistance: true,
                                lgc2: lgc2,
                                CommentBadStations: true,
                                vm: InputLine.VerticalMode.Setup,
                                frame: frameForStation,
                                restrictedMeasures: restrictedMeasures);
                            if (addGhostObservation)
                                temp = temp.Replace("*ANGL", "*ANGL\r\n" + "   " + "   " + "   " + "GHOST_" + st.Parameters2._StationPoint._Name.ToUpper() + " " + (400 - st.Parameters2.Setups.BestValues.vZero.Value).ToString() + "   OBSE 0.1   ");
                            content += temp;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"{R.FindMeas} {newLine} {ex}", ex);
                }

                return content;
            }

            private void SeveralStations_AddVariablePoints(ref string content, ref List<Point> cala, List<Point> poin, List<Point> vXY, List<Station> stations, Coordinates barycenter, bool consiLibr = false)
            {
                // lets check if all *poin have some measure available, if not we have to comment them
                List<Point> pointToBeCommented = Theodolite.FindVariablePointsWithoutObservations(poin, stations);

                //CALA
                List<Point> alreadyPresent = new List<Point>(cala);

                if (consiLibr)
                {
                    poin.AddRange(vXY);
                    vXY.Clear();
                }



                //POIN
                content += InputLine.AddVariablePoints(poin, lgc2.getTheRightCoordinates, ref alreadyPresent, "*POIN", pointToBeCommented, barycenter);

                //VXY
                content += InputLine.AddVariablePoints(vXY, lgc2.getTheRightCoordinates, ref alreadyPresent, "*VXY", pointToBeCommented, barycenter);

                //*RADI must be after *POIN .... !?

                if (vXY.Count > 0 && !consiLibr)
                {
                    content += InputLine.AddRadi(vXY);
                    content += InputLine.AddRadiLine(cala, commented: true);  // we want to get bearing of the cala point commented to be able to reactivated it easily
                }
            }

            private Coordinates SeveralStations_AddCala(ref string content, List<Station.Module> stms, List<Point> points, List<Point> cala, 
                List<Point> poin, List<Point> vXY, List<M.Measure> AllMeasures, bool consiLibre = false)
            {
                try
                {
                    // Add to cala all point that are not in variable points
                    foreach (TsuObject item in points.FindAll(x => x.State != E.Element.States.FromStation))
                    {
                        if (item is Point)
                        {
                            Point p = item as Point;
                            // si deja dans variable continue to next  point
                            if (poin.Find(x => x._Name == p._Name) != null) continue;
                            if (vXY.Find(x => x._Name == p._Name) != null) continue;
                            // si deja dans Calage continue to next  point
                            if (cala.Find(x => x._Name == p._Name) != null) continue;

                            // Do not export the station point for non-verticalized, since it is already defined in the setup frame
                            // is a station point
                            bool IsNonVerticalizedAndIsStationnedAtTheCurrentPoint(Polar.Station.Module stm)
                            {
                                Polar.Station.Parameters parameters2 = stm.StationTheodolite.Parameters2;
                                return parameters2.Setups.InitialValues.VerticalisationState == Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation.NotVerticalised
                                    && parameters2._StationPoint._Name == item._Name;
                            }
                            if (stms.OfType<Polar.Station.Module>().Any(IsNonVerticalizedAndIsStationnedAtTheCurrentPoint))
                                continue;

                            // slip if not found in any measure
                            if (Tsunami2.Preferences.Values.GuiPrefs.HideUnusedCalaPoint.IsTrue)
                            {
                                bool used = false;

                                // is a station point
                                foreach (Polar.Station.Module stm in stms.OfType<Polar.Station.Module>())
                                {
                                    if (stm.StationTheodolite.Parameters2._StationPoint != null && stm.StationTheodolite.Parameters2._StationPoint._Name == item._Name)
                                    {
                                        used = true;
                                        break;
                                    }
                                }

                                // is a measured point
                                foreach (var measure in AllMeasures)
                                {
                                    if (measure._Point._Name == item._Name)
                                    {
                                        used = true;
                                        break;
                                    }
                                }
                                if (!used) continue;
                            }

                            if (consiLibre)
                                poin.Add(p);
                            else
                                cala.Add(p);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"{R.FindCALAPt} {newLine} {ex}", ex);
                }

                Coordinates barycenter = null;
                CloneableList<M.Measure> measures = new CloneableList<M.Measure>();
                measures.AddRange(AllMeasures);
                content += InputLine.AddCalaPoints(cala, lgc2.getTheRightCoordinates, out barycenter, measures);
                return barycenter;
            }

            private void SeveralStations_CreateHeader(List<Station.Module> stms, ref string title, int forcedPrecision, ref string content, 
                ref List<Point> poin, ref List<Point> vXY, List<M.Measure> AllMeasures, List<Station> stations, bool consiLibre = false)
            {

                // Instruments
                List<InstrumentWithUsedReflector> InstrumentWithUsedReflectors = new List<InstrumentWithUsedReflector>();

                // merge from stations
                try
                {
                    if (title == "")
                    {
                        title = $"{R.InputTSU} 'Several Stations' {TSU.Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now)}";
                    }
                    else
                        title = $"{title} for several stations on {TSU.Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now)} ";

                    content += InputLine.Titr(title);

                    // Refence surface
                    string refSurfOption = lgc2.getTheRightOption();
                    content += refSurfOption + newLine;


                    if (consiLibre)
                        content += InputLine.OptionConsiLibr();
                    else
                    {
                        content += InputLine.OptionFaut();

                        if (refSurfOption.ToUpper() != "*OLOC")
                            content += InputLine.OptionCreatePunchHFile();
                        else
                            content += InputLine.OptionCreatePunchFile();

                        content += InputLine.OptionJSON(true);
                    }
                    
                    content += InputLine.Precision(forcedPrecision);
                    content += InputLine.Instr();

                    foreach (Polar.Station.Module item in stms.OfType<Polar.Station.Module>())
                    {
                        if (item is Polar.Station.Module stm)
                        {
                            var station = stm.StationTheodolite;
                            stations.Add(item._Station as Polar.Station);
                            if (station.MeasuresTaken.Count == 0) continue;

                            InstrumentWithUsedReflectors = InstrumentWithUsedReflectors.Concat(
                                Theodolite.GetNotExistingIntrumentTargetpairFrom(station, InstrumentWithUsedReflectors)).ToList();

                            poin = poin.Concat(
                                Theodolite.GetNotExistingVariablePointsFrom(station, poin)).ToList();

                            foreach (Polar.Measure measure in station.MeasuresTaken)
                            {
                                if (measure.GeodeRole == Geode.Roles.Radi)
                                    vXY.Add(measure._Point);
                            }

                            AllMeasures.AddRange(stm.StationTheodolite.MeasuresTaken);
                        }
                        else
                            InputLine.Comment($"{item.NameAndDescription} {R.T_NOT_YET_AVAILABLE}");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"{R.CollectEachSt} {newLine} {ex}", ex);
                }

                foreach (var item in InstrumentWithUsedReflectors)
                {
                    content += InputLine.Polar(item.Instrument, item.Reflectors,
                        new DoubleValue(0.0, 0.0), new DoubleValue(0.0, 0.0));
                }
            }

            //private void RemovePointsWithNoAvailableCoordinates(List<Point> pointsVariables, Strategies.GetTheRightCoordinates getTheRightCoordinates)
            //{
            //    for (int i = 0; i < pointsVariables.Count - 1; i++)
            //    {
            //        Coordinates c = getTheRightCoordinates(pointsVariables[i]);
            //        if (c.X.Value == na)
            //        {
            //            pointsVariables.RemoveAt(i);
            //            i--;
            //        }
            //    }
            //}

            #endregion

            #region INP file creation
            /// <summary>
            /// This will define the method to use in to get the rigth compute option i.e *OLOC and the right type of coordinate, it will give a ref to getTheRightOption() and GetTheRightCoordinates()
            /// </summary>
            /// <param name="station"></param>


            /// <summary>
            /// crée fichier input pour mesure fil
            /// </summary>
            /// <param name="line"></param>
            /// <param name="title"></param>
            /// 

            public static Coordinates GetAPointCloseToTheLhcCenter(ref string message)
            {
                double r = (double)Tsunami2.Properties.Rnd.Next(1, 1000) / 10000;

                message = "\t % LHC center coord. + random cm used.";

                return new Coordinates("", 666, 6666, 400) + r; // add a random 1 to 1000 mm just in case LGC complain about equal coordinates.
            }

            public void SetFileName(string fileName, string folderName, string title)
            {
                lgc2.TreatFileName(fileName, folderName);
                if (title == "")
                {
                    _ = R.StringLGC2_Title + Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now);
                }
            }

            [Flags]
            private enum LgcOptions
            {
                None = 0,
                Oloc = 1,
                Rs2k = 2,
                Libr = 4,
            }

            private void SetDefaultContent(ref string content, string title, int precision, bool edmTheo, LgcOptions option)
            {
                content += InputLine.Titr(title);

                if ((option & LgcOptions.Oloc) == LgcOptions.Oloc)
                    content += InputLine.OptionOloc() + newLine;
                else if ((option & LgcOptions.Rs2k) == LgcOptions.Rs2k)
                    content += InputLine.OptionRS2K() + newLine;

                if ((option & LgcOptions.Libr) == LgcOptions.Libr)
                    content += InputLine.OptionLIBR();

                content += InputLine.OptionHist();
                content += InputLine.OptionCreatePunchEFile();
                content += InputLine.OptionFaut();
                content += InputLine.OptionJSON(true);
                content += InputLine.Precision(precision);
                content += InputLine.Instr();
                if (edmTheo)
                {
                    content += $"{InputLine.EDMTheo()} {newLine}";
                }


            }


            public void SetCoordTheo(ref string content, Line.Module moduleLine, CoordinatesType coordType)
            {
                switch (coordType)
                {
                    case CoordinatesType.CCS:
                        content += $"{InputLine.AddAllLibrPointsCCS(moduleLine)} {newLine}";
                        content += $"{InputLine.DsptTheoCCS(moduleLine)} {newLine}";
                        break;
                    case CoordinatesType.SU:
                        content += $"{InputLine.AddAllLibrPointsSU(moduleLine)} {newLine}";
                        content += $"{InputLine.DsptTheoSU(moduleLine)} {newLine}";
                        break;
                    case CoordinatesType.UserDefined:
                        //content += $"{InputLine.DsptTheoUserDefined(moduleLine)} {newLine}";
                        break;
                    default:
                        content += $"{InputLine.DsptTheoCCS(moduleLine)} {newLine}";
                        break;
                }
                content += $"{InputLine.EchoCorrected(moduleLine)} {newLine}";
            }

            public void SetCoordTheo(ref string content, Line.Station stationLine, CoordinatesType coordType)
            {

                switch (coordType)
                {
                    case CoordinatesType.CCS:
                        content += $"{InputLine.AddCalaPointsCCS(stationLine)} {newLine}";
                        content += $"{InputLine.AddVariablePointsCCS(stationLine)} {newLine}";
                        content += $"{InputLine.DsptTheoCCS(stationLine)} {newLine}";
                        break;
                    case CoordinatesType.SU:
                        content += $"{InputLine.AddCalaPointsSU(stationLine)} {newLine}";
                        content += $"{InputLine.AddVariablePointsSU(stationLine)} {newLine}";
                        content += $"{InputLine.DsptTheoSU(stationLine)} {newLine}";
                        break;
                    case CoordinatesType.UserDefined:
                        //content += InputLine.DsptTheoUserDefined(stationLine) + newLine;
                        break;
                    default:
                        content += $"{InputLine.AddCalaPointsCCS(stationLine)} {newLine}";
                        content += $"{InputLine.AddVariablePointsCCS(stationLine)} {newLine}";
                        content += $"{InputLine.DsptTheoCCS(stationLine)} {newLine}";
                        break;

                }
                content += $"{InputLine.EchoCorrected(stationLine)} {newLine}";


            }

            public void EndFileAndSave(ref string content)
            {
                content += InputLine.End();
                File.WriteAllText(lgc2._FileName + FileFormat.INP, content);

            }

            public void SetSingleOffsetMeasurement(ref string content, Line.Station stationLine)
            {
                content += InputLine.OffsetInstrument(stationLine);
                content += $"{InputLine.AddCalaPointsOffsets(stationLine)} {newLine}";
                content += $"{InputLine.AddVariablePointsOffsets(stationLine)} {newLine}";
                content += $"{InputLine.EchoReduced(stationLine)} {newLine}";
                content += $"{InputLine.DsptTheoOffset(stationLine)} {newLine}";
            }

            public void SetSingleLevelMeasurement(ref string content, Level.Station.Module stationLevelModule)
            {
                content += InputLine.LevelInstrument(stationLevelModule);
                content += InputLine.StaffsForLeveling(stationLevelModule);
                content += InputLine.AddCalaPoints(stationLevelModule);
                content += InputLine.AddVariablePoints(stationLevelModule);
                content += InputLine.AddDVER(stationLevelModule);
                content += InputLine.AddDLEV(stationLevelModule);

            }

            public void SetAllLevelMeasurement(ref string content, Level.Module moduleLevel)
            {
                //content += InputLine.LevelInstrument(stationLevelModule); //Pas besoin car le calcul LGC en tenant compte des distances venant du niveau n'est pas intégré
                //content += InputLine.StaffsForLeveling(stationLevelModule);//Pas besoin car le calcul LGC en tenant compte des distances venant du niveau n'est pas intégré
                content += InputLine.AddAllPointsAndStation(moduleLevel);
                //content += InputLine.AddVariablePoints(levelModule);
                content += InputLine.AddDVER(moduleLevel);
                //content += InputLine.AddDLEV(stationLevelModule); //Pas besoin car le calcul LGC en tenant compte des distances venant du niveau n'est pas intégré
            }

            public void CreateAsSingleOffsetMeasurement(Line.Station stationLine, string fileName, string title = "")
            {
                int precision = 7;
                string content = "";
                SetFileName(fileName, offsetFolder, title);
                SetDefaultContent(ref content, title, precision, true, LgcOptions.Oloc);
                SetSingleOffsetMeasurement(ref content, stationLine);
                EndFileAndSave(ref content);

            }
            /// <summary>
            /// Creation Fichier LGC input pour calcul LGC global de tous les fils
            /// </summary>
            /// <param name="moduleLine"></param>
            /// <param name="title"></param>
            internal void CreateAsLibrAllWiresMeasurement(Line.Module moduleLine, string fileName, string title = "")
            {
                SetFileName(fileName, offsetFolder, title);

                int precision = 7;
                string content = "";
                CoordinatesType coordType = (moduleLine._ActiveStationModule as Line.Station.Module).WorkingAverageStationLine._Parameters._CoordType;
                LgcOptions lgcOptions = coordType == CoordinatesType.SU ? LgcOptions.Oloc : LgcOptions.Rs2k;
                lgcOptions |= LgcOptions.Libr;
                SetDefaultContent(ref content, title, precision, true, lgcOptions);
                content += InputLine.AllOffsetInstruments(moduleLine);
                SetCoordTheo(ref content, moduleLine, coordType);
                EndFileAndSave(ref content);
            }

            /// <summary>
            /// Cree le fichier pour calculer les coordonnées des points mesurés sur le fil
            /// </summary>
            /// <param name="stationLineModule"></param>
            /// <param name="title"></param>
            internal void CreateAsWireCoordinatesCalculation(Line.Station.Module stationLineModule, string fileName, string title = "")
            {
                int precision = 7;
                string content = "";
                CoordinatesType coordType = stationLineModule.WorkingAverageStationLine._Parameters._CoordType;
                Line.Station stationLine = stationLineModule.WorkingAverageStationLine._ListStationLine[0];

                SetFileName(fileName, offsetFolder, title);
                SetDefaultContent(ref content, title, precision, true, coordType == CoordinatesType.CCS ? LgcOptions.Rs2k : LgcOptions.Oloc);
                //SetCoordTypeOption(ref content, coordType);
                content += InputLine.OffsetInstrument(stationLine);
                SetCoordTheo(ref content, stationLine, coordType);
                EndFileAndSave(ref content);
            }

            internal void CreateAsSingleLevelMeasurement(Level.Station.Module stationLevelModule, string fileName, string title = "")
            //crée fichier input pour mesure fil
            {
                string content = "";
                int precision = 7;

                SetFileName(fileName, levelFolder, title);
                LgcOptions lgcOptions = LgcOptions.Oloc;
                if (!InputLine.HasCala(stationLevelModule))
                    lgcOptions |= LgcOptions.Libr;
                SetDefaultContent(ref content, title, precision, false, lgcOptions);
                SetSingleLevelMeasurement(ref content, stationLevelModule);
                EndFileAndSave(ref content);
            }

            internal void CreateAsAllLevelMeasurement(Level.Module moduleLevel, string fileName, string title = "")
            {
                string content = "";
                int precision = 7;

                SetFileName(fileName, levelFolder, title);
                LgcOptions lgcOptions = LgcOptions.Oloc;
                if (!InputLine.HasCala(moduleLevel))
                    lgcOptions |= LgcOptions.Libr;
                SetDefaultContent(ref content, title, precision, false, lgcOptions);
                SetAllLevelMeasurement(ref content, moduleLevel);
                EndFileAndSave(ref content);
            }

            /// <summary>
            /// Creation fichier LGC2 pour module guidé cheminement
            /// </summary>
            /// <param name="groupGuidedModule"></param>
            /// <param name="title"></param>
            internal void CreateAsCheminement(Guided.Group.Module groupGuidedModule, string fileName, string title = "")
            {
                lgc2.TreatFileName(fileName, levelFolder);
                Level.Module levelModule = new Level.Module();
                foreach (Guided.Module gm in groupGuidedModule.SubGuidedModules)
                {
                    if (gm.guideModuleType == GuidedModuleType.Cheminement)
                    {
                        levelModule.StationModules.Add(gm._ActiveStationModule);
                    }
                }
                CreateAsAllLevelMeasurement(levelModule, "", title);
            }

            /// <summary>
            /// Creation fichier LGC2 pour module guidé alignement levelling check
            /// </summary>
            /// <param name="groupGuidedModule"></param>
            /// <param name="title"></param>
            internal void CreateAsAlignmentLevellingCheck(Guided.Group.Module groupGuidedModule, string title)
            {
                Level.Module levelModule = new Level.Module();
                foreach (Guided.Module gm in groupGuidedModule.SubGuidedModules)
                {
                    if (gm.guideModuleType == GuidedModuleType.AlignmentLevellingCheck
                        || gm.guideModuleType == GuidedModuleType.AlignmentLevelling)
                    {
                        levelModule.StationModules.Add(gm._ActiveStationModule);
                    }
                }
                CreateAsAllLevelMeasurement(levelModule, title);
            }
            internal void CreateAsLibrWire(Guided.Group.Module guidedGroupModule, string fileName, string title = "")
            {
                lgc2.TreatFileName(fileName, offsetFolder);
                Line.Module lineModule = new Line.Module();
                foreach (Guided.Module gm in guidedGroupModule.SubGuidedModules)
                {
                    if (gm.guideModuleType == GuidedModuleType.Ecartometry)
                    {
                        lineModule.StationModules.Add(gm._ActiveStationModule);
                        lineModule._ActiveStationModule = gm._ActiveStationModule;
                    }
                }
                CreateAsLibrAllWiresMeasurement(lineModule, fileName, title);
            }

            #endregion

            #region InputLine

            public static class InputLine
            {
                private static readonly string space = " ";
                private static readonly string tab = "   ";
                private static string DefaultTarget = "";
                private static readonly CultureInfo cultureInfo = new CultureInfo("en-US");
                private static readonly string format22 = "{0,-22}";
                private static readonly string format10 = "{0,10:F6}";
                private static readonly string nameField = "   %Name                           X          Y        Z/H \r\n";
                private static readonly string idField = "   %[id]                         [m]        [m]        [m] \r\n";
                private static readonly int precDigit6 = 6;
                private static readonly int precDigit5 = 5;
                private static readonly CultureInfo specCultureInfo = CultureInfo.CreateSpecificCulture("en-US");
                private static readonly string formatF6 = "F6";
                private static readonly string formatF5 = "F5";


                public static string Titr(string text)
                {
                    string s = Option.TITR + newLine;
                    s += text + newLine;
                    return s;
                }
                public static string OptionOloc()
                {
                    return Option.OLOC;
                }
                public static string OptionRS2K()
                {
                    return Option.R2SK;
                }
                public static string OptionLep()
                {
                    return Option.LEP;
                }
                public static string OptionSphere()
                {
                    return Option.SPHE;
                }
                public static string OptionCreatePunchFile()
                {
                    return Option.PUNC + newLine;
                }

                public static string OptionCreatePunchHFile()
                {
                    return Option.PUNCH + newLine;
                }

                public static string OptionFaut()
                {
                    return "*FAUT     .01     .10 " + newLine;
                }

                public static string OptionConsiLibr()
                {
                    return "*CONSI LIBR" + newLine;
                }

                internal static string OptionCreatePunchEFile()
                //ajoute création punch file
                {
                    return Option.PUNCE + newLine;
                }
                internal static string OptionHist()
                //ajoute création Hist file
                {
                    return Option.HIST + newLine;
                }

                internal static string Comment(string text)
                //ajoute création Hist file
                {
                    return $"%{text}";
                }

                public static string Instr()
                //ajoute ligne *INSTR
                {
                    return "*INSTR \r\n";
                }
                public static string End()
                {
                    return "*END \r\n";
                }

                internal static List<Point> PointsWithCoordinates = new List<Point>();
                public static string AddVariablePoints(
                    List<Point> variablePoints,
                    Strategies.GetTheRightCoordinates GetTheRightCoordinates,
                    ref List<Point> pointAlreadyPresent,
                    string keyWord,
                    List<Point> pointToBeCommented,
                    Coordinates barycenter)
                {
                    try
                    {
                        if (PointsWithCoordinates == null) PointsWithCoordinates = new List<Point>();

                        string text = "";
                        if (variablePoints.Count > 0)
                        {
                            text = $"{keyWord} {newLine}";
                            text += nameField;
                            text += idField;

                            foreach (Point point in variablePoints)
                            {
                                string pName = point._Name;
                                if (!Analysis.Element.ContainAPointIn(pName, pointAlreadyPresent))
                                {
                                    Coordinates coord = GetTheRightCoordinates(point);
                                    string line = "";
                                    pointAlreadyPresent.Add(point);

                                    if (pointToBeCommented != null && Analysis.Element.ContainAPointIn(pName, pointToBeCommented))
                                        line += "% ";

                                    line += tab + string.Format("{0,-22}", pName.ToUpper()) + space;
                                    if (coord == null)
                                        coord = point._Coordinates.Local;

                                    bool needToReplaceCoordinates = (coord == null || !coord.AreKnown);
                                    bool baryCenterIsAvailable = barycenter != null && barycenter.AreKnown;
                                    bool baryUsed = needToReplaceCoordinates && baryCenterIsAvailable;
                                    bool lhcCenterIsUsed = needToReplaceCoordinates && !baryCenterIsAvailable;

                                    string substitutionMessage = "";
                                    if (baryUsed)
                                    {
                                        double r = (double)Tsunami2.Properties.Rnd.Next(1, 1000) / 10000;
                                        coord = barycenter + r; // add a random 1 to 1000 mm just in case LGC complain about equal coordinates.
                                        substitutionMessage = "\t % Cala barycenter coord. + random cm used.";
                                    }
                                    else if (lhcCenterIsUsed)
                                    {
                                        coord = GetAPointCloseToTheLhcCenter(ref substitutionMessage);
                                    }

                                    line += $"{string.Format(cultureInfo, format10, coord.X.Value)}{space}";
                                    line += $"{string.Format(cultureInfo, format10, coord.Y.Value)}{space}";
                                    line += $"{string.Format(cultureInfo, format10, coord.Z.Value)}{space}";

                                    line += substitutionMessage;

                                    PointsWithCoordinates.Add(point);
                                    text += $"{line}{newLine}";
                                }
                            }
                        }
                        return text;
                    }
                    catch (Exception ex)
                    {

                        throw new Exception($"{R.T_IMPOSSIBLE_TO_ADD_VARIABLE_POINTS} {newLine} {ex.Message}", ex);
                    }
                }



                public static string AddPdorPoints(List<Point> pList, Strategies.GetTheRightCoordinates GetTheRightCoordinates)
                {
                    try
                    {
                        List<string> namesAlreadyPresent = new List<string>();
                        string text = $"{Option.PDOR} {newLine}{nameField}{idField}";

                        foreach (Point point in pList)
                        {
                            string pName = point._Name.ToUpper();
                            if (!namesAlreadyPresent.Contains(pName))
                            {
                                namesAlreadyPresent.Add(pName);
                                text += $"{tab}{string.Format(format22, pName)}{space}{newLine}";
                            }
                        }
                        return text;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"{R.T_IMPOSSIBLE_TO_ADD_CALA_POINTS} {newLine} {ex.Message}", ex);
                    }
                }

                public static string AddCalaPoints(List<Point> pList, Strategies.GetTheRightCoordinates GetTheRightCoordinates, out Coordinates barycenter, CloneableList<M.Measure> measures)
                {
                    try
                    {
                        // let's compute barycenter of the cala point just in case we need to reprace the -999.9 from varialbe point with it
                        Coordinates coordinatesSum = new Coordinates("sum", 0, 0, 0);
                        int sumCount = 0;
                        List<string> namesAlreadyPresent = new List<string>();

                        string text = "";
                        if (pList.Count>0)
                            text += $"{Option.CALA} {newLine}{nameField}{idField}";

                        foreach (Point point in pList)
                        {
                            string pName = point._Name.ToUpper();
                            if (!namesAlreadyPresent.Contains(pName))
                            {
                                string line = "";
                                namesAlreadyPresent.Add(pName);
                                line += $"{tab}{string.Format(format22, pName)}{space}";
                                Coordinates coord = GetTheRightCoordinates(point);
                                if (coord == null)
                                {
                                    coord = point._Coordinates.Local;
                                }

                                line += $"{string.Format(cultureInfo, format10, coord.X.Value)}{space}";
                                line += $"{string.Format(cultureInfo, format10, coord.Y.Value)}{space}";
                                line += $"{string.Format(cultureInfo, format10, coord.Z.Value)}{space}";

                                //line += T.Conversions.StringManipulation.LengthOf13String(coord.X.ToString("m-m")) + space;
                                //line += T.Conversions.StringManipulation.LengthOf13String(coord.Y.ToString("m-m")) + space;
                                //line += T.Conversions.StringManipulation.LengthOf13String(coord.Z.ToString("m-m")) + space;

                                if (!coord.AreKnown)
                                    line = "% " + line + $"% {R.T_COMMENTED_BECAUSE_EQUAL_TO} " + na.ToString();
                                else
                                {
                                    PointsWithCoordinates.Add(point);

                                    // lets sum the coordinates of the measured point to cmpute the barycenter
                                    if (measures == null || measures.Find(x =>
                                    {
                                        if (x == null)
                                            return false;
                                        if (x._Point == null)
                                            return false;
                                        return x._Point._Name.ToUpper() == pName.ToUpper() && x._Status.Type == M.States.Types.Good;
                                    }) != null)
                                    {
                                        coordinatesSum += coord;
                                        if (sumCount == 0)
                                        {
                                            coordinatesSum.CoordinateSystem = coord.CoordinateSystem;
                                            coordinatesSum.ReferenceFrame = coord.ReferenceFrame;
                                            coordinatesSum.SystemName = coord.SystemName;
                                        }
                                        sumCount++;
                                    }
                                }
                                text += $"{line}{newLine}";
                            }
                        }

                        barycenter = coordinatesSum / sumCount;
                        return text;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"{R.T_IMPOSSIBLE_TO_ADD_CALA_POINTS} {newLine} {ex.Message}", ex);
                    }
                }

                internal static string Orie(Polar.Station station, Point ghost, DoubleValue doubleValue)
                {
                    Polar.Station.Parameters sParam = station.Parameters2;
                    string text = $"{Option.ORIE} {space}";
                    text += $"{sParam._StationPoint._Name}{space}";
                    text += ReplaceDashToUnderscore(station.Parameters2._Instrument._Name);
                    text += $"{newLine}{tab}{ghost._Name}{space}{string.Format(cultureInfo, "{0,-6:0.0000}", doubleValue.Value)}{newLine}";
                    return text;
                }

                public static string Polar(I.IPolarInstrument instr, List<Reflector> targets, DoubleValue instHt, DoubleValue targetHt)
                {
                    string b = space;

                    NumberFormatInfo nfi1 = new NumberFormatInfo();
                    nfi1.NumberDecimalDigits = 1;
                    nfi1.NumberDecimalSeparator = ".";
                    nfi1.NumberGroupSeparator = "";

                    NumberFormatInfo nfi2 = new NumberFormatInfo();
                    nfi2.NumberDecimalDigits = 2;
                    nfi2.NumberDecimalSeparator = ".";
                    nfi2.NumberGroupSeparator = "";

                    NumberFormatInfo nfi5 = new NumberFormatInfo();
                    nfi5.NumberDecimalDigits = 5;
                    nfi5.NumberDecimalSeparator = ".";
                    nfi5.NumberGroupSeparator = "";

                    NumberFormatInfo nfi4 = new NumberFormatInfo();
                    nfi4.NumberDecimalDigits = 4;
                    nfi4.NumberDecimalSeparator = ".";
                    nfi4.NumberGroupSeparator = "";

                    string text = "";
                    //    
                    text += "   %Polar Instrument    DefaultTrgt  IHeight  sIH   sICentering  const_angl \r\n";
                    text += "   %Polar [id]          [id]         [m]      [mm]  [mm]         [gon]      \r\n";
                    //   "          AT401_391055  RFI0.5_2884  0.00000  0.00  0.00         0.0000     \r\n";
                    text += $"{tab}{Option.POLAR}{space}";
                    text += $"{string.Format(cultureInfo, "{0,-13}", ReplaceDashToUnderscore(instr._Name + space))}{space}";
                    DefaultTarget = targets[0]._Name.Replace(' ', '_');
                    text += string.Format("{0,-12}", DefaultTarget) + b;

                    text += string.Format(nfi5, "{0,-8:0.00000}", instHt.Value) + b;
                    text += string.Format(nfi2, "{0,-5:0.00}", instHt.Sigma) + b;
                    text += string.Format(nfi4, "{0,-12:0.00}", instr.sigmaInstrCentering) + b;
                    text += string.Format(nfi4, "{0,-10:0.0000}", 0) + "\r\n";

                    bool first = true;
                    foreach (Reflector target in targets)
                    {
                        I.SigmaForAInstrumentCouple sigma = GetSigmaForInstr(instr, target);

                        if (sigma == null)
                            sigma = GetDefaultSigma();

                        if (sigma == null)
                            sigma = new I.SigmaForAInstrumentCouple() { sigmaAnglCc = 7.07, sigmaZenDCc = 7.07, sigmaDistMm = 0.35, ppmDistMm = 5 };

                        if (first)
                        {
                            text += "      %Target       sAngl  sZend  sDist  ppm   CsteVariable DistCorrection sDC   sCentering  Heigth  sH\r\n";
                            text += "      %[id]         [cc]   [cc]   [mm]   [mm]  [1/0]        [m]            [mm]  [mm]        [m]     [mm] \r\n";
                            //   "      RFI0.5_2884   0.0    0.0    0.00   0.00  0            0.00000        0.00  0.00  \r\n";
                            first = false;
                        }
                        text += space + space + string.Format("{0,-13}", target._Name.Replace(' ', '_')) + b;
                        text += string.Format(nfi1, "{0,-6:0.0}", sigma.sigmaAnglCc) + b; //cc
                        text += string.Format(nfi1, "{0,-6:0.0}", sigma.sigmaZenDCc) + b; //cc
                        text += string.Format(nfi2, "{0,-6:0.00}", sigma.sigmaDistMm) + b; //mm
                        text += string.Format(nfi2, "{0,-5:0.00}", sigma.ppmDistMm) + b; //mm
                        int i = target.distCorrectionKnown ? 0 : 1; // 1 pour mettre la constante comme ajustable, 0 si elle est connue
                        text += string.Format(nfi1, "{0,-12:0}", i) + b;
                        text += string.Format(nfi5, "{0,-14:0.00000}", target.distCorrectionValue) + b; //m
                        text += string.Format(nfi2, "{0,-5:0.00}", target.sigmaDCorr) + b; //mm
                        text += string.Format(nfi2, "{0,-7:0.00}", sigma.rCenteringMm) + b; //mm
                        text += string.Format(nfi5, "{0,-11:0.00000}", targetHt.Value) + b; //m
                        text += string.Format(nfi2, "{0,-8:0.00}", targetHt.Sigma) + "\r\n"; //mm
                    }

                    return text;
                }

                private static I.SigmaForAInstrumentCouple GetDefaultSigma()
                {
                    bool match(I.SigmaForAInstrumentCouple x)
                    {
                        return x.Instrument.ToUpper() == "Default"
                            && x.Target.ToUpper() == "Default";
                    }

                    return P.Preferences.Instance.InstrumentSlashTargetCoupleSigmas.Find(match);
                }

                private static I.SigmaForAInstrumentCouple GetSigmaForInstr(I.IPolarInstrument instr, Reflector target)
                {
                    bool match(I.SigmaForAInstrumentCouple x)
                    {
                        return x.Instrument.ToUpper() == instr._Model.ToUpper()
                            && x.Target.ToUpper() == target._InstrumentType.ToString().ToUpper();
                    }

                    return P.Preferences.Instance.InstrumentSlashTargetCoupleSigmas.Find(match);
                }

                public static string OffsetInstrument(Line.Station line)
                //ajoute un offsetmeter comme instrument
                {
                    string text = $"{tab}{Option.SCALE}{space}";
                    text += ReplaceDashToUnderscore($"{line._Parameters._Instrument._Name}{space}"); // nom regle decalage
                    SetOffsetInstrumentProperties(ref text);
                    return text;
                }
                public static string AllOffsetInstruments(Line.Module lineModule)
                {
                    List<I.Instrument> instrumAlreadyUsed = new List<I.Instrument>();
                    string text = "";
                    foreach (Line.Station.Module item in lineModule.StationModules.OfType<Line.Station.Module>())
                    {
                        if (item.WorkingAverageStationLine._ListStationLine.Count > 0)
                        {
                            string instrName = item.WorkingAverageStationLine._Parameters._Instrument._Name;
                            if (instrumAlreadyUsed.FindIndex(x => x._Name == instrName) == -1)
                            {
                                instrumAlreadyUsed.Add(item.WorkingAverageStationLine._Parameters._Instrument);
                                text += $"{tab}{Option.SCALE}{space}";
                                text += ReplaceDashToUnderscore($"{instrName}{space}"); // nom regle decalage
                                SetOffsetInstrumentProperties(ref text);
                            }
                        }
                    }
                    return text;
                }

                public static string ReplaceDashToUnderscore(string text)
                {
                    return text.Replace('-', '_');
                }
                public static void SetOffsetInstrumentProperties(ref string s)
                {
                    var prec = Tsunami2.Preferences.Values.InstrumentSlashTargetCoupleSigmas.FirstOrDefault(x => x.Instrument == "ECARTOMETRE");
                    s += $"{prec.sigmaDistMm} {space}"; //Standard deviation of a measured spatial distance [mm]
                    s += $"0 {space}"; //PPM error value for a measured spatial distance [mm]
                    s += $"0 {space}"; //Distance correction value for the measurements [m]
                    s += $"0 {space}"; //Standard deviation of the distance correction value [mm]
                    s += $"0 {newLine}"; //Standard deviation of the scale instrument centering [mm]

                }

                /// <summary>
                /// Defines in which call of the vertical mode we are
                /// </summary>
                public enum VerticalMode
                {
                    Default,
                    Setup,
                    Goto,
                    PointLancé
                }

                public static string Tstn(
                    Polar.Station station,
                    bool instrumentHeightKnown = false,
                    bool UseOnlyLastMeasure = false,
                    bool useObservationSigma = false,
                    bool useDistance = true,
                    Lgc2 lgc2 = null,
                    List<Point> vxy = null,
                    bool CommentBadStations = false,
                    VerticalMode vm = VerticalMode.Default,
                    Frame frame = null,
                    List<Point> restrictedMeasures = null)
                {
                    try
                    {
                        Polar.Station.Parameters p = station.Parameters2;
                        bool verticalized = station.Parameters2.Setups.InitialValues.VerticalisationState != TSU.Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation.NotVerticalised;

                        // comment everything or not
                        string prefixFromStation = "";
                        string suffixFromStation = "";

                        // check if verticalized
                        if (!verticalized)
                        {
                            //prefixFromStation = "%";
                            if (vm == VerticalMode.Default)
                                suffixFromStation = "  % Station commented because not verticalized, need t be treated with care.";
                        }
                        else
                        {
                            // check if bad
                            if (CommentBadStations)
                                if (p._State is Station.State.Bad)
                                {
                                    prefixFromStation = "%";
                                    suffixFromStation = "  % Station is Bad according to Tsunami";
                                }
                        }

                        // treat
                        string stationPoint;
                        if (p._StationPoint == null)
                            stationPoint = "not_defined";
                        else
                            stationPoint = p._StationPoint._Name.ToUpper();

                        string s = "";

                        if (!verticalized && vm != VerticalMode.Default)
                        {
                            string comment;
                            if (vm == VerticalMode.Setup)
                                comment = " % provisional XYZ = barycenter of the measured references";
                            else
                                comment = string.Empty;
                            s += $"{prefixFromStation}{frame.LGCGetFrameBeginning()}{comment}{newLine}";
                            s += $"{prefixFromStation}{Option.CALA}{newLine}";
                            s += $"{prefixFromStation}{stationPoint}{space}0.00000{space}0.00000{space}0.00000{newLine}";
                        }

                        s += $"{prefixFromStation}{Option.TSTN}{space}";
                        s += $"{stationPoint}{space}";
                        s += $"{ReplaceDashToUnderscore(station.Parameters2._Instrument._Name)}{space}";
                        if (!verticalized && vm == VerticalMode.Default)
                        {
                            s += "ROT3D";
                        }
                        else
                        {
                            if (instrumentHeightKnown)
                                s += string.Format("IHFIX  IH {0} IHSE {1}", p._InstrumentHeight.Value, p._InstrumentHeight.Sigma);
                            s += $"     % {station.ParametersBasic._Date}";
                            s += suffixFromStation;
                        }
                        s += $"{newLine}{prefixFromStation}{tab}*V0{newLine}";

                        List<M.Measure> goodMeasures;
                        goodMeasures = new List<M.Measure>();


                        // Select measures
                        if (UseOnlyLastMeasure)
                        {
                            if (station.NextMeasure != null)
                                goodMeasures.Add(station.NextMeasure);
                            else
                                goodMeasures.Add(station.MeasuresTaken[station.MeasuresTaken.Count - 1] as Polar.Measure);
                        }
                        else
                        {
                            goodMeasures = station.MeasuresToExport;
                            if (goodMeasures.Count == 0)
                            {
                                goodMeasures.AddRange(station.MeasuresTaken);
                                if (station.NextMeasure != null)
                                    goodMeasures.Add(station.NextMeasure);
                            }
                        }

                        int longestName = 30;
                        List<Point> allPointsInitialiazedInTheInput = null;
                        if (lgc2 != null)
                        {
                            allPointsInitialiazedInTheInput = lgc2._Input.ALLpoints;
                            longestName = Analysis.Element.GetLongerName(allPointsInitialiazedInTheInput);
                        }

                        if (!verticalized && vm == VerticalMode.Default)
                        {
                            s += $"{prefixFromStation} {tab} {tab} *PLR3D {newLine}";
                            // *PLR3D
                            // REF17    98.7751    104.3312    5.69222
                            foreach (Polar.Measure m in goodMeasures.OfType<Polar.Measure>())
                            {
                                Polar.Measure mf1 = Survey.GetObservationInFace1Type(m);

                                string prefixGeneral = " ";
                                if (mf1._Status is M.States.Bad)
                                {
                                    prefixGeneral = "%";
                                }

                                s += $"{prefixFromStation}{prefixGeneral}{tab}{tab}{tab}{S.AddCharsAfter(m._Point._Name.ToUpper(), ' ', longestName)}{space}";
                                s += mf1.Angles.Corrected.Horizontal.Value.ToString("F6") + space;
                                s += mf1.Angles.Corrected.Vertical.Value.ToString("F6") + space;
                                s += mf1.Distance.Corrected.Value.ToString("F6") + "\r\n";
                            }
                        }
                        else
                        {

                            // Write measures
                            string angl = $"{prefixFromStation} {tab} {tab} *ANGL {newLine}";
                            string zenD = $"{prefixFromStation} {tab} {tab} *ZEND {newLine}";
                            string dist = $"{prefixFromStation} {tab} {tab} *DIST {newLine}";

                            AddCommentedHeaders(ref angl, ref zenD, ref dist, prefixFromStation, longestName, space, tab);

                            angl += "%insert_here\r\n";

                            string previousTarget = "";
                            int countOfGoodMeasures = goodMeasures.Count;
                            foreach (Polar.Measure m in goodMeasures.OfType<Polar.Measure>())
                            {
                                if (restrictedMeasures != null)
                                {
                                    if (!restrictedMeasures.Any(x => x._Name == m._Point._Name))
                                        continue; // we skip the measure if it is not about a point from the restericted list
                                }
                                Globals.MeasureIdNumberCount++;
                                AddPolarMeasure(m, station,
                                    lgc2, allPointsInitialiazedInTheInput, ref previousTarget,
                                    prefixFromStation, useObservationSigma, useDistance, vxy, longestName,
                                    ref angl, ref zenD, ref dist, ref countOfGoodMeasures,
                                    space, tab, Globals.MeasureIdNumberCount);

                            }
                            s += $"{angl} {zenD} {dist}";
                        }

                        if (!verticalized && vm != VerticalMode.Default)
                            s += $"{prefixFromStation}{frame.LGCGetFrameEnd()}{newLine}";

                        goodMeasures.Clear();
                        return s;
                    }
                    catch (Exception ex)
                    {

                        throw new Exception($"{R.T_PROBLEM_WRITING_THE_OBSERVATION_PART_OF_THE_LGC2_INPUT}.{newLine}{ex.Message}", ex);
                    }
                }

                private static void AddCommentedHeaders(ref string angl, ref string zenD, ref string dist, string prefixFromStation, int longestName, string space, string tab)
                {
                    string comm = "Comments";
                    string obs = "  Obs";
                    string gon = "[gon]";
                    string name = "%Name";
                    string id = "%[id]";
                    string targ = "Target";
                    string targH = "Target H";
                    string sig = "sigmaA";
                    string cc = "  [CC]";
                    string dash = "--------";
                    string idSpace = "  [id]";
                    string lSpace = "        ";
                    string lM = "     [m]";

                    angl += $"{prefixFromStation} {tab}{tab}{tab}{S.AddCharsAfter(name, ' ', longestName)}{space}{obs,9}{space}{lSpace,10}" +
                            $"{space}{targ,15}{space}{sig,10}{space}{tab}{comm}{newLine}";
                    angl += $"{prefixFromStation} {tab}{tab}{tab}{S.AddCharsAfter(id, ' ', longestName)}{space}{gon,9}{space}{lSpace,10}" +
                            $"{space}{idSpace,15}{space}{cc,10}{space}{tab}{dash}{newLine}";
                    zenD += $"{prefixFromStation} {tab}{tab}{tab}{S.AddCharsAfter(name, ' ', longestName)}{space}{obs,9}{space}{targH,10}" +
                            $"{space}{targ,15}{space}{sig,10}{space}{tab}{comm}{newLine}";
                    zenD += $"{prefixFromStation} {tab}{tab}{tab}{S.AddCharsAfter(id, ' ', longestName)}{space}{gon,9}{space}{lM,10}" +
                            $"{space}{idSpace,15}{space}{cc,10}{space}{tab}{dash}{newLine}";
                    dist += $"{prefixFromStation} {tab}{tab}{tab}{S.AddCharsAfter(name, ' ', longestName)}{space}{obs,9}{space}{targH,10}" +
                            $"{space}{targ,15}{space}{sig,10}{space}{tab}{comm}{newLine}";
                    dist += $"{prefixFromStation} {tab}{tab}{tab}{S.AddCharsAfter(id, ' ', longestName)}{space}{"  [m]",9}{space}{lM,10}" +
                            $"{space}{idSpace,15}{space}{cc,10}{space}{tab}{dash}{newLine}";
                }

                public static void AddPolarMeasure(
                    Polar.Measure m,
                    Polar.Station station,
                    Lgc2 lgc2,
                    List<Point> allPointsInitialiazedInTheInput,
                    ref string previousTarget,
                    string prefixFromStation,
                    bool useObservationSigma,
                    bool useDistance,
                    List<Point> vxy,
                    int longestName,
                    ref string angl,
                    ref string zenD,
                    ref string dist,
                    ref int countOfGoodMeasures,
                    string space,
                    string tab,
                    int measureNumber)
                {
                    m.IdForLgc = measureNumber;
                    countOfGoodMeasures--;
                    // check if the point is known, if not doesnt worth to put the measure lgc will fail
                    bool CoordinateAvailable = Point.GetExisitingByPointName(PointsWithCoordinates, m._Point._Name).Count > 0;

                    var a = Tsunami2.Properties;
                    if (!CoordinateAvailable)
                        CoordinateAvailable = false;
                    // Have to switch faces2 to face 1 because PLGC handle it badly

                    Polar.Measure mf1 = Survey.GetObservationInFace1Type(m);

                    string prefixGeneral = GetPrefix(mf1, countOfGoodMeasures, lgc2);
                    string suffixGeneral = "";

                    IO.Comments.CompleteCommentFromTsunami(m, station.ClosureMeasure);

                    if (m.Comment != R.T_NO_COMMENT)
                        suffixGeneral += tab + "% " + m.Comment;

                    if (!(mf1._Status is M.States.Good))
                        suffixGeneral += tab + "% Was '" + mf1._Status._Name + "' according to Tsunami";
                    else if (!CoordinateAvailable)
                        suffixGeneral += tab + "% No coord. available";

                    if (m.GeodeRole == Geode.Roles.Unused)
                        suffixGeneral += tab + "% Not used in compensation";

                    // Check if point initilaized
                    if (allPointsInitialiazedInTheInput != null)
                    {
                        if (!Analysis.Element.ContainAPointIn(mf1._Point._Name, allPointsInitialiazedInTheInput))
                        {
                            prefixGeneral = "%";
                            suffixGeneral += " % point not initialized";
                        }
                    }
                    NumberFormatInfo nfi1 = new NumberFormatInfo() { CurrencyDecimalSeparator = ".", NumberDecimalDigits = 1 };
                    NumberFormatInfo nfi3 = new NumberFormatInfo() { CurrencyDecimalSeparator = ".", NumberDecimalDigits = 3 };


                    string prefix, suffix;

                    string target;
                    if (mf1.Distance.Reflector != null) // this is for the case of the goto compute when the measure dont have a fixed reflector yet but we dont core ecause we want the angles
                    {
                        if (mf1.Distance.Reflector._Name == R.T_MANAGED_BY_AT40X)
                            target = "CCR1.5_1";
                        else
                            target = mf1.Distance.Reflector._Name.Replace(' ', '_');
                        previousTarget = target;
                    }
                    else
                        target = previousTarget;

                    bool lgc2InputStillSucks = true;

                    // *ANGL
                    try
                    {
                        prefix = prefixGeneral;
                        suffix = suffixGeneral;
                        if (mf1.Angles.Corrected.Horizontal.Value != na)
                        {
                            if (mf1.Angles.HorizontalState == M.States.Types.Bad)
                            {
                                prefix = "%";
                                suffix += "(Horizontal angle disabled by user)";
                            }

                            angl += prefixFromStation + prefix + tab + tab + tab + S.AddCharsAfter(m._Point._Name.ToUpper(), ' ', longestName) + space;
                            angl += mf1.Angles.Corrected.Horizontal.Value.ToString("F6") + space;

                            angl += $"{" ",11}";

                            if (lgc2InputStillSucks || target != DefaultTarget) // let s override the needed values from the reflector
                                angl += string.Format("TRGT {0,10}", target) + space;
                            else
                                angl += $"{" ",15}{space}";


                            // OBSE
                            try
                            {
                                bool addOBSE = useObservationSigma;
                                double obse = 0 - 1;
                                if (mf1.Angles.Corrected.Horizontal.Sigma == 200.0000)
                                {
                                    addOBSE = true;
                                    obse = 2000000.00;
                                }
                                else
                                {
                                    // double obse = mf1.Angles.Corrected.Horizontal.Sigma; // this is bad because it is comming from instrument ona  single measure, it is more lke reproductibility than precision

                                    if (mf1.APrioriSigmas != null)
                                        obse = mf1.APrioriSigmas.sigmaAnglCc;
                                    // check if 1face to modify the sigma
                                    if (mf1.Face == I.FaceType.Face1 && obse != na && obse != -1)
                                    {
                                        addOBSE = true;
                                        // si pas 200 pour point lancé
                                        obse = obse * Math.Sqrt(2);
                                    }
                                }
                                // skipped for the moment by default, the sigma gven by the instrument concert only the sample of point it measured, and dont take into account the encoders error etc... can be mixed with the isntrument specifications in the futur
                                if (addOBSE)
                                {
                                    // !! OBSE cannot be 0.0
                                    double trucateObseInCC = Math.Round(obse * 10) / 10;
                                    if (trucateObseInCC == 0.0)
                                        trucateObseInCC += 0.1;
                                    angl += string.Format(nfi1, "OBSE {0,5:F1}", trucateObseInCC) + space;
                                }
                                else
                                    angl += $"{" ",10}{space}";
                            }
                            catch (Exception ex)
                            {
                                throw new Exception($"{R.ProblemOBSE} {mf1._Point._Name}.{newLine}", ex);
                            }

                            // Use IDs to link output and output
                            if (m.IdForLgc != 0)
                                angl += $" ID {m.IdForLgc}.H";

                            angl += suffix + newLine;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"{R.ProblemANGL} {mf1._Point._Name}.{newLine}", ex);
                    }

                    // *ZEND
                    try
                    {
                        prefix = prefixGeneral;
                        suffix = suffixGeneral;


                        /// not true if you do that the point is calculated at the original position and not on the ground for exemple in case of LHC.
                        /// For ZEND and DIST, we have to compute the right extension to apply.
                        /// There is 2 extensions, m.extension which is the physical extension you use on field, this is used to says to lgc  to compute the coordinate lower than the measured point
                        /// and then m.wantedOffset which is used to express theat the point we want to stake ut is lower or higer than the theo one.
                        ///double mixteExtensionValue = mf1.Extension.Value + mf1.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates.Value;


                        if (mf1.Angles.Corrected.Vertical.Value != na)
                        {
                            if (mf1.Angles.VerticalState == M.States.Types.Bad)
                            {
                                prefix = "%";
                                suffix += "(Vertical angle disabled by user)";
                            }

                            zenD += prefixFromStation + prefix + tab + tab + tab + S.AddCharsAfter(m._Point._Name.ToUpper(), ' ', longestName) + space;
                            zenD += mf1.Angles.Corrected.Vertical.Value.ToString("F6") + space;


                            zenD += "TH " + mf1.Extension.ToString("F6") + space;

                            if (lgc2InputStillSucks || target != DefaultTarget) // let s override the needed values from the reflector
                            {
                                zenD += string.Format("TRGT {0,10}", target) + space;
                            }
                            else
                                zenD += $"{" ",15}{space}";

                            // OBSE
                            try
                            {
                                bool addOBSE = useObservationSigma;
                                double obse = 0;
                                if (mf1.Angles.Corrected.Vertical.Sigma == 200.0000)
                                {
                                    addOBSE = true;
                                    obse = 2000000.00;
                                }
                                else
                                {
                                    // double obse = mf1.Angles.Corrected.Vertical.Sigma;// this is bad because it is comming from instrument ona  single measure, it is more lke reproductibility than precision

                                    if (mf1.APrioriSigmas != null)
                                        obse = mf1.APrioriSigmas.sigmaZenDCc;
                                    // check if 1face to modify the sigma
                                    if (mf1.Face == I.FaceType.Face1 && obse != na && obse != 0)
                                    {
                                        addOBSE = true;
                                        obse *= Math.Sqrt(2);
                                    }
                                }

                                if (addOBSE)
                                {
                                    double trucateObseInCC = Math.Round(obse * 10) / 10;
                                    if (trucateObseInCC == 0.0)
                                        trucateObseInCC += 0.1;
                                    zenD += string.Format(nfi1, "OBSE {0,5:F1}", trucateObseInCC) + space;
                                }

                                else
                                    zenD += $"{" ",10}{space}";
                            }
                            catch (Exception ex)
                            {
                                throw new Exception($"{R.ProblemOBSE} {mf1._Point._Name}.{newLine}", ex);
                            }

                            // Use IDs to link output and output
                            if (m.IdForLgc != 0)
                                zenD += $" ID {m.IdForLgc}.V";

                            zenD += $"{suffix}{newLine}";
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"{R.ProblemZEND}{mf1._Point._Name}.{newLine}", ex);
                    }

                    //*DIST
                    try
                    {
                        prefix = prefixGeneral;
                        suffix = suffixGeneral;
                        if (mf1.Distance.Corrected.Value != na)
                        {
                            if (mf1.Distance._Status is M.States.Bad)
                            {
                                prefix = "%";
                                suffix += "(Distance disabled by user)";
                            }

                            dist += prefixFromStation + prefix + tab + tab + tab + S.AddCharsAfter(m._Point._Name.ToUpper(), ' ', longestName) + space;
                            dist += mf1.Distance.Corrected.Value.ToString("F6") + space;

                            dist += "TH " + mf1.Extension.Value.ToString("F6") + space;

                            if (lgc2InputStillSucks || target != DefaultTarget) // let s override the needed values from the reflector
                            {
                                dist += string.Format("TRGT {0,10}", target) + space;
                            }
                            else
                                dist += $"{" ",15}{space}";

                            try
                            {
                                // OBSE
                                {
                                    bool addOBSE = useObservationSigma;
                                    //double obse = mf1.Distance.Corrected.Sigma;// this is bad because it is comming from instrument ona  single measure, it is more lke reproductibility than precision
                                    double obse = 0;

                                    if (mf1.Distance.Corrected.Sigma == 1000.000)
                                    {
                                        addOBSE = true;
                                        obse = 1000.000;
                                    }
                                    else
                                    {
                                        if (mf1.APrioriSigmas != null)
                                            obse = mf1.APrioriSigmas.sigmaDistMm;
                                        // check if 1face to modify the sigma
                                        if (mf1.Face == I.FaceType.Face1 && obse != na && obse != 0)
                                        {
                                            addOBSE = true;
                                            obse *= Math.Sqrt(2);
                                        }
                                    }

                                    if (useDistance && addOBSE)
                                    {
                                        double trucateObseInMm = Math.Round(obse * 1000) / 1000;
                                        if (trucateObseInMm == 0)
                                            trucateObseInMm += 0.001;
                                        dist += string.Format(nfi3, "OBSE {0,5:F3}", trucateObseInMm) + space;
                                    }
                                    else
                                        dist += $"{" ",10}{space}";
                                }

                                if (!useDistance)
                                {
                                    string obse = string.Format(nfi3, "OBSE {0,5:F3}", 1000) + space;
                                    if (vxy != null)
                                    {
                                        if (Analysis.Element.FindFirstPointByName(vxy, mf1._Point._Name) == null) // except if radi is used for this point
                                            if (!mf1._OriginalPoint._Coordinates.AreApproximate)
                                                dist += obse;
                                    }
                                    else
                                    {
                                        if (!mf1._Point._Coordinates.AreApproximate)
                                            dist += obse;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {

                                throw new Exception($"{R.ProblemOBSE} {mf1._Point._Name}.{newLine}", ex);
                            }

                            // Use IDs to link output and output
                            if (m.IdForLgc != 0)
                                dist += $" ID {m.IdForLgc}.D";

                            dist += $"{suffix} {newLine}";
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"{R.ProblemZEND} {mf1._Point._Name}.{newLine}", ex);
                    }
                }

                private static string GetPrefix(Polar.Measure m, int countOfGoodMeasures, Lgc2 lgc2)
                {
                    string p;

                    // look for control to not cancel the last control
                    if (m._Status is M.States.Control
                     && countOfGoodMeasures == 0
                     && lgc2 != null
                     && lgc2.useControlMeasures)
                        m.GeodeRole = Geode.Roles.Cala;

                    switch (m.GeodeRole)
                    {
                        case Geode.Roles.Unknown:
                        case Geode.Roles.Cala:
                        case Geode.Roles.Radi:
                        case Geode.Roles.Pdor:
                            p = " ";
                            break;
                        case Geode.Roles.Poin:
                            if (m._Status is M.States.Bad)
                                p = "%";
                            else
                                p = " ";
                            break;
                        case Geode.Roles.Unused:
                        default:
                            p = "%";
                            break;
                    }


                    return p;
                }
                //private static object GetPointWithOriginPointName(List<Point> pointsWithCoordinates, string name)
                //{
                //    throw new NotImplementedException();
                //}

                //private static List<Polar.Measure> SelectMeasuresOfState(Type type, CloneableList<M.Measure> measures)
                //{
                //    List<Polar.Measure> l = new List<Polar.Measure>();
                //    foreach (Polar.Measure m in measures)
                //    {
                //        if (m._Status.GetType() == type)
                //            l.Add(m);
                //    }

                //    return l;
                //}

                internal static string EDMTheo()
                //cree un EDM virtuel pour les distances suivant coordonnées theo pour les fils
                {
                    string zero = "0";
                    string text = $"{tab}*EDM{space}";
                    text += $"DISTTHEO{space}"; //faux EDM pour les distances theo
                    text += $"TARGETTHEO{space}"; //fausse target pour les distances theo
                    text += $"{zero}{space}";//Management.Instrument height [m]
                    text += $"{zero}{space}";//Standard deviation of the instrument height [mm]
                    text += $"{zero}{space}{newLine}";//Standard deviation of the instrument centering [mm]
                    text += $"{tab}{tab}TARGETTHEO{space}";//definition fausse target pour les distances theo
                    text += $"1{space}";//Standard deviation of a measured spatial distance [mm]
                    text += $"{zero}{space}";//PPM error value for a measured spatial distance [mm]
                    text += $"{zero}{space}";//Identifies if the distance correction value is an "unknown value" to be determined in the adjustment
                    text += $"{zero}{space}";//Distance correction value for the measurements [m]
                    text += $"{zero}{space}";//Standard deviation of the distance correction value [mm]
                    text += $"{zero}{space}";//Standard deviation of the target centering [mm]
                    text += $"{zero}{space}";//Target height [m]
                    text += $"{zero}{space}{newLine}";//Standard deviation of the target height [mm]
                    return text;
                }
                internal static string AddCalaPointsOffsets(Line.Station line)
                //Ajoute des points en cala de la liste FixPoint de la ligne ecarto avec le XYZ MLA
                {

                    List<string> namesAlreadyPresent = new List<string>();
                    string text = $"{Option.CALA}{newLine}";

                    foreach (MeasureOfOffset offset in line._MeasureOffsets)
                    {
                        string pName = offset._Point._Name;
                        if (!namesAlreadyPresent.Contains(pName))
                        {
                            namesAlreadyPresent.Add(pName);
                            if (offset._Point.LGCFixOption == LgcPointOption.CALA && offset._CorrectedReading != na)
                            {
                                AddPointNameToText(ref text, pName, offset._Status, space);
                                AddPointToText(ref text, offset._Point._Coordinates.TempCalculation, space);
                                text += newLine; //retour ligne
                            }
                        }
                    }
                    return text;
                }
                /// <summary>
                /// Ajoute les points cala avec les coordonnées CCS théoriques
                /// </summary>
                /// <param name="stationLine"></param>
                /// <returns></returns>
                internal static string AddCalaPointsCCS(Line.Station stationLine)
                {
                    List<string> namesAlreadyPresent = new List<string>();
                    string text = $"{Option.CALA} {newLine}";

                    foreach (MeasureOfOffset offset in stationLine._MeasureOffsets)
                    {
                        string pName = offset._Point._Name;
                        if (!namesAlreadyPresent.Contains(pName))
                        {
                            namesAlreadyPresent.Add(pName);
                            if (offset._Point.LGCFixOption == LgcPointOption.CALA && offset._RawReading != na)
                            {
                                AddPointNameToText(ref text, pName, offset._Status, space);

                                if (offset._Point._Coordinates.HasCcs)
                                {
                                    AddPointToText(ref text, offset._Point._Coordinates.Ccs, space);
                                }
                                else
                                {
                                    text += $"{0.ToString(formatF6, specCultureInfo)}{space}"; //ajout du X CCS
                                    text += $"{0.ToString(formatF6, specCultureInfo)}{space}"; //ajout du Y CCS
                                    text += $"{0.ToString(formatF6, specCultureInfo)}{space}"; //ajout du H car le Z CCS a été recalculé d'office avec le Geoide 2000 RS2K
                                }
                                text += newLine; //retour ligne
                            }
                        }
                    }
                    return text;
                }
                internal static string AddCalaPointsSU(Line.Station stationLine)
                {

                    List<string> namesAlreadyPresent = new List<string>();
                    string text = $"{Option.CALA} {newLine}";

                    foreach (MeasureOfOffset offset in stationLine._MeasureOffsets)
                    {
                        string pName = offset._Point._Name;
                        if (!namesAlreadyPresent.Contains(pName))
                        {
                            namesAlreadyPresent.Add(pName);
                            if (offset._Point.LGCFixOption == LgcPointOption.CALA && offset._RawReading != na && offset._Point._Coordinates.HasLocal)
                            {
                                AddPointNameToText(ref text, pName, offset._Status, space);
                                AddPointToText(ref text, offset._Point._Coordinates.Local, space);
                                text += newLine; //retour ligne
                            }
                        }
                    }
                    return text;
                }

                internal static string AddRadi(List<Point> points)
                {
                    string s = $"*RADI 0.1{newLine}";
                    return s + AddRadiLine(points);
                }
                internal static string Add90DegreeRadi(List<Point> points)
                {
                    string s = $"*RADI 20{newLine}"; //20 mm to not constrinat too much
                    s += AddRadiLine(points, commented: false, rotatedBy90: true);
                    s += $"% 90deg Radi to avoid double LS solution{newLine}";
                    return s;
                }

                /// <summary>
                /// Ajoute des radi avec des % devant
                /// </summary>
                /// <param name="vxy"></param>
                /// <returns></returns>
                internal static string AddRadiDesactivated(List<Point> points)
                {
                    return AddRadiLine(points, true);
                }

                internal static string AddRadiLine(List<Point> points, bool commented = false, bool rotatedBy90 = false)
                {
                    string comment = commented ? "%" : " ";
                    string text = "";
                    List<string> namesAlreadyPresent = new List<string>();
                    foreach (Point p in points)
                    {
                        if (p._Parameters.GisementFaisceau != na)
                        {
                            string pName = p._Name;
                            if (namesAlreadyPresent.FindIndex(x => x == pName) == -1)
                            {
                                double bearing = p._Parameters.GisementFaisceau;


                                text += $"{tab}{comment}{pName}{space}{bearing}";
                                if (rotatedBy90)
                                {
                                    text += $"   ACST 100{newLine}";
                                }
                                else
                                {
                                    text += $"{newLine}";
                                }
                                namesAlreadyPresent.Add(pName);
                            }
                        }
                    }
                    return text;
                }

                internal static string AddVariablePointsOffsets(Line.Station line)
                //Ajoute les points variable en VXY qui ne sont pas dans les points fixes dans le fil
                {
                    List<string> namesAlreadyPresent = new List<string>();
                    string text = $"{Option.VXY} {newLine}";
                    foreach (MeasureOfOffset offset in line._MeasureOffsets)
                    {
                        string pName = offset._Point._Name;
                        switch (line._Parameters._CoordType)
                        {
                            case CoordinatesType.CCS:
                                if (!namesAlreadyPresent.Contains(pName))
                                {
                                    if (offset._Point.LGCFixOption == LgcPointOption.VXY && offset._CorrectedReading != na && offset._Point._Coordinates.HasCcs)
                                    {
                                        namesAlreadyPresent.Add(pName);
                                        AddPointNameToText(ref text, pName, offset._Status, space);
                                        AddPointToText(ref text, offset._Point._Coordinates.TempCalculation, space);
                                        text += newLine; //retour ligne
                                    }
                                }
                                break;
                            case CoordinatesType.SU:
                                if (!namesAlreadyPresent.Contains(pName))
                                {
                                    if (offset._Point.LGCFixOption == LgcPointOption.VXY && offset._CorrectedReading != na && offset._Point._Coordinates.HasLocal)
                                    {
                                        namesAlreadyPresent.Add(pName);
                                        AddPointNameToText(ref text, pName, offset._Status, space);
                                        AddPointToText(ref text, offset._Point._Coordinates.TempCalculation, space);
                                        text += newLine; //retour ligne
                                    }
                                }
                                break;
                            case CoordinatesType.UserDefined:
                                break;
                            default:
                                break;
                        }
                    }
                    return text;
                }

                internal static void AddPointToText(ref string text, Coordinates pointCoord, string lineSpace)
                {
                    text += Math.Round(pointCoord.X?.Value ?? na, precDigit6).ToString(formatF6, specCultureInfo) + lineSpace; //ajout du X 
                    text += Math.Round(pointCoord.Y?.Value ?? na, precDigit6).ToString(formatF6, specCultureInfo) + lineSpace; //ajout du Y 
                    text += Math.Round(pointCoord.Z?.Value ?? na, precDigit6).ToString(formatF6, specCultureInfo) + lineSpace; //ajout du Z 
                    text += newLine;
                }

                internal static void AddPointNameToText(ref string text, string pName, State status, string lineSpace)
                {

                    if (status is M.States.Bad)
                    {
                        text += $"{tab}%{pName}{lineSpace}";  //ajout nom du point
                    }
                    else
                    {
                        text += $"{tab}{pName}{lineSpace}";  //ajout nom du point
                    }
                }

                internal static void AddCommentToText(ref string text, string comment)
                {
                    text += $"{space}$  {comment}{newLine}";
                }

                internal static string AddAllLibrPointsCCS(Line.Module lineModule)
                {
                    List<string> namesAlreadyPresent = new List<string>();
                    string text = $"{Option.VXY}{newLine}";
                    List<Point> VXYPoint = new List<Point>();
                    foreach (Line.Station.Module item in lineModule.StationModules.OfType<Line.Station.Module>())
                    {
                        if (item.WorkingAverageStationLine._ListStationLine.Count > 0)
                        {
                            Line.Station line = item.WorkingAverageStationLine._ListStationLine[0];
                            foreach (MeasureOfOffset offset in line._MeasureOffsets)
                            {
                                string pName = offset._Point._Name;
                                if (!namesAlreadyPresent.Contains(pName))
                                {
                                    if (offset._RawReading != na && offset._Point._Coordinates.HasCcs)
                                    {
                                        namesAlreadyPresent.Add(pName);
                                        VXYPoint.Add(offset._Point);
                                        AddPointNameToText(ref text, pName, offset._Status, space);
                                        AddPointToText(ref text, offset._Point._Coordinates.Ccs, space);

                                    }
                                }
                            }
                        }
                    }
                    text += AddRadiDesactivated(VXYPoint);
                    return text;
                }
                /// <summary>
                /// Ajoute les points variable en VXY qui ne sont pas dans les points fixes dans le fil en coordonnées CCS
                /// </summary>
                /// <param name="stationLine"></param>
                /// <returns></returns>
                internal static string AddVariablePointsCCS(Line.Station stationLine)
                {
                    List<string> namesAlreadyPresent = new List<string>();
                    string text = $"{Option.VXY}{newLine}";
                    foreach (MeasureOfOffset offset in stationLine._MeasureOffsets)
                    {
                        string pName = offset._Point._Name;
                        if (!namesAlreadyPresent.Contains(pName))
                        {
                            if (offset._Point.LGCFixOption == LgcPointOption.VXY && offset._RawReading != na && offset._Point._Coordinates.HasCcs)
                            {
                                namesAlreadyPresent.Add(pName);
                                AddPointNameToText(ref text, pName, offset._Status, space);
                                AddPointToText(ref text, offset._Point._Coordinates.Ccs, space);
                            }
                        }
                    }
                    return text;
                }
                /// <summary>
                /// Ajoute les points variable en VXY qui ne sont pas dans les points fixes dans le fil en coordonnées Locales
                /// </summary>
                /// <param name="stationLine"></param>
                /// <returns></returns>
                internal static string AddVariablePointsSU(Line.Station stationLine)
                {
                    List<string> namesAlreadyPresent = new List<string>();
                    string text = $"{Option.VXY}{newLine}";

                    foreach (MeasureOfOffset offset in stationLine._MeasureOffsets)
                    {
                        string pName = offset._Point._Name;
                        if (!namesAlreadyPresent.Contains(pName))
                        {
                            if (offset._Point.LGCFixOption == LgcPointOption.VXY && offset._RawReading != na && offset._Point._Coordinates.HasLocal)
                            {
                                namesAlreadyPresent.Add(pName);
                                AddPointNameToText(ref text, pName, offset._Status, space);
                                AddPointToText(ref text, offset._Point._Coordinates.Local, space);
                            }
                        }
                    }
                    return text;
                }
                /// <summary>
                /// Ajoute tous les points en coord SU pour un calcul fil en libre
                /// </summary>
                /// <param name="lineModule"></param>
                /// <returns></returns>
                internal static string AddAllLibrPointsSU(Line.Module lineModule)
                {
                    List<string> namesAlreadyPresent = new List<string>();
                    string text = $"{Option.VXY}{newLine}";
                    foreach (Line.Station.Module item in lineModule.StationModules.OfType<Line.Station.Module>())
                    {
                        if (item.WorkingAverageStationLine._ListStationLine.Count > 0)
                        {
                            Line.Station line = item.WorkingAverageStationLine._ListStationLine[0];
                            foreach (MeasureOfOffset offset in line._MeasureOffsets)
                            {
                                string pName = offset._Point._Name;
                                if (!namesAlreadyPresent.Contains(pName))
                                {
                                    if (offset._Point.LGCFixOption == LgcPointOption.VXY && offset._RawReading != na && offset._Point._Coordinates.HasLocal)
                                    {
                                        namesAlreadyPresent.Add(pName);
                                        AddPointNameToText(ref text, pName, offset._Status, space);
                                        AddPointToText(ref text, offset._Point._Coordinates.Local, space);

                                    }
                                }
                            }
                        }
                    }
                    return text;
                }

                internal static string EchoReduced(Line.Station line)
                //Ajoute toutes les mesures d'offset du fil
                {

                    List<string> namesAlreadyPresent = new List<string>();
                    //ajoute les points ancrage 1 et 2 dans la liste car ils devront être mis en premier et en dernier dans les mesures
                    namesAlreadyPresent.Add(line._Anchor1._Point._Name);
                    namesAlreadyPresent.Add(line._Anchor2._Point._Name);
                    string text = $"{Option.ECHO}{space}";
                    text += (line._Parameters._Instrument._Name + space).Replace('-', '_'); // nom regle decalage
                    text += $"{newLine}{tab}{line._Anchor1._Point._Name}{space}"; //ajoute nom premier ancrage
                    text += $"{Math.Round(line._Anchor1._ReductedReading, precDigit5).ToString(formatF5, specCultureInfo)}{space}$Debut du fil{newLine}"; //ajoute mesure ancrage 1
                    foreach (MeasureOfOffset offset in line._MeasureOffsets)
                    {
                        string pName = offset._Point._Name;
                        switch (line._Parameters._CoordType)
                        {
                            case CoordinatesType.CCS:
                                if (!namesAlreadyPresent.Contains(pName) && offset._CorrectedReading != na && offset._Point._Coordinates.HasCcs)
                                {
                                    namesAlreadyPresent.Add(pName);
                                    AddPointNameToText(ref text, pName, offset._Status, space);
                                    text += $"{Math.Round(offset._ReductedReading, precDigit6).ToString(formatF6, specCultureInfo)}"; //ajout mesure corrigee
                                    AddCommentToText(ref text, offset.Comment);
                                }
                                break;
                            case CoordinatesType.SU:
                                if (!namesAlreadyPresent.Contains(pName) && offset._CorrectedReading != na && offset._Point._Coordinates.HasSu)
                                {
                                    namesAlreadyPresent.Add(pName);
                                    AddPointNameToText(ref text, pName, offset._Status, space);
                                    text += $"{Math.Round(offset._ReductedReading, precDigit6).ToString(formatF6, specCultureInfo)}"; //ajout mesure corrigee
                                    AddCommentToText(ref text, offset.Comment);
                                }
                                break;
                            case CoordinatesType.UserDefined:
                                break;
                            default:
                                break;
                        }

                    }
                    text += $"{tab}{line._Anchor2._Point._Name}{space}"; //ajoute nom ancrage 2
                    text += $"{Math.Round(line._Anchor2._ReductedReading, precDigit5).ToString(formatF5, specCultureInfo)}{space}$Fin du fil{newLine}"; //ajoute mesure ancrage 2
                    return text;
                }
                internal static string EchoCorrected(Line.Station line, List<MeasureWithStatus> forcedList = null)
                //Ajoute toutes les mesures d'offset du fil
                {
                    List<string> namesAlreadyPresent = new List<string>();
                    //ajoute les points ancrage 1 et 2 dans la liste car ils devront être mis en premier et en dernier dans les mesures
                    namesAlreadyPresent.Add(line._Anchor1._Point._Name);
                    namesAlreadyPresent.Add(line._Anchor2._Point._Name);
                    string text = $"{Option.ECHO}{space}";
                    text += (line._Parameters._Instrument._Name + space).Replace('-', '_'); // nom regle decalage
                    text += $"{newLine}{tab}{line._Anchor1._Point._Name}{space}"; //ajoute nom premier ancrage
                    text += $"{Math.Round(line._Anchor1._CorrectedReading, precDigit5).ToString(formatF5, specCultureInfo)}{space}$Debut du fil{newLine}"; //ajoute mesure ancrage 1


                    var measureToExport = new List<MeasureOfOffset>(line._MeasureOffsets);
                    if (forcedList != null)
                    {
                        measureToExport.Clear();
                        foreach (var stateAndMeasure in forcedList)
                        {
                            measureToExport.Add(stateAndMeasure.Measure as MeasureOfOffset);
                        }
                    }
                    foreach (MeasureOfOffset offset in measureToExport)
                    {
                        string pName = offset._Point._Name;
                        switch (line._Parameters._CoordType)
                        {
                            case CoordinatesType.CCS:
                                if (!namesAlreadyPresent.Contains(pName) && offset._RawReading != na && offset._Point._Coordinates.HasCcs)
                                {
                                    AddPointNameToText(ref text, pName, offset._Status, space);
                                    text += Math.Round(offset._CorrectedReading, precDigit6).ToString(formatF5, specCultureInfo); //ajout mesure corrigee
                                    AddCommentToText(ref text, offset.Comment);
                                }
                                break;
                            case CoordinatesType.SU:
                                if (!namesAlreadyPresent.Contains(offset._Point._Name) && offset._RawReading != na && offset._Point._Coordinates.HasSu)
                                {
                                    namesAlreadyPresent.Add(offset._Point._Name);
                                    AddPointNameToText(ref text, pName, offset._Status, space);
                                    text += Math.Round(offset._CorrectedReading, precDigit6).ToString(formatF5, specCultureInfo); //ajout mesure corrigee
                                    AddCommentToText(ref text, offset.Comment);
                                }
                                break;
                            case CoordinatesType.UserDefined:
                                break;
                            default:
                                break;
                        }

                    }
                    text += $"{tab}{line._Anchor2._Point._Name}{space}"; //ajoute nom ancrage 2
                    text += $"{Math.Round(line._Anchor2._CorrectedReading, precDigit5).ToString(formatF5, specCultureInfo)}{space}$Fin du fil{newLine}"; //ajoute mesure ancrage 2
                    return text;
                }
                /// <summary>
                /// Ajoute toutes les mesures d'offset présentes dans chaque station line module du line module
                /// </summary>
                /// <param name="lineModule"></param>
                /// <returns></returns>
                internal static string EchoCorrected(Line.Module lineModule)
                {
                    string text = "";
                    foreach (Line.Station.Module item in lineModule.StationModules.OfType<Line.Station.Module>())
                    {
                        if (item.WorkingAverageStationLine._ListStationLine.Count > 0)
                        {
                            text += EchoCorrected(item.WorkingAverageStationLine._ListStationLine[0]);
                        }
                    }
                    return text;
                }
                /// <summary>
                /// Calcule les distances théoriques pour les calculer les offsets des fils et les ajoutes dans le fichier input
                /// </summary>
                /// <param name="line"></param>
                /// <returns></returns>
                internal static string DsptTheoOffset(Line.Station line, List<MeasureWithStatus> forcedList = null)
                //Ajoute les distances theo des points du fil à partir de l'ancrage 1
                {
                    List<string> namesAlreadyPresent = new List<string>();
                    //ajoute les points ancrage 1 et 2 dans la liste car ils devront être mis en premier et en dernier dans les mesures
                    Point pAnchor1 = line._Anchor1._Point;
                    Point pAnchor2 = line._Anchor2._Point;
                    namesAlreadyPresent.Add(pAnchor1._Name);
                    namesAlreadyPresent.Add(pAnchor2._Name);
                    string text = $"{Option.DSPT}{space}{pAnchor1._Name}{space}DISTTHEO{newLine}"; //ligne station ancrage 1 comme référence;

                    var measureToExport = new List<MeasureOfOffset>(line._MeasureOffsets);
                    if (forcedList != null)
                    {
                        measureToExport.Clear();
                        foreach (var stateAndMeasure in forcedList)
                        {
                            measureToExport.Add(stateAndMeasure.Measure as MeasureOfOffset);
                        }
                    }
                    foreach (MeasureOfOffset offset in measureToExport)
                    {
                        string pName = offset._Point._Name;
                        switch (line._Parameters._CoordType)
                        {
                            case CoordinatesType.CCS:
                                if (!namesAlreadyPresent.Contains(pName) && offset._CorrectedReading != na && offset._Point._Coordinates.HasCcs)
                                {
                                    namesAlreadyPresent.Add(pName);
                                    AddPointNameToText(ref text, pName, offset._Status, space);
                                    var sDist = Survey.GetSpatialDistance(pAnchor1._Coordinates.TempCalculation,
                                                                            offset._Point._Coordinates.TempCalculation);
                                    text += $"{Math.Round(sDist, precDigit6).ToString(formatF6, specCultureInfo)}{newLine}"; //ajoute la distance theo du point à partir du point d'ancrage 1
                                }
                                break;
                            case CoordinatesType.SU:
                                if (!namesAlreadyPresent.Contains(pName) && offset._CorrectedReading != na && offset._Point._Coordinates.HasLocal)
                                {
                                    namesAlreadyPresent.Add(pName);
                                    AddPointNameToText(ref text, pName, offset._Status, space);
                                    var sDist = Survey.GetSpatialDistance(pAnchor1._Coordinates.TempCalculation,
                                                                            offset._Point._Coordinates.TempCalculation);
                                    text += $"{Math.Round(sDist, precDigit6).ToString(formatF6, specCultureInfo)}{newLine}"; //ajoute la distance theo du point à partir du point d'ancrage 1
                                }
                                break;
                            case CoordinatesType.UserDefined:
                                break;
                            default:
                                break;
                        }
                    }
                    text += $"{tab}{pAnchor2._Name}{space}"; //ajout nom du point mesuré
                    var spatDist = Survey.GetSpatialDistance(pAnchor1._Coordinates.TempCalculation, pAnchor2._Coordinates.TempCalculation);
                    text += $"{Math.Round(spatDist, precDigit6).ToString(formatF6, specCultureInfo)}{newLine}"; //ajoute la distance theo entre l'ancrage 1 et l'ancrage 2
                    return text;
                }
                /// <summary>
                /// Calcule les distances théoriques pour les calculer les offsets des fils et les ajoutes dans le fichier input
                /// </summary>
                /// <param name="line"></param>
                /// <returns></returns>
                internal static string DsptTheoCCS(Line.Station line)
                //Ajoute les distances theo des points du fil à partir de l'ancrage 1
                {
                    Point pAnchor1 = line._Anchor1._Point;
                    Point pAnchor2 = line._Anchor2._Point;
                    //ajoute les points ancrage 1 et 2 dans la liste car ils devront être mis en premier et en dernier dans les mesures
                    List<string> namesAlreadyPresent = new List<string>
                    {
                        pAnchor1._Name,
                        pAnchor2._Name
                    };

                    string text = $"{Option.DSPT}{space}{pAnchor1._Name}{space}DISTTHEO{newLine}"; //ligne station ancrage 1 comme référence;
                    foreach (MeasureOfOffset offset in line._MeasureOffsets)
                    {
                        string pName = offset._Point._Name;
                        if (!namesAlreadyPresent.Contains(pName) && offset._RawReading != na && offset._Point._Coordinates.HasCcs)
                        {
                            AddPointNameToText(ref text, pName, offset._Status, space);
                            var sDist = Survey.GetSpatialDistance(pAnchor1._Coordinates.Ccs, offset._Point._Coordinates.Ccs);
                            text += $"{Math.Round(sDist, precDigit5).ToString(formatF6, specCultureInfo)}{newLine}"; //ajoute la distance theo du point à partir du point d'ancrage 1
                        }
                    }
                    text += $"{tab}{pAnchor2._Name}{space}"; //ajout nom du point mesuré
                    var spatDist = Survey.GetSpatialDistance(pAnchor1._Coordinates.Ccs, pAnchor2._Coordinates.Ccs);
                    text += $"{Math.Round(spatDist, precDigit6).ToString(formatF6, specCultureInfo)}{newLine}"; //ajoute la distance theo entre l'ancrage 1 et l'ancrage 2
                    return text;
                }
                /// <summary>
                /// Ajoute toutes les mesures d'offset présentes dans chaque station line module du line module
                /// </summary>
                /// <param name="lineModule"></param>
                /// <returns></returns>
                internal static string DsptTheoCCS(Line.Module lineModule)
                {
                    string text = "";
                    foreach (Line.Station.Module item in lineModule.StationModules.OfType<Line.Station.Module>())
                    {
                        if (item.WorkingAverageStationLine._ListStationLine.Count > 0)
                        {
                            text += DsptTheoCCS(item.WorkingAverageStationLine._ListStationLine[0]);
                        }
                    }
                    return text;
                }
                /// <summary>
                /// Ajoute toutes les dist theo présentes dans chaque station line module du line module en coord SU
                /// </summary>
                /// <param name="lineModule"></param>
                /// <returns></returns>
                internal static string DsptTheoSU(Line.Module lineModule)
                {
                    string text = "";
                    foreach (Line.Station.Module item in lineModule.StationModules.OfType<Line.Station.Module>())
                    {
                        if (item.WorkingAverageStationLine._ListStationLine.Count > 0)
                        {
                            text += DsptTheoSU(item.WorkingAverageStationLine._ListStationLine[0]);
                        }
                    }
                    return text;
                }
                /// <summary>
                /// Calcule les distances théoriques pour les calculer les offsets des fils et les ajoutes dans le fichier input
                /// </summary>
                /// <param name="line"></param>
                /// <returns></returns>
                internal static string DsptTheoSU(Line.Station line)
                //Ajoute les distances theo des points du fil à partir de l'ancrage 1
                {
                    Point pAnchor1 = line._Anchor1._Point;
                    Point pAnchor2 = line._Anchor2._Point;
                    //ajoute les points ancrage 1 et 2 dans la liste car ils devront être mis en premier et en dernier dans les mesures
                    List<string> namesAlreadyPresent = new List<string>
                    {
                        pAnchor1._Name,
                        pAnchor2._Name
                    };
                    string text = $"{Option.DSPT}{space}{pAnchor1._Name}{space}DISTTHEO{newLine}"; //ligne station ancrage 1 comme référence;
                    foreach (MeasureOfOffset offset in line._MeasureOffsets)
                    {
                        string pName = offset._Point._Name;
                        if (!namesAlreadyPresent.Contains(pName) && offset._RawReading != na && offset._Point._Coordinates.HasLocal)
                        {
                            namesAlreadyPresent.Add(pName);
                            AddPointNameToText(ref text, pName, offset._Status, space);
                            var sDist = Survey.GetSpatialDistance(pAnchor1._Coordinates.Local, offset._Point._Coordinates.Local);
                            text += $"{Math.Round(sDist, precDigit6).ToString(formatF6, specCultureInfo)}{newLine}"; //ajoute la distance theo du point à partir du point d'ancrage 1
                        }
                    }
                    text += $"{tab}{pAnchor2._Name}{space}"; //ajout nom du point mesuré
                    var spatDist = Survey.GetSpatialDistance(line._Anchor1._Point._Coordinates.Local, line._Anchor2._Point._Coordinates.Local);
                    text += $"{Math.Round(spatDist, precDigit6).ToString(formatF6, specCultureInfo)}{newLine}"; //ajoute la distance theo entre l'ancrage 1 et l'ancrage 2
                    return text;
                }

                internal static string OptionCreatePunchCooFile()
                //Option creation punch file
                {
                    return $"{Option.PUNC}OUT1{newLine}";
                }
                internal static string LevelInstrument(Level.Station.Module stationLevelModule)
                //ajoute l'instrument de nivellement dans le fichier input
                {
                    string text = "";
                    if (stationLevelModule._WorkingStationLevel._Parameters._UseDistanceForLGC2)
                    {
                        text += Instr();
                        text += $"{tab}*LEVEL{space}";
                        text += (stationLevelModule._WorkingStationLevel._Parameters._Instrument._Name + space).Replace('-', '_'); // nom niveau
                        text += $"{stationLevelModule._WorkingStationLevel._MeasureOfLevel[0]._staff._Name}{space}";//str: Default levelling staff ID (must match an ID from the levelling staff lines of this instrument)
                        text += $"{stationLevelModule._WorkingStationLevel._Parameters._IsCollimationAngleUnknown}{space}"; //bol: Identifies if the collimation angle is an "unknown value" to be determined in the adjustment
                        text += $"{stationLevelModule._WorkingStationLevel._Parameters._CollimationAngle}{newLine}"; //flt: Collimation angle value for the instrument [gon]
                    }
                    return text;
                }
                internal static string StaffsForLeveling(Level.Station.Module stationLevelModule)
                //ajoute toutes les mires de nivellement dans le fichier input
                {
                    string text = "";
                    if (stationLevelModule._WorkingStationLevel._Parameters._UseDistanceForLGC2)
                    {
                        List<I.LevelingStaff> staffused = new List<I.LevelingStaff>();
                        foreach (MeasureOfLevel meas in stationLevelModule._WorkingStationLevel._MeasureOfLevel)
                        {
                            if (!staffused.Contains(meas._staff))
                            {
                                staffused.Add(meas._staff);
                                text += $"{tab}{tab}";
                                text += $"{meas._staff._Name}{space}"; //str: Levelling staff ID
                                text += $"{meas._staff._VerticalDeviation}{space}"; //flt: Standard deviation of a measured vertical distance [mm]
                                text += $"{meas._staff._VerticalPPM}{space}"; //flt: PPM error value for a measured vertical distance [mm]
                                text += $"{meas._staff._VerticalDistanceCorrection}{space}"; //flt: Distance correction value for the measurements [m]
                                text += $"{meas._staff._VerticalDistanceCorrectionDeviation}{space}"; //flt: Standard deviation of the distance correction value [mm]
                                text += $"{meas._staff._VerticalOffset}{space}"; //flt: Vertical offset of the staff [m]
                                text += $"{meas._staff._VerticalOffsetDeviation}{newLine}"; //flt: Standard deviation of the vertical offset of the staff [mm]
                            }
                        }
                    }
                    return text;
                }

                internal static string AddCalaPoints(Level.Station.Module stationLevelModule)
                //Ajoute des points en cala en tenant compte du systeme de coordonnées choisi
                {
                    string b = space + space + space;
                    List<string> pointsAlreadyUsed = new List<string>();
                    string text = $"{Option.CALA}{newLine}";

                    foreach (MeasureOfLevel measLevel in stationLevelModule._WorkingStationLevel._MeasureOfLevel)
                    {
                        if (!pointsAlreadyUsed.Contains(measLevel._PointName) && measLevel._RawLevelReading != na)
                        {
                            if (measLevel._Point.LGCFixOption == LgcPointOption.CALA)
                            {
                                ///Met un % si la mesure est en bad et ne l'ajoute pas aux points déjà mesuré au cas ou une autre mesure ne serait pas en bad.
                                AddPointNameToText(ref text, measLevel._PointName, measLevel._Status, b);
                                var coordPoint = measLevel._Point._Coordinates;
                                switch (stationLevelModule._WorkingStationLevel._Parameters._ZType)
                                {
                                    case CoordinatesType.CCS:
                                        AddPointToText(ref text, coordPoint.Ccs, b);
                                        break;
                                    case CoordinatesType.SU:
                                        AddPointToText(ref text, coordPoint.Local, b);
                                        break;
                                    case CoordinatesType.UserDefined:
                                        AddPointToText(ref text, coordPoint.UserDefined, b);
                                        break;
                                    default:
                                        break;
                                }
                                //text += newLine; //retour ligne
                            }
                        }
                    }
                    if (stationLevelModule._WorkingStationLevel._Parameters.HStationLocked == true)
                    {
                        if (!stationLevelModule._WorkingStationLevel._Parameters._UseDistanceForLGC2) //ajoute une station nivellement sauf si calcul avec DLEV
                        {
                            text += $"{AddLevelingStation(stationLevelModule)}{newLine}{newLine}";
                        }
                    }
                    return text;
                }

                internal static string AddVariablePoints(Level.Station.Module stationLevelModule)
                //Ajout les points nivelés en VZ
                {
                    string text = "";
                    List<string> namesAlreadyPresent = new List<string>();
                    string b = space + space + space;
                    text = $"*VZ {newLine}";
                    foreach (MeasureOfLevel measLevel in stationLevelModule._WorkingStationLevel._MeasureOfLevel)
                    {
                        if (!namesAlreadyPresent.Contains(measLevel._Point._Name) && measLevel._RawLevelReading != na)
                        {
                            if (measLevel._Point.LGCFixOption == LgcPointOption.VZ)
                            {
                                ///Met un % si la mesure est en bad et ne l'ajoute pas aux points déjà mesuré au cas ou une autre mesure ne serait pas en bad.
                                AddPointNameToText(ref text, measLevel._PointName, measLevel._Status, b);
                                var pCoord = measLevel._Point._Coordinates;
                                switch (stationLevelModule._WorkingStationLevel._Parameters._ZType)
                                {
                                    case CoordinatesType.CCS:
                                        AddPointToText(ref text, pCoord.Ccs, b);
                                        break;
                                    case CoordinatesType.SU:
                                        AddPointToText(ref text, pCoord.Local, b);
                                        break;
                                    case CoordinatesType.UserDefined:
                                        AddPointToText(ref text, pCoord.UserDefined, b);
                                        break;
                                    default:
                                        break;
                                }
                                //text += newLine; //retour ligne
                            }
                        }
                    }
                    if (stationLevelModule._WorkingStationLevel._Parameters.HStationLocked == false)
                    {
                        if (!stationLevelModule._WorkingStationLevel._Parameters._UseDistanceForLGC2) //ajoute une station nivellement sauf si calcul avec DLEV
                        {
                            text += $"{AddLevelingStation(stationLevelModule)}{newLine}";
                        }
                    }
                    return text;
                }
                internal static string AddLevelingStation(Level.Station.Module stationLevelModule)
                //Ajoute la station de nivellement en point variable
                {
                    string b = space + space + space;
                    string text = "";
                    text += $"{tab}{stationLevelModule._WorkingStationLevel._Name}{b}";
                    text += $"0{b}"; //X station
                    text += $"0{b}"; //Y station
                    text += $"{Math.Round(stationLevelModule._WorkingStationLevel._Parameters._HStation, precDigit6).ToString(formatF6, specCultureInfo)}";
                    return text;
                }
                internal static string AddDVER(Level.Station.Module stationLevelModule)
                //ajoute les distances verticales sauf pour les DNA03 qui sont en DLEV
                {
                    Level.Station st = stationLevelModule._WorkingStationLevel;
                    string text = "";
                    text += AddDVerFromStation(st);
                    return text;
                }
                /// <summary>
                /// Ajoute les mesures de la station au texte, N'utilise pas les distances pour le calcul
                /// </summary>
                /// <param name="st"></param>
                /// <param name="s"></param>
                /// <returns></returns>
                internal static string AddDVerFromStation(Level.Station st)
                {
                    string text = "";
                    if (!st._Parameters._UseDistanceForLGC2 && st._MeasureOfLevel.Count != 0 && st._MeasureOfLevel.FindIndex(x => x._RawLevelReading != na && !(x._Status is M.States.Bad)) != -1)
                    {
                        string b = space + space + space;
                        var prec = Tsunami2.Preferences.Values.InstrumentSlashTargetCoupleSigmas.FirstOrDefault(x => x.Instrument == "NIVEAU");
                        text = $"*DVER {prec.sigmaDistMm} {newLine}"; //mot clé pour distances verticales;
                        foreach (MeasureOfLevel measLevel in st._MeasureOfLevel)
                        {
                            if (measLevel._RawLevelReading != na)
                            {
                                text += tab;
                                if (measLevel._Status is M.States.Bad)
                                    text += "%";

                                text += $"{st._Name}{b}{measLevel._PointName}{b}"; //ajout station +  nom du point mesuré
                                text += Math.Round(-measLevel._CorrLevelReadingForEtalonnage, precDigit6).ToString(formatF6, specCultureInfo) + b; //ajoute la lecture de nivellement, la lecture est du signe opposé dans LGC
                                                                                                                                                   //s += "OBSE" + " " + Math.Round(measLevel._staff._VerticalDeviation, 3).ToString("F3", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + b;
                                text += $"DCOR {Math.Round(-measLevel._Extension + measLevel._staff._VerticalDistanceCorrection, precDigit5).ToString(formatF5, specCultureInfo)}";
                                //Indique sens nivellement dans commentaires
                                text += $"{b}$ ";
                                switch (st._Parameters._LevelingDirection)
                                {
                                    case LevelingDirection.ALLER:
                                        text += R.StringLevel_LGCAller + space;
                                        break;
                                    case LevelingDirection.RETOUR:
                                        text += R.StringLevel_LGCRetour + space;
                                        break;
                                    case LevelingDirection.REPRISE:
                                        text += R.StringLevel_LGCReprise + space;
                                        break;
                                    default:
                                        break;
                                }
                                text += $"{measLevel.Comment}{newLine}"; // Commentaire
                            }
                        }
                    }
                    return text;
                }
                internal static string AddDLEV(Level.Station.Module stationLevelModule)
                //ajoute les distances verticales sauf pour les DNA03 qui sont en DLEV : met actuellement un % pour ne pas tenir compte des mesures de distance
                {
                    string text = "";
                    if (stationLevelModule._WorkingStationLevel._Parameters._UseDistanceForLGC2)
                    {
                        string b = space + space + space;
                        text = $"%*DLEV{b}{stationLevelModule._WorkingStationLevel._Parameters._Instrument._Name}{b}{newLine}"; //mot clé pour leveling avec distance horizontale en +;
                        foreach (MeasureOfLevel measLevel in stationLevelModule._WorkingStationLevel._MeasureOfLevel)
                        {
                            if (measLevel._RawLevelReading != na)
                            {
                                text += $"{tab}%{measLevel._PointName}{b}"; //ajout nom du point mesuré
                                var dist = -(measLevel._CorrLevelReadingForEtalonnage + measLevel._Extension);
                                text += $"{Math.Round(dist, precDigit6).ToString(formatF6, specCultureInfo)}{b}"; //flt: Observed vertical distance value [m]
                                text += $"DHOR{b}{Math.Round(measLevel._Distance, 3).ToString("F3", specCultureInfo)}{b}"; //flt: Horizontal distance value - between the reference point (instrument) and the stationed point (scale) [m]
                                text += $"DSE{b}{Math.Round(measLevel._staff._SpatialDistanceDeviation, 1).ToString("F3", specCultureInfo)}{b}"; //flt: Standard deviation of the measured horizontal distance - between the reference point (instrument) and the stationed point (scale) [mm]
                                text += $"TRGT{b}{measLevel._staff._Name}{b}"; //str: Levelling staff ID
                                text += $"OBSE{b}{measLevel._staff._VerticalDeviation}{b}"; //flt: Standard deviation of a measured vertical distance [mm]
                                text += $"PPM{b}{measLevel._staff._VerticalPPM}{b}"; //flt: PPM error value for a measured vertical distance [mm]
                                text += $"TH{b}{measLevel._staff._VerticalOffset}{b}"; //flt: Vertical offset of the staff [m]
                                text += $"THSE{b}{measLevel._staff._VerticalOffsetDeviation}{newLine}{newLine}"; //flt: Standard deviation of the vertical offset of the staff [mm]
                            }
                        }
                    }
                    return text;
                }

                /// <summary>
                /// Ajoute tous les points de cala dans le fichier LGC global de calcul de nivellement
                /// </summary>
                /// <param name="levelModule"></param>
                /// <returns></returns>
                internal static string AddAllPointsAndStation(Level.Module levelModule)
                {
                    List<string> listCalaPoints = new List<string>();
                    List<string> listVZPoints = new List<string>();
                    List<string> listStationCala = new List<string>();
                    List<string> listStationFree = new List<string>();

                    listCalaPoints.Add(Option.CALA);
                    listVZPoints.Add("*VZ");
                    foreach (Level.Station.Module stm in levelModule.StationModules.OfType<Level.Station.Module>())
                        foreach (Level.Station st in stm.AllStationLevels)
                            AddPointsAndStationToList(listCalaPoints, listVZPoints, listStationCala, listStationFree, st);
                    string text = "";
                    ///Ecrit le texte à mettre dans le fichier LGC
                    foreach (string line in listCalaPoints)
                    {
                        text += $"{line}{newLine}";
                    }
                    foreach (string line in listStationCala)
                    {
                        text += $"{line}{newLine}";
                    }
                    foreach (string line in listVZPoints)
                    {
                        text += $"{line}{newLine}";
                    }
                    foreach (string line in listStationFree)
                    {
                        text += $"{line}{newLine}";
                    }
                    return text;
                }
                /// <summary>
                /// Ajoute les points des mesures actuelles en points Cala ou VZ, Ajoute la working station dans les points en cala ou VZ
                /// </summary>
                /// <param name="listCalaPoints"></param>
                /// <param name="listVZPoints"></param>
                /// <param name="listStationCala"></param>
                /// <param name="listStationFree"></param>
                /// <param name="st"></param>
                private static void AddPointsAndStationToList(List<string> listCalaPoints, List<string> listVZPoints, List<string> listStationCala, List<string> listStationFree, Level.Station st)
                {
                    if (st._MeasureOfLevel.Count != 0 && st._MeasureOfLevel.FindIndex(x => x._RawLevelReading != na && !(x._Status is M.States.Bad)) != -1)
                    {
                        string b = space + space + space;
                        string station = $"{tab}{st._Name}{b}0{b}0{b}";
                        station += Math.Round(st._Parameters._HStation, precDigit6).ToString(formatF6, specCultureInfo);
                        if (st._Parameters.HStationLocked == true) { listStationCala.Add(station); }
                        if (st._Parameters.HStationLocked == false) { listStationFree.Add(station); }
                        foreach (MeasureOfLevel measLevel in st._MeasureOfLevel)
                        {
                            string levName = measLevel._PointName;
                            if (measLevel._RawLevelReading != na)
                            {
                                ///Met un % si la mesure est en bad et ne l'ajoute pas aux points déjà mesuré au cas ou une autre mesure ne serait pas en bad.
                                int indexCala = listCalaPoints.FindIndex(x => x.Contains("%" + levName));
                                int indexVz = listVZPoints.FindIndex(x => x.Contains("%" + levName));
                                //Enlève le point avec un % car rajouté par la suite d'office en tant que point bon ou mauvais
                                if (indexCala != -1)
                                    listCalaPoints.RemoveAt(indexCala);
                                //Enlève le point dans les Vz si déjà présent en tant que point mauvais
                                if (indexVz != -1)
                                    listVZPoints.RemoveAt(indexVz);
                                indexVz = listVZPoints.FindIndex(x => x.Contains(levName));
                                indexCala = listCalaPoints.FindIndex(x => x.Contains(levName));
                                string text = "";
                                AddPointNameToText(ref text, levName, measLevel._Status, b);

                                switch (st._Parameters._ZType)
                                {
                                    case CoordinatesType.CCS:
                                        AddPointToText(ref text, measLevel._Point._Coordinates.Ccs, b);
                                        break;
                                    case CoordinatesType.SU:
                                        AddPointToText(ref text, measLevel._Point._Coordinates.Local, b);
                                        break;
                                    case CoordinatesType.UserDefined:
                                        AddPointToText(ref text, measLevel._Point._Coordinates.UserDefined, b);
                                        break;
                                    default:
                                        break;
                                }
                                if (measLevel._Point.LGCFixOption == LgcPointOption.CALA)
                                {
                                    if (indexCala == -1)
                                        listCalaPoints.Add(text);
                                    if (indexVz != -1)
                                        listVZPoints.RemoveAt(indexVz);
                                }
                                if (measLevel._Point.LGCFixOption == LgcPointOption.VZ)
                                {
                                    if (indexCala == -1 && indexVz == -1)
                                        listVZPoints.Add(text);
                                }
                            }
                        }
                    }
                }
                /// <summary>
                /// Ajoute l'aller, le retour et les reprises dans les mesures de nivellement
                /// </summary>
                /// <param name="levelModule"></param>
                /// <returns></returns>
                internal static string AddDVER(Level.Module levelModule)
                {
                    string text = "";
                    foreach (Level.Station.Module stlm in levelModule.StationModules.OfType<Level.Station.Module>())
                        foreach (Level.Station st in stlm.AllStationLevels)
                            text += AddDVerFromStation(st);
                    return text;
                }

                internal static string Precision(int p)
                {
                    return $"*PREC {p}{newLine}";
                }

                internal static bool HasCala(Level.Station.Module stationLevelModule)
                {
                    return HasCala(stationLevelModule._WorkingStationLevel);
                }

                internal static bool HasCala(Level.Module levelModule)
                {
                    return levelModule.StationModules.OfType<Level.Station.Module>().Any(st => HasCala(st._AllerStationLevel));
                }

                internal static bool HasCala(Level.Station m)
                {
                    return m._MeasureOfLevel.Exists(x => x._Point.LGCFixOption == LgcPointOption.CALA);
                }

                /// <summary>
                /// Ajoute l'option LIBR au fichier LGC
                /// </summary>
                /// <returns></returns>
                internal static string OptionLIBR()
                {
                    // 2.8 is now robust and have consi LIBR
                    //if (P.Preferences.Dependencies.Applications.Exists(x => x._Name.ToUpper() == "LGC_NEW"))
                    return $"*CONSI LIBR{newLine}";
                    //else
                    //    return $"*LIBR{newLine}";
                }

                internal static string OptionJSON(bool value)
                {
                    return value ? $"*JSON{newLine}" : "";
                }

                internal static string Inclinometers(List<MeasureWithStatus> statesAndMeasures)
                {

                    string content = "";
                    List<string> isntruments = new List<string>();
                    var prec = Tsunami2.Preferences.Values.InstrumentSlashTargetCoupleSigmas.FirstOrDefault(x => x.Instrument == "TILTMETRE");
                    foreach (var stateAndMeasure in statesAndMeasures)
                    {
                        var measure = stateAndMeasure.Measure;
                        string isn = measure._InstrumentSN;
                        if (!isntruments.Contains(isn))
                        {
                            double sigmaInCC = prec.sigmaAnglCc;
                            double sigmaInmRad = sigmaInCC / 10000 / 200 * Math.PI * 1000;
                            //content += $"*INCL {isn} 19 0 0 0 0"; // 19cc
                            content += $"   %INCL InstrumentID s[cc] s[ppm] anglCorr[gon] sAnglCorr[cc] refAngl[gon] sRefAngl[cc]\r\n"; // *INCLY *INCL with ppm
                            content += $"   *INCL {isn,12} {sigmaInCC}    0.0           0.0           0.0          0.0          0.0"; // *INCLY *INCL with ppm
                            isntruments.Add(isn);
                        }
                    }
                    return content + "\r\n";
                }

                internal static string AddVariableInZ(List<Point> vZ, string coordinateSystemName)
                {
                    string content = "";
                    if (vZ.Count > 0)
                        content += "*VZ" + "\r\n";
                    foreach (var item in vZ)
                    {
                        content += "   " + item._Name;
                        content += "   " + "   " + item._Coordinates.GetCoordinatesInASystemByName(coordinateSystemName).X.Value;
                        content += "   " + "   " + item._Coordinates.GetCoordinatesInASystemByName(coordinateSystemName).Y.Value;
                        content += "   " + "   " + item._Coordinates.GetCoordinatesInASystemByName(coordinateSystemName).Z.Value;
                        content += "\r\n";
                    }
                    return content;
                }

                internal static string AddVariableInXY(List<Point> vXY, string coordinateSystemName)
                {
                    string content = "";
                    if (vXY.Count > 0)
                        content += "*VXY" + "\r\n";
                    foreach (var item in vXY)
                    {
                        content += "   " + item._Name;
                        content += "   " + "   " + item._Coordinates.GetCoordinatesInASystemByName(coordinateSystemName).X.Value;
                        content += "   " + "   " + item._Coordinates.GetCoordinatesInASystemByName(coordinateSystemName).Y.Value;
                        content += "   " + "   " + item._Coordinates.GetCoordinatesInASystemByName(coordinateSystemName).Z.Value;
                        content += "\r\n";
                    }
                    return content;
                }
            }

            internal void ShowInput(bool ForceSurveyPad = false)
            {
                Shell.ExecutePathInDialogView(lgc2._FileName + FileFormat.INP, "SurveyPad", R.T_CANCEL, waitProcessEnd: false);
            }

            #endregion

            /// <summary>
            /// Permet editer le fichier input et affiche un message
            /// </summary>
            internal void OpenEditor()
            {
                OpenEditor(lgc2._FileName + FileFormat.INP, waitProcessEnd: true);
            }

            public static void OpenEditor(string filename, bool waitProcessEnd = false)
            {
                //Open with what?
                var surveyPad = P.Preferences.Dependencies.Applications.Find(o => o._Name.ToUpper() == "SURVEYPAD");
                if (surveyPad != null && surveyPad.IsInstalled)
                {
                    Shell.ExecutePathInDialogView(
                        filename,
                        "SurveyPad",
                        R.T_CANCEL,
                        launchSurveyPadPlugin: false,
                        waitProcessEnd: waitProcessEnd);
                }
                else
                {
                    Shell.RunInDialogView(
                        pathToExecute: filename,
                        appName: "LGC2 INPUT");
                }
            }

            internal static List<Point> GetPointsWithoutObservations(List<Point> points, CloneableList<M.Measure> measuresTaken)
            {
                List<Point> pointsFound = new List<Point>();
                pointsFound.AddRange(points);

                foreach (Point point in points)
                {
                    foreach (M.Measure measure in measuresTaken)
                    {
                        if (measure is Polar.Measure)
                        {
                            Polar.Measure m = measure as Polar.Measure;
                            if (m._Point._Name == point._Name)
                                if (m._Status is M.States.Good)
                                    pointsFound.Remove(point);
                        }
                    }
                }
                return pointsFound;
            }


        }

    }
}