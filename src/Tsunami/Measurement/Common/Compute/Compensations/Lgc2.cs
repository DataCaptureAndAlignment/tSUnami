﻿using LibVLCSharp.Shared;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text.RegularExpressions;
using System.Windows.Forms; // for open dialog
using TSU.Common.Elements;
using TSU.ENUM;
using TSU.IO.SUSoft;
using TSU.Tools.Conversions;
using TSU.Views.Message;
using static TSU.Common.Compute.Compensations.Lgc2.Input;
using static TSU.Common.Elements.Coordinates;
using static TSU.Tilt.Smart.ViewModel;
using D = TSU.Common.Dependencies;
using E = TSU.Common.Elements;
using M = TSU.Common.Measures;
using P = TSU.Preferences;
using R = TSU.Properties.Resources;
using Z = TSU.Common.Zones;

namespace TSU.Common.Compute.Compensations
{
    public partial class Lgc2
    {
        #region field props
        private static readonly double na = Tsunami2.Preferences.Values.na;

        public Strategies.List _Strategy;
        public Input _Input;
        public Output _Output;
        public bool _Runned;

        public bool useControlMeasures = false;
        public string _FileName;
        internal Strategies.GetTheRightCoordinates getTheRightCoordinates;
        internal Strategies.GetTheRightOption getTheRightOption;
        internal CoordinateSystemsTypes coordinateSystemType;



        internal static string newLine = Environment.NewLine;

        public static class Option
        {
            public const string OLOC = "*OLOC";
            public const string R2SK = "*RS2K";
            public const string SPHE = "*SPHE";
            public const string LEP = "*LEP";
            public const string SCALE = "*SCALE";
            public const string TSTN = "*TSTN";
            public const string CALA = "*CALA";
            public const string PDOR = "*PDOR";
            public const string ORIE = "*ORIE";
            public const string POLAR = "*POLAR";
            public const string PUNC = "*PUNC ";
            public const string PUNCH = "*PUNC H";
            public const string TITR = "*TITR ";
            public const string PUNCE = "*PUNC E ";
            public const string HIST = "*HIST ";
            public const string VXY = "*VXY";
            public const string ECHO = "*ECHO";
            public const string DSPT = "*DSPT";
        }

        public static class FileFormat
        {
            public const string INP = ".inp";
            public const string DAT = ".dat";
            public const string SU = ".SU";
            public const string PHYS = ".Phys";
            public const string CCS = ".CCS";
            public const string LOG = ".log";
            public const string RES = ".res";
            public const string ERR = ".err";
            public const string Custom = ".xxx";
        }

        public Polar.Station.Parameters.Setup.Values _Result
        {
            get
            {
                if (_Output != null && _Runned)
                    return _Output._Result;
                else
                    return new Polar.Station.Parameters.Setup.Values(null);
            }
        }

        #endregion

        public static bool ComputeIntersection(out List<E.Point> intersectedPoints)
        {
            List<E.Point> noCoordinatePoints;
            List<Polar.Station.Module> stationModules;
            ComputeIntersection_Analysis(out noCoordinatePoints, out stationModules);
            return ComputeIntersection_Compute(noCoordinatePoints, stationModules, out intersectedPoints);
        }

        private static bool ComputeIntersection_Compute(List<E.Point> noCoordinatePoints, List<Polar.Station.Module> stationModules, out List<E.Point> intersectedPoints)
        {
            Lgc2 lgc2 = new Lgc2();
            lgc2._Runned = false;
            lgc2._Input = new Input(lgc2, "");

            // trick to get the rigth coordinates
            {
                Polar.Compensations.FreeStation_LGC2 temp = new Polar.Compensations.FreeStation_LGC2(null);

                int countOfTreatableStation = 0;
                foreach (var item in stationModules)
                {
                    if (item is Polar.Station.Module)
                    {
                        // if (item._Station.ParametersBasic._State is Station.State.Bad) continue; we want it exporte with '%'
                        if (item._Station.MeasuresTaken.Count < 1) continue; // a station with a single measurement is not a station  common!
                        temp.CheckIfGeoidalComputeIsPossible((item as Polar.Station.Module).StationTheodolite);
                        countOfTreatableStation++;
                    }
                }

                if (countOfTreatableStation == 0)
                    throw new Exception(R.NoStationExport);

                lgc2.getTheRightCoordinates = temp.getTheRightCoordinates;
                lgc2.getTheRightOption = temp.getTheRightOption;
                lgc2.coordinateSystemType = temp.coordianteSystemType;

                if (lgc2.getTheRightCoordinates == null || lgc2.getTheRightOption == null || lgc2.coordinateSystemType == CoordinateSystemsTypes.Unknown)
                    //    throw new Exception("Tsunami doesn't known how to treat this station with LGC2");
                    throw new Exception(R.T_TSUNAMI_DOESNT_KNOWN_HOW_TO_TREAT_THIS_STATION_WITH_LGC2);
            }


            lgc2._Input.CreateForIntersection(stationModules.Cast<Station.Module>().ToList(), noCoordinatePoints);

            if (lgc2.Run(true, true))
            {
                intersectedPoints = Output.OutputJSON.GetVariablePoints(lgc2._Output);
                foreach (var item in intersectedPoints)
                {
                    item._Origin = $"Intersection from {DateTime.Now.ToString("hh-mm-ss")}";
                }
            }
            else
                intersectedPoints = new List<E.Point>();

            return lgc2._Runned;
        }

        private static void ComputeIntersection_Analysis(out List<E.Point> noCoordinatePoints, out List<Polar.Station.Module> stationModules)
        {
            // find points without coordinates
            List<E.Point> points = Tsunami2.Properties.Points;
            noCoordinatePoints = points.Where(x => !(x._Coordinates.Ccs.AreKnown || x._Coordinates.Su.AreKnown)).ToList();

            // Get Stations
            stationModules = new List<Polar.Station.Module>();
            foreach (var module in Tsunami2.Properties.MeasurementModules)
                if (module.ObservationType == Common.ObservationType.Polar)
                    stationModules.AddRange(module.StationModules.Cast<Polar.Station.Module>());

            // Count observation and remove point without 3
            List<E.Point> pointsToRemove = new List<E.Point>();
            foreach (var point in noCoordinatePoints)
            {
                int numberOfObservations = 0;
                foreach (var stationModule in stationModules)
                {
                    foreach (var measure in stationModule.StationTheodolite.MeasuresTaken.OfType<Polar.Measure>().Where(x => x._Point._Name == point._Name && x.GeodeRole != IO.SUSoft.Geode.Roles.Cala))
                    {
                        if (measure.IsFullyMeasured) // there is some point in NF that do not have coordinates but have full measures
                        {
                            pointsToRemove.Add(point);
                            break;
                        }
                        if (!measure.Distance.Corrected.IsNa) numberOfObservations++;
                        if (!measure.Angles.Corrected.Horizontal.IsNa) numberOfObservations++;
                        if (!measure.Angles.Corrected.Vertical.IsNa) numberOfObservations++;
                    }
                }
                if (numberOfObservations < 3)
                    pointsToRemove.Add(point);
            }
            noCoordinatePoints.RemoveAll(x => pointsToRemove.Contains(x));
            noCoordinatePoints = noCoordinatePoints.GroupBy(p => p._Name)
                                   .Select(g => g.First())
                                   .ToList();

            // remove useles stations
            List<Station.Module> stationToRemove = new List<Station.Module>();
            foreach (var stationModule in stationModules)
            {
                bool containAUsefullMeasure = false;
                foreach (var point in noCoordinatePoints)
                {
                    if (stationModule.StationTheodolite.MeasuresTaken.Any(x => x._Point._Name == point._Name))
                        containAUsefullMeasure = true;
                }
                if (!containAUsefullMeasure)
                    stationToRemove.Add(stationModule);
            }
            stationModules.RemoveAll(x => stationToRemove.Contains(x));
        }

        #region constructor

        public Lgc2()
        { }

        public Lgc2(string inputFileName)
        {
            _Runned = false;

            // search option
            foreach (var line in File.ReadLines(inputFileName))
            {
                string lineU = line.ToUpper();
                if (lineU.Contains(Option.OLOC))
                {
                    getTheRightOption = new Strategies.GetTheRightOption(() => Option.OLOC);
                    coordinateSystemType = CoordinateSystemsTypes._3DCartesian;
                }
                else if (lineU.Contains(Option.R2SK))
                {
                    getTheRightOption = new Strategies.GetTheRightOption(() => Option.R2SK);
                    coordinateSystemType = CoordinateSystemsTypes._2DPlusH;
                }
                else if (lineU.Contains(Option.SPHE))
                {
                    getTheRightOption = new Strategies.GetTheRightOption(() => Option.SPHE);
                    coordinateSystemType = CoordinateSystemsTypes._2DPlusH;
                }
                else if (lineU.Contains(Option.LEP))
                {
                    getTheRightOption = new Strategies.GetTheRightOption(() => Option.LEP);
                    coordinateSystemType = CoordinateSystemsTypes._2DPlusH;
                }
            }

            _Input = new Input(this, inputFileName);
        }

        public Lgc2(
            Polar.Station station,
            Strategies.Common strategy,
            Strategies.GetTheRightCoordinates getTheRightCoordinates,
            Strategies.GetTheRightOption getTheRightOption,
            CoordinateSystemsTypes coordinateSystemType,
            string fileName = "",
            string title = "",
            bool createInput = true)
        {
            if (station == null) return;

            if (title == "")
                title = $"{R.InputTSU} {Tsunami2.Properties.Version}";

            _Runned = false;

            this.getTheRightCoordinates = getTheRightCoordinates;
            this.getTheRightOption = getTheRightOption;
            this.coordinateSystemType = coordinateSystemType;


            if (station.Parameters2.Setups.InitialValues.CompensationStrategy == null)
            {
                if (station.Parameters2.Setups.InitialValues.VerticalisationState == Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation.NotVerticalised)
                    station.Parameters2.Setups.InitialValues.CompensationStrategyType = Strategies.List.NonVerticalizedSetup;
                else if (station.Parameters2.Setups.InitialValues.IsPositionKnown)
                    station.Parameters2.Setups.InitialValues.CompensationStrategyType = Strategies.List.OrientatedOnly_LGC2;
                else
                    station.Parameters2.Setups.InitialValues.CompensationStrategyType = Strategies.List.FreeStation_LGC2;
            }

            _Strategy = strategy._strategy;

            _Input = new Input(this, fileName);
            fileName = strategy.RenameFile(fileName);

            if (createInput)
                strategy.Create_LGC2_Input(station, fileName, this, title);
        }

        public Lgc2(
            Polar.Station station,
            Strategies.Common strategy,
            string fileName = "",
            string title = "Input by Tsunami",
            bool createInput = true)
        {
            _Runned = false;

            if (station == null) return;

            // trick to get the right coordinates
            Polar.Compensations.NewFrame_LGC2 temp = new Polar.Compensations.NewFrame_LGC2(null);

            temp.CheckIfGeoidalComputeIsPossible(station, temp);

            getTheRightCoordinates = temp.getTheRightCoordinates;
            getTheRightOption = temp.getTheRightOption;
            coordinateSystemType = temp.coordianteSystemType;

            _Strategy = strategy._strategy;

            strategy.CheckIfGeoidalComputeIsPossible(station, strategy);

            getTheRightCoordinates = strategy.getTheRightCoordinates;
            getTheRightOption = strategy.getTheRightOption;
            coordinateSystemType = strategy.coordianteSystemType;

            fileName = strategy.RenameFile(fileName);
            _Input = new Input(this, fileName);
            if (createInput)
                strategy.Create_LGC2_Input(station, title, this);
        }

        public Lgc2(
            List<Station.Module> stationModuleList,
            List<E.Point> pts,
            string fileName = "",
            string title = "Input by Tsunami",
            int forcedPrecision = 7,
            bool useControls = false, bool consiLibre = false)
        {
            string treatedStationForMessage = R.String_Unknown;
            try
            {
                _Runned = false;

                // trick to get the rigth coordinates
                Polar.Compensations.FreeStation_LGC2 temp = new Polar.Compensations.FreeStation_LGC2(null);

                int countOfTreatableStation = 0;
                foreach (var item in stationModuleList)
                {
                    if (item is Polar.Station.Module)
                    {
                        treatedStationForMessage = item._Name;
                        // if (item._Station.ParametersBasic._State is Station.State.Bad) continue; we want it exporte with '%'
                        if (item._Station.MeasuresTaken.Count < 1) continue; // a station with a single measurement is not a station  common!
                        temp.CheckIfGeoidalComputeIsPossible((item as Polar.Station.Module).StationTheodolite);
                        countOfTreatableStation++;
                    }
                }

                if (countOfTreatableStation == 0)
                    throw new Exception(R.NoStationExport);

                getTheRightCoordinates = temp.getTheRightCoordinates;
                getTheRightOption = temp.getTheRightOption;
                coordinateSystemType = temp.coordianteSystemType;

                if (getTheRightCoordinates == null || getTheRightOption == null || coordinateSystemType == CoordinateSystemsTypes.Unknown)
                    //    throw new Exception("Tsunami doesn't known how to treat this station with LGC2");
                    throw new Exception(R.T_TSUNAMI_DOESNT_KNOWN_HOW_TO_TREAT_THIS_STATION_WITH_LGC2);

                _Input = new Input(this, fileName);
                useControlMeasures = useControls;
                _Input.CreateFromSeveralStations(stationModuleList, pts, title, forcedPrecision, consiLibre);
            }
            catch (Exception ex)
            {
                string custom = $"Creating LGC2 for '{treatedStationForMessage}' {R.T_FAILED}\r\n{ex.Message}";
                throw new Exception($"{custom}");
            }
        }

        public Lgc2(Line.Station stationLine, string fileName = "", string title = "Input by Tsunami")
        //constructeur fichier input pour mesure fil 
        {
            _Runned = false;
            _Input = new Input(this, fileName);
            _Input.CreateAsSingleOffsetMeasurement(stationLine, fileName + "B", title);
        }

        public Lgc2(Level.Station.Module stationLevelModule, string fileName = "", string title = "Input by Tsunami")
        {
            // constructeur fichier input pour nivellement
            _Runned = false;
            _Input = new Input(this, fileName);
            _Input.CreateAsSingleLevelMeasurement(stationLevelModule, fileName, title);
        }
        /// <summary>
        /// Crèe un fichier LGC2 reprenant toutes les stations du level module
        /// </summary>
        /// <param name="LevelModule"></param>
        /// <param name="fileName"></param>
        /// <param name="title"></param>
        public Lgc2(Level.Module LevelModule, string fileName = "", string title = "Input by Tsunami")
        {
            // constructeur fichier input pour nivellement
            _Runned = false;
            _Input = new Input(this, fileName);
            _Input.CreateAsAllLevelMeasurement(LevelModule, fileName, title);
        }
        /// <summary>
        /// Cree un input LGC pour calculer les coordonnées mesurées des points sur les fils
        /// </summary>
        /// <param name="stationLineModule"></param>
        /// <param name="fileName"></param>
        /// <param name="title"></param>
        public Lgc2(Line.Station.Module stationLineModule, string fileName = "", string title = "Input by Tsunami")
        {
            string text = fileName;
            if (text == "") text += "A";
            _Runned = false;
            _Input = new Input(this, fileName);
            _Input.CreateAsWireCoordinatesCalculation(stationLineModule, text, title);
        }
        public Lgc2(Line.Module lineModule, string fileName = "", string title = "Input by Tsunami")
        {
            // constructeur fichier input pour global wire
            _Runned = false;
            _Input = new Input(this, fileName);
            _Input.CreateAsLibrAllWiresMeasurement(lineModule, fileName, title);
        }
        public Lgc2(Guided.Group.Module guidedGroupModule, GuidedModuleType guidedModuleType, string fileName = "", string title = "Input by Tsunami")
        {
            string text = fileName;
            if (text == "")
            {
                text += "A";
            }

            _Runned = false;
            _Input = new Input(this, fileName);
            switch (guidedModuleType)
            {
                case GuidedModuleType.NoType:
                    break;
                case GuidedModuleType.Alignment:
                    break;
                case GuidedModuleType.AlignmentEcartometry:
                    break;
                case GuidedModuleType.AlignmentImplantation:
                    break;
                case GuidedModuleType.AlignmentLevelling:
                    break;
                case GuidedModuleType.AlignmentManager:
                    break;
                case GuidedModuleType.AlignmentTilt:
                    break;
                case GuidedModuleType.Cheminement:
                    _Input.CreateAsCheminement(guidedGroupModule, text, title);
                    break;
                case GuidedModuleType.AlignmentLevellingCheck:
                    _Input.CreateAsAlignmentLevellingCheck(guidedGroupModule, title);
                    break;
                case GuidedModuleType.Ecartometry:
                    _Input.CreateAsLibrWire(guidedGroupModule, text, title);
                    break;
                case GuidedModuleType.Example:
                    break;
                case GuidedModuleType.Implantation:
                    break;
                case GuidedModuleType.PointLance:
                    break;
                case GuidedModuleType.PreAlignment:
                    break;
                case GuidedModuleType.Survey1Face:
                    break;
                case GuidedModuleType.Survey2Face:
                    break;
                case GuidedModuleType.SurveyTilt:
                    break;
                case GuidedModuleType.TourDHorizon:
                    break;
                default:
                    break;
            }

        }


        #endregion

        #region methods

        const int MAX_RUNNING_TIME_SEC = 5;

        public bool Run(bool showInput = false, bool showOutput = true, bool showLog = true, int maxRunningTimeSeconds = MAX_RUNNING_TIME_SEC)
        {
            string lgcVersion = "2";
            string lgcName = "LGC";

            // we have now a robust 2.8 v
            //// *CONSI LIBR requires the 2.7.0, but in other situations we use the beta with improved convergence
            //foreach (string line in File.ReadLines(_Input.FileName))
            //{
            //    if (line.ToUpper().Contains("*CONSI LIBR"))
            //    {
            //        lgcName = "LGC_NEW";
            //        break;
            //    }
            //}

            int waitTimeInmillis = maxRunningTimeSeconds * 1000;

            try
            {
                // delete old res
                File.Delete(_FileName + FileFormat.RES);
                File.Delete(_FileName + FileFormat.LOG);
                File.Delete(_FileName + FileFormat.ERR);

                //Before show, comment out all the control lines except for the last
                string inputPath = _FileName.Replace('c', 'C') + FileFormat.INP;
                ProcessFileControlLines(inputPath);

                if (showInput && !TSU.Debug.IsRunningInATest)
                    _Input.OpenEditor();
                //lance LGC2
                D.Application app = P.Preferences.Dependencies.Applications.Find(x => x._Name.ToUpper() == lgcName && x._VersionNumbers.Major == lgcVersion);
                string pathLGC = app.Path;
                
                System.Diagnostics.ProcessStartInfo cmdsi = new System.Diagnostics.ProcessStartInfo(pathLGC)
                {
                    Arguments = " -i \"" + inputPath + "\"",
                    //Pour la cacher la fenêtre application console lorsque LGC2 tourne
                    UseShellExecute = false,
                    CreateNoWindow = true
                };

                // Before run, replace -999.9 by the LHC center
                ReplaceUnknownCoordinatesByLhcCenter(inputPath);


                // RUN
                System.Diagnostics.Process cmd = System.Diagnostics.Process.Start(cmdsi);
                Tsunami2.Preferences.Values.Processes.Add(new TsuProcess() { Application = app, Process = cmd, Input = _FileName });

                if (!cmd.WaitForExit(waitTimeInmillis))
                {
                    if (!cmd.HasExited)
                    {
                        cmd.Kill();
                        cmd.Dispose();
                    }
                    throw new Exception($"{R.NotEndLGC2} {maxRunningTimeSeconds} seconds");
                }

                if (File.Exists(_FileName + FileFormat.RES))
                {
                    //vérifie si le fichier out n'est pas vide
                    if (File.ReadLines(_FileName + FileFormat.RES).Any())
                    {
                        _Runned = true;
                        _Output = new Output(this, _FileName + FileFormat.RES)
                        {
                            _ErrorFilePath = _FileName + FileFormat.ERR
                        };

                        if (showOutput && !TSU.Debug.IsRunningInATest)
                            _Output.ShowFile();
                        //this._Output.coordianteSystemType = this._Input.coordianteSystemType;
                        return true;
                    }
                }

                if (!_Runned && File.Exists(_FileName + FileFormat.LOG))
                {
                    if (showLog) // with surveypad showing the log is better by running the .inp with laucnhed plugin it the compute fails it will show the log tab
                    {
                        _Output = new Output(this)
                        {
                            _Result = new Polar.Station.Parameters.Setup.Values(null)
                        };
                        Shell.ExecutePathInDialogView(_FileName + FileFormat.INP, R.ErrLGC2, "", null, false, MessageType.Critical, launchSurveyPadPlugin: true);
                    }
                    else
                        // throw new Exception (string.Format("{0} '{1}'", "LGC2 run into a problem, see log file:", _FileName + ".log"));
                        throw new Exception($"{R.T_LGC2_RUN_INTO_A_PROBLEM_SEE_LOG_FILE}: " +
                            $"{MessageTsu.GetLink(_FileName + FileFormat.LOG)} {newLine}" +
                            $"{R.T_LGC2_SEE_INPUT} :" +
                            $"{MessageTsu.GetLink(_FileName + FileFormat.INP)}");
                }
                return false;
            }

            catch (Exception ex)
            {
                throw new Exception($"{R.RepLGC2} {ex}");
            }
        }

        public static void ProcessFileControlLines(string filePath)
        {
            var lines = File.ReadAllLines(filePath);

            var processedLines = ProcessLinesWithControl(lines);

            File.WriteAllLines(filePath, processedLines);
        }

        public static List<string> ProcessLinesWithControl(string[] lines)
        {
            List<string> processedLines = new List<string>();

            string currentBlock = string.Empty;
            List<int> controlIndexes = new List<int>();

            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains("*ANGL") || lines[i].Contains("*ZEND") || lines[i].Contains("*DIST"))
                {
                    // Process the previous block's control lines before starting a new one
                    if (controlIndexes.Count > 0)
                    {
                        // Comment all but the last "Control" line in the previous block except for the last one
                        for (int j = 0; j < controlIndexes.Count; j++)
                        {
                            if (j != controlIndexes.Count - 1)
                            {
                                string line = lines[controlIndexes[j]];

                                if (!line.StartsWith("%"))
                                {
                                    line = "%" + line.Substring(1);
                                }

                                if (!line.Contains("% Not used in compensation"))
                                {
                                    line += " % Not used in compensation";
                                }

                                processedLines[controlIndexes[j]] = line;
                            }
                        }
                    }

                    controlIndexes.Clear();
                    currentBlock = lines[i]; // Set current block
                }

                if (lines[i].Contains("Control") && !lines[i].StartsWith("%"))
                {
                    controlIndexes.Add(i);
                }

                processedLines.Add(lines[i]);
            }

            //Lines that come after the last block starts
            if (controlIndexes.Count > 0)
            {
                for (int j = 0; j < controlIndexes.Count; j++)
                {
                    if (j != controlIndexes.Count - 1)
                    {
                        string line = lines[controlIndexes[j]];

                        if (!line.StartsWith("%"))
                        {
                            line = "%" + line.Substring(1);
                        }

                        if (!line.Contains("% Not used in compensation"))
                        {
                            line += " % Not used in compensation";
                        }

                        processedLines[controlIndexes[j]] = line;
                    }
                }
            }

            return processedLines;
        }
        public static void ReplaceUnknownCoordinatesByLhcCenter(string inputPath)
        {
            try
            {
                // Read the contents of the file
                string[] lines = File.ReadAllLines(inputPath);
                bool containsOLOC = false;

                // Check if any line contains "*OLOC"
                foreach (var line in lines)
                {
                    if (line.Contains("*OLOC"))
                    {
                        containsOLOC = true;
                        break;
                    }
                }

                string pattern = @"-999\.9\s+-999\.9\s+-999\.9";

                for (int i = 0; i < lines.Length; i++)
                {
                    if (Regex.IsMatch(lines[i], pattern))
                    {
                        string replacement;
                        if (containsOLOC)
                        {
                            replacement = "6.66   6.66   6.66   % replace because was not defined";
                        }
                        else
                        {
                            string message = "";
                            var center = Lgc2.Input.GetAPointCloseToTheLhcCenter(ref message);
                            replacement = $"{center.X}   {center.Y}   {center.Z}   %Replaced by LHC center becaause was not defined";
                        }

                        // Replace instances of the pattern with the replacement string
                        lines[i] = Regex.Replace(lines[i], pattern, replacement);
                    }
                }

                // Write the updated content back to the file
                File.WriteAllLines(inputPath, lines);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");
            }
        }

        public void TreatFileName(string fileName, string folderName = "")
        {
            if (fileName == "" || fileName == "A" || fileName == "B")
            {
                _FileName = Tsunami2.Preferences.Values.Paths.MeasureOfTheDay;
                _FileName += $@"computes\(P)LGC\";
                if (folderName != "")
                    _FileName += $@"{folderName}\";
                if (!Directory.Exists(_FileName)) Directory.CreateDirectory(_FileName);
                _FileName += Date.ToDateUpSideDown(DateTime.Now);
                _FileName += fileName;
                //_FileName += ".inp";
            }
            else
            {
                _FileName = fileName;
                if (_FileName.Length >= 5)
                {
                    if (_FileName.Substring(_FileName.Length - 4, 4).ToUpper() == ".INP")
                    {
                        _FileName = _FileName.Substring(0, _FileName.Length - 4);
                    }
                }
            }

        }

        #endregion

        public static Lgc2 RunInput(string inputFilePath)
        {
            Lgc2 lgc2 = new Lgc2(inputFilePath);
            bool runned = lgc2.Run(showInput: true, showOutput: false, showLog: true, maxRunningTimeSeconds: 10 * 60);
            return lgc2;
        }

        public static bool RunInput(string inputFilePath, out string ouputFilePath, bool reRunWithEditionIfFailed = true)
        {
            Lgc2 lgc2 = new Lgc2(inputFilePath);
            bool runned = lgc2.Run(showInput: false, showOutput: false, showLog: false, maxRunningTimeSeconds: 10 * 60);
            if (!runned && reRunWithEditionIfFailed)
                runned = lgc2.Run(showInput: true, showOutput: false, showLog: true, maxRunningTimeSeconds: 10 * 60);
            ouputFilePath = lgc2._Output.OutputFilePath;

            return runned;
        }

        public static string selectedPlace = "";

        public static string CreateMessageWithLinks(string filePath, string outputFilePath, string errorFilePath)
        {
            string messageWithLinksToCreatedFiles = "";

            // create link to resulting files
            messageWithLinksToCreatedFiles += $"{R.T_LGC_FILES_IN_FOLDER}:;{MessageTsu.GetLink(selectedPlace)}.{Environment.NewLine}";
            messageWithLinksToCreatedFiles += $"LGC files:\r\n";
            messageWithLinksToCreatedFiles += $"{MessageTsu.GetLink(filePath)}{Environment.NewLine}";
            messageWithLinksToCreatedFiles += $"{MessageTsu.GetLink(outputFilePath)}{Environment.NewLine}";
            messageWithLinksToCreatedFiles += $"{MessageTsu.GetLink(errorFilePath)}{Environment.NewLine}";
            return messageWithLinksToCreatedFiles;
        }

        public static E.Composites.TheoreticalElement CreateTheoElement(Z.Zone zone)
        {
            E.Composites.TheoreticalElement theoElement = new E.Composites.TheoreticalElement()
            {
                Definition = new E.Composites.TheoreticalElement.definition()
                {
                    LocalZone = zone,
                    InitialCoordinateSystems = CoordinateSystemsTypes._2DPlusH,
                    InitialTsunamiSystems = CoordinateSystemsTsunamiTypes.CCS
                }
            };
            return theoElement;
        }

        public static void SaveFormat(ref string messageWithLinksToCreatedFiles, string typeCoord, FileInfo fileInfo, List<E.Point> points, E.Manager.Module em)
        {
            string fileNameTemp;
            fileNameTemp = fileInfo.FullName.Replace(fileInfo.Extension, typeCoord);
            switch (typeCoord)
            {
                case FileFormat.SU:
                    em.SaveInIdxyzFormat(fileNameTemp, points);
                    break;
                case FileFormat.PHYS:
                    em.SaveInIdxyzPhysicistFormat(fileNameTemp, points);
                    break;
                case FileFormat.CCS:
                    em.SaveInIdxyzCCSFormat(fileNameTemp, points);
                    break;
                case FileFormat.Custom:
                    em.SaveInIdxyzCustomFormat(fileNameTemp, points, ref fileNameTemp);
                    break;
            }
            messageWithLinksToCreatedFiles += "\t" + $"{MessageTsu.GetLink(fileNameTemp)}{Environment.NewLine}";

        }

        public static void RunInputWithZone(string filePath = "", bool showInput = true, bool exitOnSuccess = false, bool askZoneWhenMissing = true)
        {

            // need a view for the message
            Views.TsuView v = new Views.TsuView();

            try
            {
                if (filePath == "")
                {
                    // get the input
                    OpenFileDialog theDialog = new OpenFileDialog();
                    theDialog.Title = R.T_OPEN_TEXT_FILE;
                    theDialog.Filter = "lgc input|*.inp|lgc input|*.lgc|all files|*.*";
                    theDialog.InitialDirectory = selectedPlace == "" ? Tsunami2.Preferences.Values.Paths.Data : selectedPlace;

                    TsunamiView.ViewsShownAsModal.Add(theDialog);
                    var res = theDialog.ShowDialog();
                    TsunamiView.ViewsShownAsModal.Remove(theDialog);

                    if (res == DialogResult.OK)
                    {
                        filePath = theDialog.FileName;
                    }
                    else
                        return;
                }

                FileInfo fileInfo = new FileInfo(filePath);

                // hidden option to allow the increase of the sigma
                IncreaseSigma(fileInfo);
                selectedPlace = fileInfo.DirectoryName;
                Lgc2 lgc2 = new Lgc2(filePath);
                try
                {
                    
                    bool runned = lgc2.Run(showInput: showInput, showOutput: false, showLog: true, maxRunningTimeSeconds: 10 * 60);

                    if (runned)
                    {
                        string messageWithLinksToCreatedFiles = CreateMessageWithLinks(filePath, lgc2._Output.OutputFilePath, lgc2._Output._ErrorFilePath);

                        List<E.Point> points = new List<E.Point>();

                        string jsonFile = Path.ChangeExtension(fileInfo.FullName, ".json");
                        if (!File.Exists(jsonFile))
                            throw new TsuException("Missing *JSON in the LGC2 input file");

                        points.AddRange(Output.OutputJSON.GetVariablePoints(lgc2._Output));
                        points.AddRange(lgc2._Input.CalaPointsFromFile);

                        // propose a zone if not already available
                        Z.Zone zone = Tsunami2.Properties.Zone;
                        if (zone == null && askZoneWhenMissing)
                            zone = Z.Zone.FixDefinition(v, fileInfo);

                        // export in different formats
                        E.Manager.Module em = new E.Manager.Module();
                        if (zone != null)
                        {
                            E.Composites.TheoreticalElement t = CreateTheoElement(zone);

                            foreach (var item in points)
                            {
                                t.Elements.Add(item);
                            }

                            CoordinateSystemsTypes cst = GetCoordinatesSystemsTypeBasedOn(Output.OutputJSON.GetReferenceSurface(lgc2._Output));
                            if (cst == CoordinateSystemsTypes._3DCartesian)
                                t.Definition.InitialReferenceFrames = CorrespondingReferenceFrame(CoordinateSystemsTsunamiTypes.SU, ReferenceSurfaces.RS2K);
                            else
                                t.Definition.InitialReferenceFrames = CorrespondingReferenceFrame(CoordinateSystemsTsunamiTypes.CCS, zone.Ccs2MlaInfo.ReferenceSurface);

                            E.Manager.Module.TryToFillMissingCoordinates(t);
                        }
                        else
                        { // sinon on export que en ccs.dat si c'est pas un calcul local.
                          //if (lgc2._Output._Result.ReferenceSurface != Option.OLOC)
                            if (lgc2.coordinateSystemType == CoordinateSystemsTypes._2DPlusH)
                            {
                                string fileNameTemp;

                                Transformation.Systems.HtoZ(points, ReferenceSurfaces.RS2K);
                                Transformation.Systems.SuOrCcs2Customs(points);
                            }
                        }
                        string filePathWithoutExtension = Path.Combine(fileInfo.DirectoryName, Path.GetFileNameWithoutExtension(fileInfo.Name));
                        Elements.Manager.Module.ExportToTextAndGetPaths(points, out string message, out _, filePathWithoutExtension);
                        messageWithLinksToCreatedFiles += message;

                        // show message of succes
                        string titleAndMessage = $"{R.T_COMPENSATION} {R.T_IS_SUCCESSFUL};{messageWithLinksToCreatedFiles}";
                        MessageInput mi = new MessageInput(MessageType.GoodNews, titleAndMessage)
                        {
                            ButtonTexts = new List<string> { R.T_OK, R.RunAgain }
                        };
                        string result = mi.Show().TextOfButtonClicked;
                        if (R.RunAgain == result)
                        {
                            IO.Backup.Create(filePath, triggerMessage: R.CompSuccess);
                            RunInputWithZone(filePath);
                        }
                        else if (exitOnSuccess)
                            Tsunami2.Properties.Quit(skipWarning: true);
                    }
                    else
                    {
                        string messageWithLinksToCreatedFiles = "";
                        messageWithLinksToCreatedFiles += $"{R.T_LGC_FILES_IN_FOLDER} {MessageTsu.GetLink(selectedPlace)} :{Environment.NewLine}";
                        messageWithLinksToCreatedFiles += "\t" + MessageTsu.GetLink(lgc2._FileName + FileFormat.LOG) + Environment.NewLine;
                        throw new TsuException(messageWithLinksToCreatedFiles);
                    }
                }
                catch (TsuException ex)
                {
                    string runAgain = R.T_RUN_AGAIN;
                    string titleAndMessage = $"{R.T_COMPENSATION} {R.T_FAILED};{ex.Message}";
                    MessageInput mi = new MessageInput(MessageType.Critical, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_OK, runAgain }
                    };
                    if (runAgain ==
                        mi.Show().TextOfButtonClicked)
                    {
                        IO.Backup.Create(filePath, triggerMessage: R.CompFailed);
                        RunInputWithZone(filePath, askZoneWhenMissing: askZoneWhenMissing);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
            catch (Exception ex)
            {
                string runAgain = R.T_RUN_AGAIN;
                if (runAgain == v.ShowMessageOfBug($"{R.T_MY_LGC_HURTS}!;", ex, runAgain, R.T_I_GIVE_UP))
                {
                    RunInputWithZone();
                }
            }
        }

        private static void IncreaseSigma(FileInfo fileInfo)
        {
            try
            {
                string key = "OBSEX";
                if (!fileInfo.Name.ToUpper().Contains("OBSEX")) return;
                int posFactor = fileInfo.Name.ToUpper().IndexOf(key);

                string sFactor = fileInfo.Name.ToUpper().Substring(posFactor + key.Length, 1);
                int iFactor = 1;
                if (int.TryParse(sFactor, out iFactor))
                {
                    // rename the OBSE from the *polar
                    bool polarFound = false;
                    bool polarDone = false;

                    string newContent = "";

                    string comment = "  % Sigma as been increased by Tsunami to find outliers.";

                    foreach (var item in File.ReadLines(fileInfo.FullName))
                    {
                        string newLine = item;

                        if (!polarFound && item.ToUpper().Contains(Option.POLAR))
                        {
                            polarFound = true;
                            newContent += newLine + Environment.NewLine;
                            continue;
                        }

                        // prismes lines
                        if (!polarDone && polarFound)
                        {
                            string[] parts = item.Split(new[] { '\t', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            if (parts.Length == 11)
                            {

                                double sH, sV, sD;
                                if (double.TryParse(parts[1], out sH))
                                {
                                    parts[1] = (sH * iFactor).ToString();
                                    if (double.TryParse(parts[2], out sV))
                                    {
                                        parts[2] = (sV * iFactor).ToString();
                                        if (double.TryParse(parts[3], out sD))
                                        {
                                            parts[3] = (sD * iFactor).ToString();
                                            newLine = "         ";
                                            foreach (var part in parts)
                                            {
                                                newLine += part + "\t";
                                            }
                                        }
                                    }
                                    // newLine += comment;
                                }
                            }
                            if (item.Trim().Length > 1 && item.Trim()[0] == '*')
                                polarDone = true;
                        }

                        // obs lines
                        if (polarDone && item.ToUpper().Contains("OBSE"))
                        {
                            newLine = "          ";

                            string[] parts = item.Split(new[] { '\t', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            bool nextIsTheValue = false;
                            foreach (var part in parts)
                            {
                                if (nextIsTheValue)
                                {
                                    double sO;
                                    if (double.TryParse(part, out sO))
                                    {
                                        sO *= iFactor;
                                        newLine += sO + "\t";
                                    }
                                }
                                else
                                    newLine += part + "\t";
                                nextIsTheValue = part.ToUpper() == "OBSE";

                            }
                            newLine += comment;
                        }

                        newContent += newLine + Environment.NewLine;
                    }
                    File.WriteAllLines(fileInfo.FullName, newContent.Split(new[] { Environment.NewLine }, StringSplitOptions.None).ToArray());

                }

            }
            catch (Exception ex)
            {
                Logs.Log.AddEntryAsFYI(null, $"Error when trying to increase sigmas;{ex.Message}");
            }
        }

        internal static class Restore
        {
            public static string RestoreLgcDirectory(Polar.Station.Parameters.Setup.Values psv)
            {
                //Try the saved path
                string shortInput = psv.Lgc2ProjectPath + ".inp";

                if (File.Exists(shortInput))
                    return shortInput;

                //Try the User Validated Setup path
                string validatedInput = Path.Combine(
                    Path.GetDirectoryName(shortInput),
                    "User Validated Setup",
                    Path.GetFileName(shortInput));

                if (File.Exists(validatedInput))
                    return validatedInput;

                //Check in the zipped computes; if the files are there, we extract them
                string inputpath = CheckZips(shortInput, validatedInput);
                if (inputpath != null)
                    return inputpath;

                //Restore the values from the XML
                inputpath = Tsunami2.Preferences.Values.Paths.Temporary + "temp.inp";
                string outputpath = Tsunami2.Preferences.Values.Paths.Temporary + "temp.res";
                File.WriteAllText(inputpath, psv.LgcInputContent);
                File.WriteAllText(outputpath, psv.LgcOutputContent);
                return inputpath;
            }

            private static Regex zipFormat = null;

            private static List<string> ListComputeZips(string measureOfTheDay)
            {
                //Compute the regex on first call only
                if (zipFormat is null)
                    zipFormat = new Regex(@"\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}, Computes.zip");

                List<string> files = Directory.EnumerateFiles(measureOfTheDay, "*Computes.zip").ToList();
                Debug.WriteInConsole($"ListComputeZips Step 1 found {files.Count} matching zips : ");
                // Filter the files from older versions of tsunami
                files = files.Where(fileName => zipFormat.IsMatch(Path.GetFileName(fileName))).ToList();
                Debug.WriteInConsole($"ListComputeZips Step 2 found {files.Count} matching zips : ");
                //Sort them by descending date
                files.Sort();
                files.Reverse();
                return files;
            }

            private static string CheckZips(string shortInput, string validatedInput)
            {
                // Useful paths
                string measureOfTheDay = Tsunami2.Preferences.Values.Paths.MeasureOfTheDay;
                string extractDir = Path.Combine(measureOfTheDay, "computes");

                //List the zips; finish if there are none
                List<string> files = ListComputeZips(measureOfTheDay);
                if (files.Count == 0)
                    return null;

                // Regexes to check if the entries are useful
                Regex shortInputRegex = new Regex(Regex.Escape(shortInput.Substring(0, shortInput.Length - 4)) + @"\..*");
                Regex validatedInputRegex = new Regex(Regex.Escape(validatedInput.Substring(0, validatedInput.Length - 4)) + @"\..*");

                bool fileFound = false;
                foreach (string file in files)
                {
                    Debug.WriteInConsole("CheckZips found matching zip : " + file);
                    using (Ionic.Zip.ZipFile zf = new Ionic.Zip.ZipFile(file))
                    {
                        foreach (Ionic.Zip.ZipEntry entry in zf)
                        {
                            string extractPath = Path.Combine(extractDir, entry.FileName.Replace('/', '\\'));
                            if (shortInputRegex.IsMatch(extractPath) || validatedInputRegex.IsMatch(extractPath))
                            {
                                Debug.WriteInConsole("CheckZips found matching entry :" + extractPath);
                                entry.Extract(extractDir);
                                fileFound = true;
                            }
                        }
                    }

                    if (fileFound)
                    {
                        if (File.Exists(shortInput))
                            return shortInput;

                        if (File.Exists(validatedInput))
                            return validatedInput;

                        //Just in case we have found files but not the inp, continue
                        fileFound = false;
                    }
                }

                return null;
            }
        }

    }
}