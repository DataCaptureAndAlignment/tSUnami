﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.RegularExpressions;
using TSU.Common.Elements;
using TSU.Common.Measures;
using TSU.ENUM;
using TSU.IO.SUSoft;
using TSU.Tools.Conversions;
using static TSU.Common.Elements.Coordinates;
using M = TSU.Common.Measures;
using P = TSU.Preferences;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Common.Compute.Compensations
{
    public partial class Lgc2
    {
        public class Output
        {
            internal Lgc2 lgc2;
            public List<string> _TextOutput;
            private readonly string outputFilePath;

            public string OutputFilePath
            {
                get
                {
                    if (outputFilePath != null)
                        return outputFilePath;
                    if (lgc2 != null && lgc2._FileName != "")
                        return lgc2._FileName + ".res";
                    return "";
                }
            }
            public string _ErrorFilePath;
            public string LogPath
            {
                get
                {
                    return lgc2._FileName + FileFormat.LOG;
                }
            }

            public Polar.Station.Parameters.Setup.Values _Result { get; set; }

            public Output()
            { }

            public Output(string outputFilePath)
            {
                this.outputFilePath = outputFilePath;
                // checks
                if (!File.Exists(outputFilePath)) throw new Exception(R.String_LGC2_MissingFile);
                // Read the file without the white lines
                _TextOutput = File.ReadLines(outputFilePath)
                                  .Where(arg => !string.IsNullOrWhiteSpace(arg))
                                  .ToList();
            }

            public Output(Lgc2 lgc2, string outputFilePath)
                : this(outputFilePath)
            {
                this.lgc2 = lgc2;
            }

            public void ShowFile()
            {
                // we use the input + automatic compute in survey pad to show the ouput
                Shell.ExecutePathInDialogView(this.outputFilePath.ToUpper().Replace(".RES", ".inp"), "LGC2, compensation output", launchSurveyPadPlugin: true);
            }

            internal void EditLGC2LogFile()
            {
                //Shell.RunInNotepad(_FileName + ".inp", true);
                Shell.ExecutePathInDialogView(lgc2._FileName + FileFormat.LOG, "LGC2 Log file");
            }

            public Output(Lgc2 lgc2)
            {
                this.lgc2 = lgc2;
            }

            #region RES parser 

            public static class OutputRES
            {
                #region Polar Module
                //public static void SetHeader(ref List<string> lines, ref List<string> temps, Output output, Lgc2 lgc2)
                //{
                //    output._Result = new Polar.Station.Parameters.Setup.Values(null)
                //    {
                //        Version = Parser.Header(ref lines, ref temps)
                //    };

                //    if (lgc2 != null)
                //        output._Result.CoordinatesSystemsUsedForCompute = lgc2.coordinateSystemType;
                //}

                //public static void SetSigma0(ref List<string> lines, ref List<string> temps, Output output, Lgc2 lgc2)
                //{

                //    output._Result.SigmaZero = Parser.Sigma0(ref temps);
                //    if (lgc2.getTheRightOption != null)
                //        output._Result.ReferenceSurface = lgc2.getTheRightOption();
                //}

                //public static void SetInstrumentProperties(ref List<string> lines, ref List<string> temps, Output output, Lgc2 lgc2)
                //{
                //    string KeyPhrase2;
                //    string ANGLES_HORIZONTAUX = "ANGLES HORIZONTAUX";
                //    string ANGLES_VERTICAUX = "DISTANCES ZENITHALES";
                //    string DISTANCES_MEASURED = "DISTANCES MESUREES";
                //    string END_OF_FILE = "*** FIN DE FICHIER ***";

                //    // Look for  instrument properties ----------------------------------

                //    if (lgc2._Input.expectSomeAH)
                //        KeyPhrase2 = ANGLES_HORIZONTAUX;
                //    else
                //    {
                //        if (lgc2._Input.expectSomeAV)
                //            KeyPhrase2 = ANGLES_VERTICAUX;
                //        else
                //        {
                //            if (lgc2._Input.expectSomeD)
                //                KeyPhrase2 = DISTANCES_MEASURED;
                //            else
                //                KeyPhrase2 = END_OF_FILE;
                //        }
                //    }

                //    (double hValue, double hSigma, double v0Value, double v0Sigma) = Parser.InstrumentProperties(ref lines, ref temps, KeyPhrase2);
                //    // Height
                //    output._Result.InstrumentHeight = new DoubleValue();
                //    output._Result.InstrumentHeight.Value = hValue;
                //    output._Result.InstrumentHeight.Sigma = hSigma;

                //    // Vzero
                //    output._Result.vZero = new DoubleValue();
                //    output._Result.vZero.Value = v0Value;
                //    output._Result.vZero.Sigma = v0Sigma;

                //}

                //public static void SetAH(ref List<string> lines, ref List<string> temps, Output output, Lgc2 lgc2)

                //{

                //    string ANGLES_VERTICAUX = "DISTANCES ZENITHALES";
                //    string DISTANCES_MEASURED = "DISTANCES MESUREES";
                //    string END_OF_FILE = "*** FIN DE FICHIER ***";
                //    string KeyPhrase2 = "";

                //    // Look for AH -----------------------------------------------------
                //    if (lgc2._Input.expectSomeAH)
                //    {
                //        if (lgc2._Input.expectSomeAV)
                //        {
                //            KeyPhrase2 = ANGLES_VERTICAUX;
                //        }
                //        else
                //        {
                //            if (lgc2._Input.expectSomeD)
                //            {
                //                KeyPhrase2 = DISTANCES_MEASURED;
                //            }
                //            else
                //            {
                //                KeyPhrase2 = END_OF_FILE;
                //            }
                //        }

                //    }

                //    output._Result.Measures = Parser.AH(ref lines, ref temps, KeyPhrase2);

                //}

                //public static void SetAV(ref List<string> lines, ref List<string> temps, Output output, Lgc2 lgc2, out bool noDistanceMesurees)
                //{
                //    string KeyPhrase2 = "";

                //    string ANGLES_VERTICAUX = "DISTANCES ZENITHALES";
                //    string DISTANCES_MEASURED = "DISTANCES MESUREES";
                //    string END_OF_FILE = "*** FIN DE FICHIER ***";

                //    // Look for AV -----------------------------------------------------
                //    noDistanceMesurees = false; // for now
                //    if (lgc2._Input.expectSomeAV)
                //    {
                //        if (lgc2._Input.expectSomeD)
                //        {
                //            KeyPhrase2 = DISTANCES_MEASURED;
                //        }
                //        else
                //        {
                //            KeyPhrase2 = END_OF_FILE;
                //        }

                //        List<Polar.Measure> measures = output._Result.Measures;
                //        Parser.AV(ref lines, ref temps, KeyPhrase2, ref measures, noDistanceMesurees);
                //    }
                //}

                //public static void SetD(ref List<string> lines, ref List<string> temps, Output output, Lgc2 lgc2, bool noDistanceMesurees)
                //{
                //    if (lgc2._Input.expectSomeD)
                //    {
                //        List<Polar.Measure> measures = output._Result.Measures;
                //        Parser.D(ref lines, ref temps, ref measures, noDistanceMesurees);
                //    }
                //}

                //public static void SetVariables(ref List<string> lines, ref List<string> temps, Output output, Lgc2 lgc2)
                //{
                //    output._Result.VariablePoints = new List<Point>(); // create the list before to avoid a null                
                //    output._Result.VariablePoints = Parser.VariablePoints(ref lines, ref temps, output._Result.CoordinatesSystemsUsedForCompute);
                //}

                #endregion

                #region Levelling

                //lit la hauteur station calculée et ensuite calcule les résidus, emq et lect theo comme pour SFB
                //public static void SetLevelResults(List<string> lines, Level.Station.Module stationLevelModule)
                //{
                //    var points = Parser.LevelResults(ref lines);
                //    foreach (KeyValuePair<string, double> item in points)
                //    {
                //        if (stationLevelModule._WorkingStationLevel._Name == item.Key)
                //        {
                //            if (item.Value != na)
                //            {
                //                stationLevelModule._WorkingStationLevel._Parameters._HStation = item.Value;
                //            }
                //        }
                //        stationLevelModule.TheoReadingAndResidualsCalculation();
                //    }

                //}
                #endregion

                #region Wire

                //public static void GetWireDisplacement(List<string> lines, Line.Station stationLine)
                ////Cherche dans les points variables les déplacements à faire et met à jour les théoretical deviation dans la station line
                //{

                //    var points = Parser.WireDisplacement(ref lines);
                //    foreach (KeyValuePair<string, double> measOffset in points)
                //    {
                //        string name = measOffset.Key;
                //        double moveToDo = measOffset.Value;
                //        if (stationLine._Anchor1._Point._Name == name && moveToDo != na)
                //        {
                //            SetDeviation(stationLine._Anchor1, moveToDo);
                //        }
                //        if (stationLine._Anchor2._Point._Name == name && moveToDo != na)
                //        {
                //            SetDeviation(stationLine._Anchor2, moveToDo);
                //        }
                //        foreach (MeasureOfOffset measureOffset in stationLine._MeasureOffsets)
                //        {
                //            if (measureOffset._Point._Name == name)
                //            {
                //                if (moveToDo != na && measureOffset._RawReading != na && measureOffset._IsFakeRawReading == false)
                //                {
                //                    SetDeviation(measureOffset, moveToDo);
                //                }
                //                else
                //                {
                //                    ///Pour calcul des lectures theo
                //                    if (measureOffset._IsFakeRawReading == true)
                //                    {
                //                        measureOffset._RawReading = na;
                //                        measureOffset._IsFakeRawReading = false;

                //                        if (moveToDo != na)
                //                        {
                //                            measureOffset._AverageTheoreticalReading = measureOffset._CorrectedReading + moveToDo / 1000;
                //                            measureOffset._TheoreticalDeviation = -moveToDo / 1000;
                //                            measureOffset._AverageDeviation = measureOffset._TheoreticalDeviation;
                //                        }
                //                    }

                //                }
                //            }
                //        }
                //    }
                //}

                #endregion
            }

            public static class Parser
            {
                public static int nameIndex = 1;
                public static int rawValueIndex = 2;
                public static int rawSigmaIndex = 3;
                public static int corrValueIndex = 4;
                public static int corrSigmaIndex = 5;
                public static int refConstIndex = 8;
                public static int refNameIndex = 10;
                public static int extNameIndex = 11;
                public static int divisorMM = 1000;
                public static int divisorCC = 10000;

                #region Polar Module
                public static List<Polar.Measure> AH(ref List<string> lines, ref List<string> temps, string endPhrase)
                {
                    string beginPhrase = "ANGLES HORIZONTAUX";
                    int lineToSkip = 3;

                    // find _AnglResults
                    temps = GetLinesBetween2KeyWords(ref lines, beginPhrase, endPhrase);
                    temps = temps.Skip(lineToSkip).ToList();

                    List<Polar.Measure> angles = new List<Polar.Measure>();
                    foreach (string item in temps)
                    {
                        Polar.Measure m = new Polar.Measure();
                        m._Point = new Point
                        {
                            _Name = GetTokenFromLine(item, nameIndex),
                            StorageStatus = Tsunami.StorageStatusTypes.Temp
                        }; // this will set the point in the global temporary list of tsunai points Properties.TempPoints
                        m.Angles.raw.Horizontal.Value = Numbers.ToDouble(GetTokenFromLine(item, rawValueIndex)); // used to store for observed values
                        m.Angles.raw.Horizontal.SigmaFromString(GetTokenFromLine(item, rawSigmaIndex), divisorCC); // used to store for observed values
                        m.Angles.corrected.Horizontal.Value = Numbers.ToDouble(GetTokenFromLine(item, corrValueIndex)); // used to store for computed values
                        m.Angles.corrected.Horizontal.SigmaFromString(GetTokenFromLine(item, corrSigmaIndex), divisorCC); // used to store for computed values residual
                        angles.Add(m);
                    }
                    return angles;

                }

                public static void AV(ref List<string> lines, ref List<string> temps, string endPhrase, ref List<Polar.Measure> measures, bool noDistanceMesurees)
                {
                    string EndOfFile = "*** FIN DE FICHIER ***";
                    string beginPhrase = "DISTANCES ZENITHALES";
                    int lineToSkip = 3;

                    // find _ZendResults
                    temps = GetLinesBetween2KeyWords(ref lines, beginPhrase, endPhrase);
                    temps = temps.Skip(lineToSkip).ToList();

                    int existingMeasureCount = 0; // use to insert a measure with no AH i nthe right order as it is not detect when reading the AH
                    foreach (string item in temps)
                    {
                        if (item == EndOfFile)
                        {
                            noDistanceMesurees = true;
                        }
                        else
                        {
                            Polar.Measure m = Analysis.Measure.FindByNameAndVerticalValue(measures, GetTokenFromLine(item, nameIndex), na);
                            if (m == null)
                            {
                                m = new Polar.Measure();
                                m._Point = new Point
                                {
                                    _Name = GetTokenFromLine(item, nameIndex),
                                    StorageStatus = Tsunami.StorageStatusTypes.Temp
                                }; // this will set the point in the global temporary list of tsunai points Properties.TempPoints
                                measures.Insert(existingMeasureCount, m);
                            }
                            m.Angles.raw.Vertical.Value = Numbers.ToDouble(GetTokenFromLine(item, rawValueIndex));
                            m.Angles.raw.Vertical.SigmaFromString(GetTokenFromLine(item, rawSigmaIndex), divisorCC);
                            m.Angles.corrected.Vertical.Value = Numbers.ToDouble(GetTokenFromLine(item, corrValueIndex));
                            m.Angles.corrected.Vertical.SigmaFromString(GetTokenFromLine(item, corrSigmaIndex), divisorCC);
                        }
                        existingMeasureCount++;
                    }
                }

                public static void D(ref List<string> lines, ref List<string> temps, ref List<Polar.Measure> measures, bool noDistanceMesurees)
                {
                    string beginPhrase = "DISTANCES MESUREES";
                    string endPhrase = "*** FIN DE FICHIER ***";
                    string contrPhrase = "CONTRAINTES";
                    int lineToSkip = 3;

                    if (noDistanceMesurees) return;

                    // find _DistResults
                    temps = GetLinesBetween2KeyWords(ref lines, beginPhrase, endPhrase);
                    temps = temps.Skip(lineToSkip).ToList();
                    foreach (string item in temps)
                    {
                        if (item.ToUpper().Contains(contrPhrase)) return;
                        Polar.Measure m = measures.Find(x => x._PointName == GetTokenFromLine(item, nameIndex) && x.Distance.Raw.Value == na);
                        if (m == null)
                        {
                            m = new Polar.Measure();
                            m._Point = new Point
                            {
                                _Name = GetTokenFromLine(item, nameIndex),
                                StorageStatus = Tsunami.StorageStatusTypes.Temp
                            }; // this will set the point in the global temporary list of tsunai points Properties.TempPoints
                            measures.Add(m);
                        }
                        m.Distance.Raw.Value = Numbers.ToDouble(GetTokenFromLine(item, rawValueIndex));
                        m.Distance.Raw.SigmaFromString(GetTokenFromLine(item, rawSigmaIndex), divisorMM);
                        m.Distance.Corrected = new DoubleValue (Numbers.ToDouble(GetTokenFromLine(item, corrValueIndex)));
                        m.Distance.Corrected.SigmaFromString(GetTokenFromLine(item, corrSigmaIndex), divisorMM);
                        //c._Sensibility = Convert.ToDouble(GetTokenFromLine(item, 6));
                        //c._ResBySigma = Convert.ToDouble(GetTokenFromLine(item, 7));
                        m.Distance.Reflector.Constante = Numbers.ToDouble(GetTokenFromLine(item, refConstIndex));
                        //c._Constante.Sigma = Convert.ToDouble(GetTokenFromLine(item, 9));
                        m.Distance.Reflector._Name = GetTokenFromLine(item, refNameIndex);
                        m.Extension.Value = Numbers.ToDouble(GetTokenFromLine(item, extNameIndex));
                    }

                }

                public static string Header(ref List<string> lines, ref List<string> temps)
                {
                    // Find Header with Version in file
                    string beginPhrase = "LGC2";
                    string endPhrase = "FRAME	ROOT  ID";
                    temps = GetLinesBetween2KeyWords(ref lines, beginPhrase, endPhrase);
                    string update = GetTokenFromLine(temps[0], 2);
                    update = update.Remove(update.Length - 1, 1);
                    string version = GetTokenFromLine(temps[0], 1) + " " + update;
                    return version;
                }

                public static (double, double, double, double) InstrumentProperties(ref List<string> lines, ref List<string> temps, string endPhrase)
                {

                    string beginPhrase = "INSTRUMENT POLAIRE: ";
                    // find Management.Instrument height
                    temps = GetLinesBetween2KeyWords(ref lines, beginPhrase, endPhrase);

                    // find ht
                    double hValue = Numbers.ToDouble(GetTokenFromLine(temps[1], 5));
                    double hSigma = Numbers.ToDouble(GetTokenFromLine(temps[1], 8)) / divisorMM;

                    // find Vzero
                    double v0Value = Numbers.ToDouble(GetTokenFromLine(temps[2], 7));
                    double v0Sigma = Numbers.ToDouble(GetTokenFromLine(temps[2], 10)) / divisorCC;
                    return (hValue, hSigma, v0Value, v0Sigma);

                }

                //internal static void NonVerticalized(ref List<string> lines, ref List<string> temps, Output output, Lgc2 lgc2)
                //{
                //    int lineToSkip = 7;
                //    output._Result.Measures = new List<Polar.Measure>();
                //    temps = temps.Skip(lineToSkip).ToList();
                //    foreach (var item in temps)
                //    {
                //        Polar.Measure m = new Polar.Measure();
                //        m._Point._Name = GetTokenFromLine(item, nameIndex);
                //        output._Result.Measures.Add(m);

                //        m.Angles.Raw.Horizontal.Value = Numbers.ToDouble(GetTokenFromLine(item, 3));
                //        m.Angles.Raw.Horizontal.SigmaFromString(GetTokenFromLine(item, 4), 10000);
                //        m.Angles.Corrected.Horizontal.Value = Numbers.ToDouble(GetTokenFromLine(item, 5));
                //        m.Angles.Corrected.Horizontal.SigmaFromString(GetTokenFromLine(item, 6), 10000);

                //        m.Angles.Raw.Vertical.Value = Numbers.ToDouble(GetTokenFromLine(item, 8));
                //        m.Angles.Raw.Vertical.SigmaFromString(GetTokenFromLine(item, 9), 10000);
                //        m.Angles.Corrected.Vertical.Value = Numbers.ToDouble(GetTokenFromLine(item, 10));
                //        m.Angles.Corrected.Vertical.SigmaFromString(GetTokenFromLine(item, 11), 10000);

                //        m.Distance.Raw.Value = Numbers.ToDouble(GetTokenFromLine(item, 13));
                //        m.Distance.Raw.SigmaFromString(GetTokenFromLine(item, 14), 1000);
                //        m.Distance.Corrected = new DoubleValue(Numbers.ToDouble(GetTokenFromLine(item, 15)));
                //        m.Distance.Corrected.SigmaFromString(GetTokenFromLine(item, 16), 1000);

                //        m.Distance.Reflector.Constante = Numbers.ToDouble(GetTokenFromLine(item, 22));

                //        m.Distance.Reflector._Name = GetTokenFromLine(item, 25);
                //        m.Extension.Value = Numbers.ToDouble(GetTokenFromLine(item, 26));
                //    }
                //}

                public static double Sigma0(ref List<string> temps)
                {
                    string beginPhrase = "SIGMA ZERO";
                    string endPhrase = "LES ECARTS-TYPES";
                    temps = GetLinesBetween2KeyWords(ref temps, beginPhrase, endPhrase);
                    string results = GetTokenFromLine(temps[0], 5);
                    results = results.Substring(1, results.Length - 2);
                    return Numbers.ToDouble(results);

                }

                public static ReferenceSurfaces GetReferenceSurface(string referenciel, ref int j)
                {
                    ReferenceSurfaces surf;
                    switch (referenciel)
                    {
                        case "OLOC":
                            surf = ReferenceSurfaces.Plane;
                            j = 0;
                            break;
                        case "RS2K":
                            surf = ReferenceSurfaces.RS2K;
                            j = 1; break;
                        case "SPHER":
                            surf = ReferenceSurfaces.Sphere;
                            j = 1;
                            break;
                        default:
                            surf = ReferenceSurfaces.Unknown;
                            j = 1; break;
                    }
                    return surf;
                }

                public static List<Point> VariablePoints(ref List<string> lines, ref List<string> temps, CoordinateSystemsTypes coordType)
                {
                    // Find variable point
                    string beginPhrase = "^POINTS VARIABLES";
                    string endPhrase = "SFP = Sub-Frame Point";
                    int lineToSkip = 3;
                    List<Point> variables = new List<Point>();

                    temps = GetLinesBetween2KeyWords(ref lines, beginPhrase, endPhrase);
                    if (temps != null)
                    {
                        int j = 0;
                        ReferenceSurfaces surf = GetReferenceSurface(GetTokenFromLine(temps[0], 10), ref j);
                        temps = temps.Skip(lineToSkip).ToList();

                        foreach (var item in temps)
                        {
                            if (!double.TryParse(GetTokenFromLine(item, 2), out double n)) // skip line that doesnt have a number as field #2
                                continue;

                            bool willbeCCS = surf != ReferenceSurfaces.Plane;

                            Point p = new Point();
                            Coordinates c = new Coordinates();
                            c.Set();
                            //c.ReferenceSurface = surf;
                            c.CoordinateSystem = coordType;
                            c._Name = GetTokenFromLine(item, 1);
                            c.X.ValueFromString(GetTokenFromLine(item, 2));
                            c.Y.ValueFromString(GetTokenFromLine(item, 3));
                            c.Z.ValueFromString(GetTokenFromLine(item, 4));
                            //if (j == 1) c.Z.ValueFromString(GetTokenFromLine(item, 5)); // when IN JSON will be work H - uncommennt!!!!
                            int i = 5 + j;
                            c.X.SigmaFromString(GetTokenFromLine(item, i), divisorMM);
                            c.Y.SigmaFromString(GetTokenFromLine(item, i + 1), divisorMM);
                            c.Z.SigmaFromString(GetTokenFromLine(item, i + 2), divisorMM); // nosH available we take the sZ

                            if (surf == ReferenceSurfaces.Plane)
                            {
                                p._Coordinates.Local = c;
                            }
                            else
                            {
                                p._Coordinates.Ccs = c;
                            }
                            p._Name = c._Name;
                            variables.Add(p);
                        }
                    }
                    return variables;
                }

                #endregion
                #region Levelling
                public static Dictionary<string, double> LevelResults(ref List<string> lines)
                {
                    string beginPhrase = "POINTS VARIABLES EN Z UNIQUEMENT   (NB.";
                    string endPhrase = "SFP = Sub-Frame Point";
                    int lineToSkip = 3;
                    Dictionary<string, double> points = new Dictionary<string, double>();
                    List<string> fileVarCoord = GetLinesBetween2KeyWords(ref lines, beginPhrase, endPhrase, true);
                    fileVarCoord = fileVarCoord.Skip(lineToSkip).ToList();

                    foreach (string stringLine in fileVarCoord)
                    {
                        if (!string.IsNullOrEmpty(stringLine))
                        {
                            string name = GetTokenFromLine(stringLine, nameIndex);
                            double hStation = Numbers.ToDouble(GetTokenFromLine(stringLine, 4), false, na);
                            points.Add(name, hStation);
                        }
                    }
                    return points;
                }
                #endregion

                #region Wire

                //public static Dictionary<string, double> WireDisplacement(ref List<string> lines)
                //{
                //    int lineToSkip = 3;
                //    string beginPhrase = "POINTS INVARIABLES EN Z REFERENTIEL = OLOC";
                //    string endPhrase = "SFP = Sub-Frame Point";
                //    List<string> fileVarCoord = GetLinesBetween2KeyWords(ref lines, beginPhrase, endPhrase, true);
                //    fileVarCoord = fileVarCoord.Skip(lineToSkip).ToList();
                //    Dictionary<string, double> points = new Dictionary<string, double>();

                //    if (fileVarCoord != null)
                //    {
                //        foreach (string stringLine in fileVarCoord)
                //        {
                //            if (!string.IsNullOrEmpty(stringLine))
                //            {
                //                string name = GetTokenFromLine(stringLine, 1);
                //                double moveToDo = Numbers.ToDouble(GetTokenFromLine(stringLine, 7), false, na);
                //                points.Add(name, moveToDo);
                //                TSU.Debug.WriteInConsole("res" + name + " " + moveToDo);
                //            }
                //        }
                //    }
                //    return points;
                //}

                //public static void WireCoordinates(List<string> listFileContent, Line.Station.Module stationLineModule)
                //{
                //    string beginPhrase = "POINTS INVARIABLES EN Z";
                //    string endPhrase = "SFP = Sub-Frame Point";
                //    List<string> fileVarCoord = new List<string>();
                //    fileVarCoord = GetLinesBetween2KeyWords(ref listFileContent, beginPhrase, endPhrase, true);
                //    List<List<double>> points = new List<List<double>>();

                //    foreach (string stringLine in fileVarCoord)
                //    {

                //        foreach (Point item in stationLineModule._MeasPoint)
                //        {
                //            if (item._Name == GetTokenFromLine(stringLine, 1))
                //            {
                //                double x = Numbers.ToDouble(GetTokenFromLine(stringLine, 2), false, na);
                //                double sx = P.Preferences.NotAvailableValueNa;
                //                double y = Numbers.ToDouble(GetTokenFromLine(stringLine, 3), false, na);
                //                double sy = P.Preferences.NotAvailableValueNa;
                //                //if ((double.TryParse(GetTokenFromLine(stringLine, 2), out x))&&(double.TryParse(GetTokenFromLine(stringLine, 3), out y))&&(double.TryParse(GetTokenFromLine(stringLine, 5), out sx))&&(double.TryParse(GetTokenFromLine(stringLine, 6), out sy)))
                //                if (x != na && y != na)
                //                {
                //                    switch (stationLineModule._AverageStationLine._Parameters._CoordType)
                //                    {
                //                        case CoordinatesType.CCS:
                //                            item._Coordinates.Ccs.X.Value = x;
                //                            item._Coordinates.Ccs.X.Sigma = sx; //sigma en mm
                //                            item._Coordinates.Ccs.Y.Value = y;
                //                            item._Coordinates.Ccs.Y.Sigma = sy; //sigma en mm
                //                            break;
                //                        case CoordinatesType.SU:
                //                            Coordinates newCoord = item._Coordinates.Local;
                //                            newCoord.X.Value = x;
                //                            newCoord.X.Sigma = sx; //sigma en mm
                //                            newCoord.Y.Value = y;
                //                            newCoord.Y.Sigma = sy;//sigma en mm
                //                            item.SetCoordinates(newCoord, newCoord.ReferenceFrame, newCoord.CoordinateSystem);
                //                            break;
                //                        case CoordinatesType.UserDefined:
                //                            break;
                //                        default:
                //                            //item._Coordinates.UserDefined.X.Value = x;
                //                            //item._Coordinates.UserDefined.X.Sigma = sx; //sigma en mm
                //                            //item._Coordinates.UserDefined.Y.Value = y;
                //                            //item._Coordinates.UserDefined.Y.Sigma = sy; //sigma en mm
                //                            break;
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}
                #endregion

            }

            #endregion

            #region JSON parser
            public static class OutputJSON
            {
                #region Polar Module
                public static void SetHeader(JsonElement jsonFile, Output output, Lgc2 lgc2)
                {

                    output._Result = new Polar.Station.Parameters.Setup.Values(null)
                    {
                        Version = ParserJSON.Header(jsonFile)
                    };

                    if (lgc2 != null)
                        output._Result.CoordinatesSystemsUsedForCompute = lgc2.coordinateSystemType;
                }

                public static void SetSigma0(JsonElement jsonFile, Output output, Lgc2 lgc2)
                {

                    output._Result.SigmaZero = ParserJSON.Sigma0(jsonFile);

                    if (lgc2.getTheRightOption != null)
                        output._Result.ReferenceSurface = lgc2.getTheRightOption();
                }

                public static void SetInstrumentProperties(JsonElement jsonFile, Output output, string frameName = null)
                {
                    // Instrument Height
                    output._Result.InstrumentHeight = new DoubleValue();
                    (double hValue, double hSigma) = ParserJSON.InstrumentHeight(jsonFile, frameName);
                    output._Result.InstrumentHeight.Value = hValue;
                    output._Result.InstrumentHeight.Sigma = hSigma;

                    // Instrument Vzero
                    output._Result.vZero = new DoubleValue();
                    (double v0Value, double v0Sigma) = ParserJSON.InstrumentV0(jsonFile, frameName);
                    output._Result.vZero.Value = v0Value;
                    output._Result.vZero.Sigma = v0Sigma;
                }

                public static void SetAH(JsonElement jsonFile, Output output, string frameName = null)
                {
                    output._Result.Measures = ParserJSON.AH(jsonFile, frameName);

                }

                public static void SetAV(JsonElement jsonFile, Output output, string frameName = null)
                {
                    List<Polar.Measure> measures = output._Result.Measures;
                    ParserJSON.AV(jsonFile, measures, frameName);

                }

                public static void SetD(JsonElement jsonFile, Output output, string frameName = null)
                {
                    List<Polar.Measure> measures = output._Result.Measures;
                    ParserJSON.D(jsonFile, measures, frameName);
                }

                public static List<Point> GetVariablePoints(JsonElement jsonFile)
                {
                    return ParserJSON.VariablePoints(jsonFile);
                }
                #endregion

                #region Levelling

                public static void SetLevel(JsonElement jsonFile, Level.Station.Module stationLevelModule)
                {
                    var points = ParserJSON.LevelResult(jsonFile);

                    foreach (KeyValuePair<string, double> item in points)
                    {
                        if (stationLevelModule._WorkingStationLevel._Name == item.Key)
                        {
                            if (item.Value != na)
                            {
                                stationLevelModule._WorkingStationLevel._Parameters._HStation = item.Value;
                            }
                        }
                    }
                    stationLevelModule.TheoReadingAndResidualsCalculation();
                }

                public static List<Point> SetLevel(JsonElement jsonFile, Level.Module levelModule)
                {

                    List<Point> pointList = new List<Point>();
                    var points = ParserJSON.LevelResult(jsonFile);

                    foreach (KeyValuePair<string, double> item in points)
                    {
                        foreach (Level.Station.Module stl in levelModule.StationModules.OfType<Level.Station.Module>())
                        {
                            string name = item.Key;
                            double z = item.Value;
                            
                            foreach (Level.Station str in stl.AllStationLevels)
                            {
                                if (str._Name == name)
                                {
                                    var p = SetPoint(str, name, z);
                                    pointList.Add(p);
                                }
                            }

                            foreach (Point pt in stl._TheoPoint)
                            {
                                if (pt._Name == name && pointList.FindIndex(x => x._Name == pt._Name) == -1)
                                {
                                    Point pCopy = pt.DeepCopy();
                                    SetHToPoint(stl._WorkingStationLevel, pCopy, z);
                                    pointList.Add(pCopy);
                                }
                            }
                        }
                    }
                    return pointList;
                }
                internal static Point SetPoint(Level.Station st, string name, double z)
                {

                    Point point = new Point
                    {
                        _Name = name
                    };
                    double hStation = z;
                    SetHToPoint(st, point, hStation);
                    return point;

                }
                #endregion

                #region Wire

                public static void GetOffsetResultsForWire(JsonElement jsonFile, Line.Station stationLine)
                {
                    int nombrePointCala = 0;

                    foreach (MeasureOfOffset item in stationLine._MeasureOffsets)
                    {
                        if (item._Point.LGCFixOption == LgcPointOption.CALA && !(item._Status is M.States.Bad))
                        {
                            nombrePointCala++;
                        }

                    }
                    if (nombrePointCala != stationLine._MeasureOffsets.Count)
                    {
                        GetWireDisplacement(jsonFile, stationLine);
                    }
                    ParserJSON.GetWireResidualOnCalapoints(jsonFile, stationLine);
                }

                public static void GetWireDisplacement(JsonElement jsonFile, Line.Station stationLine)
                {
                    var points = ParserJSON.WireDisplacement(jsonFile);

                    foreach (KeyValuePair<string, double> measOffset in points)
                    {
                        string name = measOffset.Key;
                        double moveToDo = measOffset.Value;
                        if (stationLine._Anchor1._Point._Name == name && moveToDo != na)
                        {
                            SetDeviation(stationLine._Anchor1, moveToDo);
                        }
                        if (stationLine._Anchor2._Point._Name == name && moveToDo != na)
                        {
                            SetDeviation(stationLine._Anchor2, moveToDo);
                        }
                        foreach (MeasureOfOffset measureOffset in stationLine._MeasureOffsets)
                        {
                            if (measureOffset._Point._Name == name)
                            {
                                if (moveToDo != na && measureOffset._RawReading != na && measureOffset._IsFakeRawReading == false)
                                {
                                    SetDeviation(measureOffset, moveToDo);
                                }
                                else
                                {
                                    ///Pour calcul des lectures theo
                                    if (measureOffset._IsFakeRawReading == true)
                                    {
                                        measureOffset._RawReading = na;
                                        measureOffset._IsFakeRawReading = false;

                                        if (moveToDo != na)
                                        {
                                            measureOffset._AverageTheoreticalReading = measureOffset._CorrectedReading + moveToDo / 1000;
                                            measureOffset._TheoreticalDeviation = -moveToDo / 1000;
                                            measureOffset._AverageDeviation = measureOffset._TheoreticalDeviation;
                                        }
                                    }

                                }
                            }
                        }
                    }
                }

                internal static void SetAnchor(MeasureOfOffset anchor, string name, double theoreticalDeviation)
                {
                    LgcPointOption cala = LgcPointOption.CALA;
                    if (anchor._Point._Name == name && anchor._Point.LGCFixOption == cala)
                    {
                        if (theoreticalDeviation != na)
                        {
                            anchor._TheoreticalDeviation = -theoreticalDeviation; // in M
                            anchor._TheoreticalReading = anchor._CorrectedReading - anchor._TheoreticalDeviation;
                            anchor._AverageTheoreticalReading = anchor._TheoreticalReading;
                            anchor._AverageDeviation = anchor._TheoreticalDeviation;
                        }
                    }
                }

                internal static void SetCoordinates(Point point, JsonElement.ArrayEnumerator vector, CoordinatesType coordType)
                {

                    double x = Numbers.ToDouble(vector.ElementAt(0).ToString());
                    double sx = P.Preferences.NotAvailableValueNa;
                    double y = Numbers.ToDouble(vector.ElementAt(1).ToString());
                    double sy = P.Preferences.NotAvailableValueNa;

                    if (x != na && y != na)
                    {
                        switch (coordType)
                        {
                            case CoordinatesType.CCS:
                                point._Coordinates.Ccs.X.Value = x;
                                point._Coordinates.Ccs.X.Sigma = sx; //sigma en mm
                                point._Coordinates.Ccs.Y.Value = y;
                                point._Coordinates.Ccs.Y.Sigma = sy; //sigma en mm
                                break;
                            case CoordinatesType.SU:
                                Coordinates newCoord = point._Coordinates.Local;
                                newCoord.X.Value = x;
                                newCoord.X.Sigma = sx; //sigma en mm
                                newCoord.Y.Value = y;
                                newCoord.Y.Sigma = sy;//sigma en mm
                                point.SetCoordinates(newCoord, newCoord.ReferenceFrame, newCoord.CoordinateSystem);
                                break;
                            case CoordinatesType.UserDefined:
                                break;
                            default:
                                break;
                        }
                    }
                }

                public static void GetWireCoordinates(JsonElement jsonFile, Line.Station.Module stationLineModule)
                {
                    var points = ParserJSON.WireCoordinates(jsonFile);
                    foreach (KeyValuePair<string, JsonElement.ArrayEnumerator> point in points)
                    {
                        foreach (Point item in stationLineModule._MeasPoint)
                        {
                            string name = point.Key;
                            var vector = point.Value;
                            CoordinatesType coordType = stationLineModule.WorkingAverageStationLine._Parameters._CoordType;
                            if (item._Name == name)
                            {
                                SetCoordinates(item, vector, coordType);
                            }
                        }
                    }
                }

                #endregion

                #region Frame

                public static bool SetFrameResults(string jsonPath, string frameName,
                        out DoubleValue TX, out DoubleValue TY, out DoubleValue TZ,
                        out DoubleValue RX, out DoubleValue RY, out DoubleValue RZ,
                        out DoubleValue EstimatedScale,
                        out bool fixedTX, out bool fixedTY, out bool fixedTZ,
                        out bool fixedRX, out bool fixedRY, out bool fixedRZ,
                        out bool fixedScale,
                        out List<(string, DoubleValue, DoubleValue, DoubleValue)> obsXyzResiduals,
                        out List<(string, DoubleValue)> inclys, out double sigma0, out List<Point> points
                        )
                {
                    bool success = false;

                    var jsonFile = ParserJSON.ReadJSONFile(jsonPath);
                    var frameParam = ParserJSON.GetFrameParameter(jsonFile, frameName);
                    TX = frameParam[0];
                    TY = frameParam[1];
                    TZ = frameParam[2];
                    RX = frameParam[3];
                    RY = frameParam[4];
                    RZ = frameParam[5];
                    EstimatedScale = frameParam[6];

                    var frameFixedParam = ParserJSON.GetFrameFixedParameter(jsonFile, frameName);
                    fixedTX = frameFixedParam[0];
                    fixedTY = frameFixedParam[1];
                    fixedTZ = frameFixedParam[2];
                    fixedRX = frameFixedParam[3];
                    fixedRY = frameFixedParam[4];
                    fixedRZ = frameFixedParam[5];
                    fixedScale = frameFixedParam[6];

                    obsXyzResiduals = ParserJSON.GetFrameOBSXYZ(jsonFile, frameName);
                    inclys = ParserJSON.GetFrameIncly(jsonFile, frameName);
                    sigma0 = ParserJSON.Sigma0(jsonFile);
                    points = ParserJSON.VariablePoints(jsonFile);
                    return success;
                }



                internal static List<Point> GetVariablePoints(Output output)
                {
                    string fileName = output.OutputFilePath.ToUpper().Replace(".RES", ".JSON");
                    JsonElement jsonFile = ParserJSON.ReadJSONFile(fileName);
                    return GetVariablePoints(jsonFile);
                }

                internal static Coordinates.ReferenceFrames GetReferenceSurface(Output output)
                {
                    string fileName = output.OutputFilePath.ToUpper().Replace(".RES", ".JSON");
                    JsonElement jsonFile = ParserJSON.ReadJSONFile(fileName);
                    return ParserJSON.ReferenceSurface(jsonFile);
                }
                #endregion
            }


            public static class ParserJSON
            {
                public static int precision = 7;
                public static int MMprecision = 1000;
                public static int CCprecision = 10000;

                public static JsonElement ReadJSONFile(string fileName)
                {
                    JsonElement results = new JsonElement();
                    try
                    {
                        using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                        {
                            using (JsonDocument jsonDocument = JsonDocument.Parse(stream))
                            {
                                results = jsonDocument.RootElement.Clone();
                            }
                        }

                        return results;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(R.T_CANNOT_PARSE_THE_LGC2_OUTPUT_FILE, ex);
                    }
                }

                public static bool TryReadElementByPath(JsonElement jsonFile, string path, out JsonElement results)
                {
                    results = jsonFile;
                    string[] paths = path.Split('.');

                    try
                    {
                        foreach (string sPath in paths)
                        {
                            if (sPath == "[0]")
                            {
                                if (results.ValueKind != JsonValueKind.Array)
                                    return false;
                                results = results.EnumerateArray().ElementAt(0);
                            }
                            else
                            {
                                if (!results.TryGetProperty(sPath, out JsonElement subResult))
                                    return false;
                                else
                                    results = subResult;
                            }
                        }

                        return true;
                    }
                    catch (Exception)
                    {
                        return false;
                    }

                }

                public static JsonElement ReadElementByPath(JsonElement jsonFile, string path)
                {
                    JsonElement results = new JsonElement();
                    string[] paths = path.Split('.');

                    try
                    {
                        foreach (string sPath in paths)
                        {
                            if (paths[0] == sPath)
                                results = jsonFile.GetProperty(sPath);
                            else
                            {
                                if (sPath == "[0]")
                                    results = results.EnumerateArray().ElementAt(0);
                                else
                                    results = results.GetProperty(sPath);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(R.T_CANNOT_PARSE_THE_LGC2_OUTPUT_FILE, ex);
                    }

                    return results;
                }

                internal static double GetPropertyRad2Gon(JsonElement jsonElement, string jsonPath)
                {
                    // Get element by path and convert from RAD to GON, with convert MODULO400
                    JsonElement jsonValue = ReadElementByPath(jsonElement, jsonPath);
                    double value = GetGonValue(jsonValue);
                    return value;
                }

                internal static double GetPropertySigmaRad2Gon(JsonElement jsonElement, string jsonPath)
                {
                    // Get element by path and convert from RAD To GON, without convert MODULO400
                    JsonElement jsonValue = ReadElementByPath(jsonElement, jsonPath);
                    double value = GetSigmaGonValue(jsonValue);
                    return value;

                }

                internal static double GetPropertyJson2Double(JsonElement jsonElement, string jsonPath)
                {
                    JsonElement jsonValue = ReadElementByPath(jsonElement, jsonPath);
                    double value = GetDoubleValue(jsonValue);
                    return value;

                }

                internal static double GetDoubleValue(JsonElement jsonElement)
                {
                    double value;
                    if (jsonElement.ValueKind == JsonValueKind.Null)
                        value = na;
                    else
                        value = Math.Round(Numbers.ToDouble(jsonElement.ToString()), precision);

                    if (value < na * 3)
                        value = na;

                    return value;
                }
                internal static double GetGonValue(JsonElement jsonElement)
                {
                    double value;
                    if (jsonElement.ValueKind == JsonValueKind.Null)
                        value = na;
                    else
                    {
                        var dV = GetDoubleValue(jsonElement);
                        var gon = T.Conversions.Angles.Rad2Gon(dV);
                        value = Math.Round(Survey.Modulo400(gon), precision);
                        if (value == 0) // to avoid sigma=0
                            value = Math.Pow(10, -precision);
                    }

                    return value;
                }

                internal static double GetSigmaGonValue(JsonElement jsonElement)
                {
                    double value;
                    if (jsonElement.ValueKind == JsonValueKind.Null)
                        value = na;
                    else
                    {
                        var dV = GetDoubleValue(jsonElement);
                        var gon = T.Conversions.Angles.Rad2Gon(dV);
                        value = Math.Round(gon, precision);
                    }
                    return value;

                }

                internal static string GetStringValue(JsonElement jsonElement)
                {
                    string param;
                    if (jsonElement.ValueKind == JsonValueKind.Null)
                        param = "";
                    else
                        param = jsonElement.ToString();

                    return param;
                }

                #region Polar Module
                public static string Header(JsonElement jsonFile)
                {
                    // JSON string
                    string lgcHeader = "LGCVersion";

                    // LGC header - STRING
                    string version = GetStringValue(ReadElementByPath(jsonFile, lgcHeader));
                    version = $"LGC2 {version}";
                    return version;
                }

                public static double Sigma0(JsonElement jsonFile)
                {
                    // JSON string
                    string sigmaPath = "LGC_DATA.fLSRelatedInfo.fS0APosteriori";

                    //sigma0 - no unit 
                    double sigma = GetPropertyJson2Double(jsonFile, sigmaPath);
                    return sigma;

                }

                private static JsonElement GetStationFromFrame(JsonElement jsonFile, string frameName)
                {
                    if (frameName == null)
                        return ReadElementByPath(jsonFile, "LGC_DATA.tree.[0].measurements.fTSTN.[0]");
                    else
                    {
                        var tree = ReadElementByPath(jsonFile, "LGC_DATA.tree").EnumerateArray();
                        foreach (var elem in tree)
                        {
                            var name = GetStringValue(ReadElementByPath(elem, "frame.name"));
                            if (name == frameName)
                            {
                                return ReadElementByPath(elem, "measurements.fTSTN.[0]");
                            }
                        }

                        throw new Exception($"{R.T_CANNOT_PARSE_THE_LGC2_OUTPUT_FILE} frame {frameName}");
                    }

                }

                public static (double, double) InstrumentHeight(JsonElement jsonFile, string frameName = null)
                {
                    // first we need to know if it is fixed of esimated
                    var station = GetStationFromFrame(jsonFile, frameName);
                    var instrumentHeightAdjustable = ReadElementByPath(station, "instrumentHeightAdjustable");
                    bool heightFixed = bool.Parse(instrumentHeightAdjustable.GetProperty("ifFixed").ToString());

                    // Height
                    // UNIT RES - HI[m] , SHI[mm])
                    // UNIT JSON - both in [m]
                    double hValue = GetPropertyJson2Double(instrumentHeightAdjustable, "fEstimatedValue");
                    double hSigma = GetPropertyJson2Double(instrumentHeightAdjustable, "fEstimatedPrecision");
                    return (hValue, hSigma);
                }

                public static (double, double) InstrumentV0(JsonElement jsonFile, string frameName = null)
                {
                    // JSON string
                    string v0ValuePath = "fEstimatedValue";
                    string v0SigmaPath = "fEstimatedPrecision";
                    string v0Path = "roms.[0].v0";

                    var station = GetStationFromFrame(jsonFile, frameName);

                    // Vzero
                    // UNIT RES - V0[gon] , SV0[CC])
                    // UNIT JSON - both in [rad]
                    var v0 = ReadElementByPath(station, v0Path);
                    double v0Value = GetPropertyRad2Gon(v0, v0ValuePath);
                    double v0Sigma = GetPropertyRad2Gon(v0, v0SigmaPath);
                    return (v0Value, v0Sigma);
                }


                public static Coordinates.ReferenceFrames ReferenceSurface(JsonElement jsonFile)
                {// JSON string
                    string varPointPath = "LGC_DATA.points";
                    string pointReferential = "fReferential";

                    List<Point> variables = new List<Point>();
                    var points = ReadElementByPath(jsonFile, varPointPath).EnumerateArray();

                    if (points.Any())
                    {
                        int EnumRefFrame = int.Parse(points.ElementAt(1).GetProperty(pointReferential).ToString());
                        CSGEOdll.ReferenceFrames csGeoRF = (CSGEOdll.ReferenceFrames)Enum.Parse(typeof(CSGEOdll.ReferenceFrames), EnumRefFrame.ToString());
                        return GetTsunamiReferenceFramesBasedOnCsGeoRF(csGeoRF);

                    }
                    else
                        return Coordinates.ReferenceFrames.Unknown;
                }


                public static List<Point> VariablePoints(JsonElement jsonFile)
                {
                    // I didn't find reference surface - there is sth FReferential but is number - 104 


                    // JSON string
                    string varPointPath = "LGC_DATA.points";
                    string pointH_Fixed = "fHfixed";
                    string pointXYZ_Fixed = "fixedState";
                    string pointReferential = "fReferential";
                    string varXYZ = "fEstimatedValueInRoot.fVector";
                    string varH = "fEstimatedHeightInRoot.fValue";
                    //string varSigmaXYZ = "fEstimatedPrecision";
                    string name = "fName";
                    string varFrame = "fFramePosition_Name";

                    // Points variables en XYZ - H not implented is JSON !!
                    // UNIT RES - X,Y,Z [M], SX,SY,SZ [MM]
                    // UNIT JSON - everything in [M]
                    List<Point> variables = new List<Point>();
                    var points = ReadElementByPath(jsonFile, varPointPath).EnumerateArray();
                    foreach (var point in points)
                    {
                        bool isInRoot = point.GetProperty(varFrame).ToString() == "ROOT";
                        bool isH_Fixed = bool.Parse(point.GetProperty(pointH_Fixed).ToString());
                        var vectorFixed = ReadElementByPath(point, pointXYZ_Fixed).EnumerateArray();
                        bool isXY_Fixed = vectorFixed.ElementAt(0).GetBoolean() && vectorFixed.ElementAt(1).GetBoolean();
                        bool isZ_Fixed = vectorFixed.ElementAt(2).GetBoolean();
                        bool isFixed = isXY_Fixed && (isH_Fixed || isZ_Fixed); // because in *OLOC H is said to be free even if Z is fixed

                        int EnumRefFrame = int.Parse(point.GetProperty(pointReferential).ToString());
                        CSGEOdll.ReferenceFrames csGeoRF = (CSGEOdll.ReferenceFrames)Enum.Parse(typeof(CSGEOdll.ReferenceFrames), EnumRefFrame.ToString());

                        bool isCalaInSubFrame = isFixed && !isInRoot;

                        if (!isFixed || isCalaInSubFrame)
                        {
                            var vector = ReadElementByPath(point, varXYZ).EnumerateArray();
                            // var sigma = ReadElementByPath(point, varSigmaXYZ).EnumerateArray(); // do not exist since LGC 2.8
                            var h = ReadElementByPath(point, varH).ToString();
                            var frame = ReadElementByPath(point, varFrame).ToString();

                            Point p = new Point();
                            Coordinates c = new Coordinates();
                            c.Set();
                            c.ReferenceFrame = GetTsunamiReferenceFramesBasedOnCsGeoRF(csGeoRF);
                            c.CoordinateSystem = GetCoordinatesSystemsTypeBasedOn(c.ReferenceFrame);
                            c._Name = point.GetProperty(name).ToString();
                            c.X.ValueFromString(vector.ElementAt(0).ToString());
                            c.Y.ValueFromString(vector.ElementAt(1).ToString());
                            if (c.CoordinateSystem == CoordinateSystemsTypes._2DPlusH) //coordType == Coordinates.CoordinateSystemsTypes._2DPlusH)
                                c.Z.ValueFromString(h.ToString()); // this is H
                            else
                                c.Z.ValueFromString(vector.ElementAt(2).ToString()); // this is Z
                            //if (frame == "ROOT")
                            //{
                            //    c.X.SigmaFromString(sigma.ElementAt(0).ToString());
                            //    c.Y.SigmaFromString(sigma.ElementAt(1).ToString());
                            //    c.Z.SigmaFromString(sigma.ElementAt(2).ToString());
                            //}
                            //else
                            {
                                // We don't have directly the precision relative to the root frame
                                // So we use the square roots of the numbers in the diagonal of the covariance matrix
                                string varCovarianceInRoot = "fCovarianceMatrixInRoot";
                                var covarianceInRoot = ReadElementByPath(point, varCovarianceInRoot);
                                c.X.Sigma = Math.Sqrt(covarianceInRoot[0][0].GetDouble());
                                c.Y.Sigma = Math.Sqrt(covarianceInRoot[1][1].GetDouble());
                                c.Z.Sigma = Math.Sqrt(covarianceInRoot[2][2].GetDouble());
                            }

                            if (c.CoordinateSystem == CoordinateSystemsTypes._2DPlusH)
                                p._Coordinates.Ccs = c;
                            else
                                p._Coordinates.Local = c;
                            p._Name = c._Name;
                            variables.Add(p);
                        }
                    }
                    return variables;
                }

                internal static List<JsonElement> InvariablePoints(JsonElement jsonFile)
                {
                    // JSON string
                    string varPointPath = "LGC_DATA.points";
                    string pointFixed = "fHfixed";
                    string pointFixedList = "fixedState";

                    // POINTS INVARIABLES EN Z
                    var points = ReadElementByPath(jsonFile, varPointPath).EnumerateArray();

                    List<JsonElement> invList = new List<JsonElement>();

                    foreach (var point in points)
                    {
                        var fixedState = point.GetProperty(pointFixedList).EnumerateArray();
                        int trueValueCount = fixedState.Count(x => x.GetBoolean());
                        if (fixedState.Count() != trueValueCount)
                        {
                            invList.Add(point);
                        }
                    }

                    return invList;

                }

                public static List<Polar.Measure> AH(JsonElement jsonFile, string frameName = null)
                {
                    // JSON string
                    const string AHPath = "roms.[0].measANGL";
                    const string idPath = "obsID";
                    const string namePath = "targetPos";
                    const string observePath = "angles.[0].fValue";
                    const string sigmaPath = "target.sigmaCombinedAngle";
                    const string residuPath = "anglesResiduals.[0].fValue";

                    var station = GetStationFromFrame(jsonFile, frameName);

                    // ANGLES HORIZONTAUX (ANGL)
                    // UNIT RES - OBSERVE[GON], SIGMA[CC], RESIDU[CC]
                    // UNIT JSON - everything in [RAD]
                    var points = ReadElementByPath(station, AHPath).EnumerateArray();
                    List<Polar.Measure> angles = new List<Polar.Measure>();

                    int counter = 1000;
                    foreach (var point in points)
                    {
                        counter++;
                        string obsId = point.GetProperty(idPath).ToString();
                        if (obsId == "")
                            obsId = $"{counter}.H";

                        string pointName = point.GetProperty(namePath).ToString();
                        Polar.Measure m = GetOrCreateTemporaryPolarMeasure(angles, obsId, pointName);

                        /// For this moment all angles values in JSON are in RAD -> in Tsunami GON
                        m.Angles.raw.Horizontal.Value = GetPropertyRad2Gon(point, observePath); // OBSERVE values
                        m.Angles.raw.Horizontal.Sigma = GetPropertySigmaRad2Gon(point, sigmaPath); // SIGMA values                     
                        m.Angles.corrected.Horizontal.Sigma = GetPropertySigmaRad2Gon(point, residuPath); // RESIDU values                       
                        m.Angles.corrected.Horizontal.Value = Survey.Modulo400(m.Angles.raw.Horizontal.Value + m.Angles.corrected.Horizontal.Sigma); // CALCULE values
                    }

                    return angles;
                }

                private static Polar.Measure GetOrCreateTemporaryPolarMeasure(List<Polar.Measure> measures, string obsId, string pointName)
                {
                    string measureID = obsId.Split('.')[0];

                    // Try to find if the measure already exists
                    Polar.Measure m = measures.Find(x => x.Id == measureID);

                    var point = new Point
                    {
                        _Name = pointName,
                        StorageStatus = Tsunami.StorageStatusTypes.Temp
                    }; // this will set the point in the global temporary list of tsunai points Properties.TempPoints

                    // If not found, create one, and add it to the list
                    if (m == null)
                    {
                        m = new Polar.Measure
                        {
                            Id = measureID,
                            PointGUID = point.Guid,
                        };
                        measures.Add(m);
                    }

                    return m;
                }

                public static void AV(JsonElement jsonFile, List<Polar.Measure> measures, string frameName = null)
                {
                    // JSON string
                    const string AVPath = "roms.[0].measZEND";
                    const string idPath = "obsID";
                    const string namePath = "targetPos";
                    const string observePath = "angles.[0].fValue";
                    const string sigmaPath = "target.sigmaCombinedAngle";
                    const string residuPath = "anglesResiduals.[0].fValue";

                    var station = GetStationFromFrame(jsonFile, frameName);

                    // DISTANCES ZENITHALES (ZEND)
                    // UNIT RES - OBSERVE[GON], SIGMA[CC], RESIDU[CC]
                    // UNIT JSON - everything in [RAD]
                    var points = ReadElementByPath(station, AVPath).EnumerateArray();

                    int counter = 1000;
                    foreach (var point in points)
                    {
                        counter++;
                        string obsId = point.GetProperty(idPath).ToString();
                        if (obsId == "")
                            obsId = $"{counter}.V";
                        string pointName = point.GetProperty(namePath).ToString();
                        Polar.Measure m = GetOrCreateTemporaryPolarMeasure(measures, obsId, pointName);

                        /// For this moment all angles values in JSON are in RAD -> in Tsunami GON
                        m.Angles.raw.Vertical.Value = GetPropertyRad2Gon(point, observePath); // OBSERVE values
                        m.Angles.raw.Vertical.Sigma = GetPropertySigmaRad2Gon(point, sigmaPath); // SIGMA values 
                        m.Angles.corrected.Vertical.Sigma = GetPropertySigmaRad2Gon(point, residuPath); ;// RESIDU values 
                        m.Angles.corrected.Vertical.Value = Survey.Modulo400(m.Angles.raw.Vertical.Value + m.Angles.corrected.Vertical.Sigma); // CALCULE values
                    }

                }

                public static void D(JsonElement jsonFile, List<Polar.Measure> measures, string frameName = null)
                {
                    // JSON string
                    const string DPath = "roms.[0].measDIST";
                    const string idPath = "obsID";
                    const string namePath = "targetPos";
                    const string observePath = "distances.[0].fValue";
                    const string sigmaPath = "target.sigmaCombinedDist";
                    const string residuPath = "distancesResiduals.[0].fValue";
                    const string targetNamePath = "target.ID";
                    const string targetConstPath = "target.distCorrectionValue";
                    const string targetHPath = "target.targetHt";

                    var station = GetStationFromFrame(jsonFile, frameName);

                    // DISTANCES MESUREES (DIST)
                    // UNIT RES - OBSERVE[M], SIGMA[MM], RESIDU[MM]
                    // UNIT JSON - everything in [M]
                    var points = ReadElementByPath(station, DPath).EnumerateArray();

                    int counter = 1000;
                    foreach (var point in points)
                    {
                        counter++;
                        string obsId = point.GetProperty(idPath).ToString();
                        if (obsId == "")
                            obsId = $"{counter}.D";
                        string pointName = point.GetProperty(namePath).ToString();
                        Polar.Measure m = GetOrCreateTemporaryPolarMeasure(measures, obsId, pointName);

                        /// For this moment all distances values in JSON are in M
                        double obsValue = GetPropertyJson2Double(point, observePath); // OBSERVE values
                        double obsSigma = GetPropertyJson2Double(point, sigmaPath); // SIGMA values
                        m.Distance.Raw = new DoubleValue(obsValue, obsSigma);
                        double corrSigma = GetPropertyJson2Double(point, residuPath); // RESIDU values
                        double corrValue = obsValue + corrSigma; // CALCULE values
                        m.Distance.Corrected = new DoubleValue(corrValue, corrSigma);
                        m.Distance.Reflector.Constante = GetPropertyJson2Double(point, targetConstPath); // CONST value
                        m.Distance.Reflector._Name = ReadElementByPath(point, targetNamePath).ToString(); // TRGT Name       
                        m.Extension.Value = GetPropertyJson2Double(point, targetHPath); // H_TRGT value                       

                    }
                }

                internal static void NonVerticalized(JsonElement jsonFile, Output output)
                {
                    //AH(jsonFile, output);
                    //AV(jsonFile, output);
                    //D(jsonFile, output);

                }

                #endregion

                #region Levelling
                public static Dictionary<string, double> LevelResult(JsonElement jsonFile)
                {
                    // JSON string
                    string namePath = "fName";
                    string vectorPath = "fEstimatedValue.fVector";

                    // Read POINTS VARIABLES EN Z UNIQUEMENT in Meter (M), Z coordinates
                    // UNIT RES - X,Y,Z[M]
                    // UNIT JSON - everything in [M]
                    List<JsonElement> invPoints = InvariablePoints(jsonFile);
                    Dictionary<string, double> points = new Dictionary<string, double>();

                    foreach (var point in invPoints)
                    {
                        string name = point.GetProperty(namePath).ToString();
                        var vector = ReadElementByPath(point, vectorPath).EnumerateArray();
                        double hStation = Math.Round(Numbers.ToDouble(vector.ElementAt(2).ToString()), precision);
                        points.Add(name, hStation);
                    }
                    return points;
                }
                #endregion

                #region Wire

                public static Dictionary<string, double> WireDisplacement(JsonElement jsonFile)
                {
                    // This JSON path is to change because is not Displacement
                    // JSON string

                    // Read POINTS INVARIABLES EN Z, in Meter (M)
                    List<JsonElement> invPoints = InvariablePoints(jsonFile);
                    Dictionary<string, double> points = new Dictionary<string, double>();

                    // we need to compute estimated-proviseional
                    foreach (JsonElement point in invPoints)
                    {
                        var name = point.GetProperty("fName").ToString();

                        var element = ReadElementByPath(point, "fEstimatedValueInRoot");
                        var estimated = element.GetProperty("fVector").EnumerateArray();
                        var dEstimated = Numbers.ToDouble(estimated.ElementAt(0).ToString(), false, na);

                        var elementProvi = ReadElementByPath(point, "fProvisionalValueInRoot");
                        var provi = elementProvi.GetProperty("fVector").EnumerateArray();
                        var dProvi = Numbers.ToDouble(provi.ElementAt(0).ToString(), false, na);

                        var difference = dEstimated - dProvi;

                        points.Add(name, difference * 1000);

                        TSU.Debug.WriteInConsole("json" + name + " " + difference);
                    }
                    return points;
                }

                public static Dictionary<string, JsonElement.ArrayEnumerator> WireCoordinates(JsonElement jsonFile)
                {
                    // JSON string
                    string namePath = "fName";
                    string varXYZ = "fEstimatedValue.fVector";

                    // Read POINTS INVARIABLES EN Z 
                    // UNIT RES - X,Y,Z[M]
                    // UNIT JSON - everything in [M]
                    List<JsonElement> invPoints = InvariablePoints(jsonFile);
                    Dictionary<string, JsonElement.ArrayEnumerator> points = new Dictionary<string, JsonElement.ArrayEnumerator>();

                    foreach (JsonElement point in invPoints)
                    {
                        string name = point.GetProperty(namePath).ToString();
                        var vector = ReadElementByPath(point, varXYZ).EnumerateArray();
                        points.Add(name, vector);
                    }
                    return points;
                }

                internal static List<Point> GetWireCoordinates(JsonElement jsonFile, Line.Module lineModule)
                {

                    // JSON string
                    string namePath = "fName";
                    string varXYZ = "fEstimatedValue.fVector";

                    // Read POINTS INVARIABLES EN Z in M
                    List<JsonElement> invPoints = InvariablePoints(jsonFile);
                    List<string> pointsNamesAlreadyUsed = new List<string>();
                    List<Point> newListPoints = new List<Point>();
                    CoordinatesType coordType = CoordinatesType.CCS;

                    foreach (Line.Station.Module stLineMod in lineModule.StationModules.OfType<Line.Station.Module>())
                    {
                        foreach (Point item in stLineMod._MeasPoint)
                        {
                            if (pointsNamesAlreadyUsed.FindIndex(x => x == item._Name) == -1)
                            {
                                pointsNamesAlreadyUsed.Add(item._Name);
                                newListPoints.Add(item.DeepCopy());
                            }
                        }
                        coordType = stLineMod.WorkingAverageStationLine._Parameters._CoordType;
                    }

                    foreach (JsonElement point in invPoints)
                    {
                        foreach (Point item in newListPoints)
                        {
                            string name = point.GetProperty(namePath).ToString();
                            var vector = ReadElementByPath(point, varXYZ).EnumerateArray();
                            if (item._Name == name)
                            {
                                //SetCoordinates(item, vector, coordType);
                                item._Origin = "GlobalEcartometryLGC2Calculation";
                            }

                        }
                    }
                    newListPoints.RemoveAll(x => x._Origin != "GlobalEcartometryLGC2Calculation");
                    return newListPoints;

                }

                internal static void GetWireResidualOnCalapoints(JsonElement jsonFile, Line.Station stationLine)
                {

                    // JSON string
                    string resPath = "distancesResiduals.[0].fValue";
                    string namePath = "targetPos";
                    string echoMeasPath = "LGC_DATA.tree.[0].measurements.fECHO.[0].measECHO";

                    // Read MESURES ECHO
                    var echoPoints = ReadElementByPath(jsonFile, echoMeasPath).EnumerateArray();
                    foreach (var point in echoPoints)
                    {
                        var res = ReadElementByPath(point, resPath).ToString();
                        double theoreticalDeviation = Numbers.ToDouble(res, false, na);
                        var name = point.GetProperty(namePath).ToString();

                        if (stationLine._Anchor1._Point._Name == name && stationLine._Anchor1._Point.LGCFixOption == LgcPointOption.CALA)
                        {
                            if (theoreticalDeviation != -9999)
                            {
                                stationLine._Anchor1._TheoreticalDeviation = -theoreticalDeviation / 1000;
                                stationLine._Anchor1._TheoreticalReading = stationLine._Anchor1._CorrectedReading - stationLine._Anchor1._TheoreticalDeviation;
                                stationLine._Anchor1._AverageTheoreticalReading = stationLine._Anchor1._TheoreticalReading;
                                stationLine._Anchor1._AverageDeviation = stationLine._Anchor1._TheoreticalDeviation;
                            }
                        }

                        if (stationLine._Anchor2._Point._Name == name && stationLine._Anchor2._Point.LGCFixOption == LgcPointOption.CALA)
                        {
                            if (theoreticalDeviation != -9999)
                            {
                                stationLine._Anchor2._TheoreticalDeviation = -theoreticalDeviation / 1000;
                                stationLine._Anchor2._TheoreticalReading = stationLine._Anchor2._CorrectedReading - stationLine._Anchor2._TheoreticalDeviation;
                                stationLine._Anchor2._AverageTheoreticalReading = stationLine._Anchor2._TheoreticalReading;
                                stationLine._Anchor2._AverageDeviation = stationLine._Anchor2._TheoreticalDeviation;
                            }
                        }

                        foreach (MeasureOfOffset item in stationLine._MeasureOffsets)
                        {
                            if (item._Point._Name == name && item._Point.LGCFixOption == LgcPointOption.CALA)
                            {
                                if (theoreticalDeviation != -9999)
                                {
                                    item._TheoreticalDeviation = -theoreticalDeviation / 1000;
                                    item._TheoreticalReading = item._CorrectedReading - item._TheoreticalDeviation;
                                    item._AverageTheoreticalReading = item._TheoreticalReading;
                                    item._AverageDeviation = item._TheoreticalDeviation;
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Frame

                internal static JsonElement FindElementByName(JsonElement jsonBranches, string paramPath, string paramToFind)
                {
                    JsonElement result = new JsonElement();
                    foreach (var branch in jsonBranches.EnumerateArray())
                    {
                        var fNames = ReadElementByPath(branch, paramPath).EnumerateArray();
                        string fName = GetStringValue(fNames.Last());
                        if (fName == paramToFind)
                        {
                            result = branch;
                            break;
                        }
                    }

                    return result;
                }

                public static List<DoubleValue> GetFrameParameter(JsonElement jsonFile, string frameName)
                {
                    // JSON string                
                    string treePath = "LGC_DATA.tree";
                    string namePath = "branch";
                    string tValuePath = "frame.fEstParameter";
                    string tPrecPath = "frame.fEstPrecision";

                    // Read PARAMETER INIT VAL and CALC VAL for FRAME
                    // UNIT RES - INIT VAL for TX,TY,TZ [M], CALC VAL for TX,TY,TZ [M], INIT VAL for RX,RY,RZ [GON], CALC VAL for RX,RY,RZ [GON]
                    // UNIT JSON - TX,TY,TZ in [M], RX,RY,RZ in [RAD]
                    List<DoubleValue> results = new List<DoubleValue>();
                    var branches = ReadElementByPath(jsonFile, treePath);
                    var branch = FindElementByName(branches, namePath, frameName);
                    if (branch.ValueKind != JsonValueKind.Null)
                    {
                        var fResults = ReadElementByPath(branch, tValuePath);
                        //var fPrecision = ReadElementByPath(branch, tPrecPath); //2.7 style

                        // We don't have directly the precision relative to the root frame
                        // So we use the square roots of the numbers in the diagonal of the covariance matrix
                        string varCovarianceInRoot = "frame.fCovarianceMatrix";
                        var fPrecision = ReadElementByPath(branch, varCovarianceInRoot);
                        var covarianceInRoot = ReadElementByPath(branch, varCovarianceInRoot);
                        
                        var txSigma = Math.Sqrt(covarianceInRoot[0][0].GetDouble());
                        var tySigma = Math.Sqrt(covarianceInRoot[1][1].GetDouble());
                        var tzSigma = Math.Sqrt(covarianceInRoot[2][2].GetDouble());
                        var rxSigma = Math.Sqrt(covarianceInRoot[3][3].GetDouble());
                        var rySigma = Math.Sqrt(covarianceInRoot[4][4].GetDouble());
                        var rzSigma = Math.Sqrt(covarianceInRoot[5][5].GetDouble());
                        var scSigma = Math.Sqrt(covarianceInRoot[6][6].GetDouble());

                        for (int i = 0; i < fResults.GetArrayLength(); i++)
                        {
#pragma warning disable S2583 // Conditionally executed code should be reachable
                            if (i >= 3 && i <= 5)
                            {
                                double residu = GetGonValue(fResults[i]);
                                double precision = Math.Sqrt(GetGonValue(fPrecision[i][i])); // diagonal of the cavirance matrix
                                results.Add(new DoubleValue(residu, precision));
                            }
                            else
                            {
                                double residu = GetDoubleValue(fResults[i]);
                                double precision = Math.Sqrt(GetDoubleValue(fPrecision[i][i])); // diagonal of the cavirance matrix
                                results.Add(new DoubleValue(residu, precision));
                            }
#pragma warning restore S2583 // Conditionally executed code should be reachable
                        }



                    }

                    return results;
                }

                public static List<bool> GetFrameFixedParameter(JsonElement jsonFile, string frameName)
                {
                    // JSON string                
                    string treePath = "LGC_DATA.tree";
                    string namePath = "branch";
                    string tValuePath = "frame.fixedTranfParam";
                    List<bool> results = new List<bool>();

                    // Read PARAMETER FIXED
                    // UNIT RES - FIXED [TRUE/FALSE]
                    // UNIT JSON - [TRUE/FALSE]
                    var branches = ReadElementByPath(jsonFile, treePath);
                    var branch = FindElementByName(branches, namePath, frameName);
                    if (branch.ValueKind != JsonValueKind.Null)
                    {
                        var fFixedResults = ReadElementByPath(branch, tValuePath).EnumerateArray();
                        foreach (var fix in fFixedResults)
                        {
                            //bool fixed = CheckIsNull(fFixedResults);
                            results.Add(Convert.ToBoolean(fix.ToString()));
                        }
                    }
                    return results;
                }

                public static List<(string, DoubleValue, DoubleValue, DoubleValue)> GetFrameOBSXYZ(JsonElement jsonFile, string frameName)
                {

                    // JSON string
                    string treePath = "LGC_DATA.tree";
                    string branchPath = "branch";
                    string obsPath = "measurements.fOBSXYZ";
                    string namePath = "targetPos";
                    string resXPath = "fXResidual";
                    string sigXPath = "fXSigmaObsVal";
                    string resYPath = "fYResidual";
                    string sigYPath = "fYSigmaObsVal";
                    string resZPath = "fZResidual";
                    string sigZPath = "fZSigmaObsVal";

                    // Read PARAMETER FIXED - all are in M
                    // UNIT RES - SIGMA and RESIDU [MM]
                    // UNIT JSON - everything in [M]
                    List<(string, DoubleValue, DoubleValue, DoubleValue)> results = new List<(string, DoubleValue, DoubleValue, DoubleValue)>();

                    var branches = ReadElementByPath(jsonFile, treePath);
                    var branch = FindElementByName(branches, branchPath, frameName);
                    if (branch.ValueKind != JsonValueKind.Null
                        && TryReadElementByPath(branch, obsPath, out JsonElement obsArray))
                    {
                        foreach (var obs in obsArray.EnumerateArray())
                        {
                            string name = GetStringValue(ReadElementByPath(obs, namePath));
                            double xVal = GetDoubleValue(ReadElementByPath(obs, resXPath));
                            double sX = GetDoubleValue(ReadElementByPath(obs, sigXPath));
                            double yVal = GetDoubleValue(ReadElementByPath(obs, resYPath));
                            double sY = GetDoubleValue(ReadElementByPath(obs, sigYPath));
                            double zVal = GetDoubleValue(ReadElementByPath(obs, resZPath));
                            double sZ = GetDoubleValue(ReadElementByPath(obs, sigZPath));
                            results.Add((name, new DoubleValue(xVal, sX), new DoubleValue(yVal, sY), new DoubleValue(zVal, sZ)));
                        }
                    }
                    return results;

                }


                public static List<(string, DoubleValue)> GetFrameIncly(JsonElement jsonFile, string frameName)
                {
                    // JSON string
                    string treePath = "LGC_DATA.tree";
                    string branchPath = "branch";
                    string obsPath = "measINCLY.[0]";
                    string namePath = "targetPos";
                    string anglePath = "angles.[0].fValue";
                    string angleResPath = "anglesResiduals.[0].fValue";
                    string angleSigmaPath = "instrument.sigmaAngl";
                    string inclyPath = "measurements.fINCLY.[0]";

                    // Read PARAMETER FIXED
                    // UNIT RES - OBSERVE[GON], RESIDU [CC]
                    // UNIT JSON - in [RAD]
                    var branches = ReadElementByPath(jsonFile, treePath);
                    var branch = FindElementByName(branches, branchPath, frameName);
                    string name = "";
                    double angle = na;
                    double res = na;
                    double sigma = na;
                    List<(string, DoubleValue)> inclys = new List<(string, DoubleValue)>();



                    try
                    {
                        if (branch.ValueKind != JsonValueKind.Null
                            && TryReadElementByPath(branch, inclyPath, out var incly))
                        {
                            var obs = ReadElementByPath(incly, obsPath);
                            name = GetStringValue(ReadElementByPath(obs, namePath));
                            angle = GetGonValue(ReadElementByPath(obs, anglePath));
                            res = GetDoubleValue(ReadElementByPath(obs, angleResPath));
                            sigma = GetGonValue(ReadElementByPath(incly, angleSigmaPath));
                            inclys.Add((name, new DoubleValue(angle, sigma)));
                        }
                    }
                    catch (Exception ex)
                    {
                        // just means not Inlcy are available?
                    }
                    return inclys;
                }
                #endregion
            }

            #endregion

            #region Wire/Line/Ecarto by Phillipe
            public static List<string> GetLinesBetween2KeyWords(ref List<string> lines, string FirstKey, string SecondKey, bool dontLookForSpace = false)
            {
                try
                {
                    int changingIndex = 0;
                    MovetoKey(lines, FirstKey, ref changingIndex, dontLookForSpace);
                    // remove the useless line of the beginning
                    lines = lines.Skip(changingIndex).ToList();
                    // Goto the second key word
                    changingIndex = 0;
                    MovetoKey(lines, SecondKey, ref changingIndex, dontLookForSpace);
                    // select the wanted lines
                    List<string> wantedLines = lines.Take(changingIndex).ToList();
                    // return the rest of the lines
                    lines = lines.Skip(changingIndex).ToList();
                    // return the wanted lines
                    return wantedLines;
                }
                catch (Exception ex)
                {
                    if (FirstKey == "POINTS VARIABLES" || FirstKey == "POINTS INVARIABLES EN Z")
                    {
                        ex.Data.Clear();
                        return null;
                    }

                    throw new Exception($"{R.T_CANNOT_FIND} '{FirstKey}' our '{SecondKey}'", ex);
                }

            }
            public static void MovetoKey(List<string> lines, string key, ref int changingIndex, bool dontLookForSpace = false)
            {
                bool found = false;

                if (dontLookForSpace)
                {
                    List<string> keySplit = key.Split(' ').Where(x => x != "").ToList();
                    while (!found)
                    {
                        if (changingIndex < lines.Count)
                        {
                            string line = lines[changingIndex].Trim();
                            int totalLength = 0;
                            List<bool> foundList = new List<bool>();
                            foreach (string individualKey in keySplit)
                            {
                                totalLength += individualKey.Length;
                                foundList.Add(line.Contains(individualKey));
                            }
                            if (line.Length >= totalLength)
                            {
                                if (foundList.Count(x => x == true) == foundList.Count)
                                    found = true;
                                else
                                    changingIndex += 1;
                            }
                            else
                            {
                                changingIndex += 1;
                            }
                        }
                        else
                        {
                            throw new Exception(string.Format(R.T050, key));
                        }
                    }
                }
                else
                {
                    while (!found)
                    {
                        string modifieddKey = key;
                        if (changingIndex < lines.Count)
                        {
                            string line = lines[changingIndex].Trim();

                            bool mustBeginTheLine = modifieddKey[0] == '^';
                            bool match = false;
                            if (mustBeginTheLine)
                            {
                                modifieddKey = key.Substring(1);
                                match = line.StartsWith(modifieddKey);
                            }
                            else
                                match = line.Contains(modifieddKey);

                            if (line.Length >= modifieddKey.Length && match)
                                found = true;
                            else
                                changingIndex += 1;
                        }
                        else
                        {
                            if (modifieddKey == "DISTANCES MESUREES") return;
                            throw new Exception(string.Format(R.T050, modifieddKey));
                        }
                    }
                }
            }
            public static string GetTokenFromLine(string line, int tokenNumber)
            {
                try
                {
                    line = Regex.Replace(line.Trim(), @"\s+", " ");
                    string[] tokens = line.Split(' ');
                    if (tokens.Length >= tokenNumber)
                        return tokens[tokenNumber - 1];
                    else
                        return "";
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format(R.T053, line, ex.Message), ex);
                }
            }

            public DoubleValue LineToV0(string line)
            {
                try
                {
                    DoubleValue content = new DoubleValue();
                    line = Regex.Replace(line.Trim(), @"\s+", " ");
                    string[] tokens = line.Split(' ');
                    int count = tokens.Count();
                    content.Value = Numbers.ToDouble(tokens[count - 4]);
                    content.Sigma = Numbers.ToDouble(tokens[count - 1]) / 10000;
                    return content;
                }
                catch (Exception ex)
                {
                    throw new Exception(R.T054 + ex.Message, ex);
                }
            }

            public DoubleValue LineToHt(string line)
            {
                try
                {
                    DoubleValue content = new DoubleValue();
                    line = Regex.Replace(line.Trim(), @"\s+", " ");
                    string[] tokens = line.Split(' ');
                    int count = tokens.Count();
                    content.Value = Numbers.ToDouble(tokens[count - 6]);
                    content.Sigma = Numbers.ToDouble(tokens[count - 3]);
                    return content;
                }
                catch (Exception ex)
                {
                    throw new Exception(R.T055 + ex.Message, ex);
                }
            }

            public void Unused_GetOffsetResultsForWire(Line.Station stationLine)
            //met à jour la station line en fonction de l'output de LGC2 et affiche l'input et output LGC2 si showfile est true
            {
                //try
                //{
                //    string outputFilePath = lgc2._FileName + ".res";
                //    string inputFilePath = lgc2._FileName + ".inp";
                //    // test
                //    if (!File.Exists(outputFilePath)) throw new Exception(R.String_LGC2_MissingFile);
                //    List<string> listFileContent = File.ReadLines(outputFilePath).ToList();
                //    //permet de vérifier que tous les points ne sont pas en Cala
                //    int nombrePointCala = 0;
                //    //int nbVarPtWithReading = 0;
                //    foreach (MeasureOfOffset item in stationLine._MeasureOffsets)
                //    {
                //        if (item._Point.LGCFixOption == LgcPointOption.CALA && !(item._Status is M.States.Bad))
                //        {
                //            nombrePointCala++;
                //        }
                //        //else
                //        //{
                //        //    if (item._RawReading != na && !(item._Status is MeasureStateBad))
                //        //    {
                //        //        nbVarPtWithReading++;
                //        //    }
                //        //}
                //    }
                //    if (nombrePointCala != stationLine._MeasureOffsets.Count)// && nbVarPtWithReading != 0)
                //    {
                //        //pas de points déplacés
                //        OutputRES.GetWireDisplacement(listFileContent, stationLine); 
                //        JsonElement jsonFile = Lgc2.Output.ParserJSON.ReadJSONFile(lgc2._FileName + ".json");
                //        OutputJSON.GetWireDisplacement(jsonFile, stationLine);
                //    }
                //    GetWireResidualOnCalapoints(listFileContent, stationLine);
                //}
                //catch (Exception e)
                ////Exception générée si fichier inexistant
                //{
                //    if (e.Message == R.String_LGC2_MissingFile)
                //    {
                //        throw;
                //    }
                //    else
                //    {
                //        e.Data.Clear();
                //    }
                //    ///Remet les rawReading à -9999 sur les fausses mesures pour calcul des lectures theo
                //    foreach (MeasureOfOffset item in stationLine._MeasureOffsets)
                //    {
                //        if (item._IsFakeRawReading)
                //        {
                //            item._RawReading = na;
                //            item._IsFakeRawReading = false;
                //        }
                //    }
                //}
            }

            /// <summary>
            /// Recupere les coordonnées calculées du fil et les met dans les coordonnées mesurées pour l'élément module 
            /// </summary>
            /// <param name="stationLineModule"></param>
            public void GetCoordinatesResultsForWire(Line.Station.Module stationLineModule)
            {
                try
                {
                    string resFilePath = lgc2._FileName + ".res";
                    // test
                    if (!File.Exists(resFilePath)) throw new Exception(R.String_LGC2_MissingFile);
                    List<string> listFileContent = File.ReadLines(resFilePath).ToList();
                    ///Si calcul en libre pas d'ajout des points dans l'élément manager
                    if (!listFileContent.Exists(x => x.Contains("RESEAU COMPLETEMENT LIBRE")))
                    {
                        JsonElement jsonFile = ParserJSON.ReadJSONFile(lgc2._FileName + ".JSON");
                        OutputJSON.GetWireCoordinates(jsonFile, stationLineModule);
                    }
                }
                catch (Exception e)
                //Exception générée si fichier inexistant
                {
                    if (e.Message == R.String_LGC2_MissingFile)
                    {
                        throw;
                    }
                    else
                    {
                        e.Data.Clear();
                    }
                }
            }
            /// <summary>
            /// Récupère les coordonnées calculées qui ne sont pas en cala et les stocke dans la liste des coordonnées mesurées de la stationLineModule
            /// </summary>
            /// <param name="listFileContent"></param>
            /// <param name="stationLineModule"></param>

            /// <summary>
            /// Lit les coord dans le fichier result pour un calcul LGC Global
            /// </summary>
            /// <param name="lineModule"></param>
            /// <returns></returns>
            public List<Point> GetCoordinatesResultsForWire(Line.Module lineModule)
            {
                List<Point> pointsList = new List<Point>();
                try
                {
                    string resFilePath = lgc2._FileName + ".res";
                    // test
                    if (!File.Exists(resFilePath)) throw new Exception(R.String_LGC2_MissingFile);
                    List<string> listFileContent = File.ReadLines(resFilePath).ToList();
                    ///Si calcul en libre pas d'ajout des points dans l'élément manager
                    if (!listFileContent.Exists(x => x.Contains("RESEAU COMPLETEMENT LIBRE")))
                    {
                        //pointList = GetWireCoordinates(listFileContent, lineModule);
                        JsonElement jsonFile = ParserJSON.ReadJSONFile(lgc2._FileName + ".JSON");
                        foreach (Line.Station.Module stationLineModule in lineModule.StationModules)
                        {
                            OutputJSON.GetWireCoordinates(jsonFile, stationLineModule);
                            foreach(Point point in stationLineModule._MeasPoint)
                            {
                                if (!pointsList.Exists(x=>x._Name == point._Name))
                                    pointsList.Add(point);
                            }
                        }
                        
                    }
                    return pointsList;
                }
                catch (Exception e)
                //Exception générée si fichier inexistant
                {
                    if (e.Message == R.String_LGC2_MissingFile)
                    {
                        throw;
                    }
                    else
                    {
                        e.Data.Clear();
                    }
                    return pointsList;
                }
            }
            internal List<Point> GetCoordinatesResultsForWire(Guided.Group.Module guidedGroupWireModule)
            {
                Line.Module lineModule = new Line.Module();
                foreach (Guided.Module gm in guidedGroupWireModule.SubGuidedModules)
                {
                    if (gm.guideModuleType == GuidedModuleType.Ecartometry)
                    {
                        lineModule.StationModules.Add(gm._ActiveStationModule);
                    }
                }
                return GetCoordinatesResultsForWire(lineModule);
            }
            /// <summary>
            /// Récupère les coordonnées calculées qui ne sont pas en cala et les stocke dans la liste des coordonnées mesurées de la stationLineModule
            /// </summary>
            /// <param name="listFileContent"></param>
            /// <param name="stationLineModule"></param>
            private List<Point> GetWireCoordinates(List<string> listFileContent, Line.Module lineModule)
            {
                List<string> pointsNamesAlreadyUsed = new List<string>();
                List<string> fileVarCoord = GetLinesBetween2KeyWords(ref listFileContent, "POINTS INVARIABLES EN Z", "SFP = Sub-Frame Point", true);
                List<Point> newListPoints = new List<Point>();
                CoordinatesType coordType = CoordinatesType.CCS;
                foreach (Line.Station.Module stLineMod in lineModule.StationModules.OfType<Line.Station.Module>())
                {
                    foreach (Point item in stLineMod._MeasPoint)
                    {
                        if (pointsNamesAlreadyUsed.FindIndex(x => x == item._Name) == -1)
                        {
                            pointsNamesAlreadyUsed.Add(item._Name);
                            Point newPt = item.DeepCopy();
                            newListPoints.Add(newPt);
                        }
                    }
                    coordType = stLineMod.WorkingAverageStationLine._Parameters._CoordType;
                }
                foreach (string stringLine in fileVarCoord)
                {
                    foreach (Point item in newListPoints)
                    {
                        if (item._Name == GetTokenFromLine(stringLine, 1))
                        {

                            double x = Numbers.ToDouble(GetTokenFromLine(stringLine, 2), false, -9999);
                            double sx = P.Preferences.NotAvailableValueNa;
                            double y = Numbers.ToDouble(GetTokenFromLine(stringLine, 3), false, -9999);
                            double sy = P.Preferences.NotAvailableValueNa;
                            //if ((double.TryParse(GetTokenFromLine(stringLine, 2), out x))&&(double.TryParse(GetTokenFromLine(stringLine, 3), out y))&&(double.TryParse(GetTokenFromLine(stringLine, 5), out sx))&&(double.TryParse(GetTokenFromLine(stringLine, 6), out sy)))
                            if (x != -9999 && y != -9999)
                            {
                                switch (coordType)
                                {
                                    case CoordinatesType.CCS:
                                        item._Coordinates.Ccs.X.Value = x;
                                        item._Coordinates.Ccs.X.Sigma = sx; //sigma en mm
                                        item._Coordinates.Ccs.Y.Value = y;
                                        item._Coordinates.Ccs.Y.Sigma = sy; //sigma en mm
                                        break;
                                    case CoordinatesType.SU:
                                        Coordinates newCoord = item._Coordinates.Local;
                                        newCoord.X.Value = x;
                                        newCoord.X.Sigma = sx; //sigma en mm
                                        newCoord.Y.Value = y;
                                        newCoord.Y.Sigma = sy;//sigma en mm
                                        item.SetCoordinates(newCoord, newCoord.ReferenceFrame, newCoord.CoordinateSystem);
                                        break;
                                    case CoordinatesType.UserDefined:
                                        break;
                                    default:
                                        break;
                                }
                            }
                            item._Origin = "GlobalEcartometryLGC2Calculation";
                        }
                    }
                }
                newListPoints.RemoveAll(x => x._Origin != "GlobalEcartometryLGC2Calculation");
                return newListPoints;
            }
            private void GetWireResidualOnCalapoints(List<string> listFileContent, Line.Station stationLine)
            //Récupère les résidus sur les points de cala et calcule la lecture théorique à avoir
            {
                List<string> fileEchoResiduals = new List<string>();
                fileEchoResiduals = GetLinesBetween2KeyWords(ref listFileContent, "POSITION            OBSERVE     SIGMA       CALCULE    RESIDU   RES/SIG", "INSTRUMENT EDM:", true);
                foreach (string stringLine in fileEchoResiduals)
                {
                    if (stationLine._Anchor1._Point._Name == GetTokenFromLine(stringLine, 1) && stationLine._Anchor1._Point.LGCFixOption == LgcPointOption.CALA)
                    {
                        double theoreticalDeviation = Numbers.ToDouble(GetTokenFromLine(stringLine, 5), false, -9999);
                        if (theoreticalDeviation != -9999)
                        {
                            stationLine._Anchor1._TheoreticalDeviation = -theoreticalDeviation / 1000;
                            stationLine._Anchor1._TheoreticalReading = stationLine._Anchor1._CorrectedReading - stationLine._Anchor1._TheoreticalDeviation;
                            stationLine._Anchor1._AverageTheoreticalReading = stationLine._Anchor1._TheoreticalReading;
                            stationLine._Anchor1._AverageDeviation = stationLine._Anchor1._TheoreticalDeviation;
                        }
                    }

                    if (stationLine._Anchor2._Point._Name == GetTokenFromLine(stringLine, 1) && stationLine._Anchor2._Point.LGCFixOption == LgcPointOption.CALA)
                    {
                        double theoreticalDeviation = Numbers.ToDouble(GetTokenFromLine(stringLine, 5), false, -9999); ;
                        if (theoreticalDeviation != -9999)
                        {
                            stationLine._Anchor2._TheoreticalDeviation = -theoreticalDeviation / 1000;
                            stationLine._Anchor2._TheoreticalReading = stationLine._Anchor2._CorrectedReading - stationLine._Anchor2._TheoreticalDeviation;
                            stationLine._Anchor2._AverageTheoreticalReading = stationLine._Anchor2._TheoreticalReading;
                            stationLine._Anchor2._AverageDeviation = stationLine._Anchor2._TheoreticalDeviation;
                        }
                    }

                    foreach (MeasureOfOffset item in stationLine._MeasureOffsets)
                    {
                        if (item._Point._Name == GetTokenFromLine(stringLine, 1) && item._Point.LGCFixOption == LgcPointOption.CALA)
                        {
                            double theoreticalDeviation = Numbers.ToDouble(GetTokenFromLine(stringLine, 5), false, -9999); ;
                            if (theoreticalDeviation != -9999)
                            {
                                item._TheoreticalDeviation = -theoreticalDeviation / 1000;
                                item._TheoreticalReading = item._CorrectedReading - item._TheoreticalDeviation;
                                item._AverageTheoreticalReading = item._TheoreticalReading;
                                item._AverageDeviation = item._TheoreticalDeviation;
                            }
                        }
                    }
                }
            }


            internal void GetResultsForLevelling(Level.Station.Module stationLevelModule)
            //met à jour la station level en fonction de l'output de LGC2 et affiche l'input et output LGC2 si showfile est true
            {
                try
                {
                    string inputFilePath = lgc2._FileName + ".inp";
                    stationLevelModule._WorkingStationLevel.ParametersBasic._LGCInputFilePath = inputFilePath;
                    stationLevelModule._WorkingStationLevel._Parameters._LGCInputFilePath = inputFilePath;
                    stationLevelModule._Station.ParametersBasic._LGCInputFilePath = inputFilePath;

                    stationLevelModule._WorkingStationLevel.ParametersBasic._LGCOutputFilePath = this.outputFilePath;
                    stationLevelModule._WorkingStationLevel._Parameters._LGCOutputFilePath = this.outputFilePath;
                    stationLevelModule._Station.ParametersBasic._LGCOutputFilePath = this.outputFilePath;

                    string jsonFilePath = lgc2._FileName + ".JSON";
                    if (!File.Exists(jsonFilePath)) throw new Exception(R.String_LGC2_MissingFile);
                    JsonElement jsonContent = ParserJSON.ReadJSONFile(jsonFilePath);
                    OutputJSON.SetLevel(jsonContent, stationLevelModule);
                }
                catch (Exception e)
                //Exception générée si fichier inexistant
                {
                    if (e.Message == R.String_LGC2_MissingFile)
                    {
                        throw;
                    }
                    else
                    {
                        e.Data.Clear();
                    }
                }
            }

            /// <summary>
            /// Get the levelling result for a level module with several stations
            /// </summary>
            /// <param name="levelModule"></param>
            /// <returns></returns>
            internal List<Point> GetResultsForLevelling(Level.Module levelModule)
            {
                try
                {
                    string jsonFilePath = lgc2._FileName + ".JSON";
                    if (!File.Exists(jsonFilePath)) throw new Exception(R.String_LGC2_MissingFile);
                    JsonElement json = ParserJSON.ReadJSONFile(jsonFilePath);
                    return OutputJSON.SetLevel(json, levelModule);
                }
                catch (Exception e)
                //Exception générée si fichier inexistant
                {
                    if (e.Message == R.String_LGC2_MissingFile)
                    {
                        throw;
                    }
                    else
                    {
                        e.Data.Clear();
                    }
                    return new List<Point>();
                }
            }

            internal List<Point> GetResultsForGuidedGroupModule(Guided.Group.Module guidedGroupModule, GuidedModuleType guidedModuleType)
            {
                List<Point> pointList = new List<Point>();
                switch (guidedModuleType)
                {
                    case GuidedModuleType.NoType:
                        break;
                    case GuidedModuleType.Alignment:
                        break;
                    case GuidedModuleType.AlignmentEcartometry:
                        break;
                    case GuidedModuleType.AlignmentImplantation:
                        break;
                    case GuidedModuleType.AlignmentLevelling:
                        break;
                    case GuidedModuleType.AlignmentManager:
                        break;
                    case GuidedModuleType.AlignmentTilt:
                        break;
                    case GuidedModuleType.AlignmentLevellingCheck:
                        pointList = this.GetResultsForLevelling(guidedGroupModule);
                        break;
                    case GuidedModuleType.Cheminement:
                        pointList = this.GetResultsForLevelling(guidedGroupModule);
                        break;
                    case GuidedModuleType.Ecartometry:
                        pointList = this.GetCoordinatesResultsForWire(guidedGroupModule);
                        break;
                    case GuidedModuleType.Example:
                        break;
                    case GuidedModuleType.Implantation:
                        break;
                    case GuidedModuleType.PointLance:
                        break;
                    case GuidedModuleType.PreAlignment:
                        break;
                    case GuidedModuleType.Survey1Face:
                        break;
                    case GuidedModuleType.Survey2Face:
                        break;
                    case GuidedModuleType.SurveyTilt:
                        break;
                    case GuidedModuleType.TourDHorizon:
                        break;
                    default:
                        break;
                }
                return pointList;
            }
            internal List<Point> GetResultsForLevelling(Guided.Group.Module guidedGroupLevelModule)
            {
                Level.Module levelModule = new Level.Module();
                foreach (Guided.Module gm in guidedGroupLevelModule.SubGuidedModules)
                {
                    if (gm.guideModuleType == GuidedModuleType.Cheminement
                        || gm.guideModuleType == GuidedModuleType.AlignmentLevellingCheck
                        || gm.guideModuleType == GuidedModuleType.AlignmentLevelling)
                    {
                        levelModule.StationModules.Add(gm._ActiveStationModule);
                    }
                }
                return GetResultsForLevelling(levelModule);
            }

            //private void GetLevelResult(List<string> listFileContent, Level.Station.Module stationLevelModule)

            //{
            //    List<string> fileVarCoord = new List<string>();
            //    fileVarCoord = GetLinesBetween2KeyWords(ref listFileContent, "POINTS VARIABLES EN Z UNIQUEMENT   (NB.", "SFP = Sub-Frame Point", true);
            //    foreach (string stringLine in fileVarCoord)
            //    {
            //        if (stationLevelModule._WorkingStationLevel._Name == GetTokenFromLine(stringLine, 1))
            //        {
            //            double hStation = Numbers.ToDouble(GetTokenFromLine(stringLine, 4), false, -9999);
            //            if (hStation != -9999)
            //            {
            //                stationLevelModule._WorkingStationLevel._Parameters._HStation = hStation;
            //            }
            //        }
            //    }
            //    stationLevelModule.TheoReadingAndResidualsCalculation();
            //}

            /// <summary>
            /// Recupère les résultats de nivellement dans le cas d'un calcul LGC2 global.
            /// </summary>
            /// <param name="listFileContent"></param>
            /// <param name="levelModule"></param>
            /// <returns></returns>
            private List<Point> GetLevelResult(List<string> listFileContent, Level.Module levelModule)
            {
                List<string> fileVarCoord = new List<string>();
                List<Point> pointList = new List<Point>();
                fileVarCoord = GetLinesBetween2KeyWords(ref listFileContent, "POINTS VARIABLES EN Z UNIQUEMENT   (NB.", "SFP = Sub-Frame Point", true);
                foreach (string stringLine in fileVarCoord)
                {
                    foreach (Level.Station.Module stl in levelModule.StationModules.OfType<Level.Station.Module>())
                    {
                        string s = GetTokenFromLine(stringLine, 1);                        
                        foreach (Level.Station str in stl.AllStationLevels)
                            if (str._Name == s)
                            {
                                Point pt = new Point();
                                pt._Name = s;
                                double hStation = Numbers.ToDouble(GetTokenFromLine(stringLine, 4), false, -9999);
                                SetHToPoint(str, pt, hStation);
                                pointList.Add(pt);
                            }
                        foreach (Point pt in stl._TheoPoint)
                            if (pt._Name == s && pointList.FindIndex(x => x._Name == pt._Name) == -1)
                            {
                                Point ptCopy = pt.DeepCopy();
                                double hpt = Numbers.ToDouble(GetTokenFromLine(stringLine, 4), false, -9999);

                                SetHToPoint(stl._WorkingStationLevel, ptCopy, hpt);
                                pointList.Add(ptCopy);
                            }
                    }
                }
                return pointList;
            }
            /// <summary>
            /// Set the Hstastion to the appropriate coordinates in the point.
            /// </summary>
            /// <param name="stl"></param>
            /// <param name="point"></param>
            /// <param name="hToSet"></param>
            private static void SetHToPoint(Level.Station stationLevel, Point point, double hToSet)
            {

                if (hToSet != na)
                {
                    switch (stationLevel._Parameters._ZType)
                    {
                        case CoordinatesType.CCS:
                            point._Coordinates.Ccs.X.Value = 0;
                            point._Coordinates.Ccs.Y.Value = 0;
                            point._Coordinates.Ccs.Z.Value = hToSet;
                            break;
                        case CoordinatesType.SU:
                            point._Coordinates.Su.X.Value = 0;
                            point._Coordinates.Su.Y.Value = 0;
                            point._Coordinates.Su.Z.Value = hToSet;
                            break;
                        case CoordinatesType.UserDefined:
                            break;
                        default:
                            break;
                    }

                }
            }

            public static void SetDeviation(MeasureOfOffset anchor, double value)
            {
                anchor._TheoreticalDeviation = -value / 1000;
                anchor._TheoreticalReading = anchor._CorrectedReading - anchor._TheoreticalDeviation;
                anchor._AverageTheoreticalReading = anchor._TheoreticalReading;
                anchor._AverageDeviation = anchor._TheoreticalDeviation;
            }

            #endregion
        }

    }
}