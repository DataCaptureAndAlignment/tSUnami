﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using TSU.Common.Elements;
using TSU.Common.Instruments;
using static TSU.Common.Compute.Compensations.Frame.FrameMeasurement;

namespace TSU.Common.Compute.Compensations
{
    [Serializable]
    public class Frame : TsuObject
    {
        public Frame Parent { get; set; }
        public List<Frame> SubFrames { get; set; } = new List<Frame>();

        //works only with simple frame for BeamOffsetCompute
        public Frame DeeperFrame
        {
            get
            {
                if (IsLeaf)
                    return this;

                Frame lastLeaf = null;
                foreach (var child in SubFrames)
                {
                    if (child.IsLeaf)
                        lastLeaf = child;
                    else
                        lastLeaf = child.DeeperFrame;
                }

                return lastLeaf;
            }
        }

        public bool IsLeaf
        {
            get { return SubFrames == null || SubFrames.Count == 0; }
        }

        [XmlAttribute]
        public double Sigma0 { get; set; }

        [XmlAttribute]
        public bool Active = true;

        public Parameters EstimatedParameters;
        public Parameters ProvisoryParameters;
        public FrameMeasurement Measurements;
        public List<ObsXYZ> CalagePoints;

        public Frame()
        {

        }

        public override string ToString()
        {
            return $"Frame: {this._Name}";
        }
        public object Clone()
        {
            Frame clone = this.MemberwiseClone() as Frame;
            clone.Parent = this.Parent;
            
            if (this.EstimatedParameters != null)
                clone.EstimatedParameters = this.EstimatedParameters.Clone() as Parameters;

            if (this.ProvisoryParameters != null)
                clone.ProvisoryParameters = this.ProvisoryParameters.Clone() as Parameters;

            if (this.Measurements != null)
                clone.Measurements = this.Measurements.Clone() as FrameMeasurement;

            clone.SubFrames = new List<Frame>();
            foreach (var item in SubFrames)
            {
                clone.SubFrames.Add(item.Clone() as Frame);
            }

            return clone;

        }


        internal static void FromJson_GetFrameResults(string lgcJsonPath, string frameName, ref Frame frame, out double sigma0, out List<Point> points)
        {
            try
            {
                Lgc2.Output.OutputJSON.SetFrameResults(lgcJsonPath, frameName,
                    out DoubleValue TX, out DoubleValue TY, out DoubleValue TZ,
                    out DoubleValue RX, out DoubleValue RY, out DoubleValue RZ,
                    out DoubleValue EstimatedScale,
                    out bool fixedTX, out bool fixedTY, out bool fixedTZ,
                    out bool fixedRX, out bool fixedRY, out bool fixedRZ,
                    out bool fixedScale,
                    out List<(string, DoubleValue, DoubleValue, DoubleValue)> obsXyzResiduals,
                    out List<(string, DoubleValue)> inclys, out sigma0, out points);

                Survey.TransformValueToBeBetween200AndMinus200(RX);
                Survey.TransformValueToBeBetween200AndMinus200(RY);
                Survey.TransformValueToBeBetween200AndMinus200(RZ);
                foreach (var item in inclys)
                {
                    Survey.TransformValueToBeBetween200AndMinus200(item.Item2);
                }


                frame.EstimatedParameters = new Parameters()
                {
                    TX = new Parameters.Parameter()
                    {
                        Value = TX.Value,
                        Sigma = TX.Sigma,
                        IsFixed = fixedTX
                    },
                    TY = new Parameters.Parameter()
                    {
                        Value = TY.Value,
                        Sigma = TY.Sigma,
                        IsFixed = fixedTY
                    },
                    TZ = new Parameters.Parameter()
                    {
                        Value = TZ.Value,
                        Sigma = TZ.Sigma,
                        IsFixed = fixedTZ
                    },
                    RX = new Parameters.Parameter()
                    {
                        Value = RX.Value,
                        Sigma = RX.Sigma,
                        IsFixed = fixedRX
                    },
                    RY = new Parameters.Parameter()
                    {
                        Value = RY.Value,
                        Sigma = RY.Sigma,
                        IsFixed = fixedRY
                    },
                    RZ = new Parameters.Parameter()
                    {
                        Value = RZ.Value,
                        Sigma = RZ.Sigma,
                        IsFixed = fixedRZ
                    },
                    Scale = new Parameters.Parameter()
                    {
                        Value = EstimatedScale.Value,
                        Sigma = EstimatedScale.Sigma,
                        IsFixed = fixedScale
                    }
                };
                if (frame.Measurements == null)
                    frame.Measurements = new FrameMeasurement();
                foreach (var item in obsXyzResiduals)
                {

                    ObsXYZ obs = frame.Measurements.GetObsXyzByName(item.Item1);
                    if (obs == null)
                    {
                        obs = new ObsXYZ() { Point = item.Item1 };
                        frame.Measurements.ObsXyzs.Add(obs);
                    }
                    obs.X.Residual = item.Item2.Value;
                    obs.Y.Residual = item.Item3.Value;
                    obs.Z.Residual = item.Item4.Value;
                }

                foreach (var incly in inclys)
                {
                    Incly existingIncly = frame.Measurements.GetInclyByName(incly.Item1);
                    if (existingIncly == null)
                    {
                        existingIncly = new Incly() { Point = incly.Item1 };
                        frame.Measurements.Inclinaisons.Add(existingIncly);
                    }
                    existingIncly.Roll.Value = incly.Item2.Value;
                    existingIncly.Roll.Sigma = incly.Item2.Sigma;
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Couldn't extract frame values from JSON", ex);
            }
        }



        [Serializable]
        public class FrameMeasurement
        {


            public List<Incly> Inclinaisons { get; set; } = new List<Incly>();

            public List<ObsXYZ> ObsXyzs { get; set; } = new List<ObsXYZ>();

            public FrameMeasurement()
            {
            }

            public override string ToString()
            {
                return $"Meas: {ObsXyzs.Count} OBSXYZ, {Inclinaisons.Count} Incly";
            }
            public ObsXYZ GetObsXyzByName(string name)
            {
                return ObsXyzs.Find(x => x.Point == name);
            }

            public Incly GetInclyByName(string name)
            {
                return Inclinaisons.Find(x => x.Point == name);
            }

            internal FrameMeasurement Clone()
            {
                FrameMeasurement clone = this.MemberwiseClone() as FrameMeasurement;

                clone.ObsXyzs = new List<ObsXYZ>();
                foreach (var item in this.ObsXyzs)
                {
                    clone.ObsXyzs.Add(item.Clone() as ObsXYZ);
                }

                clone.Inclinaisons = new List<Incly>();
                foreach (var item in this.Inclinaisons)
                {
                    clone.Inclinaisons.Add(item.Clone() as Incly);
                }

                return clone;
            }

            [Serializable]
            public class Incly
            {
                [XmlAttribute]
                public string Instrument { get; set; }
                [XmlAttribute]
                public string Point { get; set; }

                [XmlAttribute]
                public bool IsAResultFromComputation { get; set; } = false;

                [XmlAttribute]
                public bool Used { get; set; } = true;

                /// <summary>
                /// stored in RAD
                /// </summary>
                public Observation Roll { get; set; } = new Observation();
                /// <summary>
                /// stored in RAD
                /// </summary>
                public Observation dRoll { get; set; } = new Observation();
                /// <summary>
                /// stored in RAD
                /// </summary>
                public DoubleValue ReferenceAngle { get; set; } = new DoubleValue();

                public Incly()
                {
                    // parameterless constructor necessary for deserialization
                }

                public override string ToString()
                {
                    string used = Used ? "Used" : "Unused";
                    return $"{used} {Point} {Roll.Value} {Roll.Sigma}";
                }

                internal static bool ExistIn(string point, List<Incly> inclys, out Incly found)
                {
                    found = inclys.FindLast(x => x.Point == point);
                    return found != null;
                }

                public object Clone()
                {
                    Incly clone = this.MemberwiseClone() as Incly;
                    clone.Roll = this.Roll.Clone() as Observation;
                    clone.dRoll = this.dRoll.Clone() as Observation;
                    return clone;
                }
            }

            [Serializable]
            public class ObsXYZ
            {
                [XmlAttribute]
                public string Point { get; set; }
                [XmlAttribute]
                public bool Used { get; set; } = true;

                public Observation X { get; set; } = new Observation();
                public Observation Y { get; set; } = new Observation();
                public Observation Z { get; set; } = new Observation();

                [XmlAttribute]
                public string Comment { get; set; }

                public object Clone()
                {
                    ObsXYZ clone = this.MemberwiseClone() as ObsXYZ;
                    clone.X = this.X.Clone() as Observation;
                    clone.Y = this.Y.Clone() as Observation;
                    clone.Z = this.Z.Clone() as Observation;
                    return clone;
                }

                public override string ToString()
                {
                    string used = Used ? "Used" : "Unused";
                    return $"{used} {Point}   {X.Value}   {Y.Value}    {Z.Value}   {X.Sigma}   {Y.Sigma}   {Z.Sigma} ";
                }

                public ObsXYZ()
                {
                    // parameterless constructor necessary for deserialization
                }
                internal static List<Frame.FrameMeasurement.ObsXYZ> GetUsedOnes(List<Frame.FrameMeasurement.ObsXYZ> obss)
                {
                    return obss.FindAll(x => x != null && x.Used == true);
                }

            }


            [Serializable]
            public class Observation : DoubleValue
            {
                public Observation()
                {
                    // parameterless constructor necessary for deserialization
                }

                [XmlAttribute]
                public double Residual { get; set; }
                public object Clone()
                {
                    Observation clone = this.MemberwiseClone() as Observation;
                    return clone;
                }

                public override string ToString()
                {
                    return $"{Value} ({Sigma}) [{Residual}]";
                }
            }
        }

        [XmlType(TypeName = "Frame.Parameters")]
        [Serializable]
        public class Parameters
        {
            //[XmlAttribute("TX2")] 
            public Parameter TX { get; set; } = new Parameter();
            public Parameter TY { get; set; } = new Parameter();
            public Parameter TZ { get; set; } = new Parameter();
            public Parameter RX { get; set; } = new Parameter();
            public Parameter RY { get; set; } = new Parameter();
            public Parameter RZ { get; set; } = new Parameter();
            public Parameter Scale { get; set; } = new Parameter();

            public Parameters()
            {
                // parameterless constructor necessary for deserialization
            }

            public object Clone()
            {
                Parameters clone = this.MemberwiseClone() as Parameters;
                clone.TX = this.TX.Clone() as Parameter;
                clone.TY = this.TY.Clone() as Parameter;
                clone.TZ = this.TZ.Clone() as Parameter;
                clone.RX = this.RX.Clone() as Parameter;
                clone.RY = this.RY.Clone() as Parameter;
                clone.RZ = this.RZ.Clone() as Parameter;
                clone.Scale = this.Scale.Clone() as Parameter;
                return clone;
            }

            public string GetListOfVariableParameters()
            {
                string line = "";
                line += TX.IsFixed ? "" : "   TX";
                line += TY.IsFixed ? "" : "   TY";
                line += TZ.IsFixed ? "" : "   TZ";

                line += RX.IsFixed ? "" : "   RX";
                line += RY.IsFixed ? "" : "   RY";
                line += RZ.IsFixed ? "" : "   RZ";

                line += Scale.IsFixed ? "" : "SCL";
                return line;
            }

            /// <summary>
            /// Fix all parameters
            /// </summary>
            public void FixAll()
            {
                RX.IsFixed = true;
                RY.IsFixed = true;
                RZ.IsFixed = true;
                TX.IsFixed = true;
                TY.IsFixed = true;
                TZ.IsFixed = true;
                Scale.IsFixed = true;
            }

            public override string ToString()
            {
                return $"TX-{TX}, TY-{TY}, TZ-{TZ}, RX-{RX}, RY-{RY}, RZ-{RZ}, SCL-{Scale}";
            }

            [Serializable]
            public class Parameter : DoubleValue
            {
                [XmlAttribute]
                public bool IsFixed { get; set; } = true;

                public Parameter()
                {
                    // parameterless constructor necessary for deserialization
                }

                public object Clone()
                {
                    Parameter clone = this.MemberwiseClone() as Parameter;
                    return clone;
                }

                public override string ToString()
                {
                    char fv = IsFixed ? 'F' : 'V';
                    return $"{fv} {Value} ({Sigma})";
                }
            }
        }


        internal static Frame GetOriginalAssemblyFrame(string idSuAssembly, BeamOffsets.Parameters parameters, Frame allAvailableFrameInARoot)
        {
            Frame root = new Frame() { _Name = "root" };

            //if (parameters != null)
            //    root.SubFrames.Add(parameters.OrigninalAssemblyFrame);
            //else
                root.SubFrames.Add(Frame.GetAssemblyFrameByName(allAvailableFrameInARoot, idSuAssembly));

            return root;
        }

        public static string ReadSuAssemblyBlockFromString(string idSuAssembly, string content)
        {
            string frame = "";

            string searchString = idSuAssembly;

            string[] lines = content.Split('\n');

            bool found = false;
            int frameCount = 0;
            int endFrameCount = 0;
            // Loop through each line
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i].Replace("\r", "");
                if (!found) // to do until the first line of the frame ending with the magnet name
                {
                    // Split the line by whitespace
                    string[] fields = line.Split(' ');
                    // Check if the line has at least two fields
                    if (fields.Length >= 2)
                    {
                        // Check if the second field ends with the search string
                        if (fields[1].EndsWith(searchString))
                        {
                            found = true;
                            frame += line + "\r\n";
                            frameCount++;
                        }
                    }
                }
                else // read all line until the all frames close.
                {
                    if (line.TrimStart().StartsWith("*FRAME"))
                        frameCount++;
                    if (line.TrimStart().StartsWith("*ENDFRAME"))
                        endFrameCount++;

                    frame += line + "\r\n";

                    if (frameCount == endFrameCount) break;
                }
            }
            return frame;
        }

        internal static string GetEmptyFrame()
        {
            return @"
% Frame for beam offsets
*FRAME	RSTI	XE	    YE	    ZE	    0	    0	    θ	    1
*FRAME	RST	    0	    0	    0	    -φ	    τ	    -α/2	1
*FRAME	RSTBI1	-BRE    BLE	    BVE	    -φB	    0	    θB	    1
*FRAME	RSTBI2	0	    0	    0	    0	    0	    α/2	    1
*FRAME	RSTB	0	    0	    0	    0	    τB	    -α/2	1
*FRAME	RSTRI	0	    0	    0	    0	    0	    0	    1  TX  TY  TZ  RX  RZ
*FRAME	RSTR	0	    0	    0	    0	    0	    0	    1  RY
*CALA				    	    			
    %Name   	R	    S	    T	    σ_X	    σ_Y	    σ_Z	
    %*CALA%						
*OBSXYZ	
    %OBSXYZ%
*ENDFRAME								
*ENDFRAME								
*ENDFRAME								
*ENDFRAME								
*ENDFRAME								
*ENDFRAME								
*ENDFRAME								
";
        }
        /// <summary>
        /// Modify the obsxyz in the frame based on the list of obspoint
        /// </summary>
        /// <param name="frame"></param>
        /// <param name="obsPoints"></param>
        /// <returns></returns>
        public static Frame ModifyObsxyzPoints(Frame frame, List<Frame.FrameMeasurement.ObsXYZ> obsPoints)
        {
            // Frame mod = frame.Clone() as Frame;
            // Frame deeperFrame = mod.DeeperFrame;
            Frame deeperFrame = frame.DeeperFrame;

            Frame.MoveAllObsxyzToCala(deeperFrame);

            foreach (var item in obsPoints)
            {
                if (item == null) continue;

                var existingObsXyz = deeperFrame.Measurements.GetObsXyzByName(item.Point);
                if (existingObsXyz != null)
                {
                    existingObsXyz.X.Sigma = item.X.Sigma;
                    existingObsXyz.Y.Sigma = item.Y.Sigma;
                    existingObsXyz.Z.Sigma = item.Z.Sigma;
                }
                else
                {
                    var existingCala = deeperFrame.GetCalaByName(item.Point);
                    if (existingCala != null)
                    {
                        if (item.Used)
                            deeperFrame.CalagePoints.Remove(existingCala);
                        deeperFrame.Measurements.ObsXyzs.Add(new ObsXYZ()
                        {
                            Used = item.Used,
                            Point = item.Point,
                            X = new Observation() { Value = existingCala.X.Value, Sigma = item.X.Sigma },
                            Y = new Observation() { Value = existingCala.Y.Value, Sigma = item.Y.Sigma },
                            Z = new Observation() { Value = existingCala.Z.Value, Sigma = item.Z.Sigma },
                            Comment = existingCala.Comment
                        });
                    }
                }
            }
            return frame;
        }

        public static Frame ModifyObsxyzOfAllFramesBasedOnParameters(string idSuAssembly, Frame rootFrame, BeamOffsets.Parameters parameters)
        {
            TSU.Debug.WriteInConsole("((BOC))   Modify Obsxyz Of All Frames Based On Parameters");

            // get right frame
            Frame frame = rootFrame.GetBOCFrameByAssemblyID(idSuAssembly);

            // update Obs
            frame = ModifyObsxyzPoints(frame, parameters.PointsToUseAsOBSXYZ);
            return rootFrame;
        }

        private Frame GetBOCFrameByAssemblyID(string assembly)
        {
            foreach (var frame in this.SubFrames)
            {
                if (frame._Name.ToUpper().Contains(assembly))
                    return frame;
            }
            return null;
        }

        public static Frame ModifyInclyForASingleAssembly(Frame frame, List<Frame.FrameMeasurement.Incly> inclys)
        {
            Frame mod = frame.Clone() as Frame;

            foreach (var incly in mod.DeeperFrame.Measurements.Inclinaisons)
            {
                if (Incly.ExistIn(incly.Point, inclys, out Incly found))
                {
                    incly.Used = found.Used;
                    incly.Instrument = found.Instrument;
                    incly.Roll = found.Roll;
                }
                else
                    incly.Used = false;
            }

            return mod;
        }

        internal static Frame ModifyInclyOfAllFrameBasedOnParameters(string idSuAssembly, Frame rootFrame, BeamOffsets.Parameters parameters)
        {
            TSU.Debug.WriteInConsole("((BOC))   Modify Incly Of All Frame Based On Parameters");
            // get right frame
            Frame frame = rootFrame.GetBOCFrameByAssemblyID(idSuAssembly);

            // update Obs
            frame = ModifyInclyForASingleAssembly(frame, parameters.Inclys);

            return frame;
        }

        private static Frame ChangeCalaToObsXyz(Frame frame, List<Frame.FrameMeasurement.ObsXYZ> obsPoints)
        {
            Frame mod = frame.Clone() as Frame;

            foreach (Frame.FrameMeasurement.ObsXYZ op in obsPoints)
            {
                ObsXYZ p = mod.GetCalaByName(op.Point);
                if (p == null)
                    throw new Exception($"{op.Point} not available in the *cala Frame");
            }
            return mod;
        }

        private ObsXYZ GetCalaByName(string pointName)
        {
            List<ObsXYZ> points = this.CalagePoints.FindAll(x => x.Point == pointName);
            if (points.Count > 1)
                throw new Exception($"{pointName} appears more than once in the {this}");
            if (points.Count == 1)
                return points[0];
            else return null;
        }

        private static void MoveAllObsxyzToCala(Frame frame)
        {
            List<ObsXYZ> calas = frame.DeeperFrame.CalagePoints;
            List<ObsXYZ> obsxyzs = frame.DeeperFrame.Measurements.ObsXyzs;
            foreach (var item in obsxyzs)
            {
                if (calas.Find(x => x.Point == item.Point) == null)
                {
                    calas.Add(item);
                }
            }
            obsxyzs.Clear();
        }

        private static void MoveObsxyzToCala(string frame)
        {
            string modifiedFrame = "";
            string obsLines = "";

            // remove and store *OBSXYZ
            bool readingObsxyzLine = false;
            foreach (var line in frame.Split('\n'))
            {
                string cleanedLine = line.Replace("\r", "");
                if (!readingObsxyzLine)
                {
                    if (cleanedLine.TrimStart().StartsWith("*OBSXYZ"))
                        readingObsxyzLine = true;
                    modifiedFrame += cleanedLine + "\r\n";
                }
                else if (readingObsxyzLine)
                {
                    if (cleanedLine.TrimStart().StartsWith("*"))
                    {
                        modifiedFrame += cleanedLine + "\r\n";
                        readingObsxyzLine = false;
                    }
                    else
                        obsLines += cleanedLine + "\r\n";
                }
            }

            // insert previous OBS in CALA
            frame = modifiedFrame;
            modifiedFrame = "";
            bool readingCalaLine = false;
            foreach (var line in frame.Split('\n'))
            {
                string cleanedLine = line.Replace("\r", "");
                if (!readingCalaLine)
                {
                    if (cleanedLine.TrimStart().StartsWith("*CALA"))
                        readingCalaLine = true;
                    modifiedFrame += cleanedLine + "\r\n";
                }
                else if (readingCalaLine)
                {
                    if (cleanedLine.TrimStart().StartsWith("*"))
                    {
                        // insert obs
                        foreach (var obsLine in obsLines.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            string cleanedObsLine = obsLine.Replace("\r", "");
                            string[] fields = cleanedObsLine.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            // remove obse
                            fields[4] = ""; fields[5] = ""; fields[6] = "";
                            // add the line
                            string calaLine = "";
                            foreach (var field in fields)
                            {
                                calaLine += field + "   ";
                            }
                            modifiedFrame += calaLine + "\r\n";

                        }

                        modifiedFrame += cleanedLine + "\r\n";
                        readingCalaLine = false;
                    }
                    else
                        modifiedFrame += cleanedLine + "\r\n";
                }
            }
        }

        internal static string AddFrameBeforeEndKeyWord(string originalInputContent, string frame)
        {
            string modifiedInputContent = "";
            foreach (var line in originalInputContent.Split('\n'))
            {
                string cleanedLine = line.Replace("\r", "");
                if (cleanedLine.TrimStart().StartsWith("*END"))
                {
                    modifiedInputContent += frame + "\r\n";
                }
                modifiedInputContent += cleanedLine + "\r\n";
            }
            return modifiedInputContent;
        }

        public static Frame FromLGCInputFormat_GetRootFrameInputFormatString(string frameContent)
        {
            // *FRAME RSTI_103035_LHC.MQML.5L5 - 1231.090090  10389.569100   2416.394520 0 0   74.733794 1
            // *FRAME RST_103035_LHC.MQML.5L5 0 0 0 .7911910531 - .41577637333 0 1
            // *FRAME RSTRI_103035_LHC.MQML.5L5 - 0.001198      0.001174 - 0.000592 .00074272462 0 - .00045093994 1 TX TY TZ RX RZ
            // *FRAME RSTR_103035_LHC.MQML.5L5 0 0 0 0 0.001336902 0 1 RY
            // * CALA
            // BEAM_LHC.MQML.5L5.S - 0.000005      4.800002      0.000003   $13135.200 693826 coordonnées théoriques dans le système RST au 23 - FEB - 2023 00:00:00
            // BEAM_LHC.BPMR.5L5.E - 0.000003 - 0.745000      0.000001   $13129.655 683395 coordonnées théoriques dans le système RST au 23 - FEB - 2023 00:00:00
            // BEAM_LHC.BPMR.5L5.S - 0.000003 - 0.745000      0.000001   $13129.655 683396 coordonnées théoriques dans le système RST au 23 - FEB - 2023 00:00:00
            // BEAM_LHC.MQML.5L5.E                                              0.000000      0.000000      0.000000   $13130.400 693825 coordonnées théoriques dans le système RST au 23 - FEB - 2023 00:00:00
            // BEAM_LHC.MCBCV.5L5.E - 0.000004      4.990007      0.000003   $13135.390 804768 coordonnées théoriques dans le système RST au 23 - FEB - 2023 00:00:00
            // BEAM_LHC.MCBCV.5L5.S - 0.000005      5.894007      0.000003   $13136.294 804769 coordonnées théoriques dans le système RST au 23 - FEB - 2023 00:00:00
            // BEAM_LHC.MCBCA.5L5.E - 0.000004      4.990007      0.000003   $13135.390 686523 coordonnées théoriques dans le système RST au 23 - FEB - 2023 00:00:00
            // BEAM_LHC.MCBCA.5L5.S - 0.000005      5.894007      0.000003   $13136.294 686524 coordonnées théoriques dans le système RST au 23 - FEB - 2023 00:00:00
            // %LHC.MQML.5L5.T - 0.360470      4.306890      0.420950 0.1 0.1 0.1   $13134.707 516196 paramètres RST 23 - FEB - 2023 00:00:00
            // * OBSXYZ
            // LHC.MQML.5L5.E      0.168800      0.601580      0.421630 0.1 0.1 0.1   $13131.001 516195 paramètres RST 23 - FEB - 2023 00:00:00
            // LHC.MQML.5L5.S      0.167070      4.306810      0.420700 0.1 0.1 0.1   $13134.706 516197 paramètres RST 23 - FEB - 2023 00:00:00
            // *INCLY W1015
            // LHC.MQML.5L5.E - 0.533487369 RF - 0.030169411 gon
            // *ENDFRAME
            // *ENDFRAME
            // *ENDFRAME
            // *ENDFRAME
            Frame root = new Frame() { _Name = "ROOT" };

            int countOpenedFrame = 0;
            int countClosedFrame = 0;

            string currentContent = "";
            foreach (var line in frameContent.Split('\n'))
            {
                string cleanedLine = line.Replace("\r", "");

                if (countOpenedFrame > countClosedFrame)
                    currentContent += cleanedLine + "\r\n";

                if (cleanedLine.TrimStart().StartsWith("*FRAME"))
                {
                    if (countOpenedFrame == 0)
                        currentContent += cleanedLine + "\r\n";
                    countOpenedFrame++;
                }

                if (cleanedLine.TrimStart().StartsWith("*ENDFRAME"))
                {
                    countClosedFrame++;
                    if (countOpenedFrame == countClosedFrame)
                    {
                        root.SubFrames.Add(GetFromStringContainingOnlyOneFrame(currentContent));
                        currentContent = "";
                        countOpenedFrame = 0;
                        countClosedFrame = 0;
                    }
                }
            }

            if (countOpenedFrame != countClosedFrame)
                throw new Exception("Not all Frames closed with a '*ENDFRAME'");

            return root;
        }

        public static Frame GetFromStringContainingOnlyOneFrame(string frameContent)
        {
            List<string> frameLines = frameContent.Split('\n').ToList();
            return FromLGC_GetFromStringContainingOnlyOneFrame(ref frameLines);
        }

        public static Frame FromLGC_GetFromStringContainingOnlyOneFrame(ref List<string> frameLines)
        {
            // *FRAME RSTI_103035_LHC.MQML.5L5 - 1231.090090  10389.569100   2416.394520 0 0   74.733794 1
            // *FRAME RST_103035_LHC.MQML.5L5 0 0 0 .7911910531 - .41577637333 0 1
            // *FRAME RSTRI_103035_LHC.MQML.5L5 - 0.001198      0.001174 - 0.000592 .00074272462 0 - .00045093994 1 TX TY TZ RX RZ
            // *FRAME RSTR_103035_LHC.MQML.5L5 0 0 0 0 0.001336902 0 1 RY
            // * CALA
            // BEAM_LHC.MQML.5L5.S - 0.000005      4.800002      0.000003   $13135.200 693826 coordonnées théoriques dans le système RST au 23 - FEB - 2023 00:00:00
            // BEAM_LHC.BPMR.5L5.E - 0.000003 - 0.745000      0.000001   $13129.655 683395 coordonnées théoriques dans le système RST au 23 - FEB - 2023 00:00:00
            // BEAM_LHC.BPMR.5L5.S - 0.000003 - 0.745000      0.000001   $13129.655 683396 coordonnées théoriques dans le système RST au 23 - FEB - 2023 00:00:00
            // BEAM_LHC.MQML.5L5.E                                              0.000000      0.000000      0.000000   $13130.400 693825 coordonnées théoriques dans le système RST au 23 - FEB - 2023 00:00:00
            // BEAM_LHC.MCBCV.5L5.E - 0.000004      4.990007      0.000003   $13135.390 804768 coordonnées théoriques dans le système RST au 23 - FEB - 2023 00:00:00
            // BEAM_LHC.MCBCV.5L5.S - 0.000005      5.894007      0.000003   $13136.294 804769 coordonnées théoriques dans le système RST au 23 - FEB - 2023 00:00:00
            // BEAM_LHC.MCBCA.5L5.E - 0.000004      4.990007      0.000003   $13135.390 686523 coordonnées théoriques dans le système RST au 23 - FEB - 2023 00:00:00
            // BEAM_LHC.MCBCA.5L5.S - 0.000005      5.894007      0.000003   $13136.294 686524 coordonnées théoriques dans le système RST au 23 - FEB - 2023 00:00:00
            // %LHC.MQML.5L5.T - 0.360470      4.306890      0.420950 0.1 0.1 0.1   $13134.707 516196 paramètres RST 23 - FEB - 2023 00:00:00
            // * OBSXYZ
            // LHC.MQML.5L5.E      0.168800      0.601580      0.421630 0.1 0.1 0.1   $13131.001 516195 paramètres RST 23 - FEB - 2023 00:00:00
            // LHC.MQML.5L5.S      0.167070      4.306810      0.420700 0.1 0.1 0.1   $13134.706 516197 paramètres RST 23 - FEB - 2023 00:00:00
            // *INCLY W1015
            // LHC.MQML.5L5.E - 0.533487369 RF - 0.030169411 // gon
            // *ENDFRAME
            // *ENDFRAME
            // *ENDFRAME
            // *ENDFRAME

            Frame frame = new Frame();

            string subFrameContent = "";
            string currentFrameContent = "";
            string tempLine = "";


            int countOpenedFrame = 0;
            int countClosedFrame = 0;

            List<string> lines = frameLines;

            // use *frame line and remove this frame lines
            tempLine += lines[0];
            lines.RemoveAt(0);
            lines.Remove(lines.Last(x => x.Contains("*ENDFRAME")));

            frame.ProvisoryParameters = FromLGC_GetProviParametersFromString(tempLine, out string frameName);
            frame._Name = frameName;

            // read inner frames and obs
            for (int i = 0; i < lines.Count - 1; i++)
            {
                string line = lines[i];
                string cleanedLine = line.Replace("\r", "");

                if (cleanedLine.TrimStart().StartsWith("*FRAME"))
                {
                    frame.SubFrames.Add(FromLGC_GetFromStringContainingOnlyOneFrame(ref lines));
                    i = -1;
                    continue;
                }
                else if (cleanedLine.TrimStart().StartsWith("*CALA"))
                {
                    frame.CalagePoints = FromLGC_GetCalaFromStrings(ref lines);
                    i = -1;
                    continue;
                }
                else if (cleanedLine.TrimStart().StartsWith("*OBSXYZ"))
                {
                    if (frame.Measurements == null)
                        frame.Measurements = new FrameMeasurement();
                    frame.Measurements.ObsXyzs = FromLGC_GetOBSXYZFromStrings(ref lines);
                    i = -1;
                    continue;
                }
                else if (cleanedLine.TrimStart().StartsWith("*INCLY"))
                {
                    frame.Measurements.Inclinaisons = FromLGC_GetINCLYFromStrings(ref lines);
                    i = -1;
                    continue;
                }

            }
            return frame;
        }

        private static List<Incly> FromLGC_GetINCLYFromStrings(ref List<string> lines)
        {
            // * INCLY W1015
            // LHC.MQML.5L5.E - 0.533487369 RF - 0.030169411 // gon
            List<Incly> inclys = new List<Incly>();
            string cleanLine = lines[0].Replace("\r", "");
            string[] fields = cleanLine.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            string inclyInstrument = fields.Length > 1 ? fields[1] : "Unknown";
            // Remove *obs line
            lines.RemoveAt(0);

            int lineReadCount = 0;
            foreach (var line in lines)
            {
                if (line.TrimStart().StartsWith("*"))
                {
                    break;
                }

                if (line.Trim() == "")
                {
                    continue;
                }

                lineReadCount++;
                fields = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (fields.Length < 4)
                    throw new Exception("INCLY line should have at least 4 fields");
                if (!double.TryParse(fields[1], out double angle)) // gon
                    throw new Exception("INCLY field #2 line should be numeric");
                if (!double.TryParse(fields[3], out double refAngle)) // gon
                    throw new Exception("INCLY field #4 line should be numeric");
                inclys.Add(new Incly()
                {
                    Instrument = inclyInstrument,
                    Point = fields[0],
                    Roll = new Observation() { Value = angle / 200 * Math.PI }, // RAD
                    ReferenceAngle = new DoubleValue() { Value = refAngle / 200 * Math.PI }, //RAD
                });
            }

            //remove cala lines that have been read
            lines.RemoveRange(0, lineReadCount);

            return inclys;
        }

        private static List<ObsXYZ> FromLGC_GetOBSXYZFromStrings(ref List<string> lines)
        {
            // * OBSXYZ
            // LHC.MQML.5L5.E      0.168800      0.601580      0.421630 0.1 0.1 0.1   $13131.001 516195 paramètres RST 23 - FEB - 2023 00:00:00

            List<ObsXYZ> obs = new List<ObsXYZ>();

            // Remove *obs line
            lines.RemoveAt(0);

            int lineReadCount = 0;
            foreach (var line in lines)
            {
                if (line.TrimStart().StartsWith("*"))
                {
                    break;
                }
                string cleanLine = line.Replace("\r", "");
                lineReadCount++;
                string[] fields;
                fields = cleanLine.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (fields.Length < 7)
                    throw new Exception("OBSXYZ line should have at least 7 fields");
                if (!double.TryParse(fields[1], out double x))
                    throw new Exception("OBSXYZ field #2 should be numeric");
                if (!double.TryParse(fields[2], out double y))
                    throw new Exception("OBSXYZ field #3 should be numeric");
                if (!double.TryParse(fields[3], out double z))
                    throw new Exception("INCLY field #4 should be numeric");
                if (!double.TryParse(fields[4], out double sx))
                    throw new Exception("INCLY field #5 should be numeric");
                if (!double.TryParse(fields[5], out double sy))
                    throw new Exception("INCLY field #6 should be numeric");
                if (!double.TryParse(fields[6], out double sz))
                    throw new Exception("INCLY field #7 should be numeric");
                obs.Add(new ObsXYZ()
                {
                    Point = fields[0],
                    X = new Observation() { Value = x, Sigma = sx / 1000 },
                    Y = new Observation() { Value = y, Sigma = sy / 1000 },
                    Z = new Observation() { Value = z, Sigma = sz / 1000 },
                    Comment = FromLGC_GetComment(fields, lastUsableField: 6)
                });
            }

            //remove cala lines that have been read
            lines.RemoveRange(0, lineReadCount);

            return obs;
        }

        private static Parameters FromLGC_GetProviParametersFromString(string tempLine, out string FrameName)
        {
            Parameters @params = new Parameters();
            string[] fields;
            fields = tempLine.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (fields.Length < 9)
                throw new Exception("FRAME line should have at least 9 fields");
            FrameName = fields[1];
            if (!double.TryParse(fields[2], out double txx))
                throw new Exception("FRAME TX (field #3) should be numeric");
            if (!double.TryParse(fields[3], out double tyy))
                throw new Exception("FRAME TY (field #4) should be numeric");
            if (!double.TryParse(fields[4], out double tzz))
                throw new Exception("FRAME TZ (field #5) should be numeric");
            if (!double.TryParse(fields[5], out double rxx))
                throw new Exception("FRAME RX (field #6) should be numeric");
            if (!double.TryParse(fields[6], out double ryy))
                throw new Exception("FRAME RY (field #7 should be numeric");
            if (!double.TryParse(fields[7], out double rzz))
                throw new Exception("FRAME RZ (field #8) should be numeric");
            if (!double.TryParse(fields[8], out double rss))
                throw new Exception("FRAME Scale (field #9) should be numeric");
            @params.TX = new Parameters.Parameter() { Value = txx };
            @params.TY = new Parameters.Parameter() { Value = tyy };
            @params.TZ = new Parameters.Parameter() { Value = tzz };
            @params.RX = new Parameters.Parameter() { Value = rxx };
            @params.RY = new Parameters.Parameter() { Value = ryy };
            @params.RZ = new Parameters.Parameter() { Value = rzz };
            @params.Scale = new Parameters.Parameter() { Value = rss };
            if (tempLine.ToUpper().Contains("TX")) @params.TX.IsFixed = false;
            if (tempLine.ToUpper().Contains("TY")) @params.TY.IsFixed = false;
            if (tempLine.ToUpper().Contains("TZ")) @params.TZ.IsFixed = false;
            if (tempLine.ToUpper().Contains("RX")) @params.RX.IsFixed = false;
            if (tempLine.ToUpper().Contains("RY")) @params.RY.IsFixed = false;
            if (tempLine.ToUpper().Contains("RZ")) @params.RZ.IsFixed = false;
            if (tempLine.ToUpper().Contains("SCL")) @params.Scale.IsFixed = false;
            return @params;
        }

        private static List<ObsXYZ> FromLGC_GetCalaFromStrings(ref List<string> lines)
        {
            List<ObsXYZ> points = new List<ObsXYZ>();

            // Remove *cala line
            lines.RemoveAt(0);

            int lineReadCount = 0;
            foreach (var line in lines)
            {
                if (line.TrimStart().StartsWith("*"))
                {
                    break;
                }
                string cleanLine = line.Replace("\r", "");
                lineReadCount++;
                string[] fields;
                fields = cleanLine.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (fields.Length < 4)
                    throw new Exception("CALA line should have at least 4 fields");
                if (fields[0].Contains("%"))
                    continue;
                if (!double.TryParse(fields[1], out double xx))
                    throw new Exception("CALA X (field #2) should be numeric");
                if (!double.TryParse(fields[2], out double yy))
                    throw new Exception("CALA Y (field #3) should be numeric");
                if (!double.TryParse(fields[3], out double zz))
                    throw new Exception("CALA Z (field #4) should be numeric");
                points.Add(new ObsXYZ()
                {
                    Point = fields[0],

                    X = new Observation() { Value = xx },
                    Y = new Observation() { Value = yy },
                    Z = new Observation() { Value = zz },
                    Comment = FromLGC_GetComment(fields, lastUsableField: 3)
                });
            }

            //remove cala lines that have been read
            lines.RemoveRange(0, lineReadCount);

            return points;
        }

        private static string FromLGC_GetComment(string[] fields, int lastUsableField)
        {
            string comment = "";
            for (int i = lastUsableField; i < fields.Length; i++)
            {
                if (i == lastUsableField) continue;
                comment += fields[i] + " ";
            }
            return comment;
        }


        public static void UpdateObsXyz(Frame frame, List<ObsXYZ> obs)
        {
            foreach (var ob in obs)
            {
                foreach (var item in frame.Measurements.ObsXyzs)
                {
                    if (item.Point == ob.Point)
                    {
                        item.X.Sigma = ob.X.Sigma;
                        item.Y.Sigma = ob.Y.Sigma;
                        item.Z.Sigma = ob.Z.Sigma;
                    }
                }
            }
        }

        public static string ToLGC_CreateInput(Frame frame, List<Point> cala, List<Point> vxy, List<Point> vz, ref string proposedName)
        {
            try
            {

                // *FRAME RSTRI_103035_LHC.MQML.5L5   -0.001198   0.001174   -0.000592   0.00074272462   0   -0.00045093994   1   TX   TY   TZ   RX   RZ 
                // *FRAME RSTR_103035_LHC.MQML.5L5   0   0   0   0   0.001336902   0   1   RY
                // *CALA
                // BEAM_LHC.MCBCA.5L5.E   -0.000004   4.990007   0.000003   $bla
                // BEAM_LHC.MCBCA.5L5.S   -0.000005   5.894007   0.000003   $bla
                // % LHC.MQML.5L5.T   -0.360470   4.306890   0.420950   0.1   0.1   0.1   $bla
                // *OBSXYZ
                // LHC.MQML.5L5.E   0.168800   0.601580   0.421630   0.1   0.1   0.1   $bla
                // LHC.MQML.5L5.S   0.167070   4.306810   0.420700   0.1   0.1   0.2   $bla
                // *INCLY W1015
                // LHC.MQML.5L5.E   -0.533487369   RF   -0.030169411
                // *ENDFRAME
                // *ENDFRAME
                string frameBlockContent = "";

                //skip root
                bool isRoot = frame._Name.ToUpper() == "ROOT";


                // add the *FRAME
                if (!isRoot)
                {
                    frameBlockContent += frame.LGCGetFrameBeginning() + "\r\n";
                }


                // add subframes
                foreach (var item in frame.SubFrames)
                {
                    string subFrameBlockContent = ToLGC_CreateInput(item, cala, vxy, vz, ref proposedName);
                    frameBlockContent += subFrameBlockContent;
                }

                if (!isRoot)
                {
                    // add *CALA
                    if (frame.CalagePoints != null && frame.CalagePoints.Count > 0)
                    {
                        frameBlockContent += $"*CALA" + "\r\n";
                        foreach (var item in frame.CalagePoints)
                        {
                            if (Point.ExistIn(item.Point, cala))
                                continue;
                            if (Point.ExistIn(item.Point, vxy))
                                continue;
                            if (Point.ExistIn(item.Point, vz))
                                continue;
                            // BEAM_LHC.MCBCA.5L5.E   -0.000004   4.990007   0.000003   $bla
                            frameBlockContent += $"{item.Point}   {item.X.ToString("m-m")}   {item.Y.ToString("m-m")}   {item.Z.ToString("m-m")}   {item.Comment}" + "\r\n";
                        }
                    }

                    // add *OBSXYZ
                    if (frame.Measurements != null && frame.Measurements.ObsXyzs.Count > 0)
                    {
                        frameBlockContent += $"*OBSXYZ" + "\r\n";
                        foreach (var item in frame.Measurements.ObsXyzs)
                        {
                            if (!item.Used)
                                frameBlockContent += "% unused %";
                            // LHC.MQML.5L5.E   0.168800   0.601580   0.421630   0.1   0.1   0.1   $bla
                            frameBlockContent += $"" +
                                $"{item.Point}   " +
                                $"{item.X.ToString("m-m")}   {item.Y.ToString("m-m")}   {item.Z.ToString("m-m")}   " +
                                $"{item.X.GetSigma("mm")}   {item.Y.GetSigma("mm")}   {item.Z.GetSigma("mm")}   " +
                                $"{item.Comment}" + "\r\n";
                        }
                    }

                    // add *INCLY
                    if (frame.Measurements != null && frame.Measurements.Inclinaisons.Count > 0)
                    {
                        bool startInbcly = true;
                        foreach (var item in frame.Measurements.Inclinaisons)
                        {
                            if (!item.Used)
                                frameBlockContent += "% unused %";
                            else
                            {
                                if (startInbcly)
                                {
                                    startInbcly = false;
                                    frameBlockContent += $"*INCLY {frame.Measurements.Inclinaisons.LastOrDefault(x=>x.Used == true).Instrument}" + "\r\n";
                                    proposedName += "Roll ";
                                }
                            }
                            // LHC.MQML.5L5.E   -0.533487369   RF   -0.030169411   % both in gon but soter in rad in tsunami
                            frameBlockContent += $"{item.Point}   {(item.Roll.Value / Math.PI * 200):F7}   RF   {item.ReferenceAngle.Value / Math.PI * 200:F7} " + $"  ${item.Roll.Value*1000:F4} mrad\r\n";
                        }
                    }

                    // close the *ENDFRAME
                    frameBlockContent += frame.LGCGetFrameEnd() + "\r\n";
                }

                return frameBlockContent;

            }
            catch (Exception ex)
            {
                throw new Exception("Failed to create the frame input for lgc2", ex);
            }
        }

        public string LGCGetFrameBeginning()
        {
            var pp = ProvisoryParameters;
            return $"*FRAME   {_Name}   {pp.TX.Value:0.000000}   {pp.TY.Value:0.000000}   {pp.TZ.Value:0.000000}   {pp.RX.Value:0.000000}   {pp.RY.Value:0.000000}   {pp.RZ.Value:0.000000}   {pp.Scale.Value:0.000000}   {pp.GetListOfVariableParameters()}";
        }

        public string LGCGetFrameEnd()
        {
            return $"*ENDFRAME      %{_Name}";
        }

        public static Frame GetAssemblyLastFrameByName(Frame frame, string id)
        {
            Frame lookedfor;

            if (frame._Name.ToUpper().Contains("RSTR_") && frame._Name.ToUpper().Contains(id))
                return frame;

            foreach (Frame item in frame.SubFrames)
            {
                lookedfor = GetAssemblyLastFrameByName(item, id);
                if (lookedfor != null)
                    return lookedfor;
            }
            return null;
        }

        internal static Frame FromGeode_GetOriginalFrames(string idAssembly, string framesPath = "", bool bMustFound = true)
        {
            // Check if the frame is alredy been read and store in tsunami
            Frame AlreadyAvailableOriginalFrames = Tsunami2.Properties.BOC_Context.AlreadyAvailableOriginalFrames;
            if (AlreadyAvailableOriginalFrames != null)
            {
                Frame test = Frame.GetAssemblyFrameByName(AlreadyAvailableOriginalFrames, idAssembly);
                if (test != null)
                    return test;
            }

            if (framesPath == "")
            {
                if (Tsunami2.Properties.BOC_Context.SelectedGeodeFilePaths == null)
                    Tsunami2.Properties.BOC_Context.SelectedGeodeFilePaths = new List<string>();
                foreach (var path in Tsunami2.Properties.BOC_Context.SelectedGeodeFilePaths)
                {
                    Frame frame = FromGeode_GetOriginalFrames(idAssembly, path, bMustFound: false);
                    if (frame != null) return frame;
                }
            }

            // lets extract teh part of the geode file taht contain what we need:
            string currentContent = "";
            if (framesPath != "")
            {

                int frameCount = 0;
                foreach (var line in System.IO.File.ReadAllText(framesPath).Split('\n'))
                {
                    string cleanLine = line.TrimStart();

                    if (cleanLine.StartsWith("*FRAME") && cleanLine.Contains(idAssembly))
                        frameCount++;
                    else if (frameCount == 0)
                        continue;
                    else if (cleanLine.StartsWith("*ENDFRAME"))
                        frameCount--;

                    cleanLine = cleanLine.Replace("\r", "");
                    currentContent += cleanLine + "\r\n";
                }
            }

            if (currentContent == "") // nothing found in this file
            {
                if (!bMustFound)
                    return null;
                {
                    framesPath = TsuPath.GetFileNameToOpen(null, TSU.Tsunami2.Preferences.Values.Paths.Data, "", "dat files (*.dat)|*.dat", "Browse for SU assembly frames");
                    if (framesPath == "")
                        return null;
                    else
                    {
                        Tsunami2.Properties.BOC_Context.SelectedGeodeFilePaths.AddIfNotExisting(framesPath);
                        Tsunami2.Properties.Save();
                        return FromGeode_GetOriginalFrames(idAssembly, framesPath, bMustFound: true);
                    }
                }
            }
            else
            {
                Frame OriginalFrames = FromLGCInputFormat_GetRootFrameInputFormatString(currentContent);
                Tsunami2.Properties.BOC_Context.SelectedGeodeFilePaths.AddIfNotExisting(framesPath);
                if (Tsunami2.Properties.BOC_Context.AlreadyAvailableOriginalFrames == null)
                    Tsunami2.Properties.BOC_Context.AlreadyAvailableOriginalFrames = new Frame() {_Name = "AlreadyAvailableOriginalFrames" };
                Tsunami2.Properties.BOC_Context.AlreadyAvailableOriginalFrames.SubFrames.AddRange(OriginalFrames.SubFrames);
                return OriginalFrames;
            }
        }

        internal static Frame GetAssemblyFrameByName(Frame root, string id)
        {
            Frame lookedfor;

            if (root._Name.ToUpper().Contains(id))
                return root;

            foreach (Frame item in root.SubFrames)
            {
                lookedfor = GetAssemblyFrameByName(item, id);
                if (lookedfor != null)
                    return lookedfor;
            }
            return null;
        }

        internal static string GetAssemblyName(Frame frame)
        {
            string frameName = frame._Name;
            string[] parts = frameName.Split('_');

            // Get the last part of the array
            string lastPart = parts[parts.Length - 1];
            return lastPart;
        }

        internal static Frame ModifyBasedOnGatheredMeasures(Frame modifiedFrame, BeamOffsets.GatheredMeasures measures)
        {
            foreach (var item in modifiedFrame.DeeperFrame.Measurements.ObsXyzs)
            {
                if (item.Used)
                {
                    bool polarExist = measures.Polar.Exists(x => x.MeasuresWithStatus.Exists(y => y.Measure._Point._Name == item.Point && y.Measure._Status.Type != Measures.States.Types.Bad));
                    bool levelExist = measures.Level.Exists(x => x.MeasuresWithStatus.Exists(y => y.Measure._Point._Name == item.Point && y.Measure._Status.Type != Measures.States.Types.Bad));
                    bool ecartoExist = measures.Ecarto.Exists(x => x.MeasuresWithStatus.Exists(y => y.Measure._Point._Name == item.Point && y.Measure._Status.Type != Measures.States.Types.Bad));
                    if (!polarExist && !levelExist && !ecartoExist)
                        item.Used = false;
                }
            }
            return modifiedFrame;
        }
    }

    [Serializable]
    public static class ObsXYZExtensions
    {
        public static bool Contains<T>(this T src, string name) where T : List<ObsXYZ>
        {
            foreach (var item in src)
            {
                if (item.Point == name)
                    return true;
            }
            return false;
        }
    }
}
