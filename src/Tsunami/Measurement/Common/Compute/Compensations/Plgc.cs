﻿using System;
using System.Collections.Generic;
using System.Linq;
using TSU.IO.SUSoft;
using A = TSU.Common.Analysis;
using C = TSU.Tools.Conversions;
using D = TSU.Common.Dependencies;
using E = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using P = TSU.Preferences;
using R = TSU.Properties.Resources;
using CS = TSU.Common.Compute.Compensations.Strategies;

namespace TSU.Common.Compute.Compensations
{
    public class Plgc1
    {
        private const double na = P.Preferences.NotAvailableValueNa;

        private Input _Input;
        private Output _Output;
        public static string _FileName;
        private bool runned;
        internal CS.GetTheRightCoordinates getTheRightCoordinates;
        internal CS.GetTheRightOption getTheRightOption;
        internal E.Coordinates.CoordinateSystemsTypes coordinateSystemType;

        public CS.List _Strategy;

        public Plgc1()
        {

        }

        // constructor
        public Plgc1(
            Polar.Station station,
            CS.List strategy,
            string fileName = "",
            string title = "default")
        {
            if (title == "default") title = R.T056;
            _Strategy = strategy;

            // trick to get the right coordinates
            Polar.Compensations.NewFrame_LGC2 temp = new Polar.Compensations.NewFrame_LGC2(null);
            temp.CheckIfGeoidalComputeIsPossible(station);

            getTheRightCoordinates = temp.getTheRightCoordinates;
            getTheRightOption = temp.getTheRightOption;
            coordinateSystemType = temp.coordianteSystemType;

            _Input = new Input(this);


            _FileName = station.Parameters2.Setups.InitialValues.CompensationStrategy.RenameFile(fileName);
            station.Parameters2.Setups.InitialValues.CompensationStrategy.Create_PLGC1_Input(station, fileName, this, title);

            CreateFilePointNam();
        }

        public Plgc1(
            Polar.Station station,
            CS.Common strategy,
            CS.GetTheRightCoordinates getTheRightCoordinates,
            CS.GetTheRightOption getTheRightOption,
            E.Coordinates.CoordinateSystemsTypes coordianteSystemType,
            string fileName = "",
            string title = "default")
        {
            if (title == "default") title = R.T056;
            _Strategy = strategy._strategy;

            this.getTheRightCoordinates = getTheRightCoordinates;
            this.getTheRightOption = getTheRightOption;
            coordinateSystemType = coordianteSystemType;

            _Input = new Input(this);

            _FileName = strategy.RenameFile(fileName);
            strategy.Create_PLGC1_Input(station, _FileName, this, title);

            CreateFilePointNam();
        }



        // methods

        public E.Coordinates GetFirstOutputCoordinate()
        {
            _Output = new Output(this);
            E.Coordinates c = _Output.ReadProviPoint()[0];
            c.CoordinateSystem = coordinateSystemType;
            return c;
        }

        public List<E.Coordinates> GetOutputCoordinates()
        {
            _Output = new Output(this);
            return _Output.ReadProviPoint();
        }
        public void CreateFilePointNam()
        {
            string content;

            if (System.IO.File.Exists($"{_FileName}.pinp"))
            {
                System.IO.File.Copy($"{_FileName}.pinp", "c:\\TEMP\\plgc.pinp", overwrite: true);
            }
            content = $"c:\\TEMP\\plgc.pinp \r\n";
            content += $"c:\\TEMP\\plgc.pout \r\n";
            content += $"c:\\TEMP\\plgc.perr \r\n";
            content += "NOPOPUP\r\n"; // on some computer the popup (showing error and version) make the program to crash

            System.IO.DirectoryInfo di = System.IO.Directory.CreateDirectory(@"C:\temp");

            System.IO.File.WriteAllText(@"C:\temp\file.nam", content);
        }
        public bool Run(bool showInput = false, bool showOutput = false)
        {
            System.Diagnostics.Process cmd = null;
            try
            {
                const int MAX_RUNNING_TIME_SECONDS = 2;

                if (showInput) ShowInput();

                if (!P.Preferences.Dependencies.GetByBame("PLGC", out D.Application app))
                {
                    if (app !=null && !app.IsInstalled)
                        throw new Exception("PLGC not installed");
                    else
                        throw new Exception("PLGC not found");
                }

                string pathPLGC = app.Path;
                System.Diagnostics.ProcessStartInfo cmdsi = new System.Diagnostics.ProcessStartInfo(pathPLGC);
                cmd = System.Diagnostics.Process.Start(cmdsi);
                Tsunami2.Preferences.Values.Processes.Add(new TsuProcess() { Application = app, Process = cmd, Input = _FileName });
                if (!cmd.WaitForExit(MAX_RUNNING_TIME_SECONDS * 1000))
                {
                    return false;
                } // cannot use timeout, because PLGC could ask some question to the user....=> finally we use it and use barycenter if it fails

                // Removed because in preferecen we have a timer to check if there is something to kill in the Pref.Processes list every 5 minutes.
                //System.Windows.Forms.Timer t = new System.Windows.Forms.Timer() { Enabled = true, Interval = 5 * 60 * 1000 };
                //t.Tick += delegate
                //{
                //    if (!cmd.HasExited)
                //    {
                //        cmd.Kill();
                //        cmd.Dispose();
                //    }
                //};


                runned = true;

                if (System.IO.File.Exists("c:\\TEMP\\plgc.pout"))
                {
                    System.IO.File.Copy("c:\\TEMP\\plgc.pout", $"{_FileName}.pout", overwrite: false);
                }

                if (System.IO.File.Exists("c:\\TEMP\\plgc.perr"))
                {
                    System.IO.File.Copy("c:\\TEMP\\plgc.perr", $"{_FileName}.perr", overwrite: false);
                }


                if (showOutput)
                {
                    ShowOutput();

                }
                return runned;
            }
            catch (Exception ex)
            {
                throw new Exception("PLGC failed to RUN", ex);
            }
            finally
            {
                if (cmd !=null && !cmd.HasExited)
                {
                    cmd.Kill();
                    cmd.Dispose();
                }
            }
        }

        public string InputFileName
        {
            get
            {
                return _FileName + ".pinp";
            }
        }

        public void ShowInput()
        {
            Shell.ExecutePathInDialogView(InputFileName, "PLGC, approximate compute input file");
        }

        public string OutputFileName
        {
            get
            {
                return _FileName + ".pout";
            }
        }

        public string ErrorFileName
        {
            get
            {
                return _FileName + ".perr";
            }
        }

        public void ShowOutput()
        {
            if (runned)
                if (System.IO.File.Exists(OutputFileName))
                {
                    Shell.ExecutePathInDialogView(OutputFileName, "PLGC, approximate compute output file");

                }
                else
                {
                    Shell.ExecutePathInDialogView(ErrorFileName, "Error log from PLGC");
                }
            else
                throw new Exception(R.T040);
        }

        public void ShowErrorFile()
        {
            if (runned)
                if (System.IO.File.Exists(ErrorFileName))
                {
                    Shell.ExecutePathInDialogView(ErrorFileName, "PLGC, error file");
                }
        }



        // nested class

        // this is a signature to create a variable containing a method that will give the right coordinates between CCS or MLA
        public delegate E.Coordinates GetTheRightCoordinates(E.Point point);

        public class Input
        {
            public bool usingGeoid;
            public Plgc1 plgc1;

            public Input()
            {

            }

            public Input(Plgc1 plgc1)
            {
                this.plgc1 = plgc1;
            }

            internal static bool CreateBasedOn(FinalModule finalModule, ref string filePath)
            {
                string content = "";

                // Collect data
                CS.GetTheRightCoordinates getCoordinates = (p) =>
                {
                    return p._Coordinates.GetLessLocal(out string dummy);
                };

                List<Station> stations = finalModule.GetListOfStationsClosedOrBad();
                List<M.Measure> AllMeasures = finalModule.GetMeasuresFromClosedStations();

                List<E.Point> poin = A.Measure.GetPointsByGeodeRole(AllMeasures, Geode.Roles.Poin | Geode.Roles.Unused);
                List<E.Point> cala = Tsunami2.Properties.Points.Except(poin, new E.PointComparer()).ToList();


                string title = "Export PLGC";
                content += InputLine.Titr($"{title} for several stations on {DateTime.Now}");
                content += InputLine.OptionOloc();

                content += InputLine.AddCalaPoints(cala, getCoordinates);
                content += InputLine.AddVariablePoints(poin, getCoordinates);

                // merge from stations
                try
                {
                    foreach (Station station in stations)
                    {
                        if (station is Polar.Station ps)
                        {
                            content += InputLine.Tstn(ps, getCoordinates);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error collecting info from each station", ex);
                }
                content += InputLine.End();

                try
                {
                    System.IO.File.WriteAllText(filePath, content);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error writing all text of PLGC input", ex);
                }
                return true;
            }

            public static class InputLine
            {

                public static bool usingGeoid;
                private static string GetSeparator = "   ";
                public static string Titr(string text)
                {
                    string s = "*TITR\r\n";
                    s += text + "\r\n";
                    return s;
                }
                public static string OptionOloc()
                {
                    return "*OLOC\r\n";
                }
                public static string OptionCreatePunchFile()
                {
                    return "*PUNC \r\n";
                }

                public static List<E.Point> VariablePoints;
                public static string AddVariablePoints(IEnumerable<E.Point> l, CS.GetTheRightCoordinates GetTheRightCoordinates)
                {

                    if (!l.Any())
                    {
                        return "";
                    }

                    string b = GetSeparator;
                    VariablePoints = new List<E.Point>();
                    List<string> namesAlreadyPresent = new List<string>();
                    string s = "*POIN\r\n";
                    foreach (E.Point e in l)
                    {
                        if (!namesAlreadyPresent.Contains(e._Name))
                        {
                            string line = "";
                            namesAlreadyPresent.Add(e._Name);
                            line += C.StringManipulation.LengthOf20String(e._Name) + b;
                            if (GetTheRightCoordinates == null)
                            {
                                line += C.StringManipulation.LengthOf13String(e._Coordinates.Local.X.ToString("m-m")) + b;
                                line += C.StringManipulation.LengthOf13String(e._Coordinates.Local.Y.ToString("m-m")) + b;
                                line += C.StringManipulation.LengthOf13String(e._Coordinates.Local.Z.ToString("m-m")) + b;
                            }
                            else
                            {
                                var coord = GetTheRightCoordinates(e);
                                if (coord.AreKnown)
                                {
                                    line += C.StringManipulation.LengthOf13String(coord.X.ToString("m-m")) + b;
                                    line += C.StringManipulation.LengthOf13String(coord.Y.ToString("m-m")) + b;
                                    line += C.StringManipulation.LengthOf13String(coord.Z.ToString("m-m")) + b;
                                }
                                else
                                    line += "-999.9 -999.9 -999.9";
                            }
                            s += line + "\r\n";
                            VariablePoints.Add(e);
                        }
                    }
                    return s;
                }
                public static string AddCalaPoints(List<E.Point> l, CS.GetTheRightCoordinates GetTheRightCoordinates)
                {
                    string b = GetSeparator;
                    List<string> namesAlreadyPresent = new List<string>();
                    string s = "*CALA\r\n";
                    foreach (E.Point e in l)
                    {

                        if (!namesAlreadyPresent.Contains(e._Name))
                        {
                            string line = "";
                            namesAlreadyPresent.Add(e._Name);

                            line += C.StringManipulation.LengthOf20String(e._Name) + b;
                            if (GetTheRightCoordinates == null)
                            {
                                line += C.StringManipulation.LengthOf13String(e._Coordinates.Local.X.ToString("m-m")) + b;
                                line += C.StringManipulation.LengthOf13String(e._Coordinates.Local.Y.ToString("m-m")) + b;
                                line += C.StringManipulation.LengthOf13String(e._Coordinates.Local.Z.ToString("m-m")) + b;
                                if (e._Coordinates.Local.X.Value == na) line = "% " + line + "% Commented because equal to " + na.ToString();
                            }
                            else
                            {
                                E.Coordinates c = GetTheRightCoordinates(e);
                                line += C.StringManipulation.LengthOf13String(c.X.ToString("m-m")) + b;
                                line += C.StringManipulation.LengthOf13String(c.Y.ToString("m-m")) + b;
                                line += C.StringManipulation.LengthOf13String(c.Z.ToString("m-m")) + b;
                                if (c.X.Value == na) line = "% " + line + "% Commented because equal to " + na.ToString();
                            }
                            s += line + "\r\n";

                            if (e._Coordinates.Ccs.AreKnown) //throw new Exception(R.T058);
                                usingGeoid = true;
                            else
                                usingGeoid = false;
                        }
                    }
                    return s;
                }
                public static string AddVZPoints(List<E.Point> l, CS.GetTheRightCoordinates GetTheRightCoordinates)
                {
                    string b = GetSeparator;
                    List<string> namesAlreadyPresent = new List<string>();

                    string s = "";
                    if (l.Count > 0)
                        s += "*VZ" + "\r\n";

                    foreach (E.Point e in l)
                    {
                        //if (!(e._Coordinates.Local.X.Value != na && e._Coordinates.Local.Y.Value != na)) throw new Exception(R.T058);
                        if (!namesAlreadyPresent.Contains(e._Name))
                        {
                            namesAlreadyPresent.Add(e._Name);
                            s += C.StringManipulation.LengthOf20String(e._Name) + b;
                            s += C.StringManipulation.LengthOf13String(GetTheRightCoordinates(e).X.ToString("m-m")) + b;
                            s += C.StringManipulation.LengthOf13String(GetTheRightCoordinates(e).Y.ToString("m-m")) + b;
                            s += C.StringManipulation.LengthOf13String(GetTheRightCoordinates(e).Z.ToString("m-m")) + b;
                            s += "\r\n";
                        }
                    }
                    return s;
                }
                public static string Tstn(Polar.Station station, CS.GetTheRightCoordinates GetTheRightCoordinates)
                {

                    try
                    {
                        Polar.Station.Parameters p = station.Parameters2 as Polar.Station.Parameters;
                        string b = GetSeparator;

                        string s = "";

                        List<Polar.Measure> measures = new List<Polar.Measure>();
                        foreach (var item in station.MeasuresTaken)
                        {
                            measures.Add(item as Polar.Measure);
                        }
                        // goodMeasures.AddRange(A.Measure.GetPolarByState<M.States.Good, M.States.Questionnable>(station.MeasuresTaken));

                        if (station.NextMeasure != null) measures.Add(station.NextMeasure);
                        string angl = "*ANGL" + "\r\n" + "% Station name                       Point name         lecture   ([SIGMA_en_cc] [/CONST_en_grades])" + "\r\n";
                        string zenD = "*ZENH" + "\r\n" + "% Station name                       Point name         lecture   ([SIGMA_en_cc] [/H_appareil_en_m] [\\HT_REFLECT_en_m])" + "\r\n";
                        string dist = "*DMES" + "\r\n" + "% Station name                       Point name         lecture   ([SIGMA_en_mm] [SIGMA_ppm_en_mm] [\\HInst_m HRefl_m])" + "\r\n";

                        foreach (Polar.Measure m in measures)
                        {
                            // check if the point is known, if not, doesn't worth to put the measure, plgc will fail, only for cala point actually...
                            bool CoordinateAvailable = false;
                            if (GetTheRightCoordinates == null)
                            {
                                if (m._OriginalPoint != null && m._OriginalPoint._Coordinates.Local.X.Value != na)
                                    CoordinateAvailable = true;
                            }
                            else
                            {
                                if (m._OriginalPoint!=null && GetTheRightCoordinates(m._OriginalPoint).X != null)
                                    CoordinateAvailable = true;
                            }

                            if (VariablePoints != null)
                            {
                                // if it is in varialbe list then it is ok to have -999.9
                                if (m._OriginalPoint!= null && VariablePoints.Contains(m._OriginalPoint)) CoordinateAvailable = true;
                                if (m._Point != null && VariablePoints.Contains(m._Point)) CoordinateAvailable = true;
                            }


                            // Have to switch faces2 to face 1 because PLGC handle it badly
                            Polar.Measure mf1;
                            if (m.Face == I.FaceType.Face2)
                            {
                                mf1 = m.GetNiceClone();
                                mf1.Angles = Survey.TransformToOppositeFace(mf1.Angles);
                            }
                            else
                            {
                                mf1 = m;
                            }

                            string prefix = (!CoordinateAvailable || mf1._Status.Type == M.States.Types.Bad || mf1._Status.Type == M.States.Types.Control) ? "% " : "";

                            if (mf1.Angles.Corrected.Horizontal.Sigma != 200.0000)
                            {
                                string prefix2;
                                prefix2 = ""; //prefix == "" ? (mf1.Angles.Corrected.Horizontal.Value == na ? "% ": "") : "";  // z
                                angl += prefix + prefix2 + C.StringManipulation.LengthOf20String(p._StationPoint._Name) + b;
                                angl += C.StringManipulation.LengthOf20String(mf1._Point._Name) + b;
                                angl += C.StringManipulation.LengthOf13String(mf1.Angles.Corrected.Horizontal.ToString("gon-gon")) + b;
                                angl += "\r\n";

                                prefix2 = ""; //prefix == "" ? (mf1.Angles.Corrected.Vertical.Value == na ? "% " : "") : "";
                                zenD += prefix + prefix2 + C.StringManipulation.LengthOf20String(p._StationPoint._Name) + b;
                                zenD += C.StringManipulation.LengthOf20String(mf1._Point._Name) + b;
                                zenD += C.StringManipulation.LengthOf13String(mf1.Angles.Corrected.Vertical.ToString("gon-gon")) + b;
                                zenD += @"   /" + p._InstrumentHeight.ToString("m-m");
                                zenD += @"   \" + mf1.Extension.ToString("m-m");
                                zenD += "\r\n";

                                prefix2 = ""; //prefix == "" ? (mf1.Distance.Corrected.Value == na ? "% " : "") : "";
                                dist += prefix + prefix2 + C.StringManipulation.LengthOf20String(p._StationPoint._Name) + b;
                                dist += C.StringManipulation.LengthOf20String(mf1._Point._Name) + b;
                                dist += C.StringManipulation.LengthOf13String(mf1.Distance.Corrected.ToString("m-m")) + b;
                                dist += @"   \" + p._InstrumentHeight.ToString("m-m") + "   " + mf1.Extension.ToString("m-m");
                                dist += "   \r\n";
                            }
                        }
                        s += angl + zenD + dist;

                        return s;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"{R.T_PROBLEM_WRITING_THE_OBSERVATION_PART_OF_THE_PLGC_INPUT}\r\n{ex.Message}", ex);
                    }
                }
                public static string End()
                {
                    return "*END\r\n";
                }
                internal static string AddPdor(E.Point point)
                {
                    string b = GetSeparator;
                    List<string> namesAlreadyPresent = new List<string>();
                    string s = "*PDOR\r\n";
                    s += point._Name + "\r\n";
                    return s;
                }
            }

        }
        public class Output
        {
            public Plgc1 plgc1;

            public Output()
            {

            }

            public Output(Plgc1 plgc1)
            {
                this.plgc1 = plgc1;
            }

            public List<E.Coordinates> ReadProviPoint()
            {
                try
                {
                    List<E.Coordinates> l = new List<E.Coordinates>();
                    var lines = System.IO.File.ReadLines(_FileName + ".pout");
                    bool foundProvi = false;
                    string provi = "";
                    foreach (var line in lines)
                    {
                        if (foundProvi)
                        {
                            provi = line.Trim();

                            E.Coordinates p = new E.Coordinates();
                            p.CoordinateSystem = plgc1.coordinateSystemType;
                            string[] tokens = new string[4];
                            int i = 0;

                            foreach (var item in provi.Split(' '))
                            {
                                if (item != "")
                                {
                                    tokens[i] = item;
                                    i += 1;
                                }
                                if (i == 4) break;
                            }
                            if (tokens[0].Substring(0, 1) == "*") break; // si un etoile aprés provi alors fini
                            p._Name = tokens[0];
                            p.Set();
                            p.X.Value = C.Numbers.ToDouble(tokens[1]);
                            p.Y.Value = C.Numbers.ToDouble(tokens[2]);
                            p.Z.Value = C.Numbers.ToDouble(tokens[3]);

                            l.Add(p);
                        }
                        if (line.Trim().Contains("*PROVI"))
                            foundProvi = true;
                    }
                    if (!foundProvi)
                    {
                        string errorFilePath = _FileName + ".perr";
                        bool errorFilePathExist = System.IO.File.Exists(errorFilePath);
                        string errorMessage = errorFilePathExist ? System.IO.File.ReadAllText(errorFilePath) : "";

                        if (errorMessage != "")
                            throw new Exception("PLGC said: " + errorMessage);
                        else
                            throw new Exception("No PLGC OUTPUT or ERROR file found");

                    }
                    return l;
                }
                catch (Exception ex)
                {
                    throw new Exception(R.T059, ex);
                }
            }
        }
    }
}
