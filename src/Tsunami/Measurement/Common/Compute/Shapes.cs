﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;


namespace TSU.Common.Compute.Shapes
{

    public class Position
    {
        public bool isfreex { get; set; }
        public bool isfreey { get; set; }
        public bool isfreez { get; set; }
        public double sigmax { get; set; }
        public double sigmay { get; set; }
        public double sigmaz { get; set; }
        public double x { get; set; }
        public double y { get; set; }
        public double z { get; set; }
    }

    public class ExtraInfos
    {
        public string weight { get; set; }
    }

    public class Point
    {
        public bool active { get; set; }
        public ExtraInfos extraInfos { get; set; }
        public string headerComment { get; set; }
        public string inlineComment { get; set; }
        public string name { get; set; }
        public Position position { get; set; }
    }

    public class BestFit
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public List<string> Points { get; set; }
    }

    public class RootFrame
    {
        public List<object> innerFrames { get; set; }
        public bool isfreescale { get; set; }
        public string name { get; set; }
        public List<Point> points { get; set; }
        public Position rotation { get; set; }
        public int scale { get; set; }
        public Position translation { get; set; }
        public List<BestFit> BestFits { get; set; }
    }

    public class Params
    {
        public string coordsys { get; set; }
        public Dictionary<string, object> extraInfos { get; set; }
        public int precision { get; set; }
    }

    public class ShapesObject
    {
        [JsonPropertyName("params")]
        public Params Params { get; set; }
        public RootFrame rootFrame { get; set; }
        public string title { get; set; }
    }

}
