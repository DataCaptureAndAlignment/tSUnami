﻿using System;
using System.Collections.Generic;

namespace TSU.Common.Compute.ShapesOutput
{
    public class InputData
    {
        public FPoint fPoint { get; set; }
        public FMinDist fMinDist { get; set; }
        public FMinDX fMinDX { get; set; }
        public FMinDY fMinDY { get; set; }
        public FMinDZ fMinDZ { get; set; }
        public string fComment { get; set; }
    }

    public class FPoint
    {
        public bool active_ { get; set; }
        public string eolcomment { get; set; }
        public List<double> fCorrection { get; set; }
        public List<List<double>> fCovarianceMatrix { get; set; }
        public bool fCovarianceMatrixIsSet { get; set; }
        public List<double> fEstimatedPrecision { get; set; }
        public FEstimatedValue fEstimatedValue { get; set; }
        public bool fHfixed { get; set; }
        public List<bool> fixedState { get; set; }
        public string fName { get; set; }
        public FProvisionalValue fProvisionalValue { get; set; }
        public int fReferential { get; set; }
        public int fSpatialStatus { get; set; }
        public List<double> fStandardDeviations { get; set; }
        public bool fXValueSet { get; set; }
        public bool fYValueSet { get; set; }
        public string hdrcomment { get; set; }
        public int line { get; set; }
        public List<int> uidx { get; set; }
    }

    public class FEstimatedValue
    {
        public List<double> fVector { get; set; }
        public FCoordSys fCoordSys { get; set; }
    }

    public class FCoordSys
    {
        public int fCoordSysId { get; set; }
    }

    public class FProvisionalValue
    {
        public List<double?> fVector { get; set; }
        public FCoordSys fCoordSys { get; set; }
    }

    public class FMinDist
    {
        public double fValue { get; set; }
    }

    public class FMinDX
    {
        public double fValue { get; set; }
    }

    public class FMinDY
    {
        public double fValue { get; set; }
    }

    public class FMinDZ
    {
        public double fValue { get; set; }
    }
    public class PointData
    {
        public FPoint fPoint { get; set; }
        public FMinDist fMinDist { get; set; }
        public FMinDX fMinDX { get; set; }
        public FMinDY fMinDY { get; set; }
        public FMinDZ fMinDZ { get; set; }
        public string fComment { get; set; }
    }

    public class DirCosines
    {
        public List<double> fVector { get; set; }
        public FCoordSys fCoordSys { get; set; }
    }

    public class BestFit
    {
        public string fName { get; set; }
        public FBestFitParam fBestFitParam { get; set; }
    }


    public class FBestFitParam
    {
        public List<FListPoint> fListPoint { get; set; }
        public FEstParameter fEstParameter { get; set; }
        public FLineParameter fLineParameter { get; set; } 
    }

    public class FEstParameter
    {
        public FPoint coeffCircleCenter { get; set; }
        public double coeffCircleRadius { get; set; }
        public double sigmaCircleRadius { get; set; }
        public double maxDistCircleOutside { get; set; }
        public double maxDistCircleInside { get; set; }
        public FListPoint maxDistCirclePointOutside { get; set; }
        public FListPoint maxDistCirclePointInside { get; set; }
        public double distCircleCenter { get; set; }
        public double bearingVecCircleCenter { get; set; }
        public double vertiVecCircleCenter { get; set; }
        public FPlaneParameters planeParameters { get; set; }
    }


    public class FLineParameter
    {
        public double coeffA { get; set; }
        public double coeffP { get; set; }
        public double coeffB { get; set; }
        public double coeffQ { get; set; }
        public double sigmaA { get; set; }
        public double sigmaP { get; set; }
        public double sigmaB { get; set; }
        public double sigmaQ { get; set; }
        public double sigmaZero2 { get; set; }
        public DirCosines dirCosines { get; set; }
        public double bearing { get; set; }
        public double verticalAngle { get; set; }
        public double perpDist { get; set; }
        public double bearingVec { get; set; }
        public double vertiVec { get; set; }
        public PointData minDistPoint { get; set; }
        public PointData maxDistPoint { get; set; }
        public int swapped { get; set; }
    }

    public class FPlaneParameters
    {
        public FVector normalVec { get; set; }
        public Dictionary<string, double> coeffD { get; set; }
        public FVector sigmaNormalVec { get; set; }
        public Dictionary<string, double> sigmaD { get; set; }
        public double bearing { get; set; }
        public double verticalAngle { get; set; }
        public double perpDist { get; set; }
        public double bearingVec { get; set; }
        public double vertiVec { get; set; }
        public double maxDistNegSide { get; set; }
        public double maxDistPosSide { get; set; }
        public FPoint maxDistPointNegSide { get; set; }
        public FPoint maxDistPointPosSide { get; set; }
    }

    public class FListPoint
    {
        public FPoint fPoint { get; set; }
        public FMinDist fMinDist { get; set; }
        public FMinDX fMinDX { get; set; }
        public FMinDY fMinDY { get; set; }
        public FMinDZ fMinDZ { get; set; }
        public string fComment { get; set; }
    }

    public class FVector
    {
        public List<double> fVector { get; set; }
        public FCoordSys fCoordSys { get; set; }
    }

    public class ShapesObject
    {
        public string ShapesVersion { get; set; }
        public List<InputData> Input_PointData { get; set; }
        public List<BestFit> BestFits { get; set; }
    }
}
