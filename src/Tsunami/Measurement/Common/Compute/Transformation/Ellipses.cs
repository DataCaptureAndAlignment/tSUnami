﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.IO;
using TSU.IO.SUSoft;
using System.Diagnostics;
using System.Security.Cryptography.X509Certificates;

namespace TSU.Common.Compute.Transformation
{
    public class Ellipses
    {
        public class Ellipse
        {
            public double s1 { get; set; }
            public double s2 { get; set; }
            public double s3 { get; set; }
            public Ellipse()
            {

            }
        }

        public static List<(string accelerator, double averageBearing)> GetAverageBearings(Polar.Station station)
        {
            // get the average bearing by accelerator:
            List<(string accelerator, List<double> bearings)> bearingsPerAccelerator = new List<(string, List<double>)>();
            int count = 0;
            foreach (var measure in station.GoodMeasuresTaken)
            {
                if (measure.GeodeRole != Geode.Roles.Unused)
                {
                    var point = measure._OriginalPoint;
                    if (point.IsAlesage)
                    {
                        AddBearingToAccelerator(point._Parameters.GisementFaisceau, point.ZoneOrAcc, bearingsPerAccelerator);
                        count++;
                    }
                }
            }

            // average them


            // Compute the average bearing per accelerator
            List<(string accelerator, double averageBearing)> averageBearingsPerAccelerator = new List<(string, double)>();

            foreach (var entry in bearingsPerAccelerator)
            {
                double averageBearing = entry.bearings.Average();
                averageBearingsPerAccelerator.Add((entry.accelerator, averageBearing));
            }

            return averageBearingsPerAccelerator;
        }

        public static void AddBearingToAccelerator(double bearing, string acceleratorName, List<(string accelerator, List<double> bearings)> _bearingsPerAccelerator)
        {
            // Find the entry with the given accelerator name
            var acceleratorEntry = _bearingsPerAccelerator.FirstOrDefault(entry => entry.accelerator == acceleratorName);

            if (acceleratorEntry.accelerator == null)
            {
                // If the accelerator does not exist, add a new entry
                _bearingsPerAccelerator.Add((acceleratorName, new List<double> { bearing }));
            }
            else
            {
                // If the accelerator exists, add the bearing to the existing list
                acceleratorEntry.bearings.Add(bearing);
            }
        }
        public class PyEllipsoid
        {
            public static Dictionary<string, List<(string element, double Sx, double Sy, double Sz)>> Compute(Polar.Station station, string lgcFileName)
            {
                var dict = new Dictionary<string, List<(string element, double Sx, double Sy, double Sz)>>();

                List<(string accelerator, double averageBearing)> averageBearingsPerAccelerator = GetAverageBearings(station);

                // Display and use the average bearings to run the external program
                foreach (var entry in averageBearingsPerAccelerator)
                {
                    Console.WriteLine($"Accelerator: {entry.accelerator}, Average Bearing: {entry.averageBearing}");

                    // Run the external program with the average bearing as the -b parameter
                    RunScript(entry.averageBearing, lgcFileName);

                    // Process the generated JSON file and store results
                    string outputFilePath = $@"{lgcFileName}_PyEllipsoidRotation.json";
                    var elementData = ProcessGeneratedJson(outputFilePath);

                    // Add to results dictionary
                    dict[entry.accelerator] = elementData;
                }
                return dict;
            }

            public static void RunScript(double bearing, string filePath)
            {
                // Prepare the process start information
                var startInfo = new ProcessStartInfo
                {
                    FileName = @"C:\Program Files\SUSoft\SurveyPad\1.1.0\scripts\PyEllipsoidRotation.exe",
                    Arguments = $"-i {filePath}.json -t json -b {bearing}",
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    CreateNoWindow = true
                };

                try
                {
                    // Start the process
                    using (var process = Process.Start(startInfo))
                    {
                        // Read the output (if needed)
                        string output = process.StandardOutput.ReadToEnd();
                        string error = process.StandardError.ReadToEnd();
                        process.WaitForExit();

                        // Display the output
                        Console.WriteLine(output);
                        Console.WriteLine(error);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error running external program: {ex.Message}");
                }
            }

            public static List<(string element, double Sx, double Sy, double Sz)> ProcessGeneratedJson(string filePath)
            {
                var elementData = new List<(string element, double Sx, double Sy, double Sz)>();

                try
                {
                    // Read the JSON file
                    var jsonData = File.ReadAllText(filePath);

                    // Parse the JSON data
                    var jsonDoc = JsonDocument.Parse(jsonData);

                    // Extract the relevant information
                    var root = jsonDoc.RootElement;
                    var elements = root.GetProperty("ELEMENT").EnumerateObject().Select(e => e.Value.GetString()).ToList();
                    var types = root.GetProperty("Type").EnumerateObject().Select(t => t.Value.GetString()).ToList();
                    var sxValues = root.GetProperty("Sx(mm)").EnumerateObject().Select(sx => sx.Value.GetDouble()).ToList();
                    var syValues = root.GetProperty("Sy(mm)").EnumerateObject().Select(sy => sy.Value.GetDouble()).ToList();
                    var szValues = root.GetProperty("Sz(mm)").EnumerateObject().Select(sz => sz.Value.GetDouble()).ToList();

                    // Filter the elements based on the Type
                    for (int i = 0; i < elements.Count; i++)
                    {
                        if (types[i] != "CALA")
                        {
                            elementData.Add((elements[i], sxValues[i], syValues[i], szValues[i]));
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error processing JSON file: {ex.Message}");
                }

                return elementData;
            }
        }
        
        
        public static Dictionary<string, Dictionary<string, Ellipse>> Compute (Polar.Station station, string lgcFileName)
        {
            var points = new Dictionary<string, Dictionary<string, Ellipse>>();

            List<(string accelerator, double averageBearing)> averageBearingsPerAccelerator = GetAverageBearings(station);

            string lgcJsonFilePath = $@"{lgcFileName}.json";

            var pointsCovarianceMatrices = Transformation.Ellipses.ReadCovarianceMatrices(lgcJsonFilePath);


            foreach (var covarianceMatrix in pointsCovarianceMatrices)
            {
                var accelators_ellipses = new Dictionary<string, Ellipse>();
                foreach (var accelerator in averageBearingsPerAccelerator)
                {
                    // Display and use the average bearings to run the external program
                    Console.WriteLine($"Accelerator: {accelerator.accelerator}, Average Bearing: {accelerator.averageBearing}");

                    accelators_ellipses[accelerator.accelerator] = TransformCovarianceAndComputeSigmas(covarianceMatrix.Value, 0, 0, 0, Tools.Conversions.Angles.Gon.ToRad(accelerator.averageBearing));
                }
                string pointName = covarianceMatrix.Key;
                points[pointName] = accelators_ellipses;
            }
            return points;
        }


        /// <summary>
        /// Teta = Gisement (rad),  Phi = Pente faisceau (rad), ,Alpha = 1/2 Déflexion (rad), Tilt = Tilt faisceau (rad)
        /// </summary>
        /// <param name="covarianceMatrix"></param>
        /// <param name="phi"></param>
        /// <param name="tilt"></param>
        /// <param name="alpha"></param>
        /// <param name="teta"></param>
        /// <returns></returns>
        public static Ellipse TransformCovarianceAndComputeSigmas(double[,] covarianceMatrix, double phi, double tilt, double alpha, double teta)
        {
            // Generate the transformation matrix
            double[,] transformationMatrix = Rotation.ftransVectorCCStoRST(phi, tilt, alpha, teta);

            double[,] newCovarianceMatrix = TransformCovarianceMatrix(covarianceMatrix, transformationMatrix);

            // Compute the new standard deviations
            double[] newSigmas = ComputeNewSigmas(newCovarianceMatrix);

            
            return new Ellipse() { s1 = newSigmas[0], s2 = newSigmas[1], s3 = newSigmas[2] };
        }

        

        static double[,] TransformCovarianceMatrix(double[,] covMatrix, double[,] transMatrix)
        {
            // Matrix multiplication: T * C * T^T
            int size = covMatrix.GetLength(0);
            double[,] transposedTransMatrix = Rotation.TransposeMatrix(transMatrix);

            double[,] tempMatrix = Rotation.MultiplyMatrices(transMatrix, covMatrix);
            double[,] newCovMatrix = Rotation.MultiplyMatrices(tempMatrix, transposedTransMatrix);

            return newCovMatrix;
        }

        static double[] ComputeNewSigmas(double[,] covMatrix)
        {
            int size = covMatrix.GetLength(0);
            double[] sigmas = new double[size];

            for (int i = 0; i < size; i++)
            {
                sigmas[i] = Math.Sqrt(covMatrix[i, i]) * 1000; // Convert to mm
            }

            return sigmas;
        }

        

        public class LGCData
        {
            [JsonPropertyName("points")]
            public List<Point> Points { get; set; }
        }

        public class Point
        {
            [JsonPropertyName("fCovarianceMatrix")]
            public List<List<double>> FCovarianceMatrix { get; set; }

            [JsonPropertyName("fName")]
            public string FName { get; set; }
        }

        public class Root
        {
            [JsonPropertyName("LGC_DATA")]
            public LGCData LgcData { get; set; }
        }


        public static Dictionary<string, double[,]> ReadCovarianceMatrices(string jsonFilePath)
        {
            string jsonString = File.ReadAllText(jsonFilePath);
            var root = JsonSerializer.Deserialize<Root>(jsonString);

            var result = new Dictionary<string, double[,]>();

            foreach (var point in root.LgcData.Points)
            {
                if (point.FCovarianceMatrix != null)
                {
                    int size = point.FCovarianceMatrix.Count;
                    var covarianceMatrix = new double[size, size];

                    for (int i = 0; i < size; i++)
                    {
                        for (int j = 0; j < size; j++)
                        {
                            covarianceMatrix[i, j] = point.FCovarianceMatrix[i][j];
                        }
                    }

                    result[point.FName] = covarianceMatrix;
                }
            }

            return result;
        }
    }
}
