﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Xml;
using System.Xml.Serialization;
using System.Threading.Tasks;

namespace TSU.Common.Compute.Transformation
{
    public class Rotation
    {

        [XmlAttribute]
        public double Rot { get; set; }
        [XmlAttribute]
        public double SumToAvoidUnawareModification { get; set; }

        [XmlIgnore]
        public bool Validity
        {
            get
            {
                return CheckSum.Get(this) == SumToAvoidUnawareModification;
            }
        }

        public Rotation()
        {

        }

        public Rotation(double rot)
        {
            Rot = rot;
        }

        public static double[,] FMRx(double Rx)
        {
            return new double[,] {
                { 1, 0, 0 },
                { 0, Math.Cos(Rx), Math.Sin(Rx) },
                { 0, -Math.Sin(Rx), Math.Cos(Rx) }
            };
        }

        public static double[,] FMRy(double Ry)
        {
            return new double[,] {
                { Math.Cos(Ry), 0, -Math.Sin(Ry) },
                { 0, 1, 0 },
                { Math.Sin(Ry), 0, Math.Cos(Ry) }
            };
        }

        public static double[,] FMRz(double Rz)
        {
            return new double[,] {
                { Math.Cos(Rz), Math.Sin(Rz), 0 },
                { -Math.Sin(Rz), Math.Cos(Rz), 0 },
                { 0, 0, 1 }
            };
        }

        public static double[,] FMRxyz(double Rx, double Ry, double Rz)
        {
            return MultiplyMatrices(FMRx(Rx), MultiplyMatrices(FMRy(Ry), FMRz(Rz)));
        }

        public static double[,] ftransVectorCCStoRST(double Phi, double Tilt, double Alpha, double Teta)
        {
            double[,] rotationMatrix1 = FMRxyz(-Phi, Tilt, -Alpha);
            double[,] rotationMatrix2 = FMRxyz(0, 0, Teta);
            return MultiplyMatrices(TransposeMatrix(rotationMatrix1), TransposeMatrix(rotationMatrix2));
        }

        public static double[,] MultiplyMatrices(double[,] matrixA, double[,] matrixB)
        {
            int aRows = matrixA.GetLength(0);
            int aCols = matrixA.GetLength(1);
            int bRows = matrixB.GetLength(0);
            int bCols = matrixB.GetLength(1);

            if (aCols != bRows)
            {
                throw new InvalidOperationException("Matrix dimensions are not valid for multiplication.");
            }

            double[,] resultMatrix = new double[aRows, bCols];

            for (int i = 0; i < aRows; i++)
            {
                for (int j = 0; j < bCols; j++)
                {
                    resultMatrix[i, j] = 0;
                    for (int k = 0; k < aCols; k++)
                    {
                        resultMatrix[i, j] += matrixA[i, k] * matrixB[k, j];
                    }
                }
            }

            return resultMatrix;
        }

        public static double[,] TransposeMatrix(double[,] matrix)
        {
            int rows = matrix.GetLength(0);
            int cols = matrix.GetLength(1);
            double[,] transposedMatrix = new double[cols, rows];

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    transposedMatrix[j, i] = matrix[i, j];
                }
            }

            return transposedMatrix;
        }

    }

    public class Rotation3D
    {
        //[XmlAttribute]
        //public ENUM.Management.Zone Management.Zone { get; set; }
        [XmlAttribute]
        public double RotX { get; set; }
        [XmlAttribute]
        public double RotY { get; set; }
        [XmlAttribute]
        public double RotZ { get; set; }

        private double sumToAvoidUnawareModification;
        [XmlAttribute]
        public double SumToAvoidUnawareModification
        {
            get
            {
                if (sumToAvoidUnawareModification == 0)
                    return CheckSum.Get(this);
                else
                    return sumToAvoidUnawareModification;
            }
            set
            {
                sumToAvoidUnawareModification = value;
            }
        }

        [XmlIgnore]
        public bool Validity
        {
            get
            {
                double d = CheckSum.Get(this);
                return Math.Abs(d - SumToAvoidUnawareModification) < 0.00001;
            }
        }

        public Rotation3D()
        {

        }

        public override string ToString()
        {
            return "Rotation around X = " + RotX.ToString() + ", Y = " + RotY.ToString() + ", Z = " + RotZ.ToString() + " gon, Y";
        }

        public Rotation3D(double rotX, double rotY, double rotZ)
        {
            //Management.Zone = zone;
            RotX = rotX;
            RotY = rotY;
            RotZ = rotZ;
        }

    }
    public class Translation3D : TsuObject
    {
        //[XmlAttribute]
        //public ENUM.Management.Zone Management.Zone { get; set; }
        [XmlAttribute]
        public double AlongX { get; set; }
        [XmlAttribute]
        public double AlongY { get; set; }
        [XmlAttribute]
        public double AlongZ { get; set; }

        private double sumToAvoidUnawareModification;
        [XmlAttribute]
        public double SumToAvoidUnawareModification
        {
            get
            {
                if (sumToAvoidUnawareModification == 0)
                    return CheckSum.Get(this);
                else
                    return sumToAvoidUnawareModification;
            }
            set
            {
                sumToAvoidUnawareModification = value;
            }
        }

        public Translation3D()
        {

        }
        public Translation3D(double alongX, double alongY, double alongZ)
        {
            //Management.Zone = zone;
            AlongX = alongX;
            AlongY = alongY;
            AlongZ = alongZ;
        }
    }

    public class Rotation3DList
    {
        public List<Rotation3D> list { get; set; }

        public Rotation3DList()
        {
            list = new List<Rotation3D>();
        }
    }
}
