﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using E = TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using Z = TSU.Common.Zones;
using System.Windows.Media.Media3D;
using T = TSU.Tools;
using TSU.Common.Elements;
using TSU.Functionalities.TestModules;
using TSU.Common.Compute.Transformation;
using TSU.Views.Message;
using static TSU.IO.SUSoft.CSGEOdll;
using SimpleInjector;
using TSU.Common.Compute.ShapesOutput;

namespace TSU.Common.Compute.Transformation
{
    public static class Systems
    {
        private const double na = Preferences.Preferences.NotAvailableValueNa;

        #region CHABA
        public static List<Point> BasedOnCommonPoints(List<Point> pointsInDestinationFrame, List<Point> pointsInOriginalFrame, out List<string> createdFileFullPaths, string chabaName = null)
        {
            try
            {
                bool ok = RenamePointsWithLongerNames(pointsInDestinationFrame, pointsInOriginalFrame, out List<Point> newPointsInDestinationFrame, out List<Point> newPointsInOriginalFrame);
                IO.SUSoft.Chaba chaba = new IO.SUSoft.Chaba(newPointsInOriginalFrame, newPointsInDestinationFrame, out createdFileFullPaths, silent: ok, chabaName, edit: !ok);
                return chaba.GetOutputPoints();
            }
            catch (Exception ex)
            {
                //   throw new Exception(string.Format("{0}\r\n{1}", "Helmert transformation failed", ex.Message));
                throw new Exception($"{R.T_HELMERT_TRANSFORMATION_FAILED}\r\n{ex.Message}", ex);
            }
        }

        /// <summary>
        /// this remove the end of the name if there are common roots
        /// </summary>
        /// <param name="list1"></param>
        /// <param name="list2"></param>
        /// <returns></returns>
        /// 
        private static bool RenamePointsWithLongerNames(List<Point> list1, List<Point> list2, out List<Point> newList1, out List<Point> newList2)
        {
            try
            {

                List<Point> tobeRenammed;
                List<Point> tobeUsedAsRef;
                ChooseWhichListShouldBeRenammed(list1, list2, out tobeRenammed, out tobeUsedAsRef, out bool list1IsRenammed);

                List<Point> newNamedPoints = new List<Point>();
                int numberOfRenamed = 0;

                foreach (Point item in tobeRenammed)
                {
                    int found = 0;
                    string newName = "";
                    foreach (Point source in tobeUsedAsRef)
                    {
                        int length = source._Name.Length;
                        int start = item._Name.Length - length;
                        if (start < 0) continue;

                        // same root
                        string shortName = item._Name.Substring(0, item._Name.Length - start);
                        if (shortName == source._Name)
                        {
                            // but if it is a root it should be existing already as full name, not to choose '10' and '11' as 2 iteration of '1'
                            if (shortName != item._Name)
                                if (tobeRenammed.FindAll(x => x._Name == shortName).Count > 0)
                                    continue;

                            found++;
                            numberOfRenamed++;
                            newName = source._Name;
                        }
                    }

                    if (found > 1)
                        throw new Exception($"{R.T_TOO_MUCH_MATCH_FOUND_FOR} {item._Name}");

                    Point newNamedPoint = item.Clone() as Point;
                    if (found == 1) // ok
                    {
                        newNamedPoint._Name = newName;
                    }
                    else
                    {
                        newNamedPoint._Name = item._Name;
                    }
                    newNamedPoints.Add(newNamedPoint);
                }
                if (numberOfRenamed < 3)
                    throw new Exception($"{R.T_NOT_ENOUGH_MATCHING_POINTS} ({numberOfRenamed})");

                if (list1IsRenammed)
                {
                    newList1 = newNamedPoints;
                    newList2 = list2;
                }
                else
                {
                    newList2 = newNamedPoints;
                    newList1 = list1;
                }
                return true;
            }
            catch (Exception ex)
            {
                newList1 = list1;
                newList2 = list2;
                new MessageInput(MessageType.Warning, $"{R.T_NAME_MATCHING_PROBLEM}\r\n{ex.Message}").Show();
                return false;
            }
        }

        private static void ChooseWhichListShouldBeRenammed(List<Point> list1, List<Point> list2, out List<Point> tobeRenammed, out List<Point> tobeUsedAsRef, out bool list1IsRenammed)
        {
            // count the total char to find which list need to be renammed/shorted, pff this is bad idea, if you have more points in the passive it gives a bad outcome
            list1IsRenammed = IsListOneToBeRenammed(list1, list2);

            if (list1IsRenammed)
            {
                tobeRenammed = list1;
                tobeUsedAsRef = list2;
            }
            else
            {
                tobeRenammed = list2;
                tobeUsedAsRef = list1;
            }
        }

        private static bool IsListOneToBeRenammed(List<Point> list1, List<Point> list2)
        {
            int charCount1 = 0;
            int bracketsCount1 = 0;
            foreach (var item in list1)
            {
                charCount1 += item._Name.Length;
                bracketsCount1 += item._Name.Count(x => x == '[');
            }

            int charCount2 = 0;
            int bracketsCount2 = 0;
            foreach (var item in list2)
            {
                charCount2 += item._Name.Length;
                bracketsCount2 += item._Name.Count(x => x == '[');
            }


            bool morePoints1 = list1.Count > list2.Count;
            bool moreBracket1 = bracketsCount1 > bracketsCount2;
            bool moreChar1 = charCount1 > charCount2;
            bool onlyBracketIn1 = bracketsCount1 > 0 && bracketsCount2 == 0;
            bool onlyBracketIn2 = bracketsCount2 > 0 && bracketsCount1 == 0;


            if (onlyBracketIn1) return true;
            if (onlyBracketIn2) return false;
            if (moreBracket1 && moreChar1) return true;
            if (!moreBracket1 && !moreChar1) return false;

            // ask user which list to renamme
            string active = "Active points";
            MessageInput mi = new MessageInput(MessageType.Critical, "Tsunami could not identify which list to renamme (removing brackets), please help.")
            {
                ButtonTexts = new List<string> { active, "Passive points" },
                
            };
            if (mi.Show().TextOfButtonClicked == active)
                return true;

            return false;

        }

        #endregion

        public static void TryToFixMissingCoordinates(List<Point> noCcsPoints, List<Point> noMlaPoints, List<Point> noSuPoints, List<Point> noPhyPoints,
            Coordinates.ReferenceFrames inputReferenceFrames, Z.Zone localZone)
        {
            if (inputReferenceFrames == Coordinates.ReferenceFrames.Unknown) return;

            // bool for easy test
            bool ccs = noCcsPoints.Count == 0;
            bool su = noSuPoints.Count == 0;
            bool mla = noMlaPoints.Count == 0;
            bool phy = noPhyPoints.Count == 0;
            bool zoneKnown = localZone != null;
            var ccs2MlaInfo = localZone?.Ccs2MlaInfo;
            var su2PhysInGon = localZone?.Su2PhysInGon;
            var su2PhysInM = localZone?.Su2PhysInM;
            var referenceSurface = ccs2MlaInfo != null? ccs2MlaInfo.ReferenceSurface : Coordinates.ReferenceSurfaces.RS2K;

            // special case if we have CCS-Z only, for exemple in a clipboard imprt after a chaba or a shapes computation
            foreach (var point in noCcsPoints)
            {
                var coordinates = point._Coordinates;
                bool hasCCS_H = coordinates.HasCcs;
                bool hasCCS_Z = coordinates.CoordinatesExistInAGivenSystem("CCS-Z");
                if (!hasCCS_H && hasCCS_Z)
                {
                    ZToH(point, referenceSurface);
                }
            }

            // not sure what is that...
            if (!zoneKnown)
            {
                if (ccs)
                    HtoZ(noSuPoints, referenceSurface);
                return;
            }

            while (true)
            {
                // Deal with the easy possibilities... (meaning there only one way to computer the missing coord.)
                if (ccs && !mla && !su && !phy)
                {
                    if (ccs2MlaInfo != null)
                    {
                        HtoZ(noCcsPoints, referenceSurface);
                        HtoZ(noSuPoints, referenceSurface);
                        if (ccs2MlaInfo.Origin != null)
                        {
                            Ccs2Mla(noMlaPoints, inputReferenceFrames, localZone);
                            if (localZone.Mla2SuInGon != null)
                            {
                                Mla2Su(noSuPoints, localZone.Mla2SuInGon);
                                if (su2PhysInGon != null)
                                {
                                    Systems.Su2Phys(noPhyPoints, su2PhysInGon, localZone.BeamInclinaisonInRad, su2PhysInM);
                                }
                            }
                        }
                    }
                    break;
                }

                if (!ccs && !mla && !su && phy)
                {
                    if (su2PhysInGon != null)
                    {
                        Systems.Phys2Su(noSuPoints, su2PhysInGon, localZone.BeamInclinaisonInRad, su2PhysInM);
                        if (localZone.Mla2SuInGon != null)
                        {
                            Su2Mla(noMlaPoints, localZone.Mla2SuInGon);
                            if (ccs2MlaInfo != null)
                            {
                                Mla2Ccs(noCcsPoints, inputReferenceFrames, localZone);
                                HtoZ(noCcsPoints, referenceSurface);
                            }
                        }
                    }
                    break;
                }

                if (!ccs && !mla && su && !phy)
                {
                    if (su2PhysInGon != null)
                    {
                        Systems.Su2Phys(noPhyPoints, su2PhysInGon, localZone.BeamInclinaisonInRad, su2PhysInM);
                        if (localZone.Mla2SuInGon != null)
                        {
                            Su2Mla(noMlaPoints, localZone.Mla2SuInGon);
                            if (ccs2MlaInfo != null)
                            {
                                Mla2Ccs(noCcsPoints, inputReferenceFrames, localZone);
                                HtoZ(noCcsPoints, referenceSurface);
                            }
                        }
                    }
                    break;
                }

                if (!ccs && mla && !su && !phy)
                {
                    if (localZone.Mla2SuInGon != null)
                    {
                        Mla2Su(noSuPoints, localZone.Mla2SuInGon);
                        if (su2PhysInGon != null)
                        {
                            Systems.Su2Phys(noPhyPoints, su2PhysInGon, localZone.BeamInclinaisonInRad, su2PhysInM);
                        }
                    }
                    if (ccs2MlaInfo != null)
                    {
                        Mla2Ccs(noCcsPoints, inputReferenceFrames, localZone);
                        HtoZ(noCcsPoints, referenceSurface);
                    }
                    break;
                }
                break;
                throw new Exception($"{R.T_SORRY}\r\n{R.T_BUT_YOU_HAVE_POINTS_WITH_MISSING_CCS}!\r\n{R.T_THIS_COULD_BE_TO_DIFFICULT_TO_FIX_HERE}.");
            }

        }


        #region rot & transl

        public static Coordinates Translation3D(Coordinates pointToTranslate, Coordinates translationValues, bool invert = false)
        {
            if (invert)
                return new Coordinates(pointToTranslate._Name,
                    pointToTranslate.X.Value + translationValues.X.Value,
                pointToTranslate.Y.Value + translationValues.Y.Value,
                pointToTranslate.Z.Value + translationValues.Z.Value);
            else
                return new Coordinates(pointToTranslate._Name,
                    pointToTranslate.X.Value - translationValues.X.Value,
                    pointToTranslate.Y.Value - translationValues.Y.Value,
                    pointToTranslate.Z.Value - translationValues.Z.Value);
        }

        public static Coordinates Translation3D(Coordinates pointToTranslate, Translation3D translation, bool invert = false)
        {
            if (invert)
                return new Coordinates(pointToTranslate._Name,
                    pointToTranslate.X.Value - translation.AlongX,
                pointToTranslate.Y.Value - translation.AlongY,
                pointToTranslate.Z.Value - translation.AlongZ);
            else
                return new Coordinates(pointToTranslate._Name,
                    pointToTranslate.X.Value + translation.AlongX,
                    pointToTranslate.Y.Value + translation.AlongY,
                    pointToTranslate.Z.Value + translation.AlongZ);
        }

        public static Coordinates Rotate3DAroundOrigin(Coordinates pointToRotate, double xAngle, double yAngle, double zAngle, ENUM.AngularUnit unit = ENUM.AngularUnit.Gon, bool invert = false)
        {
            //by default it works in degrees
            switch (unit)
            {
                case ENUM.AngularUnit.Gon:
                    xAngle = T.Conversions.Angles.Gon2Deg(xAngle);
                    yAngle = T.Conversions.Angles.Gon2Deg(yAngle);
                    zAngle = T.Conversions.Angles.Gon2Deg(zAngle);
                    break;
                case ENUM.AngularUnit.Rad:
                    xAngle = T.Conversions.Angles.Rad2Deg(xAngle);
                    yAngle = T.Conversions.Angles.Rad2Deg(yAngle);
                    zAngle = T.Conversions.Angles.Rad2Deg(zAngle);
                    break;
                case ENUM.AngularUnit.deg:
                    break;
                default:
                    break;
            }

            Point3D p = new Point3D(
                pointToRotate.X.Value,
                pointToRotate.Y.Value,
                pointToRotate.Z.Value);

            Transform3DGroup group = new Transform3DGroup();
            if (invert)
            {
                group.Children.Add(new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(1, 0, 0), -xAngle)));
                group.Children.Add(new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(0, 1, 0), -yAngle)));
                group.Children.Add(new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(0, 0, 1), -zAngle)));
            }
            else
            {
                group.Children.Add(new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(0, 0, 1), zAngle)));
                group.Children.Add(new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(0, 1, 0), yAngle)));
                group.Children.Add(new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(1, 0, 0), xAngle)));
            }

            p = group.Transform(p);

            return new Coordinates(pointToRotate._Name, p.X, p.Y, p.Z);
        }

        public static Coordinates Rotate3DAroundAPoint(Coordinates pointToRotate, Coordinates originOfRotation, double xAngle, double yAngle, double zAngle, ENUM.AngularUnit unit = ENUM.AngularUnit.Gon, bool invert = false)
        {
            Coordinates c = Translation3D(pointToRotate, originOfRotation);

            return Rotate3DAroundOrigin(c, xAngle, yAngle, zAngle, unit, invert);
        }

        public static Coordinates Rotate3DAroundOrigin(Coordinates item, Transformation.Rotation3D rotation3D, ENUM.AngularUnit angularUnit = ENUM.AngularUnit.Rad, bool invert = false)
        {
            return Rotate3DAroundOrigin(item, rotation3D.RotX, rotation3D.RotY, rotation3D.RotZ, angularUnit, invert);
        }

        #endregion

        #region H & Z

        public static void ConvertHToZWithOffset(Coordinates.ReferenceSurfaces geoide, double offset, Point point)
        {
            var cH = point._Coordinates.GetOrCreateByName("CCS-H");
            var cZ = point._Coordinates.GetCoordinatesInASystemByName("CCS-Z");

            //rajoute le h instrum car c'est à ce niveau qu'on compare les distances
            var newZ = HtoZ(
                cH.X.Value,
                cH.Y.Value,
                cH.Z.Value + offset, geoide, out bool outOfGrid);
            cZ.Z.Value = newZ;

            if (outOfGrid)
                Logs.Log.AddEntryAsPayAttentionOf(Tsunami2.Properties, $"{point._Name} is out of the CERN grid");
        }

        //public static void HtoZ(Point point, Coordinates.ReferenceSurfaces refSurf)
        //{
        //    //string geoideRefForSurveylib = GetGeoidRefForSurveylibFromReferenceSurface(refSurf);
        //    HtoZ(point, refSurf);
        //}

        public static void HtoZ(Point point, Coordinates.ReferenceSurfaces geoideRef)
        {
            var csH = point._Coordinates.GetCoordinatesInASystemByName("CCS-H");
            var csZ = point._Coordinates.GetOrCreateByName("CCS-Z");

            csZ.X = csH.X;
            csZ.Y = csH.Y;
            csZ.Z = new DoubleValue();
            csZ.Z.Value = HtoZ(csH.X.Value, csH.Y.Value, csH.Z.Value, geoideRef, out bool outOfGrid);
            if (outOfGrid)
                Logs.Log.AddEntryAsPayAttentionOf(Tsunami2.Properties, $"{point._Name} is out of the CERN grid");
        }

        internal static string GetGeoidRefForSurveylibFromReferenceSurface(Coordinates.ReferenceSurfaces refSurf)
        {
            switch (refSurf)
            {
                case Coordinates.ReferenceSurfaces.Sphere:
                    return "SPHERE";
                case Coordinates.ReferenceSurfaces.SurfaceTopo1985:
                    return "CG1985N0";
                case Coordinates.ReferenceSurfaces.Machine1985:
                    return "CG1985";
                case Coordinates.ReferenceSurfaces.RS2K:
                    return "CG2000";
                case Coordinates.ReferenceSurfaces.Unknown:
                case Coordinates.ReferenceSurfaces.Plane:
                default:
                    return "CG2000";
            }
        }

        internal static void HtoZ(List<Point> noCcsPoints, Coordinates.ReferenceSurfaces refSurf)
        {
            foreach (Point item in noCcsPoints)
            {
                HtoZ(item, refSurf);
            }
        }
        public static double HtoZ(double xold, double yold, double hold, Coordinates.ReferenceSurfaces rf, out bool outOfGrid)
        {
            outOfGrid = false;

            int fRefFrameInput = (int)GetCsGeoDllRefFrameBasedOn(rf, Coordinates.CoordinateSystemsTsunamiTypes.CCS);

            bool needGrid = (102 <= fRefFrameInput) && (fRefFrameInput >= 106); // Geoides

            if (needGrid && !Coordinates.IsInGrid(xold, yold)) 
            {
                outOfGrid = true;
                fRefFrameInput = 107; // kCERNXYHsSphereSPS = 107, 2D + H. //X and Y coordinates come from the CCS. The H coordinate represents the height of the sphere defined for the SPS
            }

            int fRefFrameOutput = 0; // ccs cartesian

            (double x, double y, double z) = TransformPoint(xold, yold, hold,
                inputParams: new TPlainDataParameters()
                {
                    fAngUnits = (int)AngularUnits.Gons,
                    fCoordSys = (int)CSTypes._2D_H,
                    fEpoch = 0.0,
                    fRefFrame = fRefFrameInput,
                    fSolution = "\0"
                },
                inputLocalOrigin: null,
                outputParams: new TPlainDataParameters()
                {
                    fAngUnits = (int)AngularUnits.Gons,
                    fCoordSys = (int)CSTypes._3D_Cartesian,
                    fEpoch = 0.0,
                    fRefFrame = fRefFrameOutput,
                    fSolution = "\0"
                },
                null);
            return z;
        }

        public static double ZToH(double xold, double yold, double zold, Coordinates.ReferenceSurfaces rf)
        {
            int fRefFrameInput = 0;

            int fRefFrameOutput = (int)GetCsGeoDllRefFrameBasedOn(rf, Coordinates.CoordinateSystemsTsunamiTypes.CCS);

            bool needGrid = (102 <= fRefFrameInput) && (fRefFrameInput >= 106); // Geoides

            if (needGrid && !Coordinates.IsInGrid(xold, yold))
            {
                fRefFrameInput = 107; // kCERNXYHsSphereSPS   = 107, 2D + H. //X and Y coordinates come from the CCS. The H coordinate represents the height of the sphere defined for the SPS
            }

            (double x, double y, double h) = TransformPoint(xold, yold, zold,
                inputParams: new TPlainDataParameters()
                {
                    fAngUnits = (int)AngularUnits.Gons,
                    fCoordSys = (int)CSTypes._3D_Cartesian,
                    fEpoch = 0.0,
                    fRefFrame = fRefFrameInput,
                    fSolution = "\0"
                },
                inputLocalOrigin: null,
                outputParams: new TPlainDataParameters()
                {
                    fAngUnits = (int)AngularUnits.Gons,
                    fCoordSys = (int)CSTypes._2D_H,
                    fEpoch = 0.0,
                    fRefFrame = fRefFrameOutput,
                    fSolution = "\0"
                },null);
            return h;
        }

        public static void ZToH(Point point, Coordinates.ReferenceSurfaces geoideRef)
        {
            var csZ = point._Coordinates.GetCoordinatesInASystemByName("CCS-Z");
            var csH = point._Coordinates.GetOrCreateByName("CCS-H");


            csH.X = new DoubleValue(csZ.X.Value);
            csH.Y = new DoubleValue(csZ.Y.Value);
            csH.Z = new DoubleValue(ZToH(csZ.X.Value, csZ.Y.Value, csZ.Z.Value, geoideRef));
        }


        #endregion

        #region CERN <> MLA
       
        internal static void Ccs2Mla(List<Point> noMlaPoints, Coordinates.ReferenceFrames inputReferenceFrames, Z.Zone localZone)
        {
            // Check if all all ccs coordinates and H
            bool allHaveCcs = true;
            List<Point> noHPoints = new List<Point>();
            foreach (Point p in noMlaPoints)
            {
                if (!p._Coordinates.HasCcs)
                {
                    allHaveCcs = false;
                }
                if (p._Coordinates.Ccs.Z.Value == na)
                {
                    noHPoints.Add(p);
                }
            }

            foreach (Point p in noHPoints)
            {
                ZToH(p, localZone.Ccs2MlaInfo.ReferenceSurface);
            }
            if (inputReferenceFrames == Coordinates.ReferenceFrames.CCS)
                inputReferenceFrames = Coordinates.GetReferenceFrame(localZone.Ccs2MlaInfo.ReferenceSurface, Coordinates.CoordinateSystemsTypes._2DPlusH);

            // react
            if (allHaveCcs)
            {
                //inputReferenceFrames = Coordinates.ReferenceFrames.CernXYHg85Machine;
                Coordinates.ReferenceFrames toUse = inputReferenceFrames == Coordinates.ReferenceFrames.Unknown ? Coordinates.ReferenceFrames.CernXYHg00Machine : inputReferenceFrames;
                Z.Zone.Ccs2Mla info = localZone.Ccs2MlaInfo;
                foreach (Point p in noMlaPoints)
                {
                    (double x, double y, double z) = TransformPoint(p._Coordinates.Ccs.X.Value, p._Coordinates.Ccs.Y.Value, p._Coordinates.Ccs.Z.Value,
                        inputParams: new TPlainDataParameters()
                        {
                            fAngUnits = (int)AngularUnits.Gons,
                            fCoordSys = (int)CSTypes._2D_H,
                            fEpoch = 0.0,
                            fRefFrame = (int)GetCsGeoDllRefFrameBasedOn(info.ReferenceSurface, Coordinates.CoordinateSystemsTsunamiTypes.CCS),
                            fSolution = "\0"
                        },
                        inputLocalOrigin: null,
                        outputParams: new TPlainDataParameters()
                        {
                            fAngUnits = (int)AngularUnits.Gons,
                            fCoordSys = (int)CSTypes._3D_Cartesian,
                            fEpoch = 0.0,
                            fRefFrame = (int)GetCsGeoDllRefFrameBasedOn(info.ReferenceSurface, Coordinates.CoordinateSystemsTsunamiTypes.MLA),
                            fSolution = "\0"
                        },
                        outputLocalOrigin: new TPlainLocalSystemOrigin()
                        {   
                            fGisement = info.Bearing.Value,
                            fSlope = info.Slope.Value, 
                            fX = info.Origin.X.Value,
                            fY = info.Origin.Y.Value,
                            fZ = info.Origin.Z.Value,
                        });
                    
                    p._Coordinates.Mla = new Coordinates("", x, y, z); 
                }
            }
            else
            {
                //   throw new Exception("Convertion CCS to MLA Failed because Not all points have CCS coordinates");
                throw new Exception(R.T_CONVERTION_CCS_TO_MLA_FAILED_BECAUSE_NOT_ALL_POINTS_HAVE_CCS_COORDINATES);
            }

            // Check if all have now mla coordinates
            bool allHaveMla = true;
            foreach (Point p in noMlaPoints)
            {
                if (!p._Coordinates.HasMla)
                {
                    allHaveMla = false;
                    break;
                }
                else
                {
                    p._Coordinates.Mla.SystemName = "MLA";
                }

            }

            if (!allHaveMla)
            {
                //  throw new Exception("Convertion CCS to MLA Failed");
                throw new Exception(R.T_CONVERTION_CCS_TO_MLA_FAILED);
            }

        }

        private static int GetCsGeoDllRefFrameBasedOn(Coordinates.ReferenceSurfaces referenceSurface, Coordinates.CoordinateSystemsTsunamiTypes outputTtype)
        {
            switch (referenceSurface)
            {
                case Coordinates.ReferenceSurfaces.Sphere:
                    if (outputTtype == Coordinates.CoordinateSystemsTsunamiTypes.CCS)
                        return (int)ReferenceFrames.kCERNXYHsSphereSPS;
                    else
                        return (int)ReferenceFrames.kMLASphere;
                case Coordinates.ReferenceSurfaces.SurfaceTopo1985:
                    if (outputTtype == Coordinates.CoordinateSystemsTsunamiTypes.CCS)
                        return (int)ReferenceFrames.kCernXYHg85;
                    else
                        return (int)ReferenceFrames.kMLA1985H0;
                case Coordinates.ReferenceSurfaces.Machine1985:
                    if (outputTtype == Coordinates.CoordinateSystemsTsunamiTypes.CCS)
                        return (int)ReferenceFrames.kCernXYHg85Machine;
                    else
                        return (int)ReferenceFrames.kMLA1985Machine;
                case Coordinates.ReferenceSurfaces.Plane:
                case Coordinates.ReferenceSurfaces.Unknown:
                case Coordinates.ReferenceSurfaces.RS2K:
                default:
                    if (outputTtype == Coordinates.CoordinateSystemsTsunamiTypes.CCS)
                        return (int)ReferenceFrames.kCernXYHg00Machine;
                    else
                        return (int)ReferenceFrames.kMLA2000Machine;
            }
        }

        internal static void Mla2Ccs(List<Point> noCcsPoints, Coordinates.ReferenceFrames inputReferenceFrames, Z.Zone localZone)
        {
            // Check if all all ccs coordinates
            bool allHaveMla = true;
            foreach (Point p in noCcsPoints)
            {
                if (!p._Coordinates.HasMla)
                {
                    allHaveMla = false;
                    break;
                }
            }

            // react
            if (allHaveMla)
            {
                //Coordinates.ReferenceFrames inputRefFrames = CheckInputReferenceFrames(inputReferenceFrames);
                //Coordinates.ReferenceFrames toUse = inputReferenceFrames == Coordinates.ReferenceFrames.Unknown ? Coordinates.ReferenceFrames.CernXYHg00Machine : inputReferenceFrames;
                
                Z.Zone.Ccs2Mla info = localZone.Ccs2MlaInfo;
                foreach (Point p in noCcsPoints)
                {
                    (double x, double y, double z) = TransformPoint(p._Coordinates.Mla.X.Value, p._Coordinates.Mla.Y.Value, p._Coordinates.Mla.Z.Value,
                        inputParams: new TPlainDataParameters()
                        {
                            fAngUnits = (int)AngularUnits.Gons,
                            fCoordSys = (int)CSTypes._3D_Cartesian,
                            fEpoch = 0.0,
                            fRefFrame = (int)GetCsGeoDllRefFrameBasedOn(info.ReferenceSurface, Coordinates.CoordinateSystemsTsunamiTypes.MLA),
                            fSolution = "\0"
                        },
                        inputLocalOrigin: new TPlainLocalSystemOrigin()
                        {
                            fGisement = info.Bearing.Value,
                            fSlope = info.Slope.Value,
                            fX = info.Origin.X.Value,
                            fY = info.Origin.Y.Value,
                            fZ = info.Origin.Z.Value,
                        },
                        outputParams: new TPlainDataParameters()
                        {
                            fAngUnits = (int)AngularUnits.Gons,
                            fCoordSys = (int)CSTypes._2D_H,
                            fEpoch = 0.0,
                            fRefFrame = (int)GetCsGeoDllRefFrameBasedOn(info.ReferenceSurface, Coordinates.CoordinateSystemsTsunamiTypes.CCS),
                            fSolution = "\0"
                        },
                        outputLocalOrigin: null);
                    p._Coordinates.Ccs.Set(x, y, z);
                }
            }
            else
            {
                //   throw new Exception("Convertion CCS to MLA Failed because Not all points have CCS coordinates");
                throw new Exception(R.T_CONVERTION_CCS_TO_MLA_FAILED_BECAUSE_NOT_ALL_POINTS_HAVE_CCS_COORDINATES);
            }

            // Check if all have now mla coordinates
            bool allHaveCcs = true;
            foreach (Point p in noCcsPoints)
            {
                if (!p._Coordinates.HasCcs)
                {
                    allHaveCcs = false;
                    break;
                }
            }

            if (!allHaveCcs)
            {
                //  throw new Exception("Convertion CCS to MLA Failed");
                throw new Exception(R.T_CONVERTION_CCS_TO_MLA_FAILED);
            }
        }


        private static Coordinates.ReferenceFrames CheckInputReferenceFrames(Coordinates.ReferenceFrames referenceFrames)
        {
            switch (referenceFrames)
            {
                case Coordinates.ReferenceFrames.CernXYHg85Machine:
                case Coordinates.ReferenceFrames.MLA1985Machine:
                case Coordinates.ReferenceFrames.Su1985Machine:
                case Coordinates.ReferenceFrames.Phys1985Machine:
                    return Coordinates.ReferenceFrames.MLA1985Machine;

                case Coordinates.ReferenceFrames.CernXYHg00Machine:
                case Coordinates.ReferenceFrames.MLA2000Machine:
                case Coordinates.ReferenceFrames.Su2000Machine:
                case Coordinates.ReferenceFrames.Phys2000Machine:
                    return Coordinates.ReferenceFrames.MLA2000Machine;


                case Coordinates.ReferenceFrames.CERNXYHsSphereSPS:
                case Coordinates.ReferenceFrames.MLASphere:
                case Coordinates.ReferenceFrames.SuSphere:
                case Coordinates.ReferenceFrames.PhysSphere:
                    return Coordinates.ReferenceFrames.MLASphere;

                default:
                    return Coordinates.ReferenceFrames.Unknown;
            }
        }

        #endregion

        #region SU <> MLA

        public static void Su2Mla(TheoreticalElement n)
        {
            Su2Mla(n.Elements.Cast<Point>().ToList(), n.Definition.LocalZone.Mla2SuInGon);
        }

        public static void Su2Mla(List<Point> list, Transformation.Rotation3D su2mla)
        {
            if (su2mla != null)
            {
                foreach (Point item in list)
                {
                    item._Coordinates.Mla = Rotate3DAroundOrigin(item._Coordinates.Su, su2mla, ENUM.AngularUnit.Gon, true);
                }
            }
        }

        /// <summary>
        /// This replace the pooint.localcoordinate from su to MLA
        /// </summary>
        /// <param name="point"></param>
        /// <param name="su2mla"></param>
        public static void Su2Mla(Point point, Transformation.Rotation3D su2mla)
        {
            if (su2mla != null)
            {
                point._Coordinates.Local = Rotate3DAroundOrigin(point._Coordinates.Local, su2mla, ENUM.AngularUnit.Gon, true);
            }
        }

        public static void Mla2Su(TheoreticalElement n)
        {
            Mla2Su(n.Elements.Cast<Point>().ToList(), n.Definition.LocalZone.Mla2SuInGon);
        }

        public static void Mla2Su(List<Point> list, Transformation.Rotation3D mla2su)
        {
            // if (mla2su == null) throw new Exception("Rotation between MLA and SU is not defined");
            if (mla2su == null) throw new Exception(R.T_ROTATION_BETWEEN_MLA_AND_SU_IS_NOT_DEFINED);
            foreach (Point item in list)
            {
                item._Coordinates.Su = Rotate3DAroundOrigin(item._Coordinates.Mla, mla2su, ENUM.AngularUnit.Gon, false);
            }
        }

        #endregion

        #region SU <> Phys

        //public static void Su2Phys(TheoreticalElement n)
        //{
        //    if (n.Definition.LocalZone.Su2PhysInGon != null)
        //    {
        //        foreach (E.Point item in n.Elements.Cast<E.Point>().ToList())
        //        {
        //            E.Point p = item;
        //            Su2Phys(p, n.Definition.LocalZone.Su2PhysInGon, n.Definition.LocalZone.BeamInclinaisonInRad);
        //        }
        //    }
        //}

        public static void Su2Phys(List<Point> points, Transformation.Rotation3D physVsSu, Transformation.Rotation beamInclinaison, Translation3D t1 = null)
        {
            foreach (Point p in points)
            {
                Su2Phys(p, physVsSu, beamInclinaison, t1);
            }
        }

        public static Coordinates Su2Phys(Coordinates c, Z.Zone lz)
        {
            Point p = new Point();
            p._Coordinates.Su = c;
            if (lz.Su2PhysInGon != null)
            {
                Su2Phys(p, lz.Su2PhysInGon, lz.BeamInclinaisonInRad);
                return p._Coordinates.Physicist;
            }
            else
                throw new Exception(R.T029);
        }

        /// <summary>
        /// R1 is the axis flip, R2 the beam inclianions from the horizontal, t1 the translation of the su origin in the physicist cs
        /// </summary>
        /// <param name="point"></param>
        /// <param name="physVsSu"></param>
        /// <param name="beamInclinaison"></param>
        /// <param name="t1"></param>
        public static void Su2Phys(Point point, Transformation.Rotation3D physVsSu, Transformation.Rotation beamInclinaison, Translation3D t1 = null)
        {
            // Get value from xml file
            //TSU.Compute.Rotation3D r = TsuPreferences.Instance.Zone3DRotations.list.Find(x => x.Management.Zone == zone);
            bool InverseRotation = false;
            Coordinates phys = null;
            if (beamInclinaison != null)
            {
                phys = Rotate3DAroundOrigin(
                    point._Coordinates.Su,
                    0, -beamInclinaison.Rot, 0,
                    ENUM.AngularUnit.Rad, InverseRotation);
            }
            else
            {
                phys = point._Coordinates.Su.Clone() as Coordinates;
                point._Coordinates.Physicist = phys;
            }
            phys = Rotate3DAroundOrigin(
                                phys,
                                physVsSu.RotX, physVsSu.RotY, physVsSu.RotZ,
                                ENUM.AngularUnit.Gon, InverseRotation);

            if (t1 != null)
            {
                phys = Translation3D(phys, t1);
            }

            point._Coordinates.Physicist = phys;
        }

        public static void Phys2Su(List<Point> points, Transformation.Rotation3D physVsSu, Transformation.Rotation beamInclinaison, Translation3D t1 = null)
        {
            foreach (Point p in points)
            {
                Phys2Su(p, physVsSu, beamInclinaison, t1);
            }
        }

        public static void Phys2Su(TheoreticalElement n)
        {
            if (n.Definition.LocalZone.Su2PhysInGon != null)
            {
                foreach (Point item in n.Elements.Cast<Point>().ToList())
                {
                    Point p = item;
                    Phys2Su(p, n.Definition.LocalZone.Su2PhysInGon, n.Definition.LocalZone.BeamInclinaisonInRad);
                }
            }
        }

        public static void Phys2Su(Point point, Transformation.Rotation3D physVsSu, Transformation.Rotation beamInclinaison, Translation3D t1 = null)
        {
            // Get value from xml file
            //TSU.Compute.Rotation3D r = TsuPreferences.Instance.Zone3DRotations.list.Find(x => x.Management.Zone == zone);
            bool InverseRotation = true;

            if (t1 != null)
                point._Coordinates.Su = Translation3D(point._Coordinates.Physicist, t1, true);
            else
                point._Coordinates.Su = point._Coordinates.Physicist.Clone() as Coordinates;

            point._Coordinates.Su = Rotate3DAroundOrigin(
                        point._Coordinates.Su,
                        physVsSu.RotX, physVsSu.RotY, physVsSu.RotZ,
                        ENUM.AngularUnit.Gon, InverseRotation);

            if (beamInclinaison != null)
            {
                point._Coordinates.Su = Rotate3DAroundOrigin(
                        point._Coordinates.Su,
                        0, -beamInclinaison.Rot, 0,
                        ENUM.AngularUnit.Rad, InverseRotation);
            }



        }

        #endregion


        #region Compute From

        public static void RecomputeCoordinateFromCCS(Point p)
        {
            List<Point> pointsWithoutCoord = new List<Point>() { p };
            List<Point> pointsWithCoord = new List<Point>();

            Z.Zone zone = Tsunami2.Properties.Zone;
            if (zone == null) zone = new Z.Zone() { Ccs2MlaInfo = new Z.Zone.Ccs2Mla() { ReferenceSurface = Coordinates.ReferenceSurfaces.RS2K } };
            Coordinates.ReferenceSurfaces rs = zone.Ccs2MlaInfo.ReferenceSurface;
            Coordinates.ReferenceFrames rf = Coordinates.GetXYhSystem(rs);

            var csH = p._Coordinates.GetCoordinatesInASystemByName("CCS-H");
            var csZ = p._Coordinates.GetCoordinatesInASystemByName("CCS-Z");

            HtoZ(p, rs);
            TryToFixMissingCoordinates(pointsWithCoord, pointsWithoutCoord, pointsWithoutCoord, pointsWithoutCoord, rf, zone);
        }
        public static void RecomputeCoordinateFromMLA(Point p)
        {
            List<Point> pointsWithoutCoord = new List<Point>() { p };
            List<Point> pointsWithCoord = new List<Point>();

            Z.Zone zone = Tsunami2.Properties.Zone;

            if (zone == null) zone = new Z.Zone() { Ccs2MlaInfo = new Z.Zone.Ccs2Mla() { ReferenceSurface = Coordinates.ReferenceSurfaces.RS2K } };
            Coordinates.ReferenceFrames rf = Coordinates.GetXYhSystem(zone.Ccs2MlaInfo.ReferenceSurface);

            TryToFixMissingCoordinates(pointsWithoutCoord, pointsWithCoord, pointsWithoutCoord, pointsWithoutCoord, rf, zone);
        }

        public static void RecomputeCoordinateFromSu(Point p)
        {
            List<Point> pointsWithoutCoord = new List<Point>() { p };
            List<Point> pointsWithCoord = new List<Point>();

            Z.Zone zone = Tsunami2.Properties.Zone;

            if (zone == null) zone = new Z.Zone() { Ccs2MlaInfo = new Z.Zone.Ccs2Mla() { ReferenceSurface = Coordinates.ReferenceSurfaces.RS2K } };
            Coordinates.ReferenceFrames rf = Coordinates.GetXYhSystem(zone.Ccs2MlaInfo.ReferenceSurface);

            TryToFixMissingCoordinates(pointsWithoutCoord, pointsWithoutCoord, pointsWithCoord, pointsWithoutCoord, rf, zone);
        }

        public static void RecomputeCoordinateFromPhy(Point p)
        {
            List<Point> pointsWithoutCoord = new List<Point>() { p };
            List<Point> pointsWithCoord = new List<Point>();

            Z.Zone zone = Tsunami2.Properties.Zone;

            if (zone == null) zone = new Z.Zone() { Ccs2MlaInfo = new Z.Zone.Ccs2Mla() { ReferenceSurface = Coordinates.ReferenceSurfaces.RS2K } };
            Coordinates.ReferenceFrames rf = Coordinates.GetXYhSystem(zone.Ccs2MlaInfo.ReferenceSurface);

            TryToFixMissingCoordinates(pointsWithoutCoord, pointsWithoutCoord, pointsWithoutCoord, pointsWithCoord, rf, zone);
        }

        /// <summary>
        /// create and add coordinates to everycustom system , aplyign the transforamtion matrix of the system to the SU coordiantes if existing or to the CCS ones.
        /// </summary>
        /// <param name="measuredPoint"></param>
        /// <exception cref="NotImplementedException"></exception>
        internal static void SuOrCcs2Customs(Point point)
        {
            var customCs = Tsunami2.Properties.CoordinatesSystems.GetCustoms();
            foreach (var cs in customCs)
            {
                E.Manager.Module.NFC_ApplyTransformation(new List<Point>() { point }, cs);
            }
        }

        internal static void SuOrCcs2Customs(List<Point> points)
        {
            var customCs = Tsunami2.Properties.CoordinatesSystems.GetCustoms();
            foreach (var cs in customCs)
            {
                E.Manager.Module.NFC_ApplyTransformation(points, cs);
            }
        }

        public static bool Ccs2Mla(double xOri, double yOri, double zOri, ref double xTransform, ref double yTransform, ref double zTransform, Coordinates.ReferenceSurfaces geoid)
        {
            int fRefFrame = (int)GetCsGeoDllRefFrameBasedOn(geoid, Coordinates.CoordinateSystemsTsunamiTypes.CCS);
            (xTransform, yTransform, zTransform) = TransformPoint(xTransform, yTransform, zTransform,
                inputParams: new TPlainDataParameters()
                {
                    fAngUnits = (int)AngularUnits.Gons,
                    fCoordSys = (int)CSTypes._3D_Cartesian,
                    fEpoch = 0.0,
                    fRefFrame = 0,
                    fSolution = "\0"
                },
                inputLocalOrigin: null,
                outputParams: new TPlainDataParameters()
                {
                    fAngUnits = (int)AngularUnits.Gons,
                    fCoordSys = (int)CSTypes._3D_Cartesian,
                    fEpoch = 0.0,
                    fRefFrame = (int)GetCsGeoDllRefFrameBasedOn(geoid, Coordinates.CoordinateSystemsTsunamiTypes.MLA),
                    fSolution = "\0"
                },
                outputLocalOrigin: new TPlainLocalSystemOrigin()
                {
                    fGisement = 0,
                    fSlope = 0,
                    fX = xOri,
                    fY = yOri,
                    fZ = zOri,
                });;
            return true;
        }

        internal static void Mla2CcsH(double oX, double oY, double oZ, ref double x, ref double y, ref double h, Coordinates.ReferenceSurfaces geoid)
        {
            (x, y, h) = TransformPoint(oX, oY, oZ,
                inputParams: new TPlainDataParameters()
                {
                    fAngUnits = (int)AngularUnits.Gons,
                    fCoordSys = (int)CSTypes._2D_H,
                    fEpoch = 0.0,
                    fRefFrame = (int)GetCsGeoDllRefFrameBasedOn(geoid, Coordinates.CoordinateSystemsTsunamiTypes.MLA),
                    
                    fSolution = "\0"
                },
                inputLocalOrigin: null,
                outputParams: new TPlainDataParameters()
                {
                    fAngUnits = (int)AngularUnits.Gons,
                    fCoordSys = (int)CSTypes._2D_H,
                    fEpoch = 0.0,
                    fRefFrame = (int)GetCsGeoDllRefFrameBasedOn(geoid, Coordinates.CoordinateSystemsTsunamiTypes.CCS),
                    fSolution = "\0"
                },
                outputLocalOrigin: new TPlainLocalSystemOrigin()
                {
                    fGisement = 0,
                    fSlope = 0,
                    fX = oX,
                    fY = oY,
                    fZ = oZ,
                });
        }

        #endregion

    }
}
