﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using CsvHelper;
using CsvHelper.Configuration;
using TSU.Views.Message;
using static TSU.Common.Compute.Rabot.Rabot;
using R = TSU.Common.Compute.Rabot;

namespace TSU.Common.Compute.CsvImporter
{
    public static class CsvImporter
    {
        public static List<R.Rabot> ImportRabotData(string filePath)
        {
            using (var reader = new StreamReader(filePath))
            using (var csv = new CsvReader(reader, new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                Delimiter = ",",
                HasHeaderRecord = true,
                MissingFieldFound = null,
                BadDataFound = null,
                HeaderValidated = null
            }))
            {
                csv.Context.RegisterClassMap<RabotMap>();
                var records = csv.GetRecords<R.Rabot>().ToList();
                return records;
            }
        }



        public static string ImportRabotCSV(TSU.Common.FinalModule module)
        {

            string openedFile = "";
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                // Set filter options and filter index.
                Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*",
                FilterIndex = 1,

                // Set the initial directory to the user's documents folder.
                InitialDirectory = Tsunami2.Preferences.Values.Paths.FichierTheo,

                // Restore the directory to the previously selected directory.
                RestoreDirectory = true
            };

            // Show the dialog and get result
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                // Get the path of the selected file
                string filePath = openFileDialog.FileName;

                using (var reader = new StreamReader(filePath))
                using (var csv = new CsvReader(reader, new CsvConfiguration(CultureInfo.InvariantCulture) { HasHeaderRecord = false }))
                {
                    if (csv.Read())
                    {
                        string firstColumnValue = csv.GetField(0).ToUpper();
                        if (firstColumnValue == "VERTICAL" || firstColumnValue == "RADIAL")
                        {
                            //Create a copy of the file without the header
                            // Get the directory name and file name without extension
                            string directory = Path.GetDirectoryName(filePath);
                            string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(filePath);

                            // Combine directory and file name without extension to get full path without extension
                            string fullPathWithoutExtension = Path.Combine(directory, fileNameWithoutExtension);
                            string fileExtension = Path.GetExtension(filePath);

                            // Add "_copy" suffix before the file extension
                            string newPath = $"{fullPathWithoutExtension}_copy{fileExtension}";
                            var retainedLines = File.ReadAllLines(filePath).Skip(2); // with new rabot 1.4.4_beta2 ther is 3 headers line
                            File.WriteAllLines(newPath, retainedLines);
                            // Skip the type row and read actual data
                            List<R.Rabot> listRabot = ImportRabotData(newPath);
                            var rabotDictionary = firstColumnValue == "VERTICAL"
                                ? module.VerticalRabot
                                : module.RadialRabot;
                            openedFile = firstColumnValue == "VERTICAL"
                                ? "Vertical csv: "
                                : "Radial csv: ";
                            openedFile += filePath;
                            if (!module.RabotFiles.Contains(openedFile))
                            {
                                module.RabotFiles.Add(openedFile);
                            }

                            foreach (R.Rabot rabot in listRabot)
                            {
                                if (!rabotDictionary.ContainsKey(rabot.Name))
                                    rabotDictionary.Add(rabot.Name, rabot);
                                else
                                    AddOrUpdateRabot(rabotDictionary, rabot.Name, rabot);
                            }
                            if (File.Exists(newPath)) File.Delete(newPath);
                            if (module.RabotStrategy == R.Strategy.Not_Defined)
                            {
                                MessageInput mi = new MessageInput(MessageType.Choice, "Please choose type of Rabot offset displacement")
                                {
                                    ButtonTexts = new List<string>() { "Expected Offset", "Relative Displacement" }
                                };
                                if (mi.Show().TextOfButtonClicked == "Expected Offset")
                                    module.RabotStrategy = R.Strategy.Expected_Offset;
                                else
                                    module.RabotStrategy = R.Strategy.Relative_Displacement;
                                Console.WriteLine(module.RabotStrategy);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Invalid file type in the first row. Should be ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }

                return openedFile;
        }

        public static void AddOrUpdateRabot(Dictionary<string, R.Rabot> dictionary, string key, R.Rabot newRabot)
        {
            if (dictionary.ContainsKey(key))
            {
                if (dictionary[key].OffsetDate <= newRabot.OffsetDate)
                {
                    dictionary[key] = newRabot;
                }
            }
            else
            {
                dictionary.Add(key, newRabot);
            }
        }

    }

}

