﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Runtime.InteropServices;
using TSU.ENUM;
using T = TSU.Tools;
using TSU.Common.Compute.Transformation;
using I = TSU.Common.Instruments;
using TSU.Common.Instruments.Device;
using M = TSU.Common.Measures;
using E = TSU.Common.Elements;
using Z = TSU.Common.Zones;
using TSU.Common.Analysis;

namespace TSU.Common.Compute
{
    public static class Survey
    {
        private const double na = Preferences.Preferences.NotAvailableValueNa;
        // Computation of averages

        // the sigma become the standard error and we lost the initial sigma of the measure
        public static DoubleValue DoAverage(List<IDoubleValue> list)
        {
            // Preparation
            int n = list.Count;
            DoubleValue average = new DoubleValue();
            average.Value = 0;
            average.Sigma = 0;
            bool allValuesAlreadyHaveSigma = true;

            // Averaging values
            {
                double sum = 0;
                foreach (IDoubleValue item in list)
                {
                    sum += item.Value;
                    if (item.Sigma == na)
                        allValuesAlreadyHaveSigma = false;
                }
                average.Value = sum / n;
            }

            // Sigma estimation
            if (allValuesAlreadyHaveSigma) // AT40x measurement comes with sigma
            {
                // (square root of the sum of the sigma^2) / n
                double sumOfSquares = 0;
                foreach (IDoubleValue item in list)
                {
                    sumOfSquares += Math.Pow(item.Sigma, 2);
                }
                average.Sigma = Math.Sqrt(sumOfSquares) / n;
            }
            else // the other instruments measurement does not come with sigma
            {
                // square root of the sum of (the offset to the mean)^2 / n-1
                double sumOfSquares = 0;
                foreach (IDoubleValue item in list)
                {
                    sumOfSquares += Math.Pow(item.Value - average.Value, 2);
                }
                average.Sigma = Math.Sqrt(sumOfSquares) / (n - 1);
            }

            // Returning
            return average;
        }
        public static M.MeasureOfDistance DoAverage(List<M.IMeasureOfDistance> list) // do an average but keep the properties of the fisrt measure of the list
        {
            // Preparation
            M.MeasureOfDistance average = (M.MeasureOfDistance)list[0].Clone();

            List<IDoubleValue> raws = new List<IDoubleValue>();
            List<IDoubleValue> correcteds = new List<IDoubleValue>();

            int nombre = list.Count;

            // Averaging
            foreach (M.IMeasureOfDistance item in list)
            {
                raws.Add(item.Raw);
                correcteds.Add(item.Corrected);
            }

            // Returning
            average.Raw = DoAverage(raws);
            average.Corrected = DoAverage(correcteds);
            return average;
        }
        public static E.Angles DoAverage(List<E.IAngles> list)
        {
            E.Angles a = new E.Angles();

            List<IDoubleValue> horizontals;
            List<IDoubleValue> verticals;

            // Preparation
            horizontals = new List<IDoubleValue>();
            verticals = new List<IDoubleValue>();

            // for horizontal we have to check that we dont have stuff like  0.1 and 399.9 that will screw the average
            bool isAny399 = list.Any(x => x.Horizontal.Value > 399);
            bool isAny0 = list.Any(x => x.Horizontal.Value < 1);

            if (isAny399 && isAny0) // let traasnform the 399s into -0s
            {
                foreach (E.IAngles item in list)
                {
                    if (item.Horizontal.Value > 399)
                        horizontals.Add(new DoubleValue(-(400 - item.Horizontal.Value), item.Horizontal.Sigma));
                    else
                        horizontals.Add(item.Horizontal);
                }
            }
            else
            {
                foreach (E.IAngles item in list) horizontals.Add(item.Horizontal);
            }

            // For vertical 
            foreach (E.IAngles item in list) verticals.Add(item.Vertical);

            // Averaging
            a.Horizontal = DoAverage(horizontals);
            a.Horizontal.Value = Modulo400(a.Horizontal.Value); // beacuse average of 399s and 0s could be negative

            a.Vertical = DoAverage(verticals);
            return a;
        }

        // Return a value between 0 and 400
        public static void Modulo400(DoubleValue doubleValue)
        {
            doubleValue.Value = Modulo400(doubleValue.Value);
        }

        // Return a value between 0 and 400
        public static double Modulo400(double value)
        {
            double d = value;
            while (d >= 400) d -= 400;
            while (d < 0) d += 400;
            return d;
        }

        // Advanced.Theodolite
        public static Polar.Measure GetDifference(Polar.Measure m, Polar.Measure t, double H_Tol_CC, double V_Tol_CC, double D_Tol_mm, ref bool IsOverTolerance, out string message, ref bool IsSuperOverTolerance)
        {
            IsOverTolerance = false;
            Polar.Measure d = m.Clone() as Polar.Measure; // this sdoesnt CloneableList the Pointt!


            // numberdecimal for distances
            int nDD = Tsunami2.Preferences.Values.GuiPrefs == null ? 7 : Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
            // numberdecimal for angles
            int nDA = Tsunami2.Preferences.Values.GuiPrefs == null ? 5 : Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;

            d._Name = "Diff(" + m._Name + "-" + t._Name + ")";
            double ah = m.Angles.Corrected.Horizontal.Value - t.Angles.Corrected.Horizontal.Value;
            double av = m.Angles.Corrected.Vertical.Value - t.Angles.Corrected.Vertical.Value;
            double d3d = m.Distance.Corrected.Value - t.Distance.Corrected.Value;
            d.Angles.Corrected.Horizontal.Value = ah;
            d.Angles.Corrected.Vertical.Value = av;
            d.Distance.Corrected = new DoubleValue(d3d);

            // same reflector
            if (m.Distance.Reflector._Name == t.Distance.Reflector._Name)
                d.Distance.Reflector = new I.Reflector.Reflector() { _Name = "Same" };
            else
            {
                d.Distance.Reflector = new I.Reflector.Reflector() { _Name = "Not the Same" };
                IsOverTolerance = true;
            }

            // Same Extension
            if (m.Extension == t.Extension)
                d.Extension = new DoubleValue(m.Extension.Value - t.Extension.Value, 0);
            else
            {
                d.Extension = new DoubleValue(m.Extension.Value - t.Extension.Value, 0);
                IsOverTolerance = true;
            }

            // test tolerance
            double errAh = Math.Abs(m.Distance.Corrected.Value * Math.Sin(ah / 200 * Math.PI));
            double errAv = Math.Abs(m.Distance.Corrected.Value * Math.Sin(av / 200 * Math.PI));
            double errD = Math.Abs(d3d);
            double err3D = Math.Sqrt(Math.Pow(errAh, 2) + Math.Pow(errAv, 2) + Math.Pow(errD, 2));
            d.Offsets = new E.CoordinatesInAllSystems();
            E.Coordinates errorEcart = new E.Coordinates("Err", errAh, errAv, errD);
            d.Offsets.Local = errorEcart;

            message = "";
            if (Math.Abs(ah) > H_Tol_CC / 10000) IsOverTolerance = true;
            if (Math.Abs(ah) > 5 * H_Tol_CC / 10000) IsSuperOverTolerance = true;
            message += $"\t dHz= {new DoubleValue(ah, 0).ToString(nDA - 4, 10000)} CC";
            if (m.Distance.Corrected.Value != na)
                message += $" => {new DoubleValue(errAh, 0).ToString(nDD - 3, 1000)} mm";
            message += ",\r\n";

            if (Math.Abs(av) > H_Tol_CC / 10000) IsOverTolerance = true;
            if (Math.Abs(av) > 5 * H_Tol_CC / 10000) IsSuperOverTolerance = true;
            message += $"\t dV = {new DoubleValue(av, 0).ToString(nDA - 4, 10000)} CC";
            if (m.Distance.Corrected.Value != na)
                message += $" => {new DoubleValue(errAv, 0).ToString(nDD - 3, 1000)} mm,\r\n";
            message += "\r\n";

            if (Math.Abs(errD) > D_Tol_mm / 1000) IsOverTolerance = true;
            if (Math.Abs(errD) > 5 * D_Tol_mm / 1000) IsSuperOverTolerance = true;
            if (m.Distance.Corrected.Value != na)
            {
                message += $"\t dD = {new DoubleValue(d3d, 0).ToString(nDD - 3, 1000)} mm.\r\n\r\n";
                message += $"\t 3D distance error = {new DoubleValue(err3D, 0).ToString(nDD - 3, 1000)} mm";
            }
            errorEcart._Name = message;

            return d;
        }

        internal static DoubleValue ZtOneMeasure(E.Coordinates coordinatesStation, E.Coordinates theoreticalCoordinatesPoint, DoubleValue extension, DoubleValue verticalAngleRead, DoubleValue distance)
        {
            if (theoreticalCoordinatesPoint.Z == null)
                return new DoubleValue();
            double av = Modulo400(verticalAngleRead.Value);
            double dV = distance.Value * Math.Cos(av / 200 * Math.PI);
            return new DoubleValue(theoreticalCoordinatesPoint.Z.Value + extension.Value - dV, 0);
        }
        /// <summary>
        /// Calcule le Vzero à partir d'une mesure et de la station
        /// </summary>
        /// <param name="coordinatesStation"></param>
        /// <param name="theoreticalCoordinatesPoint"></param>
        /// <param name="horizontalAngleRead"></param>
        /// <returns></returns>
        internal static DoubleValue VZeroOneMeasure(E.Coordinates coordinatesStation, E.Coordinates theoreticalCoordinatesPoint, DoubleValue horizontalAngleRead)
        {
            if (!coordinatesStation.AreKnown) return null;
            if (!theoreticalCoordinatesPoint.AreKnown) return null;
            double bearing = GetBearing(
                coordinatesStation,
                theoreticalCoordinatesPoint);
            double reading = horizontalAngleRead.Value;
            return new DoubleValue(Modulo400(reading - bearing), 0);
        }
        /// <summary>
        /// Calcule le goto horizontal à faire pour une mesure en fonction du vzero
        /// </summary>
        /// <param name="coordinatesStation"></param>
        /// <param name="theoreticalCoordinatesPoint"></param>
        /// <param name="vzero"></param>
        /// <returns></returns>
        internal static DoubleValue GotoHorizOneMeasure(E.Coordinates coordinatesStation, E.Coordinates theoreticalCoordinatesPoint, DoubleValue vzero)
        {
            if (!coordinatesStation.AreKnown) return null;
            if (!theoreticalCoordinatesPoint.AreKnown) return null;
            if (vzero.Value == Preferences.Preferences.NotAvailableValueNa) return null;
            double bearing = GetBearing(
                coordinatesStation,
                theoreticalCoordinatesPoint);
            return new DoubleValue(Modulo400(bearing + vzero.Value), 0);

        }
        /// <summary>
        /// Calcule le goto vertical à faire pour une mesure à partir d'une station
        /// </summary>
        /// <param name="coordinatesStation"></param>
        /// <param name="theoreticalCoordinatesPoint"></param>
        /// <param name="extension"></param>
        /// <param name="verticalAngleRead"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        internal static DoubleValue GotoVerticOneMeasure(E.Coordinates coordinatesStation, E.Coordinates theoreticalCoordinatesPoint, double extension, double hInstrument)
        {
            if (!coordinatesStation.AreKnown) return null;
            if (!theoreticalCoordinatesPoint.AreKnown) return null;
            double na = Preferences.Preferences.NotAvailableValueNa;
            double distHoriz = GetHorizontalDistance(coordinatesStation, theoreticalCoordinatesPoint);
            if (distHoriz == 0) return null;
            double dV = na;
            ///Si pas de H, essaye de calculer avec les Z
            if (coordinatesStation.Z.Value != na && theoreticalCoordinatesPoint.Z.Value != na && dV == na)
            {
                dV = theoreticalCoordinatesPoint.Z.Value - coordinatesStation.Z.Value - hInstrument + extension;
            }
            if (dV == na) return null;
            double angledV = Math.Atan(dV / distHoriz) * 200 / Math.PI;
            double angleVertic = 100 - angledV;
            return new DoubleValue(angleVertic, 0);
        }
        /// <summary>
        /// get horizontal distance between 2 points
        /// </summary>
        /// <param name="stationPoint"></param>
        /// <param name="originalPoint"></param>
        /// <param name="cCS"></param>
        /// <returns></returns>
        internal static double GetHorizontalDistance(E.Point p1, E.Point p2, E.Coordinates.CoordinateSystemsTsunamiTypes cs = E.Coordinates.CoordinateSystemsTsunamiTypes.Unknown)
        {
            if (cs == E.Coordinates.CoordinateSystemsTsunamiTypes.Unknown)
            {
                if (p1._Coordinates.HasSu && p2._Coordinates.HasSu)
                    cs = E.Coordinates.CoordinateSystemsTsunamiTypes.SU;
                else if (p1._Coordinates.HasMla && p2._Coordinates.HasMla)
                    cs = E.Coordinates.CoordinateSystemsTsunamiTypes.MLA;
                else if (p1._Coordinates.HasStationCs && p2._Coordinates.HasStationCs)
                    cs = E.Coordinates.CoordinateSystemsTsunamiTypes.ToStation;
                else if (p1._Coordinates.HasCcs && p2._Coordinates.HasCcs)
                    cs = E.Coordinates.CoordinateSystemsTsunamiTypes.CCS;
            }
            switch (cs)
            {
                case E.Coordinates.CoordinateSystemsTsunamiTypes.CCS:
                    // this cannot be computed in CCS because horizontal doesnt exist except at P0 of PS complex
                    E.Point p1c = p1.Clone() as E.Point;
                    E.Point p2c = p2.Clone() as E.Point;
                    //Compute.Transformation.Systems.HtoZ(theoPoint, Coordinates.GetReferenceSurface(rF));
                    Systems.Ccs2Mla(
                        new List<E.Point>() { p1c, p2c },
                        E.Coordinates.ReferenceFrames.CernXYHg00Machine,
                        new Z.Zone()
                        {
                            Ccs2MlaInfo = new Z.Zone.Ccs2Mla()
                            {
                                Origin = p1._Coordinates.Ccs,
                                ReferenceSurface = E.Coordinates.ReferenceSurfaces.RS2K,
                                Bearing = new DoubleValue(0, 0),
                                Slope = new DoubleValue(0, 0)

                            }
                        });

                    return GetHorizontalDistance(p1c._Coordinates.Mla, p2c._Coordinates.Mla);
                case E.Coordinates.CoordinateSystemsTsunamiTypes.SU:
                    return GetHorizontalDistance(p1._Coordinates.Su, p2._Coordinates.Su);
                case E.Coordinates.CoordinateSystemsTsunamiTypes.ToStation:
                    return GetHorizontalDistance(p1._Coordinates.StationCs, p2._Coordinates.StationCs);
                case E.Coordinates.CoordinateSystemsTsunamiTypes.MLA:
                    return GetHorizontalDistance(p1._Coordinates.Mla, p2._Coordinates.Mla);
                case E.Coordinates.CoordinateSystemsTsunamiTypes.PHYS:
                case E.Coordinates.CoordinateSystemsTsunamiTypes.UserDefined:
                case E.Coordinates.CoordinateSystemsTsunamiTypes.Beam:
                case E.Coordinates.CoordinateSystemsTsunamiTypes.Displacement:
                case E.Coordinates.CoordinateSystemsTsunamiTypes.Unknown:
                default:

                    throw new Exception(R.T_NOT_YET_AVAILABLE);
            }
        }

        public static Polar.Measure DoFaceAverage(Polar.Measure mf1, Polar.Measure mf2, double H_Tol, double V_Tol, double D_Tol, out Polar.Measure diff, out bool outOfTolerance, out string message, ref bool completelyOut)
        {
            if (mf1.IsMeasured && mf2.IsMeasured &&
                mf1.Face == I.FaceType.Face1 &&
                mf2.Face == I.FaceType.Face2)
            {
                // Setting properties of the average by "object initializers"
                Polar.Measure dbl = mf1.Clone() as Polar.Measure;
                diff = mf1.Clone() as Polar.Measure;

                dbl._Status = mf1._Status;
                dbl._Date = mf1._Date > mf2._Date ? mf1._Date : mf2._Date;

                if (mf1.Extension.Value == mf2.Extension.Value)
                {
                    dbl.Extension = mf1.Extension;
                }
                else
                {
                    throw new NotImplementedException(R.T014);
                }

                Polar.Measure mf2inf1 = (Polar.Measure)mf2.Clone(); //clone of face2 to convert it in face1
                mf2inf1.Angles = TransformToOppositeFace(mf2inf1.Angles);
                dbl.Distance = DoAverage(new List<M.IMeasureOfDistance> { mf1.Distance, mf2.Distance });
                dbl.Angles = DoAverage(new List<M.IMeasureOfAngles> { mf1.Angles, mf2inf1.Angles });

                dbl.Face = I.FaceType.DoubleFace;
                outOfTolerance = false;
                diff = GetDifference(mf1, mf2inf1, H_Tol, V_Tol, D_Tol, ref outOfTolerance, out message, ref completelyOut);

                // we remove sigma because it should not contains the collimation error
                {
                    dbl.Distance.Corrected.Sigma = na;
                    dbl.Distance.Raw.Sigma = na;
                    dbl.Angles.Corrected.Horizontal.Sigma = na;
                    dbl.Angles.Corrected.Vertical.Sigma = na;
                    dbl.Angles.Raw.Horizontal.Sigma = na;
                    dbl.Angles.Raw.Vertical.Sigma = na;
                }
                // compute new sigma if (F1+F2)/2, s = sqrt( (d(F)/dF1)^2 * sF1^2 + (d(F)/dF2)^2 *sF2^2 )
                // ex: sF1 = 2cc, sF2 = 4cc s = sqrt( 1/4  *4 + 1/4 *16 ) = sqrt(5):
                {
                    dbl.Distance.Raw.Sigma = SigmaForAverageOf2UnlinkedMeasures(mf1.Distance.Raw.Sigma, mf2.Distance.Raw.Sigma);
                    dbl.Distance.Corrected.Sigma = dbl.Distance.Raw.Sigma;
                    dbl.Angles.Raw.Horizontal.Sigma = SigmaForAverageOf2UnlinkedMeasures(mf1.Angles.Raw.Horizontal.Sigma, mf2.Angles.Raw.Horizontal.Sigma);
                    dbl.Angles.Corrected.Horizontal.Sigma = dbl.Angles.Raw.Horizontal.Sigma;
                    dbl.Angles.Raw.Vertical.Sigma = SigmaForAverageOf2UnlinkedMeasures(mf1.Angles.Raw.Vertical.Sigma, mf2.Angles.Raw.Vertical.Sigma);
                    dbl.Angles.Corrected.Vertical.Sigma = dbl.Angles.Raw.Vertical.Sigma;
                }

                return dbl;
            }
            else
            {
                outOfTolerance = true;
                diff = null;
                //   message = "Face average failed";
                message = R.T_FACE_AVERAGE_FAILED;
                return null;
            }

        }

        public static double SigmaForAverageOf2UnlinkedMeasures(double sigma1, double sigma2)
        {
            return Math.Sqrt(0.25 * Math.Pow(sigma1, 2) + 0.25 * Math.Pow(sigma2, 2));
        }

        // do average of the obs and compute the « Standard error » ou Écart type des moyennes
        public static Polar.Measure DoAverage(List<Polar.Measure> list)
        {
            // Preparation
            Polar.Measure average = list[0].Clone() as Polar.Measure;

            List<M.IMeasureOfAngles> angles = new List<M.IMeasureOfAngles>();
            List<M.IMeasureOfDistance> distances = new List<M.IMeasureOfDistance>();

            foreach (Polar.Measure item in list)
            {
                angles.Add(item.Angles);
                distances.Add(item.Distance);
            }

            // Average
            average.Angles = DoAverage(angles);
            average.Distance = DoAverage(distances);

            //return
            return average;
        }

        // do average of the angle and compute the « Standard error » ou Écart type des moyennes
        public static M.MeasureOfAngles DoAverage(List<M.IMeasureOfAngles> list)
        {
            // Preparation
            M.MeasureOfAngles average = (M.MeasureOfAngles)list[0].Clone();

            List<E.IAngles> raws = new List<E.IAngles>();
            List<E.IAngles> correcteds = new List<E.IAngles>();

            int nombre = list.Count;

            // Averaging
            foreach (M.IMeasureOfAngles item in list)
            {
                raws.Add(item.Raw);
                correcteds.Add(item.Corrected);
            }

            // Returning
            //average._Comment = "Is an average of " + nombre + " Management.Measure of Angles";
            //average._Date = DateTime.Now;
            average.Raw = DoAverage(raws);
            average.Corrected = DoAverage(correcteds);


            return average;

        }



        public static M.MeasureOfAngles TransformToOppositeFace(M.MeasureOfAngles measureFace2)
        {
            measureFace2.corrected = TransformToOppositeFace(measureFace2.corrected);
            measureFace2.raw = TransformToOppositeFace(measureFace2.raw);
            return measureFace2;
        }
        public static E.Angles TransformToOppositeFace(E.IAngles angleFace2)
        {
            angleFace2.Horizontal.Value -= 200;
            if (angleFace2.Horizontal.Value < 0) { angleFace2.Horizontal.Value += 400; }

            angleFace2.Vertical.Value = 400 - angleFace2.Vertical.Value;
            if (angleFace2.Vertical.Value < 0) { angleFace2.Vertical.Value += 400; }

            return (E.Angles)angleFace2;
        }

        public static double GetBearing(E.Point p1, E.Point p2)
        {
            if (p1._Coordinates.HasCcs && p2._Coordinates.HasCcs)
                return GetBearing(
                    p1._Coordinates.Ccs.X.Value,
                    p2._Coordinates.Ccs.X.Value,
                    p1._Coordinates.Ccs.Y.Value,
                    p2._Coordinates.Ccs.Y.Value);
            else
                return GetBearing(
                    p1._Coordinates.Local.X.Value,
                    p2._Coordinates.Local.X.Value,
                    p1._Coordinates.Local.Y.Value,
                    p2._Coordinates.Local.Y.Value);
        }

        internal static Polar.Measure GetObservationInFace1Type(Polar.Measure m)
        {
            Polar.Measure mf1;
            if (m.Face == I.FaceType.Face2)
            {
                mf1 = m.GetNiceClone();
                mf1.Angles = TransformToOppositeFace(mf1.Angles);
            }
            else
            {
                mf1 = m;
            }
            return mf1;
        }



        /// <summary>
        ///  donne le gisement en GON en fonction de 2 coordonnées
        /// </summary>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <returns></returns>

        public static double GetBearing(E.Coordinates c1, E.Coordinates c2)
        {
            return GetBearing(
                c1.X.Value,
                c2.X.Value,
                c1.Y.Value,
                c2.Y.Value);
        }

        /// <summary>
        ///  donne la pente entre deux jeux de coordonnées, 
        ///  unit = 1 pour mm/m (mrad), 
        ///  unit = 2 pour grades,
        ///  unit = 0 for rad, 
        /// the function comes from topocalc, a flat surface is equal to a slop of 0mm/m but 100grade
        /// </summary>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <returns></returns>
        public static double GetSlope(E.Coordinates c1, E.Coordinates c2, int unit = 0)
        {
            double d1;
            double d2;
            double pente_en_rien;
            double pente_en_grad;

            d1 = GetHorizontalDistance(c1, c2);
            d2 = c2.Z.Value - c1.Z.Value;

            pente_en_rien = d2 / d1; // m/m

            switch (unit)
            {
                case 0:
                    return Math.Atan(pente_en_rien);
                case 1:
                    return pente_en_rien * 1000; // mm/m
                case 2:
                    pente_en_grad = Math.Atan(pente_en_rien) / Math.PI * 200;
                    pente_en_grad = 100 - pente_en_grad;
                    return pente_en_grad;
                default:
                    //    throw new Exception("Bad unit input for GetSlope function");
                    throw new Exception(R.T_BAD_UNIT_INPUT_FOR_GETSLOPE_FUNCTION);
            }
        }

        public static double GetBearing(double x1, double x2, double y1, double y2)
        {
            if (x1 == x2 && y1 == y2)
            {
                return 0;
            }
            else
            {
                double x2x1 = x2 - x1;
                double y2y1 = y2 - y1;

                double b = T.Conversions.Angles.Rad2Gon(Math.Atan2(x2x1, y2y1));
                //double sum2 = Math.Pow(x2x1, 2) + Math.Pow(y2y1, 2);
                //double sqrTSUm2 = Math.Sqrt(sum2);
                //double divisor = sqrTSUm2 + y2y1;
                //double b;
                //if (divisor==0)
                //    b = T.Conversions.Angles.Rad2Gon(Math.Atan(x2x1/ y2y1));
                //else
                //    b =  Conversions.Angles.Rad2Gon(2 * Math.Atan(x2x1/ divisor));
                if (b < 0) b += 400;
                if (b >= 400) b -= 400;
                return b;
            }
        }

        /// <summary>
        /// Return in m the horizontal distance from a Hz difference angle in gon at a given distance in m.
        /// </summary>
        /// <param name="dH"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public static double GetOppositeSide(double a, double d)
        {
            return Math.Sin(a / 200 * Math.PI) * d;
        }

        public static double GetAdjacentSide(double a, double d)
        {
            return Math.Cos(a / 200 * Math.PI) * d;
        }

        /// <summary>
        /// Will return the horizontal distance based on an 3d distance and a vertical angler
        /// </summary>
        /// <param name="verticalAngle"></param>
        /// <param name="distance3d"></param>
        /// <returns></returns>
        public static double GetHorizontalDistance(double verticalAngle, double distance3d)
        {
            double a = verticalAngle;
            double d = distance3d;

            if (a > 0 && a < 200)
            {
                a = Math.Abs(100 - a);
            }
            else
            {
                if (a > 200 && a < 400)
                    a = Math.Abs(300 - a);
            }
            return GetAdjacentSide(a, d);
        }

        /// <summary>
        /// Return in m the vertical distance from a V difference angle in gon and the vertical angle in gon at a given distance in m.
        /// </summary>
        /// <param name="dH"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public static double GetVerticalDistance(double dV, double V, double d)
        {
            V = Math.Abs(GetVerticalAngleFromTheHorizontal(V));

            double vDistFromV = Math.Sin(V / 200 * Math.PI) * d;
            double vDistFromVplusdV = Math.Sin((V + dV) / 200 * Math.PI) * d;
            return Math.Abs(vDistFromV - vDistFromVplusdV);
        }

        public static double GetVerticalDistance(double measuredVerticalAngle, double measured3dDistance, double hI, double ext)
        {
            double V = GetVerticalAngleFromTheHorizontal(measuredVerticalAngle);
            double d = measured3dDistance;

            double visee = Math.Sin(V / 200 * Math.PI) * d;

            return hI + visee - ext;
        }

        private static double GetVerticalAngleFromTheHorizontal(double measuredVerticalAngle)
        {
            double V = Modulo400(measuredVerticalAngle);

            //change face2 to face1
            if (V > 200)
                V = 400 - V;

            V = 100 - V;

            return V;
        }

        public static double GetHorizontalDistance(E.Coordinates p1, E.Coordinates p2)
        {
            return GetHorizontalDistance(
                p1.X.Value,
                p1.Y.Value,
                p2.X.Value,
                p2.Y.Value);
        }

        public static double GetHorizontalDistance(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
        }
        public static double GetVerticalAngle(E.Point p1, E.Point p2)
        {
            if (p1._Coordinates.HasCcs && p2._Coordinates.HasCcs)
                return GetVerticalAngle(p1._Coordinates.Ccs, p2._Coordinates.Ccs);
            else
                return GetVerticalAngle(p1._Coordinates.Local, p2._Coordinates.Local);
        }
        public static double GetVerticalAngle(E.Coordinates p1, E.Coordinates p2, double extension1 = 0, double extension2 = 0)
        {
            double dh = GetHorizontalDistance(p1, p2);
            return 100 + T.Conversions.Angles.Rad2Gon(Math.Atan((p1.Z.Value + extension1 - (p2.Z.Value + extension2)) / dh));
        }


        /// <summary>
        /// Compute the coordinated of the point lancé in the station CS
        /// </summary>
        /// <param name="m"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public static E.Coordinates GetPointLancé(double v0, double ht, E.Coordinates station, double h, double v, double d, double extension)
        {
            if (d == na || h == na || v == na || v0 == na) throw new Exception(R.T015);

            v0 = T.Conversions.Angles.Gon2Rad(v0);
            h = T.Conversions.Angles.Gon2Rad(h);
            v = T.Conversions.Angles.Gon2Rad(v);

            double Disthorizontale = Math.Abs(d * Math.Sin(v));
            double Gisementvisee = v0 + h;
            double X = station.X.Value + Disthorizontale * Math.Sin(Gisementvisee);
            double Y = station.Y.Value + Disthorizontale * Math.Cos(Gisementvisee);
            double Z = station.Z.Value + ht - extension + d * Math.Cos(v);
            return new E.Coordinates(R.String_Unknown, X, Y, Z);
        }

        /// <summary>
        /// Compute the coordinated of the point lancé in the station CS
        /// </summary>
        /// <param name="m"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public static E.Coordinates GetPointLancé(DoubleValue v0, DoubleValue ht, E.Coordinates station, Polar.Measure m)
        {
            double d = m.Distance.Corrected.Value;
            double h = m.Angles.Corrected.Horizontal.Value;
            double v = m.Angles.Corrected.Vertical.Value;

            return GetPointLancé(v0.Value, ht.Value, station, h, v, d, m.Extension.Value);
        }

        /// <summary>
        /// Compute the coordinated of the point lancé in the station CS
        /// </summary>
        /// <param name="m"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public static E.Coordinates GetPointLancé(Polar.Measure m, Polar.Station s)
        {
            Polar.Station.Parameters p = s.Parameters2 as Polar.Station.Parameters;
            E.Coordinates cs = p._StationPoint._Coordinates.Local;
            double h = m.Angles.Corrected.Horizontal.Value;
            double v = m.Angles.Corrected.Vertical.Value;
            double d = m.Distance.Corrected.Value;
            double e = m.Extension.Value;

            return GetPointLancé(
                p.vZero,
                p._InstrumentHeight,
                cs, m);
        }
        public static E.Coordinates GetPointLancé(Polar.Station s, double h, double v, double d, double extension)
        {
            Polar.Station.Parameters p = s.Parameters2 as Polar.Station.Parameters;
            if (p._StationPoint == null)
                throw new Exception(R.T016);
            else
                if (!p._StationPoint._Coordinates.HasAny)
                throw new Exception(R.T016);
            //if (!s.Parameters2._IsSetup) throw new Exception(R.T_STH_NoV0);
            E.Coordinates cs = p._StationPoint._Coordinates.Local;
            return GetPointLancé(
                p.vZero.Value,
                p._InstrumentHeight.Value,
                cs, h, v, d, extension);
        }

        /// <summary>
        /// calcule la distance 3D entre 2 points
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public static double GetSpatialDistance(E.Coordinates p1, E.Coordinates p2)
        {
            return Math.Sqrt(Math.Pow(p2.X.Value - p1.X.Value, 2) + Math.Pow(p2.Y.Value - p1.Y.Value, 2) + Math.Pow(p2.Z.Value - p1.Z.Value, 2));
        }




        // Reduction Tour d'Horizon
        public static List<Polar.Measure> DoReduction(List<Polar.Measure> l)
        {
            if (l.Count > 1)
            {
                List<Polar.Measure> reductedList = new List<Polar.Measure>();
                if (l[0]._Point._Name != l[l.Count - 1]._Point._Name) throw new Exception(R.T018);
                // Check is measure are Face1 double face or not
                switch (AnalyseTypeOfFace(l))
                {
                    case I.FaceType.Face1:
                        reductedList = DoReductionFace1(l);
                        break;
                    case I.FaceType.Face2:
                        break;
                    case I.FaceType.DoubleFace:
                        reductedList = DoReductionDoubleFaceByTime(l);
                        break;
                    case I.FaceType.Face1Reducted:
                        break;
                    case I.FaceType.DoubleFaceReducted:
                        break;
                    case I.FaceType.UnknownFace:
                        throw new Exception(R.T019);
                    default:
                        break;
                }
                return reductedList;
            }
            else
                throw new Exception(R.T020);
        }
        private static I.FaceType AnalyseTypeOfFace(List<Polar.Measure> l)
        {
            bool face1Missing = false;
            bool face2Missing = false;
            bool found;
            foreach (Polar.Measure item in l)
            {
                found = false;
                switch (item.Face)
                {
                    case I.FaceType.Face1:
                        foreach (Polar.Measure item2 in l)
                        {
                            if (item2._Point._Name == item._Point._Name)
                                if (item2.Face == I.FaceType.Face2)
                                    found = true;
                        }
                        if (!found) face2Missing = true;
                        break;
                    case I.FaceType.Face2:
                        foreach (Polar.Measure item2 in l)
                        {
                            if (item2._Point._Name == item._Point._Name)
                                if (item2.Face == I.FaceType.Face1)
                                    found = true;
                        }
                        if (!found) face1Missing = true;
                        break;
                    default:
                        break;
                }
            }
            if (!face1Missing && !face2Missing) return I.FaceType.DoubleFace;
            if (!face1Missing && face2Missing) return I.FaceType.Face1;
            return I.FaceType.UnknownFace;
        }


        private static List<Polar.Measure> ComputeReduction(List<Polar.Measure> l, ReductionType type, I.FaceType faceTypeToGiveTotheResult)
        {
            List<Polar.Measure> reductedList = new List<Polar.Measure>();
            if (l.Count > 0)
            {
                double ecartFermetureH = l[l.Count - 1].Angles.Corrected.Horizontal.Value - l[0].Angles.Corrected.Horizontal.Value;
                int numberOfMeasures = l.Count - 1;
                double correction;
                double lectureZero;
                int currentMeasure;
                switch (type)
                {
                    case ReductionType.ByAverage:
                        correction = ecartFermetureH / 2;
                        lectureZero = l[0].Angles.Corrected.Horizontal.Value;
                        currentMeasure = 0;
                        foreach (Polar.Measure item in l)
                        {
                            double original = item.Angles.Corrected.Horizontal.Value;
                            item.Angles.Corrected.Horizontal.Value = original - lectureZero + correction;
                            currentMeasure += 1;
                            item.Face = faceTypeToGiveTotheResult;
                            reductedList.Add(item);
                        }
                        break;
                    case ReductionType.ByNumberOfMeasure:
                        correction = ecartFermetureH / numberOfMeasures;
                        lectureZero = l[0].Angles.Corrected.Horizontal.Value;
                        currentMeasure = 0;
                        foreach (Polar.Measure item in l)
                        {
                            double original = item.Angles.Corrected.Horizontal.Value;
                            item.Angles.Corrected.Horizontal.Value = original - lectureZero + currentMeasure * correction;
                            currentMeasure += 1;
                            item.Face = faceTypeToGiveTotheResult;
                            reductedList.Add(item);
                        }
                        break;
                    case ReductionType.ByTime:
                        throw new Exception(R.T021);
                    default:
                        break;
                }
                return reductedList;
            }
            else
                throw new Exception(R.T022);
        }
        private static List<Polar.Measure> DoReductionFace1(List<Polar.Measure> l)
        {
            List<Polar.Measure> listToReduce = new List<Polar.Measure>();
            foreach (Polar.Measure item in l)
            {
                if (item.Face == I.FaceType.Face1) listToReduce.Add(item);
            }
            return ComputeReduction(listToReduce, ReductionType.ByAverage, I.FaceType.Face1Reducted);
        }

        // Will reduct each round of tdh and compute for each pont the standard error from the average
        internal static string CompareRoundsOfTdH(M.TheodoliteRounds rounds, M.TheodoliteRounds reductedRounds,
            List<Polar.Measure> average, out List<string> listOfPointNamesToPickFrom)
        {
            try
            {
                // create a list to pick from /!\ not cloned
                List<Polar.Measure> listToPickFrom = new List<Polar.Measure>();
                foreach (M.TheodoliteRound item in rounds.FindAll(x => x.State != M.States.Types.Bad))
                {
                    listToPickFrom.AddRange(item.Measures.FindAll(x => !(x._Status is M.States.Bad)));
                }

                // Find point names
                listOfPointNamesToPickFrom = GetPointList(listToPickFrom);

                // Reduct each round
                // if (reductedRounds.Count == 0) return "No reduction available yet";
                if (reductedRounds.Count == 0)
                    return R.T_NO_REDUCTION_AVAILABLE_YET;

                // Rounds numbers
                string origins = "";
                origins = "\r\nOrigins (Time of each round):\r\n";
                //for (int i = 0; i < reductedRounds.Count; i++)
                //    origins += string.Format("Round {0} from {1}\r\n", i + 1, reductedRounds[i].Name);
                origins += rounds.ToString();

                // compute spread to the mean for each point name
                string obs = "";
                string disp = "";
                foreach (var name in listOfPointNamesToPickFrom)
                {
                    int roundNumber = 1;
                    obs += string.Format("{0}: \r\n", name);
                    foreach (var round in reductedRounds) // all rounds to have the right round number...
                    {
                        if (round.State != M.States.Types.Bad)
                        {
                            Polar.Measure m = average.First(x => x._Point._Name == name);

                            foreach (var item in round.Measures.FindAll(x => x._Point._Name == name && !(x._Status is M.States.Bad)))
                            {
                                double spread = (item.Angles.Corrected.Horizontal.Value - m.Angles.Corrected.Horizontal.Value) * 10000;

                                // saving spread

                                // create string
                                obs += string.Format("{0,-25}({1,5}){2,15}{3,15}\r\n",
                                   roundNumber.ToString(),
                                   item.Face.ToString(),
                                   item.Angles.Corrected.Horizontal.ToString(4),
                                   spread.ToString("F1"));
                                disp += string.Format("{0,6}: {1,25} {2,10} {3,10} {4,10}\r\n",
                                   roundNumber, item._Point._Name,
                                   item.Angles.Corrected.Horizontal.Sigma == na ? "-" : (item.Angles.Corrected.Horizontal.Sigma * 10000).ToString("F1"),
                                   item.Angles.Corrected.Vertical.Sigma == na ? "-" : (item.Angles.Corrected.Vertical.Sigma * 10000).ToString("F1"),
                                   item.Distance.Corrected.Sigma == na ? "-" : (item.Distance.Corrected.Sigma * 10000).ToString("F1"));
                            }
                        }
                        roundNumber++;
                    }
                }

                // Create comment the string
                string s = "";
                s += origins + "\r\n" + " Rounds reduction (Opening set to 0 gon + Closure error spreaded on the number of measure) and spread (offset between each measures and the average of all round):" + "\r\n";

                s += string.Format("{0,-25}({1,5}){2,15}{3,15}\r\n\r\n",
                    "Round",
                    "Face#",
                    "AH[gon]",
                    "Spread[cc]");

                s += obs;
                s += $"\r\n{R.T_DISPERSION_IN_CASE_OF_MULTIPLE_POINTING_OR_SIGMA_IF_INSTRUMENT_IS_GIVING_ONE}\r\n";
                s += string.Format("{0,6}: {1,25} {2,10} {3,10} {4,10}\r\n",
                    "Round", "PointName", "Hz [cc]", "Vt [cc]", "D [mm]");

                s += disp;


                return s;
            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_IMPOSSIBLE_TO_REDUCE_AND_AVERAGE_THE_ROUNDS}\r\n{ex.Message}", ex);
            }
        }

        internal static List<string> GetPointList(List<Polar.Measure> allMeasure)
        {
            List<string> l = new List<string>();
            foreach (Polar.Measure item in allMeasure)
            {
                if (!l.Contains(item._Point._Name)) l.Add(item._Point._Name);
            }
            return l;
        }



        /// <summary>
        /// will return a list the face round reducted sepearetly one from the other
        /// </summary>
        internal static List<Polar.Measure> ReductFaceRounds(List<Polar.Measure> listToPickFrom, bool TransformFace2InFace1 = false)
        {
            List<Polar.Measure> listOfReductedMeasures = new List<Polar.Measure>();
            int round = 0;
            while (listToPickFrom.Count > 0)
            {
                round++;
                double opening;
                List<Polar.Measure> currentBlock;

                // seperate next round block
                {
                    string origin = listToPickFrom[0].Origin;
                    currentBlock = listToPickFrom.FindAll(x => x.Origin == origin);
                    listToPickFrom.RemoveAll(x => x.Origin == origin);
                }

                // compute closure
                double closure = currentBlock[0].Angles.Corrected.Horizontal.Value - currentBlock.Last().Angles.Corrected.Horizontal.Value;
                int numberOfmeasures = currentBlock.FindAll(x => x._Status is M.States.Good).Count; // nombre de mesure - la ref en double

                // Get opening hz
                {
                    if (currentBlock[0].Face == I.FaceType.Face2)
                        opening = currentBlock[0].Angles.Corrected.Horizontal.Value + 200;
                    else
                        opening = currentBlock[0].Angles.Corrected.Horizontal.Value;
                }

                // Reduct block
                {
                    for (int i = 0; i < currentBlock.Count; i++)
                    {
                        Polar.Measure clone = currentBlock[i].GetNiceClone();

                        // Change Comments
                        {
                            clone.CommentFromTsunami = $"{R.T_ROUND} {round.ToString()} ({clone.Face.ToString()})";
                        }

                        if (clone.Face == I.FaceType.Face2 && TransformFace2InFace1) clone.Angles = TransformToOppositeFace(clone.Angles);

                        if (i < currentBlock.Count - 1) // to avoid closure
                            clone.Angles.Corrected.Horizontal.Value += closure * i / numberOfmeasures;

                        clone.Angles.Corrected.Horizontal.Value -= opening;

                        // Modulo corrections
                        {
                            if (i == currentBlock.Count - 1) // special treat fot the closure
                            {
                                if (clone.Face == I.FaceType.Face1)
                                {
                                    if (clone.Angles.Corrected.Horizontal.Value > 399) clone.Angles.Corrected.Horizontal.Value -= 400; // to avoid 399 avec average at 200
                                }
                                else
                                    if (clone.Angles.Corrected.Horizontal.Value > 399) clone.Angles.Corrected.Horizontal.Value -= 400;
                            }
                            else
                            {
                                clone.Angles.Corrected.Horizontal.Value = Modulo400(clone.Angles.Corrected.Horizontal.Value);
                            }
                        }

                        listOfReductedMeasures.Add(clone);
                    }
                }
            }
            return listOfReductedMeasures;
        }



        internal static M.TheodoliteRounds ReductFaceRounds(M.TheodoliteRounds rounds, bool TransformFace2InFace1 = false)
        {
            try
            {
                M.TheodoliteRounds reductedRounds = new M.TheodoliteRounds();

                foreach (var round in rounds.GoodOnes)
                {
                    if (round.ControlMeasure != null)
                        reductedRounds.Add(new M.TheodoliteRound(round.ReductedMeasures));
                }

                return reductedRounds;
            }
            catch (Exception ex)
            {
                //  throw new Exception(string.Format("{0}\r\n{1}", "Getting reducted rounds failed:", ex.Message));
                throw new Exception($"{R.T_GETTING_REDUCTED_ROUNDS_FAILED}:\r\n{ex.Message}", ex);
            }

        }
        /// <summary>
        ///  list the face round reducted sepearetly one from the other and then average them
        /// </summary>
        internal static List<Polar.Measure> AverageFaceRounds(List<Polar.Measure> measures)
        {
            List<Polar.Measure> averagedFace1 = DoAverageByPointName(measures.FindAll(x => x.Face == I.FaceType.Face1 && (x._Status is M.States.Good || x._Status is M.States.Control)));
            if (averagedFace1[0].Angles.Corrected.Horizontal.Value > 399) averagedFace1[0].Angles.Corrected.Horizontal.Value -= 400;
            List<Polar.Measure> averagedFace2 = DoAverageByPointName(measures.FindAll(x => x.Face == I.FaceType.Face1Reducted && (x._Status is M.States.Good || x._Status is M.States.Control)));
            List<Polar.Measure> averaged = DoFacesAverageByPointName(averagedFace1.Concat(averagedFace2).ToList());
            return averaged;
        }

        /// <summary>
        /// return a list containing the Face averaging for each single point name (you should have only one F1 anf 1 F2 for each point
        /// </summary>
        /// <param name="measures"></param>
        /// <returns></returns>
        private static List<Polar.Measure> DoFacesAverageByPointName(List<Polar.Measure> measures)
        {
            List<Polar.Measure> averaged = new List<Polar.Measure>();

            List<string> listOfPointNamesToPickFrom = GetPointList(measures);
            while (listOfPointNamesToPickFrom.Count > 0)
            {
                List<Polar.Measure> currentBlock = measures.FindAll(x => x._Point._Name == listOfPointNamesToPickFrom[0]);
                listOfPointNamesToPickFrom.RemoveAll(x => x == listOfPointNamesToPickFrom[0]);

                Polar.Measure average = DoFaceAverage(currentBlock);
                average.Face = I.FaceType.Face1;
                averaged.Add(average);
            }

            return averaged;
        }

        /// <summary>
        /// return  the Face averaging of a single point name (you should have several point in input parameter list)
        /// </summary>
        /// <param name="MeasureMixed"></param>
        /// <returns></returns>
        private static Polar.Measure DoFaceAverage(List<Polar.Measure> UniquePointMeasureMixed)
        {
            // Use same add as the first measure
            Polar.Measure average = UniquePointMeasureMixed.Last().GetNiceClone();

            // Clone the list
            List<Polar.Measure> clones = new List<Polar.Measure>();
            foreach (var item in UniquePointMeasureMixed)
            {
                clones.Add(item.GetNiceClone());
            }


            List<M.IMeasureOfAngles> angles = new List<M.IMeasureOfAngles>();
            List<M.IMeasureOfDistance> dists = new List<M.IMeasureOfDistance>();
            foreach (var item in clones)
            {
                dists.Add(item.Distance);

                if (item.Face == I.FaceType.Face2)
                    angles.Add(TransformToOppositeFace(item.Angles));
                else
                    angles.Add(item.Angles);
            }

            average.Angles = DoAverage(angles);
            average.Distance = DoAverage(dists);
            return average;
        }

        /// <summary>
        /// return a list containing the average of all the measurement of each single point name (you should not mix Face1 and Face2 in the input parameter)
        /// </summary>
        /// <param name="measures"></param>
        /// <returns></returns>
        private static List<Polar.Measure> DoAverageByPointName(List<Polar.Measure> measures)
        {
            List<Polar.Measure> averaged = new List<Polar.Measure>();

            List<string> listOfPointNamesToPickFrom = GetPointList(measures);
            while (listOfPointNamesToPickFrom.Count > 0)
            {
                List<Polar.Measure> currentBlock = measures.FindAll(x => x._Point._Name == listOfPointNamesToPickFrom[0]);
                listOfPointNamesToPickFrom.RemoveAll(x => x == listOfPointNamesToPickFrom[0]);

                Polar.Measure average = DoAverage(currentBlock);
                averaged.Add(average);
            }

            return averaged;
        }

        private static DoubleValue DoHorizontalAngleAverage(List<Polar.Measure> currentBlock)
        {
            List<IDoubleValue> dvs = new List<IDoubleValue>();
            foreach (Polar.Measure item in currentBlock)
            {
                if (item.Angles.Corrected.Vertical.Value < 200)  // if(item.Face == FaceType.Face1) this will not work for face 2 reducted to face 1
                    dvs.Add(item.Angles.Corrected.Horizontal);
                else
                    dvs.Add(new DoubleValue(item.Angles.Corrected.Horizontal.Value - 400, item.Angles.Corrected.Horizontal.Sigma));
            }
            return DoAverage(dvs);
        }

        private static List<Polar.Measure> DoReductionDoubleFaceByTime(List<Polar.Measure> l)
        {
            bool inconsistanceFound = false;
            List<Polar.Measure> tempListToPickFrom = new List<Polar.Measure>();
            tempListToPickFrom.AddRange(l);
            List<Polar.Measure> mf1s = new List<Polar.Measure>();
            List<Polar.Measure> mf2s = new List<Polar.Measure>();

            // Trier et retirer les non-complete et transforme f2 en f1
            foreach (Polar.Measure item in l.FindAll(x => x.Face == I.FaceType.Face1 && x._Status is M.States.Good))
            {
                inconsistanceFound = false;
                Polar.Measure mf1 = tempListToPickFrom.First(x => x.Face == I.FaceType.Face1 && x._Point._Name == item._Point._Name);
                Polar.Measure mf2 = tempListToPickFrom.Last(x => x.Face == I.FaceType.Face2 && x._Point._Name == item._Point._Name);
                if (mf1 != null && mf2 != null)
                {
                    mf1s.Add(mf1);
                    Polar.Measure mf2d1 = mf2.GetNiceClone();
                    mf2d1.Angles = TransformToOppositeFace(mf2d1.Angles);
                    mf2s.Add(mf2d1);
                }
                else
                    inconsistanceFound = true;
            }
            List<Polar.Measure> listToReduce = new List<Polar.Measure>();
            listToReduce.AddRange(mf1s);
            mf2s.Reverse();
            listToReduce.AddRange(mf2s);

            double HE = mf2s.Last().Angles.Corrected.Horizontal.Value - mf1s[0].Angles.Corrected.Horizontal.Value; // horizontal ecart
            double VE = mf2s.Last().Angles.Corrected.Horizontal.Value - mf1s[0].Angles.Corrected.Horizontal.Value; // vertical ecart
            TimeSpan DE = mf2s.Last()._Date - mf1s[0]._Date; // time ecart

            if (inconsistanceFound)
                //   throw new Exception("Inconsistence found in the faces of the measure you want to reduct");
                throw new Exception(R.T_INCONSISTENCE_FOUND_IN_THE_FACES);
            else
                return ComputeReduction(listToReduce, ReductionType.ByAverage, I.FaceType.DoubleFaceReducted);
        }

        /// <summary>
        /// Distance Corrections
        /// </summary>
        /// <param name="rawMeasure"></param>
        /// <param name="tda"></param>
        /// <returns></returns>
        public static Result ApplyPPMWeatherCorrection(ref Polar.Measure rawMeasure, I.Device.TDA5005.Module tda) //Après vérification Pascal a déjà écrit cette correction.
        {
            Result result = new Result();
            try
            {
                double P = tda.WeatherConditionsStatic.pressure;
                double Ts = tda.WeatherConditionsStatic.dryTemperature;
                double H = tda.WeatherConditionsStatic.humidity;
                double distBrute = rawMeasure.Distance.Raw.Value;
                double PPM = 0;
                PPM = 281.8 - (0.29065 * P / (1 + Ts / 273.16) - 0.0004126 * H / (1 + Ts / 273.16) * Math.Pow(10, 7.5 * Ts / (237.3 + Ts) + 0.7857));
                rawMeasure.Distance.Raw.Value = rawMeasure.Distance.Raw.Value * (1 + 0.000001 * PPM);
                rawMeasure.Distance.IsCorrectedForWeatherConditions = true;
                result.Success = true;
                result.AccessToken = R.T023;
                return result;
            }
            catch (Exception e)
            {
                result.Success = false;
                result.ErrorMessage = e.Message;
                result.AccessToken = e.Source;
                return result;
            }
        }
        public static Result CorrectForWeatherConditions(M.MeasureOfDistance m, I.Instrument i)
        {
            Result result = new Result();
            result.Success = false;

            if (m.Reflector._InstrumentType == I.InstrumentTypes.CHAINE)
            {
                result.Success = true;
                result.AccessToken = "not corrected because type is 'Chain'";
                return result;
            }

            if (!m.IsCorrectedForWeatherConditions)
            {
                if (m.WeatherConditions != null)
                {
                    m.Corrected.Value = m.Raw.Value * (1 + 0.000001 * ComputePpm(i, m.WeatherConditions, m.MeasurementMode));
                    result.Success = true;
                }
                else
                {
                    result.ErrorMessage = R.T024;
                }
            }
            else
            {
                result.ErrorMessage = R.T025;
            }

            return result;
        }
        private static double ComputePpm(I.IInstrument i, M.WeatherConditions c, DistanceMeasurementMode mode)
        {
            double ppm = 0;
            double value1;
            double value2;

            double PorteuseInMicrometers = GetWaveLength(i, mode);

            if (PorteuseInMicrometers == 0.85)
            {
                //onde_porteuse = 0.85 micrometer TC1800, TC2002, DI2000, DI2002, TCA2003
                value1 = 281.8;
                value2 = 0.29065;
            }
            else if (PorteuseInMicrometers == 0.78)
            {
                //onde_porteuse = 0.78 micrometer TCRA1101 and TCRP1201 infrarouge
                value1 = 283.04;
                value2 = 0.29195;
            }
            else if (PorteuseInMicrometers == 0.67)
            {
                // onde_porteuse = 0.67 micrometer TCRA1101 and TCRP1201 laser 
                value1 = 285.92;
                value2 = 0.29492;
            }
            else if (PorteuseInMicrometers == 0.658)
            // onde_porteuse = 0.658 micrometer pour TS60
            // values  are found in 819179_Leica_MS60_TS60_UM_v2.0.0_en.pdf page 84 in \\cern.ch\dfs\Support\SurveyingEng\Computing\Software\test\Tsunami\Documentation\Technical\SDK TS60\Leica Captivate Documentation\Manuals\User Manual\MS60_TS60_User Manual

            {
                value1 = 286.338;
                value2 = 0.29535;
            }
            else
            {
                throw new Exception(R.T026);
            }

            // BAD because depending of culture, it will give , ou .
            //string s = PorteuseInMicrometers.ToString();
            //switch (s)//Leica formulas for reference values Tref = 12.0 degree, Pref = 1013.25 mbar and Humref = 60%
            //{
            //    case "0.85": //onde_porteuse = 0.85 micrometer TC1800, TC2002, DI2000, DI2002, TCA2003
            //        value1 = 281.8;
            //        value2 = 0.29065;
            //        break;
            //    case "0.78": //onde_porteuse = 0.78 micrometer TCRA1101 and TCRP1201 infrarouge
            //        value1 = 283.04;
            //        value2 = 0.29195;
            //        break;
            //    case "0.67": // onde_porteuse = 0.67 micrometer TCRA1101 and TCRP1201 laser 
            //        value1 = 285.92;
            //        value2 = 0.29492;
            //        break;
            //    default:
            //        throw new Exception(R.T026);
            //}
            double tdry = c.dryTemperature;
            ppm = value1 -
                (
                    value2 * c.pressure / (1 + tdry / 273.16) -

                        0.0004126 * c.humidity / (1 + tdry / 273.16)
                       * Math.Pow(10, 7.5 * tdry / (237.3 + tdry) + 0.7857)

                 );
            return ppm;
        }
        public static double GetWaveLength(I.IInstrument instrument, DistanceMeasurementMode mode)
        {
            //onde_porteuse in micrometer
            //0.85 for the TC1800, TC2002, Di2000, Di2002, TCA2003, TDA5005
            //0.78 for the TCRA mode infrared (IR)
            //0.67 for the TCRA mode laser (RL)
            //0.658 for the TS60 mode laser (RL) and IR

            switch (instrument._Model)
            {
                case "TC2002":
                    return 0.85;
                case "TCA2003":
                    return 0.85;
                case "TC1800":
                    return 0.85;
                case "TDA5005":
                    return 0.85;
                case "DI2000":
                    return 0.85;
                case "TCRA1101":
                    return mode == DistanceMeasurementMode.WithReflector ? 0.78 : 0.67;
                case "TCRP1201":
                    return mode == DistanceMeasurementMode.WithReflector ? 0.78 : 0.67;
                case "TS60":
                    return 0.658;
                default:
                    return 0.85;
                    throw new Exception(R.T027);
            }
        }
        public static Result CorrectForReflectorConstant(M.MeasureOfDistance m)
        {
            Result result = new Result();
            result.Success = false;

            if (!m.IsCorrectedForPrismConstanteValue)
            {
                //if (Reflector.Constante != 0)
                //{
                m.Corrected.Value = m.Corrected.Value + (double)m.Reflector.Constante;
                result.Success = true;
                //}
                //else result.ErrorMessage = "The prism constant is null";

            }
            else result.ErrorMessage = R.T028;

            return result;
        }
        public static void DoDistanceCorrections(M.MeasureOfDistance m, I.Instrument i)
        {
            m.Corrected = (DoubleValue)m.Raw.Clone();
            // Weather
            m.IsCorrectedForWeatherConditions = CorrectForWeatherConditions(m, i).Success;

            // Calibration
            Result r = CorrectForEtalonnage(m, i);
            m.IsCorrectedForEtalonnage = r.Success;


        }

        /// <summary>
        /// Corrige la lecture brute en fonction de l'étalonnage 
        /// </summary>
        public static void CorrectReadingForEtalonnage(M.MeasureOfOffset m, I.EtalonnageParameter paramEtalonnage, Line.Station.Parameters stParam)
        {
            //corrige la lecture brute en fonction de l'étalonnage
            CorrectForEtalonnage(m, paramEtalonnage, stParam);
        }


        /// <summary>
        ///  Calcule la lecture corrigée de la calibration pour un écartomètre
        /// </summary>
        public static Result CorrectForEtalonnage(M.MeasureOfOffset m, I.EtalonnageParameter param, Line.Station.Parameters stParam)
        {
            Result result = new Result();
            result.Success = false;
            double dist = Math.Abs(m._RawReading);
            double signe;
            if (m._RawReading >= 0)
            {
                signe = 1;
            }
            else
            {
                signe = -1;
            }
            //correction dilattion thermique de la mesure brute seulement pour les RS
            if (stParam != null)
            {
                if (stParam._Instrument != null)
                {
                    if (stParam._Instrument._Model.Contains("RS") || stParam._Instrument._Model.Contains("R_AUTO"))
                        dist = dist * (1 + Tsunami2.Preferences.Values.GuiPrefs.DilatationCoeffEcartoTemperature * (stParam._Temperature - Tsunami2.Preferences.Values.GuiPrefs.ReferenceEcartoTemperature));
                }
            }
            if (param != null && (dist >= param.fourrierMinimumDistance || dist <= param.fourrierMaximumDistance))
            {
                // Get the list of calibration for the actual instrument
                double waveLength = param.fourrierWaveLength;
                
                dist = dist * param.fourrierScale - param.fourrierA0Constant; //Regression linéeaire
                // Ne fait pas le calcul si longueur d'onde = 0 car division par 0 dans le dephasage
                if (waveLength != 0)
                {
                    double Dephasage = (dist - waveLength * Math.Truncate(dist / waveLength)) * 2 * Math.PI / waveLength;
                    for (int i = 1; i <= 49; i++)
                    {
                        double memberA = param.fourrierCoeff_A[i - 1] * Math.Cos(i * Dephasage);
                        double memberB = param.fourrierCoeff_B[i - 1] * Math.Sin(i * Dephasage);
                        dist = dist - memberA - memberB;
                    }
                }
                dist = signe * dist;
                m._CorrectedReading = dist;
                result.Success = true;
            }
            else
            {
                ///Dans le cas ou on n'applique que la dilation thermique
                m._CorrectedReading = signe * dist;
            }
            return result;
        }
        /// <summary>
        /// Fait des itérations pour calculer le average theoretical reading correspondant au corrected reading dans les mesures de fil
        /// </summary>
        /// <param name="m"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static Result CalculOffsetRawReading(M.MeasureOfOffset m, I.EtalonnageParameter param, Line.Station.Parameters stParam)
        {
            Result result = new Result();
            result.Success = false;
            M.MeasureOfOffset mCalc = m.DeepCopy();
            mCalc._RawReading = m._RawReading - m._AverageDeviation;
            for (int i = 0; i < 10; i++)
            {
                CorrectForEtalonnage(mCalc, param, stParam);
                mCalc._RawReading = mCalc._RawReading + (m._AverageTheoreticalReading - mCalc._CorrectedReading);
                if (Math.Abs(m._AverageTheoreticalReading - mCalc._CorrectedReading) < 0.000005)
                {
                    result.Success = true;
                    m._AverageTheoreticalReading = mCalc._RawReading;
                    return result;
                }
            }
            result.Success = false;
            m._AverageTheoreticalReading = mCalc._RawReading;
            return result;
        }
        /// <summary>
        /// Corrige la lecture brute en fonction de l'étalonnage, température et zero mire
        /// </summary>
        public static void CorrectLevelReadingForEverything(M.MeasureOfLevel m, I.EtalonnageParameter paramEtalonnage, Level.Station.Parameters stParam)
        {
            ///Corrige la lecture brute du zero mire
            CorrectForZeroStaffOffset(m);
            //corrige la lecture brute en fonction de l'étalonnage et de la température
            CorrectForEtalonnage(m, paramEtalonnage, stParam);
        }

        private static void CorrectForZeroStaffOffset(M.MeasureOfLevel m)
        {
            if (m._staff != null)
            {
                if (m._staff._zeroCorrection != na)
                {
                    m._CorrLevelReadingForZero = m._RawLevelReading + m._staff._zeroCorrection;
                }
                else
                {
                    m._CorrLevelReadingForZero = m._RawLevelReading;
                }
            }
            else
            {
                m._CorrLevelReadingForZero = m._RawLevelReading;
            }
        }

        public static Result CorrectForEtalonnage(M.MeasureOfLevel m, I.EtalonnageParameter param, Level.Station.Parameters stParam)
        {
            Result result = new Result();
            result.Success = false;
            double dist = Math.Abs(m._CorrLevelReadingForZero);
            double signe;
            if (m._CorrLevelReadingForZero >= 0)
            {
                signe = 1;
            }
            else
            {
                signe = -1;
            }
            //correction dilatation thermique de la mesure brute seulement pour les mires invar dans la liste de préférence
            if (m._staff != null)
            {
                if (Tsunami2.Preferences.Values.GuiPrefs.staffTypeForTemperatureCorrection.Contains(m._staff._StaffType))
                    dist = dist * (1 + Tsunami2.Preferences.Values.GuiPrefs.DilatationCoeffInvarStaffemperature * (stParam._Temperature - Tsunami2.Preferences.Values.GuiPrefs.ReferenceLevellingStaffTemperature));
            }
            if (param != null)
            {
                //if ((dist >= param.fourrierMinimumDistance) || (dist <= param.fourrierMaximumDistance))
                //{
                //    // Get the list of calibration for the actual instrument

                //    double Dephasage = (dist - param.fourrierWaveLength * Math.Truncate(dist / param.fourrierWaveLength)) * 2 * Math.PI / param.fourrierWaveLength;
                //    dist = dist * param.fourrierScale - param.fourrierA0Constant; //Regression linéeaire
                //                                                                  // Ne fait pas le calcul si longueur d'onde = 0 car division par 0 dans le dephasage
                //    if (param.fourrierWaveLength != 0)
                //    {
                //        for (int i = 1; i <= 49; i++)
                //        {
                //            double memberA = param.fourrierCoeff_A[i - 1] * Math.Cos(i * Dephasage);
                //            double memberB = param.fourrierCoeff_B[i - 1] * Math.Sin(i * Dephasage);
                //            dist = dist - memberA - memberB;
                //        }
                //    }
                //    dist = signe * dist;
                //    m._CorrLevelReadingForEtalonnage = dist;
                //    result.Success = true;
                //}
                //else
                //{
                //    ///Dans le cas ou on n'applique que la dilation thermique
                //    m._CorrLevelReadingForEtalonnage = signe * dist;
                //}
            }
            else
            {
                ///Dans le cas ou on n'applique que la dilation thermique
                m._CorrLevelReadingForEtalonnage = signe * dist;
            }
            return result;
        }
        //        Public Sub Corrige_OFF()
        //Dim Dephasage As Double
        //Dim i As Integer
        //Dim db As Double
        //Dim signe As Double

        //On Error Resume Next

        //If Not IsNull(Lect) Then
        //    If Lect >= 0 Then
        //        signe = 1
        //    Else
        //        signe = -1
        //    End If
        //    db = Abs(CDbl(Lect)) / 1000
        //    Dephasage = (db - Appa_off.long_onde * Int(db / Appa_off.long_onde)) * 2 * Pi / Appa_off.long_onde
        //        db = db * Appa_off.Echelle - Appa_off.Cste_A0  'regression linéaire
        //        If db >= Appa_off.minDist Or db <= Appa_off.maxDist Then
        //            For i = 1 To 50
        //                db = db - Appa_off.Coeff_A(i) * Cos(i * Dephasage) - Appa_off.Coeff_B(i) * Sin(i * Dephasage)
        //            Next i
        //        End If
        //        Lect_corr = signe * db * 1000
        //Else
        //    Lect_corr = Lect
        //End If
        //End Sub
        public static Result CorrectForEtalonnage(M.MeasureOfDistance m, I.Instrument i)
        {
            Result result = new Result();
            result.Success = false;

            // only if instrument is a TotalStation
            if (i is I.TotalStation)
            {
                // Get the list of calibration for the actual instrument
                List<I.EtalonnageParameter.Theodolite> parameterList = ((I.TotalStation)i).EtalonnageParameterList;
                // Check for the calibration of the current reflector
                I.EtalonnageParameter.Theodolite param = parameterList.Find(
                    x => x.PrismeRef._SerialNumber == m.Reflector._SerialNumber &&
                    x.PrismeRef._InstrumentType == m.Reflector._InstrumentType);
                // If the calibration exist for this reflector...
                if (param == null)
                {
                    if (CorrectForReflectorConstant(m).Success)
                    {
                        param = parameterList.Find(x => x.PrismeRef.Id == "PREF_1");
                        if (param == null)
                        { 
                            if (TSU.Debug.IsRunningInATest)
                            {
                                result.Success = true;
                                return result;
                            }
                            throw new Exception("No etalonnage parameter available for this kind of prisme");
                        }
                    }
                    else
                    {
                        result.ErrorMessage =
                        "Pas de Calibration spécifique \r\n" +
                        "Le fichier d'étalonnage ne contient pas de parametre pour le prisme choisi (" +
                        m.Reflector._InstrumentType.ToString() + " " + m.Reflector.Id + "). \r\n" +
                        "Les parametres du prisme de reference aurait du être appliqués ainsi d'une constante de prisme différencielle," +
                        " mais le Reflector est absent du fichier deConstante de prisme";
                        return result;
                    }
                }

                double dist = m.Corrected.Value;

                // 1st step: correction par regression linéaire
                double waveLength = param.fourrierWaveLength;
                
                dist = dist * param.fourrierScale - param.fourrierA0Constant;

                // 2eme step: Correction non linéaire si longue d'onde differente de zero et dans la zone de mesure
                bool inRange = dist > param.fourrierMinimumDistance && dist < param.fourrierMaximumDistance;
                if (waveLength != 0 && inRange)
                {
                    double Dephasage = (dist - waveLength * Math.Truncate(dist / waveLength)) * 2 * Math.PI / waveLength;
                    for (int j = 1; j <= 49; j++)
                    {
                        double memberA = param.fourrierCoeff_A[j - 1] * Math.Cos(j * Dephasage);
                        double memberB = param.fourrierCoeff_B[j - 1] * Math.Sin(j * Dephasage);
                        dist = dist - memberA - memberB;
                    }
                }
                else
                {
                    ///Dans le cas du TS60 sur CR et RL, des calibrations bidons on été insérées avec la longueur d'onde = 0
                    if (!inRange)
                    {
                        m.CommentFromTsunami = R.T_THE_DISTANCE_IS_NOT_IN_THE_RANGE_OF_THE_CALIBRATION_ONLY_A0_CSTE_AND_SCALE_FACTOR_HAVE_BEEN_USED;
                        m.IsDistanceOutOfRange = true;
                    }
                }

                // 3eme étape correction pour edm placé sur theodolite avec deplacement d'angle vertical entre mesure d'angle et de distance
                dist = Math.Sqrt(Math.Pow(dist, 2) + Math.Pow(param.Ecart_axe_edm, 2));

                m.Corrected.Value = dist;

                if (!inRange)
                {
                    m.CommentFromTsunami = R.T_DistanceOutOCalibratedrange;
                }
                result.Success = true;
            }
            else
                result.Success = true;

            return result;
        }
        internal static void DoCollimationCorrection(M.MeasureOfAngles m)
        {
            // apply collimation here
            m.Corrected = m.Raw.Clone() as E.Angles;
        }
        public static double Atan2(double n, double m)
        {
            //calcule l'arctangente de n/m en fonction du quadrant trigonométrique. Utilisé par la fonction HtoZ dans transformation
            if (m > 0)
            {
                return Math.Atan(n / m);
            }
            if (n > 0)
            {
                return Math.Atan(n / m) + Math.PI;
            }
            if (n < 0)
            {
                return Math.Atan(n / m) - Math.PI;
            }
            else
            {
                return Math.Atan(n / m);
            }
        }

        /// <summary>
        /// Translate les coordonnées du système MLA vers un système user avec une nouvelle origine
        /// </summary>
        public static void DoAxisTranslation(M.MeasureOfOffset m, M.MeasureOfOffset origin)
        {
            if (m._CorrectedReading != na)
            {
                if (m._Point._Coordinates.HasLocal && origin._Point._Coordinates.HasLocal)
                {
                    m._Point._Coordinates.TempCalculation = Do2DAxisTranslation(origin._Point._Coordinates.Local, m._Point._Coordinates.Local);
                }
                else
                {
                    m._Point._Coordinates.TempCalculation.X.Value = 0;
                    m._Point._Coordinates.TempCalculation.Y.Value = 0;
                    m._Point._Coordinates.TempCalculation.Z.Value = 0;
                }
            }

        }

        /// <summary>
        /// Translate un système de coordonnées en XY suivant une nouvelle origine.
        /// </summary>
        public static E.Coordinates Do2DAxisTranslation(E.Coordinates newOrigin, E.Coordinates oldCoord)

        {
            E.Coordinates newCoord = new E.Coordinates();
            newCoord.Set();
            newCoord.X.Value = oldCoord.X.Value - newOrigin.X.Value;
            newCoord.Y.Value = oldCoord.Y.Value - newOrigin.Y.Value;
            newCoord.Z.Value = oldCoord.Z.Value;
            newCoord.X.Sigma = oldCoord.X.Sigma;
            newCoord.Y.Sigma = oldCoord.Y.Sigma;
            newCoord.Z.Sigma = oldCoord.Z.Sigma;
            return newCoord;
        }

        /// <summary>
        /// Tourne le système d'axe d'un angle à partir du système user existant
        /// </summary>
        public static void DoAxisRotation(M.MeasureOfOffset m, double angle)
        {
            if (m._CorrectedReading != na)
            {
                m._Point._Coordinates.TempCalculation = Do2DAxisRotation(angle, m._Point._Coordinates.TempCalculation);
            }
        }

        /// <summary>
        /// Fait une rotation en plan du système d'axe suivant un angle en radian
        /// </summary>
        public static E.Coordinates Do2DAxisRotation(double angle, E.Coordinates oldCoord)
        {
            E.Coordinates newCoord = new E.Coordinates();
            newCoord.Set();
            newCoord.X.Value = oldCoord.X.Value * Math.Cos(angle) + oldCoord.Y.Value * Math.Sin(angle);
            newCoord.Y.Value = -oldCoord.X.Value * Math.Sin(angle) + oldCoord.Y.Value * Math.Cos(angle);
            newCoord.Z.Value = oldCoord.Z.Value;
            newCoord.X.Sigma = oldCoord.X.Sigma;
            newCoord.Y.Sigma = oldCoord.Y.Sigma;
            newCoord.Z.Sigma = oldCoord.Z.Sigma;
            return newCoord;
        }

        /// <summary>
        /// reduit les mesures par rapport une mesure de référence
        /// </summary>
        public static void DoReduction(M.MeasureOfOffset m, M.MeasureOfOffset origin)
        {
            if (m._RawReading != na)
            {
                m._ReductedReading = m._CorrectedReading - origin._CorrectedReading;
            }
        }

        private static void FindApproximatesV0AndHt(Polar.Station st, List<Polar.Measure> l_m, ref bool verifiedApprochedV0AndHt)
        {
            double temp;

            // Calculate and comprae v0
            double v0 = 0;
            double currentV0;
            const double V0_TOLERANCE = 0.0050; // gon

            bool verifiedV0 = false;
            // Compare the last measure with all the other until you find a correspondance in the v0
            for (int i = l_m.Count - 1; i > -1; i--)
            {
                temp = GetBearing(st.Parameters2._StationPoint, l_m[i]._Point);
                currentV0 = temp - (l_m[i] as Polar.Measure).Angles.Corrected.Horizontal.Value;
                if (currentV0 > 400) v0 -= 400;
                if (currentV0 < 0) v0 += 400;

                if (v0 == 0)
                    v0 = currentV0;
                else
                    if (currentV0 - v0 < V0_TOLERANCE)
                {
                    verifiedV0 = true;
                    break;
                }
            }
            st.Parameters2.Setups.TemporaryValues.vZero = new DoubleValue(v0, na);

            // Calculate and comparee hT
            double ht = 0;
            double currentHt;
            const double HT_TOLERANCE = 0.0050; // m

            bool verifiedHt = false;
            // Compare the last measure with all the other until you find a correspondance in the Ht
            for (int i = l_m.Count - 1; i > -1; i--)
            {
                temp = GetBearing(st.Parameters2._StationPoint, l_m[i]._Point);
                currentHt = l_m[i]._Point._Coordinates.Local.Z.Value - (l_m[i] as Polar.Measure).Distance.Corrected.Value / Math.Sin((l_m[i] as Polar.Measure).Angles.Corrected.Vertical.Value / 200 * Math.PI);
                currentHt = currentHt - st.Parameters2._StationPoint._Coordinates.Local.Z.Value;

                if (ht == 0)
                    ht = currentHt;
                else
                    if (currentHt - ht < HT_TOLERANCE)
                {
                    verifiedHt = true;
                    break;
                }
            }
            st.Parameters2.Setups.TemporaryValues.InstrumentHeight = new DoubleValue(ht, na);

            // Report if matches have been found.
            verifiedApprochedV0AndHt = verifiedHt && verifiedV0;
        }

        public static double XLMod(double a, double b)
        {
            while (a < 0)
                a += b;

            while (a >= b)
                a -= b;

            // This replicates the Excel MOD function
            //return a - b * Math.Round(a / b,0);
            return a;
        }

        public static double ATAN_2(double X, double Y)
        {
            // 'from VV
            const double Pi = 3.14159265359;

            switch (Math.Sign(X))
            {
                case -1:
                    switch (Math.Sign(Y))
                    {
                        case -1:
                            return Math.Atan(Y / X) - Pi;
                        case 0:
                            return 0;
                        case 1:
                            return Pi + Math.Atan(Y / X);
                        default:
                            throw new Exception("Atan2 error");
                    }
                case 0:
                    switch (Math.Sign(Y))
                    {
                        case -1:
                            return -Pi / 2;
                        case 0:
                            throw new Exception("Atan2 error");
                        case 1:
                            return Pi / 2;
                        default:
                            throw new Exception("Atan2 error");
                    }
                case 1:
                    return Math.Atan(Y / X);
                default:
                    throw new Exception("Atan2 error");
            }
        }

        // this is a signature to create a variable containing a method that will give the right coordinates between CCS or MLA
        public delegate E.Coordinates GetTheRightCoordinates(E.Point point);
        public delegate void PutTheRightCoordinates(E.Coordinates coordinates, E.Point point);
        public static GetTheRightCoordinates getTRC;
        public static PutTheRightCoordinates putTRC;

        public static bool FindApproximatesCoordinates(Polar.Station st, List<Polar.Measure> mlist, ref bool verified, out string message)
        {
            try
            {
                if (!mlist[0]._OriginalPoint._Coordinates.HasAny)
                {
                    message = R.T_MEASUREMENT_OF_POINT_WITH_NO_COORDINATES;
                    return false;
                }


                if (mlist[0]._OriginalPoint._Coordinates.HasCcs)
                {
                    getTRC = new GetTheRightCoordinates(delegate (E.Point point)
                    {
                        E.Coordinates c = point._Coordinates.Ccs.Clone() as E.Coordinates;
                        return c;
                    });
                    putTRC = new PutTheRightCoordinates(delegate (E.Coordinates coordinates, E.Point point)
                    {
                        point._Coordinates.Ccs = coordinates;
                    });
                }
                else
                {
                    getTRC = new GetTheRightCoordinates(delegate (E.Point point)
                    {
                        return point._Coordinates.Local;
                    });
                    putTRC = new PutTheRightCoordinates(delegate (E.Coordinates coordinates, E.Point point)
                    {
                        point._Coordinates.Local = coordinates;
                    });
                }
                double Pi = 4 * Math.Atan(1);



                Polar.Measure m = new Polar.Measure(), m1 = new Polar.Measure(), m2 = new Polar.Measure();

                E.Point p = new E.Point(), p1 = new E.Point(), p2 = new E.Point();

                double theta, eta, phi, sin_phi;
                double cos_Eta;
                double BearP1_P2, BearP1_P, BearP2_P, HorDistP1_P2, HorDistP_P1, HorDistP_P2, DHeigthP_P1, DHeigthP_P2;

                double test;

                double v01, v02;
                double xp1, xp2, yp1, yp2, zp1, zp2, ztp1, ztp2;
                double smallestD3D = 1000;


                int cnt = 0;
                test = mlist.Count - 1;
                while (cnt != test)
                {

                    m1 = mlist[cnt] as Polar.Measure; // gointg througt all measures
                    m2 = mlist[mlist.Count - 1] as Polar.Measure; //last measure

                    E.Coordinates c1; c1 = getTRC(m1._OriginalPoint);
                    E.Coordinates c2; c2 = getTRC(m2._OriginalPoint);


                    //Calculates the observation angle
                    theta = XLMod((m2.Angles.Corrected.Horizontal.Value - m1.Angles.Corrected.Horizontal.Value) * Pi / 200, 2 * Pi);

                    //Checks and keeps p1 on left side
                    if (theta > Pi)
                    {
                        m = m1;
                        m1 = m2;
                        m2 = m;
                        c1 = getTRC(m1._OriginalPoint);
                        c2 = getTRC(m2._OriginalPoint);

                        theta = XLMod((m2.Angles.Corrected.Horizontal.Value - m1.Angles.Corrected.Horizontal.Value) * Pi / 200, 2 * Pi);
                    }


                    double distance1 = m1.Distance.Corrected.Value;
                    double distance2 = m2.Distance.Corrected.Value;
                    double vertical1 = m1.Angles.Corrected.Vertical.Value;
                    double vertical2 = m2.Angles.Corrected.Vertical.Value;
                    double horizontal1 = m1.Angles.Corrected.Horizontal.Value;
                    double horizontal2 = m2.Angles.Corrected.Horizontal.Value;

                    if (vertical1 > 200)
                    {
                        vertical1 = 400 - vertical1;
                        horizontal1 = Modulo400(horizontal1 + 200);

                    }
                    if (vertical2 > 200)
                    {
                        vertical2 = 400 - vertical2;
                        horizontal2 = Modulo400(horizontal2 + 200);
                    }

                    //Calculates the bearing [rad] and the distance [m] of the known points
                    BearP1_P2 = XLMod(ATAN_2(
                        c2.Y.Value - c1.Y.Value,
                        c2.X.Value - c1.X.Value), 2 * Pi);

                    HorDistP1_P2 = Math.Sqrt(Math.Pow(c2.X.Value - c1.X.Value, 2)
                        + Math.Pow(c2.Y.Value - c1.Y.Value, 2));

                    //Calculates the horizontal distances [m] from the observations
                    HorDistP_P1 = Math.Sin(vertical1 * Pi / 200) * distance1;
                    HorDistP_P2 = Math.Sin(vertical2 * Pi / 200) * distance2;

                    //Calculates the height differences [m] from the observations
                    DHeigthP_P1 = Math.Cos(vertical1 * Pi / 200) * distance1;
                    DHeigthP_P2 = Math.Cos(vertical2 * Pi / 200) * distance2;

                    //Calculates the rest two angles (eta from p1 and phi from p2)
                    cos_Eta = (Math.Pow(HorDistP_P2, 2) - (Math.Pow(HorDistP_P1, 2) + Math.Pow(HorDistP1_P2, 2))) / (-2 * HorDistP_P1 * HorDistP1_P2);

                    if (cos_Eta < -1 || cos_Eta > 1)
                        goto NextIteration;

                    eta = XLMod(Math.Acos(cos_Eta), 2 * Pi);

                    sin_phi = HorDistP_P1 * Math.Sin(theta) / HorDistP1_P2;
                    if (sin_phi < -1 || sin_phi > 1)
                        goto NextIteration;

                    phi = XLMod(Math.Asin(sin_phi), 2 * Pi);

                    //Check if it is a triangle
                    double anglesSum = theta + eta + phi;
                    double offsetTo200 = Math.Abs(Pi - anglesSum) / Pi * 200;

                    double toleranceGrad;
                    if (mlist.Count < 3)
                        toleranceGrad = 10;
                    else
                        toleranceGrad = 1;

                    if (offsetTo200 > toleranceGrad)
                    {
                        phi = Pi - phi;
                        anglesSum = theta + eta + phi;
                        offsetTo200 = Math.Abs(Pi - anglesSum) / Pi * 200;
                        if (offsetTo200 > 1)
                            goto NextIteration;
                    }

                    //Calculates the rest two bearings (from p1 and p2 to the station)
                    BearP1_P = XLMod(BearP1_P2 + eta, 2 * Pi);
                    BearP2_P = XLMod(BearP1_P2 + Pi - phi, 2 * Pi);

                    //Calculates V0 from measurement
                    v01 = XLMod(2 * Pi - (horizontal1 * Pi / 200 - (BearP1_P + Pi)), 2 * Pi);
                    v02 = XLMod(2 * Pi - (horizontal2 * Pi / 200 - (BearP2_P + Pi)), 2 * Pi);

                    //Calculates coordinates and Zt from p1 measurement
                    xp1 = c1.X.Value + HorDistP_P1 * Math.Sin(BearP1_P);
                    yp1 = c1.Y.Value + HorDistP_P1 * Math.Cos(BearP1_P);
                    ztp1 = c1.Z.Value + m1.Extension.Value - DHeigthP_P1;
                    zp1 = ztp1 - st.Parameters2._InstrumentHeight.Value;

                    //Calculates coordinates and Zt from p2 measurement
                    test = c1.Y.Value;
                    xp2 = c2.X.Value + HorDistP_P2 * Math.Sin(BearP2_P);
                    test = c2.Y.Value + HorDistP_P2 * Math.Cos(BearP2_P);
                    yp2 = c2.Y.Value + HorDistP_P2 * Math.Cos(BearP2_P);
                    ztp2 = c2.Z.Value + m2.Extension.Value - DHeigthP_P2;
                    zp2 = ztp2 - st.Parameters2._InstrumentHeight.Value;

                    //Compares the results and if less than 1 mm then returns the average
                    double dx = Math.Abs(xp1 - xp2);
                    double dy = Math.Abs(yp1 - yp2);
                    double dz = Math.Abs(zp1 - zp2);

                    double d3D = Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2) + Math.Pow(dz, 2));
                    smallestD3D = d3D < smallestD3D ? d3D : smallestD3D;

                    double toleranceM;
                    if (mlist.Count < 3)
                        toleranceM = 10;
                    else
                        toleranceM = 0.02;

                    bool xOk = dx < toleranceM; //m
                    bool yOk = dy < toleranceM; //m
                    bool zOk = dz < toleranceM; //m

                    if (xOk && yOk && zOk)
                    {
                        st.Parameters2.Setups.TemporaryValues = new Polar.Station.Parameters.Setup.Values(null) { InstrumentHeight = new DoubleValue(0, 0), StationPoint = st.Parameters2._StationPoint };
                        E.Point sp = st.Parameters2.Setups.TemporaryValues.StationPoint;
                        E.Coordinates c = new E.Coordinates(sp._Name, (xp1 + xp2) / 2, (yp1 + yp2) / 2, (zp1 + zp2) / 2);
                        putTRC(c, sp);
                        message = string.Format("dx = {0:F3}m, dY = {1:F3}m, dZ = {2:F3}m,", dx, dy, dz);
                        verified = true;
                        return true;
                    }

                NextIteration:
                    cnt += 1;
                }

                verified = false;
                string max = smallestD3D == 1000 ? "huge" : smallestD3D.ToString("F3") + "m";
                message = $"{R.T_ALL_COMBINATIONS_FAILED_THE_SMALLEST_3D_DISTANCE_FOR_ONE_COMBINATION_OF_WAS} {max}";

                // then use the baricentre od the measure points!
                {
                    st.Parameters2.Setups.TemporaryValues = new Polar.Station.Parameters.Setup.Values(null) { InstrumentHeight = new DoubleValue(0, 0), StationPoint = st.Parameters2._StationPoint };
                    E.Point sp = st.Parameters2.Setups.TemporaryValues.StationPoint;
                    E.Point barycentre = Element.GetBaricentre(st.MeasuresTaken, Tsunami2.Properties.Zone);
                    st.Parameters2.Setups.TemporaryValues.StationPoint._Coordinates = barycentre._Coordinates;
                }
                return false;
            }
            catch (Exception ex)
            {
                message = $"{R.T_COMPUTE_STOP_UNEXPECTABLY}\r\n{ex.Message}";
                verified = false;
                return false;
            }
        }

        // this compute the coordinates of the theoritical point in a system centered on the measured point and looking a the station
        public static void ComputeCoordinateInStationCSFromCCS(E.Point stationPoint, E.Point theoPoint, E.Point measuredPoint, E.Coordinates.ReferenceFrames referenceFrames)
        {
            try
            {
                // define a station zone
                Z.Zone z = new Z.Zone();
                z.Ccs2MlaInfo = new Z.Zone.Ccs2Mla();

                // admin
                z._Name = "StationCS";

                Systems.HtoZ(measuredPoint, E.Coordinates.GetReferenceSurface(referenceFrames));
                z.Ccs2MlaInfo.Origin = measuredPoint._Coordinates.GetCoordinatesInASystemByName("CCS-Z");
                z.Ccs2MlaInfo.ReferenceSurface = E.Coordinates.GetReferenceSurface(referenceFrames);
                if (z.Ccs2MlaInfo.ReferenceSurface == E.Coordinates.ReferenceSurfaces.Unknown) z.Ccs2MlaInfo.ReferenceSurface = E.Coordinates.ReferenceSurfaces.RS2K;
                // Bearing is the bearing of the 3d line in express in the CCS that you want your Yaxis to be projection of.
                if (!stationPoint._Coordinates.CoordinatesExistInAGivenSystem("CCS-Z"))
                    Systems.HtoZ(stationPoint, E.Coordinates.GetReferenceSurface(referenceFrames));
                z.Ccs2MlaInfo.Bearing = new DoubleValue(GetBearing(measuredPoint._Coordinates.GetOrCreateByName("CCS-Z"), stationPoint._Coordinates.GetOrCreateByName("CCS-Z")), 0);
                // Slope is the slop of the 3d line in express in the CCS that you want your Yaxis to be projection of.
                z.Ccs2MlaInfo.Slope = new DoubleValue(GetSlope(measuredPoint._Coordinates.GetOrCreateByName("CCS-Z"), stationPoint._Coordinates.GetOrCreateByName("CCS-Z"), 0), 0);

                // make a temp clone to later move the value stored in .MLA into .StationCS 
                E.Point tempPoint = theoPoint.Clone() as E.Point;
                List<E.Point> noMlaPoints = new List<E.Point>(); noMlaPoints.Add(tempPoint);

                Systems.Ccs2Mla(noMlaPoints, referenceFrames, z);
                measuredPoint._Coordinates.StationCs = tempPoint._Coordinates.Mla;
            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_FAILED_TO_COMPUTE_THE_COORDINATES_OF} '{measuredPoint._Name}' {R.T_IN_THE_STATION_CS}.\r\n\r\n{ex.Message}", ex);
            }
        }

        public static void ComputeCoordinateInStationCSFromSU(E.Point stationPoint, E.Point theoPoint, E.Point measuredPoint, E.Coordinates.ReferenceFrames referenceFrames)
        {
            try
            {
                // define a station zone
                Z.Zone z = new Z.Zone();

                // admin
                z._Name = "StationCS";

                // make a temp clone to later move
                E.Point tempPoint = theoPoint.Clone() as E.Point;

                // bring the measurepoint as origin of the system;
                tempPoint._Coordinates.Local = Systems.Translation3D(tempPoint._Coordinates.Local, measuredPoint._Coordinates.Local);

                // Bearing is the bearing of the the horizontql line betwen measure and station points 
                double bearing = GetBearing(measuredPoint._Coordinates.Local, stationPoint._Coordinates.Local);

                tempPoint._Coordinates.Local = Systems.Rotate3DAroundOrigin(tempPoint._Coordinates.Local, new Rotation3D(0, 0, bearing), AngularUnit.Gon);

                measuredPoint._Coordinates.StationCs = tempPoint._Coordinates.Local;
            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_FAILED_TO_COMPUTE_THE_COORDINATES_OF} '{measuredPoint._Name}' {R.T_IN_THE_STATION_CS}.\r\n\r\n{ex.Message}", ex);
            }
        }

        // this compute the coordinates of a ccs point in a cs based on a theorical point on the beam, .SU will store MLA on the point and.PHYS will store in cs orineted with the beam (slope included)
        public static void ComputeCoordinateInBeamCS(E.Point theoreticalPoint, E.Point measuredPoint, E.Coordinates.ReferenceFrames referenceFrames)
        {
            // lets copy the theo point not to modify it
            E.Point theoPoint = theoreticalPoint.Clone() as E.Point;
            double slopeToUse;

            if (theoPoint._Coordinates.HasCcs)
            {
                try
                {
                    // 1.02.00beta15, if no bearing or no slope, no compute
                    // 1.02.00beta16, we learn that pillar can have bearing but no slope...
                    if (theoPoint.IsPilier && !theoPoint._Parameters.hasSlope)
                        slopeToUse = 0;
                    else
                        slopeToUse = theoPoint._Parameters.Slope;

                    if (!theoPoint._Parameters.hasGisement) // || 1.02.00beta15 !theoPoint._Parameters.hasSlope)
                    {
                        measuredPoint._Coordinates.Physicist = new E.Coordinates();
                        return;
                    }


                    List<E.Point> noMlaPoints = new List<E.Point>();
                    if (referenceFrames == E.Coordinates.ReferenceFrames.Unknown) referenceFrames = E.Coordinates.ReferenceFrames.CernXYHg00Machine;
                    Systems.HtoZ(theoPoint, E.Coordinates.GetReferenceSurface(referenceFrames));
                    noMlaPoints.Add(theoPoint);

                    // make a temp clone to later move the value stored in .MLA into .PHYS 
                    E.Point tempPoint = measuredPoint.Clone() as E.Point;
                    noMlaPoints.Add(tempPoint);

                    // create a ghost point (with the theo bearing and slope) to be able to compute the bearing and slope in MLA system // point must have a Z
                    E.Point ghost = ComputeNewPoint(theoPoint, theoPoint._Parameters.GisementFaisceau, slopeToUse);
                    Systems.ZToH(ghost, E.Coordinates.GetReferenceSurface(referenceFrames));
                    noMlaPoints.Add(ghost);


                    //
                    // Define temporary zone
                    //
                    Z.Zone z = new Z.Zone();
                    z._Name = "BeamCS";
                    z.Ccs2MlaInfo = new Z.Zone.Ccs2Mla();
                    z.Ccs2MlaInfo.Origin = theoPoint._Coordinates.GetOrCreateByName("CCS-Z");
                    z.Ccs2MlaInfo.ReferenceSurface = E.Coordinates.GetReferenceSurface(referenceFrames);
                    if (z.Ccs2MlaInfo.ReferenceSurface == E.Coordinates.ReferenceSurfaces.Unknown) z.Ccs2MlaInfo.ReferenceSurface = E.Coordinates.ReferenceSurfaces.RS2K;
                    // Bearing is the bearing of the 3d line in express in the CCS that you want your Yaxis to be projection of.
                    z.Ccs2MlaInfo.Bearing = new DoubleValue(theoPoint._Parameters.GisementFaisceau, 0);
                    // Sloe is the slop of the 3d line in express in the CCS that you want your Yaxis to be projection of.
                    z.Ccs2MlaInfo.Slope = new DoubleValue(slopeToUse, 0);


                    //
                    // csgeo compute CCS to MLA on the theo point
                    //
                    Systems.Ccs2Mla(noMlaPoints, referenceFrames, z);

                    // get the info and put it in .SU
                    E.Coordinates temporarySuSaving = measuredPoint._Coordinates.Su;
                    measuredPoint._Coordinates.Su = tempPoint._Coordinates.Mla;

                    // coorsdintae in local orinetated with the beam
                    z.Su2PhysInGon = new Rotation3D(0, 0, 0);
                    measuredPoint._Coordinates.BeamV = Systems.Su2Phys(measuredPoint._Coordinates.Su, z);

                    bool oldWay = false;// was ok (roght ahanded byt ASG want the radial first, R, L, V
                    if (oldWay)
                    {
                        // one more rotation to have radial potive the the left of the beam because it is like that since 30 years...
                        measuredPoint._Coordinates.BeamV = Systems.Rotate3DAroundOrigin(measuredPoint._Coordinates.BeamV, 0, 0, -100, AngularUnit.Gon, invert: false);
                    }
                    else
                    {
                        // to have the order  X y Z = R L V
                        measuredPoint._Coordinates.BeamV.X.Value = -measuredPoint._Coordinates.BeamV.X.Value;
                    }

                    //
                    // Rotated to Fitting to theoPoint beam Slope
                    //

                    // compute beam slope in MLA, slope between theoPoint and GhostPoint
                    double slopeMLA = GetSlope(theoPoint._Coordinates.Mla, ghost._Coordinates.Mla);
                    // Rotate
                    z.Su2PhysInGon = new Rotation3D(-slopeMLA / Math.PI * 200, 0, 0);
                    // set phys/beam coord.
                    measuredPoint._Coordinates.Beam = Systems.Su2Phys(measuredPoint._Coordinates.Su, z);

                    if (oldWay)
                    {
                        // one more rotation to have radial potive the the left of the beam because it is like that since 30 years...
                        measuredPoint._Coordinates.Beam = Systems.Rotate3DAroundOrigin(measuredPoint._Coordinates.Beam, 0, 0, -100, AngularUnit.Gon, invert: false);
                    }
                    else
                    {
                        // to have the order  X y Z = R L V
                        measuredPoint._Coordinates.Beam.X.Value = -measuredPoint._Coordinates.Beam.X.Value;
                    }

                    // remove temp mla from SU
                    measuredPoint._Coordinates.Su = temporarySuSaving;
                }
                catch (Exception ex)
                {
                    throw new Exception($"{R.T_FAILED_TO_COMPUTE_THE_COORDINATES_OF} '{measuredPoint._Name}' {R.T_IN_THE_BEAM_CS}.\r\n\r\n{ex.Message}", ex);
                }
            }
            else // for local systems
            {
                E.Coordinates temporarySuSaving = measuredPoint._Coordinates.Su.Clone() as E.Coordinates;
                E.Point tempPoint = measuredPoint.Clone() as E.Point;

                //measured - theo
                tempPoint._Coordinates.Su.X.Value = tempPoint._Coordinates.Su.X.Value - theoPoint._Coordinates.Su.X.Value;
                tempPoint._Coordinates.Su.Y.Value = tempPoint._Coordinates.Su.Y.Value - theoPoint._Coordinates.Su.Y.Value;
                tempPoint._Coordinates.Su.Z.Value = tempPoint._Coordinates.Su.Z.Value - theoPoint._Coordinates.Su.Z.Value;

                // Rotate around Z for beam V
                tempPoint._Coordinates.Su = Systems.Rotate3DAroundOrigin(tempPoint._Coordinates.Su, 0, 0, -(100 - theoPoint._Parameters.GisementFaisceau), AngularUnit.Gon);

                measuredPoint._Coordinates.BeamV = tempPoint._Coordinates.Su.Clone() as E.Coordinates;

                // swith axis to have R, L, V order
                DoubleValue oldX = measuredPoint._Coordinates.BeamV.X.Clone() as DoubleValue;
                measuredPoint._Coordinates.BeamV.X = measuredPoint._Coordinates.BeamV.Y.Clone() as DoubleValue;
                measuredPoint._Coordinates.BeamV.X.Value = -measuredPoint._Coordinates.BeamV.X.Value;
                measuredPoint._Coordinates.BeamV.Y = oldX;


                // beam vertical angle
                tempPoint._Coordinates.Su = Systems.Rotate3DAroundOrigin(tempPoint._Coordinates.Su, 0, theoPoint._Parameters.Slope, 0, AngularUnit.Rad);

                measuredPoint._Coordinates.Beam = tempPoint._Coordinates.Su.Clone() as E.Coordinates;

                // swith axis to have R, L, V order
                oldX = measuredPoint._Coordinates.Beam.X.Clone() as DoubleValue;
                measuredPoint._Coordinates.Beam.X = measuredPoint._Coordinates.Beam.Y.Clone() as DoubleValue;
                measuredPoint._Coordinates.Beam.X.Value = -measuredPoint._Coordinates.Beam.X.Value;
                measuredPoint._Coordinates.Beam.Y = oldX;


                // restore to su
                tempPoint._Coordinates.Su = temporarySuSaving;
            }
        }

        // return a new point with ccs coordinate of the given point with  (bearing (gon)+ slope(rad))* dH) 
        public static E.Point ComputeNewPoint(E.Point theoPoint, double bearing, double slope, double distanceHorizontale = 10)
        {
            try
            {
                E.Coordinates theoCCS = theoPoint._Coordinates.GetOrCreateByName("CCS-Z");
                //theoCCS.Set(1, 2,3);
                E.Coordinates ghostCCS = new E.Coordinates();
                ghostCCS.Set();
                ghostCCS.X = new DoubleValue(theoCCS.X.Value + Math.Sin(bearing / 200 * Math.PI) * distanceHorizontale, 0);
                ghostCCS.Y = new DoubleValue(theoCCS.Y.Value + Math.Cos(bearing / 200 * Math.PI) * distanceHorizontale, 0);
                ghostCCS.Z = new DoubleValue(theoCCS.Z.Value + Math.Tan(slope) * distanceHorizontale, 0);

                E.Point newPoint = new E.Point(theoPoint._Name + "+x" + distanceHorizontale.ToString() + "m");
                newPoint._Coordinates.AddOrReplaceCoordinatesInOneSystem("CCS-Z", ghostCCS);
                return newPoint;
            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_FAILED_TO_COMPUTE_THE_TROWN_POINT_FROM_THE_STATION_IN_CERN_CS}\r\n\r\n{ex.Message}", ex);
            }
        }

        internal static void TransformValueToBeBetween200AndMinus200(DoubleValue dv)
        {
            double v = dv.Value;
            if (v > 200)
                v = -(400 - v);
            if (v < -200)
                v = (400 + v);
            dv.Value = v;
        }
    }

}
