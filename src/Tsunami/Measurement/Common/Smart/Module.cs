﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using TSU.Common;

namespace TSU.Common.Smart
{
    [Serializable]
    [XmlType("Smart.Module")]
    public class Module : FinalModule
    {
        public Module()
            : base()
        {
        }

        public Module(IModule parentModule, string nameAndDescription)
            : base(parentModule, nameAndDescription)
        {
            if (!(Tsunami2.View._Strategy is TsunamiView.Strategy.Tab))
                Tsunami2.View.ChangeStrategy(TsunamiView.TsunamiViewsStrategies.Tab);
        }

        internal override void ReCreateWhatIsNotSerialized(bool saveSomeMemory = false)
        {
            base.ReCreateWhatIsNotSerialized(saveSomeMemory);
            if (!(Tsunami2.View._Strategy is TsunamiView.Strategy.Tab))
                Tsunami2.View.ChangeStrategy(TsunamiView.TsunamiViewsStrategies.Tab);
        }

        internal override IEnumerable<Station> GetStations()
        {
            foreach (Station.Module item in StationModules)
                yield return item._Station;
        }
    }
}
