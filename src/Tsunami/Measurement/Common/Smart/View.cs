﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSU.Views;

namespace TSU.Common.Smart
{
    public partial class View : TSU.Views.ModuleView
    {
        public TSU.Module ParentModule;

        public View()
        {
            InitializeComponent();
        }

        public View(TSU.Module parentModule):
            base(parentModule)
        {
            InitializeComponent();
        }

        public override void AddView(TsuView view)
        {           
        }
    }
}
