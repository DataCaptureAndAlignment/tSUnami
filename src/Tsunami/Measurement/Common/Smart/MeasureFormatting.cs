﻿using System;
using System.Linq;
using System.Text;

namespace TSU.Common.Smart
{
    internal static class MeasureFormatting
    {
        internal static bool CheckToleranceForAngle(double value)
        {
            return Math.Abs(value) < TSU.Tsunami2.Preferences.Values.Tolerances.Alignment_Roll_mrad;
        }

        internal static bool CheckToleranceForAngle(double? nullable)
        {
            return nullable.HasValue
                && CheckToleranceForAngle(nullable.Value);
        }

        internal static bool CheckToleranceForAngle(DoubleValue doubleValue)
        {
            return IsValid(doubleValue)
                && CheckToleranceForAngle(doubleValue.Value * 1000);
        }

        internal static bool CheckToleranceForDistance(double value)
        {
            return Math.Abs(value) < TSU.Tsunami2.Preferences.Values.Tolerances.Alignment_Radial_mm;
        }

        internal static bool CheckToleranceForDistance(double? nullable)
        {
            return nullable.HasValue
                && CheckToleranceForDistance(nullable.Value);
        }

        internal static bool CheckToleranceForDistance(DoubleValue doubleValue)
        {
            return IsValid(doubleValue)
                && CheckToleranceForDistance(doubleValue.Value * 1000);
        }

        internal static string FormatAngle(double? nullable)
        {
            if (nullable == null)
                return string.Empty;
            else
                return FormatAngle(nullable.Value);
        }

        internal static string FormatAngle(DoubleValue doubleValue)
        {
            if (!IsValid(doubleValue))
                return "";

            return FormatAngle(doubleValue.Value * 1000);
        }

        internal static string FormatAngle(double val)
        {
            string stringVal = val.ToString("0.000");

            AddSign(val, ref stringVal);

            return stringVal;
        }

        internal enum DistanceUnit
        {
            m, mm, centmm
        }

        internal static string FormatDistance(double? nullable, bool withPlusSign = true)
        {
            if (nullable == null)
                return string.Empty;
            else
                return FormatDistance(nullable.Value, withPlusSign);
        }


        internal static string FormatDistance(DoubleValue doubleValue, bool withPlusSign = true)
        {
            if (!IsValid(doubleValue))
                return "";

            return FormatDistance(doubleValue.Value, withPlusSign);
        }

        internal static string FormatDistance(double val, string format, bool withPlusSign = true)
        {
            string stringVal = val.ToString(format);

            if (withPlusSign)
                AddSign(val, ref stringVal);

            return stringVal;
        }

        internal static string FormatDistance(double val, DistanceUnit du = DistanceUnit.mm, bool withPlusSign = true)
        {
            string format;
            switch (du)
            {
                case DistanceUnit.m:
                    format = "0.00000";
                    break;
                case DistanceUnit.centmm:
                    format = "0";
                    break;
                default:
                    format = "0.00";
                    break;
            }

            string stringVal = val.ToString(format);

            if (withPlusSign)
                AddSign(val, ref stringVal);

            return stringVal;
        }

        private static void AddSign(double val, ref string stringVal)
        {
            if (stringVal[0] != '-')
            {
                // positive value
                if (val >= 0d)
                {
                    // Add the + only if required
                    stringVal = "+" + stringVal;
                }
                // negative value rounded down to zero
                else
                {
                    // Add - 
                    stringVal = "-" + stringVal;
                }
            }
        }

        /// <summary>
        /// Some Tilt DoubleValues have a 9999 default instaead of NA
        /// </summary>
        /// <param name="doubleValue"></param>
        /// <returns></returns>
        internal static bool IsValid(DoubleValue doubleValue)
        {
            return !doubleValue.IsNa && doubleValue.Value != 9999;
        }

        internal static double? SigmaOrNull(DoubleValue doubleValue)
        {
            if (doubleValue.Sigma.IsNa())
                return null;
            else
                return doubleValue.Sigma;
        }

        private static readonly char[] acceptedCharsForPositiveInteger = "0123456789".ToCharArray();

        public static string FilterInputPositiveInteger(string value)
        {
            StringBuilder filteredText = new StringBuilder(value.Length);
            foreach (char c in value)
                if (acceptedCharsForPositiveInteger.Contains(c))
                    filteredText.Append(c);
            return filteredText.ToString();
        }

        private static readonly char[] acceptedCharsForRelativeDouble = "-0123456789.".ToCharArray();

        public static string FilterInputRelativeDouble(string value)
        {
            StringBuilder filteredText = new StringBuilder(value.Length);
            foreach (char c in value)
                if (acceptedCharsForRelativeDouble.Contains(c))
                    filteredText.Append(c);
            return filteredText.ToString();
        }

    }
}