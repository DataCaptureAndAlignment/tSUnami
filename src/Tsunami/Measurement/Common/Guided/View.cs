﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Windows.Forms;
using System.Drawing;
using System.Threading.Tasks;
using TSU.Views;
using M = TSU;
using TSU.Common.Guided.Steps;

namespace TSU.Common.Guided
{
    public class View : ModuleView, IDisposable
    {
        #region fields

        private new Module Module
        {
            get
            {
                return _Module as Module;
            }
        }
        BigButton previousButton;
        BigButton nextButton;
        public bool previousButtonClicked = false;
        public bool nextButtonsClicked = false;
        public Bitmap buttonImage;
        #endregion

        #region Construct

        public View(IModule module) : base(module) // Taking form already in controllerModuleView
        {

        }

        protected override void Dispose(bool disposing)
        {
            if (previousButton != null) previousButton.Dispose();
            previousButton = null;
            if (nextButton != null) nextButton.Dispose();
            nextButton = null;
            if (buttonImage != null) buttonImage.Dispose();
            buttonImage = null;

            foreach (TsuView view in childViews)
            {
                view.Dispose();

            }
            base.Dispose(disposing);
        }



        private void InitializeComponent()
        {
            SuspendLayout();
            // 
            // _PanelBottom
            // 
            _PanelBottom.Size = new Size(508, 401);
            // 
            // _PanelTop
            // 
            _PanelTop.Size = new Size(508, 75);
            // 
            // View
            // 
            AutoScaleDimensions = new SizeF(6F, 13F);
            ClientSize = new Size(518, 486);
            Name = "View";
            Controls.SetChildIndex(_PanelTop, 0);
            Controls.SetChildIndex(_PanelBottom, 0);
            ResumeLayout(false);
            PerformLayout();

        }

        private void On_GuidedModule_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.N:
                    MoveToNextStep();
                    break;
                case Keys.P:
                    MoveToPreviousStep();
                    break;
                default:
                    break;
            }
        }

        class Buttons
        {
            public Buttons(View view)
            {
            }
        }

        #endregion

        #region Update

        public DataGridView grid;
        public TreeGrid treeGrid;



        TableLayoutPanel TLP4PnN;
        TableLayoutPanel TLP4C;
        SplitContainer splitStep;

        public void AddShortcutTextInButton()
        {
            try
            {
                string n = " (press 'n')";
                nextButton.AddCommentToTitle(n);

                string p = " (press 'p')";
                previousButton.AddCommentToTitle(p);

                TSU.Debug.WriteInConsole("AddshortcutToButton");
            }
            catch (Exception)
            {
            }

        }

        public void RemoveShortcutTextInButton()
        {
            try
            {
                nextButton.AddCommentToTitle("");

                previousButton.AddCommentToTitle("");
                TSU.Debug.WriteInConsole("RemoveshortcutToButton");
            }
            catch (Exception)
            {
            }

        }

        public override void UpdateView()
        {

            if (Module.Steps == null) return;

            CreateLayout();

            CreatePreviousButton();
            CreateNextButton(GetColorFromStepState());

            CreateStepListView();
            AddStepViews();

            base.UpdateView();
            splitStep.Panel2.Controls.Add(TLP4PnN); TLP4PnN.SendToBack();
            splitStep.Panel2.Controls.Add(TLP4C);
        }

        public void LaunchStepUpdateEvent()
        {
            if (Module.CurrentStep != null) Module.CurrentStep.Update();
        }

        private void AddStepViews()
        {
            if (Module.CurrentStep != null)
            {
                if (Module.CurrentStep._TsuView == null) Module.CurrentStep.UseForFirstTime();
                Module.CurrentStep._TsuView.TopLevel = false;
                Module.CurrentStep._TsuView.ShowDockedFill();
                Controls.Clear();
                splitStep.Panel1.SuspendLayout();
                splitStep.Panel1.Controls.Add(Module.CurrentStep._TsuView);
                Controls.Add(splitStep);

                splitStep.Panel1.ResumeLayout();
                splitStep.ResumeLayout();
            }
        }

        private void On_Step_Double(object sender, EventArgs e)
        {
            // get step number from name
            Label l = sender as Label;
            Steps.Step s = l.Tag as Steps.Step;
            Module.MoveToStep(s.Id, runIntermediateSteps: false);
        }

        private void CreateStepListView()
        {
            if (Module.CurrentStep != null)
            {
                // le nombre d'element = nombre a montrer + actuel + previous + next
                int numberToShow = ComputeNumberOfElementWantedInTheList() + 3;

                List<Steps.Step> stepsToShow = GetStepsToShow(numberToShow);
                numberToShow = stepsToShow.Count;

                // Create layout of 1 row and number of colonne = step to wanted
                TLP4C = new TableLayoutPanel()
                {
                    RowCount = 1,
                    ColumnCount = numberToShow,
                    Dock = DockStyle.Bottom,
                    Height = Tsunami2.Preferences.Theme.ButtonHeight / 2,
                    Padding = new Padding(0),
                    Name = "TLP4C"
                };


                double percentage = 100.0 / numberToShow;

                bool passedCurrent = false;

                foreach (Steps.Step item in stepsToShow)
                {
                    Color fc = Tsunami2.Preferences.Theme.Colors.Object;
                    Color bc = Tsunami2.Preferences.Theme.Colors.Background;
                    int stepNumber = Module.Steps.FindIndex(x => x == item);
                    TLP4C.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, (float)percentage));

                    if (item == Module.CurrentStep)
                    {
                        bc = GetColorFromStepState(item, redoTest: false);
                        passedCurrent = true;
                    }
                    else
                    {
                        if (!passedCurrent)
                            fc = GetColorFromStepState(item);
                    }

                    // Create the label
                    Label label = new Label()
                    {
                        Text = stepNumber + ": " + item._Name,
                        TextAlign = ContentAlignment.MiddleCenter,
                        Dock = DockStyle.Fill,
                        ForeColor = fc,
                        BackColor = bc,
                        Tag = item

                    };
                    label.DoubleClick += On_Step_Double;
                    TLP4C.Controls.Add(label);
                }
            }
        }


        private List<Steps.Step> GetStepsToShow(int numberToShow)
        {
            try
            {
                List<Steps.Step> l = Module.Steps.FindAll(x => x.Visible == true);

                if (l.Count < numberToShow) numberToShow = l.Count;
                int halfNumberToShow = numberToShow / 2;

                int indexOfCurrentStep = l.FindIndex(x => x == Module.CurrentStep);
                int availableInFront = indexOfCurrentStep;
                int availableAfter = l.Count - indexOfCurrentStep - 1;

                if (availableInFront > halfNumberToShow) availableInFront = halfNumberToShow;
                if (availableAfter > halfNumberToShow) availableAfter = halfNumberToShow;

                //int offsetCorrection=0;
                while (availableInFront + 1 + availableAfter != numberToShow)
                {
                    if (availableInFront < halfNumberToShow)
                    {
                        availableAfter += 1;
                    }
                    else
                        availableInFront += 1;
                }

                l = l.GetRange(indexOfCurrentStep - availableInFront, numberToShow);

                return l;
            }
            catch (Exception ex)
            {
                throw new Exception(R.GM_PM_ComputeList, ex);
            }
        }

        private int ComputeNumberOfElementWantedInTheList()
        {
            return 4;
        }

        private Color GetColorFromStepState(Steps.Step s = null, bool redoTest = false)
        {
            if (s == null) s = Module.CurrentStep;

            if (s == null)
                return Tsunami2.Preferences.Theme.Colors.Highlight;
            else
            {
                if (redoTest) s.Test();
                switch (s.State)
                {
                    case StepState.Valid: return Tsunami2.Preferences.Theme.Colors.Good;
                    case StepState.ReadyToSkip: return Tsunami2.Preferences.Theme.Colors.Attention;
                    case StepState.NotReady: return Tsunami2.Preferences.Theme.Colors.Bad;
                    default: return Tsunami2.Preferences.Theme.Colors.Highlight;
                }
            }

        }

        private void CreateNextButton(Color c)
        {
            // look for next visible step
            int index = Module.currentIndex;

            bool find = false;
            bool wasLastVisibleStep = false;

            do
            {
                index++;

                if (index >= Module.Steps.Count)
                {
                    find = true;
                    wasLastVisibleStep = true;
                }
                else
                {
                    if (Module.Steps[index].Visible) find = true;
                }

            } while (!find);

            // Fill the button description, shoudl show the next step details if the curretn step is valid, if not should show the error message of the step
            if (!wasLastVisibleStep)
            {
                string text;
                string title = R.T_NEXTSTEP;

                if (Module.CurrentStep.State == Steps.StepState.NotReady)
                {
                    string errorFromTest = Module.CurrentStep.MessageFromTest;
                    string errorMessage = errorFromTest == null ? R.T_SOMETHING_IS_MISSING_IN_THE_ACTUAL_STEP : errorFromTest;
                    text = $"{R.T_NEXTSTEP} {R.T_BLOCKED};{errorMessage}";
                }
                else
                {
                    text = $"{R.T_NEXTSTEP};{R.T_NEXTSTEPDETAILS}: {Module.Steps[index]._Name}. {Module.Steps[index].Utility}";
                }

                nextButton = new BigButton(
                            text,
                           R.Next,
                            MoveToNextStep,
                            c);
                nextButton.BigButtonSpecialClicked += SkipStep;

                if (TLP4PnN != null)
                {
                    if (TLP4PnN.Controls.Count == 2)
                        TLP4PnN.Controls.RemoveAt(1);
                    TLP4PnN.Controls.Add(nextButton, 1, 0);
                }
            }
        }

        // this is used with a special click on the button (right down when left is already down) to speed debug process
        private void SkipStep(object source, TsuObjectEventArgs args)
        {
            // temporary put the step to 'skipable' to able to move to thenext step
            int i = Module.currentIndex;
            bool b = Module.Steps[i].skipable;
            Module.Steps[i].skipable = true;
            MoveToNextStep();
            Module.Steps[i].skipable = b;
        }

        internal void UpdateNextButton()
        {
            Debug.WriteInConsole("update button color");
            CreateNextButton(GetColorFromStepState(null, false));

            CreateStepListView();
            if (splitStep != null)
            {
                Control[] ctrl = splitStep.Panel2.Controls.Find("TLP4C", false);
                if (ctrl.Length == 1) splitStep.Panel2.Controls.Remove(ctrl[0]);
                splitStep.Panel2.Controls.Add(TLP4C);
            }
            //if (this.Controls.Count >2) this.Controls.RemoveAt(2);
            //this.Controls.Add(TLP4C);
            Debug.WriteInConsole("update button colordone");
        }

        private void CreatePreviousButton()
        {

            // look for previous visible step
            int index = Module.currentIndex;

            bool find = false;
            bool wasTheFirstVisibleStep = false;

            do
            {
                index--;

                if (index == -1)
                {
                    find = true;
                    wasTheFirstVisibleStep = true;
                }
                else
                {
                    if (index > Module.Steps.Count - 1) index = Module.Steps.Count - 1;
                    if (Module.Steps[index].Visible) find = true;
                }

            } while (!find);

            // Fill the button description
            if (!wasTheFirstVisibleStep)
            {
                string text = R.T_PREVIOUSSTEP + ";" + R.T_PREVIOUSSTEPDETAILS + ": " + Module.Steps[index]._Name + ". " + Module.Steps[index].Utility;
                //string text = this.GuidedModule.Steps[index]._Name + ";" + this.GuidedModule.Steps[index].utility;
                previousButton = new BigButton(
                            text,
                           R.Previous,
                            MoveToPreviousStep,
                           Tsunami2.Preferences.Theme.Colors.Highlight);
                previousButton.BigButtonSpecialClicked += SkipStep;
            }
            else
            {
                previousButton = new BigButton("", null) { Visible = false };
            }

            TLP4PnN.Controls.Clear();
            TLP4PnN.Controls.Add(previousButton, 0, 0);
        }

        /// <summary>
        /// Create a loyout to put the step view and the two buttons (previous and next)
        /// </summary>
        private void CreateLayout()
        {
            TLP4PnN = new TableLayoutPanel()
            {
                RowCount = 1,
                ColumnCount = 2,
                Dock = DockStyle.Bottom,
                Height = Tsunami2.Preferences.Theme.ButtonHeight + 5,
                Name = "TLP4PnN"
            };

            TLP4PnN.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0F));
            TLP4PnN.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0F));
            splitStep = new SplitContainer
            {
                Dock = DockStyle.Fill,
                Orientation = Orientation.Horizontal,
                Panel2MinSize = (int)(1.55 * Tsunami2.Preferences.Theme.ButtonHeight + 5),
                FixedPanel = FixedPanel.Panel2,
                IsSplitterFixed = true,
                Name = "splitStep",
                SplitterWidth = 1,
                Padding = new Padding(0)
            };
        }


        #endregion

        #region Moves

        private void MoveToPreviousStep()
        {
            nextButtonsClicked = false;
            previousButtonClicked = true;
            TryAction(Module.MoveToPreviousStep);
        }

        private void MoveToNextStep()
        {
            nextButtonsClicked = true;
            previousButtonClicked = false;
            TryAction(() => Module.MoveToNextStep());
        }

        #endregion

        #region Actions

        internal void Open()
        {
            throw new NotImplementedException();
        }

        internal void Edit()
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
