﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using TSU.Common.Compute.Rabot;
using TSU.Common.Guided.Alignment;
using TSU.Common.Guided.Steps;
using TSU.Length.GuidedModules;
using TSU.Level.GuidedModules;
using TSU.Level.GuidedModules.Steps;
using TSU.Line.GuidedModules;
using TSU.Polar.GuidedModules;
using TSU.Tilt.GuidedModules;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using R = TSU.Properties.Resources;

namespace TSU.Common.Guided
{
    public interface IGuidedModule : IFinalModule
    {
        Guided.View View { get; }
        Group.Module GroupModule { get; }
    }


    [XmlType("GuidedModule")]
    [XmlInclude(typeof(TdH))]
    [Serializable]
    public class Module : FinalModule, IGuidedModule
    {
        #region Field and Properties


        bool cancelAllStepUpdate;
        public bool quickMeasureAllowed = false;

        public virtual List<Polar.Measure> CustomMeasureToBeDone { get; set; }

        // this is reset to false after 3 sec.
        public bool CancelAllStepUpdate
        {
            get
            {
                return cancelAllStepUpdate;
            }
            set
            {
                cancelAllStepUpdate = value;
                System.Timers.Timer t = new System.Timers.Timer() { Interval = 3000, Enabled = true };
                t.Elapsed += delegate { cancelAllStepUpdate = false; t.Stop(); t.Dispose(); };
                t.Start();
            }
        }

        // was here for alignment module but DEP made it a groupeModule
        // public List<Guided.Module> SubGuidedModules;

        [XmlIgnore]
        public new Guided.View View
        {
            get
            {
                return _TsuView as Guided.View;
            }
        }
        public Group.Module GroupModule
        {
            get
            {
                if (ParentModule is Group.Module) return ParentModule as Group.Module;
                return null;
            }
        }


        #region Steps
        [XmlIgnore]
        public Step FirstStep
        {
            get
            {
                if (Steps.Count > 0)
                    return Steps[0];
                else
                    return null;
            }
        }
        [XmlIgnore]
        public Step LastStep
        {
            get
            {
                if (Steps.Count > 0)
                    return Steps[Steps.Count - 1];
                else
                    return null;
            }
        }
        [XmlIgnore]
        public Step LastAddedStep;

        [XmlIgnore]
        public Step CurrentStep
        {
            get
            {
                if (Steps != null)
                {
                    if (Steps.Count > 0)
                    {
                        if (currentIndex > Steps.Count - 1) currentIndex = Steps.Count - 1;
                        return Steps[currentIndex];
                    }
                    else
                        return null;
                }
                else
                {
                    return null;
                }
            }
        }
        [XmlIgnore]
        public Step PreviousStep
        {
            get
            {
                if (currentIndex < 1) return null;
                return Steps[currentIndex - 1];
            }
        }
        [XmlIgnore]
        public Step NextStep
        {
            get
            {
                if (currentIndex + 1 >= Steps.Count) return null;
                return Steps[currentIndex + 1];
            }
        }
        [XmlIgnore]
        public Step NextVisibleStep
        {
            get
            {
                int index = currentIndex;
                while (true)
                {
                    index += 1;
                    if (index >= Steps.Count) return null;
                    if (Steps[index].Visible) return Steps[index];
                }

            }
        }
        [XmlIgnore]
        private List<Step> steps;
        [XmlIgnore]
        public List<Step> Steps
        {
            get
            {
                return steps;
            }

            set
            {
                steps = value;
            }
        }

        internal int GetStepIndexBasedOnName(string name)
        {
            int stepIndex = 0;
            foreach (var item in Steps)
            {
                if (item.Id == name)
                {
                    return stepIndex;
                }
                stepIndex++;
            }
            return 0;
        }

        public string currentStepName;

        public int currentIndex { get; set; }

        public Polar.GuidedModules.Steps.Measurement.MeasureTypeOptions MeasureTypeOption { get; set; }

        public Polar.GuidedModules.Steps.Measurement.MeasureGotoOptions MeasureGotoOption { get; set; }


        /// <summary>
        /// Pour distinguer type de module guidé après ouverture sauvegarde
        /// </summary>
        public ENUM.GuidedModuleType guideModuleType = ENUM.GuidedModuleType.NoType;
        internal int PreviousScrollPosition = -1;

        // used when moving several steps to avoid un needed update of the step
        public int targetStepIndex = -1;
        public bool isTargetStep
        {
            get
            {
                if (targetStepIndex == -1) return true;
                return currentIndex == targetStepIndex;
            }
        }

        #endregion

        #endregion

        #region Construction

        public override List<DsaFlag> DsaFlags
        {
            get => Tsunami2.Properties.DsaFlagForGuidedModule;
        }

        public Module()
        {
        }

        public Module(TSU.Module parentModule) : base(parentModule)
        {
        }

        public Module(TSU.Module parentModule, string name)
            : base(parentModule, name)
        {

        }

        //public override object Clone()
        //{
        //    M.Module m = this.MemberwiseClone() as M.Module  ; // make a clone of the object, work sif there is no other tye than primitive tyes.
        //    m.

        //    return m;
        //}

        public override void Initialize()
        {
            steps = new List<Step>();
            currentIndex = 0;
            base.Initialize();
        }
        //Lors ouverture sauvegarde, recree ce qui n'a pas été sérialisé
        internal override void ReCreateWhatIsNotSerialized(bool saveSomeMemory = false)
        {
            try
            {

                _ElementManager = new E.Manager.Module(this);
                _ElementManager.ReCreateWhatIsNotSerialized(saveSomeMemory);

                base.ReCreateWhatIsNotSerialized(saveSomeMemory);
                switch (guideModuleType)
                {
                    case ENUM.GuidedModuleType.NoType:
                        break;
                    case ENUM.GuidedModuleType.Alignment:
                        break;
                    case ENUM.GuidedModuleType.AlignmentEcartometry:
                        Line.GuidedModules.Steps.Alignment.ReCreateWhatIsNotSerialized(this);
                        break;
                    case ENUM.GuidedModuleType.AlignmentImplantation:
                        Polar.GuidedModules.Steps.Alignment.ReCreateWhatIsNotSerialized(this);
                        break;
                    case ENUM.GuidedModuleType.AlignmentLevelling:
                        Level.GuidedModules.Steps.Alignment.ReCreateWhatIsNotSerialized(this);
                        break;
                    case ENUM.GuidedModuleType.AlignmentLevellingCheck:
                        AlignmentCheck.ReCreateWhatIsNotSerialized(this);
                        break;
                    case ENUM.GuidedModuleType.AlignmentLengthSetting:
                        Length.GuidedModules.Steps.Alignment.ReCreateWhatIsNotSerialized(this);
                        break;
                    case ENUM.GuidedModuleType.AlignmentManager:
                        Admin.ReCreateWhatIsNotSerialized(this);
                        break;
                    case ENUM.GuidedModuleType.AlignmentTilt:
                        Tilt.GuidedModules.Steps.Alignment.ReCreateWhatIsNotSerialized(this);
                        break;
                    case ENUM.GuidedModuleType.Cheminement:
                        Cheminement.ReCreateWhatIsNotSerialized(this);
                        ///Cache le step select sequence niv si pas de file sequence niv
                        if (_ElementManager.GetAllSequenceNiv().Count == 0)
                        {
                            Steps[2].Visible = false;
                        }
                        else
                        {
                            Steps[2].Visible = true;
                        }
                        break;
                    case ENUM.GuidedModuleType.Ecartometry:
                        GuidedEcartometry.ReCreateWhatIsNotSerialized(this);
                        ///Cache le step select sequence niv si pas de file sequence niv
                        if (_ElementManager.GetAllSequenceFil().Count == 0)
                        {
                            Steps[2].Visible = false;
                        }
                        else
                        {
                            Steps[2].Visible = true;
                        }
                        break;
                    case ENUM.GuidedModuleType.Example:
                        break;
                    case ENUM.GuidedModuleType.Implantation:
                        Implantation.ReCreateWhatIsNotSerialized(this);
                        break;
                    case ENUM.GuidedModuleType.PointLance:
                        PointLancé.ReCreateWhatIsNotSerialized(this);
                        break;
                    case ENUM.GuidedModuleType.PreAlignment:
                        PreAlignment.ReCreateWhatIsNotSerialized(this);
                        break;
                    case ENUM.GuidedModuleType.Survey1Face:
                        Survey1Face.ReCreateWhatIsNotSerialized(this);
                        break;
                    case ENUM.GuidedModuleType.EquiDistance:
                        EquiDistance.ReCreateWhatIsNotSerialized(this);
                        break;
                    case ENUM.GuidedModuleType.Survey2Face:
                        Survey2Face.ReCreateWhatIsNotSerialized(this);
                        break;
                    case ENUM.GuidedModuleType.Survey2FaceArray:
                        Survey2Face.ReCreateWhatIsNotSerialized(this);
                        break;
                    case ENUM.GuidedModuleType.SurveyTilt:
                        Survey.ReCreateWhatIsNotSerialized(this);
                        break;
                    case ENUM.GuidedModuleType.TourDHorizon:
                        TourDHorizon.ReCreateWhatIsNotSerialized(this);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Cannot re-create what was not serialized in {_Name}", ex);
            }
        }
        /// <summary>
        /// mise à jour point qui a été modifié dans autre élément manager
        /// </summary>
        /// <param name="originalPoint"></param>
        /// <param name="pointModified"></param>
        internal override void UpdateTheoCoordinates(E.Point originalPoint, E.Point pointModified)
        {
            base.UpdateTheoCoordinates(originalPoint, pointModified);
            foreach (Station.Module item in StationModules)
            {
                item.UpdateTheoCoordinates(originalPoint, pointModified);
            }
        }

        /// <summary>
        /// Will add the given step after the current on if offset set to 1, 2 step forward if =2 etc
        /// </summary>
        /// <param name="step"></param>
        /// <param name="stepOffset"></param>
        internal void AddNewStepAfterCurrentOne(Step step, int stepOffset = 1)
        {
            Steps.Insert(currentIndex + stepOffset, step);
            LastAddedStep = step;
        }

        public event EventHandler<GuidedModuleEventArgs> StepBeingAdded;
        public event EventHandler<GuidedModuleEventArgs> StepAdded;

        // use to add a step and raise event linked
        internal void Add(Step step)
        {
            if (step != null)
            {
                StepBeingAdded?.Invoke(this, new GuidedModuleEventArgs() { step = step });
                Steps.Add(step);
                StepAdded?.Invoke(this, new GuidedModuleEventArgs() { step = step });
            }
            //System.Threading.Thread.Sleep(800);
        }

        /// <summary>
        /// this will actually, not remove the step but make it visible=false so it will not be shown again
        /// </summary>
        internal void RemoveCurrent()
        {
            CurrentStep.MakeDesappear();
        }

        internal void RemoveNextStep(bool erase = false)
        {
            if (erase)
                Steps.RemoveAt(currentIndex + 1);
            else
                NextStep.MakeDesappear();
        }

        internal void RemoveStep(int number, bool erase = false)
        {
            if (erase)
                Steps.RemoveAt(number);
            else
                Steps[number].MakeDesappear();
        }

        #endregion

        #region Actions

        #region Moving

        public override void Start()
        {
            base.Start();

            CurrentStep?.Enter();

            UpdateView();
        }

        internal override List<Measures.Measure> GetMeasures()
        {
            if (this.guideModuleType == ENUM.GuidedModuleType.AlignmentTilt 
                || this.guideModuleType == ENUM.GuidedModuleType.AlignmentLevelling)
            {
                return this.ReceivedMeasures;
            }
            else
            {
                List<Measures.Measure> measures = new List<Measures.Measure>();
                foreach (Station station in GetStations())
                {
                    int countBefore = measures.Count;
                    measures.AddRange(station.MeasuresTaken);
                    if (station is Tilt.Station)
                    {
                        for (int i = measures.Count - 1; i >= countBefore; i--)
                        {
                            if (measures[i]._Point == null)
                            {
                                measures.RemoveAt(i);
                            }
                            else
                                measures[i]._InstrumentSN = station.ParametersBasic._Instrument._Name;
                        }
                    }
                }
                return measures;
            }
        }

        public override void Restart()
        {
            MoveToStep(currentIndex, runIntermediateSteps: false, leaveCurrentStep: false, enterCurrent: false); // enter false because we enter when we restart();

            base.Restart();
        }

        /// <summary>
        /// Move to the next step of the guided module unless CancelExit=true
        /// </summary>
        internal void MoveToNextStep(bool skipUpdate = false)
        {
            bool okToLeave = CheckCurrentStepValidity();

            if (okToLeave)
            {
                CurrentStep.Leave();
                if (CurrentStep.Cancel)
                    return;
                else
                    StartNextStep();
            }
            if (!skipUpdate)
                _TsuView.UpdateView();

            CurrentStep.InvokeEntered();
        }

        private void StartNextStep()
        {
            // Change Step
            bool foundVisibleStep = false;
            while (!foundVisibleStep)
            {
                if (currentIndex < Steps.Count)
                    currentIndex += 1;
                else
                    throw new Exception(R.T231);

                if (CurrentStep.Visible) foundVisibleStep = true;
            }

            // Do action entering the step
            CurrentStep?.Enter();
        }

        /// <summary>
        /// check the step validity by running its Test() 
        /// </summary>
        /// <returns></returns>
        private bool CheckCurrentStepValidity()
        {
            CurrentStep?.Test();

            if (CurrentStep.State == StepState.NotReady)
            {
                if (CurrentStep.ForceNextStep())
                    return true;
                else
                    return false;
            }
            else
                return true;
        }

        internal void MoveToPreviousStep()
        {
            if (CurrentStep.IsOkToGoToPreviousStep())
            {
                // Do action before leaving the step
                CurrentStep?.Leave();

                StartPreviousStep();

                // Update
                _TsuView.UpdateView();
            }
        }

        private void StartPreviousStep()
        {
            // Change Step
            bool foundVisibleStep = false;
            while (!foundVisibleStep)
            {
                if (currentIndex > 0)
                    currentIndex -= 1;
                else
                    throw new Exception(R.T232);
                if (CurrentStep.Visible) foundVisibleStep = true;
            }
            // Do action before leaving the step
            CurrentStep?.Enter();
        }

        #endregion

        #region Save, export ,..

        internal void Export2Csv()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Update

        public override void OnNext(TsuObject TsuObject)
        {
            dynamic dynamic = TsuObject;
            OnNextBasedOn(dynamic);
        }

        #endregion



        #endregion

        internal void ShowInParentModule()
        {
            (ParentModule as Guided.Group.Module).SetActiveGuidedModule(this);
        }

        /// <summary>
        /// move to step number p
        /// </summary>
        /// <param name="p"></param>
        internal void MoveToStep(int p, bool runIntermediateSteps = true, bool leaveCurrentStep = true, bool enterCurrent = true)
        {
            targetStepIndex = p;
            if (p < Steps.Count)
            {
                if (runIntermediateSteps)
                {
                    if (p >= currentIndex)
                    {
                        for (int i = currentIndex; i < p; i++)
                        {
                            MoveToNextStep();
                        }
                    }
                    else
                    {
                        for (int i = currentIndex; i > p; i--)
                        {
                            MoveToPreviousStep();
                        }
                    }
                }
                else
                {
                    if (leaveCurrentStep)
                        CurrentStep.Leave();
                    currentIndex = p;
                    if (enterCurrent)
                        CurrentStep.Enter();
                    UpdateView();
                }
            }
            targetStepIndex = -1;
        }

        // move to step by the _ID string value
        internal void MoveToStep(string id, bool runIntermediateSteps = true)
        {
            int index = Steps.FindIndex(x => x.Id == id);
            //if (index==-1) index = Steps.FindIndex(x => x.TAG.ToString() == id);
            if (index != -1)
                MoveToStep(index, runIntermediateSteps);
            else
                //    this.View.ShowMessageOfCritical(string.Format("Failed to move to '{0}' step, it doesnt exist", id));
            {
                string titleAndMessage = $"{R.T_FAILED_TO_MOVE_TO} '{id}' {R.T_STEP_IT_DOESNT_EXIST}";
                new MessageInput(MessageType.Critical, titleAndMessage).Show();
            }
        }

        public override string ToString()
        {
            return $"GM: {_Name}, {StationModules.Count} Stations";
        }

        internal void HideStep(string id)
        {
            int index = Steps.FindIndex(x => x.Id == id);
            steps[index].Visible = true;
        }

        internal void ResetStep(string id, string idPart = "")
        {
            List<Step> l = Steps.FindAll(x => x.Id == id);
            foreach (Step item in l)
            {
                item.firstUse = true;
            }

            if (idPart != "")
            {
                l = Steps.FindAll(x => x.Id.Contains(idPart));
                foreach (Step item in l)
                {
                    item.firstUse = true;
                }
            }
        }
        internal override void SetActiveStationModule(Station.Module stationModule, bool createView = true)
        {
            UnsubscribeStepsEvents();
            base.SetActiveStationModule(stationModule, createView);
            SubscribeStepsEvents();
        }
        /// <summary>
        /// subscribe the events of steps
        /// </summary>
        internal void SubscribeStepsEvents()
        {
            CurrentStep?.UnsubscribeEvents();
        }
        /// <summary>
        /// unsubscribe the events of steps
        /// </summary>
        internal void UnsubscribeStepsEvents()
        {
            CurrentStep?.SubscribeEvents();
        }

        internal List<string> GetListOfSuAssembly()
        {
            List<string> ids = new List<string>();
            foreach (E.Point point in this.PointsToBeAligned)
            {
                string assembly = $"{point.ZoneOrAcc}.{point._ClassAndNumero}";
                if (!ids.Contains(assembly))
                    ids.Add(assembly);
            }
            return ids;
        }

        internal override IEnumerable<Station> GetStations()
        {
            var stations = new List<Station>();
            foreach (var item in this.StationModules)
            {
                stations.Add(item._Station);
            }
            return stations;
        }
    }
    public class GuidedModuleEventArgs : EventArgs
    {
        public Step step;
        public GuidedModuleEventArgs()
        {
        }
    }

    static public class Decore
    {
        static public void ShowRaisedEvent(Module module)
        {
            Views.Message.RaisedEventMessage message = new Views.Message.RaisedEventMessage
            {
                ParentView = module.View,
                coveringMessage = true
            };
            module.StepAdded += message.OnGuidedModuleEvent;
            module.Started += message.OnModuleStarted;
            message.Show();

            //message.ShowDialog(module.View);
            message.SetTitleAndMessage($"{R.T_CREATING_GUIDED_MODULE};{R.T_STARTING}", "G", out System.Drawing.Size dummy);
            message.Refresh();
        }

        static public void ShowRaisedEvent(Guided.Group.Module module)
        {
            Views.Message.RaisedEventMessage message = new Views.Message.RaisedEventMessage
            {
                ParentView = module.View,
                coveringMessage = true
            };
            module.GuidedModuleAdded += message.OnGuidedModuleAddedEvent;
            module.ActiveGuidedModuleChanged += message.OnActiveGuidedModuleChanged;
            message.Show();
            message.SetTitleAndMessage($"{R.T_CREATING_GUIDED_MODULE};{R.T_STARTING}", "G", out System.Drawing.Size dummy);
            message.Refresh();
        }


    }

}
