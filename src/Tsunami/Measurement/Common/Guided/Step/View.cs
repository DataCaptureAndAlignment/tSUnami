﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TSU.Common.Elements.Composites;
using TSU.Measurement.Common.Guided.Step;
using TSU.Views;
using TSU.Views.Message;
using static TSU.Polar.GuidedModules.Steps.StakeOut.Tools;
using E = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using P = TSU.Tsunami2.Preferences;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Common.Guided.Steps
{
    public partial class View : TsuView
    {
        Step Step
        {
            get
            {
                return this._Module as Step;
            }
        }
        public List<Control> ControlList;

        public Font DescriptionFont;
        public Font TitleFont;

        #region construct

        public View()
        {
            Initialize();
        }

        public View(TSU.Module module)
            : base(module) // Taking form already in controllerModuleView
        {
            this._Name = this._Name.Replace("View of", "View of Step");
            Initialize();
        }

        private void Initialize()
        {
            InitializeComponent();
            this.ApplyThemeColors();
            this.Resize += OnViewResized;

            CreateHiddenBoxForFocus();

            this.AutoScroll = true; // would like to set to false for the datagid of step to stop moving but no scroll is appearing...
            this.VScroll = true;

            TitleFont = P.Theme.Fonts.Large;
            DescriptionFont = P.Theme.Fonts.MonospaceFont;
        }
        private void ApplyThemeColors()
        {
            this.BackColor = P.Theme.Colors.Background;
        }
        protected override Point ScrollToControl(Control activeControl)
        {
            // the step is always scroling in every direction to bring active control on scrool position, we want to avoid it..
            return this.AutoScrollPosition;
        }

        TextBox hiddenTextBox;

        private void CreateHiddenBoxForFocus()
        {
            ControlList = new List<Control>();
            hiddenTextBox = new TextBox() { Left = 200, Visible = true, Top = -100 };
            hiddenTextBox.KeyUp += OnKeyUpped;
            hiddenTextBox.GotFocus += delegate
            {
                try
                {
                    Debug.WriteInConsole("hiddentext got focus");
                    TsuView parent = GetTsuViewParentFrom(this, typeof(Guided.View));
                    if (parent is Guided.View gv)
                    {
                        gv.AddShortcutTextInButton();
                    }
                    else
                        Debug.WriteInConsole("parent view was not guided view");
                }
                catch
                {

                }

            };
            hiddenTextBox.LostFocus += delegate
            {
                try
                {
                    var parent = GetTsuViewParentFrom(this, typeof(Guided.View));
                    if (parent is Guided.View guidedparent)
                    {
                        guidedparent.RemoveShortcutTextInButton();
                        Debug.WriteInConsole("hiddentext lost focus");
                    }
                    else
                        Debug.WriteInConsole("parent view was not guided view");
                }
                catch
                {

                }

            };
            ControlList.Add(hiddenTextBox);
            this.Click += delegate
            {
                hiddenTextBox.Focus();
            };
        }



        /// <summary>
        ///  Will set the max size of the controls when the view is resized
        /// </summary>
        private void OnViewResized(object sender, EventArgs e)
        {
            foreach (Control item in this.Controls)
            {
                if (item is Label label)
                {
                    label.MaximumSize = new Size(this.Width - 20, 0);
                }

            }
            var focusedControls = this.Controls.Cast<Control>().Where(x => x.Focused);

            if (focusedControls.FirstOrDefault() == null)
                if (this.focusBox != null)
                    FocusView();
        }

        public override bool Focused
        {
            get
            {
                if (this.focusBox != null)
                    return this.focusBox.Focused;
                else
                    return false;
            }
        }

        public override void FocusView()
        {
            this.focusBox.Focus();
        }

        #endregion

        #region Update

        /// <summary>
        /// Will clear the controls list and re-add the control to make the view and button s thta desappear to other views to come back.
        /// </summary>
        public TextBox focusBox;

        internal void ResetView()
        {
            this.Controls.Clear();
            foreach (Control item in ControlList.Reverse<Control>())
            {
                this.Controls.Add(item);
            }

            // add a invisble textbox to allow focus
            focusBox = new TextBox() { Top = this.Controls[this.Controls.Count - 1].Top, Left = this.Controls[this.Controls.Count - 1].Left + 10, Width = 5 };
            focusBox.KeyUp += this.OnKeyUpped;
            this.Controls.Add(focusBox);
        }

        public enum LayoutOption
        {
            Suspend,
            Resume,
        }

        int numberSuspendedCalls = 0;

        public void LayoutLayering(
            LayoutOption option = LayoutOption.Resume)
        {
            if (option == LayoutOption.Suspend)
            {
                if (numberSuspendedCalls == 0)
                    this.SuspendLayout();
                numberSuspendedCalls++;
            }
            else
            {
                numberSuspendedCalls--;
                if (numberSuspendedCalls < 0)
                    numberSuspendedCalls = 0;

                if (numberSuspendedCalls == 0)
                    this.ResumeLayout(performLayout: true);
            }
        }


        public override void UpdateView()
        {
            LayoutLayering(LayoutOption.Suspend);
            base.UpdateView();

            foreach (var item in this.Controls)
            {
                //if (item is TextBox)
                //{ 
                //    (item as TextBox).Focus();
                //    (item as TextBox).SelectAll();
                //}
                if (item is ManagerView mv)
                {
                    mv.UpdateView();
                    mv.FocusView();
                }
            }
            LayoutLayering(LayoutOption.Resume);
            this.focusBox.Focus();
        }

        internal bool ForceNextStep(string message = "")
        {
            if (message == "" || message == null) message = R.T269;
            //   string force = "Force next step";
            string force = R.T_FORCE_NEXT_STEP;
            string titleAndMessage = R.String_GM_MsgBlockTitle + message;
            MessageInput mi = new MessageInput(MessageType.Warning, titleAndMessage)
            {
                ButtonTexts = new List<string> { R.T_OK, force }
            };
            return (mi.Show().TextOfButtonClicked == force);
        }

        #endregion

        #region Views

        /// <summary>
        /// Will add a TSUview to the stepview
        /// </summary>
        /// <param name="c"></param>
        internal void AddControl(TSU.Module module)
        {
            TsuView view = module._TsuView;
            if (view is E.Manager.View emv) emv.buttons.BlockAdvanced();
            view.ShowDockedFill();
            this.Controls.Add(view);
            view.BringToFront();
            ControlList.Add(view);
            module.Changed -= this.Step.Manager_Changed; // remove first to avoid to be there several tilmes
            module.Changed += this.Step.Manager_Changed;
        }

        // if you want a control change event inked you shuld use the AddControl with manager as parameter
        internal void AddControl(TsuView view)
        {
            if (view is E.Manager.View emv) emv.buttons.BlockAdvanced();
            view.ShowDockedFill();
            this.Controls.Add(view);
            view.BringToFront();
            ControlList.Add(view);
        }

        /// <summary>
        /// Add a grid view
        /// </summary>
        /// <param name="c"></param>
        internal void AddGrid(DataGridView c, DockStyle dockStyle = DockStyle.Top)
        {
            c.BringToFront();
            ControlList.Add(c);
            c.Dock = dockStyle;
        }

        internal void AddGrid(Views.TreeGrid c, DockStyle dockStyle = DockStyle.Top)
        {
            c.BringToFront();
            ControlList.Add(c);
            c.Dock = dockStyle;
        }

        /// <summary>
        /// Remove all controls
        /// </summary>
        internal void ClearAllControls()
        {
            ControlList.Clear();
        }


        internal void RemoveControl(Manager manager)
        {
            this.Controls.Remove(manager.View);
            manager.Changed -= this.Step.Manager_Changed;
        }

        #endregion

        #region Label

        internal void AddTitle(string text, string name = "Unknwon")
        {
            AddLabel(text, name, TitleFont);
        }

        internal void AddDescription(string text, string name = "Unknwon")
        {
            AddLabel(text, name, DescriptionFont);
        }

        /// <summary>
        /// Change le texte dans le label description suivant le description Name or create it if it doesnt exist
        /// </summary>
        /// <param name="descriptionName"></param>
        /// <param name="text"></param>
        internal void ModifyLabel(string text, string descriptionName, bool addToExistingText = false)
        {
            if (GetLabelByName(descriptionName, out Label l))
            {
                if (addToExistingText)
                    l.Text += text;
                else
                    l.Text = text;

                l.MaximumSize = new Size(this.Width - 20, 0);
            }
            else
            {
                AddDescription(text, descriptionName);
            }
        }

        internal Label GetPreparedLabel(string newText, string descriptionName)
        {
            return new Label()
            {
                Font = DescriptionFont,
                Text = newText,
                ForeColor = P.Theme.Colors.LightForeground,
                Name = descriptionName,
                Dock = DockStyle.Fill,
                TextAlign = ContentAlignment.MiddleCenter
            };
        }

        private bool GetButtonByName(string descriptionName, out BigButton buttonFound)
        {
            buttonFound = null;
            if (GetControlByName(descriptionName, this.Controls, out Control control))
            {
                if (control is BigButton bb)
                {
                    buttonFound = bb;
                    return true;
                }
            }
            return false;
        }

        private bool GetLabelByName(string descriptionName, out Label labelFound)
        {
            labelFound = null;
            if (GetControlByName(descriptionName, this.Controls, out Control control))
            {
                if (control is Label l)
                {
                    labelFound = l;
                    return true;
                }
            }
            return false;
        }

        private bool GetControlByName(string descriptionName, Control.ControlCollection collection, out Control controlFound)
        {
            controlFound = null;
            if (collection.ContainsKey(descriptionName))
            {
                controlFound = collection.Find(descriptionName, false)[0];
                return true;
            }
            foreach (Control item in collection)
            {
                if (item is TableLayoutPanel)
                    if (GetControlByName(descriptionName, item.Controls, out controlFound))
                        return true;
            }
            return false;
        }

        #region re arrangeable data grid

        DataGridView RearrangableGrid;
        internal DataGridView AddRearrangableGrid(DataGridView grid, DockStyle dockStyle)
        {

            RearrangableGrid = Grid.Initialize();
            RearrangableGrid.MouseMove += RearrangableGrid_MouseMove;
            RearrangableGrid.MouseDown += RearrangableGrid1_MouseDown;
            RearrangableGrid.DragOver += RearrangableGrid_DragOver;
            RearrangableGrid.DragDrop += RearrangableGrid_DragDrop;

            this.Controls.Add(RearrangableGrid);
            RearrangableGrid.BringToFront();
            ControlList.Add(RearrangableGrid);
            RearrangableGrid.Dock = dockStyle;


            return RearrangableGrid;
        }

        private Rectangle dragBoxFromMouseDown;
        public int rowIndexFromMouseDown;
        public int rowIndexOfItemUnderMouseToDrop;
        private void RearrangableGrid_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                // If the mouse moves outside the rectangle, start the drag.
                if (dragBoxFromMouseDown != Rectangle.Empty &&
                    !dragBoxFromMouseDown.Contains(e.X, e.Y))
                {

                    // Proceed with the drag and drop, passing in the list item.                    
                    DragDropEffects dropEffect = RearrangableGrid.DoDragDrop(
                    RearrangableGrid.Rows[rowIndexFromMouseDown],
                    DragDropEffects.Move);
                }
            }
        }

        private void RearrangableGrid1_MouseDown(object sender, MouseEventArgs e)
        {
            // Get the index of the item the mouse is below.
            rowIndexFromMouseDown = RearrangableGrid.HitTest(e.X, e.Y).RowIndex;
            if (rowIndexFromMouseDown != -1)
            {
                // Remember the point where the mouse down occurred. 
                // The DragSize indicates the size that the mouse can move 
                // before a drag event should be started.                
                Size dragSize = SystemInformation.DragSize;

                // Create a rectangle using the DragSize, with the mouse position being
                // at the center of the rectangle.
                dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2),
                                                               e.Y - (dragSize.Height / 2)),
                                    dragSize);
            }
            else
                // Reset the rectangle if the mouse is not over an item in the ListBox.
                dragBoxFromMouseDown = Rectangle.Empty;
        }

        private void RearrangableGrid_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }


        public bool GridHasBeenRearanged = true;


        private void RearrangableGrid_DragDrop(object sender, DragEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            // The mouse locations are relative to the screen, so they must be 
            // converted to client coordinates.
            Point clientPoint = RearrangableGrid.PointToClient(new Point(e.X, e.Y));

            // Get the row index of the item the mouse is below. 
            rowIndexOfItemUnderMouseToDrop =
                RearrangableGrid.HitTest(clientPoint.X, clientPoint.Y).RowIndex;

            GridHasBeenRearanged = true;
            (dgv.Parent as View).Step.UpdateForReal();

        }

        #endregion


        internal void ModifyLabel(string newText, string descriptionName, Color color, bool bChangeDockStyle = false, DockStyle dockstyle = DockStyle.Top)
        {
            ModifyLabel(newText, descriptionName);
            ModifyLabelColor(color, descriptionName);
            if (bChangeDockStyle)
                ModifyLabelDockStyle(dockstyle, descriptionName);
        }

        private void ModifyLabelDockStyle(DockStyle dockstyle, string descriptionName)
        {
            if (GetControlByName(descriptionName, this.Controls, out Control controlFound))
            {
                (controlFound as Label).AutoSize = false;
                controlFound.Dock = dockstyle;
                controlFound.MaximumSize = new Size(200, 0); // set the maximum width of the label
                (controlFound as Label).AutoSize = true;
            }
        }

        internal void ModifyLabelColor(Color color, string descriptionName)
        {
            if (GetControlByName(descriptionName, this.Controls, out Control controlFound))
            {
                controlFound.ForeColor = color;
            }
        }

        /// <summary>
        /// Donne aussi un nom au label créé pour ensuite pouvoir le mettre à jour
        /// </summary>
        /// <param name="text"></param>
        /// <param name="name"></param>
        private void AddLabel(string text, string name, Font font)
        {
            Label l = CreateLabel(text, name, font);
            this.Controls.Add(l);
            l.BringToFront();
            ControlList.Add(l);
        }

        public Label CreateLabel(string text, string name, Font font)
        {
            return new Label()
            {
                Dock = DockStyle.Top,
                ForeColor = P.Theme.Colors.LightForeground,
                Text = text,
                Name = name,
                AutoSize = true,
                Font = font

            };
        }

        #endregion

        #region button

        internal BigButton AddButton(string titleAndDescription, Image image, EventHandler<TsuObjectEventArgs> handler, Color? color = null)
        {
            BigButton b = new BigButton(titleAndDescription, image, handler, this.Step.GuidedModule) { Dock = DockStyle.Top };
            this.Controls.Add(b);
            b.BringToFront();
            ControlList.Add(b);
            return b;
        }

        internal void AddButton(BigButton existingButton, string name = "", DockStyle dockStyle = DockStyle.Top)
        {
            if (existingButton == null) return;
            if (name != "") existingButton.Name = name;
            existingButton.Dock = dockStyle;
            existingButton.HasSubButtons = true;
            this.Controls.Add(existingButton);
            existingButton.BringToFront();
            ControlList.Add(existingButton);
        }

        internal void AddSidedControls(List<Control> existingButtons, DockStyle dockstyle = DockStyle.Bottom, List<int> forcedSizesInPourcent = null)
        {
            TableLayoutPanel TLP = CreateLayoutForButtons(existingButtons, dockstyle, forcedSizesInPourcent);

            this.Controls.Add(TLP);
            ControlList.Add(TLP);
        }



        internal void AddButton(ToolStripItem existingToolStripItem, string name = "")
        {
            this.AddButton((existingToolStripItem as BigButtonMenuItem).bigButton, name);
        }

        #endregion

        #region TextBox

        internal void AddTextBox(TsuView TsuView, string defaultString)
        {
            TextBox l = new TextBox()
            {
                Font = P.Theme.Fonts.Normal,
                Dock = DockStyle.Top,
                ForeColor = P.Theme.Colors.NormalFore,
                Text = defaultString
            };
            l.TextChanged += OnTextChanged;
            l.KeyDown += delegate (object sender, KeyEventArgs e)
            {
                if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                    this.FocusView();
            };

            this.Controls.Add(l);
            l.BringToFront();

            ControlList.Add(l);
        }

        private void OnTextChanged(object sender, EventArgs e)
        {
            (this._Module as Step).TAG = (sender as TextBox).Text;
            this.Step.ControlChange();
        }

        #endregion

        #region panel

        internal Process AddShowDatFilePanel(string filePath)
        {
            Panel p = new Panel();
            Process process = Shell.RunInPanel(p, filePath, false, "notepad.exe");
            p.Dock = DockStyle.Fill;
            //ForeColor = Tsunami2.Preferences.Theme._Colors.Foreground;
            p.BackColor = P.Theme.Colors.Background;
            this.Controls.Add(p);
            p.BringToFront();
            ControlList.Add(p);
            return process;
        }
        /// <summary>
        /// Show LGC calculation in 2 panels
        /// </summary>
        /// <param name="inputFilePath"></param>
        /// <param name="outputFilePath"></param>
        /// <returns></returns>
        internal List<Process> AddLGCFilesPanel(string inputFilePath, string outputFilePath)
        {
            SplitContainer splitContainerLGC = new SplitContainer();
            splitContainerLGC.Dock = DockStyle.Fill;
            splitContainerLGC.Location = new Point(0, 0);
            splitContainerLGC.Name = "splitContainerLGC";
            splitContainerLGC.Orientation = Orientation.Vertical;
            Panel panelInput = new Panel();
            Process processInput = Shell.RunInPanel(panelInput, inputFilePath, false, "notepad.exe");
            panelInput.Dock = DockStyle.Fill;
            panelInput.BackColor = P.Theme.Colors.Background;
            splitContainerLGC.Panel1.Controls.Add(panelInput);
            Panel panelOutput = new Panel();
            Process processOutput = Shell.RunInPanel(panelOutput, outputFilePath, false, "notepad.exe");
            panelOutput.Dock = DockStyle.Fill;
            panelOutput.BackColor = P.Theme.Colors.Background;
            splitContainerLGC.Panel2.Controls.Add(panelOutput);

            this.Controls.Add(splitContainerLGC);
            splitContainerLGC.BringToFront();
            ControlList.Add(splitContainerLGC);
            splitContainerLGC.SplitterDistance = Convert.ToInt32(splitContainerLGC.Width / 2);
            List<Process> processList = new List<Process>();
            processList.Add(processInput);
            processList.Add(processOutput);
            P.Theme.ApplyTo(splitContainerLGC);
            return processList;
        }

        internal void OnKeyUpped(object sender, KeyEventArgs e)
        {
            Module m = (this._Module as Step).GuidedModule;

            switch (e.KeyCode)
            {
                case Keys.N:
                case Keys.Right:
                    m.MoveToNextStep();
                    break;

                case Keys.P:
                case Keys.Left:
                    m.MoveToPreviousStep();
                    break;
                default:
                    // check if there is a button statring withthis letter

                    foreach (BigButton b in BigButton.GetButtonsFrom(this))
                    {
                        KeysConverter kc = new KeysConverter();
                        Char[] charArray = kc.ConvertToString(e.KeyCode).ToCharArray();
                        //N'autorise pas les touches spéciales comme enter, shift, etc
                        if (charArray.Count() == 1)
                        {
                            char keyChar = kc.ConvertToString(e.KeyCode)[0];
                            if (b.Title[0] == keyChar)
                            {
                                b.ShowButtonAsHoovered();
                                b.Do();
                                b.ShowButtonAsNotHoovered();
                                break;
                            }
                        }
                    }
                    break;
            }
        }
        #endregion


        #region datagridviewMesures
        //DataGridView for management module result
        internal DataGridView dataGridViewResult;
        internal Views.TreeGrid treeGridViewResultBOC;
        private DataGridViewTextBoxColumn PointName;
        private DataGridViewTextBoxColumn Dcum;
        private DataGridViewTextBoxColumn Tilt;
        private DataGridViewTextBoxColumn LevelDV;
        private DataGridViewTextBoxColumn PolarDV;
        private DataGridViewTextBoxColumn WireDR;
        private DataGridViewTextBoxColumn PolarDR;
        private DataGridViewTextBoxColumn PolarDL;
        private DataGridViewTextBoxColumn DLAlone;
        private DataGridViewImageColumn Remove;
        private DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
        //Pour le tri dans le datagridviewResult
        private ENUM.SortDirection sortDirection = ENUM.SortDirection.Dcum;

        /// <summary>
        /// Crèe le datagridview result
        /// </summary>
        public void CreateDatagridViewResult()
        {
            this.dataGridViewResult = new DataGridView();

            this.treeGridViewResultBOC = new Views.TreeGrid();
            this.PointName = new DataGridViewTextBoxColumn();
            this.Dcum = new DataGridViewTextBoxColumn();
            this.Tilt = new DataGridViewTextBoxColumn();
            this.LevelDV = new DataGridViewTextBoxColumn();
            this.PolarDV = new DataGridViewTextBoxColumn();
            this.WireDR = new DataGridViewTextBoxColumn();
            this.PolarDR = new DataGridViewTextBoxColumn();
            this.PolarDL = new DataGridViewTextBoxColumn();
            this.DLAlone = new DataGridViewTextBoxColumn();
            this.Remove = new DataGridViewImageColumn();
        }

        /// <summary>
        /// Initialise le datagridview result
        /// </summary>
        public void InitializeDatagridViewResult()
        {
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).BeginInit();
            // 
            // dataGridView1
            // 
            this.dataGridViewResult.AllowUserToResizeColumns = true;
            this.dataGridViewResult.AllowUserToResizeRows = false;
            this.dataGridViewResult.MultiSelect = false;
            this.dataGridViewResult.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = P.Theme.Colors.Object;
            dataGridViewCellStyle1.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = P.Theme.Colors.NormalFore;
            dataGridViewCellStyle1.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            dataGridViewCellStyle1.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            dataGridViewCellStyle1.WrapMode = DataGridViewTriState.True;
            this.dataGridViewResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewResult.ColumnHeadersHeight = 400;
            this.dataGridViewResult.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.dataGridViewResult.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.dataGridViewResult.DefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            this.dataGridViewResult.DefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.dataGridViewResult.DefaultCellStyle.Font = P.Theme.Fonts.Normal;
            this.dataGridViewResult.RowHeadersDefaultCellStyle.Font = P.Theme.Fonts.Normal;
            this.dataGridViewResult.ColumnHeadersDefaultCellStyle.Font = P.Theme.Fonts.Normal;

            this.dataGridViewResult.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewResult.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewResult.AllowUserToAddRows = false;
            this.dataGridViewResult.AllowUserToDeleteRows = false;
            this.dataGridViewResult.Columns.AddRange(
            this.Remove,
            this.PointName,
            this.Dcum,
            this.WireDR,
            this.PolarDR,
            this.DLAlone,
            this.PolarDL,
            this.LevelDV,
            this.PolarDV,
            this.Tilt);
            this.dataGridViewResult.Dock = DockStyle.Fill;
            this.dataGridViewResult.EnableHeadersVisualStyles = false;
            this.dataGridViewResult.GridColor = P.Theme.Colors.Background;
            this.dataGridViewResult.Location = new Point(0, 0);
            this.dataGridViewResult.MultiSelect = false;
            this.dataGridViewResult.Name = "dataGridViewResult";
            this.dataGridViewResult.RowHeadersVisible = false;
            this.dataGridViewResult.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewResult.ShowCellErrors = false;
            this.dataGridViewResult.ShowEditingIcon = false;
            this.dataGridViewResult.ShowRowErrors = false;
            this.dataGridViewResult.Size = new Size(1206, 223);
            this.dataGridViewResult.TabIndex = 15;
            this.dataGridViewResult.ReadOnly = true;
            // 
            // Remove 0
            // 
            this.Remove.HeaderText = "";
            this.Remove.ImageLayout = DataGridViewImageCellLayout.Stretch;
            this.Remove.Name = "Remove";
            this.Remove.ReadOnly = true;
            this.Remove.Resizable = DataGridViewTriState.False;
            this.Remove.Width = 30;
            // 
            // PointName 1
            // 
            this.PointName.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.PointName.HeaderText = R.StringAlignment_Datagrid_PointName;
            this.PointName.Resizable = DataGridViewTriState.True;
            this.PointName.SortMode = DataGridViewColumnSortMode.Automatic;
            this.PointName.Name = "PointName";
            this.PointName.ReadOnly = true;
            this.PointName.Width = 250;
            // 
            // Dcum 2
            // 
            this.Dcum.SortMode = DataGridViewColumnSortMode.NotSortable;
            this.Dcum.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.Dcum.HeaderText = R.StringTilt_DataGrid_Dcum;
            this.Dcum.Name = "Dcum";
            this.Dcum.Width = 100;
            this.Dcum.ReadOnly = true;
            // 
            // Tilt 9
            // 
            this.Tilt.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.Tilt.HeaderText = R.StringAlignment_Datagrid_Tilt;
            this.Tilt.SortMode = DataGridViewColumnSortMode.NotSortable;
            this.Tilt.Name = "Tilt";
            this.Tilt.ReadOnly = true;
            // 
            // Level DV 7
            // 
            this.LevelDV.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.LevelDV.HeaderText = R.StringAlignment_Datagrid_Level;
            this.LevelDV.SortMode = DataGridViewColumnSortMode.Automatic;
            this.LevelDV.Name = "LevelDV";
            this.LevelDV.ReadOnly = true;
            // 
            // Polar DV 8
            // 
            this.PolarDV.SortMode = DataGridViewColumnSortMode.Automatic;
            this.PolarDV.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.PolarDV.HeaderText = R.StringAlignment_Datagrid_PolarDV;
            this.PolarDV.Name = "PolarDV";
            this.PolarDV.ReadOnly = true;
            // 
            //  Measure Polar DL 6
            // 
            this.PolarDL.SortMode = DataGridViewColumnSortMode.Automatic;
            this.PolarDL.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.PolarDL.HeaderText = R.StringAlignment_Datagrid_PolarDL;
            this.PolarDL.Name = "PolarDL";
            this.PolarDL.ReadOnly = true;
            // 
            // DL Length setting 5
            // 
            this.DLAlone.SortMode = DataGridViewColumnSortMode.Automatic;
            this.DLAlone.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.DLAlone.HeaderText = R.StringAlignment_Datagrid_DLAlone;
            this.DLAlone.Name = "DLAlone";
            this.DLAlone.ReadOnly = true;
            // 
            // Wire DR 3
            // 
            this.WireDR.SortMode = DataGridViewColumnSortMode.Automatic;
            this.WireDR.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.WireDR.HeaderText = R.StringAlignment_Datagrid_WireDR;
            this.WireDR.Name = "WireDR";
            this.WireDR.ReadOnly = true;
            // 
            // Polar DR 4
            // 
            this.PolarDR.SortMode = DataGridViewColumnSortMode.Automatic;
            this.PolarDR.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.PolarDR.HeaderText = R.StringAlignment_Datagrid_PolarDR;
            this.PolarDR.Name = "PolarDR";
            this.PolarDR.ReadOnly = true;

            this.dataGridViewResult.CellDoubleClick += new DataGridViewCellEventHandler(this.dataGridViewResult_CellDoubleClick);
            this.dataGridViewResult.ColumnHeaderMouseClick += new DataGridViewCellMouseEventHandler(this.dataGridViewresult_ColumnHeaderMouseClick);
            this.dataGridViewResult.CellContentClick += Grid_CellContentClick;
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).EndInit();

            var splitContainer = new SplitContainer() { Dock = DockStyle.Fill, Orientation = Orientation.Horizontal };
            P.Theme.ApplyTo(splitContainer);
            //splitContainer.Panel2.BackColor = System.Drawing.Color.Red;

            splitContainer.Panel1.Controls.Add(this.dataGridViewResult);

            var treeGrid = this.treeGridViewResultBOC;
            treeGrid.Dock = DockStyle.Fill;
            splitContainer.Panel2.Controls.Add(treeGrid);
            treeGrid.BringToFront();

            this.Controls.Add(splitContainer);
            this.dataGridViewResult.BringToFront();
            ControlList.Add(splitContainer);
        }

        private void Grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0) //the button 'remove'
            {
                DataGridView dv = (sender as DataGridView);
                E.Point p = dv.Rows[e.RowIndex].Tag as E.Point;
                if (p != null)
                {
                    string titleAndMessage = String.Format(R.StringAlignment_Datagrid_ConfirmDeletePoint, p._Name);
                    MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == R.T_OK)
                    {
                        this.Step.GuidedModule.PointsToBeAligned.Remove(p);
                    }
                }
                CloneableList<E.Point> allPoints = this.Step.GuidedModule.PointsToBeAligned;
                List<Magnet> allMagnets = this.Step.GuidedModule.MagnetsToBeAligned;
                Management.ResetPointToMeasureInStations(this.Step, allPoints, allMagnets);

                this.Step.Update();
            }
            //tri par dcum
            if (e.RowIndex == -1 && e.ColumnIndex == 2)
            {
                this.SortMeasureInDataGridViewResult(this.Step.GuidedModule.PointsToBeAligned);
            }
        }

        /// <summary>
        /// Complète le datagridview avec les écarts
        /// </summary>
        internal void RedrawDataGridViewResult(List<E.Point> pointsToBeAligned,
            List<M.Measure> receivedMeasures,
            double rollTolerance,
            double verticalTolerance,
            double lengthTolerance,
            double radialTolerance)
        {
            if (dataGridViewResult == null) return;
            dataGridViewResult.Rows.Clear();
            DataGridUtility.SaveSorting(dataGridViewResult);
            //TO BE TESTED
            //this.SortMeasureInDataGridViewResult(pointsToBeAligned);
            List<List<object>> listRow = new List<List<object>>();
            foreach (E.Point point in pointsToBeAligned)
            {
                AddNewRowInDataGridView(listRow, point, receivedMeasures, pointsToBeAligned);
            }
            foreach (List<Object> ligne in listRow)
            {
                DataGridViewRow newRow = new DataGridViewRow();
                E.Point p = ligne[10] as E.Point; // point ref
                ligne.RemoveAt(10);
                dataGridViewResult.Rows.Add(ligne.ToArray());
                dataGridViewResult.Rows[dataGridViewResult.Rows.Count - 1].Tag = p;
            }
            for (int i = 0; i < dataGridViewResult.RowCount; i++)
            {
                //Met en surligné rouge si écart au dessus de la tolérance, met en vert si écart OK (commenté temporairement en attendant validation tolérance)
                //Ecart Tilt
                double DTilt = T.Conversions.Numbers.ToDouble(dataGridViewResult[9, i].Value.ToString(), true, -9999);
                //if (DTilt != -9999)
                //{
                //    dataGridViewResult[1, i].Style.BackColor = (Math.Abs(DTilt) > rollTolerance) ? Tsunami2.Preferences.Theme._Colors.Bad : this.dataGridViewResult.Rows[i].DefaultCellStyle.BackColor;
                //    dataGridViewResult[1, i].Style.ForeColor = (Math.Abs(DTilt) > rollTolerance) ? this.dataGridViewResult.Rows[i].DefaultCellStyle.ForeColor : Tsunami2.Preferences.Theme._Colors.Good;
                //}
                //Ecart Level
                double DVertLevel = T.Conversions.Numbers.ToDouble(dataGridViewResult[7, i].Value.ToString(), true, -9999);
                //if (DVertLevel != -9999)
                //{
                //    dataGridViewResult[2, i].Style.BackColor = (Math.Abs(DVertLevel) > verticalTolerance) ? Tsunami2.Preferences.Theme._Colors.Bad : this.dataGridViewResult.Rows[i].DefaultCellStyle.BackColor;
                //    dataGridViewResult[2, i].Style.ForeColor = (Math.Abs(DVertLevel) > verticalTolerance) ? this.dataGridViewResult.Rows[i].DefaultCellStyle.ForeColor : Tsunami2.Preferences.Theme._Colors.Good;
                //}
                //Ecart polar vertical
                double DVertPolar = T.Conversions.Numbers.ToDouble(dataGridViewResult[8, i].Value.ToString(), true, -9999);
                //if (DVertPolar != -9999)
                //{
                //    dataGridViewResult[3, i].Style.BackColor = (Math.Abs(DVertPolar) > verticalTolerance) ? Tsunami2.Preferences.Theme._Colors.Bad : this.dataGridViewResult.Rows[i].DefaultCellStyle.BackColor;
                //    dataGridViewResult[3, i].Style.ForeColor = (Math.Abs(DVertPolar) > verticalTolerance) ? this.dataGridViewResult.Rows[i].DefaultCellStyle.ForeColor : Tsunami2.Preferences.Theme._Colors.Good;
                //}
                //Ecart Dl alone
                double DLAlone = T.Conversions.Numbers.ToDouble(dataGridViewResult[5, i].Value.ToString(), true, -9999);
                //if (DLAlone != -9999)
                //{
                //    dataGridViewResult[4, i].Style.BackColor = (Math.Abs(DLAlone) > lengthTolerance) ? Tsunami2.Preferences.Theme._Colors.Bad : this.dataGridViewResult.Rows[i].DefaultCellStyle.BackColor;
                //    dataGridViewResult[4, i].Style.ForeColor = (Math.Abs(DLAlone) > lengthTolerance) ? this.dataGridViewResult.Rows[i].DefaultCellStyle.ForeColor : Tsunami2.Preferences.Theme._Colors.Good;
                //}
                //Ecart Dl polar
                double DLPolar = T.Conversions.Numbers.ToDouble(dataGridViewResult[6, i].Value.ToString(), true, -9999);
                //if (DLPolar != -9999)
                //{
                //    dataGridViewResult[5, i].Style.BackColor = (Math.Abs(DLPolar) > lengthTolerance) ? Tsunami2.Preferences.Theme._Colors.Bad : this.dataGridViewResult.Rows[i].DefaultCellStyle.BackColor;
                //    dataGridViewResult[5, i].Style.ForeColor = (Math.Abs(DLPolar) > lengthTolerance) ? this.dataGridViewResult.Rows[i].DefaultCellStyle.ForeColor : Tsunami2.Preferences.Theme._Colors.Good;
                //}
                //Ecart DR wire
                double DRWire = T.Conversions.Numbers.ToDouble(dataGridViewResult[3, i].Value.ToString(), true, -9999);
                //if (DRWire != -9999)
                //{
                //    dataGridViewResult[6, i].Style.BackColor = (Math.Abs(DRWire) > radialTolerance) ? Tsunami2.Preferences.Theme._Colors.Bad : this.dataGridViewResult.Rows[i].DefaultCellStyle.BackColor;
                //    dataGridViewResult[6, i].Style.ForeColor = (Math.Abs(DRWire) > radialTolerance) ? this.dataGridViewResult.Rows[i].DefaultCellStyle.ForeColor : Tsunami2.Preferences.Theme._Colors.Good;
                //}
                //Ecart DR polar
                double DRPolar = T.Conversions.Numbers.ToDouble(dataGridViewResult[4, i].Value.ToString(), true, -9999);
                //if (DRPolar != -9999)
                //{
                //    dataGridViewResult[7, i].Style.BackColor = (Math.Abs(DRPolar) > radialTolerance) ? Tsunami2.Preferences.Theme._Colors.Bad : this.dataGridViewResult.Rows[i].DefaultCellStyle.BackColor;
                //    dataGridViewResult[7, i].Style.ForeColor = (Math.Abs(DRPolar) > radialTolerance) ? this.dataGridViewResult.Rows[i].DefaultCellStyle.ForeColor : Tsunami2.Preferences.Theme._Colors.Good;
                //}
            }
            DataGridUtility.RestoreSorting(dataGridViewResult);
        }


        internal void RedrawTreeGridViewResult(List<E.Point> pointsToBeAligned,
        List<M.Measure> receivedMeasures,
        List<Compute.Compensations.BeamOffsets.Results> bOC_Results,
        double rollTolerance,
        double verticalTolerance,
        double lengthTolerance,
        double radialTolerance)
        {
            var treeGrid = this.treeGridViewResultBOC;

            RedrawTreeGridViewResult_DefineCastingWay(treeGrid);

            treeGrid.BackColor = Color.Green;
            treeGrid.Dock = DockStyle.Fill;
            //treeGrid.Show();
            //treeGrid.BringToFront();
            var globalResults = GlobalAlignmentResults.Categorize(pointsToBeAligned, receivedMeasures, bOC_Results);

            if (globalResults != null)
            {
                treeGrid.DataSource = globalResults;
                treeGrid.HiddenColumns = GlobalAlignmentResults.GetColumnsNameToHide(receivedMeasures);
                treeGrid.DatagridView1_CellMouseEnter += TreegridDatagridView_CellMouseEnter;
                treeGrid.DatagridView1_CellMouseLeave += TreegridDatagridView_CellMouseLeave;
                treeGrid.DatagridView1_CellDoubleClick += TreegridDatagridView_CellDoubleClick;
                treeGrid.DatagridView1_CellPaint += TreegridDatagridView_CellPainting;

                this.treeGridViewResultBOC.Show();
            }
        }

        private void TreegridDatagridView_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            //TSU.Debug.WriteInConsole($"Pre CellPainting {e.RowIndex}");

            if (e.Value is GlobalAlignmentResults.ResultValue rv && rv.Style != null)
                e.CellStyle.ApplyStyle(rv.Style);

            //TSU.Debug.WriteInConsole($"After DatagridView1_CellPainting {e.RowIndex}");
        }

        private void TreegridDatagridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Debug.WriteInConsole("Tree Grid cell double clicked");

            if (e.RowIndex < 0) return;
            var grid = sender as DataGridView;
            var GrGm = this.Step.GuidedModule.ParentModule as Group.Module;

            bool cellEmpty = grid[e.ColumnIndex, e.RowIndex].Value == null;

            // dRoll
            if (grid.Columns[e.ColumnIndex].HeaderText.Contains("dRoll") && !cellEmpty)
                if (treeGridViewResultBOC.FlattenCategories[e.RowIndex] is GlobalAlignmentResults gar)
                    if (gar.Tag is Tilt.Measure tiltMeasure)
                        DataGridView_Action_Roll_measurement(tiltMeasure._Point._Name, GrGm);
            // Wire
            if (grid.Columns[e.ColumnIndex].HeaderText.Contains("Wire") && !cellEmpty)
                if (treeGridViewResultBOC.FlattenCategories[e.RowIndex] is GlobalAlignmentResults gar)
                    if (gar.Tag is E.Point point)
                        DataGridView_Action_Launch_Wire_Measurement_DR(point._Name, GrGm);

            // Level
            if (grid.Columns[e.ColumnIndex].HeaderText.Contains("Level") && !cellEmpty)
                if (treeGridViewResultBOC.FlattenCategories[e.RowIndex] is GlobalAlignmentResults gar)
                    if (gar.Tag is E.Point point)
                        DataGridView_Action_Level_Measurement(point._Name, GrGm);

            // Length
            if (grid.Columns[e.ColumnIndex].HeaderText.Contains("Dist") && !cellEmpty)
                if (treeGridViewResultBOC.FlattenCategories[e.RowIndex] is GlobalAlignmentResults gar)
                    if (gar.Tag is E.Point point)
                        DataGridView_Action_Launch_Length_Measurement_DL(point._Name, GrGm);

            // Polar
            if (grid.Columns[e.ColumnIndex].HeaderText.Contains("Polar") && !cellEmpty)
                if (treeGridViewResultBOC.FlattenCategories[e.RowIndex] is GlobalAlignmentResults gar)
                    if (gar.Tag is E.Point point)
                        DataGridView_Action_Launch_Polar_Measurement(point._Name, GrGm);
        }

        private void TreegridDatagridView_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            // Debug.WriteInConsole("Tree Grid cell mouse enter");

            var grid = sender as DataGridView;

            if (e.RowIndex < 0)
                grid.Cursor = Cursors.Default;
            else
            {
                if (e.ColumnIndex <= 2)
                    grid.Cursor = Cursors.Default;
                else
                {
                    bool cellEmpty = grid[e.ColumnIndex, e.RowIndex].Value == null;
                    if (cellEmpty)
                        grid.Cursor = Cursors.Default;
                    else
                    {
                        if (treeGridViewResultBOC.FlattenCategories[e.RowIndex] is GlobalAlignmentResults gar)
                        {
                            if (gar.Tag is E.Point)
                                grid.Cursor = Cursors.Hand;
                            else

                            {
                                if (gar.Tag is Tilt.Measure)
                                    grid.Cursor = Cursors.Hand;
                                else
                                    grid.Cursor = Cursors.Default;
                            }
                        }
                        else
                            grid.Cursor = Cursors.Default;
                    }
                }
            }
        }

        private void TreegridDatagridView_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            var grid = sender as DataGridView;
            grid.Cursor = Cursors.Default;
        }

        private static void RedrawTreeGridViewResult_DefineCastingWay(Views.TreeGrid treeGrid)
        {
            if (treeGrid.CastToRelevantType == null)
            {
                treeGrid.CastToRelevantType = (selectedFlatenHierarchy) =>
                {
                    // we received the selected Hierarchy from the treeview and need to create the datasource for the gridview
                    List<GlobalAlignmentResults> selectedResults = new List<GlobalAlignmentResults>();
                    foreach (var item in selectedFlatenHierarchy)
                    {
                        selectedResults.Add(item as GlobalAlignmentResults);
                    }
                    return selectedResults;
                };
            }
        }

        private static void AddNewRowInDataGridView(List<List<object>> listRow, E.Point point, List<M.Measure> receivedMeasures, List<E.Point> pointsToBeAligned)
        {
            int nd = (P.Values.GuiPrefs == null) ? 7 : P.Values.GuiPrefs.NumberOfDecimalForDistances;
            int na = (P.Values.GuiPrefs == null) ? 5 : P.Values.GuiPrefs.NumberOfDecimalForAngles;
            List<object> row = new List<object>();
            row.Add(R.Length_Delete_White); // 0 Remove
            row.Add(""); // 1 Point name
            row.Add(""); // 2 Dcum
            row.Add(""); // 3 wire DR
            row.Add(""); // 4 polar DR
            row.Add(""); // 5 Length setting DL
            row.Add(""); // 6 PolarDL 
            row.Add(""); // 7 Level DV
            row.Add(""); // 8 polar DV
            row.Add(""); // 9 Roll
            row.Add(point); // 10 is the point as tag to know what to remove with the remove button.
            listRow.Add(row);
            //Nom du point
            listRow[listRow.Count - 1][1] = point._Name;
            //Dcum
            if (point._Parameters.Cumul != P.Values.na) listRow[listRow.Count - 1][2] = point._Parameters.Cumul.ToString("F3", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            //Ecart tilt
            Tilt.Measure mtlt;
            if ((mtlt = receivedMeasures.Find(x => x is Tilt.Measure && x._PointName == point._Name && x._Point._Origin == point._Origin) as Tilt.Measure) != null)
                if (mtlt._Ecart != na) listRow[listRow.Count - 1][9] = Math.Round(mtlt._Ecart.Value * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            //Ecart vertical nivellement
            M.MeasureOfLevel ml;
            if ((ml = receivedMeasures.Find(x => x is M.MeasureOfLevel && x._PointName == point._Name && x._Point._Origin == point._Origin) as M.MeasureOfLevel) != null)
                if (ml._TheoReading != na && ml._RawLevelReading != na) listRow[listRow.Count - 1][7] = Math.Round((ml._TheoReading - ml._RawLevelReading) * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            //Ecart vertical theodolite
            Polar.Measure mtv;
            if ((mtv = receivedMeasures.FindLast(x => x is Polar.Measure && x._Point._Name == point._Name) as Polar.Measure) != null)
                listRow[listRow.Count - 1][8] = mtv._Point._Coordinates.StationCs.Z.ToString(nd - 3, -1000); // -1000 because we want offset in vertical local and station cs= displacement
            //Ecart Dl mesure polaire
            Polar.Measure mtl;
            if ((mtl = receivedMeasures.FindLast(x => x is Polar.Measure && x._Point._Name == point._Name) as Polar.Measure) != null)
                listRow[listRow.Count - 1][6] = mtl._Point._Coordinates.BeamV.Y.ToString(nd - 3, 1000);
            //Ecart DL length setting
            M.MeasureOfDistance mlength;
            if ((mlength = receivedMeasures.FindLast(x => x is M.MeasureOfDistance && x._PointName == point._Name) as M.MeasureOfDistance) != null)
                listRow[listRow.Count - 1][5] = (mlength.distAvgMove.Value == na) ? "" : Math.Round(-mlength.distAvgMove.Value * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            //Ecart DR wire
            M.MeasureOfOffset mo;
            if ((mo = receivedMeasures.Find(x => x is M.MeasureOfOffset && x._PointName == point._Name && x._Point._Origin == point._Origin) as M.MeasureOfOffset) != null)
                if (mo._AverageDeviation != na) listRow[listRow.Count - 1][3] = Math.Round(mo._AverageDeviation * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            //Ecart DR mesure polaire
            Polar.Measure mtr;
            if ((mtr = receivedMeasures.FindLast(x => x is Polar.Measure && x._Point._Name == point._Name) as Polar.Measure) != null)
                listRow[listRow.Count - 1][4] = mtr._Point._Coordinates.BeamV.X.ToString(nd - 3, 1000);
        }
        /// <summary>
        /// Change le sens de tri des mesures dans le datagridviewResult et le redessine
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewresult_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //if (e.ColumnIndex == 2)
            //{
            //    this.SortDatagridview();
            //}
        }
        ///// <summary>
        ///// Trie le datagridView
        ///// </summary>
        //internal void SortDatagridview()
        //{
        //    if (dataGridViewResult.RowCount > 0)
        //    {
        //        switch (this.sortDirection)
        //        {
        //            case TSU.ENUM.SortDirection.Dcum:
        //                this.sortDirection = TSU.ENUM.SortDirection.InvDcum;
        //                break;
        //            case TSU.ENUM.SortDirection.InvDcum:
        //                this.sortDirection = TSU.ENUM.SortDirection.Dcum;
        //                break;
        //            default:
        //                break;
        //        }
        //        this.Step.Update();
        //    }
        //}

        /// <summary>
        /// Trie les points dans le datagridviewResult par Dcum ou inverse Dcum
        /// </summary>
        /// <param name="pointsToBeAligned"></param>
        private void SortMeasureInDataGridViewResult(List<E.Point> pointsToBeAligned)
        {
            if (dataGridViewResult.RowCount > 0)
            {
                switch (this.sortDirection)
                {
                    case ENUM.SortDirection.Dcum:
                        this.sortDirection = ENUM.SortDirection.InvDcum;
                        break;
                    case ENUM.SortDirection.InvDcum:
                        this.sortDirection = ENUM.SortDirection.Dcum;
                        break;
                    default:
                        break;
                }
                pointsToBeAligned.Sort();
                if (this.sortDirection == ENUM.SortDirection.InvDcum) pointsToBeAligned.Reverse();
                DataGridUtility.SetToDcumSorting(dataGridViewResult, 2);
                this.Step.Update();
            }
        }
        /// <summary>
        /// Permet d'encoder les mesures sur les points à aligner directement dans le datagridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewResult_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView_Actions(e);
        }

        private void DataGridView_Actions(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1 || e.ColumnIndex == -1) { return; }
            else
            {
                if (dataGridViewResult[e.ColumnIndex, e.RowIndex].Value != null)
                {
                    //S'il n'y a rien dans la case cela signifie que le calcul des écarts n'est pas possible
                    if (dataGridViewResult[e.ColumnIndex, e.RowIndex].Value.ToString() != "" && dataGridViewResult[1, e.RowIndex].Value.ToString() != "" && e.ColumnIndex != 2)
                    {
                        Group.Module GrGm = this.Step.GuidedModule.ParentModule as Group.Module;
                        switch (e.ColumnIndex)
                        {
                            case 9: //Colonne tilt
                                {
                                    DataGridView_Action_Roll_measurement(dataGridViewResult[1, e.RowIndex].Value.ToString(), GrGm);
                                }
                                break;
                            case 7: //Colonne Level DV
                                {
                                    DataGridView_Action_Level_Measurement(dataGridViewResult[1, e.RowIndex].Value.ToString(), GrGm);
                                }
                                break;
                            case 8: //Colonne Polar DV
                                {// show measuring in datagrid view
                                    string message = R.StringAlignment_Datagrid_Measuring;
                                    dataGridViewResult[4, e.RowIndex].Value = message;
                                    dataGridViewResult[6, e.RowIndex].Value = message;
                                    dataGridViewResult[8, e.RowIndex].Value = message;
                                    DataGridView_Action_Launch_Polar_Measurement(dataGridViewResult[1, e.RowIndex].Value.ToString(), GrGm);
                                    return; // not to update before the acquisition is done;
                                }
                            case 6: //Colonne Polar DL
                                {
                                    string message = R.StringAlignment_Datagrid_Measuring;
                                    dataGridViewResult[4, e.RowIndex].Value = message;
                                    dataGridViewResult[6, e.RowIndex].Value = message;
                                    dataGridViewResult[8, e.RowIndex].Value = message;
                                    DataGridView_Action_Launch_Polar_Measurement(dataGridViewResult[1, e.RowIndex].Value.ToString(), GrGm);
                                    return; // not to update before the acquisition is done;
                                }
                            case 5: //Colonne Length Setting DL
                                {
                                    string message = R.StringAlignment_Datagrid_Measuring;
                                    dataGridViewResult[5, e.RowIndex].Value = message;
                                    DataGridView_Action_Launch_Length_Measurement_DL(dataGridViewResult[1, e.RowIndex].Value.ToString(), GrGm);
                                    return; // not to update before the acquisition is done;
                                }
                            case 4: //Colonne Polar DR
                                {
                                    // show measuring in datagrid view
                                    string message = R.StringAlignment_Datagrid_Measuring;
                                    dataGridViewResult[4, e.RowIndex].Value = message;
                                    dataGridViewResult[6, e.RowIndex].Value = message;
                                    dataGridViewResult[8, e.RowIndex].Value = message;
                                    DataGridView_Action_Launch_Polar_Measurement(dataGridViewResult[1, e.RowIndex].Value.ToString(), GrGm);
                                    return; // not to update before the acquisition is done;
                                }
                            case 3: //Colonne Wire DR
                                {
                                    DataGridView_Action_Launch_Wire_Measurement_DR(dataGridViewResult[1, e.RowIndex].Value.ToString(), GrGm);
                                }
                                break;
                            default:
                                return;
                        }
                        this.Step.Update();
                    }
                }
            }
        }

        private static void DataGridView_Action_Launch_Wire_Measurement_DR(string pointName, Group.Module GrGm)
        {
            Line.Station.Module stationLineModule = null;
            foreach (Module gm in GrGm.SubGuidedModules)
            {
                if (gm._ActiveStationModule is Line.Station.Module lsm)
                {
                    stationLineModule = lsm;
                    ///Force à retourner dans le step mesure du fil
                    if (gm.currentIndex != 2)
                    {
                        gm.currentIndex = 2;
                        gm.CurrentStep.View.MustBeUpdated = true;
                        gm.CurrentStep.Enter();
                    }
                }
            }
            if (stationLineModule != null)
            {
                stationLineModule.View.EnterWireValue(pointName);
            }
        }

        private static void DataGridView_Action_Launch_Length_Measurement_DL(string pointName, Group.Module GrGm)
        {
            // show measuring in datagrid view
            
            Length.Station.Module stationLengthModule = null;
            foreach (Module gm in GrGm.SubGuidedModules)
            {
                if (gm._ActiveStationModule is Length.Station.Module lsm)
                {
                    stationLengthModule = lsm;
                    ///Force à retourner dans le step mesure pour encoder dans la station aller
                    if (gm.currentIndex != 3)
                    {
                        gm.currentIndex = 3;
                        gm.CurrentStep.View.MustBeUpdated = true;
                        gm.CurrentStep.Enter();
                    }
                }
            }
            if (stationLengthModule != null)
            {
                int index = stationLengthModule._LengthStation._MeasureOfTheodolite.FindIndex(x => x._PointName == pointName);
                if (index != -1)
                {
                    stationLengthModule.FinalModule.SetInstrumentInActiveStationModule();
                    stationLengthModule.LaunchMeasure(stationLengthModule._LengthStation._MeasureOfTheodolite[index]);
                }
            }
        }

        private void DataGridView_Action_Launch_Polar_Measurement(string pointName, Group.Module GrGm)
        {
            Polar.Station.Module stationTheodoliteModule = null;
            foreach (Module gm in GrGm.SubGuidedModules)
            {
                if (gm._ActiveStationModule is Polar.Station.Module psm)
                {
                    stationTheodoliteModule = psm;
                }
            }
            if (stationTheodoliteModule != null)
            {
                stationTheodoliteModule.FinalModule.SetInstrumentInActiveStationModule();
                LaunchPolarMeasure(GrGm, pointName);
            }
        }



        private static void DataGridView_Action_Roll_measurement(string pointName, Group.Module GrGm)
        {
            Tilt.Station.Module stationTiltModule = null;
            Module gmTilt = new Module();
            foreach (Module gm in GrGm.SubGuidedModules)
            {
                if (gm._ActiveStationModule is Tilt.Station.Module tsm)
                {
                    stationTiltModule = tsm;
                    gmTilt = gm;
                    ///Force à retourner dans le step mesure du tilt
                    if (gm.currentIndex != 1)
                    {
                        gm.currentIndex = 1;
                        gm.CurrentStep.View.MustBeUpdated = true;
                        gm.CurrentStep.Enter();
                    }
                }
            }
            if (stationTiltModule != null)
            {
                //Met la station tilt active comme le nom du point
                stationTiltModule.View.SearchTilt(pointName);
                stationTiltModule.View.EnterTiltInDatagridPopUp();
                gmTilt.CurrentStep.Update();
            }
        }


        private static void DataGridView_Action_Level_Measurement(string pointName, Group.Module GrGm)
        {
            Level.Station.Module stationLevelModule = null;
            foreach (Module gm in GrGm.SubGuidedModules)
            {
                if (gm._ActiveStationModule is Level.Station.Module && gm.guideModuleType == ENUM.GuidedModuleType.AlignmentLevelling)
                {
                    stationLevelModule = gm._ActiveStationModule as Level.Station.Module;
                    ///Force à retourner dans le step mesure aller pour encoder dans la station aller
                    if (gm.currentIndex != 2)
                    {
                        gm.currentIndex = 2;
                        gm.CurrentStep.View.MustBeUpdated = true;
                        gm.CurrentStep.Enter();
                    }
                }
            }
            if (stationLevelModule != null)
            {
                ///Dans le cas d'un niveau non manuel, force à retourner dans le module nivellement pour les mesures (ex DNA03)
                if (stationLevelModule._WorkingStationLevel._Parameters._Instrument != null && stationLevelModule._InstrumentManager.SelectedInstrumentModule == null)
                {
                    var stp = stationLevelModule._WorkingStationLevel._Parameters;

                    I.Instrument oldInstrument = stationLevelModule._InstrumentManager.AllElements.OfType<I.Instrument>().FirstOrDefault(
                               inst => inst._SerialNumber == stp._Instrument._SerialNumber
                               && (inst._Model == stp._Instrument._Model));


                    if (oldInstrument != null)
                    {
                        stationLevelModule._InstrumentManager.SetInstrument(oldInstrument as I.Sensor);
                    }
                }
                TsuView v = stationLevelModule._InstrumentManager.SelectedInstrumentModule._TsuView;
                if (v is I.Device.Manual.Level.View)
                {
                    stationLevelModule.View.EnterLevelValue(pointName);
                }
                if (v is I.Device.DNA03.View)
                {
                    stationLevelModule.View.LaunchAutomaticMeasure(pointName);
                }
            }
        }

        private void LaunchPolarMeasure(Group.Module grGm, string name)
        {
            Polar.Station.Module stationModule = null;
            foreach (Module gm in grGm.SubGuidedModules)
            {
                if (gm._ActiveStationModule is Polar.Station.Module psm)
                {
                    stationModule = psm;
                    Polar.Measure m;
                    m = GetLastMeasureByPointName(stationModule.StationTheodolite.MeasuresToDo, name);
                    if (m == null) m = GetLastMeasureByPointName(gm.ReceivedMeasures, name);
                    if (m != null)
                    {
                        M.MeasurementEventHandler actionAtTheEndOftheMeasure = null;
                        actionAtTheEndOftheMeasure = delegate
                        {
                            stationModule._InstrumentManager.SelectedInstrumentModule.FinishingMeasurement -= actionAtTheEndOftheMeasure;
                            this.Step.Update();
                        };
                        stationModule._InstrumentManager.SelectedInstrumentModule.FinishingMeasurement += actionAtTheEndOftheMeasure;

                        M.MeasurementEventHandler actionIfMesaureFailed = null;
                        actionIfMesaureFailed = delegate (object sender, M.MeasurementEventArgs e)
                        {
                            stationModule._InstrumentManager.SelectedInstrumentModule.MeasurementFailed -= actionIfMesaureFailed;
                            string message = e.InstrumentModule.LastMessage;
                            //   if (message != "") this.Step.View.ShowMessageOfCritical(message + "\r\n" + "You may find more information using the polar module");
                            if (message != "")
                            {
                                string titleAndMessage = message + "\r\n" + R.T_YOU_MAY_FIND_MORE_INFORMATION_USING_THE_POLAR_MODULE;
                                new MessageInput(MessageType.Critical, titleAndMessage).Show();
                            }
                        };
                        stationModule._InstrumentManager.SelectedInstrumentModule.MeasurementFailed += actionIfMesaureFailed;

                        stationModule.SendMeasureToInstrument(m, new M.States.Questionnable(M.States.Types.Questionnable));
                        stationModule.AskIntrumentToMeasure();
                        //stationModule.View.ShowInstrumentModuleAsDialog(true);
                    }
                }
            }
        }

        private static Polar.Measure GetLastMeasureByPointName(IEnumerable<M.Measure> list, string name)
        {
            return list.LastOrDefault(x => x._Point._Name == name) as Polar.Measure;
        }

        #endregion
        #region datagridViewMoveLevel
        internal DataGridView dataGridViewMoveLevel;
        private DataGridViewTextBoxColumn PointNameMoveLevel;
        private DataGridViewTextBoxColumn DcumMoveLevel;
        private DataGridViewTextBoxColumn InitialMoveMoveLevel;
        private DataGridViewTextBoxColumn InitialTheoReadingMoveLevel;
        private DataGridViewTextBoxColumn ActualTheoReadingMoveLevel;
        private DataGridViewTextBoxColumn ActualMoveMoveLevel;
        private DataGridViewTextBoxColumn ActualReadingMoveLevel;
        private DataGridViewTextBoxColumn ActualExtensionMoveLevel;
        private DataGridViewTextBoxColumn InitialExtensionMoveLevel;
        DataGridViewCellStyle dataGridViewCellStyleMoveLevelTitle;
        DataGridViewCellStyle dataGridViewCellStyleMoveLevelRow;
        DataGridViewCellStyle dataGridViewCellStyleEditableMoveLevelRow;
        DataGridViewCellStyle dataGridViewCellStyleMoveLevelPointName;
        public void CreateDatagridViewMoveLevel()
        {
            this.dataGridViewMoveLevel = new DataGridView();
            this.PointNameMoveLevel = new DataGridViewTextBoxColumn();
            this.DcumMoveLevel = new DataGridViewTextBoxColumn();
            this.InitialMoveMoveLevel = new DataGridViewTextBoxColumn();
            this.ActualTheoReadingMoveLevel = new DataGridViewTextBoxColumn();
            this.ActualMoveMoveLevel = new DataGridViewTextBoxColumn();
            this.ActualReadingMoveLevel = new DataGridViewTextBoxColumn();
            this.ActualExtensionMoveLevel = new DataGridViewTextBoxColumn();
            this.InitialExtensionMoveLevel = new DataGridViewTextBoxColumn();
            this.InitialTheoReadingMoveLevel = new DataGridViewTextBoxColumn();
            this.dataGridViewCellStyleMoveLevelTitle = new DataGridViewCellStyle();
            this.dataGridViewCellStyleMoveLevelRow = new DataGridViewCellStyle();
            this.dataGridViewCellStyleEditableMoveLevelRow = new DataGridViewCellStyle();
            this.dataGridViewCellStyleMoveLevelPointName = new DataGridViewCellStyle();
        }
        public void InitializeDatagridViewMoveLevel()
        {
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMoveLevel)).BeginInit();
            // 
            // dataGridViewMoveLevel
            // 
            this.dataGridViewMoveLevel.AllowUserToAddRows = false;
            this.dataGridViewMoveLevel.AllowUserToDeleteRows = false;
            this.dataGridViewMoveLevel.AllowUserToResizeColumns = true;
            this.dataGridViewMoveLevel.AllowUserToResizeRows = false;
            this.dataGridViewMoveLevel.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewMoveLevel.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyleMoveLevelTitle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyleMoveLevelTitle.BackColor = P.Theme.Colors.Object;
            dataGridViewCellStyleMoveLevelTitle.Font = new Font("Calibri", 15F, FontStyle.Italic);
            dataGridViewCellStyleMoveLevelTitle.ForeColor = P.Theme.Colors.NormalFore;
            dataGridViewCellStyleMoveLevelTitle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            dataGridViewCellStyleMoveLevelTitle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            dataGridViewCellStyleMoveLevelTitle.WrapMode = DataGridViewTriState.True;
            this.dataGridViewMoveLevel.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.dataGridViewMoveLevel.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.dataGridViewMoveLevel.DefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            this.dataGridViewMoveLevel.DefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.dataGridViewMoveLevel.ColumnHeadersDefaultCellStyle = dataGridViewCellStyleMoveLevelTitle;
            this.dataGridViewMoveLevel.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMoveLevel.Columns.AddRange(
            this.PointNameMoveLevel, //0
            this.DcumMoveLevel, //1
            this.InitialTheoReadingMoveLevel, //2
            this.InitialMoveMoveLevel, //3
            this.InitialExtensionMoveLevel, //4
            this.ActualExtensionMoveLevel, //5
            this.ActualReadingMoveLevel, //6
            this.ActualTheoReadingMoveLevel, //7
            this.ActualMoveMoveLevel); //8
            this.dataGridViewMoveLevel.DefaultCellStyle = dataGridViewCellStyleMoveLevelTitle;
            this.dataGridViewMoveLevel.Dock = DockStyle.Fill;
            this.dataGridViewMoveLevel.EnableHeadersVisualStyles = false;
            this.dataGridViewMoveLevel.Location = new Point(3, 103);
            this.dataGridViewMoveLevel.MultiSelect = false;
            this.dataGridViewMoveLevel.Name = "dataGridViewMoveLevel";
            this.dataGridViewMoveLevel.ReadOnly = false;
            this.dataGridViewMoveLevel.RowHeadersDefaultCellStyle = dataGridViewCellStyleMoveLevelTitle;
            this.dataGridViewMoveLevel.RowHeadersVisible = false;
            this.dataGridViewMoveLevel.Size = new Size(511, 480);
            this.dataGridViewMoveLevel.TabIndex = 0;
            dataGridViewCellStyleMoveLevelRow.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyleMoveLevelRow.BackColor = P.Theme.Colors.LightBackground;
            dataGridViewCellStyleMoveLevelRow.Font = new Font("Calibri", 15F, FontStyle.Italic);
            dataGridViewCellStyleMoveLevelRow.ForeColor = P.Theme.Colors.NormalFore;
            dataGridViewCellStyleMoveLevelRow.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            dataGridViewCellStyleMoveLevelRow.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            dataGridViewCellStyleMoveLevelRow.WrapMode = DataGridViewTriState.False;
            dataGridViewCellStyleMoveLevelPointName.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyleMoveLevelPointName.BackColor = P.Theme.Colors.LightBackground;
            dataGridViewCellStyleMoveLevelPointName.Font = new Font("Calibri", 15F, FontStyle.Italic);
            dataGridViewCellStyleMoveLevelPointName.ForeColor = P.Theme.Colors.NormalFore;
            dataGridViewCellStyleMoveLevelPointName.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            dataGridViewCellStyleMoveLevelPointName.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            dataGridViewCellStyleMoveLevelPointName.WrapMode = DataGridViewTriState.False;
            this.dataGridViewCellStyleEditableMoveLevelRow.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewCellStyleEditableMoveLevelRow.BackColor = P.Theme.Colors.Action;
            this.dataGridViewCellStyleEditableMoveLevelRow.Font = new Font("Calibri", 15F, FontStyle.Italic);
            this.dataGridViewCellStyleEditableMoveLevelRow.ForeColor = P.Theme.Colors.NormalFore;
            this.dataGridViewCellStyleEditableMoveLevelRow.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            this.dataGridViewCellStyleEditableMoveLevelRow.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.dataGridViewCellStyleEditableMoveLevelRow.WrapMode = DataGridViewTriState.False;
            // 
            // Point Name Move Level 0
            // 
            this.PointNameMoveLevel.DefaultCellStyle = dataGridViewCellStyleMoveLevelPointName;
            this.PointNameMoveLevel.HeaderText = R.StringDataGridLevel_Name;
            //this.PointNameMoveLevel.Width = 200;
            this.PointNameMoveLevel.Name = "PointName";
            this.PointNameMoveLevel.ReadOnly = true;
            this.PointNameMoveLevel.Resizable = DataGridViewTriState.True;
            this.PointNameMoveLevel.SortMode = DataGridViewColumnSortMode.Automatic;
            // 
            // Dcum Move Level 1
            // 
            this.DcumMoveLevel.DefaultCellStyle = dataGridViewCellStyleMoveLevelRow;
            this.DcumMoveLevel.SortMode = DataGridViewColumnSortMode.Automatic;
            this.DcumMoveLevel.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.DcumMoveLevel.HeaderText = R.StringDataGridLevel_Dcum;
            this.DcumMoveLevel.Name = "Dcum";
            //this.DcumMoveLevel.Width = 100;
            this.DcumMoveLevel.ReadOnly = true;
            // 
            // Initial TheoReading 2
            // 
            this.InitialTheoReadingMoveLevel.DefaultCellStyle = dataGridViewCellStyleMoveLevelRow;
            this.InitialTheoReadingMoveLevel.HeaderText = R.StringDataGridLevel_InitialTheo;
            this.InitialTheoReadingMoveLevel.Name = "InitialTheoReading";
            this.InitialTheoReadingMoveLevel.ReadOnly = true;
            this.InitialTheoReadingMoveLevel.Visible = false;
            this.InitialTheoReadingMoveLevel.Resizable = DataGridViewTriState.True;
            this.InitialTheoReadingMoveLevel.SortMode = DataGridViewColumnSortMode.Automatic;
            // 
            // Initial Move 3
            // 
            this.InitialMoveMoveLevel.DefaultCellStyle = dataGridViewCellStyleMoveLevelRow;
            this.InitialMoveMoveLevel.HeaderText = R.StringDataGridLevel_InitialMove;
            this.InitialMoveMoveLevel.Name = "InitialMove";
            this.InitialMoveMoveLevel.ReadOnly = true;
            this.InitialMoveMoveLevel.Visible = false;
            this.InitialMoveMoveLevel.Resizable = DataGridViewTriState.True;
            this.InitialMoveMoveLevel.SortMode = DataGridViewColumnSortMode.Automatic;
            // 
            // Initial Extension 4
            // 
            this.InitialExtensionMoveLevel.DefaultCellStyle = dataGridViewCellStyleMoveLevelRow;
            this.InitialExtensionMoveLevel.HeaderText = R.StringDataGridLevel_InitialExtension;
            this.InitialExtensionMoveLevel.Name = "InitialExtension";
            this.InitialExtensionMoveLevel.ReadOnly = true;
            this.InitialExtensionMoveLevel.Visible = false;
            this.InitialExtensionMoveLevel.Resizable = DataGridViewTriState.True;
            this.InitialExtensionMoveLevel.SortMode = DataGridViewColumnSortMode.Automatic;
            // 
            // Actual Extension 5
            // 
            this.ActualExtensionMoveLevel.DefaultCellStyle = dataGridViewCellStyleEditableMoveLevelRow;
            this.ActualExtensionMoveLevel.HeaderText = R.StringDataGridLevel_ActualExtension;
            this.ActualExtensionMoveLevel.Name = "ActualExtension";
            this.ActualExtensionMoveLevel.ReadOnly = false;
            this.ActualExtensionMoveLevel.Resizable = DataGridViewTriState.True;
            this.ActualExtensionMoveLevel.SortMode = DataGridViewColumnSortMode.Automatic;
            // 
            // Actual Reading 6
            // 
            this.ActualReadingMoveLevel.DefaultCellStyle = dataGridViewCellStyleEditableMoveLevelRow;
            this.ActualReadingMoveLevel.HeaderText = R.StringDataGridLevel_ActualReading;
            this.ActualReadingMoveLevel.Name = "ActualReading";
            this.ActualReadingMoveLevel.ReadOnly = false;
            this.ActualReadingMoveLevel.Resizable = DataGridViewTriState.True;
            this.ActualReadingMoveLevel.SortMode = DataGridViewColumnSortMode.Automatic;
            // 
            // Actual TheoReading 7
            // 
            this.ActualTheoReadingMoveLevel.DefaultCellStyle = dataGridViewCellStyleMoveLevelRow;
            this.ActualTheoReadingMoveLevel.HeaderText = R.StringDataGridLevel_ActualTheo;
            this.ActualTheoReadingMoveLevel.Name = "ActualTheoReading";
            this.ActualTheoReadingMoveLevel.ReadOnly = true;
            this.ActualTheoReadingMoveLevel.Resizable = DataGridViewTriState.True;
            this.ActualTheoReadingMoveLevel.SortMode = DataGridViewColumnSortMode.Automatic;
            // 
            // Actual Move 8
            // 
            this.ActualMoveMoveLevel.DefaultCellStyle = dataGridViewCellStyleMoveLevelRow;
            this.ActualMoveMoveLevel.HeaderText = R.StringDataGridLevel_ActualMove;
            this.ActualMoveMoveLevel.Name = "ActualMove";
            this.ActualMoveMoveLevel.ReadOnly = true;
            this.ActualMoveMoveLevel.Resizable = DataGridViewTriState.True;
            this.ActualMoveMoveLevel.SortMode = DataGridViewColumnSortMode.Automatic;
            this.dataGridViewMoveLevel.GridColor = P.Theme.Colors.Background;
            this.dataGridViewMoveLevel.CellValueChanged += new DataGridViewCellEventHandler(this.dataGridViewMoveLevel_CellValueChanged);
            this.dataGridViewMoveLevel.ColumnHeaderMouseClick += new DataGridViewCellMouseEventHandler(this.dataGridViewMoveLevel_ColumnHeaderMouseClick);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMoveLevel)).EndInit();
            //this.TLPDatagridViewMoveLevel
        }
        /// <summary>
        /// Recalcule les déplacements et lectures theo actuelles dans le datagridMoveLevel
        /// </summary>
        internal void RedrawDataGridViewMoveLevel()
        {
            for (int i = 0; i < dataGridViewMoveLevel.RowCount; i++)
            {
                double initTheoReading = T.Conversions.Numbers.ToDouble(dataGridViewMoveLevel[2, i].Value.ToString(), true, -9999);
                double initMove = T.Conversions.Numbers.ToDouble(dataGridViewMoveLevel[3, i].Value.ToString(), true, -9999);
                double initExtension = T.Conversions.Numbers.ToDouble(dataGridViewMoveLevel[4, i].Value.ToString(), true, -9999);
                double actualExtension = T.Conversions.Numbers.ToDouble(dataGridViewMoveLevel[5, i].Value.ToString(), true, -9999);
                if (dataGridViewMoveLevel[6, i].Value != null)
                {

                    double reading = T.Conversions.Numbers.ToDouble(dataGridViewMoveLevel[6, i].Value.ToString(), true, -9999);
                    if (initTheoReading != -9999 && actualExtension != -9999 && initExtension != -9999)
                    {
                        double actualTheoReading = initTheoReading - actualExtension * 100 + initExtension * 100;
                        dataGridViewMoveLevel[5, i].Value = Math.Round(actualExtension, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                        dataGridViewMoveLevel[6, i].Value = Math.Round(reading, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                        dataGridViewMoveLevel[7, i].Value = Math.Round(actualTheoReading, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                        dataGridViewMoveLevel[8, i].Value = Math.Round(reading - actualTheoReading, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                    }
                }
                else
                {
                    if (initTheoReading != -9999 && actualExtension != -9999 && initExtension != -9999)
                    {
                        double actualTheoReading = initTheoReading - actualExtension * 100 + initExtension * 100;
                        dataGridViewMoveLevel[5, i].Value = Math.Round(actualExtension, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                        dataGridViewMoveLevel[7, i].Value = Math.Round(actualTheoReading, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                        dataGridViewMoveLevel[8, i].Value = Math.Round(0 - actualTheoReading, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                    }
                }
            }
        }
        private void dataGridViewMoveLevel_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        //evenements liés au changement d'une valeur dans le tableau
        {
            //Encodage actual reading
            if (dataGridViewMoveLevel.Columns[e.ColumnIndex].Name == "ActualReading")
            {
                if (dataGridViewMoveLevel[e.ColumnIndex, e.RowIndex].Value != null)
                {
                    double datagridNumber = T.Conversions.Numbers.ToDouble(dataGridViewMoveLevel[e.ColumnIndex, e.RowIndex].Value.ToString(), true, -9999);
                    if (datagridNumber == -9999)
                    {
                        //message erreur, valeur encodée incorrecte
                        new MessageInput(MessageType.Critical, R.StringLevel_WrongCellValue).Show();
                        dataGridViewMoveLevel[e.ColumnIndex, e.RowIndex].Value = null;
                    }
                }
            }
            ///Encodage actual extension
            if (dataGridViewMoveLevel.Columns[e.ColumnIndex].Name == "ActualExtension")
            {
                if (dataGridViewMoveLevel[e.ColumnIndex, e.RowIndex].Value != null)
                {
                    double datagridNumber = T.Conversions.Numbers.ToDouble(dataGridViewMoveLevel[e.ColumnIndex, e.RowIndex].Value.ToString(), true, -9999);
                    if (datagridNumber == -9999)
                    {
                        //message erreur, valeur encodée incorrecte => remet extension initiale
                        this.dataGridViewMoveLevel[e.ColumnIndex, e.RowIndex].Value = this.dataGridViewMoveLevel[4, e.RowIndex].Value;
                        new MessageInput(MessageType.Critical, R.StringLevel_WrongCellValue).Show();
                    }
                }
                else
                {
                    this.dataGridViewMoveLevel[e.ColumnIndex, e.RowIndex].Value = this.dataGridViewMoveLevel[4, e.RowIndex].Value;
                }
            }
            this.RedrawDataGridViewMoveLevel();
        }
        private void dataGridViewMoveLevel_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //tri par dcum
            if (e.ColumnIndex == 1)
            {
                // To be implemented
            }
        }

        internal bool HighlightButtonColorByButtonName(string name, Color good)
        {
            if (GetControlByName(name, this.Controls, out Control control))
            {
                if (control is BigButton bb)
                {
                    bb.HighLightWithColor(good);
                    return true;
                }
            }
            return false;
        }

        internal bool RestoreButtonColorByButtonName(string name)
        {
            if (GetControlByName(name, this.Controls, out Control control))
            {
                if (control is BigButton bb)
                {
                    bb.RestoreColor();
                    return true;
                }
            }
            return false;
        }
        #endregion
    }
}
