﻿using System;
using System.Collections.Generic;
using System.Text;
using TSU.Common;
using TSU.Common.Compute.Compensations.BeamOffsets;
using TSU.Common.Elements;
using TSU.Common.Measures;
using TSU.Polar.GuidedModules.Steps;
using TSU.Views;
using TSU.Tools;
using System.Xml;
using System.Windows.Forms;
using TSU.Preferences.GUI;

namespace TSU.Measurement.Common.Guided.Step
{
    public class GlobalAlignmentResults : BaseTreeGridCategory, ITreeGridCategory
    {

        public override string Categories { get; set; }
        public override string Name { get; set; }

        public class ResultValue
        {
            public ResultValue(string value, DataGridViewCellStyle style = null)
            {
                this.Value = value;
                this.Style = style;
            }

            public ResultValue(List<Results> bOC_Results, string beamPointName, ObservationType observationType, string csName, string coordinatesPropertyName)
            {
                var coordinates = bOC_Results.GetOffset(beamPointName, observationType, csName, out bool obsolete);
                if (coordinates == null)
                {
                    this.Value = "-";
                    return;
                }

                // get value to show
                DoubleValue value = coordinates.GetType().GetProperty(coordinatesPropertyName).GetValue(coordinates) as DoubleValue; // this will return one of the coordinate X; Y or Z.
                this.Value = value.ToString("m-mm");
                this.SetStyle(obsolete);
            }

            public string Value { get; set; }
            public DataGridViewCellStyle Style { get; set; }
            public override string ToString()
            {
                return Value;
            }


            internal void SetStyle(bool obsolete)
            {
                // choose style of the cell
                var theme = Tsunami2.Preferences.Theme.Colors;
                this.Style = new DataGridViewCellStyle();
                this.Style.BackColor = theme.Background;
                if (obsolete)
                {
                    this.Style.ForeColor = theme.Attention;
                }
                else
                {
                    this.Style.ForeColor = theme.BestForeBasedOnBackground(theme.Background);
                }
            }
        }

        public string Dcum_M_ { get; set; }
        public ResultValue Wire_DR_MM_ { get; set; }
        public ResultValue Polar_DR_MM_ { get; set; }
        public ResultValue Dist_DL_MM_ { get; set; }
        public ResultValue Polar_DL_MM_ { get; set; }
        public ResultValue Polar_DV_MM_ { get; set; }
        public ResultValue Level_DV_MM_ { get; set; }
        public ResultValue dRoll_MRAD_ { get; set; }

        public string Time { get; set; }

        public GlobalAlignmentResults()
        {
        }
        /// <summary>
        /// constructor for beam offset computed points
        /// </summary>
        /// <param name="bOC_Results"></param>
        /// <param name="beamPointName"></param>
        /// <param name="csName"></param>
        public GlobalAlignmentResults(List<Results> bOC_Results, string beamPointName, string csName)
        {
            this.Wire_DR_MM_ = new ResultValue(bOC_Results, beamPointName, ObservationType.Ecarto, csName, "X");
            this.Polar_DR_MM_ = new ResultValue(bOC_Results, beamPointName, ObservationType.Polar, csName, "X");
            this.Dist_DL_MM_ = new ResultValue(bOC_Results, beamPointName, ObservationType.Length, csName, "Y");
            this.Polar_DL_MM_ = new ResultValue(bOC_Results, beamPointName, ObservationType.Polar, csName, "Y");
            this.Level_DV_MM_ = new ResultValue(bOC_Results, beamPointName, ObservationType.Level, csName, "Z");
            this.Polar_DV_MM_ = new ResultValue(bOC_Results, beamPointName, ObservationType.Polar, csName, "Z");
            this.dRoll_MRAD_ = new ResultValue("-");
            this.Time = "-";
        }

        public GlobalAlignmentResults(Results rollResults)
        {
            double dRoll_mrad = rollResults.dRoll.Value;
            double dRoll_cc = rollResults.dRoll.Value/1000/Math.PI*200*10000;
            this.Categories = "Comp.";
            this.Obsolete = rollResults.Obsolete;
            this.Dcum_M_ = "-";
            this.Wire_DR_MM_ = new ResultValue("-");
            this.Polar_DR_MM_ = new ResultValue("-");
            this.Dist_DL_MM_ = new ResultValue("-");
            this.Polar_DL_MM_ = new ResultValue("-");
            this.Level_DV_MM_ = new ResultValue("-");
            this.Polar_DV_MM_ = new ResultValue("-");
            this.dRoll_MRAD_ = new ResultValue(dRoll_mrad.ToString("F3") + "(" + dRoll_cc.ToString("F1") + "cc)");
            this.dRoll_MRAD_.SetStyle(rollResults.Obsolete);
            this.Time = rollResults.Date.ToShortTimeString();
        }

        public GlobalAlignmentResults(Tilt.Measure tiltMeasure)
        {
            this.Name = "Measured Roll";
            this.Categories = "Meas.";
            this.Dcum_M_ = "-";
            this.Wire_DR_MM_ = new ResultValue("-");
            this.Polar_DR_MM_ = new ResultValue("-");
            this.Dist_DL_MM_ = new ResultValue("-");
            this.Polar_DL_MM_ = new ResultValue("-");
            this.Level_DV_MM_ = new ResultValue("-");
            this.Polar_DV_MM_ = new ResultValue("-");
            this.Tag = tiltMeasure;
            this.Time = tiltMeasure._Date.ToShortTimeString();
            this.dRoll_MRAD_ = new ResultValue(Math.Round(tiltMeasure._Ecart.Value * 1000, 2).ToString("F3", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
        }

        public GlobalAlignmentResults((string, double) magnetNameAndCumul)
        {
            var magnetName = magnetNameAndCumul.Item1 as string;
            var cumul = magnetNameAndCumul.Item2;
            this.Name = magnetName;
            this.Categories = Point.NameManipulation.RemoveZone(magnetName);
            this.Dcum_M_ = cumul.ToString("F2");
            this.Wire_DR_MM_ = new ResultValue("-");
            this.Polar_DR_MM_ = new ResultValue("-");
            this.Dist_DL_MM_ = new ResultValue("-");
            this.Polar_DL_MM_ = new ResultValue("-");
            this.Level_DV_MM_ = new ResultValue("-");
            this.Polar_DV_MM_ = new ResultValue("-");
            this.dRoll_MRAD_ = new ResultValue("-");
            this.Time = "-";
        }

       


        /// <summary>
        /// should return something like:
        /// MANGET NAME
        ///     SOCKET
        ///         .E
        ///         .S
        ///         OTHERS
        ///     BEAM 
        ///         .E
        ///         .S
        ///         OTHERS
        /// </summary>
        /// <param name="pointsToBeAligned"></param>
        /// <param name="receivedMeasures"></param>
        /// <param name="bOC_Results"></param>
        /// <returns></returns>
        internal static ITreeGridCategory Categorize(List<Point> pointsToBeAligned, List<Measure> receivedMeasures, List<Results> bOC_Results)
        {
            ITreeGridCategory categorizedResults = new CategorizedResult() { Name = "Categories" };

            // Get Magnets
            var magnets = pointsToBeAligned.GetMagnetsNamesAndCumul();

            // Create and fill Magnets categories
            foreach (var magnetNameAndCumul in magnets)
            {
                var magnetName = magnetNameAndCumul.Item1;

                // Add Magnet
                GlobalAlignmentResults magnetCategory = Categorize_AddMagnet(bOC_Results, categorizedResults, magnetNameAndCumul);

                // Add dRolls

                var rolls = Categorize_AddDeltaRolls(bOC_Results, receivedMeasures, magnetName);
                magnetCategory.Add(rolls, skipIfEmpty: false); // false because if only measured rol avaible this is direct node with the tilt and not a folder

                // Add Socket
                var sockets = Categorize_AddMagnetSockets(pointsToBeAligned, receivedMeasures, magnetName, bOC_Results);
                magnetCategory.Add(sockets, skipIfEmpty: true);

                // Add Beam points
                var bps = Categorize_AddBeamPoints(pointsToBeAligned, bOC_Results, magnetName);
                magnetCategory.Add(bps, skipIfEmpty: true);
            }
            return categorizedResults;
        }

        public override string ToString()
        {
            return $"{Name} {Categories}";
        }

        private void Add(GlobalAlignmentResults results, bool skipIfEmpty = false)
        {
            if (results == null)
                return;
            if (skipIfEmpty && results.SubCategories.Count == 0)
                return;
            base.Add(results);
        }

        private static GlobalAlignmentResults Categorize_AddMagnet(List<Results> bOC_Results, ITreeGridCategory categorizedResults, (string, double) magnetNameAndCumul)
        {
            GlobalAlignmentResults magnetCategory = new GlobalAlignmentResults(magnetNameAndCumul);

            categorizedResults.Add(magnetCategory);

            return magnetCategory;
        }

        private static GlobalAlignmentResults Categorize_AddBeamPoints(List<Point> pointsToBeAligned, List<Results> bOC_Results, string magnetName)
        {
            GlobalAlignmentResults beamPointsCategory = new GlobalAlignmentResults() { Name = "", Categories = "Beam Points", Highlighted = true };
            GlobalAlignmentResults otherBeamPointsCategory = new GlobalAlignmentResults() { Name = "", Categories = "Others", Highlighted = true };

            var beamPoints = bOC_Results.GetBeamPointsBasedOn(new List<string>() { magnetName });
            foreach (var items in beamPoints)
            {
                var assemblyId = items.Item1;
                var beamPoint = items.Item3;
                var computeDate = items.Item4;
                var beamPointName = beamPoint._Name;
                var socketName = Point.GetPointAkaSocketName(beamPoint._Name);

                var r = new GlobalAlignmentResults(bOC_Results, beamPointName, "BEAM")
                {
                    Name = beamPoint._Name,
                    Categories = socketName,
                    Dcum_M_ = "-",
                    Highlighted = true,
                    Time = computeDate.ToShortTimeString(),
                };

                // in BEAM POINTS
                if ((socketName == "E" || socketName == "S") && beamPointName.Contains(magnetName))
                    beamPointsCategory.Add(r);
                else
                    // in OTHER BEAM POINTS
                    otherBeamPointsCategory.Add(r);
            }

            if (otherBeamPointsCategory.SubCategories.Count > 0)
                beamPointsCategory.Add(otherBeamPointsCategory);
            return beamPointsCategory;
        }


        private static GlobalAlignmentResults Categorize_AddDeltaRolls(List<Results> bOC_Results, List<Measure> receivedMeasures, string magnetName)
        {
            // level computed Roll
            GlobalAlignmentResults levelComputedDeltaRoll = null;
            bool hasLevelComputedDeltaRoll = false;
            var lastLevel = bOC_Results.GetLastByObservationType(magnetName, ObservationType.Level);
            if (lastLevel != null)
            {
                hasLevelComputedDeltaRoll = true;
                levelComputedDeltaRoll = new GlobalAlignmentResults(lastLevel) { Name = "Level based" };
            }

            // polar computed roll
            GlobalAlignmentResults polarComputedDeltaRoll = null;
            bool hasPolarComputedDeltaRoll = false;
            var lastPolar = bOC_Results.GetLastByObservationType(magnetName, ObservationType.Polar);
            if (lastPolar != null)
            {
                hasPolarComputedDeltaRoll = true;
                polarComputedDeltaRoll = new GlobalAlignmentResults(lastPolar) { Name = "\u251cPolar based" };
            }

            // Measured Roll
            bool hasMeasuredDeltaRoll = false;
            GlobalAlignmentResults measuredDeltaRollResult = null;
            var lastTiltMeasure = receivedMeasures.FindLastByAssemblyNameAndType(magnetName, typeof(Tilt.Measure));
            if (lastTiltMeasure != null && lastTiltMeasure is Tilt.Measure tm)
            {
                measuredDeltaRollResult = new GlobalAlignmentResults(tm);
                hasMeasuredDeltaRoll = true;

            }

            if (hasPolarComputedDeltaRoll || hasLevelComputedDeltaRoll)
            {
                GlobalAlignmentResults dRollsCategory = new GlobalAlignmentResults() { Name = "", Categories = "Roll" };
                if (hasMeasuredDeltaRoll)
                    dRollsCategory.Add(measuredDeltaRollResult);
                if (hasPolarComputedDeltaRoll)
                    dRollsCategory.Add(polarComputedDeltaRoll);
                if (hasLevelComputedDeltaRoll)
                    dRollsCategory.Add(levelComputedDeltaRoll);
                return dRollsCategory;
            }
            else
                return measuredDeltaRollResult;
        }

        private static GlobalAlignmentResults Categorize_AddMagnetSockets(List<Point> pointsToBeAligned, List<Measure> receivedMeasures, string magnetName, List<Results> bOC_Results)
        {
            GlobalAlignmentResults socketsCategory = new GlobalAlignmentResults() { Name = "", Categories = "Sockets" };
            GlobalAlignmentResults otherSocketsCategory = new GlobalAlignmentResults() { Name = "", Categories = "Others" };

            // this is only sockets not removed by the user
            var sockets = pointsToBeAligned.GetByAssembly(magnetName);
            // check if there is other in the frame may be using the non beam point from the frame is better?
            var socketsInFrame = bOC_Results.GetSocketsFromLastResultByAssemblyId(magnetName);
            socketsInFrame.RemoveAllByName(sockets);

            foreach (var socket in sockets)
            {
                GlobalAlignmentResults socketResult = Categorize_CreateFromMeasures(socket, receivedMeasures);

                // in SOCKETS
                var socketType = socket._Point;
                if ((socketType == "E" || socketType == "S"))
                    socketsCategory.Add(socketResult);
                else
                {
                    // in OTHER SOCKETS
                    otherSocketsCategory.Add(socketResult);
                }
            }

            // Computed Sockets
            foreach (var socket in socketsInFrame)
            {
                GlobalAlignmentResults socketResult = Categorize_CreateFromMeasures(socket, receivedMeasures);
                socketResult.Highlighted = true; // computed values are in different color
                // in SOCKETS
                var socketType = socket._Point;
                if ((socketType == "E" || socketType == "S"))
                    socketsCategory.Add(socketResult);
                else
                {
                    // in OTHER SOCKETS
                    otherSocketsCategory.Add(socketResult);
                }
            }

            if (otherSocketsCategory.SubCategories.Count > 0)
                socketsCategory.Add(otherSocketsCategory);

            return socketsCategory;
        }

        private static GlobalAlignmentResults Categorize_CreateFromMeasures(Point socketPoint, List<Measure> receivedMeasures)
        {
            var name = socketPoint._Name;
            var r = new GlobalAlignmentResults()
            {
                Name = name,
                Categories = socketPoint._Point,
                Dcum_M_ = socketPoint._Parameters.Cumul.ToString("F2"),
                Tag = socketPoint,
                dRoll_MRAD_ = new ResultValue("-"),
            };

            DateTime olderMeasureDate = DateTime.Now;
            var polarMeasure = receivedMeasures.FindLastByPointNameAndType(name, typeof(Polar.Measure));
            if (polarMeasure != null && polarMeasure is Polar.Measure pm)
            {
                var c = pm.Offsets?.ListOfAllCoordinates?.GetByFrameName("BEAM") ;
                if (c ==null)
                {
                    c = new Coordinates();
                    c.Set();
                }
                r.Polar_DR_MM_ = new ResultValue(c.X.ToString("m-mm"));
                r.Polar_DL_MM_ = new ResultValue(c.Y.ToString("m-mm"));
                r.Polar_DV_MM_ = new ResultValue(c.Z.ToString("m-mm"));
                olderMeasureDate = olderMeasureDate.ReplaceIfNewerThan(pm._Date); // keep the oldest
            }

            var levelMeasure = receivedMeasures.FindLastByPointNameAndType(name, typeof(MeasureOfLevel));
            if (levelMeasure != null && levelMeasure is MeasureOfLevel lm)
            {
                var value = Math.Round((lm._TheoReading - lm._RawLevelReading) * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                r.Level_DV_MM_ = new ResultValue(value);
                olderMeasureDate = olderMeasureDate.ReplaceIfNewerThan(lm._Date); // keep the oldest
            }

            var offsetMeasure = receivedMeasures.FindLastByPointNameAndType(name, typeof(MeasureOfOffset));
            if (offsetMeasure != null && offsetMeasure is MeasureOfOffset om)
            {
                r.Wire_DR_MM_ = new ResultValue(Math.Round(om._AverageDeviation * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                olderMeasureDate = olderMeasureDate.ReplaceIfNewerThan(om._Date); // keep the oldest
            }

            //var tiltMeasure = receivedMeasures.FindLastByPointNameAndType(name, typeof(Tilt.Measure));
            //if (tiltMeasure != null && tiltMeasure is Tilt.Measure tm)
            //{
            //    r.dRoll_MRAD_ = Math.Round(tm._Ecart.Value * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            //}

            var distMeasure = receivedMeasures.FindLastByPointNameAndType(name, typeof(MeasureOfDistance));
            if (distMeasure != null && distMeasure is MeasureOfDistance dm)
            {
                r.Dist_DL_MM_ = new ResultValue(Math.Round(-dm.distAvgMove.Value * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                olderMeasureDate = olderMeasureDate.ReplaceIfNewerThan(dm._Date); // keep the oldest
            }

            //show the time of the older meas.
            r.Time = olderMeasureDate.ToShortTimeString();
            return r;
        }

        internal static List<string> GetColumnsNameToHide(List<Measure> receivedMeasures)
        {
            // Measured?
            bool wireMeas = false;
            bool PolarMeas = false;
            bool LevelMeas = false;
            bool DistMeas = false;
            for (int i = receivedMeasures.Count - 1; i >= 0; i--)
            {
                if (!wireMeas)
                    if (receivedMeasures[i] is MeasureOfOffset)
                        wireMeas = true;
                if (!PolarMeas)
                    if (receivedMeasures[i] is Polar.Measure)
                        PolarMeas = true;
                if (!LevelMeas)
                    if (receivedMeasures[i] is MeasureOfLevel)
                        LevelMeas = true;
                if (!DistMeas)
                    if (receivedMeasures[i] is MeasureOfDistance)
                        DistMeas = true;
            }

            List<string> list = new List<string>();

            if (!wireMeas) list.Add("Wire_DR_MM_");
            if (!LevelMeas) list.Add("Level_DV_MM_");
            if (!DistMeas) list.Add("Dist_DL_MM_");

            if (!PolarMeas) list.Add("Polar_DR_MM_");
            if (!PolarMeas) list.Add("Polar_DL_MM_");
            if (!PolarMeas) list.Add("Polar_DV_MM_ ");
            if (!PolarMeas) list.Add("FullStructureName");

            return list;
        }
    }
    public static class GlobalResultExtensions
    {
        public static void AddIfNotExisting<T>(this T subCategories, string categoryName, out GlobalAlignmentResults found) where T : List<ITreeGridCategory>
        {
            if (!subCategories.ContainsByName(categoryName, out found))
            {
                found = new GlobalAlignmentResults() { Name = categoryName };
                subCategories.Add(found);
            }
            return;
        }

        public static bool ContainsByName<T>(this T subCategories, string categoryName, out GlobalAlignmentResults found) where T : List<ITreeGridCategory>
        {
            foreach (var category in subCategories)
            {
                if (category.Name == categoryName)
                {
                    found = category as GlobalAlignmentResults;
                    return true;
                }
            }
            found = null;
            return false;
        }
    }
}