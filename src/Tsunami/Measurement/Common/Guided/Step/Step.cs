﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using TSU.Views;
using System.Windows.Forms;
using O = TSU.Common.Operations;
using Z = TSU.Common.Zones;
using E = TSU.Common.Elements;
using M = TSU.Common.Measures;

using System.Xml;
using System.Xml.Serialization;
using TSU.Common.Elements.Manager;
using TSU.Common.Instruments;

namespace TSU.Common.Guided.Steps
{
    public enum StepState
    {
        Valid,
        ReadyToSkip,
        NotReady
    }
}

namespace TSU.Common.Guided.Steps
{
    public class Step : TSU.Module
    {
        #region Fields

        public object TAG;

        // Used to communicate bewtween different eventhandler
        internal object internalMessageOrObject = null;


        public StepState State { get; set; }
        public int gridRowIndex { get; set; }


        public bool Visible = true;
        internal string MessageFromTest;
        public bool skipable;
        internal bool firstUse = true;
        internal bool isAStepOfChoice = false;
        internal bool choiceCancelled = false;
        internal bool stepsRemovedByUser = false;
        internal bool okToMoveBack = true;
        public Module GuidedModule
        {
            get
            {
                return ParentModule as Module;
            }
        }
        [XmlIgnore]
        internal new View View
        {
            get
            {
                return _TsuView as View;
            }
            set
            {
                _TsuView = value;
            }
        }

        #endregion Fields

        #region Construction

        public Step()
        {
            Initialize();

        }

        public Step(TSU.Module parentModule, string titleAndDeescription, TsuObject tag = null)
            : base(parentModule, titleAndDeescription, false)
        {
            if (tag != null) TAG = tag;
            if (Id == null) Id = "none";
            Changed += delegate { (parentModule as Module).View.UpdateNextButton(); };
        }

        public override void Initialize()
        {

        }

        internal void CreateViewWithNameAndDescription()
        {
            View = new View(this);
            View.AddTitle(_Name, "Title");
            View.AddDescription(Utility, "Description");
        }

        public override void Add(TSU.Module module)
        {
            base.Add(module);
            module._TsuView.TopLevel = false;
            module._TsuView.ShowDockedFill();
            View.Controls.Add(module._TsuView);
        }
        public override string ToString()
        {
            return _Name + ", " + Utility;
        }

        #endregion

        #region Adds




        internal void AddButton(BigButton existingButton, string name = "")
        {
            Control.ControlCollection c = View.Controls;
            if (!c.ContainsKey(name))
                View.AddButton(existingButton, name);
        }

        internal void AddButton(ToolStripItem existingToolStripItem, string name = "")
        {
            AddButton((existingToolStripItem as BigButtonMenuItem).bigButton, name);
        }
        #endregion

        #region Validation

        internal void Validate()
        {
            State = StepState.Valid;
        }
        internal void UnValidate()
        {
            if (skipable)
                State = StepState.ReadyToSkip;
            else
                State = StepState.NotReady;
        }
        internal bool ForceNextStep()
        {
            return View.ForceNextStep(MessageFromTest);

        }

        /// <summary>
        /// YOu can Enter 'the expression you want to check' instead of 'bool value', This function will update the step state between 'Valid' and 'NotReady'
        /// </summary>
        /// <param name="resultOfAnExpression"></param>
        internal bool CheckValidityOf(bool resultOfAnExpression, string errorMessage = "")
        {
            if (errorMessage == "")
                errorMessage = R.T_SOMETHING_IS_MISSING_IN_THE_ACTUAL_STEP;

            
            if (resultOfAnExpression)
            {
                this.MessageFromTest = "";
                Validate();
                return true;
            }
            else
            {
                this.MessageFromTest = errorMessage;
                UnValidate();
                return false;
            }
        }

        /// <summary>
        /// if the result is already false, we skip the test and return false, else, we eval the expression and upadte the message
        /// </summary>
        /// <param name="resultOfAnExpression"></param>
        internal bool UpdateValidityOf(bool currentlyValid, bool resultOfAnExpression, string errorMessage)
        {
            bool stillValid = currentlyValid && resultOfAnExpression;

            if (stillValid)
            {
                return CheckValidityOf(stillValid);
            }
            else
            {
                string mergedMessage = this.MessageFromTest;
                if (mergedMessage != "")
                    mergedMessage += ", ";
                mergedMessage += errorMessage;
                return CheckValidityOf(stillValid, mergedMessage);
            }
        }

        #endregion

        #region StepEvents

        #region Declaration

        /// <summary>
        /// definition of the method that an event should implement
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        public delegate void StepEventHandler(object sender, StepEventArgs e);

        internal Step.StepEventHandler Tester;

        #endregion

        #region EventsHandlers

        /// <summary>
        /// This action will be exectue when you enter the step
        /// It will automatically show title and descriptions of the step
        /// Should be assigned before you use the function 'AddButton'
        /// </summary>
        [field: XmlIgnore]
        public event StepEventHandler FirstUse;

        /// <summary>
        /// This action will be exectue when you click the 'next' button but also any tiem a change happen to change the color of the next button
        /// </summary>
        /// 
        [field: XmlIgnore]
        public event StepEventHandler Testing;


        [field: XmlIgnore]
        public event StepEventHandler CheckValidityToMoveToPreviousStep;

        /// <summary>
        /// This action will be exectue on Update() calls
        /// </summary>
        /// 
        [field: XmlIgnore]
        public event StepEventHandler Updating;
        /// <summary>
        /// This action will be exectue on Subscribe() calls
        /// </summary>
        /// 
        [field: XmlIgnore]
        public event StepEventHandler SubscribingEvents;
        /// <summary>
        /// This action will be exectue on Unsubscribe() calls
        /// </summary>
        /// 
        [field: XmlIgnore]
        public event StepEventHandler UnSubscribingEvents;
        /// <summary>
        /// This action will be exectuted after you click on any button added in the view of the step
        /// Should be assigned before you use the function 'AddButton'
        /// </summary>
        [field: XmlIgnore]
        public event StepEventHandler ControlChanged;

        /// <summary>
        /// To be use when the step has changed
        /// </summary>
        [field: XmlIgnore]
        public new event StepEventHandler Changed;

        /// <summary>
        /// This action will be exectue when you enter the step
        /// </summary>
        [field: XmlIgnore]
        public event StepEventHandler Entered;

        /// <summary>
        /// This action will be exectue when you enter the step
        /// </summary>
        [field: XmlIgnore]
        public event StepEventHandler Entering;

        /// <summary>
        /// This action will be exectue when you leave the step
        /// </summary>
        [field: XmlIgnore]
        public event StepEventHandler Leaving;


        [field: XmlIgnore]
        public event StepEventHandler Left;

        /// <summary>
        /// This action will be exectue when the step is hidden from the module steps
        /// </summary>
        [field: XmlIgnore]
        public event StepEventHandler Desappearing;

        /// <summary>
        /// This action will be exectue when you leave the step
        /// </summary>
        [field: XmlIgnore]
        public M.MeasurementEventHandler MeasureReceived;

        /// <summary>
        /// This action will be exectue when you leave the step
        /// </summary>
        [field: XmlIgnore]
        public event StepEventHandler AutoFillForDebug;

        #endregion

        #region actions

        /// <summary>
        /// Create the step view than Will trigger the FirstUse event and then will enter the step (triggering 'entering' event)
        /// </summary>
        public void UseForFirstTime()
        {
            Debug.WriteInConsole("TSU.STEP.FIRSTentering " + $"'{_Name}' {R.T_IS_ENTERING_FOR_THE_FIRST_TIME}");
            try
            {
                CreateViewWithNameAndDescription();

                View.SuspendLayout();

                if (FirstUse != null)
                    FirstUse(this, new StepEventArgs(GuidedModule));

                // if debugging
                if (System.Diagnostics.Debugger.IsAttached)
                    if (AutoFillForDebug != null)
                        AutoFillForDebug(this, new StepEventArgs(GuidedModule));

                firstUse = false;

                // sinon on entre deux fois Enter();
            }
            catch (Exception ex)
            {
                //  this.View.ShowMessageOfBug(string.Format(R.T_STP_UNABLE_First, this._Name, ex.Message));
                View.ShowMessageOfBug($"{R.T_STP_UNABLE_First} '{_Name}.\r\n", ex);
            }
            Debug.WriteInConsole("TSU.STEP.FIRSTentered" + $"...'{_Name}' {R.T_HAS_BEEN_ENTERED_FOR_THE_FIRST_TIME}");
        }

        /// <summary>
        /// Will execute eventhandlers 'controlchanged' when a control changed the step should react and then update
        /// </summary>
        public void ControlChange()
        {
            Debug.WriteInConsole("STEP.CONTROL.CHANGED..." + $"'{_Name}' {R.T_IS_CHANGING}");

            try

            {
                if (ControlChanged != null)
                    ControlChanged(this, new StepEventArgs(GuidedModule));

                // when a step changed is should be tested and updated.
                Test();
                Update();

            }
            catch (Exception ex)
            {
                //  this.View.ShowMessageOfBug(string.Format(R.T_STP_UNABLE_ControlChange, this._Name, ex.Message));
                View.ShowMessageOfBug($"{R.T_STP_UNABLE_ControlChange} '{_Name}.\r\n", ex);
            }
            Debug.WriteInConsole("...STEP.CONTROL.CHANGED" + $"...'{_Name}' {R.T_HAS_BEEN_CHANGED}");
        }

        /// <summary>
        /// Will execute eventhandlers 'controlchanged' when a control changed the step should react and then update
        /// </summary>
        public new void Change()
        {
            Debug.WriteInConsole("TSU.STEP.CHANGING " + $"'{_Name}' {R.T_IS_CHANGING}");

            try
            {
                Test();
                if (Changed != null)
                    Changed(this, new StepEventArgs(GuidedModule));

            }
            catch (Exception ex)
            {
                //  this.View.ShowMessageOfBug(string.Format(R.T_STP_UNABLE_Change, this._Name, ex.Message));
                View.ShowMessageOfBug($"{R.T_STP_UNABLE_Change} '{_Name}'.\r\n", ex);
            }

            Debug.WriteInConsole("TSU.STEP.CHANGED " + $"...'{_Name}' {R.T_HAS_BEEN_CHANGED}");
        }

        /// <summary>
        /// will be executed everytime you enter the step excep t the first time, will trigger the entering event, test() and update();
        /// </summary>
        public void Enter()
        {
            if (View != null) View.LayoutLayering(View.LayoutOption.Suspend);

            Debug.WriteInConsole("TSU.STEP.ENTERING " + $"'{_Name}' {R.T_IS_ENTERING}");
            try
            {
                if (firstUse) UseForFirstTime(); // in case we create the module and jump directily to a step (than the previous vew are not created)
                if (Entering != null)
                    Entering(this, new StepEventArgs(GuidedModule));

                MakeButtonsClicksTriggeringChanges(this);

                GuidedModule.currentStepName = Id; // to be able to reopen on the same step.

                Test();
                View.ResetView();

                Update();

            }
            catch (Exception ex)
            {
                View.ShowMessageOfBug($"{R.T_STP_UNABLE_Enter} '{_Name}\r\n", ex);
            }
            Debug.WriteInConsole("TSU.STEP.ENTERED " + $"... '{_Name}' {R.T_HAS_BEEN_ENTERED}");

            if (View != null) View.LayoutLayering(View.LayoutOption.Resume);
        }

        private void MakeButtonsClicksTriggeringChanges(Step step)
        {
            // remove the controlchange firing from the button
            foreach (var item in (step.View as View).Controls)
            {
                if (item is BigButton bb) bb.BigButtonClicked += AnyButton_BigButtonClicked;
                if (item is TableLayoutPanel tlp)
                    foreach (var subitem in tlp.Controls)
                        if (subitem is BigButton bbb) bbb.BigButtonClicked += AnyButton_BigButtonClicked;
            }
        }

        private void AnyButton_BigButtonClicked(object source, TsuObjectEventArgs e)
        {

            if (source is BigButton b)
            {
                if (b.HasSubButtons) return;
                if (b.isSpecial) return;

                if (isAStepOfChoice)
                {
                    foreach (var item in (View as View).Controls)
                    {
                        if (item is BigButton bb) bb.RestoreColor();
                    }
                    if (!choiceCancelled)
                        b.HighLightWithColor(Tsunami2.Preferences.Theme.Colors.Good);
                }
                ControlChange();
            }
        }

        public void InvokeEntered()
        {
            if (Entered != null)
                Entered(this, new StepEventArgs(GuidedModule));

        }

        internal virtual bool IsOkToGoToPreviousStep()
        {
            // by default is it ok, to be override if some check needed

            if (CheckValidityToMoveToPreviousStep != null)
                CheckValidityToMoveToPreviousStep(this, new StepEventArgs(GuidedModule));
            return okToMoveBack;
        }

        /// <summary>
        /// Will trigger the testing event then;
        /// </summary>
        public void Test()
        {
            Debug.WriteInConsole("TSU.STEP.TESTING " + $"'{_Name}' {R.T_IS_TESTING}");

            try
            {
                CheckIfInstrumentStillLinked();

                if (Testing != null)
                    Testing(this, new StepEventArgs(GuidedModule));

            }
            catch (Exception ex)
            {
                // this.View.ShowMessageOfBug(string.Format(R.T_STP_UNABLE_Test, this._Name, ex.Message));
                View.ShowMessageOfBug($"{R.T_STP_UNABLE_Test} '{_Name}.\r\n", ex);
            }
            Debug.WriteInConsole("TSU.STEP.TESTED " + $"... '{_Name}' {R.T_HAS_BEEN_TESTED_AND_IS} '{State.ToString()}'");
        }
        /// <summary>
        /// Will trigger the subscribing event then;
        /// </summary>
        public void SubscribeEvents()
        {
            Debug.WriteInConsole("TSU.STEP.SUBSCRIBING EVENTS " + $"'{_Name}' {R.T_IS_SUBSCRIBING_EVENTS}");

            try
            {

                if (SubscribingEvents != null)
                    SubscribingEvents(this, new StepEventArgs(GuidedModule));

            }
            catch (Exception ex)
            {
                View.ShowMessageOfBug($"{R.T_STP_UNABLE_SUBSCRIBE_EVENTS} '{_Name}.\r\n", ex);
            }
            Debug.WriteInConsole("TSU.STEP.SUBSCRIBED EVENTS " + $"... '{_Name}' {R.T_IS_SUBSCRIBED_EVENTS} '{State.ToString()}'");
        }
        /// <summary>
        /// Will trigger the subscribing event then;
        /// </summary>
        public void UnsubscribeEvents()
        {
            Debug.WriteInConsole("TSU.STEP.UNSUBSCRIBING EVENTS " + $"'{_Name}' {R.T_IS_UNSUBSCRIBING_EVENTS}");

            try
            {

                if (SubscribingEvents != null)
                    SubscribingEvents(this, new StepEventArgs(GuidedModule));

            }
            catch (Exception ex)
            {
                View.ShowMessageOfBug($"{R.T_STP_UNABLE_UNSUBSCRIBE_EVENTS} '{_Name}.\r\n", ex);
            }
            Debug.WriteInConsole("TSU.STEP.UNSUBSCRIBED EVENTS " + $"... '{_Name}' {R.T_IS_UNSUBSCRIBED_EVENTS} '{State.ToString()}'");
        }
        private void CheckIfInstrumentStillLinked()
        {
            //foreach (var item in this.GuidedModule._ActiveStationModule)
            //{
            if (GuidedModule._ActiveStationModule is Polar.Station.Module stm)
            {
                stm._InstrumentManager.LinkUnlinkedInstrument();

            }
            //}
        }

        bool updateTriggered = false;
        Timer TimerForUpdate;

        private void CreateUpdateTimer()
        {
            TimerForUpdate = new Timer() { Enabled = true, Interval = 100 };

            TimerForUpdate.Tick += OnTimerForUpdateTick;
        }

        private void OnTimerForUpdateTick(object sender, EventArgs e)
        {
            try
            {
                TimerForUpdate.Stop();
                updateTriggered = false;
                UpdateForReal();
            }
            catch (Exception ex)
            {
                TimerForUpdate.Stop(); // also here or it will show in cirle during the time that all message are not closed
                                       //  this.View.ShowMessageOfBug(string.Format(R.T_STP_UNABLE_UPDATE, this._Name, ex.Message));
                View.ShowMessageOfBug($"{R.T_STP_UNABLE_UPDATE} '{_Name}'.\r\n", ex);
            }
            finally
            {
                if (TimerForUpdate != null)
                {
                    TimerForUpdate.Stop();
                    TimerForUpdate.Dispose();
                    TimerForUpdate = null;
                }
            }
        }

        /// <summary>
        /// Will trigger Updating evetn and will update the next button color and update the guided view when i updating t then trigger 'change() to spread the news
        /// </summary>
        public void Update()
        {
            if (GuidedModule.CancelAllStepUpdate) return;

            if (TimerForUpdate != null && updateTriggered && GuidedModule.isTargetStep)
            {
                if (!TimerForUpdate.Enabled) // to be sure taht timer is runnng is triggerd
                    TimerForUpdate.Start();

                Debug.WriteInConsole("TSU.STEP.Update was already requested " + $"'{_Name}' {R.T_IS_UPDATING}");
                return;
            }
            else
            {
                updateTriggered = true;
                if (TimerForUpdate == null)
                    CreateUpdateTimer();
                TimerForUpdate.Start();
            }

            InvokeOnApplicationDispatcher(View.Refresh);
        }

        public void UpdateForReal()
        {
            Debug.WriteInConsole("TSU.STEP.Updating " + $"'{this._Name}' {R.T_IS_UPDATING}");

            // top the timer or iw will update as long as the update method is not finish, for exemple if there is a message opened
            TimerForUpdate.Stop();
            // Only update if the step is current one.
            if (GuidedModule.isTargetStep)
            {
                //   TSU.Debug.WriteInConsole(DateTime.Now.Second + ":" + DateTime.Now.Millisecond + ": " + "TSU.STEP.Updating " + string.Format("'{0}' is updating...", this._Name));
                Debug.WriteInConsole("TSU.STEP.Updating " + $"'{_Name}' {R.T_IS_UPDATING}");
                
                if (Updating != null)
                    Updating(this, new StepEventArgs(GuidedModule));
            }
            Change();

            // move the scrll bar to reproduce the previous one?
            int previous = GuidedModule.PreviousScrollPosition;
            if (previous != -1)
            {
                if (previous <= View.VerticalScroll.Maximum && previous >= View.VerticalScroll.Minimum)
                    View.VerticalScroll.Value = previous;
                GuidedModule.PreviousScrollPosition = -1;
            }

            View.ResumeLayout(true);

            Debug.WriteInConsole("TSU.STEP.Updated " + $"...'{_Name}' {R.T_HAS_BEEN_UPDATED}");
        }

        /// <summary>
        /// Will trigger the Leaving event then;
        /// </summary>
        public void Leave()
        {
            Debug.WriteInConsole("TSU.STEP.Leaving " + $"'{_Name}' {R.T_IS_LEAVING_AND_IS} '{State.ToString()}'...");

            try
            {
                if (Leaving != null)
                    Leaving(this, new StepEventArgs(GuidedModule));
                else
                    State = StepState.Valid;

                // remove the controlchange firing from the button
                if (View != null)
                {
                    foreach (var item in (View as View).Controls)
                    {
                        if (item is BigButton) (item as BigButton).BigButtonClicked -= AnyButton_BigButtonClicked;
                        if (item is TableLayoutPanel)
                            foreach (var subitem in (item as TableLayoutPanel).Controls)
                                if (subitem is BigButton) (subitem as BigButton).BigButtonClicked -= AnyButton_BigButtonClicked;

                    }
                }

                if (!Cancel)
                    if (Left != null)
                        Left(this, new StepEventArgs(GuidedModule));
            }
            catch (Exception ex)
            {
                //  this.View.ShowMessageOfBug(string.Format(R.T_STP_UNABLE_LEAVE, this._Name, ex.Message));
                View.ShowMessageOfBug($"{R.T_STP_UNABLE_LEAVE} '{_Name}'.\r\n", ex);
            }
            Debug.WriteInConsole("TSU.STEP.Left " + $"...'{_Name}' {R.T_HAS_LEFT}\r\n");
        }

        /// <summary>
        /// Will trigger the Leaving event then;
        /// </summary>
        public void MakeDesappear()
        {
            Debug.WriteInConsole("TSU.STEP.Desappearing " + $"'{_Name}' {R.T_IS_DISAPPEARING_AND_IS} '{State.ToString()}'...");

            try

            {
                if (Desappearing != null)
                    Desappearing(this, new StepEventArgs(GuidedModule));
                else
                    State = StepState.Valid;
            }
            catch (Exception ex)
            {
                //  this.View.ShowMessageOfBug(string.Format(R.T_STP_UNABLE_Desappear, this._Name, ex.Message));
                View.ShowMessageOfBug($"{R.T_STP_UNABLE_Desappear} '{_Name}'.\r\n", ex);
            }

            Visible = false;

            Debug.WriteInConsole("TSU.STEP.desappeared " + $"...'{_Name}' {R.T_HAS_DISAPPEARED}\r\n");
        }


        #endregion

        #endregion

        #region Other Events

        internal void Manager_Changed(object source, ModuleChangeEventArgs args)
        {
            //this.Test(); noramu do anyway buy the guided module

            ControlChange();
        }

        #endregion


        public bool Cancel { get; set; }
    }
}

namespace TSU.Common.Guided.Steps
{
    public class StepEventArgs : EventArgs
    {
        public Step step;
        public Module guidedModule;
        public E.Manager.Module ElementManager;
        public Z.Manager ZoneManger;
        public O.Manager OperationManager;
        public Instruments.Manager.Module InstrumentManager;
        public Instruments.Module InstrumentModule;
        public Station ActiveStation;
        public Station.Module ActiveStationModule;

        public StepEventArgs()
        {
        }

        public StepEventArgs(Module guidedModule)
        {
            FillEventArgs(guidedModule);
        }

        private void FillEventArgs(Module guidedModule)
        {
            step = guidedModule.CurrentStep;
            this.guidedModule = guidedModule;
            ElementManager = guidedModule._ElementManager;
            ZoneManger = guidedModule.ZoneManager;
            OperationManager = guidedModule.OperationManager;

            if (guidedModule._ActiveStationModule != null)
            {
                ActiveStationModule = guidedModule._ActiveStationModule;
                ActiveStation = guidedModule._ActiveStationModule._Station;
                InstrumentManager = guidedModule._ActiveStationModule._InstrumentManager;
                InstrumentModule = guidedModule._ActiveStationModule._InstrumentManager.SelectedInstrumentModule;
            }
        }
    }
}