﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using TSU.Views;
using System.Windows.Forms;
using M = TSU;
using T = TSU.Tilt;
using E = TSU.Line;
using L = TSU.Level;
using TSU.Common.Guided.Steps;

namespace TSU.Common.Guided.Alignment
{

    static public class Admin
    {
        static public Module Get(M.Module parentModule, string nameAndDescription)
        {
            Module gm = new Module(parentModule, nameAndDescription);
            Group.Module grGm = parentModule as Group.Module;
            grGm.Type = ENUM.GuidedModuleType.Alignment;
            gm.guideModuleType = ENUM.GuidedModuleType.AlignmentManager;
            BuildSteps(nameAndDescription, gm);
            gm.Start();

            return gm;
        }

        private static void BuildSteps(string nameAndDescription, Module gm)
        {
            //Step 0 : presentation//
            gm.Steps.Add(Management.Declaration(gm, nameAndDescription));
            //Step 1 : choix fichier theorique
            //gm.Steps.Add(Step.Management.ChooseAlignmentTheoreticalFile(gm));
            // Step 2 Choix eaquipe
            //gm.Steps.Add(Step.Management.EnterAlignmentTeam(gm));
            // Step 3: Choix operation
            //gm.Steps.Add(Step.Management.ChooseAlignmentOperation(gm));
            //Step 1 choix parametre admin
            gm.Steps.Add(Management.ChooseALignmentAdministrativeParameters(gm));
            // Step 2: Choix elements à aligner
            gm.Steps.Add(Management.ChooseMagnetsToAlign(gm));
            // Step 3: résultat alignement
            gm.Steps.Add(Management.AlignmentResults(gm));
            // Step 4: affiche le fichier Geode sauvegarder
            gm.Steps.Add(Management.ShowDatFiles(gm));
            //// Step 6: Step final
            //gm.Steps.Add(Step.Management.LastAdminStep(gm));
        }
        internal static void ReCreateWhatIsNotSerialized(Module gm)
        {
            gm.Steps = new List<Steps.Step>();
            //gm.currentIndex = 0;
            BuildSteps(gm.NameAndDescription, gm);
            //gm.Start();
            //gm.MoveToStep(gm.currentIndex);
        }
    }
}
