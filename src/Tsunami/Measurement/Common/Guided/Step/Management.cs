﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TSU.Common.Elements;
using TSU.Views;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;
using T = TSU.Tools;
using BOC = TSU.Common.Compute.Compensations.BeamOffsets;
using TSU.Common.Instruments;
using TSU.Common.Compute.CsvImporter;
using TSU.Common.Compute.Rabot;
using System.Runtime.CompilerServices;
using TSU.Preferences.GUI;

namespace TSU.Common.Guided.Steps
{
    public static class Management
    {
        /// <summary>
        /// Presentation of the Module
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        static public Step Declaration(Module gm, string OverridedTitleAndContents = null)
        {
            OverridedTitleAndContents = OverridedTitleAndContents ?? gm._Name + ";" + gm.Utility;
            Step s = new Step(gm, OverridedTitleAndContents);

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                if (Tsunami2.Preferences.Values.GuiPrefs.DontEverShowTheDeclarationStepOfGuidedModule.IsTrue)
                {
                    s.Validate();
                    s.GuidedModule.currentIndex++;
                }
                BigButton dontShowEver = new BigButton(
                    $"{R.T_DONT_SHOW_ME_THIS_LIST_AGAIN_EVER};{R.T_IF_YOU_KNOW_VERY_WELL_ALL_THE_MODULES}",
                    R.StatusBad,
                    delegate
                    {
                        Tsunami2.Preferences.Values.GuiPrefs.DontEverShowTheDeclarationStepOfGuidedModule.IsTrue = true;
                        Tsunami2.Preferences.Values.SaveGuiPrefs();
                        s.Validate();
                        s.GuidedModule.MoveToNextStep();
                    }, null, null, false);
                s.View.AddButton(dontShowEver);

                dontShowEver.Dock = DockStyle.Bottom; // override

            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                // Show list of all steps
                {
                    s.View.ModifyLabel("\r\n" + R.T_STEP_LIST + ":" +
                                        "\r\n" + "----------", "listTitle");
                    string stepNames = "";
                    foreach (var item in gm.Steps.Skip(1))
                    {
                        stepNames += "\t - " + item._Name + "\r\n";
                    }

                    s.View.ModifyLabel(stepNames, "listNames");
                }
            };

            s.Testing += delegate (object source, StepEventArgs e)
            {
                // This is put here becaue if tehre is no test (no testing event) existing the step will be ne put in "skippable" and not "Ready"
                e.step.CheckValidityOf(true);
            };

            return s;
        }
        /// <summary>
        /// End invisible step
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        static public Step End(Module gm, string OverridedTitleAndContents = null)
        {
            OverridedTitleAndContents = OverridedTitleAndContents ?? R.String_GM_CloseModule;
            //BigButton buttonOpen = new BigButton();
            BigButton buttonFinished = new BigButton();
            Step s = new Step(gm, OverridedTitleAndContents);
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                // add a button close the module and add it to finished module
                buttonFinished = e.step.View.AddButton(
                    R.String_GM_FinishModule,
                    R.FinishHim,
                    delegate
                    {
                        FinishModule(source, e);
                    },
                    null
                );
            };
            s.Entering += delegate (object source, StepEventArgs e)
            {
            };
            // Updating() is triggered by Update()
            s.Updating += delegate (object source, StepEventArgs e)
            {
                //if (gm.finished == false)
                //{
                //    buttonFinished.Visible = true;
                //    //buttonOpen.Visible = false;
                //}
                //else
                //{
                //    buttonFinished.Visible = false;
                //    buttonOpen.Visible = true;
                //}
            };
            return s;
        }

        internal static void FinishModule(object source, StepEventArgs e)
        {
            Module gm = e.guidedModule;
            gm.Finished = true;
            gm._TsuView.Hide();
            if (gm.ParentModule is Tsunami)
            {
                (gm.ParentModule as Tsunami).Menu.SetAnotherModuleAsActive(gm);
                (gm.ParentModule as Tsunami).Menu.RefreshOpenedAndFinishedModule();
                e.step.Update();
                gm.Save();
            }
        }

        /// <summary>
        /// Allows to select a file and show it contents
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        static public Step ChooseTheoreticalFile(Module guidedModule)
        {
            Step s = new Step(guidedModule, R.GM_M_STF);

            BigButton ShowFileButton = null;

            void OnElementManagerChanged(object source, ModuleChangeEventArgs args)
            {
                s.ControlChange();
            }

            s.FirstUse += delegate (object source, StepEventArgs e)
            {

                e.ElementManager.View.flagProposeFileAtUpdate = false;

                // add a button to directly browse for theoretical file
                e.step.View.AddButton(
                    R.T244,
                    R.Open,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Module gm = args2.TsuObject as Module;
                        gm._ElementManager.View.TryOpen();
                        if (e.ElementManager.AllElements.Count > 0)
                        {
                            if (!gm._ElementManager.View.Visible)
                                ShowFileButton.Available = true;
                            e.step.View.ModifyLabel("\r\n" + (gm._ElementManager.AllElements[0] as E.Element)._Origin + "\r\n\r\n", "FileName");
                            e.step.View.ModifyLabelColor(Tsunami2.Preferences.Theme.Colors.Good, "FileName");
                        }
                    }
                );

                // possibibly add a button to open the same file already open in a module
                List<E.Composites.TheoreticalElement> f = e.ElementManager.FindOpenedFiles();
                if (f.Count == 1)
                {
                    e.step.View.AddButton(
                    $"{R.T_KEEP_SAME_FILE}; {f[0]._Name}",
                    R.Open,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Module gm = args2.TsuObject as Module;
                        e.ElementManager.AddTheoreticalElement(f[0]);
                        if (e.ElementManager.AllElements.Count > 0)
                        {
                            if (!gm._ElementManager.View.Visible)
                                ShowFileButton.Available = true;
                            e.step.View.ModifyLabel("\r\n" + (gm._ElementManager.AllElements[0] as E.Element)._Origin + "\r\n\r\n", "FileName");
                            e.step.View.ModifyLabelColor(Tsunami2.Preferences.Theme.Colors.Good, "FileName");
                        }
                    }
                );
                }


                // add label for file name
                e.step.View.ModifyLabel("\r\n" + R.T_NO_FILE_SELECTED_YET + "\r\n\r\n", "FileName");
                e.step.View.ModifyLabelColor(Tsunami2.Preferences.Theme.Colors.Bad, "FileName");

                // add button show file if wanted
                ShowFileButton = new BigButton(
                    R.T_STM_ELEMENT_MANAGER,
                    R.Element_Aimant,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Module gm = args2.TsuObject as Module;
                        s.GuidedModule._ElementManager.MultiSelection = false;
                        s.GuidedModule._ElementManager.View.CheckBoxesVisible = false;
                        s.GuidedModule._ElementManager.UpdateView();
                        s.GuidedModule.CurrentStep.View.AddControl(s.GuidedModule._ElementManager);
                        ShowFileButton.Available = false;
                    });
                ShowFileButton.Available = false;
                e.step.View.AddButton(ShowFileButton);

            };

            s.AutoFillForDebug += delegate (object source, StepEventArgs e)
            {
                //                string path =TSU.Tsunami2.TsunamiPreferences.Values.FilePaths[Tsunami2.Preferences.FilePathsKeys.Temporary.ToString()] + "res.dat";
                //                string content =
                //                    @"% Point List in IdXYZ Format created by Tsunami
                //                    LHCb      ;Unknown.ref.1                   ;           ;4471.977723;5004.633559;2330.311343;331.443201;      000;      0000;      00000;      0000;  ; ;01-Jan-0001;                    ;;
                //LHCb      ;Unknown.ref.2                   ;           ;4472.387186;5004.636217;2330.292671;331.424684;      000;      0000;      00000;      0000;  ; ;01-Jan-0001;                    ;;
                //LHCb      ;Unknown.ref.3                   ;           ;4474.893087;5004.654543;2330.532816;331.665777;      000;      0000;      00000;      0000;  ; ;01-Jan-0001;                    ;;
                //LHCb      ;Unknown.ref.4                   ;           ;4473.157711;5008.064313;2330.445056;331.578937;      000;      0000;      00000;      0000;  ; ;01-Jan-0001;                    ;;
                //LHCb      ;Unknown.ref.5                   ;           ;4471.659831;5008.005486;2330.448860;331.582153;      000;      0000;      00000;      0000;  ; ;01-Jan-0001;                    ;;
                //LHCb      ;Unknown.STL.001                 ;           ;4472.525917;5005.972899;2330.049052;331.181733;      000;      0000;      00000;      0000;  ; ;01-Jan-0001;  
                //LHCb      ;QTGD.411500.E                   ;           ;4471.977723;5004.633559;2330.311343;331.443201; 1.000000;   -.00035;   0.075819; 99.541786; 1;A;01-Jan-0001;                     ;;
                //LHCb      ;QTGD.411500.S                   ;           ;4474.893087;5004.654543;2330.532816;331.665777; 2.923839;   -.00035;   0.075819; 99.541786; 1;P;01-Jan-0001;"   ;

                //                System.IO.File.WriteAllText(path, content);
                //                e.ElementManager.zone =TSU.Tsunami2.TsunamiPreferences.Values.MlaZones[0];
                //                e.ElementManager.AddFromFile(new System.IO.FileInfo(path), E.ElementManager.sourceFile.geodeXYZH);
                //                e.ElementManager.SelectableType = ENUM.ElementType.Point;
                //                e.ElementManager.View.MultiSelection = true;
                //                e.ElementManager.View.CheckBoxes = true;
                //                e.ElementManager.UpdateView();
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.ElementManager.Changed += OnElementManagerChanged;
                //delegate { e.step.ControlChange(); };

                e.ElementManager.SetSelectableToAllPoints();
                e.ElementManager.MultiSelection = false;
                e.ElementManager.View.CheckBoxesVisible = false;
                if (e.ElementManager.AllElements.Count > 0)
                {
                    //e.guidedModule.CurrentStep.View.AddControl(e.guidedModule._ElementManager.View);
                    e.step.View.ModifyLabel("\r\n" + (e.ElementManager.AllElements[0] as E.Element)._Origin + "\r\n\r\n", "FileName");
                    e.step.View.ModifyLabelColor(Tsunami2.Preferences.Theme.Colors.Good, "FileName");
                }
                e.ElementManager.UpdateView();
            };

            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = false;
                e.step.CheckValidityOf(e.ElementManager.GetAllTheoreticalPoints().Count > 0);
            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {
                e.ElementManager.Changed -= OnElementManagerChanged;

                e.step.View.RemoveControl(e.ElementManager);

            };
            return s;
        }



        /// <summary>
        /// Allows to select a file and show it contents
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        static public Step ChooseSequenceFile(Module guidedModule)
        {
            Step s = new Step(guidedModule, R.StringGuided_SelectSequenceFile);

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                // add a button to directly browse for theoretical file
                e.step.View.AddButton(
                    R.T244,
                    R.Open,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Module gm = args2.TsuObject as Module;
                        e.step.View.RemoveControl(e.ElementManager);
                        gm._ElementManager.SetSelectableToAllSequenceFil();
                        gm._ElementManager.MultiSelection = false;
                        gm._ElementManager.View.CheckBoxesVisible = false;
                        gm._ElementManager.UpdateView();
                        gm.CurrentStep.View.AddControl(gm._ElementManager);
                        gm._ElementManager.View.TrySelectSequenceFile();
                    }
                );
                e.ElementManager.Changed += delegate { e.step.ControlChange(); };
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.ElementManager.SetSelectableToAllSequenceFil();
                e.ElementManager.MultiSelection = false;
                e.ElementManager.View.CheckBoxesVisible = false;
                if (e.ElementManager.AllElements.Count > 0)
                {
                    e.guidedModule.CurrentStep.View.AddControl(e.guidedModule._ElementManager);
                }
                e.ElementManager.UpdateView();
            };


            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                // check to be implemented in subClass with the right sequence type
                //e.step.CheckValidityOf(e.ElementManager.AllElements.Count > 0);
            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {
                e.step.View.RemoveControl(e.ElementManager);
            };


            return s;
        }
        /// <summary>
        /// Allows to select a instrument
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        static public Step ChooseInstrument(Module guidedModule, InstrumentClasses ic)
        {
            Step s = new Step(guidedModule, R.soai);

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                e.step.View.AddDescription(R.ctiywtu);
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.InstrumentManager._SelectedObjects.Clear();

                e.InstrumentManager.SetInstrumentSelectableList(e.InstrumentManager.GetByClass(ic));

                e.InstrumentManager.View.CheckBoxesVisible = false;
                e.InstrumentManager.MultiSelection = false;

                e.step.View.AddControl(e.InstrumentManager);

                // check if an instrument to propose
                if (FindInstrumentToPropose(e.ActiveStation, e.InstrumentManager, out I.Instrument instrument))
                {
                    e.InstrumentManager._SelectedObjectInBlue = instrument;
                    e.InstrumentManager.AddSelectedObjects(instrument);
                }
                e.InstrumentManager.UpdateView();
            };

            s.ControlChanged += delegate (object source, StepEventArgs e)
            {
                ChangeInstrument(e, s);
            };

            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = false;
                e.step.MessageFromTest = R.String_GM_MsgBlockSelectInstrument;

                // selection = instrumetn of the statin
                if (e.InstrumentManager._SelectedObjects.Count == 1)
                {
                    I.Instrument instrument = e.InstrumentManager._SelectedObjects[0] as I.Instrument;

                    if (instrument != null && e.InstrumentManager.SelectedInstrumentModule != null && e.InstrumentManager.SelectedInstrumentModule.Instrument == instrument)
                    {
                        e.step.Validate();
                        return;
                    }
                    else
                    {
                        ChangeInstrument(e, s);
                        e.step.Test();
                        return;
                    }

                }

                // pas d'instrument module?
                if (e.InstrumentManager.SelectedInstrumentModule == null)
                {
                    e.step.UnValidate();
                    return;
                }

                // pas d'instrument?
                if (e.InstrumentManager.SelectedInstrumentModule.Instrument == null)
                {
                    e.step.UnValidate();
                    return;
                }

                // nothing selected?
                if (e.InstrumentManager._SelectedObjects.Count == 0)
                {
                    e.step.UnValidate();
                    return;
                }


                e.step.UnValidate();
            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {
                e.step.View.RemoveControl(e.InstrumentManager);
            };
            return s;
        }

        public static bool FindInstrumentToPropose(Station station, I.Manager.Module instrumentManager, out I.Instrument instrument)
        {
            I.Instrument proposedInstrument = null;

            if (station.ParametersBasic._Instrument._Name != R.String_Unknown)
                proposedInstrument = station.ParametersBasic._Instrument;

            if (proposedInstrument == null)
            {
                List<I.Instrument> usedInstruments = Tsunami2.Preferences.Values.UsedInstruments;
                foreach (var item in usedInstruments)
                {
                    if (item is I.Sensor)
                    {
                        if (instrumentManager.SelectableObjects.OfType<Sensor>().FirstOrDefault(x => x._Name == item._Name) != null)
                        {
                            proposedInstrument = item;
                            break;
                        }
                    }
                }
            }

            if (proposedInstrument != null)
                if (proposedInstrument._Name != R.String_Unknown)
                {
                    instrument = instrumentManager.AllElements.Find(x => x._Name == proposedInstrument._Name) as I.Instrument;
                    return true;
                }
            instrument = null;
            return false;
        }

        private static void ChangeInstrument(StepEventArgs e, Step s)
        {
            // need for instrument to create an instrument module with the right model
            //if (e.InstrumentManager._SelectedObjects[0] is I.Device.AT40x.At40x)
            if (e.InstrumentManager._SelectedObjects.Count == 0) return;

            e.ActiveStationModule.OnInstrumentChanged(e.InstrumentManager._SelectedObjects[0] as I.Sensor);
            //else
            //   e.InstrumentManager.ValidateSelection();

            // Remove connection step if present
            if (e.guidedModule.NextStep != null)
                if (e.guidedModule.NextStep._Name == T.Conversions.StringManipulation.SeparateTitleFromMessage(R.T245)) e.guidedModule.RemoveNextStep(true);

            // Add connection step if connectabel instrument
            if (e.InstrumentManager.SelectedInstrumentModule != null && e.InstrumentManager.SelectedInstrumentModule is I.IConnectable && s.GuidedModule._ActiveStationModule is Polar.Station.Module)
            {
                Step connect = ConnectInstrument(e.guidedModule);
                e.guidedModule.AddNewStepAfterCurrentOne(connect);
            }
        }

        /// <summary>
        /// Allows to use the instrument interface for connection and initialization (only valid for theodolite type)
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        static public Step ConnectInstrument(Module guidedModule)
        {
            Step s = new Step(guidedModule, R.T245);

            // Usefull references
            Polar.Station.Module stm = s.GuidedModule._ActiveStationModule as Polar.Station.Module;
            Polar.Station.View stmv = stm.View;
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                stmv.buttons.ShowInstrumentModule.ChangeNameAndDescription($"{R.T_SHOW_AND_CONNECT};{R.T_SHOW_AND_CONNECT_DETAILS}");
                e.step.View.AddButton(stmv.buttons.ShowInstrumentModule);
                s.View.AddControl(guidedModule._ActiveStationModule._InstrumentManager.SelectedInstrumentModule);
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {

                e.ActiveStationModule._InstrumentManager.SelectedInstrumentModule._TsuView.UpdateView();
            };

            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.Validate();
            };

            return s;
        }

        /// <summary>
        /// Allows to schoose the team
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        /// 
        static public Step EnterATeam(Module guidedModule)
        {
            Step s = EnterAString(guidedModule, R.T247, Tsunami2.Preferences.Values.GuiPrefs.LastTeamUsed);
            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                string proposedteam = e.ActiveStationModule._Station.ParametersBasic._Team;
                if (proposedteam == R.String_UnknownTeam)
                    proposedteam = Tsunami2.Preferences.Values.UsedTeam;
                if (proposedteam != R.String_UnknownTeam)
                {
                    foreach (var item in e.step.View.Controls)
                    {
                        if (item is TextBox)
                        {
                            (item as TextBox).Text = proposedteam;
                        }
                    }
                }
                // just select the text
                Step temp = source as Step;

                // temp._TsuView.UpdateView();
            };

            s.Entered += delegate (object source, StepEventArgs e)
            {

                SetFocusOnTextBox(e.step.View.Controls.OfType<TextBox>());
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                // feed the team parameter in the station
                string temp = e.step.TAG as string;
                //Vérifie que le nom d'équipe contient que des caractères alphanumériques et est de maximum 8 caractères
                if (System.Text.RegularExpressions.Regex.IsMatch(temp, @"^[a-zA-Z]+$") && (temp.Length < 9 || temp == R.String_UnknownTeam))
                {
                    temp = temp.ToUpper();
                    e.ActiveStationModule._Station.ParametersBasic._Team = temp;
                    e.step.Validate();
                }
                else
                {
                    e.step.UnValidate();
                }
                //if (temp != "")
                //{
                //    e.ActiveStationModule._Station.ParametersBasic._Team = temp;
                //    e.step.Validate();
                //}
                //else
                //    e.step.UnValidate();
            };
            return s;
        }

        /// <summary>
        /// receive a text from the user
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <param name="defaultString"></param>
        /// <returns></returns>
        static public Step EnterAString(Module guidedModule, string titleAnddescription, string defaultString = "$d")
        {

            if (defaultString == "$d") defaultString = R.T248;
            Step s = new Step(guidedModule, titleAnddescription);
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                e.step.View.AddTextBox(guidedModule._TsuView, defaultString);
                e.step.TAG = defaultString;
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
            };
            //this.treeView_Parameters.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView_Parameters_NodeMouseClick);
            return s;

        }

        internal static void SetFocusOnTextBox(IEnumerable<TextBox> enumerable)
        {
            enumerable.First().Focus();
            enumerable.First().SelectAll();

            enumerable.First().Focus();
        }

        /// <summary>
        /// Allows to select an operation
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        static public Step ChooseOperation(Module guidedModule)
        {
            Step s = new Step(guidedModule, R.T249);

            // FirstUse() is triggered by UseForFirstTime()
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
            };

            // Entering() is triggered by Enter()
            s.Entering += delegate (object source, StepEventArgs e)
            {
                //e.step.Change();
                e.step.View.AddControl(e.guidedModule.OperationManager);
                e.OperationManager.OperationTree.ExpandToLevel(e.OperationManager.OperationTree.Nodes, 2);
                e.OperationManager._SelectedObjects.Clear();
                O.Operation operation = e.OperationManager.AllElements.Find(x => x._Name == e.ActiveStationModule._Station.ParametersBasic._Operation._Name) as O.Operation;
                e.OperationManager._SelectedObjectInBlue = operation;
                e.OperationManager.AddSelectedObjects(operation);
                e.OperationManager.UpdateView();
            };

            // Changed() is triggered by Change()
            s.ControlChanged += delegate (object source, StepEventArgs e)
            {
                e.ActiveStation.ParametersBasic._Operation = e.OperationManager.SelectedOperation;
            };

            // Updating() is triggered by Update()
            s.Updating += delegate (object source, StepEventArgs e)
            {
                O.Operation o = e.ActiveStation.ParametersBasic._Operation;
                //e.step.View.ModifyLabel(string.Format("The initial operation will be : {0}", o.value.ToString() + ": " + o.Description), "opDescription");
            };

            // Testing() is triggered by Test()
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                e.step.CheckValidityOf(e.OperationManager.SelectedOperation.IsSet);
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                e.step.View.RemoveControl(e.guidedModule.OperationManager);
            };
            return s;
        }

        /// <summary>
        /// Choose points from theorietcal file
        /// </summary>
        /// <param name="gm"></param>
        /// <returns></returns>
        internal static Step ChooseElement(Module guidedModule)
        {
            Step s = new Step(guidedModule, R.S_SN_ChooseElement);

            s.Left += delegate (object source, StepEventArgs e)
            {
                if (e.step.View == null) return;
                e.step.View.RemoveControl(e.ElementManager);
            };

            return s;
        }

        /// <summary>
        /// Choose points from theorietcal file, it uses a modified "choose elements" step 
        /// </summary>
        /// <param name="gm"></param>
        /// <returns></returns>
        internal static Step ChoosePoints(Module guidedModule, string titleAndDescription = null)
        {
            Step s = ChooseElement(guidedModule);

            if (titleAndDescription == null)
            {
                s._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.T251);
                s.Utility = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.T251);
            }
            else
            {
                s._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(titleAndDescription);
                s.Utility = T.Conversions.StringManipulation.SeparateTitleFromMessage(titleAndDescription);
            }

            s.Tester = (sender, e) =>
            {
                e.step.CheckValidityOf(e.ElementManager._SelectedObjects.Count > 0);
            };

            s.Testing += s.Tester;

            return s;
        }


        /// <summary>
        /// Choose Magnets from theorietcal file
        /// </summary>
        /// <param name="gm"></param>
        /// <returns></returns>
        internal static Step ShowMeasurements(Module guidedModule)
        {
            Step s = new Step(guidedModule, R.S_SMM);
            s.Id = "ShowMeasurements";
            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.ActiveStationModule._MeasureManager.MultiSelection = true;
                e.step.AddButton(e.ActiveStationModule.View.buttons.MeasureDetails, "mes");
                e.step.AddButton(
                new BigButton(
                    $"{R.T_SHOW_ELEMENT_MODULE};{R.T_TO_VISUALIZE_AND_PLAY_WITH_THE_POINTS}",
                    R.Element_Aimant,
                    delegate
                    {
                        e.ElementManager.SetSelectableToAllPoints();

                        e.ElementManager.View.ShowInMessageTsu("Elements", R.T_OK, null);
                    }, null, null, false), "element");
                e.step.Update();
            };

            s.Updating += delegate (object source, StepEventArgs e)
            {
                bool headerNeeded = true;
                e.step.View.SuspendLayout();
                for (int i = 0; i < e.ActiveStationModule._MeasureManager.AllElements.Count; i++)
                {
                    M.Measure m = e.ActiveStationModule._MeasureManager.AllElements[i] as M.Measure;
                    e.step.View.ModifyLabel(m.ToString_List_Diff_Poin(headerNeeded), "MesureAsText" + i.ToString());
                    if (m._Status is M.States.Bad) e.step.View.ModifyLabelColor(Tsunami2.Preferences.Theme.Colors.Bad, "MesureAsText" + i.ToString());
                    if (m._Status is M.States.Questionnable) e.step.View.ModifyLabelColor(Tsunami2.Preferences.Theme.Colors.Attention, "MesureAsText" + i.ToString());
                    if (m._Status is M.States.Cancel) e.step.View.ModifyLabelColor(Tsunami2.Preferences.Theme.Colors.Bad, "MesureAsText" + i.ToString());
                    if (m._Status is M.States.Temporary) e.step.View.ModifyLabelColor(Tsunami2.Preferences.Theme.Colors.Object, "MesureAsText" + i.ToString());
                    if (m._Status is M.States.Control) e.step.View.ModifyLabelColor(Tsunami2.Preferences.Theme.Colors.Highlight, "MesureAsText" + i.ToString());
                    if (m._Status is M.States.Good) e.step.View.ModifyLabelColor(Tsunami2.Preferences.Theme.Colors.Good, "MesureAsText" + i.ToString());
                    headerNeeded = false;
                }
                e.step.View.ResumeLayout(true);
            };
            return s;
        }

        internal static Step ShowMeasurementsWithoutValidation(Module guidedModule)
        {
            Step s = ShowMeasurements(guidedModule);
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.CheckValidityOf(true);
            };
            return s;
        }

        internal static Step Export(Module guidedModule)
        {
            Step s = new Step(guidedModule, R.T_TH_Export);
            s.TAG = "Export";

            s.FirstUse += delegate (object source, StepEventArgs e)
            {

                ///only for the actve station
                e.step.AddButton(new BigButton(R.T_B_MeasureExport2Geode, R.Geode, () =>
                {
                    bool doExport = true;
                    if (e.guidedModule._ActiveStationModule is Polar.Station.Module stm)
                        stm.CloseStation(out doExport);

                    if (doExport)
                    {
                        try
                        {
                            if (e.guidedModule.ExportToGeode())
                            {
                                s.View.ModifyLabel($"\r\n{R.T_MEASURES} {R.T_EXPORTED} {R.T_FOR} Geode ({DateTime.Now.ToString("hh:mm:ss")})", "save", addToExistingText: true);
                                s.View.ModifyLabelColor(Tsunami2.Preferences.Theme.Colors.Good, "save");
                            }
                        }
                        catch (Exception ex)
                        {
                            string titleAndMessage = $"{R.T_FAILED};{ex.Message}";
                            new MessageInput(MessageType.Critical, titleAndMessage).Show();
                        }
                    }
                }));
                //e.step.AddButton(e.ActiveStationModule._MeasureManager.View.buttons.geode, "geode");  
                //e.step.AddButton(e.ActiveStationModule._MeasureManager.View.buttons.lgc2, "lg2");
                e.step.AddButton(new BigButton(R.T_B_MeasureExportLGC2, R.Lgc, () =>
                {
                    try
                    {
                        if (e.guidedModule.ExportToLGC2())
                        {
                            s.View.ModifyLabel($"\r\n{R.T_MEASURES} {R.T_EXPORTED} {R.T_FOR} LGC2 ({DateTime.Now.ToString("hh:mm:ss")})", "save", addToExistingText: true);
                            s.View.ModifyLabelColor(Tsunami2.Preferences.Theme.Colors.Good, "save");
                        }
                    }
                    catch (Exception ex)
                    {
                        string titleAndMessage = $"{R.T_FAILED};{ex.Message}";
                        new MessageInput(MessageType.Critical, titleAndMessage).Show();
                    }

                }));
                s.View.ModifyLabel("", R.T_SAVE, Tsunami2.Preferences.Theme.Colors.Good);

                // ad acces to measuremet in the bottom of the step
                BigButton b = (s.GuidedModule._ActiveStationModule as Polar.Station.Module).View.buttons.MeasureDetails.GetClone() as BigButton;

                s.View.AddButton(b, "mes", DockStyle.Bottom);
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.step.Update();
            };

            s.Updating += delegate (object source, StepEventArgs e)
            {
                if (e.step.GuidedModule.PointsToBeAligned.Count > 0)
                {
                    List<M.Measure> saved = e.step.GuidedModule._ActiveStationModule._Station.MeasuresTaken;
                    List<M.Measure> waiting = e.step.GuidedModule.ReceivedMeasures;
                    string message = "";
                    foreach (var item in e.step.GuidedModule.PointsToBeAligned)
                    {
                        List<M.Measure> allMeasures = new List<M.Measure>();
                        allMeasures.AddRange(saved);
                        allMeasures.AddRange(waiting);
                        if (allMeasures.FindLast(x => x._Point._Name == item._Name) == null)
                            message += $"{R.T_YOU_DONT_HAVE_ANY_MEASUREMENT_FO} {item._Name}\r\n";
                        else
                        {
                            if (saved.FindLast(x => x._Point._Name == item._Name && x._Status is M.States.Good) == null)
                                message += $"{R.T_YOU_DONT_HAVE_A_VALIDATED_MEASUREMENT_FOR} {item._Name}\r\n";
                        }
                    }
                    s.View.ModifyLabel(message, "save", addToExistingText: false);
                    s.View.ModifyLabelColor(Tsunami2.Preferences.Theme.Colors.Bad, "save");
                }

            };

            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.CheckValidityOf(true);
            };


            return s;
        }


        internal static Step AddAStation(Module gm)
        {
            Step s = new Step(gm, $"{R.T_ADD_A_OTHER_STATION};{R.T_YOU_CAN_EITHER_CREATE_A_NEW_STATION_OR_MOVE_TO_THE_NEXT_STEP_TO_EXPORT_YOUR_MEASUREMENT}");
            s.TAG = "AddAStation";

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                e.step.AddButton(
                    new BigButton(
                        $"{R.T_ADD_STATION};{R.T_ADD_A_NEW_STATION_TO_THE_MODULE}",
                        R.Add,
                        delegate
                        {

                            // Create new stationModule
                            Polar.Station.Module stm = new Polar.Station.Module(gm);

                            // Close previous and copy flags
                            if (e.guidedModule._ActiveStationModule is Polar.Station.Module stm2)
                            {
                                stm2.CloseStation(out bool dummy);
                            }

                            // Copy parameters
                            Station.Parameters p = gm._ActiveStationModule._Station.ParametersBasic;
                            stm._Station.ParametersBasic._Team = p._Team;
                            stm._Station.ParametersBasic._Operation = p._Operation.Clone() as O.Operation;
                            stm._Station.ParametersBasic.DefaultMeasure = p.DefaultMeasure.Clone() as M.Measure;
                            stm._Station.ParametersBasic.DefaultMeasure._Point = p.DefaultMeasure._Point.Clone() as E.Point;
                            stm._Station.ParametersBasic.DefaultMeasure.Guid = Guid.NewGuid();

                            stm._Station.ParametersBasic._Instrument = p._Instrument;
                            ///stm.OnInstrumentChanged(stm._Station.ParametersBasic._Instrument);

                            //set new to active one
                            gm.SwitchToANewStationModule(stm);

                            // reset step as first use to update the station m
                            gm.ResetStep("StationPosition");
                            gm.ResetStep("StationHeight");
                            gm.ResetStep("StationDefaultMeasurementParameters");
                            gm.ResetStep("ChoosePointsToMeasure");
                            gm.ResetStep("Abstract", "Abstract");
                            gm.ResetStep("ShowMeasurements");
                            gm.ResetStep("Closure");

                            gm.MoveToStep("StationPosition", false);
                        }, null, null, false), "Add");
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.step.Update();
            };

            s.Updating += delegate (object source, StepEventArgs e)
            {

            };

            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.CheckValidityOf(true);
            };


            return s;
        }

        /// <summary>
        /// Show the Geode dat File that has been recorded
        /// </summary>
        /// <param name="gm"></param>
        /// <returns></returns>
        internal static Step ShowDatFiles(Module guidedModule)
        {
            Step s = new Step(guidedModule, R.String_Step_ShowDatFile);
            s._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.String_Step_ShowDatFile);
            s.Utility = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.String_Step_ShowDatFile);
            Process process = null;
            BigButton buttonOpen = new BigButton();
            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                if (process == null)
                {
                    if (e.ActiveStationModule != null)
                    {

                        if (e.ActiveStationModule._Station.ParametersBasic._GeodeDatFilePath != "")
                        {
                            if (System.IO.File.Exists(e.ActiveStationModule._Station.ParametersBasic._GeodeDatFilePath))
                            {
                                buttonOpen = e.step.View.AddButton(
                                    R.String_GM_OpendatFile,
                                    R.Open,
                                    delegate (object source2, TsuObjectEventArgs args2)
                                    {
                                        Shell.Run(e.ActiveStationModule._Station.ParametersBasic._GeodeDatFilePath);
                                    }
                                );
                                process = s.View.AddShowDatFilePanel(e.ActiveStationModule._Station.ParametersBasic._GeodeDatFilePath);
                            }
                            else
                            {
                                new MessageInput(MessageType.Warning, R.StringGuided_NoDatFile).Show();
                                process = null;
                            }
                        }
                        else
                        {
                            new MessageInput(MessageType.Warning, R.StringGuided_NoDatFile).Show();
                            process = null;
                        }


                    }
                    else
                    {
                        if (e.guidedModule.ParentModule is Group.Module)
                        {
                            Group.Module GrGm = e.guidedModule.ParentModule as Group.Module;
                            string filepath = "";
                            foreach (Module item in GrGm.SubGuidedModules)
                            {
                                if (item._ActiveStationModule != null)
                                {
                                    if (item._ActiveStationModule._Station != null)
                                    {
                                        if (item._ActiveStationModule._Station.ParametersBasic._GeodeDatFilePath != "")
                                        {
                                            filepath = item._ActiveStationModule._Station.ParametersBasic._GeodeDatFilePath;
                                            if (System.IO.File.Exists(filepath))
                                            {
                                                process = s.View.AddShowDatFilePanel(filepath);
                                            }
                                            else
                                            {
                                                process = null;
                                            }
                                            return;
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            };
            s.Updating += delegate (object source, StepEventArgs e)
            {

            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                if (process != null)
                {
                    e.step.View.Controls.Remove(buttonOpen);
                    e.step.View.ControlList.Remove(buttonOpen);
                    process.CloseMainWindow();
                    process = null;
                }
            };
            return s;
        }
        /// <summary>
        /// Show the Geode dat File that has been recorded
        /// </summary>
        /// <param name="gm"></param>
        /// <returns></returns>
        internal static Step ShowLGCFiles(Module guidedModule)
        {
            Step s = new Step(guidedModule, R.String_Step_ShowLGCFiles);
            s._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.String_Step_ShowLGCFiles);
            s.Utility = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.String_Step_ShowLGCFiles);
            List<Process> processList = new List<Process>();
            BigButton buttonOpen = new BigButton();
            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                if (processList.Count == 0)
                {
                    if (e.ActiveStationModule._Station.ParametersBasic._LGCInputFilePath != "" && e.ActiveStationModule._Station.ParametersBasic._LGCOutputFilePath != "")
                    {
                        if (System.IO.File.Exists(e.ActiveStationModule._Station.ParametersBasic._LGCInputFilePath) && System.IO.File.Exists(e.ActiveStationModule._Station.ParametersBasic._LGCOutputFilePath))
                        {
                            buttonOpen = e.step.View.AddButton(
                                R.String_GM_OpenLGCFile,
                                R.Open,
                                delegate (object source2, TsuObjectEventArgs args2)
                                {
                                    Shell.Run(e.ActiveStationModule._Station.ParametersBasic._GeodeDatFilePath);
                                }
                            );
                            processList = s.View.AddLGCFilesPanel(e.ActiveStationModule._Station.ParametersBasic._LGCInputFilePath, e.ActiveStationModule._Station.ParametersBasic._LGCOutputFilePath);
                        }
                        else
                        {
                            new MessageInput(MessageType.Warning, R.StringGuided_NoLGCFiles).Show();
                        }

                    }
                    else
                    {
                        new MessageInput(MessageType.Warning, R.StringGuided_NoLGCFiles).Show();
                    }
                }

            };
            s.Updating += delegate (object source, StepEventArgs e)
            {

            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                foreach (Process process in processList)
                {
                    e.step.View.Controls.Remove(buttonOpen);
                    e.step.View.ControlList.Remove(buttonOpen);
                    process.CloseMainWindow();
                }
                processList.Clear();
            };
            return s;
        }

        /// <summary>
        /// Choose points from theorietcal file, it uses a modified "choose elements" step 
        /// </summary>
        /// <param name="gm"></param>
        /// <returns></returns>
        internal static Step LastAdminStep(Module guidedModule)
        {
            Step s = new Step(guidedModule, R.StringAlignment_AdminCompleted);

            s._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringAlignment_AdminCompleted);
            s.Utility = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringAlignment_AdminCompleted);

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                Module gm = e.guidedModule;
                Group.Module GrGm = gm.ParentModule as Group.Module;
                foreach (Module item in GrGm.SubGuidedModules)
                {
                    if (item.View != null)
                    {
                        switch (item.guideModuleType)
                        {
                            case ENUM.GuidedModuleType.AlignmentLevellingCheck:
                                ///Garde le module levelling check invisible tant qu'on a pas fait le nivellement normal
                                break;
                            default:
                                item.View.Visible = true;
                                break;
                        }
                    }
                }
            };

            return s;
        }

        /// <summary>
        /// Choose Magnets from theorietcal file, it uses a modified "choose elements" step 
        /// </summary>
        /// <param name="gm"></param>
        /// <returns></returns>
        internal static Step AlignmentResults(Module guidedModule)
        {
            Step s = LastAdminStep(guidedModule);

            //   s._Name = "Global visualisation step";
            s._Name = R.T_GLOBAL_VISUALISATION_STEP;
            //  s.utility = "You can see here, the different offsets gathered from all the specialised modules";
            s.Utility = R.T_YOU_CAN_SEE_HERE_THE_DIFFERENT_OFFSETS_GATHERED_FROM_ALL_THE_SPECIALISED_MODULES;

            Module adminModule = s.GuidedModule;
            Group.Module GrGm = adminModule.ParentModule as Group.Module;
            //Tolérance module guidé alignement à décider plus tard
            double rollTolerance = 0.05; //mrad
            double lengthTolerance = 0.5; //mm
            double verticalTolerance = 0.2; //mm
            double radialTolerance = 0.2; //mm
            Label fakeLabel = new Label();
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                //e.step.View.ClearAllControls();
                e.step.View.Dock = DockStyle.Fill;
                fakeLabel.Dock = DockStyle.Top;
                fakeLabel.Text = "";
                fakeLabel.AutoSize = false;
                fakeLabel.Height = 0;
                fakeLabel.Width = 0;
                e.step.View.ControlList.Add(fakeLabel);
                e.step.View.Controls.Add(fakeLabel);
                // creating zones for button and results
                List<Control> l = new List<Control>();
                try
                {

                    l.Add(new BigButton(
                        R.T_B_TILT_ALIGN,
                        R.Tilt,
                        GrGm.SubGuidedModules.Find(x => x._ActiveStationModule is Tilt.Station.Module).ShowInParentModule));
                    l.Add(new BigButton(
                        R.T_B_ALTI_ALIGN,
                        R.Level,
                        GrGm.SubGuidedModules.Find(x => x._ActiveStationModule is Level.Station.Module).ShowInParentModule));
                    l.Add(new BigButton(
                        R.T_B_LENGTH_ALIGN,
                        R.Theodolite,
                        GrGm.SubGuidedModules.Find(x => x._ActiveStationModule is Polar.Station.Module).ShowInParentModule));
                    l.Add(new BigButton(
                        R.T_B_RADIAL_ALIGN,
                        R.Line,
                        GrGm.SubGuidedModules.Find(x => x._ActiveStationModule is Line.Station.Module).ShowInParentModule));
                    l.Add(new BigButton(
                        R.StringLength_ButtonAlignLengthSetting,
                        R.LengthSetting,
                        GrGm.SubGuidedModules.Find(x => x._ActiveStationModule is Length.Station.Module).ShowInParentModule));
                }
                catch (Exception)
                {
                    throw new Exception($"Cannot create buttons to sub modules because one of them do not exist, try to opent the TSU with version of TSUnam that created it");
                }
                e.step.View.AddSidedControls(l);
                List<Control> save = new List<Control>();
                //string tolerance;
                //tolerance = string.Format(R.StringAlignment_RollTolerance, rollTolerance);
                //tolerance += " ";
                //tolerance += string.Format(R.StringAlignment_VerticalTolerance, verticalTolerance);
                //tolerance += " ";
                //tolerance += string.Format(R.StringAlignment_LengthTolerance, lengthTolerance);
                //tolerance += " ";
                //tolerance += string.Format(R.StringAlignment_RadialTolerance, radialTolerance);

                save.Add(new BigButton(
                    R.StringAlignment_SaveAllButton, R.Save, GrGm.SaveAll));
                save.Add(Compute.Compensations.BeamOffsets.WorkFlow.Buttons.GetComputeButton(action: GetBocAction(e)));
                e.step.View.AddSidedControls(save);
                e.step.View.CreateDatagridViewResult();
                e.step.View.InitializeDatagridViewResult();
                e.step.View.dataGridViewResult.Dock = DockStyle.Fill;
                e.step.View.RedrawDataGridViewResult(adminModule.PointsToBeAligned, adminModule.ReceivedMeasures, rollTolerance, verticalTolerance, lengthTolerance, radialTolerance);
                e.step.View.RedrawTreeGridViewResult(adminModule.PointsToBeAligned, adminModule.ReceivedMeasures, Tsunami2.Properties.BOC_Context.ExistingResults, rollTolerance, verticalTolerance, lengthTolerance, radialTolerance);

                //e.step.View.SortDatagridview();
                //e.step.View.ModifyLabel("No result yet. Please use the different guided modules available to take process some measurement.", "Title");
                //e.step.View.ModifyLabelColor(Tsunami2.Preferences.Theme._Colors.Bad, "Title");
                //e.step.View.ModifyLabel("", "Results");

            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                // coounting magnet
                s.Update();
            };
            s.ControlChanged += delegate (object source, StepEventArgs e)
            {

            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {

            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                /// Met à jour la liste des received measures dans l'adminModule
                adminModule.ReceivedMeasures.Clear();
                foreach (Module gm in GrGm.SubGuidedModules)
                {
                    adminModule.ReceivedMeasures.AddRange(gm.ReceivedMeasures);
                }
                e.step.View.RedrawDataGridViewResult(adminModule.PointsToBeAligned, adminModule.ReceivedMeasures, rollTolerance, verticalTolerance, lengthTolerance, radialTolerance);
                e.step.View.RedrawTreeGridViewResult(adminModule.PointsToBeAligned, adminModule.ReceivedMeasures, Tsunami2.Properties.BOC_Context.ExistingResults, rollTolerance, verticalTolerance, lengthTolerance, radialTolerance);

            };

            return s;
        }

        private static Action GetBocAction(StepEventArgs e)
        {
            return () =>
            {
                var ids = BOC.WorkFlow.DetermineAssemblyIds(e.guidedModule.MagnetsToBeAligned);

                Views.Message.ProgressMessage pm = new Views.Message.ProgressMessage(e.guidedModule.View,
                            R.T_TSUNAMI_IS_RUNNING_THINGS_FOR_YOU, R.T_PROGRESS,
                            stepsCount: ids.Count, 2000);

                foreach (var id in ids)
                {
                    bool launchComputeWithAllObservation = true;

                    if (launchComputeWithAllObservation)
                    {
                        if (pm.IsCancelled) break;
                        pm.BeginAstep($"BOC-ALL for {id}");
                        BOC.Results r = BOC.WorkFlow.Compute(e.guidedModule, Tsunami2.Properties.MeasurementModules, id, ObservationType.All);
                        BOC.WorkFlow.OffsetComputedInAllSystems.WaitOne(5 * 1000);
                        string message = BOC.WorkFlow.ResultsToString(id, r.Offsets, r.dRoll);
                        new MessageInput(MessageType.FYI, message).Show();
                    }
                    else
                    // launch all BOC separtely
                    {
                        if (Tsunami2.Preferences.Values.GuiPrefs.BOC_from_ADMIN_step_mixes_all_observations.IsTrue)
                        {
                            try
                            {
                                if (pm.IsCancelled) break;
                                pm.BeginAstep($"BOC-LEVEL for {id}");
                                var modules = Tsunami2.Properties.MeasurementModules.GetByObservationType(ObservationType.All, addRollModules: false);
                                var modulesWithRoll = Tsunami2.Properties.MeasurementModules.GetByObservationType(ObservationType.All, addRollModules: true);
                                if (modules.Count > 0)
                                    BOC.WorkFlow.Compute(e.guidedModule, modulesWithRoll, id, ObservationType.All);
                            }
                            catch (Exception)
                            {
                                pm.EndAll();
                            }
                        }
                        else
                        {
                            // level with roll
                            try
                            {
                                if (pm.IsCancelled) break;
                                pm.BeginAstep($"BOC-LEVEL for {id}");
                                var levelModules = Tsunami2.Properties.MeasurementModules.GetByObservationType(ObservationType.Level, addRollModules: false);
                                var levelModulesWithRoll = Tsunami2.Properties.MeasurementModules.GetByObservationType(ObservationType.Level, addRollModules: true);
                                if (levelModules.Count > 0)
                                    BOC.WorkFlow.Compute(e.guidedModule, levelModulesWithRoll, id, ObservationType.Level);
                            }
                            catch (Exception ex)
                            {
                                pm.EndAll();
                            }

                            try
                            {
                                // polar with roll

                                if (pm.IsCancelled) break;
                                pm.BeginAstep($"BOC-POLAR for {id}");
                                var polarModules = Tsunami2.Properties.MeasurementModules.GetByObservationType(ObservationType.Polar, addRollModules: false);
                                var polarModulesWithRoll = Tsunami2.Properties.MeasurementModules.GetByObservationType(ObservationType.Polar, addRollModules: true);
                                if (polarModules.Count > 0)
                                    BOC.WorkFlow.Compute(e.guidedModule, polarModulesWithRoll, id, ObservationType.Polar);
                            }
                            catch (Exception ex)
                            {
                                pm.EndAll();
                            }

                            try
                            {
                                // wire with roll

                                if (pm.IsCancelled) break;
                                pm.BeginAstep($"BOC-ECARTO for {id}");
                                var ecartoModules = Tsunami2.Properties.MeasurementModules.GetByObservationType(ObservationType.Ecarto, addRollModules: false);
                                var ecartoModulesWithRoll = Tsunami2.Properties.MeasurementModules.GetByObservationType(ObservationType.Ecarto, addRollModules: true);
                                if (ecartoModules.Count > 0)
                                    BOC.WorkFlow.Compute(e.guidedModule, ecartoModulesWithRoll, id, ObservationType.Ecarto);
                            }
                            catch (Exception ex)
                            {
                                pm.EndAll();
                            }
                        }
                    }
                    pm.EndAll();
                    pm.Dispose();
                }
            };
        }


        /// <summary>
        /// Allows to choose the team for an element alignment
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        /// 
        static public Step EnterAlignmentTeam(Module guidedModule)
        {
            Step s = EnterAString(guidedModule, R.T247, Tsunami2.Preferences.Values.GuiPrefs.LastTeamUsed);
            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                Group.Module GrGm = e.guidedModule.ParentModule as Group.Module;
                if (GrGm.SubGuidedModules[1]._ActiveStationModule._Station.ParametersBasic._Team != R.String_UnknownTeam)
                {
                    foreach (var item in e.step.View.Controls)
                    {
                        if (item is TextBox)
                        {
                            (item as TextBox).Text = GrGm.SubGuidedModules[1]._ActiveStationModule._Station.ParametersBasic._Team;
                        }
                    }
                }
                // just select the text
                Step temp = source as Step;

                // temp._TsuView.UpdateView();
            };

            s.Entered += delegate (object source, StepEventArgs e)
            {

                SetFocusOnTextBox(e.step.View.Controls.OfType<TextBox>());
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                // feed the team parameter in the station
                string temp = e.step.TAG as string;
                //Vérifie que le nom d'équipe contient que des caractères alphanumériques et est de maximum 8 caractères
                if (System.Text.RegularExpressions.Regex.IsMatch(temp, @"^[a-zA-Z]+$") && (temp.Length < 9 || temp == R.String_UnknownTeam))
                {
                    temp = temp.ToUpper();
                    Group.Module GrGm = e.guidedModule.ParentModule as Group.Module;
                    foreach (Module item in GrGm.SubGuidedModules)
                    {
                        if (item._ActiveStationModule is Tilt.Station.Module) { (item._ActiveStationModule as Tilt.Station.Module).ChangeTeam(temp); }
                        if (item._ActiveStationModule is Line.Station.Module) { (item._ActiveStationModule as Line.Station.Module).ChangeTeam(temp); }
                        if (item._ActiveStationModule is Level.Station.Module) { (item._ActiveStationModule as Level.Station.Module).ChangeTeam(temp); }
                        if (item._ActiveStationModule is Polar.Station.Module) { (item._ActiveStationModule as Polar.Station.Module)._Station.ParametersBasic._Team = temp; }
                    }
                    e.step.Validate();
                }
                else
                {
                    e.step.UnValidate();
                }
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {

            };
            return s;
        }
        /// <summary>
        /// Allows to select an operation for the element alignment guided module
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        static public Step ChooseAlignmentOperation(Module guidedModule)
        {
            Step s = new Step(guidedModule, R.T249);
            // Entering() is triggered by Enter()
            s.Entering += delegate (object source, StepEventArgs e)
            {
                Group.Module GrGm = e.guidedModule.ParentModule as Group.Module;
                //e.step.Change();
                e.step.View.AddControl(e.guidedModule.OperationManager);
                //e.OperationManager.OperationTree.ExpandAll();
                e.OperationManager._SelectedObjects.Clear();
                O.Operation operation = e.OperationManager.AllElements.Find(x => x._Name == GrGm.SubGuidedModules[1]._ActiveStationModule._Station.ParametersBasic._Operation._Name) as O.Operation;
                e.OperationManager._SelectedObjectInBlue = operation;
                e.OperationManager.AddSelectedObjects(operation);
                e.OperationManager.UpdateView();
            };
            // Testing() is triggered by Test()
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                e.step.CheckValidityOf(e.OperationManager.SelectedOperation.IsSet);
            };
            s.ControlChanged += delegate (object source, StepEventArgs e)
            {
                Group.Module GrGm = e.guidedModule.ParentModule as Group.Module;
                foreach (Module item in GrGm.SubGuidedModules)
                {
                    if (item._ActiveStationModule is Tilt.Station.Module) { (item._ActiveStationModule as Tilt.Station.Module).ChangeOperationID(e.OperationManager.SelectedOperation); }
                    if (item._ActiveStationModule is Line.Station.Module) { (item._ActiveStationModule as Line.Station.Module).ChangeOperationID(e.OperationManager.SelectedOperation); }
                    if (item._ActiveStationModule is Level.Station.Module) { (item._ActiveStationModule as Level.Station.Module).ChangeOperationID(e.OperationManager.SelectedOperation); }
                    if (item._ActiveStationModule is Polar.Station.Module) { (item._ActiveStationModule as Polar.Station.Module).ChangeOperationID(e.OperationManager.SelectedOperation); }
                }
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                e.step.View.RemoveControl(e.guidedModule.OperationManager);
            };
            return s;
        }

        internal static void RemovePointToMeasureToStations(Step s, E.Point p)
        {
            Group.Module GrGm = s.GuidedModule.ParentModule as Group.Module;
            foreach (Module item in GrGm.SubGuidedModules)
            {
                item.MagnetsToBeAligned = s.GuidedModule._ElementManager.GetMagnetsInSelectedObjects().Clone();
                item.PointsToBeAligned = s.GuidedModule._ElementManager.GetPointsInSelectedObjects().Clone();
                ///Enlève toutes les mesures reçues car un calcul doit de nouveau être fait
                item.ReceivedMeasures.Clear();
                if (item._ActiveStationModule is Tilt.Station.Module)
                {
                    Tilt.Station.Module stationTiltModule = item._ActiveStationModule as Tilt.Station.Module;
                    if (stationTiltModule._InitStationTilt.ParametersBasic._Instrument._Model != null)
                    {
                        stationTiltModule.SetPointsToMeasure(item.MagnetsToBeAligned);
                        if (item.currentIndex == 1)
                        {
                            item.CurrentStep.View.MustBeUpdated = true;
                            item.CurrentStep.Enter();
                        }
                    }
                }
                if (item._ActiveStationModule is Line.Station.Module)
                {
                    Line.Station.Module stationLineModule = item._ActiveStationModule as Line.Station.Module;
                    if (stationLineModule.ecartoInstrument != null)
                    {
                        CloneableList<E.Point> allPoints = new CloneableList<E.Point>();
                        foreach (E.Point pt in item.PointsToBeAligned)
                        {
                            pt.LGCFixOption = ENUM.LgcPointOption.VXY;
                            int indexCala = item.CalaPoints.FindIndex(x => x._Name == pt._Name && x._Origin == pt._Origin);
                            if (indexCala != -1) item.CalaPoints.RemoveAt(indexCala);
                        }
                        allPoints.AddRange(item.CalaPoints);
                        allPoints.AddRange(item.PointsToBeAligned);
                        stationLineModule.UpdateCalaVSPointToBeAligned(allPoints);
                        stationLineModule.SetPointsToMeasure(allPoints);
                        if (item.currentIndex > 1) item.currentIndex = 1;
                        if (item.currentIndex == 1)
                        {
                            item.CurrentStep.View.MustBeUpdated = true;
                            item.CurrentStep.Enter();
                        }
                    }
                }
                if (item._ActiveStationModule is Level.Station.Module)
                {
                    Level.Station.Module stationLevelModule = item._ActiveStationModule as Level.Station.Module;
                    foreach (E.Point pt in item.PointsToBeAligned)
                    {
                        pt.LGCFixOption = ENUM.LgcPointOption.VZ;
                        int indexCala = item.CalaPoints.FindIndex(x => x._Name == pt._Name && x._Origin == pt._Origin);
                        if (indexCala != -1) item.CalaPoints.RemoveAt(indexCala);
                    }
                    if (stationLevelModule._AllerStationLevel._Parameters._Instrument._Model != null)
                    {
                        CloneableList<E.Point> allPoints = new CloneableList<E.Point>();
                        allPoints.AddRange(item.CalaPoints);
                        allPoints.AddRange(item.PointsToBeAligned);
                        stationLevelModule.UpdateCalaVSPointToBeAligned(allPoints);
                        stationLevelModule._WorkingStationLevel = stationLevelModule._AllerStationLevel;
                        //stationLevelModule.SetPointsToMeasure(allPoints);
                        stationLevelModule._WorkingStationLevel.CleanAllCalaCalculation();
                        stationLevelModule.UpdateMeasPointList();
                        stationLevelModule.ResetAndCreateRetourAndReprise();
                        if (item.currentIndex > 2) item.currentIndex = 2;
                        if (item.currentIndex == 2)
                        {
                            item.CurrentStep.View.MustBeUpdated = true;
                            item.CurrentStep.Enter();
                        }
                    }
                }
                if (item._ActiveStationModule is Length.Station.Module)
                {
                    Length.Station.Module stationLengthModule = item._ActiveStationModule as Length.Station.Module;
                    foreach (E.Point pt in item.PointsToBeAligned)
                    {
                        pt.LGCFixOption = ENUM.LgcPointOption.POIN;
                        int indexCala = item.CalaPoints.FindIndex(x => x._Name == pt._Name && x._Origin == pt._Origin);
                        if (indexCala != -1) item.CalaPoints.RemoveAt(indexCala);
                    }
                    if (stationLengthModule._LengthStation._Parameters._Instrument._Model != null)
                    {
                        CloneableList<E.Point> allPoints = new CloneableList<E.Point>();
                        allPoints.AddRange(item.CalaPoints);
                        allPoints.AddRange(item.PointsToBeAligned);
                        stationLengthModule.SetPointsToMeasure(allPoints.Clone());
                        stationLengthModule.UpdateCalaVSPointToBeAligned(allPoints);
                        stationLengthModule.UpdateOffsets(true, false);
                        if (item.currentIndex > 2) item.currentIndex = 2;
                        if (item.currentIndex == 2)
                        {
                            item.CurrentStep.View.MustBeUpdated = true;
                            item.CurrentStep.Enter();
                        }
                    }
                }
                if (item._ActiveStationModule is Polar.Station.Module) // a way to known what king of guigede module we deal with
                {
                    // at this point, MagnetsToBeAligned and PointsToBeAligned are already in the final module guided implantation...but if you want a reaction when it change you can add code here.

                    Polar.Station.Module stationTheodoliteModule = item._ActiveStationModule as Polar.Station.Module;
                    if (stationTheodoliteModule._Station.ParametersBasic._IsSetup)
                        item.MoveToStep("Abstract" + Polar.GuidedModules.Steps.Measurement.Types.PreAlignment);

                    ///Completer pour ajouter les elements à aligner dans le station theodolite module
                }
            }
        }

        internal static void ResetPointToMeasureInStations(Step s,
            CloneableList<E.Point> allPointsToBeAlign, List<E.Composites.Magnet> allMagnets)
        {
            Group.Module GrGm = s.GuidedModule.ParentModule as Group.Module;
            foreach (Module item in GrGm.SubGuidedModules)
            {
                item.MagnetsToBeAligned = allMagnets;
                item.PointsToBeAligned = allPointsToBeAlign;
                ///Enlève toutes les mesures reçues car un calcul doit de nouveau être fait
                item.ReceivedMeasures.Clear();
                if (item._ActiveStationModule is Tilt.Station.Module)
                {
                    Tilt.Station.Module stationTiltModule = item._ActiveStationModule as Tilt.Station.Module;
                    if (stationTiltModule._InitStationTilt.ParametersBasic._Instrument._Model != null)
                    {
                        stationTiltModule.SetPointsToMeasure(item.MagnetsToBeAligned);
                        if (item.currentIndex == 1)
                        {
                            if (item.CurrentStep.View != null) item.CurrentStep.View.MustBeUpdated = true;
                            item.CurrentStep.Enter();
                        }
                    }
                }
                if (item._ActiveStationModule is Line.Station.Module)
                {
                    Line.Station.Module stationLineModule = item._ActiveStationModule as Line.Station.Module;
                    if (stationLineModule.ecartoInstrument != null)
                    {
                        CloneableList<E.Point> allPoints = new CloneableList<E.Point>();
                        foreach (E.Point pt in item.PointsToBeAligned)
                        {
                            pt.LGCFixOption = ENUM.LgcPointOption.VXY;
                            int indexCala = item.CalaPoints.FindIndex(x => x._Name == pt._Name && x._Origin == pt._Origin);
                            if (indexCala != -1) item.CalaPoints.RemoveAt(indexCala);
                        }
                        allPoints.AddRange(item.CalaPoints);
                        allPoints.AddRange(item.PointsToBeAligned);
                        stationLineModule.UpdateCalaVSPointToBeAligned(allPoints);
                        stationLineModule.SetPointsToMeasure(allPoints);
                        if (item.currentIndex > 1) item.currentIndex = 1;
                        if (item.currentIndex == 1)
                        {
                            if (item.CurrentStep.View != null) item.CurrentStep.View.MustBeUpdated = true;
                            item.CurrentStep.Enter();
                        }
                    }
                }
                if (item._ActiveStationModule is Level.Station.Module && item.guideModuleType == ENUM.GuidedModuleType.AlignmentLevelling)
                {
                    Level.Station.Module stationLevelModule = item._ActiveStationModule as Level.Station.Module;
                    foreach (E.Point pt in item.PointsToBeAligned)
                    {
                        pt.LGCFixOption = ENUM.LgcPointOption.VZ;
                        int indexCala = item.CalaPoints.FindIndex(x => x._Name == pt._Name && x._Origin == pt._Origin);
                        if (indexCala != -1) item.CalaPoints.RemoveAt(indexCala);
                    }
                    if (stationLevelModule._AllerStationLevel._Parameters._Instrument._Model != null && stationLevelModule._defaultStaff != null)
                    {
                        CloneableList<E.Point> allPoints = new CloneableList<E.Point>();
                        allPoints.AddRange(item.CalaPoints);
                        allPoints.AddRange(item.PointsToBeAligned);
                        stationLevelModule.UpdateCalaVSPointToBeAligned(allPoints);
                        stationLevelModule.SetPointsToMeasure(allPoints);
                        stationLevelModule._WorkingStationLevel = stationLevelModule._AllerStationLevel;
                        //stationLevelModule.SetPointsToMeasure(allPoints);
                        stationLevelModule._WorkingStationLevel.CleanAllCalaCalculation();
                        stationLevelModule.UpdateMeasPointList();
                        stationLevelModule.ResetAndCreateRetourAndReprise();
                        if (item.currentIndex > 2) item.currentIndex = 2;
                        if (item.currentIndex == 2)
                        {
                            if (item.CurrentStep.View != null) item.CurrentStep.View.MustBeUpdated = true;
                            item.CurrentStep.Enter();
                        }
                    }
                }
                if (item._ActiveStationModule is Level.Station.Module && item.guideModuleType == ENUM.GuidedModuleType.AlignmentLevellingCheck)
                {
                    Level.Station.Module stationLevelModule = item._ActiveStationModule as Level.Station.Module;
                    foreach (E.Point pt in item.PointsToBeAligned)
                    {
                        pt.LGCFixOption = ENUM.LgcPointOption.VZ;
                        int indexCala = item.CalaPoints.FindIndex(x => x._Name == pt._Name && x._Origin == pt._Origin);
                        if (indexCala != -1) item.CalaPoints.RemoveAt(indexCala);
                    }
                    if (stationLevelModule._AllerStationLevel._Parameters._Instrument._Model != null && stationLevelModule._defaultStaff != null)
                    {
                        CloneableList<E.Point> allPoints = new CloneableList<E.Point>();
                        allPoints.AddRange(item.CalaPoints);
                        allPoints.AddRange(item.PointsToBeAligned);
                        stationLevelModule.UpdateCalaVSPointToBeAligned(allPoints);
                        stationLevelModule.SetPointsToMeasure(allPoints);
                        stationLevelModule._WorkingStationLevel = stationLevelModule._AllerStationLevel;
                        //stationLevelModule.SetPointsToMeasure(allPoints);
                        stationLevelModule._WorkingStationLevel.CleanAllCalaCalculation();
                        stationLevelModule.UpdateMeasPointList();
                        stationLevelModule.ResetAndCreateRetourAndReprise();
                        if (item.currentIndex > 2) item.currentIndex = 2;
                        if (item.currentIndex == 2)
                        {
                            if (item.CurrentStep.View != null) item.CurrentStep.View.MustBeUpdated = true;
                            item.CurrentStep.Enter();
                        }
                    }
                }
                if (item._ActiveStationModule is Length.Station.Module)
                {
                    Length.Station.Module stationLengthModule = item._ActiveStationModule as Length.Station.Module;
                    foreach (E.Point pt in item.PointsToBeAligned)
                    {
                        pt.LGCFixOption = ENUM.LgcPointOption.POIN;
                        int indexCala = item.CalaPoints.FindIndex(x => x._Name == pt._Name && x._Origin == pt._Origin);
                        if (indexCala != -1) item.CalaPoints.RemoveAt(indexCala);
                    }
                    if (stationLengthModule._LengthStation._Parameters._Instrument._Model != null)
                    {
                        CloneableList<E.Point> allPoints = new CloneableList<E.Point>();
                        allPoints.AddRange(item.CalaPoints);
                        allPoints.AddRange(item.PointsToBeAligned);
                        stationLengthModule.SetPointsToMeasure(allPoints.Clone());
                        stationLengthModule.UpdateCalaVSPointToBeAligned(allPoints);
                        stationLengthModule.UpdateOffsets(true, false);
                        if (item.currentIndex > 1) item.currentIndex = 1;
                        if (item.currentIndex == 1)
                        {
                            if (item.CurrentStep.View != null) item.CurrentStep.View.MustBeUpdated = true;
                            item.CurrentStep.Enter();
                        }
                    }
                }
                if (item._ActiveStationModule is Polar.Station.Module) // a way to known what king of guigede module we deal with
                {
                    // at this point, MagnetsToBeAligned and PointsToBeAligned are already in the final module guided implantation...but if you want a reaction when it change you can add code here.

                    Polar.Station.Module stationTheodoliteModule = item._ActiveStationModule as Polar.Station.Module;
                    if (stationTheodoliteModule._Station.ParametersBasic._IsSetup)
                        item.MoveToStep("Abstract" + Polar.GuidedModules.Steps.Measurement.Types.PreAlignment);

                    ///Completer pour ajouter les elements à aligner dans le station theodolite module
                }
            }
        }
        /// <summary>
        /// Choose Magnets from theorietcal file, it uses a modified "choose elements" step 
        /// </summary>
        /// <param name="gm"></param>
        /// <returns></returns>
        internal static Step ChooseMagnetsToAlign(Module guidedModule)
        {
            Step s = ChooseElement(guidedModule);

            s._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringAlignment_SelectElement);
            s.Utility = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringAlignment_SelectElement);


            void OnElementManagerChanged(object source, ModuleChangeEventArgs args)
            {
                s.ControlChange();
            }

            s.FirstUse += delegate (object source, StepEventArgs e)
            {
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {

                e.ElementManager._SelectedObjects.Clear();
                e.ElementManager.MultiSelection = true;
                e.ElementManager.View.CheckBoxesVisible = true;
                e.ElementManager.SetSelectableToAllMagnets();
                e.step.View.AddControl(e.guidedModule._ElementManager);
                e.ElementManager.AddSelectedObjects(e.guidedModule.MagnetsToBeAligned.Cast<TsuObject>().ToList());
                e.ElementManager.View.buttons.ShowTypes.Available = true;
                e.ElementManager.View.buttons.ShowPiliers.Available = false;
                e.ElementManager.View.buttons.ShowPoints.Available = false;
                e.ElementManager.View.buttons.ShowMagnets.Available = true;
                e.ElementManager.View.buttons.ShowAll.Available = false;
                e.ElementManager.View.buttons.ShowAlesages.Available = false;
                e.ElementManager.View.buttons.clean.Available = false;
                e.ElementManager.View.buttons.add.Available = false;
                e.ElementManager.View.buttons.importRefenceFile.Available = true;

                e.ElementManager.UpdateView();
            };

            s.ControlChanged += delegate (object source, StepEventArgs e)
            {
                CloneableList<E.Point> allPoints = s.GuidedModule._ElementManager.GetPointsInSelectedObjects().Clone();
                CloneableList<E.Composites.Magnet> allMagnets = s.GuidedModule._ElementManager.GetMagnetsInSelectedObjects().Clone();
                ResetPointToMeasureInStations(s, allPoints, allMagnets);
            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                e.step.Test();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = false;
                e.step.MessageFromTest = R.String_GM_MsgBlockMagnet;
                e.step.CheckValidityOf(e.ElementManager._SelectedObjects.Count() >= 1);
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                e.step.View.RemoveControl(e.guidedModule._ElementManager);
                Module gm = e.guidedModule;
                e.step.Cancel = false;
                foreach (var item in gm.PointsToBeAligned)
                {
                    bool alreadyAdded = false; // TODO : dont ask if already asked, PS: I dont know how to know that in this module
                    if (!alreadyAdded)
                    {
                        if (item.SerialNumber != "" && item._Point == "E")
                        {
                            string serailNumberWithoutLast4Chars = item.SerialNumber.Substring(0, item.SerialNumber.Length - 4);
                            string titleAndMessage = string.Format("{1} {0}?", R.T_M_SERIAL_CHECK, item._ClassAndNumero);

                            MessageTsu.ShowMessageWithTextBox(titleAndMessage, null, out _, out string textInput, serailNumberWithoutLast4Chars, false);

                            if (item.SerialNumber != textInput)
                            {
                                string cancel = R.T_CANCEL;
                                MessageInput mi = new MessageInput(MessageType.Critical, R.T_M_WRONG_SERIAL_NUMBER)
                                {
                                    ButtonTexts = new List<string> { R.T_I_KNOW, cancel }
                                };
                                if (mi.Show().TextOfButtonClicked == cancel)
                                {
                                    e.step.Cancel = true;
                                    e.step.Enter();
                                    return;
                                }
                            }
                        }
                    }
                }
            };

            return s;
        }
        /// <summary>
        /// Allows to select a file and show it contents
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        static public Step ChooseAlignmentTheoreticalFile(Module guidedModule)
        {
            Step s = ChooseTheoreticalFile(guidedModule);

            s.Leaving += delegate (object source, StepEventArgs e)
            {
                /// set same element in all element manager of guided module
                Group.Module GrGm = e.guidedModule.ParentModule as Group.Module;
                for (int i = 1; i <= GrGm.SubGuidedModules.Count - 1; i++)
                {
                    (GrGm.SubGuidedModules[i] as Module)._ElementManager.AllElements = e.guidedModule._ElementManager.AllElements;
                }
            };
            return s;
        }
        static public Step ChooseALignmentAdministrativeParameters(Module guidedModule)
        {
            Step s = new Step(guidedModule, R.String_GM_AdministrativeAlignmentParameters);
            BigButton buttonSelectTheoElementFile = new BigButton();
            BigButton buttonImportCSVOffsetsDisplacement = new BigButton();
            BigButton buttonSelectOperation = new BigButton();
            BigButton buttonCreateOperation = new BigButton();
            TextBox textBoxTeam = new TextBox();
            TextBox textBoxTemperature = new TextBox();
            Font TitleFont = Tsunami2.Preferences.Theme.Fonts.Normal;
            string operationName = "...";
            string team = "";
            string temperature = "";
            string openFilesDescription = "";
            if (guidedModule.RabotFiles.Count > 0) openFilesDescription += string.Join("\n", guidedModule.RabotFiles);
            string importCSVDescription = openFilesDescription == "" ? R.T_WITH_EXPECTED_OFFSETS_OR_RELATIVE_DISPLACEMENT : openFilesDescription;
            string csvTitleAndDescription = $"{R.T_IMPORT_A_CSV_FILE};{importCSVDescription}";


            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                // add a button to directly browse for theoretical file
                e.step.View.AddButton(
                    string.Format(R.String_GM_SelectTheoElementFile, "..."),
                    R.Open,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Module gm = args2.TsuObject as Module;
                        gm._ElementManager.MultiSelection = false;
                        gm._ElementManager.View.CheckBoxesVisible = false;
                        gm._ElementManager.SetSelectableToAllTheoreticalPoints();
                        gm._ElementManager.View.buttons.HideAllButtons();
                        gm._ElementManager.View.TryOpen();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectTheoElementFile = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectTheoElementFile.SetColors(Tsunami2.Preferences.Theme.Colors.Bad);
                // add a button to create operation
                e.step.View.AddButton(
                    csvTitleAndDescription,
                    R.Operation_Add,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Console.WriteLine("Importing CSV points regarding offsets and displacements");
                        string openedFile = CsvImporter.ImportRabotCSV(guidedModule);
                        //update button text
                        if (openedFile != "" && !csvTitleAndDescription.Contains(openedFile))
                            csvTitleAndDescription += $"\r\n{openedFile}";
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonImportCSVOffsetsDisplacement = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonImportCSVOffsetsDisplacement.SetColors(Tsunami2.Preferences.Theme.Colors.Attention);
                // add a button to select operation
                e.step.View.AddButton(
                    string.Format(R.String_GM_SelectOperation, operationName),
                    R.Operation,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        e.OperationManager.SelectOperation();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectOperation = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Attention);
                // add a button to create operation
                e.step.View.AddButton(
                    string.Format(R.String_GM_CreateOperation, operationName),
                    R.Operation_Add,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Module gm = args2.TsuObject as Module;
                        e.OperationManager.View.AddNew();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonCreateOperation = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonCreateOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Attention);
                /// ajout temperature ambiante
                e.step.View.AddTitle(R.String_GM__AmbientTemperature, "temperatureLabel");
                e.step.View.AddTextBox(guidedModule._TsuView, temperature);
                textBoxTemperature = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as TextBox;
                textBoxTemperature.BackColor = Tsunami2.Preferences.Theme.Colors.Attention;
                /// ajout nom de l'équipe
                e.step.View.AddTitle(R.String_GM_TeamLabel, "teamLabel");
                e.step.View.AddTextBox(guidedModule._TsuView, team);
                textBoxTeam = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as TextBox;
                textBoxTeam.BackColor = Tsunami2.Preferences.Theme.Colors.Bad;
                textBoxTeam.Enter += delegate
                {
                    textBoxTeam.SelectAll();
                };
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                Group.Module GrGm = e.guidedModule.ParentModule as Group.Module;
                Module Gm = e.guidedModule as Module;
                //e.step.Change();
                //e.OperationManager.OperationTree.ExpandAll();
                e.OperationManager._SelectedObjects.Clear();
                O.Operation operation;
                operation = e.OperationManager.AllElements.Find(x => x._Name == GrGm.SubGuidedModules[1]._ActiveStationModule._Station.ParametersBasic._Operation._Name) as O.Operation;

                e.OperationManager._SelectedObjectInBlue = operation;
                e.OperationManager.AddSelectedObjects(operation);
                e.OperationManager.UpdateView();
                if (GrGm.SubGuidedModules[3]._ActiveStationModule is Line.Station.Module)
                {
                    temperature = (GrGm.SubGuidedModules[3]._ActiveStationModule as Line.Station.Module).WorkingAverageStationLine._Parameters._Temperature.ToString("F1", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                }
                textBoxTemperature.Text = temperature;
                if (GrGm.SubGuidedModules[1]._ActiveStationModule._Station.ParametersBasic._Team != R.String_UnknownTeam)
                {
                    team = GrGm.SubGuidedModules[1]._ActiveStationModule._Station.ParametersBasic._Team;
                    textBoxTeam.Text = team;
                }
                else
                {
                    textBoxTeam.Text = "";
                }
                operationName = GrGm.SubGuidedModules[1]._ActiveStationModule._Station.ParametersBasic._Operation.ToString();
                textBoxTeam.Focus();
                textBoxTeam.SelectAll();
                textBoxTeam.Focus();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = false;
                bool stepValid = true;
                bool stepIsSkipable = false;
                string msgBlocked = R.String_GM_MsgBlockAdministrative;
                Group.Module GrGm = e.guidedModule.ParentModule as Group.Module;
                ///Vérification si fichier theo sélectionné
                ///Pas besoin de le mettre dans les autres sub guided module car element manager commun
                if (e.ElementManager.GetAllTheoreticalPoints().Count > 0)
                {
                    buttonSelectTheoElementFile.SetColors(Tsunami2.Preferences.Theme.Colors.Good);
                    buttonSelectTheoElementFile.ChangeImage(R.StatusGood);
                    buttonSelectTheoElementFile.ChangeNameAndDescription(string.Format(R.String_GM_SelectTheoElementFile, (e.ElementManager.AllElements[0] as E.Element)._Origin));
                }
                else
                {
                    msgBlocked += " " + R.String_GM_MsgBlockTheoFile;
                    stepValid = false;
                }

                buttonImportCSVOffsetsDisplacement.ChangeNameAndDescription(csvTitleAndDescription);
                if (guidedModule.VerticalRabot.Count > 0 || guidedModule.RadialRabot.Count > 0)
                {
                    buttonImportCSVOffsetsDisplacement.SetColors(Tsunami2.Preferences.Theme.Colors.Good);
                    buttonImportCSVOffsetsDisplacement.ChangeImage(R.StatusGood);
                    //buttonImportCSVOffsetsDisplacement.ChangeNameAndDescription("Import a CSV file with expected offsets or relative displacement; 2/2 Files imported");
                }
                //else if (guidedModule.VerticalRabot.Count > 0 && guidedModule.RadialRabot.Count == 0)
                //{
                //    buttonImportCSVOffsetsDisplacement.ChangeNameAndDescription("Import a CSV file with expected offsets or relative displacement; Vertical Rabot file imported");
                //}

                //else if (guidedModule.VerticalRabot.Count == 0 && guidedModule.RadialRabot.Count > 0)
                //{
                //    buttonImportCSVOffsetsDisplacement.ChangeNameAndDescription("Import a CSV file with expected offsets or relative displacement; Radial Rabot file imported");
                //}

                //Vérification si opération sélectionnée, si 0 met bouton en red
                string oldOperationName = operationName;
                if (e.OperationManager.SelectedOperation.IsSet) //&& (e.OperationManager.SelectedOperation.value != 0))
                {
                    if (GrGm.SubGuidedModules[1]._ActiveStationModule._Station.ParametersBasic._Operation != null)
                    {
                        buttonSelectOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Good);
                        buttonSelectOperation.ChangeImage(R.StatusGood);
                        operationName = e.OperationManager.SelectedOperation.ToString();
                        buttonSelectOperation.ChangeNameAndDescription(string.Format(R.String_GM_SelectOperation, operationName));
                        buttonCreateOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Good);
                        buttonCreateOperation.ChangeImage(R.StatusGood);
                        buttonCreateOperation.ChangeNameAndDescription(string.Format(R.String_GM_CreateOperation, operationName));

                    }
                }
                else
                {
                    if (GrGm.SubGuidedModules[1]._ActiveStationModule._Station.ParametersBasic._Operation != null)
                    {
                        if (e.OperationManager._SelectedObjects.Count > 0)
                        {
                            buttonSelectOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Attention);
                            buttonSelectOperation.ChangeImage(R.Operation);
                            operationName = e.OperationManager.SelectedOperation.ToString();
                            buttonSelectOperation.ChangeNameAndDescription(string.Format(R.String_GM_SelectOperation, operationName));
                            buttonCreateOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Attention);
                            buttonCreateOperation.ChangeImage(R.Operation_Add);
                            buttonCreateOperation.ChangeNameAndDescription(string.Format(R.String_GM_CreateOperation, operationName));
                            stepIsSkipable = true;
                        }
                    }
                }
                //Met à jour les opérations si l'opération a changé
                if (oldOperationName != operationName)
                {
                    foreach (Module item in GrGm.SubGuidedModules)
                    {
                        item.OperationManager = e.OperationManager;
                        var asm = item._ActiveStationModule;
                        if (asm is Tilt.Station.Module) { (asm as Tilt.Station.Module).ChangeOperationID(e.OperationManager.SelectedOperation); }
                        if (asm is Line.Station.Module) { (asm as Line.Station.Module).ChangeOperationID(e.OperationManager.SelectedOperation); }
                        if (asm is Level.Station.Module) { (asm as Level.Station.Module).ChangeOperationID(e.OperationManager.SelectedOperation); }
                        if (asm is Polar.Station.Module) { (asm as Polar.Station.Module).ChangeOperationID(e.OperationManager.SelectedOperation); }
                        if (asm is Length.Station.Module) { (asm as Length.Station.Module).ChangeOperationID(e.OperationManager.SelectedOperation); }
                    }
                }
                //Vérifie que la temperature contient bien un nombre double
                double newTemperature = T.Conversions.Numbers.ToDouble(textBoxTemperature.Text, true, -9999);
                if (newTemperature != -9999)
                {
                    textBoxTemperature.BackColor = Tsunami2.Preferences.Theme.Colors.Good;
                }
                else
                {
                    stepValid = false;
                    if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                    msgBlocked += " " + R.String_GM_MsgBlockTemperature;
                    textBoxTemperature.BackColor = Tsunami2.Preferences.Theme.Colors.Bad;
                }
                //Vérifie que le nom d'équipe contient que des caractères alphanumériques et est de maximum 8 caractères
                string tempTeam = textBoxTeam.Text;
                string oldTeam = team;
                if (System.Text.RegularExpressions.Regex.IsMatch(tempTeam, @"^[a-zA-Z]+$") && (tempTeam.Length < 9 || tempTeam == R.String_UnknownTeam))
                {
                    tempTeam = tempTeam.ToUpper();
                    team = tempTeam;
                    textBoxTeam.BackColor = Tsunami2.Preferences.Theme.Colors.Good;
                }
                else
                {
                    stepValid = false;
                    textBoxTeam.BackColor = Tsunami2.Preferences.Theme.Colors.Bad;
                    if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                    msgBlocked += " " + R.String_GM_MsgBlockTeam;
                }
                if (oldTeam != team)
                {
                    foreach (Module item in GrGm.SubGuidedModules)
                    {
                        if (item._ActiveStationModule is Tilt.Station.Module) { (item._ActiveStationModule as Tilt.Station.Module).ChangeTeam(team); }
                        if (item._ActiveStationModule is Line.Station.Module) { (item._ActiveStationModule as Line.Station.Module).ChangeTeam(team); }
                        if (item._ActiveStationModule is Level.Station.Module) { (item._ActiveStationModule as Level.Station.Module).ChangeTeam(team); }
                        if (item._ActiveStationModule is Polar.Station.Module) { (item._ActiveStationModule as Polar.Station.Module)._Station.ParametersBasic._Team = team; }
                        if (item._ActiveStationModule is Length.Station.Module) { (item._ActiveStationModule as Length.Station.Module).ChangeTeam(team); }
                    }
                }
                e.guidedModule.CurrentStep.MessageFromTest = msgBlocked;
                e.step.CheckValidityOf(stepValid);
            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {
                Group.Module GrGm = e.guidedModule.ParentModule as Group.Module;
                double newTemperature = T.Conversions.Numbers.ToDouble(textBoxTemperature.Text, true, -9999);
                if (newTemperature != -9999)
                {
                    if (GrGm.SubGuidedModules[3]._ActiveStationModule is Line.Station.Module)
                    {
                        (GrGm.SubGuidedModules[3]._ActiveStationModule as Line.Station.Module).ChangeTemperature(newTemperature);
                    }
                    if (GrGm.SubGuidedModules[2]._ActiveStationModule is Level.Station.Module)
                    {
                        (GrGm.SubGuidedModules[2]._ActiveStationModule as Level.Station.Module).ChangeTemperature(newTemperature);
                    }
                    if (GrGm.SubGuidedModules[6]._ActiveStationModule is Level.Station.Module)
                    {
                        (GrGm.SubGuidedModules[6]._ActiveStationModule as Level.Station.Module).ChangeTemperature(newTemperature);
                    }
                }
            };
            return s;
        }
        // New step
        //internal static GS.Step TdHReduction(Module gm)
        //{
        //    GS.Step s = new GS.Step(gm,R.T_TdHCheckpointTitle);

        //    Stations.Theodolite.StationTheodoliteModule stm = (s.GuidedModule._ActiveStationModule as Stations.Theodolite.StationTheodoliteModule);
        //    S.StationTheodolite st = stm._TheodoliteStation;
        //    Stations.Theodolite.StationTheodoliteModuleView v = stm.View;

        //    #region Step Events

        //    #region FirstUse

        //    s.FirstUse += delegate(object source, GS.StepEventArgs e)
        //    {
        //        // Get some view controls
        //        e.step.skipable = true;

        //        e.step.View.ModifyLabel(R.T_TdhFace1NotOk, "state");
        //        e.step.View.ModifyLabel(R.T_TdHCheckpoint, "next");

        //        BigButton quit = new BigButton(R.T_TdhQuit,R.StatusBad, Tsunami2.Preferences.Theme._Colors.Bad);
        //        quit.BigButtonClicked += delegate
        //        {
        //            if (s.View.ShowMessageOfCritical(R.T_TdhQuitConfirm, R.T_NO, R.T_YES) == R.T_YES)
        //                for (int i = 0; i < e.guidedModule.Steps.Count; i++)
        //                {
        //                    if (e.guidedModule.Steps[i].TAG != null)
        //                        if (e.guidedModule.Steps[i].TAG.ToString() == "Export")
        //                        {
        //                            e.guidedModule.MoveToStep(i);
        //                            break;
        //                        }
        //                }
        //        };
        //        e.step.View.AddButton(quit);
        //    };

        //    #endregion

        //    #region Entering

        //    s.Entering += delegate(object source, GS.StepEventArgs e)
        //    {

        //    };

        //    #endregion

        //    #region Updating

        //    s.Updating += delegate(object source, GS.StepEventArgs e)
        //    {

        //    };

        //    #endregion

        //    #region Leaving

        //    s.Leaving += delegate(object source, GS.StepEventArgs e)
        //    {

        //    };

        //    #endregion

        //    #region Testing

        //    s.Testing += delegate(object source, GS.StepEventArgs e)
        //    {

        //    };

        //    #endregion

        //    #region Desappearing

        //    s.Desappearing += delegate(object source, GS.StepEventArgs e)
        //    {
        //    };

        //    #endregion

        //    #endregion

        //    return s;
        //}
    }
}
