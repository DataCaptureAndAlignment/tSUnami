﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml.Serialization;
using TSU.Common.Compute.Compensations;
using TSU.Common.Guided.Alignment;
using TSU.ENUM;
using TSU.Level.GuidedModules;
using TSU.Level.GuidedModules.Steps;
using TSU.Line.GuidedModules;
using TSU.Polar.GuidedModules.Steps;
using TSU.Views;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;

namespace TSU.Common.Guided.Group
{
    [Serializable]
    [XmlType("GuidedGroupModule")]
    public class Module : FinalModule
    {
        public GuidedModuleType Type;

        [XmlIgnore]
        public Guided.Module ActiveSubGuidedModule;
        public int activeSubGuidedModuleIndex = -1;
        [XmlIgnore]
        public string savePath = "";

        public List<Guided.Module> SubGuidedModules { get; set; } = new List<Guided.Module>();
        //{
        //    get
        //    {
        //        List<Guided.Module> l = new List<Guided.Module>();
        //        foreach (M.Module item in this.childModules)
        //            if (item is Guided.Module) l.Add(item as Guided.Module);
        //        return l;
        //    }
        //    set
        //    {
        //        List<Guided.Module> l = new List<Guided.Module>();
        //        this.childModules.Clear();
        //        foreach (Guided.Module item in value)
        //        {
        //            this.childModules.Add(item);
        //        }
        //    }
        //}

        public override string ToString()
        {
            return $"GGM: {_Name}, {SubGuidedModules.Count} Sub GM";
        }

        public Module()
        {
            //ShowSaveMessageOfSuccess = new TsuBool();
        }

        public Module(TSU.Module parentModule, string name)
            : base(parentModule, name)
        {
            //ShowSaveMessageOfSuccess = new TsuBool();
        }

        public override void Dispose()
        {
            if (CompositeView != null)
                CompositeView.Dispose();


            base.Dispose();
        }



        /// <summary>
        /// Actions à faire lors ouverture d'un fichier xml de sauvegarde
        /// </summary>
        internal override void ReCreateWhatIsNotSerialized(bool saveSomeMemory = false)
        {
            try
            {
                base.ReCreateWhatIsNotSerialized(saveSomeMemory);



                foreach (Guided.Module gm in SubGuidedModules)
                {
                    try
                    {
                        gm.ParentModule = this;
                        gm.ReCreateWhatIsNotSerialized(saveSomeMemory);

                        gm._TsuView.ShowDockedFill();
                        ///Pour le module guidé alignement, il faut remettre l'operation dans l'opération manager
                        if (gm.guideModuleType == GuidedModuleType.AlignmentManager)
                        {
                            gm.OperationManager._SelectedObjects.Clear();
                            O.Operation operation;
                            try
                            {
                                operation = gm.OperationManager.AllElements.Find(x => x._Name == SubGuidedModules[1]._ActiveStationModule._Station.ParametersBasic._Operation._Name) as O.Operation;

                            }
                            catch (Exception)
                            {
                                operation = new O.Operation(9999, "lost");
                            }
                            gm.OperationManager._SelectedObjectInBlue = operation;
                            gm.OperationManager.AddSelectedObjects(operation);
                        }
                        gm.Start();
                        gm.MoveToStep(gm.currentIndex);
                    }
                    catch (Exception ex)
                    {
                        string titleAndMessage = $"{gm} could not be load;{ex.Message}";
                        new MessageInput(MessageType.Critical, titleAndMessage).Show();
                    }

                }


                (View as View).SetRightActionAndImageToButtons(this, SubGuidedModules[0].guideModuleType);

                if (activeSubGuidedModuleIndex != -1 && activeSubGuidedModuleIndex < SubGuidedModules.Count)
                {
                    SetActiveGuidedModule(SubGuidedModules[activeSubGuidedModuleIndex]);
                }
                else
                {
                    SetActiveGuidedModule(SubGuidedModules[0]);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Cannot re-create what was not serialized in {_Name}", ex);
            }
        }
        /// <summary>
        /// Crèe la station suivante dans la séquence de nivellement
        /// </summary>
        internal void GoNextLevelSquence()
        {
            /// bouton aller à la sequence suivante
            Guided.Module oldGm = ActiveSubGuidedModule;
            Level.Station.Module oldStationModule = oldGm._ActiveStationModule as Level.Station.Module;
            if (oldStationModule._SequenceSelected.Elements.Count != 0)
            {
                AddNewLevellingModule();
                Guided.Module newGm = ActiveSubGuidedModule;
                Level.Station.Module newStationModule = newGm._ActiveStationModule as Level.Station.Module;
                newStationModule.ChangeOperationID(oldStationModule._AllerStationLevel.ParametersBasic._Operation);
                newStationModule.ChangeTeam(oldStationModule._AllerStationLevel.ParametersBasic._Team);
                newStationModule.SetGeodeFilePathInNewStationLevelModule(oldStationModule);
                newStationModule.SetInstrumentInNewStationLevelModule(oldStationModule);
                newStationModule.SetDefaultStaffInNewStationLevelModule(oldStationModule);
                newStationModule.SetSecondaryStaffInNewStationLevelModule(oldStationModule);
                newStationModule.SetCoordinateSystemInNewStationLevelModule(oldStationModule);
                newStationModule.SetNextElementSequenceInNewStationModule(oldStationModule);
                newStationModule.View.MaximizeDataGridView();
                newGm.MoveToStep(4);
            }
        }
        /// <summary>
        /// Finish the guided group module
        /// </summary>
        internal new void FinishModule()
        {
            Finished = true;
            _TsuView.Hide();
            if (ParentModule is Tsunami)
            {
                (ParentModule as Tsunami).Menu.SetAnotherModuleAsActive(this);
                (ParentModule as Tsunami).Menu.RefreshOpenedAndFinishedModule();
                if (ActiveSubGuidedModule != null) ActiveSubGuidedModule.Save();
            }
        }
        /// <summary>
        /// mise à jour point qui a été modifié dans autre élément manager
        /// </summary>
        /// <param name="originalPoint"></param>
        /// <param name="pointModified"></param>
        internal override void UpdateTheoCoordinates(E.Point originalPoint, E.Point pointModified)
        {
            base.UpdateTheoCoordinates(originalPoint, pointModified);
            foreach (Guided.Module item in SubGuidedModules)
            {
                item.UpdateTheoCoordinates(originalPoint, pointModified);
            }
        }
        /// <summary>
        /// Efface le actual level subGuided module
        /// </summary>
        internal void DeleteActualSubGuidedModule()
        {
            if (ActiveSubGuidedModule._ActiveStationModule != null)
            {
                int index = activeSubGuidedModuleIndex;
                if (SubGuidedModules.Count != 1)
                {
                    SubGuidedModules.Remove(ActiveSubGuidedModule);
                    if (index > 0) { index -= 1; }
                    else { index = 0; }
                    SetActiveGuidedModule(SubGuidedModules[index]);
                }
            }
        }

        internal void DoLevelCalculationWithLGC2()
        {
            Level.Station.Module stationLevelModule = ActiveSubGuidedModule._ActiveStationModule as Level.Station.Module;
            string filepath = string.Format("{0}{1}{2}_{3}.inp", Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, "LGC_Global\\", "GlobalLevelling", TSU.Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now));
            if (!System.IO.Directory.Exists(Tsunami2.Preferences.Values.Paths.MeasureOfTheDay)) { System.IO.Directory.CreateDirectory(Tsunami2.Preferences.Values.Paths.MeasureOfTheDay); }
            if (!System.IO.Directory.Exists(string.Format("{0}{1}", Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, "LGC_Global\\"))) { System.IO.Directory.CreateDirectory(string.Format("{0}{1}", Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, "LGC_Global\\")); }
            if (System.IO.File.Exists(filepath))
            {
                string titleAndMessage = string.Format(R.StringLevel_LGCReplaceFile, filepath);
                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_OK, R.T_DONT }
                };
                string respond = mi.Show().TextOfButtonClicked;
            }
            Lgc2 lgc2 = new Lgc2(this, ActiveSubGuidedModule.guideModuleType, filepath);
            lgc2.Run(true, true);
            List<E.Point> pointList = lgc2._Output.GetResultsForGuidedGroupModule(this, ActiveSubGuidedModule.guideModuleType);
            if (pointList.Count != 0)
            {
                //string saveName = this._ActiveStationModule._Name;
                //this._ActiveStationModule._Name = "GlobalLevellingLGC2Calculation";
                foreach (E.Point item in pointList)
                {
                    //item._Origin = "GlobalLevellingLGC2Calculation";
                    _ElementManager.RemoveElementByNameAndOrigin(item._Name, "GlobalLevellingLGC2Calculation");
                    _ElementManager.AddElement("GlobalLevellingLGC2Calculation", item);
                }
                _ElementManager.View.UpdateView();
                Save();
                //this._ActiveStationModule._Name = saveName;
            }
        }
        /// <summary>
        /// Do a global LGC calculation to do an average for an level alignment using several station
        /// </summary>
        /// <param name="dataGridViewMoveLevel"></param>
        internal void DoLevelCalculationWithLGC2ForAlignment()
        {
            Level.Station.Module stationLevelModule = ActiveSubGuidedModule._ActiveStationModule as Level.Station.Module;
            string filepath = string.Format("{0}{1}{2}_{3}.inp", Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, "LGC_Global\\", "GlobalLevelling", TSU.Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now));
            if (!System.IO.Directory.Exists(Tsunami2.Preferences.Values.Paths.MeasureOfTheDay)) { System.IO.Directory.CreateDirectory(Tsunami2.Preferences.Values.Paths.MeasureOfTheDay); }
            if (!System.IO.Directory.Exists(string.Format("{0}{1}", Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, "LGC_Global\\"))) { System.IO.Directory.CreateDirectory(string.Format("{0}{1}", Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, "LGC_Global\\")); }
            if (System.IO.File.Exists(filepath))
            {
                string titleAndMessage = string.Format(R.StringLevel_LGCReplaceFile, filepath);
                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_OK, R.T_DONT }
                };
                string respond = mi.Show().TextOfButtonClicked;
            }
            Lgc2 lgc2 = new Lgc2(this, ActiveSubGuidedModule.guideModuleType, filepath);
            lgc2.Run(true, true);
            List<E.Point> pointList = lgc2._Output.GetResultsForGuidedGroupModule(this, ActiveSubGuidedModule.guideModuleType);
            List<string> listPointNames = new List<string>();
            List<string> listDcum = new List<string>();
            List<string> listMoveToDo = new List<string>();
            List<string> listTheoReading = new List<string>();
            List<string> listExtension = new List<string>();
            List<string> listRawReading = new List<string>();
            if (pointList.Count != 0)
            {
                foreach (M.MeasureOfLevel meas in stationLevelModule._WorkingStationLevel._MeasureOfLevel)
                {
                    if (meas._Point.LGCFixOption != LgcPointOption.CALA)
                    {
                        double hMes;
                        double hTheo;
                        E.Point ptTheo = stationLevelModule._TheoPoint.Find(x => x._Name == meas._PointName);
                        switch (stationLevelModule._WorkingStationLevel._Parameters._ZType)
                        {
                            case CoordinatesType.CCS:
                                hTheo = ptTheo._Coordinates.Ccs.Z.Value;
                                break;
                            case CoordinatesType.SU:
                                hTheo = ptTheo._Coordinates.Local.Z.Value;
                                break;
                            //case CoordinatesType.UserDefined:
                            //    hTheo = ptTheo._Coordinates.UserDefined.Z.Value;
                            //    break;
                            default:
                                hTheo = ptTheo._Coordinates.Ccs.Z.Value;
                                break;
                        }
                        int index;
                        index = pointList.FindIndex(x => x._Name == meas._PointName);
                        if (index != -1)
                        {
                            ///Si pas de lecture sur le point, on ne l'affiche pas dans le tableau
                            if (meas._RawLevelReading != na)
                            {
                                switch (stationLevelModule._WorkingStationLevel._Parameters._ZType)
                                {
                                    case CoordinatesType.CCS:
                                        hMes = pointList[index]._Coordinates.Ccs.Z.Value;
                                        break;
                                    case CoordinatesType.SU:
                                        hMes = pointList[index]._Coordinates.Local.Z.Value;
                                        break;
                                    default:
                                        hMes = pointList[index]._Coordinates.Ccs.Z.Value;
                                        break;
                                }
                                double move = hTheo - hMes;
                                double theoReading = meas._RawLevelReading - move;
                                listPointNames.Add(meas._PointName + " ");
                                //Ajoute la Dcum
                                if (meas._Point._Parameters.Cumul != Tsunami2.Preferences.Values.na)
                                {
                                    listDcum.Add(meas._Point._Parameters.Cumul.ToString("F3", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                                }
                                else
                                {
                                    listDcum.Add("");
                                }
                                listMoveToDo.Add(Math.Round(move * 100000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " ");
                                listTheoReading.Add(Math.Round(theoReading * 100000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                                listExtension.Add(Math.Round(meas._Extension * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                                listRawReading.Add(Math.Round(meas._RawLevelReading * 100000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                            }
                        }
                    }
                }
                //Efface les résultats des calculs précédent dans les tableaux de tous les steps
                foreach (Steps.Step step in ActiveSubGuidedModule.Steps)
                {
                    if (step != null)
                    {
                        if (step.View != null)
                        {
                            if (step.View.dataGridViewMoveLevel != null)
                            {
                                step.View.dataGridViewMoveLevel.Rows.Clear();
                            }
                        }
                    }
                }
                FillDataGridMoveLevel(ActiveSubGuidedModule.CurrentStep.View.dataGridViewMoveLevel, listPointNames, listDcum, listTheoReading, listMoveToDo, listExtension, listRawReading);
            }
        }

        private void FillDataGridMoveLevel(DataGridView dataGridViewMoveLevel, List<string> listPointNames, List<string> listDcum, List<string> listTheoReading, List<string> listMoveToDo, List<string> listExtension, List<string> listRawReading)
        {
            DataGridUtility.SaveSorting(dataGridViewMoveLevel);
            dataGridViewMoveLevel.Rows.Clear();
            List<List<object>> listRow = new List<List<object>>();
            int actualLineIndex = 0;
            List<int> index = new List<int>();
            foreach (string item in listPointNames)
            {
                AddNewMoveRowInDataGridView(listRow, actualLineIndex, listPointNames, listDcum, listTheoReading, listMoveToDo, listExtension, listRawReading);
                actualLineIndex++;
            }
            foreach (List<object> ligne in listRow)
            {
                dataGridViewMoveLevel.Rows.Add(ligne.ToArray());
            }
            for (int i = 0; i < dataGridViewMoveLevel.RowCount; i++)
            {
                dataGridViewMoveLevel.Rows[i].Height = 30;
            }
            DataGridUtility.RestoreSorting(dataGridViewMoveLevel);
            dataGridViewMoveLevel.Refresh();
        }
        /// <summary>
        /// Add a new row of move measure in the datagridview
        /// </summary>
        /// <param name="listRow"></param>
        /// <param name="item"></param>
        private void AddNewMoveRowInDataGridView(List<List<object>> listRow, int index, List<string> listPointNames, List<string> listDcum, List<string> listTheoReading, List<string> listMoveToDo, List<string> listExtension, List<string> listRawReading)
        {
            List<object> row = new List<object>();
            row.Add(""); // 0 Point Name
            row.Add(""); // 1 Dcum
            row.Add(""); // 2 Initial Theo reading
            row.Add(""); // 3 Initial Move
            row.Add(""); // 4 Initial Extension
            row.Add(""); // 5 Actual Extension
            row.Add(""); // 6 Actual Reading
            row.Add(""); // 7 Actual theo reading
            row.Add(""); // 8 Actual Move
            listRow.Add(row);
            listRow[listRow.Count - 1][0] = listPointNames[index];
            listRow[listRow.Count - 1][1] = listDcum[index];
            listRow[listRow.Count - 1][2] = listTheoReading[index];
            listRow[listRow.Count - 1][3] = listMoveToDo[index];
            listRow[listRow.Count - 1][4] = listExtension[index];
            listRow[listRow.Count - 1][5] = listExtension[index];
            listRow[listRow.Count - 1][6] = listRawReading[index];
            listRow[listRow.Count - 1][7] = listTheoReading[index];
            listRow[listRow.Count - 1][8] = listMoveToDo[index];
        }
        internal void GoPreviousLevelSequence()
        {
            /// bouton aller à la sequence précédente
            /// this.CheckBlueElevation();
            Guided.Module oldGm = ActiveSubGuidedModule;
            Level.Station.Module oldStationModule = oldGm._ActiveStationModule as Level.Station.Module;
            if (oldStationModule._SequenceSelected.Elements.Count != 0)
            {
                AddNewLevellingModule();
                Guided.Module newGm = ActiveSubGuidedModule;
                Level.Station.Module newStationModule = newGm._ActiveStationModule as Level.Station.Module;
                newStationModule.ChangeOperationID(oldStationModule._AllerStationLevel.ParametersBasic._Operation);
                newStationModule.ChangeTeam(oldStationModule._AllerStationLevel.ParametersBasic._Team);
                newStationModule.SetGeodeFilePathInNewStationLevelModule(oldStationModule);
                newStationModule.SetInstrumentInNewStationLevelModule(oldStationModule);
                newStationModule.SetDefaultStaffInNewStationLevelModule(oldStationModule);
                newStationModule.SetSecondaryStaffInNewStationLevelModule(oldStationModule);
                newStationModule.SetCoordinateSystemInNewStationLevelModule(oldStationModule);
                newStationModule.SetPreviousElementSequenceInNewStationModule(oldStationModule);
                newStationModule.View.MaximizeDataGridView();
                newGm.MoveToStep(4);
            }
        }
        /// <summary>
        /// Fait le nivellement retour de la station précédente
        /// </summary>
        internal void DoLevelReturnPreviousStation()
        {
            Guided.Module gm = ActiveSubGuidedModule;
            Level.Station.Module levelStationModule = gm._ActiveStationModule as Level.Station.Module;
            int i = SubGuidedModules.IndexOf(gm);
            if (i > 0)
            {
                SetActiveGuidedModule(SubGuidedModules[i - 1]);
                SubGuidedModules[i - 1].MoveToStep(5);
            }
            else
            {
                new MessageInput(MessageType.Warning, R.StringCheminement_ErrorPreviousReturnStation)
                {
                    ButtonTexts = new List<string> { R.T_OK + "!" }
                }.Show();
            }
        }

        /// <summary>
        /// Clonage de la station actuelle de cheminement avec les mêmes paramètres administratifs
        /// </summary>
        internal void CloneLevelling()
        {
            Guided.Module oldGm = ActiveSubGuidedModule;
            AddNewLevellingModule();
            Guided.Module newGm = ActiveSubGuidedModule;
            // garder l'ordre de la liste
            try
            {
                newGm._ElementManager.View.currentStrategy.CloneListOrder(oldGm._ElementManager.View.currentStrategy);
            }
            catch (Exception)
            {

            }
            newGm._ElementManager.AllElements = oldGm._ElementManager.AllElements;
            Level.Station.Module newStationModule = newGm._ActiveStationModule as Level.Station.Module;
            Level.Station.Module oldStationModule = oldGm._ActiveStationModule as Level.Station.Module;
            newStationModule.ChangeOperationID(oldStationModule._AllerStationLevel.ParametersBasic._Operation);
            newStationModule.ChangeTeam(oldStationModule._AllerStationLevel.ParametersBasic._Team);
            newStationModule.SetGeodeFilePathInNewStationLevelModule(oldStationModule);
            newStationModule.SetInstrumentInNewStationLevelModule(oldStationModule);
            newStationModule.SetDefaultStaffInNewStationLevelModule(oldStationModule);
            newStationModule.SetSecondaryStaffInNewStationLevelModule(oldStationModule);
            newStationModule.CreatedOn = DateTime.Now;
            newStationModule.ChangeTolerance(oldStationModule._WorkingStationLevel._Parameters._Tolerance);
            newStationModule.ChangeTemperature(oldStationModule._WorkingStationLevel._Parameters._Temperature);
            newStationModule.SetCoordinateSystemInNewStationLevelModule(oldStationModule);
            newStationModule.View.MaximizeDataGridView();
            //si pas de fichiers de séquence va au step de sélection des points
            if (newGm._ElementManager.GetAllSequenceNiv().Count == 0)
            {
                newGm.Steps[2].Visible = false;
            }
            else
            {
                newGm.Steps[2].Visible = true;
            }
            newGm.MoveToStep(2);

        }
        /// <summary>
        /// Fait le contrôle de marche avec les cotes bleues (différence de lecture de mire
        /// </summary>
        internal string CheckBlueElevation(bool showCancelButton = false)
        {
            Guided.Module gm = ActiveSubGuidedModule;
            string respond = "";
            if (gm != null)
            {
                Level.Station.Module levelStationModule = gm._ActiveStationModule as Level.Station.Module;
                if (levelStationModule != null)
                {
                    int index = SubGuidedModules.FindIndex(x => x.Guid == gm.Guid);
                    if (index != -1)
                    {
                        //Cheminement
                        if (SubGuidedModules[index].guideModuleType == GuidedModuleType.Cheminement)
                        {
                            if (index < SubGuidedModules.Count - 1)
                            {
                                if (index == 0 && SubGuidedModules[index + 1].guideModuleType == GuidedModuleType.Cheminement)
                                {
                                    // première station compare uniquement avec la suivante
                                    respond = levelStationModule.CheckBlueElevation(null, SubGuidedModules[index + 1]._ActiveStationModule as Level.Station.Module, index + 1, showCancelButton);
                                }
                                else
                                {
                                    if (SubGuidedModules[index + 1].guideModuleType == GuidedModuleType.Cheminement
                                        && SubGuidedModules[index - 1].guideModuleType == GuidedModuleType.Cheminement)
                                    {
                                        //station intermédiaire fait une comparaison avec précédente et suivante
                                        respond = levelStationModule.CheckBlueElevation(SubGuidedModules[index - 1]._ActiveStationModule as Level.Station.Module, SubGuidedModules[index + 1]._ActiveStationModule as Level.Station.Module, index + 1, showCancelButton);
                                    }

                                }
                            }
                            else
                            {
                                //dernière station, fait une comparaison avec la précédente
                                if (index != 0 && SubGuidedModules[index - 1].guideModuleType == GuidedModuleType.Cheminement)
                                {
                                    respond = levelStationModule.CheckBlueElevation(SubGuidedModules[index - 1]._ActiveStationModule as Level.Station.Module, null, index + 1, showCancelButton);
                                }
                            }
                        }
                        //Element alignment
                        if (SubGuidedModules[index].guideModuleType == GuidedModuleType.AlignmentLevelling)
                        {
                            respond = levelStationModule.CheckBlueElevation(null, SubGuidedModules[6]._ActiveStationModule as Level.Station.Module, 1, showCancelButton);
                        }
                        if (SubGuidedModules[index].guideModuleType == GuidedModuleType.AlignmentLevellingCheck)
                        {
                            respond = levelStationModule.CheckBlueElevation(SubGuidedModules[2]._ActiveStationModule as Level.Station.Module, null, 2, showCancelButton);
                        }
                    }
                }
            }
            return respond;
        }
        /// <summary>
        /// Clonage du fil existant avec les mêmes paramètres administratifs
        /// </summary>
        internal void CloneWire()
        {
            Guided.Module gm = ActiveSubGuidedModule;
            AddNewEcartometryModule();
            Guided.Module newGm = ActiveSubGuidedModule;
            // garder l'ordre de la liste
            try
            {
                newGm._ElementManager.View.currentStrategy.CloneListOrder(gm._ElementManager.View.currentStrategy);
            }
            catch (Exception)
            {

            }
            Line.Station.Module newStationLineModule = newGm._ActiveStationModule as Line.Station.Module;
            Line.Station.Module oldStationLineModule = gm._ActiveStationModule as Line.Station.Module;
            newStationLineModule.ChangeOperation(oldStationLineModule._Station.ParametersBasic._Operation);
            newStationLineModule.ChangeTeam(oldStationLineModule._Station.ParametersBasic._Team);
            newStationLineModule.SetGeodeFilePathInNewStationLineModule(oldStationLineModule);
            newStationLineModule.SetInstrumentInNewStationLineModule(oldStationLineModule);
            newStationLineModule.CreatedOn = DateTime.Now;
            newStationLineModule.ChangeTolerance(oldStationLineModule.WorkingAverageStationLine._Parameters._Tolerance);
            newStationLineModule.ChangeTemperature(oldStationLineModule.WorkingAverageStationLine._Parameters._Temperature);
            newStationLineModule.View.MaximizeDataGridView();
            //si pas de fichiers de séquence va au step de sélection des points
            if (newGm._ElementManager.GetAllSequenceFil().Count == 0)
            {
                newGm.Steps[2].Visible = false;
            }
            else
            {
                newGm.Steps[2].Visible = true;
            }
            newGm.MoveToStep(2);
        }
        /// <summary>
        /// Launch a libr calculation for several wire in a guided ecartometry module, if input manually updated by user, coordinates could be added to element manager
        /// </summary>
        internal void DoLibrWireCalculationWithLGC2()
        {
            Line.Station.Module stationLineModule = ActiveSubGuidedModule._ActiveStationModule as Line.Station.Module;
            string filepath = string.Format("{0}{1}{2}_{3}.inp", Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, "LGC_Global\\", "GlobalEcartometry", TSU.Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now));
            if (!System.IO.Directory.Exists(string.Format("{0}{1}", Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, "LGC_Global\\"))) { System.IO.Directory.CreateDirectory(string.Format("{0}{1}", Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, "LGC_Global\\")); }
            if (System.IO.File.Exists(filepath))
            {
                string titleAndMessage = string.Format(R.StringLine_LGCReplaceFile, filepath);
                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_OK, R.T_DONT }
                };
                string respond = mi.Show().TextOfButtonClicked;
                if (respond == R.T_DONT) return;
            }
            Lgc2 lgc2 = new Lgc2(this, ActiveSubGuidedModule.guideModuleType, filepath, "Free Wire Compensation");
            lgc2.Run(showInput: false, showOutput: true);
            List<E.Point> pointList = lgc2._Output.GetResultsForGuidedGroupModule(this, ActiveSubGuidedModule.guideModuleType);
            if (pointList.Count != 0)
            {
                //string saveName = this._ActiveStationModule._Name;
                //this._ActiveStationModule._Name = "GlobalEcartometryLGC2Calculation";
                foreach (E.Point item in pointList)
                {
                    //item._Origin = "GlobalEcartometryLGC2Calculation";
                    _ElementManager.RemoveElementByNameAndOrigin(item._Name, "GlobalEcartometryLGC2Calculation");
                    item.StorageStatus = Tsunami.StorageStatusTypes.Keep;
                    _ElementManager.AddElement("GlobalEcartometryLGC2Calculation", item);
                }
                _ElementManager.View.UpdateView();
                Save();
                //this._ActiveStationModule._Name = saveName;
            }
        }
        /// <summary>
        /// Sauvegarde en dat geode toutes les stations de nivellement dans le cheminement
        /// </summary>
        internal void ExportAllLevelDat()
        {
            bool saveAllSuccess = true;
            string respond = "";
            string geodeFilePath = "";
            foreach (Guided.Module gm in SubGuidedModules)
            {
                try
                {
                    if (gm._ActiveStationModule is Level.Station.Module)
                    {
                        Level.Station.Module stationLevelModule = gm._ActiveStationModule as Level.Station.Module;
                        geodeFilePath = stationLevelModule._WorkingStationLevel.ParametersBasic._GeodeDatFilePath;
                        string activeRepriseStationName = "";
                        //Pour garder en mémoire la station reprise active
                        if (stationLevelModule._LevelingDirectionStrategy is Level.Station.RepriseStrategy)
                        {
                            activeRepriseStationName = stationLevelModule._WorkingStationLevel._Name;
                        }
                        if (stationLevelModule._AllerStationLevel._Parameters._State is Station.State.StationLevelReadyToBeSaved)
                        {
                            //stationLevelModule.ShowSaveMessageOfSuccess.IsTrue = ShowSaveMessageOfSuccess.IsTrue;
                            stationLevelModule._WorkingStationLevel = stationLevelModule._AllerStationLevel;
                            stationLevelModule.ExportToGeode();
                            if (stationLevelModule._WorkingStationLevel.ParametersBasic._GeodeDatFilePath != "")
                                geodeFilePath = stationLevelModule._WorkingStationLevel.ParametersBasic._GeodeDatFilePath;
                            //if (!ShowSaveMessageOfSuccess.IsTrue) { ShowSaveMessageOfSuccess.IsTrue = stationLevelModule.ShowSaveMessageOfSuccess.IsTrue; }
                        }
                        else
                        {
                            if (stationLevelModule._AllerStationLevel._Parameters._Instrument._Model != null
                                && !(stationLevelModule._AllerStationLevel._Parameters._State is Station.State.StationLevelSaved)
                                && stationLevelModule._AllerStationLevel._Parameters._Team == R.String_UnknownTeam)
                            {
                                string titleAndMessage = string.Format(R.StringAlignment_CannotSaveLevel, stationLevelModule._AllerStationLevel._Name);
                                new MessageInput(MessageType.Warning, titleAndMessage).Show();
                                saveAllSuccess = false;
                            }
                        }
                        if (stationLevelModule._RetourStationLevel._Parameters._State is Station.State.StationLevelReadyToBeSaved)
                        {
                            //stationLevelModule.ShowSaveMessageOfSuccess.IsTrue = ShowSaveMessageOfSuccess.IsTrue;
                            stationLevelModule._WorkingStationLevel = stationLevelModule._RetourStationLevel;
                            stationLevelModule.ExportToGeode();
                            if (stationLevelModule._WorkingStationLevel.ParametersBasic._GeodeDatFilePath != "")
                                geodeFilePath = stationLevelModule._WorkingStationLevel.ParametersBasic._GeodeDatFilePath;
                            //if (!ShowSaveMessageOfSuccess.IsTrue) { ShowSaveMessageOfSuccess.IsTrue = stationLevelModule.ShowSaveMessageOfSuccess.IsTrue; }
                        }
                        else
                        {
                            if (stationLevelModule._RetourStationLevel._Parameters._Instrument._Model != null
                                && !(stationLevelModule._RetourStationLevel._Parameters._State is Station.State.StationLevelSaved)
                                && stationLevelModule._RetourStationLevel._Parameters._Team == R.String_UnknownTeam)
                            {
                                string titleAndMessage = string.Format(R.StringAlignment_CannotSaveLevel, stationLevelModule._RetourStationLevel._Name);
                                new MessageInput(MessageType.Warning, titleAndMessage).Show();
                                saveAllSuccess = false;
                            }
                        }
                        if (stationLevelModule._RepriseStationLevel.Count > 0)
                        {

                            foreach (Level.Station reprise in stationLevelModule._RepriseStationLevel)
                            {
                                if (reprise != null)
                                {
                                    if (reprise._Parameters._State is Station.State.StationLevelReadyToBeSaved)
                                    {
                                        //stationLevelModule.ShowSaveMessageOfSuccess.IsTrue = ShowSaveMessageOfSuccess.IsTrue;
                                        stationLevelModule._WorkingStationLevel = reprise;
                                        stationLevelModule.ExportToGeode();
                                        if (stationLevelModule._WorkingStationLevel.ParametersBasic._GeodeDatFilePath != "")
                                            geodeFilePath = stationLevelModule._WorkingStationLevel.ParametersBasic._GeodeDatFilePath;
                                        //if (!ShowSaveMessageOfSuccess.IsTrue) { ShowSaveMessageOfSuccess.IsTrue = stationLevelModule.ShowSaveMessageOfSuccess.IsTrue; }
                                    }
                                    else
                                    {
                                        if (reprise._Parameters._Instrument._Model != null
                                            && !(reprise._Parameters._State is Station.State.StationLevelSaved)
                                            && reprise._Parameters._Team == R.String_UnknownTeam)
                                        {
                                            string titleAndMessage = string.Format(R.StringAlignment_CannotSaveLevel, reprise._Name);
                                            new MessageInput(MessageType.Warning, titleAndMessage).Show();
                                            saveAllSuccess = false;
                                        }
                                    }
                                }
                            }
                        }
                        if (stationLevelModule._LevelingDirectionStrategy is Level.Station.AllerStrategy)
                        {
                            stationLevelModule._WorkingStationLevel = stationLevelModule._AllerStationLevel;
                        }
                        if (stationLevelModule._LevelingDirectionStrategy is Level.Station.RetourStrategy)
                        {
                            stationLevelModule._WorkingStationLevel = stationLevelModule._RetourStationLevel;
                        }
                        if (stationLevelModule._LevelingDirectionStrategy is Level.Station.RepriseStrategy)
                        {
                            int i = stationLevelModule._RepriseStationLevel.FindIndex(x => x._Name == activeRepriseStationName);
                            if (i != -1)
                            {
                                stationLevelModule._WorkingStationLevel = stationLevelModule._RepriseStationLevel[i];
                            }
                            else
                            {
                                stationLevelModule._WorkingStationLevel = stationLevelModule._RepriseStationLevel[0];
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"{R.T_COULD_NOT} {R.Export} {R.T_LEVELLING}\r\n\r\n{ex.Message}");
                }
            }
            if (saveAllSuccess && geodeFilePath != "")
            {
                string titleAndMessage = string.Format(R.StringLevel_SaveAllSucces);
                MessageInput mi = new MessageInput(MessageType.GoodNews, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_OK, R.T_OpenFileLocation }
                };
                respond = mi.Show().TextOfButtonClicked;
            }
            else
            {
                if (saveAllSuccess)
                {
                    string titleAndMessage = string.Format(R.StringLevel_SaveAllSucces);
                    new MessageInput(MessageType.GoodNews, titleAndMessage).Show();
                }
            }
            if (respond == R.T_OpenFileLocation)
            {
                if (System.IO.File.Exists(geodeFilePath))
                {
                    System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo("explorer.exe", " /select, " + geodeFilePath));
                }
            }
        }
        /// <summary>
        /// Exporte tous les nouveaux points dans un cheminement dans un fichier theo dat
        /// </summary>
        internal void ExportLevelNewPointTheoDat()
        {
            if (ActiveSubGuidedModule._ActiveStationModule != null)
            {
                if (ActiveSubGuidedModule._ActiveStationModule.ElementModule != null)
                {
                    List<E.Point> allPoints = new List<E.Point>();
                    List<E.Point> newCreatedByUserPoints = new List<E.Point>();
                    List<E.Point> actualStationPoints = new List<E.Point>();
                    List<E.Point> lgcGlobalPoints = new List<E.Point>();
                    allPoints = ActiveSubGuidedModule._ElementManager.GetPointsInAllElements();
                    newCreatedByUserPoints = allPoints.FindAll(x => x._Origin == "Created by user");
                    if (newCreatedByUserPoints != null)
                    {
                        ///Remplace les nouveaux points par les points calculés dans la station actuelle si disponible
                        string groupName = (ActiveSubGuidedModule._ActiveStationModule as Level.Station.Module).measGroupName;
                        groupName = groupName.Replace('.', '_');
                        foreach (E.Point item in newCreatedByUserPoints)
                        {
                            E.Point pt = allPoints.Find(x => x._Origin == groupName && x._Name == item._Name);
                            if (pt != null)
                            {
                                actualStationPoints.Add(pt);
                            }
                            else
                            {
                                ///il faut un H au point pour réimporter les points dans Tsunami 
                                E.Point itemWithH = item.DeepCopy();
                                if (item._Coordinates.Ccs.Z.Value == Preferences.Preferences.NotAvailableValueNa)
                                {
                                    itemWithH._Coordinates.Ccs.Z.Value = 400;
                                }
                                actualStationPoints.Add(itemWithH);
                            }
                        }
                        ///Remplace les nouveaux points par les points calculés dans un calcul LGC global si disponible
                        foreach (E.Point item in actualStationPoints)
                        {
                            E.Point pt = allPoints.Find(x => x._Origin == "GlobalLevellingLGC2Calculation" && x._Name == item._Name);
                            if (pt != null)
                            {
                                lgcGlobalPoints.Add(pt);
                            }
                            else
                            {
                                lgcGlobalPoints.Add(item);
                            }
                        }
                        string path = TsuPath.GetFileNameToSave("", string.Format("{0:yyyyMMdd}", DateTime.Now) + "_Elements", "Geode(*.dat) | *.dat", "");
                        if (path != "")
                        {
                            IO.SUSoft.Geode.Export(path, lgcGlobalPoints);
                            Shell.Run(path);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Exporte tous les nouveaux points d'un module guidé ecarto dans un fichier theo dat
        /// </summary>
        internal void ExportWireNewPointTheoDat()
        {
            if (ActiveSubGuidedModule._ActiveStationModule != null)
            {
                if (ActiveSubGuidedModule._ActiveStationModule.ElementModule != null)
                {
                    List<E.Point> allPoints = new List<E.Point>();
                    List<E.Point> newCreatedByUserPoints = new List<E.Point>();
                    List<E.Point> actualStationPoints = new List<E.Point>();
                    List<E.Point> lgcGlobalPoints = new List<E.Point>();
                    allPoints = ActiveSubGuidedModule._ElementManager.GetPointsInAllElements();
                    newCreatedByUserPoints = allPoints.FindAll(x => x._Origin == "Created by user");
                    if (newCreatedByUserPoints != null)
                    {
                        ///Remplace les nouveaux points par les points calculés dans la station actuelle si disponible
                        string groupName = (ActiveSubGuidedModule._ActiveStationModule as Line.Station.Module).measPointgroupName;
                        groupName = groupName.Replace('.', '_');
                        foreach (E.Point item in newCreatedByUserPoints)
                        {
                            E.Point pt = allPoints.Find(x => x._Origin == groupName && x._Name == item._Name && x._Coordinates.HasCcs);
                            if (pt != null)
                            {
                                actualStationPoints.Add(pt);
                            }
                            else
                            {
                                ///il faut un H au point pour réimporter les points dans Tsunami 
                                E.Point itemWithH = item.DeepCopy();
                                if (item._Coordinates.Ccs.Z.Value == Preferences.Preferences.NotAvailableValueNa)
                                {
                                    itemWithH._Coordinates.Ccs.Z.Value = 400;
                                }
                                actualStationPoints.Add(itemWithH);
                            }
                        }
                        ///Remplace les nouveaux points par les points calculés dans un calcul LGC global si disponible
                        foreach (E.Point item in actualStationPoints)
                        {
                            E.Point pt = allPoints.Find(x => x._Origin == "GlobalLevellingLGC2Calculation" && x._Name == item._Name && x._Coordinates.HasCcs);
                            if (pt != null)
                            {
                                lgcGlobalPoints.Add(pt);
                            }
                            else
                            {
                                if (item._Coordinates.HasCcs) lgcGlobalPoints.Add(item);
                            }
                        }
                        if (lgcGlobalPoints.Count > 0)
                        {
                            string path = TsuPath.GetFileNameToSave("", string.Format("{0:yyyyMMdd}", DateTime.Now) + "_Elements", "Geode(*.dat) | *.dat", "");
                            if (path != "")
                            {
                                IO.SUSoft.Geode.Export(path, lgcGlobalPoints);
                                Shell.Run(path);
                            }
                        }
                        else
                        {
                            new MessageInput(MessageType.Warning, R.StringLine_CannotExportPointInTheoDat).Show();
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Export de tous les fils dans un module d'écartométrie
        /// </summary>
        internal void ExportAllWiresDat()
        {
            bool saveAllSuccess = true;
            string respond = "";
            string geodeFilePath = "";
            int wireNumber = 0;
            foreach (Guided.Module gm in SubGuidedModules)
            {
                try
                {
                    if (gm._ActiveStationModule is Line.Station.Module)
                    {
                        Line.Station.Module stationLineModule = gm._ActiveStationModule as Line.Station.Module;
                        geodeFilePath = stationLineModule.WorkingAverageStationLine.ParametersBasic._GeodeDatFilePath;
                        wireNumber++;
                        //if (item.CheckIfReadyToBeSaved())
                        if (stationLineModule.WorkingAverageStationLine._Parameters._State is Station.State.WireReadyToBeSaved)
                        {
                            //stationLineModule.ShowSaveMessageOfSuccess.IsTrue = ShowSaveMessageOfSuccess.IsTrue;
                            stationLineModule.ExportToGeode(false);
                            stationLineModule.View.RedrawTreeviewParameters();
                            stationLineModule.View.UpdateView();
                            if (stationLineModule.WorkingAverageStationLine.ParametersBasic._GeodeDatFilePath != "") geodeFilePath = stationLineModule.WorkingAverageStationLine.ParametersBasic._GeodeDatFilePath;
                            //if (!ShowSaveMessageOfSuccess.IsTrue) { ShowSaveMessageOfSuccess.IsTrue = stationLineModule.ShowSaveMessageOfSuccess.IsTrue; }
                        }
                        else
                        {
                            if (stationLineModule.ecartoInstrument != null
                                && !(stationLineModule.WorkingAverageStationLine._Parameters._State is Station.State.WireSaved)
                                && stationLineModule.WorkingAverageStationLine._Parameters._Team == R.String_UnknownTeam)
                            {
                                string titleAndMessage = string.Format(R.StringLine_CannotSaveWire, wireNumber);
                                new MessageInput(MessageType.Warning, titleAndMessage).Show();
                                saveAllSuccess = false;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"{R.T_COULD_NOT} {R.Export} {R.T_ECARTO_OFFSET}\r\n\r\n{ex.Message}");
                }
            }
            if (saveAllSuccess && geodeFilePath != "")
            {
                string titleAndMessage = string.Format(R.StringLine_SaveAllSuccess);
                MessageInput mi = new MessageInput(MessageType.GoodNews, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_OK, R.T_OpenFileLocation }
                };
                respond = mi.Show().TextOfButtonClicked;
            }
            else
            {
                if (saveAllSuccess)
                {
                    string titleAndMessage = string.Format(R.StringLine_SaveAllSuccess);
                    new MessageInput(MessageType.GoodNews, titleAndMessage).Show();
                }
            }
            if (respond == R.T_OpenFileLocation)
            {
                if (System.IO.File.Exists(geodeFilePath))
                {
                    System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo("explorer.exe", " /select, " + geodeFilePath));
                }
            }
        }

        public event EventHandler<GroupGuidedModuleEventArgs> GuidedModuleAdded;
        public event EventHandler<GroupGuidedModuleEventArgs> ActiveGuidedModuleChanged;

        public override void Add(TSU.Module module)
        {
            base.Add(module);
            SubGuidedModules.Add(module as Guided.Module);
            ActiveSubGuidedModule = module as Guided.Module;
            activeSubGuidedModuleIndex = SubGuidedModules.FindIndex(x => x == module);
            if (GuidedModuleAdded != null) GuidedModuleAdded(this, new GroupGuidedModuleEventArgs() { GuidedModule = ActiveSubGuidedModule });
            View.UpdateView();
        }

        internal void SetActiveGuidedModule(Guided.Module module)
        {
            (View as Group.View).SetActiveGuidedModule(module);
        }
        internal void ActionSetActiveGuidedModule(Guided.Module module)
        {
            BroadcastMessage("Retreiving active guided module");
            if (ActiveSubGuidedModule != null) ActiveSubGuidedModule.UnsubscribeStepsEvents();
            ///Pour inscrire et deésincrire l'instrument manager
            if (module.guideModuleType == GuidedModuleType.AlignmentEcartometry
                || module.guideModuleType == GuidedModuleType.AlignmentLevelling
                || module.guideModuleType == GuidedModuleType.AlignmentLevellingCheck
                || module.guideModuleType == GuidedModuleType.AlignmentTilt
                || module.guideModuleType == GuidedModuleType.Cheminement
                || module.guideModuleType == GuidedModuleType.Ecartometry
                || module.guideModuleType == GuidedModuleType.SurveyTilt
                || module.guideModuleType == GuidedModuleType.AlignmentLengthSetting
                || module.guideModuleType == GuidedModuleType.LengthSetting
                || module.guideModuleType == GuidedModuleType.AlignmentImplantation)
            {

                BroadcastMessage("Link related instrument in station module");
                module.SetInstrumentInActiveStationModule();
            }

            BroadcastMessage("Subscribing to guide module events");
            module.SubscribeStepsEvents();
            ActiveSubGuidedModule = module;
            activeSubGuidedModuleIndex = SubGuidedModules.FindIndex(x => x == module);
            if (ActiveGuidedModuleChanged != null) ActiveGuidedModuleChanged(this, new GroupGuidedModuleEventArgs() { GuidedModule = module });
        }
        /// <summary>
        /// Met à jour le top button avec le nom de la station active
        /// </summary>
        internal void UpdateBigButtonTop()
        {
            (View as View).UpdateBigButtonTop();
        }
        internal void UpdateBigButtonTop(string utility)
        {
            (View as View).UpdateBigButtonTop(utility);
        }
        internal void AddNewSubModule()
        {
            //int i = this.SubGuidedModules.Count + 1;
            //string nameAndDescription = String.Format(R.String_GuidedLine_WireName, i.ToString(), "A", "B"); 
            //Guided.Module gm = new Guided.Module(this, nameAndDescription);
            //this.Add(gm);
        }

        internal void OnMeasureReceived(object sender, M.MeasurementEventArgs e)
        {
            Guided.Module admin = SubGuidedModules.FindLast(x => x.guideModuleType == GuidedModuleType.AlignmentManager);
            admin.ReceivedMeasures.Add(e.Measure);
            Guided.Module theo = SubGuidedModules.FindLast(x => x.guideModuleType == GuidedModuleType.AlignmentImplantation);
            theo.ReceivedMeasures.Add(e.Measure);
        }

        /// <summary>
        /// Create a new sub guided module and fill it with a new guided module with the operation manager, element manager and team from a master
        /// </summary>
        /// <param name="master"></param>
        /// <param name="slave"></param>
        internal void CloneSubModule(Guided.Module slave, Guided.Module master)
        {
            //slave.OperationManager.AllElements = master.OperationManager.AllElements;
            slave._ElementManager.AllElements = master._ElementManager.AllElements;
            slave._ActiveStationModule._Station.ParametersBasic._Operation = master._ActiveStationModule._Station.ParametersBasic._Operation;
            slave._ActiveStationModule._Station.ParametersBasic._Team = master._ActiveStationModule._Station.ParametersBasic._Team;
            slave._ActiveStationModule._Station.ParametersBasic._Instrument = master._ActiveStationModule._Station.ParametersBasic._Instrument;
        }
        /// <summary>
        /// Add a new guided module of ecartometry
        /// </summary>
        internal void AddNewEcartometryModule()
        {
            int i = SubGuidedModules.Count + 1;
            Guided.Module Ecartometry = GuidedEcartometry.Get(this, i);
            Ecartometry.View.buttonImage = R.Line;

            Add(Ecartometry);
            //Force la tolérance à 0.2mm pour les modules guidés
            Line.Station.Module st = Ecartometry._ActiveStationModule as Line.Station.Module;
            st.ChangeTolerance(0.0002);
        }
        /// <summary>
        /// Add a new cheminement guided module
        /// </summary>
        internal void AddNewLevellingModule()
        {
            int i = SubGuidedModules.Count + 1;
            Guided.Module Levelling = Cheminement.Get(this, i);
            Levelling.View.buttonImage = R.Level;
            Add(Levelling);
            //Force la tolérance à 0.2mm pour les modules guidés
            Level.Station.Module st = Levelling._ActiveStationModule as Level.Station.Module;
            st.ChangeTolerance(0.0002);
        }
        /// <summary>
        /// Add a new manager for alignment 2D+1
        /// </summary>
        internal void AddNewAlignmentAdminModule()
        {

            Guided.Module alignmentAdmin = Admin.Get(this, R.StringAlignment_AdminButton);
            alignmentAdmin.View.buttonImage = R.Admin;
            Add(alignmentAdmin);
        }
        /// <summary>
        /// Add a new surveyTilt
        /// </summary>
        internal void AddNewAlignmentTiltModule()
        {
            Guided.Module tilt = Tilt.GuidedModules.Steps.Alignment.Get(this, R.StringGuidedTilt_MainButton);
            tilt.View.buttonImage = R.Tilt;
            Add(tilt);
            //Force la tolérance à 0.05mm pour les modules guidés
            Tilt.Station.Module st = tilt._ActiveStationModule as Tilt.Station.Module;
            st.ChangeTolerance(0.00005);
        }
        /// <summary>
        /// Add a new cheminement guided module for element alignment
        /// </summary>
        internal void AddNewAlignmentLevellingModule()
        {
            Guided.Module Levelling = Level.GuidedModules.Steps.Alignment.Get(this);
            Levelling.View.buttonImage = R.Level;
            Add(Levelling);
            //Force la tolérance à 0.2mm pour les modules guidés
            Level.Station.Module st = Levelling._ActiveStationModule as Level.Station.Module;
            st.ChangeTolerance(0.0002);
        }
        /// <summary>
        /// Add a new cheminement guided module for element check after alignment
        /// </summary>
        internal void AddNewAlignmentLevellingCheckModule()
        {
            Guided.Module Levelling = AlignmentCheck.Get(this);
            Levelling.View.buttonImage = R.level_Check;
            Add(Levelling);
            //Force la tolérance à 0.2mm pour les modules guidés
            Level.Station.Module st = Levelling._ActiveStationModule as Level.Station.Module;
            st.ChangeTolerance(0.0002);
        }
        /// <summary>
        /// Add a new guided module of ecartometry for element alignment
        /// </summary>
        internal void AddNewAlignmentEcartometryModule()
        {
            Guided.Module Ecartometry = Line.GuidedModules.Steps.Alignment.Get(this);
            Ecartometry.View.buttonImage = R.Line;
            Add(Ecartometry);
            //Force la tolérance à 0.2mm pour les modules guidés
            Line.Station.Module st = Ecartometry._ActiveStationModule as Line.Station.Module;
            st.ChangeTolerance(0.0002);
        }
        /// <summary>
        /// Add a new guided module of ecartometry for element alignment
        /// </summary>
        internal void AddNewAlignmentLengthSettingModule()
        {
            Guided.Module LengthSetting = Length.GuidedModules.Steps.Alignment.Get(this);
            LengthSetting.View.buttonImage = R.LengthSetting;
            Add(LengthSetting);
        }
        /// <summary>
        /// Add a new guided module of theodolite for element alignment
        /// </summary>
        internal void AddNewAlignmentTheodoliteModule(Module guidedModuleGroup)
        {
            Guided.Module Theodolite = Polar.GuidedModules.Steps.Alignment.Get(this);
            Theodolite.View.buttonImage = R.Theodolite;
            Theodolite._ActiveStationModule.MeasureThreated += guidedModuleGroup.OnMeasureReceived;
            Add(Theodolite);
        }
        /// <summary>
        /// Lance la sauvegarde de tous les modules guidés dans le groupe de module guidés
        /// </summary>
        internal void SaveAll()
        {
            bool saveAllSuccess = true;
            foreach (Guided.Module item in SubGuidedModules)
            {
                try
                {
                    ///Tilt
                    if (item._ActiveStationModule is Tilt.Station.Module)
                    {
                        try
                        {
                            Tilt.Station.Module stationTiltModule = item._ActiveStationModule as Tilt.Station.Module;
                            //stationTiltModule.ShowSaveMessageOfSuccess.IsTrue = ShowSaveMessageOfSuccess.IsTrue;
                            if (!stationTiltModule.View.SaveAllTiltSequenceOnlyInActualOperation()) saveAllSuccess = false;
                            //if (!ShowSaveMessageOfSuccess.IsTrue) { ShowSaveMessageOfSuccess.IsTrue = stationTiltModule.ShowSaveMessageOfSuccess.IsTrue; }

                            //if (stationTiltModule._ActualStationTilt.TiltParameters._State is TSU.Stations.Station.State.StationTiltReadyToBeSaved)
                            //{
                            //    stationTiltModule.SaveTilt(false);
                            //}
                            //else
                            //{
                            //    if (stationTiltModule._InitStationTilt.ParametersBasic._Instrument._Model != null)
                            //    {
                            //        this.View.ShowMessageOfExclamation(R.StringAlignment_CannotSaveTilt);
                            //        saveAllSuccess = false;
                            //    }

                            //}
                        }
                        catch (Exception ex)
                        {
                            throw new Exception($"{R.T_COULD_NOT} {R.Export} Tilt\r\n\r\n{ex.Message}");
                        }
                    }

                    ///WIre
                    if (item._ActiveStationModule is Line.Station.Module)
                    {
                        try
                        {
                            Line.Station.Module stationLineModule = item._ActiveStationModule as Line.Station.Module;
                            //if (stationLineModule.CheckIfReadyToBeSaved())
                            if (stationLineModule.WorkingAverageStationLine._Parameters._State is Station.State.WireReadyToBeSaved)
                            {
                                //stationLineModule.ShowSaveMessageOfSuccess.IsTrue = ShowSaveMessageOfSuccess.IsTrue;
                                stationLineModule.ExportToGeode(false);
                                stationLineModule.View.RedrawTreeviewParameters();
                                stationLineModule.View.UpdateView();
                                //if (!ShowSaveMessageOfSuccess.IsTrue) { ShowSaveMessageOfSuccess.IsTrue = stationLineModule.ShowSaveMessageOfSuccess.IsTrue; }
                            }
                            else
                            {
                                if (stationLineModule.ecartoInstrument != null
                                    && !(stationLineModule.WorkingAverageStationLine._Parameters._State is Station.State.WireSaved)
                                    && stationLineModule.WorkingAverageStationLine._Parameters._Team == R.String_UnknownTeam)
                                {
                                    new MessageInput(MessageType.Warning, R.StringAlignment_CannotSaveWire).Show();
                                    saveAllSuccess = false;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception($"{R.T_COULD_NOT} {R.Export} {R.T_ECARTO_OFFSET}\r\n\r\n{ex.Message}");
                        }
                    }

                    ///Nivellement
                    if (item._ActiveStationModule is Level.Station.Module)
                    {
                        try
                        {
                            Level.Station.Module stationLevelModule = item._ActiveStationModule as Level.Station.Module;
                            if (stationLevelModule._AllerStationLevel._Parameters._State is Station.State.StationLevelReadyToBeSaved)
                            {
                                //stationLevelModule.ShowSaveMessageOfSuccess.IsTrue = ShowSaveMessageOfSuccess.IsTrue;
                                stationLevelModule._WorkingStationLevel = stationLevelModule._AllerStationLevel;
                                stationLevelModule.ExportToGeode();
                                //if (!ShowSaveMessageOfSuccess.IsTrue) { ShowSaveMessageOfSuccess.IsTrue = stationLevelModule.ShowSaveMessageOfSuccess.IsTrue; }
                            }
                            else
                            {
                                if (stationLevelModule._AllerStationLevel._Parameters._Instrument._Model != null
                                    && !(stationLevelModule._AllerStationLevel._Parameters._State is Station.State.StationLevelSaved)
                                    && stationLevelModule._AllerStationLevel._Parameters._Team == R.String_UnknownTeam)
                                {
                                    string titleAndMessage = string.Format(R.StringAlignment_CannotSaveLevel, stationLevelModule._AllerStationLevel._Name);
                                    new MessageInput(MessageType.Warning, titleAndMessage).Show();
                                    saveAllSuccess = false;
                                }
                            }
                            if (stationLevelModule._RetourStationLevel._Parameters._State is Station.State.StationLevelReadyToBeSaved)
                            {
                                //stationLevelModule.ShowSaveMessageOfSuccess.IsTrue = ShowSaveMessageOfSuccess.IsTrue;
                                stationLevelModule._WorkingStationLevel = stationLevelModule._RetourStationLevel;
                                stationLevelModule.ExportToGeode();
                                //if (!ShowSaveMessageOfSuccess.IsTrue) { ShowSaveMessageOfSuccess.IsTrue = stationLevelModule.ShowSaveMessageOfSuccess.IsTrue; }
                            }
                            else
                            {
                                if (stationLevelModule._RetourStationLevel._Parameters._Instrument._Model != null
                                    && !(stationLevelModule._RetourStationLevel._Parameters._State is Station.State.StationLevelSaved)
                                    && stationLevelModule._RetourStationLevel._Parameters._Team == R.String_UnknownTeam)
                                {
                                    string titleAndMessage = string.Format(R.StringAlignment_CannotSaveLevel, stationLevelModule._RetourStationLevel._Name);
                                    new MessageInput(MessageType.Warning, titleAndMessage).Show();
                                    saveAllSuccess = false;
                                }
                            }
                            if (stationLevelModule._RepriseStationLevel.Count > 0)
                            {
                                if (stationLevelModule._RepriseStationLevel[0] != null)
                                {
                                    if (stationLevelModule._RepriseStationLevel[0]._Parameters._State is Station.State.StationLevelReadyToBeSaved)
                                    {
                                        //stationLevelModule.ShowSaveMessageOfSuccess.IsTrue = ShowSaveMessageOfSuccess.IsTrue;
                                        stationLevelModule._WorkingStationLevel = stationLevelModule._RepriseStationLevel[0];
                                        stationLevelModule.ExportToGeode();
                                        // if (!ShowSaveMessageOfSuccess.IsTrue) { ShowSaveMessageOfSuccess.IsTrue = stationLevelModule.ShowSaveMessageOfSuccess.IsTrue; }
                                    }
                                    else
                                    {
                                        if (stationLevelModule._RepriseStationLevel[0]._Parameters._Instrument._Model != null
                                            && !(stationLevelModule._RepriseStationLevel[0]._Parameters._State is Station.State.StationLevelSaved)
                                            && stationLevelModule._RepriseStationLevel[0]._Parameters._Team == R.String_UnknownTeam)
                                        {
                                            string titleAndMessage = string.Format(R.StringAlignment_CannotSaveLevel, stationLevelModule._RepriseStationLevel[0]._Name);
                                            new MessageInput(MessageType.Warning, titleAndMessage).Show();
                                            saveAllSuccess = false;
                                        }
                                    }
                                }
                            }
                            if (stationLevelModule._LevelingDirectionStrategy is Level.Station.AllerStrategy)
                            {
                                stationLevelModule._WorkingStationLevel = stationLevelModule._AllerStationLevel;
                            }
                            if (stationLevelModule._LevelingDirectionStrategy is Level.Station.RetourStrategy)
                            {
                                stationLevelModule._WorkingStationLevel = stationLevelModule._RetourStationLevel;
                            }
                            if (stationLevelModule._LevelingDirectionStrategy is Level.Station.RepriseStrategy)
                            {
                                stationLevelModule._WorkingStationLevel = stationLevelModule._RepriseStationLevel[0];
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception($"{R.T_COULD_NOT} {R.Export} {R.T_LEVELLING}\r\n\r\n{ex.Message}");
                        }
                    }

                    ///Polar
                    if (item._ActiveStationModule is Polar.Station.Module) // a way to known what king of guigede module we deal with
                    {
                        try
                        {
                            Polar.Station.Module stationTheodoliteModule = item._ActiveStationModule as Polar.Station.Module;
                            StakeOut.Tools.SaveLastmeasurements(item, stationTheodoliteModule.StationTheodolite, stationTheodoliteModule, exportTogeode: true);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception($"{R.T_COULD_NOT} {R.Export} {R.T_POLAR_MEASUREMENTS}\r\n\r\n{ex.Message}");
                        }
                    }

                    ///Length Setting
                    if (item._ActiveStationModule is Length.Station.Module)
                    {
                        try
                        {
                            Length.Station.Module stationLengthModule = item._ActiveStationModule as Length.Station.Module;
                            if (stationLengthModule._LengthStation._Parameters._State is Station.State.StationLengthReadyToBeSaved)
                            {
                                //stationLengthModule.ShowSaveMessageOfSuccess.IsTrue = ShowSaveMessageOfSuccess.IsTrue;
                                stationLengthModule.ExportToGeode();
                                //if (!ShowSaveMessageOfSuccess.IsTrue) { ShowSaveMessageOfSuccess.IsTrue = stationLengthModule.ShowSaveMessageOfSuccess.IsTrue; }
                            }
                            else
                            {
                                if (stationLengthModule._LengthStation._Parameters._Instrument._Model != null
                                    && !(stationLengthModule._LengthStation._Parameters._State is Station.State.StationLengthSaved)
                                    && stationLengthModule._LengthStation._Parameters._Team == R.String_UnknownTeam)
                                {
                                    new MessageInput(MessageType.Warning, R.StringAlignment_CannotSaveLengthSetting).Show();
                                    saveAllSuccess = false;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception($"{R.T_COULD_NOT} {R.Export} {R.T_LENGTH_MEASUREMENTS}\r\n\r\n{ex.Message}");
                        }
                    }
                }
                catch (Exception ex)
                {
                    saveAllSuccess = false;
                    string titleAndMessage = $"{R.T_EXPORT_IS_NOT_COMPLETE};{ex.Message}";
                    MessageInput mi = new MessageInput(MessageType.Critical, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_OK + "!" }
                    };
                    string temp = mi.Show().TextOfButtonClicked;
                }
            }
            if (saveAllSuccess)
            {
                DsaFlag ShowSaveMessageOfSuccess = DsaFlag.GetByNameOrAdd(this.DsaFlags, "ShowSaveMessageOfSuccess");
                string titleAndMessage = string.Format(R.StringAlignment_SuccessSaveAll);
                new MessageInput(MessageType.GoodNews, titleAndMessage)
                {
                    DontShowAgain = ShowSaveMessageOfSuccess
                }.Show();
            }
        }
        /// <summary>
        /// Met à jour toutes les mires en fonction du zéro mire
        /// </summary>
        internal override void UpdateAllStaff()
        {
            foreach (Guided.Module gm in SubGuidedModules)
            {
                if (gm._ActiveStationModule is Level.Station.Module stm)
                {
                    stm.UpdateAllStaff(staffUsed);
                    if (gm._ActiveStationModule.FinalModule != null)
                    {
                        stm.UpdateAllOffsets();
                    }
                }
            }
        }
        /// <summary>
        /// Affiche le module pour contrôler le zéro des mires
        /// </summary>
        internal void ZeroStaffCheck()
        {
            if (staffModule != null)
            {
                string respond = View.ShowMessageOfZeroStaffCheck(R.StringLevel_ZeroStaffCheckMsgTitle + "\r\n" + R.StringLevel_ZeroStaffCheckMsgDescription, R.T_OK, R.T_CANCEL, staffUsed, staffModule);
                if (respond != R.T_CANCEL)
                {
                    timeStaffZeroCheck = DateTime.Now;
                    SetIsStaffZeroCheckExported(false);
                    UpdateAllStaff();
                }
            }
        }
        /// <summary>
        /// Lance le contrôle collimation du niveau
        /// </summary>
        internal void CollimCheck()
        {
            if (ActiveSubGuidedModule != null)
            {
                if (ActiveSubGuidedModule.guideModuleType == GuidedModuleType.AlignmentLevelling
                    || ActiveSubGuidedModule.guideModuleType == GuidedModuleType.Cheminement
                    || ActiveSubGuidedModule.guideModuleType == GuidedModuleType.AlignmentLevellingCheck)
                {
                    if (ActiveSubGuidedModule._ActiveStationModule != null)
                    {
                        if (ActiveSubGuidedModule._ActiveStationModule is Level.Station.Module)
                        {
                            Level.Station.Module slm = ActiveSubGuidedModule._ActiveStationModule as Level.Station.Module;
                            if (slm._WorkingStationLevel != null)
                            {
                                if (slm._WorkingStationLevel.ParametersBasic._Instrument != null)
                                {
                                    if (slm._WorkingStationLevel.ParametersBasic._Instrument is I.Level && slm._WorkingStationLevel.ParametersBasic._Instrument._Name != R.String_Unknown)
                                    {
                                        I.Level level = (slm._WorkingStationLevel.ParametersBasic._Instrument as I.Level).DeepCopy();
                                        int index = levelUsed.FindIndex(x => x._Name == level._Name);
                                        if (index != -1)
                                        {
                                            level._LA1 = levelUsed[index]._LA1;
                                            level._LA2 = levelUsed[index]._LA2;
                                            level._LB1 = levelUsed[index]._LB1;
                                            level._LB2 = levelUsed[index]._LB2;
                                            level._ErrorCollim = levelUsed[index]._ErrorCollim;
                                            level._DistCollim = levelUsed[index]._DistCollim;
                                            level._IsCollimExported = levelUsed[index]._IsCollimExported;
                                        }
                                        string respond = View.ShowMessageCollimLevelCheck(R.StringMsgColimLevel_Title + "\r\n" + string.Format(R.StringMsgColimLevel_Description, level._Name),
                                            R.T_OK,
                                            R.T_CANCEL,
                                            level);
                                        if (respond != R.T_CANCEL)
                                        {
                                            if (index != -1)
                                            {
                                                levelUsed.RemoveAt(index);
                                            }
                                            levelUsed.Add(level);
                                            (slm._WorkingStationLevel.ParametersBasic._Instrument as I.Level)._LA1 = level._LA1;
                                            (slm._WorkingStationLevel.ParametersBasic._Instrument as I.Level)._LA2 = level._LA2;
                                            (slm._WorkingStationLevel.ParametersBasic._Instrument as I.Level)._LB1 = level._LB1;
                                            (slm._WorkingStationLevel.ParametersBasic._Instrument as I.Level)._LB2 = level._LB2;
                                            (slm._WorkingStationLevel.ParametersBasic._Instrument as I.Level)._ErrorCollim = level._ErrorCollim;
                                            (slm._WorkingStationLevel.ParametersBasic._Instrument as I.Level)._DistCollim = level._DistCollim;
                                            (slm._WorkingStationLevel.ParametersBasic._Instrument as I.Level)._IsCollimExported = level._IsCollimExported;
                                            foreach (Guided.Module gm in SubGuidedModules)
                                            {
                                                if (gm.guideModuleType == GuidedModuleType.AlignmentLevelling
                                                    || gm.guideModuleType == GuidedModuleType.Cheminement
                                                    || gm.guideModuleType == GuidedModuleType.AlignmentLevellingCheck)
                                                {
                                                    if (gm._ActiveStationModule != null)
                                                    {
                                                        if (gm._ActiveStationModule is Level.Station.Module)
                                                        {
                                                            Level.Station.Module stationLevelModule = gm._ActiveStationModule as Level.Station.Module;
                                                            if (stationLevelModule._WorkingStationLevel.ParametersBasic._Instrument != null)
                                                            {
                                                                if (level._Name == stationLevelModule._WorkingStationLevel.ParametersBasic._Instrument._Name)
                                                                {
                                                                    stationLevelModule.UpdateAllColimCheck();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            Save();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

        internal override IEnumerable<Station> GetStations()
        {
            var stations = new List<Station>();
            foreach (var item in this.SubGuidedModules)
            {
                foreach (var item2 in item.StationModules)
                {
                    stations.Add(item2._Station);
                }
            }
            return stations;
        }
    }
    public class GroupGuidedModuleEventArgs : EventArgs
    {
        public Guided.Module GuidedModule;
        public GroupGuidedModuleEventArgs()
        {
        }
    }
}


