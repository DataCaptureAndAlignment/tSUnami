﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Windows.Forms;
using System.Drawing;
using System.Threading.Tasks;
using TSU.Views;
using System.Globalization;
using M = TSU;
using L = TSU.Level;
using EC = TSU.Line;


namespace TSU.Common.Guided.Group
{
    public class View : ModuleView
    {
        #region fields

        private new Module Module
        {
            get
            {
                return _Module as Module;
            }
        }
        public Buttons buttons;
        public BigButton bigbuttonTop;
        private double na = Tsunami2.Preferences.Values.na;
        //BigButton previousButton;
        //BigButton nextButton;

        #endregion

        #region Construct

        public View(IModule module) : base(module) // Taking form already in controllerModuleView
        {
            InitializeMenu();
        }

        private void InitializeMenu()
        {
            //Creation of buttons
            buttons = new Buttons(this);

            //Main Button
            string creationDate = "(" + R.StringBigButton_CreatedOn + Module.CreatedOn.ToString("G", CultureInfo.CreateSpecificCulture("nl-BE")) + ")";
            bigbuttonTop = new BigButton(string.Format(R.T236, _Module._Name, creationDate),
               R.Element_Aimant,
                ShowContextMenuGlobal);

            _PanelTop.Controls.Add(bigbuttonTop);

            SetContextMenuToGlobal();
        }

        private void InitializeComponent()
        {
            SuspendLayout();
            // 
            // _PanelBottom
            // 
            _PanelBottom.Location = new Point(5, 68);
            _PanelBottom.Size = new Size(738, 413);
            // 
            // _PanelTop
            // 
            _PanelTop.Size = new Size(738, 63);
            // 
            // View
            // 
            ClientSize = new Size(748, 486);
            Name = "View";
            Controls.SetChildIndex(_PanelTop, 0);
            Controls.SetChildIndex(_PanelBottom, 0);
            ResumeLayout(false);

        }

        public class Buttons
        {
            private View _view;
            public BigButton open;
            public BigButton save;
            public BigButton modify;
            public BigButton add;
            public BigButton cloneLevelling;
            public BigButton cloneWire;
            public BigButton doReturnPreviousLevelStation;
            public BigButton goNextLevelSequence;
            public BigButton goPreviousLevelSequence;
            public BigButton doLevelGlobalLGC2CAlculation;
            public BigButton deleteActualLevelStation;
            public BigButton deleteActualWire;
            public BigButton FinishModule;
            public BigButton doWireLibrLgc2;
            public BigButton exportAllLevelDat;
            public BigButton exportLevelTheoDat;
            public BigButton exportAllWiresDat;
            public BigButton exportWireTheoDat;
            public Buttons(View view)
            {
                _view = view;
                //open = new BigButton(R.T239,
                //   R.Open, _view.Open);

                //save = new BigButton(R.T240,
                //   R.Save, _view.GuidedModule.Save);

                //modify = new BigButton(R.T241,
                //   R.Edit, _view.Edit);

                add = new BigButton(R.T210, R.Add, view.AddNewSubModule);
                cloneLevelling = new BigButton(R.StringCheminement_CloneModuleParam, R.Copy, view.CloneLevelling);
                doReturnPreviousLevelStation = new BigButton(R.StringCheminement_DoReturnPreviousStation, R.Level_Retour, view.DoLevelReturnPreviousStation);
                goNextLevelSequence = new BigButton(R.StringCheminement_NextStationInSequence, R.Level, view.GoNextLevelSquence);
                goPreviousLevelSequence = new BigButton(R.StringCheminement_PreviousStationInSequence, R.Level, view.GoPreviousLevelSequence);
                doLevelGlobalLGC2CAlculation = new BigButton(R.StringLevel_GlobalCalculationLGC2, R.Lgc, view.DoLevelCalculationWithLGC2);
                deleteActualLevelStation = new BigButton(R.StringLevel_DeleteActualStation, R.Level_Delete, view.DeleteActualStation);
                cloneWire = new BigButton(R.StringGuidedLine_NewGuidedModule, R.Line1, view.CloneWire);
                deleteActualWire = new BigButton(R.StringLine_DeleteActualWire, R.Delete_Ancrage, view.DeleteActualStation);
                FinishModule = new BigButton(R.String_GM_FinishGroupModule, R.FinishHim, view.FinishModule);
                doWireLibrLgc2 = new BigButton(R.StringLine_GlobalCalculationLGC2, R.Lgc, view.DoLibrWireCalculationWithLGC2);
                exportAllLevelDat = new BigButton(R.StringLevel_SaveAll, R.Save, view.ExportAllLevelDat);
                exportAllWiresDat = new BigButton(R.StringLine_SaveAll, R.Save, view.ExportAllWireDat);
                exportLevelTheoDat = new BigButton(R.StringLevel_ExportTheoDat, R.Export, view.ExportLevelNewPointTheoDat);
                exportWireTheoDat = new BigButton(R.StringLine_ExportTheoDat, R.Export, view.ExportWireNewPointTheoDat);
            }
        }
        #endregion

        #region Update

        public override void UpdateView()
        {
            base.UpdateView();
            _PanelBottom.Controls.Clear();
            SetContextMenuToGlobal();
            if (Module.ActiveSubGuidedModule != null)
            {
                Module.ActiveSubGuidedModule.View.ShowDockedFill();
                _PanelBottom.Controls.Add(Module.ActiveSubGuidedModule.View);
                Module.ActiveSubGuidedModule.View.BringToFront();
                UpdateBigButtonTop();
            }
        }

        private void SetContextMenuToGlobal()
        {
            contextButtons.Clear();

            if (Module.ActiveSubGuidedModule != null)
            {
                ///Ajoute les boutons supplémentaires pour un cheminement
                if (Module.ActiveSubGuidedModule.guideModuleType == ENUM.GuidedModuleType.Cheminement)
                {
                    contextButtons.Add(buttons.add);
                    //contextButtons.Add(this.buttons.doLevelGlobalLGC2CAlculation);
                    //contextButtons.Add(this.buttons.cloneLevelling);
                    //contextButtons.Add(this.buttons.doReturnPreviousLevelStation);
                    //contextButtons.Add(this.buttons.goPreviousLevelSequence);
                    //contextButtons.Add(this.buttons.goNextLevelSequence);
                    //this.buttons.doLevelGlobalLGC2CAlculation.Enabled = (this.Module.SubGuidedModules.Count > 0) ? true : false;
                    //this.buttons.cloneLevelling.Enabled = (this.Module.ActiveSubGuidedModules.currentIndex > 1) ? true : false;
                    //this.buttons.doReturnPreviousLevelStation.Enabled = (this.Module.activeSubGuidedModuleIndex > 0) ? true : false;
                    //this.buttons.goNextLevelSequence.Enabled = (StationModule._SequenceSelected.Elements.Count != 0) ? true : false;
                    //this.buttons.goPreviousLevelSequence.Enabled = (StationModule._SequenceSelected.Elements.Count != 0) ? true : false;

                    //Ne permet de faire un calcul LGC global que s'il y a au moins un subguided module
                    if (Module.SubGuidedModules.Count > 0)
                    {
                        contextButtons.Add(buttons.exportAllLevelDat);
                        ///Bouton export new points seulement si points created by user
                        if (Module.ActiveSubGuidedModule._ActiveStationModule != null)
                        {
                            if (Module.ActiveSubGuidedModule._ActiveStationModule != null)
                            {
                                List<Elements.Point> allPoints = new List<Elements.Point>();
                                allPoints = Module.ActiveSubGuidedModule._ElementManager.GetPointsInAllElements();
                                if (allPoints.FindIndex(x => x._Origin == "Created by user") != -1)
                                { contextButtons.Add(buttons.exportLevelTheoDat); }
                            }
                        }
                        contextButtons.Add(buttons.doLevelGlobalLGC2CAlculation);
                    }
                    //Ne permet le clonage que si on a sélectionné les paramètres admin
                    if (Module.ActiveSubGuidedModule.currentIndex > 1)
                    {
                        contextButtons.Add(buttons.cloneLevelling);
                        contextButtons.Remove(buttons.add);
                    }
                    //Ne permet le retour de la  statiopn précédente que si on est au moins à la 2e station
                    if (Module.activeSubGuidedModuleIndex > 0) contextButtons.Add(buttons.doReturnPreviousLevelStation);
                    ///Ne permet d'aller à une autres séquence que si on a sélectionné une séquence
                    L.Station.Module StationModule = Module.ActiveSubGuidedModule._ActiveStationModule as L.Station.Module;

                    if (StationModule._SequenceSelected.Elements.Count != 0)
                    {
                        contextButtons.Add(buttons.goNextLevelSequence);
                        contextButtons.Add(buttons.goPreviousLevelSequence);
                    }

                    ///Ajoute le bouton de suppression de la station actuelle que s'il n'y a pas de mesures encodées et s'il y a plusieurs stations
                    if (Module.ActiveSubGuidedModule._ActiveStationModule != null)
                    {

                        if (Module.SubGuidedModules.Count > 1)
                        {
                            bool addDeleteButton = true;
                            L.Station.Module am = Module.ActiveSubGuidedModule._ActiveStationModule as L.Station.Module;
                            if (am._AllerStationLevel._MeasureOfLevel.FindIndex(x => x._RawLevelReading != na) != -1) addDeleteButton = false;
                            if (am._RetourStationLevel._MeasureOfLevel.FindIndex(x => x._RawLevelReading != na) != -1) addDeleteButton = false;
                            foreach (L.Station st in am._RepriseStationLevel)
                            {
                                if (st._MeasureOfLevel.FindIndex(x => x._RawLevelReading != na) != -1) addDeleteButton = false;
                            }
                            buttons.deleteActualLevelStation.Enabled = addDeleteButton;
                            if (addDeleteButton) contextButtons.Add(buttons.deleteActualLevelStation);
                        }
                    }
                }

                ///Ajoute les boutons supplémentaires pour les mesures écartométries
                if (Module.ActiveSubGuidedModule.guideModuleType == ENUM.GuidedModuleType.Ecartometry)
                {

                    contextButtons.Add(buttons.add);
                    //Ne permet de faire un calcul LGC global que s'il y a au moins un subguided module
                    if (Module.SubGuidedModules.Count > 0)
                    {
                        contextButtons.Add(buttons.exportAllWiresDat);
                        ///Bouton export new points seulement si points created by user
                        if (Module.ActiveSubGuidedModule._ActiveStationModule != null)
                        {
                            if (Module.ActiveSubGuidedModule._ActiveStationModule != null)
                            {
                                List<Elements.Point> allPoints = new List<Elements.Point>();
                                allPoints = Module.ActiveSubGuidedModule._ElementManager.GetPointsInAllElements();
                                if (allPoints.FindIndex(x => x._Origin == "Created by user") != -1)
                                { contextButtons.Add(buttons.exportWireTheoDat); }
                            }
                        }
                        contextButtons.Add(buttons.doWireLibrLgc2);
                    }
                    //contextButtons.Add(this.buttons.cloneWire);
                    //this.buttons.cloneWire.Enabled = (this.Module.ActiveSubGuidedModules.currentIndex > 1) ? true : false;
                    //Ne permet le clonage que si on a sélectionné les paramètres admin
                    if (Module.ActiveSubGuidedModule.currentIndex > 1)
                    {
                        contextButtons.Add(buttons.cloneWire);
                        contextButtons.Remove(buttons.add);
                    }

                    ///Ajoute le bouton de suppression de la station actuelle que s'il n'y a pas de mesures encodées et s'il y a plusieurs stations
                    if (Module.ActiveSubGuidedModule._ActiveStationModule != null)
                    {
                        if (Module.SubGuidedModules.Count > 1)
                        {
                            bool addDeleteButton = true;
                            EC.Station.Module am = Module.ActiveSubGuidedModule._ActiveStationModule as EC.Station.Module;
                            foreach (EC.Station st in am.WorkingAverageStationLine._ListStationLine)
                            {
                                if (st._MeasureOffsets.FindIndex(x => x._RawReading != na) != -1) addDeleteButton = false;
                            }
                            if (addDeleteButton) contextButtons.Add(buttons.deleteActualLevelStation);
                        }
                    }
                }
            }
            foreach (Guided.Module item in Module.SubGuidedModules)
            {
                if (item.View != null)
                {
                    if (item.View.Visible == true)
                    {
                        BigButton stationButton = new BigButton(
                        item._Name + ";" + item.Utility,
                        item.View.buttonImage,
                        item.ShowInParentModule);
                        if (item == Module.ActiveSubGuidedModule)
                        {
                            stationButton.SetColors(Tsunami2.Preferences.Theme.Colors.Highlight);
                        }
                        contextButtons.Add(stationButton);
                    }
                }
            }

            contextButtons.Add(buttons.FinishModule);
        }

        public void UpdateBigButtonTop()
        {
            if (Module.ActiveSubGuidedModule != null)
            {
                _PanelTop.Controls.Clear();
                string creationDate = "(" + R.StringBigButton_CreatedOn + Module.CreatedOn.ToString("G", CultureInfo.CreateSpecificCulture("nl-BE")) + ")";
                bigbuttonTop = new BigButton(string.Format(R.StringGuided_TopButton, Module._Name, creationDate, Module.ActiveSubGuidedModule._Name, Module.ActiveSubGuidedModule.Utility),
                   R.Element_Aimant,
                    ShowContextMenuGlobal);
                _PanelTop.Controls.Add(bigbuttonTop);
            }
        }
        public void UpdateBigButtonTop(string utility)
        {
            if (Module.ActiveSubGuidedModule != null)
            {
                _PanelTop.Controls.Clear();
                string creationDate = "(" + R.StringBigButton_CreatedOn + Module.CreatedOn.ToString("G", CultureInfo.CreateSpecificCulture("nl-BE")) + ")";
                bigbuttonTop = new BigButton(string.Format(R.StringGuided_TopButton, Module._Name, creationDate, Module.ActiveSubGuidedModule._Name, Module.ActiveSubGuidedModule.Utility),
                   R.Element_Aimant,
                    ShowContextMenuGlobal);
                _PanelTop.Controls.Add(bigbuttonTop);
            }
        }

        private int notOkMenuReloadedCount = 0;
        private void ShowContextMenuGlobal()
        {
            SetContextMenuToGlobal();
            bool ok = ShowPopUpMenu(contextButtons);
            // il peut arriver lors de l'ajout d'un nouveau bouton que l'affichage des contrôles dans le popup menu ne soit pas dans l'ordre. Le réaffichage du pop up semble résoudre le problème.
            if (!ok)
            {
                notOkMenuReloadedCount++; //ps :j'ai ajouter un timer sinon cette fonction peut tourner en boucle indefiniment...
                if (notOkMenuReloadedCount < 2)
                    ShowContextMenuGlobal();
                else
                {
                    notOkMenuReloadedCount = 0;
                    ShowPopUpMenu(contextButtons);
                }
            }

        }

        #endregion


        #region Actions
        /// <summary>
        /// Assigne la bonne action au bouton add lorsqu'on ouvre une sauvegarde
        /// </summary>
        /// <param name="GrGm"></param>
        /// <param name="guideModuleType"></param>
        internal void SetRightActionAndImageToButtons(Module GrGm, ENUM.GuidedModuleType guideModuleType)
        {
            switch (guideModuleType)
            {
                case ENUM.GuidedModuleType.NoType:
                    break;
                case ENUM.GuidedModuleType.Alignment:
                    break;
                case ENUM.GuidedModuleType.AlignmentEcartometry:
                    break;
                case ENUM.GuidedModuleType.AlignmentImplantation:
                    break;
                case ENUM.GuidedModuleType.AlignmentLevelling:
                    break;
                case ENUM.GuidedModuleType.AlignmentManager:
                    foreach (Guided.Module gm in GrGm.SubGuidedModules)
                    {
                        switch (gm.guideModuleType)
                        {
                            case ENUM.GuidedModuleType.AlignmentEcartometry:
                                gm.View.buttonImage = R.Line;
                                buttons.add.Available = false;
                                break;
                            case ENUM.GuidedModuleType.AlignmentImplantation:
                                gm.View.buttonImage = R.Theodolite;
                                gm._ActiveStationModule.MeasureThreated += GrGm.OnMeasureReceived;
                                buttons.add.Available = false;
                                break;
                            case ENUM.GuidedModuleType.AlignmentLengthSetting:
                                gm.View.buttonImage = R.LengthSetting;
                                buttons.add.Available = false;
                                break;
                            case ENUM.GuidedModuleType.AlignmentLevelling:
                                gm.View.buttonImage = R.Level;
                                buttons.add.Available = false;
                                break;
                            case ENUM.GuidedModuleType.AlignmentLevellingCheck:
                                gm.View.buttonImage = R.level_Check;
                                buttons.add.Available = false;
                                break;
                            case ENUM.GuidedModuleType.AlignmentManager:
                                gm.View.buttonImage = R.Admin;
                                buttons.add.Available = false;
                                break;
                            case ENUM.GuidedModuleType.AlignmentTilt:
                                gm.View.buttonImage = R.Tilt;
                                buttons.add.Available = false;
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case ENUM.GuidedModuleType.AlignmentTilt:
                    break;
                case ENUM.GuidedModuleType.Cheminement:
                    buttons.add.BigButtonClicked += delegate { GrGm.AddNewLevellingModule(); };
                    foreach (Guided.Module gm in GrGm.SubGuidedModules)
                    {
                        gm.View.buttonImage = R.Level;
                    }
                    break;
                case ENUM.GuidedModuleType.Ecartometry:
                    buttons.add.BigButtonClicked += delegate { GrGm.AddNewEcartometryModule(); };
                    foreach (Guided.Module gm in GrGm.SubGuidedModules)
                    {
                        gm.View.buttonImage = R.Line;
                    }
                    break;
                case ENUM.GuidedModuleType.Example:
                    break;
                case ENUM.GuidedModuleType.Implantation:
                    break;
                case ENUM.GuidedModuleType.PointLance:
                    break;
                case ENUM.GuidedModuleType.PreAlignment:
                    break;
                case ENUM.GuidedModuleType.Survey1Face:
                    break;
                case ENUM.GuidedModuleType.Survey2Face:
                    break;
                case ENUM.GuidedModuleType.SurveyTilt:
                    break;
                case ENUM.GuidedModuleType.TourDHorizon:
                    break;
                default:
                    break;
            }
        }
        internal void Open()
        {
            throw new NotImplementedException();
        }

        internal void Edit()
        {
            throw new NotImplementedException();
        }
        internal void DeleteActualStation()
        {

            TryAction(Module.DeleteActualSubGuidedModule);
        }
        internal void DoLevelCalculationWithLGC2()
        {
            TryAction(Module.DoLevelCalculationWithLGC2);
        }
        internal void GoPreviousLevelSequence()
        {
            TryAction(Module.GoPreviousLevelSequence);
        }
        internal void GoNextLevelSquence()
        {
            TryAction(Module.GoNextLevelSquence);
        }
        internal void DoLevelReturnPreviousStation()
        {
            TryAction(Module.DoLevelReturnPreviousStation);
        }
        internal void CloneLevelling()
        {
            TryAction(Module.CloneLevelling);
        }
        internal void AddNewSubModule()
        {
            TryAction(Module.AddNewSubModule);
        }
        private void CloneWire()
        {
            TryAction(Module.CloneWire);
        }
        private void FinishModule()
        {
            TryAction(Module.FinishModule);
        }
        internal void DoLibrWireCalculationWithLGC2()
        {
            TryAction(Module.DoLibrWireCalculationWithLGC2);
        }
        private void ExportAllLevelDat()
        {
            TryAction(Module.ExportAllLevelDat);
        }
        private void ExportAllWireDat()
        {
            TryAction(Module.ExportAllWiresDat);
        }
        private void ExportLevelNewPointTheoDat()
        {
            TryAction(Module.ExportLevelNewPointTheoDat);
        }
        private void ExportWireNewPointTheoDat()
        {
            TryAction(Module.ExportWireNewPointTheoDat);
        }
        #endregion
        #region TsuTask
        internal void SetActiveGuidedModule(Guided.Module module)
        {
            if (Module.ActiveSubGuidedModule != null)
            {
                //n'affiche un waiting form que si on passe de un module avec instrument polaire vers un  autre module avec instrument polaire
                if (module.guideModuleType == ENUM.GuidedModuleType.AlignmentLengthSetting || Module.ActiveSubGuidedModule.guideModuleType == ENUM.GuidedModuleType.AlignmentImplantation
                    || module.guideModuleType == ENUM.GuidedModuleType.AlignmentImplantation || Module.ActiveSubGuidedModule.guideModuleType == ENUM.GuidedModuleType.AlignmentLengthSetting)
                    WaitingForm.Show(Module, Module.View, "Changing active module...", 1000);
            }



            Module.ActionSetActiveGuidedModule(module);
            Visible = false;
            UpdateView();
            // ajout pour éviter refresh à l'infini du step dans certains cas (TSU-1374)
            Module.ActiveSubGuidedModule.CurrentStep.Enter();
            Visible = true;
            HideWaitingForm();

        }
        #endregion
    }
}
