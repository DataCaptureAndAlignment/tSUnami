﻿using System;
using System.Windows.Forms;
using TSU.Views;

namespace TSU.Common.Operations
{
    [Serializable]
    public class Operation : TsuObject
    {
        private const int NoOpValue = 999999;
        public int value = NoOpValue;
        public int parentOperationValue = -1;
        public string Description = "NA";

        public bool IsSet
        {
            get
            {
                return value != NoOpValue && value != 0;
            }
        }

        /// <summary>
        /// Before creating a new operation check if it not in the Elments list and if you want the 9999 use 'TSU.Preferences.Preferences.Instance.Operations.Tag as TSU.Common.Operations.Operation;'
        /// </summary>
        /// 
        public Operation()
        {
            _Name = "Operation " + value + ":" + Description;
        }

        public Operation(int value, string description)
        {
            this.value = value;
            Description = description;
            _Name = "Operation " + value + ":" + Description;
        }

        public override string ToString()
        {
            return "Operation # " + value + " (" + Description + ")";
        }

        public override object Clone()
        {
            Operation newOp = (Operation)MemberwiseClone(); // to make a clone of the primitive types fields
            

            return newOp;
        }

        private static Operation _noOp;

        public static Operation NoOp
        {
            get
            {
                if (_noOp == null)
                    _noOp = new Operation(NoOpValue, "NA");
                return _noOp;
            }
        }
    }

    [Serializable]
    public class OperationTree: TsuNode
    {
        public void ExpandToLevel(TreeNodeCollection nodes, int level)
        {
            if (level > 0)
            {
                foreach (TreeNode node in nodes)
                {
                    node.Expand();
                    ExpandToLevel(node.Nodes, level - 1);
                }
            }
        }
    }

}
