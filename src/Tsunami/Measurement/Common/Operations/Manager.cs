﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;
using TSU.Views;
using P = TSU.Preferences;
using System.Windows.Forms;
using TSU.Views.Message;
using T = TSU.Tools;


namespace TSU.Common.Operations
{
    [XmlType(Namespace = "Operations", TypeName = "Manager")]
    public class Manager : TSU.Manager
    {
        [XmlIgnore]
        public new View View
        {
            get
            {
                return this._TsuView as View;
            }
        }

        [XmlIgnore]
        public OperationTree OperationTree { get; set; }
        
        public Operation SelectedOperation
        {
            get
            {
                if (this._SelectedObjects.Count > 0)
                    return this._SelectedObjects.Cast<Operation>().ToList()[0];
                else
                    return P.Preferences.Instance.Operations.Tag as TSU.Common.Operations.Operation;
            }
        }

        [XmlIgnore]
        public List<Operation> Operations
        {
            get
            {
                return this.AllElements.Cast<Operation>().ToList();
            }
        }

        

        #region Construction

        public Manager():base()
        {
        }

        public Manager(TSU.Module parentModule)
            : base(parentModule,R.T_MNU_OPERATION)
        {
        }

        public override void Initialize()
        {
            //1
            this.OperationTree = P.Preferences.Instance.Operations;
            List<Operation> l = GetList(OperationTree);

            //2
            base.Initialize();

            //3

            this.AllElements = new List<TsuObject>();
            this.AllElements.AddRange(l);
        }

        #endregion

        #region Event
        internal void ValidateSelection()
        {
            this.View.ValidateSelection();

            if (this._SelectedObjects.Count > 0)
            {
                foreach (var item in this._SelectedObjects)
                {
                    SendMessage(item);
                }
            }

        }
        #endregion

        #region Selection

        internal override bool IsItemSelectable(TsuObject TsuObject)
        {
            return (TsuObject is Operation);
        }

        public void SelectOperations()
        {
            throw new NotImplementedException();
        }

        public Operation SelectOperation()
        {
            if (this._SelectedObjects.Count>1) {this._SelectedObjects.Clear();}
            //if (this._SelectedObjects.Count>0)
            //{
            //    if ((this._SelectedObjects[0] as Operation).value == 9999)
            //    {
            //        this._SelectedObjects.Clear();
            //    }
            //}           
            Operation o = this.View.SelectOne();
            return o;
        }
        internal void StoreToLocalOperationsFile(int numero, string description)
        {
            string filePath = Tsunami2.Preferences.Values.Paths.PersonalPreferencesPath;

            const string format = "{0,-9}{1,-9}{2}";
            string newLine = string.Format(format, "-999", numero.ToString(), description);


            string directoryPath = Path.GetDirectoryName(filePath);

            // Ensure that the directory exists
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            if (!File.Exists(filePath))
            {
                using (File.Create(filePath)) { }

                Console.WriteLine("File created.");
            }

            File.AppendAllText(filePath, newLine + Environment.NewLine);
        }

        internal void AddNew(int numero, string description = null)
        {
            description = (description!=null) ? description:R.T_NA;
            int i = this.Operations.FindIndex(f => f.value == numero);
            if (i != -1) throw new Exception(R.O_OE);
            // create operation
            Operation newOne = new Operation(numero, description);
            newOne.parentOperationValue = -1;
            this.AllElements.Add(newOne);

            StoreToLocalOperationsFile(numero, description);

            // reload pref
            P.Preferences.Instance.LoadOperationNumber();
            this.OperationTree = P.Preferences.Instance.Operations;
            this.AllElements.Clear();
            this.AllElements.AddRange(GetList(this.OperationTree));
            // Set the new opération as the selected one
            this._SelectedObjects.Clear();
            Operation operation = (this.AllElements.Find(x => x.ToString() == newOne.ToString()) as Operation);
            this._SelectedObjectInBlue = operation;
            this.AddSelectedObjects(operation);
            // update view
            this.UpdateView();
            this.Change();
            //this._TsuView.UpdateView();
        }

        internal void CleanLocalChantierFile()
        {
            string filePath = Tsunami2.Preferences.Values.Paths.PersonalPreferencesPath;
            if (File.Exists(filePath)) { 
                File.Delete(filePath);
            }
            // reload pref
            P.Preferences.Instance.LoadOperationNumber();
            this.OperationTree = P.Preferences.Instance.Operations;
            this.AllElements.Clear();
            this.AllElements.AddRange(GetList(this.OperationTree));
            // update view
            this.UpdateView();
            this.Change();
        }

        internal void SetSelectedOperation(int number)
        {
            int i = this.AllElements.FindIndex(x => (x as Operation).value == number);
            if (i!=-1)
            {
                this._SelectedObjects.Clear();
                this.AddSelectedObjects(this.AllElements[i]);
                this._SelectedObjectInBlue = this.AllElements[i];
                this.UpdateView();
                this.Change();
            }
            
        }

        /// <summary>
        /// Vérifie si le numéro d'opération n'existe pas déjà
        /// </summary>
        /// <param name="numero"></param>
        /// <returns></returns>
        internal bool CheckIfExistingOperation(int numero)
        {
            bool existing = false;
            int i = this.AllElements.FindIndex(x => (x as Operation).value == numero);
            if (i != -1) { existing = true; }
            return existing;
        }
        private List<Operation> GetList(TsuNode node)
        {
            if (node == null) return null;
            List<Operation> l =new List<Operation>();
            foreach (TsuNode item in node.Nodes)
            {
                l.Add(item.Tag as Operation);
                l.AddRange(GetList(item));
            }
            return l;
        }

       #endregion

        #region Open

        public void Open(string path)
        {
            

            Action InnedMethod = () =>
            {
                try
                {
                    if (path != "")
                    {
                        OperationTree ot = T.Conversions.FileFormat.OperationFile2Nodes(new System.IO.FileInfo(path), out int count);
                        var myArray = ot.Nodes.Cast<TreeNode>().ToArray();
                        this.OperationTree.Nodes.AddRange(myArray);
                        this.AllElements.AddRange(GetList(ot));

                        this._SelectedObjectInBlue = null;
                        this._SelectedObjects.Clear();
                        MultiSelection = false;
                        this.Change();
                        this.UpdateView();
                    }
                }
                catch (Exception ex)
                {
                    string titleAndMessage = string.Format(R.T272, ex.Source, ex.Message);
                    new MessageInput(MessageType.Critical, titleAndMessage)
                    {
                        Sender = System.Reflection.MethodBase.GetCurrentMethod().Name
                    }.Show();
                }
            };
            this.View.WaitingForm = new Views.Message.ProgressMessage(
                this.View, "Opening File", string.Format(R.TM_SeqLoad, path),1,2000);
            InnedMethod();
            this.View.WaitingForm.Finish();
            this.View.WaitingForm.EndAll();
        }

        internal static Operation GetOperationbyNumber(int operationNumber)
        {
            bool IsTheRightOperation(TreeNode node)
            {
                Operation operation = node.Tag as Operation;
                return operation != null && operation.value == operationNumber;
            }

            TreeNode selectedTreeNode = TsuNode.EnumerateAllNodes(Tsunami2.Preferences.Values.Operations.Nodes)
                                               .OfType<TreeNode>()
                                               .FirstOrDefault(IsTheRightOperation);

            if (selectedTreeNode == null)
                return null;

            return selectedTreeNode.Tag as Operation;
        }

        #endregion
    }
}
