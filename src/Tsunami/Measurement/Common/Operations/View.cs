﻿using System;
using System.Collections.Generic;
using System.Windows.Controls.Primitives;
using System.Windows.Forms;
using TSU.Views;
using TSU.Views.Message;
using R = TSU.Properties.Resources;
using V = TSU.Common.Strategies.Views;


namespace TSU.Common.Operations
{
    public partial class View : ManagerView
    {
        // Fields
        public Buttons buttons;

        public new Manager Module
        {
            get
            {
                return this._Module as Manager;
            }
        }

        //Constructor
        public View(IModule module)
            : base(module) // Taking form already in controllerModuleView
        {
            this.InitializeComponent();
            this.ApplyThemeColors();
            this.AdaptTopPanelToButtonHeight();
            this.InitializeMenu();
            this.Module.MultiSelection = false;

            this.AutoCheckChildren = false;
            
            this.currentStrategy.Update();
            this.currentStrategy.Show();
            this.Image = R.Operation;
            
        }
        private void ApplyThemeColors()
        {

        }
        internal override void InitializeStrategy()
        {
            this.AvailableViewStrategies = new List<V.Types>() { V.Types.Tree};
            if (TSU.Tsunami2.Preferences.Values!=null && TSU.Tsunami2.Preferences.Values.GuiPrefs!=null)
                this.SetStrategy(TSU.Tsunami2.Preferences.Values.GuiPrefs.OperationViewType);
            else
                this.SetStrategy( V.Types.Tree);
        }

        internal override void SavePreference(V.Types viewStrategyType)
        {
           TSU.Tsunami2.Preferences.Values.GuiPrefs.OperationViewType = viewStrategyType;
            base.SavePreference(viewStrategyType);
        }

        internal override void InitializeMenu()
        {
            //Creation of buttons
            buttons = new Buttons(this);
            
            base.InitializeMenu();

            bigbuttonTop.ChangeNameAndDescription(R.T362);
            bigbuttonTop.ChangeImage(R.Operation);
        }

        internal override void ShowContextMenuGlobal()
        {
            contextButtons = new List<Control>();
            contextButtons.Add(buttons.newOperation);
            contextButtons.Add(buttons.cleanLocalChantierFile);

            contextButtons.Add(buttons.Open);
            this.ShowPopUpMenu(contextButtons);
        }
        // Strategy
        // define an interface for different strategy of showing the elements
        public override void UpdateView()
        {
            base.UpdateView();
            this.currentStrategy.Update();
            //this.TryAction(this.UpdateView);
        }

        

        internal void AddNew()
        {
            string cancel = R.T_CANCEL;
            string na = R.T_NA;
            string ok = R.T_OK;
            string addDescription = "Add a description";

            string description = na;

            // ask for number
            MessageTsu.ShowMessageWithTextBox(R.MT_TM_ANO, new List<string> { addDescription, ok, cancel }, 
                out string buttonClicked, out string textInput);

            if (buttonClicked == cancel)
                return;

            if (Int32.TryParse(textInput, out int number))
            {
                if (!this.Module.CheckIfExistingOperation(number))
                {
                    if (buttonClicked == addDescription)
                    {
                        // ask for description
                        MessageTsu.ShowMessageWithTextBox(R.MT_TM_ADO, new List<string> { ok, cancel }, 
                            out string buttonClicked1, out string textInput1, na);

                        if (buttonClicked1 != cancel)
                            description = textInput1;
                    }

                    // Create operation
                    this.Module.AddNew(number, description);
                }
                else
                {
                    this.Module.SetSelectedOperation(number);
                }
            }
            else
            {
                new MessageInput(MessageType.Warning, R.MT_NAI).Show();
            }
        }

        internal void CleanLocalChantierFile()
        {
            this.Module.CleanLocalChantierFile();
        }

        public override List<string> ListviewColumnHeaderTexts
        {

            get
            {
                List<string> l = new List<string>();
                l.Add("ID");
                l.Add("Description");
                l.Add("Parent Operation ID");
                return l;
            }
            set { }
        }

        #region actions

        public Operation SelectOne(string titleAndMessage = null)
        {
            this.Module.MultiSelection = false;

            // lets see if we have existing operation in other modules
            {
                List<Operation> operations = Tsunami2.Properties.GetOperationNumbers();
                if (operations.Count == 1) // more we dont know what to do.
                {
                    this.Module._SelectedObjects.Clear();
                    this.Module._SelectedObjects.Add(operations[0]);
                    this.Module._SelectedObjectInBlue = operations[0];
                    new MessageInput(MessageType.FYI, $"{R.T_OPERATION_FOUND};{R.T_A_SINGLE_OPERATION_HAS_BEEN_FOUND_IN_OTHER_MODULES__SO_IT_IS_PRE_SELECTED}\r\n\r\n\r\n\r\n").Show();
                }
            }
            
            this.UpdateView();

            void HideMessageVIew(object o, V.EventArgs e)
            {
                if (e.ModuleView.Parent?.Parent?.Parent is MessageTsu m)
                {
                    m.Hide();
                }
            };


            this.currentStrategy.DoubleClick += HideMessageVIew;


            string respond = this.ShowInMessageTsu(
                titleAndMessage ??R.T363,
                R.T_SELECT, this.ValidateSelection,
                R.T_CANCEL, this.CancelSelection,
                showdialog:true);
            
            this.currentStrategy.DoubleClick -= HideMessageVIew;

            if (this.Module._SelectedObjects.Count > 0 && respond != R.T_CANCEL)
            {
                return this.Module._SelectedObjects[0] as Operation;
            }
            else

            {
                this.Module._SelectedObjects.Clear();
                return null;
            }
        }

        public void Open()
        {
            string filename = TsuPath.GetFileNameToOpen(this,
                System.IO.Path.GetDirectoryName(TSU.Tsunami2.Preferences.Values.Paths.Operations),
                "", "Geode files (*.dat)|*.dat|Xml files (*.xml)|*.xml",
                R.T_BROWSE_FOR_AN_OPERATION_FILE_IN_XML_FORMAT);

            if(filename!="")
                this.Module.Open(filename);
        }

        #endregion

        public new class Buttons
        {
            private View _view;
            public List<BigButton> all;
            public BigButton newOperation;
            public BigButton cleanLocalChantierFile;
            public BigButton Open;
            public Buttons(View view)
            {
                _view = view;
                all = new List<BigButton>();
                Open = new BigButton(R.T_OP_IMPORT, R.Open, _view.Open); all.Add(Open);
                newOperation = new BigButton(R.T_OP_NEW,R.Operation, _view.AddNew); all.Add(newOperation);
                cleanLocalChantierFile = new BigButton(R.T_CLEAN_LOCAL, R.Edit, _view.CleanLocalChantierFile); all.Add(cleanLocalChantierFile);
            }

            internal void Dispose()
            {
                _view = null;
                for (int i = 0; i < this.all.Count; i++)
                {
                    if (all[i] != null)
                        all[i].Dispose();
                }
                for (int i = 0; i < this.all.Count; i++)
                {
                    all[i] = null;
                }
                all.Clear();
                all = null;
            }



            public void ReleaseAll()
            {
                foreach (BigButton item in this.all)
                    item.Available = true;
            }

            public void BlockAll()
            {
                foreach (BigButton item in this.all)
                    item.Available = false;
            }

            public void Release(BigButton buttonToRelease)
            {
                (buttonToRelease ).Available = true;
            }

            public void Block(BigButton buttonToBLock)
            {
                (buttonToBLock).Available = false;
            }

            public void BlockAdvanced()
            {
                this.BlockAll();
                //this.Release(this.add);
            }
        }

    }
    
}
