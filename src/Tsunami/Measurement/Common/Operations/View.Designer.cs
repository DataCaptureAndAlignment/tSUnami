﻿
using R = TSU.Properties.Resources;

namespace TSU.Common.Operations
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (this.buttons != null)
                this.buttons.Dispose();
            this.buttons = null;


            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // _PanelBottom
            // 
            this._PanelBottom.Size = new System.Drawing.Size(513, 409);
            // 
            // _PanelTop
            // 
            this._PanelTop.Size = new System.Drawing.Size(513, 63);
            // 
            // ExplorationModuleView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 482);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name =R.T364;
            this.Controls.SetChildIndex(this._PanelTop, 0);
            this.Controls.SetChildIndex(this._PanelBottom, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
    }
}
