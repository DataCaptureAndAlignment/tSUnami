﻿using System.Windows.Controls;
using TSU.Views;
using System.Windows.Input;
using System.Windows;
using System.Windows.Data;

namespace TSU.Line.Smart.View
{
    public partial class MeasureView : TsuUserControl
    {
        public MeasureView() : base()
        {
            InitializeComponent();
        }

        public override TsuView MainView
        {
            get
            {
                if (DataContext is ViewModel vm)
                    return vm.MainView;
                else
                    return base.MainView;
            }
        }

        private void Measures_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataContext is ViewModel vm && vm.CurrentPage == ViewModel.Page.Measure)
            {
                TextBoxRawReading.SelectAll();
                TextBoxRawReading.Focus();
            }
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                BindingOperations.GetBindingExpression((DependencyObject)sender, TextBox.TextProperty)?.UpdateSource();
            else if (e.Key == Key.Up && DataContext is ViewModel vm1)
            {
                if (vm1.CurrentMeasureIndex > 0)
                {
                    BindingOperations.GetBindingExpression((DependencyObject)sender, TextBox.TextProperty)?.UpdateSource();
                    vm1.CurrentMeasureIndex--;
                }
            }
            else if (e.Key == Key.Down && DataContext is ViewModel vm2)
            {
                if (vm2.CurrentMeasureIndex < vm2.AllMeasures.Count - 1)
                {
                    BindingOperations.GetBindingExpression((DependencyObject)sender, TextBox.TextProperty)?.UpdateSource();
                    vm2.CurrentMeasureIndex++;
                }
            }
        }
    }
}
