﻿using TSU.Views;

namespace TSU.Line.Smart.View
{
    public partial class LogView : TsuUserControl
    {
        public LogView() : base() => InitializeComponent();

        public override TsuView MainView => DataContext is ViewModel vm ? vm.MainView : base.MainView;
    }
}
