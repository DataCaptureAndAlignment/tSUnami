﻿using MathNet.Numerics.Statistics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Input;
using System.Xml.Serialization;
using TSU.Common.Blocks;
using TSU.Common.Smart;
using TSU.Views.Message;
using static TSU.Common.Station.State;
using static TSU.Line.Smart.StationViewModel;
using static TSU.Line.Station;
using R = TSU.Properties.Resources;

namespace TSU.Line.Smart
{
    public partial class ViewModel : ViewModelBase
    {
        private static void ComputeAll(IEnumerable<StationViewModel> stvms)
        {
            foreach (StationViewModel s in stvms)
            {
                List<RoundRmsViewModel> rmsLst = new List<RoundRmsViewModel>();
                string currentRoundRms = "NA";

                int roundNumber = 0;
                foreach (Average r in s.AllRounds)
                {
                    roundNumber++;
                    string roundRmsAsString = ComputeRoundRms(r);
                    Debug.WriteInConsole($"roundNumber={roundNumber},roundRms={roundRmsAsString}");
                    rmsLst.Add(new RoundRmsViewModel {  RoundNumber = roundNumber, RoundRms = roundRmsAsString });
                    if (roundNumber == s.CurrentRoundAsInt)
                        currentRoundRms = roundRmsAsString;
                }

                s.Rms = currentRoundRms;
                s.RoundRms = rmsLst;
            }
        }

        private static string ComputeRoundRms(Average r)
        {
            if (r?.HasStationLine() ?? false)
            {
                double roundRms = r._ListStationLine[0]._MeasureOffsets
                                   .Select(m => m._RawReading)
                                   .Where(m => !m.IsNa())
                                   .StandardDeviation();

                if (!double.IsNaN(roundRms))
                    return MeasureFormatting.FormatDistance(roundRms * 1000d, MeasureFormatting.DistanceUnit.mm, false);
            }

            return "NA";
        }

        #region BtnExportAllMeasurement

        private DelegateCommand btnExportAllMeasurement;

        [XmlIgnore]
        public ICommand BtnExportAllMeasurement
        {
            get
            {
                if (btnExportAllMeasurement == null)
                    btnExportAllMeasurement = new DelegateCommand(BtnExportAllMeasurementClick, BtnExportAllMeasurementCanExecute);
                return btnExportAllMeasurement;
            }
        }

        private bool BtnExportAllMeasurementCanExecute()
        {
            try
            {
                return AllStations?.Any(StationViewModelMatches) ?? false;

                bool StationViewModelMatches(StationViewModel stationViewModel)
                {
                    return stationViewModel?.StationModule?.AverageStations?.Exists(AverageStationMatches) ?? false;
                }

                bool AverageStationMatches(Average averageStation)
                {
                    return averageStation?._Parameters?._State is WireReadyToBeSaved;
                }
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        private void BtnExportAllMeasurementClick()
        {
            try
            {
                string filepath = GetFileNameForGeode();

                //Reset the file
                if (File.Exists(filepath))
                    File.Delete(filepath);

                Result saveDatOK = new Result();
                int stationNumber = 0;
                foreach (StationViewModel s in AllStations)
                {
                    stationNumber++;
                    Station.Module stationModule = s.StationModule;
                    if (stationModule != null)
                    {
                        foreach (Average averageStation in stationModule.AverageStations)
                        {
                            Common.Station.State state = averageStation._Parameters._State;
                            if (state is WireReadyToBeSaved || state is WireSaved)
                            {
                                stationModule.WorkingAverageStationLine = averageStation;
                                saveDatOK.MergeString(SaveToGeode.SaveFilToGeode(stationModule, false, true, filepath));
                            }
                        }
                    }
                }

                if (saveDatOK.Success)
                {
                    string titleAndMessage = string.Format(R.StringSmart_AllStationsExported, filepath);
                    DsaFlag ShowSaveMessageOfSuccess = DsaFlag.GetByNameOrAdd(SmartModule.DsaFlags, "ShowSaveMessageOfSuccess");
                    MessageInput mi = new MessageInput(MessageType.GoodNews, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.T_OpenFileLocation },
                        DontShowAgain = ShowSaveMessageOfSuccess
                    };
                    if (mi.Show().TextOfButtonClicked == R.T_OpenFileLocation
                        && File.Exists(filepath))
                    {
                        Process.Start(new ProcessStartInfo("explorer.exe", " /select, " + filepath));
                    }
                }

                // Save the final module
                SmartModule.Save();

            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        private string GetFileNameForGeode()
        {
            string dir = Tsunami2.Preferences.Values.Paths.MeasureOfTheDay;

            Parameters parameters = DefaultsStationModule.WorkingAverageStationLine._Parameters;

            int geodeOp = parameters._Operation.value;
            DateTime geodeDate = parameters._Date;
            string geodeTeam = parameters._Team;

            string filepath = $"{dir}{geodeOp:00000}_{geodeDate:yyyy-MM-dd}_{geodeTeam:6}_SmartWire.dat";

            return filepath;
        }

        #endregion

    }
}
