﻿using System;
using System.Collections.Generic;
using System.IO.Packaging;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using TSU.Common.Blocks;
using TSU.Common.Elements;
using static TSU.Line.Station;
using C = TSU.Common.Elements.Composites;

namespace TSU.Line.Smart
{
    [XmlType("LineStationViewModel")]
    public class StationViewModel : ViewModelBase
    {
        public StationViewModel()
        {
            Debug.WriteInConsole($"StationViewModel ctor");
            NumberAsInt = 0;
        }

        public StationViewModel(C.SequenceFil seq) : this()
        {
            Sequence = seq;
            LoadSequence();
        }

        public C.SequenceFil Sequence { get; set; }

        public void LoadSequence()
        {
            // FirstAndLastPoints
            StringBuilder ret = new StringBuilder();
            if (Sequence.Elements.Count > 0)
            {
                ret.Append(Sequence.Elements[0]._Name);
                if (Sequence.Elements.Count > 1)
                {
                    ret.AppendLine();
                    ret.Append(Sequence.Elements[Sequence.Elements.Count - 1]._Name);
                }
            }
            FirstAndLastPoints = ret.ToString();

            // AllPoints
            StringBuilder ret2 = new StringBuilder();
            foreach (Element e in Sequence.Elements)
            {
                if (ret2.Length > 0)
                    ret2.AppendLine();
                ret2.Append(e._Name);
            }
            AllPoints = ret2.ToString();
        }

        private string firstAndLastPoints;
        [XmlIgnore]
        public string FirstAndLastPoints
        {
            get => firstAndLastPoints;
            set => SetProperty(ref firstAndLastPoints, value);
        }

        private string allPoints;
        [XmlIgnore]
        public string AllPoints
        {
            get => allPoints;
            set => SetProperty(ref allPoints, value);
        }

        [XmlIgnore]
        public ViewModel Parent { get; set; }

        internal void InitializeWithParent(ViewModel parent)
        {
            Debug.WriteInConsole("StationViewModel.InitializeWithParent");

            // Remove notifications
            Suspend();

            Parent = parent;

            //Restore or begin notifications            
            PropertyChanged += parent.OnStationPropertyChanged;

            // For new stations, we may not have a value
            if (CurrentRoundAsInt <= 0)
                CurrentRoundAsInt = 1;
        }

        private string numberAsString;
        [XmlIgnore]
        public string NumberAsString
        {
            get => numberAsString;
            set => SetProperty(ref numberAsString, value);
        }

        private int numberAsInt;
        public int NumberAsInt
        {
            get => numberAsInt;
            set => SetProperty(ref numberAsInt, value);
        }

        public string StationModuleGuid { get; set; }

        private Station.Module stationModule;
        [XmlIgnore]
        public Station.Module StationModule
        {
            get
            {
                if (stationModule == null && !string.IsNullOrEmpty(StationModuleGuid) && Parent?.SmartModule?.StationModules != null)
                {
                    Guid toFind = Guid.Parse(StationModuleGuid);
                    stationModule = Parent.SmartModule.StationModules.OfType<Station.Module>().FirstOrDefault(sm => sm.Guid.Equals(toFind));
                }

                return stationModule;
            }
        }

        public Average AverageStation
        {
            get => StationModule?.WorkingAverageStationLine;
        }

        private string currentRoundAsString;
        [XmlIgnore]
        public string CurrentRoundAsString
        {
            get => currentRoundAsString;
            set => SetProperty(ref currentRoundAsString, value);
        }

        private int currentRoundAsInt;

        public int CurrentRoundAsInt
        {
            get => currentRoundAsInt;
            set
            {
                if (SetProperty(ref currentRoundAsInt, value))
                    CurrentRoundAsString = $"{currentRoundAsInt:D1}/{RoundCount:D1}";
            }
        }

        private string rms;
        [XmlIgnore]
        public string Rms
        {
            get => rms;
            set => SetProperty(ref rms, value);
        }

        public int RoundCount { get; set; } = 1;

        public CloneableList<string> RoundGuids { get; set; }

        internal void AddRound()
        {
            Debug.WriteInConsole($"Before creating new Round, RoundGuids={RoundGuids}");
            StationModule.AddStation();
            Debug.WriteInConsole($"After creating new Round, RoundGuids={RoundGuids}");
            //Memorize the link to station
            RoundGuids.Add(StationModule.WorkingAverageStationLine.Guid.ToString());
            Debug.WriteInConsole($"After adding new Round, RoundGuids={RoundGuids}");
        }

        public IEnumerable<Average> AllRounds
        {
            get
            {
                if (RoundGuids != null)
                    foreach (string stationGuid in RoundGuids)
                        yield return StationModule.GetAverageLineStationByGuid(stationGuid);
            }
        }

        public class RoundRmsViewModel : ViewModelBase
        {
            private int roundNumber;
            
            public int RoundNumber
            { 
                get => roundNumber;
                set => SetProperty(ref roundNumber, value);
            }

            private string roundRms;

            public string RoundRms
            {
                get => roundRms;
                set => SetProperty(ref roundRms, value);
            }
        }

        public List<RoundRmsViewModel> RoundRms { 
            get; 
            set; 
        }

        public Station StationLine
        {
            get => StationModule?.WorkingAverageStationLine?._ListStationLine[0];
        }

        public override string ToString()
        {
            StringBuilder ret = new StringBuilder();
            ret.Append("Number=");
            ret.Append(NumberAsString);
            ret.Append(",Sequence={");
            bool first = true;
            foreach (Element e in Sequence.Elements)
            {
                if (first)
                    first = false;
                else
                    ret.Append(",");
                ret.Append(e._Name);
            }
            ret.Append("}");
            return ret.ToString();
        }
    }
}
