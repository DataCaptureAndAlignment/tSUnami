﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Xml.Serialization;
using TSU.Common.Blocks;
using TSU.Common.Elements;
using TSU.Common.Measures;
using TSU.Common.Operations;
using TSU.Views;
using static TSU.Common.Station.State;
using static TSU.Line.Station;
using C = TSU.Common.Elements.Composites;
using CI = TSU.Common.Instruments;
using EM = TSU.Common.Elements.Manager;
using O = TSU.Common.Operations;
using P = TSU.Preferences;
using R = TSU.Properties.Resources;

namespace TSU.Line.Smart
{
    public partial class ViewModel : ViewModelBase
    {
        private void InitializeAdminPage()
        {
            string newTeam = ActiveAverageStation?.ParametersBasic?._Team;
            if (!string.IsNullOrWhiteSpace(newTeam) && newTeam != R.String_UnknownTeam)
                Team = newTeam;
            else
                Team = string.Empty;

            string newTheoFilePath = R.StringSmart_NoTheoreticalFile;
            List<C.TheoreticalElement> filesOpened = SmartModule?._ElementManager?.filesOpened;
            if (filesOpened != null && filesOpened.Count > 0)
            {
                string lastFileName = filesOpened[filesOpened.Count - 1]._Name;
                if (!string.IsNullOrEmpty(lastFileName))
                    newTheoFilePath = lastFileName;
            }
            TheoFilePath = newTheoFilePath;

            SequenceFilePath = R.StringSmart_NoSequenceFile;

            string defaultOperation = DefaultsStationModule?.WorkingAverageStationLine?.ParametersBasic?._Operation?.value.ToString();
            if (string.IsNullOrEmpty(defaultOperation))
                OperationNumber = defaultOperation;
            else
                OperationNumber = Operation.NoOp.value.ToString();

            string instrumentName = DefaultsStationModule?.WorkingAverageStationLine?._Parameters?._Instrument?._Name;
            if (string.IsNullOrEmpty(instrumentName) || instrumentName == R.String_Unknown)
                CurrentInstrumentName = R.StringSmart_InstrumentSelection;
            else
                CurrentInstrumentName = instrumentName;

            MeasPerPointAsNumber = 3;
        }

        private string team;
        public string Team
        {
            get => team;
            set => SetProperty(ref team, value);
        }

        private string theoFilePath;
        public string TheoFilePath
        {
            get => theoFilePath;
            set => SetProperty(ref theoFilePath, value);
        }

        private string sequenceFilePath;
        public string SequenceFilePath
        {
            get => sequenceFilePath;
            set => SetProperty(ref sequenceFilePath, value);
        }

        private string operationNumber;
        public string OperationNumber
        {
            get => operationNumber;
            set => SetProperty(ref operationNumber, value);
        }
        public string OperationFile => "Chantier.dat";

        private string currentInstrumentName;
        public string CurrentInstrumentName
        {
            get => currentInstrumentName;
            set => SetProperty(ref currentInstrumentName, value);
        }

        private int? measPerPointAsNumber;
        public int? MeasPerPointAsNumber
        {
            get => measPerPointAsNumber;
            set
            {
                if (SetProperty(ref measPerPointAsNumber, value))
                    MeasPerPointAsString = measPerPointAsNumber != null ? measPerPointAsNumber.ToString() : string.Empty;
            }
        }

        private string measPerPointAsString;
        public string MeasPerPointAsString
        {
            get => measPerPointAsString;
            set
            {
                if (SetProperty(ref measPerPointAsString, value))
                {
                    if (int.TryParse(measPerPointAsString, out int newValue))
                        MeasPerPointAsNumber = newValue;
                    else
                        MeasPerPointAsNumber = null;
                }
            }
        }

        private string measPerPointError;
        public string MeasPerPointError
        {
            get => measPerPointError;
            set => SetProperty(ref measPerPointError, value);
        }

        #region BtnTheo
        private DelegateCommand btnTheo;
        [XmlIgnore]
        public ICommand BtnTheo
        {
            get
            {
                if (btnTheo == null)
                    btnTheo = new DelegateCommand(BtnTheoClick);
                return btnTheo;
            }
        }

        private void BtnTheoClick()
        {
            try
            {
                string filePath = TsuPath.GetFileNameToOpen(
                    MainView,
                    P.Preferences.Instance.Paths.FichierTheo,
                    "",
                    "Geode files (*.dat)|*.dat",
                    R.T_BROWSE_THEORETICAL_FILE);

                if (string.IsNullOrEmpty(filePath))
                    return;

                void Loading()
                {
                    try
                    {
                        int id = Debug.GetNextId();
                        Debug.WriteInConsole("Begin Loading in BtnTheoClick", id);
                        TheoFilePath = filePath;
                        if (CheckTheo())
                            LoadTheo();
                        Debug.WriteInConsole("End Loading in BtnTheoClick", id);
                    }
                    catch (OperationCanceledException)
                    {
                        MainView?.WaitingForm?.Stop();
                    }
                    catch (Exception ex)
                    {
                        ShowMessageOfBug(ex);
                        MainView?.WaitingForm?.Stop();
                    }
                }

                MainView.ShowProgress(MainView, Loading, 2, string.Format(R.TM_TheoLoad, filePath));
            }
            catch (Exception e)
            {
                ShowMessageOfBug(e);
                MainView?.WaitingForm?.Stop();
            }
        }
        #endregion

        #region BtnSequence
        private DelegateCommand btnOpensSequence;
        [XmlIgnore]
        public ICommand BtnOpenSequence
        {
            get
            {
                if (btnOpensSequence == null)
                    btnOpensSequence = new DelegateCommand(BtnOpenSequenceClick);
                return btnOpensSequence;
            }
        }

        private void BtnOpenSequenceClick()
        {
            try
            {
                string filenameFullPath = TsuPath.GetFileNameToOpen(MainView,
                    P.Preferences.Instance.Paths.FichierTheo,
                    "",
                    "All files (*.*)|*.*|Reseau files (*.res)|*.res|Geode files (*.dat)|*.dat|Xml files (*.xml)|*.xml",
                    R.T_BROWSE_SEQ);


                if (filenameFullPath != "")
                {
                    void Loading()
                    {
                        try
                        {
                            int id = Debug.GetNextId();
                            Debug.WriteInConsole("Begin Loading in BtnOpenSequenceClick", id);
                            LoadSequenceFile(filenameFullPath);
                            Debug.WriteInConsole("End Loading in BtnOpenSequenceClick", id);
                        }
                        catch (OperationCanceledException)
                        {
                            MainView?.WaitingForm?.Stop();
                        }
                        catch (Exception ex)
                        {
                            ShowMessageOfBug(ex);
                            MainView?.WaitingForm?.Stop();
                        }
                    }

                    MainView.ShowProgress(MainView, Loading, 2, string.Format(R.TM_SeqLoad, filenameFullPath));
                }
            }
            catch (OperationCanceledException)
            {
                MainView?.WaitingForm?.Stop();
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                MainView?.WaitingForm?.Stop();
            }
        }


        #endregion

        #region BtnOperation
        private DelegateCommand btnOperation;
        [XmlIgnore]
        public ICommand BtnOperation
        {
            get
            {
                if (btnOperation == null)
                    btnOperation = new DelegateCommand(BtnOperationClick);
                return btnOperation;
            }
        }
        private void BtnOperationClick()
        {
            try
            {
                Operation o = SmartModule.OperationManager.SelectOperation();
                if (o == null) return;
                OperationNumber = o.value.ToString();
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }
        #endregion

        #region BtnSelectInstrument

        private DelegateCommand btnSelectInstrument;

        [XmlIgnore]
        public ICommand BtnSelectInstrument
        {
            get
            {
                if (btnSelectInstrument == null)
                    btnSelectInstrument = new DelegateCommand(BtnSelectInstrumentClick);
                return btnSelectInstrument;
            }
        }

        private void BtnSelectInstrumentClick()
        {
            try
            {
                Station.Module stationModule = DefaultsStationModule;
                CI.Manager.Module instrumentManagerModule = stationModule._InstrumentManager;
                instrumentManagerModule.CreateViewIfNull();
                instrumentManagerModule._SelectedObjects.Clear();
                List<TsuObject> preselected = new List<TsuObject>();
                if (stationModule.WorkingAverageStationLine._Parameters._Instrument != null)
                {
                    Station.Parameters stp = stationModule.WorkingAverageStationLine._Parameters;

                    CI.Instrument oldInstrument = instrumentManagerModule.AllElements.OfType<CI.Instrument>().FirstOrDefault(
                        inst => inst._SerialNumber == stp._Instrument._SerialNumber
                                && (inst._Model == stp._Instrument._Model));

                    if (oldInstrument != null)
                    {
                        preselected.Add(oldInstrument);
                    }
                }
                List<TsuObject> selectables = instrumentManagerModule.GetByClass(CI.InstrumentClasses.ECARTOMETRE);

                CI.Instrument newInstrument = instrumentManagerModule.SelectInstrument(R.StringLevel_InstrumentChoice, selectables, multiSelection: false, preselected);

                if (newInstrument == null) return;

                CurrentInstrumentName = newInstrument._Name;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        #region BtnValidate
        private DelegateCommand btnValidate;
        [XmlIgnore]
        public ICommand BtnValidate
        {
            get
            {
                if (btnValidate == null)
                    btnValidate = new DelegateCommand(BtnValidateClick, BtnValidateCanExecute);
                return btnValidate;
            }
        }
        private bool BtnValidateCanExecute()
        {
            try
            {
                return CheckAdminPage();
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        private void BtnValidateClick()
        {
            try
            {
                if (!CheckAdminPage())
                    return;

                ActiveAverageStation.ParametersBasic._Team = Team;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return;
            }

            CurrentPage = Page.Sequence;
        }
        #endregion

        #region Error labels

        private string teamError;
        public string TeamError
        {
            get => teamError;
            set => SetProperty(ref teamError, value);
        }

        private string theoError;
        public string TheoError
        {
            get => theoError;
            set => SetProperty(ref theoError, value);
        }

        private string sequenceError;
        public string SequenceError
        {
            get => sequenceError;
            set => SetProperty(ref sequenceError, value);
        }

        private string operationError;
        public string OperationError
        {
            get => operationError;
            set => SetProperty(ref operationError, value);
        }

        private string instrumentError;
        public string InstrumentError
        {
            get => instrumentError;
            set => SetProperty(ref instrumentError, value);
        }

        #endregion

        private static readonly string[] adminPropertiesToCheck = {
            nameof(Team),
            nameof(TheoFilePath),
            nameof(OperationNumber),
            nameof(MeasPerPointAsString),
            nameof(CurrentInstrumentName),
        };

        private void OnChangeAdminPage(object sender, PropertyChangedEventArgs e)
        {
            if (adminPropertiesToCheck.Contains(e.PropertyName))
                CheckAdminPage();
        }

        private bool CheckTeam()
        {
            string value = Team;

            if (value == R.StringSmart_NoTeam)
            {
                TeamError = R.StringSmart_NoTeam;
                return false;
            }

            value = value.Trim().ToUpper();

            if (!Regex.IsMatch(value, @"^[A-Z]+$") || value.Length >= 8)
            {
                TeamError = R.StringSmart_TeamFormat;
                return false;
            }

            Team = value;
            TeamError = "";
            return true;
        }

        private bool CheckInstrument()
        {
            if (string.IsNullOrEmpty(CurrentInstrumentName)
                || CurrentInstrumentName == R.StringSmart_InstrumentSelection)
            {
                InstrumentError = R.StringSmart_NoInstrumentSelected;
                return false;
            }

            if (!(DefaultsStationModule._InstrumentManager._Instruments.Find(i => i._Name == CurrentInstrumentName) is CI.OffsetMeter newLevel))
            {
                InstrumentError = R.StringSmart_NoInstrumentSelected;
                return false;
            }

            bool UsesDifferentInstrument(Station.Module stm)
            {
                CI.OffsetMeter oldInstrument = stm.WorkingAverageStationLine._Parameters._Instrument;
                return newLevel._Name != oldInstrument._Name
                    || newLevel._SerialNumber != oldInstrument._SerialNumber;
            }

            List<Station.Module> toChange = new List<Station.Module>();
            foreach (Station.Module stm in SmartModule.StationModules.OfType<Station.Module>())
                if (UsesDifferentInstrument(stm) && CanChangeInstrument(stm.WorkingAverageStationLine))
                    toChange.Add(stm);

            if (toChange.Count > 0)
            {
                void Work()
                {
                    int id = Debug.GetNextId();
                    Debug.WriteInConsole("Begin Work in CheckInstrument", id);
                    foreach (Station.Module stm in toChange)
                        stm.SetOffsetMeter(newLevel);
                    SmartModule.Save();
                    Debug.WriteInConsole("End Work in CheckInstrument", id);
                }

                MainView.ShowProgress(MainView, Work, 2, string.Format(R.StringSmart_SettingInstrument));
            }

            InstrumentError = string.Empty;
            return true;

        }

        private static bool CanChangeInstrument(Average x)
        {
            Common.Station.State s = x._Parameters._State;
            return s is MeasureToEnter
                || s is ElementSelection
                || s is InstrumentSelection
                || s is Measuring
                || s is Opening;
        }

        private bool CheckMeasPerPoint()
        {
            if (string.IsNullOrEmpty(MeasPerPointAsString))
            {
                MeasPerPointError = R.StringSmart_NoMeasPerPoint;
                return false;
            }

            if (!MeasPerPointAsNumber.HasValue || MeasPerPointAsNumber < 1 || MeasPerPointAsNumber > 30)
            {
                MeasPerPointError = R.StringSmart_MeasPerPointFormat;
                return false;
            }

            MeasPerPointError = string.Empty;
            return true;
        }

        private bool CheckAdminPage()
        {
            bool teamOk = CheckTeam();
            CheckTheo();
            bool opOk = CheckOperation();
            int id = Debug.GetNextId();
            Debug.WriteInConsole("Begin CheckInstrument", id);
            bool instrOk = CheckInstrument();
            Debug.WriteInConsole("End CheckInstrument", id);
            bool measOk = CheckMeasPerPoint();

            if (teamOk && opOk && measOk && instrOk)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool CheckOperation()
        {
            if (string.IsNullOrEmpty(OperationNumber))
            {
                OperationError = R.StringSmart_NoOperation;
                return false;
            }

            if (!int.TryParse(OperationNumber, out int operationNumberLoc) || operationNumberLoc <= 0)
            {
                OperationError = string.Format(R.StringSmart_OperationFormat, OperationNumber);
                return false;
            }

            Operation op = O.Manager.GetOperationbyNumber(operationNumberLoc);
            if (op == null)
            {
                OperationError = string.Empty;
                return true;
            }

            if (!op.IsSet)
            {
                OperationError = R.StringSmart_NoOperation;
                return false;
            }

            foreach (Station.Module stm in SmartModule.StationModules.OfType<Station.Module>())
                stm.ChangeOperationID(op);

            OperationError = string.Empty;
            return true;
        }

        private bool CheckTheo()
        {
            string filePath = TheoFilePath;

            if (filePath == R.StringSmart_NoTheoreticalFile)
            {
                TheoError = R.StringSmart_NoTheoreticalFile;
                return false;
            }

            if (!File.Exists(filePath))
            {
                TheoError = string.Format(R.StringSmart_TheFileDoesNotExist, filePath);
                return false;
            }

            TheoError = string.Empty;
            return true;
        }

        private void LoadTheo()
        {
            EM.Module em = SmartModule._ElementManager;
            FileInfo fileInfo = new FileInfo(TheoFilePath);
            Coordinates.CoordinateSystemsTsunamiTypes type = em.GetProbableType(fileInfo, out bool isDotDat);
            if (type == Coordinates.CoordinateSystemsTsunamiTypes.Unknown)
            {
                if (em.View.AskForCoordinateType(out Coordinates.CoordinateSystemsTsunamiTypes chosenType))
                    type = chosenType;
                else
                {
                    TheoError = R.T_CANCEL_BY_USER;
                    return;
                }
            }

            em.AddElementsFromFile(fileInfo, type, isDotDat, updateView: false);
            P.Preferences.Instance.Paths.FichierTheo = fileInfo.Directory.FullName;
            TheoError = string.Empty;
        }
    }
}