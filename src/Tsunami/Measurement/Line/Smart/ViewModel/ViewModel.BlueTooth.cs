﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using TSU.Common.Blocks;
using TSU.Views.Message;
using CI = TSU.Common.Instruments;
using R = TSU.Properties.Resources;

namespace TSU.Line.Smart
{
    public partial class ViewModel : ViewModelBase
    {

        #region BtnConnectBt

        private DelegateCommand btnConnectBt;

        public ICommand BtnConnectBt
        {
            get
            {
                if (btnConnectBt == null)
                {
                    btnConnectBt = new DelegateCommand(BtnConnectBtClick, BtnConnectBtCanExecute);
                }

                return btnConnectBt;
            }
        }

        private void BtnConnectBtClick()
        {
            try
            {
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        private bool BtnConnectBtCanExecute()
        {
            try
            {
                return IsCurrentInstrumentDigital();
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        #endregion

        private CI.Module CurrentInstrumentModule
        {
            get
            {
                return CurrentStation?.StationModule?._InstrumentManager?.SelectedInstrumentModule
                    ?? DefaultsStationModule?._InstrumentManager?.SelectedInstrumentModule;
            }
        }

        private bool IsCurrentInstrumentDigital()
        {
            return CurrentInstrumentModule?.Instrument?._Model == "RS_1000V2";
        }

        public Visibility ShowMeasure
        {
            get
            {
                if (IsCurrentInstrumentDigital())
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
        }

    }
}