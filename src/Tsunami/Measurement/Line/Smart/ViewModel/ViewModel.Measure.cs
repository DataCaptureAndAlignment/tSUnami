﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using System.Xml.Serialization;
using TSU.Common;
using TSU.Common.Blocks;
using static TSU.Line.Station;
using E = TSU.Common.Elements;
using M = TSU.Common.Measures;

namespace TSU.Line.Smart
{
    public partial class ViewModel : ViewModelBase
    {
        private bool showAdd;
        [XmlIgnore]
        public bool ShowAdd
        {
            get => showAdd;
            set => SetProperty(ref showAdd, value);
        }

        private bool doNotShowAdd;
        [XmlIgnore]
        public bool DoNotShowAdd
        {
            get => doNotShowAdd;
            set => SetProperty(ref doNotShowAdd, value);
        }

        private ObservableCollection<MeasureOfOffsetViewModel> allMeasures;
        [XmlIgnore]
        public ObservableCollection<MeasureOfOffsetViewModel> AllMeasures
        {
            get => allMeasures;
            set => SetProperty(ref allMeasures, value);
        }

        private MeasureOfOffsetViewModel currentMeasure;
        [XmlIgnore]
        public MeasureOfOffsetViewModel CurrentMeasure
        {
            get => currentMeasure;
            set => SetProperty(ref currentMeasure, value);
        }

        private int currentMeasureIndex;        
        public int CurrentMeasureIndex
        {
            get => currentMeasureIndex;
            set => SetProperty(ref currentMeasureIndex, value);
        }

        private string currentStationRms;
        public string CurrentStationRms 
        { 
            get => currentStationRms; 
            set => SetProperty(ref currentStationRms, value); 
        }

        #region BtnNextStation

        private DelegateCommand btnNextStation;

        [XmlIgnore]
        public ICommand BtnNextStation
        {
            get
            {
                if (btnNextStation == null)
                    btnNextStation = new DelegateCommand(BtnNextStationClick, BtnNextStationCanExecute);
                return btnNextStation;
            }
        }

        private bool BtnNextStationCanExecute()
        {
            try
            {
                return AllStations.Count > 1;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        private void BtnNextStationClick()
        {
            try
            {
                int cur = CurrentStation.NumberAsInt;
                int next;
                if (cur >= AllStations.Count)
                    //If cur is the last station, select the first one
                    next = 0;
                else
                    //Add 1 to have the next, remove 1 to have an index -> no operation
                    next = cur;
                CurrentStation = AllStations[next];
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        #region BtnPreviousStation

        private DelegateCommand btnPreviousStation;

        [XmlIgnore]
        public ICommand BtnPreviousStation
        {
            get
            {
                if (btnPreviousStation == null)
                    btnPreviousStation = new DelegateCommand(BtnPreviousStationClick, BtnPreviousStationCanExecute);
                return btnPreviousStation;
            }
        }

        private bool BtnPreviousStationCanExecute()
        {
            try
            {
                return AllStations.Count > 1;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        private void BtnPreviousStationClick()
        {
            try
            {
                int cur = CurrentStation.NumberAsInt;
                int next;
                if (cur == 1)
                    //If cur is the first station, select the last one
                    next = AllStations.Count - 1;
                else
                    //Remove 1 to have the previous, remove 1 to have an index -> no operation
                    next = cur - 2;
                CurrentStation = AllStations[next];
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        #region BtnNextRound

        private DelegateCommand btnNextRound;

        [XmlIgnore]
        public ICommand BtnNextRound
        {
            get
            {
                if (btnNextRound == null)
                    btnNextRound = new DelegateCommand(BtnNextRoundClick, BtnNextRoundCanExecute);
                return btnNextRound;
            }
        }

        private void BtnNextRoundClick()
        {
            try
            {
                int cur = CurrentStation.CurrentRoundAsInt;
                //If cur is the last station, create a new one
                if (cur == CurrentStation.RoundCount)
                {
                    CurrentStation.RoundCount++;
                    if (CurrentStation.StationModule != null)
                        CurrentStation.AddRound();
                }
                CurrentStation.CurrentRoundAsInt = cur + 1;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        private bool BtnNextRoundCanExecute()
        {
            try
            {
                return CurrentStation != null && (CurrentStation.CurrentRoundAsInt < 9);
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        #endregion

        #region BtnPreviousRound

        private DelegateCommand btnPreviousRound;

        [XmlIgnore]
        public ICommand BtnPreviousRound
        {
            get
            {
                if (btnPreviousRound == null)
                    btnPreviousRound = new DelegateCommand(BtnPreviousRoundClick);
                return btnPreviousRound;
            }
        }

        private void BtnPreviousRoundClick()
        {
            try
            {
                int cur = CurrentStation.CurrentRoundAsInt;
                int next;
                if (cur == 1)
                    //If cur is the first station, select the last one
                    next = CurrentStation.RoundCount;
                else
                    //Remove 1 to have the previous
                    next = cur - 1;
                CurrentStation.CurrentRoundAsInt = next;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        public void InitMeasurePage()
        {
            if (CurrentStation == null && AllStations != null && AllStations.Count > 0)
                CurrentStation = AllStations[0];
        }

        public void InitStationModule(StationViewModel station)
        {
            // The module 0 has the default settings, get a copy of it
            Station.Module oldStationModule = (Station.Module)SmartModule.StationModules[0];

            FinalModule finalModule = oldStationModule.FinalModule;
            finalModule.SetActiveStationModule(oldStationModule, false);
            Station.Module newStationModule = new Station.Module(finalModule, false);
            station.StationModuleGuid = newStationModule.Guid.ToString();
            var oldWorkingStation = oldStationModule.WorkingAverageStationLine;
            newStationModule.ChangeOperationID(oldWorkingStation.ParametersBasic._Operation);
            newStationModule.ChangeTeam(oldWorkingStation.ParametersBasic._Team);
            newStationModule.SetGeodeFilePathInNewStationLineModule(oldStationModule);
            newStationModule.SetInstrumentInNewStationLineModule(oldStationModule);
            newStationModule.ChangeTolerance(oldWorkingStation._Parameters._Tolerance);
            newStationModule.ChangeTemperature(oldWorkingStation._Parameters._Temperature);
            newStationModule.CreatedOn = DateTime.Now;
            finalModule.SetActiveStationModule(newStationModule, false);

            // Apply the settings
            newStationModule._Name = "STM of " + station.Sequence._Name;
            newStationModule._ComputeStrategy = new LGC2Strategy();
            CloneableList<E.Point> points = new CloneableList<E.Point>();
            points.AddRange(station.Sequence.GetPoints());
            newStationModule.SetPointsToMeasure(points);

            ////Memorize the round/station guid association, because they may change role depending on RMS values, but they should keep the same round number
            station.RoundGuids = new CloneableList<string>
            {
                newStationModule.WorkingAverageStationLine.Guid.ToString(),
            };

            //Create the extra rounds
            for (int i = station.RoundGuids.Count; i < station.RoundCount; i++)
                station.AddRound();
        }

        private bool acceptChanges = true;

        internal void OnMeasureChanged(object sender, PropertyChangedEventArgs e, MeasureOfOffsetViewModel MeasureOfOffsetViewModel)
        {
            if (acceptChanges
                && (e.PropertyName == nameof(MeasureOfOffsetViewModel.RawReading))
                && (MeasureOfOffsetViewModel == CurrentMeasure))
            {
                // Do not react on changes triggered on CurrentMeasure during the process
                acceptChanges = false;

                try
                {
                    // Create the station module if it's the first measure
                    if (CurrentStation.StationModule == null)
                    {
                        // Save the values
                        string rawLevelReading = CurrentMeasure.RawReading;
                        // Initialize the station
                        InitStationModule(CurrentStation);
                        // Create the real measures
                        UpdateMeasuresFromStationModule();
                        // Write the values in the new CurrentMeasure
                        CurrentMeasure.RawReading = rawLevelReading;
                    }

                    // Convert the string values to double values (when format is ok) and save the double values to the Measure
                    // Also recomputes the offsets
                    SetWorkingAverage();
                    CurrentMeasure?.SaveMeasure();

                    // Reload the measures, to update the deltas, and restore the current measure
                    UpdateMeasuresFromStationModule();

                    // Compute the RMS and log page
                    ComputeAll(new List<StationViewModel> { CurrentStation });
                }
                catch (Exception ex)
                {
                    ShowMessageOfBug(ex);
                }
                finally
                {
                    // Accept future chnages
                    acceptChanges = true;
                }
            }
        }

        private void OnChangeMeasuresPage(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(CurrentMeasureIndex))
            {
                UpdateCurrentMeasure();
            }

            if (e.PropertyName == nameof(CurrentStation)
             || e.PropertyName == nameof(StationViewModel.CurrentRoundAsInt))
            {
                if (CurrentStation != null)
                {
                    // Clear all the measures to avoid crash because WPF doesn't understand what changed or not
                    if (e.PropertyName == nameof(CurrentStation))
                        AllMeasures = new ObservableCollection<MeasureOfOffsetViewModel>();

                    if (CurrentStation.StationModule == null)
                    {
                        // Use temp measures to avoid creating the StationModule until we save
                        UpdateMeasuresFromPointList();
                    }
                    else
                    {
                        // Get the measures from StationModule
                        UpdateMeasuresFromStationModule();
                    }

                    // Select the first measure
                    if (AllMeasures.Count > 0)
                        CurrentMeasureIndex = 0;
                }
            }

            if (e.PropertyName == nameof(CurrentStation)
             || e.PropertyName == nameof(StationViewModel.CurrentRoundAsInt)
             || e.PropertyName == nameof(StationViewModel.Rms))
            {
                string rms = CurrentStation?.Rms;
                CurrentStationRms = string.IsNullOrEmpty(rms) ? "NA" : rms;
            }

            //Update dependant properties
            if ((e.PropertyName == nameof(CurrentStation)
               || e.PropertyName == nameof(StationViewModel.CurrentRoundAsInt)
               || e.PropertyName == nameof(StationViewModel.AllRounds))
               && CurrentStation != null)
            {
                ShowAdd = CurrentStation.CurrentRoundAsInt == CurrentStation.RoundCount;
                DoNotShowAdd = !ShowAdd;
            }
        }

        private void UpdateMeasuresFromPointList()
        {
            // Do not react on changes triggered on CurrentMeasure during the process
            acceptChanges = false;

            List<M.MeasureOfOffset> tempMeasures = CurrentStation.Sequence.GetPoints().ConvertAll(InitMeasureOfOffset);
            UpdateViewModelList(listToUpdate: AllMeasures,
                                newValues: tempMeasures,
                                ElementInitializer: InitMeasureOfOffsetViewModel,
                                ElementUpdater: UpdateMeasureOfOffsetViewModel);
            UpdateCurrentMeasure();

            // React again
            acceptChanges = true;
        }

        private M.MeasureOfOffset InitMeasureOfOffset(E.Point p)
        {
            // Create a temporary measure
            M.MeasureOfOffset m = new M.MeasureOfOffset()
            {
                _Status = new M.States.Temporary(),
                _Point = p,
                _InstrumentSN = CurrentInstrumentModule.Instrument._SerialNumber
            };

            return m;
        }

        private void UpdateMeasuresFromStationModule()
        {
            SetWorkingAverage();
            List<M.MeasureOfOffset> stationMeasures = CurrentStation.StationModule.WorkingAverageStationLine._ListStationLine[0]._MeasureOffsets;
            UpdateViewModelList(listToUpdate: AllMeasures,
                                newValues: stationMeasures,
                                ElementInitializer: InitMeasureOfOffsetViewModel,
                                ElementUpdater: UpdateMeasureOfOffsetViewModel);
            UpdateCurrentMeasure();
        }

        private MeasureOfOffsetViewModel InitMeasureOfOffsetViewModel(M.MeasureOfOffset m)
        {
            MeasureOfOffsetViewModel MeasureOfOffsetViewModel = new MeasureOfOffsetViewModel();
            MeasureOfOffsetViewModel.Initialize(this, m);
            return MeasureOfOffsetViewModel;
        }

        private void UpdateMeasureOfOffsetViewModel(MeasureOfOffsetViewModel model, M.MeasureOfOffset level)
        {
            model.Measure = level;
            model.LoadMeasure();
        }

        private void UpdateCurrentMeasure()
        {
            if (AllMeasures != null
                && CurrentMeasureIndex >= 0
                && CurrentMeasureIndex < AllMeasures.Count)
            {
                CurrentMeasure = AllMeasures[CurrentMeasureIndex];
            }
            else
            {
                CurrentMeasure = null;
            }
            // Force the update on all fields based on CurrentMeasure
            // Because the propoerties of the MeasureOfOffset can have changed before we assigned it to CurrentMeasure
            OnPropertyChanged(nameof(CurrentMeasure));
        }

        private void SetWorkingAverage()
        {
            int index = CurrentStation.CurrentRoundAsInt - 1;
            string guid = CurrentStation.RoundGuids[index];
            CurrentStation.StationModule.SetWorkingAverageStationLineByGuid(guid);
        }
    }
}
