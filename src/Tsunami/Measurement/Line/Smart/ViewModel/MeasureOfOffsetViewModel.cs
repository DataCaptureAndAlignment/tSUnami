﻿using System;
using System.ComponentModel;
using TSU.Common.Blocks;
using TSU.Common.Measures;
using TSU.Common.Smart;
using R = TSU.Properties.Resources;

namespace TSU.Line.Smart
{
    public class MeasureOfOffsetViewModel : ViewModelBase
    {
        public MeasureOfOffset Measure { get; set; } = null;
        public ViewModel Parent { get; private set; }
        public StationViewModel Station { get; set; } = null;

        private string instrumentName;
        public string InstrumentName
        {
            get => instrumentName;
            set => SetProperty(ref instrumentName, value);
        }

        private string rawReading;
        public string RawReading
        {
            get => rawReading;
            set => SetProperty(ref rawReading, value);
        }

        private string rawReadingError;
        public string RawReadingError
        {
            get => rawReadingError;
            set => SetProperty(ref rawReadingError, value);
        }

        private string pointName;
        public string PointName
        {
            get => pointName;
            set => SetProperty(ref pointName, value);
        }

        private string delta;
        public string Delta
        {
            get => delta;
            set => SetProperty(ref delta, value);
        }

        internal void LoadMeasure()
        {
            if (Measure._RawReading.IsNa())
                RawReading = "";
            else
                RawReading = MeasureFormatting.FormatDistance(Measure._RawReading * 1000d, MeasureFormatting.DistanceUnit.mm);
            Delta = ComputeDelta();
            CheckInput();
            PointName = Measure._PointName;
            InstrumentName = Measure._InstrumentSN;
        }

        private string ComputeDelta()
        {
            if (Measure._RawReading.IsNa())
                return "";

            int measuresCount = 0;
            double rawReadingSum = 0d;

            foreach (Station.Average r in Station.AllRounds)
                if (r?._ListStationLine != null && r._ListStationLine.Count > 0 && r._ListStationLine[0]._MeasureOffsets != null)
                    foreach (MeasureOfOffset m in r._ListStationLine[0]._MeasureOffsets)
                        if (!ReferenceEquals(m, Measure) && m._PointName == Measure._PointName && !m._RawReading.IsNa())
                        {
                            measuresCount++;
                            rawReadingSum += m._RawReading;
                        }

            if (measuresCount == 0)
                return R.String_Unknown;

            return MeasureFormatting.FormatDistance((Measure._RawReading - (rawReadingSum / measuresCount)) * 1000d, MeasureFormatting.DistanceUnit.mm);
        }

        internal void SaveMeasure()
        {
            double readingStorageValue = CheckInput();

            if (Math.Abs(readingStorageValue - Measure._RawReading) >= 0.000005d)
            {
                //Station?.StationModule?._AverageStationLine?.CopyAsObsolete(Measure);
                Measure._RawReading = readingStorageValue;
                Measure._Date = DateTime.Now;
                if (Station?.StationModule?.WorkingAverageStationLine?.ParametersBasic != null)
                    Station.StationModule.WorkingAverageStationLine.ParametersBasic.LastChanged = DateTime.Now;
                Station?.StationModule?.UpdateOffsets(Measure);
                LoadMeasure();
            }
        }

        private double CheckInput()
        {
            if (double.TryParse(RawReading, out double rawLevelReadingAsDouble))
            {
                RawReadingError = string.Empty;
                RawReading = MeasureFormatting.FormatDistance(rawLevelReadingAsDouble, MeasureFormatting.DistanceUnit.mm);
                return rawLevelReadingAsDouble / 1000d;
            }
            else
            {
                RawReadingError = R.StringSmart_RawLevelReadingInvalid;
                return Tsunami2.Preferences.Values.na;
            }
        }

        internal void Initialize(ViewModel parent, MeasureOfOffset measure)
        {
            Debug.WriteInConsole($"StationViewModel.Initialize for measure {measure.Guid}");

            // Remove notifications
            Suspend();

            Station = parent.CurrentStation;
            Measure = measure;
            Parent = parent;
            LoadMeasure();

            //Restore or begin notifications            
            PropertyChanged += OnPropertyChanged;
        }

        public void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Parent.OnMeasureChanged(sender, e, this);
        }

        public override void Suspend()
        {
            base.Suspend();
            Debug.WriteInConsole($"Suspended MeasureOfOffsetViewModel for measure {Measure?.Guid}");
        }
    }
}
