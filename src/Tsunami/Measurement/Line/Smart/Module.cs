﻿using System;
using System.Xml.Serialization;

namespace TSU.Line.Smart
{
    [Serializable]
    [XmlType("Line.Smart.Module")]
    public class Module : Common.Smart.Module
    {
        public ViewModel ViewModel { get; set; }

        public override Common.ObservationType ObservationType { get; set; } = Common.ObservationType.Ecarto;

        public Module() // need a parameterless constructor for xml serialazation
            : base()
        {
        }

        public Module(IModule parentModule, string nameAndDescription)
            : base(parentModule, nameAndDescription)
        {
            ViewModel.LoadModule(this);
        }

        public override void Initialize()
        {
            Debug.WriteInConsole($"Initialization started {this._Name}");
            base.Initialize();
            Station.Module stm = new Station.Module(this, false);
            SwitchToANewStationModule(stm);
            ViewModel = new ViewModel(this);
            Debug.WriteInConsole($"Initialization ended {this._Name}");
        }

        internal override void ReCreateWhatIsNotSerialized(bool saveSomeMemory = false)
        {
            base.ReCreateWhatIsNotSerialized(saveSomeMemory);
            ViewModel.LoadModule(this);
        }

        public override void Dispose()
        {
            base.Dispose();
            ViewModel.Suspend();
        }
    }
}
