﻿using MathNet.Numerics.Statistics;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using TSU.Common.Blocks;
using TSU.Common.Measures;
using CS = TSU.Common.Smart;

namespace TSU.Line.Smart.MeasureInput
{
    public class ViewModel : ViewModelBase
    {
        #region Properties

        public ObservableCollection<SubReading> SubReadings { get; set; }

        public class SubReading : ViewModelBase
        {
            private double reading;
            public double Reading
            {
                get => reading;
                set
                {
                    Debug.WriteInConsole($"Reading.set, reading={reading}, value={value}");
                    if (SetProperty(ref reading, value))
                        ReadingAsString = double.IsNaN(reading) ? string.Empty : CS.MeasureFormatting.FormatDistance(reading, CS.MeasureFormatting.DistanceUnit.mm);
                }
            }

            private string readingAsString;
            public string ReadingAsString
            {
                get => readingAsString;
                set
                {
                    Debug.WriteInConsole($"ReadingAsString.set, readingAsString={readingAsString}, value={value}");
                    string filteredValue = CS.MeasureFormatting.FilterInputRelativeDouble(value);
                    Debug.WriteInConsole($"ReadingAsString.set, filteredValue={filteredValue}");
                    if (SetProperty(ref readingAsString, filteredValue))
                    {
                        if (!double.TryParse(filteredValue, out double newValue))
                            newValue = double.NaN;
                        Reading = newValue;
                    }
                }
            }

            private bool use;
            public bool Use
            {
                get => use;
                set => SetProperty(ref use, value);
            }

            private double delta;

            public double Delta
            {
                get => delta;
                set
                {
                    if (SetProperty(ref delta, value))
                        DeltaAsString = double.IsNaN(delta) ? string.Empty : CS.MeasureFormatting.FormatDistance(delta, CS.MeasureFormatting.DistanceUnit.mm, false);
                }
            }

            private string deltaAsString;
            public string DeltaAsString
            {
                get => deltaAsString;
                set => SetProperty(ref deltaAsString, value);
            }

            public SubReading()
            {
                reading = double.NaN;
                use = true;
            }

            public override string ToString()
            {
                return $"value={reading},use={use}";
            }

        }

        private double mean;
        private double Mean
        {
            get => mean;
            set
            {
                //Update the formatted numbers, only if the values have changed
                if (SetProperty(ref mean, value))
                    MeanAsString = double.IsNaN(mean) ? string.Empty : CS.MeasureFormatting.FormatDistance(mean, CS.MeasureFormatting.DistanceUnit.mm);
            }
        }

        private string meanAsString;
        public string MeanAsString
        {
            get => meanAsString;
            set => SetProperty(ref meanAsString, value);
        }

        private double sigma;
        private double Sigma
        {
            get => sigma;
            set
            {
                //Update the formatted numbers, only if the values have changed
                if (SetProperty(ref sigma, value))
                    SigmaAsString = double.IsNaN(sigma) ? string.Empty : CS.MeasureFormatting.FormatDistance(sigma, CS.MeasureFormatting.DistanceUnit.mm, false);
            }
        }

        private string sigmaAsString;
        public string SigmaAsString
        {
            get => sigmaAsString;
            set => SetProperty(ref sigmaAsString, value);
        }

        private bool negativeSign;
        public bool NegativeSign
        {
            get => negativeSign;
            set => SetProperty(ref negativeSign, value);
        }

        private MeasureOfOffset measure;

        public MeasureOfOffset Measure
        {
            get => measure;
            set => SetProperty(ref measure, value);
        }

        #endregion

        public ViewModel()
        {
            SubReadings = new ObservableCollection<SubReading>();
            RegisterEvents();
        }

        private void RegisterEvents()
        {
            SubReadings.CollectionChanged += OnCollectionChanged;
            PropertyChanged += OnPropertyChanged;
        }

        private void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
                foreach (SubReading sr in e.NewItems)
                    sr.PropertyChanged += OnPropertyChanged;
            UpdateSigns();
            UpdateMeanStandardDeviation();
        }

        private static readonly string[] propertiesToCheckForUpdateMeanStandardDeviation = {
            nameof(SubReading.Use),
            nameof(SubReading.Reading),
            nameof(SubReading.ReadingAsString)
        };

        private static readonly string[] propertiesToCheckForUpdateSigns = {
            nameof(NegativeSign),
            nameof(SubReading.Reading),
            nameof(SubReading.ReadingAsString)
        };

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(Measure))
                UpdateFromMeasure();
            if (propertiesToCheckForUpdateSigns.Contains(e.PropertyName))
                UpdateSigns();
            if (propertiesToCheckForUpdateMeanStandardDeviation.Contains(e.PropertyName))
                UpdateMeanStandardDeviation();
        }

        /// <summary>
        /// Check that all the values have the right sign
        /// </summary>
        private void UpdateSigns()
        {
            foreach (SubReading subReading in SubReadings)
            {
                if (NegativeSign)
                {
                    if (subReading.Reading > 0)
                        subReading.Reading = -subReading.Reading;
                }
                else
                {
                    if (subReading.Reading < 0)
                        subReading.Reading = -subReading.Reading;
                }
            }
        }

        /// <summary>
        /// Update Mean and Sigma
        /// </summary>
        private void UpdateMeanStandardDeviation()
        {
            //Compute Mean and Standard Deviation (Sigma)
            (Mean, Sigma) = SubReadings.Where(sr => sr.Use && !double.IsNaN(sr.Reading))
                                                 .Select(sr => sr.Reading)
                                                 .MeanStandardDeviation();

            //Compute deltas
            foreach (SubReading sr in SubReadings)
                sr.Delta = Math.Abs(sr.Reading - Mean);
        }

        internal void UpdateMeasure()
        {
            measure._Date = DateTime.Now;
            measure._RawReading = double.IsNaN(Mean) ? DoubleValue.Na : (Mean / 1000);
            measure._StandardDeviation = double.IsNaN(Mean) ? DoubleValue.Na : (Sigma / 1000);
            measure.SubReadings = new List<MeasureOfOffset.SubReading>();
            foreach (SubReading sr in SubReadings)
                if (!double.IsNaN(sr.Reading))
                    measure.SubReadings.Add(new MeasureOfOffset.SubReading(sr.Reading / 1000, sr.Use));
        }

        private void UpdateFromMeasure()
        {
            // Determine the sign; invalid values are assumed to be positive as a default value
            NegativeSign = !measure._RawReading.IsNa()
                        && measure._RawReading != -9999
                        && measure._RawReading < 0;

            // Copy the subreadings, this will trigger UpdateSigns and UpdateMeanStandardDeviation
            SubReadings.Clear();
            if (measure.SubReadings != null)
                foreach (var sr in measure.SubReadings)
                    SubReadings.Add(new SubReading()
                    {
                        Reading = sr.Reading * 1000,
                        Use = sr.Use
                    });
        }


        /// <summary>
        /// Stop managing the update events
        /// </summary>
        public void Suspend()
        {
            foreach (SubReading sr in SubReadings)
                sr.PropertyChanged -= OnPropertyChanged;
            SubReadings.CollectionChanged -= OnCollectionChanged;
            PropertyChanged -= OnPropertyChanged;
        }

        public override string ToString()
        {
            StringWriter sw = new StringWriter();
            int counter = 0;
            foreach (SubReading v in SubReadings)
            {
                sw.WriteLine($"SubReadings[{counter}]={v}");
                counter++;
            }
            sw.WriteLine($"Mean={MeanAsString}");
            sw.WriteLine($"Sigma={SigmaAsString}");
            sw.WriteLine($"NegativeSign={NegativeSign}");
            return sw.ToString();
        }
    }
}
