﻿using System.Drawing;
using System.Windows.Controls;
using TSU.Common.Measures;
using WF = System.Windows.Forms;
using WFI = System.Windows.Forms.Integration;
using R = TSU.Properties.Resources;
using TSU.Views;
using System.Collections.Generic;
using System;

namespace TSU.Line.Smart.MeasureInput
{
    /// <summary>
    /// Interaction logic for MeasureInput.xaml
    /// </summary>
    public partial class MeasureInput : UserControl
    {
        public MeasureInput()
        {
            App.EnsureApplicationResources();
            InitializeComponent();
        }

        private void OnAddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            e.NewItem = new ViewModel.SubReading();
        }

        public static bool Show(ref MeasureOfOffset measureOfOffset, TsuView parent = null, string sender = "")
        {
            //default parent is TsunamiView
            if (parent is null)
                parent = Tsunami2.View;

            //Create ViewModel
            ViewModel vm = new ViewModel
            {
                Measure = measureOfOffset
            };

            //Create View
            MeasureInput mi = new MeasureInput
            {
                DataContext = vm
            };

            //Create the eh
            WFI.ElementHost eh = new WFI.ElementHost
            {
                BackColor = Color.White,
                Child = mi,
                Height = 300
            };

            var res = parent.ShowMessageWithFillControl(control: eh,
                                                        titleAndMessage: R.T_M_ECARTO_MEASURE_AVERAGE,
                                                        buttonTexts: new List<string>() { R.T_OK, R.T_CANCEL },
                                                        sender: sender);

            bool isOk = res.TextOfButtonClicked == R.T_OK;

            if (isOk)
                vm.UpdateMeasure();

            vm.Suspend();
            eh.Dispose();
            vm.Dispose();

            return isOk;
        }
    }
}
