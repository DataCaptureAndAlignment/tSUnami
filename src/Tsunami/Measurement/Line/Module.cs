using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using TSU.Common;
using TSU.Common.Compute.Compensations;
using TSU.Common.Elements;
using TSU.Views;
using TSU.Views.Message;
using R = TSU.Properties.Resources;

namespace TSU.Line
{
    [XmlType(TypeName = "Line.Module")]
    public class Module : FinalModule
    {
        #region fields

        public override ObservationType ObservationType { get; set; } = Common.ObservationType.Ecarto;
        [XmlIgnore]
        public Station.Module stationLineModule
        {
            get
            {
                return this._ActiveStationModule as Station.Module;
            }
            set
            {
                this._ActiveStationModule = value;
            }
        }
        [XmlIgnore]
        public new CompositeView View
        {
            get
            {
                return this._TsuView as CompositeView;
            }
            set
            {
                this._TsuView = value;
            }
        }
        public TsuBool ShowSaveMessageOfSuccess { get; set; }
        #endregion
        #region Constructor
        public Module()
            : base()
        {
            ShowSaveMessageOfSuccess = new TsuBool();
        }

        public Module(TSU.Module parentModule, string nameAndDescription)
            : base(parentModule, nameAndDescription)
        {
            ShowSaveMessageOfSuccess = new TsuBool();
        }
        #endregion
        #region Observer

        /// <summary>
        /// dispatcheur d'evenement si un type n'est pas connu il est trait� par OnNextBasedOn(TsuObject TsuObject)
        /// </summary>
        /// <param name="TsuObject"></param>
        public override void OnNext(TsuObject TsuObject)
        {
            dynamic dynamic = TsuObject;
            OnNextBasedOn(dynamic);
        }

        /// <summary>
        /// �v�nement li� � la r�ception de points de l'Element MOdule Manager et ajout � la s�quence de points
        /// </summary>
        /// <param name="elementsFromElementModule"></param>
        public new void OnNextBasedOn(Common.Elements.Composites.CompositeElement elementsFromElementModule)
        {
            //lance la fonction pour donner les points � mesurer
            if (elementsFromElementModule != null)
            {
                if (elementsFromElementModule.Elements.Count > 0)
                {
                    if ((elementsFromElementModule.Elements[0] is Common.Elements.Composites.SequenceFil) && !(elementsFromElementModule is Common.Elements.Composites.SequenceFils) && (elementsFromElementModule.Elements.Count == 1))
                    {
                        //lance la fonction pour integrer les points de la s�quence simple
                        (this._ActiveStationModule as Station.Module).SetSequenceToMeasure(elementsFromElementModule.Elements[0] as Common.Elements.Composites.SequenceFil);
                    }
                    else
                    {
                        (this._ActiveStationModule as Station.Module).SetPointsToMeasure(elementsFromElementModule);
                    }
                }
                else
                {
                    (this._ActiveStationModule as Station.Module).SetPointsToMeasure(elementsFromElementModule);
                }
            }
        }
        #endregion
        #region Method

        /// <summary>
        /// Cr�e une nouvelle stationLine en conservant les param�tres de la station line pr�c�dente
        /// </summary>
        internal void CloneStation()
        {
            Station.Module newStationLineModule = new Station.Module(this);
            if (this._ActiveStationModule != null)
            {
                Station.Module oldStationLineModule = this._ActiveStationModule as Station.Module;
                newStationLineModule.ChangeOperation(oldStationLineModule._Station.ParametersBasic._Operation);
                newStationLineModule.ChangeTeam(oldStationLineModule._Station.ParametersBasic._Team);
                newStationLineModule.SetGeodeFilePathInNewStationLineModule(oldStationLineModule);
                newStationLineModule.SetInstrumentInNewStationLineModule(oldStationLineModule);
                newStationLineModule.ChangeTolerance(oldStationLineModule.WorkingAverageStationLine._Parameters._Tolerance);
                newStationLineModule.ChangeTemperature(oldStationLineModule.WorkingAverageStationLine._Parameters._Temperature);
                newStationLineModule.View.RedrawTreeviewParameters();
                newStationLineModule.CreatedOn = DateTime.Now;
            }
            this.SwitchToANewStationModule(newStationLineModule);
        }
        /// <summary>
        /// Cr�e une nouvelle stationLine sans conserver les param�tres de la station line pr�c�dente
        /// </summary>
        internal void NewStation()
        {
            this.SwitchToANewStationModule(new Station.Module(this));
        }
        /// <summary>
        /// Efface la station actuelle et choisit la pr�c�dente station
        /// On ne peut pas effacer la premi�re station
        /// </summary>
        internal void DeleteActualStation()
        {
            if (this._ActiveStationModule != null)
            {
                int index = this.StationModules.FindIndex(x => x._Station._Name == this._ActiveStationModule._Station._Name);
                if (this.StationModules.Count != 1)
                {
                    this.StationModules.Remove(this._ActiveStationModule);
                    if (index > 0) { index -= 1; }
                    else { index = 0; }
                    this.SetActiveStationModule(this.StationModules[index]);
                }
            }
        }

        internal override void SetActiveStationModule(Common.Station.Module stationModule, bool createView = true)
        {
            base.SetActiveStationModule(stationModule, true);
            this.View?.SetStationNameInTopButton(stationModule);
        }
        /// <summary>
        /// mise � jour point qui a �t� modifi� dans autre �l�ment manager
        /// </summary>
        /// <param name="originalPoint"></param>
        /// <param name="pointModified"></param>
        internal override void UpdateTheoCoordinates(Point originalPoint, Point pointModified)
        {
            base.UpdateTheoCoordinates(originalPoint, pointModified);
            foreach (Station.Module stm in this.StationModules.OfType<Station.Module>())
            {
                stm.UpdateTheoCoordinates(originalPoint, pointModified);
            }
        }
        /// <summary>
        /// Fait un calcul global avec LGC2 de toutes les stations de nivellement.
        /// </summary>
        internal void DoLibrCalculationWithLGC2()
        {
            string filepath = string.Format("{0}{1}{2}_{3}.inp", TSU.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, "LGC_Global\\", "GlobalEcartometry", TSU.Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now));
            if (!System.IO.Directory.Exists(string.Format("{0}{1}", TSU.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, "LGC_Global\\"))) { System.IO.Directory.CreateDirectory(string.Format("{0}{1}", TSU.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, "LGC_Global\\")); }
            if (System.IO.File.Exists(filepath))
            {
                string titleAndMessage = string.Format(R.StringLine_LGCReplaceFile, filepath);
                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_OK, R.T_DONT }
                };
                string respond = mi.Show().TextOfButtonClicked;
                if (respond == R.T_DONT) return;
            }
            Lgc2 lgc2 = new Lgc2(this, filepath);
            lgc2.Run(true, true);
            List<Point> pointList = lgc2._Output.GetCoordinatesResultsForWire(this);
            if (pointList.Count != 0)
            {
                //string saveName = this._ActiveStationModule._Name;
                //this._ActiveStationModule._Name = "GlobalEcartometryLGC2Calculation";
                foreach (Point item in pointList)
                {
                    //item._Origin = "GlobalEcartometryLGC2Calculation";
                    this._ElementManager.RemoveElementByNameAndOrigin(item._Name, "GlobalEcartometryLGC2Calculation");
                    this._ElementManager.AddElement("GlobalEcartometryLGC2Calculation", item);
                }
                this._ElementManager.View.UpdateView();
                this.Save("LIBR");
                //this._ActiveStationModule._Name = saveName;
            }
        }

        /// <summary>
        /// Export all stations that are ready to be saved in a dat file for Geode
        /// </summary>
        internal void ExportAll()
        {
            bool saveAllSuccess = true;
            string respond = "";
            string geodeFilePath = "";
            int wireNumber = 0;
            if (this._ActiveStationModule != null)
            {
                Station.Module module = this._ActiveStationModule as Station.Module;
                geodeFilePath = module.WorkingAverageStationLine.ParametersBasic._GeodeDatFilePath;
            }

            foreach (Station.Module item in this.StationModules.OfType<Station.Module>())
            {
                try
                {
                    wireNumber++;
                    //if (item.CheckIfReadyToBeSaved())
                    if (item.WorkingAverageStationLine._Parameters._State is Common.Station.State.WireReadyToBeSaved)
                    {
                        item.ShowSaveMessageOfSuccess.IsTrue = ShowSaveMessageOfSuccess.IsTrue;
                        item.ExportToGeode(false);
                        item.View.RedrawTreeviewParameters();
                        item.View.UpdateView();
                        if (item.WorkingAverageStationLine.ParametersBasic._GeodeDatFilePath != "")
                            geodeFilePath = item.WorkingAverageStationLine.ParametersBasic._GeodeDatFilePath;
                        if (!ShowSaveMessageOfSuccess.IsTrue)
                            ShowSaveMessageOfSuccess.IsTrue = stationLineModule.ShowSaveMessageOfSuccess.IsTrue;
                    }
                    else
                    {
                        if (stationLineModule.ecartoInstrument != null
                            && !(stationLineModule.WorkingAverageStationLine._Parameters._State is Common.Station.State.WireSaved)
                            && stationLineModule.WorkingAverageStationLine._Parameters._Team == R.String_UnknownTeam)
                        {
                            string titleAndMessage = string.Format(R.StringLine_CannotSaveWire, wireNumber);
                            new MessageInput(MessageType.Warning, titleAndMessage).Show();
                            saveAllSuccess = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"{R.T_COULD_NOT} {R.T_Export} {R.T_ECARTO_OFFSET}\r\n\r\n{ex.Message}");
                }

            }
            if (saveAllSuccess && geodeFilePath != "")
            {
                string titleAndMessage = string.Format(R.StringLine_SaveAllSuccess);
                MessageInput mi = new MessageInput(MessageType.GoodNews, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_OK, R.T_OpenFileLocation }
                };
                respond = mi.Show().TextOfButtonClicked;
            }
            else
            {
                if (saveAllSuccess)
                {
                    string titleAndMessage = string.Format(R.StringLine_SaveAllSuccess);
                    new MessageInput(MessageType.GoodNews, titleAndMessage).Show();
                }
            }
            if (respond == R.T_OpenFileLocation && System.IO.File.Exists(geodeFilePath))
            {
                System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo("explorer.exe", " /select, " + geodeFilePath));
            }
        }

        /// <summary>
        /// Exportes les nouveaux points dans le fichier theo dat
        /// </summary>
        internal void ExportNewPointTheoDat()
        {
            if (this._ActiveStationModule?.ElementModule != null)
            {
                List<Point> actualStationPoints = new List<Point>();
                List<Point> lgcGlobalPoints = new List<Point>();
                List<Point> allPoints = this._ActiveStationModule.ElementModule.GetPointsInAllElements();
                List<Point> newCreatedByUserPoints = allPoints.FindAll(x => x._Origin == "Created by user");
                if (newCreatedByUserPoints != null)
                {
                    ///Remplace les nouveaux points par les points calcul�s dans la station actuelle si disponible et si des coordonn�es sont diponibles
                    string groupName = (this._ActiveStationModule as Station.Module).measPointgroupName;
                    groupName = groupName.Replace('.', '_');
                    foreach (Point item in newCreatedByUserPoints)
                    {
                        Point pt = allPoints.Find(x => x._Origin == groupName && x._Name == item._Name && x._Coordinates.HasCcs);
                        if (pt != null)
                        {
                            actualStationPoints.Add(pt);
                        }
                        else
                        {
                            ///il faut un H au point pour r�importer les points dans Tsunami 
                            Point itemWithH = item.DeepCopy();
                            if (item._Coordinates.Ccs.Z.Value == TSU.Preferences.Preferences.NotAvailableValueNa)
                            {
                                itemWithH._Coordinates.Ccs.Z.Value = 400;
                            }
                            actualStationPoints.Add(itemWithH);
                        }
                    }
                    ///Remplace les nouveaux points par les points calcul�s dans un calcul LGC global si disponible
                    foreach (Point item in actualStationPoints)
                    {
                        Point pt = allPoints.Find(x => x._Origin == "GlobalLevellingLGC2Calculation" && x._Name == item._Name && x._Coordinates.HasCcs);
                        if (pt != null)
                        {
                            lgcGlobalPoints.Add(pt);
                        }
                        else
                        {
                            if (item._Coordinates.HasCcs) lgcGlobalPoints.Add(item);
                        }
                    }
                    if (lgcGlobalPoints.Count > 0)
                    {
                        string path = TsuPath.GetFileNameToSave("", string.Format("{0:yyyyMMdd}", DateTime.Now) + "_Elements", "Geode(*.dat) | *.dat", "");
                        if (path != "")
                        {
                            IO.SUSoft.Geode.Export(path, lgcGlobalPoints);
                            Shell.Run(path);
                        }
                    }
                    else
                    {
                        new MessageInput(MessageType.Warning, R.StringLine_CannotExportPointInTheoDat).Show();
                    }
                }
            }
        }
        #endregion

        public override string ToString()
        {
            return $"{this._Name} = {base.ToString()}";
        }

        internal override IEnumerable<Common.Station> GetStations()
        {
            var stations = new List<Common.Station>();
            foreach (var item in this.StationModules)
            {
                stations.Add(item._Station);
            }
            return stations;
        }

        /// <summary>
        /// une current module as input for Beam offset computation
        /// </summary>
        public override void BOC_Compute()
        {
            // identify assembly
            var ids = new List<string>();

            // Add
            if (this._ActiveStationModule is Station.Module lsm)
            {
                foreach (Point point in lsm._MeasPoint)
                {
                    var tempId = Point.GetAssembly(point._Name);
                    ids.AddIfNotExisting(tempId);
                }
            }

            // Remove calas
            foreach (Point point in this.CalaPoints)
            {
                var tempId = Point.GetAssembly(point._Name);
                ids.Remove(tempId);
            }

            // select module: current + roll
            var modules = new List<FinalModule>
            {
                this
            };
            var rollModules = Tsunami2.Properties.MeasurementModules.GetRollModules();
            modules.AddRange(rollModules);

            // run BOC for each assembly
            string message = "";
            foreach (string id in ids)
            {
                try
                {
                    var r = Common.Compute.Compensations.BeamOffsets.WorkFlow.Compute(this, modules, id, ObservationType.Ecarto);
                    string titleAndMessage = r.ToString("4ECARTO");
                    new MessageInput(MessageType.FYI, titleAndMessage).Show();
                }
                catch (Exception ex)
                {
                    message += ex.ToString() + "\r\n";
                }

            }
        }
    }
}
