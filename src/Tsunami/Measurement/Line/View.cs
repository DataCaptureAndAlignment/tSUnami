﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;
using R = TSU.Properties.Resources;
using TSU.ENUM;
using System.Xml.Serialization;
using TSU.Views;
using TSU.Common.Elements;
using TSU.Common.Instruments;
using TSU.Common.Measures;
using T = TSU.Tools;
using M = TSU;
using System.Globalization;
using System.Drawing;


namespace TSU.Line
{
    public class View : CompositeView
    {
        private Buttons buttons;
        private double na { get; set; } =TSU.Tsunami2.Preferences.Values.na;
        public Line.Module LineModule
        {
            get
            {
                return this._Module as Line.Module;
            }
        }
        public View(Line.Module parent)
            : base(parent, Orientation.Horizontal, ModuleType.Theodolite)
        {
            this.Image =R.Line;
            string creationDate = "(" + R.StringBigButton_CreatedOn + this.Module.CreatedOn.ToString("G", CultureInfo.CreateSpecificCulture("nl-BE")) + ")";
            string buttontext = string.Format("{0} {1};{2}", T.Conversions.StringManipulation.SeparateTitleFromMessage(this._Module.NameAndDescription), creationDate, T.Conversions.StringManipulation.SeparateMessageFromTitle(this._Module.NameAndDescription));
            this.buttons = new Buttons(this);
            this.AddTopButton(
                buttontext,
                R.Line, this.ShowContextMenuGlobal);
            base.AddPlusView(buttons.New);
            this.Visible = true;
            UpdateView();
        }
        /// <summary>
        /// Change le nom de la station active dans le top button
        /// </summary>
        /// <param name="stationLevelModule"></param>
        internal override void SetStationNameInTopButton(Common.Station.Module stationLineModule)
        {
            this._PanelTop.Controls.Clear();
            string creationDate = "(" + R.StringBigButton_CreatedOn + this.Module.CreatedOn.ToString("G", CultureInfo.CreateSpecificCulture("nl-BE")) + ")";
            string buttontext = string.Format("{0} {1};{2}", T.Conversions.StringManipulation.SeparateTitleFromMessage(this._Module.NameAndDescription), creationDate, T.Conversions.StringManipulation.SeparateMessageFromTitle(this._Module.NameAndDescription));
            Common.FinalModule m;
            if ((m = this.LineModule) != null)
            {
                int stationNumber = 0;
                foreach (Station.Module stationModule in m.StationModules)
                {
                    if (stationModule != null)
                    {
                        stationNumber++;
                        if (stationModule._Station._Name == stationLineModule._Station._Name)
                        {
                            buttontext = string.Format("{0} {1}: {2};{3}",
                                T.Conversions.StringManipulation.SeparateTitleFromMessage(this._Module.NameAndDescription),
                                creationDate,
                                string.Format(T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringLine_BigButtonLineModuleView), stationNumber),
                                string.Format("{0} ({1})", T.Conversions.StringManipulation.SeparateMessageFromTitle(this._Module.NameAndDescription), stationModule._Station._Name));
                        }
                    }
                }
            }
            this.AddTopButton(buttontext, R.Line, this.ShowContextMenuGlobal);
        }
        private void ShowContextMenuGlobal()
        {
            this.contextButtons.Clear();
            //this.contextButtons.Add(buttons.Open);
            //this.contextButtons.Add(buttons.Save);
            this.contextButtons.Add(buttons.New);
            this.contextButtons.Add(buttons.FinishModule);
            if (this.LineModule._ActiveStationModule != null)
            {
                Line.Station.View v = this.LineModule._ActiveStationModule.View as Line.Station.View;
                if (v!=null) v.CheckDatagridIsInEditMode();
                if (this.LineModule.StationModules.Count > 0)
                {
                    this.contextButtons.Add(buttons.exportAllDat);
                    // Bouton export new points seulement si points created by user
                    if (this.LineModule._ActiveStationModule != null)
                    {
                        if (this.LineModule._ActiveStationModule.ElementModule != null)
                        {
                            List<TSU.Common.Elements.Point> allPoints = new List<TSU.Common.Elements.Point>();
                            allPoints = this.LineModule._ActiveStationModule.ElementModule.GetPointsInAllElements();
                            if (allPoints.FindIndex(x => x._Origin == "Created by user") != -1)
                            { this.contextButtons.Add(buttons.exportTheoDat); }
                        }
                    }
                    this.contextButtons.Add(buttons.librLgc2);

                    this.contextButtons.Add(Common.Compute.Compensations.BeamOffsets.WorkFlow.Buttons.GetComputeButton(this.LineModule.BOC_Compute));
                }
                if (this.LineModule._ActiveStationModule._Station.ParametersBasic._Instrument._Model != null)
                {
                    this.contextButtons.Add(buttons.Clone);
                    this.contextButtons.Remove(buttons.New);
                }

            }
            Common.FinalModule m;
            List<BigButton> stationButtons = new List<BigButton>();
            if ((m = this.LineModule) != null)
            {
                int stationNumber = 0;
                ///Ajoute le bouton de suppression de la station actuelle que s'il n'y a pas de mesures encodées et s'il y a plusieurs stations
                if (this.LineModule.StationModules.Count > 1)
                {
                    bool addDeleteButton = true;
                    if (this.LineModule._ActiveStationModule != null)
                    {
                        Line.Station.Module am = this.LineModule._ActiveStationModule as Line.Station.Module;
                        foreach (Line.Station item in am.WorkingAverageStationLine._ListStationLine)
                        {
                            if (item._MeasureOffsets.FindIndex(x => x._RawReading != na) != -1) addDeleteButton = false;
                        }
                        if (addDeleteButton) this.contextButtons.Add(buttons.deleteActualStation);
                    }
                }
                    foreach (Station.Module stationModule in m.StationModules)
                {
                    if (stationModule != null)
                    {
                        stationNumber++;
                        BigButton stationButton = new BigButton(
                            string.Format(R.StringLine_BigButtonLineModuleView, stationNumber, stationModule._Station._Name),
                            this.Image, stationModule.ShowInParentModule);
                        if (stationModule == this.LineModule._ActiveStationModule)
                        {
                            stationButton.SetColors(M.Tsunami2.Preferences.Theme.Colors.Highlight);
                        }
                        stationButtons.Add(stationButton);
                    }
                }
                //Inverse la liste des stations
                for (int i = stationButtons.Count - 1; i >= 0; i--)
                {
                    this.contextButtons.Add(stationButtons[i]);
                }
            }
            bool ok = this.ShowPopUpMenu(contextButtons);
            // il peut arriver lors de l'ajout d'un nouveau bouton que l'affichage des contrôles dans le popup menu ne soit pas dans l'ordre. Le réaffichage du pop up semble résoudre le problème.
            //if (!ok) { this.ShowContextMenuGlobal(); }
        }
        private void OnNewStation(object sender, EventArgs e)
        {
            NewStation();
        }
        public void NewStation()
        {
            this.TryAction(this.LineModule.NewStation);
        }
        public void Save()
        {
            this.TryAction(()=>this.LineModule.Save());
        }
        //public void Open()
        //{
        //    this.TryAction(this.LineModule.Open);
        //}
        public void CloneStation()
        {
            this.TryAction(this.LineModule.CloneStation);
        }
        internal void DeleteActualStation()
        {
            this.TryAction(this.LineModule.DeleteActualStation);
        }
        private void FinishModule()
        {
            this.TryAction(this.LineModule.FinishModule);
        }
        internal void DoFreeCalculationWithLGC2()
        {
            this.TryAction(this.LineModule.DoLibrCalculationWithLGC2);
        }
        private void ExportAllDatToGeode()
        {
            this.TryAction(this.LineModule.ExportAll);
        }
        private void ExportNewPointTheoDat()
        {
            this.TryAction(this.LineModule.ExportNewPointTheoDat);
        }
        class Buttons
        {
            private View view;
            public BigButton New;
            //public BigButton Open;
            public BigButton Save;
            public BigButton Clone;
            public BigButton deleteActualStation;
            public BigButton FinishModule;
            public BigButton librLgc2;
            public BigButton BOC;
            public BigButton exportAllDat;
            public BigButton exportTheoDat;
            public Buttons(View view)
            {
                this.view = view;
                //Open = new BigButton(string.Format(R.T_OpenModule, this.view.LineModule._Name),R.Open, view.Open);
                Save = new BigButton(string.Format(R.T_SaveModule, this.view.LineModule._Name),R.Save, view.Save);
                New = new BigButton(R.T074,R.Add, view.NewStation);
                Clone = new BigButton(R.StringLine_CloneStation,R.Copy, view.CloneStation);
                librLgc2 = new BigButton(R.StringLine_GlobalCalculationLGC2, R.Lgc, view.DoFreeCalculationWithLGC2);
                BOC = new BigButton(R.T_BOC, R.BOC, view.LineModule.BOC_Compute);
                deleteActualStation = new BigButton(R.StringLine_DeleteActualWire, R.Delete_Ancrage, view.DeleteActualStation);
                FinishModule = new BigButton(R.String_GM_FinishModule, R.FinishHim, view.FinishModule);
                exportAllDat = new BigButton(R.StringLine_SaveAll, R.Save, view.ExportAllDatToGeode);
                exportTheoDat = new BigButton(R.StringLine_ExportTheoDat, R.Export, view.ExportNewPointTheoDat);
            }
        }
    }
}
