﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using E = TSU.Line;
using M = TSU;
using TSU.Common.Guided.Steps;

namespace TSU.Line.GuidedModules.Steps
{
    static public class Alignment
    {
        static public Common.Guided.Module Get(M.Module parentModule)
        {
            //Guided.Group.Module GrGm = new Guided.Group.Module(parentModule, R.String_GuidedLine_GroupWireName);
            Common.Guided.Group.Module GrGm = parentModule as Common.Guided.Group.Module;

            // Guided.Module gm = new Guided.Module(parentModule, String.Format(R.StringGuidedLine_WireName, "", "A", "B"));
            Common.Guided.Module gm = new Common.Guided.Module(parentModule, $"{R.StringGuidedLine_WireName};A=>B");
            gm.guideModuleType = ENUM.GuidedModuleType.AlignmentEcartometry;
            gm.ObservationType = Common.ObservationType.Ecarto;
            // Invisible step: : Add needed station modules
            gm.SwitchToANewStationModule(new Line.Station.Module(gm));
            //Stations.Offset.Module st = gm._ActiveStationModule as Stations.Offset.Module;

            BuildSteps(gm);

            gm.Start();

            return gm;
        }

        private static void BuildSteps(Common.Guided.Module gm)
        {
            //// Step 0: Choose Management.Instrument
            gm.Steps.Add(Steps.Measurement.ChooseEcartometer(gm));

            /// Step 1 : Choose CALA points
            gm.Steps.Add(Steps.Measurement.ChooseCalaPointsForElementAlignment(gm));

            /// Step 2 : Measure Wire
            gm.Steps.Add(Steps.Measurement.MeasureWireForElementAlignment(gm));

            /// Step 3 : Show dat saved file
            gm.Steps.Add(Management.ShowDatFiles(gm));

            /// Step 4 : Show LGC Files
            gm.Steps.Add(Management.ShowLGCFiles(gm));
        }

        internal static void ReCreateWhatIsNotSerialized(Common.Guided.Module gm)
        {
            gm.Steps = new List<Step>();
            Station.Module st = gm._ActiveStationModule as Station.Module;
            if (st != null) { st.View.moduleType = ENUM.ModuleType.Guided; }
            BuildSteps(gm);
            //gm.Start();
            //gm.MoveToStep(gm.currentIndex);
        }
    }
}
