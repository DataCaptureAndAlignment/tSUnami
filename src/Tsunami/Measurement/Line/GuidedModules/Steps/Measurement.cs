﻿using System;
using System.Collections.Generic;
using System.Linq;
using TSU.Common.Guided.Steps;
using TSU.Views;
using TSU.Views.Message;
using Drawing = System.Drawing;
using EL = TSU.Common.Elements;
using F = System.Windows.Forms;
using I = TSU.Common.Instruments;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Line.GuidedModules.Steps
{
    public static class Measurement
    {
        internal static Step MeasureWire(Common.Guided.Module guidedModule)
        {
            Station.Module st = guidedModule._ActiveStationModule as Station.Module;
            bool firstUse = true;
            //Char splitChar = '_';
            //string[] nameSplit = st._AverageStationLine._Name.Split(splitChar);
            //string n = string.Format("{0} {1} => {2}", R.String_GuidedLine_MeasureWireName, nameSplit[3], nameSplit[4]);
            //string n = string.Format("{0}=>{1}", nameSplit[3], nameSplit[4]);
            Step s = new Step(guidedModule, R.StringGuidedLine_WireMeas);
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                //Remove title and description to have more space for the measurements
                e.step.View.ClearAllControls();
                //st = e.ActiveStationModule as Stations.Offset.Module;
                ////Force la tolérance à 0.2mm pour les modules guidés
                //st.ChangeTolerance(0.0002);
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                st = e.ActiveStationModule as Station.Module;
                guidedModule.Utility = st.Utility;
                st.View.moduleType = ENUM.ModuleType.Guided;
                st.View.MaximizeDataGridView();
                st.View.RedrawTreeviewParameters();
                st.View.showAdmin = true;
                st.View.Dock = F.DockStyle.Bottom;
                //st.UpdateView();
                e.step.View.AddControl(st);

            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                e.step.Test();
                st = e.ActiveStationModule as Station.Module;
                guidedModule.Utility = st.Utility;
                //To set the scroll bar to left and at the top
                if (firstUse) st.View.SetScrollBarAtZero();
                firstUse = false;
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                Station.Module stationLineModule = e.ActiveStationModule as Station.Module;
                e.step.CheckValidityOf(stationLineModule.WorkingAverageStationLine._Parameters._State is Common.Station.State.WireSaved);
            };
            s.Changed += delegate (object source, StepEventArgs e)
            {
                //Stations.Offset.Module stationLineModule = e.ActiveStationModule as Stations.Offset.Module;
                //stationLineModule.View.Timer_Keep_Cell_Focus.Enabled = true;
                //stationLineModule.View.Timer_Keep_Cell_Focus.Start();
            };
            //Leaving() is triggered by Leave()
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                st = e.ActiveStationModule as Station.Module;
                st.View.dataGridViewMesures.CancelEdit();
                if (st.WorkingAverageStationLine._Parameters._State is Common.Station.State.WireReadyToBeSaved)
                {
                    string save = R.T_Export;
                    MessageInput mi = new MessageInput(MessageType.Choice, R.StringGuidedLine_WireNotSaved)
                    {
                        ButtonTexts = new List<string> { save, R.T_DONT }
                    };
                    string respond = mi.Show().TextOfButtonClicked;
                    if (respond == save)
                    {
                        st.ExportToGeode(false, true);
                    }
                    //e.ActiveStationModule.View.ShowMessageOfExclamation(R.StringGuidedLine_WireNotSaved, R.T_OK);
                }
            };
            return s;
        }
        /// <summary>
        /// Step de measure de fil pour l'alignement des elements qui met à jour le step alignment result dans l'admin module
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step MeasureWireForElementAlignment(Common.Guided.Module guidedModule)
        {
            Step s = MeasureWire(guidedModule);
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                Common.Guided.Group.Module GrGm = e.guidedModule.ParentModule as Common.Guided.Group.Module;
                // L'admin module est toujours le premier dans la liste des guided modules lors d'un alignement d'aimant
                // Le step 5 est step avec les résultats d'alignement qu'il faut mettre à jour
                if (GrGm.SubGuidedModules[0].currentIndex == 3)
                {
                    GrGm.SubGuidedModules[0].CurrentStep.Update();
                }
            };
            s.ControlChanged += delegate (object source, StepEventArgs e)
            {

            };
            return s;
        }
        /// <summary>
        /// Select the instrument and maximize the datagridview
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step ChooseEcartometer(Common.Guided.Module guidedModule)
        {
            Step s = Management.ChooseInstrument(guidedModule, I.InstrumentClasses.ECARTOMETRE);
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
            };
            s.Entering += delegate (object source, StepEventArgs e)
            {
            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
            };
            //Leaving() is triggered by Leave()
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                (e.guidedModule._ActiveStationModule as Station.Module).View.moduleType = ENUM.ModuleType.Guided;
                (e.guidedModule._ActiveStationModule as Station.Module).View.MaximizeDataGridView();
            };
            return s;
        }
        /// <summary>
        /// Create a empty new guided module or a new guided module with the same instrument, theoretical file, same operation, same team and go to select element step
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step CloneWire(Module guidedModule)
        {
            Station.Module st = guidedModule._ActiveStationModule as Station.Module;
            char splitChar = '_';
            string[] nameSplit = st.WorkingAverageStationLine._Name.Split(splitChar);
            Step s = new Step(guidedModule, R.StringGuidedLine_CloneWire);
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                Station.Module actualStationLineModule = e.ActiveStationModule as Station.Module;
                e.step.View.AddButton(
                    R.StringGuidedLine_NewGuidedModule,
                    R.Line1,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = e.guidedModule;//(args2.TsuObject as Guided.Module);
                        Common.Guided.Group.Module grGm = gm.ParentModule as Common.Guided.Group.Module;
                        grGm.AddNewEcartometryModule();
                        Common.Guided.Module newGm = grGm.ActiveSubGuidedModule;
                        Station.Module newStationLineModule = newGm._ActiveStationModule as Station.Module;
                        Station.Module oldStationLineModule = gm._ActiveStationModule as Station.Module;
                        newGm._ElementManager.AllElements = gm._ElementManager.AllElements;
                        newStationLineModule.ChangeOperation(oldStationLineModule._Station.ParametersBasic._Operation);
                        newStationLineModule.ChangeTeam(oldStationLineModule._Station.ParametersBasic._Team);
                        newStationLineModule.SetGeodeFilePathInNewStationLineModule(oldStationLineModule);
                        newStationLineModule.SetInstrumentInNewStationLineModule(oldStationLineModule);
                        newGm.MoveToStep(6);
                    }
                );
                e.step.View.AddButton(
                    R.T210,
                    R.Add,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Group.Module grGm = e.guidedModule.ParentModule as Common.Guided.Group.Module;
                        grGm.AddNewEcartometryModule();
                    }
                );
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                nameSplit = st.WorkingAverageStationLine._Name.Split(splitChar);
                foreach (var item in e.step.View.Controls)
                {
                    if (item is F.Label)
                    {
                        F.Label label = item as F.Label;
                        if (label.Name == "Title")
                        {
                            label.Text = string.Format("{0} {1} => {2}", R.StringGuidedLine_CloneWireName, nameSplit[3], nameSplit[4]);
                        }
                        if (label.Name == "Description")
                        {
                            label.Text = "";
                        }
                    }
                }
            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                e.step.Test();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
            };
            //Leaving() is triggered by Leave()
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                //guidedModule.ParentModule.Change();
            };
            return s;
        }
        /// <summary>
        /// STep to choose points of the wire, keep only the theoretical points
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step ChoosePoints(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.StringGuidedLine_SelectVariablePoints);
            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.ElementManager.MultiSelection = true;
                e.ElementManager.View.CheckBoxesVisible = true;
                e.ElementManager.SetSelectableToAllUniquePointForGuidedOffsetModule();
                //List<BigButton> buttonToHide = new List<BigButton>();
                //buttonToHide.Add(e.ElementManager.View.buttons.compute);
                //buttonToHide.Add(e.ElementManager.View.buttons.open);
                //buttonToHide.Add(e.ElementManager.View.buttons.sequence);
                //e.ElementManager.View.buttons.HideOnlyButtons(buttonToHide);
                //e.ElementManager.View.buttons.ShowAllButtons();
                //e.ElementManager.View.buttons.ReleaseAll();
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                e.step.View.AddControl(e.ElementManager);
                e.ElementManager.View.buttons.ShowTypes.Available = true;
                e.ElementManager.View.buttons.ShowPiliers.Available = true;
                e.ElementManager.View.buttons.ShowPoints.Available = true;
                e.ElementManager.View.buttons.ShowMagnets.Available = true;
                e.ElementManager.View.buttons.ShowAll.Available = true;
                e.ElementManager.View.buttons.ShowAlesages.Available = true;
                e.ElementManager.View.buttons.clean.Available = false;
                e.ElementManager.View.buttons.importRefenceFile.Available = true;
                e.ElementManager.View.buttons.add.Available = true;
                e.step.Test();
            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                e.step.Test();
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                e.step.View.RemoveControl(e.ElementManager);
            };
            return s;
        }
        /// <summary>
        /// Step to choose the theoretical file for wire with only showing theoretical points
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        static public Step ChooseTheoreticalFile(Common.Guided.Module guidedModule, bool skipStepsCompleted = false)
        {
            Step s = Management.ChooseTheoreticalFile(guidedModule);

            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };
            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.ElementManager.SetSelectableToAllTheoreticalPoints();
                e.ElementManager.View.buttons.HideAllButtons();
            };
            return s;
        }
        /// <summary>
        /// Step to choose the sequence file for wire
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        static public Step ChooseSequenceFile(Common.Guided.Module guidedModule, bool skipStepsCompleted = false)
        {
            Step s = Management.ChooseSequenceFile(guidedModule);

            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };
            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.ElementManager.SetSelectableToAllSequenceFil();
                e.ElementManager.View.buttons.HideAllButtons();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                e.step.CheckValidityOf(e.ElementManager.GetAllSequenceFil().Count > 0);
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                if (e.ElementManager.GetAllSequenceFil().Count == 0)
                {
                    guidedModule.Steps[6].Visible = false;
                }
                else
                {
                    guidedModule.Steps[6].Visible = true;
                }
            };
            return s;
        }
        /// <summary>
        /// Choose sequence fil from theorietcal file, it uses a modified "choose elements" step 
        /// </summary>
        /// <param name="gm"></param>
        /// <returns></returns>
        internal static Step ChooseSequenceFil(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.StringGuided_ChooseSequence);

            //s._Name = Compute.Conversions.SeparateTitleFromMessage(R.String_GuidedLine_ChooseSequenceFil);
            //s.utility = Compute.Conversions.SeparateMessageFromTitle(R.String_GuidedLine_ChooseSequenceFil);

            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };
            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.ElementManager.MultiSelection = false;
                e.ElementManager.View.CheckBoxesVisible = true;
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.SetSequenceInElementManager();
                e.step.View.AddControl(e.ElementManager);
                e.ElementManager.View.buttons.HideAllButtons();
                e.ElementManager.UpdateView();
                e.ElementManager.View.buttons.ShowTypes.Available = false;
                e.ElementManager.View.buttons.ShowPiliers.Available = false;
                e.ElementManager.View.buttons.ShowPoints.Available = false;
                e.ElementManager.View.buttons.ShowMagnets.Available = false;
                e.ElementManager.View.buttons.ShowAll.Available = false;
                e.ElementManager.View.buttons.ShowAlesages.Available = false;
                e.ElementManager.View.buttons.clean.Available = false;
                e.ElementManager.View.buttons.sequence.Available = true;
                e.ElementManager.View.buttons.add.Available = false;
            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                e.step.Test();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                if (e.ElementManager._currentSetSelectableListType != EL.Manager.Module.currentSetSelectableListType.setSelectableToAllSequenceFil)
                {
                    e.ElementManager.SetSelectableToAllSequenceFil();
                }
                e.step.CheckValidityOf(e.ElementManager._SelectedObjects.Count >= 1);
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                e.ElementManager.ValidateSelection();
                e.step.View.RemoveControl(e.ElementManager);
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.FinalModule.PointsToBeAligned.Clear();
                stationModule.FinalModule.CalaPoints.Clear();
                foreach (EL.Point item in stationModule._TheoPoint)
                {
                    if (item.LGCFixOption == ENUM.LgcPointOption.VXY) { stationModule.FinalModule.PointsToBeAligned.Add(item.DeepCopy()); }
                    if (item.LGCFixOption == ENUM.LgcPointOption.CALA) { stationModule.FinalModule.CalaPoints.Add(item.DeepCopy()); }
                }
            };
            return s;
        }
        /// <summary>
        /// STep to choose points of the wire and add them in the Station module, keep only the theoretical points
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step ChooseVXYPoints(Common.Guided.Module guidedModule)
        {
            Step s = ChoosePoints(guidedModule);
            s._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringGuidedLine_SelectVariablePoints);
            s.Utility = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringGuidedLine_SelectVariablePoints);
            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.View.moduleType = ENUM.ModuleType.Guided;
                stationModule.SetVXYPointsInElementManager();
                e.ElementManager.UpdateView();
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                CloneableList<EL.Point> pointsList = e.ElementManager.GetPointsInSelectedObjects();
                //Recupere tous les points de cala
                foreach (EL.Point item in pointsList)
                {
                    item.LGCFixOption = ENUM.LgcPointOption.VXY;
                }
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.FinalModule.PointsToBeAligned = pointsList.Clone();
                pointsList.AddRange(stationModule.FinalModule.CalaPoints);
                stationModule.SetPointsToMeasure(pointsList);
                //e.ElementManager.ValidateSelection();
            };
            return s;
        }
        /// <summary>
        /// STep to choose Cala points of the wire and add them in the Station module, keep only the theoretical points
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step ChooseCalaPoints(Common.Guided.Module guidedModule)
        {
            Step s = ChoosePoints(guidedModule);
            s._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringGuidedLine_SelectCalaPts);
            s.Utility = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringGuidedLine_SelectCalaPts);
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                if (stationModule._ComputeStrategy is Station.LGC2Strategy && stationModule.WorkingAverageStationLine._ListStationLine.Count == 0)
                {
                    new MessageInput(MessageType.GoodNews, R.StringGuidedLine_AutomaticSelectionOfWireExtremities).Show();
                }
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.View.moduleType = ENUM.ModuleType.Guided;
                stationModule.SetCalaPointsInElementManager();
                e.ElementManager.UpdateView();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                e.step.MessageFromTest = R.String_GM_MsgBlock2Points;
                e.step.CheckValidityOf(e.ElementManager.GetPointsInSelectedObjects().Count() >= 2);
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                CloneableList<EL.Point> pointsList = e.ElementManager.GetPointsInSelectedObjects();
                //Recupere tous les points de cala
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                foreach (EL.Point item in pointsList)
                {
                    item.LGCFixOption = ENUM.LgcPointOption.CALA;
                    int index = stationModule.FinalModule.PointsToBeAligned.FindIndex(x => x._Name == item._Name && x._Origin == item._Origin);
                    if (index != -1)
                    {
                        //Dans le cas ou le point sélectionné en référence fait déjà partie des points à aligner
                        stationModule.FinalModule.PointsToBeAligned.RemoveAt(index);
                        stationModule.FinalModule.CalaPoints.Add(item);
                        int index3 = stationModule.FinalModule.PointsToBeMeasured.FindIndex(x => x._Name == item._Name && x._Origin == item._Origin);
                        if (index3 != -1) { stationModule.FinalModule.PointsToBeMeasured[index3].LGCFixOption = ENUM.LgcPointOption.CALA; }
                        int index1 = stationModule._TheoPoint.FindIndex(x => x._Name == item._Name && x._Origin == item._Origin);
                        if (index1 != -1) { stationModule._TheoPoint[index1].LGCFixOption = ENUM.LgcPointOption.CALA; }
                        foreach (Station line in stationModule.WorkingAverageStationLine._ListStationLine)
                        {
                            int index2 = line._MeasureOffsets.FindIndex(x => x._PointName == item._Name && x._Point._Origin == item._Origin);
                            if (index2 != -1) { line._MeasureOffsets[index2]._Point.LGCFixOption = ENUM.LgcPointOption.CALA; }
                        }

                    }
                }
                //Si pas encore de ligne, il faut désigner les 2 ancrages qui doivent être des cala
                if (stationModule.WorkingAverageStationLine._ListStationLine.Count == 0)
                {
                    stationModule.SetPointsToMeasure(pointsList, false, false);
                }
                //stationModule.FinalModule.CalaPoints = pointsList.Clone();
                pointsList.AddRange(stationModule.FinalModule.PointsToBeAligned);
                stationModule.SetPointsToMeasure(pointsList);
            };
            return s;
        }
        /// <summary>
        /// Choisit les points de calage pour aligner les aimants en enlevant les aimants à aligner de la liste des points sélectionnables
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step ChooseCalaPointsForElementAlignment(Common.Guided.Module guidedModule)
        {
            Step s = ChoosePoints(guidedModule);
            s._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringGuidedLine_SelectCalaPts);
            s.Utility = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringGuidedLine_SelectCalaPts);
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                if (stationModule._ComputeStrategy is Station.LGC2Strategy && stationModule.WorkingAverageStationLine._ListStationLine.Count == 0)
                {
                    new MessageInput(MessageType.GoodNews, R.StringGuidedLine_AutomaticSelectionOfWireExtremities).Show();
                }
            };
            s.Entering += delegate (object source, StepEventArgs e)
            {
                var stationModule = e.ActiveStationModule as Station.Module;
                var calas = stationModule.FinalModule.CalaPoints;
                //calas.RemoveAt(3);
                //stationModule.SetCalaPointsInElementManager();
                if (stationModule.FinalModule.PointsToBeAligned != null && stationModule.ElementModule.SelectableObjects != null)
                {
                    foreach (EL.Point item in stationModule.FinalModule.PointsToBeAligned)
                    {
                        TsuObject objectFound = stationModule.ElementModule.SelectableObjects.Find(x => (x as EL.Point)._Name == item._Name);
                        if (objectFound != null)
                        {
                            stationModule.ElementModule.RemoveSelectableObjects(objectFound);
                        }
                    }
                }
                e.ElementManager._SelectedObjects.Clear(); 
                foreach (var item in calas)
                {
                    e.ElementManager.AddSelectedObjects(item);
                }
                e.ElementManager.UpdateView();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = false;
                e.step.CheckValidityOf(e.ElementManager.GetPointsInSelectedObjects().Count() >= 2);
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                CloneableList<EL.Point> pointsList = e.ElementManager.GetPointsInSelectedObjects();
                //Recupere tous les points de cala
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.FinalModule.CalaPoints.Clear();
                foreach (EL.Point item in pointsList)
                {
                    item.LGCFixOption = ENUM.LgcPointOption.CALA;
                    if(!stationModule.FinalModule.CalaPoints.Contains(item))
                        stationModule.FinalModule.CalaPoints.Add(item);
                    int index = stationModule.FinalModule.PointsToBeAligned.FindIndex(x => x._Name == item._Name && x._Origin == item._Origin);
                    if (index != -1)
                    {
                        //Dans le cas ou le point sélectionné en référence fait déjà partie des points à aligner
                        stationModule.FinalModule.PointsToBeAligned.RemoveAt(index);
                        if (!stationModule.FinalModule.CalaPoints.Contains(item))
                            stationModule.FinalModule.CalaPoints.Add(item);
                        int index3 = stationModule.FinalModule.PointsToBeMeasured.FindIndex(x => x._Name == item._Name && x._Origin == item._Origin);
                        if (index3 != -1) { stationModule.FinalModule.PointsToBeMeasured[index3].LGCFixOption = ENUM.LgcPointOption.CALA; }
                        int index1 = stationModule._TheoPoint.FindIndex(x => x._Name == item._Name && x._Origin == item._Origin);
                        if (index1 != -1) { stationModule._TheoPoint[index1].LGCFixOption = ENUM.LgcPointOption.CALA; }
                        foreach (Station line in stationModule.WorkingAverageStationLine._ListStationLine)
                        {
                            int index2 = line._MeasureOffsets.FindIndex(x => x._PointName == item._Name && x._Point._Origin == item._Origin);
                            if (index2 != -1) { line._MeasureOffsets[index2]._Point.LGCFixOption = ENUM.LgcPointOption.CALA; }
                        }
                    }
                }

                //Si pas encore de ligne, il faut désigner les 2 ancrages qui doivent être des cala
                if (stationModule.WorkingAverageStationLine._ListStationLine.Count == 0)
                {
                    stationModule.SetPointsToMeasure(pointsList, false, false);
                }
                //stationModule.FinalModule.CalaPoints = pointsList.Clone();
                pointsList.AddRange(stationModule.FinalModule.PointsToBeAligned);
                stationModule.SetPointsToMeasure(pointsList, false, false);
                foreach (EL.Point pt in stationModule.FinalModule.PointsToBeAligned)
                {
                    int index = stationModule.WorkingAverageStationLine._ListStationLine[0]._MeasureOffsets.FindIndex(x => x._PointName == pt._Name && x._Point._Origin == pt._Origin);
                    if (index != -1) stationModule.WorkingAverageStationLine._ListStationLine[0]._MeasureOffsets[index]._Point.LGCFixOption = ENUM.LgcPointOption.VXY;
                }
                stationModule.UpdateOffsets(false);
            };
            return s;
        }
        static public Step ChooseAllAdministrativeparameters(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.String_GM_AdministrativeParameters);
            if (Tsunami2.Preferences.Theme.Verbose)
            {
                string fichier = s.GuidedModule._ElementManager.filesOpened.ToString();
                LinkPanel p = new LinkPanel() { Font = new Drawing.Font("arial", 20), Dock = F.DockStyle.Top };
                p.Add($"Le $fichier théorique%1$ est {fichier}.", new List<Action>() { });
            }
            BigButton buttonSelectTheoElementFile = new BigButton();
            BigButton buttonSelectTheoSeqFile = new BigButton();
            BigButton buttonSelectEcarto = new BigButton();
            BigButton buttonSelectOperation = new BigButton();
            BigButton buttonCreateOperation = new BigButton();
            F.TextBox textBoxTeam = new F.TextBox();
            F.TextBox textBoxTemperature = new F.TextBox();
            Drawing.Font TitleFont = new Drawing.Font("Microsoft Sans Serif", 15.75F, Drawing.FontStyle.Regular, Drawing.GraphicsUnit.Point, 0);
            string instrumentname = "...";
            string operationName = "...";
            string team = "";
            string temperature = "";
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                // add a button to directly browse for theoretical file
                e.step.View.AddButton(
                    string.Format(R.String_GM_SelectTheoElementFile, "..."),
                    R.Open,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        gm._ElementManager.MultiSelection = false;
                        gm._ElementManager.View.CheckBoxesVisible = false;
                        gm._ElementManager.SetSelectableToAllTheoreticalPoints();
                        //gm._ElementManager.View.buttons.HideAllButtons();
                        gm._ElementManager.View.TryOpen();
                        s.Test();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectTheoElementFile = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectTheoElementFile.SetColors(Tsunami2.Preferences.Theme.Colors.Bad);
                // add a button to directly browse for sequence theoretical file
                e.step.View.AddButton(
                    string.Format(R.String_GM_SelectTheoSeqFile, "..."),
                    R.Open,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        e.ElementManager.SetSelectableToAllSequenceNiv();
                        gm._ElementManager.MultiSelection = false;
                        gm._ElementManager.View.CheckBoxesVisible = false;
                        gm._ElementManager.View.TrySelectSequenceFile();
                        s.Test();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectTheoSeqFile = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectTheoSeqFile.Available = false;
                buttonSelectTheoSeqFile.SetColors(Tsunami2.Preferences.Theme.Colors.Attention);
                // add a button to select instrument
                e.step.View.AddButton(
                    string.Format(R.String_GM_SelectInstrument, instrumentname),
                    R.Instrument,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        Station.Module st = gm._ActiveStationModule as Station.Module;
                        st.SelectionEcartometre();
                        s.Test();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectEcarto = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectEcarto.SetColors(Tsunami2.Preferences.Theme.Colors.Bad);
                // add a button to select operation
                e.step.View.AddButton(
                    string.Format(R.String_GM_SelectOperation, operationName),
                    R.Operation,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        Station.Module st = gm._ActiveStationModule as Station.Module;
                        if (st.View != null)
                        {
                            st.View.SelectOperation();
                            s.Test();
                        }
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectOperation = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Attention);
                // add a button to create operation
                e.step.View.AddButton(
                    string.Format(R.String_GM_CreateOperation, operationName),
                    R.Operation_Add,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        Station.Module st = gm._ActiveStationModule as Station.Module;
                        e.OperationManager.View.AddNew();
                        if (e.OperationManager.SelectedOperation != null)
                        {
                            st.ChangeOperation(e.OperationManager.SelectedOperation);
                        }
                        s.Test();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonCreateOperation = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonCreateOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Attention);
                /// ajout temperature ambiante
                e.step.View.AddTitle(R.String_GM__AmbientTemperature, "temperatureLabel");
                e.step.View.AddTextBox(guidedModule._TsuView, temperature);
                textBoxTemperature = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as F.TextBox;
                textBoxTemperature.BackColor = Tsunami2.Preferences.Theme.Colors.Attention;
                /// ajout nom de l'équipe
                e.step.View.AddTitle(R.String_GM_TeamLabel, "teamLabel");
                e.step.View.AddTextBox(guidedModule._TsuView, team);
                textBoxTeam = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as F.TextBox;
                textBoxTeam.BackColor = Tsunami2.Preferences.Theme.Colors.Bad;
                textBoxTeam.Enter += delegate
                {
                    textBoxTeam.SelectAll();
                };
                s.Test();
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                if (st.WorkingAverageStationLine._Parameters._Instrument != null)
                {
                    e.InstrumentManager._SelectedObjects.Clear();
                    instrumentname = st.WorkingAverageStationLine._Parameters._Instrument._Name;
                    I.Sensor instrument = e.InstrumentManager.AllElements.Find(x => x._Name == st.WorkingAverageStationLine._Parameters._Instrument._Name) as I.Sensor;
                    e.InstrumentManager.AddSelectedObjects(instrument);
                    e.InstrumentManager.View.currentStrategy.ShowSelectionInButton();
                    e.InstrumentManager._SelectedObjectInBlue = instrument;
                    e.InstrumentManager.UpdateView();
                }
                O.Operation operation = e.OperationManager.AllElements.Find(x => x._Name == st.WorkingAverageStationLine.ParametersBasic._Operation._Name) as O.Operation;
                e.OperationManager._SelectedObjects.Clear();
                e.OperationManager.AddSelectedObjects(operation);
                e.OperationManager.View.currentStrategy.ShowSelectionInButton();
                e.OperationManager._SelectedObjectInBlue = operation;
                e.OperationManager.UpdateView();
                temperature = st.WorkingAverageStationLine._Parameters._Temperature.ToString("F1", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                textBoxTemperature.Text = temperature;
                if (e.ActiveStationModule._Station.ParametersBasic._Team != R.String_UnknownTeam)
                {
                    team = e.ActiveStationModule._Station.ParametersBasic._Team;
                    textBoxTeam.Text = team;
                }
                else
                {
                    textBoxTeam.Text = "";
                }
                operationName = st.WorkingAverageStationLine.ParametersBasic._Operation.ToString();
                s.Test();
                textBoxTeam.Focus();
                textBoxTeam.SelectAll();
                textBoxTeam.Focus();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = false;
                bool stepValid = true;
                string msgBlocked = R.String_GM_MsgBlockAdministrative;
                Station.Module st = e.ActiveStationModule as Station.Module;
                ///Vérification si fichier theo sélectionné
                if (e.ElementManager.GetAllTheoreticalPoints().Count > 0)
                {
                    buttonSelectTheoElementFile.SetColors(Tsunami2.Preferences.Theme.Colors.Good);
                    buttonSelectTheoElementFile.ChangeImage(R.StatusGood);
                    buttonSelectTheoSeqFile.Available = true;
                    buttonSelectTheoElementFile.ChangeNameAndDescription(string.Format(R.String_GM_SelectTheoElementFile, (e.ElementManager.AllElements[0] as EL.Element)._Origin));
                }
                else
                {
                    msgBlocked += " " + R.String_GM_MsgBlockTheoFile;
                    stepValid = false;
                }
                ///Vérification si fichier séquence sélectionné (pas obligatoire)
                if (e.ElementManager.GetAllSequenceFil().Count > 0)
                {
                    buttonSelectTheoSeqFile.SetColors(Tsunami2.Preferences.Theme.Colors.Good);
                    buttonSelectTheoSeqFile.ChangeImage(R.StatusGood);
                    buttonSelectTheoSeqFile.ChangeNameAndDescription(string.Format(R.String_GM_SelectTheoSeqFile, e.ElementManager.GetAllSequenceFil()[0]._Origin));
                }
                ///Vérification si instrument sélectionné
                if (e.InstrumentManager._SelectedObjects.Count == 1)
                {
                    I.Instrument instrument = e.InstrumentManager._SelectedObjects[0] as I.Instrument;
                    if (instrument._Model != null)
                    {
                        buttonSelectEcarto.SetColors(Tsunami2.Preferences.Theme.Colors.Good);
                        buttonSelectEcarto.ChangeImage(R.StatusGood);
                        if (st.WorkingAverageStationLine._Parameters._Instrument != null) instrumentname = st.WorkingAverageStationLine._Parameters._Instrument._Name;
                        buttonSelectEcarto.ChangeNameAndDescription(string.Format(R.String_GM_SelectInstrument, instrumentname));
                    }
                    else
                    {
                        if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                        msgBlocked += " " + R.String_GM_MsgBlockInstrument;
                        stepValid = false;
                    }
                }
                else
                {
                    if (st.WorkingAverageStationLine._Parameters._Instrument == null)
                    {
                        if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                        msgBlocked += " " + R.String_GM_MsgBlockInstrument;
                        stepValid = false;
                    }
                    else
                    {
                        if (st.WorkingAverageStationLine._Parameters._Instrument._Name == R.String_Unknown)
                        {
                            if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                            msgBlocked += " " + R.String_GM_MsgBlockInstrument;
                            stepValid = false;
                        }
                    }
                }
                //Vérification si opération sélectionnée, si 0 met bouton en red
                if (e.OperationManager.SelectedOperation.IsSet) //&& (e.OperationManager.SelectedOperation.value != 0))
                {
                    if (st.WorkingAverageStationLine.ParametersBasic._Operation != null)
                    {
                        buttonSelectOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Good);
                        buttonSelectOperation.ChangeImage(R.StatusGood);
                        operationName = st.WorkingAverageStationLine.ParametersBasic._Operation.ToString();
                        buttonSelectOperation.ChangeNameAndDescription(string.Format(R.String_GM_SelectOperation, operationName));
                        buttonCreateOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Good);
                        buttonCreateOperation.ChangeImage(R.StatusGood);
                        buttonCreateOperation.ChangeNameAndDescription(string.Format(R.String_GM_CreateOperation, operationName));
                    }
                }
                else
                {
                    if (st.WorkingAverageStationLine.ParametersBasic._Operation != null)
                    {
                        if (!st.WorkingAverageStationLine.ParametersBasic._Operation.IsSet)// || st._AverageStationLine.ParametersBasic._Operation.value == 0)
                        {
                            buttonSelectOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Attention);
                            buttonSelectOperation.ChangeImage(R.Operation);
                            operationName = st.WorkingAverageStationLine.ParametersBasic._Operation.ToString();
                            buttonSelectOperation.ChangeNameAndDescription(string.Format(R.String_GM_SelectOperation , operationName));
                            buttonCreateOperation.SetColors(Tsunami2.Preferences.Theme.Colors.Attention);
                            buttonCreateOperation.ChangeImage(R.Operation_Add);
                            buttonCreateOperation.ChangeNameAndDescription(string.Format(R.String_GM_CreateOperation, operationName));
                        }
                    }
                }
                //Vérifie que la temperature contient bien un nombre double
                double newTemperature = T.Conversions.Numbers.ToDouble(textBoxTemperature.Text, true, -9999);
                if (newTemperature != -9999)
                {
                    textBoxTemperature.BackColor = Tsunami2.Preferences.Theme.Colors.Good;
                }
                else
                {
                    stepValid = false;
                    if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                    msgBlocked += " " + R.String_GM_MsgBlockTemperature;
                    textBoxTemperature.BackColor = Tsunami2.Preferences.Theme.Colors.Bad;
                }
                //Vérifie que le nom d'équipe contient que des caractères alphanumériques et est de maximum 8 caractères
                string tempTeam = textBoxTeam.Text;
                if (System.Text.RegularExpressions.Regex.IsMatch(tempTeam, @"^[a-zA-Z]+$") && (tempTeam.Length < 9 || tempTeam == R.String_UnknownTeam))
                {
                    tempTeam = tempTeam.ToUpper();
                    e.ActiveStationModule._Station.ParametersBasic._Team = tempTeam;
                    team = e.ActiveStationModule._Station.ParametersBasic._Team;
                    textBoxTeam.BackColor = Tsunami2.Preferences.Theme.Colors.Good;
                }
                else
                {
                    stepValid = false;
                    if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                    msgBlocked += " " + R.String_GM_MsgBlockTeam;
                    textBoxTeam.BackColor = Tsunami2.Preferences.Theme.Colors.Bad;
                }
                e.guidedModule.CurrentStep.MessageFromTest = msgBlocked;
                e.step.CheckValidityOf(stepValid);
                e.step.Update();
            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                if (st.WorkingAverageStationLine._Parameters._Instrument != null)
                {
                    if (instrumentname != st.WorkingAverageStationLine._Parameters._Instrument._Name) st.View.MaximizeDataGridView();
                }
                ///Cache le step select sequence niv si pas de file sequence niv
                if (e.ElementManager.GetAllSequenceFil().Count == 0)
                {
                    guidedModule.Steps[2].Visible = false;
                }
                else
                {
                    guidedModule.Steps[2].Visible = true;
                }
                double newTemperature = T.Conversions.Numbers.ToDouble(textBoxTemperature.Text, true, -9999);
                if (newTemperature != -9999)
                {
                    st.ChangeTemperature(newTemperature);
                }
            };
            return s;
        }
    }
}
