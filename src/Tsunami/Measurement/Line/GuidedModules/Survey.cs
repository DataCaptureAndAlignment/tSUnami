﻿using System.Collections.Generic;
using TSU.Common.Guided;
using TSU.Common.Guided.Steps;
using LineGuidedSteps = TSU.Line.GuidedModules.Steps;
using M = TSU;
using R = TSU.Properties.Resources;

namespace TSU.Line.GuidedModules
{
    static public class GuidedEcartometry
    {
        static public Common.Guided.Module Get(M.Module parentModule, int wireNumber)
        {
            //Guided.Group.Module GrGm = new Guided.Group.Module(parentModule, R.String_GuidedLine_GroupWireName);
            Common.Guided.Group.Module GrGm = parentModule as Common.Guided.Group.Module;
            GrGm.Type = ENUM.GuidedModuleType.Ecartometry;

            Common.Guided.Module gm = new Common.Guided.Module(parentModule, $"{R.StringGuidedLine_WireName} {wireNumber};A=>B");

            Decore.ShowRaisedEvent(gm);

            gm.guideModuleType = ENUM.GuidedModuleType.Ecartometry;
            gm.ObservationType = Common.ObservationType.Ecarto;

            // Invisible step: : Add needed station modules
            gm.SwitchToANewStationModule(new Station.Module(gm));
            //Stations.Offset.Module st = gm._ActiveStationModule as Stations.Offset.Module;

            BuildSteps(gm);

            gm.Start();

            return gm;
        }

        private static void BuildSteps(Common.Guided.Module gm)
        {
            // Step 0: Presentation
            gm.Add(Management.Declaration(gm, R.StringGuidedLine_EcartometrySurveyPresentation));
            //*******************************************************************
            ////// Step 1: Choose Element Theoretical File
            //gm.Add(Step.OffsetMeter.ChooseTheoreticalFile(gm));

            ////// Step 2: Choose sequence Theoretical File
            //gm.Add(Step.OffsetMeter.ChooseSequenceFile(gm));

            ////// Step 3: Choose Management.Instrument
            //gm.Add(Step.OffsetMeter.ChooseEcartometer(gm));

            ////// Step 4: Choose Team
            //gm.Add(Step.Management.EnterATeam(gm));

            ///// Step 5 : Choose Operation
            //gm.Add(Step.Management.ChooseOperation(gm));
            //*************************************************************************
            /// Step 1 : Choose all administrative parameters
            gm.Add(LineGuidedSteps.Measurement.ChooseAllAdministrativeparameters(gm));
            /// Step 2 : Choose Sequence
            gm.Add(LineGuidedSteps.Measurement.ChooseSequenceFil(gm));

            /// Step 3 : Choose CALA points
            gm.Add(LineGuidedSteps.Measurement.ChooseCalaPoints(gm));

            /// Step 4 : Choose points
            gm.Add(LineGuidedSteps.Measurement.ChooseVXYPoints(gm));

            /// Step 5 : Measure Wire
            gm.Add(LineGuidedSteps.Measurement.MeasureWire(gm));

            /// Step 6 : Clone Wire
            //gm.Add(Step.OffsetMeter.CloneWire(gm));

            /// Step 6 : Show dat saved file
            gm.Add(Management.ShowDatFiles(gm));

            ///// Step 12 : Show LGC Files
            //gm.Add(Step.Management.ShowLGCFiles(gm));
        }

        internal static void ReCreateWhatIsNotSerialized(Common.Guided.Module gm)
        {
            gm.Steps = new List<Step>();
            //gm.currentIndex = 0;
            BuildSteps(gm);

            //gm.Start();
            //gm.MoveToStep(gm.currentIndex);
        }
    }
}
