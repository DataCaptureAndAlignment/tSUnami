﻿using System;
using System.Collections.Generic;
using System.IO;
using TSU.Common.Measures;
using TSU.ENUM;
using static TSU.Line.Station;
using M = TSU.Common.Measures;

namespace TSU.Line
{
    internal static class SaveToGeode
    {
        internal static double na => Tsunami2.Preferences.Values.na;

        internal static Result SaveFilToGeode(Station.Module stationLineModule, bool showDatFile = false, bool insertOffset = true, string filepath = null)
        {
            //To be implemented when LGC2 is integrated in Apex. Pour l'instant, ne met que le premier fil avec ses extrémités dans le fichier à insérer
            Average averageStationLine = stationLineModule.WorkingAverageStationLine;
            List<string> textToWriteInFile = new List<string>();
            int lineNumber = 1;
            string lineText = "";
            Result saveSuccess = new Result();
            //Crèe une liste avec tous les ancrages de tous les fils pour filtrage des mesures lors écriture des fils. 
            List<string> listAnchors = new List<string>();
            if (filepath == null)
            {
                filepath = Tsunami2.Preferences.Values.Paths.MeasureOfTheDay + IO.SUSoft.Geode.GetFileName(averageStationLine.ParametersBasic, "OFFSET");
                //Crèe le répertoire Mesures si nécessaire
                if (!Directory.Exists(Tsunami2.Preferences.Values.Paths.MeasureOfTheDay))
                    Directory.CreateDirectory(Tsunami2.Preferences.Values.Paths.MeasureOfTheDay);
            }
            List<int> lineNumbersAlreadyRecorded = new List<int>();
            // DEP: string filepath = String.Format("{0}{1}_{2}_{3}_{4}_{5}.dat",TSU.Tsunami2.TsunamiPreferences.Values.Paths.MeasureOfTheDay, averageStationLine.ParametersBasic._Operation.value, averageStationLine.ParametersBasic._Team, DateTime.Now.Day.ToString("00"), DateTime.Now.Month.ToString("00"), (DateTime.Now.Year - 2000).ToString());
            //lecture du fichier existant s'il existe
            try
            {
                string[] readedTextInFile;
                readedTextInFile = File.ReadAllLines(filepath);
                ///Ajoute la ligne RE en entête si inexistante
                if (readedTextInFile.Length > 0)
                {
                    if (!readedTextInFile[0].Contains(";RE;"))
                    {
                        AddLineRE(averageStationLine, textToWriteInFile, ref lineNumber);
                    }
                }
                else
                {
                    AddLineRE(averageStationLine, textToWriteInFile, ref lineNumber);
                }
                foreach (string line in readedTextInFile)
                {
                    if (line != "")
                    {
                        textToWriteInFile.Add(line);
                        //int numberOfLine;
                        ////incrémentation du numéro de ligne
                        //if (int.TryParse(line.Substring(0, 3), out numberOfLine))
                        //{
                        //    lineNumber = numberOfLine + 1;
                        //}
                        if (line.IndexOf(stationLineModule.WorkingAverageStationLine._Name) != -1)
                        {
                            lineNumbersAlreadyRecorded.Add(lineNumber);
                        }
                        lineNumber++;
                    }
                }
            }
            catch (Exception e)
            //Exception générée si fichier inexistant
            {
                AddLineRE(averageStationLine, textToWriteInFile, ref lineNumber);
                e.Data.Clear();
            }
            finally
            {
                //Ecrit pour le premier fil les offsets dans le fichier
                listAnchors.Add(averageStationLine._ListStationLine[0]._Anchor1._PointName);
                listAnchors.Add(averageStationLine._ListStationLine[0]._Anchor2._PointName);

                //Ecriture entête station
                AddLineRefStation(stationLineModule, textToWriteInFile, ref lineNumber, lineNumbersAlreadyRecorded);
                //Ecriture ligne entete PV avec temperature
                lineText = string.Format("  {0}PV;;;;;;;;;{1, -7};;;;;;", WriteLineNumber(lineNumber), Math.Round(averageStationLine._Parameters._Temperature, 1).ToString("F1", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                textToWriteInFile.Add(lineText);
                lineNumber++;
                //Ecriture extremité 1
                MeasureOfOffset ext1 = averageStationLine._ListStationLine[0]._MeasureOffsets.Find(x => x._PointName == averageStationLine._ListStationLine[0]._Anchor1._PointName);
                WriteMeasOffset(averageStationLine, textToWriteInFile, ref lineNumber, ext1);
                foreach (MeasureOfOffset offset in averageStationLine._ListStationLine[0]._MeasureOffsets)
                {
                    //Ecriture mesures du fil sans les ancrages
                    if (!listAnchors.Exists(x => x == offset._PointName) && offset._RawReading != na)
                    {
                        WriteMeasOffset(averageStationLine, textToWriteInFile, ref lineNumber, offset);
                    }
                }
                //Ecriture extremité 2
                MeasureOfOffset ext2 = averageStationLine._ListStationLine[0]._MeasureOffsets.Find(x => x._PointName == averageStationLine._ListStationLine[0]._Anchor2._PointName);
                WriteMeasOffset(averageStationLine, textToWriteInFile, ref lineNumber, ext2);
                if (insertOffset)
                {
                    //Ecriture des écarts du fil
                    foreach (MeasureOfOffset offset in averageStationLine._ListStationLine[0]._MeasureOffsets)
                    {
                        if (offset._TheoreticalDeviation != na
                         && stationLineModule.CheckIfTheoPointHasCoordinate(offset._Point)
                         && offset._Point.LGCFixOption != LgcPointOption.CALA)
                        {
                            ///Met un % devant les mesures bad
                            if (offset._Status is M.States.Bad)
                            {
                                lineText = "% ";
                            }
                            else
                            {
                                lineText = "  ";
                            }
                            lineText += string.Format("{0}EC;", WriteLineNumber(lineNumber));
                            lineNumber++;
                            //cas d'un alésage
                            if (offset._Point._Point != "")
                            {
                                lineText += string.Format("{0,-10};", offset._Point._Accelerator);
                                lineText += string.Format("{0,-33};", offset._Point._ClassAndNumero + "." + offset._Point._Point);
                            }
                            //cas d'un pilier
                            else
                            {
                                lineText += string.Format("{0,-10};", offset._Point._Zone);
                                lineText += string.Format("{0,-33};", offset._Point._ClassAndNumero + ".");
                            }
                            lineText += string.Format("{0,10};          ;          ;          ;dr dl dh dRoll [mm]|[mrad];", Math.Round(offset._TheoreticalDeviation * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                            textToWriteInFile.Add(lineText);
                        }
                    }
                }
                File.WriteAllLines(filepath, textToWriteInFile.ToArray());
                if (File.Exists(filepath))
                {
                    averageStationLine.ParametersBasic._State = new Common.Station.State.WireSaved();
                    averageStationLine.ParametersBasic._GeodeDatFilePath = filepath;
                    averageStationLine.ParametersBasic.LastSaved = DateTime.Now;
                    saveSuccess.Success = true;
                    //No more the saved file automatically after saving
                    if (showDatFile)
                    {
                        Shell.ExecutePathInDialogView(filepath);
                    }
                }
                else
                {
                    saveSuccess.Success = false;
                }
            }
            return saveSuccess;
        }

        private static string AddLineRE(Average averageStationLine, List<string> textToWriteInFile, ref int lineNumber)
        {
            //Ligne entête date + team + Opération seulement si fichier n'existe pas encore
            string lineText = string.Format("  {0}RE;", WriteLineNumber(lineNumber));
            lineNumber++;
            lineText += WriteDate(averageStationLine.ParametersBasic._Date);
            lineText += string.Format("{0,-10};", averageStationLine.ParametersBasic._Team);
            lineText += string.Format("{0,6};", averageStationLine.ParametersBasic._Operation.value.ToString());
            textToWriteInFile.Add(lineText);
            return lineText;
        }

        /// <summary>
        /// Ecrit la ligne d'information début de station pour vérifier si station en double
        /// </summary>
        /// <param name="lineNumber"></param>
        /// <returns></returns>
        private static void AddLineRefStation(Station.Module stationLineModule, List<string> textToWriteInFile, ref int lineNumber, List<int> lineNumberAlreadyRecorded)
        {
            string lineText = "";
            lineText = string.Format("! {0}----------", WriteLineNumber(lineNumber));
            lineText = lineText.Replace(';', ' ');
            lineNumber++;
            lineText += string.Format("{0} Wire recorded at ", stationLineModule.WorkingAverageStationLine._Name);
            lineText += string.Format("{0,-8}", DateTime.Now.ToString("T", System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR")));
            if (lineNumberAlreadyRecorded.Count != 0)
            {
                int i = 1;
                foreach (int item in lineNumberAlreadyRecorded)
                {
                    if (i == 1) { lineText += string.Format(" ALREADY RECORDED AT LINE {0}", item); }
                    else { lineText += string.Format(", {0}", item); }
                    i++;
                }
            }
            lineText += "----------";
            textToWriteInFile.Add(lineText);
        }

        private static string WriteDate(DateTime date)
        //Ecrit la date dans l'entête pour le fichier txt pour Geode A corriger quand LGC2 est implémenté dans Geode
        {
            //jour
            string dateText = date.Day.ToString() + "-";
            switch (date.Month)
            {
                //Choix du mois
                case 1:
                    dateText = dateText + "Jan-";
                    break;
                case 2:
                    dateText = dateText + "Feb-";
                    break;
                case 3:
                    dateText = dateText + "Mar-";
                    break;
                case 4:
                    dateText = dateText + "Apr-";
                    break;
                case 5:
                    dateText = dateText + "May-";
                    break;
                case 6:
                    dateText = dateText + "Jun-";
                    break;
                case 7:
                    dateText = dateText + "Jul-";
                    break;
                case 8:
                    dateText = dateText + "Aug-";
                    break;
                case 9:
                    dateText = dateText + "Sep-";
                    break;
                case 10:
                    dateText = dateText + "Oct-";
                    break;
                case 11:
                    dateText = dateText + "Nov-";
                    break;
                case 12:
                    dateText = dateText + "Dec-";
                    break;
                default:
                    dateText = dateText + "Jan-";
                    break;
            }
            //année
            return dateText += date.Year + ";";
        }

        private static string WriteLineNumber(int lineNumber)
        //écrit le numéro de ligne dans le fichier txt pour Geode, A corriger quand LGC2 est implémenté dans Geode
        {
            string lineText = "";
            for (int i = 0; i < 3 - lineNumber.ToString().Length; i++)
            {
                lineText = lineText + "0";
            }
            return lineText + lineNumber.ToString() + ";";
        }

        /// <summary>
        /// Ecrit la ligne de mesure offset
        /// </summary>
        /// <param name="averageStationLine"></param>
        /// <param name="textToWriteInFile"></param>
        /// <param name="lineNumber"></param>
        /// <param name="offsetMeasure"></param>
        /// <returns></returns>
        private static void WriteMeasOffset(Average averageStationLine, List<string> textToWriteInFile, ref int lineNumber, MeasureOfOffset offsetMeasure)
        {
            //On insère toutes les mesures même celle en bad
            string lineText;
            if (offsetMeasure == null)
                throw new Exception("Trying to save a wire measurement null");
            ///Met un % devant les mesures bad
            if (offsetMeasure._Status is M.States.Bad)
            {
                lineText = "% ";
            }
            else
            {
                lineText = "  ";
            }
            lineText += string.Format("{0}OF;", WriteLineNumber(lineNumber));
            lineNumber++;
            lineText += string.Format("{0,-9};", averageStationLine.ParametersBasic._Instrument._Model);
            lineText += string.Format("{0,-12};", averageStationLine.ParametersBasic._Instrument._SerialNumber);
            if (offsetMeasure._Point._Point != "")
            //cas d'un alésage
            {
                lineText += string.Format("{0,-10};", offsetMeasure._Point._Accelerator);
                lineText += string.Format("{0,-33};", offsetMeasure._Point._ClassAndNumero + "." + offsetMeasure._Point._Point);
            }
            else
            //cas d'un pilier
            {
                lineText += string.Format("{0,-10};", offsetMeasure._Point._Zone);
                lineText += string.Format("{0,-33};", offsetMeasure._Point._ClassAndNumero + ".");
            }
            lineText += string.Format("{0,8};;;;;;;;;;;", Math.Round(offsetMeasure._RawReading, 5).ToString("F5", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
            if (offsetMeasure._Point.LGCFixOption == LgcPointOption.CALA) lineText += "R;;";
            else lineText += "A;;";
            lineText += string.Format("{0,-8};", offsetMeasure._Date.ToString("T", System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR")));

            string comment = offsetMeasure.Comment;
            if (offsetMeasure.SubReadings != null && offsetMeasure.SubReadings.Count > 0)
            {
                if (offsetMeasure.Comment.Length > 0)
                    comment += " ";

                comment += $"sigma={offsetMeasure._StandardDeviation},";
                bool first = true;
                foreach (MeasureOfOffset.SubReading sr in offsetMeasure.SubReadings)
                {
                    if (first)
                    {
                        comment += "{";
                        first = false;
                    }
                    else
                    {
                        comment += ",";
                    }
                    comment += $"(val={sr.Reading},use={sr.Use})";
                }

                comment += "}";
            }
            lineText += string.Format("{0,-1};", comment);

            // interfaces
            var interfaces = offsetMeasure.Interfaces;
            lineText += interfaces.ToString(1, ';');
            lineText += interfaces.ToString(2, ';');
            lineText += interfaces.ToString(3, ';');

            textToWriteInFile.Add(lineText);
        }
    }
}