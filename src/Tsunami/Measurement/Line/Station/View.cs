﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using TSU.Common;
using TSU.Common.Compute.CsvImporter;
using TSU.Common.Compute.Rabot;
using TSU.Common.Instruments;
using TSU.Common.Measures;
using TSU.ENUM;
using TSU.Views;
using TSU.Views.Message;
using BOC = TSU.Common.Compute.Compensations.BeamOffsets;
using D = TSU.Common.Instruments.Device;
using E = TSU.Common.Elements;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Line
{
    public partial class Station
    {
        public partial class View : Common.Station.View
        {
            #region Field et Prop

            public new Module Module
            {
                get
                {
                    return this._Module as Module;
                }
            }

            internal Module stationLineModule;
            private double na = Tsunami2.Preferences.Values.na;
            // boutons du menu contexte
            internal BigButton bigButton_Elements;
            internal BigButton bigButton_selectFil;
            internal BigButton bigButton_selectSequenceFil;
            internal BigButton bigButton_importCSV;
            internal BigButton bigButton_save;
            internal BigButton bigButton_Operation;
            internal BigButton bigbuttonTop;
            internal BigButton bigButton_Cancel;
            internal BigButton bigButton_Sort;
            internal BigButton bigButton_CreateNewPoint;
            internal BigButton bigButton_ShowDatFile;
            internal BigButton bigButton_Help;
            internal TsuNode node_Parameters;
            //internal TsuNode node_CalculationOption;
            internal TsuNode node_Admin;
            internal TsuNode node_Computation;
            int rowActiveCell = 1;
            int columnActiveCell = 2;
            bool checkCurrentCell = true;
            //internal bool showLGC2Output { get; set; }
            /// <summary>
            /// Show the node coord type in the Admin Node
            /// </summary>
            internal bool showCoordType = true;
            internal OffsetStrategy computeStrategy = new LGC2Strategy();
            /// <summary>
            /// Affiche ou cache le noeud admin dans le treeview
            /// </summary>
            internal bool showAdmin = true;
            /// <summary>
            /// Propriété qui défini si l'on peut voir le noeud du choix de la méthode de calcul
            /// </summary>
            internal bool showCalculationOption = true;
            /// <summary>
            /// Propriété qui défini si l'on peut voir le noeud du choix de la méthode de calcul
            /// </summary>
            internal bool showComputation = true;
            /// <summary>
            /// Propriété qui défini si c'est un module guidé ou avancé
            /// </summary>
            /// 
            internal ModuleType moduleType = ModuleType.Advanced;
            private string zone = "";
            private string classe = "PT";
            private string number = string.Format("{0}{1}{2}{3}", (DateTime.Now.Year - 2000), DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00"), "1");
            private string pointNewPoint = "";

            private bool stopCheckingContextMenuOrder = false;

            //internal System.Windows.Forms.Timer Timer_Keep_Cell_Focus;

            #endregion
            #region constructeur et view
            public View(Module parentModule)
                : base(parentModule, Orientation.Vertical)
            //affichage du form lors de son lancement
            {
                InitializeComponent();
                this.ApplyThemeColors();
                this.stationLineModule = parentModule;
                this.computeStrategy = this.stationLineModule.WorkingAverageStationLine._strategy;
                //showLGC2Output = false;
                InitializeMenu();
                this.AddWaitingView();
                this.ShowDockedFill();
                //this.Timer_Keep_Cell_Focus = new System.Windows.Forms.Timer(this.components);
                //this.Timer_Keep_Cell_Focus.Interval = 100;
                //this.Timer_Keep_Cell_Focus.Enabled = false;
                //this.Timer_Keep_Cell_Focus.Tick += new System.EventHandler(this.Timer_Keep_Cell_Focus_Tick);
            }
            private void ApplyThemeColors()
            {
                this._PanelBottom.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this._PanelTop.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.splitContainer1.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.splitContainer1.Panel1.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.splitContainer1.Panel2.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.splitContainerFil.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.splitContainerFil.Panel1.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.splitContainerFil.Panel2.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.treeView_Parameters.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.dataGridViewMesures.ColumnHeadersDefaultCellStyle.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                this.dataGridViewMesures.ColumnHeadersDefaultCellStyle.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
                this.dataGridViewMesures.ColumnHeadersDefaultCellStyle.SelectionBackColor = Tsunami2.Preferences.Theme.Colors.TextHighLightBack;
                this.dataGridViewMesures.ColumnHeadersDefaultCellStyle.SelectionForeColor = Tsunami2.Preferences.Theme.Colors.TextHighLightFore;
                this.dataGridViewMesures.GridColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.dataGridViewMesures.DefaultCellStyle.BackColor = Tsunami2.Preferences.Theme.Colors.LightBackground;
                this.dataGridViewMesures.DefaultCellStyle.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
                this.dataGridViewMesures.DefaultCellStyle.SelectionBackColor = Tsunami2.Preferences.Theme.Colors.TextHighLightBack;
                this.dataGridViewMesures.DefaultCellStyle.SelectionForeColor = Tsunami2.Preferences.Theme.Colors.TextHighLightFore;
                this.Nom.HeaderText = R.StringLine_DataGrid_Name;
                this.Use.HeaderText = R.StringLine_DataGrid_Use;
                this.RawMeas.HeaderText = R.StringLine_DataGrid_RawMeas;
                this.CorrectedMeas.HeaderText = R.StringLine_DataGrid_CorrMeas;
                this.Theorique.HeaderText = R.StringLine_DataGrid_AvgTheo;
                this.moveAverage.HeaderText = "Offset (mm)";
                this.Objective.HeaderText = "Objective (mm)";
                this.Move.HeaderText = R.StringLine_DataGrid_MoveAvg;
                this.DeleteRow.HeaderText = R.StringLine_DataGrid_DelRow;
                this.Dcum.HeaderText = R.StringLine_DataGrid_Dcum;
                this.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                this.treeView_Parameters.BackColor = Tsunami2.Preferences.Theme.Colors.LightBackground;
                this.treeView_Parameters.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            }
            // the symbol of instrument inthe instrument view waiting for the real instrument
            private void AddWaitingView()
            {
                TsuView v = new TsuView();
                v.BackgroundImage = R.Instrument;
                v.BackgroundImageLayout = ImageLayout.Zoom;
                v.ShowDockedFill();
                splitContainer1.Panel2.Controls.Add(v);
            }
            public override void UpdateView()
            {
                base.UpdateView();
                //condition ajoutée car dans le module guidé fait une erreur si utilisateur ne choisit pas d'instrument et fait un previous step au step instrument
                if (this.stationLineModule.ecartoInstrument != null)
                {
                    this._PanelTop.Controls.Clear();
                    this.tableLayoutPanel_Top.Controls.Clear();
                    bigButton_Help = new BigButton(
                    R.String_NoText,
                    R.MessageTsu_Interrogation,
                    button_Help_Click);
                    bigButton_Help.Width = bigButton_Help.BB_Picture.Width;
                    bigButton_Help.Height = bigButton_Help.BB_Picture.Height;
                    bigButton_Help.Dock = DockStyle.Left;
                    tableLayoutPanel_Top.Dock = DockStyle.Fill;
                    switch (moduleType)
                    {
                        case ModuleType.Guided:
                            List<Control> existingButtons = new List<Control>();

                            bigbuttonTop = new BigButton(string.Format(R.StringLine_TopButton_Guided,
                                this.stationLineModule.Utility,
                                this.stationLineModule.ecartoInstrument._Name,
                                Math.Round(this.stationLineModule.WorkingAverageStationLine._Parameters._Tolerance * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"))),
                                R.Save, this.buttonSave_Click);

                            BigButton BOCB = BOC.WorkFlow.Buttons.GetComputeButton(action: () =>
                            {
                                var fm = this.Module.FinalModule;
                                var ids = BOC.WorkFlow.DetermineAssemblyIds(fm.MagnetsToBeAligned);
                                foreach (var id in ids)
                                {
                                    BOC.Results r = BOC.WorkFlow.Compute(fm, Tsunami2.Properties.MeasurementModules.GetByObservationType(ObservationType.Ecarto, addRollModules: true), id, ObservationType.Ecarto);
                                    if (r.OK)
                                    {
                                        BOC.WorkFlow.OffsetComputedInAllSystems.WaitOne(10000);
                                        string titleAndMessage = r.ToString("4ECARTO");
                                        new MessageInput(MessageType.FYI, titleAndMessage).Show();
                                    }
                                }
                            });

                            existingButtons.Add(bigbuttonTop);
                            existingButtons.Add(BOCB);
                            bigButton_Help.AutoSize = true;
                            bigButton_Help.Width = 200;
                            existingButtons.Add(bigButton_Help);


                            this.tableLayoutPanel_Top = CreateLayoutForButtons(existingButtons, DockStyle.Top, new List<int>() { 40, 40, 20 });
                            this.tableLayoutPanel_Top.Margin = new Padding(0);
                            this.tableLayoutPanel_Top.Padding = new Padding(0);
                            this.tableLayoutPanel_Top.BackColor = Color.White;
                            this._PanelTop.Padding = new Padding(0);
                            this._PanelTop.Margin = new Padding(0);
                            this._PanelTop.Controls.Add(this.tableLayoutPanel_Top);

                            //bigbuttonTop = new BigButton(string.Format(R.StringLine_TopButton_Guided,
                            //    this.stationLineModule.utility,
                            //    this.stationLineModule.ecartoInstrument._Name,
                            //    Math.Round(this.stationLineModule._AverageStationLine._Parameters._Tolerance * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"))),
                            //   R.Save,
                            //    this.buttonSave_Click);
                            //bigbuttonTop.Dock = DockStyle.Fill;
                            //this._PanelTop.Controls.Add(this.tableLayoutPanel_Top);
                            //this.tableLayoutPanel_Top.ColumnStyles.RemoveAt(1);
                            //this.tableLayoutPanel_Top.ColumnStyles.RemoveAt(0);
                            //if ((this._PanelTop.Width - bigButton_Help.Width - 10) > 0)
                            //{
                            //    this.tableLayoutPanel_Top.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, this._PanelTop.Width - bigButton_Help.Width - 10));
                            //    this.tableLayoutPanel_Top.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, bigButton_Help.Width + 10));
                            //}
                            //else
                            //{
                            //    this.tableLayoutPanel_Top.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
                            //    this.tableLayoutPanel_Top.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
                            //}

                            //bigButton_Help.Margin = new Padding(0);
                            //bigbuttonTop.Margin = new Padding(0);
                            //this.tableLayoutPanel_Top.Controls.Add(bigButton_Help, 1, 0);
                            //this.tableLayoutPanel_Top.Controls.Add(bigbuttonTop, 0, 0);

                            if (this.stationLineModule.WorkingAverageStationLine._Parameters._State is State.WireReadyToBeSaved)
                            {
                                bigbuttonTop.Available = true;
                            }
                            else
                            {
                                bigbuttonTop.Available = false;
                            }
                            break;
                        case ModuleType.Advanced:
                            bigbuttonTop = new BigButton(string.Format(R.StringLine_TopButton_Advanced,
                                this.stationLineModule.Utility,
                                Math.Round(this.stationLineModule.WorkingAverageStationLine._Parameters._Tolerance * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"))),
                               R.Line,
                                ShowContextMenuGlobal);
                            bigbuttonTop.Dock = DockStyle.Fill;
                            this._PanelTop.Controls.Add(this.tableLayoutPanel_Top);
                            this.tableLayoutPanel_Top.ColumnStyles.RemoveAt(1);
                            this.tableLayoutPanel_Top.ColumnStyles.RemoveAt(0);
                            if ((this._PanelTop.Width - bigButton_Help.Width - 10) > 0)
                            {
                                this.tableLayoutPanel_Top.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, this._PanelTop.Width - bigButton_Help.Width - 10));
                                this.tableLayoutPanel_Top.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, bigButton_Help.Width + 10));
                            }
                            else
                            {
                                this.tableLayoutPanel_Top.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 90F));
                                this.tableLayoutPanel_Top.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10F));
                            }

                            bigButton_Help.Margin = new Padding(0);
                            bigbuttonTop.Margin = new Padding(0);

                            this.tableLayoutPanel_Top.Controls.Add(bigButton_Help, 1, 0);
                            this.tableLayoutPanel_Top.Controls.Add(bigbuttonTop, 0, 0);
                            break;
                        default:
                            bigbuttonTop = new BigButton(string.Format(R.StringLine_TopButton_Advanced,
                                this.stationLineModule.Utility,
                                Math.Round(this.stationLineModule.WorkingAverageStationLine._Parameters._Tolerance * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"))),
                               R.Line,
                                ShowContextMenuGlobal);
                            bigbuttonTop.Dock = DockStyle.Fill;
                            this._PanelTop.Controls.Add(this.tableLayoutPanel_Top);
                            this.tableLayoutPanel_Top.ColumnStyles.RemoveAt(1);
                            this.tableLayoutPanel_Top.ColumnStyles.RemoveAt(0);
                            if ((this._PanelTop.Width - bigButton_Help.Width - 10) > 0)
                            {
                                this.tableLayoutPanel_Top.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, this._PanelTop.Width - bigButton_Help.Width - 10));
                                this.tableLayoutPanel_Top.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, bigButton_Help.Width + 10));
                            }
                            else
                            {
                                this.tableLayoutPanel_Top.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 90F));
                                this.tableLayoutPanel_Top.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10F));
                            }
                            this.tableLayoutPanel_Top.Controls.Add(bigButton_Help, 1, 0);
                            this.tableLayoutPanel_Top.Controls.Add(bigbuttonTop, 0, 0);
                            break;
                    }
                }

            }
            private void _PanelTop_Resize(object sender, EventArgs e)
            {
                this.UpdateView();
            }
            private void InitializeMenu()
            {
                //Main Button
                this.AddTopButton(string.Format(R.StringLine_TopButton_Advanced,
                this.stationLineModule.Utility,
                Math.Round(this.stationLineModule.WorkingAverageStationLine._Parameters._Tolerance * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"))),
               R.Line,
                ShowContextMenuGlobal);
                this.stationLineModule.WorkingAverageStationLine.ParametersBasic._State = new State.InstrumentSelection();
                this.CreateContextMenuButtons();
                this.InitializeTreeviewParameters();
                this.InitializeDataGridViewMesure();
            }
            //here so they will be create only once and not each time you show the context men
            private void CreateContextMenuButtons()
            {
                bigButton_Elements = new BigButton(
                   R.StringLine_SelectElements,
                   R.Open,
                    buttonSelectPoint_Click);
                //bouton select sequence
                bigButton_selectSequenceFil = new BigButton(
                   R.StringLine_SelectSequence,
                   R.Open,
                    button_SelectSequence_Click);
                string openFilesDescription = "";
                if (this.Module.FinalModule.RabotFiles.Count > 0) openFilesDescription += string.Join("\n", this.Module.FinalModule.RabotFiles);
                string importCSVDescription = openFilesDescription == "" ? R.T_WITH_EXPECTED_OFFSETS_OR_RELATIVE_DISPLACEMENT : openFilesDescription;
                bigButton_importCSV = new BigButton($"{R.T_IMPORT_A_CSV_FILE};{importCSVDescription}", R.Open, button_ImportCSV_Click);
                bigButton_save = new BigButton(
                   R.StringLine_SaveWire,
                   R.Save,
                    buttonSave_Click);

                //bouton select un fil pour mesure type sfb avec plusieurs fils 
                bigButton_selectFil = new BigButton(
                   R.StringLine_AddWire,
                   R.Add_Line,
                    button_SelectFil_Click);

                bigButton_Operation = new BigButton(
                   R.StringTsuNode_Line_Operation,
                    R.Operation,
                    this.ChangeOperation);
                bigButton_Cancel = new BigButton(
                   R.StringLine_CancelMenu,
                   R.Cancel,
                    buttonCancel_Click);
                bigButton_Sort = new BigButton(
                   R.StringLine_Sort,
                   R.Vertical,
                    buttonSort_Click);
                bigButton_CreateNewPoint = new BigButton(
                   R.StringLine_AddNewPointButton,
                   R.Add,
                    buttonCreateNewPoint_Click);
                bigButton_ShowDatFile = new BigButton(R.StringLine_OpenDatFile, R.Export, bigButton_ShowDatFile_Click);
            }

            #endregion
            #region datagridview
            //Methods
            /// <summary>
            /// Initialization du datagridview
            /// </summary>
            public void InitializeDataGridViewMesure()
            //Initialization du datagridview
            {
                dataGridViewMesures.ColumnHeadersHeight = 400;
                dataGridViewMesures.DefaultCellStyle.Font = Tsunami2.Preferences.Theme.Fonts.NormalItalic;
                dataGridViewMesures.RowHeadersDefaultCellStyle.Font = Tsunami2.Preferences.Theme.Fonts.NormalItalic;
                dataGridViewMesures.ColumnHeadersDefaultCellStyle.Font = Tsunami2.Preferences.Theme.Fonts.NormalItalic;
                dataGridViewMesures.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }

            private void dataGridViewMeasure_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
            {
                if (e.RowIndex > 0 && e.ColumnIndex == 4)
                {
                    string pName = dataGridViewMesures[1, e.RowIndex].Value.ToString();

                    MeasureOfOffset mesToModify = this.stationLineModule.WorkingAverageStationLine._ListStationLine[0]._MeasureOffsets.Find(x => x._PointName == pName);
                    if (mesToModify == null)
                        throw new Exception(R.T_NO_MEASURE_TO_MODIFY);
                    bool hasSubMeasures = mesToModify.SubReadings != null && mesToModify.SubReadings.Count > 1;
                    if (hasSubMeasures)
                    {
                        e.Cancel = true;


                        var r = new MessageInput(MessageType.Warning, "Sub-Measures present;Do you want edit them or remove them?")
                        {
                            ButtonTexts = new List<string>() { "Edit", R.T_REMOVE },
                        }.Show();
                        if (r.TextOfButtonClicked == R.T_REMOVE)
                        {
                            mesToModify.SubReadings.Clear();
                            System.Windows.Forms.Timer t = new System.Windows.Forms.Timer()
                            {
                                Interval = 100,
                                Enabled = true,
                            };
                            t.Tick += delegate {

                                t.Stop();
                                t.Dispose();
                                dataGridViewMesures[e.ColumnIndex, e.RowIndex].Selected = true;
                                dataGridViewMesures.BeginEdit(true);
                            };
                        }
                        else if (r.TextOfButtonClicked == "Edit")
                        {
                            e.Cancel = true;
                            System.Windows.Forms.Timer t = new System.Windows.Forms.Timer()
                            {
                                Interval = 100,
                                Enabled = true,
                            };
                            t.Tick += delegate {

                                t.Stop();
                                t.Dispose();
                                this.EnterWireMeasures(pName);
                            };
                        }
                }
                else
                    if ( mesToModify.SubReadings!=null)
                        mesToModify.SubReadings.Clear();
            }
        }

        /// <summary>
        /// événement lié si on change une mesure dans le tableau.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewMesures_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        //événement lié si on change une mesure dans le tableau Est remplacé par cellEndEdit
        {
            //this.timer_Mouse_Click.Stop();
            //this.timer_Mouse_Click.Enabled = false;
            //garde en mémoire la cellule active
            if (e.RowIndex < dataGridViewMesures.Rows.Count && e.RowIndex != -1)
            {
                rowActiveCell = e.RowIndex;
            }
            //mesure brute entrée
            if (e.RowIndex > 0 && e.ColumnIndex == 4)
            {
                //Sélectionne la mesure de l'élement suivant
                if (rowActiveCell < dataGridViewMesures.RowCount - 1)
                {
                    columnActiveCell = 4;
                    rowActiveCell++;
                    this.checkCurrentCell = true;
                }
                double datagridNumber;
                if (dataGridViewMesures[4, e.RowIndex].Value != null)
                {
                    datagridNumber = T.Conversions.Numbers.ToDouble(dataGridViewMesures[4, e.RowIndex].Value.ToString(), true, -9999);
                }
                else
                {
                    //mesure effacée
                    datagridNumber = -999900;
                }
                if (datagridNumber != -9999)
                {
                    //encode la nouvelle mesure dans une mesure temporaire et lance la mise à jour de l'average station line
                    MeasureOfOffset mesureEntree = new MeasureOfOffset();
                    mesureEntree._Point._Name = dataGridViewMesures[1, e.RowIndex].Value.ToString();
                    mesureEntree._RawReading = datagridNumber / 1000;
                    mesureEntree._Date = DateTime.Now;
                    this.stationLineModule.WorkingAverageStationLine.ParametersBasic.LastChanged = DateTime.Now;
                    this.stationLineModule.UpdateOffsets(mesureEntree);
                    return;
                }
                else
                {
                    //message erreur, valeur encodée incorrecte
                    this.ShowMessage(R.StringLine_CellIncorrectValue);
                    this.stationLineModule.UpdateOffsets();
                    return;
                }
            }
            //commentaire entré
            if (e.RowIndex > -1 && e.ColumnIndex == 13)
            {
                if (e.RowIndex >= 0)
                {
                    //Sélectionne le commentaire de l'élement suivant
                    this.checkCurrentCell = false;
                    if (rowActiveCell < dataGridViewMesures.RowCount - 1)
                    {
                        columnActiveCell = 13;
                        rowActiveCell++;
                        this.checkCurrentCell = true;
                    }
                }
                if (dataGridViewMesures[e.ColumnIndex, e.RowIndex].Value != null)
                {
                    string newComment = dataGridViewMesures[e.ColumnIndex, e.RowIndex].Value.ToString();
                    if (newComment.Length < 126)
                    {
                        if (!newComment.Contains(";") && !newComment.Contains("%") && !newComment.Contains("$")) // tsu-3144 && (System.Text.RegularExpressions.Regex.IsMatch(newComment, @"^[a-zA-Z0-9 ]+$"))))
                        {
                            this.stationLineModule.ChangeComment(dataGridViewMesures[1, e.RowIndex].Value.ToString(), newComment);
                            return;
                        }
                        else
                        {
                            this.ShowMessage(R.StringTilt_WrongCommentSemicolon);
                            this.UpdateListMeasure();
                            return;
                        }
                    }
                    else
                    {
                        this.ShowMessage(R.StringLine_TooLongComment125);
                        this.UpdateListMeasure();
                        return;
                    }
                }
                else
                {
                    //Commentaire efface
                    this.stationLineModule.ChangeComment(dataGridViewMesures[1, e.RowIndex].Value.ToString(), "");
                    return;
                }
            }
            ////commentaire entré
            //if (e.RowIndex > 0 && e.ColumnIndex == dataGridViewMesures.Columns.Count - 1)
            //{
            //    MeasureOfOffset mesureEntree = new MeasureOfOffset();
            //    mesureEntree._Date = System.DateTime.MinValue;
            //    mesureEntree._PointName = dataGridViewMesures[0, e.RowIndex].Value.ToString();
            //    mesureEntree._Comment = dataGridViewMesures[dataGridViewMesures.Columns.Count - 1, e.RowIndex].Value.ToString();
            //    this.stationLineModule.UpdateOffsets(mesureEntree);
            //}
        }

        /// <summary>
        /// Réception d'une nouvelle mesure de l'instrument
        /// </summary>
        /// <param name="instrumentMeasure"></param>
        internal void AddInstrumentMeasure(IMeasure instrumentMeasure)
        //ajoute la mesure venant dans l'instrument dans le tableau et lance la mise à jour de l'average station line
        {
            if (dataGridViewMesures.CurrentCell != null)
            {
                //ajoute seulement la mesure si on est dans une ligne du tableau avec une mesure
                if (dataGridViewMesures.CurrentCell.RowIndex > 0 && dataGridViewMesures.CurrentCell.ColumnIndex != -1)
                {
                    //garde en mémoire la cellule active
                    if (dataGridViewMesures.CurrentCell.RowIndex < dataGridViewMesures.Rows.Count - 1)
                    {
                        rowActiveCell = dataGridViewMesures.CurrentCell.RowIndex + 1;
                    }
                    else
                    {
                        rowActiveCell = dataGridViewMesures.CurrentCell.RowIndex;
                    }
                    this.CheckDatagridIsInEditMode();
                    MeasureOfOffset mesureEntree = instrumentMeasure as MeasureOfOffset;
                    //ajoute la mesure dans la colonne mesure brute
                    mesureEntree._Point._Name = dataGridViewMesures[1, dataGridViewMesures.CurrentCell.RowIndex].Value.ToString();
                    mesureEntree._RawReading = mesureEntree._RawReading / 1000;
                    this.stationLineModule.UpdateOffsets(mesureEntree);
                }
            }
        }

        private void dataGridViewMesures_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dataGridViewMesures.Rows[e.RowIndex].Height = 30;
        }

        /// <summary>
        /// événements si on clique dans le tableau (effacement points, fil)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewMeasure_CellClick(object sender, DataGridViewCellEventArgs e)
        //événements si on clique dans le tableau (effacement points, fil)
        {
            if (e.RowIndex < dataGridViewMesures.Rows.Count && e.RowIndex != -1)
            {
                rowActiveCell = e.RowIndex;
            }
            if (e.ColumnIndex != -1)
            {
                columnActiveCell = e.ColumnIndex;
            }
            if (dataGridViewMesures.IsCurrentCellInEditMode) return;
            this.checkCurrentCell = false;
            string nomFil;
            int numeroFil;
            if (e.RowIndex != -1)
            {
                switch (e.ColumnIndex)
                {
                    case 0:
                        if (dataGridViewMesures.RowCount > 3 && dataGridViewMesures[1, e.RowIndex].Value.ToString() != "")
                        {
                            //efface un point si on clique sur image de suppression
                            string titleAndMessage = string.Format(R.StringLine_ConfirmDeletePoint, dataGridViewMesures[1, e.RowIndex].Value);
                            MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                            {
                                ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL }
                            };
                            string respond = mi.Show().TextOfButtonClicked;
                            if (respond == R.T_OK)
                            {
                                this.stationLineModule.DeletePoint(dataGridViewMesures[1, e.RowIndex].Value.ToString());
                            }
                        }
                        break;
                    case 2:
                        //met une mesure not used ou used
                        if ((dataGridViewMesures.RowCount > 2))
                        {
                            this.stationLineModule.ReverseMeasureStatus(dataGridViewMesures[1, e.RowIndex].Value.ToString());
                        }
                        break;
                    case 3:
                        break;
                    case 14:
                        var pointName = dataGridViewMesures[1, e.RowIndex].Value.ToString();
                        this.Module.ChangeInterfaces(pointName);
                        break;
                    default:
                        break;
                }
                ///Change ancrage ou extemmité du fil
                if ((dataGridViewMesures.Columns[e.ColumnIndex].HeaderText == R.StringLine_DataGrid_A1
                    || dataGridViewMesures.Columns[e.ColumnIndex].HeaderText == R.StringLine_DataGrid_A2
                    || dataGridViewMesures.Columns[e.ColumnIndex].HeaderText == R.StringLine_DataGrid_Fix
                    || dataGridViewMesures.Columns[e.ColumnIndex].HeaderText == R.StringLine_DataGrid_E1
                    || dataGridViewMesures.Columns[e.ColumnIndex].HeaderText == R.StringLine_DataGrid_E2)
                    && dataGridViewMesures[1, e.RowIndex].Value.ToString() != "") //lorsque le nom du point est vide, c'est la ligne blanche pour calculer la moyenne des dipôles SPS
                {
                    // Dans le cas de calcul SFB, la ligne 0 est vide et est pour effacer un fil
                    if (e.RowIndex != 0)
                    {
                        if ((dataGridViewMesures.Columns[e.ColumnIndex].HeaderText == R.StringLine_DataGrid_E1
                    || dataGridViewMesures.Columns[e.ColumnIndex].HeaderText == R.StringLine_DataGrid_E2)
                    && this.moduleType == ModuleType.Guided)
                        {
                            ///Dans les modules guidés, on ne permet pas de changer les extrémités par l'utilisateur.
                            new MessageInput(MessageType.Warning, R.StringLine_ChangeExtremitiesNotPermitted).Show();
                            return;
                        }
                        else
                        {
                            this.computeStrategy.ChangeAnchorsDataGridView(dataGridViewMesures, e, this.stationLineModule);
                            this.RedrawTreeviewParameters();
                        }
                    }
                }
                //efface un fil
                if (e.RowIndex == 0 && e.ColumnIndex != 9)
                {
                    if (dataGridViewMesures.Columns[e.ColumnIndex].HeaderText == "A1" || dataGridViewMesures.Columns[e.ColumnIndex].HeaderText == "E1")
                    {
                        nomFil = dataGridViewMesures.Columns[e.ColumnIndex - 1].Name.ToString();
                        nomFil = nomFil.Remove(0, 8);
                        int.TryParse(nomFil, out numeroFil);
                        this.stationLineModule.DeleteFil(numeroFil);
                    }
                }
            }

        }

        private void DataGridViewMesures_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
                switch (e.ColumnIndex)
                {
                    case 4:
                        string pointName = dataGridViewMesures[1, e.RowIndex].Value.ToString();
                        if (pointName != "")
                            this.EnterWireMeasures(pointName);
                        break;
                }
        }

        private void EnterWireMeasures(string pointName)
        {
            MeasureOfOffset mesToModify = this.stationLineModule.WorkingAverageStationLine._ListStationLine[0]._MeasureOffsets.Find(x => x._PointName == pointName);
            if (mesToModify == null)
                throw new Exception(R.T_NO_MEASURE_TO_MODIFY);

            bool hasSubMeasures = mesToModify.SubReadings != null && mesToModify.SubReadings.Count > 0;
            var hasRawMeasure = mesToModify._RawReading != -9999 && mesToModify._RawReading != -999.9;
            if (!hasSubMeasures && hasRawMeasure)
            {
                mesToModify.SubReadings = new List<MeasureOfOffset.SubReading>();
                var subReading = new MeasureOfOffset.SubReading(mesToModify._RawReading, true);
                mesToModify.SubReadings.Add(subReading);
            }

            //return;
            bool ret = Smart.MeasureInput.MeasureInput.Show(ref mesToModify, this, Module._Name);

            // Cancelled
            if (!ret)
                return;

            //message erreur, valeur encodée incorrecte
            if (mesToModify._RawReading == -9999 || double.IsNaN(mesToModify._RawReading))
            {
                this.ShowMessage(R.StringLine_CellIncorrectValue);
                this.stationLineModule.UpdateOffsets();
                return;
            }

            //Utile ??
            this.Module.CheckBocValidity(mesToModify);

            //Lance la mise à jour
            this.stationLineModule.UpdateOffsets(mesToModify);
        }

        /// <summary>
        /// Click sur entete d'une colonne fait le tri par dcum ou inverse dcum
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewMesures_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //tri par DCUM disponible dans stratégie SFB
            if (dataGridViewMesures.IsCurrentCellInEditMode) return;
            //if (dataGridViewMesures.RowCount > 0 && this.stationLineModule.sortOnlyDcum == true)
            //{
            //    this.buttonSort_Click();
            //}
            if (dataGridViewMesures.RowCount > 0 && e.ColumnIndex == 3)//&& this.stationLineModule.sortOnlyDcum == false)
            {
                this.buttonSort_Click();
            }
        }
        /// <summary>
        /// Permet de détecter quand la current cell est changée automatiquement
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewMesures_CurrentCellChanged(object sender, EventArgs e)
        {
            //permet de détecter quand le datagridview change automatiquement la ligne courante après avoir entré une valeur dans une case.  
            if (this.checkCurrentCell == true && this.dataGridViewMesures.CurrentCell != null)
            {

                ////BeginInvoke crèe un processus asynchrone qui empêche de tourner en boucle lorsqu'on change la current cell
                Delegate delegateCurrentCell = new MethodInvoker(() =>
                {
                    //this.Module.InvokeOnApplicationDispatcher(SetCurrentCell);
                    this.CellFocus();
                });
                IAsyncResult async = this.BeginInvoke(delegateCurrentCell);
                this.checkCurrentCell = false;
            }
        }
        /// <summary>
        /// Force le datagrid à reprendre le focus si on passe la souris dessus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewMesures_MouseEnter(object sender, EventArgs e)
        {
            Action a = () =>
            {
                dataGridViewMesures.Focus();
            };
            Tsunami2.Properties.InvokeOnApplicationDispatcher(a);
        }
        /// <summary>
        /// Check if datagrid is in edit mode and validate it before leaving
        /// </summary>
        internal void CheckDatagridIsInEditMode()
        {
            if (dataGridViewMesures.IsCurrentCellInEditMode)
            {
                //this.dataGridViewLevel.CellValueChanged -= this.dataGridViewLevel_CellValueChanged;
                int saveRow = rowActiveCell;
                int saveColumn = columnActiveCell;
                //Try car de temps en temps endEdit peut faire une exception
                try
                {
                    dataGridViewMesures.EndEdit();

                }
                catch (Exception ex)
                {
                    ex.Data.Clear();
                }
                //this.dataGridViewLevel.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewLevel_CellValueChanged);
                rowActiveCell = saveRow;
                columnActiveCell = saveColumn;
            }
        }
        /// <summary>
        /// Hide the treeview and the instrument ussing the splitter position in the splitcontainers
        /// </summary>
        internal void MaximizeDataGridView()
        {
            // Hide the treeview
            this.splitContainerFil.Panel1MinSize = 0;
            this.splitContainerFil.SplitterDistance = 0;
            this.HideManualOffsetmeterView();
        }

        private void SetFirstDisplayedRowIndex(int rowIndex)
        {
            // Use BeginInvoke to defer the operation to the UI thread
            dataGridViewMesures.BeginInvoke((MethodInvoker)delegate
            {
                try
                {
                    dataGridViewMesures.FirstDisplayedScrollingRowIndex = rowIndex;
                }
                catch (Exception ex)
                {
                    Logs.Log.AddEntryAsPayAttentionOf(this.Module, $"Cannot reset the datagridView to the row #{rowIndex} {ex}");
                }
            });
        }

        /// <summary>
        /// Met la scroll bar horizontale à gauche et la vertical en haut
        /// </summary>
        internal void SetScrollBarAtZero()
        {
            if (this.dataGridViewMesures.Columns.Count > 0 && this.dataGridViewMesures.Rows.Count > 1)
            {
                if (this.dataGridViewMesures.Columns[0].Visible && this.dataGridViewMesures.Rows[1].Visible)
                {
                    this.dataGridViewMesures.FirstDisplayedScrollingColumnIndex = 0;
                    SetFirstDisplayedRowIndex(1);
                }
                else
                {
                    if (this.dataGridViewMesures.Columns.Count > 1 && this.dataGridViewMesures.Rows.Count > 1)
                    {
                        if (this.dataGridViewMesures.Columns[1].Visible && this.dataGridViewMesures.Rows[1].Visible)
                        {
                            this.dataGridViewMesures.FirstDisplayedScrollingColumnIndex = 1;
                            SetFirstDisplayedRowIndex(1);
                        }
                    }
                }
            }
        }
        ///// <summary>
        ///// garde le focus sur la current cell dans le datagrid
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void Timer_Keep_Cell_Focus_Tick(object sender, EventArgs e)
        //{
        //this.Timer_Keep_Cell_Focus.Enabled = false;
        //this.Timer_Keep_Cell_Focus.Stop();
        //this.Timer_Keep_Cell_Focus.Dispose();
        //DataGridViewCellEventArgs eventCell = new DataGridViewCellEventArgs(-1, -1);
        //dataGridViewMeasure_CellClick(this.dataGridViewMesures, eventCell);
        //}
        #endregion
        #region treeview
        /// <summary>
        /// Evenements liés lorsqu'on clique sur les noeuds du treeview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeView_Parameters_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        //Evenements liés si on clique dans le treeview
        {
            if (!e.Node.Bounds.Contains(e.Location)) return;
            if (e.Node == null) return;
            this.CheckDatagridIsInEditMode();
            string[] nameSplit;
            char splitChar = '_';
            nameSplit = e.Node.Name.Split(splitChar);
            if (this.moduleType == ModuleType.Advanced)
            {
                if ((nameSplit[0] == "Parameters") && (nameSplit[1] == "")) { this.RenameStation(); }
                if (nameSplit[1] == "Team") { this.ChangeTeam(); }
                if (nameSplit[1] == "OperationID") { this.ChangeOperation(); }
                if (nameSplit[1] == "Instrument") { this.ChangeInstrument(); }
                if (nameSplit[1] == "Strategy" && nameSplit[2] != "ShowLGC") { this.ChangeComputeStrategy(); }
                if (nameSplit[0] == "Comp" && nameSplit[1] != "Strategy") { this.LaunchLGCCalculationToShowIt(); }
            }
            if (nameSplit[1] == "CoordType") { this.ChangeCoordType(); }
            if (nameSplit[1] == "Tolerance") { this.ChangeTolerance(); }
            if (nameSplit[1] == "Temperature") { this.ChangeTemperature(); }
        }
        /// <summary>
        /// creation du treeview avec tous les nodes
        /// </summary>
        public void InitializeTreeviewParameters()
        //creation du treeview avec tous les nodes
        {
            switch (this.moduleType)
            {
                case ModuleType.Guided:
                    showCalculationOption = false;
                    break;
                case ModuleType.Advanced:
                    showCalculationOption = true;
                    break;
                default:
                    showCalculationOption = true;
                    break;
            }
            this.treeView_Parameters.BeginUpdate();
            this.treeView_Parameters.ImageList = TreeNodeImages.ImageListInstance;
            this.treeView_Parameters.SelectedImageKey = "StationParameters";
            //Noeud parameter
            node_Parameters = new TsuNode();
            node_Parameters.SelectedImageKey = "StationParameters";
            node_Parameters.ImageKey = "StationParameters";
            this.treeView_Parameters.Nodes.Add(node_Parameters);
            this.DressNodeParameter();
            //Noeud Admin
            this.node_Admin = new TsuNode();
            if (showAdmin)
            {
                this.node_Parameters.Nodes.Add(node_Admin);
                this.node_Admin.BasedOn(this.stationLineModule.WorkingAverageStationLine._Parameters, this.computeStrategy, this.showCoordType);
            }
            //Noeud Computation
            this.node_Computation = new TsuNode();
            if (this.showComputation)
            {
                this.node_Parameters.Nodes.Add(this.node_Computation);
                this.node_Computation.ComputationBasedOn(this);
            }
            contextMenuStrip.Click += new EventHandler(Context_OnClick);
            this.treeView_Parameters.ExpandAll();
            this.treeView_Parameters.EndUpdate();
        }

        /// <summary>
        /// Choix du nom du noeud paramètre et de son format
        /// </summary>
        private void DressNodeParameter()
        {
            //Choix de l'entête du noeud paramètre
            string chosenStrategy = (this.Module.FinalModule.RabotStrategy ==  Strategy.Not_Defined) ? string.Empty : " (Curve smooth alignment strategy: " + this.Module.FinalModule.RabotStrategy + ")";
            this.node_Parameters.Name = "Parameters__";
            if (this.stationLineModule.WorkingAverageStationLine != null)
                this.node_Parameters.Text = string.Format(R.StringLine_parameters + chosenStrategy, this.stationLineModule.WorkingAverageStationLine._Name, this.stationLineModule.WorkingAverageStationLine._Parameters._State._Description);
            this.node_Parameters.State = StateType.Normal;
            if (this.stationLineModule.WorkingAverageStationLine._Parameters._State is State.Opening) this.node_Parameters.State = StateType.Normal;
            if (this.stationLineModule.WorkingAverageStationLine._Parameters._State is State.SFBcalculationSuccessfull) this.node_Parameters.State = StateType.Normal;
            if (this.stationLineModule.WorkingAverageStationLine._Parameters._State is State.LGC2CalculationSuccessfull) this.node_Parameters.State = StateType.Normal;
            if (this.stationLineModule.WorkingAverageStationLine._Parameters._State is State.WireReadyToBeSaved) this.node_Parameters.State = StateType.Ready;
            if (this.stationLineModule.WorkingAverageStationLine._Parameters._State is State.WireSaved) this.node_Parameters.State = StateType.Ready;
            if (this.stationLineModule.WorkingAverageStationLine._Parameters._State is State.LGC2CalculationFailed) this.node_Parameters.State = StateType.NotReady;
            if (this.stationLineModule.WorkingAverageStationLine._Parameters._State is State.SFBcalculationFailed) this.node_Parameters.State = StateType.NotReady;
            this.node_Parameters.ColorNodeBaseOnState(this.node_Parameters);
            this.node_Parameters.Tag = this.stationLineModule.WorkingAverageStationLine;
        }
        /// <summary>
        /// creation du treeview avec tous les nodes
        /// </summary>
        public void RedrawTreeviewParameters()
        //creation du treeview avec tous les nodes
        {
            switch (this.moduleType)
            {
                case ModuleType.Guided:
                    showCalculationOption = false;
                    break;
                case ModuleType.Advanced:
                    showCalculationOption = true;
                    break;
                default:
                    showCalculationOption = true;
                    break;
            }
            //
            TsuNode ghostNode_Computation = new TsuNode();
            TsuNode ghostNode_Admin = new TsuNode();
            this.treeView_Parameters.BeginUpdate();
            //Noeud parametre
            this.DressNodeParameter();
            //node_Parameters.BasedOn(this.stationLineModule._AverageStationLine._Parameters);
            //Noeud Admin
            if (showAdmin)
            {
                ghostNode_Admin.BasedOn(this.stationLineModule.WorkingAverageStationLine._Parameters, this.computeStrategy, this.showCoordType);
                this.node_Admin.UpdateTsuNode(ghostNode_Admin);
            }
            else
            {
                this.node_Admin.Nodes.Clear();
            }
            if (this.showComputation)
            {
                ghostNode_Computation.ComputationBasedOn(this);
                this.node_Computation.UpdateTsuNode(ghostNode_Computation);
            }
            else
            {
                this.node_Computation.Nodes.Clear();
            }
            this.treeView_Parameters.EndUpdate();
            this.stationLineModule.Change();
        }
        #endregion
        #region contextmenu
        /// <summary>
        /// creation de la liste des boutons de menu
        /// </summary>
        private void ShowContextMenuGlobal()
        // creation de la liste des boutons de menu
        {
            this.CheckDatagridIsInEditMode();
            contextButtons = new List<Control>();
            if (this.stationLineModule.WorkingAverageStationLine._Parameters._Instrument._Model != null)
            {
                contextButtons.Add(bigButton_Elements);
                contextButtons.Add(bigButton_importCSV);
                contextButtons.Add(bigButton_selectSequenceFil);
                contextButtons.Add(bigButton_CreateNewPoint);
            }
            if (this.computeStrategy is SFBStrategy) { contextButtons.Add(bigButton_selectFil); }
            if (this.stationLineModule.WorkingAverageStationLine._Parameters._State is State.WireReadyToBeSaved)
            { contextButtons.Add(bigButton_save); }
            string filepath = this.stationLineModule.WorkingAverageStationLine.ParametersBasic._GeodeDatFilePath;
            if (filepath != "") contextButtons.Add(bigButton_ShowDatFile);
            if (this.dataGridViewMesures.RowCount != 0) { contextButtons.Add(bigButton_Sort); }
            contextButtons.Add(bigButton_Cancel);
            bool ok = this.ShowPopUpMenu(contextButtons);
            // il peut arriver lors de l'ajout d'un nouveau bouton que l'affichage des contrôles dans le popup menu ne soit pas dans l'ordre. Le réaffichage du pop up semble résoudre le problème.
            // Ajout 24-03-20 pour bouton default staff et secondary staff, ne le fait plus qu'une seule fois pour éviter une boucle infinie
            //if (!ok && !stopCheckingContextMenuOrder)
            //{
            //    stopCheckingContextMenuOrder = true;
            //    this.ShowContextMenuGlobal();
            //    stopCheckingContextMenuOrder = false;
            //}
        }
        /// <summary>
        /// cache le menu context si on clique dessus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Context_OnClick(object sender, EventArgs e)
        //cache le menu context si on clique dessus
        {
            contextMenuStrip.Hide();
        }

        //private void RefreshGlobalContextMenuItemCollection()
        ////raffraichit le menu context qui apparait lorsqu'on clique sur le gros bouton
        //{
        //    _ContextMenuStrip.Items.Clear();
        //    _ContextMenuStrip.Items.AddRange(contextMenu.ToArray());
        //    _ContextMenuStrip.Refresh();
        //}
        #endregion
        #region method
        /// <summary>
        /// permet d'afficher le form de l'instrument dans le panel du dessous
        /// </summary>
        /// <param name="i"></param>
        internal override void UpdateInstrumentView()
        //permet d'afficher le form de l'instrument dans le panel du dessous
        {
            splitContainer1.Panel2.Controls.Clear();
            if (this.stationLineModule._InstrumentManager.SelectedInstrumentModule == null)
                this.stationLineModule._InstrumentManager.SetInstrument(this.stationLineModule.WorkingAverageStationLine.ParametersBasic._Instrument);
            TsuView v = this.stationLineModule._InstrumentManager.SelectedInstrumentModule._TsuView;
            if (v != null)
            {
                v.TopLevel = false;
                v.Dock = DockStyle.None;
                this.ScaleInstrumentView();
                v.Show();
                v.FormBorderStyle = FormBorderStyle.None;
                splitContainer1.Panel2.Controls.Add(v);
                this.instrumentView = v;
            }
        }
        /// <summary>
        /// met à jour le datagrid en fonction de l'average station line
        /// </summary>
        internal void UpdateListMeasure()
        //met à jour le datagrid en fonction de l'average station line
        {
            //RefreshGlobalContextMenuItemCollection();
            this.dataGridViewMesures.CellValueChanged -= this.dataGridViewMesures_CellValueChanged;
            this.dataGridViewMesures.CurrentCellChanged -= this.dataGridViewMesures_CurrentCellChanged;
            RedrawTreeviewParameters();
            if (this.stationLineModule.WorkingAverageStationLine._ListStationLine.Count != 0) 
                    this.computeStrategy.UpdateDatagridView(this.stationLineModule, dataGridViewMesures);
            this.UpdateView();
            this.dataGridViewMesures.CellValueChanged += new DataGridViewCellEventHandler(this.dataGridViewMesures_CellValueChanged);
            this.dataGridViewMesures.CurrentCellChanged += new EventHandler(this.dataGridViewMesures_CurrentCellChanged);
            if (this.checkCurrentCell)
            {
                CellFocus();
            }
            else
            {
                dataGridViewMesures.ClearSelection();
            }
        }
        /// <summary>
        /// Remet le focus sur la cellule courante et la met en mode edition
        /// </summary>
        private void CellFocus()
        {
            Action a = () =>
            {
                if (dataGridViewMesures.RowCount > 0)
                {
                    //remet le focus sur la cellule active avant refresh du tableau
                    if (rowActiveCell >= dataGridViewMesures.RowCount)
                    {
                        rowActiveCell = 1;
                    }
                    if (columnActiveCell != dataGridViewMesures.ColumnCount - 1)
                    {
                        columnActiveCell = 4;
                    }

                    ///Dans le cas des dipoles SPS, pour sauter la ligne d'interconnection
                    if (dataGridViewMesures[columnActiveCell, rowActiveCell].ReadOnly)
                    {
                        if (rowActiveCell < dataGridViewMesures.RowCount - 1)
                        {
                            rowActiveCell++;
                        }
                    }
                    this.dataGridViewMesures.CurrentCellChanged -= this.dataGridViewMesures_CurrentCellChanged;
                    dataGridViewMesures.Focus();
                    dataGridViewMesures.ClearSelection();
                    dataGridViewMesures.CurrentCell = dataGridViewMesures[columnActiveCell, rowActiveCell];
                    dataGridViewMesures.Rows[rowActiveCell].Cells[columnActiveCell].Selected = true;
                    //dataGridViewMesures.BeginEdit(true);
                    this.dataGridViewMesures.CurrentCellChanged += new EventHandler(this.dataGridViewMesures_CurrentCellChanged);
                }
            };
            Tsunami2.Properties.InvokeOnApplicationDispatcher(a);
        }
        /// <summary>
        /// Affiche la fenêtre d'erreur si encodage incorrect
        /// </summary>
        /// <param name="titre"></param>
        /// <param name="message"></param>
        internal void ShowMessage(string titreAndMessage)
        //Affiche la fenêtre d'erreur si encodage incorrect
        {
            new MessageInput(MessageType.Critical, titreAndMessage).Show();
        }
        /// <summary>
        /// Montre le fichier dat exporté
        /// </summary>
        private void bigButton_ShowDatFile_Click()
        {
            string filepath = this.stationLineModule.WorkingAverageStationLine.ParametersBasic._GeodeDatFilePath;
            if (System.IO.File.Exists(filepath)) Shell.Run(filepath);
        }
        /// <summary>
        /// Clic sur le bouton de sélection de l'instrument
        /// </summary>
        private void buttonSelectInstrument_Click()
        //selection de l'instrument
        {
            OffsetMeter ecarto;
            this.dataGridViewMesures.CancelEdit();
            ecarto = this.stationLineModule.SelectionEcartometre();
            if (ecarto != null) { this.RedrawTreeviewParameters(); }

        }
        /// <summary>
        /// sauvegarde des mesures dans un fichier texte
        /// </summary>
        private void buttonSave_Click()
        //sauvegarde des mesures dans un fichier texte
        {
            this.dataGridViewMesures.CancelEdit();
            if (this.stationLineModule.CheckIfReadyToBeSaved())
            {
                this.stationLineModule.ExportToGeode(false, true);
                this.RedrawTreeviewParameters();
                this.UpdateView();
            }
            else
            {
                this.ShowMessage(R.StringLine_CannotSave);
            }
        }
        /// <summary>
        /// Clic sur le bouton de sélection des points
        /// </summary>
        private void buttonSelectPoint_Click()
        //selection des points
        {
            this.dataGridViewMesures.CancelEdit();
            this.stationLineModule.SelectElements();
        }
        /// <summary>
        /// Clic sur le bouton Select Fil
        /// </summary>
        private void button_SelectFil_Click()
        //Ajout d'un fil
        {
            this.dataGridViewMesures.CancelEdit();
            this.stationLineModule.AddFil();
        }
        private void button_Help_Click()
        //Affichage aide fil
        {
            this.dataGridViewMesures.CancelEdit();
            this.ShowMessageOfImage(R.StringLine_Help, R.T_OK, R.WireOffsetv2);
        }
        /// <summary>
        /// Change l'option d'affichage des résultats du calcul LGC2
        /// </summary>
        private void LaunchLGCCalculationToShowIt()
        {
            this.dataGridViewMesures.CancelEdit();
            //active ou désactive affichage fichier texte LGC2 après calcul
            //this.showLGC2Output = true;
            //if (this.showLGC2Output == true)
            //    this.showLGC2Output = false;
            //else
            //    this.showLGC2Output = true;
            var controls = new List<Control>();
            controls.Add(new BigButton(R.StringTsuNode_Line_ShowLGCFile, R.Lgc, button_showLGC2_Click));
            this.ShowPopUpMenu(controls, "Ecarto Compute Files");
            //foreach (TSU.Stations.StationLine line in this.stationLineModule._AverageStationLine._ListStationLine)
            //{
            //    line._showLGC2Files = this.showLGC2Output;
            //}
            //this.RedrawTreeviewParameters();
        }

        private void button_showLGC2_Click()
        {
            this.stationLineModule.WorkingAverageStationLine._showLGC2Files = true;
            this.stationLineModule.UpdateOffsets();
            this.stationLineModule.WorkingAverageStationLine._showLGC2Files = false;
        }

        /// <summary>
        /// Changement de la tolérance d'alignement
        /// </summary>
        private void ChangeTolerance()
        {
            this.dataGridViewMesures.CancelEdit();

            //changement de la tolérance d'alignement
            string initialTextBoxText = Math.Round(this.stationLineModule.WorkingAverageStationLine._Parameters._Tolerance * 1000, 2).ToString();
            List<string> buttonTexts = new List<string> { R.T_OK, R.T_CANCEL };
            MessageTsu.ShowMessageWithTextBox(R.StringLine_ChooseTolerance, buttonTexts, out string buttonClicked, out string textInput, initialTextBoxText);

            if (buttonClicked != R.T_CANCEL)
            {
                double newTolerance = T.Conversions.Numbers.ToDouble(textInput, true, -9999);
                if (newTolerance != -9999)
                {
                    this.stationLineModule.ChangeTolerance(newTolerance / 1000);
                    this.RedrawTreeviewParameters();
                    this.stationLineModule.UpdateOffsets();
                    this.UpdateView();
                }
                else
                {
                    this.ShowMessage(R.String_WrongTolerance);
                }
            }
        }
        /// <summary>
        /// Changement de la temperature de mesure
        /// </summary>
        private void ChangeTemperature()
        {
            this.dataGridViewMesures.CancelEdit();

            //changement de la temperature de mesure
            string initialTextBoxText = Math.Round(this.stationLineModule.WorkingAverageStationLine._Parameters._Temperature, 1).ToString();
            List<string> buttonTexts = new List<string> { R.T_OK, R.T_CANCEL };
            MessageTsu.ShowMessageWithTextBox(R.StringLine_ChangeTemperature, buttonTexts, out string buttonClicked, out string textInput, initialTextBoxText);

            if (buttonClicked != R.T_CANCEL)
            {
                double newTemperature = T.Conversions.Numbers.ToDouble(textInput, true, -9999);
                if (newTemperature != -9999)
                {
                    this.stationLineModule.ChangeTemperature(newTemperature);
                    this.RedrawTreeviewParameters();
                    //this.stationLineModule.UpdateOffsets();
                    this.UpdateView();
                }
                else
                {
                    this.ShowMessage(R.String_WrongTemperature);
                }
            }
        }
        /// <summary>
        /// Change la statégie de calcul de SFB vers LGC2 et vice versa
        /// </summary>
        private void ChangeComputeStrategy()
        {
            var controls = new List<Control>();
            controls.Add(new BigButton(R.StringLine_LGC2button, R.Lgc, button_LGC2_Click));
            controls.Add(new BigButton(R.StringLine_SFBbutton, R.Sfb, button_SFB_Click));
            this.ShowPopUpMenu(controls, "Ecarto Compute Strategy");
        }
        /// <summary>
        /// Encodage d'une mesure d'offset dans un popup dans le datagridview result du module guidé element alignment
        /// </summary>
        /// <param name="pointName"></param>
        internal void EnterWireValue(string pointName)
        {
            MeasureOfOffset mesOff = this.stationLineModule.WorkingAverageStationLine._ListStationLine[0]._MeasureOffsets.Find(x => x._PointName == pointName);
            string theoReading;
            if (mesOff._TheoreticalReading != na)
                theoReading = Math.Round(mesOff._TheoreticalReading * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            else
                theoReading = "---";

            string previousValueText;
            if (mesOff._RawReading != na)
                previousValueText = Math.Round(mesOff._RawReading * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            else
                previousValueText = "";

            string titleAndMessage = pointName + "\r\n" + string.Format(R.StringAlignment_EnterOffsetMeasure, theoReading);
            List<string> buttonTexts = new List<string> { R.T_OK, R.T_CANCEL };
            MessageTsu.ShowMessageWithTextBox(titleAndMessage, buttonTexts, out string buttonClicked, out string textInput, previousValueText);

            if (buttonClicked != R.T_CANCEL)
            {
                if (textInput == "")
                {
                    //Effacement de la mesure
                    mesOff._RawReading = na;
                    mesOff._TheoreticalReading = na;
                    mesOff._ReductedReading = na;
                    mesOff._CorrectedReading = na;
                    mesOff._TheoreticalReading = na;
                    mesOff._TheoreticalDeviation = na;
                    mesOff._AverageDeviation = na;
                    mesOff._AverageTheoreticalReading = na;
                    mesOff._Date = DateTime.Now;
                    this.stationLineModule.UpdateOffsets(mesOff);
                }
                else
                {
                    double j = T.Conversions.Numbers.ToDouble(textInput, true, -9999);

                    if (j != -9999)
                    {
                        //encode la nouvelle mesure dans une mesure temporaire et lance la mise à jour
                        mesOff._RawReading = j / 1000;
                        mesOff._Date = DateTime.Now;
                        this.Module.CheckBocValidity(mesOff);
                        this.stationLineModule.UpdateOffsets(mesOff);
                    }
                    else
                    {
                        //message erreur, valeur encodée incorrecte
                        this.ShowMessage(R.StringLine_CellIncorrectValue);
                        this.stationLineModule.UpdateOffsets();
                    }
                }
            }
        }
        /// <summary>
        /// Change l'instrument sélectionné
        /// </summary>
        private void ChangeInstrument()
        {
            //choix de l'instrument
            OffsetMeter instrument;
            this.dataGridViewMesures.CancelEdit();
            instrument = this.stationLineModule.SelectionEcartometre();
            this.HideManualOffsetmeterView();
            this.RedrawTreeviewParameters();
        }
        /// <summary>
        /// Change l'équipe
        /// </summary>
        private void ChangeTeam()
        {
            this.dataGridViewMesures.CancelEdit();
            List<string> buttonTexts = new List<string> { R.T_OK, R.T_CANCEL };
            MessageTsu.ShowMessageWithTextBox(R.StringLine_ChooseTeam, buttonTexts, out string buttonClicked, out string textInput, this.stationLineModule.WorkingAverageStationLine.ParametersBasic._Team.ToString());

            if (buttonClicked != R.T_CANCEL)
            {
                //Vérifie que le nom d'équipe contient que des caractères alphanumériques et est de maximum 8 caractères
                //System.Text.RegularExpressions.Regex checkMsgResponse=new System.Text.RegularExpressions.Regex(MsgResponse);
                if (System.Text.RegularExpressions.Regex.IsMatch(textInput, @"^[a-zA-Z]+$") && (textInput.Length < 9 || textInput == R.String_UnknownTeam))
                {
                    textInput = textInput.ToUpper();
                    this.stationLineModule.ChangeTeam(textInput);
                    this.RedrawTreeviewParameters();
                }
                else
                {
                    this.ShowMessage(R.StringLine_InvalidTeam);
                }
            }
        }
        /// <summary>
        /// Permet à l'utilisateur de renommer la station comme il le veut
        /// </summary>
        private void RenameStation()
        {
            List<string> buttonTexts = new List<string> { R.T_OK, R.T_CANCEL };

            MessageTsu.ShowMessageWithTextBox(R.String_StationName, buttonTexts, out string buttonClicked, out string textInput, this.stationLineModule.WorkingAverageStationLine.specialPartOfStationName);

            if (buttonClicked != R.T_CANCEL)
            {
                //Vérifie que le nom d'équipe contient que des caractères alphanumériques et est de maximum 8 caractères
                if ((System.Text.RegularExpressions.Regex.IsMatch(textInput, @"^[a-zA-Z0-9._-]+$") || textInput == "") && (textInput.Length < 20))
                {
                    textInput = textInput.ToUpper();
                    string saveAverageLineName = this.stationLineModule.WorkingAverageStationLine._Name;
                    if (this.stationLineModule.WorkingAverageStationLine.ReName(textInput))
                    {
                        this.stationLineModule.AddElementsToElementModule();
                        this.stationLineModule.RemoveFromElementModule(saveAverageLineName);
                        if (this.stationLineModule.FinalModule is Line.Module lm) lm.View.SetStationNameInTopButton(this.stationLineModule);
                        if (this.stationLineModule.FinalModule.ParentModule is Common.Guided.Group.Module gm) gm.UpdateBigButtonTop();
                    }
                    this.RedrawTreeviewParameters();
                }
                else
                {
                    this.ShowMessage(R.String_WrongStationName);
                }
            }
        }
        /// <summary>
        /// Affiche le context menu pour sélection d'une opération existante ou création d'une nouvelle opération
        /// </summary>
        private void ChangeOperation()
        {
            contextMenuStrip.Items.Clear();
            contextMenuStrip.Items.Add(new BigButtonMenuItem(R.StringTsuNode_Line_Operation, R.Operation, this.SelectOperation));
            contextMenuStrip.Show(MousePosition.X - 70, MousePosition.Y - 40);
            this.SelectOperation();
        }

        /// <summary>
        /// Lance la fenêtre de sélection d'une opération existante
        /// </summary>
        internal void SelectOperation()
        {
            this.dataGridViewMesures.CancelEdit();
            O.Operation o = this.stationLineModule.FinalModule.OperationManager.SelectOperation();
            if (o != null)
            {
                this.stationLineModule.ChangeOperationID(o);
                this.RedrawTreeviewParameters();
            }
        }
        private void ChangeCoordType()
        {
            this.dataGridViewMesures.CancelEdit();
            //changement du type de coordonnées pour calcul
            string titleAndMessage = R.StringLine_AllOperation + this.stationLineModule.WorkingAverageStationLine._Parameters._CoordType.ToString();
            MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
            {
                ButtonTexts = new List<string> { R.StringLine_CCS, R.StringLine_SU },
                Sender = this.stationLineModule._Name
            };
            string MsgResponse = mi.Show().TextOfButtonClicked;
            if (MsgResponse == R.StringLine_CCS)
            {
                this.stationLineModule.WorkingAverageStationLine._Parameters._CoordType = CoordinatesType.CCS;
                this.RedrawTreeviewParameters();
                this.stationLineModule.UpdateOffsets();
            }
            if (MsgResponse == R.StringLine_SU)
            {
                this.stationLineModule.WorkingAverageStationLine._Parameters._CoordType = CoordinatesType.SU;
                this.RedrawTreeviewParameters();
                this.stationLineModule.UpdateOffsets();
            }
            //if (MsgResponse ==R.StringLine_UserDefined)
            //{
            //    this.stationLineModule._AverageStationLine._Parameters._CoordType = TSU.ENUM.CoordinatesType.UserDefined;
            //    this.RedrawTreeviewParameters();
            //    this.stationLineModule.UpdateOffsets();
            //}
        }
        /// <summary>
        /// changement strategie calcul en LGC2
        /// </summary>
        private void button_LGC2_Click()
        //changement strategie calcul en LGC2
        {
            this.dataGridViewMesures.CancelEdit();
            this.computeStrategy = new LGC2Strategy();
            this.stationLineModule._ComputeStrategy = new LGC2Strategy();
            this.stationLineModule.WorkingAverageStationLine._strategy = new LGC2Strategy();
            foreach (Station line in this.stationLineModule.WorkingAverageStationLine._ListStationLine)
            {
                line.strategy = new LGC2Strategy();
            }
            RedrawTreeviewParameters();
            this.stationLineModule.UpdateOffsets();
        }
        /// <summary>
        /// changement strategie calcul en SFB
        /// </summary>
        private void button_SFB_Click()
        //changement strategie calcul en SFB
        {
            this.dataGridViewMesures.CancelEdit();
            this.stationLineModule.SortMeasure();
            this.computeStrategy = new SFBStrategy();
            this.stationLineModule._ComputeStrategy = new SFBStrategy();
            this.stationLineModule.WorkingAverageStationLine._strategy = new SFBStrategy();
            foreach (Station line in this.stationLineModule.WorkingAverageStationLine._ListStationLine)
            {
                line.strategy = new SFBStrategy();
            }
            RedrawTreeviewParameters();
            this.stationLineModule.UpdateOffsets();
        }
        /// <summary>
        /// Ferme le context menu si on l'a ouvert par mégarde
        /// </summary>
        private void buttonCancel_Click()
        {
            //this.CloseMenu();
        }
        /// <summary>
        /// Trie par Dcum ou inverse decum
        /// </summary>
        private void buttonSort_Click()
        {
            switch (this.stationLineModule.sortDirection)
            {
                case SortDirection.Dcum:
                    this.stationLineModule.sortDirection = SortDirection.InvDcum;
                    break;
                case SortDirection.InvDcum:
                    this.stationLineModule.sortDirection = SortDirection.Dcum;
                    break;
                default:
                    break;
            }
            this.stationLineModule.SortMeasure();
            this.checkCurrentCell = false;
            DataGridUtility.SetToDcumSorting(dataGridViewMesures, 3);
            this.UpdateListMeasure();
        }
        /// <summary>
        /// ouvre l'element module pour sélectionner un fil d'une séquence
        /// </summary>
        private void button_SelectSequence_Click()
        {
            this.stationLineModule.SelectSequence();
        }
        private void button_ImportCSV_Click()
        {
            Console.WriteLine("Importing CSV FIle from advanced offset module");
            string openedFile = CsvImporter.ImportRabotCSV(this.Module.FinalModule);

            //update button text
            var button = this.bigButton_importCSV;
            if (openedFile != "")
            {
                string newText = $"{button.Title};";
                newText += button.Description.Contains("csv:") ? button.Description + "\r\n" : "";
                if (!newText.Contains(openedFile))
                {
                    newText += openedFile;
                    button.ChangeNameAndDescription(newText);
                }

            }
        }
        /// <summary>
        /// Crèe un nouveau point à mesurer
        /// </summary>
        private void buttonCreateNewPoint_Click()
        {
            if (this.zone == "")
            {
                CloneableList<Station> listStationLine = this.stationLineModule.WorkingAverageStationLine._ListStationLine;
                if (listStationLine.Count != 0)
                {
                    CloneableList<MeasureOfOffset> measureOffsets = listStationLine[0]._MeasureOffsets;
                    if (measureOffsets.Count != 0)
                    {
                        this.zone = measureOffsets[measureOffsets.Count - 1]._Point._Zone;
                    }
                }
            }

            List<string> titles = new List<string> { "Zone/Acc", "Class", "Numéro", "Point" };
            List<string> names = new List<string> { zone, classe, number, pointNewPoint };
            if (dataGridViewMesures.IsCurrentCellInEditMode) dataGridViewMesures.EndEdit();

            TableLayoutPanel tlp = CreationHelper.GetTableLayoutForRenamingPoint(names, this.stationLineModule.ElementModule.GetPointsNames(), titles);

            MessageInput mi = new MessageInput(MessageType.Choice, R.StringLine_AddNewPointWindow)
            {
                ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL },
                Controls = new List<Control> { tlp }
            };
            List<string> responds;
            using (var result = mi.Show())
            {
                if (result.TextOfButtonClicked == R.T_CANCEL)
                {
                    return;
                }

                responds = new List<string>();
                TableLayoutPanel tplr = (TableLayoutPanel)result.ReturnedControls[0];
                responds.Add(tplr.Controls["Zone/Acc"].Text);
                responds.Add(tplr.Controls["Class"].Text);
                responds.Add(tplr.Controls["Numéro"].Text);
                responds.Add(tplr.Controls["Point"].Text);
            }

            string respond = "";
            if (responds.FindIndex(x => x == "") == -1)
            {
                //Point alesage au format Geode
                respond = string.Join(".", responds);
            }
            else
            {
                if (responds[3] == "" & responds[0] != "" & responds[1] != "" && responds[2] != "")
                {
                    //Point pilier au format Geode
                    respond = string.Join(".", responds);
                }
                else
                {
                    //Autre format
                    respond = string.Join("", responds);
                }

            }
            int index = this.stationLineModule.ElementModule.GetPointsInAllElements().FindIndex(x => x._Name == respond && x._Origin != this.stationLineModule.measPointgroupName);
            if (index != -1)
            {
                if (this.stationLineModule._TheoPoint.FindIndex(x => x._Name == respond) == -1)
                {
                    MessageInput mi1 = new MessageInput(MessageType.Choice, R.StringLine_PointNameAlreadyExistInElementModule)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.T_DONT }
                    };
                    string respond1 = mi1.Show().TextOfButtonClicked;
                    if (respond1 == R.T_OK)
                    {
                        E.Point newPoint = this.stationLineModule.ElementModule.GetPointsInAllElements()[index].DeepCopy();
                        this.stationLineModule.SetNewPointAlreadyExisting(newPoint);
                    }
                    return;
                }
                else
                {
                    new MessageInput(MessageType.Warning, R.StringLine_PointNameAlreadySelected).Show();
                    return;
                };
            }
            if (this.stationLineModule._TheoPoint.FindIndex(x => x._Name == respond) == -1)
            {
                string[] nameSplit;
                char splitChar = '.';
                respond = respond.ToUpper();
                nameSplit = respond.Split(splitChar);
                ///on ne peut pas avoir plus de 15 caractères par champs pour geode
                foreach (string item in responds)
                {
                    if (item.Length > 15)
                    {
                        new MessageInput(MessageType.Warning, R.StringLevel_WrongSocketCode1).Show();
                        return;
                    }
                }
                if ((nameSplit.Length == 1 || (responds[0] != "" && responds[1] != "" && responds[2] != "")) && System.Text.RegularExpressions.Regex.IsMatch(respond, @"^[a-zA-Z0-9+._-]+$"))
                {
                    E.SocketCode.GetSmartCodeFromName(respond, out E.SocketCode socketCode, out string message2);

                    if (socketCode != null)
                    {
                        this.stationLineModule.CreateNewPoint(respond, socketCode);
                        if (this.stationLineModule._TheoPoint.Count > 1)
                        {
                            this.UpdateListMeasure();
                        }
                        else
                        {
                            new MessageInput(MessageType.Warning, R.StringLine_OnlyOnePointForWire).Show();
                        }
                        this.zone = responds[0];
                        this.classe = responds[1];
                        this.number = responds[2];
                        this.pointNewPoint = responds[3];
                        ///incrémentation du point ou du numéro si pas dispo
                        int nb;
                        if (int.TryParse(this.pointNewPoint, out nb))
                        {
                            nb++;
                            this.pointNewPoint = nb.ToString();
                        }
                        else
                        {
                            int n;
                            if (int.TryParse(this.number, out n))
                            {
                                n++;
                                this.number = n.ToString();
                            }
                        }
                    }
                    else
                    {
                        this.ShowMessage(R.StringLine_WrongSocketCode);
                    }
                }
                else
                {
                    this.ShowMessage(R.StringLine_WrongPointName);
                }
            }
            else
            {
                this.ShowMessage(R.StringLine_PointNameAlreadyExist);
            }
        }
        /// <summary>
        /// Affiche la fenêtre pour sélectionner le code
        /// </summary>
        /// <returns></returns>
        private E.SocketCode SelectCode()
        {
            string cancel = R.T_CANCEL;


            BindingSource bindingSource = new BindingSource();
            bindingSource.DataSource = Tsunami2.Preferences.Values.SocketCodes;

            E.SocketCode socketCode = null;
            var respond = this.ShowMessageOfPredefinedInput(
                string.Format("{0};{1}", R.T_SELECT_SOCKET_CODE, R.T_THE_CODE_DEFINE_THE_VERTICAL_EXTENSION_THAT_TSUNAMI_WILL_AUTOMATICALY_SELECTE),

                R.T_OK,
                cancel,
                bindingSource, "FullName", null, false, R.SocketCode);
            if (respond == null) return null;


            if (respond is E.SocketCode sc)
            {
                socketCode = sc;
            }
            else if (respond is string str)
            {
                if (str == cancel) return null;
                int codeId;
                if (int.TryParse(str, out codeId))
                {
                    socketCode = Tsunami2.Preferences.Values.SocketCodes.Find(x => x.Id == codeId.ToString());
                }
            }
            return socketCode;
        }
        /// <summary>
        /// Met à jour l'echelle de la vue instrument
        /// </summary>
        internal void ScaleInstrumentView()
        //Met à jour l'echelle de la vue instrument
        {
            D.View v = this.stationLineModule._InstrumentManager.SelectedInstrumentModule._TsuView as D.View;
            if (v != null)
            {
                float factorScale = Convert.ToSingle(splitContainer1.Panel2.Height) / Convert.ToSingle((v.startingHeight));
                if (!float.IsInfinity(factorScale))
                {
                    v.Width = Convert.ToInt32(v.startingWidth * factorScale);
                    v.Height = splitContainer1.Panel2.Height;
                    (v as D.Manual.Ecartometer.View).ChangeScale(factorScale);
                }
            }


        }
        /// <summary>
        /// hide the manual instrument panel 
        /// </summary>
        internal void HideManualOffsetmeterView()
        {
            if (this.stationLineModule._Station.ParametersBasic._Instrument._Model != "R_AUTO")
            {
                //Hide instrument view except for the RS_auto
                this.splitContainer1.Panel2MinSize = 0;
                this.splitContainer1.SplitterDistance = this.splitContainer1.Size.Height - this.splitContainer1.SplitterWidth;
                //this.splitContainer1.Panel2Collapsed = true;
            }
            else
            {
                this.splitContainer1.Panel2MinSize = 0;
                this.splitContainer1.SplitterDistance = 459;
            }
        }
        /// <summary>
        /// Met à jour l'échelle de la fenêtre instrument lorsqu'on déplace le curseur
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            if (this.stationLineModule != null)
            {
                if (this.stationLineModule.ecartoInstrument != null)
                {
                    this.CheckDatagridIsInEditMode();
                    this.UpdateInstrumentView();
                }
            }
        }

        #endregion
    }
}
}
