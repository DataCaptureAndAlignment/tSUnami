using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using TSU.Common.Compute;
using TSU.Common.Compute.Transformation;
using TSU.Common.Elements;
using TSU.Common.Instruments;
using TSU.Common.Measures;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Line
{
    [XmlType(TypeName = "Line.Station")]
    [Serializable]
    public partial class Station : Common.Station, IEquatable<Station>, ICloneable
    {
        public override CloneableList<Measure> MeasuresTaken
        {
            get
            {
                CloneableList<Measure> l = new CloneableList<Measure>();
                foreach (var item in this.ObsoleteMeasureOfOffset)
                {
                    if (item._Date.Year >= 2000)
                    {
                        item.IsObsolete = true;
                        l.Add(item);
                    }
                }
                l.AddRange(_MeasureOffsets);
                return l;
            }
        }

        [NonSerialized]
        public Sensor sensor;
        public MeasureOfOffset _Anchor1 { get; set; }
        public MeasureOfOffset _Anchor2 { get; set; }
        public bool _showLGC2Files { get; set; }

        public List<MeasureOfOffset> ObsoleteMeasureOfOffset { get; set; } = new List<MeasureOfOffset>();


        CloneableList<MeasureOfOffset> measureOffsets { get; set; }
        public CloneableList<MeasureOfOffset> _MeasureOffsets
        {
            get
            {
                return measureOffsets;
            }
            set
            {
                measureOffsets = value;
            }
        }
        public OffsetStrategy strategy { get; set; }

        [XmlIgnore]
        public Parameters _Parameters
        {
            get
            {
                return this.ParametersBasic as Parameters;
            }
            set
            {
                this.ParametersBasic = value;
            }
        }
        public Station()
        {
            this.sensor = new Sensor();
            this._Parameters = new Parameters();
            _Anchor1 = new MeasureOfOffset();
            _Anchor2 = new MeasureOfOffset();
            _MeasureOffsets = new CloneableList<MeasureOfOffset>();
            this._Name = $"STLINE_{DateTime.Now:yyyyMMdd_HHmmss}_";
            this._Parameters._StationName = this._Name;
            this._Parameters._Station = this;
        }
        public Station(Sensor sensor)
        {
            this.sensor = sensor;
            _Anchor1 = new MeasureOfOffset();
            _Anchor2 = new MeasureOfOffset();
            _MeasureOffsets = new CloneableList<MeasureOfOffset>();
            this._Parameters._Instrument = sensor as OffsetMeter;
        }

        public override string ToString()
        {
            return string.Format(R.TS_Station, this._Name);
        }

        public bool Equals(Station a)
        {
            return this._Anchor1.Equals(a._Anchor1) && this._Anchor2.Equals(a._Anchor2);
        }
        // Default comparer for Measure of level to sort the list.
        public override bool Equals(object other)
        {
            if (other is Station objAsMeas)
                return Equals(objAsMeas);
            else
                return false;
        }
        public new object Clone()
        {
            Station newStationLine = (Station)this.MemberwiseClone(); // to make a clone of the primitive types fields
            newStationLine._Anchor1.DeepCopy();
            newStationLine._Anchor2.DeepCopy();
            newStationLine._MeasureOffsets = (this._MeasureOffsets != null) ? newStationLine._MeasureOffsets.Clone() : null;
            return newStationLine;
        }
        public new Station DeepCopy()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                Station deepCopy = (Station)formatter.Deserialize(stream);
                deepCopy._Parameters._Instrument = this._Parameters._Instrument;
                return deepCopy;
            }
        }
        public override int GetHashCode()
        {
            return this._Anchor1.ToString().GetHashCode() + this._Anchor2.ToString().GetHashCode();
        }
        public Result CCSToMLA(Coordinates.ReferenceSurfaces geoid)
        {
            Result result = new Result();

            double xOri = this._Anchor1._Point._Coordinates.Ccs.X.Value;
            double yOri = this._Anchor1._Point._Coordinates.Ccs.Y.Value;
            double zOri = this._Anchor1._Point._Coordinates.Ccs.Z.Value;
            //TSU.Module.Station.StationLine localStationLine = new TSU.Module.Station.StationLine();
            //Transforme le premier ancrage ( doit normalement donner 0,0,0)
            double xTransform = this._Anchor1._Point._Coordinates.Ccs.X.Value;
            double yTransform = this._Anchor1._Point._Coordinates.Ccs.Y.Value;
            double zTransform = this._Anchor1._Point._Coordinates.Ccs.Z.Value;
            //resMLA = TSU.IO.SUSoft.SpatialObjDLL_32bit.TransformToMLA(xOri, yOri, zOri, ref xTransform, ref yTransform, ref zTransform, geoid);
            bool ok = Systems.Ccs2Mla(xOri, yOri, zOri, ref xTransform, ref yTransform, ref zTransform, geoid);

            if (!ok)
            {
                result.Success = false;
                result.ErrorMessage = R.T393;
                return result;
            }
            var mla1 = this._Anchor1._Point._Coordinates.GetExistingOrCreateOne("MLA", Coordinates.CoordinateSystemsTypes._3DCartesian);
            mla1.X.Value = xTransform;
            mla1.Y.Value = yTransform;
            mla1.Z.Value = zTransform;
            //Transforme le second ancrage 
            var ccs2 = this._Anchor2._Point._Coordinates.GetExistingOrCreateOne("CCS-H", Coordinates.CoordinateSystemsTypes._2DPlusH);
            xTransform = ccs2.X.Value;
            yTransform = ccs2.Y.Value;
            zTransform = ccs2.Z.Value;
            //resMLA = TSU.IO.SUSoft.SpatialObjDLL_32bit.TransformToMLA(xOri, yOri, zOri, ref xTransform, ref yTransform, ref zTransform, geoid);
            ok = Systems.Ccs2Mla(xOri, yOri, zOri, ref xTransform, ref yTransform, ref zTransform, geoid);
            if (!ok)
            {
                result.Success = false;
                result.ErrorMessage = R.T393;
                return result;
            }
            var mla2 = this._Anchor2._Point._Coordinates.GetExistingOrCreateOne("MLA", Coordinates.CoordinateSystemsTypes._3DCartesian);
            mla2.X.Value = xTransform;
            mla2.Y.Value = yTransform;
            mla2.Z.Value = zTransform;
            foreach (MeasureOfOffset pointMesure in this._MeasureOffsets)
            {
                var ccs = pointMesure._Point._Coordinates.GetExistingOrCreateOne("CCS-H", Coordinates.CoordinateSystemsTypes._2DPlusH);
                xTransform = ccs.X.Value;
                yTransform = ccs.Y.Value;
                zTransform = ccs.Z.Value;
                //resMLA = TSU.IO.SUSoft.SpatialObjDLL_32bit.TransformToMLA(xOri, yOri, zOri, ref xTransform, ref yTransform, ref zTransform, geoid);
                ok = Systems.Ccs2Mla(xOri, yOri, zOri, ref xTransform, ref yTransform, ref zTransform, geoid);
                if (!ok)
                {
                    result.Success = false;
                    result.ErrorMessage = R.T394;
                    return result;
                }
                var mla = pointMesure._Point._Coordinates.GetExistingOrCreateOne("MLA", Coordinates.CoordinateSystemsTypes._3DCartesian);
                mla.X.Value = xTransform;
                mla.Y.Value = yTransform;
                mla.Z.Value = zTransform;
            }
            return result;
        }
        public Result UpdateTheoReadingAndDeviation()
        {
            Result result;
            result = this.strategy.WireOffsetCalculation(this, _showLGC2Files);
            return result;
        }
        public void ConvertHToZ(Coordinates.ReferenceSurfaces geoide)
        //convertit le H de chaque point de la station en Z CCS en utilisant le Geoide geoide pour le calcul
        {
            Systems.HtoZ(this._Anchor1._Point, geoide);
            Systems.HtoZ(this._Anchor2._Point, geoide);
            foreach (MeasureOfOffset item in this._MeasureOffsets)
            {
                Systems.HtoZ(item._Point, geoide);
            }
        }

        public void ConvertZtoH(Coordinates.ReferenceSurfaces geoide)
        //convertit le ZCCS de chaque point de la station en H en utilisant le Geoide geoide pour le calcul
        {
            Systems.ZToH(this._Anchor1._Point, geoide);
            Systems.ZToH(this._Anchor2._Point, geoide);
            foreach (MeasureOfOffset item in this._MeasureOffsets)
            {
                Systems.ZToH(item._Point, geoide);
            }
        }
        public void DoReduction()
        //Reduit les mesures d'un fil par rapport � l'ancrage 1
        {
            Survey.DoReduction(this._Anchor1, this._Anchor1);
            Survey.DoReduction(this._Anchor2, this._Anchor1);
            foreach (MeasureOfOffset item in this._MeasureOffsets)
            {
                bool hasCoord = false;
                switch (this._Parameters._CoordType)
                {
                    case ENUM.CoordinatesType.CCS:
                        hasCoord = item._Point._Coordinates.HasCcs;
                        break;
                    case ENUM.CoordinatesType.SU:
                        hasCoord = item._Point._Coordinates.HasSu;
                        break;
                    case ENUM.CoordinatesType.UserDefined:
                        break;
                    default:
                        break;
                }
                if (item._Point.LGCFixOption == ENUM.LgcPointOption.VXY && item._RawReading == na && hasCoord)
                {
                    // pour avoir les lectures theo sur les points sans lecture

                    item._IsFakeRawReading = true;
                    item._RawReading = this._Anchor1._RawReading;
                    Survey.CorrectReadingForEtalonnage(item, (this.ParametersBasic._Instrument as OffsetMeter).EtalonnageParameter, this._Parameters);
                }
                Survey.DoReduction(item, this._Anchor1);
            }
        }
        public void DoAxisTranslation()
        //Translate le syst�me de coordonn�e du syst�me mla vers un syst�me user en utilisant l'ancrage 1 comme origine
        {
            Survey.DoAxisTranslation(this._Anchor1, this._Anchor1);
            Survey.DoAxisTranslation(this._Anchor2, this._Anchor1);
            foreach (MeasureOfOffset item in this._MeasureOffsets)
            {
                Survey.DoAxisTranslation(item, this._Anchor1);
            }
        }
        public void DoAxisRotation()
        //Tourne le syst�me d'axe user pour orienter l'axe Y suivant le fil
        {
            double gisA1A2 = T.Conversions.Angles.Gon2Rad(Survey.GetBearing(this._Anchor1._Point._Coordinates.TempCalculation, this._Anchor2._Point._Coordinates.TempCalculation));
            double distA1A2 = Survey.GetHorizontalDistance(this._Anchor1._Point._Coordinates.TempCalculation, this._Anchor2._Point._Coordinates.TempCalculation);
            double gisFil = gisA1A2;
            if (this._Anchor2._ReductedReading != 0 && distA1A2 != 0)
            {
                gisFil += Math.Asin(this._Anchor2._ReductedReading / distA1A2);
            }

            //Survey.DoAxisRotation(this._Anchor1,-gisFil); anchors are also in the _MeasureOffsets
            //Survey.DoAxisRotation(this._Anchor2,-gisFil);
            foreach (MeasureOfOffset item in this._MeasureOffsets)
            {
                Survey.DoAxisRotation(item, -gisFil);
            }
        }
        /// <summary>
        /// Trie toutes les mesures par decum 
        /// </summary>
        internal void SortMeasures()
        {
            this._MeasureOffsets.Sort();
        }
        /// <summary>
        /// Trie toutes les mesures par inverse decum
        /// </summary>
        internal void ReverseMeasures()
        {
            this._MeasureOffsets.Sort();
            this._MeasureOffsets.Reverse();
        }
        /// <summary>
        /// Trie les mesures du SPS en mettant le sextant 6 avant le sextant 1  
        /// </summary>
        internal void SortMeasForSPSOrigin()
        {
            List<MeasureOfOffset> sextant6;
            sextant6 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("6"));
            sextant6.Sort();
            List<MeasureOfOffset> sextant1;
            sextant1 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("1"));
            sextant1.Sort();
            this._MeasureOffsets.Clear();
            this._MeasureOffsets.AddRange(sextant6);
            this._MeasureOffsets.AddRange(sextant1);
        }
        /// <summary>
        /// Trie les mesures du PS ring dans l'ordre correct pour la partie proche de l'origine  
        /// </summary>
        internal void SortMeasForPROrigin()
        {
            List<MeasureOfOffset> part8;
            part8 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("8"));
            List<MeasureOfOffset> part9;
            part9 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("9"));
            part9.Sort();
            List<MeasureOfOffset> part00;
            part00 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("00"));
            part00.Sort();
            List<MeasureOfOffset> part0;
            part0 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("01")
                                                   || x._Point._Numero.StartsWith("02")
                                                   || x._Point._Numero.StartsWith("03")
                                                   || x._Point._Numero.StartsWith("04")
                                                   || x._Point._Numero.StartsWith("05")
                                                   || x._Point._Numero.StartsWith("06")
                                                   || x._Point._Numero.StartsWith("07")
                                                   || x._Point._Numero.StartsWith("08")
                                                   || x._Point._Numero.StartsWith("09"));
            part0.Sort();
            List<MeasureOfOffset> part1;
            part1 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("1"));
            part1.Sort();
            List<MeasureOfOffset> mergeList = new List<MeasureOfOffset>();
            mergeList.AddRange(part8);
            mergeList.AddRange(part9);
            mergeList.AddRange(part00);
            mergeList.AddRange(part0);
            mergeList.AddRange(part1);
            foreach (MeasureOfOffset item in mergeList)
            {
                this._MeasureOffsets.Remove(item);
            }
            this._MeasureOffsets.AddRange(mergeList);
        }
        /// <summary>
        /// Trie les mesures du Booster ring dans l'ordre correct pour la partie proche de l'origine  
        /// </summary>
        internal void SortMeasForBROrigin()
        {
            List<MeasureOfOffset> part1;
            part1 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("1")
            && !x._Point._Numero.Contains("16")
            && !x._Point._Numero.Contains("15")
            && !x._Point._Numero.Contains("14")
            && !x._Point._Numero.Contains("13")
            && !x._Point._Numero.Contains("12")
            && !x._Point._Numero.Contains("11")
            && !x._Point._Numero.Contains("10"));
            part1.Sort();
            List<MeasureOfOffset> part2;
            part2 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("2"));
            part2.Sort();
            List<MeasureOfOffset> part3;
            part3 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("3"));
            part3.Sort();
            List<MeasureOfOffset> part4;
            part4 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("4"));
            part4.Sort();
            List<MeasureOfOffset> part5;
            part5 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("5"));
            part5.Sort();
            List<MeasureOfOffset> part6;
            part6 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("6"));
            part6.Sort();
            List<MeasureOfOffset> part7;
            part7 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("7"));
            part7.Sort();
            List<MeasureOfOffset> part8;
            part8 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("8"));
            part8.Sort();
            List<MeasureOfOffset> part9;
            part9 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("9"));
            part9.Sort();
            List<MeasureOfOffset> part10;
            part10 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("10"));
            part10.Sort();
            List<MeasureOfOffset> part11;
            part11 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("11"));
            part11.Sort();
            List<MeasureOfOffset> part12;
            part12 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("12"));
            part12.Sort();
            List<MeasureOfOffset> part13;
            part13 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("13"));
            part13.Sort();
            List<MeasureOfOffset> part14;
            part14 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("14"));
            part14.Sort();
            List<MeasureOfOffset> part15;
            part15 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("15"));
            part15.Sort();
            List<MeasureOfOffset> part16;
            part16 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("16"));
            part16.Sort();
            List<MeasureOfOffset> mergeList = new List<MeasureOfOffset>();
            mergeList.AddRange(part8);
            mergeList.AddRange(part9);
            mergeList.AddRange(part10);
            mergeList.AddRange(part11);
            mergeList.AddRange(part12);
            mergeList.AddRange(part13);
            mergeList.AddRange(part14);
            mergeList.AddRange(part15);
            mergeList.AddRange(part16);
            mergeList.AddRange(part1);
            mergeList.AddRange(part2);
            mergeList.AddRange(part3);
            mergeList.AddRange(part4);
            mergeList.AddRange(part5);
            mergeList.AddRange(part6);
            mergeList.AddRange(part7);
            foreach (MeasureOfOffset item in mergeList)
            {
                this._MeasureOffsets.Remove(item);
            }
            this._MeasureOffsets.AddRange(mergeList);
        }
        /// <summary>
        /// Met les points de AD ring dans l'ordre
        /// </summary>
        /// <param name="theoPoint"></param>
        internal void SortMeasForDROrigin()
        {
            List<MeasureOfOffset> part0;
            part0 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("0"));
            part0.Sort();
            List<MeasureOfOffset> part1;
            part1 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("1"));
            part1.Sort();
            List<MeasureOfOffset> part2;
            part2 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("2"));
            part2.Sort();
            List<MeasureOfOffset> part3;
            part3 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("3"));
            part3.Sort();
            List<MeasureOfOffset> part4;
            part4 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("4"));
            part4.Sort();
            List<MeasureOfOffset> part5;
            part5 = this._MeasureOffsets.FindAll(x => x._Point._Numero.StartsWith("5"));
            part5.Sort();
            List<MeasureOfOffset> mergeList = new List<MeasureOfOffset>();
            mergeList.AddRange(part3);
            mergeList.AddRange(part4);
            mergeList.AddRange(part5);
            mergeList.AddRange(part0);
            mergeList.AddRange(part1);
            mergeList.AddRange(part2);
            foreach (MeasureOfOffset item in mergeList)
            {
                this._MeasureOffsets.Remove(item);
            }
            this._MeasureOffsets.AddRange(mergeList);
        }
        /// <summary>
        /// Met � jour les coordonn�es au th�orique pour chaque point mesur�
        /// </summary>
        /// <param name="listTheoPoint"></param>
        internal void UpdateTheoCoordinates(List<Point> listTheoPoint)
        // Met � jour les coordonn�es au th�orique pour chaque point mesur�
        {
            this._Anchor1._Point._Coordinates = listTheoPoint.Find(x => x._Name == this._Anchor1._PointName)._Coordinates.DeepCopy();
            this._Anchor2._Point._Coordinates = listTheoPoint.Find(x => x._Name == this._Anchor2._PointName)._Coordinates.DeepCopy();
            foreach (MeasureOfOffset meas in this._MeasureOffsets)
            {
                meas._Point._Coordinates = listTheoPoint.Find(x => x._Name == meas._PointName)._Coordinates.DeepCopy();
            }
        }
        ///// <summary>
        ///// Remplace les coordonn�es locales par les coordonn�es user defined dans chaque mesures et ancrages
        ///// </summary>
        //internal void ReplaceMlaByUserCoordinates()
        //// Remplace les coordonn�es locales par les coordonn�es user defined dans chaque mesures et ancrages
        //{
        //    this._Anchor1._Point._Coordinates.Mla = this._Anchor1._Point._Coordinates.UserDefined.DeepCopy();
        //    this._Anchor2._Point._Coordinates.Mla = this._Anchor2._Point._Coordinates.UserDefined.DeepCopy();
        //    foreach (MeasureOfOffset meas in this._MeasureOffsets)
        //    {
        //        meas._Point._Coordinates.Mla = meas._Point._Coordinates.UserDefined.DeepCopy();
        //    }
        //}

        /// <summary>
        /// Remet � la valeur par d�faut tous les d�placements et �carts de chaque mesure de la ligne
        /// </summary>
        internal void ResetMeasuresInTheLine()
        {
            foreach (MeasureOfOffset meas in this._MeasureOffsets)
            {
                meas._CorrectedReading = na;
                meas._ReductedReading = na;
                meas._TheoreticalReading = na;
                meas._TheoreticalDeviation = na;
                meas._AverageDeviation = na;
                meas._AverageTheoreticalReading = na;
                meas._IsFakeRawReading = false;
            }
        }
        /// <summary>
        /// Si le calcul LGC n'a pas fonctionn�, met NaN � toutes les mesures
        /// </summary>
        internal void LGCCalculationFailed()
        {
            foreach (MeasureOfOffset meas in this._MeasureOffsets)
            {
                meas._TheoreticalReading = double.NaN;
                meas._TheoreticalDeviation = double.NaN;
                meas._AverageDeviation = double.NaN;
                meas._AverageTheoreticalReading = double.NaN;
                if (meas._IsFakeRawReading)
                {
                    meas._RawReading = na;
                    meas._IsFakeRawReading = false;
                }
            }
        }
    }
}
