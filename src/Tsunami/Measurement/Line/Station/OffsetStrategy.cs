﻿using System;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace TSU.Line
{
    public partial class Station
    {
        [Serializable]
        [XmlInclude(typeof(SFBStrategy))]
        [XmlInclude(typeof(LGC2Strategy))]
        public abstract class OffsetStrategy
        //Strategies calcul fils
        {
            internal string _name { get; set; }
            internal double na => Tsunami2.Preferences.Values.na;

            //internal abstract Result SaveFil(Stations.Offset.Module stationLineModule, bool showFile = false, bool insertOffset = true, bool showMessageOfSuccess = false);
            internal abstract void UpdateDatagridView(Module stationLineModule, DataGridView dataGridViewMesures);
            internal abstract void ChangeAnchorsDataGridView(DataGridView dataGridViewMesures, DataGridViewCellEventArgs e, Module stationLineModule);
            internal abstract Result WireOffsetCalculation(Station stationLine, bool showLGC2Files);
            internal abstract Result UpdateTheoReadingAndDeviation(Average averageStationLine);
            internal abstract Result CalculateWireCoordinates(Module stationLineModule);
            internal abstract void UpdateStationState(Module stationLineModule);
            internal abstract bool CheckBadIsPossible(string pointName, Module stationLineModule);
            internal abstract Result SaveLGCFileWire(Module stationLineModule, bool showMessageOfSuccess = true);
            internal abstract void ReCreateWhatIsNotSerialized(Module stationLineModule);
        }
    }
}