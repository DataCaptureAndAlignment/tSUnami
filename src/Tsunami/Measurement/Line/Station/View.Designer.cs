﻿
using System;
using System.Windows.Forms;
using R = TSU.Properties.Resources;

namespace TSU.Line
{
    public partial class Station
    {
        partial class View
        {
            /// <summary>
            /// Required designer variable.
            /// </summary>
            private System.ComponentModel.IContainer components = null;

            /// <summary>
            /// Clean up any resources being used.
            /// </summary>
            /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }

            #region Windows Form Designer generated code

            /// <summary>
            /// Required method for Designer support - do not modify
            /// the contents of this method with the code editor.
            /// </summary>
            private void InitializeComponent()
            {
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
                this.tableLayoutPanel_Top = new System.Windows.Forms.TableLayoutPanel();

                this.splitContainer1 = new System.Windows.Forms.SplitContainer();
                TSU.Tsunami2.Preferences.Theme.ApplyTo(splitContainer1);

                this.splitContainerFil = new System.Windows.Forms.SplitContainer();
                TSU.Tsunami2.Preferences.Theme.ApplyTo(splitContainer1);

                this.treeView_Parameters = new System.Windows.Forms.TreeView();
                this.dataGridViewMesures = new System.Windows.Forms.DataGridView();
                this.DeleteRow = new System.Windows.Forms.DataGridViewImageColumn();
                this.Nom = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.Use = new System.Windows.Forms.DataGridViewImageColumn();
                this.Dcum = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.RawMeas = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.CorrectedMeas = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.Theorique = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.moveAverage = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.Objective = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.Move = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this._PanelBottom.SuspendLayout();
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
                this.splitContainer1.Panel1.SuspendLayout();
                this.splitContainer1.SuspendLayout();
                ((System.ComponentModel.ISupportInitialize)(this.splitContainerFil)).BeginInit();
                this.splitContainerFil.Panel1.SuspendLayout();
                this.splitContainerFil.Panel2.SuspendLayout();
                this.splitContainerFil.SuspendLayout();
                ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMesures)).BeginInit();
                this.SuspendLayout();
                // 
                // instrumentView
                // 
                this.instrumentView.Location = new System.Drawing.Point(130, 130);
                // 
                // _PanelBottom
                // 
                this._PanelBottom.Controls.Add(this.splitContainer1);
                this._PanelBottom.Location = new System.Drawing.Point(5, 80);
                this._PanelBottom.Size = new System.Drawing.Size(1206, 595);
                // 
                // _PanelTop
                // 
                this._PanelTop.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
                this._PanelTop.Size = new System.Drawing.Size(1206, 75);
                this._PanelTop.Resize += new System.EventHandler(this._PanelTop_Resize);
                // 
                // tableLayoutPanel_Top
                // 
                this.tableLayoutPanel_Top.ColumnCount = 2;
                this.tableLayoutPanel_Top.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
                this.tableLayoutPanel_Top.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
                this.tableLayoutPanel_Top.Dock = System.Windows.Forms.DockStyle.Fill;
                this.tableLayoutPanel_Top.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
                this.tableLayoutPanel_Top.Location = new System.Drawing.Point(0, 0);
                this.tableLayoutPanel_Top.Name = "tableLayoutPanel_Top";
                this.tableLayoutPanel_Top.RowCount = 1;
                this.tableLayoutPanel_Top.RowStyles.Add(new System.Windows.Forms.RowStyle());
                this.tableLayoutPanel_Top.Size = new System.Drawing.Size(1202, 75);
                this.tableLayoutPanel_Top.TabIndex = 0;
                // 
                // splitContainer1
                // 
                this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
                this.splitContainer1.Location = new System.Drawing.Point(0, 0);
                this.splitContainer1.Name = "splitContainer1";
                this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
                // 
                // splitContainer1.Panel1
                // 
                this.splitContainer1.Panel1.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
                this.splitContainer1.Panel1.Controls.Add(this.splitContainerFil);
                this.splitContainer1.Size = new System.Drawing.Size(1206, 595);
                this.splitContainer1.SplitterDistance = 459;
                this.splitContainer1.TabIndex = 0;
                this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
                // 
                // splitContainerFil
                // 
                this.splitContainerFil.Dock = System.Windows.Forms.DockStyle.Fill;
                this.splitContainerFil.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
                this.splitContainerFil.Location = new System.Drawing.Point(0, 0);
                this.splitContainerFil.Name = "splitContainerFil";
                this.splitContainerFil.Orientation = System.Windows.Forms.Orientation.Horizontal;
                // 
                // splitContainerFil.Panel1
                // 
                this.splitContainerFil.Panel1.Controls.Add(this.treeView_Parameters);
                // 
                // splitContainerFil.Panel2
                // 
                this.splitContainerFil.Panel2.Controls.Add(this.dataGridViewMesures);
                this.splitContainerFil.Size = new System.Drawing.Size(1206, 459);
                this.splitContainerFil.SplitterDistance = 232;
                this.splitContainerFil.TabIndex = 0;
                // 
                // treeView_Parameters
                // 
                this.treeView_Parameters.AllowDrop = true;
                this.treeView_Parameters.Dock = System.Windows.Forms.DockStyle.Fill;
                this.treeView_Parameters.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.treeView_Parameters.ItemHeight = 32;
                this.treeView_Parameters.Location = new System.Drawing.Point(0, 0);
                this.treeView_Parameters.Name = "treeView_Parameters";
                this.treeView_Parameters.ShowNodeToolTips = true;
                this.treeView_Parameters.Size = new System.Drawing.Size(1206, 232);
                this.treeView_Parameters.TabIndex = 0;
                this.treeView_Parameters.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView_Parameters_NodeMouseClick);
                // 
                // dataGridViewMesures
                // 
                this.dataGridViewMesures.AllowUserToAddRows = false;
                this.dataGridViewMesures.AllowUserToDeleteRows = false;
                this.dataGridViewMesures.AllowUserToResizeColumns = true;
                this.dataGridViewMesures.AllowUserToResizeRows = false;
                this.dataGridViewMesures.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
                dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                dataGridViewCellStyle8.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.DarkBackground;
                dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dataGridViewCellStyle8.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
                dataGridViewCellStyle8.SelectionBackColor = TSU.Tsunami2.Preferences.Theme.Colors.TextHighLightBack;
                dataGridViewCellStyle8.SelectionForeColor = TSU.Tsunami2.Preferences.Theme.Colors.TextHighLightFore;
                dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                this.dataGridViewMesures.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
                this.dataGridViewMesures.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
                this.dataGridViewMesures.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DeleteRow,
            this.Nom,
            this.Use,
            this.Dcum,
            this.RawMeas,
            this.CorrectedMeas,
            this.Theorique,
            this.moveAverage,
            this.Objective,
            this.Move});
                this.dataGridViewMesures.Dock = System.Windows.Forms.DockStyle.Fill;
                this.dataGridViewMesures.EnableHeadersVisualStyles = false;
                this.dataGridViewMesures.GridColor = TSU.Tsunami2.Preferences.Theme.Colors.Object;
                this.dataGridViewMesures.Location = new System.Drawing.Point(0, 0);
                this.dataGridViewMesures.MultiSelect = false;
                this.dataGridViewMesures.Name = "dataGridViewMesures";
                this.dataGridViewMesures.RowHeadersVisible = false;
                this.dataGridViewMesures.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                this.dataGridViewMesures.ShowCellErrors = false;
                this.dataGridViewMesures.ShowEditingIcon = false;
                this.dataGridViewMesures.ShowRowErrors = false;
                this.dataGridViewMesures.Size = new System.Drawing.Size(1206, 223);
                this.dataGridViewMesures.TabIndex = 15;
                this.dataGridViewMesures.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewMeasure_CellClick);
                this.dataGridViewMesures.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGridViewMeasure_CellBeginEdit);
                this.dataGridViewMesures.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewMesures_CellValueChanged);
                this.dataGridViewMesures.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewMesures_ColumnHeaderMouseClick);
                this.dataGridViewMesures.CurrentCellChanged += new System.EventHandler(this.dataGridViewMesures_CurrentCellChanged);
                this.dataGridViewMesures.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridViewMesures_RowsAdded);
                this.dataGridViewMesures.MouseEnter += new System.EventHandler(this.dataGridViewMesures_MouseEnter);
                this.dataGridViewMesures.CellDoubleClick += DataGridViewMesures_CellDoubleClick;
                // 
                // DeleteRow
                // 
                this.DeleteRow.HeaderText = "";
                this.DeleteRow.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
                this.DeleteRow.Name = "DeleteRow";
                this.DeleteRow.Resizable = System.Windows.Forms.DataGridViewTriState.False;
                this.DeleteRow.Width = 30;
                // 
                // Nom
                // 
                dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                this.Nom.DefaultCellStyle = dataGridViewCellStyle9;
                this.Nom.HeaderText = "Nom";
                this.Nom.Name = "Nom";
                this.Nom.ReadOnly = true;
                this.Nom.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                this.Nom.Width = 250;
                this.Nom.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
                // 
                // Use
                // 
                this.Use.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Use;
                this.Use.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
                this.Use.MinimumWidth = 40;
                this.Use.Name = "Use";
                this.Use.ReadOnly = true;
                this.Use.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                this.Use.Width = 40;
                this.Use.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
                // 
                // Dcum
                // 
                dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.Dcum.DefaultCellStyle = dataGridViewCellStyle10;
                this.Dcum.HeaderText = "Dcum (m)";
                this.Dcum.Name = "Dcum";
                this.Dcum.ReadOnly = true;
                this.Dcum.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
                // 
                // RawMeas
                // 
                dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.RawMeas.DefaultCellStyle = dataGridViewCellStyle11;
                this.RawMeas.HeaderText = "Mes Brute (mm)";
                this.RawMeas.Name = "RawMeas";
                this.RawMeas.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
                // 
                // CorrectedMeas
                // 
                dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.CorrectedMeas.DefaultCellStyle = dataGridViewCellStyle12;
                this.CorrectedMeas.HeaderText = " Mes corr (mm)";
                this.CorrectedMeas.Name = "CorrectedMeas";
                this.CorrectedMeas.ReadOnly = true;
                this.CorrectedMeas.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
                // 
                // Theorique
                // 
                dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.Theorique.DefaultCellStyle = dataGridViewCellStyle13;
                this.Theorique.HeaderText = "Theo Moy (mm)";
                this.Theorique.Name = "Theorique";
                this.Theorique.ReadOnly = true;
                this.Theorique.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                this.Theorique.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
                // 
                // moveAverage
                // 
                dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.moveAverage.DefaultCellStyle = dataGridViewCellStyle14;
                this.moveAverage.HeaderText = "Offset (mm)";
                this.moveAverage.Name = "moveAverage";
                this.moveAverage.ReadOnly = true;
                this.moveAverage.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                this.moveAverage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
                // 
                // Objective
                // 
                dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.Objective.DefaultCellStyle = dataGridViewCellStyle15;
                this.Objective.HeaderText = "Objective (mm)";
                this.Objective.Name = "Objective";
                this.Objective.ReadOnly = true;
                this.Objective.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                this.Objective.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
                // 
                // Move
                // 
                dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.Move.DefaultCellStyle = dataGridViewCellStyle16;
                this.Move.HeaderText = "Move (mm)";
                this.Move.Name = "Move";
                this.Move.ReadOnly = true;
                this.Move.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                this.Move.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
                // 
                // StationLineModuleView
                // 
                this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
                this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
                this.AutoSize = true;
                this.ClientSize = new System.Drawing.Size(1216, 680);
                this.Name = "StationLineModuleView";
                this.Text = "OffsetModuleView";
                this.Controls.SetChildIndex(this._PanelTop, 0);
                this.Controls.SetChildIndex(this._PanelBottom, 0);
                this._PanelBottom.ResumeLayout(false);
                this.splitContainer1.Panel1.ResumeLayout(false);
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
                this.splitContainer1.ResumeLayout(false);
                this.splitContainerFil.Panel1.ResumeLayout(false);
                this.splitContainerFil.Panel2.ResumeLayout(false);
                ((System.ComponentModel.ISupportInitialize)(this.splitContainerFil)).EndInit();
                this.splitContainerFil.ResumeLayout(false);
                ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMesures)).EndInit();
                this.ResumeLayout(false);

            }

           

            #endregion

            private System.Windows.Forms.SplitContainer splitContainer1;
            private System.Windows.Forms.SplitContainer splitContainerFil;
            internal System.Windows.Forms.DataGridView dataGridViewMesures;
            private System.Windows.Forms.TreeView treeView_Parameters;
            private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_Top;
            private System.Windows.Forms.DataGridViewImageColumn DeleteRow;
            private System.Windows.Forms.DataGridViewTextBoxColumn Nom;
            private System.Windows.Forms.DataGridViewImageColumn Use;
            private System.Windows.Forms.DataGridViewTextBoxColumn Dcum;
            private System.Windows.Forms.DataGridViewTextBoxColumn RawMeas;
            private System.Windows.Forms.DataGridViewTextBoxColumn CorrectedMeas;
            private System.Windows.Forms.DataGridViewTextBoxColumn Theorique;
            private System.Windows.Forms.DataGridViewTextBoxColumn moveAverage;
            private System.Windows.Forms.DataGridViewTextBoxColumn Objective;
            private System.Windows.Forms.DataGridViewTextBoxColumn Move;
        }
    }
}