﻿using System;
using System.Xml.Serialization;
using TSU.Common.Instruments;

namespace TSU.Line
{
    public partial class Station
    {
        [Serializable]
        [XmlType(TypeName = "Stations.Line.Parameters")]
        public class Parameters : Common.Station.Parameters
        //pour les fils
        {
            /// <summary>
            /// tolérance pour l'alignement des éléments
            /// </summary>
            public virtual double _Tolerance { get; set; }

            public virtual string _Comment { get; set; }

            /// <summary>
            /// type de coordonnée à utiliser pour le calcul
            /// </summary>
            public virtual ENUM.CoordinatesType _CoordType { get; set; }

            /// <summary>
            /// température de mesure
            /// </summary>
            public virtual double _Temperature { get; set; }

            [XmlIgnore]
            public new OffsetMeter _Instrument
            {
                get => base._Instrument as OffsetMeter;
                set => base._Instrument = value;
            }

            public Parameters()
                : base()
            //Constructeurs
            {
                this._Date = DateTime.Now;
                this._Team = base._Team;
                this._Operation = base._Operation;
                this._IsSetup = false;
                this._Instrument = new OffsetMeter();
                this._State = new State.Opening();
                this._Tolerance = Tsunami2.Preferences.Values.Tolerances.Alignment_Radial_mm / 1000;
                this._Comment = "";
                this._Instrument = new OffsetMeter();
                this._GeodeDatFilePath = "";
                this._Temperature = Tsunami2.Preferences.Values.GuiPrefs.ReferenceEcartoTemperature;
            }
        }
    }
}
