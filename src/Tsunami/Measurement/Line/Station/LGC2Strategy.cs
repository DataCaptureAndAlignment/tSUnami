﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.Json;
using System.Windows.Forms;
using System.Xml.Serialization;
using TSU.Common.Compute;
using TSU.Common.Compute.Compensations;
using TSU.Common.Measures;
using TSU.ENUM;
using TSU.Views.Message;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Line
{
    public partial class Station
    {
        #region offset compute strategy
        [Serializable]

        [XmlType("LGC2OffsetStrategy")]
        public class LGC2Strategy : OffsetStrategy
        {
            public LGC2Strategy()
            {
                this._name = "LGC2";
            }

            internal override void UpdateDatagridView(Module stationLineModule, DataGridView dataGridViewMesures)
            //Met à jour le datagridView en fonction du calcul des offsets
            {
                Average averageStationLine = stationLineModule.WorkingAverageStationLine;
                dataGridViewMesures.Rows.Clear();
                DataGridUtility.SaveSorting(dataGridViewMesures);
                //Ne garde que les 9 premières colonnes du datagridview
                int j = dataGridViewMesures.Columns.Count;
                for (int i = 10; i < j; i++)
                {
                    dataGridViewMesures.Columns.RemoveAt(10);
                }
                List<List<object>> listRow = new List<List<object>>();
                DataGridViewCellStyle cellStyle = new DataGridViewCellStyle();
                cellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                DataGridViewCellStyle cellStyleComment = new DataGridViewCellStyle();
                cellStyleComment.Alignment = DataGridViewContentAlignment.MiddleLeft;
                //ajoute la colonne calage
                DataGridViewImageColumn columnCala = new DataGridViewImageColumn();
                columnCala.HeaderText = R.StringLine_DataGrid_Fix;
                columnCala.Name = "Fixed_Fil";
                columnCala.DefaultCellStyle = cellStyle;
                columnCala.ValuesAreIcons = false;
                columnCala.ImageLayout = DataGridViewImageCellLayout.Stretch;
                columnCala.Width = 30;
                columnCala.ReadOnly = true;
                dataGridViewMesures.Columns.Add(columnCala);
                //ajoute les colonnes pour définir les extrémités du fil
                DataGridViewImageColumn columnAnc1 = new DataGridViewImageColumn();
                columnAnc1.HeaderText = R.StringLine_DataGrid_E1;
                columnAnc1.Name = "Anc1_Fil0";
                columnAnc1.ValuesAreIcons = false;
                columnAnc1.ImageLayout = DataGridViewImageCellLayout.Stretch;
                columnAnc1.Width = 30;
                columnAnc1.DefaultCellStyle = cellStyle;
                columnAnc1.ReadOnly = true;
                dataGridViewMesures.Columns.Add(columnAnc1);
                DataGridViewImageColumn columnAnc2 = new DataGridViewImageColumn();
                columnAnc2.HeaderText = R.StringLine_DataGrid_E2;
                columnAnc2.Name = "Anc2_Fil0";
                columnAnc2.DefaultCellStyle = cellStyle;
                columnAnc2.ValuesAreIcons = false;
                columnAnc2.ImageLayout = DataGridViewImageCellLayout.Stretch;
                columnAnc2.Width = 30;
                columnAnc2.ReadOnly = true;
                dataGridViewMesures.Columns.Add(columnAnc2);
                //ajoute la colonne commentaire à la fin
                DataGridViewTextBoxColumn colonneCommentaire = new DataGridViewTextBoxColumn();
                colonneCommentaire.DefaultCellStyle = cellStyleComment;
                colonneCommentaire.HeaderText = R.StringLine_DataGrid_Comment;
                colonneCommentaire.Name = "commentaire";
                colonneCommentaire.MaxInputLength = 150;
                colonneCommentaire.Width = 300;
                colonneCommentaire.ReadOnly = false;
                colonneCommentaire.Resizable = DataGridViewTriState.True;
                colonneCommentaire.SortMode = DataGridViewColumnSortMode.Automatic;
                dataGridViewMesures.Columns.Add(colonneCommentaire);
                //ajoute la colonne interfaces à la fin
                DataGridViewTextBoxColumn colonneInterfaces = new DataGridViewTextBoxColumn();
                colonneInterfaces.DefaultCellStyle = cellStyleComment;
                colonneInterfaces.HeaderText = "Interfaces";
                colonneInterfaces.Name = "Interfaces";
                colonneInterfaces.MaxInputLength = 150;
                colonneInterfaces.Width = 150;
                colonneInterfaces.ReadOnly = false;
                colonneInterfaces.Resizable = DataGridViewTriState.True;
                dataGridViewMesures.Columns.Add(colonneInterfaces);
                //ajoute une ligne en entête blanche
                listRow.Add(this.AddBlankLine());
                //ajoute toutes les mesures dans le tableau ligne par ligne
                int indexOfMeasureOfOffset = 0;
                if ((averageStationLine._ListStationLine[0]._MeasureOffsets.FindIndex(x => x._Point._Accelerator == "SPS" && (x._Point._Class == "MBA" || x._Point._Class == "MBB")) != -1)
                    && (stationLineModule.sortOnlyDcum == false))
                {
                    stationLineModule.SortMeasure();
                }
                foreach (M.MeasureOfOffset measureOfOffset in averageStationLine._ListStationLine[0]._MeasureOffsets)
                {
                    ///Pour les dipoles SPS
                    if (measureOfOffset._Point._Accelerator == "SPS" && (measureOfOffset._Point._Class == "MBA" || measureOfOffset._Point._Class == "MBB"))
                    {
                        int index = averageStationLine._ListStationLine[0]._MeasureOffsets.FindIndex(x => x._PointName == measureOfOffset._PointName);
                        string prevMagnet = "";
                        string nextMagnet = "";
                        string actualMagnet = measureOfOffset._Point._Numero.Substring(3, 2) + measureOfOffset._Point._Point;
                        if (index > 0)
                        {
                            prevMagnet = averageStationLine._ListStationLine[0]._MeasureOffsets[index - 1]._Point._Numero.Substring(3, 2) + averageStationLine._ListStationLine[0]._MeasureOffsets[index - 1]._Point._Point;
                        }
                        if (index < averageStationLine._ListStationLine[0]._MeasureOffsets.Count - 1)
                        {
                            nextMagnet = averageStationLine._ListStationLine[0]._MeasureOffsets[index + 1]._Point._Numero.Substring(3, 2) + averageStationLine._ListStationLine[0]._MeasureOffsets[index + 1]._Point._Point;
                        }
                        if (((prevMagnet == "30S" && actualMagnet == "50E")
                            || (prevMagnet == "50S" && actualMagnet == "70E")
                            || (prevMagnet == "70S" && actualMagnet == "90E"))
                            && stationLineModule.sortDirection == SortDirection.Dcum)
                        {
                            listRow.Add(this.AddBlankLine());
                            indexOfMeasureOfOffset++;
                            if (measureOfOffset._RawReading != na && averageStationLine._ListStationLine[0]._MeasureOffsets[index - 1]._RawReading != na
                                && !(measureOfOffset._Status is M.States.Bad) && !(averageStationLine._ListStationLine[0]._MeasureOffsets[index - 1]._Status is M.States.Bad))
                            {
                                if (stationLineModule.calculationToDo == true && stationLineModule.CheckIfTheoPointHasCoordinate(measureOfOffset._Point) && stationLineModule.CheckIfTheoPointHasCoordinate(averageStationLine._ListStationLine[0]._MeasureOffsets[index - 1]._Point))
                                {
                                    double average = (-measureOfOffset._AverageDeviation - averageStationLine._ListStationLine[0]._MeasureOffsets[index - 1]._AverageDeviation) / 2;
                                    listRow[indexOfMeasureOfOffset][7] = Math.Round(average * 1000
                                        , 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                                }
                            }
                        }
                        if (((prevMagnet == "90E" && actualMagnet == "70S")
                            || (prevMagnet == "70E" && actualMagnet == "50S")
                            || (prevMagnet == "50E" && actualMagnet == "30S"))
                            && stationLineModule.sortDirection == SortDirection.InvDcum)
                        {
                            listRow.Add(this.AddBlankLine());
                            indexOfMeasureOfOffset++;
                            if (measureOfOffset._RawReading != na && averageStationLine._ListStationLine[0]._MeasureOffsets[index - 1]._RawReading != na
                                && !(measureOfOffset._Status is M.States.Bad) && !(averageStationLine._ListStationLine[0]._MeasureOffsets[index - 1]._Status is M.States.Bad))
                            {
                                if (stationLineModule.calculationToDo == true && stationLineModule.CheckIfTheoPointHasCoordinate(measureOfOffset._Point) && stationLineModule.CheckIfTheoPointHasCoordinate(averageStationLine._ListStationLine[0]._MeasureOffsets[index - 1]._Point))
                                {
                                    double average = (-measureOfOffset._AverageDeviation - averageStationLine._ListStationLine[0]._MeasureOffsets[index - 1]._AverageDeviation) / 2;
                                    listRow[indexOfMeasureOfOffset][7] = Math.Round(average * 1000
                                        , 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                                }
                            }
                        }
                    }
                    indexOfMeasureOfOffset++;
                    List<object> row = new List<object>();
                    //Colonne 0 : delete
                    row.Add(R.Delete_Ancrage);
                    //Colonne 1 : Name
                    row.Add("");
                    //colonne 2 : checkbox use meas
                    row.Add(R.CheckedBoxDatagridView);
                    //Colonne 3 : Dcum
                    row.Add(null);
                    //Colonne 4 : raw Meas
                    row.Add(null);
                    //Colonne 5 : corr Meas
                    row.Add(null);
                    //Colonne 6 : avg theo
                    row.Add(null);
                    //Colonne 7 : offset
                    row.Add(null);
                    //Colonne 7 : objective
                    row.Add(null);
                    //Colonne 9 : move avg
                    row.Add(null);
                    //Colonne 10 calage
                    row.Add(R.Ancrage_Fil_Vide);
                    //Colonne 11 : Extremite 1
                    row.Add(R.Ancrage_Fil_Vide);
                    //Colonne 12 : Extremite 2
                    row.Add(R.Ancrage_Fil_Vide);
                    //colonne 13 commentaire
                    row.Add("");
                    //colonne 14 interfaces
                    row.Add("");
                    listRow.Add(row);
                    listRow[indexOfMeasureOfOffset][0] = R.Delete_Ancrage;
                    listRow[indexOfMeasureOfOffset][1] = measureOfOffset._PointName;
                    //Ajoute la Dcum
                    listRow[indexOfMeasureOfOffset][3] = (measureOfOffset._Point._Parameters.Cumul == Tsunami2.Preferences.Values.na) ? "" : measureOfOffset._Point._Parameters.Cumul.ToString("F3", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                    if (measureOfOffset._RawReading != na)
                    {
                        listRow[indexOfMeasureOfOffset][4] = Math.Round(measureOfOffset._RawReading * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                        listRow[indexOfMeasureOfOffset][5] = Math.Round(measureOfOffset._CorrectedReading * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                        if (stationLineModule.calculationToDo == true && stationLineModule.CheckIfTheoPointHasCoordinate(measureOfOffset._Point) && !(measureOfOffset._Status is M.States.Bad))
                        {
                            ///La lecture theo à lire sur la règle est la lecture brute - l'écart théorique, cette formule est approchés car elle ne tient pas compte de la différence d'étalonnage entre l'ancienne lecture brute et la nouvelle
                            if (measureOfOffset._AverageTheoreticalReading != na)
                            {
                                //listRow[indexOfMeasureOfOffset][5] = Math.Round((measureOfOffset._RawReading - measureOfOffset._AverageDeviation) * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                                Survey.CalculOffsetRawReading(measureOfOffset, (stationLineModule.WorkingAverageStationLine.ParametersBasic._Instrument as I.OffsetMeter).EtalonnageParameter, stationLineModule.WorkingAverageStationLine._Parameters);
                                listRow[indexOfMeasureOfOffset][6] = Math.Round(measureOfOffset._AverageTheoreticalReading * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                            }
                            if (measureOfOffset._AverageDeviation != na)
                            {
                                listRow[indexOfMeasureOfOffset][7] = Math.Round(measureOfOffset._AverageDeviation * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));

                                if (measureOfOffset._Point.LGCFixOption != LgcPointOption.CALA
                                    && stationLineModule.FinalModule.RabotStrategy != Common.Compute.Rabot.Strategy.Not_Defined
                                    && stationLineModule.FinalModule.RadialRabot.ContainsKey(measureOfOffset._PointName))
                                {
                                    if (stationLineModule.FinalModule.RabotStrategy == Common.Compute.Rabot.Strategy.Expected_Offset)
                                    {
                                        listRow[indexOfMeasureOfOffset][8] = Math.Round(stationLineModule.FinalModule.RadialRabot[measureOfOffset._PointName].Smooth.Value, 2).ToString("F2", CultureInfo.CreateSpecificCulture("en-US"));
                                        listRow[indexOfMeasureOfOffset][9] = Math.Round(stationLineModule.FinalModule.RadialRabot[measureOfOffset._PointName].Smooth.Value - measureOfOffset._AverageDeviation * 1000, 2).ToString("F2", CultureInfo.CreateSpecificCulture("en-US"));
                                    }
                                    else
                                    {
                                        double expO = Tsunami2.Preferences.Values.na;
                                        double move = Tsunami2.Preferences.Values.na;
                                        double firstOffset = Tsunami2.Preferences.Values.na;
                                        double firstMove = Tsunami2.Preferences.Values.na;
                                        List<M.Measure> pointMeasureList = stationLineModule.FinalModule._ActiveStationModule._Station.MeasuresTaken
                                                    .GetList()
                                                    .Where(measure => measure._PointName == measureOfOffset._PointName && !(measure._Status.Type == M.States.Types.Bad && !measure.IsObsolete))
                                                    .ToList();

                                        if (pointMeasureList.Count > 0)
                                        {
                                            if(pointMeasureList.Count == 1)
                                            {
                                                move = -1 * stationLineModule.FinalModule.RadialRabot[measureOfOffset._PointName].RoughMinusSmooth.Value;
                                                expO = move + measureOfOffset._AverageDeviation * 1000;
                                            }
                                            else
                                            {
                                                MeasureOfOffset firstMeasure = pointMeasureList.FirstOrDefault() as MeasureOfOffset;
                                                firstOffset = firstMeasure._AverageDeviation * 1000;
                                                expO = firstOffset - stationLineModule.FinalModule.RadialRabot[measureOfOffset._PointName].RoughMinusSmooth.Value;
                                                move = expO - measureOfOffset._AverageDeviation * 1000;
                                            }
                                        }

                                        listRow[indexOfMeasureOfOffset][8] = Math.Round(expO, 2)
                                            .ToString("F2", CultureInfo.CreateSpecificCulture("en-US"));
                                        listRow[indexOfMeasureOfOffset][9] = Math.Round(move, 2)
                                            .ToString("F2", CultureInfo.CreateSpecificCulture("en-US"));
                                    }
                                }
                                else
                                {
                                    listRow[indexOfMeasureOfOffset][8] = "";
                                    listRow[indexOfMeasureOfOffset][9] = "";
                                }
                            }
                        }
                    }
                    else
                    {
                        if (measureOfOffset._CorrectedReading != na && stationLineModule.calculationToDo == true && stationLineModule.CheckIfTheoPointHasCoordinate(measureOfOffset._Point) && !(measureOfOffset._Status is M.States.Bad))
                        {
                            ///La lecture theo à lire sur la règle est la lecture brute - l'écart théorique, cette formule est approchés car elle ne tient pas compte de la différence d'étalonnage entre l'ancienne lecture brute et la nouvelle
                            if (measureOfOffset._AverageTheoreticalReading != na)
                            {
                                //listRow[indexOfMeasureOfOffset][5] = Math.Round((averageStationLine._ListStationLine[0]._Anchor1._RawReading - measureOfOffset._AverageDeviation) * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                                Survey.CalculOffsetRawReading(measureOfOffset, (stationLineModule.WorkingAverageStationLine.ParametersBasic._Instrument as I.OffsetMeter).EtalonnageParameter, stationLineModule.WorkingAverageStationLine._Parameters);
                                listRow[indexOfMeasureOfOffset][6] = Math.Round(measureOfOffset._AverageTheoreticalReading * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                            }
                        }
                    }
                    listRow[indexOfMeasureOfOffset][10] = R.Ancrage_Fil_Vide;
                    listRow[indexOfMeasureOfOffset][13] = measureOfOffset.CommentFromUser;
                    listRow[indexOfMeasureOfOffset][14] = measureOfOffset.Interfaces._Name;
                    if (measureOfOffset._Point.LGCFixOption == LgcPointOption.CALA)
                    {
                        listRow[indexOfMeasureOfOffset][10] = R.I_Lock;
                    }
                    //Indique dans le datagridview que c'est un point d'ancrage
                    if (measureOfOffset._PointName == averageStationLine._ListStationLine[0]._Anchor1._PointName)
                    {
                        listRow[indexOfMeasureOfOffset][11] = R.Extremite_1;
                    }
                    if (measureOfOffset._PointName == averageStationLine._ListStationLine[0]._Anchor2._PointName)
                    {
                        listRow[indexOfMeasureOfOffset][12] = R.Extremite_2;
                    }
                    if (measureOfOffset._Status is M.States.Bad)
                    {
                        listRow[indexOfMeasureOfOffset][2] = R.UncheckedBoxDatagridView;
                    }
                }
                foreach (List<object> ligne in listRow)
                {
                    var array = ligne.ToArray();
                    //System.Collections.IList list = dataGridViewMesures.Columns;
                    //for (int i = 0; i < list.Count; i++)
                    //{
                    //    DataGridViewColumn col = (DataGridViewColumn)list[i];
                    //    //if (col.CellType is )

                    //}
                    //if (dataGridViewMesures.Columns.Count != array.Length)
                    //{

                    //}
                    dataGridViewMesures.Rows.Add(array);
                }
                dataGridViewMesures.Rows[0].Visible = false;
                if (stationLineModule.FinalModule.RabotStrategy == Common.Compute.Rabot.Strategy.Not_Defined)
                {
                    dataGridViewMesures.Columns[8].Visible = false;
                    dataGridViewMesures.Columns[9].Visible = false;
                }
                else
                {
                    dataGridViewMesures.Columns[8].Visible = true;
                    dataGridViewMesures.Columns[9].Visible = true;
                }
                for (int i = 1; i < dataGridViewMesures.RowCount; i++)
                {
                    double p;
                    if (dataGridViewMesures[7, i].Value != null)
                    {
                        p = T.Conversions.Numbers.ToDouble(dataGridViewMesures[7, i].Value.ToString(), false, -9999);
                    }
                    else
                    {
                        p = -9999;
                    }

                    //met en gris les lignes si c'est un point de calage
                    if ((dataGridViewMesures[10, i].Value as System.Drawing.Bitmap).Width == 128)
                    {
                        dataGridViewMesures[1, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewMesures[2, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewMesures[3, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewMesures[4, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewMesures[5, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewMesures[6, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewMesures[7, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewMesures[8, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewMesures[9, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                    }
                    ///Temporairement n'affiche plus les couleurs de tolérance en module guidé
                    //met en rouge le texte si le déplacement moyen est > à la tolérance d'alignement
                    if (p != -9999 && stationLineModule.View.moduleType == ModuleType.Advanced)
                    {
                        if (Math.Abs(p) > averageStationLine._Parameters._Tolerance * 1000)
                        {
                            dataGridViewMesures[7, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Bad;
                        }
                        else
                        {
                            if (!double.IsNaN(p)) dataGridViewMesures[7, i].Style.ForeColor = Tsunami2.Preferences.Theme.Colors.Good;
                        }
                    }
                    // Ligne moyenne points ancrage pour dipole SPS
                    //Met en jaune la colonne raw meas si pas de mesure encodée
                    if (dataGridViewMesures[1, i].Value.ToString() != "")
                    {
                        dataGridViewMesures[4, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Action;
                    }
                    if (dataGridViewMesures[1, i].Value.ToString() == "")
                    {
                        System.Drawing.Color c = Tsunami2.Preferences.Theme.Colors.Highlight;
                        dataGridViewMesures[4, i].ReadOnly = true;
                        dataGridViewMesures[dataGridViewMesures.ColumnCount - 1, i].ReadOnly = true;
                        dataGridViewMesures[0, i].Style.BackColor = c;
                        dataGridViewMesures[1, i].Value = R.StringLine_Datagrid_AverageInterconnection;
                        dataGridViewMesures[1, i].Style.BackColor = c;
                        dataGridViewMesures[2, i].Style.BackColor = c;
                        dataGridViewMesures[3, i].Style.BackColor = c;
                        dataGridViewMesures[4, i].Style.BackColor = c;
                        dataGridViewMesures[5, i].Style.BackColor = c;
                        dataGridViewMesures[6, i].Style.BackColor = c;
                        dataGridViewMesures[7, i].Style.BackColor = c;
                        dataGridViewMesures[8, i].Style.BackColor = c;
                        dataGridViewMesures[9, i].Style.BackColor = c;
                        dataGridViewMesures[10, i].Style.BackColor = c;
                        dataGridViewMesures[11, i].Style.BackColor = c;
                        dataGridViewMesures[12, i].Style.BackColor = c;
                        dataGridViewMesures[13, i].Style.BackColor = c;
                        dataGridViewMesures[14, i].Style.BackColor = c;
                        ///Temporairement n'affiche plus les couleurs de tolérance en module guidé
                        if (p != -9999 && stationLineModule.View.moduleType == ModuleType.Advanced)
                        {
                            //Met en rouge le fond de la ligne d'interconnection si écart > tolerance
                            if (Math.Abs(p) > averageStationLine._Parameters._Tolerance * 1000)
                            {
                                dataGridViewMesures[7, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Bad;
                            }
                            else
                            //Met en vert le texte de la ligne d'interconnection si écart < tolerance
                            {
                                if (!double.IsNaN(p)) dataGridViewMesures[7, i].Style.ForeColor = Tsunami2.Preferences.Theme.Colors.Good;
                            }
                        }
                    }
                }
                //Force le tri des mesures par Dcum si dipôle SPS
                if (averageStationLine._ListStationLine[0]._MeasureOffsets.FindIndex(x => x._Point._Accelerator == "SPS" && (x._Point._Class == "MBA" || x._Point._Class == "MBB")) != -1)
                {
                    foreach (var col in dataGridViewMesures.Columns)
                    {
                        if (col is DataGridViewTextBoxColumn)
                        {
                            (col as DataGridViewTextBoxColumn).SortMode = DataGridViewColumnSortMode.NotSortable;
                        }
                    }
                    stationLineModule.sortOnlyDcum = true;
                }
                else
                {
                    foreach (var col in dataGridViewMesures.Columns)
                    {
                        if (col is DataGridViewTextBoxColumn)
                        {
                            //if ((col as DataGridViewTextBoxColumn).Name == "commentaire" 
                            //    || (col as DataGridViewTextBoxColumn).Name == "Nom")
                            //{
                            //    (col as DataGridViewTextBoxColumn).SortMode = DataGridViewColumnSortMode.Automatic;
                            //}
                            //else
                            //{
                            //    (col as DataGridViewTextBoxColumn).SortMode = DataGridViewColumnSortMode.NotSortable;
                            //}
                            if ((col as DataGridViewTextBoxColumn).Name == "Dcum")
                            {
                                (col as DataGridViewTextBoxColumn).SortMode = DataGridViewColumnSortMode.NotSortable;
                            }
                            else
                            {
                                (col as DataGridViewTextBoxColumn).SortMode = DataGridViewColumnSortMode.Automatic;
                            }
                        }
                    }
                    stationLineModule.sortOnlyDcum = false;
                    if (averageStationLine._ListStationLine[0]._MeasureOffsets.FindIndex(x => x._Point._Accelerator == "SPS" && (x._Point._Class == "MBA" || x._Point._Class == "MBB")) == -1)
                    {
                        DataGridUtility.RestoreSorting(dataGridViewMesures);
                    }
                }
                dataGridViewMesures.Columns[0].Visible = true;
                ////Cache la colonne pour effacer les points dans le module guidé
                //switch (stationLineModule.View.moduleType)
                //{
                //    case ModuleType.Guided:
                //        dataGridViewMesures.Columns[0].Visible = false;
                //        break;
                //    case ModuleType.Advanced:
                //        dataGridViewMesures.Columns[0].Visible = true;
                //        break;
                //    default:
                //        dataGridViewMesures.Columns[0].Visible = true;
                //        break;
                //}
                //Met en read only la premiere ligne du tableau
                dataGridViewMesures.Rows[0].ReadOnly = true;
            }
            /// <summary>
            /// Ajoute une ligne blanche dans le tableau
            /// </summary>
            private List<object> AddBlankLine()
            {
                List<object> rowBlank = new List<object>();
                //Colonne 0 : delete
                rowBlank.Add(R.Interconnection_Dipole_Vide);
                //Colonne 1 : Name
                rowBlank.Add("");
                //Colonne 2 : Use
                rowBlank.Add(R.Interconnection_Dipole_Vide);
                //Colonne 3 : Dcum
                rowBlank.Add(null);
                //Colonne 4 : raw Meas
                rowBlank.Add(null);
                //Colonne 5 : corr Meas
                rowBlank.Add(null);
                //Colonne 6 : avg theo
                rowBlank.Add(null);
                //Colonne 7 : offset
                rowBlank.Add(null);
                //Colonne 8 : expected offset
                rowBlank.Add(null);
                //Colonne 9 : move avg
                rowBlank.Add(null);
                //Colonne 10 calage
                rowBlank.Add(R.Interconnection_Dipole_Vide);
                //Colonne 11 : Extremite 1
                rowBlank.Add(R.Interconnection_Dipole_Vide);
                //Colonne 12 : Extremite 2
                rowBlank.Add(R.Interconnection_Dipole_Vide);
                //colonne 13 commentaire
                rowBlank.Add("");
                //colonne 14 interfaces
                rowBlank.Add("");
                return rowBlank;
            }
            internal override void ChangeAnchorsDataGridView(DataGridView dataGridViewMesures, DataGridViewCellEventArgs e, Module stationLineModule)
            //Ajoute un ancrage si on double clique sur cellule ancrage
            {
                string nomFil;
                int numeroFil;
                int ancrage1ou2 = 0;
                ///Vérifie que la mesure associée n'est pas en status bad
                M.MeasureOfOffset meas = stationLineModule.WorkingAverageStationLine._ListStationLine[0]._MeasureOffsets.Find(x => x._PointName == dataGridViewMesures[1, e.RowIndex].Value.ToString());
                if (meas != null)
                {
                    if (e.ColumnIndex != 8 && meas._Status is M.States.Bad)
                    {
                        new MessageInput(MessageType.Warning, R.StringLine_CannotBeExtremity).Show();
                        return;
                    }
                }
                if (dataGridViewMesures.Columns[e.ColumnIndex].HeaderText == R.StringLine_DataGrid_E1)
                {
                    System.Drawing.Bitmap image = dataGridViewMesures[e.ColumnIndex + 1, e.RowIndex].Value as System.Drawing.Bitmap;
                    //Astuce image ancrage vide fait 65 de large, permet de voir si ancrage 1 = ancrage2
                    if (image.Width == 65)
                    {
                        ancrage1ou2 = 1;
                        nomFil = dataGridViewMesures.Columns[e.ColumnIndex].Name.ToString();
                        nomFil = nomFil.Remove(0, 8);
                        int.TryParse(nomFil, out numeroFil);
                        stationLineModule.AddAnchor(dataGridViewMesures[1, e.RowIndex].Value.ToString(), numeroFil, ancrage1ou2);
                    }

                }
                if (dataGridViewMesures.Columns[e.ColumnIndex].HeaderText == R.StringLine_DataGrid_E2)
                {
                    System.Drawing.Bitmap image = dataGridViewMesures[e.ColumnIndex - 1, e.RowIndex].Value as System.Drawing.Bitmap;
                    //Astuce image ancrage vide fait 65 de large, permet de voir si ancrage 1 = ancrage2
                    if (image.Width == 65)
                    {
                        ancrage1ou2 = 2;
                        nomFil = dataGridViewMesures.Columns[e.ColumnIndex].Name.ToString();
                        nomFil = nomFil.Remove(0, 8);
                        int.TryParse(nomFil, out numeroFil);
                        stationLineModule.AddAnchor(dataGridViewMesures[1, e.RowIndex].Value.ToString(), numeroFil, ancrage1ou2);
                    }
                }
                if (dataGridViewMesures.Columns[e.ColumnIndex].HeaderText == R.StringLine_DataGrid_Fix)
                {
                    System.Drawing.Bitmap image = dataGridViewMesures[e.ColumnIndex, e.RowIndex].Value as System.Drawing.Bitmap;
                    //Astuce image ancrage vide fait 65 de large, permet de voir si ancrage 1 = ancrage2
                    if (image.Width == 65) //point libre devient point cala
                    {
                        stationLineModule.ChangeLgcPointOption(dataGridViewMesures[1, e.RowIndex].Value.ToString(), LgcPointOption.CALA);
                    }
                    else
                    //point cala devient point VXY
                    {
                        stationLineModule.ChangeLgcPointOption(dataGridViewMesures[1, e.RowIndex].Value.ToString(), LgcPointOption.VXY);
                    }
                }
                if (stationLineModule.WorkingAverageStationLine.ReName())
                {
                    stationLineModule.AddElementsToElementModule();
                    if (stationLineModule.FinalModule is Line.Module) (stationLineModule.FinalModule as Line.Module).View.SetStationNameInTopButton(stationLineModule);
                    if (stationLineModule.FinalModule.ParentModule is Common.Guided.Group.Module) (stationLineModule.FinalModule.ParentModule as Common.Guided.Group.Module).UpdateBigButtonTop();
                }
            }
            internal override Result WireOffsetCalculation(Station stationLine, bool showLGC2Files = false)
            //Calcule la lecture theo et l'écart theo pour chaque point d'un fil avec décalage sur les points d'ancrage en utilisant LGC2.
            {

                Result result = new Result();
                stationLine.DoReduction();
                stationLine.DoAxisTranslation();
                stationLine.DoAxisRotation();
                Lgc2 lgc2 = new Lgc2(stationLine);
                //no more show the LGC calculation to calculate the offset

                lgc2.Run(false, false);
                if (lgc2._Runned == true)
                {
                    string fileName = lgc2._FileName + ".JSON";// "C:\\Users\\malgorza\\cernbox\\WINDOWS\\Desktop\\JSON\\Wire.json";
                    JsonElement jsonFile = Lgc2.Output.ParserJSON.ReadJSONFile(fileName);
                    Lgc2.Output.OutputJSON.GetOffsetResultsForWire(jsonFile, stationLine);
                    result.Success = true;
                }
                else
                {
                    ///Si calcul LGC a échoué, remet les raw reading à -9999
                    foreach (M.MeasureOfOffset item in stationLine._MeasureOffsets)
                    {
                        if (item._IsFakeRawReading)
                        {
                            item._RawReading = na;
                            item._IsFakeRawReading = false;
                        }
                    }
                    stationLine.LGCCalculationFailed();
                    result.Success = false;
                }
                return result;
            }
            internal override Result UpdateTheoReadingAndDeviation(Average averageStationLine)
            //Calcul de l'écart au théorique avec LGC2 pour le premier fil et met à jour les écarts sur les autres fils avec les mêmes valeurs
            {
                Result result = new Result();
                result = averageStationLine._ListStationLine[0].UpdateTheoReadingAndDeviation();
                if (result.Success == false)
                {
                    averageStationLine.ParametersBasic._State = new State.LGC2CalculationFailed();
                    return result;
                }
                foreach (Station line in averageStationLine._ListStationLine)
                {
                    line._Anchor1._AverageDeviation = averageStationLine._ListStationLine[0]._Anchor1._AverageDeviation;
                    line._Anchor1._AverageTheoreticalReading = averageStationLine._ListStationLine[0]._Anchor1._AverageTheoreticalReading;
                    line._Anchor1._TheoreticalDeviation = averageStationLine._ListStationLine[0]._Anchor1._TheoreticalDeviation;
                    line._Anchor1._TheoreticalReading = averageStationLine._ListStationLine[0]._Anchor1._TheoreticalReading;
                    line._Anchor2._AverageDeviation = averageStationLine._ListStationLine[0]._Anchor2._AverageDeviation;
                    line._Anchor2._AverageTheoreticalReading = averageStationLine._ListStationLine[0]._Anchor2._AverageTheoreticalReading;
                    line._Anchor2._TheoreticalDeviation = averageStationLine._ListStationLine[0]._Anchor2._TheoreticalDeviation;
                    line._Anchor2._TheoreticalReading = averageStationLine._ListStationLine[0]._Anchor2._TheoreticalReading;
                    int index = 0;
                    foreach (M.MeasureOfOffset measure in line._MeasureOffsets)
                    {
                        measure._AverageDeviation = averageStationLine._ListStationLine[0]._MeasureOffsets[index]._AverageDeviation;
                        measure._AverageTheoreticalReading = averageStationLine._ListStationLine[0]._MeasureOffsets[index]._AverageTheoreticalReading;
                        measure._TheoreticalDeviation = averageStationLine._ListStationLine[0]._MeasureOffsets[index]._TheoreticalDeviation;
                        measure._TheoreticalReading = averageStationLine._ListStationLine[0]._MeasureOffsets[index]._TheoreticalReading;
                        measure._AverageDeviation = averageStationLine._ListStationLine[0]._MeasureOffsets[index]._AverageDeviation;
                        measure._AverageTheoreticalReading = averageStationLine._ListStationLine[0]._MeasureOffsets[index]._AverageTheoreticalReading;
                        measure._TheoreticalDeviation = averageStationLine._ListStationLine[0]._MeasureOffsets[index]._TheoreticalDeviation;
                        measure._TheoreticalReading = averageStationLine._ListStationLine[0]._MeasureOffsets[index]._TheoreticalReading;
                        index++;
                    }

                }
                averageStationLine.ParametersBasic._State = new State.LGC2CalculationSuccessfull();
                return result;
            }
            internal override Result CalculateWireCoordinates(Module stationLineModule)
            {
                Result result = new Result();
                Lgc2 lgc2 = new Lgc2(stationLineModule);
                // let decide if show the LGC calculation 
                lgc2.Run(stationLineModule.WorkingAverageStationLine._showLGC2Files, stationLineModule.WorkingAverageStationLine._showLGC2Files);
                if (lgc2._Runned == true)
                {
                    lgc2._Output.GetCoordinatesResultsForWire(stationLineModule);
                    result.Success = true;
                }
                else
                {
                    result.Success = false;
                }
                return result;
            }
            /// <summary>
            /// met à jour le status de la station si le calcul n'est pas possible
            /// </summary>
            /// <param name="stationLineModule"></param>
            internal override void UpdateStationState(Module stationLineModule)
            {
                stationLineModule.WorkingAverageStationLine._Parameters._State = new State.LGC2CalculationFailed();
            }
            /// <summary>
            /// Vérifie que le status bad peut-être appliqué à la mesure
            /// </summary>
            /// <param name="pointName"></param>
            /// <returns></returns>
            internal override bool CheckBadIsPossible(string pointName, Module stationLineModule)
            {
                Station line = stationLineModule.WorkingAverageStationLine._ListStationLine[0];
                M.MeasureOfOffset meas = line._MeasureOffsets.Find(x => x._PointName == pointName);
                if (meas != null)
                {
                    if (meas._PointName == line._Anchor1._PointName || meas._PointName == line._Anchor2._PointName) { return false; }
                    if (meas._Point.LGCFixOption != LgcPointOption.CALA || meas._Status is M.States.Bad) { return true; }
                    if (line._MeasureOffsets.FindAll(x => (x._Point.LGCFixOption == LgcPointOption.CALA) & !(x._Status is M.States.Bad)).Count > 2) { return true; }
                }
                return false;
            }
            /// <summary>
            /// Export les mesures du fil au format LGC2 pour les expériences
            /// </summary>
            /// <param name="stationLineModule"></param>
            /// <param name="showMessageOfSuccess"></param>
            /// <returns></returns>
            internal override Result SaveLGCFileWire(Module stationLineModule, bool showMessageOfSuccess = true)
            {
                Result result = new Result();
                string filepath = string.Format("{0}{1}_{2}_{3}_{4}.inp", Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, stationLineModule.WorkingAverageStationLine._Name, DateTime.Now.Day.ToString("00"), DateTime.Now.Month.ToString("00"), (DateTime.Now.Year - 2000).ToString());
                if (!System.IO.Directory.Exists(Tsunami2.Preferences.Values.Paths.MeasureOfTheDay)) { System.IO.Directory.CreateDirectory(Tsunami2.Preferences.Values.Paths.MeasureOfTheDay); }
                if (System.IO.File.Exists(filepath))
                {
                    string respond = R.T_OK;
                    if (stationLineModule.View.moduleType == ModuleType.Advanced)
                    {
                        string titleAndMessage = string.Format(R.StringLine_LGCReplaceFile, filepath);
                        MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                        {
                            ButtonTexts = new List<string> { R.T_OK, R.T_DONT }
                        };
                        respond = mi.Show().TextOfButtonClicked;
                    }
                    if (respond == R.T_DONT)
                    {
                        result.Success = false;
                        return result;
                    }
                }
                try
                {
                    Lgc2 lgc2 = new Lgc2(stationLineModule, filepath);
                    if (System.IO.File.Exists(filepath))
                    {
                        result.Success = true;
                        stationLineModule.WorkingAverageStationLine.ParametersBasic._LGCSaveFilePath = filepath;
                    }
                    else
                    {
                        result.Success = false;
                        result.ErrorMessage = R.StringLine_LGCInpNotCreated;
                        stationLineModule.WorkingAverageStationLine._Parameters._State = new State.WireReadyToBeSaved();
                        string titleAndMessage = string.Format(R.StringLine_LGCInpNotCreated, filepath);
                        new MessageInput(MessageType.Critical, titleAndMessage).Show();
                    }
                    return result;
                }
                catch (Exception e)
                //Exception générée si fichier inexistant
                {
                    result.Success = false;
                    result.ErrorMessage = e.Message;
                    e.Data.Clear();
                    return result;
                }
            }
            /// <summary>
            /// Met la bonne stratégie pour l'affichage du datagridview dans la vue
            /// </summary>
            /// <param name="stationLineModule"></param>
            internal override void ReCreateWhatIsNotSerialized(Module stationLineModule)
            {
                if (stationLineModule.View != null)
                {
                    stationLineModule.View.computeStrategy = new LGC2Strategy();
                    stationLineModule.View.HideManualOffsetmeterView();
                    stationLineModule.View.UpdateListMeasure();
                }
                stationLineModule.WorkingAverageStationLine._strategy = new LGC2Strategy();
                foreach (Station line in stationLineModule.WorkingAverageStationLine._ListStationLine)
                {
                    line.strategy = new LGC2Strategy();
                }

            }
            #endregion
        }

    }
}
