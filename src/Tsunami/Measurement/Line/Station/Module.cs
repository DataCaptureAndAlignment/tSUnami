using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Shapes;
using System.Xml.Serialization;
using TSU.Common.Compute;
using TSU.Common.Instruments.Device;
using TSU.Common.Measures;
using TSU.ENUM;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using EC = TSU.Common.Elements.Composites;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;

namespace TSU.Line
{
    public partial class Station
    {
       
        [Serializable]
        [XmlType(TypeName = "Stations.Offset.Module")]
        public class Module : Common.Station.Module
        {
            #region  Fields & Props

            public OffsetStrategy _ComputeStrategy { get; set; }

            [XmlIgnore]
            internal Average WorkingAverageStationLine
            {
                get => this._Station as Average;
                set => this._Station = value;
            }

            public List<Average> AverageStations
            {
                get;
                set;
            }

            [XmlIgnore]
            internal new View View
            {
                get => this._TsuView as View;
                set => this._TsuView = value;
            }

            [XmlIgnore]
            public override ReadOnlyCollection<M.Measure> MeasuresReceived
            {
                get
                {
                    var list = new List<M.Measure>();
                    return new ReadOnlyCollection<M.Measure>(list);
                }
            }

            public List<E.Point> _TheoPoint { get; set; }

            public List<E.Point> _MeasPoint { get; set; }

            public EC.SequenceFil _SequenceSelected { get; set; }

            public I.OffsetMeter ecartoInstrument { get; set; }

            public bool calculationToDo { get; set; }

            public string measPointgroupName { get; set; }

            public TsuBool ShowSaveMessageOfSuccess { get; set; }

            private bool showMsgCorrectWireExtremities = true;

            public bool sortOnlyDcum = false;

            #endregion

            #region  Constructors & init

            public Module()
                : base()
            {
            }
            public Module(Common.FinalModule module, bool createView = true)
                : base(module, string.Format("{0};{1}", R.T_STATION_MODULE_LINE, R.T_MODULE_MANAGING_A_LINE), createView)
            {
            }

            internal override void ReCreateWhatIsNotSerialized(bool saveSomeMemory = false)
            {
                try
                {
                    this.WorkingAverageStationLine.ParametersBasic.LastSaved = DateTime.MinValue;
                    this.WorkingAverageStationLine.ParametersBasic.LastChanged = DateTime.Now;
                    //Dans le cas ou l'on ouvre une sauvegarde, l'op s�lectionn�e n'est plus coch�e
                    O.Operation oldOp = (this.FinalModule.OperationManager.AllElements.Find(x => x._Name == this.WorkingAverageStationLine.ParametersBasic._Operation._Name) as O.Operation);
                    if (oldOp != null)
                    {
                        this.FinalModule.OperationManager._SelectedObjectInBlue = oldOp;
                        this.FinalModule.OperationManager.AddSelectedObjects(oldOp);
                        this.FinalModule.OperationManager.UpdateView();
                    }
                    this._ComputeStrategy.ReCreateWhatIsNotSerialized(this);
                    base.ReCreateWhatIsNotSerialized(saveSomeMemory);
                    ///Reselection du bon instrument dans l'instrument manager apr�s r�ouverture fichier tsu
                    if (this.WorkingAverageStationLine.ParametersBasic._Instrument.Id != null)
                    {
                        var stp = this.WorkingAverageStationLine.ParametersBasic;

                        I.Instrument oldInstrument = this._InstrumentManager.AllElements.OfType<I.Instrument>().FirstOrDefault(
                                   inst => inst._SerialNumber == stp._Instrument._SerialNumber
                                   && (inst._Model == stp._Instrument._Model));
                        I.Sensor i = oldInstrument as I.Sensor;

                        this._InstrumentManager._SelectedObjects.Clear();
                        this._InstrumentManager._SelectedObjectInBlue = i;
                        this._InstrumentManager._SelectedObjects.Add(i);
                        //this._InstrumentManager.ValidateSelection();
                        if (this.View != null) this.View.HideManualOffsetmeterView();
                    }
                    this.CheckIfReadyToBeSaved();
                }
                catch (Exception ex)
                {
                    throw new Exception($"Cannot re-create what was not serialized in {this._Name}", ex);
                }
            }

            /// <summary>
            /// This method is called by the base class "Module" when this object is created
            /// </summary>
            public override void Initialize()
            {
                this.Utility = "A--B";
                this.ShowSaveMessageOfSuccess = new TsuBool(false);
                this.WorkingAverageStationLine = new Average();
                this.AverageStations = new List<Average> { this.WorkingAverageStationLine };
                this._ComputeStrategy = new LGC2Strategy();
                this._TheoPoint = new List<E.Point>();
                this._MeasPoint = new List<E.Point>();
                this._SequenceSelected = new EC.SequenceFil();
                base.Initialize(); // this will add a instrumetn Manager but with no instrument in the list, need to call _InstrumentManager.LoadInstruments()
                measPointgroupName = "";
                calculationToDo = true;
                this.LoadInstrumentTypes();
            }


            /// <summary>
            /// Copie l'instrument du oldStationLineModule vers l'actuel stationLineModule
            /// </summary>
            /// <param name="oldStationLineModule"></param>
            internal void SetInstrumentInNewStationLineModule(Module oldStationLineModule)
            {
                TsuObject selectedInstrument = this._InstrumentManager.AllElements.Find(x => x._Name == oldStationLineModule._Station.ParametersBasic._Instrument._Name);
                this._InstrumentManager._SelectedObjects.Clear();
                this._InstrumentManager._SelectedObjects.Add(selectedInstrument);
                this._InstrumentManager._SelectedObjectInBlue = selectedInstrument;
                this._InstrumentManager.ValidateSelection();
                this.View?.HideManualOffsetmeterView();
            }
            /// <summary>
            /// Set the Geode file path from the old station lline Module into the new station level module
            /// </summary>
            /// <param name="oldStationModule"></param>
            internal void SetGeodeFilePathInNewStationLineModule(Module oldStationLineModule)
            {
                this.WorkingAverageStationLine.ParametersBasic._GeodeDatFilePath = oldStationLineModule.WorkingAverageStationLine.ParametersBasic._GeodeDatFilePath;
                this.ShowSaveMessageOfSuccess = oldStationLineModule.ShowSaveMessageOfSuccess;
            }
            #endregion

            #region Selection

            internal override void OnInstrumentChanged(I.Sensor i = null)
            {
                base.OnInstrumentChanged(i);
                this.View?.UpdateInstrumentView();
            }

            internal void ChangeTeam(string newTeam)
            //change l'�quipe dans chaque station line
            {
                this.WorkingAverageStationLine.ParametersBasic._Team = newTeam;
                this.WorkingAverageStationLine.ParametersBasic.LastChanged = DateTime.Now;
                this.CheckIfReadyToBeSaved();

                Tsunami2.Preferences.Values.GuiPrefs.AddLastTeamtUsed(newTeam);
            }

            /// <summary>
            ///  Change le num�ro d'op�ration dans chaque station line
            /// </summary>
            /// <param name="o"></param>
            internal override void ChangeOperationID(O.Operation o)
            {
                if (this.WorkingAverageStationLine == null) return;
                this.WorkingAverageStationLine.ParametersBasic._Operation = o;
                this.WorkingAverageStationLine._Parameters._Operation = o;
                foreach (Station item in this.WorkingAverageStationLine._ListStationLine)
                {
                    item.ParametersBasic._Operation = o;
                    item._Parameters._Operation = o;
                }
                this.WorkingAverageStationLine.ParametersBasic.LastChanged = DateTime.Now;
                this.CheckIfReadyToBeSaved();

                Tsunami2.Preferences.Values.GuiPrefs.LastOpUsed = o;
            }
            /// <summary>
            /// Change le commentaire de la mesure
            /// </summary>
            /// <param name="measLevel"></param>
            internal void ChangeComment(string pointName, string newComment)
            //change le commentaire
            {
                //newComment = newComment.ToUpper();
                M.MeasureOfOffset measOffset = this.WorkingAverageStationLine._ListStationLine[0]._MeasureOffsets.Find(x => x._PointName == pointName);
                measOffset.CommentFromUser = newComment;
                this.WorkingAverageStationLine.ParametersBasic.LastChanged = DateTime.Now;
                this.UpdateOffsets(measOffset);
            }

            /// <summary>
            /// ask the view to show interface choices and then trigger or not the interface cahnge in the module
            /// </summary>
            /// <param name="measure"></param>
            internal void ChangeInterfaces(string pointName)
            {
                var current = this.WorkingAverageStationLine.MeasuresTaken.Find(x => x._Point._Name == pointName);
                this.View.SelectInterfaces(
                    previousInterFaces: current.Interfaces,
                    currenObject: current,
                    model: null,
                    fan: null,
                    additionalAction: UpdateView);
            }

            /// <summary>
            /// Replace the interface in a meaure
            /// </summary>
            /// <param name="currenObject"></param>
            /// <param name="newInterFaces"></param>
            internal override void ChangeInterfacesBy(TsuObject currenObject, Interfaces newInterFaces)
            {
                var measure = currenObject as M.MeasureOfOffset;
                measure.Interfaces = newInterFaces;
                this.UpdateOffsets(measure);
            }

            internal void ChangeLgcPointOption(string pointName, LgcPointOption typeCala)
            {

                int numeroMesure = this.WorkingAverageStationLine._ListStationLine[0]._MeasureOffsets.FindIndex(x => x._PointName == pointName);
                if (numeroMesure != -1)
                {
                    foreach (Station line in this.WorkingAverageStationLine._ListStationLine)
                    {
                        //Permet de garder au moins 2 points en cala
                        if (typeCala == LgcPointOption.VXY)
                        {
                            //Autorisation d'avoir moins que 2 points en CALA 17-01-2020
                            //if (line._MeasureOffsets.FindAll(x => x._Point.LGCFixOption == ENUM.LgcPointOption.CALA).Count > 2)
                            //{
                            line._MeasureOffsets[numeroMesure]._Point.LGCFixOption = typeCala;
                            //}
                            //else
                            //{
                            //    //si on essaye d'effacer les 2 derniers points en cala, message d'erreur, mais seulement 1 fois affich�
                            //    if (!errorMessageShown)
                            //    {
                            //        this.View.ShowMessage(R.StringLine_ErrorAnchor);
                            //        errorMessageShown = true;
                            //    }
                            //}
                        }
                        if (typeCala == LgcPointOption.CALA)
                        {
                            line._MeasureOffsets[numeroMesure]._Point.LGCFixOption = typeCala;
                        }
                    }
                    ///Remet cala ou VXY au point theo pour la s�lection des r�f�rences et points VXY dans les modules guid�s.
                    int indexPtTheo = this._TheoPoint.FindIndex(x => x._Name == pointName);
                    if (indexPtTheo != -1)
                    {
                        this._TheoPoint[indexPtTheo].LGCFixOption = this.WorkingAverageStationLine._ListStationLine[0]._MeasureOffsets[numeroMesure]._Point.LGCFixOption;
                        //met le point modifi� dans la bonne liste de points dans le final module
                        switch (this._TheoPoint[indexPtTheo].LGCFixOption)
                        {
                            case LgcPointOption.CALA:
                                this.FinalModule.CalaPoints.Add(this._TheoPoint[indexPtTheo]);
                                int index1 = this.FinalModule.PointsToBeAligned.FindIndex(x => x._Name == this._TheoPoint[indexPtTheo]._Name);
                                if (index1 != -1)
                                {
                                    this.FinalModule.PointsToBeAligned.RemoveAt(index1);
                                }
                                break;
                            case LgcPointOption.VXY:
                                this.FinalModule.PointsToBeAligned.Add(this._TheoPoint[indexPtTheo]);
                                int index2 = this.FinalModule.CalaPoints.FindIndex(x => x._Name == this._TheoPoint[indexPtTheo]._Name);
                                if (index2 != -1)
                                {
                                    this.FinalModule.CalaPoints.RemoveAt(index2);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    this.WorkingAverageStationLine.ParametersBasic.LastChanged = DateTime.Now;
                    this.UpdateOffsets();
                }
            }
            /// <summary>
            /// Change la tol�rance d'alignement en m
            /// </summary>
            /// <param name="newTolerance"></param>
            internal void ChangeTolerance(double newTolerance)
            {
                this.WorkingAverageStationLine._Parameters._Tolerance = Math.Abs(newTolerance);
            }
            /// <summary>
            /// Changement de la temperature ambiante
            /// </summary>
            /// <param name="newTemperature"></param>
            internal void ChangeTemperature(double newTemperature)
            {
                this.WorkingAverageStationLine._Parameters._Temperature = newTemperature;
                this.WorkingAverageStationLine.ParametersBasic.LastChanged = DateTime.Now;
                foreach (Station item in this.WorkingAverageStationLine._ListStationLine)
                {
                    item._Parameters._Temperature = newTemperature;
                }
                this.UpdateOffsets();
            }
            internal void SelectElements()
            //Selectionne des �lements dans un fichier theo
            {
                this.SetAllPointsInElementManager();
                this.ElementModule.View.buttons.HideOnlyButtons(this.ElementModule.View.buttons.sequence);
                // ouvre la fen�tre du module �lement de s�lection des points et n'utilise que la liste selectable definie avant
                this.ElementModule.SelectPoints(null, R.T_VALID, R.T_CANCEL, selectOnlyPointsForWire: true);
                ///Montre le message indiquant de s�lectionner les extr�mit�s correctes.
                if (this._ComputeStrategy is LGC2Strategy && this.showMsgCorrectWireExtremities)
                {
                    new MessageInput(MessageType.GoodNews, R.StringLine_SelectCorrectExtremities).Show();
                    this.showMsgCorrectWireExtremities = false;
                }
                //La fonction SetPointsToMeasure est lanc�e automatique par la fonction OnNext dans le final Module
            }
            /// <summary>
            /// Set the actual theoretical points in the element manager
            /// </summary>
            internal void SetAllPointsInElementManager()
            {
                ///Coche les points d�j� s�lectionn� dans l'element module
                //this.ElementModule.SetSelectableToAllUniquePoint();
                this.ElementModule.SetSelectableToAllUniquePointForAdvanceWire();
                this.ElementModule._SelectedObjects.Clear();
                if (this._TheoPoint != null && this.ElementModule.SelectableObjects != null)
                {
                    //List<TsuObject> selected = new List<TsuObject>();
                    foreach (E.Point item in this._TheoPoint)
                    {
                        TsuObject objectFound = this.ElementModule.SelectableObjects.Find(x => x is E.Point p && p._Name == item._Name && p._Origin == item._Origin);
                        if (objectFound != null)
                        {
                            this.ElementModule.AddSelectedObjects(objectFound);
                        }
                    }
                }
            }
            /// <summary>
            /// Set the actual VXY theoretical points in the element manager
            /// </summary>
            internal void SetVXYPointsInElementManager()
            {
                ///Coche les points d�j� s�lectionn� dans l'element module
                //this.ElementModule.SetSelectableToAllUniquePointForAdvanceWire(); ;
                this.ElementModule._SelectedObjects.Clear();
                if (this._TheoPoint != null && this.ElementModule.SelectableObjects != null)
                {
                    //List<TsuObject> selected = new List<TsuObject>();
                    //List<TsuObject> toRemove = new List<TsuObject>();
                    foreach (E.Point item in this._TheoPoint)
                    {
                        TsuObject objectToRemoveFound = this.ElementModule.SelectableObjects.Find(x => x is E.Point p && p._Name == item._Name && p._Origin == item._Origin && (item.LGCFixOption == LgcPointOption.CALA));
                        if (objectToRemoveFound != null)
                        {
                            this.ElementModule.SelectableObjects.Remove(objectToRemoveFound);
                        }
                        TsuObject objectToSelectFound = this.ElementModule.SelectableObjects.Find(x => x is E.Point p && p._Name == item._Name && p._Origin == item._Origin && (item.LGCFixOption == LgcPointOption.VXY));
                        if (objectToSelectFound != null)
                        {
                            this.ElementModule.AddSelectedObjects(objectToSelectFound);
                        }
                    }
                }
            }
            /// <summary>
            /// Set the actual Cala theoretical points in the element manager
            /// </summary>
            internal void SetCalaPointsInElementManager()
            {
                ///Coche les points d�j� s�lectionn� dans l'element module
                //this.ElementModule.SetSelectableToAllUniquePointForAdvanceWire();
                this.ElementModule._SelectedObjects.Clear();
                if (this._TheoPoint != null && this.ElementModule.SelectableObjects != null)
                {
                    //List<TsuObject> selected = new List<TsuObject>();
                    foreach (E.Point item in this._TheoPoint)
                    {
                        TsuObject objectFound = this.ElementModule.SelectableObjects.Find(x => x is E.Point p && p._Name == item._Name && p._Origin == item._Origin && (item.LGCFixOption == LgcPointOption.CALA));
                        if (objectFound != null)
                        {
                            this.ElementModule.AddSelectedObjects(objectFound);
                        }
                    }
                }
            }

            /// <summary>
            /// S�lectionne un fichier de s�quence
            /// </summary>
            internal void SelectSequence()
            {
                this.SetSequenceInElementManager();
                this.ElementModule.SelectOneSequenceFil();
            }
            /// <summary>
            /// Set the actual sequence in the element manager for sequence selection
            /// </summary>
            internal void SetSequenceInElementManager()
            {
                this.ElementModule.SetSelectableToAllSequenceFil();
                this.ElementModule._SelectedObjects.Clear();
                //Coche les points d�j� s�lectionn� dans l'element module
                if (this._SequenceSelected != null && this.ElementModule.SelectableObjects != null)
                {
                    TsuObject selected = this.ElementModule.SelectableObjects.Find(x => x._Name == this._SequenceSelected._Name);
                    if (selected != null)
                    {
                        this.ElementModule.AddSelectedObjects(selected);
                    }
                }
            }
            /// <summary>
            /// Override the method in Final Module when received event to add points to measure
            /// </summary>
            /// <param name="compositeSelected"></param>
            internal override void SetPointsToMeasure(EC.CompositeElement compositeSelected)
            {
                CloneableList<E.Point> pointsSelected = new CloneableList<E.Point>();
                bool isSequence = false;
                if (compositeSelected != null)
                {
                    foreach (var item in compositeSelected)
                    {
                        if (item is E.Point p)
                            pointsSelected.Add(p);
                        if (item is EC.Magnet m)
                            pointsSelected.AddRange(m.GetAlesages());
                    }
                }
                //Pour �viter l'�v�nement lorsqu'on s�lectionne un fichier theo
                if (!(compositeSelected is EC.TheoreticalElement) && !(compositeSelected is EC.Sequence))
                    this.SetPointsToMeasure(pointsSelected, isSequence);
            }
            /// <summary>
            /// recupere les points dans la sequence fil et ajoute les points
            /// </summary>
            /// <param name="sequenceFromElementModule"></param>
            internal void SetSequenceToMeasure(EC.SequenceFil sequenceFromElementModule)
            {
                CloneableList<E.Point> pointsSelected = new CloneableList<E.Point>();
                bool isSequence = true;
                if (sequenceFromElementModule != null)
                {
                    bool messageOfChoiceAlreadyShown = false;
                    bool usePointsWithoutTheoCoord = true;
                    foreach (var item in sequenceFromElementModule.Elements)
                    {
                        if (item is E.Point pt)
                        {
                            // Utilisation ou pas des points non-pr�sents dans le fichier th�orique choisi
                            if (item._Origin == sequenceFromElementModule._Origin)
                            {
                                if (messageOfChoiceAlreadyShown == false)
                                {
                                    MessageInput mi = new MessageInput(MessageType.Choice, R.StringLine_UseOrNotNewCreatedPoints)
                                    {
                                        ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                                    };
                                    string answer = mi.Show().TextOfButtonClicked;
                                    messageOfChoiceAlreadyShown = true;
                                    if (answer == R.T_YES)
                                    {
                                        usePointsWithoutTheoCoord = true;
                                    }
                                    else
                                    {
                                        usePointsWithoutTheoCoord = false;
                                    }
                                }
                                if (usePointsWithoutTheoCoord == true)
                                {
                                    pt.LGCFixOption = LgcPointOption.VXY;
                                    pointsSelected.Add(pt);
                                }

                            }
                            else
                            {
                                pt.LGCFixOption = LgcPointOption.VXY;
                                pointsSelected.Add(pt);
                            }
                        }
                    }


                }
                //Rajoute les anciens points theo qui ne sont pas dans les points s�lectionn�s
                if (this._SequenceSelected._Name == sequenceFromElementModule._Name)
                {
                    foreach (E.Point theoPoint in this._TheoPoint)
                    {
                        if (!pointsSelected.Exists(x => x._Name == theoPoint._Name))
                        {
                            E.Point pointToAdd = this.ElementModule.GetPointsInAllElements().Find(x => x._Name == theoPoint._Name && x._Origin == theoPoint._Origin);
                            if (pointToAdd != null)
                            {
                                pointsSelected.Add(pointToAdd);
                            }
                        }
                    }
                }
                else
                {

                }
                ///enleve toutes les mesures si on a s�lectionn� une autre s�quence fil
                if (this._SequenceSelected._Name != sequenceFromElementModule._Name && pointsSelected.Count >= 2)
                {
                    this.WorkingAverageStationLine._ListStationLine.Clear();
                    this._MeasPoint.Clear();
                    this._TheoPoint.Clear();
                    this.FinalModule.PointsToBeMeasured.Clear();
                    this.FinalModule.CalaPoints.Clear();
                    this.FinalModule.PointsToBeAligned.Clear();
                    //Met le premier et dernier point de la s�quence en CALA
                    for (int i = 0; i < pointsSelected.Count; i++)
                    {
                        if (i == 0 || (i == pointsSelected.Count - 1))
                            pointsSelected[i].LGCFixOption = LgcPointOption.CALA;
                        else
                            pointsSelected[i].LGCFixOption = LgcPointOption.VXY;
                    }
                }
                this._SequenceSelected = sequenceFromElementModule;
                //S'il n'y a pas d�j� de ligne, cr�e la ligne et force les 2 extr�mit�s de la s�quence comme point de cala
                //if (this._AverageStationLine._ListStationLine.Count == 0)
                //{
                //    this.SetPointsToMeasure(this.FinalModule.CalaPoints);
                //}
                this.SetPointsToMeasure(pointsSelected, isSequence);
            }
            /// <summary>
            /// Set the points to measure on the stationLine
            /// </summary>
            /// <param name="pointsSelected"></param>
            internal void SetPointsToMeasure(CloneableList<E.Point> pointsSelected, bool isSequence = false, bool saveModule = true)
            {
                if (pointsSelected.Count >= 2)
                {
                    // si c'est une s�quence on efface les anciens points et on cr�e un nouveau fil
                    //if (isSequence)
                    //{
                    //    this._AverageStationLine._ListStationLine.Clear();
                    //    this._MeasPoint.Clear();
                    //    this._TheoPoint.Clear();
                    //    this.FinalModule.PointsToBeMeasured.Clear();
                    //}
                    if (this.WorkingAverageStationLine._ListStationLine.Count == 0)
                    {
                        Station newStationLine = new Station();
                        newStationLine.strategy = this._ComputeStrategy;
                        newStationLine.ParametersBasic._Instrument = this.WorkingAverageStationLine.ParametersBasic._Instrument;
                        newStationLine._Parameters._Instrument = this.WorkingAverageStationLine._Parameters._Instrument;
                        this.WorkingAverageStationLine._ListStationLine.Add(newStationLine);
                        this.WorkingAverageStationLine.ParametersBasic.LastChanged = DateTime.Now;
                        this._TheoPoint.Clear();
                        this._MeasPoint.Clear();
                        this.AddPoints(pointsSelected, isSequence);
                        // si s�quence on ne trie pas les points et le premier et dernier point sont les ancrages
                        //if (!isSequence) { this._AverageStationLine.SortMeasures(); }
                        if (this.View != null)
                        {
                            switch (this.View.moduleType)
                            {
                                case ModuleType.Guided:
                                    if (!this.CheckIfAcrossOrigin())
                                        this.SortMeasure();
                                    else
                                        this.SortMeasAcrossOrigin();
                                    break;
                                case ModuleType.Advanced:
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                        {
                            if (!this.CheckIfAcrossOrigin())
                                this.SortMeasure();
                            else
                                this.SortMeasAcrossOrigin();
                        }
                        //Met le premier point et le dernier point en cala pour au moins avoir 2 cala pour le premier fil
                        Station stationLine = this.WorkingAverageStationLine._ListStationLine[0];
                        if (this.View?.moduleType == ModuleType.Advanced)
                        {
                            stationLine._MeasureOffsets[0]._Point.LGCFixOption = LgcPointOption.CALA;
                            stationLine._MeasureOffsets[stationLine._MeasureOffsets.Count - 1]._Point.LGCFixOption = LgcPointOption.CALA;
                        }
                        stationLine.strategy = this.WorkingAverageStationLine._strategy;
                        stationLine._Anchor1 = stationLine._MeasureOffsets[0].DeepCopy();
                        stationLine._Anchor1._Date = DateTime.Now;
                        stationLine._Anchor2 = stationLine._MeasureOffsets[stationLine._MeasureOffsets.Count - 1].DeepCopy();
                        stationLine._Anchor2._Date = DateTime.Now;
                        this.WorkingAverageStationLine.ReName();
                        this.Utility = string.Format("{0}=>{1}", stationLine._Anchor1._PointName, stationLine._Anchor2._PointName);
                        this.FinalModule.Utility = this.Utility;
                        if (this.FinalModule is Line.Module lm) lm.View.SetStationNameInTopButton(this);
                        if (this.FinalModule.ParentModule is Common.Guided.Group.Module gm) gm.UpdateBigButtonTop();
                        this.CheckIfCalculationPossible();
                        if (this._TheoPoint.Find(x => x._OriginType == ElementType.SequenceFile) == null) { this.AddElementsToElementModule(); }
                        //recalcule les offsets et les mets � jour + raffraichit la vue
                    }
                    else
                    {
                        this.RemoveObsoleteMeas(pointsSelected);
                        //s'il y a des d�j� une ligne, lance la fonction pour ajouter tous les points � chaque ligne
                        this.AddPoints(pointsSelected);
                        //if (!isSequence) { this._AverageStationLine.SortMeasures(); }
                        if (this.View != null)
                        {
                            switch (this.View.moduleType)
                            {
                                case ModuleType.Guided:
                                    if (!this.CheckIfAcrossOrigin())
                                        this.SortMeasure();
                                    else
                                        this.SortMeasAcrossOrigin();
                                    break;
                                case ModuleType.Advanced:
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                        {
                            if (!this.CheckIfAcrossOrigin())
                                this.SortMeasure();
                            else
                                this.SortMeasAcrossOrigin();
                        }
                        this.CheckIfAnchorInTheoPoints();
                        // this.AddElementsToElementModule(); already done in UpdateOffsets
                        //this._AverageStationLine.ReName();
                        //recalcule les offsets et les mets � jour + raffraichit la vue
                    }
                    //Remet les extr�mit�s du fil dans le sens des cumul�es et en prenant les points avec la plus petite et la plus grande cumul�e. Calcul fait comme dans Geode 
                    if (this.View?.moduleType == ModuleType.Guided && this._ComputeStrategy is LGC2Strategy)
                    {
                        this.ResetWireExtremities();
                    }
                    this.UpdateOffsets(saveModule);
                }
                else
                {
                    //message d'erreur
                    //this.View.ShowMessage(R.StringLine_ErrorPoint);
                }
            }
            /// <summary>
            /// Trie les points et les mesures pour un fil passant par l'origine d'un acc�l�rateur
            /// </summary>
            private void SortMeasAcrossOrigin()
            {
                if (this._TheoPoint.FindIndex(X => X._Accelerator == "SPS") != -1)
                {
                    this.SortPointsForSPSOrigin(this._TheoPoint);
                    this.SortPointsForSPSOrigin(this._MeasPoint);
                    this.SortPointsForSPSOrigin(this.FinalModule.PointsToBeMeasured);
                    this.SortPointsForSPSOrigin(this.FinalModule.CalaPoints);
                    this.SortPointsForSPSOrigin(this.FinalModule.PointsToBeAligned);
                    this.WorkingAverageStationLine.SortMeasForSPSOrigin();
                }
                if (this._TheoPoint.FindIndex(X => X._Accelerator == "PR") != -1)
                {
                    this.SortPointsForPROrigin(this._TheoPoint);
                    this.SortPointsForPROrigin(this._MeasPoint);
                    this.SortPointsForPROrigin(this.FinalModule.PointsToBeMeasured);
                    this.SortPointsForPROrigin(this.FinalModule.CalaPoints);
                    this.SortPointsForPROrigin(this.FinalModule.PointsToBeAligned);
                    this.WorkingAverageStationLine.SortMeasForPROrigin();
                }
                if (this._TheoPoint.FindIndex(X => X._Accelerator == "BR") != -1)
                {
                    this.SortPointsForBROrigin(this._TheoPoint);
                    this.SortPointsForBROrigin(this._MeasPoint);
                    this.SortPointsForBROrigin(this.FinalModule.PointsToBeMeasured);
                    this.SortPointsForBROrigin(this.FinalModule.CalaPoints);
                    this.SortPointsForBROrigin(this.FinalModule.PointsToBeAligned);
                    this.WorkingAverageStationLine.SortMeasForBROrigin();
                }
                if (this._TheoPoint.FindIndex(X => X._Accelerator == "DR") != -1)
                {
                    this.SortPointsForDROrigin(this._TheoPoint);
                    this.SortPointsForDROrigin(this._MeasPoint);
                    this.SortPointsForDROrigin(this.FinalModule.PointsToBeMeasured);
                    this.SortPointsForDROrigin(this.FinalModule.CalaPoints);
                    this.SortPointsForDROrigin(this.FinalModule.PointsToBeAligned);
                    this.WorkingAverageStationLine.SortMeasForDROrigin();
                }
            }
            /// <summary>
            /// Check if the wire is crossing the origin of an accelerator
            /// </summary>
            private bool CheckIfAcrossOrigin()
            {
                // Pour le SPS cherche si la liste des points contient un aimant dans le sextant 6 et dans le sextant 1
                if (this._TheoPoint.FindIndex(X => X._Accelerator == "SPS" && X._Numero.StartsWith("6")) != -1
                    && this._TheoPoint.FindIndex(X => X._Accelerator == "SPS" && X._Numero.StartsWith("1")) != -1)

                    return true;
                //// Pour le PS ring cherche si dans les num�ros, on a des 0 et des 9 en m�me temps
                if (this._TheoPoint.FindIndex(X => X._Accelerator == "PR" && X._Numero.StartsWith("0")) != -1
                && this._TheoPoint.FindIndex(X => X._Accelerator == "PR" && X._Numero.StartsWith("9")) != -1)

                    return true;
                //// Pour le booster ring cherche si dans les num�ros, on a des 1 et des 16 en m�me temps
                if (this._TheoPoint.FindIndex(X => X._Accelerator == "BR" && X._Numero.StartsWith("1")
                && !X._Numero.Contains("16")
                && !X._Numero.Contains("15")
                && !X._Numero.Contains("14")
                && !X._Numero.Contains("13")
                && !X._Numero.Contains("12")
                && !X._Numero.Contains("11")
                && !X._Numero.Contains("10")) != -1
                && this._TheoPoint.FindIndex(X => X._Accelerator == "BR" && X._Numero.StartsWith("16")) != -1)

                    return true;
                //// Pour AD ring cherche si dans les num�ros, on a des 0 et des 5 en m�me temps
                if (this._TheoPoint.FindIndex(X => X._Accelerator == "DR" && X._Numero.StartsWith("0")) != -1
                && this._TheoPoint.FindIndex(X => X._Accelerator == "DR" && X._Numero.StartsWith("5")) != -1)

                    return true;
                return false;
            }

            /// <summary>
            /// Met � jour les mesures et coordonn�es theo suite � une modification dans l'element manager
            /// </summary>
            /// <param name="originalPoint"></param>
            /// <param name="pointModified"></param>
            internal override void UpdateTheoCoordinates(E.Point originalPoint, E.Point pointModified)
            {
                bool update = false;
                foreach (Station line in this.WorkingAverageStationLine._ListStationLine)
                {
                    int index = line._MeasureOffsets.FindIndex(x => (x._Point._Name == originalPoint._Name) && (x._Point._Origin == originalPoint._Origin));
                    if (index != -1)
                    {
                        E.Point initialPoint = line._MeasureOffsets[index]._Point.DeepCopy();
                        line._MeasureOffsets[index]._Point.AssignProperties(pointModified);
                        line._MeasureOffsets[index]._Point.LGCFixOption = initialPoint.LGCFixOption;
                        update = true;
                    }
                }
                int indexTheo = this._TheoPoint.FindIndex(x => x._Name == originalPoint._Name && x._Origin == originalPoint._Origin);
                if (indexTheo != -1)
                {
                    E.Point initialPoint = this._TheoPoint[indexTheo].DeepCopy();
                    this._TheoPoint[indexTheo].AssignProperties(pointModified);
                    this._TheoPoint[indexTheo].LGCFixOption = initialPoint.LGCFixOption;
                }
                if (update) this.UpdateOffsets();
            }
            /// <summary>
            /// Remet les extr�mit�s du fil dans le sens des DCum
            /// </summary>
            private void ResetWireExtremities()
            {
                switch (this.sortDirection)
                {
                    case SortDirection.Dcum:
                        foreach (Station line in this.WorkingAverageStationLine._ListStationLine)
                        {
                            line._Anchor1 = line._MeasureOffsets[0].DeepCopy();
                            line._Anchor2 = line._MeasureOffsets[line._MeasureOffsets.Count - 1].DeepCopy();
                        }
                        break;
                    case SortDirection.InvDcum:
                        foreach (Station line in this.WorkingAverageStationLine._ListStationLine)
                        {
                            line._Anchor2 = line._MeasureOffsets[0].DeepCopy();
                            line._Anchor1 = line._MeasureOffsets[line._MeasureOffsets.Count - 1].DeepCopy();
                        }
                        break;
                    default:
                        break;
                }
            }
            /// <summary>
            /// Cr�e un nouveau point theo et l'ajoute � l'element module
            /// </summary>
            /// <param name="respond"></param>
            internal void CreateNewPoint(string newPointName, E.SocketCode socketCode)
            {
                this.ElementModule.SetSelectableToAllUniquePointForAdvanceWire();
                if (this.ElementModule.SelectableObjects.Find(x => (x as E.Point)._Name == newPointName && (x as E.Point)._Origin == R.T_CREATED_BY_USER) == null)
                {
                    E.Point newPoint = new E.Point(newPointName);
                    newPoint.SocketCode = socketCode;
                    newPoint.Date = DateTime.Now;
                    //Coordinates must have approximate coordinates to be able to do LGC calculation later, so beter to give no coordinate. NP will be not exported in dat file.
                    //newPoint._Coordinates.Ccs.X.Value = 0;
                    //newPoint._Coordinates.Ccs.Y.Value = 0;
                    //newPoint._Coordinates.Ccs.H.Value = 400;
                    if (newPoint._Point != "" && newPoint._Zone != "" && newPoint._Numero != "" && newPoint._Class != "")
                    {
                        newPoint.fileElementType = ElementType.Alesage;

                    }
                    else
                    {
                        newPoint.fileElementType = ElementType.Pilier;
                    }
                    string saveName = this._Name;
                    this._Name = R.T_CREATED_BY_USER;
                    this.SavePoint(newPoint);
                    this._Name = saveName;
                    this.ElementModule.SetSelectableToAllUniquePointForAdvanceWire();
                    if (this._TheoPoint.FindIndex(x => x._Name == newPoint._Name) == -1)
                    {
                        this._TheoPoint.Add(newPoint.DeepCopy());
                        this._MeasPoint.Add(newPoint.DeepCopy());
                    }
                    if (this._TheoPoint.Count > 1)
                    {
                        this.WorkingAverageStationLine.UpdateListMeasureOffset(newPoint);
                        this.SetAllPointsInElementManager();
                        this.ElementModule.ValidateSelection();
                    }
                }
            }
            //V�rifie que les ancrages existants sont toujours dans les points theo et les change si n�cessaire
            private void CheckIfAnchorInTheoPoints()
            {
                foreach (Station line in this.WorkingAverageStationLine._ListStationLine)
                {
                    if (this._TheoPoint.Find(x => x._Name == line._Anchor1._PointName) == null)
                    {
                        //Pour les modules guid�s, on a un step particulier pour les r�f�rences
                        if (this.View.moduleType == ModuleType.Advanced) { line._MeasureOffsets[0]._Point.LGCFixOption = LgcPointOption.CALA; }
                        line._Anchor1 = line._MeasureOffsets[0].DeepCopy();
                        line._Anchor1._Date = DateTime.Now;
                    }
                    if (this._TheoPoint.Find(x => x._Name == line._Anchor2._PointName) == null)
                    {
                        //Pour les modules guid�s, on a un step particulier pour les r�f�rences
                        if (this.View.moduleType == ModuleType.Advanced) { line._MeasureOffsets[line._MeasureOffsets.Count - 1]._Point.LGCFixOption = LgcPointOption.CALA; }
                        line._Anchor2 = line._MeasureOffsets[line._MeasureOffsets.Count - 1].DeepCopy();
                        line._Anchor2._Date = DateTime.Now;
                    }
                }
            }
            //Permet lorsqu'on cr�e un point � partir de l'element manager de mettre l'origine � created by user
            internal override string GetNewPointGroupName()
            {
                return R.T_CREATED_BY_USER;
            }
            /// <summary>
            /// Check if in the theo point that all points have coordinates to do a calculation 
            /// </summary>
            private void CheckIfCalculationPossible()
            {
                this.calculationToDo = true;
                if (this._TheoPoint.Find(x => x._OriginType == ElementType.SequenceFile) != null)//x._Origin == this._SequenceSelected._Origin
                {
                    this.calculationToDo = false;
                    return;
                }
                else
                {
                    if (this.WorkingAverageStationLine._ListStationLine.Count == 0)
                    {
                        this.calculationToDo = false;
                        return;
                    }
                    foreach (Station line in this.WorkingAverageStationLine._ListStationLine)
                    {
                        if (line._Anchor1._RawReading == na || line._Anchor2._RawReading == na)
                        {
                            this.calculationToDo = false;
                            return;
                        }
                        if (line._MeasureOffsets.FindAll(x => x._Point.LGCFixOption == LgcPointOption.CALA && !(x._Status is M.States.Bad)).Count < 2)
                        {
                            //un calcul SFB ne tient pas compte des CALA mais seulement des ancrages (extr�mit�s)
                            if (this._ComputeStrategy is LGC2Strategy)
                            {
                                this.calculationToDo = false;
                                return;
                            }
                        }
                        foreach (M.MeasureOfOffset meas in line._MeasureOffsets)
                        {
                            if (meas._RawReading == na && meas._Point.LGCFixOption == LgcPointOption.CALA && !(meas._Status is M.States.Bad))
                            {
                                this.calculationToDo = false;
                                return;
                            }
                            else
                            {
                                if (meas._Point.LGCFixOption == LgcPointOption.CALA && !this.CheckIfTheoPointHasCoordinate(meas._Point))
                                {
                                    this.calculationToDo = false;
                                    return;
                                    //    switch (this._AverageStationLine._Parameters._CoordType)
                                    //    {
                                    //        case CoordinatesType.CCS:
                                    //            if (meas._Point._Coordinates.Ccs.X.Value == na || meas._Point._Coordinates.Ccs.Y.Value == na || meas._Point._Coordinates.Ccs.H.Value == na)
                                    //            {
                                    //                this.calculationToDo = false;
                                    //            }
                                    //         break;
                                    //        case CoordinatesType.SU:
                                    //         if (meas._Point._Coordinates.Su.X.Value == na|| meas._Point._Coordinates.Su.Y.Value == na || meas._Point._Coordinates.Su.Z.Value == na)
                                    //             {
                                    //                 this.calculationToDo = false;
                                    //             }
                                    //         break;
                                    //        default:
                                    //         break;
                                    //    }   
                                }
                            }
                        }
                        if (this.CheckIfTheoPointHasCoordinate(line._Anchor1._Point) && this.CheckIfTheoPointHasCoordinate(line._Anchor2._Point))
                        {
                            //Calculation is possible   
                        }
                        else
                        {
                            this.calculationToDo = false;
                            return;
                        }

                        //switch (this._AverageStationLine._Parameters._CoordType)
                        //{
                        //    case CoordinatesType.CCS:
                        //        if (line._Anchor1._Point._Coordinates.Ccs.X.Value == na || line._Anchor1._Point._Coordinates.Ccs.Y.Value == na || line._Anchor1._Point._Coordinates.Ccs.H.Value == na||line._Anchor2._Point._Coordinates.Ccs.X.Value == na || line._Anchor2._Point._Coordinates.Ccs.Y.Value == na || line._Anchor2._Point._Coordinates.Ccs.H.Value == na)
                        //        {
                        //            this.calculationToDo = false;
                        //        }
                        //     break;
                        //    case CoordinatesType.SU:
                        //     if (line._Anchor1._Point._Coordinates.Su.X.Value == na || line._Anchor1._Point._Coordinates.Su.Y.Value == na || line._Anchor1._Point._Coordinates.Su.Z.Value == na || line._Anchor2._Point._Coordinates.Su.X.Value == na || line._Anchor2._Point._Coordinates.Su.Y.Value ==na || line._Anchor2._Point._Coordinates.Su.Z.Value == na)
                        //         {
                        //             this.calculationToDo = false;
                        //         }
                        //     break;
                        //    default:
                        //     break;
                        //}   
                    }
                }
            }
            /// <summary>
            /// Remove the measure if no more present in the point to be measured in the element module
            /// </summary>
            /// <param name="pointsSelected"></param>
            private void RemoveObsoleteMeas(CloneableList<E.Point> pointsSelected)
            {
                foreach (Station line in this.WorkingAverageStationLine._ListStationLine)
                {
                    CloneableList<M.MeasureOfOffset> measToRemove = new CloneableList<M.MeasureOfOffset>();
                    foreach (M.MeasureOfOffset meas in line._MeasureOffsets)
                    {
                        if (pointsSelected.Find(x => x._Name == meas._Point._Name) == null)
                        {
                            measToRemove.Add(meas);
                        }
                    }
                    foreach (M.MeasureOfOffset meas in measToRemove)
                    {
                        line._MeasureOffsets.Remove(meas);
                        this.WorkingAverageStationLine.ParametersBasic.LastChanged = DateTime.Now;
                        this._TheoPoint.Remove(this._TheoPoint.Find(x => x._Name == meas._Point._Name));
                        this._MeasPoint.Remove(this._MeasPoint.Find(x => x._Name == meas._Point._Name));
                        this.FinalModule.PointsToBeMeasured.Remove(this.FinalModule.PointsToBeMeasured.Find(x => x._Name == meas._Point._Name));
                        this.FinalModule.PointsToBeAligned.Remove(this.FinalModule.PointsToBeAligned.Find(x => x._Name == meas._Point._Name));
                        this.FinalModule.CalaPoints.Remove(this.FinalModule.CalaPoints.Find(x => x._Name == meas._Point._Name));
                    }
                }
            }
            /// <summary>
            /// Remove the measure if no more present in the actual CalaPoints
            /// </summary>
            /// <param name="pointsSelected"></param>
            private void RemoveObsoleteAnchors(CloneableList<E.Point> pointsSelected)
            {
                foreach (Station line in this.WorkingAverageStationLine._ListStationLine)
                {
                    CloneableList<M.MeasureOfOffset> measToRemove = new CloneableList<M.MeasureOfOffset>();
                    foreach (M.MeasureOfOffset meas in line._MeasureOffsets)
                    {
                        if (pointsSelected.Find(x => x._Name == meas._Point._Name) == null && meas._Point.LGCFixOption == LgcPointOption.CALA)
                        {
                            measToRemove.Add(meas);
                        }
                    }
                    foreach (M.MeasureOfOffset meas in measToRemove)
                    {
                        line._MeasureOffsets.Remove(meas);
                        this.WorkingAverageStationLine.ParametersBasic.LastChanged = DateTime.Now;
                        this._TheoPoint.Remove(this._TheoPoint.Find(x => x._Name == meas._Point._Name));
                        this._MeasPoint.Remove(this._MeasPoint.Find(x => x._Name == meas._Point._Name));
                        this.FinalModule.PointsToBeMeasured.Remove(this.FinalModule.PointsToBeMeasured.Find(x => x._Name == meas._Point._Name));
                        this.FinalModule.PointsToBeAligned.Remove(this.FinalModule.PointsToBeAligned.Find(x => x._Name == meas._Point._Name));
                        this.FinalModule.CalaPoints.Remove(this.FinalModule.CalaPoints.Find(x => x._Name == meas._Point._Name));
                    }
                }
            }
            /// <summary>
            /// Ajoute des points dans chaque ligne et mets � jour la average station line
            /// </summary>
            /// <param name="pointsToAdd"></param>
            internal void AddPoints(CloneableList<E.Point> pointsToAdd, bool isSequence = false)
            {
                try
                {
                    CloneableList<E.Point> cleanedSelectedPoint = CheckIfPointAlreadyMeasured(pointsToAdd);
                    foreach (E.Point pt in cleanedSelectedPoint)
                    //Cr�e une nouvelle mesure dans la liste des mesures et y affecte les points s�lectionn�s
                    {
                        this.WorkingAverageStationLine.ParametersBasic.LastChanged = DateTime.Now;
                        this.WorkingAverageStationLine.UpdateListMeasureOffset(pt);
                        this._TheoPoint.Add(pt);
                        this._MeasPoint.Add((pt).DeepCopy());
                        this.FinalModule.PointsToBeMeasured.Add((pt).DeepCopy());
                        if (pt.LGCFixOption == LgcPointOption.CALA) { this.FinalModule.CalaPoints.Add(pt.DeepCopy()); }
                        if (pt.LGCFixOption == LgcPointOption.VXY) { this.FinalModule.PointsToBeAligned.Add(pt.DeepCopy()); }
                    }
                    //if (!isSequence)
                    //{
                    //this.SortMeasure();
                    //}
                    this.CheckIfCalculationPossible();
                }
                catch (Exception e)
                {

                    if (e is NullReferenceException) { e.Data.Clear(); }
                    else
                    {
                        throw;
                    }
                }
            }
            /// <summary>
            /// Sort measure by dcum or reverse dcum
            /// </summary>
            internal void SortMeasure()
            {
                this.SortPoints(this._TheoPoint);
                this.SortPoints(this._MeasPoint);
                this.SortPoints(this.FinalModule.PointsToBeMeasured);
                this.SortPoints(this.FinalModule.CalaPoints);
                this.SortPoints(this.FinalModule.PointsToBeAligned);
                switch (this.sortDirection)
                {
                    case SortDirection.Dcum:
                        this.WorkingAverageStationLine.SortMeasures();
                        break;
                    case SortDirection.InvDcum:
                        this.WorkingAverageStationLine.ReverseMeasures();
                        break;
                    default:
                        break;
                }
            }

            /// <summary>
            /// Ajoute les points theo s�lectionn� dans element module
            /// </summary>
            internal void AddElementsToElementModule()
            //ajoute tous les points theo s�lectionn� dans element module
            {
                string groupName = this.WorkingAverageStationLine._Name;
                groupName = groupName.Replace('.', '_');
                string saveName = this._Name;
                this._Name = groupName;
                this.measPointgroupName = groupName;
                foreach (E.Point point in this._MeasPoint)
                {
                    if (this.CheckIfTheoPointHasCoordinate(point))
                    {
                        point.Type = E.Point.Types.NewPoint;
                        this.SavePoint(point);
                    }
                }
                this._Name = saveName;
            }
            /// <summary>
            /// V�rifie si le point theo correspondant au point mesur� a des coordonn�es.
            /// </summary>
            /// <param name="measPoint"></param>
            /// <returns></returns>
            internal bool CheckIfTheoPointHasCoordinate(E.Point measPoint)
            {
                bool hasCoord = true;
                double na = Tsunami2.Preferences.Values.na;
                E.Point theoPoint = this._TheoPoint.Find(x => x._Name == measPoint._Name);
                switch (this.WorkingAverageStationLine._Parameters._CoordType)
                {
                    case CoordinatesType.CCS:
                        if (theoPoint._Coordinates.Ccs.X.Value == na || theoPoint._Coordinates.Ccs.Y.Value == na || theoPoint._Coordinates.Ccs.Z.Value == na)
                        {
                            hasCoord = false;
                        }
                        break;
                    case CoordinatesType.SU:
                        if (theoPoint._Coordinates.Local.X.Value == na || theoPoint._Coordinates.Local.Y.Value == na || theoPoint._Coordinates.Local.Z.Value == na)
                        {
                            hasCoord = false;
                        }
                        break;
                    default:
                        break;
                }

                return hasCoord;
            }
            /// <summary>
            /// V�rifie si tous les ancrages de l'averageStationLine ont des ancrages
            /// </summary>
            /// <returns></returns>
            internal bool CheckIfAllAnchorsHasReading()
            {
                bool hasReading = true;
                foreach (Station line in this.WorkingAverageStationLine._ListStationLine)
                {
                    hasReading = CheckIfAnchorsHasReading(line);
                }
                return hasReading;
            }
            /// <summary>
            /// V�rifie si les 2 ancrages de la ligne ont des lectures
            /// </summary>
            /// <param name="line"></param>
            /// <returns></returns>
            internal bool CheckIfAnchorsHasReading(Station line)
            {
                bool hasReading = true;
                if (line._Anchor1._RawReading == na || line._Anchor2._RawReading == na)
                {
                    hasReading = false;
                }
                return hasReading;
            }
            /// <summary>
            /// Enl�ve les points de l'element module avec l'origine origin
            /// </summary>
            /// <param name="origin"></param>
            internal void RemoveFromElementModule(string origin)
            {
                List<E.Point> savedMeasPointList = new List<E.Point>();
                foreach (E.Point pt in this._MeasPoint)
                {
                    E.Point copyP = pt.DeepCopy();
                    savedMeasPointList.Add(copyP);
                }
                string saveName = this._Name;
                origin = origin.Replace('.', '_');
                this._Name = origin;
                this.ElementModule.Clear(this);
                this._Name = saveName;
                this._MeasPoint = savedMeasPointList;
            }
            #endregion

            #region Observer observable
            public override void OnNext(TsuObject tsuObject)
            // dispatcehr d'evenement si un type n'est pas connu il est trait� par OnNextBasedOn(TsuObject tsuObject)
            {
                dynamic dynamic = tsuObject;
                OnNextBasedOn(dynamic);
            }
            public void OnNextBasedOn(I.Instrument tsuObject)
            {
                //already threated in "Management.Instrument.OffsetMeter SelectionEcartometre()"
                if (tsuObject is I.OffsetMeter om)
                {
                    this.SetOffsetMeter(om);
                }
            }

            public void OnNextBasedOn(M.Measure measureFromInstrument)
            //�v�nement li� � la r�ception d'une mesure de l'instrument et lancement de la fonction pour l'ajouter dans le tableau
            {
                //lance la fonction pour 
                this.View.AddInstrumentMeasure(measureFromInstrument);
                //this._OffsetModuleView.SendInstrumentReading(value)
                //this._MeasureModule.OnNext(value);
            }
            /// <summary>
            /// Fonction lanc�e lorsqu'on re�oit un instrument de l'instrument manager
            /// </summary>
            /// <param name="offsetMeter"></param>
            internal void SetOffsetMeter(I.OffsetMeter offsetMeter)
            {
                if (offsetMeter != null)
                {
                    ecartoInstrument = offsetMeter;
                    this.WorkingAverageStationLine.ChangeInstrument(offsetMeter);
                    this.WorkingAverageStationLine._Parameters._State = new State.ElementSelection();
                    this.UpdateOffsets();
                    this.View?.UpdateInstrumentView();
                }
            }

            /// <summary>
            /// Selection de l'instrument pour les mesures ecarto.
            /// </summary>
            /// <returns></returns>
            internal I.OffsetMeter SelectionEcartometre()
            {
                var preselected = new List<TsuObject>() { this.WorkingAverageStationLine._Parameters._Instrument };
                var selectables = this._InstrumentManager.GetByClass(I.InstrumentClasses.ECARTOMETRE);
                var offsetMeter = this._InstrumentManager.SelectInstrument(R.StringLine_SelectOffsetmeter, selectables, multiSelection: false, preselected) as I.OffsetMeter;

                this.SetOffsetMeter(offsetMeter);
                return offsetMeter;
            }

            #endregion

            #region Gestion fils & point & offsets

            internal void AddFil()
            //ajoute un fil
            {
                //Cr�e une nouvelle mesure dans la liste des mesures et y affecte les points s�lectionn�s
                Station newStationLine = new Station();
                newStationLine._Parameters._Instrument = ecartoInstrument;
                newStationLine._Parameters._Team = this.WorkingAverageStationLine._Parameters._Team;
                newStationLine._Parameters._Operation = this.WorkingAverageStationLine._Parameters._Operation;
                newStationLine.ParametersBasic._Operation = this.WorkingAverageStationLine.ParametersBasic._Operation;
                this.WorkingAverageStationLine.ParametersBasic.LastChanged = DateTime.Now;
                if (this.WorkingAverageStationLine._ListStationLine.Count != 0)
                {
                    newStationLine = this.WorkingAverageStationLine._ListStationLine[0].DeepCopy();
                    newStationLine._Anchor1 = this.WorkingAverageStationLine._ListStationLine[0]._MeasureOffsets[0].DeepCopy();
                    newStationLine._Anchor2 = this.WorkingAverageStationLine._ListStationLine[0]._MeasureOffsets[this.WorkingAverageStationLine._ListStationLine[0]._MeasureOffsets.Count - 1].DeepCopy();
                }
                WorkingAverageStationLine._ListStationLine.Add(newStationLine);
                this.UpdateOffsets();
                //this._OffsetModuleView.UpdateListMeasure(this._AverageStationLine);
            }

            internal void DeleteFil(int numeroFil)

            //Efface le fil s�lectionn� sauf le dernier fil
            {

                if (this.WorkingAverageStationLine._ListStationLine.Count != 1)
                {
                    this.WorkingAverageStationLine._ListStationLine.RemoveAt(numeroFil);
                    this.WorkingAverageStationLine.ParametersBasic.LastChanged = DateTime.Now;
                }
                this.UpdateOffsets();
            }
            internal void ExportToGeode(bool askIfSaveLGC = true, bool alwaysSaveLGC = true)
            {
                //Result saveLGCOK = new Result();
                string respond = "";
                //No more save each station avec LGC, a global export to LGC is now available
                //if (askIfSaveLGC && this._ComputeStrategy is LGC2Strategy)
                //{
                //    if  (this.View.ShowMessageOfChoice(R.StringLine_LGCAskIfSave, R.T_YES, R.T_NO) == R.T_YES)
                //    {
                //        saveLGCOK = this._ComputeStrategy.SaveLGCFileWire(this,true);
                //    }
                //}
                //else
                //{
                //    if (this._ComputeStrategy is LGC2Strategy && alwaysSaveLGC)
                //    {
                //        saveLGCOK = this._ComputeStrategy.SaveLGCFileWire(this, true);
                //    }
                //}
                //saveLGCOK.Success = true;
                Result saveDatOK = SaveToGeode.SaveFilToGeode(this, false, this.calculationToDo);
                //if (saveLGCOK.Success & saveDatOK.Success & !ShowSaveMessageOfSuccess.IsTrue)
                //{
                //    respond = this.View.ShowMessageOfValidation(String.Format(R.StringLine_WIreSavedInLGCAndDat, this._AverageStationLine.ParametersBasic._GeodeDatFilePath, this._AverageStationLine.ParametersBasic._LGCSaveFilePath), R.T_OK, R.T_OpenFileLocation, "", "", ShowSaveMessageOfSuccess);
                //}
                //}
                //else
                //{
                DsaFlag dsaFlag = DsaFlag.GetByNameOrAdd(this.FinalModule.DsaFlags, "ShowSaveMessageOfSuccess");
                if (saveDatOK.Success)
                {
                    string titleAndMessage = string.Format(R.StringLine_WireSaved, ":");

                    base.ShowGeodeExportSuccess(titleAndMessage, this.WorkingAverageStationLine.ParametersBasic._GeodeDatFilePath);

                    //respond = new MessageInput(MessageType.GoodNews, titleAndMessage)
                    //{
                    //    ButtonTexts = new List<string> { R.T_OK, R.T_OpenFileLocation },
                    //    DontShowAgain = dsaFlag
                    //}.Show().TextOfButtonClicked;

                    ////if (saveLGCOK.Success & !ShowSaveMessageOfSuccess.IsTrue)
                    ////{
                    ////    respond = this.View.ShowMessageOfValidation(String.Format(R.StringLine_WireLGCSaved, this._AverageStationLine.ParametersBasic._LGCSaveFilePath), R.T_OK, R.T_OpenFileLocation, "", "", ShowSaveMessageOfSuccess);
                    ////}
                    ////}
                    //if (respond == R.T_OpenFileLocation)
                    //{
                    //    if (System.IO.File.Exists(this.WorkingAverageStationLine.ParametersBasic._GeodeDatFilePath))
                    //    {
                    //        System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo("explorer.exe", " /select, " + this.WorkingAverageStationLine.ParametersBasic._GeodeDatFilePath));
                    //    }
                    //}
                }

                this.FinalModule.Save();
            }

            internal EC.CompositeElement CheckIfPointAlreadyMeasured(EC.CompositeElement selectedPoint)
            //V�rifie si des points s�lectionn� sont d�j� dans la liste des stations lines et les enl�ves pour �viter les doublons
            {
                EC.CompositeElement cleanedSelectedPoint = selectedPoint.Clone() as EC.CompositeElement;
                foreach (E.Element el in selectedPoint.Elements)
                {
                    if (el is E.Point newElementSelected)
                    {
                        //V�rifie qu'il y a au moins une station line
                        if (this.WorkingAverageStationLine._ListStationLine.Count != 0)
                        {
                            Station stationLine = this.WorkingAverageStationLine._ListStationLine[0];
                            if (stationLine._MeasureOffsets.Count != 0) //Evite le test s'il n'y a pas encore de points dans le tableau
                            {
                                foreach (M.MeasureOfOffset measureOfOffset in stationLine._MeasureOffsets)
                                {
                                    //v�rifie si Accelerator + Management.Zone + Class + Numero + Point sont les m�mes, si oui enl�ve l'�l�ment de la liste
                                    if (measureOfOffset._PointName == newElementSelected._Name)
                                    {
                                        cleanedSelectedPoint.Elements.Remove(el);
                                    }
                                }
                            }
                            else
                            {
                                return cleanedSelectedPoint;
                            }
                        }
                    }
                }
                return cleanedSelectedPoint;
            }
            /// <summary>
            /// V�rifie que les points ne font pas d�j� partie de la ligne
            /// </summary>
            /// <param name="pointsToAdd"></param>
            /// <returns></returns>
            private CloneableList<E.Point> CheckIfPointAlreadyMeasured(CloneableList<E.Point> pointsToAdd)
            {
                CloneableList<E.Point> cleanedSelectedPoint = pointsToAdd.Clone();
                foreach (E.Point pt in pointsToAdd)
                {
                    //V�rifie qu'il y a au moins une station line
                    if (this.WorkingAverageStationLine._ListStationLine.Count != 0)
                    {
                        Station stationLine = this.WorkingAverageStationLine._ListStationLine[0];
                        if (stationLine._MeasureOffsets.Count != 0) //Evite le test s'il n'y a pas encore de points dans le tableau
                        {
                            foreach (M.MeasureOfOffset measureOfOffset in stationLine._MeasureOffsets)
                            {
                                //v�rifie si Accelerator + Management.Zone + Class + Numero + Point sont les m�mes, si oui enl�ve l'�l�ment de la liste
                                if (measureOfOffset._PointName == pt._Name)
                                {
                                    cleanedSelectedPoint.Remove(pt);
                                }
                            }
                        }
                        else
                        {
                            return cleanedSelectedPoint;
                        }
                    }
                }
                return cleanedSelectedPoint;
            }

            internal void DeletePoint(string nameOfPoint)
            //efface une mesure au rowindew dans chaque station line de average station line
            {
                foreach (Station stationLine in this.WorkingAverageStationLine._ListStationLine)
                {
                    //Change les ancrages du fil si un des points effac�s est un ancrage de ce fil
                    if ((nameOfPoint == stationLine._Anchor1._PointName) || (nameOfPoint == stationLine._Anchor2._PointName))
                    {
                        //enl�ve la mesure
                        stationLine._MeasureOffsets.Remove(stationLine._MeasureOffsets.Find(x => nameOfPoint == x._PointName));
                        stationLine._Anchor1 = stationLine._MeasureOffsets[0].DeepCopy();
                        stationLine._Anchor2 = stationLine._MeasureOffsets[stationLine._MeasureOffsets.Count - 1].DeepCopy();
                    }
                    else
                    {
                        //enl�ve la mesure
                        stationLine._MeasureOffsets.Remove(stationLine._MeasureOffsets.Find(x => nameOfPoint == x._PointName));
                    }
                }
                //this.CheckNumberOfCalaPoints(this._AverageStationLine._ListStationLine[0]);
                this.FinalModule.PointsToBeMeasured.Remove(this.FinalModule.PointsToBeMeasured.Find(x => x._Name == nameOfPoint));
                this.FinalModule.PointsToBeAligned.Remove(this.FinalModule.PointsToBeAligned.Find(x => x._Name == nameOfPoint));
                this.FinalModule.CalaPoints.Remove(this.FinalModule.CalaPoints.Find(x => x._Name == nameOfPoint));
                this._TheoPoint.Remove(this._TheoPoint.Find(x => nameOfPoint == x._Name));
                this._MeasPoint.Remove(this._MeasPoint.Find(x => nameOfPoint == x._Name));
                this.WorkingAverageStationLine.ParametersBasic.LastChanged = DateTime.Now;
                // this.AddElementsToElementModule(); already done in UpdateOffsets
                //this._AverageStationLine.ReName(); already done in UpdateOffsets
                //mise � jour des offsets
                this.UpdateOffsets();
            }
            /// <summary>
            /// Verifie qu'on a au moins 2 points en cala si on supprime un point cala et met si n�cessaire le premier et dernier point en cala
            /// </summary>
            /// <param name="stationLine"></param>
            private void CheckNumberOfCalaPoints(Station stationLine)
            {
                List<M.MeasureOfOffset> measCala = stationLine._MeasureOffsets.FindAll(x => x._Point.LGCFixOption == LgcPointOption.CALA);
                if (measCala.Count < 2)
                {
                    stationLine._MeasureOffsets[0]._Point.LGCFixOption = LgcPointOption.CALA;
                    stationLine._MeasureOffsets[stationLine._MeasureOffsets.Count - 1]._Point.LGCFixOption = LgcPointOption.CALA;
                }
            }
            /// <summary>
            /// Lorsqu'on change les options de cala de points d�j� existant dans les modules guid�s
            /// </summary>
            /// <param name="pointList"></param>
            internal void UpdateCalaVSPointToBeAligned(CloneableList<E.Point> pointList)
            {
                foreach (E.Point pt in pointList)
                {
                    foreach (Station line in this.WorkingAverageStationLine._ListStationLine)
                    {
                        int index = line._MeasureOffsets.FindIndex(x => x._PointName == pt._Name && x._Point._Origin == pt._Origin);
                        if (index != -1) line._MeasureOffsets[index]._Point.LGCFixOption = pt.LGCFixOption;
                    }
                }
            }
            /// <summary>
            /// Inverse le status d'une mesure entre Unknown et bad.
            /// </summary>
            /// <param name="pointName"></param>
            internal void ReverseMeasureStatus(string pointName)
            {
                bool BadIsOk = this._ComputeStrategy.CheckBadIsPossible(pointName, this);
                foreach (Station line in this.WorkingAverageStationLine._ListStationLine)
                {
                    M.MeasureOfOffset m = line._MeasureOffsets.Find(x => x._PointName == pointName);
                    if (m != null)
                    {
                        if (m._Status is M.States.Bad)
                        {
                            m._Status = new M.States.Unknown();
                            if (m.CommentFromTsunami == R.StringLine_BadMeas) m.CommentFromTsunami = "";
                        }
                        else
                        {
                            if (BadIsOk)
                            {
                                m._Status = new M.States.Bad();
                                if ((m.Comment.Length + R.StringLine_BadMeas.Length) <= 80 && !m.Comment.Contains(R.StringLine_BadMeas))
                                    m.CommentFromTsunami = R.StringLine_BadMeas + m.CommentFromTsunami;
                            }
                            else
                            {
                                new MessageInput(MessageType.Warning, R.StringLine_CannotPutBad).Show();
                            }

                        }
                    }
                }
                this.WorkingAverageStationLine.ParametersBasic.LastChanged = DateTime.Now;
                this.UpdateOffsets();
            }
            internal void UpdateOffsets(M.MeasureOfOffset mesureEntree, bool saveModule = true)
            // met � jour tous les offsets dans le average station line en fonction de la mesure entr�e
            {
                //Trie les mesures par decum
                //this._AverageStationLine.SortMeasures(); Removed to keep the points like the sequence

                // keep obosoloete
                foreach (Station line in this.WorkingAverageStationLine._ListStationLine)
                {
                    foreach (M.MeasureOfOffset item in line._MeasureOffsets)
                    {
                        if (item._PointName == mesureEntree._PointName)
                        {
                            if (item._Date.Year >= 2000)
                            {
                                var copy = item.DeepCopy();
                                copy.IsObsolete = true;
                                copy._Status = new M.States.Bad();
                                line.ObsoleteMeasureOfOffset.Add(copy);
                            }
                            
                        }
                    }
                }

                this.WorkingAverageStationLine.UpdateTheoCoordinates(this._TheoPoint);
                this.WorkingAverageStationLine.ResetMeasuresInAllLines();
                //Efface les coordonn�es mesur�es et les remplace par les coordonn�es th�oriques
                this.RemoveFromElementModule(this.WorkingAverageStationLine._Name);
                this._MeasPoint.Clear();
                this.Utility = string.Format("{0}=>{1}",
                                             this.WorkingAverageStationLine._ListStationLine[0]._Anchor1._PointName,
                                             this.WorkingAverageStationLine._ListStationLine[0]._Anchor2._PointName);
                this.FinalModule.Utility = this.Utility;
                //V�rifie si la mesure entr�e correspond � un ancrage
                foreach (Station line in this.WorkingAverageStationLine._ListStationLine)
                {
                    if (mesureEntree._PointName == line._Anchor1._PointName)
                    {
                        //si pas de date signifie que c'est un commentaire qui est mis � jour
                        if (mesureEntree._Date != DateTime.MinValue)
                        {
                            line._Anchor1._RawReading = mesureEntree._RawReading;
                            line._Anchor1._Date = mesureEntree._Date;
                            Survey.CorrectReadingForEtalonnage(line._Anchor1,
                                                               (this.WorkingAverageStationLine.ParametersBasic._Instrument as I.OffsetMeter).EtalonnageParameter,
                                                               this.WorkingAverageStationLine._Parameters);
                        }
                        else
                        {
                            line._Anchor1.CommentFromTsunami = mesureEntree.CommentFromTsunami;
                            line._Anchor1.CommentFromUser = mesureEntree.CommentFromUser;
                        }
                    }
                    if (mesureEntree._PointName == line._Anchor2._PointName)
                    {
                        //si pas de date signifie que c'est un commentaire qui est mis � jour
                        if (mesureEntree._Date != DateTime.MinValue)
                        {
                            line._Anchor2._RawReading = mesureEntree._RawReading;
                            line._Anchor2._Date = mesureEntree._Date;
                            Survey.CorrectReadingForEtalonnage(line._Anchor2,
                                                               (this.WorkingAverageStationLine.ParametersBasic._Instrument as I.OffsetMeter).EtalonnageParameter,
                                                               this.WorkingAverageStationLine._Parameters);
                        }
                        else
                        {
                            line._Anchor2.CommentFromTsunami = mesureEntree.CommentFromTsunami;
                            line._Anchor2.CommentFromUser = mesureEntree.CommentFromUser;
                        }
                    }
                    //Modifie la valeur entr�e dans chaque fil
                    foreach (M.MeasureOfOffset item in line._MeasureOffsets)
                    {
                        if (item._PointName == mesureEntree._PointName)
                        {

                            //si pas de date signifie que c'est un commentaire qui est mis � jour
                            if (mesureEntree._Date != DateTime.MinValue)
                            {
                                item._RawReading = mesureEntree._RawReading;
                                item._Date = mesureEntree._Date;
                            }
                            else
                            {
                                item.CommentFromTsunami = mesureEntree.CommentFromTsunami;
                                item.CommentFromUser = mesureEntree.CommentFromUser;
                            }
                        }
                        if (item._RawReading != na)
                            Survey.CorrectReadingForEtalonnage(item,
                                                               (this.WorkingAverageStationLine.ParametersBasic._Instrument as I.OffsetMeter).EtalonnageParameter,
                                                               this.WorkingAverageStationLine._Parameters);
                    }
                }

                this.CheckIfCalculationPossible();
                if (this.calculationToDo == true)
                {
                    foreach (E.Point item in this._TheoPoint)
                    {
                        /// pas de calcul des coordonn�es CCS si c'est la m�thode de calcul SFB
                        if (!(this._ComputeStrategy is SFBStrategy))
                        {
                            // Ne met dans les points mesur�s les points dont les mesures asscoci�es ne sont pas en bad
                            int index = this.WorkingAverageStationLine._ListStationLine[0]._MeasureOffsets.FindIndex(x => x._Point._Name == item._Name);
                            if (index != -1)
                            {
                                if (!(this.WorkingAverageStationLine._ListStationLine[0]._MeasureOffsets[index]._Status is M.States.Bad))
                                {
                                    this._MeasPoint.Add(item.DeepCopy());
                                }
                            }
                        }
                    }
                    switch (this.WorkingAverageStationLine._Parameters._CoordType)
                    {
                        case CoordinatesType.CCS:
                            //Calcule les coordonn�es mesur�es en CCS
                            this.WorkingAverageStationLine.HtoZ(E.Coordinates.ReferenceSurfaces.RS2K);
                            this.WorkingAverageStationLine.CalculateWireCoordinates(this);
                            //Calcule les nouveaux �carts
                            this.WorkingAverageStationLine.CCSToMLA();
                            this.WorkingAverageStationLine.UpdateTheoReadingAndDeviation();
                            break;
                        case CoordinatesType.SU:
                            //Calcule les coordonn�es mesur�es
                            this.WorkingAverageStationLine.CalculateWireCoordinates(this);
                            //pas besoin de convertir vers MLA
                            this.WorkingAverageStationLine.UpdateTheoReadingAndDeviation();
                            break;
                        //case CoordinatesType.UserDefined:
                        //    //Calcule les coordonn�es mesur�es
                        //    this._AverageStationLine.CalculateWireCoordinates(this);
                        //    //Copie avant des coordonn�es user defined vers coordonn�es locales pour le calcul
                        //    this._AverageStationLine.ReplaceLocalByUserCoordinates();
                        //    //pas besoin de convertir vers MLA
                        //    this._AverageStationLine.UpdateTheoReadingAndDeviation();
                        //    break;
                        default:
                            //Calcule les coordonn�es mesur�es
                            this.WorkingAverageStationLine.HtoZ(E.Coordinates.ReferenceSurfaces.RS2K);
                            this.WorkingAverageStationLine.CalculateWireCoordinates(this);
                            //Calcule les nouveaux �carts
                            this.WorkingAverageStationLine.CCSToMLA();
                            this.WorkingAverageStationLine.UpdateTheoReadingAndDeviation();
                            break;
                    }
                    //Met � jour les points dans le element module
                    this.AddElementsToElementModule();
                    this.AddAllMeasuresToFinalModule();
                }
                this.CheckIfReadyToBeSaved();
                if (this.WorkingAverageStationLine.ReName())
                {
                    this.AddElementsToElementModule();
                    if (this.FinalModule is Line.Module lm) lm.View.SetStationNameInTopButton(this);
                    if (this.FinalModule.ParentModule is Common.Guided.Group.Module gm) gm.UpdateBigButtonTop();
                }
                if (saveModule) this.FinalModule.Save();
                //Met � jour le dataGridView et treeview
                this.View?.UpdateListMeasure();
            }
            /// <summary>
            /// va chercher un point existant dans l'element module et le rajoute dans les points � mesurer
            /// </summary>
            /// <param name="newPoint"></param>
            internal void SetNewPointAlreadyExisting(E.Point newPoint)
            {
                if (this._TheoPoint.FindIndex(x => x._Name == newPoint._Name) == -1)
                {
                    CloneableList<E.Point> newListPoint = new CloneableList<E.Point>();
                    newListPoint.AddRange(this._TheoPoint);
                    newListPoint.Add(newPoint);
                    this.SetPointsToMeasure(newListPoint);
                }
            }
            /// <summary>
            /// Ajoute les derni�res mesures dans le final module dans la liste des received measures
            /// </summary>
            private void AddAllMeasuresToFinalModule()
            {
                this.RemoveAllMeasuresFromFinalModule();
                List<M.MeasureOfOffset> measToAdd = this.WorkingAverageStationLine._ListStationLine[0]._MeasureOffsets.FindAll(x => x._AverageDeviation != na);
                if (measToAdd != null)
                {
                    this.FinalModule.ReceivedMeasures.AddRange(measToAdd);
                }

            }
            /// <summary>
            /// Enl�ve toutes les mesuresOfOffset du final module dans la liste des received measures
            /// </summary>
            private void RemoveAllMeasuresFromFinalModule()
            {
                this.FinalModule.ReceivedMeasures.RemoveAll(x => x is M.MeasureOfOffset);
            }
            internal void UpdateOffsets(bool saveModule = true)
            //Met � jour tous les offsets dans le average station line
            {
                if (this.WorkingAverageStationLine._ListStationLine.Count > 0)
                {
                    if (this.WorkingAverageStationLine._ListStationLine[0]._MeasureOffsets.Count > 0)
                    {
                        M.MeasureOfOffset mesure = this.WorkingAverageStationLine._ListStationLine[0]._MeasureOffsets[0].DeepCopy();
                        this.UpdateOffsets(mesure);
                    }
                }
            }
            internal CloneableList<M.MeasureOfOffset> DeepCopyOfListMeasureOfOffset(CloneableList<M.MeasureOfOffset> measure)
            //fait une copie en profondeur de la liste des mesures Offsets
            {
                CloneableList<M.MeasureOfOffset> listClone = new CloneableList<M.MeasureOfOffset>();
                foreach (M.MeasureOfOffset item in measure)
                {
                    listClone.Add(item.DeepCopy());
                }
                return listClone;
            }

            internal void AddAnchor(string pointName, int numeroFil, int ancrage1ou2)
            //Modifie l'ancrage du fil 
            {
                int PositionMesure = this.WorkingAverageStationLine._ListStationLine[numeroFil]._MeasureOffsets.FindIndex(x => x._PointName == pointName);
                this.WorkingAverageStationLine.ParametersBasic.LastChanged = DateTime.Now;
                if (ancrage1ou2 == 1 && PositionMesure != -1)
                {
                    this.WorkingAverageStationLine._ListStationLine[numeroFil]._Anchor1 = this.WorkingAverageStationLine._ListStationLine[numeroFil]._MeasureOffsets[PositionMesure].DeepCopy();
                    this.UpdateOffsets(this.WorkingAverageStationLine._ListStationLine[numeroFil]._Anchor1);
                }
                if (ancrage1ou2 == 2 && PositionMesure != -1)
                {
                    this.WorkingAverageStationLine._ListStationLine[numeroFil]._Anchor2 = this.WorkingAverageStationLine._ListStationLine[numeroFil]._MeasureOffsets[PositionMesure].DeepCopy();
                    this.UpdateOffsets(this.WorkingAverageStationLine._ListStationLine[numeroFil]._Anchor2);
                }
            }

            #endregion


            /// <summary>
            /// v�rifie si la station peut-�tre sauv�e et met le status � jour dans les param�tres
            /// </summary>
            /// <returns></returns>
            internal bool CheckIfReadyToBeSaved()
            // v�rifie si la station peut-�tre sauv�e et met le status � jour dans les param�tres
            {
                //if ((this._AverageStationLine._Parameters._Team !=R.String_UnknownTeam) //&& (this._AverageStationLine._Parameters._Operation.IsSet) 
                //    && (this._AverageStationLine._Parameters._Instrument != null) && (this._AverageStationLine._ListStationLine.Count != 0) && (this.CheckIfAllAnchorsHasReading()))
                //{

                //    this._AverageStationLine._Parameters._State = new TSU.Stations.Station.State.WireReadyToBeSaved();
                //    return true;
                //}
                //else
                //{
                //    if ((this._AverageStationLine._Parameters._Team !=R.String_UnknownTeam) //&& (this._AverageStationLine._Parameters._Operation.IsSet) 
                //    && (this._AverageStationLine._Parameters._Instrument != null) && (this._AverageStationLine._ListStationLine.Count != 0) && (this.calculationToDo == false))
                //    {
                //        this._ComputeStrategy.UpdateStationState(this);
                //    }
                //    return false;
                //}
                bool a = this.WorkingAverageStationLine._Parameters._Team != R.String_UnknownTeam;
                bool b = this.WorkingAverageStationLine._Parameters._Instrument != null;
                bool c = this.WorkingAverageStationLine._ListStationLine.Count != 0;
                bool d = this.CheckIfAllAnchorsHasReading();
                bool e = this.WorkingAverageStationLine._Parameters._Operation.IsSet;
                bool f = this.WorkingAverageStationLine.ParametersBasic.LastChanged > this.WorkingAverageStationLine.ParametersBasic.LastSaved;

                if (a && b && c && d && f)
                {
                    this.WorkingAverageStationLine._Parameters._State = new State.WireReadyToBeSaved();
                    return true;
                }
                else
                {
                    if (a && b && c && d && !f)
                    {
                        this.WorkingAverageStationLine._Parameters._State = new State.WireSaved();
                    }
                    if (a && b && c && this.calculationToDo == false)
                    {
                        this._ComputeStrategy.UpdateStationState(this);
                    }
                    return false;
                }
            }

            internal void SetWorkingAverageStationLineByGuid(string guid)
            {
                Average newWorking = GetAverageLineStationByGuid(guid);
                if (newWorking != null)
                    WorkingAverageStationLine = newWorking;
            }

            internal Average GetAverageLineStationByGuid(string guid)
            {
                Guid toFind = Guid.Parse(guid);
                bool match(Average st) => st.Guid.Equals(toFind);
                return AverageStations?.Find(match);
            }

            internal void AddStation()
            {
                Average newStation = (Average)WorkingAverageStationLine.Clone();
                if (newStation.HasStationLine())
                    foreach (var mesOff in newStation._ListStationLine[0]._MeasureOffsets)
                        ClearMeasure(mesOff);
                AverageStations.Add(newStation);
                WorkingAverageStationLine = newStation;
            }

            private void ClearMeasure(M.MeasureOfOffset mesOff)
            {
                mesOff._RawReading = na;
                mesOff._TheoreticalReading = na;
                mesOff._ReductedReading = na;
                mesOff._CorrectedReading = na;
                mesOff._TheoreticalReading = na;
                mesOff._TheoreticalDeviation = na;
                mesOff._AverageDeviation = na;
                mesOff._AverageTheoreticalReading = na;
                mesOff._Date = DateTime.Now;
                mesOff.SubReadings = null;
                mesOff._StandardDeviation = na;
                UpdateOffsets(mesOff);
            }
        }

    }
}
