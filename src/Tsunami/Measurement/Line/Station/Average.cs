﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using TSU.Common.Elements;
using TSU.Common.Instruments;
using TSU.Common.Measures;
using TSU.Common.Measures.States;

namespace TSU.Line
{
    public partial class Station
    {
        [Serializable()]
        [XmlType(TypeName = "Line.Station.Average")]
        public class Average : Common.Station, ICloneable
        //Classe globale reprenant la liste de tous les fils pour une stion écarto
        {
            //classe contenant la moyenne des fils fait et la liste des fils
            public CloneableList<Station> _ListStationLine;
            //public TSU.Module.Station.StationParameters _parameters;
            internal OffsetStrategy _strategy { get; set; }
            public bool _showLGC2Files { get; set; }
            public bool nameSetByUser = false;
            public string specialPartOfStationName = "";
            public Average()
            {
                _ListStationLine = new CloneableList<Station>();
                _Parameters = new Station.Parameters();
                ParametersBasic = _Parameters;
                ParametersBasic._Instrument = _Parameters._Instrument;
                //this._tolerance = 0.2;
                _Name = "STLINE_" + string.Format("{0:yyyyMMdd_HHmmss}" + "_", DateTime.Now) + "A" + "_" + "B";
                _Parameters._StationName = _Name;
                _Parameters._Station = this;
                _showLGC2Files = false;
                _strategy = new LGC2Strategy();
            }

            public override CloneableList<Measure> MeasuresTaken
            {
                get
                {
                    CloneableList<Measure> l = new CloneableList<Measure>();
                    foreach (var item in _ListStationLine)
                    {
                        l.AddRange(item.MeasuresTaken);
                    }
                    foreach (var item in l)
                    {
                        if (item._Status.Type == Types.Unknown)
                            item._Status = new Good();
                    }

                    return l;
                }
            }

            public Station.Parameters _Parameters
            {
                get
                {
                    return ParametersBasic as Station.Parameters;
                }
                set
                {
                    ParametersBasic = value;
                }
            }
            /// <summary>
            /// Renomme l'average station line en fonction des ancrages de la première ligne ou du nom choisi par l'utilisateur
            /// </summary>
            internal bool ReName(string userStationName = "NotSet")
            // Renomme l'average station line en fonction des ancrages de la première ligne ou du nom choisi par l'utilisateur
            {
                string commonPart = _Name.Substring(0, 23);
                string oldName = _Name;
                if (_ListStationLine.Count > 0 && !nameSetByUser)
                {
                    _Name = commonPart + _ListStationLine[0]._Anchor1._PointName + "_" + _ListStationLine[0]._Anchor2._PointName;
                    _Parameters._StationName = _Name;
                    specialPartOfStationName = _ListStationLine[0]._Anchor1._PointName + "_" + _ListStationLine[0]._Anchor2._PointName;
                }
                if (userStationName != "NotSet")
                {
                    _Name = commonPart + userStationName;
                    _Parameters._StationName = _Name;
                    nameSetByUser = true;
                    specialPartOfStationName = userStationName;
                }
                if (oldName != _Name)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public override object Clone()
            {
                Average newAverageStationLine = (Average)base.Clone();
                newAverageStationLine._ListStationLine = _ListStationLine?.Clone();
                return newAverageStationLine;
            }

            /// <summary>
            /// Change l'instrument pour toute la liste des station line et de l'average stationline
            /// </summary>
            /// <param name="ecartometre"></param>
            internal void ChangeInstrument(OffsetMeter ecartometre)
            {
                if (_Parameters._Instrument._Name != ecartometre._Name)
                {
                    _Parameters._Instrument = ecartometre;
                    ParametersBasic._Instrument = ecartometre;
                    ParametersBasic.LastChanged = DateTime.Now;
                    if (_ListStationLine.Count != 0)
                    {
                        foreach (Station line in _ListStationLine)
                        {
                            line._Parameters._Instrument = ecartometre;
                            line.ParametersBasic._Instrument = ecartometre;
                        }
                    }
                }
            }
            ///// <summary>
            ///// Change l'equipe dans toutes les station lines
            ///// </summary>
            ///// <param name="team"></param>
            //internal void ChangeTeam(string team)
            //{
            //    this._Parameters._Team = team;
            //    if (this._ListStationLine.Count != 0)
            //    {
            //        foreach (StationLine line in this._ListStationLine)
            //        {
            //            line._Parameters._Team = team;
            //        }
            //    }
            //}
            /// <summary>
            /// Change le numéro d'opération dans toutes les station lines
            /// </summary>
            /// <param name="OpID"></param>
            //internal void ChangeOperationID(Operations.Operation op)
            //{
            //    this._Parameters._Operation = op;
            //    if (this._ListStationLine.Count != 0)
            //    {
            //        foreach (StationLine line in this._ListStationLine)
            //        {
            //            line._Parameters._Operation = op;
            //        }
            //    }
            //}
            internal void UpdateListMeasureOffset(Point newPoint)
            //met à jour la liste des station line avec la nouvelle liste des measures of offset
            {
                foreach (Station stationLine in _ListStationLine)
                {
                    MeasureOfOffset blankMeasureOfOffset = new MeasureOfOffset();
                    blankMeasureOfOffset._Date = DateTime.MinValue;
                    blankMeasureOfOffset._Point = newPoint.DeepCopy();
                    if (newPoint.LGCFixOption == ENUM.LgcPointOption.CALA)
                    {
                        blankMeasureOfOffset._Point.LGCFixOption = ENUM.LgcPointOption.CALA;
                    }
                    else
                    {
                        blankMeasureOfOffset._Point.LGCFixOption = ENUM.LgcPointOption.VXY;
                    }
                    stationLine._MeasureOffsets.Add(blankMeasureOfOffset);
                }
            }
            public bool CheckIfStationLineAlreadyExist(Station a)
            //Vérifie si un fil est déjà présent dans les fils existants
            {
                foreach (var item in _ListStationLine)
                {
                    if ((item._Anchor1._Point._Name == a._Anchor1._Point._Name) && (item._Anchor2._Point._Name == a._Anchor2._Point._Name))
                    {
                        return true;
                    }
                }
                return false;
            }
            public Result CCSToMLA()
            {
                //Transforme toute la liste des stations lines du CCS en MLA2 avec le système centré sur le premier ancrage. Met à jour les coordonnées locales de toutes les mesures et ancrages
                Result result = new Result();
                foreach (Station CCSStationLine in _ListStationLine)
                {
                    //localStationLine = CCSStationLine;
                    //A compléter lorsque le geoid sera mis dans TSU preference
                    result = CCSStationLine.CCSToMLA(Coordinates.ReferenceSurfaces.RS2K);
                    if (!result.Success)
                    {
                        return result;
                    }

                }
                return result;
            }
            public void HtoZ(Coordinates.ReferenceSurfaces geoid)
            {
                //calcule le Z CCS en fonction du geoid pour tous les points des stations line et remplace le Z CCS déjà présent dans le point.
                foreach (Station CCSStationLine in _ListStationLine)
                {
                    //A compléter lorsque le geoid sera mis dans TSU preference
                    CCSStationLine.ConvertHToZ(geoid);
                }
            }
            public Result UpdateTheoReadingAndDeviation()
            {
                //Calcule la lecture theo et l'écart theo pour chaque fil sur les points d'ancrage et calcule la moyenne.
                return _strategy.UpdateTheoReadingAndDeviation(this);
            }
            public void DoReduction()
            {
                foreach (Station line in _ListStationLine)
                {
                    line.DoReduction();
                }
            }
            public void DoAxisTranslation()
            //Translate le système de coordonnée du système mla vers un système user en utilisant l'ancrage 1 comme origine
            {
                foreach (Station line in _ListStationLine)
                {
                    line.DoAxisTranslation();
                }
            }
            /// <summary>
            /// Trie les mesures dans l'ordre de la cumulée des points
            /// </summary>
            internal void SortMeasures()
            {
                foreach (Station line in _ListStationLine)
                {
                    line.SortMeasures();
                }
            }
            /// <summary>
            /// Trie les mesures dans l'ordre inverse de la cumulée des points
            /// </summary>
            internal void ReverseMeasures()
            {
                foreach (Station line in _ListStationLine)
                {

                    line.ReverseMeasures();
                }
            }
            /// <summary>
            /// Met à jour les coordonnées theoriques dans chaque stationline
            /// </summary>
            /// <param name="list"></param>
            internal void UpdateTheoCoordinates(List<Point> listTheoPoint)
            {
                foreach (Station line in _ListStationLine)
                {
                    line.UpdateTheoCoordinates(listTheoPoint);
                }
            }
            ///// <summary>
            ///// Remplace les coordonnées locales par les coordonnées user defined dans chaque ligne
            ///// </summary>
            //internal void ReplaceLocalByUserCoordinates()
            //{
            //    foreach (StationLine line in this._ListStationLine)
            //    {
            //        line.ReplaceMlaByUserCoordinates();
            //    }
            //}
            /// <summary>
            /// Calcule les coordonnées CCS de chaque point mesuré du fil
            /// </summary>
            /// <param name="stationLineModule"></param>
            internal void CalculateWireCoordinates(Station.Module stationLineModule)
            {
                _strategy.CalculateWireCoordinates(stationLineModule);
            }
            /// <summary>
            /// Remet à la valeur par défaut les déplacements et écarts de chaque mesures dans toutes les lignes
            /// </summary>
            internal void ResetMeasuresInAllLines()
            {
                foreach (Station line in _ListStationLine)
                {
                    line.ResetMeasuresInTheLine();
                }
            }
            /// <summary>
            /// Met les mesures du SPS du sextant 6 avant celle du sextant 1 pour chaque ligne
            /// </summary>
            internal void SortMeasForSPSOrigin()
            {
                foreach (Station line in _ListStationLine)
                {
                    line.SortMeasForSPSOrigin();
                }
            }
            /// <summary>
            /// Met les mesures du PS ring dans l'ordre pour chaque ligne
            /// </summary>
            internal void SortMeasForPROrigin()
            {
                foreach (Station line in _ListStationLine)
                {
                    line.SortMeasForPROrigin();
                }
            }
            /// <summary>
            /// Met les mesures du Booster ring dans l'ordre pour chaque ligne
            /// </summary>
            internal void SortMeasForBROrigin()
            {
                foreach (Station line in _ListStationLine)
                {
                    line.SortMeasForBROrigin();
                }
            }
            /// <summary>
            /// Met les mesures du Booster ring dans l'ordre pour chaque ligne
            /// </summary>
            internal void SortMeasForDROrigin()
            {
                foreach (Station line in _ListStationLine)
                {
                    line.SortMeasForDROrigin();
                }
            }

            public bool HasStationLine()
            {
                return _ListStationLine != null && _ListStationLine.Count > 0 && _ListStationLine[0] != null;
            }

        }
    }
}
