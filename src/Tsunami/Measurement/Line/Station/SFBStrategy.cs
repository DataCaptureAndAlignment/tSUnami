﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Forms;
using System.Xml.Serialization;
using TSU.Common.Compute;
using TSU.ENUM;
using TSU.Properties;
using TSU.Views.Message;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Line
{
    public partial class Station
    {
        [Serializable]
        [XmlType("SFBOffsetStrategy")]
        public class SFBStrategy : OffsetStrategy
        //stratégie calcul SFB
        {

            public SFBStrategy()
            {
                this._name = "SFB";
            }
            
            internal override void UpdateDatagridView(Module stationLineModule, DataGridView dataGridViewMesures)
            //Met à jour le datagridView en fonction du calcul des offsets
            {
                Average averageStationLine = stationLineModule.WorkingAverageStationLine;

                dataGridViewMesures.Rows.Clear();
                //ne garde que les 7 premières colonnes du datagridview
                int j = dataGridViewMesures.Columns.Count;
                for (int i = 8; i < j; i++)
                {
                    dataGridViewMesures.Columns.RemoveAt(8);
                }
                List<List<object>> listRow = new List<List<object>>();
                DataGridViewCellStyle cellStyle = new DataGridViewCellStyle();
                cellStyle.BackColor = Tsunami2.Preferences.Theme.Colors.LightBackground;
                cellStyle.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
                cellStyle.SelectionBackColor = Tsunami2.Preferences.Theme.Colors.TextHighLightBack;
                cellStyle.SelectionForeColor = Tsunami2.Preferences.Theme.Colors.TextHighLightFore;
                cellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                DataGridViewCellStyle cellStyleComment = new DataGridViewCellStyle();
                cellStyleComment.Alignment = DataGridViewContentAlignment.MiddleLeft;
                for (int i = 0; i < averageStationLine._ListStationLine.Count; i++)
                {
                    DataGridViewTextBoxColumn colonneEcart = new DataGridViewTextBoxColumn();
                    colonneEcart.DefaultCellStyle = cellStyle;
                    colonneEcart.HeaderText = string.Format(R.StringLine_DataGrid_OffWire, i.ToString());
                    colonneEcart.Name = "ecartFil" + i.ToString();
                    colonneEcart.ReadOnly = true;
                    dataGridViewMesures.Columns.Add(colonneEcart);
                    DataGridViewImageColumn columnAnc1 = new DataGridViewImageColumn();
                    columnAnc1.HeaderText = R.StringLine_DataGrid_A1;
                    columnAnc1.Name = "Anc1_Fil" + i.ToString();
                    columnAnc1.ValuesAreIcons = false;
                    columnAnc1.ImageLayout = DataGridViewImageCellLayout.Stretch;
                    columnAnc1.Width = 30;
                    columnAnc1.DefaultCellStyle = cellStyle;
                    columnAnc1.ReadOnly = true;
                    dataGridViewMesures.Columns.Add(columnAnc1);
                    DataGridViewImageColumn columnAnc2 = new DataGridViewImageColumn();
                    columnAnc2.HeaderText = R.StringLine_DataGrid_A2;
                    columnAnc2.Name = "Anc2_Fil" + i.ToString();
                    columnAnc2.DefaultCellStyle = cellStyle;
                    columnAnc2.ValuesAreIcons = false;
                    columnAnc2.ImageLayout = DataGridViewImageCellLayout.Stretch;
                    columnAnc2.Width = 30;
                    columnAnc2.ReadOnly = true;
                    dataGridViewMesures.Columns.Add(columnAnc2);
                }
                //ajoute la colonne commentaire à la fin
                DataGridViewTextBoxColumn colonneCommentaire = new DataGridViewTextBoxColumn();
                colonneCommentaire.DefaultCellStyle = cellStyleComment;
                colonneCommentaire.HeaderText = R.StringLine_DataGrid_Comment;
                colonneCommentaire.Name = "commentaire";
                colonneCommentaire.MaxInputLength = 150;
                colonneCommentaire.Width = 300;
                colonneCommentaire.ReadOnly = false;
                colonneCommentaire.Resizable = DataGridViewTriState.True;
                dataGridViewMesures.Columns.Add(colonneCommentaire);

                //ajoute la colonne interfaces à la fin
                DataGridViewTextBoxColumn colonneInterfaces = new DataGridViewTextBoxColumn();
                colonneInterfaces.DefaultCellStyle = cellStyleComment;
                colonneInterfaces.HeaderText = "Interfaces";
                colonneInterfaces.Name = "Interfaces";
                colonneInterfaces.MaxInputLength = 150;
                colonneInterfaces.Width = 150;
                colonneInterfaces.ReadOnly = false;
                colonneInterfaces.Resizable = DataGridViewTriState.True;
                dataGridViewMesures.Columns.Add(colonneInterfaces);
                //ajoute une ligne en entête pour effacer les fils
                List<object> rowDeleteFil = new List<object>();
                //Colonne 0: delete point
                rowDeleteFil.Add(R.Ancrage_Fil_Vide);
                //Colonne 1: nom
                rowDeleteFil.Add("");
                //colonne checkbox use meas 2
                rowDeleteFil.Add(R.Ancrage_Fil_Vide);
                //colonne Dcum 3
                rowDeleteFil.Add("");
                //Colonne 4: raw meas
                rowDeleteFil.Add(null);
                //Colonne 5: corr meas
                rowDeleteFil.Add(null);
                //Colonne 6: avg theo
                rowDeleteFil.Add(null);
                //Colonne 7: move avg
                rowDeleteFil.Add(null);
                //Colonne 8: offset wire
                rowDeleteFil.Add(null);
                //Colonne 9: A1
                rowDeleteFil.Add(R.Ancrage_Fil_Vide);
                //Colonne 10: A2
                rowDeleteFil.Add(R.Ancrage_Fil_Vide);
                for (int i = 1; i < averageStationLine._ListStationLine.Count; i++)
                {
                    rowDeleteFil.Add("");
                    rowDeleteFil.Add(R.Delete_Ancrage);
                    rowDeleteFil.Add(R.Ancrage_Fil_Vide);
                }

                //Colonne commentaire
                rowDeleteFil.Add("");
                //colonne 12 interfaces
                rowDeleteFil.Add("");


                listRow.Add(rowDeleteFil);

                //ajoute toutes les mesures dans le tableau ligne par ligne
                int indexOfLine = -1;
                foreach (Station line in averageStationLine._ListStationLine)
                {
                    indexOfLine++;
                    int indexOfMeasureOfOffset = 0;
                    foreach (M.MeasureOfOffset measureOfOffset in line._MeasureOffsets)
                    {
                        indexOfMeasureOfOffset++;
                        if (indexOfLine == 0)
                        {
                            List<object> row = new List<object>();
                            //Colonne 0: delete point
                            row.Add(R.Delete_Ancrage);
                            //Colonne 1: nom
                            row.Add("");
                            //colonne 2 : checkbox use meas
                            row.Add(R.CheckedBoxDatagridView);
                            //Colonne 3: Dcum
                            row.Add(null);
                            //Colonne 4: raw meas
                            row.Add(null);
                            //Colonne 5: corr meas
                            row.Add(null);
                            //Colonne 6: avg theo
                            row.Add(null);
                            //Colonne 7: move avg
                            row.Add(null);
                            for (int i = 0; i < averageStationLine._ListStationLine.Count; i++)
                            {
                                row.Add(null);
                                row.Add(R.Ancrage_Fil_Vide);
                                row.Add(R.Ancrage_Fil_Vide);
                            }
                            //Colonne commentaire
                            row.Add(measureOfOffset.CommentFromUser);
                            //colonne 12 interfaces
                            row.Add(measureOfOffset.Interfaces._Name);

                            listRow.Add(row);
                            listRow[indexOfMeasureOfOffset][0] = R.Delete_Ancrage;
                            listRow[indexOfMeasureOfOffset][1] = measureOfOffset._PointName;
                            //Ajoute la Dcum
                            listRow[indexOfMeasureOfOffset][3] = (measureOfOffset._Point._Parameters.Cumul == Tsunami2.Preferences.Values.na) ? "" : measureOfOffset._Point._Parameters.Cumul.ToString("F3", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                            if (measureOfOffset._RawReading != na)
                            {
                                listRow[indexOfMeasureOfOffset][4] = Math.Round(measureOfOffset._RawReading * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                                listRow[indexOfMeasureOfOffset][5] = Math.Round(measureOfOffset._CorrectedReading * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                                if (stationLineModule.calculationToDo == true && stationLineModule.CheckIfTheoPointHasCoordinate(measureOfOffset._Point) && !(measureOfOffset._Status is M.States.Bad))
                                {
                                    ///La lecture theo à lire sur la règle est la lecture brute - l'écart théorique, cette formule est approchés car elle ne tient pas compte de la différence d'étalonnage
                                    if (measureOfOffset._AverageTheoreticalReading != na)
                                    {
                                        //listRow[indexOfMeasureOfOffset][5] = Math.Round((measureOfOffset._RawReading- measureOfOffset._AverageDeviation) * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                                        Survey.CalculOffsetRawReading(measureOfOffset, (stationLineModule.WorkingAverageStationLine.ParametersBasic._Instrument as I.OffsetMeter).EtalonnageParameter, stationLineModule.WorkingAverageStationLine._Parameters);
                                        listRow[indexOfMeasureOfOffset][6] = Math.Round(measureOfOffset._AverageTheoreticalReading * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                                        listRow[indexOfMeasureOfOffset][7] = Math.Round(-measureOfOffset._AverageDeviation * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                                    }
                                }
                            }
                            //listRow[indexOfMeasureOfOffset][row.Count - 1] = measureOfOffset.Comment;

                            listRow[indexOfMeasureOfOffset][11] = measureOfOffset.CommentFromUser;
                            listRow[indexOfMeasureOfOffset][12] = measureOfOffset.Interfaces._Name;
                        }
                        //Indique dans le datagridview que c'est un point d'ancrage
                        if (measureOfOffset._PointName == line._Anchor1._PointName)
                        {
                            listRow[indexOfMeasureOfOffset][9 + indexOfLine * 3] = R.lock_anchor1;
                        }
                        if (measureOfOffset._PointName == line._Anchor2._PointName)
                        {
                            listRow[indexOfMeasureOfOffset][10 + indexOfLine * 3] = R.lock_anchor2;
                        }
                        if (measureOfOffset._RawReading != na && stationLineModule.calculationToDo == true && stationLineModule.CheckIfTheoPointHasCoordinate(measureOfOffset._Point) && !(measureOfOffset._Status is M.States.Bad))
                        {
                            listRow[indexOfMeasureOfOffset][8 + indexOfLine * 3] = Math.Round(measureOfOffset._TheoreticalDeviation * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                        }
                        if (measureOfOffset._Status is M.States.Bad)
                        {
                            listRow[indexOfMeasureOfOffset][2] = R.UncheckedBoxDatagridView;
                        }
                    }
                }
                foreach (List<object> ligne in listRow)
                {
                    dataGridViewMesures.Rows.Add(ligne.ToArray());
                }
                dataGridViewMesures.Rows[0].DefaultCellStyle.BackColor = Tsunami2.Preferences.Theme.Colors.LightBackground;
                dataGridViewMesures.Rows[0].DefaultCellStyle.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
                for (int r = 1; r < dataGridViewMesures.RowCount; r++)
                {
                    double p;
                    if (dataGridViewMesures[7, r].Value != null)
                    {
                        p = T.Conversions.Numbers.ToDouble(dataGridViewMesures[7, r].Value.ToString(), false, -9999);
                    }
                    else
                    {
                        p = -9999;
                    }
                    for (int c = 9; c < dataGridViewMesures.ColumnCount - 1; c = c + 3)
                    {
                        if ((dataGridViewMesures[c, r].Value as System.Drawing.Bitmap).Width == 128)
                        {
                            dataGridViewMesures[1, r].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewMesures[2, r].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewMesures[3, r].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewMesures[4, r].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewMesures[5, r].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewMesures[6, r].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewMesures[7, r].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        }
                    }
                    for (int c = 10; c < dataGridViewMesures.ColumnCount - 1; c = c + 3)
                    {
                        if ((dataGridViewMesures[c, r].Value as System.Drawing.Bitmap).Width == 128)
                        {
                            dataGridViewMesures[1, r].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewMesures[2, r].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewMesures[3, r].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewMesures[4, r].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewMesures[5, r].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewMesures[6, r].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                            dataGridViewMesures[7, r].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        }
                    }
                    if (stationLineModule.View.moduleType == ModuleType.Advanced)
                    {
                        //Temporairement plus de couleur pour les tolerances
                        //met en rouge le fond si le déplacement moyen est > à la tolérance d'alignement ou le texte en vert si dans la tolérance d'alignement
                        if (p != -9999)
                        {
                            if (Math.Abs(p) > averageStationLine._Parameters._Tolerance * 1000)
                            {
                                dataGridViewMesures[7, r].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Bad;
                            }
                            else
                            {
                                if (!double.IsNaN(p)) dataGridViewMesures[7, r].Style.ForeColor = Tsunami2.Preferences.Theme.Colors.Good;
                            }
                        }
                    }
                    //Met en jaune la colonne raw meas si pas de mesure encodée
                    if (dataGridViewMesures[1, r].Value.ToString() != "")
                    {
                        dataGridViewMesures[4, r].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Action;
                    }
                }
                foreach (var col in dataGridViewMesures.Columns)
                {
                    if (col is DataGridViewTextBoxColumn)
                    {
                        (col as DataGridViewTextBoxColumn).SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
                stationLineModule.sortOnlyDcum = true;
                dataGridViewMesures.Columns[0].Visible = true;
                ////Cache la colonne pour effacer les points dans le module guidé
                //switch (stationLineModule.View.moduleType)
                //{
                //    case ModuleType.Guided:
                //        dataGridViewMesures.Columns[0].Visible = false;
                //        break;
                //    case ModuleType.Advanced:
                //        dataGridViewMesures.Columns[0].Visible = true;
                //        break;
                //    default:
                //        dataGridViewMesures.Columns[0].Visible = true;
                //        break;
                //}
                //Met en read only la premiere ligne du tableau
                dataGridViewMesures.Rows[0].ReadOnly = true;
            }
            internal override void ChangeAnchorsDataGridView(DataGridView dataGridViewMesures, DataGridViewCellEventArgs e, Module stationLineModule)
            //Ajoute un ancrage si on double clique sur cellule ancrage
            {
                string nomFil;
                int numeroFil;
                int ancrage1ou2 = 0;
                ///Vérifie que la mesure associée n'est pas en status bad
                M.MeasureOfOffset meas = stationLineModule.WorkingAverageStationLine._ListStationLine[0]._MeasureOffsets.Find(x => x._PointName == dataGridViewMesures[0, e.RowIndex].Value.ToString());
                if (meas != null)
                {
                    if (e.ColumnIndex != 8 && meas._Status is M.States.Bad)
                    {
                        new MessageInput(MessageType.Warning, R.StringLine_CannotBeAnchor).Show();
                        return;
                    }
                }
                if (dataGridViewMesures.Columns[e.ColumnIndex].HeaderText == R.StringLine_DataGrid_A1)
                {
                    System.Drawing.Bitmap image = dataGridViewMesures[e.ColumnIndex + 1, e.RowIndex].Value as System.Drawing.Bitmap;
                    //Astuce image ancrage vide fait 65 de large, permet de voir si ancrage 1 = ancrage2
                    if (image.Width == 65)
                    {
                        ancrage1ou2 = 1;
                        nomFil = dataGridViewMesures.Columns[e.ColumnIndex].Name.ToString();
                        nomFil = nomFil.Remove(0, 8);
                        int.TryParse(nomFil, out numeroFil);
                        stationLineModule.AddAnchor(dataGridViewMesures[1, e.RowIndex].Value.ToString(), numeroFil, ancrage1ou2);
                    }

                }
                if (dataGridViewMesures.Columns[e.ColumnIndex].HeaderText == R.StringLine_DataGrid_A2)
                {
                    System.Drawing.Bitmap image = dataGridViewMesures[e.ColumnIndex - 1, e.RowIndex].Value as System.Drawing.Bitmap;
                    //Astuce image ancrage vide fait 65 de large, permet de voir si ancrage 1 = ancrage2
                    if (image.Width == 65)
                    {
                        ancrage1ou2 = 2;
                        nomFil = dataGridViewMesures.Columns[e.ColumnIndex].Name.ToString();
                        nomFil = nomFil.Remove(0, 8);
                        int.TryParse(nomFil, out numeroFil);
                        stationLineModule.AddAnchor(dataGridViewMesures[1, e.RowIndex].Value.ToString(), numeroFil, ancrage1ou2);
                    }
                }
                if (stationLineModule.WorkingAverageStationLine.ReName())
                {
                    stationLineModule.AddElementsToElementModule();
                    if (stationLineModule.FinalModule is Line.Module) (stationLineModule.FinalModule as Line.Module).View.SetStationNameInTopButton(stationLineModule);
                    if (stationLineModule.FinalModule.ParentModule is Common.Guided.Group.Module) (stationLineModule.FinalModule.ParentModule as Common.Guided.Group.Module).UpdateBigButtonTop();
                }
            }
            internal override Result WireOffsetCalculation(Station stationLine, bool showLGC2Files)
            //Calcule la lecture theo et l'écart theo pour chaque point d'un fil avec décalage sur les points d'ancrage.
            //Formule recopiée de SFB
            {
                Result result = new Result();
                double dX12 = stationLine._Anchor2._Point._Coordinates.Local.X.Value - stationLine._Anchor1._Point._Coordinates.Local.X.Value;
                double dY12 = stationLine._Anchor2._Point._Coordinates.Local.Y.Value - stationLine._Anchor1._Point._Coordinates.Local.Y.Value;
                double decalage1 = stationLine._Anchor1._CorrectedReading;
                double decalage2 = stationLine._Anchor2._CorrectedReading;
                stationLine._Anchor1._TheoreticalReading = decalage1;
                stationLine._Anchor2._TheoreticalReading = decalage2;
                double d12 = Math.Sqrt(Math.Pow(dX12, 2) + Math.Pow(dY12, 2));
                double angleA = (decalage2 - decalage1) / d12;
                //Angle A dans triangle 212b
                double angleA1 = Math.Abs(angleA) > 1 ? 0 : Math.Asin(angleA);
                foreach (M.MeasureOfOffset item in stationLine._MeasureOffsets)
                {
                    if (item._RawReading != na && !(item._Status is M.States.Bad))
                    {
                        double dX13 = item._Point._Coordinates.Local.X.Value - stationLine._Anchor1._Point._Coordinates.Local.X.Value;
                        double dY13 = item._Point._Coordinates.Local.Y.Value - stationLine._Anchor1._Point._Coordinates.Local.Y.Value;
                        item._TheoreticalReading = ((dX12 * Math.Sin(angleA1) - dY12 * Math.Cos(angleA1)) * dX13 + dY13 * (dX12 * Math.Cos(angleA1) + dY12 * Math.Sin(angleA1))) / d12 + decalage1;
                        item._TheoreticalDeviation = item._CorrectedReading - item._TheoreticalReading;
                        item._AverageTheoreticalReading = item._TheoreticalReading;
                        item._AverageDeviation = item._TheoreticalDeviation;
                    }
                }
                return result;


            }
            internal override Result UpdateTheoReadingAndDeviation(Average averageStationLine)
            //Calcule la lecture theo et l'écart theo pour chaque fil sur les points d'ancrage et calcule la moyenne.
            {
                Result result = new Result();
                //matrice qui va stocker tous les écarts pour chaque mesure de chaque fil.
                double[,] matriceTheoDeviation = new double[averageStationLine._ListStationLine[0]._MeasureOffsets.Count, averageStationLine._ListStationLine.Count];
                //Lecture de toutes les lignes et calcul de l'écart théorique et lecture théorique
                int lineIndex = 0;
                foreach (Station line in averageStationLine._ListStationLine)
                {
                    //Calcul de l'écart au théorique pour chaque fil
                    result = line.UpdateTheoReadingAndDeviation();
                    if (result.Success == false)
                    {
                        return result;
                    }
                    foreach (M.MeasureOfOffset measure in line._MeasureOffsets)
                    {
                        matriceTheoDeviation[line._MeasureOffsets.IndexOf(measure), lineIndex] = measure._TheoreticalDeviation;
                    }
                    lineIndex++;
                }
                //Array qui va stocker l'écart moyen au théorique pour le set de mesure        
                double[] averageTheoDeviation = new double[averageStationLine._ListStationLine[0]._MeasureOffsets.Count];
                //calcul de la moyenne
                for (int i = 0; i < averageStationLine._ListStationLine[0]._MeasureOffsets.Count; i++)
                {
                    for (int j = 0; j < averageStationLine._ListStationLine.Count; j++)
                    {
                        averageTheoDeviation[i] = averageTheoDeviation[i] + matriceTheoDeviation[i, j];
                    }
                }
                for (int i = 0; i < averageStationLine._ListStationLine[0]._MeasureOffsets.Count; i++)
                {
                    averageTheoDeviation[i] = averageTheoDeviation[i] / averageStationLine._ListStationLine.Count;
                }
                //mise de l'écart moyen dans chaque mesure
                foreach (Station line in averageStationLine._ListStationLine)
                {
                    foreach (M.MeasureOfOffset measure in line._MeasureOffsets)
                    {
                        measure._AverageDeviation = averageTheoDeviation[line._MeasureOffsets.IndexOf(measure)];
                        measure._AverageTheoreticalReading = measure._CorrectedReading - measure._AverageDeviation;
                    }

                }
                if (result.Success == false)
                {
                    averageStationLine.ParametersBasic._State = new State.SFBcalculationFailed();
                }
                else
                {
                    averageStationLine.ParametersBasic._State = new State.SFBcalculationSuccessfull();
                }
                return result;
            }
            /// <summary>
            /// Pour la stratégie SFB, les coordonnées du fil ne peuvent pas être calculées.
            /// </summary>
            /// <param name="stationLineModule"></param>
            /// <returns></returns>
            internal override Result CalculateWireCoordinates(Module stationLineModule)
            {
                Result result = new Result();
                result.Success = false;
                return result;
            }
            /// <summary>
            /// met à jour le status de la station si le calcul n'est pas possible
            /// </summary>
            /// <param name="stationLineModule"></param>
            internal override void UpdateStationState(Module stationLineModule)
            {
                stationLineModule.WorkingAverageStationLine._Parameters._State = new State.SFBcalculationFailed();
            }
            /// <summary>
            /// Vérifie que le status bad peut-être appliqué à la mesure
            /// </summary>
            /// <param name="pointName"></param>
            /// <returns></returns>
            internal override bool CheckBadIsPossible(string pointName, Module stationLineModule)
            {
                foreach (Station line in stationLineModule.WorkingAverageStationLine._ListStationLine)
                {
                    if ((line._Anchor1._PointName == pointName) || (line._Anchor2._PointName == pointName))
                    {
                        return false;
                    }
                }
                return true;
            }
            /// <summary>
            /// Export au format LGC2 n'est pas possible pour la méthode de calcul SFB.
            /// </summary>
            /// <param name="stationLineModule"></param>
            /// <param name="showMessageOfSuccess"></param>
            /// <returns></returns>
            internal override Result SaveLGCFileWire(Module stationLineModule, bool showMessageOfSuccess = true)
            {
                Result result = new Result();
                result.Success = false;
                return result;
            }
            /// <summary>
            /// Met la bonne stratégie pour l'affichage du datagridview dans la vue
            /// </summary>
            /// <param name="stationLineModule"></param>
            internal override void ReCreateWhatIsNotSerialized(Module stationLineModule)
            {
                if (stationLineModule.View != null)
                {
                    stationLineModule.View.computeStrategy = new SFBStrategy();
                    stationLineModule.View.HideManualOffsetmeterView();
                    stationLineModule.View.UpdateListMeasure();
                }
                stationLineModule.WorkingAverageStationLine._strategy = new SFBStrategy();
                foreach (Station line in stationLineModule.WorkingAverageStationLine._ListStationLine)
                {
                    line.strategy = new SFBStrategy();
                }
            }
        }

    }
}
