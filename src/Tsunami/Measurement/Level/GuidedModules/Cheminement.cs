﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using TSU.Views;
using System.Windows.Forms;
using M = TSU;
using L = TSU.Level;
using TSU.Common.Guided.Steps;

namespace TSU.Level.GuidedModules
{

    static public class Cheminement
    {
        static public Common.Guided.Module Get(M.Module parentModule, int stationNumber)
        {
            Common.Guided.Group.Module GrGm = parentModule as Common.Guided.Group.Module;
            GrGm.Type = ENUM.GuidedModuleType.Cheminement;

            Common.Guided.Module gm = new Common.Guided.Module(parentModule, $"{R.StringCheminement_StationName} {stationNumber.ToString()};A=>B");
            gm.guideModuleType = ENUM.GuidedModuleType.Cheminement;
            gm.ObservationType = Common.ObservationType.Level;

            // Invisible step: : Add needed station modules
            gm.SwitchToANewStationModule(new Station.Module(gm));

            BuildSteps(gm);

            gm.Start();

            return gm;
        }

        private static void BuildSteps(Common.Guided.Module gm)
        {
            // Step 0: Presentation
            gm.Steps.Add(Management.Declaration(gm, R.StringCheminement_CheminementPresentation));
            //// Step 1: Choose administrative parameters
            gm.Steps.Add(Steps.Measurement.ChooseAllAdministrativeparameters(gm));
            //*****************************************************
            //Old steps version
            //// Step 1: Choose Element Theoretical File
            //gm.Steps.Add(Step.Level.ChooseTheoreticalFile(gm));

            //// Step 2: Choose sequence Theoretical File
            //gm.Steps.Add(Step.Level.ChooseSequenceFile(gm));

            //// Step 3: Choose Management.Instrument
            //gm.Steps.Add(Step.Level.ChooseLevel(gm));

            //// Step 4: Choose default staff
            //gm.Steps.Add(Step.Level.ChooseDefaultStaff(gm));

            //// Step 5: Choose Team
            //gm.Steps.Add(Step.Management.EnterATeam(gm));

            /// Step 6 : Choose Operation
            //gm.Steps.Add(Step.Level.ChooseOperation(gm));
            //************************************************
            /// Step 2 : Choose sequence niv
            gm.Steps.Add(Steps.Measurement.ChooseSequenceNiv(gm));

            /// Step 8 : Choose points
            //gm.Steps.Add(Step.Level.ChoosePointsAndValidate(gm));

            /// Step 8 : Choose CALA points
            //gm.Steps.Add(Step.Level.ChooseCalaPoints(gm));
            //In new version only select variable points
            /// Step 3 : Choose VZ points
            gm.Steps.Add(Steps.Measurement.ChooseVZPoints(gm));

            /// Step 4 : Mesures aller
            gm.Steps.Add(Steps.Measurement.MeasureCheminementAller(gm));

            /// Step 11: Clone, previous return, etc
            //gm.Steps.Add(Step.Level.CloneLevelStation(gm));

            /// Step 5 : Mesures retour
            gm.Steps.Add(Steps.Measurement.MeasureCheminementRetour(gm));

            /// Step 13: Clone, previous return, etc
            //gm.Steps.Add(Step.Level.CloneLevelStation(gm));

            /// Step 6 : Mesures reprise
            gm.Steps.Add(Steps.Measurement.MeasureCheminementReprise(gm));

            /// Step 15: Clone, previous return, etc
            //gm.Steps.Add(Step.Level.CloneLevelStation(gm));

            /// Step 7 : Show dat saved file
            gm.Steps.Add(Steps.Measurement.ShowDatFiles(gm));

            /// Step 8 : Show LGC Files aller
            //gm.Steps.Add(Step.Level.ShowAllerLGCFiles(gm));

            /// Step 9 : Show LGC Files retour
            //gm.Steps.Add(Step.Level.ShowRetourLGCFiles(gm));

            /// Step 10 : Show LGC Files reprise
            //gm.Steps.Add(Step.Level.ShowRepriseLGCFiles(gm));
        }

        internal static void ReCreateWhatIsNotSerialized(Common.Guided.Module gm)
        {
            gm.Steps = new List<Step>();
            //gm.currentIndex = 0;
            BuildSteps(gm);
            //gm.Start();
            //gm.MoveToStep(gm.currentIndex);
        }
    }
}
