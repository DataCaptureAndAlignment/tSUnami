﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using TSU.Views;
using System.Windows.Forms;
using M = TSU;
using L = TSU.Level;
using TSU.Common.Guided.Steps;

namespace TSU.Level.GuidedModules.Steps
{

    static public class Alignment
    {
        static public Common.Guided.Module Get(M.Module parentModule)
        {
            Common.Guided.Group.Module GrGm = parentModule as Common.Guided.Group.Module;

            // Guided.Module gm = new Guided.Module(parentModule, String.Format(R.StringCheminement_StationName, "", "A", "B"));
            Common.Guided.Module gm = new Common.Guided.Module(parentModule, $"{R.StringCheminement_StationName} {""};A=>B");
            gm.guideModuleType = ENUM.GuidedModuleType.AlignmentLevelling;
            gm.ObservationType = Common.ObservationType.Level;
            // Invisible step: : Add needed station modules
            gm.SwitchToANewStationModule(new TSU.Level.Station.Module(gm));

            BuildSteps(gm);

            gm.Start();

            return gm;
        }

        private static void BuildSteps(Common.Guided.Module gm)
        {
            ////// Step 0: Choose Management.Instrument
            //gm.Steps.Add(Step.Level.ChooseAlignmentLevel(gm));

            ////// Step 1: Choose default staff
            //gm.Steps.Add(Step.Level.ChooseAlignmentDefaultStaff(gm));

            /// Step 0 : Choose Instrument and staffs
            gm.Steps.Add(Measurement.ChooseAdministrativeparametersForAlignmentLevel(gm));

            /// Step 1 : Choose Cala Points
            gm.Steps.Add(Measurement.ChooseCalaPointsForElementAlignment(gm));

            /// Step 2 : Mesures alignement aller
            gm.Steps.Add(Measurement.MeasureAlignementAller(gm));

            /// Step 3 : Mesures retour
            gm.Steps.Add(Measurement.MeasureAlignementRetour(gm));

            /// Step 4 : Mesures reprise
            gm.Steps.Add(Measurement.MeasureReprise(gm));

            /// Step 5 : Show dat saved file
            gm.Steps.Add(Measurement.ShowDatFiles(gm));

            /// Step 6 : Show LGC Files aller
            gm.Steps.Add(Measurement.ShowAllerLGCFiles(gm));

            /// Step 7 : Show LGC Files retour
            gm.Steps.Add(Measurement.ShowRetourLGCFiles(gm));

            /// Step 8 : Show LGC Files reprise
            gm.Steps.Add(Measurement.ShowRepriseLGCFiles(gm));
        }

        internal static void ReCreateWhatIsNotSerialized(Common.Guided.Module gm)
        {
            gm.Steps = new List<Step>();
            TSU.Level.Station.Module st = gm._ActiveStationModule as TSU.Level.Station.Module;
            if (st != null) { st.View.moduleType = ENUM.ModuleType.Guided; }
            //gm.currentIndex = 0;
            BuildSteps(gm);
            //gm.Start();
            //gm.MoveToStep(gm.currentIndex);
        }
    }
}
