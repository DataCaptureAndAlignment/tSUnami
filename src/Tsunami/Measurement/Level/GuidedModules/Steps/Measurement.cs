﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TSU.Common.Elements;
using TSU.Common.Guided.Steps;
using TSU.Views;
using TSU.Views.Message;
using Drawing = System.Drawing;
using E = TSU.Common.Elements;
using F = System.Windows.Forms;
using I = TSU.Common.Instruments;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Level.GuidedModules.Steps
{
    public static class Measurement
    {
        /// <summary>
        /// Step mesures aller
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step MeasureAller(Common.Guided.Module guidedModule)
        {
            Station.Module st = guidedModule._ActiveStationModule as Station.Module;
            Step s = new Step(guidedModule, R.StringCheminement_OutwardMeas);
            bool firstUse = true;
            double na = TSU.Tsunami2.Preferences.Values.na;
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                //Remove title and description to have more space for the measurements
                e.step.View.ClearAllControls();
                st = e.ActiveStationModule as Station.Module;
                st.View.MaximizeDataGridView();
            };
            s.Entering += delegate (object source, StepEventArgs e)
            {
                st = e.ActiveStationModule as Station.Module;
                st.View.ChangeModuleType(ENUM.ModuleType.Guided);
                st.ChangeLevelingDirectionToAller();
                guidedModule.Utility = st.Utility;
                st.View.showAdmin = true;
                st.View._AllerRetourRepriseViewStrategy.InitializeTreeviewParameters(st.View);
                st.View.dataGridViewLevel.Columns[0].Visible = false;
                st.View.ResetSplitterDNA();
                st.View.UpdateListMeasure();
                //st.View.bigbuttonTop.Update();
                //st.View.UpdateView();
                e.step.View.AddControl(st);

            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                e.step.Test();
                st = e.ActiveStationModule as Station.Module;
                guidedModule.Utility = st.Utility;
                //To set the scroll bar to left and at the top
                if (firstUse) st.View.SetScrollBarAtZero();
                firstUse = false;
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                e.step.CheckValidityOf(stationModule._WorkingStationLevel._Parameters._State is Common.Station.State.StationLevelSaved);
            };
            s.Changed += delegate (object source, StepEventArgs e)
            {
                //TSU.Level.Station.Module stationModule = e.ActiveStationModule as TSU.Level.Station.Module;
                //stationModule.View.Timer_Keep_Cell_Focus.Enabled = true;
                //stationModule.View.Timer_Keep_Cell_Focus.Start();
            };
            //Leaving() is triggered by Leave()
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.View.dataGridViewLevel.CancelEdit();
                if (e.step.State == StepState.ReadyToSkip && stationModule._WorkingStationLevel._Parameters._State is Common.Station.State.StationLevelReadyToBeSaved)
                {
                    st = e.ActiveStationModule as Station.Module;
                    st.View._AllerRetourRepriseViewStrategy.ShowMessageSave(st.View);
                }
            };
            return s;
        }
        internal static Step MeasureAlignementAller(Common.Guided.Module guidedModule)
        {
            Step s = MeasureAller(guidedModule);
            double na = TSU.Tsunami2.Preferences.Values.na;
            s.Updating += delegate (object source, StepEventArgs e)
            {
                Common.Guided.Group.Module GrGm = e.guidedModule.ParentModule as Common.Guided.Group.Module;
                Station.Module st = guidedModule._ActiveStationModule as Station.Module;
                // L'admin module est toujours le premier dans la liste des guided modules lors d'un alignement d'aimant
                // Le step 3 est step avec les résultats d'alignement qu'il faut mettre à jour
                if (GrGm.SubGuidedModules[0].currentIndex == 3)
                {
                    GrGm.SubGuidedModules[0].CurrentStep.Update();
                }
                ///Affiche le module alignment levelling check seulement si on a encodé des mesures en nivellement alignement
                if (st._AllerStationLevel._MeasureOfLevel.FindIndex(x => x._RawLevelReading != na) != -1)
                {
                    if (GrGm.SubGuidedModules[6].View != null)
                    {
                        GrGm.SubGuidedModules[6].View.Visible = true;
                    }
                }
            };
            return s;
        }
        /// <summary>
        /// Step simplifié pour le cheminement aller
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step MeasureCheminementAller(Common.Guided.Module guidedModule)
        {
            Step s = MeasureAller(guidedModule);
            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                //Cache colonnes CALA, HTheo, HMes, lect theo et deplacement
                st.View.dataGridViewLevel.Columns[2].Visible = false;
                st.View.dataGridViewLevel.Columns[5].Visible = false;
                st.View.dataGridViewLevel.Columns[6].Visible = false;
                st.View.dataGridViewLevel.Columns[11].Visible = false;
                st.View.dataGridViewLevel.Columns[12].Visible = false;
            };
            return s;
        }
        /// <summary>
        /// step mesures retour
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step MeasureRetour(Common.Guided.Module guidedModule)
        {
            Station.Module st = guidedModule._ActiveStationModule as Station.Module;
            Step s = new Step(guidedModule, R.StringCheminement_ReturnMeas);
            bool firstUse = true;
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                //Remove title and description to have more space for the measurements
                e.step.View.ClearAllControls();
                st = e.ActiveStationModule as Station.Module;
                st._RetourStationLevel.ParametersBasic._GeodeDatFilePath = "";
                st.View.MaximizeDataGridView();
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                st = e.ActiveStationModule as Station.Module;
                if (!st.retourDone)
                {
                    st.View.ChangeModuleType(ENUM.ModuleType.Guided);
                    st.ResetAndCreateRetourAndReprise();
                }
                st.ChangeLevelingDirectionToRetour();
                guidedModule.Utility = st.Utility;
                st.View.showAdmin = true;
                st.View._AllerRetourRepriseViewStrategy.InitializeTreeviewParameters(st.View);
                st.View.dataGridViewLevel.Columns[0].Visible = false;
                st.View.ResetSplitterDNA();
                st.View.UpdateListMeasure();
                //st.View.bigbuttonTop.Update();
                //st.View.UpdateView();
                e.step.View.AddControl(st);

            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                e.step.Test();
                st = e.ActiveStationModule as Station.Module;
                guidedModule.Utility = st.Utility;
                //To set the scroll bar to left and at the top
                if (firstUse) st.View.SetScrollBarAtZero();
                firstUse = false;
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                e.step.CheckValidityOf(stationModule._WorkingStationLevel._Parameters._State is Common.Station.State.StationLevelSaved);
            };
            s.Changed += delegate (object source, StepEventArgs e)
            {
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.View.dataGridViewLevel.Focus();

            };
            //Leaving() is triggered by Leave()
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.View.dataGridViewLevel.CancelEdit();
                if (e.step.State == StepState.ReadyToSkip && stationModule._WorkingStationLevel._Parameters._State is Common.Station.State.StationLevelReadyToBeSaved)
                {
                    st = e.ActiveStationModule as Station.Module;
                    st.View._AllerRetourRepriseViewStrategy.ShowMessageSave(st.View);
                }
            };
            return s;
        }
        /// <summary>
        /// step mesures retour
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step MeasureReprise(Common.Guided.Module guidedModule)
        {
            Station.Module st = guidedModule._ActiveStationModule as Station.Module;
            Step s = new Step(guidedModule, R.StringCheminement_RepeatMeas);
            bool firstUse = true;
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                //Remove title and description to have more space for the measurements
                e.step.View.ClearAllControls();
                st = e.ActiveStationModule as Station.Module;
                st._RepriseStationLevel[st._RepriseStationLevel.Count - 1].ParametersBasic._GeodeDatFilePath = "";
                st.View.MaximizeDataGridView();
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                st = e.ActiveStationModule as Station.Module;
                st.View.ChangeModuleType(ENUM.ModuleType.Guided);
                st.ChangeLevelingDirectionToReprise(1);
                guidedModule.Utility = st.Utility;
                st.View.showAdmin = true;
                st.View._AllerRetourRepriseViewStrategy.InitializeTreeviewParameters(st.View);
                st.View.ResetSplitterDNA();
                st.View.UpdateListMeasure();
                //st.View.bigbuttonTop.Update();
                //st.View.UpdateView();
                e.step.View.AddControl(st);

            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                e.step.Test();
                st = e.ActiveStationModule as Station.Module;
                guidedModule.Utility = st.Utility;
                if (firstUse) st.View.SetScrollBarAtZero();
                firstUse = false;
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                e.step.CheckValidityOf(stationModule._WorkingStationLevel._Parameters._State is Common.Station.State.StationLevelSaved);
            };
            s.Changed += delegate (object source, StepEventArgs e)
            {
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.View.dataGridViewLevel.Focus();
            };
            //Leaving() is triggered by Leave()
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.View.dataGridViewLevel.CancelEdit();
                if (e.step.State == StepState.ReadyToSkip && stationModule._WorkingStationLevel._Parameters._State is Common.Station.State.StationLevelReadyToBeSaved)
                {
                    st = e.ActiveStationModule as Station.Module;
                    st.View._AllerRetourRepriseViewStrategy.ShowMessageSave(st.View);
                }
            };
            return s;
        }
        /// <summary>
        /// Step mesure cheminement reprise cache les colonnes CALA, H theo, H Mes, lect theo et déplacement
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step MeasureCheminementReprise(Common.Guided.Module guidedModule)
        {
            Step s = MeasureReprise(guidedModule);
            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                //Cache colonnes CALA, HTheo, HMes, lect theo et deplacement
                st.View.dataGridViewLevel.Columns[2].Visible = false;
                st.View.dataGridViewLevel.Columns[5].Visible = false;
                st.View.dataGridViewLevel.Columns[6].Visible = false;
                st.View.dataGridViewLevel.Columns[11].Visible = false;
                st.View.dataGridViewLevel.Columns[12].Visible = false;
            };
            return s;
        }
        /// <summary>
        /// step mesures retour pour cheminement : enlève le step du choix clonage si reprise voulue
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step MeasureCheminementRetour(Common.Guided.Module guidedModule)
        {
            Station.Module st = guidedModule._ActiveStationModule as Station.Module;
            Step s = MeasureRetour(guidedModule);
            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                //Cache colonnes CALA, HTheo, HMes, lect theo et deplacement
                st.View.dataGridViewLevel.Columns[2].Visible = false;
                st.View.dataGridViewLevel.Columns[5].Visible = false;
                st.View.dataGridViewLevel.Columns[6].Visible = false;
                st.View.dataGridViewLevel.Columns[11].Visible = false;
                st.View.dataGridViewLevel.Columns[12].Visible = false;
            };
            return s;
        }
        /// <summary>
        /// step mesures retour
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step MeasureAlignementRetour(Common.Guided.Module guidedModule)
        {
            Station.Module st = guidedModule._ActiveStationModule as Station.Module;
            double na = TSU.Tsunami2.Preferences.Values.na;
            Step s = MeasureRetour(guidedModule);
            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };

            s.Entering += delegate (object source, StepEventArgs e)
            {

            };
            s.Updating += delegate (object source, StepEventArgs e)
            {

            };
            s.Testing += delegate (object source, StepEventArgs e)
            {

            };
            //Leaving() is triggered by Leave()
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                if (e.step.State != StepState.NotReady && stationModule._RetourStationLevel._Parameters._EmqAller > 0.00007
                    && stationModule._RetourStationLevel._Parameters._EmqAller != na)
                {
                    string titleAndMessage = string.Format(R.StringLevel_BadEmqReturnShowing, Math.Round(stationModule._RetourStationLevel._Parameters._EmqAller * 1000, 2).ToString());
                    new MessageInput(MessageType.Warning, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_OK + "!" }
                    }.Show();
                }
            };
            return s;
        }
        ///// <summary>
        ///// Create a empty new guided module or a new guided module with the same instrument, theoretical file, same operation, same team and go to select element step
        ///// </summary>
        ///// <param name="guidedModule"></param>
        ///// <returns></returns>
        //internal static Step CloneLevelStation(Module guidedModule)
        //{
        //    Step s = new Step(guidedModule, R.StringCheminement_CloneOptions);

        //    s.Entering += delegate(object source, StepEventArgs e)
        //    {
        //        e.step.View.ClearAllControls();
        //        TSU.Level.Station.Module stationLevelModule = e.ActiveStationModule as TSU.Level.Station.Module;
        //        // Ne permet de faire le retour de la station avant qu'après avoir fait le retour. 
        //        int indexStepAller = e.guidedModule.Steps.FindIndex(x => x._Name == Compute.Conversions.SeparateTitleFromMessage(R.StringCheminement_OutwardMeas));
        //        if (indexStepAller != -1)
        //        {
        //            if (indexStepAller != e.guidedModule.currentIndex - 1 && (guidedModule.ParentModule as Guided.Group.Module).SubGuidedModules.IndexOf(guidedModule)!=0)
        //            {
        //                /// bouton faire retour de la station precédente
        //                e.step.View.AddButton(
        //                   R.StringCheminement_DoReturnPreviousStation,
        //                   R.Level_Retour,
        //                    new EventHandler<TsuObjectEventArgs>(delegate(object source2, TsuObjectEventArgs args2)
        //                    {
        //                        Guided.Module gm = e.guidedModule;
        //                        Guided.Group.Module grGm = gm.ParentModule as Guided.Group.Module;
        //                        TSU.Level.Station.Module levelStationModule = gm._ActiveStationModule as TSU.Level.Station.Module;
        //                        int i = grGm.SubGuidedModules.IndexOf(gm);
        //                        if (i > 0)
        //                        {
        //                            grGm.SetActiveGuidedModule(grGm.SubGuidedModules[i - 1]);
        //                            grGm.ActiveSubGuidedModules.MoveToStep(12);
        //                        }
        //                        else
        //                        {
        //                            gm._ActiveStationModule.View.ShowMessageOfExclamation(R.StringCheminement_ErrorPreviousReturnStation,R.T_OK);
        //                        }
        //                    }));
        //            }
        //        }
        //        ///Bouton clone de la station sans choisir les points
        //        e.step.View.AddButton(
        //           R.StringCheminement_CloneModuleParam,
        //           R.Copy,
        //            new EventHandler<TsuObjectEventArgs>(delegate(object source2, TsuObjectEventArgs args2)
        //            {
        //                Guided.Module oldGm = e.guidedModule;
        //                Guided.Group.Module grGm = oldGm.ParentModule as Guided.Group.Module;
        //                grGm.AddNewLevellingModule();
        //                G.Module newGm = grGm.ActiveSubGuidedModules;
        //                newGm._ElementManager.AllElements = oldGm._ElementManager.AllElements;
        //                TSU.Level.Station.Module newStationModule = newGm._ActiveStationModule as TSU.Level.Station.Module;
        //                TSU.Level.Station.Module oldStationModule = oldGm._ActiveStationModule as TSU.Level.Station.Module;
        //                newStationModule.ChangeOperationID(oldStationModule._AllerStationLevel.ParametersBasic._Operation);
        //                newStationModule.ChangeTeam(oldStationModule._AllerStationLevel.ParametersBasic._Team);
        //                newStationModule.SetGeodeFilePathInNewStationLevelModule(oldStationModule);
        //                newStationModule.SetInstrumentInNewStationLevelModule(oldStationModule);
        //                newStationModule.SetDefaultStaffInNewStationLevelModule(oldStationModule);
        //                newStationModule.SetSecondaryStaffInNewStationLevelModule(oldStationModule);
        //                newStationModule.SetCoordinateSystemInNewStationLevelModule(oldStationModule);
        //                newStationModule.View.MaximizeDataGridView();
        //                newGm.MoveToStep(7);
        //            }));
        //        /////Bouton clone de la station en choissisant les points mesurés
        //        ///Remplacé par le calcul LGC global
        //        //e.step.View.AddButton(
        //        //   R.StringCheminement_CloneAndUseCoordinate,
        //        //   R.Copy,
        //        //    new EventHandler<TsuObjectEventArgs>(delegate(object source2, TsuObjectEventArgs args2)
        //        //    {
        //        //        Guided.Module oldGm = e.guidedModule;
        //        //        Guided.Group.Module grGm = oldGm.ParentModule as Guided.Group.Module;
        //        //        grGm.AddNewLevellingModule();
        //        //        G.Module newGm = grGm.ActiveSubGuidedModules;
        //        //        newGm._ElementManager.AllElements = oldGm._ElementManager.AllElements;
        //        //        TSU.Level.Station.Module newStationModule = newGm._ActiveStationModule as TSU.Level.Station.Module;
        //        //        TSU.Level.Station.Module oldStationModule = oldGm._ActiveStationModule as TSU.Level.Station.Module;
        //        //        newStationModule.ChangeOperationID(oldStationModule._AllerStationLevel.ParametersBasic._Operation);
        //        //        newStationModule.ChangeTeam(oldStationModule._AllerStationLevel.ParametersBasic._Team);
        //        //        newStationModule.SetGeodeFilePathInNewStationLevelModule(oldStationModule);
        //        //        newStationModule.SetInstrumentInNewStationLevelModule(oldStationModule);
        //        //        newStationModule.SetDefaultStaffInNewStationLevelModule(oldStationModule);
        //        //        newStationModule.SetOldStationLevelMeasPointIntoNewStationLevelModule(oldStationModule);
        //        //        newStationModule.SetOldStaffInToNewStationModule(oldStationModule);
        //        //        newStationModule.View.MaximizeDataGridView();
        //        //        newGm.MoveToStep(8);
        //        //    }));
        //        /// bouton aller à la sequence suivante
        //        if (stationLevelModule._SequenceSelected.Elements.Count != 0)
        //        {
        //            e.step.View.AddButton(
        //           R.StringCheminement_NextStationInSequence,
        //           R.Level,
        //            new EventHandler<TsuObjectEventArgs>(delegate(object source2, TsuObjectEventArgs args2)
        //            {
        //                Guided.Module oldGm = e.guidedModule;
        //                Guided.Group.Module grGm = oldGm.ParentModule as Guided.Group.Module;
        //                grGm.AddNewLevellingModule();
        //                G.Module newGm = grGm.ActiveSubGuidedModules;
        //                newGm._ElementManager.AllElements = oldGm._ElementManager.AllElements;
        //                TSU.Level.Station.Module newStationModule = newGm._ActiveStationModule as TSU.Level.Station.Module;
        //                TSU.Level.Station.Module oldStationModule = oldGm._ActiveStationModule as TSU.Level.Station.Module;
        //                newStationModule.ChangeOperationID(oldStationModule._AllerStationLevel.ParametersBasic._Operation);
        //                newStationModule.ChangeTeam(oldStationModule._AllerStationLevel.ParametersBasic._Team);
        //                newStationModule.SetGeodeFilePathInNewStationLevelModule(oldStationModule);
        //                newStationModule.SetInstrumentInNewStationLevelModule(oldStationModule);
        //                newStationModule.SetDefaultStaffInNewStationLevelModule(oldStationModule);
        //                newStationModule.SetSecondaryStaffInNewStationLevelModule(oldStationModule);
        //                newStationModule.SetCoordinateSystemInNewStationLevelModule(oldStationModule);
        //                newStationModule.SetNextElementSequenceInNewStationModule(oldStationModule);
        //                newStationModule.View.MaximizeDataGridView();
        //                newGm.MoveToStep(8);
        //            }));
        //            /// bouton aller à la sequence précédente
        //            e.step.View.AddButton(
        //           R.StringCheminement_PreviousStationInSequence,
        //           R.Level,
        //            new EventHandler<TsuObjectEventArgs>(delegate(object source2, TsuObjectEventArgs args2)
        //            {
        //                Guided.Module oldGm = e.guidedModule;
        //                Guided.Group.Module grGm = oldGm.ParentModule as Guided.Group.Module;
        //                grGm.AddNewLevellingModule();
        //                G.Module newGm = grGm.ActiveSubGuidedModules;
        //                newGm._ElementManager.AllElements = oldGm._ElementManager.AllElements;
        //                TSU.Level.Station.Module newStationModule = newGm._ActiveStationModule as TSU.Level.Station.Module;
        //                TSU.Level.Station.Module oldStationModule = oldGm._ActiveStationModule as TSU.Level.Station.Module;
        //                newStationModule.ChangeOperationID(oldStationModule._AllerStationLevel.ParametersBasic._Operation);
        //                newStationModule.ChangeTeam(oldStationModule._AllerStationLevel.ParametersBasic._Team);
        //                newStationModule.SetGeodeFilePathInNewStationLevelModule(oldStationModule);
        //                newStationModule.SetInstrumentInNewStationLevelModule(oldStationModule);
        //                newStationModule.SetDefaultStaffInNewStationLevelModule(oldStationModule);
        //                newStationModule.SetSecondaryStaffInNewStationLevelModule(oldStationModule);
        //                newStationModule.SetCoordinateSystemInNewStationLevelModule(oldStationModule);
        //                newStationModule.SetPreviousElementSequenceInNewStationModule(oldStationModule);
        //                newStationModule.View.MaximizeDataGridView();
        //                newGm.MoveToStep(8);
        //            }));
        //        }
        //        e.step.View.AddButton(
        //       R.T210,
        //       R.Add, new EventHandler<TsuObjectEventArgs>(delegate(object source2, TsuObjectEventArgs args2)
        //        {
        //            Guided.Group.Module grGm = e.guidedModule.ParentModule as Guided.Group.Module;
        //            grGm.AddNewLevellingModule();
        //        }));
        //        foreach (var item in e.step.View.Controls)
        //        {
        //            if (item is System.Windows.Forms.Label)
        //            {
        //                System.Windows.Forms.Label label = item as System.Windows.Forms.Label;
        //                if (label.Name == "Title")
        //                {
        //                    label.Text = stationLevelModule.utility;
        //                }
        //                if (label.Name == "Description")
        //                {
        //                    label.Text = "";
        //                }
        //            }
        //        }
        //    };
        //    s.Updating += delegate(object source, StepEventArgs e)
        //    {
        //        e.step.Test();
        //    };
        //    s.Testing += delegate(object source, StepEventArgs e)
        //    {
        //    };
        //    //Leaving() is triggered by Leave()
        //    s.Leaving += delegate(object source, StepEventArgs e)
        //    {
        //        //guidedModule.ParentModule.Change();
        //    };
        //    return s;
        //}
        ///// <summary>
        ///// Set the meas Point from the old station level module into the new station level module as CALA points
        ///// </summary>
        ///// <param name="newStationModule"></param>
        ///// <param name="oldStationModule"></param>
        //private static void SetOldStationLevelMeasPointIntoNewStationLevelModule(TSU.Level.Station.Module newStationModule, TSU.Level.Station.Module oldStationModule)
        //{
        //    oldStationModule.SetMeasPointsInElementManager(newStationModule.ElementModule);
        //    newStationModule.ElementModule.ValidateSelection();
        //    foreach (MeasureOfLevel item in newStationModule._AllerStationLevel._MeasureOfLevel)
        //    {
        //        item._Point.LGCFixOption = ENUM.LgcPointOption.CALA;
        //    }
        //}

        //private static void SetNextElementSequenceInNewStationModule(TSU.Level.Station.Module newStationModule, TSU.Level.Station.Module oldStationModule)
        //{
        //    oldStationModule.SetMeasPointsInElementManager(newStationModule.ElementModule);
        //    newStationModule.ElementModule._SelectedObjects.Clear();
        //    newStationModule.ElementModule.SetSelectableToAllSequenceNiv();
        //    //Coche les points déjà sélectionné dans l'element module
        //    if (oldStationModule._SequenceSelected != null && newStationModule.ElementModule.SelectableObjects != null)
        //    {
        //        int i = newStationModule.ElementModule.SelectableObjects.FindIndex(x => x._Name == oldStationModule._SequenceSelected._Name);
        //        i++;
        //        if (i < newStationModule.ElementModule.SelectableObjects.Count && i != -1)
        //        {
        //            newStationModule.ElementModule._SelectedObjects.Add(newStationModule.ElementModule.SelectableObjects[i]);
        //        }
        //        else
        //        {
        //            newStationModule.View.ShowMessageOfExclamation(R.StringLevel_ErrorNextSequence,R.T_OK);
        //        }
        //    }
        //    else
        //    {
        //        newStationModule.View.ShowMessageOfExclamation(R.StringLevel_ErrorNextSequence,R.T_OK);
        //    }
        //    newStationModule.ElementModule.ValidateSelection();
        //}
        ///// <summary>
        ///// Set the Geode file path from the old station level Module into the new station level module
        ///// </summary>
        ///// <param name="newStationModule"></param>
        ///// <param name="oldStationModule"></param>
        //private static void SetGeodeFilePathInNewStationLevelModule(TSU.Level.Station.Module newStationModule, TSU.Level.Station.Module oldStationModule)
        //{
        //    newStationModule._AllerStationLevel._Parameters._GeodeDatFilePath = oldStationModule._AllerStationLevel._Parameters._GeodeDatFilePath;
        //    newStationModule._RetourStationLevel._Parameters._GeodeDatFilePath = oldStationModule._RetourStationLevel._Parameters._GeodeDatFilePath;
        //    if (newStationModule._RepriseStationLevel.Count > 0 && oldStationModule._RepriseStationLevel.Count > 0)
        //    {
        //        newStationModule._RepriseStationLevel[0]._Parameters._GeodeDatFilePath = oldStationModule._RepriseStationLevel[0]._Parameters._GeodeDatFilePath;
        //    }
        //}
        ///// <summary>
        ///// Set the elements from the old station module and select the previous sequence
        ///// </summary>
        ///// <param name="newStationModule"></param>
        ///// <param name="oldStationModule"></param>
        //private static void SetPreviousElementSequenceInNewStationModule(TSU.Level.Station.Module newStationModule, TSU.Level.Station.Module oldStationModule)
        //{
        //    oldStationModule.SetMeasPointsInElementManager(newStationModule.ElementModule);
        //    newStationModule.ElementModule._SelectedObjects.Clear();
        //    newStationModule.ElementModule.SetSelectableToAllSequenceNiv();
        //    //Coche les points déjà sélectionné dans l'element module
        //    if (oldStationModule._SequenceSelected.Elements.Count != 0 && newStationModule.ElementModule.SelectableObjects != null)
        //    {
        //        int i = newStationModule.ElementModule.SelectableObjects.FindIndex(x => x._Name == oldStationModule._SequenceSelected._Name);
        //        i--;
        //        if (i >= 0 && i != -1)
        //        {
        //            newStationModule.ElementModule._SelectedObjects.Add(newStationModule.ElementModule.SelectableObjects[i]);
        //        }
        //        else
        //        {
        //            newStationModule.View.ShowMessageOfExclamation(R.StringLevel_ErrorPreviousSequence,R.T_OK);
        //        }
        //    }
        //    else
        //    {
        //        newStationModule.View.ShowMessageOfExclamation(R.StringLevel_ErrorPreviousSequence,R.T_OK);
        //    }
        //    newStationModule.ElementModule.ValidateSelection();
        //}
        ///// <summary>
        ///// Set the staff from the old station module to the new one 
        ///// </summary>
        ///// <param name="newStationModule"></param>
        ///// <param name="oldStationModule"></param>
        //private static void SetStaffInNewStationLevelModule(TSU.Level.Station.Module newStationModule, TSU.Level.Station.Module oldStationModule)
        //{
        //    TSU.TsuObject selectedStaff = newStationModule.staffModule.AllElements.Find(x => x._Name == oldStationModule._defaultStaff._Name);
        //    newStationModule.staffModule._SelectedObjects.Clear();
        //    newStationModule.staffModule.AddSelectedObjects(selectedStaff);
        //    newStationModule.staffModule._SelectedObjectInBlue = selectedStaff;
        //    newStationModule.staffModule.ValidateSelection();
        //}
        ///// <summary>
        ///// Set the instrument from the old station module to the new one 
        ///// </summary>
        ///// <param name="newStationModule"></param>
        ///// <param name="oldStationModule"></param>
        //private static void SetInstrumentInNewStationLevelModule(TSU.Level.Station.Module newStationModule, TSU.Level.Station.Module oldStationModule)
        //{
        //    TSU.TsuObject selectedInstrument = newStationModule._InstrumentManager.AllElements.Find(x => x._Name == oldStationModule._AllerStationLevel.ParametersBasic._Instrument._Name);
        //    newStationModule._InstrumentManager._SelectedObjects.Clear();
        //    newStationModule._InstrumentManager.AddSelectedObjects(selectedInstrument);
        //    newStationModule._InstrumentManager._SelectedObjectInBlue = selectedInstrument;
        //    newStationModule._InstrumentManager.ValidateSelection();
        //}
        /// <summary>
        /// STep to choose points of the wire, keep only the theoretical points
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step ChoosePoints(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.T251);
            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.ElementManager.MultiSelection = true;
                e.ElementManager.View.CheckBoxesVisible = true;
                e.ElementManager.SetSelectableToAllUniquePointForGuidedModule();
                //List<BigButton> buttonToHide = new List<BigButton>();
                //buttonToHide.Add(e.ElementManager.View.buttons.compute);
                //buttonToHide.Add(e.ElementManager.View.buttons.open);
                //buttonToHide.Add(e.ElementManager.View.buttons.sequence);
                //e.ElementManager.View.buttons.HideOnlyButtons(buttonToHide);
                //e.ElementManager.View.buttons.ReleaseAll();
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.View.ChangeModuleType(ENUM.ModuleType.Guided);
                e.step.View.AddControl(e.ElementManager);
                e.ElementManager.View.buttons.ShowTypes.Available = true;
                e.ElementManager.View.buttons.ShowPiliers.Available = true;
                e.ElementManager.View.buttons.ShowPoints.Available = true;
                e.ElementManager.View.buttons.ShowMagnets.Available = true;
                e.ElementManager.View.buttons.ShowAll.Available = true;
                e.ElementManager.View.buttons.ShowAlesages.Available = true;
                e.ElementManager.View.buttons.clean.Available = false;
                e.ElementManager.View.buttons.importRefenceFile.Available = true;

                e.ElementManager.View.buttons.add.Available = true;
                e.step.Test();
            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                e.step.Test();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                e.step.MessageFromTest = R.String_GM_MsgBlock1Point;
                e.step.CheckValidityOf(e.ElementManager.GetPointsInSelectedObjects().Count() >= 1);
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                //e.ElementManager.ValidateSelection();
                e.step.View.RemoveControl(e.ElementManager);

            };
            return s;
        }
        /// <summary>
        /// Step to choose the theoretical file for levelling with only showing theoretical points
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        static public Step ChooseTheoreticalFile(Common.Guided.Module guidedModule, bool skipStepsCompleted = false)
        {
            Step s = Management.ChooseTheoreticalFile(guidedModule);

            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };
            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.ElementManager.SetSelectableToAllTheoreticalPoints();
                e.ElementManager.View.buttons.HideAllButtons();
            };
            return s;
        }
        /// <summary>
        /// Step to choose the sequence file for levelling
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        static public Step ChooseSequenceFile(Common.Guided.Module guidedModule, bool skipStepsCompleted = false)
        {
            Step s = Management.ChooseSequenceFile(guidedModule);

            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };
            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.ElementManager.SetSelectableToAllSequenceNiv();
                e.ElementManager.View.buttons.HideAllButtons();
                e.ElementManager.View.UpdateView();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                e.step.CheckValidityOf(e.ElementManager.GetAllSequenceNiv().Count > 0);
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                if (e.ElementManager.GetAllSequenceNiv().Count == 0)
                {
                    guidedModule.Steps[7].Visible = false;
                }
                else
                {
                    guidedModule.Steps[7].Visible = true;
                }
            };
            return s;
        }


        /// <summary>
        /// Step to show the aller station LGC files
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        static public Step ShowAllerLGCFiles(Common.Guided.Module guidedModule, bool skipStepsCompleted = false)
        {
            Step s = new Step(guidedModule, R.StringCheminement_ShowLGCAller);
            s._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringCheminement_ShowLGCAller);
            s.Utility = T.Conversions.StringManipulation.SeparateMessageFromTitle(R.StringCheminement_ShowLGCAller);
            List<Process> processList = new List<Process>();
            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.ChangeLevelingDirectionToAller();
                if (processList.Count == 0)
                {
                    if (stationModule._AllerStationLevel.ParametersBasic._LGCInputFilePath != "" && stationModule._AllerStationLevel.ParametersBasic._LGCOutputFilePath != "")
                    {
                        if (System.IO.File.Exists(stationModule._AllerStationLevel.ParametersBasic._LGCInputFilePath) && System.IO.File.Exists(stationModule._AllerStationLevel.ParametersBasic._LGCOutputFilePath))
                        {
                            processList = s.View.AddLGCFilesPanel(stationModule._AllerStationLevel.ParametersBasic._LGCInputFilePath, stationModule._AllerStationLevel.ParametersBasic._LGCOutputFilePath);
                        }
                        else
                        {
                            new MessageInput(MessageType.Warning, R.StringGuided_NoLGCFiles).Show();
                        }
                    }
                    else
                    {
                        new MessageInput(MessageType.Warning, R.StringGuided_NoLGCFiles).Show();
                    }
                }
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                foreach (Process process in processList)
                {
                    process.CloseMainWindow();
                }
                processList.Clear();
            };
            return s;
        }
        /// <summary>
        /// Step to show the retour station LGC files
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        static public Step ShowRetourLGCFiles(Common.Guided.Module guidedModule, bool skipStepsCompleted = false)
        {
            Step s = new Step(guidedModule, R.StringCheminement_ShowLGCRetour);
            s._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringCheminement_ShowLGCRetour);
            s.Utility = T.Conversions.StringManipulation.SeparateMessageFromTitle(R.StringCheminement_ShowLGCRetour);
            List<Process> processList = new List<Process>();
            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.ChangeLevelingDirectionToRetour();
                if (processList.Count == 0)
                {
                    if (stationModule._RetourStationLevel.ParametersBasic._LGCInputFilePath != "" && stationModule._RetourStationLevel.ParametersBasic._LGCOutputFilePath != "")
                    {
                        if (System.IO.File.Exists(stationModule._RetourStationLevel.ParametersBasic._LGCInputFilePath) && System.IO.File.Exists(stationModule._RetourStationLevel.ParametersBasic._LGCOutputFilePath))
                        {
                            processList = s.View.AddLGCFilesPanel(stationModule._RetourStationLevel.ParametersBasic._LGCInputFilePath, stationModule._RetourStationLevel.ParametersBasic._LGCOutputFilePath);
                        }
                        else
                        {
                            new MessageInput(MessageType.Warning, R.StringGuided_NoLGCFiles).Show();
                        }

                    }
                    else
                    {
                        new MessageInput(MessageType.Warning, R.StringGuided_NoLGCFiles).Show();
                    }
                }
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                foreach (Process process in processList)
                {
                    process?.CloseMainWindow();
                }
                processList.Clear();
            };
            return s;
        }
        /// <summary>
        /// Step to show the reprise station LGC files
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        static public Step ShowRepriseLGCFiles(Common.Guided.Module guidedModule, bool skipStepsCompleted = false)
        {
            Step s = new Step(guidedModule, R.StringCheminement_ShowLGCReprise);
            s._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringCheminement_ShowLGCReprise);
            s.Utility = T.Conversions.StringManipulation.SeparateMessageFromTitle(R.StringCheminement_ShowLGCReprise);
            List<Process> processList = new List<Process>();
            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.ChangeLevelingDirectionToReprise(stationModule._NumeroReprise);
                if (processList.Count == 0)
                {
                    if (stationModule._RepriseStationLevel[stationModule._NumeroReprise - 1].ParametersBasic._LGCInputFilePath != ""
                    && stationModule._RepriseStationLevel[stationModule._NumeroReprise - 1].ParametersBasic._LGCOutputFilePath != "")
                    {
                        if (System.IO.File.Exists(stationModule._RepriseStationLevel[stationModule._NumeroReprise - 1].ParametersBasic._LGCInputFilePath)
                        && System.IO.File.Exists(stationModule._RepriseStationLevel[stationModule._NumeroReprise - 1].ParametersBasic._LGCOutputFilePath))
                        {
                            processList = s.View.AddLGCFilesPanel(stationModule._RepriseStationLevel[stationModule._NumeroReprise - 1].ParametersBasic._LGCInputFilePath, stationModule._RepriseStationLevel[stationModule._NumeroReprise - 1].ParametersBasic._LGCOutputFilePath);
                        }
                        else
                        {
                            new MessageInput(MessageType.Warning, R.StringGuided_NoLGCFiles).Show();
                        }

                    }
                    else
                    {
                        new MessageInput(MessageType.Warning, R.StringGuided_NoLGCFiles).Show();
                    }
                }
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                foreach (Process process in processList)
                {
                    process?.CloseMainWindow();
                }
                processList.Clear();
            };
            return s;
        }
        /// <summary>
        /// Choose sequence niv from theorietcal file, it uses a modified "choose elements" step 
        /// </summary>
        /// <param name="gm"></param>
        /// <returns></returns>
        internal static Step ChooseSequenceNiv(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.StringGuided_ChooseSequence);
            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };
            s.Entering += delegate (object source, StepEventArgs e)
            {
                e.ElementManager.MultiSelection = false;
                e.ElementManager.View.CheckBoxesVisible = true;
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.SetSequenceInElementManager();
                e.ElementManager.View.buttons.HideAllButtons();
                e.step.View.AddControl(e.ElementManager);
                e.ElementManager.UpdateView();
                e.ElementManager.View.buttons.ShowTypes.Available = false;
                e.ElementManager.View.buttons.ShowPiliers.Available = false;
                e.ElementManager.View.buttons.ShowPoints.Available = false;
                e.ElementManager.View.buttons.ShowMagnets.Available = false;
                e.ElementManager.View.buttons.ShowAll.Available = false;
                e.ElementManager.View.buttons.ShowAlesages.Available = false;
                e.ElementManager.View.buttons.clean.Available = false;
                e.ElementManager.View.buttons.sequence.Available = true;
                e.ElementManager.View.buttons.add.Available = false;
            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                e.step.Test();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                if (e.ElementManager._currentSetSelectableListType != E.Manager.Module.currentSetSelectableListType.setSelectableToAllSequenceNiv)
                {
                    e.ElementManager.SetSelectableToAllSequenceNiv();
                }
                e.step.CheckValidityOf(e.ElementManager._SelectedObjects.Count >= 1);
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                e.ElementManager.ValidateSelection();
                e.step.View.RemoveControl(e.ElementManager);
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.FinalModule.PointsToBeAligned.Clear();
                stationModule.FinalModule.CalaPoints.Clear();
                foreach (Point item in stationModule._TheoPoint)
                {
                    if (item.LGCFixOption == ENUM.LgcPointOption.VZ) { stationModule.FinalModule.PointsToBeAligned.Add(item.DeepCopy()); }
                    if (item.LGCFixOption == ENUM.LgcPointOption.CALA) { stationModule.FinalModule.CalaPoints.Add(item.DeepCopy()); }
                }
            };
            return s;
        }
        ///// <summary>
        ///// Select the default staff for the measurement
        ///// </summary>
        ///// <param name="gm"></param>
        ///// <returns></returns>
        //internal static Step ChooseDefaultStaff(Module guidedModule)
        //{
        //    Step s = new Step(guidedModule, R.StringCheminement_SelectDefaultStaff);
        //    s.FirstUse += delegate(object source, StepEventArgs e)
        //    {
        //        TSU.Level.Station.Module stationLevelModule = e.ActiveStationModule as TSU.Level.Station.Module;
        //        e.step.View.AddDescription(R.StringCheminement_DescriptionSelectStaff);
        //        e.step.View.AddControl(stationLevelModule.staffModule.View);
        //        stationLevelModule.staffModule.View.CheckBoxesVisible = false;
        //        stationLevelModule.staffModule.View.MultiSelection = false;
        //    };

        //    s.Entering += delegate(object source, StepEventArgs e)
        //    {
        //        TSU.Level.Station.Module stationLevelModule = e.ActiveStationModule as TSU.Level.Station.Module;
        //        stationLevelModule.staffModule._SelectedObjects.Clear();
        //        stationLevelModule.staffModule._SelectedObjectInBlue = stationLevelModule._defaultStaff;
        //        stationLevelModule.staffModule.AddSelectedObjects(stationLevelModule._defaultStaff);
        //        stationLevelModule.staffModule.UpdateView();
        //    };
        //    s.Updating += delegate(object source, StepEventArgs e)
        //    {
        //    };
        //    s.Testing += delegate(object source, StepEventArgs e)
        //    {
        //        TSU.Level.Station.Module stationLevelModule = e.ActiveStationModule as TSU.Level.Station.Module;
        //        e.step.MessageFromTest = R.String_GM_MsgBlockSelectStaff;
        //        e.step.skipable = false;
        //            e.step.CheckValidityOf(stationLevelModule.staffModule._SelectedObjects.Count == 1);


        //    };
        //    //Leaving() is triggered by Leave()
        //    s.Leaving += delegate(object source, StepEventArgs e)
        //    {
        //        TSU.Level.Station.Module stationLevelModule = e.ActiveStationModule as TSU.Level.Station.Module;
        //        stationLevelModule.staffModule.ValidateSelection();
        //    };
        //    return s;
        //}
        ///// Pour le module alignement levelling, met également la même mire pour le contrôle
        //internal static Step ChooseAlignmentDefaultStaff(Module guidedModule)
        //{
        //    Step s = ChooseDefaultStaff(guidedModule);
        //    //Leaving() is triggered by Leave()
        //    s.ControlChanged += delegate (object source, StepEventArgs e)
        //    {
        //        if (guidedModule.ParentModule is G.Group.Module)
        //        {
        //            G.Group.Module grGm = guidedModule.ParentModule as G.Group.Module;
        //            TSU.Level.Station.Module st = e.ActiveStationModule as TSU.Level.Station.Module;
        //            if (grGm.SubGuidedModules.Count >= 7)
        //            {
        //                if (grGm.SubGuidedModules[6].guideModuleType == ENUM.GuidedModuleType.AlignmentLevellingCheck)
        //                {
        //                    TSU.Level.Station.Module stationLevelModuleCheck = grGm.SubGuidedModules[6]._ActiveStationModule as TSU.Level.Station.Module;
        //                    stationLevelModuleCheck.staffModule._SelectedObjects.Clear();
        //                    stationLevelModuleCheck.staffModule._SelectedObjectInBlue = st.staffModule._SelectedObjectInBlue;
        //                    stationLevelModuleCheck.staffModule.AddSelectedObjects(st.staffModule._SelectedObjects);
        //                    stationLevelModuleCheck.staffModule.ValidateSelection();
        //                }
        //            }
        //        }
        //    };
        //    return s;
        //}
        /// <summary>
        /// Show the Geode dat File that has been recorded
        /// </summary>
        /// <param name="gm"></param>
        /// <returns></returns>
        internal static Step ShowDatFiles(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.String_Step_ShowDatFile);
            s._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.String_Step_ShowDatFile);
            s.Utility = T.Conversions.StringManipulation.SeparateMessageFromTitle(R.String_Step_ShowDatFile);
            Process process = null;
            BigButton buttonOpen = new BigButton();
            s.FirstUse += delegate (object source, StepEventArgs e)
            {

            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module stationLevelModule = e.ActiveStationModule as Station.Module;
                if (process == null)
                {
                    if (stationLevelModule._AllerStationLevel.ParametersBasic._GeodeDatFilePath != "")
                    {
                        if (System.IO.File.Exists(stationLevelModule._AllerStationLevel.ParametersBasic._GeodeDatFilePath))
                        {
                            buttonOpen = e.step.View.AddButton(
                                R.String_GM_OpendatFile,
                                R.Open,
                                delegate (object source2, TsuObjectEventArgs args2)
                                {
                                    Shell.Run(stationLevelModule._AllerStationLevel.ParametersBasic._GeodeDatFilePath);
                                }
                            );
                            process = s.View.AddShowDatFilePanel(stationLevelModule._AllerStationLevel.ParametersBasic._GeodeDatFilePath);
                        }
                        else
                        {
                            new MessageInput(MessageType.Warning, R.StringGuided_NoDatFile).Show();
                            process = null;
                        }
                    }
                    else
                    {
                        if (stationLevelModule._RetourStationLevel.ParametersBasic._GeodeDatFilePath != "")
                        {
                            if (System.IO.File.Exists(stationLevelModule._RetourStationLevel.ParametersBasic._GeodeDatFilePath))
                            {
                                buttonOpen = e.step.View.AddButton(
                                    R.String_GM_OpendatFile,
                                    R.Open,
                                    delegate (object source2, TsuObjectEventArgs args2)
                                    {
                                        Shell.Run(stationLevelModule._RetourStationLevel.ParametersBasic._GeodeDatFilePath);
                                    }
                                );
                                process = s.View.AddShowDatFilePanel(stationLevelModule._RetourStationLevel.ParametersBasic._GeodeDatFilePath);
                            }
                            else
                            {
                                new MessageInput(MessageType.Warning, R.StringGuided_NoDatFile).Show();
                                process = null;
                            }
                        }
                        else
                        {
                            if (stationLevelModule._RepriseStationLevel[stationLevelModule._NumeroReprise - 1].ParametersBasic._GeodeDatFilePath != "")
                            {
                                if (System.IO.File.Exists(stationLevelModule._RepriseStationLevel[stationLevelModule._NumeroReprise - 1].ParametersBasic._GeodeDatFilePath))
                                {
                                    buttonOpen = e.step.View.AddButton(
                                        R.String_GM_OpendatFile,
                                        R.Open,
                                        delegate (object source2, TsuObjectEventArgs args2)
                                        {
                                            Shell.Run(stationLevelModule._RepriseStationLevel[stationLevelModule._NumeroReprise - 1].ParametersBasic._GeodeDatFilePath);
                                        }
                                    );
                                    process = s.View.AddShowDatFilePanel(stationLevelModule._RepriseStationLevel[stationLevelModule._NumeroReprise - 1].ParametersBasic._GeodeDatFilePath);
                                }
                                else
                                {
                                    new MessageInput(MessageType.Warning, R.StringGuided_NoDatFile).Show();
                                    process = null;
                                }
                            }
                            else
                            {
                                new MessageInput(MessageType.Warning, R.StringGuided_NoDatFile).Show();
                                process = null;
                            }
                        }
                    }
                }

            };
            s.Updating += delegate (object source, StepEventArgs e)
            {

            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                if (process != null)
                {
                    process.CloseMainWindow();
                    e.step.View.Controls.Remove(buttonOpen);
                    e.step.View.ControlList.Remove(buttonOpen);
                    process = null;
                }
            };
            return s;
        }
        ///// <summary>
        ///// Allows to select an operation
        ///// </summary>
        ///// <param name="guidedModule"></param>
        ///// <returns></returns>
        //static public Step ChooseOperation(Module guidedModule)
        //{
        //    Step s = new Step(guidedModule,R.T249);

        //    // FirstUse() is triggered by UseForFirstTime()
        //    s.FirstUse += delegate(object source, StepEventArgs e)
        //    {
        //    };

        //    // Entering() is triggered by Enter()
        //    s.Entering += delegate(object source, StepEventArgs e)
        //    {
        //        //e.step.Change();
        //        TSU.Level.Station.Module stationLevelModule = e.ActiveStationModule as TSU.Level.Station.Module;
        //        e.step.View.AddControl(e.guidedModule.OperationManager.View);
        //        e.OperationManager.OperationTree.ExpandAll();
        //        e.OperationManager._SelectedObjects.Clear();
        //        O.Operation operation = (e.OperationManager.AllElements.Find(x => x._Name == stationLevelModule._AllerStationLevel.ParametersBasic._Operation._Name) as O.Operation);
        //        e.OperationManager._SelectedObjectInBlue = operation;
        //        e.OperationManager.AddSelectedObjects(operation);
        //        e.OperationManager.UpdateView();
        //    };

        //    // Changed() is triggered by Change()
        //    s.ControlChanged += delegate(object source, StepEventArgs e)
        //    {
        //        TSU.Level.Station.Module stationLevelModule = e.ActiveStationModule as TSU.Level.Station.Module;
        //        stationLevelModule.ChangeOperationID(e.OperationManager.SelectedOperation);
        //    };

        //    // Testing() is triggered by Test()
        //    s.Testing += delegate(object source, StepEventArgs e)
        //    {
        //        e.step.skipable = true;
        //        e.step.CheckValidityOf(e.OperationManager.SelectedOperation.IsSet);
        //    };
        //    s.Leaving += delegate(object source, StepEventArgs e)
        //    {
        //        e.step.View.RemoveControl(e.guidedModule.OperationManager.View);
        //    };
        //    return s;
        //}
        ///// <summary>
        ///// Allows to schoose the team
        ///// </summary>
        ///// <param name="guidedModule"></param>
        ///// <returns></returns>
        ///// 
        //static public Step EnterATeam(Module guidedModule)
        //{
        //    Step s = Management.EnterAString(guidedModule,R.T247, "AABB");

        //    s.FirstUse += delegate(object source, StepEventArgs e)
        //    {
        //        TSU.Level.Station.Module stationLevelModule = e.ActiveStationModule as TSU.Level.Station.Module;
        //        if (stationLevelModule._AllerStationLevel.ParametersBasic._Team !=R.String_UnknownTeam)
        //        {
        //            foreach (var item in e.step.View.Controls)
        //            {
        //                if (item is F.TextBox)
        //                {
        //                    (item as F.TextBox).Text = stationLevelModule._AllerStationLevel.ParametersBasic._Team;
        //                }
        //            }
        //        }
        //    };

        //    s.Entered += delegate(object source, StepEventArgs e)
        //    {
        //        Management.SetFocusOnTextBox(e.step.View.Controls.OfType<F.TextBox>());
        //    };
        //    s.Testing += delegate(object source, StepEventArgs e)
        //    {
        //        // feed the team parameter in the station
        //        string temp = e.step.TAG as string;
        //        //Vérifie que le nom d'équipe contient que des caractères alphanumériques et est de maximum 8 caractères
        //        if (System.Text.RegularExpressions.Regex.IsMatch(temp, @"^[a-zA-Z]+$") && (temp.Length < 9 || temp ==R.String_UnknownTeam))
        //        {
        //            TSU.Level.Station.Module stationLevelModule = e.ActiveStationModule as TSU.Level.Station.Module;
        //            temp.ToUpper();
        //            stationLevelModule._AllerStationLevel.ParametersBasic._Team = temp;
        //            stationLevelModule._RetourStationLevel.ParametersBasic._Team = temp;
        //            foreach (StationLevel item in stationLevelModule._RepriseStationLevel)
        //            {
        //                item.ParametersBasic._Team = temp;
        //            }
        //            e.step.Validate();
        //        }
        //        else
        //        {
        //            e.step.UnValidate();
        //        }
        //    };
        //    return s;
        //}
        ///// <summary>
        ///// STep to choose Cala points for levelling and add them in the Station module, keep only the theoretical points
        ///// </summary>
        ///// <param name="guidedModule"></param>
        ///// <returns></returns>
        //internal static Step ChooseCalaPoints(Module guidedModule)
        //{
        //    Step s = ChoosePoints(guidedModule);
        //    s._Name = Compute.Conversions.SeparateTitleFromMessage(R.StringCheminement_SelectReferencePoints);
        //    s.utility = Compute.Conversions.SeparateMessageFromTitle(R.StringCheminement_SelectReferencePoints);
        //    s.Entering += delegate(object source, StepEventArgs e)
        //    {
        //        TSU.Level.Station.Module stationModule = e.ActiveStationModule as TSU.Level.Station.Module;
        //        stationModule.SetTheoCalaPointsInElementManager();
        //        e.ElementManager.UpdateView();
        //    };
        //    s.Leaving += delegate(object source, StepEventArgs e)
        //    {
        //        T.CloneableList<Point> pointsList = e.ElementManager.GetPointsInSelectedObjects();
        //        TSU.Level.Station.Module stationModule = e.ActiveStationModule as TSU.Level.Station.Module;
        //        //Recupere tous les points de cala
        //        foreach (Point item in pointsList)
        //        {
        //            item.LGCFixOption = TSU.ENUM.LgcPointOption.CALA;
        //            int index = stationModule.FinalModule.PointsToBeAligned.FindIndex(x => x._Name == item._Name && x._Origin == item._Origin);
        //            if (index != -1)
        //            {
        //                //Dans le cas ou le point sélectionné en référence fait déjà partie des points à aligner
        //                stationModule.FinalModule.PointsToBeAligned.RemoveAt(index);
        //                stationModule.FinalModule.CalaPoints.Add(item);
        //                int index5=stationModule.FinalModule.PointsToBeMeasured.FindIndex(x => x._Name == item._Name && x._Origin == item._Origin);
        //                if (index5 != -1) { stationModule.FinalModule.PointsToBeMeasured[index5].LGCFixOption = TSU.ENUM.LgcPointOption.CALA; }
        //                int index1 = stationModule._TheoPoint.FindIndex(x => x._Name == item._Name && x._Origin == item._Origin);
        //                if (index1 != -1) { stationModule._TheoPoint[index1].LGCFixOption = TSU.ENUM.LgcPointOption.CALA; }
        //                int index3 = stationModule._AllerStationLevel._MeasureOfLevel.FindIndex(x => x._PointName == item._Name && x._Point._Origin == item._Origin);
        //                if (index3!=-1) { stationModule._AllerStationLevel._MeasureOfLevel[index3]._Point.LGCFixOption = TSU.ENUM.LgcPointOption.CALA; }
        //                int index4 = stationModule._RetourStationLevel._MeasureOfLevel.FindIndex(x => x._PointName == item._Name && x._Point._Origin == item._Origin);
        //                if (index4 != -1) { stationModule._RetourStationLevel._MeasureOfLevel[index4]._Point.LGCFixOption = TSU.ENUM.LgcPointOption.CALA; }
        //                foreach (Stations.StationLevel st in stationModule._RepriseStationLevel)
        //                {
        //                    int index2 = st._MeasureOfLevel.FindIndex(x => x._PointName == item._Name && x._Point._Origin == item._Origin);
        //                    if (index2 != -1) { st._MeasureOfLevel[index2]._Point.LGCFixOption = TSU.ENUM.LgcPointOption.CALA; }
        //                }

        //            }
        //        }
        //        //Si pas encore de station, il faut désigner un point qui doit être en cala
        //        if (stationModule._WorkingStationLevel._MeasureOfLevel.Count == 0)
        //        {
        //            stationModule.SetPointsToMeasure(pointsList.Clone(),false,false);
        //        }
        //        stationModule.FinalModule.CalaPoints = pointsList.Clone();
        //        pointsList.AddRange(stationModule.FinalModule.PointsToBeAligned.Clone());
        //        stationModule.UpdateCalaVSPointToBeAligned(pointsList);
        //        stationModule._WorkingStationLevel = stationModule._AllerStationLevel;
        //        stationModule.SetPointsToMeasure(pointsList.Clone(),false,true);
        //        stationModule.ResetAndCreateRetourAndReprise();
        //    };
        //    return s;
        //}
        /// <summary>
        /// Choisit les points de calage pour aligner les aimants en enlevant les aimants à aligner de la liste des points sélectionnables
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step ChooseCalaPointsForElementAlignment(Common.Guided.Module guidedModule)
        {
            Step s = ChoosePoints(guidedModule);
            s._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringCheminement_SelectReferencePoints);
            s.Utility = T.Conversions.StringManipulation.SeparateMessageFromTitle(R.StringCheminement_SelectReferencePoints);
            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.SetTheoCalaPointsInElementManager();
                if (stationModule.FinalModule.PointsToBeAligned != null && stationModule.ElementModule.SelectableObjects != null)
                {
                    foreach (Point item in stationModule.FinalModule.PointsToBeAligned)
                    {
                        TsuObject objectFound = stationModule.ElementModule.SelectableObjects.Find(x => (x as Point)._Name == item._Name);
                        if (objectFound != null)
                        {
                            stationModule.ElementModule.RemoveSelectableObjects(objectFound);
                        }
                    }
                }
                e.ElementManager.UpdateView();
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                CloneableList<Point> pointsList = e.ElementManager.GetPointsInSelectedObjects();
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                //Recupere tous les points de cala
                foreach (Point item in pointsList)
                {
                    item.LGCFixOption = ENUM.LgcPointOption.CALA;
                    int index = stationModule.FinalModule.PointsToBeAligned.FindIndex(x => x._Name == item._Name && x._Origin == item._Origin);
                    if (index != -1)
                    {
                        //Dans le cas ou le point sélectionné en référence fait déjà partie des points à aligner
                        stationModule.FinalModule.PointsToBeAligned.RemoveAt(index);
                        stationModule.FinalModule.CalaPoints.Add(item);
                        int index5 = stationModule.FinalModule.PointsToBeMeasured.FindIndex(x => x._Name == item._Name && x._Origin == item._Origin);
                        if (index5 != -1) { stationModule.FinalModule.PointsToBeMeasured[index5].LGCFixOption = ENUM.LgcPointOption.CALA; }
                        int index1 = stationModule._TheoPoint.FindIndex(x => x._Name == item._Name && x._Origin == item._Origin);
                        if (index1 != -1) { stationModule._TheoPoint[index1].LGCFixOption = ENUM.LgcPointOption.CALA; }
                        int index3 = stationModule._AllerStationLevel._MeasureOfLevel.FindIndex(x => x._PointName == item._Name && x._Point._Origin == item._Origin);
                        if (index3 != -1) { stationModule._AllerStationLevel._MeasureOfLevel[index3]._Point.LGCFixOption = ENUM.LgcPointOption.CALA; }
                        int index4 = stationModule._RetourStationLevel._MeasureOfLevel.FindIndex(x => x._PointName == item._Name && x._Point._Origin == item._Origin);
                        if (index4 != -1) { stationModule._RetourStationLevel._MeasureOfLevel[index4]._Point.LGCFixOption = ENUM.LgcPointOption.CALA; }
                        foreach (Station st in stationModule._RepriseStationLevel)
                        {
                            int index2 = st._MeasureOfLevel.FindIndex(x => x._PointName == item._Name && x._Point._Origin == item._Origin);
                            if (index2 != -1) { st._MeasureOfLevel[index2]._Point.LGCFixOption = ENUM.LgcPointOption.CALA; }
                        }

                    }
                }
                //Si pas encore de station, il faut désigner un point qui doit être en cala
                if (stationModule._WorkingStationLevel._MeasureOfLevel.Count == 0)
                {
                    stationModule.SetPointsToMeasure(pointsList.Clone(), false);
                }
                stationModule.FinalModule.CalaPoints = pointsList.Clone();
                pointsList.AddRange(stationModule.FinalModule.PointsToBeAligned.Clone());
                stationModule.UpdateCalaVSPointToBeAligned(pointsList);
                stationModule._WorkingStationLevel = stationModule._AllerStationLevel;
                stationModule.SetPointsToMeasure(pointsList.Clone(), false);
                if (!stationModule.retourDone) stationModule.ResetAndCreateRetourAndReprise();
                ///Met à jour les VZ dans les mesures en fonction des points à aligner 
                foreach (Point pt in stationModule.FinalModule.PointsToBeAligned)
                {
                    int indexAller = stationModule._AllerStationLevel._MeasureOfLevel.FindIndex(x => x._PointName == pt._Name && x._Point._Origin == pt._Origin);
                    if (indexAller != -1) stationModule._AllerStationLevel._MeasureOfLevel[indexAller]._Point.LGCFixOption = ENUM.LgcPointOption.VZ;
                    int indexRetour = stationModule._RetourStationLevel._MeasureOfLevel.FindIndex(x => x._PointName == pt._Name && x._Point._Origin == pt._Origin);
                    if (indexRetour != -1) stationModule._RetourStationLevel._MeasureOfLevel[indexAller]._Point.LGCFixOption = ENUM.LgcPointOption.VZ;
                    foreach (Station reprise in stationModule._RepriseStationLevel)
                    {
                        int indexReprise = reprise._MeasureOfLevel.FindIndex(x => x._PointName == pt._Name && x._Point._Origin == pt._Origin);
                        if (indexReprise != -1) reprise._MeasureOfLevel[indexAller]._Point.LGCFixOption = ENUM.LgcPointOption.VZ;
                    }
                }
                stationModule.UpdateOffsets(true, false);
            };
            return s;

        }
        /// <summary>
        /// STep to choose points for levelling and add them in the Station module, keep only the theoretical points
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step ChooseVZPoints(Common.Guided.Module guidedModule)
        {
            Step s = ChoosePoints(guidedModule);
            s._Name = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringCheminement_SelectVZPoint);
            s.Utility = T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringCheminement_SelectVZPoint);
            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.SetVZPointsInElementManager();
                e.ElementManager.UpdateView();
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                CloneableList<Point> pointsList = e.ElementManager.GetPointsInSelectedObjects();
                //Recupere tous les points de cala
                foreach (Point item in pointsList)
                {
                    item.LGCFixOption = ENUM.LgcPointOption.VZ;
                }
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                stationModule.FinalModule.PointsToBeAligned = pointsList.Clone();
                pointsList.AddRange(stationModule.FinalModule.CalaPoints);
                ///Met à jour les cala ou VZ dans les mesures
                stationModule.UpdateCalaVSPointToBeAligned(pointsList);
                stationModule.SetPointsToMeasure(pointsList);
                if (!stationModule.retourDone) stationModule.ResetAndCreateRetourAndReprise();
            };
            return s;
        }
        ///// <summary>
        ///// STep to choose points of the station level and add them in the Station module, keep only the theoretical points
        ///// </summary>
        ///// <param name="guidedModule"></param>
        ///// <returns></returns>
        //internal static Step ChoosePointsAndValidate(Module guidedModule)
        //{
        //    Step s = ChoosePoints(guidedModule);
        //    s.Entering += delegate(object source, StepEventArgs e)
        //    {
        //        TSU.Level.Station.Module stationModule = e.ActiveStationModule as TSU.Level.Station.Module;
        //        stationModule.SetTheoPointsInElementManager(); ;
        //        e.ElementManager.UpdateView();
        //    };
        //    s.Leaving += delegate(object source, StepEventArgs e)
        //    {
        //        e.ElementManager.ValidateSelection();
        //        TSU.Level.Station.Module stationModule = e.ActiveStationModule as TSU.Level.Station.Module;
        //        ///Crée une station retour et une station reprise + copie dedans les mesures déjà faites
        //        stationModule.ResetAndCreateRetourAndReprise();
        //    };
        //    return s;
        //}
        ///// <summary>
        ///// Selection du niveau avec mise à jour de la taille de la vue instrument si l'instrument a changé
        ///// </summary>
        ///// <param name="guidedModule"></param>
        ///// <returns></returns>
        //static public Step ChooseLevel(Module guidedModule)
        //{
        //    Step s = Management.ChooseInstrument(guidedModule);
        //    string instrumentname = "";
        //    s.Entering += delegate (object source, StepEventArgs e)
        //    {
        //        TSU.Level.Station.Module st = e.ActiveStationModule as TSU.Level.Station.Module;
        //        if (st._WorkingStationLevel._Parameters._Instrument != null) instrumentname = st._WorkingStationLevel._Parameters._Instrument._Name;

        //    };
        //    s.Leaving += delegate (object source, StepEventArgs e) 
        //    {
        //        TSU.Level.Station.Module st = e.ActiveStationModule as TSU.Level.Station.Module;
        //        if (st._WorkingStationLevel._Parameters._Instrument != null)
        //        {
        //            if (instrumentname != st._WorkingStationLevel._Parameters._Instrument._Name) st.View.MaximizeDataGridView();
        //        }
        //    };
        //    return s;
        //}
        ///// <summary>
        ///// Pour le module alignement levelling, met également le même niveau pour le contrôle
        ///// </summary>
        ///// <param name="guidedModule"></param>
        ///// <returns></returns>
        //internal static Step ChooseAlignmentLevel(Module guidedModule)
        //{
        //    Step s = ChooseLevel(guidedModule);
        //    s.ControlChanged += delegate (object source, StepEventArgs e)
        //    {
        //        TSU.Level.Station.Module st = e.ActiveStationModule as TSU.Level.Station.Module;
        //        string instrumentname = "";
        //        if (st._WorkingStationLevel._Parameters._Instrument != null) instrumentname = st._WorkingStationLevel._Parameters._Instrument._Name;
        //        //Selectionne aussi le niveau dans le module levelling check
        //        if (guidedModule.ParentModule is G.Group.Module)
        //        {
        //            G.Group.Module grGm = guidedModule.ParentModule as G.Group.Module;
        //            if (grGm.SubGuidedModules.Count >= 7)
        //            {
        //                if (grGm.SubGuidedModules[6].guideModuleType == ENUM.GuidedModuleType.AlignmentLevellingCheck)
        //                {
        //                    TSU.Level.Station.Module stationLevelModuleCheck = grGm.SubGuidedModules[6]._ActiveStationModule as TSU.Level.Station.Module;
        //                    //stationLevelModuleCheck._InstrumentManager._SelectedObjects.Clear();
        //                    //stationLevelModuleCheck._InstrumentManager._SelectedObjectInBlue = st._InstrumentManager._SelectedObjectInBlue;
        //                    //stationLevelModuleCheck._InstrumentManager.AddSelectedObjects(st._InstrumentManager._SelectedObjects);
        //                    stationLevelModuleCheck.OnInstrumentChanged(st._InstrumentManager._SelectedObjects[0] as I.Sensor);
        //                }
        //            }
        //        }
        //    };
        //    //Leaving() is triggered by Leave()
        //    s.Leaving += delegate (object source, StepEventArgs e)
        //    {
        //        TSU.Level.Station.Module st = e.ActiveStationModule as TSU.Level.Station.Module;
        //        string instrumentname = "";
        //        if (st._WorkingStationLevel._Parameters._Instrument != null) instrumentname = st._WorkingStationLevel._Parameters._Instrument._Name;
        //        if (guidedModule.ParentModule is G.Group.Module)
        //        {
        //            G.Group.Module grGm = guidedModule.ParentModule as G.Group.Module;
        //            if (grGm.SubGuidedModules.Count >= 7)
        //            {
        //                if (grGm.SubGuidedModules[6].guideModuleType == ENUM.GuidedModuleType.AlignmentLevellingCheck)
        //                {
        //                    TSU.Level.Station.Module stationLevelModuleCheck = grGm.SubGuidedModules[6]._ActiveStationModule as TSU.Level.Station.Module;
        //                    if (stationLevelModuleCheck._WorkingStationLevel._Parameters._Instrument != null)
        //                    {
        //                        if (instrumentname != stationLevelModuleCheck._WorkingStationLevel._Parameters._Instrument._Name) stationLevelModuleCheck.View.MaximizeDataGridView();
        //                    }
        //                }
        //            }
        //        }
        //    };
        //    return s;
        //}
        static public Step ChooseAllAdministrativeparameters(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.String_GM_AdministrativeParameters);
            BigButton buttonSelectTheoElementFile = new BigButton();
            BigButton buttonSelectTheoSeqFile = new BigButton();
            BigButton buttonSelectLevel = new BigButton();
            BigButton buttonSelectOperation = new BigButton();
            BigButton buttonCreateOperation = new BigButton();
            BigButton buttonSelectLevellingStaff = new BigButton();
            BigButton buttonSelectSecondaryStaff = new BigButton();
            BigButton buttonZeroStaffCheck = new BigButton();
            BigButton buttonCollimCheck = new BigButton();
            F.TextBox textBoxTeam = new F.TextBox();
            F.TextBox textBoxTemperature = new F.TextBox();
            Drawing.Font TitleFont = new Drawing.Font("Microsoft Sans Serif", 15.75F, Drawing.FontStyle.Regular, Drawing.GraphicsUnit.Point, 0);
            string instrumentname = "...";
            string staffName = "...";
            string secondaryStaffName = "...";
            string operationName = "...";
            string team = "";
            string temperature = "";
            double na = TSU.Tsunami2.Preferences.Values.na;
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                // add a button to directly browse for theoretical file
                e.step.View.AddButton(
                    string.Format(R.String_GM_SelectTheoElementFile, "..."),
                    R.Open,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        gm._ElementManager.MultiSelection = false;
                        gm._ElementManager.View.CheckBoxesVisible = false;
                        gm._ElementManager.SetSelectableToAllTheoreticalPoints();
                        gm._ElementManager.View.buttons.HideAllButtons();
                        gm._ElementManager.View.TryOpen();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectTheoElementFile = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectTheoElementFile.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Bad);
                // add a button to directly browse for sequence theoretical file
                e.step.View.AddButton(
                   string.Format(R.String_GM_SelectTheoSeqFile, "..."),
                   R.Open,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        e.ElementManager.SetSelectableToAllSequenceNiv();
                        gm._ElementManager.MultiSelection = false;
                        gm._ElementManager.View.CheckBoxesVisible = false;
                        gm._ElementManager.View.TrySelectSequenceFile();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectTheoSeqFile = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectTheoSeqFile.Available = false;
                buttonSelectTheoSeqFile.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                // add a button to select instrument
                e.step.View.AddButton(
                    string.Format(R.String_GM_SelectInstrument, instrumentname),
                    R.Level,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        Station.Module st = gm._ActiveStationModule as Station.Module;
                        st.SelectInstrument();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectLevel = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectLevel.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Bad);
                // add a button to select default staff
                e.step.View.AddButton(
                    string.Format(R.String_GM_SelectDefaultStaff, staffName),
                    R.Main_staff,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        Station.Module st = gm._ActiveStationModule as Station.Module;
                        //Already done in st.SelectDefaultStaff()
                        //if (st._defaultStaff != null)
                        //{
                        //    st.staffModule._SelectedObjects.Clear();
                        //    staffName = st._defaultStaff._Name;
                        //    I.LevelingStaff staff = (st.staffModule.AllElements.Find(x => x._Name == st._defaultStaff._Name) as I.LevelingStaff);
                        //    st.staffModule.AddSelectedObjects(staff);
                        //    st.staffModule.View.currentStrategy.ShowSelectionInButton();
                        //    st.staffModule._SelectedObjectInBlue = staff;
                        //    st.staffModule.UpdateView();
                        //}
                        st.SelectDefaultStaff();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectLevellingStaff = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectLevellingStaff.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Bad);
                // add a button to select secondary staff
                e.step.View.AddButton(
                    string.Format(R.String_GM_SelectSecondaryStaff, secondaryStaffName),
                    R.Second_staff,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        Station.Module st = gm._ActiveStationModule as Station.Module;
                        st.SelectSecondaryStaff();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectSecondaryStaff = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectSecondaryStaff.SetColors(Drawing.Color.Orange);
                // add a button to select operation
                e.step.View.AddButton(
                    string.Format(R.String_GM_SelectOperation, operationName),
                    R.Operation,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        Station.Module st = gm._ActiveStationModule as Station.Module;
                        if (st.View != null)
                        {
                            st.View.SelectOperation();
                        }
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectOperation = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectOperation.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                // add a button to create operation
                e.step.View.AddButton(
                    string.Format(R.String_GM_CreateOperation, operationName),
                    R.Operation_Add,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        Station.Module st = gm._ActiveStationModule as Station.Module;
                        e.OperationManager.View.AddNew();
                        if (e.OperationManager.SelectedOperation != null)
                        {
                            st.ChangeOperation(e.OperationManager.SelectedOperation);
                        }
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonCreateOperation = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonCreateOperation.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                // add a button to open the zero staff check message box
                e.step.View.AddButton(
                    string.Format(R.StringLevel_ZeroStaffCheck),
                    R.Zero_Staff_Check,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        if (gm.ParentModule is Common.Guided.Group.Module)
                        {
                            (gm.ParentModule as Common.Guided.Group.Module).ZeroStaffCheck();
                        }
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonZeroStaffCheck = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonZeroStaffCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                // add a button to open the colim level check message box
                e.step.View.AddButton(
                    R.StringLevel_CollimCheckButton,
                    R.Level_Collim,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        if (gm.ParentModule is Common.Guided.Group.Module)
                        {
                            (gm.ParentModule as Common.Guided.Group.Module).CollimCheck();
                        }
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonCollimCheck = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonCollimCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                buttonCollimCheck.DisableButton();
                /// ajout temperature ambiante
                e.step.View.AddTitle(R.String_GM__AmbientTemperature, "temperatureLabel");
                e.step.View.AddTextBox(guidedModule._TsuView, temperature);
                textBoxTemperature = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as F.TextBox;
                textBoxTemperature.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Attention;
                /// ajout nom de l'équipe
                e.step.View.AddTitle(R.String_GM_TeamLabel, "teamLabel");
                e.step.View.AddTextBox(guidedModule._TsuView, team);
                textBoxTeam = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as F.TextBox;
                textBoxTeam.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
                textBoxTeam.Enter += delegate
                {
                    textBoxTeam.SelectAll();
                };
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                if (st._Station.ParametersBasic._Instrument != null)
                {
                    if (st._Station.ParametersBasic._Instrument._Name != R.String_Unknown)
                    {
                        e.InstrumentManager._SelectedObjects.Clear();
                        instrumentname = st._Station.ParametersBasic._Instrument._Name;
                        I.Sensor instrument = e.InstrumentManager.AllElements.Find(x => x._Name == st._Station.ParametersBasic._Instrument._Name) as I.Sensor;
                        e.InstrumentManager.AddSelectedObjects(instrument);
                        e.InstrumentManager.View.currentStrategy.ShowSelectionInButton();
                        e.InstrumentManager._SelectedObjectInBlue = instrument;
                        e.InstrumentManager.UpdateView();
                        buttonCollimCheck.EnableButton();
                    }
                    else buttonCollimCheck.DisableButton();
                }
                else buttonCollimCheck.DisableButton();
                O.Operation operation = e.OperationManager.AllElements.Find(x => x._Name == st._AllerStationLevel.ParametersBasic._Operation._Name) as O.Operation;
                e.OperationManager._SelectedObjects.Clear();
                e.OperationManager.AddSelectedObjects(operation);
                e.OperationManager.View.currentStrategy.ShowSelectionInButton();
                e.OperationManager._SelectedObjectInBlue = operation;
                e.OperationManager.UpdateView();
                temperature = st._WorkingStationLevel._Parameters._Temperature.ToString("F1", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                textBoxTemperature.Text = temperature;
                if (e.ActiveStationModule._Station.ParametersBasic._Team != R.String_UnknownTeam)
                {
                    team = e.ActiveStationModule._Station.ParametersBasic._Team;
                    textBoxTeam.Text = team;
                }
                else
                {
                    textBoxTeam.Text = "";
                }
                operationName = st._AllerStationLevel.ParametersBasic._Operation.ToString();
                textBoxTeam.Focus();
                textBoxTeam.SelectAll();
                textBoxTeam.Focus();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = false;
                bool stepValid = true;
                bool stepIsSkipable = false;
                string msgBlocked = R.String_GM_MsgBlockAdministrative;
                Station.Module st = e.ActiveStationModule as Station.Module;
                ///Vérification si fichier theo sélectionné
                if (e.ElementManager.GetAllTheoreticalPoints().Count > 0)
                {
                    buttonSelectTheoElementFile.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Good);
                    buttonSelectTheoElementFile.ChangeImage(R.StatusGood);
                    buttonSelectTheoSeqFile.Available = true;
                    buttonSelectTheoElementFile.ChangeNameAndDescription(string.Format(R.String_GM_SelectTheoElementFile, (e.ElementManager.AllElements[0] as Element)._Origin));
                }
                else
                {
                    msgBlocked += " " + R.String_GM_MsgBlockTheoFile;
                    stepValid = false;
                }
                ///Vérification si fichier séquence sélectionné (pas obligatoire)
                if (e.ElementManager.GetAllSequenceNiv().Count > 0)
                {
                    buttonSelectTheoSeqFile.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Good);
                    buttonSelectTheoSeqFile.ChangeImage(R.StatusGood);
                    buttonSelectTheoSeqFile.ChangeNameAndDescription(string.Format(R.String_GM_SelectTheoSeqFile, e.ElementManager.GetAllSequenceNiv()[0]._Origin));
                }
                ///Vérification si instrument sélectionné
                if (e.InstrumentManager._SelectedObjects.Count == 1)
                {
                    I.Instrument instrument = e.InstrumentManager._SelectedObjects[0] as I.Instrument;
                    if (instrument._Model != null)
                    {
                        if (instrument._Name != R.String_Unknown)
                        {
                            buttonSelectLevel.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Good);
                            buttonSelectLevel.ChangeImage(R.StatusGood);
                            if (st._WorkingStationLevel._Parameters._Instrument != null)
                            {
                                if (st._WorkingStationLevel._Parameters._Instrument._Name != R.String_Unknown)
                                {
                                    if (instrumentname != st._WorkingStationLevel._Parameters._Instrument._Name)
                                    {
                                        st._AllerStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                                        if (st._RepriseStationLevel.Count != 0) st._RepriseStationLevel[0].ParametersBasic.LastChanged = DateTime.Now;
                                        st._RetourStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                                        st.CheckIfReadyToBeSaved();
                                        instrumentname = st._WorkingStationLevel._Parameters._Instrument._Name;
                                        buttonCollimCheck.EnableButton();
                                    }
                                    else buttonCollimCheck.EnableButton();
                                }
                                else buttonCollimCheck.DisableButton();
                            }
                            else buttonCollimCheck.DisableButton();
                        }
                        else buttonCollimCheck.DisableButton();
                        buttonSelectLevel.ChangeNameAndDescription(string.Format(R.String_GM_SelectInstrument, instrumentname));
                    }
                    else
                    {
                        if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                        msgBlocked += " " + R.String_GM_MsgBlockInstrument;
                        stepValid = false;
                        buttonCollimCheck.DisableButton();
                    }
                }
                else
                {
                    if (st._AllerStationLevel._Parameters._Instrument == null)
                    {
                        if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                        msgBlocked += " " + R.String_GM_MsgBlockInstrument;
                        stepValid = false;
                        buttonCollimCheck.DisableButton();
                    }
                    else
                    {
                        if (st._AllerStationLevel._Parameters._Instrument._Name == R.String_Unknown)
                        {
                            if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                            msgBlocked += " " + R.String_GM_MsgBlockInstrument;
                            stepValid = false;
                            buttonCollimCheck.DisableButton();
                        }
                    }
                }
                ///Verification si mire selectionnée
                //if (st.staffModule._SelectedObjects.Count == 1)
                //{
                //    TSU.Common.Instruments.LevelingStaff staff = st.staffModule._SelectedObjects[0] as TSU.Common.Instruments.LevelingStaff;
                //    if (staff._Model != null)
                //    {
                //        buttonSelectLevellingStaff.SetColors(Tsunami2.Preferences.Theme._Colors.Good);
                //        buttonSelectLevellingStaff.ChangeImage(R.StatusGood);
                //        if (st._defaultStaff != null) staffName = st._defaultStaff._Name;
                //        buttonSelectLevellingStaff.ChangeNameAndDescription(string.Format(R.String_GM_SelectDefaultStaff, staffName));
                //    }
                //    else
                //    {
                //        if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                //        msgBlocked += " " + R.String_GM_MsgBlockStaff;
                //        stepValid = false;
                //    }
                //}
                //else
                //{
                if (st._defaultStaff == null)
                {
                    if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                    msgBlocked += " " + R.String_GM_MsgBlockStaff;
                    stepValid = false;
                }
                else
                {
                    if (st._defaultStaff._Name == R.String_Unknown)
                    {
                        if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                        msgBlocked += " " + R.String_GM_MsgBlockStaff;
                        stepValid = false;
                    }
                    else
                    {
                        buttonSelectLevellingStaff.SetColors(Drawing.Color.GreenYellow);
                        buttonSelectLevellingStaff.ChangeImage(R.StatusGood);
                        if (st._defaultStaff != null) staffName = st._defaultStaff._Name;
                        buttonSelectLevellingStaff.ChangeNameAndDescription(string.Format(R.String_GM_SelectDefaultStaff, staffName));
                    }
                }
                //}
                if (st._secondaryStaff == null)
                {
                    secondaryStaffName = "...";
                    buttonSelectSecondaryStaff.ChangeNameAndDescription(string.Format(R.String_GM_SelectSecondaryStaff, secondaryStaffName));
                    buttonSelectSecondaryStaff.SetColors(Drawing.Color.Orange);
                    buttonSelectSecondaryStaff.ChangeImage(R.Second_staff);
                    stepIsSkipable = true;
                }
                else
                {
                    if (st._secondaryStaff._Name == R.String_Unknown)
                    {
                        secondaryStaffName = "...";
                        buttonSelectSecondaryStaff.ChangeNameAndDescription(string.Format(R.String_GM_SelectSecondaryStaff, secondaryStaffName));
                        buttonSelectSecondaryStaff.SetColors(Drawing.Color.Orange);
                        buttonSelectSecondaryStaff.ChangeImage(R.Second_staff);
                        stepIsSkipable = true;
                    }
                    else
                    {
                        buttonSelectSecondaryStaff.SetColors(Drawing.Color.GreenYellow);
                        buttonSelectSecondaryStaff.ChangeImage(R.StatusGood);
                        if (st._secondaryStaff != null) secondaryStaffName = st._secondaryStaff._Name;
                        buttonSelectSecondaryStaff.ChangeNameAndDescription(string.Format(R.String_GM_SelectSecondaryStaff, secondaryStaffName));
                    }
                }
                //Vérification si opération sélectionnée, si 0 met bouton en red
                if (e.OperationManager.SelectedOperation.IsSet) //&& (e.OperationManager.SelectedOperation.value != 0))
                {
                    if (st._AllerStationLevel.ParametersBasic._Operation != null)
                    {
                        buttonSelectOperation.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Good);
                        buttonSelectOperation.ChangeImage(R.StatusGood);
                        operationName = st._AllerStationLevel.ParametersBasic._Operation.ToString();
                        buttonSelectOperation.ChangeNameAndDescription(string.Format(R.String_GM_SelectOperation, operationName));
                        buttonCreateOperation.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Good);
                        buttonCreateOperation.ChangeImage(R.StatusGood);
                        buttonCreateOperation.ChangeNameAndDescription(string.Format(R.String_GM_CreateOperation, operationName));
                    }
                }
                else
                {
                    if (st._AllerStationLevel.ParametersBasic._Operation != null)
                    {
                        if (!st._AllerStationLevel.ParametersBasic._Operation.IsSet) //|| st._AllerStationLevel.ParametersBasic._Operation.value == 0)
                        {
                            buttonSelectOperation.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                            buttonSelectOperation.ChangeImage(R.Operation);
                            operationName = st._AllerStationLevel.ParametersBasic._Operation.ToString();
                            buttonSelectOperation.ChangeNameAndDescription(string.Format(R.String_GM_SelectOperation, operationName));
                            buttonCreateOperation.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                            buttonCreateOperation.ChangeImage(R.Operation_Add);
                            buttonCreateOperation.ChangeNameAndDescription(string.Format(R.String_GM_CreateOperation, operationName));
                            stepIsSkipable = true;
                        }
                    }
                }
                //Vérifie que la temperature contient bien un nombre double
                double newTemperature = T.Conversions.Numbers.ToDouble(textBoxTemperature.Text, true, -9999);
                if (newTemperature != -9999)
                {
                    textBoxTemperature.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Good;
                }
                else
                {
                    stepValid = false;
                    if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                    msgBlocked += " " + R.String_GM_MsgBlockTemperature;
                    textBoxTemperature.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
                }
                //Vérifie que le nom d'équipe contient que des caractères alphanumériques et est de maximum 8 caractères
                string tempTeam = textBoxTeam.Text;
                if (System.Text.RegularExpressions.Regex.IsMatch(tempTeam, @"^[a-zA-Z]+$") && (tempTeam.Length < 9 || tempTeam == R.String_UnknownTeam))
                {
                    tempTeam = tempTeam.ToUpper();
                    e.ActiveStationModule._Station.ParametersBasic._Team = tempTeam;
                    team = e.ActiveStationModule._Station.ParametersBasic._Team;
                    textBoxTeam.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Good;
                }
                else
                {
                    stepValid = false;
                    if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                    msgBlocked += " " + R.String_GM_MsgBlockTeam;
                    textBoxTeam.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
                }
                ///vérifie qu'on a fait un zéro staff check
                if (guidedModule.ParentModule != null)
                {
                    if (guidedModule.ParentModule is Common.Guided.Group.Module)
                    {
                        Common.Guided.Group.Module GrGm = guidedModule.ParentModule as Common.Guided.Group.Module;
                        if (GrGm.staffUsed.Find(x => x._zeroCorrectionReading != na) != null)
                        {
                            buttonZeroStaffCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Good);
                        }
                        else
                        {
                            buttonZeroStaffCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                            stepIsSkipable = true;
                        }
                    }
                    else
                    {
                        buttonZeroStaffCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                        stepIsSkipable = true;
                    }
                }
                else
                {
                    buttonZeroStaffCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                    stepIsSkipable = true;
                }
                ///vérifie qu'on a fait un contrôle colimation avec le niveau sélectionné
                if (guidedModule.ParentModule != null)
                {
                    if (guidedModule.ParentModule is Common.Guided.Group.Module)
                    {
                        Common.Guided.Group.Module GrGm = guidedModule.ParentModule as Common.Guided.Group.Module;
                        if (st._WorkingStationLevel._Parameters._Instrument != null)
                        {
                            if (st._WorkingStationLevel._Parameters._Instrument._Name != R.String_Unknown)
                            {
                                string actualInstrumentName = st._WorkingStationLevel._Parameters._Instrument._Name;
                                if (GrGm.levelUsed.Find(x => x._ErrorCollim != na && x._Name == instrumentname) != null)
                                {
                                    buttonCollimCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Good);
                                }
                                else
                                {
                                    buttonCollimCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                                    stepIsSkipable = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        buttonCollimCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                        stepIsSkipable = true;
                    }
                }
                else
                {
                    buttonCollimCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                    stepIsSkipable = true;
                }
                e.guidedModule.CurrentStep.MessageFromTest = msgBlocked;
                e.step.CheckValidityOf(stepValid);
                e.step.Update();
            };

            s.Leaving += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                if (st._WorkingStationLevel._Parameters._Instrument != null)
                {
                    if (instrumentname != st._WorkingStationLevel._Parameters._Instrument._Name) st.View.MaximizeDataGridView();
                }
                ///Cache le step select sequence niv si pas de file sequence niv
                if (e.ElementManager.GetAllSequenceNiv().Count == 0)
                {
                    guidedModule.Steps[2].Visible = false;
                }
                else
                {
                    guidedModule.Steps[2].Visible = true;
                }
                double newTemperature = T.Conversions.Numbers.ToDouble(textBoxTemperature.Text, true, -9999);
                if (newTemperature != -9999 && temperature != textBoxTemperature.Text)
                {
                    st.ChangeTemperature(newTemperature);
                }
            };
            return s;
        }
        /// <summary>
        /// Step pour le levelling check aller avec calcul LGC global dans le module guidé alignement
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step LevelAlignmentAllerCheck(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.String_GM_OutwardLevellingCheck);
            F.TableLayoutPanel TLPDatagridViewMoveLevel = new F.TableLayoutPanel();
            TsuView TsuView = new TsuView(s);
            Common.Guided.Group.Module grGm = guidedModule.ParentModule as Common.Guided.Group.Module;
            BigButton LGCGlobal = new BigButton(R.String_GM_Level_GlobalLGCCalculation, R.Lgc, grGm.DoLevelCalculationWithLGC2ForAlignment);
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                e.step.View.CreateDatagridViewMoveLevel();
                e.step.View.InitializeDatagridViewMoveLevel();
                e.step.View.ClearAllControls();
                TLPDatagridViewMoveLevel.RowCount = 3;
                TLPDatagridViewMoveLevel.ColumnCount = 1;
                TLPDatagridViewMoveLevel.Dock = F.DockStyle.Fill;
                LGCGlobal.Dock = F.DockStyle.Top;
                TLPDatagridViewMoveLevel.RowStyles.Add(new F.RowStyle(F.SizeType.Percent, 50F));
                TLPDatagridViewMoveLevel.RowStyles.Add(new F.RowStyle(F.SizeType.Absolute, LGCGlobal.Height));
                TLPDatagridViewMoveLevel.RowStyles.Add(new F.RowStyle(F.SizeType.Percent, 50F));
                TsuView.Controls.Add(TLPDatagridViewMoveLevel);
                e.step.View.AddControl(TsuView);
                TsuView.Dock = F.DockStyle.Fill;
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                st.View.ChangeModuleType(ENUM.ModuleType.Guided);
                st.ChangeLevelingDirectionToAller();
                guidedModule.Utility = st.Utility;
                e.step.View.Dock = F.DockStyle.Fill;
                st.View.MaximizeDataGridView();
                ///Efface le tableau pour obliger l'utilisateur à relancer un calcul LGC au cas ou il a modifié des valeurs dans un autre step
                //e.step.View.dataGridViewMoveLevel.Rows.Clear();
                TLPDatagridViewMoveLevel.Controls.Clear();
                TLPDatagridViewMoveLevel.Controls.Add(st.View, 0, 0);
                TLPDatagridViewMoveLevel.Controls.Add(LGCGlobal, 0, 1);
                TLPDatagridViewMoveLevel.Controls.Add(e.step.View.dataGridViewMoveLevel, 0, 2);

            };
            s.ControlChanged += delegate (object source, StepEventArgs e)
            {

            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {

            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                e.step.Test();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                e.step.CheckValidityOf(stationModule._WorkingStationLevel._Parameters._State is Common.Station.State.StationLevelSaved);
            };
            return s;
        }
        /// <summary>
        /// Step pour le levelling check retour avec calcul LGC global dans le module guidé alignement
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step LevelAlignmentRetourCheck(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.String_GM_ReturnLevellingCheck);
            F.TableLayoutPanel TLPDatagridViewMoveLevel = new F.TableLayoutPanel();
            TsuView TsuView = new TsuView(s);
            Common.Guided.Group.Module grGm = guidedModule.ParentModule as Common.Guided.Group.Module;
            BigButton LGCGlobal = new BigButton(R.String_GM_Level_GlobalLGCCalculation, R.Lgc, grGm.DoLevelCalculationWithLGC2ForAlignment);
            double na = TSU.Tsunami2.Preferences.Values.na;
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                e.step.View.CreateDatagridViewMoveLevel();
                e.step.View.InitializeDatagridViewMoveLevel();
                e.step.View.ClearAllControls();
                TLPDatagridViewMoveLevel.RowCount = 3;
                TLPDatagridViewMoveLevel.ColumnCount = 1;
                TLPDatagridViewMoveLevel.Dock = F.DockStyle.Fill;
                LGCGlobal.Dock = F.DockStyle.Top;
                TLPDatagridViewMoveLevel.RowStyles.Add(new F.RowStyle(F.SizeType.Percent, 50F));
                TLPDatagridViewMoveLevel.RowStyles.Add(new F.RowStyle(F.SizeType.Absolute, LGCGlobal.Height));
                TLPDatagridViewMoveLevel.RowStyles.Add(new F.RowStyle(F.SizeType.Percent, 50F));
                TsuView.Controls.Add(TLPDatagridViewMoveLevel);
                e.step.View.AddControl(TsuView);
                TsuView.Dock = F.DockStyle.Fill;
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                st.View.ChangeModuleType(ENUM.ModuleType.Guided);
                st.ChangeLevelingDirectionToRetour();
                guidedModule.Utility = st.Utility;
                e.step.View.Dock = F.DockStyle.Fill;
                st.View.MaximizeDataGridView();
                ///Efface le tableau pour obliger l'utilisateur à relancer un calcul LGC au cas ou il a modifié des valeurs dans un autre step
                //e.step.View.dataGridViewMoveLevel.Rows.Clear();
                TLPDatagridViewMoveLevel.Controls.Clear();
                TLPDatagridViewMoveLevel.Controls.Add(st.View, 0, 0);
                TLPDatagridViewMoveLevel.Controls.Add(LGCGlobal, 0, 1);
                TLPDatagridViewMoveLevel.Controls.Add(e.step.View.dataGridViewMoveLevel, 0, 2);
            };
            s.ControlChanged += delegate (object source, StepEventArgs e)
            {

            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                if (e.step.State != StepState.NotReady && stationModule._RetourStationLevel._Parameters._EmqAller > 0.00007
                    && stationModule._RetourStationLevel._Parameters._EmqAller != na)
                {
                    string titleAndMessage = string.Format(R.StringLevel_BadEmqReturnShowing, Math.Round(stationModule._RetourStationLevel._Parameters._EmqAller * 1000, 2).ToString());
                    new MessageInput(MessageType.Warning, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_OK + "!" }
                    }.Show();
                }
            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                e.step.Test();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                e.step.CheckValidityOf(stationModule._WorkingStationLevel._Parameters._State is Common.Station.State.StationLevelSaved);
            };
            return s;
        }
        /// <summary>
        /// Step pour le levelling check reprise avec calcul LGC global dans le module guidé alignement
        /// </summary>
        /// <param name="guidedModule"></param>
        /// <returns></returns>
        internal static Step LevelAlignmentRepriseCheck(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.String_GM_RepeatLevellingCheck);
            F.TableLayoutPanel TLPDatagridViewMoveLevel = new F.TableLayoutPanel();
            TsuView TsuView = new TsuView(s);
            Common.Guided.Group.Module grGm = guidedModule.ParentModule as Common.Guided.Group.Module;
            BigButton LGCGlobal = new BigButton(R.String_GM_Level_GlobalLGCCalculation, R.Lgc, grGm.DoLevelCalculationWithLGC2ForAlignment);
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                e.step.View.CreateDatagridViewMoveLevel();
                e.step.View.InitializeDatagridViewMoveLevel();
                e.step.View.ClearAllControls();
                TLPDatagridViewMoveLevel.RowCount = 3;
                TLPDatagridViewMoveLevel.ColumnCount = 1;
                TLPDatagridViewMoveLevel.Dock = F.DockStyle.Fill;
                LGCGlobal.Dock = F.DockStyle.Top;
                TLPDatagridViewMoveLevel.RowStyles.Add(new F.RowStyle(F.SizeType.Percent, 50F));
                TLPDatagridViewMoveLevel.RowStyles.Add(new F.RowStyle(F.SizeType.Absolute, LGCGlobal.Height));
                TLPDatagridViewMoveLevel.RowStyles.Add(new F.RowStyle(F.SizeType.Percent, 50F));
                TsuView.Controls.Add(TLPDatagridViewMoveLevel);
                e.step.View.AddControl(TsuView);
                TsuView.Dock = F.DockStyle.Fill;
            };

            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                st.View.ChangeModuleType(ENUM.ModuleType.Guided);
                st.ChangeLevelingDirectionToReprise(1);
                guidedModule.Utility = st.Utility;
                e.step.View.Dock = F.DockStyle.Fill;
                st.View.MaximizeDataGridView();
                ///Efface le tableau pour obliger l'utilisateur à relancer un calcul LGC au cas ou il a modifié des valeurs dans un autre step
                //e.step.View.dataGridViewMoveLevel.Rows.Clear();
                TLPDatagridViewMoveLevel.Controls.Clear();
                TLPDatagridViewMoveLevel.Controls.Add(st.View, 0, 0);
                TLPDatagridViewMoveLevel.Controls.Add(LGCGlobal, 0, 1);
                TLPDatagridViewMoveLevel.Controls.Add(e.step.View.dataGridViewMoveLevel, 0, 2);
            };
            s.ControlChanged += delegate (object source, StepEventArgs e)
            {

            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {

            };
            s.Updating += delegate (object source, StepEventArgs e)
            {
                e.step.Test();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = true;
                Station.Module stationModule = e.ActiveStationModule as Station.Module;
                e.step.CheckValidityOf(stationModule._WorkingStationLevel._Parameters._State is Common.Station.State.StationLevelSaved);
            };
            return s;
        }
        static public Step ChooseAdministrativeparametersForAlignmentLevel(Common.Guided.Module guidedModule)
        {
            Step s = new Step(guidedModule, R.String_GM_AdministrativeParametersForLevelAlign);
            BigButton buttonSelectLevel = new BigButton();
            BigButton buttonSelectLevellingStaff = new BigButton();
            BigButton buttonSelectSecondaryStaff = new BigButton();
            BigButton buttonZeroStaffCheck = new BigButton();
            BigButton buttonCollimCheck = new BigButton();
            Drawing.Font TitleFont = new Drawing.Font("Microsoft Sans Serif", 15.75F, Drawing.FontStyle.Regular, Drawing.GraphicsUnit.Point, 0);
            string instrumentname = "...";
            string staffName = "...";
            string secondaryStaffName = "...";
            double na = TSU.Tsunami2.Preferences.Values.na;
            s.FirstUse += delegate (object source, StepEventArgs e)
            {
                // add a button to select instrument
                e.step.View.AddButton(
                    string.Format(R.String_GM_SelectInstrument, instrumentname),
                    R.Level,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        Station.Module st = gm._ActiveStationModule as Station.Module;
                        st.SelectInstrument();
                        if (guidedModule.ParentModule is Common.Guided.Group.Module)
                        {
                            Common.Guided.Group.Module grGm = guidedModule.ParentModule as Common.Guided.Group.Module;
                            if (grGm.SubGuidedModules.Count >= 7)
                            {
                                if (grGm.SubGuidedModules[6].guideModuleType == ENUM.GuidedModuleType.AlignmentLevellingCheck)
                                {
                                    Station.Module stationLevelModuleCheck = grGm.SubGuidedModules[6]._ActiveStationModule as Station.Module;
                                    if (st._InstrumentManager._SelectedObjects.Count > 0)
                                    {
                                        stationLevelModuleCheck.OnInstrumentChanged(st._InstrumentManager._SelectedObjects[0] as I.Sensor);
                                    }
                                }
                            }
                        }
                        gm.UpdateView();
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectLevel = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectLevel.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Bad);
                // add a button to select default staff
                e.step.View.AddButton(
                    string.Format(R.String_GM_SelectDefaultStaff, staffName),
                    R.Main_staff,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        Station.Module st = gm._ActiveStationModule as Station.Module;
                        st.SelectDefaultStaff();
                        if (guidedModule.ParentModule is Common.Guided.Group.Module)
                        {
                            Common.Guided.Group.Module grGm = guidedModule.ParentModule as Common.Guided.Group.Module;
                            if (grGm.SubGuidedModules.Count >= 7)
                            {
                                if (grGm.SubGuidedModules[6].guideModuleType == ENUM.GuidedModuleType.AlignmentLevellingCheck)
                                {
                                    Station.Module stationLevelModuleCheck = grGm.SubGuidedModules[6]._ActiveStationModule as Station.Module;
                                    if (stationLevelModuleCheck._InstrumentManager != null)
                                    {
                                        stationLevelModuleCheck._InstrumentManager._SelectedObjects.Clear();
                                        stationLevelModuleCheck._InstrumentManager._SelectedObjectInBlue = st._InstrumentManager._SelectedObjectInBlue;
                                        stationLevelModuleCheck._InstrumentManager.AddSelectedObjects(st._InstrumentManager._SelectedObjects);
                                        stationLevelModuleCheck.selectingSecondaryStaff = false;
                                        stationLevelModuleCheck._InstrumentManager.ValidateSelection();
                                    }
                                }
                            }
                        }
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectLevellingStaff = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectLevellingStaff.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Bad);
                // add a button to select secondary staff
                e.step.View.AddButton(
                    string.Format(R.String_GM_SelectSecondaryStaff, secondaryStaffName),
                    R.Second_staff,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        Station.Module st = gm._ActiveStationModule as Station.Module;
                        st.SelectSecondaryStaff();
                        if (guidedModule.ParentModule is Common.Guided.Group.Module)
                        {
                            Common.Guided.Group.Module grGm = guidedModule.ParentModule as Common.Guided.Group.Module;
                            if (grGm.SubGuidedModules.Count >= 7)
                            {
                                if (grGm.SubGuidedModules[6].guideModuleType == ENUM.GuidedModuleType.AlignmentLevellingCheck)
                                {
                                    Station.Module stationLevelModuleCheck = grGm.SubGuidedModules[6]._ActiveStationModule as Station.Module;
                                    if (stationLevelModuleCheck._InstrumentManager != null)
                                    {
                                        stationLevelModuleCheck._InstrumentManager._SelectedObjects.Clear();
                                        stationLevelModuleCheck._InstrumentManager._SelectedObjectInBlue = st._InstrumentManager._SelectedObjectInBlue;
                                        stationLevelModuleCheck._InstrumentManager.AddSelectedObjects(st._InstrumentManager._SelectedObjects);
                                        stationLevelModuleCheck.selectingSecondaryStaff = true;
                                        stationLevelModuleCheck._InstrumentManager.ValidateSelection();
                                        stationLevelModuleCheck.selectingSecondaryStaff = false;
                                    }
                                }
                            }
                        }
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonSelectSecondaryStaff = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonSelectSecondaryStaff.SetColors(Drawing.Color.Orange);
                // add a button to open the zero staff check message box
                e.step.View.AddButton(
                    R.StringLevel_ZeroStaffCheck,
                    R.Zero_Staff_Check,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        if (gm.ParentModule is Common.Guided.Group.Module)
                        {
                            (gm.ParentModule as Common.Guided.Group.Module).ZeroStaffCheck();
                        }
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonZeroStaffCheck = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonZeroStaffCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                // add a button to open the colim level check message box
                e.step.View.AddButton(
                    R.StringLevel_CollimCheckButton,
                    R.Level_Collim,
                    delegate (object source2, TsuObjectEventArgs args2)
                    {
                        Common.Guided.Module gm = args2.TsuObject as Common.Guided.Module;
                        if (gm.ParentModule is Common.Guided.Group.Module)
                        {
                            (gm.ParentModule as Common.Guided.Group.Module).CollimCheck();
                        }
                    }
                );
                // récupère la référence du dernier contrôle ajouté qui est le big button
                buttonCollimCheck = e.step.View.ControlList[e.step.View.ControlList.Count - 1] as BigButton;
                buttonCollimCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                buttonCollimCheck.DisableButton();
            };
            s.Entering += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                if (st._Station.ParametersBasic._Instrument != null)
                {
                    if (st._Station.ParametersBasic._Instrument._Name != R.String_Unknown)
                    {
                        e.InstrumentManager._SelectedObjects.Clear();
                        instrumentname = st._Station.ParametersBasic._Instrument._Name;
                        I.Sensor instrument = e.InstrumentManager.AllElements.Find(x => x._Name == st._Station.ParametersBasic._Instrument._Name) as I.Sensor;
                        e.InstrumentManager.AddSelectedObjects(instrument);
                        e.InstrumentManager.View.currentStrategy.ShowSelectionInButton();
                        e.InstrumentManager._SelectedObjectInBlue = instrument;
                        e.InstrumentManager.UpdateView();
                        buttonCollimCheck.EnableButton();
                    }
                    else buttonCollimCheck.DisableButton();
                }
                else buttonCollimCheck.DisableButton();
            };
            s.Testing += delegate (object source, StepEventArgs e)
            {
                e.step.skipable = false;
                bool stepValid = true;
                bool stepIsSkipable = false;
                string msgBlocked = R.String_GM_MsgBlockAdministrative;
                Station.Module st = e.ActiveStationModule as Station.Module;
                ///Vérification si instrument sélectionné
                if (e.InstrumentManager._SelectedObjects.Count == 1)
                {
                    I.Instrument instrument = e.InstrumentManager._SelectedObjects[0] as I.Instrument;
                    if (instrument != null)
                    {
                        if (instrument._Model != null && instrument._Name != R.String_Unknown)
                        {
                            buttonSelectLevel.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Good);
                            buttonSelectLevel.ChangeImage(R.StatusGood);
                            if (st._WorkingStationLevel._Parameters._Instrument != null)
                            {
                                if (st._WorkingStationLevel._Parameters._Instrument._Name != R.String_Unknown)
                                {
                                    if (instrumentname != st._WorkingStationLevel._Parameters._Instrument._Name)
                                    {
                                        st._AllerStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                                        if (st._RepriseStationLevel.Count != 0) st._RepriseStationLevel[0].ParametersBasic.LastChanged = DateTime.Now;
                                        st._RetourStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                                        st.CheckIfReadyToBeSaved();
                                        instrumentname = st._WorkingStationLevel._Parameters._Instrument._Name;
                                        buttonCollimCheck.EnableButton();
                                    }
                                    else buttonCollimCheck.EnableButton();
                                }
                                else buttonCollimCheck.DisableButton();
                            }
                            else buttonCollimCheck.DisableButton();
                            buttonSelectLevel.ChangeNameAndDescription(string.Format(R.String_GM_SelectInstrument, instrumentname));
                        }
                        else
                        {
                            if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                            msgBlocked += " " + R.String_GM_MsgBlockInstrument;
                            stepValid = false;
                            buttonCollimCheck.DisableButton();
                        }
                    }
                }
                else
                {
                    if (st._AllerStationLevel._Parameters._Instrument == null)
                    {
                        if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                        msgBlocked += " " + R.String_GM_MsgBlockInstrument;
                        stepValid = false;
                        buttonCollimCheck.DisableButton();
                    }
                    else
                    {
                        if (st._AllerStationLevel._Parameters._Instrument._Name == R.String_Unknown)
                        {
                            if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                            msgBlocked += " " + R.String_GM_MsgBlockInstrument;
                            stepValid = false;
                            buttonCollimCheck.DisableButton();
                        }
                    }
                }
                if (st._defaultStaff == null)
                {
                    if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                    msgBlocked += " " + R.String_GM_MsgBlockStaff;
                    stepValid = false;
                }
                else
                {
                    if (st._defaultStaff._Name == R.String_Unknown)
                    {
                        if (msgBlocked != R.String_GM_MsgBlockAdministrative) { msgBlocked += ","; }
                        msgBlocked += " " + R.String_GM_MsgBlockStaff;
                        stepValid = false;
                    }
                    else
                    {
                        buttonSelectLevellingStaff.SetColors(Drawing.Color.GreenYellow);
                        buttonSelectLevellingStaff.ChangeImage(R.StatusGood);
                        if (st._defaultStaff != null) staffName = st._defaultStaff._Name;
                        buttonSelectLevellingStaff.ChangeNameAndDescription(string.Format(R.String_GM_SelectDefaultStaff, staffName));
                    }
                }
                if (st._secondaryStaff == null)
                {
                    secondaryStaffName = "...";
                    buttonSelectSecondaryStaff.ChangeNameAndDescription(string.Format(R.String_GM_SelectSecondaryStaff, secondaryStaffName));
                    buttonSelectSecondaryStaff.SetColors(Drawing.Color.Orange);
                    buttonSelectSecondaryStaff.ChangeImage(R.Second_staff);
                    stepIsSkipable = true;
                }
                else
                {
                    if (st._secondaryStaff._Name == R.String_Unknown)
                    {
                        secondaryStaffName = "...";
                        buttonSelectSecondaryStaff.ChangeNameAndDescription(string.Format(R.String_GM_SelectSecondaryStaff, secondaryStaffName));
                        buttonSelectSecondaryStaff.SetColors(Drawing.Color.Orange);
                        buttonSelectSecondaryStaff.ChangeImage(R.Second_staff);
                        stepIsSkipable = true;
                    }
                    else
                    {
                        buttonSelectSecondaryStaff.SetColors(Drawing.Color.GreenYellow);
                        buttonSelectSecondaryStaff.ChangeImage(R.StatusGood);
                        if (st._secondaryStaff != null) secondaryStaffName = st._secondaryStaff._Name;
                        buttonSelectSecondaryStaff.ChangeNameAndDescription(string.Format(R.String_GM_SelectSecondaryStaff, secondaryStaffName));
                    }
                }
                ///vérifie qu'on a fait un zéro staff check
                if (guidedModule.ParentModule != null)
                {
                    if (guidedModule.ParentModule is Common.Guided.Group.Module)
                    {
                        Common.Guided.Group.Module GrGm = guidedModule.ParentModule as Common.Guided.Group.Module;
                        if (GrGm.staffUsed.Find(x => x._zeroCorrectionReading != na) != null)
                        {
                            buttonZeroStaffCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Good);
                        }
                        else
                        {
                            buttonZeroStaffCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                            stepIsSkipable = true;
                        }
                    }
                    else
                    {
                        buttonZeroStaffCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                        stepIsSkipable = true;
                    }
                }
                else
                {
                    buttonZeroStaffCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                    stepIsSkipable = true;
                }
                ///vérifie qu'on a fait un contrôle colimation avec le niveau sélectionné
                if (guidedModule.ParentModule != null)
                {
                    if (guidedModule.ParentModule is Common.Guided.Group.Module)
                    {
                        Common.Guided.Group.Module GrGm = guidedModule.ParentModule as Common.Guided.Group.Module;
                        if (st._WorkingStationLevel._Parameters._Instrument != null)
                        {
                            if (st._WorkingStationLevel._Parameters._Instrument._Name != R.String_Unknown)
                            {
                                string actualInstrumentName = st._WorkingStationLevel._Parameters._Instrument._Name;
                                if (GrGm.levelUsed.Find(x => x._ErrorCollim != na && x._Name == instrumentname) != null)
                                {
                                    buttonCollimCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Good);
                                }
                                else
                                {
                                    buttonCollimCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                                    stepIsSkipable = true;
                                }
                            }
                        }

                    }
                    else
                    {
                        buttonCollimCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                        stepIsSkipable = true;
                    }
                }
                else
                {
                    buttonCollimCheck.SetColors(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                    stepIsSkipable = true;
                }
                e.guidedModule.CurrentStep.MessageFromTest = msgBlocked;
                e.step.CheckValidityOf(stepValid);
            };
            s.Leaving += delegate (object source, StepEventArgs e)
            {
                Station.Module st = e.ActiveStationModule as Station.Module;
                if (st._WorkingStationLevel._Parameters._Instrument != null)
                {
                    if (instrumentname != st._WorkingStationLevel._Parameters._Instrument._Name) st.View.MaximizeDataGridView();
                }
                if (guidedModule.ParentModule is Common.Guided.Group.Module)
                {
                    Common.Guided.Group.Module grGm = guidedModule.ParentModule as Common.Guided.Group.Module;
                    if (grGm.SubGuidedModules.Count >= 7)
                    {
                        if (grGm.SubGuidedModules[6].guideModuleType == ENUM.GuidedModuleType.AlignmentLevellingCheck)
                        {
                            Station.Module stationLevelModuleCheck = grGm.SubGuidedModules[6]._ActiveStationModule as Station.Module;
                            if (stationLevelModuleCheck._WorkingStationLevel._Parameters._Instrument != null)
                            {
                                if (instrumentname != stationLevelModuleCheck._WorkingStationLevel._Parameters._Instrument._Name) stationLevelModuleCheck.View.MaximizeDataGridView();
                            }
                        }
                    }
                }
            };
            return s;
        }
    }


}
