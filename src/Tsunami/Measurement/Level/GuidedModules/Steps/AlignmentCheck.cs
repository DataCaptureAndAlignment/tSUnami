﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using TSU.Views;
using System.Windows.Forms;
using M = TSU;
using L = TSU.Level;
using TSU.Common.Guided.Steps;

namespace TSU.Level.GuidedModules.Steps
{

    static public class AlignmentCheck
    {
        static public Common.Guided.Module Get(M.Module parentModule)
        {
            Common.Guided.Group.Module GrGm = parentModule as Common.Guided.Group.Module;

            // Guided.Module gm = new Guided.Module(parentModule, String.Format(R.StringCheminement_StationName, "", "A", "B"));
            Common.Guided.Module gm = new Common.Guided.Module(parentModule, $"{R.StringCheminement_StationNameCheck} {""};A=>B");
            gm.guideModuleType = ENUM.GuidedModuleType.AlignmentLevellingCheck;
            gm.ObservationType = Common.ObservationType.Level;
            // Invisible step: : Add needed station modules
            gm.SwitchToANewStationModule(new TSU.Level.Station.Module(gm));

            BuildSteps(gm);

            gm.Start();

            return gm;
        }

        private static void BuildSteps(Common.Guided.Module gm)
        {
            /// Step 0 : Choose Cala Points
            gm.Steps.Add(Measurement.ChooseCalaPointsForElementAlignment(gm));

            /// Step 1 : Mesures alignement aller
            gm.Steps.Add(Measurement.LevelAlignmentAllerCheck(gm));

            /// Step 2 : Mesures retour
            gm.Steps.Add(Measurement.LevelAlignmentRetourCheck(gm));

            /// Step 3 : Mesures reprise
            gm.Steps.Add(Measurement.LevelAlignmentRepriseCheck(gm));

            /// Step 4 : Show dat saved file
            gm.Steps.Add(Measurement.ShowDatFiles(gm));

            /// Step 5 : Show LGC Files aller
            gm.Steps.Add(Measurement.ShowAllerLGCFiles(gm));

            /// Step 6 : Show LGC Files retour
            gm.Steps.Add(Measurement.ShowRetourLGCFiles(gm));

            /// Step 7 : Show LGC Files reprise
            gm.Steps.Add(Measurement.ShowRepriseLGCFiles(gm));
        }

        internal static void ReCreateWhatIsNotSerialized(Common.Guided.Module gm)
        {
            gm.Steps = new List<Step>();
            TSU.Level.Station.Module st = gm._ActiveStationModule as TSU.Level.Station.Module;
            if (st != null) { st.View.moduleType = ENUM.ModuleType.Guided; }
            //gm.currentIndex = 0;
            BuildSteps(gm);
            //gm.Start();
            //gm.MoveToStep(gm.currentIndex);
        }
    }
}
