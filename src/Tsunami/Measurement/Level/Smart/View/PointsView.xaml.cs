﻿using TSU.Views;

namespace TSU.Level.Smart.View
{
    public partial class PointsView : TsuUserControl
    {
        public PointsView() : base() => InitializeComponent();

        public override TsuView MainView => DataContext is ViewModel vm ? vm.MainView : base.MainView;
    }
}
