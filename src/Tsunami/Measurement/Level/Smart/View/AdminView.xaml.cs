﻿using TSU.Views;

namespace TSU.Level.Smart.View
{
    public partial class AdminView : TsuUserControl
    {
        public AdminView() : base() => InitializeComponent();

        public override TsuView MainView => DataContext is ViewModel vm ? vm.MainView : base.MainView;
    }
}
