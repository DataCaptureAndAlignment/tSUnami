﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using TSU.Views;
using R = TSU.Properties.Resources;

namespace TSU.Level.Smart.View
{
    public partial class Main : Common.Smart.View
    {
        private Buttons buttons;

        public new Module Module
        {
            get
            {
                return _Module as Module;
            }
        }

        public Main() :
            base()
        {
            InitializeComponent();
        }

        public Main(Module parentModule) :
            base(parentModule)
        {
            InitializeComponent();
            buttons = new Buttons(this);
        }

        private void ShowContextMenuGlobal()
        {
            contextButtons.Clear();
            contextButtons.Add(buttons.FinishModule);
            this.ShowPopUpMenu(contextButtons);
        }

        private void CloseView()
        {
            Control.ControlCollection ctrlColl = _PanelBottom.Controls;
            for (int i = ctrlColl.Count - 1; i >= 0; i--)
            {
                Control curCtrl = ctrlColl[i];
                if (curCtrl is ElementHost eh && eh.Child is IDisposable disp)
                {
#pragma warning disable S3966 // Objects should not be disposed more than once
                    disp.Dispose();
#pragma warning restore S3966 // Objects should not be disposed more than once
                    eh.Child = null;
                }
                ctrlColl.RemoveAt(i);
                curCtrl.Dispose();
            }
        }

        internal void SwitchToAdmin()
        {
            CloseView();
            HostView(new AdminView() { DataContext = Module.ViewModel });
        }

        internal void SwitchToSequence()
        {
            CloseView();
            HostView(new SequenceView() { DataContext = Module.ViewModel });
        }

        internal void SwitchToPoints()
        {
            CloseView();
            HostView(new PointsView() { DataContext = Module.ViewModel });
        }

        internal void SwitchToMeasure()
        {
            CloseView();
            HostView(new MeasureView() { DataContext = Module.ViewModel });
        }

        internal void SwitchToLog()
        {
            CloseView();
            HostView(new LogView() { DataContext = Module.ViewModel });
        }

        internal Form PrepareForm(Form page)
        {
            page.TopLevel = false;
            page.Tag = this;
            page.Dock = DockStyle.Fill;
            page.Show();
            return page;
        }

        internal void HostView(System.Windows.Controls.UserControl page)
        {
            ElementHost eh = new ElementHost
            {
                BackColor = Color.White,
                Dock = DockStyle.Fill,
                Child = page
            };
            eh.Show();
            _PanelBottom.Controls.Add(eh);
        }

        private void FinishModule()
        {
            TryAction(Module.FinishModule);
            CloseView();
        }

        private void PanelTop_Click(object sender, EventArgs e)
        {
            ShowContextMenuGlobal();
        }

        internal void UpdateMeasuresDataGridLayout()
        {
            ElementHost eh = _PanelBottom.Controls.OfType<ElementHost>().FirstOrDefault();
            MeasureView mv = eh?.Child as MeasureView;
            mv?.UpdateDataGridLayout();
        }

        class Buttons
        {
            public BigButton FinishModule;

            public Buttons(Main view)
            {
                FinishModule = new BigButton(R.String_GM_FinishModule, R.FinishHim, view.FinishModule);
            }
        }
    }
}
