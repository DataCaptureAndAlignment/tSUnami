﻿using TSU.Views;

namespace TSU.Level.Smart.View
{
    public partial class LogView : TsuUserControl
    {
        public LogView() : base() => InitializeComponent();

        public override TsuView MainView => DataContext is ViewModel vm ? vm.MainView : base.MainView;
    }
}
