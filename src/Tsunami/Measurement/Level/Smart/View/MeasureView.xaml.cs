﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using TSU.Views;

namespace TSU.Level.Smart.View
{
    public partial class MeasureView : TsuUserControl
    {
        public MeasureView() : base()
        {
            InitializeComponent();
        }

        public override TsuView MainView
        {
            get
            {
                if (DataContext is ViewModel vm)
                    return vm.MainView;
                else
                    return base.MainView;
            }
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataContext is ViewModel vm && vm.CurrentPage == ViewModel.Page.Measure)
                TextBoxRawLevelReading.Focus();
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                BindingOperations.GetBindingExpression((DependencyObject)sender, TextBox.TextProperty)?.UpdateSource();
            else if (e.Key == Key.Up && DataContext is ViewModel vm1)
            {
                if (vm1.CurrentMeasureIndex > 0)
                {
                    BindingOperations.GetBindingExpression((DependencyObject)sender, TextBox.TextProperty)?.UpdateSource();
                    vm1.CurrentMeasureIndex--;
                }
            }
            else if (e.Key == Key.Down && DataContext is ViewModel vm2)
            {
                if (vm2.CurrentMeasureIndex < vm2.AllMeasures.Count - 1)
                {
                    BindingOperations.GetBindingExpression((DependencyObject)sender, TextBox.TextProperty)?.UpdateSource();
                    vm2.CurrentMeasureIndex++;
                }
            }
        }

        public void UpdateDataGridLayout()
        {
            //Save columns
            List<DataGridColumn> lst = new List<DataGridColumn>(MeasuresDataGrid.Columns);

            //Delete columns, except the 1st one, which has a complex binding
            for (int i = MeasuresDataGrid.Columns.Count - 1; i > 0; i--)
                MeasuresDataGrid.Columns.RemoveAt(i);

            //Recreate columns, except the 1st one
            for (int i = 1; i < lst.Count; i++)
                MeasuresDataGrid.Columns.Add(lst[i]);
        }
    }
}
