﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using TSU.Common.Blocks;
using TSU.Common.Elements;
using TSU.Common.Smart;
using TSU.Level.Compute;
using C = TSU.Common.Elements.Composites;

namespace TSU.Level.Smart
{
    public class StationViewModel : ViewModelBase
    {
        public StationViewModel()
        {
            Debug.WriteInConsole($"StationViewModel ctor");
            NumberAsInt = 0;
            AllR2RRMS = new ObservableCollection<RoundToRoundRmsViewModel>();
        }

        public StationViewModel(C.SequenceNiv seq) : this()
        {
            Sequence = seq;
            LoadSequence();
        }

        public C.SequenceNiv Sequence { get; set; }

        public void LoadSequence()
        {
            // FirstAndLastPoints
            StringBuilder ret = new StringBuilder();
            if (Sequence.Elements.Count > 0)
            {
                ret.Append(Sequence.Elements[0]._Name);
                if (Sequence.Elements.Count > 1)
                {
                    ret.AppendLine();
                    ret.Append(Sequence.Elements[Sequence.Elements.Count - 1]._Name);
                }
            }
            FirstAndLastPoints = ret.ToString();

            // AllPoints
            StringBuilder ret2 = new StringBuilder();
            foreach (Element e in Sequence.Elements)
            {
                if (ret2.Length > 0)
                    ret2.AppendLine();
                ret2.Append(e._Name);
            }
            AllPoints = ret2.ToString();
        }

        private string firstAndLastPoints;
        [XmlIgnore]
        public string FirstAndLastPoints
        {
            get => firstAndLastPoints;
            set => SetProperty(ref firstAndLastPoints, value);
        }

        private string allPoints;
        [XmlIgnore]
        public string AllPoints
        {
            get => allPoints;
            set => SetProperty(ref allPoints, value);
        }

        [XmlIgnore]
        public ViewModel Parent { get; set; }

        internal void InitializeWithParent(ViewModel parent)
        {
            Debug.WriteInConsole("StationViewModel.InitializeWithParent");

            // Remove notifications
            Suspend();

            Parent = parent;

            //Restore or begin notifications            
            PropertyChanged += parent.OnStationPropertyChanged;

            // For new stations, we may not have a value
            if (CurrentRoundAsInt <= 0)
                CurrentRoundAsInt = 1;
        }

        private string numberAsString;
        [XmlIgnore]
        public string NumberAsString
        {
            get => numberAsString;
            set => SetProperty(ref numberAsString, value);
        }

        private int numberAsInt;
        public int NumberAsInt
        {
            get => numberAsInt;
            set => SetProperty(ref numberAsInt, value);
        }

        public string StationModuleGuid { get; set; }

        private Station.Module stationModule;
        [XmlIgnore]
        public Station.Module StationModule
        {
            get
            {
                if (stationModule == null && !string.IsNullOrEmpty(StationModuleGuid) && Parent?.SmartModule?.StationModules != null)
                {
                    Guid toFind = Guid.Parse(StationModuleGuid);
                    stationModule = Parent.SmartModule.StationModules.OfType<Station.Module>().FirstOrDefault(sm => sm.Guid.Equals(toFind));
                }

                return stationModule;
            }
        }

        private string currentRoundAsString;
        [XmlIgnore]
        public string CurrentRoundAsString
        {
            get => currentRoundAsString;
            set => SetProperty(ref currentRoundAsString, value);
        }

        private int currentRoundAsInt;

        public int CurrentRoundAsInt
        {
            get => currentRoundAsInt;
            set
            {
                if (SetProperty(ref currentRoundAsInt, value))
                    CurrentRoundAsString = $"{currentRoundAsInt:D1}/2";
            }
        }

        public int RoundCount { get; set; } = 2;

        public CloneableList<string> RoundGuids { get; set; }

        public IEnumerable<Station> AllRounds
        {
            get
            {
                if (RoundGuids != null)
                    foreach (string stationGuid in RoundGuids)
                        yield return StationModule.GetStationLevelByGuid(stationGuid);
            }
        }        

        public override string ToString()
        {
            StringBuilder ret = new StringBuilder();
            ret.Append("Number=");
            ret.Append(NumberAsString);
            ret.Append(",Sequence={");
            bool first = true;
            foreach (Element e in Sequence.Elements)
            {
                if (first)
                    first = false;
                else
                    ret.Append(",");
                ret.Append(e._Name);
            }
            ret.Append("}");
            return ret.ToString();
        }

        public void ComputeAllRmsAndUpdate()
        {
            // Get the Station.Module if it exists; stop if there's none
            Station.Module stm = StationModule;
            if (stm == null)
                return;

            RoundToRoundRms.Compute(AllRounds.ToList(),
                out List<RoundToRoundRms> allR2RRms,
                out RoundToRoundRms bestR2RRms,
                out double? avgRMS);

            // Update the Measure tab
            BestR2RRMSAsString = bestR2RRms == null ? "NA" : MeasureFormatting.FormatDistance(bestR2RRms.Rms * 100000d, "0.0", false);
            AvgR2RRMSAsString = avgRMS == null ? "NA" : MeasureFormatting.FormatDistance(avgRMS.Value * 100000d, "0.0", false);

            // Update the Log tab
            UpdateViewModelList(listToUpdate: AllR2RRMS,
                                newValues: allR2RRms,
                                ElementInitializer: Parent.InitRoundToRoundRmsViewModel,
                                ElementUpdater: UpdateRoundToRoundRmsViewModel);

            // Assign the roles to the rounds
            if (bestR2RRms != null)
            {
                int idx = 0;
                int nbReprise = 0;
                // Make a copy of the stations, as we will override the links
                List<Station> allRounds = AllRounds.ToList();
                // Re-assign the roles
                foreach (Station round in allRounds)
                {
                    if (idx == bestR2RRms.Round1)
                    {
                        round._Parameters._LevelingDirection = ENUM.LevelingDirection.ALLER;
                        round._Parameters._State = new Common.Station.State.Good();
                        stm._AllerStationLevel = round;

                    }
                    else if (idx == bestR2RRms.Round2)
                    {
                        round._Parameters._LevelingDirection = ENUM.LevelingDirection.RETOUR;
                        round._Parameters._State = new Common.Station.State.Good();
                        stm._RetourStationLevel = round;
                    }
                    else
                    {
                        round._Parameters._LevelingDirection = ENUM.LevelingDirection.REPRISE;
                        round._Parameters._State = new Common.Station.State.Bad();
                        stm._RepriseStationLevel[nbReprise] = round;
                        nbReprise++;
                    }
                    idx++;
                }
            }

            // Recompute all stations by switching to them
            stm.ChangeLevelingDirectionToAller();
            stm.ChangeLevelingDirectionToRetour();
            int nbRepriseFinal = stm._RepriseStationLevel.Count;
            for (int idxReprise = 0; idxReprise < nbRepriseFinal; idxReprise++)
            {
                stm.ChangeLevelingDirectionToReprise(idxReprise + 1);
            }
        }

        private static void UpdateRoundToRoundRmsViewModel(RoundToRoundRmsViewModel model, RoundToRoundRms rms)
        {
            model.R2RRms = rms;
        }

        internal void AddRound()
        {
            Debug.WriteInConsole($"Before creating new Round, RoundGuids={RoundGuids}");
            StationModule.AddRepriseStation();
            Debug.WriteInConsole($"After creating new Round, RoundGuids={RoundGuids}");
            //Memorize the link to station
            RoundGuids.Add(StationModule._WorkingStationLevel.Guid.ToString());
            Debug.WriteInConsole($"After adding new Round, RoundGuids={RoundGuids}");
        }

        private string bestR2RRMSAsString;

        [XmlIgnore]
        public string BestR2RRMSAsString
        {
            get => bestR2RRMSAsString;
            set => SetProperty(ref bestR2RRMSAsString, value);
        }

        private string avgR2RRMSAsString;

        [XmlIgnore]
        public string AvgR2RRMSAsString
        {
            get => avgR2RRMSAsString;
            set => SetProperty(ref avgR2RRMSAsString, value);
        }

        private ObservableCollection<RoundToRoundRmsViewModel> allR2RRMS;

        [XmlIgnore]
        public ObservableCollection<RoundToRoundRmsViewModel> AllR2RRMS
        {
            get => allR2RRMS;
            set => SetProperty(ref allR2RRMS, value);
        }

        private StationToStationComparisonViewModel s2SComparison;

        [XmlIgnore]
        public StationToStationComparisonViewModel S2SComparison
        {
            get => s2SComparison;
            set => SetProperty(ref s2SComparison, value);
        }
    }
}
