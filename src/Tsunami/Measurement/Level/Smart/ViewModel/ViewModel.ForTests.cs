﻿using System.Linq;
using TSU.Common.Blocks;
using CI = TSU.Common.Instruments;

// This file contains methods intended to give acces to some elements for tests

namespace TSU.Level.Smart
{
    public partial class ViewModel : ViewModelBase
    {
        public CI.LevelingStaff Test_GetStaffByName(string name)
        {
            return DefaultsStationModule._InstrumentManager.GetByClass(CI.InstrumentClasses.MIRE)
                                                           .OfType<CI.LevelingStaff>()
                                                           .FirstOrDefault(x => x._Name == name);
        }

        /// <summary>
        /// Used to set the staff without dialog box
        /// </summary>
        /// <param name="staffName"></param>
        public void Test_SetCurrentMeasureStaff(CI.LevelingStaff newStaff)
        {
            CurrentStation.StationModule.ChangeStaff(currentMeasure.Measure, newStaff);
            // Update the ViewModel to match the changes in Measure
            currentMeasure.LoadMeasure();
        }

        /// <summary>
        /// To open a sequence without selection and without question
        /// </summary>
        /// <param name="filenameFullPath"></param>
        /// <param name="forcedAnswer"></param>
        public void Test_BtnOpenSequenceInnerAction(string filenameFullPath, int forcedAnswer)
        {
            BtnOpenSequenceInnerAction(filenameFullPath, forcedAnswer);
        }

        /// <summary>
        /// To open a theo file without selection
        /// </summary>
        /// <param name="filePath"></param>
        public void Test_BtnTheoClickInnerAction(string filePath)
        {
            BtnTheoClickInnerAction(filePath);
        }

        public void Test_ExportAllMeasurementsToGeode(out string filepath, out Result saveDatOK)
        {
            ExportAllMeasurementsToGeode(out filepath, out saveDatOK);
        }

    }
}