﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using TSU.Common.Blocks;
using TSU.Level.Compute;

namespace TSU.Level.Smart
{
    public class StationToStationComparisonViewModel : ViewModelBase
    {
        public StationToStationComparisonViewModel()
        {
            CommonPoints = new ObservableCollection<CommonPointViewModel>();
            Clear();
        }

        internal void Clear()
        {
            ShowStations = Visibility.Collapsed;
            ShowTable = Visibility.Collapsed;
        }

        internal void UpdateStationToStationComparison(StationToStationComparison s2sc, TSU.Level.Smart.ViewModel vm)
        {
            int s1 = s2sc.Station1 + 1;
            int s2 = s2sc.Station2 + 1;
            if (s2sc.CommonPoints.Count == 0)
            {
                ShowTable = Visibility.Collapsed;
                Stations = $"St{s1:D2} and St{s2:D2} have no common points";
                ShowStations = Visibility.Visible;
            }
            else
            {
                ShowStations = Visibility.Collapsed;
                UpdateViewModelList(listToUpdate: CommonPoints,
                                    newValues: s2sc.CommonPoints,
                                    ElementInitializer: vm.InitCommonPointViewModel,
                                    ElementUpdater: UpdateCommonPointViewModel);
                MeanVsStation1AllerHeader = $"St{s1:D2}-A";
                MeanVsStation1RetourHeader = $"St{s1:D2}-R";
                MeanVsStation2AllerHeader = $"St{s2:D2}-A";
                MeanVsStation2RetourHeader = $"St{s2:D2}-R";
                ShowTable = Visibility.Visible;
            }
        }

        private static void UpdateCommonPointViewModel(CommonPointViewModel model, CommonPoint point)
        {
            model.CommonPoint = point;
        }

        private Visibility showStations;
        public Visibility ShowStations
        {
            get => showStations;
            set => SetProperty(ref showStations, value);
        }

        private string stations;
        public string Stations
        {
            get => stations;
            set => SetProperty(ref stations, value);
        }

        private Visibility showTable;
        public Visibility ShowTable
        {
            get => showTable;
            set => SetProperty(ref showTable, value);
        }

        private string meanVsStation1AllerHeader;
        public string MeanVsStation1AllerHeader
        {
            get => meanVsStation1AllerHeader;
            set => SetProperty(ref meanVsStation1AllerHeader, value);
        }

        private string meanVsStation1RetourHeader;
        public string MeanVsStation1RetourHeader
        {
            get => meanVsStation1RetourHeader;
            set => SetProperty(ref meanVsStation1RetourHeader, value);
        }

        private string meanVsStation2AllerHeader;
        public string MeanVsStation2AllerHeader
        {
            get => meanVsStation2AllerHeader;
            set => SetProperty(ref meanVsStation2AllerHeader, value);
        }

        private string meanVsStation2RetourHeader;
        public string MeanVsStation2RetourHeader
        {
            get => meanVsStation2RetourHeader;
            set => SetProperty(ref meanVsStation2RetourHeader, value);
        }

        private ObservableCollection<CommonPointViewModel> commonPoints;
        public ObservableCollection<CommonPointViewModel> CommonPoints
        {
            get => commonPoints;
            set => SetProperty(ref commonPoints, value);
        }
    }
}
