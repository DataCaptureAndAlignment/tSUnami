﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using System.Xml.Serialization;
using TSU.Common.Blocks;
using TSU.Level.Smart.View;

namespace TSU.Level.Smart
{
    [XmlType("Level.Smart.ViewModel")]
    public partial class ViewModel : ViewModelBase
    {
        [XmlIgnore]
        public Module SmartModule { get; private set; }

        [XmlIgnore]
        public Main MainView
        {
            get
            {
                return SmartModule?.View as Main;
            }
        }

        [XmlIgnore]
        public Station ActiveLevellingStation
        {
            get
            {
                return SmartModule?._ActiveStationModule?._Station as Station;
            }
        }

        [XmlIgnore]
        public Station.Module ActiveLevellingStationModule
        {
            get
            {
                return SmartModule?._ActiveStationModule as Station.Module;
            }
        }

        [XmlIgnore]
        public Station.Module DefaultsStationModule
        {
            get
            {
                return SmartModule?.StationModules[0] as Station.Module;
            }
        }

        /// <summary>
        /// Parameterless constructor for serialization
        /// </summary>
        public ViewModel()
        {
            AllStations = new ObservableCollection<StationViewModel>();
            AllStations.CollectionChanged += OnAllStationsCollectionChanged;
            AllMeasures = new ObservableCollection<MeasureOfLevelViewModel>();
        }

        public ViewModel(Module smartModule) : this()
        {
            SmartModule = smartModule;
            InitializeAdminPage();
        }

        private void OnAllStationsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
                foreach (object i in e.OldItems)
                    if (i is StationViewModel svm)
                        svm.Suspend();

            if (e.NewItems != null)
                foreach (object i in e.NewItems)
                    if (i is StationViewModel svm)
                    {
                        svm.InitializeWithParent(this);
                        svm.LoadSequence();
                    }

            OnPropertyChanged(nameof(AllStations));
        }

        public void OnStationPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
        }

        public void OnMeasurePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
        }

        public void LoadModule(Module smartModuleToLoad)
        {
            Debug.WriteInConsole("LoadModule called");
            SmartModule = smartModuleToLoad;
            //Remove the views that were created automatically by deserialization
            foreach (Station.Module module in SmartModule.StationModules.OfType<Station.Module>())
            {
                module.CreateViewAutmatically = false;
                if (module.View != null)
                {
                    module.View.Dispose();
                    module.View = null;
                }
            }
            //We add the listener here, so that the OnChange/CanExecute aren't called before we have a link to the module
            PropertyChanged += OnChange;
            //Restore the current page
            UpdateCurrentPage();
            //Restore the stations
            OnPropertyChanged(nameof(AllStations));
            if (CurrentStationIndex >= AllStations.Count)
                CurrentStationIndex = AllStations.Count - 1;
            else
                OnPropertyChanged(nameof(CurrentStationIndex));
            OnPropertyChanged(nameof(CurrentStation));
            // Recompute the log page for all stations
            ComputeAll(AllStations);
        }

        [XmlType("Level.Smart.ViewModel.Page")]
        public enum Page
        {
            Admin,
            Sequence,
            Points,
            Measure,
            Log
        }

        private Page currentPage;

        public Page CurrentPage
        {
            get => currentPage;
            set
            {
                //if (!ConfirmDiscard())
                //    return;
                currentPage = value;
                UpdateCurrentPage();
            }
        }

        private void UpdateCurrentPage()
        {
            switch (currentPage)
            {
                case Page.Admin:
                    MainView?.SwitchToAdmin();
                    break;
                case Page.Sequence:
                    MainView?.SwitchToSequence();
                    break;
                case Page.Points:
                    MainView?.SwitchToPoints();
                    break;
                case Page.Measure:
                    MainView?.SwitchToMeasure();
                    InitMeasurePage();
                    break;
                case Page.Log:
                    MainView?.SwitchToLog();
                    break;
            }
        }

        #region BtnReturnToAdministrativePage
        private DelegateCommand btnReturnToAdministrativePage;
        [XmlIgnore]
        public ICommand BtnReturnToAdministrativePage
        {
            get
            {
                if (btnReturnToAdministrativePage == null)
                    btnReturnToAdministrativePage = new DelegateCommand(BtnReturnToAdministrativePageClick);
                return btnReturnToAdministrativePage;
            }
        }
        private void BtnReturnToAdministrativePageClick()
        {
            try
            {
                CurrentPage = Page.Admin;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }
        #endregion

        #region BtnSequences
        private DelegateCommand btnSequence;
        [XmlIgnore]
        public ICommand BtnSequence
        {
            get
            {
                if (btnSequence == null)
                    btnSequence = new DelegateCommand(BtnSequenceClick, BtnSequenceCanExecute);
                return btnSequence;
            }
        }
        private bool BtnSequenceCanExecute()
        {
            try
            {
                return CheckOperation();
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }
        private void BtnSequenceClick()
        {
            try
            {
                CurrentPage = Page.Sequence;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }
        #endregion

        #region BtnPoints
        private DelegateCommand btnPoints;
        [XmlIgnore]
        public ICommand BtnPoints
        {
            get
            {
                if (btnPoints == null)
                    btnPoints = new DelegateCommand(BtnPointsClick);
                return btnPoints;
            }
        }
        private void BtnPointsClick()
        {
            try
            {
                CurrentPage = Page.Points;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }
        #endregion

        #region BtnMeasure
        private DelegateCommand btnMeasure;
        [XmlIgnore]
        public ICommand BtnMeasure
        {
            get
            {
                if (btnMeasure == null)
                    btnMeasure = new DelegateCommand(BtnMeasureClick, BtnMeasureCanExecute);
                return btnMeasure;
            }
        }
        private bool BtnMeasureCanExecute()
        {
            try
            {
                return currentStation != null;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }
        private void BtnMeasureClick()
        {
            try
            {
                CurrentPage = Page.Measure;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }
        #endregion

        #region BtnLog
        private DelegateCommand btnLog;

        [XmlIgnore]
        public ICommand BtnLog
        {
            get
            {
                if (btnLog == null)
                    btnLog = new DelegateCommand(BtnLogClick);
                return btnLog;
            }
        }
        private void BtnLogClick()
        {
            try
            {
                CurrentPage = Page.Log;
                //UpdateLogPage();
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }
        #endregion


        private void OnChange(object sender, PropertyChangedEventArgs e)
        {
            try
            {
                int id = Debug.GetNextId();
                Debug.WriteInConsole($"begin OnChange({e.PropertyName})", id);
                OnChangeAdminPage(sender, e);
                OnChangeSequencesPage(sender, e);
                OnChangeMeasuresPage(sender, e);
                Debug.WriteInConsole($"end OnChange({e.PropertyName})", id);
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        private void ShowMessageOfBug(Exception ex, [CallerMemberName] string callerMemberName = "?")
        {
            string title = $"Exception in {callerMemberName}";
            string message = ex.Message;

            if (MainView != null)
            {
                MainView.ShowMessageOfBug($"{title};{message}", ex);
            }
            else
            {
                System.Windows.MessageBox.Show(message, title);
            }
        }

        /// <summary>
        /// Stop managing the update events
        /// </summary>
        public override void Suspend()
        {
            base.Suspend();
            if (AllStations != null)
            {
                AllStations.CollectionChanged -= OnAllStationsCollectionChanged;
                foreach (var s in AllStations)
                    s.Suspend();
            }
            if (AllMeasures != null)
                foreach (var m in AllMeasures)
                    m.Suspend();
        }
    }
}
