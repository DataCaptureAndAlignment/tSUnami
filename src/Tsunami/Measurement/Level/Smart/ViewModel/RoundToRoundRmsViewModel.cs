﻿using TSU.Common.Blocks;
using TSU.Common.Smart;
using TSU.Level.Compute;

namespace TSU.Level.Smart
{
    public class RoundToRoundRmsViewModel : ViewModelBase
    {
        private RoundToRoundRms r2RRms;

        public RoundToRoundRms R2RRms
        {
            get => r2RRms;
            set
            {
                r2RRms = value;
                Rms = IsValid(r2RRms.Rms) ? MeasureFormatting.FormatDistance(r2RRms.Rms * 100000d, "0.0", false) : "NA";
                Rounds = $"{r2RRms.Round1 + 1}-{r2RRms.Round2 + 1}";
            }
        }

        private static double na => Tsunami2.Preferences.Values.na;

        private static bool IsValid(double d) => !double.IsNaN(d) && !double.IsInfinity(d) && d != na;

        private string rms;
        public string Rms
        {
            get => rms;
            set => SetProperty(ref rms, value);
        }

        private string rounds;
        public string Rounds
        {
            get => rounds;
            set => SetProperty(ref rounds, value);
        }
    }
}
