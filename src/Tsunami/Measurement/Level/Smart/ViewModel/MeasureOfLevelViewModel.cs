﻿using System;
using System.ComponentModel;
using TSU.Common.Blocks;
using TSU.Common.Measures;
using TSU.Common.Smart;
using R = TSU.Properties.Resources;

namespace TSU.Level.Smart
{
    public class MeasureOfLevelViewModel : ViewModelBase
    {
        public MeasureOfLevel Measure { get; set; } = null;
        public ViewModel Parent { get; private set; }
        public StationViewModel Station { get; set; } = null;

        public bool FromInstrument { get; set; } = false;

        private string distance;
        public string Distance
        {
            get => distance;
            set => SetProperty(ref distance, value);
        }

        private string distanceError;
        public string DistanceError
        {
            get => distanceError;
            set => SetProperty(ref distanceError, value);
        }

        private string rawLevelReading;
        public string RawLevelReading
        {
            get => rawLevelReading;
            set => SetProperty(ref rawLevelReading, value);
        }

        private string rawLevelReadingError;
        public string RawLevelReadingError
        {
            get => rawLevelReadingError;
            set => SetProperty(ref rawLevelReadingError, value);
        }

        private string pointName;
        public string PointName
        {
            get => pointName;
            set => SetProperty(ref pointName, value);
        }

        private string staffName;
        public string StaffName
        {
            get => staffName;
            set => SetProperty(ref staffName, value);
        }

        private string extension;
        public string Extension
        {
            get => extension;
            set => SetProperty(ref extension, value);
        }

        private string extensionError;
        public string ExtensionError
        {
            get => extensionError;
            set => SetProperty(ref extensionError, value);
        }

        private string delta;
        public string Delta
        {
            get => delta;
            set => SetProperty(ref delta, value);
        }

        internal void CopyMeasureFromInstrument(MeasureOfLevel input)
        {
            Parent.AcceptChanges = false;
            Measure._RawLevelReading = input._RawLevelReading;
            Measure._RawEmqLevelReading = input._RawEmqLevelReading;
            Measure._Distance = input._Distance;
            Measure._EmqDistanceReading = input._EmqDistanceReading;
            Measure._Compass = input._Compass;
            Measure._InstrumentSN = input._InstrumentSN;
            Measure._Date = input._Date;
            FromInstrument = true;
            Parent.AcceptChanges = false;
            SaveMeasure();
        }

        internal void LoadMeasure()
        {
            if (Measure._RawLevelReading.IsNa())
                RawLevelReading = "";
            else
                RawLevelReading = MeasureFormatting.FormatDistance(Measure._RawLevelReading * 100000d, MeasureFormatting.DistanceUnit.centmm);
            if (Measure._Distance.IsNa())
                Distance = "";
            else
                Distance = MeasureFormatting.FormatDistance(Measure._Distance, MeasureFormatting.DistanceUnit.m, false);
            if (Measure._Extension.IsNa())
                Extension = "";
            else
                Extension = MeasureFormatting.FormatDistance(Measure._Extension * 1000d, MeasureFormatting.DistanceUnit.mm);
            Delta = ComputeDelta();
            CheckInputs(out _, out _, out _);
            StaffName = Measure._staff?._Name ?? R.StringLevel_UnknownStaff;
            PointName = Measure._PointName;
        }

        private string ComputeDelta()
        {
            if (Measure._RawLevelReading.IsNa())
                return "";

            int measuresCount = 0;
            double rawReadingSum = 0d;

            foreach (Station r in Station.AllRounds)
                foreach (MeasureOfLevel m in r._MeasureOfLevel)
                    if (!ReferenceEquals(m, Measure)
                        && m._PointName == Measure._PointName
                        && !m._RawLevelReading.IsNa())
                    {
                        measuresCount++;
                        rawReadingSum += m._RawLevelReading;
                    }

            if (measuresCount == 0)
                return R.String_Unknown;

            return MeasureFormatting.FormatDistance((Measure._RawLevelReading - (rawReadingSum / measuresCount)) * 100000d, MeasureFormatting.DistanceUnit.centmm);
        }

        internal void SaveMeasure()
        {
            CheckInputs(out double rawLevelReadingStorageValue,
                        out double distanceStorageValue,
                        out double extensionStorageValue);

            if (Math.Abs(rawLevelReadingStorageValue - Measure._RawLevelReading) >= 0.000005d
                || Math.Abs(distanceStorageValue - Measure._Distance) >= 0.000005d
                || Math.Abs(extensionStorageValue - Measure._Extension) >= 0.000005d)
            {
                Station?.StationModule?._WorkingStationLevel?.CopyAsObsolete(Measure);
                Measure._RawLevelReading = rawLevelReadingStorageValue;
                Measure._Date = DateTime.Now;
                Measure._staff._SpatialDistanceTargetHeight = rawLevelReadingStorageValue;
                Measure._Distance = distanceStorageValue;
                Measure._Extension = extensionStorageValue;
                if (Station?.StationModule?._WorkingStationLevel?.ParametersBasic != null)
                    Station.StationModule._WorkingStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                Station?.StationModule?.UpdateOffsets();
                LoadMeasure();
            }
        }

        private void CheckInputs(out double rawLevelReadingStorageValue, out double distanceStorageValue, out double extensionStorageValue)
        {
            if (double.TryParse(RawLevelReading, out double rawLevelReadingAsDouble))
            {
                RawLevelReadingError = string.Empty;
                rawLevelReadingStorageValue = rawLevelReadingAsDouble / 100000d;
                RawLevelReading = MeasureFormatting.FormatDistance(rawLevelReadingAsDouble, MeasureFormatting.DistanceUnit.centmm);
            }
            else
            {
                RawLevelReadingError = R.StringSmart_RawLevelReadingInvalid;
                rawLevelReadingStorageValue = Tsunami2.Preferences.Values.na;
            }
            if (double.TryParse(Distance, out double distanceAsDouble))
            {
                //discard the negative sign
                if (distanceAsDouble < 0d) distanceAsDouble = -distanceAsDouble;
                distanceStorageValue = distanceAsDouble;
                Distance = MeasureFormatting.FormatDistance(distanceAsDouble, MeasureFormatting.DistanceUnit.m, false);

                if (FromInstrument
                    && (Measure._Distance <= 1.8
                       || (Measure._Distance >= 13.25 && Measure._Distance <= 13.5)
                       || (Measure._Distance >= 26.65 && Measure._Distance <= 26.9)
                       || Measure._Distance >= 30))
                    DistanceError = R.StringSmart_DistanceInvalidRange;
                else
                    DistanceError = string.Empty;
            }
            else
            {
                DistanceError = R.StringSmart_DistanceInvalid;
                distanceStorageValue = Tsunami2.Preferences.Values.na;
            }

            if (double.TryParse(Extension, out double extensionAsDouble))
            {
                ExtensionError = string.Empty;
                extensionStorageValue = extensionAsDouble / 1000d;
                Extension = MeasureFormatting.FormatDistance(extensionAsDouble, MeasureFormatting.DistanceUnit.mm);
            }
            else
            {
                ExtensionError = R.StringSmart_ExtensionInvalid;
                extensionStorageValue = Tsunami2.Preferences.Values.na;
            }
        }

        internal void Initialize(ViewModel parent, MeasureOfLevel measure)
        {
            Debug.WriteInConsole($"MeasureOfLevelViewModel.Initialize for measure {measure.Guid}");

            // Remove notifications
            Suspend();

            Station = parent.CurrentStation;
            Measure = measure;
            Parent = parent;
            LoadMeasure();

            //Restore or begin notifications            
            PropertyChanged += OnPropertyChanged;
        }

        public void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Parent.OnMeasureChanged(sender, e, this);
        }

        public override void Suspend()
        {
            base.Suspend();
            Debug.WriteInConsole($"Suspended MeasureOfLevelViewModel for measure {Measure?.Guid}");
        }

        public override string ToString()
        {
            return $"Level Measurement ViewModel on {Measure}";
        }
    }
}
