﻿using TSU.Common.Blocks;
using TSU.Common.Smart;
using TSU.Level.Compute;

namespace TSU.Level.Smart
{
    public class CommonPointViewModel : ViewModelBase
    {
        private CommonPoint commonPoint;

        public CommonPoint CommonPoint
        {
            get => commonPoint;
            set
            {
                commonPoint = value;
                PointName = commonPoint.PointName;
                MeanVsStation1Aller = FormatMeasure(commonPoint.MeanVsStation1Aller);
                MeanVsStation1Retour = FormatMeasure(commonPoint.MeanVsStation1Retour);
                MeanVsStation2Aller = FormatMeasure(commonPoint.MeanVsStation2Aller);
                MeanVsStation2Retour = FormatMeasure(commonPoint.MeanVsStation2Retour);
            }
        }

        private static string FormatMeasure(double d)
        {
            if (!double.IsNaN(d) && !double.IsInfinity(d) && d != Tsunami2.Preferences.Values.na)
                return MeasureFormatting.FormatDistance(d * 100000d, MeasureFormatting.DistanceUnit.centmm, true);
            else
                return "NA";
        }

        private string pointName;
        public string PointName
        {
            get => pointName;
            set => SetProperty(ref pointName, value);
        }

        private string meanVsStation1Aller;
        public string MeanVsStation1Aller
        {
            get => meanVsStation1Aller;
            set => SetProperty(ref meanVsStation1Aller, value);
        }

        private string meanVsStation1Retour;
        public string MeanVsStation1Retour
        {
            get => meanVsStation1Retour;
            set => SetProperty(ref meanVsStation1Retour, value);
        }

        private string meanVsStation2Aller;
        public string MeanVsStation2Aller
        {
            get => meanVsStation2Aller;
            set => SetProperty(ref meanVsStation2Aller, value);
        }

        private string meanVsStation2Retour;
        public string MeanVsStation2Retour
        {
            get => meanVsStation2Retour;
            set => SetProperty(ref meanVsStation2Retour, value);
        }

    }
}
