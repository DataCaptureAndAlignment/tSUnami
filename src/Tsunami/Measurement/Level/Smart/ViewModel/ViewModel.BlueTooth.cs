﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using TSU.Common.Blocks;
using CI = TSU.Common.Instruments;
using LS15 = TSU.Common.Instruments.Device.LS15;
using R = TSU.Properties.Resources;

namespace TSU.Level.Smart
{
    public partial class ViewModel : ViewModelBase
    {

        #region BtnConnectBt

        private DelegateCommand btnConnectBt;

        public ICommand BtnConnectBt
        {
            get
            {
                if (btnConnectBt == null)
                {
                    btnConnectBt = new DelegateCommand(BtnConnectBtClick, BtnConnectBtCanExecute);
                }

                return btnConnectBt;
            }
        }

        private void BtnConnectBtClick()
        {
            try
            {
                LS15.View view = CurrentInstrumentModule._TsuView as LS15.View;

                void OnShow(object sender, EventArgs args)
                {
                    view.buttonPower.PerformClick();
                }

                view.ShowInMessageTsu("View", R.T_CLOSE, null, onShowingMessage: OnShow);
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        private bool BtnConnectBtCanExecute()
        {
            try
            {
                // BT connection is possible only for LS15
                return CurrentInstrumentModule is LS15.Module m && !m.InstrumentIsOn;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        #endregion

        #region BtnMeasureBt

        private DelegateCommand btnMeasureBt;

        public ICommand BtnMeasureBt
        {
            get
            {
                if (btnMeasureBt == null)
                    btnMeasureBt = new DelegateCommand(BtnMeasureBtClick, IsCurrentInstrumentDigital);

                return btnMeasureBt;
            }
        }

        private void BtnMeasureBtClick()
        {
            try
            {
                // Create the station module if it's the first measure
                if (CurrentStation.StationModule == null)
                {
                    AcceptChanges = false;
                    // Initialize the station
                    InitStationModule(CurrentStation);
                    // Create the real measures
                    UpdateMeasuresFromStationModule();
                    AcceptChanges = true;
                }

                if (CurrentInstrumentModule is LS15.Module module)
                {
                    LS15.View view = module._TsuView as LS15.View;

                    void OnShow(object sender, EventArgs args)
                    {
                        if (module.InstrumentIsOn)
                            StartMeasurement();
                        else
                        {
                            module._NumberOfMeasurement = MeasPerPointAsNumber ?? 3;
                            view.backgroundWorker_Connecting.RunWorkerCompleted += OnConnectWorkerCompleted;
                            view.buttonPower.PerformClick();
                        }
                    }

                    void OnConnectWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
                    {
                        view.backgroundWorker_Connecting.RunWorkerCompleted -= OnConnectWorkerCompleted;
                        // If not cancelled by user or aborted by an error
                        if (!e.Cancelled && e.Error == null)
                            StartMeasurement();
                    }

                    void StartMeasurement()
                    {
                        module._NumberOfMeasurement = MeasPerPointAsNumber ?? 3;
                        view.buttonMeasure.PerformClick();
                    }

                    view.ShowInMessageTsu("View", R.T_CLOSE, null, onShowingMessage: OnShow);
                }
                else
                    ShowMessageOfBug(new TsuException("Not a LS15"));
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        private CI.Module CurrentInstrumentModule
        {
            get
            {
                return CurrentStation?.StationModule?._InstrumentManager?.SelectedInstrumentModule
                    ?? DefaultsStationModule?._InstrumentManager?.SelectedInstrumentModule;
            }
        }

        private bool IsCurrentInstrumentDigital()
        {
            return CurrentInstrumentModule is LS15.Module;
        }

        public Visibility ShowMeasure
        {
            get
            {
                if (IsCurrentInstrumentDigital())
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
        }

    }
}