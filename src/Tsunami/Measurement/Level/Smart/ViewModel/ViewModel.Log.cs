﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Xml.Serialization;
using TSU.Common.Blocks;
using TSU.Level.Compute;
using TSU.Views.Message;
using static TSU.Common.Station.State;
using static TSU.Level.Station;
using R = TSU.Properties.Resources;

namespace TSU.Level.Smart
{
    public partial class ViewModel : ViewModelBase
    {
        internal RoundToRoundRmsViewModel InitRoundToRoundRmsViewModel(RoundToRoundRms r)
        {
            RoundToRoundRmsViewModel roundToRoundRmsViewModel = new RoundToRoundRmsViewModel
            {
                R2RRms = r,
            };
            roundToRoundRmsViewModel.PropertyChanged += (a, b) =>
            {
                OnPropertyChanged(b.PropertyName);
            };
            return roundToRoundRmsViewModel;
        }

        private StationToStationComparisonViewModel InitStationToStationComparisonViewModel()
        {
            StationToStationComparisonViewModel s2sComparisonViewModel = new StationToStationComparisonViewModel();
            s2sComparisonViewModel.PropertyChanged += (a, b) =>
            {
                OnPropertyChanged(b.PropertyName);
            };
            return s2sComparisonViewModel;
        }

        public CommonPointViewModel InitCommonPointViewModel(CommonPoint commonPoint)
        {
            CommonPointViewModel commonPointViewModel = new CommonPointViewModel
            {
                CommonPoint = commonPoint,
            };
            commonPointViewModel.PropertyChanged += (a, b) =>
            {
                OnPropertyChanged(b.PropertyName);
            };
            return commonPointViewModel;
        }

        private void ComputeAll(IEnumerable<StationViewModel> stvms)
        {
            // Compute the RMS results for all stations (used in Log and Measure pages)
            foreach (StationViewModel stvm in stvms)
                try
                {
                    stvm.ComputeAllRmsAndUpdate();
                }
                catch (Exception ex)
                {
                    Debug.WriteInConsole($"Exception happenned during ComputeAllRmsAndUpdate : {ex.Message}\r\n{ex.StackTrace}");
                }

            // Compute all common points comparison (used in Log page)
            try
            {
                ComputeCommonPointsComparisonsAndUpdate();
            }
            catch (Exception ex)
            {
                Debug.WriteInConsole($"Exception happenned during CommonPointsComparison : {ex.Message}\r\n{ex.StackTrace}");
            }
        }

        private void ComputeCommonPointsComparisonsAndUpdate()
        {

            // Compute the new values
            List<StationToStationComparison> allS2SComparisons = StationToStationComparison.Compute(AllStations.Select(s => s.StationModule)).ToList();

            // Update, initialize or clear the models to match the results
            for (int i = 0; i < AllStations.Count; i++)
            {
                StationViewModel s = AllStations[i];
                if (s.S2SComparison == null)
                    s.S2SComparison = InitStationToStationComparisonViewModel();
                StationToStationComparison news2sc = allS2SComparisons.Find(s2sc => s2sc.Station1 == i);
                if (news2sc == null)
                    s.S2SComparison.Clear();
                else
                    s.S2SComparison.UpdateStationToStationComparison(news2sc, this);
            }

        }

        #region BtnExportAllMeasurement

        private DelegateCommand btnExportAllMeasurement;

        [XmlIgnore]
        public ICommand BtnExportAllMeasurement
        {
            get
            {
                if (btnExportAllMeasurement == null)
                    btnExportAllMeasurement = new DelegateCommand(BtnExportAllMeasurementClick, BtnExportAllMeasurementCanExecute);
                return btnExportAllMeasurement;
            }
        }

        private bool BtnExportAllMeasurementCanExecute()
        {
            try
            {
                return AllStations.Any(
                    s => s.StationModule?.AllStationLevels.Any(
                        l => l._Parameters._State is StationLevelReadyToBeSaved
                    ) ?? false
                );
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        //Compile the regex to extract the variable parts of the station name
        private static Regex regex = new Regex("STLEV_(.*)_(.*)_(.*)_(.*)", RegexOptions.Compiled);

        private void BtnExportAllMeasurementClick()
        {
            try
            {
                ExportAllMeasurementsToGeode(out string filepath, out Result saveDatOK);

                if (saveDatOK.Success)
                {
                    string titleAndMessage = string.Format(R.StringSmart_AllStationsExported, filepath);
                    DsaFlag ShowSaveMessageOfSuccess = DsaFlag.GetByNameOrAdd(SmartModule.DsaFlags, "ShowSaveMessageOfSuccess");
                    MessageInput mi = new MessageInput(MessageType.GoodNews, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_OK, R.T_OpenFileLocation },
                        DontShowAgain = ShowSaveMessageOfSuccess
                    };
                    if (mi.Show().TextOfButtonClicked == R.T_OpenFileLocation
                        && File.Exists(filepath))
                    {
                        Process.Start(new ProcessStartInfo("explorer.exe", " /select, " + filepath));
                    }
                }

                // Save the final module
                SmartModule.Save();

            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        private void ExportAllMeasurementsToGeode(out string filepath, out Result saveDatOK)
        {
            filepath = GetFileNameForGeode();

            //Reset the file
            if (File.Exists(filepath))
                File.Delete(filepath);

            saveDatOK = new Result();
            int stationNumber = 0;
            foreach (StationViewModel s in AllStations)
            {
                stationNumber++;
                Station.Module stationModule = s.StationModule;
                if (stationModule != null)
                {
                    int roundNumber = 0;
                    foreach (Station station in s.AllRounds)
                    {
                        roundNumber++;
                        Common.Station.State state = station._Parameters._State;
                        if (state is StationLevelReadyToBeSaved || state is StationLevelSaved)
                        {
                            bool asBad = false;

                            // Select the station
                            if (station == stationModule._AllerStationLevel)
                                stationModule.ChangeLevelingDirectionToAller();
                            else if (station == stationModule._RetourStationLevel)
                                stationModule.ChangeLevelingDirectionToRetour();
                            else
                            {
                                stationModule.ChangeLevelingDirectionToReprise(stationModule._RepriseStationLevel.IndexOf(station) + 1);
                                asBad = true;
                            }

                            // Reformat the station name
                            string stationName = stationModule._WorkingStationLevel._Name;
                            Match match = regex.Match(stationName);
                            if (match.Success)
                            {
                                string date = match.Groups[1].Value;
                                string time = match.Groups[2].Value;
                                string specific = match.Groups[4].Value;
                                stationName = $"STLEV_{stationNumber}_{date}_{time}_{roundNumber}_{specific}";
                            }

                            // Save the station
                            saveDatOK.MergeString(SaveToGeode.SaveDATLevelingStation(stationModule, filepath, stationName, false, asBad));
                        }
                    }
                }
            }
        }

        private string GetFileNameForGeode()
        {
            string dir = Tsunami2.Preferences.Values.Paths.MeasureOfTheDay;

            Station.Parameters parameters = DefaultsStationModule._WorkingStationLevel._Parameters;

            int geodeOp = parameters._Operation.value;
            DateTime geodeDate = parameters._Date;
            string geodeTeam = parameters._Team;

            string filepath = $"{dir}{geodeOp:00000}_{geodeDate:yyyy-MM-dd}_{geodeTeam:6}_SmartLev.dat";

            return filepath;
        }

        #endregion

    }
}
