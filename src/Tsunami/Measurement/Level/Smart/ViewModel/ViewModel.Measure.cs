﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using System.Xml.Serialization;
using TSU.Common;
using TSU.Common.Blocks;
using static TSU.Level.Station;
using CI = TSU.Common.Instruments;
using E = TSU.Common.Elements;
using M = TSU.Common.Measures;
using R = TSU.Properties.Resources;

namespace TSU.Level.Smart
{
    public partial class ViewModel : ViewModelBase
    {

        private string levellingDirection;
        [XmlIgnore]
        public string LevellingDirection
        {
            get => levellingDirection;
            set => SetProperty(ref levellingDirection, value);
        }

        private bool showAdd;
        [XmlIgnore]
        public bool ShowAdd
        {
            get => showAdd;
            set => SetProperty(ref showAdd, value);
        }

        private bool doNotShowAdd;
        [XmlIgnore]
        public bool DoNotShowAdd
        {
            get => doNotShowAdd;
            set => SetProperty(ref doNotShowAdd, value);
        }

        private ObservableCollection<MeasureOfLevelViewModel> allMeasures;
        [XmlIgnore]
        public ObservableCollection<MeasureOfLevelViewModel> AllMeasures
        {
            get => allMeasures;
            set => SetProperty(ref allMeasures, value);
        }

        private MeasureOfLevelViewModel currentMeasure;
        [XmlIgnore]
        public MeasureOfLevelViewModel CurrentMeasure
        {
            get => currentMeasure;
            set => SetProperty(ref currentMeasure, value);
        }

        private int currentMeasureIndex;
        public int CurrentMeasureIndex
        {
            get => currentMeasureIndex;
            set => SetProperty(ref currentMeasureIndex, value);
        }

        #region BtnNextStation

        private DelegateCommand btnNextStation;

        [XmlIgnore]
        public ICommand BtnNextStation
        {
            get
            {
                if (btnNextStation == null)
                    btnNextStation = new DelegateCommand(BtnNextStationClick, BtnNextStationCanExecute);
                return btnNextStation;
            }
        }

        private bool BtnNextStationCanExecute()
        {
            try
            {
                return AllStations.Count > 1;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        private void BtnNextStationClick()
        {
            try
            {
                int cur = CurrentStation.NumberAsInt;
                int next;
                if (cur >= AllStations.Count)
                    //If cur is the last station, select the first one
                    next = 0;
                else
                    //Add 1 to have the next, remove 1 to have an index -> no operation
                    next = cur;
                CurrentStation = AllStations[next];
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        #region BtnPreviousStation

        private DelegateCommand btnPreviousStation;

        [XmlIgnore]
        public ICommand BtnPreviousStation
        {
            get
            {
                if (btnPreviousStation == null)
                    btnPreviousStation = new DelegateCommand(BtnPreviousStationClick, BtnPreviousStationCanExecute);
                return btnPreviousStation;
            }
        }

        private bool BtnPreviousStationCanExecute()
        {
            try
            {
                return AllStations.Count > 1;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        private void BtnPreviousStationClick()
        {
            try
            {
                int cur = CurrentStation.NumberAsInt;
                int next;
                if (cur == 1)
                    //If cur is the first station, select the last one
                    next = AllStations.Count - 1;
                else
                    //Remove 1 to have the previous, remove 1 to have an index -> no operation
                    next = cur - 2;
                CurrentStation = AllStations[next];
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        #region BtnNextRound

        private DelegateCommand btnNextRound;

        [XmlIgnore]
        public ICommand BtnNextRound
        {
            get
            {
                if (btnNextRound == null)
                    btnNextRound = new DelegateCommand(BtnNextRoundClick, BtnNextRoundCanExecute);
                return btnNextRound;
            }
        }

        private void BtnNextRoundClick()
        {
            try
            {
                int cur = CurrentStation.CurrentRoundAsInt;
                //If cur is the last station, create a new one
                if (cur == CurrentStation.RoundCount)
                {
                    CurrentStation.RoundCount++;
                    if (CurrentStation.StationModule != null)
                        CurrentStation.AddRound();
                }
                CurrentStation.CurrentRoundAsInt = cur + 1;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        private bool BtnNextRoundCanExecute()
        {
            try
            {
                return CurrentStation != null && (CurrentStation.CurrentRoundAsInt < 9);
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        #endregion

        #region BtnPreviousRound

        private DelegateCommand btnPreviousRound;

        [XmlIgnore]
        public ICommand BtnPreviousRound
        {
            get
            {
                if (btnPreviousRound == null)
                    btnPreviousRound = new DelegateCommand(BtnPreviousRoundClick);
                return btnPreviousRound;
            }
        }

        private void BtnPreviousRoundClick()
        {
            try
            {
                int cur = CurrentStation.CurrentRoundAsInt;
                int next;
                if (cur == 1)
                    //If cur is the first station, select the last one
                    next = CurrentStation.RoundCount;
                else
                    //Remove 1 to have the previous
                    next = cur - 1;
                CurrentStation.CurrentRoundAsInt = next;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        #region BtnSelectStaffForMeasure

        private DelegateCommand btnSelectStaffForMeasure;

        public ICommand BtnSelectStaffForMeasure
        {
            get
            {
                if (btnSelectStaffForMeasure == null)
                    btnSelectStaffForMeasure = new DelegateCommand(BtnSelectStaffForMeasureClick);

                return btnSelectStaffForMeasure;
            }
        }

        private void BtnSelectStaffForMeasureClick()
        {
            try
            {
                CurrentStation.StationModule.ChangeStaff(currentMeasure.Measure);
                // Update the ViewModel to match the changes in Measure
                currentMeasure.LoadMeasure();
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        #endregion

        public void InitMeasurePage()
        {
            if (CurrentStation == null && AllStations != null && AllStations.Count > 0)
                CurrentStation = AllStations[0];
        }

        public void InitStationModule(StationViewModel station)
        {
            // The module 0 has the default settings, get a copy of it
            Station.Module oldStationModule = (Station.Module)SmartModule.StationModules[0];

            FinalModule finalModule = oldStationModule.FinalModule;
            finalModule.SetActiveStationModule(oldStationModule, false);
            Station.Module newStationModule = new Station.Module(finalModule, false);
            station.StationModuleGuid = newStationModule.Guid.ToString();
            newStationModule.ChangeOperationID(oldStationModule._AllerStationLevel.ParametersBasic._Operation);
            newStationModule.ChangeTeam(oldStationModule._AllerStationLevel.ParametersBasic._Team);
            newStationModule.SetGeodeFilePathInNewStationLevelModule(oldStationModule);
            newStationModule.SetInstrumentInNewStationLevelModule(oldStationModule);
            newStationModule.SetDefaultStaffInNewStationLevelModule(oldStationModule);
            newStationModule.SetSecondaryStaffInNewStationLevelModule(oldStationModule);
            newStationModule.ChangeTolerance(oldStationModule._AllerStationLevel._Parameters._Tolerance);
            newStationModule.ChangeTemperature(oldStationModule._AllerStationLevel._Parameters._Temperature);
            newStationModule.SetCoordinateSystemInNewStationLevelModule(oldStationModule);
            newStationModule.CreatedOn = DateTime.Now;
            finalModule.SetActiveStationModule(newStationModule, false);

            // Apply the settings
            newStationModule._Name = "STM of " + station.Sequence._Name;
            newStationModule._LevelingComputationStrategy = new SFBStrategy();
            newStationModule.SetPointsToMeasure(station.Sequence.GetPoints());

            //Force the creation of the return station
            newStationModule.ChangeLevelingDirectionToRetour();

            //Back to the first round
            newStationModule.ChangeLevelingDirectionToAller();

            //Memorize the round/station guid association, because they may change role depending on RMS values, but they should keep the same round number
            station.RoundGuids = new CloneableList<string>
            {
                newStationModule._AllerStationLevel.Guid.ToString(),
                newStationModule._RetourStationLevel.Guid.ToString()
            };

            //Create the extra rounds
            for (int i = station.RoundGuids.Count; i < station.RoundCount; i++)
                station.AddRound();
        }

        [XmlIgnore]
        public bool AcceptChanges { get; set; } = true;

        internal void OnMeasureChanged(object sender, PropertyChangedEventArgs e, MeasureOfLevelViewModel measureOfLevelViewModel)
        {
            if (AcceptChanges
                && (e.PropertyName == nameof(MeasureOfLevelViewModel.Distance)
                || e.PropertyName == nameof(MeasureOfLevelViewModel.Extension)
                || e.PropertyName == nameof(MeasureOfLevelViewModel.StaffName)
                || e.PropertyName == nameof(MeasureOfLevelViewModel.RawLevelReading))
                && (measureOfLevelViewModel == CurrentMeasure))
            {
                // Do not react on changes triggered on CurrentMeasure during the process
                AcceptChanges = false;

                try
                {
                    // Create the station module if it's the first measure
                    if (CurrentStation.StationModule == null)
                    {
                        // Save the values
                        string rawLevelReading = CurrentMeasure.RawLevelReading;
                        string extension = CurrentMeasure.Extension;
                        string distance = CurrentMeasure.Distance;
                        CI.LevelingStaff staff = CurrentMeasure.Measure._staff;
                        // Initialize the station
                        InitStationModule(CurrentStation);
                        // Create the real measures
                        UpdateMeasuresFromStationModule();
                        // Write the values in the new CurrentMeasure
                        CurrentMeasure.RawLevelReading = rawLevelReading;
                        CurrentMeasure.Extension = extension;
                        CurrentMeasure.Distance = distance;
                        CurrentMeasure.Measure._staff = staff;
                    }

                    // Convert the string values to double values (when format is ok) and save the double values to the Measure
                    // Also recomputes the offsets
                    SetWorkingLevel();
                    CurrentMeasure?.SaveMeasure();

                    // Reload the measures, to update the deltas, and restore the current measure
                    UpdateMeasuresFromStationModule();

                    // Compute the RMS and log page
                    ComputeAll(AllStations);
                }
                catch (Exception ex)
                {
                    ShowMessageOfBug(ex);
                }
                finally
                {
                    // Accept future chnages
                    AcceptChanges = true;
                }
            }

            if (e.PropertyName == nameof(MeasureOfLevelViewModel.Delta)
               || e.PropertyName == nameof(MeasureOfLevelViewModel.RawLevelReading))
            {
                MainView?.UpdateMeasuresDataGridLayout();
            }
        }

        private void OnChangeMeasuresPage(object sender, PropertyChangedEventArgs e)
        {

            if (e.PropertyName == nameof(CurrentMeasureIndex))
            {
                UpdateCurrentMeasure();
            }

            if (e.PropertyName == nameof(CurrentStation)
                || e.PropertyName == nameof(StationViewModel.CurrentRoundAsInt))
            {
                if (CurrentStation != null)
                {
                    // Clear all the measures to avoid crash because WPF doesn't understand what changed or not
                    if (e.PropertyName == nameof(CurrentStation))
                        AllMeasures = new ObservableCollection<MeasureOfLevelViewModel>();

                    if (CurrentStation.StationModule == null)
                    {
                        // Use temp measures to avoid creating the StationModule until we save
                        UpdateMeasuresFromPointList();
                    }
                    else
                    {
                        // Get the measures from StationModule
                        UpdateMeasuresFromStationModule();
                    }

                    // Select the first measure
                    if (AllMeasures.Count > 0)
                        CurrentMeasureIndex = 0;
                }
            }

            //Update dependant properties
            if ((e.PropertyName == nameof(CurrentStation)
               || e.PropertyName == nameof(StationViewModel.CurrentRoundAsInt)
               || e.PropertyName == nameof(StationViewModel.AllRounds))
               && CurrentStation != null)
            {
                ShowAdd = CurrentStation.CurrentRoundAsInt == CurrentStation.RoundCount;
                DoNotShowAdd = !ShowAdd;
            }

            if (e.PropertyName == nameof(CurrentStation)
                || e.PropertyName == nameof(StationViewModel.NumberAsInt))
            {
                if (CurrentStation == null)
                    LevellingDirection = string.Empty;
                else if (CurrentStation.NumberAsInt % 2 == 1)
                    LevellingDirection = R.StringSmart_LevellingDirectionBackward;
                else
                    LevellingDirection = R.StringSmart_LevellingDirectionForward;
            }

        }

        private void UpdateMeasuresFromPointList()
        {
            // Do not react on changes triggered on CurrentMeasure during the process
            AcceptChanges = false;

            List<M.MeasureOfLevel> tempMeasures = CurrentStation.Sequence.GetPoints().ConvertAll(InitMeasureOfLevel);
            UpdateViewModelList(listToUpdate: AllMeasures,
                                newValues: tempMeasures,
                                ElementInitializer: InitMeasureOfLevelViewModel,
                                ElementUpdater: UpdateMeasureOfLevelViewModel,
                                Match: MatchMeasure);
            UpdateCurrentMeasure();

            // React again
            AcceptChanges = true;
        }

        private static bool MatchMeasure(M.MeasureOfLevel measure, MeasureOfLevelViewModel model)
        {
            return ReferenceEquals(model.Measure, measure)
                || model.Measure?.PointGUID == measure?.PointGUID
                || model.Measure?._PointName == measure?._PointName;
        }

        private M.MeasureOfLevel InitMeasureOfLevel(E.Point p)
        {
            // Create a temporary measure
            M.MeasureOfLevel m = new M.MeasureOfLevel()
            {
                _Status = new M.States.Temporary(),
            };

            InitBlankMeasure(m, p, DefaultsStationModule._defaultStaff, DefaultsStationModule._secondaryStaff);

            return m;
        }

        private void UpdateMeasuresFromStationModule()
        {
            SetWorkingLevel();
            List<M.MeasureOfLevel> stationMeasures = CurrentStation.StationModule._WorkingStationLevel._MeasureOfLevel;
            List<M.MeasureOfLevel> orderedMeasures = CurrentStation.Sequence.GetPoints().ConvertAll(p => stationMeasures.Find(m => m._PointName == p._Name));
            UpdateViewModelList(listToUpdate: AllMeasures,
                                newValues: orderedMeasures,
                                ElementInitializer: InitMeasureOfLevelViewModel,
                                ElementUpdater: UpdateMeasureOfLevelViewModel,
                                Match: MatchMeasure);
            UpdateCurrentMeasure();
        }

        private MeasureOfLevelViewModel InitMeasureOfLevelViewModel(M.MeasureOfLevel m)
        {
            MeasureOfLevelViewModel measureOfLevelViewModel = new MeasureOfLevelViewModel();
            measureOfLevelViewModel.Initialize(this, m);
            return measureOfLevelViewModel;
        }

        private static void UpdateMeasureOfLevelViewModel(MeasureOfLevelViewModel model, M.MeasureOfLevel level)
        {
            model.Measure = level;
            model.LoadMeasure();
        }

        private void UpdateCurrentMeasure()
        {
            if (AllMeasures != null
                && CurrentMeasureIndex >= 0
                && CurrentMeasureIndex < AllMeasures.Count)
            {
                CurrentMeasure = AllMeasures[CurrentMeasureIndex];
            }
            else
            {
                CurrentMeasure = null;
            }
            // Force the update on all fields based on CurrentMeasure
            // Because the properties of the MeasureOfLevel can have changed before we assigned it to CurrentMeasure
            OnPropertyChanged(nameof(CurrentMeasure));
        }

        private void SetWorkingLevel()
        {
            int index = CurrentStation.CurrentRoundAsInt - 1;
            string guid = CurrentStation.RoundGuids[index];
            CurrentStation.StationModule.SetWorkingStationLevelByGuid(guid);
        }
    }
}
