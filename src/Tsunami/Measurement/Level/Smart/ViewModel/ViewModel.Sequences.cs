﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using System.Xml.Serialization;
using TSU.Common.Blocks;
using TSU.Common.Elements;
using TSU.Views.Message;
using C = TSU.Common.Elements.Composites;
using EM = TSU.Common.Elements.Manager;
using R = TSU.Properties.Resources;

namespace TSU.Level.Smart
{
    public partial class ViewModel
    {
        public ObservableCollection<StationViewModel> AllStations { get; set; }

        private StationViewModel currentStation;

        [XmlIgnore]
        public StationViewModel CurrentStation
        {
            get => currentStation;
            set => SetProperty(ref currentStation, value);
        }

        private int currentStationIndex;

        public int CurrentStationIndex
        {
            get => currentStationIndex;
            set => SetProperty(ref currentStationIndex, value);
        }

        private void LoadSequenceFile(string filenameFullPath, int forcedAnswer = -1)
        {
            EM.Module em = SmartModule._ElementManager;

            C.Sequence seq = new C.SequenceNivs
            {
                _Origin = filenameFullPath
            };

            em.LoadSequenceFromFile(seq, filenameFullPath, forcedAnswer);

            int nextNumber = AllStations.Count + 1;

            foreach (C.SequenceNiv sn in seq.Elements.OfType<C.SequenceNiv>())
            {
                AllStations.Add(new StationViewModel(sn));
                nextNumber++;
            }

            SequenceFilePath = filenameFullPath;
        }

        #region BtnDeleteStation

        private DelegateCommand btnDeleteStation;
        [XmlIgnore]
        public ICommand BtnDeleteStation
        {
            get
            {
                if (btnDeleteStation == null)
                    btnDeleteStation = new DelegateCommand(BtnDeleteStationClick, BtnDeleteStationCanExecute);
                return btnDeleteStation;
            }
        }

        private void BtnDeleteStationClick()
        {
            try
            {
                if (!BtnDeleteStationCanExecute())
                    return;

                MessageInput mi = new MessageInput(MessageType.Choice, R.StringSmart_ConfirmDeleteStation)
                {
                    ButtonTexts = CreationHelper.GetYesNoButtons(),
                    Sender = SmartModule._Name
                };
                MessageResult res = mi.Show();
                if (res.TextOfButtonClicked != R.T_YES)
                    return;

                int index = CurrentStation.NumberAsInt - 1;
                AllStations.RemoveAt(index);

                // Determine the new selected station
                // If the deleted station was not the last, select the next one
                if (index < AllStations.Count)
                    CurrentStation = AllStations[index];
                // If it was the last and not the only, select the new last
                else if (AllStations.Count > 0)
                    CurrentStation = AllStations[AllStations.Count - 1];
                // If it was the only station, select none
                else
                    CurrentStation = null;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        private bool BtnDeleteStationCanExecute()
        {
            try
            {
                return CurrentStation != null;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        #endregion

        #region BtnCopyStation

        private DelegateCommand btnCopyStation;
        [XmlIgnore]
        public ICommand BtnCopyStation
        {
            get
            {
                if (btnCopyStation == null)
                    btnCopyStation = new DelegateCommand(BtnCopyStationClick, BtnCopyStationCanExecute);
                return btnCopyStation;
            }
        }

        private void BtnCopyStationClick()
        {
            try
            {
                if (!BtnCopyStationCanExecute())
                    return;

                // Get all points
                List<Point> allPoints = currentStation.Sequence.GetPoints();

                // Add missing points
                foreach (Point point in allPoints)
                    if (!Points.Contains(point))
                        Points.Add(point);

                // Select all points
                SelectedPoints.Clear();
                SelectedPoints.AddRange(allPoints);

                // Switch to the points tab
                CurrentPage = Page.Points;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        private bool BtnCopyStationCanExecute()
        {
            try
            {
                return CurrentStation != null;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }

        #endregion

        #region BtnMoveDownStation
        private DelegateCommand btnMoveDownStation;
        [XmlIgnore]
        public ICommand BtnMoveDownStation
        {
            get
            {
                if (btnMoveDownStation == null)
                    btnMoveDownStation = new DelegateCommand(BtnMoveDownStationClick, BtnMoveDownStationCanExecute);
                return btnMoveDownStation;
            }
        }
        private bool BtnMoveDownStationCanExecute()
        {
            try
            {
                if (CurrentStation == null)
                    return false;

                return currentStation.NumberAsInt != AllStations.Count;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }
        }
        private void BtnMoveDownStationClick()
        {
            try
            {
                if (!BtnMoveDownStationCanExecute())
                    return;

                SwitchSequences(+1);
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }

        private void SwitchSequences(int delta)
        {
            StationViewModel current = CurrentStation;
            StationViewModel replacement = AllStations[current.NumberAsInt + delta - 1];
            Debug.WriteInConsole($"SwitchSequences, initially current={current}, replacement={replacement}");
            current.NumberAsInt += delta;
            replacement.NumberAsInt -= delta;
            Debug.WriteInConsole($"SwitchSequences, after renumbering current={current}, replacement={replacement}");
            AllStations[current.NumberAsInt - 1] = current;
            AllStations[replacement.NumberAsInt - 1] = replacement;
            // The number of the selected element is different,
            // although it's the same element, so we force refresh
            CurrentStation = current;
        }

        #endregion

        #region BtnMoveUpStation
        private DelegateCommand btnMoveUpStation;
        [XmlIgnore]
        public ICommand BtnMoveUpStation
        {
            get
            {
                if (btnMoveUpStation == null)
                    btnMoveUpStation = new DelegateCommand(BtnMoveUpStationClick, BtnMoveUpStationCanExecute);
                return btnMoveUpStation;
            }
        }
        private bool BtnMoveUpStationCanExecute()
        {
            try
            {
                if (CurrentStation == null)
                    return false;

                return currentStation.NumberAsInt != 1;
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
                return false;
            }

        }
        private void BtnMoveUpStationClick()
        {
            try
            {
                if (!BtnMoveUpStationCanExecute())
                    return;

                SwitchSequences(-1);
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(ex);
            }
        }
        #endregion

        private void OnChangeSequencesPage(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(CurrentStationIndex))
                if (CurrentStationIndex == -1)
                    CurrentStation = null;
                else
                    CurrentStation = AllStations[CurrentStationIndex];

            if (e.PropertyName == nameof(CurrentStation))
                if (CurrentStation == null)
                    CurrentStationIndex = -1;
                else
                    CurrentStationIndex = AllStations.IndexOf(CurrentStation);

            if (e.PropertyName == nameof(AllStations))
            {
                for (int i = 0; i < AllStations.Count; i++)
                {
                    StationViewModel s = AllStations[i];
                    int numberAsInt = i + 1;
                    s.NumberAsInt = numberAsInt;
                    s.NumberAsString = $"#{numberAsInt:D4}";
                }
            }

        }
    }
}
