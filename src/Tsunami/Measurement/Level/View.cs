﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using R = TSU.Properties.Resources;
using System.Windows.Forms;
using TSU.ENUM;
using System.Xml.Serialization;
using TSU.Views;
using E = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using T = TSU.Tools;
using System.Globalization;
using System.Linq;


namespace TSU.Level
{
    public class View : CompositeView
    {
        private Buttons buttons;
        private double na = Tsunami2.Preferences.Values.na;
        public Module LevelModule
        {
            get
            {
                return this._Module as Module;
            }
        }
        public View(Module parent)
            : base(parent, Orientation.Horizontal, ModuleType.Levelling)
        {
            this.Image = R.Level;
            this.buttons = new Buttons(this);
            string creationDate = "(" + R.StringBigButton_CreatedOn + this.Module.CreatedOn.ToString("G", CultureInfo.CreateSpecificCulture("nl-BE")) + ")";
            string buttontext = string.Format("{0} {1};{2}", T.Conversions.StringManipulation.SeparateTitleFromMessage(this._Module.NameAndDescription), creationDate, T.Conversions.StringManipulation.SeparateMessageFromTitle(this._Module.NameAndDescription));
            this.AddTopButton(buttontext,
                    R.Level, this.ShowContextMenuGlobal);
            AddPlusView(buttons.New);
            this.Visible = true;
            UpdateView();
        }
        /// <summary>
        /// Change le nom de la station active dans le top button
        /// </summary>
        /// <param name="stationLevelModule"></param>
        internal override void SetStationNameInTopButton(Common.Station.Module stationLevelModule)
        {
            this._PanelTop.Controls.Clear();
            string creationDate = "(" + R.StringBigButton_CreatedOn + this.Module.CreatedOn.ToString("G", CultureInfo.CreateSpecificCulture("nl-BE")) + ")";
            string buttontext = string.Format("{0} {1};{2}", T.Conversions.StringManipulation.SeparateTitleFromMessage(this._Module.NameAndDescription), creationDate, T.Conversions.StringManipulation.SeparateMessageFromTitle(this._Module.NameAndDescription));
            Common.FinalModule m;
            if ((m = this.LevelModule) != null)
            {
                int stationNumber = 0;
                foreach (Station.Module stationModule in m.StationModules)
                {
                    if (stationModule != null)
                    {
                        stationNumber++;
                        if (stationModule._Station._Name == stationLevelModule._Station._Name)
                        {
                            buttontext = string.Format("{0} {1}: {2};{3}",
                                T.Conversions.StringManipulation.SeparateTitleFromMessage(this._Module.NameAndDescription),
                                creationDate,
                                string.Format(T.Conversions.StringManipulation.SeparateTitleFromMessage(R.StringLevel_BigButtonLevelModuleView), stationNumber),
                                string.Format("{0} ({1})", T.Conversions.StringManipulation.SeparateMessageFromTitle(this._Module.NameAndDescription), stationModule._Station._Name));
                        }
                    }
                }
            }
            this.AddTopButton(buttontext, R.Level, this.ShowContextMenuGlobal);
        }
        private void ShowContextMenuGlobal()
        {
            this.contextButtons.Clear();
            //this.contextButtons.Add(buttons.Open);
            //this.contextButtons.Add(buttons.Save);
            this.contextButtons.Add(buttons.New);
            this.contextButtons.Add(buttons.FinishModule);
            if (this.LevelModule.StationModules.Count > 0) this.contextButtons.Add(buttons.zeroStaffCheck);
            if (this.LevelModule._ActiveStationModule != null)
            {
                Station.Module actualStationLevelModule = this.LevelModule._ActiveStationModule as Station.Module;
                Station.View v = this.LevelModule._ActiveStationModule.View as Station.View;
                if (v != null) v.CheckDatagridIsInEditMode();
                //this.contextButtons.Add(buttons.lgc2);
                //this.contextButtons.Add(buttons.CloneAller);
                //this.contextButtons.Add(buttons.CloneRetour);
                //this.contextButtons.Add(buttons.CloneWithTheoPoint);
                //this.contextButtons.Add(buttons.CloneForReturnWithTheoPoints);
                //this.contextButtons.Add(buttons.deleteActualStation);
                //this.buttons.lgc2.Enabled = (this.LevelModule.StationModules.Count > 1) ? true : false;
                //this.buttons.CloneAller.Enabled = (actualStationLevelModule._WorkingStationLevel._Parameters._Instrument._Model != null) ? true : false;
                //this.buttons.CloneRetour.Enabled = (actualStationLevelModule._WorkingStationLevel._Parameters._Instrument._Model != null) ? true : false;
                //this.buttons.CloneWithTheoPoint.Enabled = (actualStationLevelModule._WorkingStationLevel._Parameters._Instrument._Model != null && actualStationLevelModule._TheoPoint.Count >= 2) ? true : false;
                //this.buttons.CloneForReturnWithTheoPoints.Enabled = (actualStationLevelModule._WorkingStationLevel._Parameters._Instrument._Model != null && actualStationLevelModule._TheoPoint.Count >= 2) ? true : false;
                //this.buttons.deleteActualStation.Enabled = false;
                //Ne permet de faire un calcul LGC global que s'il y a au moins un  module
                this.contextButtons.Add(Common.Compute.Compensations.BeamOffsets.WorkFlow.Buttons.GetComputeButton(this.LevelModule.BOC_Compute));

                if (this.LevelModule.StationModules.Count > 0)
                {
                    if (this.LevelModule._ActiveStationModule is Station.Module am 
                        && am._WorkingStationLevel.ParametersBasic._Instrument is I.Level
                        && am._WorkingStationLevel.ParametersBasic._Instrument._Name != R.String_Unknown)
                        this.contextButtons.Add(buttons.CollimCheck);
                    this.contextButtons.Add(buttons.exportAllDat);
                    ///Bouton export new points seulement si points created by user
                    if (this.LevelModule._ActiveStationModule != null 
                     && this.LevelModule._ActiveStationModule.ElementModule != null)
                    {
                        List<E.Point> allPoints = this.LevelModule._ActiveStationModule.ElementModule.GetPointsInAllElements();
                        if (allPoints.Exists(x => x._Origin == "Created by user"))
                            this.contextButtons.Add(buttons.exportTheoDat);
                    }
                    this.contextButtons.Add(buttons.lgc2);
                }
                if (actualStationLevelModule._WorkingStationLevel._Parameters._Instrument._Model != null)
                {
                    this.contextButtons.Add(buttons.CloneAller);
                    this.contextButtons.Remove(buttons.New);
                    this.contextButtons.Add(buttons.CloneRetour);
                    if (actualStationLevelModule._TheoPoint.Count >= 2)
                    {
                        this.contextButtons.Add(buttons.CloneWithTheoPoint);
                        this.contextButtons.Add(buttons.CloneForReturnWithTheoPoints);
                    }
                }
            }
            Common.FinalModule m = this.LevelModule;
            if (m != null)
            {
                List<BigButton> stationButtons = new List<BigButton>();
                int stationNumber = 0;
                ///Ajoute le bouton de suppression de la station actuelle que s'il n'y a pas de mesures encodées et s'il y a plusieurs stations
                ///
                if (this.LevelModule.StationModules.Count > 1
                 && this.LevelModule._ActiveStationModule is Station.Module am
                 && !am.AllStationLevels.Any(s => s._MeasureOfLevel.Exists(x => x._RawLevelReading != na)))
                    this.contextButtons.Add(buttons.deleteActualStation);
                foreach (var stLevelModule in m.StationModules.OfType<Station.Module>())
                {
                    stationNumber++;
                    BigButton stationButton = new BigButton(
                        string.Format(R.StringLevel_BigButtonLevelModuleView, stationNumber, stLevelModule._Station._Name),
                        this.Image, stLevelModule.ShowStationLevelModule);
                    if (stLevelModule == this.LevelModule._ActiveStationModule)
                    {
                        stationButton.SetColors(Tsunami2.Preferences.Theme.Colors.Highlight);
                    }
                    stationButtons.Add(stationButton);
                }
                //Inverse la liste des stations
                for (int i = stationButtons.Count - 1; i >= 0; i--)
                {
                    this.contextButtons.Add(stationButtons[i]);
                }
            }
            this.ShowPopUpMenu(contextButtons);
            // il peut arriver lors de l'ajout d'un nouveau bouton que l'affichage des contrôles dans le popup menu ne soit pas dans l'ordre. Le réaffichage du pop up semble résoudre le problème.
            //if (!ok) { this.ShowContextMenuGlobal(); }
        }

        private void OnNewStation(object sender, EventArgs e)
        {
            NewStation();
        }
        public void NewStation()
        {
            this.TryAction(this.LevelModule.NewStation);
        }
        public void CloneAllerStation()
        {
            this.TryAction(this.LevelModule.CloneAllerStation);
        }
        public void CloneRetourStation()
        {
            this.TryAction(this.LevelModule.CloneRetourStation);
        }
        public void Save()
        {
            this.TryAction(() => this.LevelModule.Save());
        }
        //public void Open()
        //{
        //    this.TryAction(this.LevelModule.Open);
        //}
        public void CloneStationWithTheoPoints()
        {
            this.TryAction(this.LevelModule.CloneStationWithTheoPoints);
        }
        public void CloneStationWithTheoPointForReturn()
        {
            this.TryAction(this.LevelModule.CloneStationForReturnWithTheoPoints);
        }
        internal void DoCalculationWithLGC2()
        {
            this.TryAction(this.LevelModule.DoCalculationWithLGC2);
        }
        internal void DeleteActualStation()
        {
            this.TryAction(this.LevelModule.DeleteActualStation);
        }
        private void FinishModule()
        {
            this.TryAction(this.LevelModule.FinishModule);
        }
        private void ExportAllDatToGeode()
        {
            this.TryAction(this.LevelModule.ExportAllToGeode);
        }
        private void ExportNewPointTheoDat()
        {
            this.TryAction(this.LevelModule.ExportNewPointTheoDat);
        }
        private void ZeroStaffCheck()
        {
            this.TryAction(this.LevelModule.ZeroStaffCheck);
        }
        private void CollimCheck()
        {
            this.TryAction(this.LevelModule.CollimCheck);
        }
        class Buttons
        {
            private View view;
            public BigButton New;
            //public BigButton Open;
            public BigButton Save;
            public BigButton CloneAller;
            public BigButton CloneRetour;
            public BigButton CloneWithTheoPoint;
            public BigButton CloneForReturnWithTheoPoints;
            public BigButton lgc2;
            public BigButton deleteActualStation;
            public BigButton exportAllDat;
            public BigButton FinishModule;
            public BigButton exportTheoDat;
            public BigButton zeroStaffCheck;
            public BigButton CollimCheck;
            public Buttons(View view)
            {
                this.view = view;
                //Open = new BigButton(string.Format(R.T_OpenModule, this.view.LevelModule._Name),R.Open, view.Open);
                Save = new BigButton(string.Format(R.T_SaveModule, this.view.LevelModule._Name), R.Save, view.Save);
                New = new BigButton(R.StringLevel_NewStation, R.Add, view.NewStation);
                CloneAller = new BigButton(R.StringLevel_CloneAllerStation, R.Copy, view.CloneAllerStation);
                CloneRetour = new BigButton(R.StringLevel_CloneReturnSation, R.Copy, view.CloneRetourStation);
                CloneWithTheoPoint = new BigButton(R.StringLevel_CloneWithTheoPoints, R.Copy, view.CloneStationWithTheoPoints);
                CloneForReturnWithTheoPoints = new BigButton(R.StringLevel_CloneReturnStationWithTheoPoints, R.Copy, view.CloneStationWithTheoPointForReturn);
                lgc2 = new BigButton(R.StringLevel_GlobalCalculationLGC2, R.Lgc, view.DoCalculationWithLGC2);
                deleteActualStation = new BigButton(R.StringLevel_DeleteActualStation, R.Level_Delete, view.DeleteActualStation);
                FinishModule = new BigButton(R.String_GM_FinishModule, R.FinishHim, view.FinishModule);
                exportAllDat = new BigButton(R.StringLevel_SaveAll, R.Save, view.ExportAllDatToGeode);
                exportTheoDat = new BigButton(R.StringLevel_ExportTheoDat, R.Export, view.ExportNewPointTheoDat);
                zeroStaffCheck = new BigButton(R.StringLevel_ZeroStaffCheck, R.Zero_Staff_Check, view.ZeroStaffCheck);
                CollimCheck = new BigButton(R.StringLevel_CollimCheckButton, R.Level_Collim, view.CollimCheck);
            }
        }


    }
}
