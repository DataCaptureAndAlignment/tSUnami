﻿using System;
using System.Xml.Serialization;
using M = TSU.Common.Measures;
using R = TSU.Properties.Resources;

namespace TSU.Level
{
    public partial class Station
    {
        [XmlInclude(typeof(AllerStrategy))]
        [XmlInclude(typeof(RetourStrategy))]
        [XmlInclude(typeof(RepriseStrategy))]
        public abstract class LevelingDirectionStrategy
        {
            public string _name { get; set; }

            internal abstract void ChangeLevellingDirection(Module stationLevelModule, int numeroReprise = 0);

            internal abstract void ReverseMeasureStatus(string pointName, Module stationLevelModule);
        }

        public class AllerStrategy : LevelingDirectionStrategy
        {
            public AllerStrategy()
            {
                _name = "A";
            }
            internal override void ChangeLevellingDirection(Module stationLevelModule, int numeroReprise = 0)
            //change la station nivellement actuelle en aller
            {
                stationLevelModule._WorkingStationLevel = stationLevelModule._AllerStationLevel;
                stationLevelModule._WorkingStationLevel.Rename(this);
                string stationName = stationLevelModule._WorkingStationLevel._Name;
                stationLevelModule.measGroupName = stationName.Substring(0, 22) + stationName.Substring(24, stationName.Length - 24);
                stationLevelModule._Station = stationLevelModule._AllerStationLevel;
                stationLevelModule.UpdateOffsets(false, false);
                stationLevelModule.CheckIfReadyToBeSaved();
                if (stationLevelModule.FinalModule is Level.Module levelModule)
                    levelModule.View.SetStationNameInTopButton(stationLevelModule);
                if (stationLevelModule.FinalModule.ParentModule is Common.Guided.Group.Module groupModule)
                    groupModule.UpdateBigButtonTop();
            }
            /// <summary>
            /// Inverse le status d'une mesure et ajoute not used au commentaire
            /// </summary>
            /// <param name="pointName"></param>
            /// <param name="stationLevelModule"></param>
            internal override void ReverseMeasureStatus(string pointName, Module stationLevelModule)
            {
                M.MeasureOfLevel m = stationLevelModule._WorkingStationLevel._MeasureOfLevel.Find(x => x._PointName == pointName);
                if (m != null)
                {
                    if (m._Status is M.States.Bad)
                    {
                        m._Status = new M.States.Unknown();
                        if (m.CommentFromTsunami == R.StringLevel_BadMeas) m.CommentFromTsunami = "";
                    }
                    else
                    {

                        m._Status = new M.States.Bad();
                        //Pour mesure aller, on a droit à 80 caractère pour le comnmentaire
                        if ((m.Comment.Length + R.StringLine_BadMeas.Length) <= 80 && !m.Comment.Contains(R.StringLevel_BadMeas))
                            m.CommentFromTsunami = R.StringLevel_BadMeas;

                    }
                    stationLevelModule._WorkingStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                }
                stationLevelModule.UpdateOffsets();
            }
        }
        public class RetourStrategy : LevelingDirectionStrategy
        {
            public RetourStrategy()
            {
                _name = "R";
            }
            internal override void ChangeLevellingDirection(Module stationLevelModule, int numeroReprise = 0)
            //change la station nivellement actuelle en retour
            {
                stationLevelModule._WorkingStationLevel = stationLevelModule._RetourStationLevel;
                stationLevelModule._Station = stationLevelModule._RetourStationLevel;
                stationLevelModule._WorkingStationLevel.Rename(this);
                stationLevelModule.UpdateOffsets(false, false);
                string stationName = stationLevelModule._WorkingStationLevel._Name;
                stationLevelModule.measGroupName = stationName.Substring(0, 22) + stationName.Substring(24, stationName.Length - 24);
                stationLevelModule.CheckIfReadyToBeSaved();
                if (stationLevelModule.FinalModule is Level.Module levelModule)
                    levelModule.View.SetStationNameInTopButton(stationLevelModule);
                if (stationLevelModule.FinalModule.ParentModule is Common.Guided.Group.Module groupModule)
                    groupModule.UpdateBigButtonTop();
            }
            /// <summary>
            /// Inverse le status d'une mesure et ajoute not used au commentaire
            /// </summary>
            /// <param name="pointName"></param>
            /// <param name="stationLevelModule"></param>
            internal override void ReverseMeasureStatus(string pointName, Module stationLevelModule)
            {
                M.MeasureOfLevel m = stationLevelModule._WorkingStationLevel._MeasureOfLevel.Find(x => x._PointName == pointName);
                if (m != null)
                {
                    if (m._Status is M.States.Bad)
                    {
                        m._Status = new M.States.Unknown();
                        if (m.CommentFromTsunami == R.StringLevel_BadMeas) m.CommentFromTsunami = "";
                    }
                    else
                    {
                        m._Status = new M.States.Bad();
                        //Pour mesure retour, on a droit à 67 caractère pour le comnmentaire
                        if ((m.Comment.Length + R.StringLine_BadMeas.Length) <= 67 && !m.Comment.Contains(R.StringLevel_BadMeas))
                            m.CommentFromTsunami = R.StringLevel_BadMeas;
                    }
                    stationLevelModule._WorkingStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                }
                stationLevelModule.UpdateOffsets();
            }
        }
        public class RepriseStrategy : LevelingDirectionStrategy
        {
            public RepriseStrategy()
            {
                _name = "P";
            }

            internal override void ChangeLevellingDirection(Module stationLevelModule, int numeroReprise = 0)
            //change la station nivellement actuelle en reprise numéro numéroReprise
            {
                if (numeroReprise != 0 && numeroReprise <= stationLevelModule._RepriseStationLevel.Count)
                {
                    _name = "P" + numeroReprise.ToString();
                    stationLevelModule._WorkingStationLevel = stationLevelModule._RepriseStationLevel[numeroReprise - 1];
                    stationLevelModule._Station = stationLevelModule._RepriseStationLevel[numeroReprise - 1];
                    stationLevelModule._WorkingStationLevel.Rename(this);
                    string stationName = stationLevelModule._WorkingStationLevel._Name;
                    if (numeroReprise >= 10)
                    {
                        //Change le nom de station en celui de la station level en enlevant le caractère donnant le sens
                        stationLevelModule.measGroupName = stationName.Substring(0, 22) + stationName.Substring(26, stationName.Length - 26);
                    }
                    else
                    {
                        //Change le nom de station en celui de la station level en enlevant le caractère donnant le sens
                        stationLevelModule.measGroupName = stationName.Substring(0, 22) + stationName.Substring(25, stationName.Length - 25);
                    }
                    stationLevelModule.UpdateOffsets(false, false);
                    stationLevelModule.CheckIfReadyToBeSaved();
                }
                if (stationLevelModule.FinalModule is Level.Module levelModule)
                    levelModule.View.SetStationNameInTopButton(stationLevelModule);
                if (stationLevelModule.FinalModule.ParentModule is Common.Guided.Group.Module groupModule)
                    groupModule.UpdateBigButtonTop();
            }

            /// <summary>
            /// Inverse le status d'une mesure et ajoute not used au commentaire
            /// </summary>
            /// <param name="pointName"></param>
            /// <param name="stationLevelModule"></param>
            internal override void ReverseMeasureStatus(string pointName, Module stationLevelModule)
            {
                M.MeasureOfLevel m = stationLevelModule._WorkingStationLevel._MeasureOfLevel.Find(x => x._PointName == pointName);
                if (m != null)
                {
                    if (m._Status is M.States.Bad)
                    {
                        m._Status = new M.States.Unknown();
                        if (m.CommentFromTsunami == R.StringLevel_BadMeas) m.CommentFromTsunami = "";
                    }
                    else
                    {
                        m._Status = new M.States.Bad();
                        //Pour mesure reprise, on a droit à 59 caractère pour le comnmentaire
                        if ((m.Comment.Length + R.StringLine_BadMeas.Length) <= 59 && !m.Comment.Contains(R.StringLevel_BadMeas))
                            m.CommentFromTsunami = R.StringLevel_BadMeas;
                    }
                    stationLevelModule._WorkingStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                }
                stationLevelModule.UpdateOffsets();
            }
        }
    }
}
