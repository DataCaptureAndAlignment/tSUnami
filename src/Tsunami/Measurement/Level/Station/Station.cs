using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using TSU.Common.Compute;
using TSU.Common.Elements;
using TSU.Common.Measures;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using R = TSU.Properties.Resources;

namespace TSU.Level
{
    [Serializable]
    [XmlType(TypeName = "Level.Station")]
    public partial class Station : Common.Station
    {
        //variables

        public override CloneableList<M.Measure> MeasuresTaken
        {
            get
            {
                CloneableList<M.Measure> l = new CloneableList<M.Measure>();

                foreach (var item in this._ObsoleteMeasureOfLevel)
                {
                    item.IsObsolete = true;
                    if (item._Date.Year >= 2000)
                    {
                        l.Add(item);
                    }
                }
                l.AddRange(this._MeasureOfLevel);
                return l;
            }
        }

        public List<M.MeasureOfLevel> _MeasureOfLevel { get; set; }
        public List<M.MeasureOfLevel> _ObsoleteMeasureOfLevel { get; set; }
        public bool nameSetByUser { get; set; }
        public string specialPartOfStationName { get; set; }
        [XmlIgnore]
        public Parameters _Parameters
        {
            get
            {
                return this.ParametersBasic as Parameters;
            }
            set
            {
                this.ParametersBasic = value;
            }
        }
        public Station()
            //constructeur
            : base()
        {
            this._Parameters = new Parameters();
            this._MeasureOfLevel = new List<M.MeasureOfLevel>();
            this._ObsoleteMeasureOfLevel = new List<M.MeasureOfLevel>();
            this._Name = "STLEV_" + string.Format("{0:yyyyMMdd_HHmmss}" + "_", DateTime.Now);
            this._Parameters._StationName = this._Name;
            this._Parameters._Station = this;
            this.specialPartOfStationName = "";
            this.nameSetByUser = false;
        }

        public override string ToString()
        {
            return string.Format(R.TS_Station, this._Name);
        }

        public new Station DeepCopy()
        //copie avec un memory stream de toute la station level sauf l'instrument et la mire dont on garde la r�f�rence ancienne
        {
            using (MemoryStream stream = new MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                Station deepCopy = (Station)formatter.Deserialize(stream);
                // Get a new GUID
                deepCopy.Guid = Guid.NewGuid();
                // r�f�rence vers l'instrument actuel
                deepCopy._Parameters._Instrument = this._Parameters._Instrument;
                deepCopy.ParametersBasic._Instrument = deepCopy._Parameters._Instrument;

                //copie manuelle de la r�f�rence vers la mire pour chaque mesure
                foreach (M.MeasureOfLevel meas in deepCopy._MeasureOfLevel)
                {
                    meas._staff = this._MeasureOfLevel.Find(x => x._PointName == meas._PointName)._staff;
                }
                return deepCopy;
            }
        }

        public override object Clone()
        {
            // to make a clone of the primitive types fields with a new guid
            Station newStation = (Station)base.Clone();
            newStation.ParametersBasic = (Parameters)ParametersBasic.Clone();
            newStation.ParametersBasic._Date = ParametersBasic._Date; //not clonable
            //Link the new Parameters to the new Station
            newStation.ParametersBasic._Station = newStation;
            //useful only for the subclasses that dont override MeasuresTaken.get
            newStation._MeasureOfLevel = new List<M.MeasureOfLevel>(_MeasureOfLevel);
            return newStation;
        }

        internal void UpdateListMeasureLevel(Module stationLevelModule, Point point)
        //cr�ation d'une mesure vide avec les param�tres par d�faut.
        {
            M.MeasureOfLevel blankMeasureOfLevel = new M.MeasureOfLevel();
            I.LevelingStaff defaultStaff = stationLevelModule._defaultStaff;
            I.LevelingStaff secondaryStaff = stationLevelModule._secondaryStaff;
            InitBlankMeasure(blankMeasureOfLevel, point, defaultStaff, secondaryStaff);

            this._MeasureOfLevel.Add(blankMeasureOfLevel);
        }

        public static void InitBlankMeasure(MeasureOfLevel blankMeasureOfLevel, Point point, I.LevelingStaff defaultStaff, I.LevelingStaff secondaryStaff)
        {
            blankMeasureOfLevel._Date = DateTime.MinValue;
            blankMeasureOfLevel._Point = point.DeepCopy();
            if (point.LGCFixOption == ENUM.LgcPointOption.CALA)
                blankMeasureOfLevel._Point.LGCFixOption = ENUM.LgcPointOption.CALA;
            else
                blankMeasureOfLevel._Point.LGCFixOption = ENUM.LgcPointOption.VZ;
            blankMeasureOfLevel.SelectDefaultExtension();

            blankMeasureOfLevel._staff = PickTheRightStaff(defaultStaff, secondaryStaff, blankMeasureOfLevel);
        }

        public static I.LevelingStaff PickTheRightStaff(I.LevelingStaff defaultStaff, I.LevelingStaff secondaryStaff, MeasureOfLevel m)
        {
            //If only one staff is selected, retrun it
            if (defaultStaff == null)
                return secondaryStaff;
            else if (secondaryStaff == null)
                return defaultStaff;
            //If we have booth, it depends on the socketcode
            else if (Tsunami2.Preferences.Values.SecondaryStaffCode.Contains(m._Point.SocketCode.Id))
                return secondaryStaff;
            else
                return defaultStaff;
        }

        internal void Rename(LevelingDirectionStrategy levellingDirectionStrategy, string userStationName = "NotSet")
        //Renomme le nom de la station en fonction de la strat�gie aller-retour et du premier point mesur�
        {
            string commonPart = this._Name.Substring(0, 22) + levellingDirectionStrategy._name + "_";
            ///Change the name with the correct levelling direction
            this._Name = commonPart + this.specialPartOfStationName;
            if (this._MeasureOfLevel.Count > 0 && this.nameSetByUser == false)
            {
                this._Name = commonPart + this._MeasureOfLevel[0]._PointName;
                this._Parameters._StationName = this._Name;
                specialPartOfStationName = this._MeasureOfLevel[0]._PointName;
            }
            if (userStationName != "NotSet")
            {
                this._Name = commonPart + userStationName;
                this._Parameters._StationName = this._Name;
                this.nameSetByUser = true;
                this.specialPartOfStationName = userStationName;
            }
        }
        internal void CleanAllMeasure()
        //Nettoye toutes les lectures de la station level
        {
            foreach (M.MeasureOfLevel measLevel in this._MeasureOfLevel)
            {
                measLevel.CommentFromTsunami = "";
                measLevel.CommentFromUser = "";
                measLevel._Date = DateTime.MinValue;
                measLevel._Distance = na;
                measLevel._Compass = na;
                measLevel._EcartAller = na;
                measLevel._EcartRetour = na;
                measLevel._ResidualAller = na;
                measLevel._ResidualRetour = na;
                measLevel._Hmes = na;
                measLevel._RawLevelReading = na;
                measLevel._Residual = na;
                measLevel._TheoReading = na;
                if (measLevel._Status is M.States.Bad) { measLevel.CommentFromTsunami = R.StringLevel_BadMeas; }
            }
            this._ObsoleteMeasureOfLevel.Clear();
        }
        internal void CleanAllCalaCalculation()
        //Nettoye toutes les r�sultats de calcul LGC de la station level
        {
            foreach (M.MeasureOfLevel measLevel in this._MeasureOfLevel)
            {
                //measLevel._Comment = "";
                // measLevel._Date = System.DateTime.Now;
                measLevel._Hmes = na;
                measLevel._Residual = na;
                measLevel._TheoReading = na;
            }
            this._Parameters._EmqCala = na;
            this._Parameters._HStation = na;
        }
        /// <summary>
        /// copie dans les measureOfLevel de la station les mesures existantes dans les savedMeas
        /// </summary>
        /// <param name="stationLevel"></param>
        /// <param name="savedMeas"></param>
        internal void CopyMeasAlreadyDone(List<M.MeasureOfLevel> savedMeas, List<M.MeasureOfLevel> savedObsoleteMeas)
        {
            List<M.MeasureOfLevel> copyOfMeasOfLevel = new List<M.MeasureOfLevel>();
            foreach (M.MeasureOfLevel meas in this._MeasureOfLevel)
            {
                int index = savedMeas.FindIndex(x => x._PointName == meas._PointName);
                if (index != -1)
                {
                    //permet de garder la m�me mire que l'aller si on n'a pas encore fait de mesures retour
                    if (savedMeas[index]._RawLevelReading != na)
                    {
                        copyOfMeasOfLevel.Add(savedMeas[index].DeepCopy());
                    }
                    else
                    {
                        copyOfMeasOfLevel.Add(meas.DeepCopy());
                    }
                }
                else
                {
                    copyOfMeasOfLevel.Add(meas.DeepCopy());
                }
            }
            this._MeasureOfLevel = copyOfMeasOfLevel;
            List<M.MeasureOfLevel> copyObsoleteMeasOfLevel = new List<M.MeasureOfLevel>();
            foreach (M.MeasureOfLevel meas in this._ObsoleteMeasureOfLevel)
            {
                int index = savedObsoleteMeas.FindIndex(x => x._PointName == meas._PointName && x._Date == meas._Date);
                if (index != -1)
                {
                    copyObsoleteMeasOfLevel.Add(savedObsoleteMeas[index].DeepCopy());
                }
                else
                {
                    copyObsoleteMeasOfLevel.Add(meas.DeepCopy());
                }
            }
            this._ObsoleteMeasureOfLevel = copyObsoleteMeasOfLevel;
        }
        /// <summary>
        /// Trie les mesures dans l'ordre des Dcum
        /// </summary>
        internal void SortMeasures()
        {
            this._MeasureOfLevel.Sort();
        }
        /// <summary>
        /// Trie les mesures dans l'ordre inverse des Dcum
        /// </summary>
        internal void ReverseMeasures()
        {
            this._MeasureOfLevel.Sort();
            this._MeasureOfLevel.Reverse();
        }
        /// <summary>
        /// Trie les mesures du SPS en mettant le sextant 6 avant le sextant 1  
        /// </summary>
        internal void SortMeasForSPSOrigin()
        {
            List<M.MeasureOfLevel> sextant6 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("6"));
            sextant6.Sort();
            List<M.MeasureOfLevel> sextant1 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("1"));
            sextant1.Sort();
            this._MeasureOfLevel.Clear();
            this._MeasureOfLevel.AddRange(sextant6);
            this._MeasureOfLevel.AddRange(sextant1);
        }
        /// <summary>
        /// Trie les mesures du PS ring dans l'ordre correct pour la partie proche de l'origine  
        /// </summary>
        internal void SortMeasForPROrigin()
        {
            List<M.MeasureOfLevel> part8 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("8"));
            List<M.MeasureOfLevel> part9 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("9"));
            part9.Sort();
            List<M.MeasureOfLevel> part00 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("00"));
            part00.Sort();
            List<M.MeasureOfLevel> part0 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("01")
            || x._Point._Numero.StartsWith("02")
            || x._Point._Numero.StartsWith("03")
            || x._Point._Numero.StartsWith("04")
            || x._Point._Numero.StartsWith("05")
            || x._Point._Numero.StartsWith("06")
            || x._Point._Numero.StartsWith("07")
            || x._Point._Numero.StartsWith("08")
            || x._Point._Numero.StartsWith("09"));
            part0.Sort();
            List<M.MeasureOfLevel> part1 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("1"));
            part1.Sort();
            List<M.MeasureOfLevel> mergeList = new List<M.MeasureOfLevel>();
            mergeList.AddRange(part8);
            mergeList.AddRange(part9);
            mergeList.AddRange(part00);
            mergeList.AddRange(part0);
            mergeList.AddRange(part1);
            foreach (M.MeasureOfLevel item in mergeList)
            {
                this._MeasureOfLevel.Remove(item);
            }
            this._MeasureOfLevel.AddRange(mergeList);
        }
        /// <summary>
        /// Trie les mesures du Booster ring dans l'ordre correct pour la partie proche de l'origine  
        /// </summary>
        internal void SortMeasForBROrigin()
        {
            List<M.MeasureOfLevel> part1 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("1")
            && !x._Point._Numero.Contains("16")
            && !x._Point._Numero.Contains("15")
            && !x._Point._Numero.Contains("14")
            && !x._Point._Numero.Contains("13")
            && !x._Point._Numero.Contains("12")
            && !x._Point._Numero.Contains("11")
            && !x._Point._Numero.Contains("10"));
            part1.Sort();
            List<M.MeasureOfLevel> part2 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("2"));
            part2.Sort();
            List<M.MeasureOfLevel> part3 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("3"));
            part3.Sort();
            List<M.MeasureOfLevel> part4 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("4"));
            part4.Sort();
            List<M.MeasureOfLevel> part5 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("5"));
            part5.Sort();
            List<M.MeasureOfLevel> part6 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("6"));
            part6.Sort();
            List<M.MeasureOfLevel> part7 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("7"));
            part7.Sort();
            List<M.MeasureOfLevel> part8 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("8"));
            part8.Sort();
            List<M.MeasureOfLevel> part9 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("9"));
            part9.Sort();
            List<M.MeasureOfLevel> part10 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("10"));
            part10.Sort();
            List<M.MeasureOfLevel> part11 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("11"));
            part11.Sort();
            List<M.MeasureOfLevel> part12 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("12"));
            part12.Sort();
            List<M.MeasureOfLevel> part13 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("13"));
            part13.Sort();
            List<M.MeasureOfLevel> part14 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("14"));
            part14.Sort();
            List<M.MeasureOfLevel> part15 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("15"));
            part15.Sort();
            List<M.MeasureOfLevel> part16 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("16"));
            part16.Sort();
            List<M.MeasureOfLevel> mergeList = new List<M.MeasureOfLevel>();
            mergeList.AddRange(part8);
            mergeList.AddRange(part9);
            mergeList.AddRange(part10);
            mergeList.AddRange(part11);
            mergeList.AddRange(part12);
            mergeList.AddRange(part13);
            mergeList.AddRange(part14);
            mergeList.AddRange(part15);
            mergeList.AddRange(part16);
            mergeList.AddRange(part1);
            mergeList.AddRange(part2);
            mergeList.AddRange(part3);
            mergeList.AddRange(part4);
            mergeList.AddRange(part5);
            mergeList.AddRange(part6);
            mergeList.AddRange(part7);
            foreach (M.MeasureOfLevel item in mergeList)
            {
                this._MeasureOfLevel.Remove(item);
            }
            this._MeasureOfLevel.AddRange(mergeList);
        }
        /// <summary>
        /// Met les points de AD ring dans l'ordre
        /// </summary>
        /// <param name="theoPoint"></param>
        internal void SortMeasForDROrigin()
        {
            List<M.MeasureOfLevel> part0 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("0"));
            part0.Sort();
            List<M.MeasureOfLevel> part1 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("1"));
            part1.Sort();
            List<M.MeasureOfLevel> part2 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("2"));
            part2.Sort();
            List<M.MeasureOfLevel> part3 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("3"));
            part3.Sort();
            List<M.MeasureOfLevel> part4 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("4"));
            part4.Sort();
            List<M.MeasureOfLevel> part5 = this._MeasureOfLevel.FindAll(x => x._Point._Numero.StartsWith("5"));
            part5.Sort();
            List<M.MeasureOfLevel> mergeList = new List<M.MeasureOfLevel>();
            mergeList.AddRange(part3);
            mergeList.AddRange(part4);
            mergeList.AddRange(part5);
            mergeList.AddRange(part0);
            mergeList.AddRange(part1);
            mergeList.AddRange(part2);
            foreach (M.MeasureOfLevel item in mergeList)
            {
                this._MeasureOfLevel.Remove(item);
            }
            this._MeasureOfLevel.AddRange(mergeList);
        }
        /// <summary>
        /// Met � jour toutes les mires en fonction de la liste. Changement z�ro mire
        /// </summary>
        internal void UpdateAllStaff(List<I.LevelingStaff> listStaff)
        {
            foreach (M.MeasureOfLevel mes in this._MeasureOfLevel)
            {
                I.LevelingStaff staff = listStaff.Find(x => x._Name == mes._staff._Name);
                if (staff != null)
                {
                    mes._staff = staff.DeepCopy();
                }
            }
            this.ParametersBasic.LastChanged = DateTime.Now;
            foreach (M.MeasureOfLevel mes in this._ObsoleteMeasureOfLevel)
            {
                I.LevelingStaff staff = listStaff.Find(x => x._Name == mes._staff._Name);
                if (staff != null)
                {
                    mes._staff = staff.DeepCopy();
                }

            }
        }
        /// <summary>
        /// Corrige toutes les mesures de la temp�rature, zero mire et param�tre �talonnage
        /// </summary>
        internal void CorrectAllMeas()
        {
            I.Level level = (I.Level)this.ParametersBasic._Instrument;

            foreach (M.MeasureOfLevel mes in this._MeasureOfLevel)
            {
                Survey.CorrectLevelReadingForEverything(mes, level.EtalonnageParameter, this._Parameters);
            }
        }

        internal void CopyAsObsolete(MeasureOfLevel m)        
        {
            //Don't save na measures
            if (m._RawLevelReading == na) return;

            MeasureOfLevel badCopy = m.DeepCopy();
            badCopy._Status = new M.States.Bad();
            _ObsoleteMeasureOfLevel.Add(badCopy);
        }
    }
}
