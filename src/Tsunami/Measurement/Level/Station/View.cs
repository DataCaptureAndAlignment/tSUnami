﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using TSU.Common.Instruments;
using TSU.Common.Measures;
using TSU.ENUM;
using TSU.Views;
using TSU.Views.Message;
using D = TSU.Common.Instruments.Device;
using E = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;
using T = TSU.Tools;
using TSU.Common.Compute.CsvImporter;
using TSU.Common.Compute.Rabot;
using System.Linq;
using TSU.Common.Guided;

namespace TSU.Level
{
    public partial class Station
    {
        public partial class View : Common.Station.View
        {
            #region Fields & props

            public new Module Module;

            private double na = Tsunami2.Preferences.Values.na;

            //Ligne et colonne active dans le datagridview
            internal int rowActiveCell = 0;
            internal int columnActiveCell = 5;
            internal int nombreReprise = 0;
            internal int _numeroReprise = 0;

            internal LevelingDirectionViewStrategy _AllerRetourRepriseViewStrategy;

            //Main Button
            internal BigButton bigbuttonTop;

            // boutons du menu contexte
            internal BigButton bigButton_Elements;
            internal BigButton bigButton_selectSequenceNiv;
            internal BigButton bigButton_ImportCSV;
            internal BigButton bigButton_save;
            internal BigButton bigButton_Aller;
            internal BigButton bigButton_Retour;
            internal BigButton bigButton_AddReprise;
            internal BigButton bigButton_Reprise0;
            internal BigButton bigButton_Reprise1;
            internal BigButton bigButton_Reprise2;
            internal BigButton bigButton_Reprise3;
            internal BigButton bigButton_Reprise4;
            internal BigButton bigButton_Reprise5;
            internal BigButton bigButton_Reprise6;
            internal BigButton bigButton_Reprise7;
            internal BigButton bigButton_Reprise8;
            internal BigButton bigButton_Reprise9;
            internal BigButton bigButton_Cancel;
            internal BigButton bigButton_Sort;
            internal BigButton bigButton_CreateNewPoint;
            internal BigButton bigButton_CheckBlueElevation;
            internal BigButton bigButton_ShowDatFile;
            internal BigButton bigButton_SecondaryStaff;
            internal List<BigButton> allRepriseButtons;
            internal TsuNode node_Parameters;
            internal TsuNode node_Admin;
            internal TsuNode node_Computation;
            internal new ContextMenuStrip contextMenuStrip;

            /// <summary>
            /// Affiche ou cache le noeud admin dans le treeview
            /// </summary>
            internal bool showAdmin = true;

            /// <summary>
            /// Propriété qui défini si l'on peut voir le noeud du choix de la méthode de calcul
            /// </summary>
            internal bool showCalculationOption = true;

            /// <summary>
            /// Show the node coord type in the Admin Node
            /// </summary>
            internal bool showCoordType = true;

            /// <summary>
            /// Show the node computation result
            /// </summary>
            internal bool showComputation = true;

            /// <summary>
            /// Propriété qui défini si c'est un module guidé ou avancé
            /// </summary>
            /// 
            internal bool showNodeObsoleteMeas = true;

            internal ModuleType moduleType = ModuleType.Advanced;
            internal string nodeNameSelected = "";
            private string zone = "";
            private string classe = "CP";

            private string number = string.Format("{0}{1}{2}{3}", (DateTime.Now.Year - 2000),
                DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00"), "01");

            private string pointNewPoint = "";

            private bool stopCheckingContextMenuOrder = false;
            //internal System.Windows.Forms.Timer Timer_Keep_Cell_Focus;

            #endregion

            #region Construction & init

            public View(Module parentModule)
                : base(parentModule, Orientation.Vertical)
            //affichage du form lors de son lancement
            {
                InitializeComponent();
                ApplyThemeColors();
                Module = parentModule;
                _AllerRetourRepriseViewStrategy = new AllerViewStrategy();
                InitializeMenu();
                AddWaitingView();
                ShowDockedFill();
                //this.Timer_Keep_Cell_Focus = new System.Windows.Forms.Timer(this.components);
                //this.Timer_Keep_Cell_Focus.Interval = 100;
                //this.Timer_Keep_Cell_Focus.Enabled = false;
                //this.Timer_Keep_Cell_Focus.Tick += new System.EventHandler(this.Timer_Keep_Cell_Focus_Tick);
            }

            private void ApplyThemeColors()
            {
                _PanelBottom.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                _PanelTop.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                splitContainer1.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                splitContainer1.Panel1.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                splitContainer1.Panel2.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                splitContainerNivel.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                splitContainerNivel.Panel1.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                splitContainerNivel.Panel2.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                treeView_Parameters.BackColor = Tsunami2.Preferences.Theme.Colors.LightBackground;
                treeView_Parameters.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
                panel_AlignWindow.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                splitContainer_AlignWindow.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                splitContainer_AlignWindow.Panel1.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                splitContainer_AlignWindow.Panel2.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                splitContainer_AlignText.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                splitContainer_AlignText.Panel1.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                splitContainer_AlignText.Panel2.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                textBox_PointAlign.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                textBox_PointAlign.ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground;
                textBox_PointMovement.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                textBox_PointMovement.ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground;
                splitContainer_ProgressBar.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                splitContainer_ProgressBar.Panel1.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                splitContainer_ProgressBar.Panel2.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                progressBar_TooHigh.ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground;
                progressBar_TooHigh.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                progressBar_TooLow.ForeColor = Tsunami2.Preferences.Theme.Colors.LightForeground;
                progressBar_TooLow.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
                dataGridViewLevel.ColumnHeadersDefaultCellStyle.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                dataGridViewLevel.ColumnHeadersDefaultCellStyle.ForeColor =
                    Tsunami2.Preferences.Theme.Colors.NormalFore;
                dataGridViewLevel.ColumnHeadersDefaultCellStyle.SelectionBackColor =
                    Tsunami2.Preferences.Theme.Colors.TextHighLightBack;
                dataGridViewLevel.ColumnHeadersDefaultCellStyle.SelectionForeColor =
                    Tsunami2.Preferences.Theme.Colors.TextHighLightFore;
                dataGridViewLevel.DefaultCellStyle.BackColor = Tsunami2.Preferences.Theme.Colors.LightBackground;
                dataGridViewLevel.DefaultCellStyle.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
                dataGridViewLevel.DefaultCellStyle.SelectionBackColor =
                    Tsunami2.Preferences.Theme.Colors.TextHighLightBack;
                dataGridViewLevel.DefaultCellStyle.SelectionForeColor =
                    Tsunami2.Preferences.Theme.Colors.TextHighLightFore;
                dataGridViewLevel.GridColor = Tsunami2.Preferences.Theme.Colors.Background;
                LeveldataGridViewName.HeaderText = R.StringDataGridLevel_Name;
                LeveldataGridViewUse.HeaderText = R.StringDataGridLevel_Use;
                LeveldataGridViewCala.HeaderText = "A";
                LevelDataGridViewDcum.HeaderText = R.StringDataGridLevel_Dcum;
                LeveldataGridViewH.HeaderText = R.StringDataGridLevel_Htheo;
                LeveldataGridViewHmes.HeaderText = R.StringDataGridLevel_Hmes;
                LeveldataGridViewDist.HeaderText = R.StringDataGridLevel_Dist;
                LeveldataGridViewRall.HeaderText = R.StringDataGridLevel_Extension;
                LeveldataGridViewTheo.HeaderText = R.StringDataGridLevel_Theo;
                LeveldataGridViewDepla.HeaderText = R.StringDataGridLevel_Offset;
                LeveldataGridViewObj.HeaderText = R.StringDataGridLevel_Obj;
                LeveldataGridViewMove.HeaderText = R.StringDataGridLevel_Move;
                LeveldataGridViewRawMeas.HeaderText = R.StringDataGridLevel_RawMeas;
                LeveldataGridViewCorrMeas.HeaderText = R.StringDataGridLevel_CorrMeas;
                LeveldataGridViewResidusAller.HeaderText = R.StringDataGridLevel_ResidusAller;
                LeveldataGridViewResidusRetour.HeaderText = R.StringDataGridLevel_ResidusRetour;
                LeveldataGridViewCode.HeaderText = R.StringDataGridLevel_CodeAlesage;
                LeveldataGridViewStaff.HeaderText = R.StringDataGridLevel_Staff;
                LeveldataGridViewInterfaces.HeaderText = "Interfaces";
                LeveldataGridViewComment.HeaderText = R.StringDataGridLevel_Comment;
                LeveldataGridViewTime.HeaderText = R.StringDataGridLevel_Time;
                BackColor = Tsunami2.Preferences.Theme.Colors.Background;
            }

            //Methods
            //Initialization du treeView parametre fil
            private void InitializeMenu()
            {
                //Main Button
                bigbuttonTop = new BigButton(R.StringLevel_BigButtonTop, R.Level, ShowContextMenuGlobal);
                _PanelTop.Controls.Add(bigbuttonTop);
                CreateContextMenuButtons();
                Module._WorkingStationLevel._Parameters._State = new State.InstrumentSelection();
                //RefreshGlobalContextMenuItemCollection();
                InitializeTreeviewParameters();
                InitializedataGridViewLevel();
            }

            private void CreateContextMenuButtons()
            //creation du menu de boutons
            {
                allRepriseButtons = new List<BigButton>();
                bigButton_Elements = new BigButton(R.StringLevel_SelectElement, R.Open, buttonSelectPoint_Click);
                //bouton select sequence
                bigButton_selectSequenceNiv =
                    new BigButton(R.StringLevel_SelectSequence, R.Open, button_SelectSequence_Click);
                string openFilesDescription = "";
                if (this.Module.FinalModule.RabotFiles.Count > 0) openFilesDescription += string.Join("\n", this.Module.FinalModule.RabotFiles);
                string importCSVDescription = openFilesDescription == "" ? R.T_WITH_EXPECTED_OFFSETS_OR_RELATIVE_DISPLACEMENT : openFilesDescription;
                bigButton_ImportCSV = new BigButton($"{R.T_IMPORT_A_CSV_FILE};{importCSVDescription}", R.Open, button_ImportCSV_Click);
                bigButton_save = new BigButton(R.StringLevel_Save, R.Save, buttonSave_Click);
                bigButton_ShowDatFile = new BigButton(R.StringLevel_OpenDatFile, R.Export, bigButton_ShowDatFile_Click);
                bigButton_Aller = new BigButton(R.StringLevel_Aller, R.Level_Aller, buttonAller_Click);
                bigButton_Retour = new BigButton(R.StringLevel_Retour, R.Level_Retour, buttonRetour_Click);
                bigButton_Reprise1 = new BigButton(R.StringLevel_Reprise1, R.Level_Reprise_1, buttonReprise1_Click);
                bigButton_Reprise2 = new BigButton(R.StringLevel_Reprise2, R.Level_Reprise_2, buttonReprise2_Click);
                bigButton_Reprise3 = new BigButton(R.StringLevel_Reprise3, R.Level_Reprise_3, buttonReprise3_Click);
                bigButton_Reprise4 = new BigButton(R.StringLevel_Reprise4, R.Level_Reprise_4, buttonReprise4_Click);
                bigButton_Reprise5 = new BigButton(R.StringLevel_Reprise5, R.Level_Reprise_5, buttonReprise5_Click);
                bigButton_Reprise6 = new BigButton(R.StringLevel_Reprise6, R.Level_Reprise_6, buttonReprise6_Click);
                bigButton_Reprise7 = new BigButton(R.String_Level_Reprise8, R.Level_Reprise_7, buttonReprise7_Click);
                bigButton_Reprise8 = new BigButton(R.StringLevel_Reprise9, R.Level_Reprise_8, buttonReprise8_Click);
                bigButton_Reprise9 = new BigButton(R.StringLevel_Reprise10, R.Level_Reprise_9, buttonReprise9_Click);
                bigButton_Reprise0 = new BigButton(R.StringLevel_Reprise10, R.Level_Reprise_10, buttonReprise10_Click);
                bigButton_AddReprise =
                    new BigButton(R.StringLevel_AjoutReprise, R.Level_Reprise_Add, buttonAddReprise_Click);
                bigButton_Cancel = new BigButton(R.StringLevel_CancelMenu, R.Cancel, buttonCancel_Click);
                bigButton_Sort = new BigButton(R.StringLevel_Sort, R.Vertical, buttonSort_Click);
                bigButton_CreateNewPoint = new BigButton(R.StringLevel_AddNewPoint, R.Add, buttonCreateNewPoint_Click);
                bigButton_CheckBlueElevation = new BigButton(R.StringLevel_CheckBlueElevation,
                    R.Level_blueElevationCheck, CheckBlueElevation_Click);
                bigButton_SecondaryStaff =
                    new BigButton(string.Format(R.StringLevel_SecondStaffChooseButton, R.StringLevel_UnknownStaff),
                        R.Second_staff, bigButton_SecondaryStaff_Click);
                allRepriseButtons.Add(bigButton_Reprise1);
                allRepriseButtons.Add(bigButton_Reprise2);
                allRepriseButtons.Add(bigButton_Reprise3);
                allRepriseButtons.Add(bigButton_Reprise4);
                allRepriseButtons.Add(bigButton_Reprise5);
                allRepriseButtons.Add(bigButton_Reprise6);
                allRepriseButtons.Add(bigButton_Reprise7);
                allRepriseButtons.Add(bigButton_Reprise8);
                allRepriseButtons.Add(bigButton_Reprise9);
                allRepriseButtons.Add(bigButton_Reprise0);
            }

            private void ShowContextMenuGlobal()
            {
                CheckDatagridIsInEditMode();
                switch (moduleType)
                {
                    case ModuleType.Guided:
                        break;
                    case ModuleType.Advanced:
                        // creation de la liste des boutons de menu pour le module avancé
                        contextButtons = new List<Control>();
                        if (Module._WorkingStationLevel._Parameters._Instrument._Model != null
                            && _AllerRetourRepriseViewStrategy is AllerViewStrategy)
                        {
                            contextButtons.Add(bigButton_Elements);
                            contextButtons.Add(bigButton_ImportCSV);
                            contextButtons.Add(bigButton_selectSequenceNiv);
                            contextButtons.Add(bigButton_CreateNewPoint);
                        }
                        else
                        {
                            if (_AllerRetourRepriseViewStrategy is RetourViewStrategy
                                && Module._AllerStationLevel._MeasureOfLevel.FindIndex(x => x._RawLevelReading != na) ==
                                -1)
                            {
                                contextButtons.Add(bigButton_Elements);
                                contextButtons.Add(bigButton_ImportCSV);
                                contextButtons.Add(bigButton_selectSequenceNiv);
                                contextButtons.Add(bigButton_CreateNewPoint);
                            }
                        }

                        if (Module._defaultStaff != null)
                        {
                            if (Module._secondaryStaff != null)
                            {
                                bigButton_SecondaryStaff.ChangeNameAndDescription(
                                    string.Format(R.StringLevel_SecondStaffChooseButton, Module._secondaryStaff._Name));
                            }
                            else
                            {
                                bigButton_SecondaryStaff.ChangeNameAndDescription(
                                    string.Format(R.StringLevel_SecondStaffChooseButton, R.StringLevel_UnknownStaff));
                            }

                            contextButtons.Add(bigButton_SecondaryStaff);
                        }

                        if (Module._WorkingStationLevel._Parameters._State is State.StationLevelReadyToBeSaved)
                        {
                            contextButtons.Add(bigButton_save);
                        }

                        string filepath = Module._WorkingStationLevel.ParametersBasic._GeodeDatFilePath;
                        if (filepath == "") filepath = Module._AllerStationLevel.ParametersBasic._GeodeDatFilePath;
                        if (filepath == "") filepath = Module._RetourStationLevel.ParametersBasic._GeodeDatFilePath;
                        if (filepath == "")
                        {
                            int index = Module._RepriseStationLevel.FindIndex(x =>
                                x.ParametersBasic._GeodeDatFilePath != "");
                            if (index != -1)
                                filepath = Module._RepriseStationLevel[index].ParametersBasic._GeodeDatFilePath;
                        }

                        if (filepath != "") contextButtons.Add(bigButton_ShowDatFile);
                        if (dataGridViewLevel.RowCount != 0)
                        {
                            contextButtons.Add(bigButton_Sort);
                            contextButtons.Add(bigButton_CheckBlueElevation);
                        }

                        _AllerRetourRepriseViewStrategy.RefreshContextMenu(this);
                        contextButtons.Add(bigButton_Cancel);
                        ShowPopUpMenu(contextButtons);
                        // il peut arriver lors de l'ajout d'un nouveau bouton que l'affichage des contrôles dans le popup menu ne soit pas dans l'ordre. Le réaffichage du pop up semble résoudre le problème.
                        // Ajout 24-03-20 pour bouton default staff et secondary staff, ne le fait plus qu'une seule fois pour éviter une boucle infinie
                        //if (!ok && !stopCheckingContextMenuOrder)
                        //{
                        //    stopCheckingContextMenuOrder = true;
                        //    this.ShowContextMenuGlobal();
                        //    stopCheckingContextMenuOrder = false;
                        //}
                        break;
                    default:
                        break;
                }
            }

            private void AddWaitingView()
            // Add a waiting instrument module
            {
                TsuView v = new TsuView();
                v.BackgroundImage = R.Instrument;
                v.BackgroundImageLayout = ImageLayout.Zoom;
                v.ShowDockedFill();
                splitContainer1.Panel2.Controls.Add(v);
            }

            #endregion

            #region Updates

            public override void UpdateView()
            {
                base.UpdateView();
                if (Module.FinalModule.ParentModule is Common.Guided.Group.Module guidedGroupModule 
                    && guidedGroupModule.ActiveSubGuidedModule?.CurrentStep != null)
                {
                    guidedGroupModule.ActiveSubGuidedModule.CurrentStep.Update();
                    guidedGroupModule.UpdateBigButtonTop();
                }

                UpdateListMeasure();
            }

            internal void UpdateListMeasure(bool cellFocus = true)
            //met à jour le datagrid en fonction de la working station level
            {
                //RefreshGlobalContextMenuItemCollection();
                Module.CheckIfReadyToBeSaved();
                _AllerRetourRepriseViewStrategy.UpdateBigButtonImage(this);
                UpdateDatagridViewLevel();
                RedrawTreeviewParameters();
                if (cellFocus)
                {
                    CellFocusDataGridView();
                }
            }

            #endregion

            #region DatagridView

            public void InitializedataGridViewLevel()
            //Initialization du dataGridView
            {
                dataGridViewLevel.ColumnHeadersHeight = 400;
                dataGridViewLevel.DefaultCellStyle.Font = Tsunami2.Preferences.Theme.Fonts.NormalItalic;
                dataGridViewLevel.RowHeadersDefaultCellStyle.Font = Tsunami2.Preferences.Theme.Fonts.NormalItalic;
                dataGridViewLevel.ColumnHeadersDefaultCellStyle.Font = Tsunami2.Preferences.Theme.Fonts.NormalItalic;
                dataGridViewLevel.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }

            internal void UpdateDatagridViewLevel()
            //met à jour le datagridview
            {
                dataGridViewLevel.CurrentCellChanged -= dataGridViewLevel_CurrentCellChanged;
                dataGridViewLevel.Rows.Clear();
                //this.MaximizeDataGridView();
                DataGridUtility.SaveSorting(dataGridViewLevel);
                List<List<object>> listRow = new List<List<object>>();
                double biggestAllerResidualValue = 0;
                List<string> biggestAllerResidualpoint = new List<string>();
                double biggestRetourResidualValue = 0;
                List<string> biggestRetourResidualpoint = new List<string>();
                bool hideMesCorr = true;
                //ajoute toutes les mesures dans le tableau ligne par ligne
                foreach (MeasureOfLevel measureOfLevel in Module._WorkingStationLevel._MeasureOfLevel)
                {
                    List<object> row = new List<object>();
                    //colonne suppression mesure 0
                    row.Add(R.Level_Delete);
                    //colonne nom 1
                    row.Add("");
                    //colonne cala 2
                    row.Add(R.Level_Case_Vide);
                    //colonne checkbox use meas 3
                    row.Add(R.CheckedBoxDatagridView);
                    //Dcum 4
                    row.Add("");
                    //Htheo 5
                    row.Add(0);
                    //Hmes 6
                    row.Add(0);
                    //colonne Dist 7
                    row.Add(0);
                    //Rallonge 8
                    row.Add(0);
                    //colonne Mes Brute 9
                    row.Add(0);
                    //colonne Mes Corr 10
                    row.Add(0);
                    //colonne Theo 11
                    row.Add(0);
                    //colonne Depla 12
                    row.Add(0);
                    //colonne Obj 13
                    row.Add(0);
                    //colonne Move 14
                    row.Add(0);
                    //colonne Residus / Aller 14
                    row.Add(0);
                    //colonne Residus / Retour 15
                    row.Add(0);
                    //Code Alesage 16
                    row.Add("");
                    //Mire 17
                    row.Add("");
                    //assemblage Interfaces 18
                    row.Add("");
                    //Commentaire 19
                    row.Add("");
                    //Time 20
                    row.Add("");
                    row[1] = measureOfLevel._PointName;
                    bool notShowTheoReadingAndDepla = true;
                    int indexMeas = Module._MeasPoint.FindIndex(x => x._Name == measureOfLevel._Point._Name);
                    //Ajoute la Dcum
                    row[4] = (measureOfLevel._Point._Parameters.Cumul == Tsunami2.Preferences.Values.na)
                        ? ""
                        : measureOfLevel._Point._Parameters.Cumul.ToString("F3",
                            CultureInfo.CreateSpecificCulture("en-US"));
                    switch (Module._WorkingStationLevel._Parameters._ZType)
                    {
                        case CoordinatesType.CCS:
                            row[6] = (indexMeas != -1)
                                ? Math.Round(Module._MeasPoint[indexMeas]._Coordinates.Ccs.Z.Value, 5)
                                    .ToString("F5", CultureInfo.CreateSpecificCulture("en-US"))
                                : "";
                            if (measureOfLevel._Point._Coordinates.Ccs.Z != null
                                && measureOfLevel._Point._Coordinates.Ccs.Z.Value != Tsunami2.Preferences.Values.na)
                            {
                                row[5] = Math.Round(measureOfLevel._Point._Coordinates.Ccs.Z.Value, 5)
                                    .ToString("F5", CultureInfo.CreateSpecificCulture("en-US"));
                                notShowTheoReadingAndDepla = false;
                            }
                            else
                            {
                                row[5] = "";
                            }

                            break;
                        case CoordinatesType.SU:
                            row[6] = (indexMeas != -1)
                                ? Math.Round(Module._MeasPoint[indexMeas]._Coordinates.Local.Z.Value, 5)
                                    .ToString("F5", CultureInfo.CreateSpecificCulture("en-US"))
                                : "";
                            if (measureOfLevel._Point._Coordinates.Local.Z != null
                                && measureOfLevel._Point._Coordinates.Local.Z.Value != Tsunami2.Preferences.Values.na)
                            {
                                row[5] = Math.Round(measureOfLevel._Point._Coordinates.Local.Z.Value, 5)
                                    .ToString("F5", CultureInfo.CreateSpecificCulture("en-US"));
                                notShowTheoReadingAndDepla = false;
                            }
                            else
                            {
                                row[5] = "";
                            }

                            break;
                        //case CoordinatesType.UserDefined:
                        //    row[3] = Math.Round(measureOfLevel._Point._Coordinates.UserDefined.Z.Value, 5);
                        //    break;
                        default:
                            break;
                    }

                    row[7] = (measureOfLevel._Distance == na)
                        ? ""
                        : Math.Round(measureOfLevel._Distance, 3)
                            .ToString("F3", CultureInfo.CreateSpecificCulture("en-US"));
                    row[8] = Math.Round(measureOfLevel._Extension * 1000, 2)
                        .ToString("F2", CultureInfo.CreateSpecificCulture("en-US")); //Rallonge
                    row[9] = (measureOfLevel._RawLevelReading == na)
                        ? ""
                        : Math.Round(measureOfLevel._RawLevelReading * 100000, 0)
                            .ToString("F0", CultureInfo.CreateSpecificCulture("en-US"));
                    row[10] = (measureOfLevel._RawLevelReading == na)
                        ? ""
                        : Math.Round(measureOfLevel._CorrLevelReadingForEtalonnage * 100000, 0)
                            .ToString("F0", CultureInfo.CreateSpecificCulture("en-US"));
                    row[11] = (measureOfLevel._TheoReading == na || notShowTheoReadingAndDepla)
                        ? ""
                        : Math.Round(measureOfLevel._TheoReading * 100000, 0)
                            .ToString("F0", CultureInfo.CreateSpecificCulture("en-US"));
                    row[12] = (measureOfLevel._Hmes == na || notShowTheoReadingAndDepla)
                        ? ""
                        : Math.Round(-(measureOfLevel._RawLevelReading - measureOfLevel._TheoReading) * 100000, 0)
                            .ToString("F0", CultureInfo.CreateSpecificCulture("en-US"));
                    if ((measureOfLevel._Hmes == na || notShowTheoReadingAndDepla))
                    {
                        row[13] = "";
                        row[14] = "";
                    }
                    else if (this._Module.FinalModule.VerticalRabot.ContainsKey(measureOfLevel._PointName))
                    {
                        if (this._Module.FinalModule.RabotStrategy == Strategy.Expected_Offset)
                        {
                            row[13] = Math.Round(this._Module.FinalModule.VerticalRabot[measureOfLevel._PointName].Smooth.GetValueOrDefault() * 100, 0).ToString("F0", CultureInfo.CreateSpecificCulture("en-US"));
                            row[14] = Math.Round(this._Module.FinalModule.VerticalRabot[measureOfLevel._PointName].Smooth.GetValueOrDefault() * 100 + (measureOfLevel._RawLevelReading - measureOfLevel._TheoReading) * 100000, 0).ToString("F0", CultureInfo.CreateSpecificCulture("en-US"));
                        }
                        else
                        {
                            double expOHbv = Tsunami2.Preferences.Values.na;
                            double moveHbv = Tsunami2.Preferences.Values.na;
                            List<M.Measure> pointMeasureList = this._Module.FinalModule._ActiveStationModule._Station.MeasuresTaken
                                .GetList()
                                .Where(measure => measure._PointName == measureOfLevel._PointName && !(measure._Status.Type == M.States.Types.Bad && !measure.IsObsolete))
                                .ToList();

                            if (pointMeasureList.Count == 1)
                            {
                                moveHbv = -1 * this._Module.FinalModule.VerticalRabot[measureOfLevel._PointName].RoughMinusSmooth.GetValueOrDefault() * 100;
                                expOHbv = moveHbv + Math.Round(-(measureOfLevel._RawLevelReading - measureOfLevel._TheoReading) * 100000, 0);
                            }
                            else if (pointMeasureList.Count > 1)
                            {
                                MeasureOfLevel firstMeasure = pointMeasureList.FirstOrDefault() as MeasureOfLevel;
                                double firstMeasureOffset = -(firstMeasure._RawLevelReading - firstMeasure._TheoReading) * 100000;
                                expOHbv = firstMeasureOffset - this._Module.FinalModule.VerticalRabot[measureOfLevel._PointName].RoughMinusSmooth.GetValueOrDefault() * 100;
                                moveHbv = expOHbv - Math.Round(-(measureOfLevel._RawLevelReading - measureOfLevel._TheoReading) * 100000, 0);
                            }
                            row[13] = Math.Round(expOHbv, 0)
                                .ToString("F0", CultureInfo.CreateSpecificCulture("en-US"));
                            row[14] = Math.Round(moveHbv, 0)
                                .ToString("F0", CultureInfo.CreateSpecificCulture("en-US"));
                        }
                    }
                    //Affiche les résidus aller si mesure présente dans l'aller
                    if (measureOfLevel._ResidualAller == na)
                    {
                        row[15] = "";
                    }
                    else
                    {
                        row[15] = Math.Round(measureOfLevel._ResidualAller * 100000, 0)
                            .ToString("F0", CultureInfo.CreateSpecificCulture("en-US"));
                        if (Math.Abs(measureOfLevel._ResidualAller) >= biggestAllerResidualValue)
                        {
                            if (Math.Abs(measureOfLevel._ResidualAller) == biggestAllerResidualValue)
                            {
                                biggestAllerResidualpoint.Add(row[1].ToString());
                            }
                            else
                            {
                                biggestAllerResidualValue = Math.Abs(measureOfLevel._ResidualAller);
                                biggestAllerResidualpoint.Clear();
                                biggestAllerResidualpoint.Add(row[1].ToString());
                            }
                        }
                    }

                    //Affiche les résidus retour si mesure présente dans le retour
                    if (measureOfLevel._EcartRetour == na)
                    {
                        row[16] = "";
                    }
                    else
                    {
                        row[16] = Math.Round(measureOfLevel._ResidualRetour * 100000, 0)
                            .ToString("F0", CultureInfo.CreateSpecificCulture("en-US"));
                        if (Math.Abs(measureOfLevel._ResidualRetour) >= biggestRetourResidualValue)
                        {
                            if (Math.Abs(measureOfLevel._ResidualRetour) == biggestRetourResidualValue)
                            {
                                biggestRetourResidualpoint.Add(row[1].ToString());
                            }
                            else
                            {
                                biggestRetourResidualValue = Math.Abs(measureOfLevel._ResidualRetour);
                                biggestRetourResidualpoint.Clear();
                                biggestRetourResidualpoint.Add(row[1].ToString());
                            }

                        }
                    }

                    if (measureOfLevel._Point.SocketCode != null)
                        row[17] = measureOfLevel._Point.SocketCode.Id;
                    else
                        row[17] = "";
                    row[18] = measureOfLevel._staff?._Name;
                    row[19] = measureOfLevel.Interfaces.ToString();
                    row[20] = measureOfLevel.Comment;
                    if (measureOfLevel._Date != DateTime.MinValue)
                        row[21] = measureOfLevel._Date.ToString("T", CultureInfo.CreateSpecificCulture("fr-FR"));
                    //Indique dans le datagridview que c'est un point de cala
                    if (measureOfLevel._Point.LGCFixOption == LgcPointOption.CALA)
                    {
                        row[2] = R.Level_Lock;
                    }

                    if (measureOfLevel._Status is M.States.Bad)
                    {
                        row[3] = R.UncheckedBoxDatagridView;
                    }

                    //Cache la colonne mes corrigée si elle est la même que la mesure brute
                    if (measureOfLevel._RawLevelReading != measureOfLevel._CorrLevelReadingForEtalonnage)
                        hideMesCorr = false;
                    listRow.Add(row);
                }

                foreach (List<object> ligne in listRow)
                {
                    dataGridViewLevel.Rows.Add(ligne.ToArray());
                }

                for (int i = 0; i < dataGridViewLevel.RowCount; i++)
                {
                    //met en gris les lignes si c'est un point de calage
                    if ((dataGridViewLevel[2, i].Value as System.Drawing.Bitmap).Width == 128)
                    {
                        dataGridViewLevel[1, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[3, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[4, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[5, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[6, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[7, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[8, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[9, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[10, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[11, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[12, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[13, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[14, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[15, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[16, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[17, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[18, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[19, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[20, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[21, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
                    }

                    //cache la colonne mesure corrigée si toute les raw mesure sont égales à la mesure corrigée
                    dataGridViewLevel.Columns[10].Visible = !hideMesCorr;
                    //met en rouge le fond de la cellule déplacement si le déplacement moyen est > à la tolérance d'alignement ou le etxte en vert si deplacement < tolerance
                    double p = T.Conversions.Numbers.ToDouble(dataGridViewLevel[12, i].Value.ToString(), true, -9999);
                    ///Temporairement n'affiche plus les couleurs en module guidé tant que les tolérances ne sont pas décidées.
                    if (p != -9999 && moduleType == ModuleType.Advanced)
                    {
                        dataGridViewLevel[12, i].Style.BackColor =
                            (Math.Abs(p) / 100 > Module._WorkingStationLevel._Parameters._Tolerance * 1000)
                                ? Tsunami2.Preferences.Theme.Colors.Bad
                                : dataGridViewLevel[12, i].Style.BackColor;
                        dataGridViewLevel[12, i].Style.ForeColor =
                            (Math.Abs(p) / 100 > Module._WorkingStationLevel._Parameters._Tolerance * 1000)
                                ? dataGridViewLevel[12, i].Style.ForeColor
                                : Tsunami2.Preferences.Theme.Colors.Good;
                    }

                    //met en rouge le fond dans residus aller si le résidus aller est > à 0.10mm et si c'est le plus grand
                    ///Temporairement n'affiche plus les couleurs en module guidé tant que les tolérances ne sont pas décidées.
                    p = T.Conversions.Numbers.ToDouble(dataGridViewLevel[15, i].Value.ToString(), false, -9999);
                    if ((p != -9999) &&
                        (biggestAllerResidualpoint.Contains(dataGridViewLevel[1, i].Value.ToString())) &&
                        moduleType == ModuleType.Advanced)
                    {
                        dataGridViewLevel[15, i].Style.BackColor = (Math.Abs(p) > 10)
                            ? Tsunami2.Preferences.Theme.Colors.Bad
                            : dataGridViewLevel[15, i].Style.BackColor;
                        dataGridViewLevel[15, i].Style.ForeColor = (Math.Abs(p) > 10)
                            ? dataGridViewLevel[15, i].Style.ForeColor
                            : Tsunami2.Preferences.Theme.Colors.Good;
                    }
                    else
                    {
                        if (p != -9999 && moduleType == ModuleType.Advanced)
                        {
                            dataGridViewLevel[15, i].Style.ForeColor = (Math.Abs(p) > 10)
                                ? dataGridViewLevel[15, i].Style.ForeColor
                                : Tsunami2.Preferences.Theme.Colors.Good;
                        }
                    }

                    //met en rouge le fond dans ecart retour et residus retour est > à 0.10mm et si c'est le plus grand
                    p = T.Conversions.Numbers.ToDouble(dataGridViewLevel[16, i].Value.ToString(), false, -9999);
                    ///Temporairement n'affiche plus les couleurs en module guidé tant que les tolérances ne sont pas décidées.
                    if ((p != -9999) && biggestRetourResidualpoint.Contains(dataGridViewLevel[1, i].Value.ToString()) &&
                        moduleType == ModuleType.Advanced)
                    {
                        dataGridViewLevel[16, i].Style.BackColor = (Math.Abs(p) > 10)
                            ? Tsunami2.Preferences.Theme.Colors.Bad
                            : dataGridViewLevel[16, i].Style.BackColor;
                        dataGridViewLevel[16, i].Style.ForeColor = (Math.Abs(p) > 10)
                            ? dataGridViewLevel[16, i].Style.ForeColor
                            : Tsunami2.Preferences.Theme.Colors.Good;
                    }
                    else
                    {
                        if (p != -9999 && moduleType == ModuleType.Advanced)
                        {
                            dataGridViewLevel[16, i].Style.ForeColor = (Math.Abs(p) > 10)
                                ? dataGridViewLevel[16, i].Style.ForeColor
                                : Tsunami2.Preferences.Theme.Colors.Good;
                        }
                    }

                    //Met en jaune la colonne raw meas et rallonge
                    if (dataGridViewLevel[1, i].Value.ToString() != "")
                    {
                        dataGridViewLevel[7, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Action;
                        dataGridViewLevel[8, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Action;
                        dataGridViewLevel[9, i].Style.BackColor = Tsunami2.Preferences.Theme.Colors.Action;
                    }

                    //Affiche ou cache la colonne Ecart Aller
                    _AllerRetourRepriseViewStrategy.RefreshDatagridView(this);
                    //Affiche la colonne distance que lorsqu'on utilise un DNA
                    var type = Module._WorkingStationLevel._Parameters._Instrument._levelType;
                    if (type == LevelType.DNA03 || type == LevelType.LS15)
                    {
                        dataGridViewLevel.Columns[7].Visible = true;
                        dataGridViewLevel.Columns[7].ReadOnly = false;
                        dataGridViewLevel.Columns[7].ReadOnly = false;
                    }
                    else
                    {
                        dataGridViewLevel.Columns[7].Visible = false;
                        dataGridViewLevel.Columns[7].ReadOnly = false;
                        dataGridViewLevel.Columns[7].ReadOnly = false;
                    }
                }

                if (Module._WorkingStationLevel._Parameters.showObsoleteMeas &&
                    Module._WorkingStationLevel._ObsoleteMeasureOfLevel.Count > 0) AddObsoleteMeasInDatagridView();
                DataGridUtility.RestoreSorting(dataGridViewLevel);
                dataGridViewLevel.CurrentCellChanged += new EventHandler(dataGridViewLevel_CurrentCellChanged);
            }

            private void AddObsoleteMeasInDatagridView()
            {
                List<List<object>> listRow = new List<List<object>>();
                //ajoute toutes les mesures dans le tableau ligne par ligne
                foreach (MeasureOfLevel measureOfLevel in Module._WorkingStationLevel._ObsoleteMeasureOfLevel)
                {
                    List<object> row = new List<object>();
                    //colonne suppression mesure 0
                    row.Add(R.Level_Case_Vide);
                    //colonne nom 1
                    row.Add("");
                    //colonne cala 2
                    row.Add(R.Level_Case_Vide);
                    //colonne checkbox use meas 3
                    row.Add(R.UncheckedBoxDatagridView_Old_Meas);
                    //Dcum 4
                    row.Add("");
                    //Htheo 5
                    row.Add("");
                    //Hmes 6
                    row.Add("");
                    //colonne Dist 7
                    row.Add("");
                    //Rallonge 8
                    row.Add("");
                    //colonne Mes Brute 9
                    row.Add("");
                    //colonne Mes Corr 10
                    row.Add(0);
                    //colonne Theo 11
                    row.Add("");
                    //colonne Depla 12
                    row.Add("");
                    //colonne Obj 13
                    row.Add("");
                    //colonne Move 14
                    row.Add("");
                    //colonne Residus / Aller 15
                    row.Add("");
                    //colonne Residus / Retour 16
                    row.Add("");
                    //Code Alesage 17
                    row.Add("");
                    //Mire 18
                    row.Add("");
                    //Commentaire 19
                    row.Add("");
                    //Time 20
                    row.Add("");
                    row[1] = measureOfLevel._PointName;
                    //Ajoute la Dcum
                    row[4] = (measureOfLevel._Point._Parameters.Cumul == Tsunami2.Preferences.Values.na)
                        ? ""
                        : measureOfLevel._Point._Parameters.Cumul.ToString("F3",
                            CultureInfo.CreateSpecificCulture("en-US"));
                    row[7] = (measureOfLevel._Distance == na)
                        ? ""
                        : Math.Round(measureOfLevel._Distance, 3)
                            .ToString("F3", CultureInfo.CreateSpecificCulture("en-US"));
                    row[8] = Math.Round(measureOfLevel._Extension * 1000, 2)
                        .ToString("F2", CultureInfo.CreateSpecificCulture("en-US")); //Rallonge
                    row[9] = (measureOfLevel._RawLevelReading == na)
                        ? ""
                        : Math.Round(measureOfLevel._RawLevelReading * 100000, 0)
                            .ToString("F0", CultureInfo.CreateSpecificCulture("en-US"));
                    row[10] = (measureOfLevel._RawLevelReading == na)
                        ? ""
                        : Math.Round(measureOfLevel._CorrLevelReadingForEtalonnage * 100000, 0)
                            .ToString("F0", CultureInfo.CreateSpecificCulture("en-US"));
                    row[17] = measureOfLevel._Point.SocketCode.ToString();
                    row[18] = measureOfLevel._staff._Name;
                    row[19] = R.StringDataGridLevel_ObsoleteMeas;
                    if (measureOfLevel._Date != DateTime.MinValue)
                        row[20] = measureOfLevel._Date.ToString("T", CultureInfo.CreateSpecificCulture("fr-FR"));
                    listRow.Add(row);

                }

                AddBlankLineInDataGridView(listRow);
                //Inverse la liste pour avoir les mesures les plus récentes en premier.
                listRow.Reverse();
                foreach (List<object> ligne in listRow)
                {
                    dataGridViewLevel.Rows.Add(ligne.ToArray());
                }

                for (int i = dataGridViewLevel.RowCount - 1; i > dataGridViewLevel.RowCount - listRow.Count - 1; i--)
                {
                    for (int j = 0; j < dataGridViewLevel.ColumnCount; j++)
                    {
                        dataGridViewLevel[j, i].Style.ForeColor = Tsunami2.Preferences.Theme.Colors.Object;
                        dataGridViewLevel[j, i].ReadOnly = true;
                    }
                }
            }

            /// <summary>
            /// Ajoute une ligne blanche dans le dataGridView
            /// </summary>
            /// <param name="listRow"></param>
            private void AddBlankLineInDataGridView(List<List<object>> listRow)
            {
                List<object> row = new List<object>();
                //colonne suppression mesure 0
                row.Add(R.Level_Case_Vide);
                //colonne nom 1
                row.Add("");
                //colonne cala 2
                row.Add(R.Level_Case_Vide);
                //colonne checkbox use meas 3
                row.Add(R.Level_Case_Vide);
                //Dcum 4
                row.Add("");
                //Htheo 5
                row.Add("");
                //Hmes 6
                row.Add("");
                //colonne Dist 7
                row.Add("");
                //Rallonge 8
                row.Add("");
                //colonne Mes Brute 9
                row.Add("");
                //colonne Mes Corr 10
                row.Add("");
                //colonne Theo 11
                row.Add("");
                //colonne Depla 12
                row.Add("");
                //Obj 13
                row.Add("");
                //Move 14
                row.Add("");
                //colonne Residus / Aller 15
                row.Add("");
                //colonne Residus / Retour 16
                row.Add("");
                //Code Alesage 17
                row.Add("");
                //Mire 18
                row.Add("");
                //Commentaire 19
                row.Add("");
                //Time 20
                row.Add("");
                listRow.Add(row);
            }

            private void dataGridViewLevel_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
            //change la hauteur de la ligne dans le dataGridview quand ajout ligne
            {
                dataGridViewLevel.Rows[e.RowIndex].Height = 30;
            }

            private void dataGridViewLevel_CellClick(object sender, DataGridViewCellEventArgs e)
            //événements si on clique dans le tableau (effacement points)
            {
                if (e.RowIndex != -1 && e.ColumnIndex != -1)
                {
                    rowActiveCell = e.RowIndex;
                    //Ne permet de mettre le focus sur la case de suppression car cela pose des problèmes dans les modules guidés lors du focus sur une cellule invisible
                    if (e.ColumnIndex != 0)
                        columnActiveCell = e.ColumnIndex;
                    else
                        columnActiveCell = 9;
                }

                if (dataGridViewLevel.IsCurrentCellInEditMode) return;
                if (e.RowIndex == -1 && e.ColumnIndex == 4)
                {
                    if (dataGridViewLevel.RowCount > 1)
                    {
                        buttonSort_Click();
                    }

                    return;
                }

                if (e.RowIndex != -1)
                {
                    var type = Module._WorkingStationLevel._Parameters._Instrument._levelType;
                    if (type == LevelType.DNA03 || type == LevelType.LS15)
                    {
                        if (dataGridViewLevel[9, e.RowIndex].Style.ForeColor !=
                            Tsunami2.Preferences.Theme.Colors
                                .Object) //rowActiveCell < this._stationLevelModule._WorkingStationLevel._MeasureOfLevel.Count())
                        {
                            MeasureOfLevel mesureSelected = Module._WorkingStationLevel._MeasureOfLevel.Find(x =>
                                x._PointName == dataGridViewLevel[1, rowActiveCell].Value.ToString());
                            if (Module._InstrumentManager.SelectedInstrumentModule._TsuView is D.DNA03.View v)
                                v.label_PointName.Text = mesureSelected._PointName;
                        }
                    }

                    ///Point effacé
                    if ((e.ColumnIndex == 0) && (dataGridViewLevel.RowCount > 1) &&
                        (dataGridViewLevel[9, e.RowIndex].Style.ForeColor !=
                         Tsunami2.Preferences.Theme.Colors
                             .Object)) //&& (e.RowIndex < this._stationLevelModule._WorkingStationLevel._MeasureOfLevel.Count))
                    {
                        string titleAndMessage = string.Format(R.StringLevel_ConfirmDeletePoint,
                            dataGridViewLevel[1, e.RowIndex].Value.ToString());
                        MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                        {
                            ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL }
                        };
                        string respond = mi.Show().TextOfButtonClicked;
                        if (respond == R.T_OK)
                        {
                            Module.DeletePointInAllStation(dataGridViewLevel[1, e.RowIndex].Value.ToString());
                        }
                    }

                    ///Changement mire
                    if ((e.ColumnIndex == 18) && (dataGridViewLevel.RowCount >= 1) &&
                        (dataGridViewLevel[9, e.RowIndex].Style.ForeColor !=
                         Tsunami2.Preferences.Theme.Colors
                             .Object)) //&& (e.RowIndex < this._stationLevelModule._WorkingStationLevel._MeasureOfLevel.Count))
                    {
                        var measure = Module._WorkingStationLevel._MeasureOfLevel.Find(x =>
                            x._PointName == dataGridViewLevel[1, e.RowIndex].Value.ToString());
                        Module.ChangeStaff(measure);
                    }

                    ///Changement interfaces
                    if ((e.ColumnIndex == 19) && (dataGridViewLevel.RowCount >= 1) &&
                        (dataGridViewLevel[9, e.RowIndex].Style.ForeColor !=
                         Tsunami2.Preferences.Theme.Colors
                             .Object)) //&& (e.RowIndex < this._stationLevelModule._WorkingStationLevel._MeasureOfLevel.Count))
                    {
                        var measure = Module._WorkingStationLevel._MeasureOfLevel.Find(x =>
                            x._PointName == dataGridViewLevel[1, e.RowIndex].Value.ToString());
                        Module.ChangeInterfaces(measure);
                    }

                    ///Changement status mesure se détecte avec la couleur du texte en lightgray car les lignes peuvent être dans le désordre maintenant
                    if ((e.ColumnIndex == 3) && (dataGridViewLevel.RowCount > 1) &&
                        (dataGridViewLevel[9, e.RowIndex].Style.ForeColor !=
                         Tsunami2.Preferences.Theme.Colors
                             .Object)) //&& (e.RowIndex < this._stationLevelModule._WorkingStationLevel._MeasureOfLevel.Count))
                    {
                        Module.ReverseMeasureStatus(dataGridViewLevel[1, e.RowIndex].Value.ToString());
                    }

                    ///Récupération d'une mesure obsolete se détecte avec la couleur du texte en lightgray car les lignes peuvent être dans le désordre maintenant
                    if ((e.ColumnIndex == 3) && (dataGridViewLevel.RowCount > 1) &&
                        (dataGridViewLevel[9, e.RowIndex].Style.ForeColor ==
                         Tsunami2.Preferences.Theme.Colors
                             .Object)) //&& (e.RowIndex > this._stationLevelModule._WorkingStationLevel._MeasureOfLevel.Count))
                    {
                        string titleAndMessage = string.Format(R.StringLevel_AskIfReplaceActualMeasByOldMeas,
                            dataGridViewLevel[1, e.RowIndex].Value.ToString(),
                            dataGridViewLevel[20, e.RowIndex].Value.ToString());
                        MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                        {
                            ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                        };
                        string msgResponse = mi.Show().TextOfButtonClicked;
                        if (msgResponse == R.T_YES)
                        {
                            int index = Module._WorkingStationLevel._ObsoleteMeasureOfLevel.FindIndex(
                                x => x._PointName == dataGridViewLevel[1, e.RowIndex].Value.ToString()
                                  && x._Date.ToString("T", CultureInfo.CreateSpecificCulture("fr-FR")) == dataGridViewLevel[18, e.RowIndex].Value.ToString()
                                  && Math.Round(x._RawLevelReading * 100000, 0).ToString("F0", CultureInfo.CreateSpecificCulture("en-US")) == dataGridViewLevel[9, e.RowIndex].Value.ToString());
                            if (index != -1)
                            {
                                Module.ReplaceActualbyOldMeasure(
                                    Module._WorkingStationLevel._ObsoleteMeasureOfLevel[index]);
                            }
                        }
                    }

                    //Ancrage
                    //Ne permet de modifier les ancrages que si on est à l'aller ou si on est au retour sans avoir encodé des mesures aller
                    if (e.ColumnIndex == 2
                        //&& (e.RowIndex < this._stationLevelModule._WorkingStationLevel._MeasureOfLevel.Count) 
                        && (dataGridViewLevel[9, e.RowIndex].Style.ForeColor !=
                            Tsunami2.Preferences.Theme.Colors.Object)
                        && (_AllerRetourRepriseViewStrategy is AllerViewStrategy
                            || (Module._AllerStationLevel._MeasureOfLevel.FindIndex(x => x._RawLevelReading != na) ==
                                -1 && _AllerRetourRepriseViewStrategy is RetourViewStrategy)))
                    {
                        //Ajoute un ancrage si on clique sur cellule ancrage. Modifie l'ancrage dans aller-retour et reprise
                        System.Drawing.Bitmap image = dataGridViewLevel[2, e.RowIndex].Value as System.Drawing.Bitmap;
                        string pointName = dataGridViewLevel[1, e.RowIndex].Value.ToString();
                        //Astuce image ancrage vide fait 65 de large
                        if (image.Width == 65)
                        {
                            int index2 = Module._TheoPoint.FindIndex(x => x._Name == pointName);
                            if (index2 != -1)
                            {
                                Module._TheoPoint[index2].LGCFixOption = LgcPointOption.CALA;
                                Module.FinalModule.CalaPoints.Add(Module._TheoPoint[index2]);
                            }

                            int index1 = Module.FinalModule.PointsToBeAligned.FindIndex(x => x._Name == pointName);
                            if (index1 != -1)
                            {
                                Module.FinalModule.PointsToBeAligned.RemoveAt(index1);
                            }

                            foreach (Station st in Module.AllStationLevels)
                            {
                                st._MeasureOfLevel.Find(x => x._PointName == pointName)._Point.LGCFixOption = LgcPointOption.CALA;
                                st.ParametersBasic.LastChanged = DateTime.Now;
                            }
                        }
                        else
                        {
                            //if (this._stationLevelModule._AllerStationLevel._MeasureOfLevel.FindAll(x => x._Point.LGCFixOption == TSU.ENUM.LgcPointOption.CALA).Count > 1)
                            //{
                            int index3 = Module._TheoPoint.FindIndex(x => x._Name == pointName);
                            if (index3 != -1)
                            {
                                Module._TheoPoint[index3].LGCFixOption = LgcPointOption.VZ;
                                Module.FinalModule.PointsToBeAligned.Add(Module._TheoPoint[index3]);
                            }

                            int index4 = Module.FinalModule.CalaPoints.FindIndex(x => x._Name == pointName);
                            if (index4 != -1)
                            {
                                Module.FinalModule.CalaPoints.RemoveAt(index4);
                            }

                            foreach (Station st in Module.AllStationLevels)
                            {
                                st._MeasureOfLevel.Find(x => x._PointName == pointName)._Point.LGCFixOption =
                                    LgcPointOption.VZ;
                            }
                        }

                        Module.UpdateAllOffsets();
                    }
                    else
                    {
                        if (e.ColumnIndex == 2 && (dataGridViewLevel[9, e.RowIndex].Style.ForeColor !=
                                                   Tsunami2.Preferences.Theme.Colors
                                                       .Object)) //&& (e.RowIndex < this._stationLevelModule._WorkingStationLevel._MeasureOfLevel.Count))
                        {
                            new MessageInput(MessageType.Warning, R.StringLevel_ChangeCalaNotAvailable).Show();
                        }
                    }
                }
            }

            private void dataGridViewLevel_MouseClick(object sender, MouseEventArgs e)
            //affiche le context menu si on clique droit
            {
                //if (e.Button == System.Windows.Forms.MouseButtons.Right)
                //{
                //    //this.RefreshGlobalContextMenuItemCollection();
                //    this.ShowPopUpMenu();
                //}
            }

            private void dataGridViewLevel_CurrentCellChanged(object sender, EventArgs e)
            {
                /// change le current index du datagrid
                if (dataGridViewLevel.CurrentCell != null)
                {
                    if (dataGridViewLevel.CurrentCell.RowIndex != -1 && dataGridViewLevel.CurrentCell.ColumnIndex != -1)
                    {
                        rowActiveCell = dataGridViewLevel.CurrentCell.RowIndex;
                        columnActiveCell = dataGridViewLevel.CurrentCell.ColumnIndex;
                        ///Met à jour le nom du point à mesurer dans la vue instrument DNA03
                        var type = Module._WorkingStationLevel._Parameters._Instrument._levelType;
                        if (type == LevelType.DNA03 || type == LevelType.LS15)
                        {
                            if (rowActiveCell < Module._WorkingStationLevel._MeasureOfLevel.Count)
                            {
                                MeasureOfLevel mesureSelected = Module._WorkingStationLevel._MeasureOfLevel.Find(x =>
                                    x._PointName == dataGridViewLevel[1, rowActiveCell].Value.ToString());
                                if (Module._InstrumentManager.SelectedInstrumentModule._TsuView is D.DNA03.View v)
                                    v.label_PointName.Text = mesureSelected._PointName;
                            }
                        }
                    }
                }
            }

            private void dataGridViewLevel_CellValueChanged(object sender, DataGridViewCellEventArgs e)
            //evenements liés au changement d'une valeur dans le tableau
            {
                //garde en mémoire la cellule active
                if (e.RowIndex < dataGridViewLevel.Rows.Count && e.RowIndex != -1)
                {
                    rowActiveCell = e.RowIndex;
                }

                if (e.ColumnIndex < dataGridViewLevel.ColumnCount && e.ColumnIndex != -1)
                {
                    columnActiveCell = e.ColumnIndex;
                }

                //mesure de distance entrée
                if (e.RowIndex > -1 && e.ColumnIndex == 7)
                {
                    // evite nullreferenceException si case vide
                    if (dataGridViewLevel[e.ColumnIndex, e.RowIndex].Value == null)
                    {
                        //encode la nouvelle mesure dans une mesure temporaire et lance la mise à jour
                        MeasureOfLevel mesureEntree = Module._WorkingStationLevel._MeasureOfLevel.Find(x =>
                            x._PointName == dataGridViewLevel[1, e.RowIndex].Value.ToString());
                        if (mesureEntree._Distance != na)
                        {
                            mesureEntree._Distance = na;
                            mesureEntree._Date = DateTime.Now;
                            Module._WorkingStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                            Module.UpdateOffsets();
                        }

                    }
                    else
                    {
                        double datagridNumber =
                            T.Conversions.Numbers.ToDouble(
                                dataGridViewLevel[e.ColumnIndex, e.RowIndex].Value.ToString(), true, -9999);
                        if (datagridNumber != -9999)
                        {
                            //encode la nouvelle mesure dans une mesure temporaire et lance la mise à jour
                            MeasureOfLevel mesureEntree = Module._WorkingStationLevel._MeasureOfLevel.Find(x =>
                                x._PointName == dataGridViewLevel[1, e.RowIndex].Value.ToString());
                            if (mesureEntree._Distance != datagridNumber)
                            {
                                mesureEntree._Distance = datagridNumber;
                                mesureEntree._Date = DateTime.Now;
                                Module._WorkingStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                                Module.UpdateOffsets();
                            }
                        }
                        else
                        {
                            //message erreur, valeur encodée incorrecte
                            ShowMessage(R.StringLevel_WrongCellValue);
                            UpdateDatagridViewLevel();
                        }
                    }
                }

                //mesure brute entrée
                if (e.RowIndex > -1 && e.ColumnIndex == 9)
                {
                    string pointName = dataGridViewLevel[1, e.RowIndex].Value.ToString();
                    object newValue = dataGridViewLevel[e.ColumnIndex, e.RowIndex].Value;

                    // evite nullreferenceException si case vide
                    if (newValue == null)
                    {
                        //encode la nouvelle mesure dans une mesure temporaire et lance la mise à jour
                        Module.InitializeMeasure(pointName);
                    }
                    else
                    {
                        double datagridNumber = T.Conversions.Numbers.ToDouble(newValue.ToString(), true, -9999);
                        if (datagridNumber != -9999)
                        {
                            double rawLevelReading = datagridNumber / 100000;
                            //encode la nouvelle mesure dans une mesure temporaire et lance la mise à jour
                            Module.ChangeMeasure(pointName, rawLevelReading);
                        }
                        else
                        {
                            //message erreur, valeur encodée incorrecte
                            ShowMessage(R.StringLevel_WrongCellValue);
                            UpdateDatagridViewLevel();
                        }
                    }
                }

                //commentaire entré
                if (e.RowIndex > -1 && e.ColumnIndex == 20)
                {
                    if (e.RowIndex >= 0)
                    {
                        //Sélectionne le commentaire de l'élement suivant
                        if (rowActiveCell < Module._WorkingStationLevel._MeasureOfLevel.Count - 1)
                        {
                            columnActiveCell = 20;
                            rowActiveCell++;
                        }
                    }

                    if (dataGridViewLevel[e.ColumnIndex, e.RowIndex].Value != null)
                    {
                        string newComment = dataGridViewLevel[e.ColumnIndex, e.RowIndex].Value.ToString();
                        int maxLength = 80;
                        switch (Module._WorkingStationLevel._Parameters._LevelingDirection)
                        {
                            case LevelingDirection.ALLER:
                                maxLength = 111;
                                break;
                            case LevelingDirection.RETOUR:
                                maxLength = 96;
                                break;
                            case LevelingDirection.REPRISE:
                                maxLength = 81;
                                break;
                            default:
                                break;
                        }

                        if (newComment.Length < maxLength)
                        {
                            if (!newComment.Contains(";") && !newComment.Contains("%") &&
                                !newComment.Contains(
                                    "$")) // tsu-3144 && (System.Text.RegularExpressions.Regex.IsMatch(newComment, @"^[a-zA-Z0-9 ]+$"))))
                            {
                                MeasureOfLevel mesureEntree = Module._WorkingStationLevel._MeasureOfLevel.Find(x =>
                                    x._PointName == dataGridViewLevel[1, e.RowIndex].Value.ToString());
                                Module.ChangeComment(mesureEntree, newComment);
                                return;
                            }
                            else
                            {
                                ShowMessage(R.StringTilt_WrongCommentSemicolon);
                                UpdateView();
                                return;
                            }
                        }
                        else
                        {
                            switch (Module._WorkingStationLevel._Parameters._LevelingDirection)
                            {
                                case LevelingDirection.ALLER:
                                    ShowMessage(R.StringLevel_TooLongComment110);
                                    break;
                                case LevelingDirection.RETOUR:
                                    ShowMessage(R.StringLevel_TooLongComment95);
                                    break;
                                case LevelingDirection.REPRISE:
                                    ShowMessage(R.StringLevel_TooLongComment80);
                                    break;
                                default:
                                    break;
                            }

                            UpdateView();
                            return;
                        }
                    }
                    else
                    {
                        //Commentaire effacé
                        MeasureOfLevel mesureEntree = Module._WorkingStationLevel._MeasureOfLevel.Find(x =>
                            x._PointName == dataGridViewLevel[1, e.RowIndex].Value.ToString());
                        Module.ChangeComment(mesureEntree, "");
                        return;
                    }
                }

                //Rallonge entrée
                if (e.RowIndex > -1 && e.ColumnIndex == 8)
                {
                    // evite nullreferenceException si case vide
                    if (dataGridViewLevel[e.ColumnIndex, e.RowIndex].Value == null)
                    {
                        //encode la nouvelle rallonge et lance la mise à jour
                        MeasureOfLevel mesureEntree = Module._WorkingStationLevel._MeasureOfLevel.Find(x =>
                            x._PointName == dataGridViewLevel[1, e.RowIndex].Value.ToString());
                        
                        Module._WorkingStationLevel.CopyAsObsolete(mesureEntree);

                        mesureEntree._Extension = 0;
                        mesureEntree._Date = DateTime.Now;
                        Module._WorkingStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                        Module.UpdateOffsets();
                    }
                    else
                    {
                        double datagridNumber =
                            T.Conversions.Numbers.ToDouble(
                                dataGridViewLevel[e.ColumnIndex, e.RowIndex].Value.ToString(), true, -9999);
                        if (datagridNumber != -9999)
                        {
                            MeasureOfLevel mesureEntree = Module._WorkingStationLevel._MeasureOfLevel.Find(x =>
                                x._PointName == dataGridViewLevel[1, e.RowIndex].Value.ToString());
                            
                            Module._WorkingStationLevel.CopyAsObsolete(mesureEntree);

                            //encode la nouvelle rallonge et lance la mise à jour
                            mesureEntree._Extension = datagridNumber / 1000;
                            mesureEntree._Date = DateTime.Now;
                            Module._WorkingStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                            Module.UpdateOffsets();
                        }
                        else
                        {
                            //message erreur, valeur encodée incorrecte
                            ShowMessage(R.StringLevel_WrongCellValue);
                            UpdateDatagridViewLevel();
                        }
                    }
                }
            }

            internal void CellFocusDataGridView()
            //remet le focus sur la cellule active avant refresh du tableau
            {
                Action a = () =>
                {
                    dataGridViewLevel.CurrentCellChanged -= dataGridViewLevel_CurrentCellChanged;
                    if (rowActiveCell >= dataGridViewLevel.RowCount || rowActiveCell < 0)
                    {
                        rowActiveCell = 0;
                    }

                    if (columnActiveCell >= dataGridViewLevel.ColumnCount || columnActiveCell < 0 ||
                        columnActiveCell == 20)
                    {
                        columnActiveCell = 9;
                    }

                    if (dataGridViewLevel.RowCount > 0)
                    {
                        if (!dataGridViewLevel[columnActiveCell, rowActiveCell].Visible)
                        {
                            rowActiveCell = 0;
                            columnActiveCell = 9;
                        }

                        dataGridViewLevel.Focus();
                        dataGridViewLevel.ClearSelection();
                        dataGridViewLevel.CurrentCell = dataGridViewLevel[columnActiveCell, rowActiveCell];
                        dataGridViewLevel.Rows[rowActiveCell].Cells[columnActiveCell].Selected = true;
                        //dataGridViewLevel.BeginEdit(true);
                    }

                    //Permet de mettre le nom du point dans la vue DNA03
                    var type = Module._WorkingStationLevel._Parameters._Instrument._levelType;
                    if (type == LevelType.DNA03 || type == LevelType.LS15)
                    {
                        if (rowActiveCell < Module._WorkingStationLevel._MeasureOfLevel.Count)
                        {
                            MeasureOfLevel mesureSelected = Module._WorkingStationLevel._MeasureOfLevel.Find(x =>
                                x._PointName == dataGridViewLevel[1, rowActiveCell].Value.ToString());
                            if (instrumentView is D.DNA03.View v)
                                v.label_PointName.Text = mesureSelected._PointName;
                        }
                    }

                    dataGridViewLevel.CurrentCellChanged += new EventHandler(dataGridViewLevel_CurrentCellChanged);
                };
                Tsunami2.Properties.InvokeOnApplicationDispatcher(a);
            }

            internal void CellFocusDataGridView(string pointName)
            //remet le focus sur la cellule mesure du point name
            {
                for (int i = 0; i < dataGridViewLevel.RowCount; i++)
                {
                    if (dataGridViewLevel[1, i].Value.ToString() == pointName)
                    {
                        rowActiveCell = i;
                        columnActiveCell = 9;
                        CellFocusDataGridView();
                    }
                }
            }

            private void dataGridViewLevel_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
            //événements liés si on double clique dans le tableau (modification ancrages)
            {
                // tout déplacé dans l'event dataGridViewLevel_CellClick pour ne plus utiliser le double click sur tablette
            }

            /// <summary>
            /// Pour forcer le datagrid à reprendre le focus dans module guidé
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void dataGridViewLevel_MouseEnter(object sender, EventArgs e)
            {
                Action a = () => { dataGridViewLevel.Focus(); };
                Tsunami2.Properties.InvokeOnApplicationDispatcher(a);
            }

            /// <summary>
            /// make the datagridview in full window
            /// </summary>
            internal void MaximizeDataGridView()
            {
                // Hide the treeview
                splitContainerNivel.Panel1MinSize = 0;
                splitContainerNivel.SplitterDistance = 0;
                HideManualLevelView();
            }

            private void SetFirstDisplayedRowIndex(int rowIndex)
            {
                // Use BeginInvoke to defer the operation to the UI thread
                dataGridViewLevel.BeginInvoke((MethodInvoker)delegate
                {
                    try
                    {
                        dataGridViewLevel.FirstDisplayedScrollingRowIndex = rowIndex;
                    }
                    catch (Exception ex)
                    {
                        Logs.Log.AddEntryAsPayAttentionOf(Module,
                            $"Cannot reset the datagridView to the row #{rowIndex} {ex}");
                    }
                });
            }

            /// <summary>
            /// Met la scroll bar horizontale à gauche et la vertical en haut
            /// </summary>
            internal void SetScrollBarAtZero()
            {
                if (dataGridViewLevel.Columns.Count > 0 && dataGridViewLevel.Rows.Count > 0)
                {
                    if (dataGridViewLevel.Columns[0].Visible && dataGridViewLevel.Rows[0].Visible)
                    {
                        dataGridViewLevel.FirstDisplayedScrollingColumnIndex = 0;
                        SetFirstDisplayedRowIndex(0);
                    }
                    else
                    {
                        if (dataGridViewLevel.Columns.Count > 1 && dataGridViewLevel.Rows.Count > 0)
                        {
                            if (dataGridViewLevel.Columns[1].Visible && dataGridViewLevel.Rows[0].Visible)
                            {
                                dataGridViewLevel.FirstDisplayedScrollingColumnIndex = 1;
                                SetFirstDisplayedRowIndex(0);
                            }
                        }
                    }
                }
            }

            ///// <summary>
            ///// garde le focus sur la current cell dans le datagrid
            ///// </summary>]
            ///// <param name="sender"></param>
            ///// <param name="e"></param>
            //private void Timer_Keep_Cell_Focus_Tick(object sender, EventArgs e)
            //{
            //    this.Timer_Keep_Cell_Focus.Enabled = false;
            //    this.Timer_Keep_Cell_Focus.Stop();
            //    this.Timer_Keep_Cell_Focus.Dispose();
            //    //DataGridViewCellEventArgs eventCell = new DataGridViewCellEventArgs(this.columnActiveCell, this.rowActiveCell);
            //    //dataGridViewLevel_CellClick(this.dataGridViewLevel, eventCell);
            //}

            #endregion

            #region Treeview

            public void InitializeTreeviewParameters()
            //creation du treeview avec tous les nodes
            {
                switch (moduleType)
                {
                    case ModuleType.Guided:
                        showCalculationOption = false;
                        break;
                    case ModuleType.Advanced:
                        showCalculationOption = true;
                        break;
                    default:
                        showCalculationOption = true;
                        break;
                }

                _AllerRetourRepriseViewStrategy.InitializeTreeviewParameters(this);
                Module.Change();
            }

            public void RedrawTreeviewParameters()
            //creation du treeview avec tous les nodes
            {
                _AllerRetourRepriseViewStrategy.RedrawTreeviewParameters(this);
            }

            /// <summary>
            /// Choix du nom du noeud paramètre et de son format
            /// </summary>
            internal void DressNodeParameter()
            {
                //Choix de l'entête du noeud paramètre
                string chosenStrategy = (this.Module.FinalModule.RabotStrategy ==  Strategy.Not_Defined) ? string.Empty : " (Curve smooth alignment strategy: " + this.Module.FinalModule.RabotStrategy + ")";
                node_Parameters.Name = "Parameters__";
                if (Module._WorkingStationLevel != null)
                    node_Parameters.Text = string.Format(R.StringLevel_Parameters + chosenStrategy, Module._WorkingStationLevel._Name,
                        Module._WorkingStationLevel._Parameters._State._Description);
                node_Parameters.State = StateType.Normal;
                if (Module._WorkingStationLevel._Parameters._State is State.Opening)
                    node_Parameters.State = StateType.Normal;
                if (Module._WorkingStationLevel._Parameters._State is State.SFBcalculationSuccessfull)
                    node_Parameters.State = StateType.Normal;
                if (Module._WorkingStationLevel._Parameters._State is State.LGC2CalculationSuccessfull)
                    node_Parameters.State = StateType.Normal;
                if (Module._WorkingStationLevel._Parameters._State is State.StationLevelReadyToBeSaved)
                    node_Parameters.State = StateType.Ready;
                if (Module._WorkingStationLevel._Parameters._State is State.StationLevelSaved)
                    node_Parameters.State = StateType.Ready;
                if (Module._WorkingStationLevel._Parameters._State is State.LGC2CalculationFailed)
                    node_Parameters.State = StateType.NotReady;
                if (Module._WorkingStationLevel._Parameters._State is State.SFBcalculationFailed)
                    node_Parameters.State = StateType.NotReady;
                node_Parameters.ColorNodeBaseOnState(node_Parameters);
                node_Parameters.Tag = Module._WorkingStationLevel;
            }

            private void treeView_Parameters_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
            //Evenements liés si on clique dans le treeview
            {
                if (!e.Node.Bounds.Contains(e.Location)) return;
                if (e.Node == null) return;
                CheckDatagridIsInEditMode();
                string[] nameSplit;
                char splitChar = '_';
                nameSplit = e.Node.Name.Split(splitChar);
                if (moduleType == ModuleType.Advanced)
                {
                    if ((nameSplit[0] == "Parameters") && (nameSplit[1] == ""))
                    {
                        RenameStation();
                    }

                    if (nameSplit[1] == "Team")
                    {
                        ChangeTeam();
                    }

                    if (nameSplit[1] == "OperationID")
                    {
                        ChangeOperation();
                    }

                    if (nameSplit[1] == "Instrument")
                    {
                        ChangeInstrument();
                    }

                    if (nameSplit[1] == "Strategy" && nameSplit[2] != "ShowLGC")
                    {
                        ChangeComputeStrategy();
                    }

                    if (nameSplit[2] == "ShowLGC")
                    {
                        RunAndShowLGC2Calculation();
                    }

                    if (nameSplit[2] == "UseDist")
                    {
                        ReverseUseDistanceForLGC2();
                    } //non actif have to uncomment in ComputationBasedOn()
                    //if (nameSplit[1] == "HStation") { this.UseHGlobalLGCCalculation(); }
                }

                if (nameSplit[1] == "CoordType")
                {
                    ChangeZType();
                }

                if (nameSplit[1] == "Tolerance")
                {
                    ChangeTolerance();
                }

                if (nameSplit[1] == "ShowObsoleteMeas")
                {
                    ReverseShowObsoleteMeas();
                }

                if (nameSplit[1] == "Temperature")
                {
                    ChangeTemperature();
                }
            }

            private void treeView_Parameters_MouseClick(object sender, MouseEventArgs e)
            //Affiche le menu global context si on clique droit
            {
                //if (e.Button == System.Windows.Forms.MouseButtons.Right)
                //{
                //    //this.RefreshGlobalContextMenuItemCollection();
                //    this.ShowPopUpMenu();
                //}
            }

            #endregion

            #region Encodage mesure guided module element alignment

            internal void EnterLevelValue(string pointName)
            //Affiche le form pour encoder une mesure manuellement dans le cas du module guidé element alignment
            {
                MeasureOfLevel mesLevel =
                    Module._WorkingStationLevel._MeasureOfLevel.Find(x => x._PointName == pointName);
                string rallonge = Math.Round(mesLevel._Extension * 1000, 2)
                    .ToString("F2", CultureInfo.CreateSpecificCulture("en-US"));
                string theoReading = (mesLevel._TheoReading != na)
                    ? Math.Round(mesLevel._TheoReading * 100000, 0)
                        .ToString("F0", CultureInfo.CreateSpecificCulture("en-US"))
                    : "---";
                string previousValueText = (mesLevel._RawLevelReading != na)
                    ? Math.Round(mesLevel._RawLevelReading * 100000, 0)
                        .ToString("F0", CultureInfo.CreateSpecificCulture("en-US"))
                    : "";
                string titleAndMessage = pointName + "\r\n" +
                                         string.Format(R.StringAlignment_EnterLevelMesure, theoReading, rallonge);
                List<string> buttonTexts = new List<string> { R.T_OK, R.T_CANCEL };

                MessageTsu.ShowMessageWithTextBox(titleAndMessage, buttonTexts, out string buttonClicked,
                    out string textInput, previousValueText);

                if (buttonClicked != R.T_CANCEL)
                {
                    if (textInput == "")
                    {
                        //Effacement de la mesure
                        mesLevel._RawLevelReading = na;
                        mesLevel._Hmes = na;
                        mesLevel._EcartAller = na;
                        mesLevel._EcartRetour = na;
                        mesLevel._Residual = na;
                        mesLevel._ResidualAller = na;
                        mesLevel._ResidualRetour = na;
                        mesLevel._TheoReading = na;
                        mesLevel._Date = DateTime.MinValue;
                        mesLevel._staff._SpatialDistanceTargetHeight = mesLevel._RawLevelReading;
                        Module.UpdateOffsets();
                    }
                    else
                    {
                        double j = T.Conversions.Numbers.ToDouble(textInput, true, -9999);

                        if (j != -9999)
                        {
                            Module._WorkingStationLevel.CopyAsObsolete(mesLevel);

                            //encode la nouvelle mesure dans une mesure temporaire et lance la mise à jour
                            mesLevel._RawLevelReading = j / 100000;
                            mesLevel._Date = DateTime.Now;
                            mesLevel._staff._SpatialDistanceTargetHeight = mesLevel._RawLevelReading;
                            Module.CheckBocValidity(mesLevel);
                            Module.UpdateOffsets();
                        }
                        else
                        {
                            //message erreur, valeur encodée incorrecte
                            ShowMessage(R.StringLevel_WrongCellValue);
                            UpdateDatagridViewLevel();
                        }
                    }
                }
            }

            /// <summary>
            /// Lance une mesure automatique de nivellement sur le point sélectionné
            /// </summary>
            /// <param name="pointName"></param>
            internal void LaunchAutomaticMeasure(string pointName)
            {
                string MsgResponse = "";
                MeasureOfLevel mesLevel =
                    Module._WorkingStationLevel._MeasureOfLevel.Find(x => x._PointName == pointName);
                string rallonge = Math.Round(mesLevel._Extension * 1000, 2)
                    .ToString("F2", CultureInfo.CreateSpecificCulture("en-US"));
                string theoReading = (mesLevel._TheoReading != na)
                    ? Math.Round(mesLevel._TheoReading * 100000, 0)
                        .ToString("F0", CultureInfo.CreateSpecificCulture("en-US"))
                    : "---";
                string previousValueText = (mesLevel._RawLevelReading != na)
                    ? Math.Round(mesLevel._RawLevelReading * 100000, 0)
                        .ToString("F0", CultureInfo.CreateSpecificCulture("en-US"))
                    : "";
                string titleAndMessage = pointName + "\r\n" + string.Format(R.StringAlignment_LaunchAutomaticMeasure,
                    theoReading, rallonge, previousValueText);
                MessageInput mi = new MessageInput(MessageType.GoodNews, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL }
                };
                MsgResponse = mi.Show().TextOfButtonClicked;
                if (MsgResponse != R.T_CANCEL)
                {
                    CellFocusDataGridView(pointName);
                    ///Dans le cas d'un niveau non manuel, force à retourner dans le module nivellement pour les mesures (ex DNA03)
                    if (Module._InstrumentManager.SelectedInstrumentModule._TsuView is D.DNA03.View v)
                        v.LaunchMeasure();
                }
            }

            #endregion

            #region Alignment window

            private void UpdateAlignmentWindow(MeasureOfLevel mesureEntree)
            //fenêtre d'alignement d'élément lors de mesures continues 
            {
                textBox_PointAlign.Text = mesureEntree._PointName;
                double pointOffset = Math.Round((mesureEntree._TheoReading - mesureEntree._RawLevelReading) * 1000, 2);
                double pointDisplacement = -pointOffset;
                textBox_PointMovement.Text =
                    pointDisplacement.ToString("F2", CultureInfo.CreateSpecificCulture("en-US")) + " mm";
                //choix de la vertical progress bar à utiliser
                if (Math.Sign(pointOffset) >= 0)
                {
                    progressBar_TooHigh._doubleValue = pointOffset;
                    progressBar_TooLow._doubleValue = 0;
                }
                else
                {
                    progressBar_TooHigh._doubleValue = 0;
                    progressBar_TooLow.RightToLeft = RightToLeft.Yes;
                    progressBar_TooLow._doubleValue = pointOffset;
                }

                //Changement de la couleur du texte en fonction de l'écart et de la tolérance
                if (Math.Abs(pointDisplacement) < Module._WorkingStationLevel._Parameters._Tolerance)
                {
                    textBox_PointAlign.ForeColor = Tsunami2.Preferences.Theme.Colors.Good;
                    textBox_PointMovement.ForeColor = Tsunami2.Preferences.Theme.Colors.Good;
                }
                else
                {
                    textBox_PointAlign.ForeColor = Tsunami2.Preferences.Theme.Colors.Bad;
                    textBox_PointMovement.ForeColor = Tsunami2.Preferences.Theme.Colors.Bad;
                    if (Math.Abs(pointDisplacement) > 1.5 * Module._WorkingStationLevel._Parameters._Tolerance)
                    {
                        textBox_PointAlign.ForeColor = Tsunami2.Preferences.Theme.Colors.Bad;
                        textBox_PointMovement.ForeColor = Tsunami2.Preferences.Theme.Colors.Bad;
                    }
                }

            }

            #endregion

            #region Instrument view

            /// <summary>
            /// Redimensionne le clavier du manual ecartometer lorsqu'on deplace le splitter
            /// </summary>
            private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
            {
                if (Module != null)
                {
                    if (Module._InstrumentManager != null)
                    {
                        if (Module._InstrumentManager.SelectedInstrumentModule != null)
                        {
                            UpdateInstrumentView();
                            CheckDatagridIsInEditMode();
                        }
                    }
                }
            }

            internal void AddInstrumentMeasure(Measure measureFromInstrument)
            //ajoute la mesure venant dans l'instrument dans le tableau et lance la mise à jour de la station nivellement
            {
                if (InvokeRequired)
                {
                    // Invoke the event handler on the main UI thread
                    BeginInvoke(new Action(() => AddInstrumentMeasure(measureFromInstrument)));
                }
                else
                {
                    if (rowActiveCell != -1 && columnActiveCell != -1)
                    {
                        //ajoute seulement la mesure si on est dans une ligne du tableau avec une mesure et dans la colonne dist ou raw meas
                        if (rowActiveCell < Module._WorkingStationLevel._MeasureOfLevel.Count)
                        {
                            CheckDatagridIsInEditMode();
                            MeasureOfLevel mesureEntree = Module._WorkingStationLevel._MeasureOfLevel.Find(x =>
                                x._PointName == dataGridViewLevel[1, rowActiveCell].Value.ToString());
                            Module._WorkingStationLevel.CopyAsObsolete(mesureEntree);

                            MeasureOfLevel measureOfLevel = (MeasureOfLevel)measureFromInstrument;
                            //ajoute la mesure dans la colonne mesure brute
                            mesureEntree._RawLevelReading = measureOfLevel._RawLevelReading;
                            mesureEntree._Distance = measureOfLevel._Distance;
                            mesureEntree._Date = measureOfLevel._Date;
                            mesureEntree._Compass = measureOfLevel._Compass;
                            mesureEntree._EmqDistanceReading = measureOfLevel._EmqDistanceReading;
                            mesureEntree._RawEmqLevelReading = measureOfLevel._RawEmqLevelReading;
                            Module.UpdateOffsets();
                            if (measureOfLevel._ContinuousMeasurement)
                            {
                                panel_AlignWindow.Visible = true;
                                UpdateAlignmentWindow(mesureEntree);
                            }
                            else
                            {
                                panel_AlignWindow.Visible = false;
                            }

                            //garde en mémoire la cellule active
                            if (rowActiveCell < dataGridViewLevel.Rows.Count - 1)
                            {
                                //si c'est une mesure continue ne passe pas à la ligne suivante dans le tableau
                                if (!measureOfLevel._ContinuousMeasurement)
                                {
                                    rowActiveCell++;
                                }

                                CellFocusDataGridView();
                            }
                        }
                    }
                    //if (dataGridViewLevel.CurrentCell != null)
                    //{
                    //    //ajoute seulement la mesure si on est dans une ligne du tableau avec une mesure et dans la colonne dist ou raw meas
                    //    if (dataGridViewLevel.CurrentCell.RowIndex > -1 && (dataGridViewLevel.CurrentCell.ColumnIndex == 8 || dataGridViewLevel.CurrentCell.ColumnIndex == 6))
                    //    {
                    //        //garde en mémoire la cellule active
                    //        if (dataGridViewLevel.CurrentCell.RowIndex < dataGridViewLevel.Rows.Count - 1)
                    //        {
                    //            //si c'est une mesure continue ne passe pas à la ligne suivante dans le tableau
                    //            if ((measureFromInstrument as MeasureOfLevel)._ContinuousMeasurement == true)
                    //            {
                    //                rowActiveCell = dataGridViewLevel.CurrentCell.RowIndex;
                    //            }
                    //            else
                    //            {
                    //                rowActiveCell = dataGridViewLevel.CurrentCell.RowIndex + 1;
                    //            }

                    //        }
                    //        else
                    //        {
                    //            rowActiveCell = dataGridViewLevel.CurrentCell.RowIndex;
                    //        }
                    //        if (dataGridViewLevel.CurrentCell.ColumnIndex < dataGridViewLevel.Columns.Count - 1)
                    //        {
                    //            columnActiveCell = dataGridViewLevel.CurrentCell.ColumnIndex;
                    //        }
                    //        else
                    //        {
                    //            columnActiveCell = 5;
                    //        }
                    //        if (dataGridViewLevel.CurrentCell.RowIndex < this._stationLevelModule._WorkingStationLevel._MeasureOfLevel.Count())
                    //        {
                    //            if (dataGridViewLevel.IsCurrentCellInEditMode) dataGridViewLevel.EndEdit();
                    //            MeasureOfLevel mesureEntree = this._stationLevelModule._WorkingStationLevel._MeasureOfLevel.Find(x => x._PointName == dataGridViewLevel[1, dataGridViewLevel.CurrentCell.RowIndex].Value.ToString());
                    //            if (mesureEntree._LevelReading != 9999)
                    //            {
                    //                this._stationLevelModule._WorkingStationLevel._ObsoleteMeasureOfLevel.Add(mesureEntree.DeepCopy());
                    //                this._stationLevelModule._WorkingStationLevel._ObsoleteMeasureOfLevel[this._stationLevelModule._WorkingStationLevel._ObsoleteMeasureOfLevel.Count - 1]._Status = new MeasureStateBad();
                    //            }
                    //            //ajoute la mesure dans la colonne mesure brute
                    //            mesureEntree._LevelReading = (measureFromInstrument as MeasureOfLevel)._LevelReading;
                    //            mesureEntree._Distance = (measureFromInstrument as MeasureOfLevel)._Distance;
                    //            mesureEntree._Date = (measureFromInstrument as MeasureOfLevel)._Date;
                    //            mesureEntree._EmqDistanceReading = (measureFromInstrument as MeasureOfLevel)._EmqDistanceReading;
                    //            mesureEntree._EmqLevelReading = (measureFromInstrument as MeasureOfLevel)._EmqLevelReading;
                    //            this._stationLevelModule.UpdateOffsets();
                    //            this.CellFocusDataGridView();
                    //            if ((measureFromInstrument as MeasureOfLevel)._ContinuousMeasurement == true)
                    //            {
                    //                this.panel_AlignWindow.Visible = true;
                    //                this.UpdateAlignmentWindow(mesureEntree);
                    //            }
                    //            else
                    //            {
                    //                this.panel_AlignWindow.Visible = false;
                    //            }
                    //        }

                    //    }
                    //}
                }
            }

            /// <summary>
            /// Check if datagrid is in edit mode and validate it before leaving
            /// </summary>
            internal void CheckDatagridIsInEditMode()
            {
                if (dataGridViewLevel.IsCurrentCellInEditMode)
                {
                    //this.dataGridViewLevel.CellValueChanged -= this.dataGridViewLevel_CellValueChanged;
                    int saveRow = rowActiveCell;
                    int saveColumn = columnActiveCell;
                    //Try car de temps en temps endEdit peut faire une exception
                    try
                    {
                        dataGridViewLevel.EndEdit();

                    }
                    catch (Exception ex)
                    {
                        ex.Data.Clear();
                    }

                    //this.dataGridViewLevel.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewLevel_CellValueChanged);
                    rowActiveCell = saveRow;
                    columnActiveCell = saveColumn;
                }
            }

            /// <summary>
            /// Met à jour l'echelle de la vue instrument
            /// </summary>
            internal void ScaleInstrumentView()
            //Met à jour l'echelle de la vue instrument
            {
                //D.View v = this._stationLevelModule._InstrumentManager.SelectedInstrumentModule._TsuView as D.View;
                D.View v = instrumentView as D.View;
                if (v != null)
                {
                    float factorScale = Convert.ToSingle(splitContainer1.Panel2.Height) /
                                        Convert.ToSingle(v.startingHeight);
                    if (!double.IsNaN(factorScale) && !double.IsInfinity(factorScale))
                    {
                        v.Width = Convert.ToInt32(v.startingWidth * factorScale);
                        v.Height = splitContainer1.Panel2.Height;
                        (v as D.Manual.Level.View).ChangeScale(factorScale);
                    }
                }
            }

            internal override void UpdateInstrumentView()
            //permet d'afficher le form de l'instrument dans le panel du dessous
            {
                splitContainer1.Panel2.Controls.Clear();
                if (Module._InstrumentManager.SelectedInstrumentModule == null)
                    Module._InstrumentManager.SetInstrument(Module._WorkingStationLevel.ParametersBasic._Instrument);
                TsuView v = Module._InstrumentManager.SelectedInstrumentModule._TsuView;
                v.Dock = DockStyle.None;
                v.FormBorderStyle = FormBorderStyle.None;
                if (v is D.Manual.Level.View)
                {
                    ScaleInstrumentView();
                    v.TopLevel = false;
                    v.Show();
                }
                else
                {
                    v.ShowDockedFill();
                }

                splitContainer1.Panel2.Controls.Add(v);
                instrumentView = v;
            }

            /// <summary>
            /// permet d'annuler le background worker dans la vue du DNA03
            /// </summary>
            internal void CancelDnaBackGroundWorker()
            {
                if (Module._InstrumentManager.SelectedInstrumentModule == null)
                    Module._InstrumentManager.SetInstrument(Module._WorkingStationLevel.ParametersBasic._Instrument);
                if (Module._InstrumentManager.SelectedInstrumentModule?._TsuView is D.DNA03.View v)
                    Module.InvokeOnApplicationDispatcher(v.CancelBackGroundWorker);
            }

            #endregion


            #region button and click actions

            internal void buttonSave_Click()
            //enregistre les mesures dans un fichier dat
            {
                dataGridViewLevel.CancelEdit();
                if (Module._WorkingStationLevel._Parameters._State is State.StationLevelReadyToBeSaved)
                {
                    string respond = CheckBlueElevation_Click(true);
                    if (respond != R.StringDataGridBlueElevation_CancelExport)
                    {
                        Module.ExportToGeode();
                        switch (moduleType)
                        {
                            case ModuleType.Guided:
                                //demande s'il faut faire une reprise après retour si l'Emq aller est > 0.07mm
                                if ((Module._WorkingStationLevel._Parameters._EmqAller > 0.00007)
                                    && (Module._WorkingStationLevel._Parameters._EmqAller != na)
                                    && (_AllerRetourRepriseViewStrategy._name == "Retour"))
                                {
                                    string titleAndMessage = string.Format(R.StringCheminement_BadEmqReturn,
                                        Math.Round(Module._RetourStationLevel._Parameters._EmqAller * 1000, 2)
                                            .ToString());
                                    new MessageInput(MessageType.Warning, titleAndMessage).Show();
                                }

                                if ((Module._WorkingStationLevel._Parameters._EmqAller > 0.00007
                                     && Module._WorkingStationLevel._Parameters._EmqRetour > 0.00007)
                                    && (Module._WorkingStationLevel._Parameters._EmqAller != na)
                                    && (Module._WorkingStationLevel._Parameters._EmqRetour != na)
                                    && (_AllerRetourRepriseViewStrategy._name == "Reprise"))
                                {
                                    string titleAndMessage = string.Format(R.StringCheminement_BadEmqReprise,
                                        Math.Round(Module._WorkingStationLevel._Parameters._EmqAller * 1000, 2)
                                            .ToString(),
                                        Math.Round(Module._WorkingStationLevel._Parameters._EmqRetour * 1000, 2)
                                            .ToString());
                                    new MessageInput(MessageType.Warning, titleAndMessage).Show();
                                }

                                break;
                            case ModuleType.Advanced:
                                //Demande si on veut faire le retour lorsqu'on sauvegarde l'aller
                                if (_AllerRetourRepriseViewStrategy._name == "Aller")
                                {
                                    //string MsgResponse = "";
                                    //MsgResponse = this.ShowMessageOfChoice(R.StringLevel_Ask4ReturnMeas, R.T_YES, R.T_NO);
                                    //if (MsgResponse == R.T_YES)
                                    //{
                                    //    //  crèe une station de retour
                                    //    bigButton_AddReprise.Available = true;
                                    //    this._AllerRetourRepriseViewStrategy = new RetourViewStrategy();
                                    //    this._stationLevelModule.ChangeLevelingDirectionToRetour();
                                    //    this._AllerRetourRepriseViewStrategy.InitializeTreeviewParameters(this);
                                    //    this._stationLevelModule.UpdateOffsets();
                                    //}
                                }
                                else
                                {
                                    //demande s'il faut faire une reprise après retour si l'Emq aller est > 0.07mm
                                    if ((Module._WorkingStationLevel._Parameters._EmqAller > 0.00007)
                                        && (Module._WorkingStationLevel._Parameters._EmqAller != na)
                                        && (_AllerRetourRepriseViewStrategy._name == "Retour"))
                                    {
                                        string titleAndMessage = string.Format(R.StringLevel_BadEmqReturn,
                                            Math.Round(Module._WorkingStationLevel._Parameters._EmqAller * 1000, 2)
                                                .ToString());
                                        MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                                        {
                                            ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                                        };
                                        if (mi.Show().TextOfButtonClicked == R.T_YES)
                                        {
                                            // Crèe une station de reprise
                                            nombreReprise += 1;
                                            _numeroReprise = nombreReprise;
                                            Module.AddRepriseStation();
                                            _AllerRetourRepriseViewStrategy = new RepriseViewStrategy();
                                            _AllerRetourRepriseViewStrategy.InitializeTreeviewParameters(this);
                                            Module.UpdateOffsets();
                                        }
                                    }
                                    else
                                    {
                                        // demande s'il faut faire une reprise de reprise quand les 2 emq aller et retour sont supérieur à 0.07mm
                                        if ((Module._WorkingStationLevel._Parameters._EmqAller > 0.00007
                                             && Module._WorkingStationLevel._Parameters._EmqRetour > 0.00007)
                                            && (Module._WorkingStationLevel._Parameters._EmqAller != na)
                                            && (Module._WorkingStationLevel._Parameters._EmqRetour != na)
                                            && (_AllerRetourRepriseViewStrategy._name == "Reprise"))
                                        {
                                            string titleAndMessage = string.Format(R.StringLevel_BadEmqRepeat,
                                                Math.Round(
                                                        Module._WorkingStationLevel._Parameters._EmqAller * 1000,
                                                        2)
                                                    .ToString(),
                                                Math.Round(
                                                        Module._WorkingStationLevel._Parameters._EmqRetour * 1000,
                                                        2)
                                                    .ToString());
                                            MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                                            {
                                                ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                                            };
                                            if (mi.Show().TextOfButtonClicked == R.T_YES)
                                            {
                                                // Crèe une station de reprise
                                                nombreReprise += 1;
                                                _numeroReprise = nombreReprise;
                                                Module.AddRepriseStation();
                                                _AllerRetourRepriseViewStrategy = new RepriseViewStrategy();
                                                _AllerRetourRepriseViewStrategy.InitializeTreeviewParameters(this);
                                                Module.UpdateOffsets();
                                            }
                                        }
                                    }
                                }

                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            internal void buttonSelectPoint_Click()
            //sélection d'élement
            {
                //this.TryAction(this._stationLevelModule.SelectElements);
                dataGridViewLevel.CancelEdit();
                Module.SelectPoints();
            }

            /// <summary>
            /// ouvre l'element module pour sélectionner un fil d'une séquence
            /// </summary>
            private void button_SelectSequence_Click()
            {
                dataGridViewLevel.CancelEdit();
                Module.SelectSequence();
            }

            private void button_ImportCSV_Click()
            {
                Console.WriteLine("Importing CSV FIle from advanced level module");
                string openedFile = CsvImporter.ImportRabotCSV(this.Module.FinalModule);

                //update button text
                var button = bigButton_ImportCSV;
                if (openedFile != "")
                {
                    string newText = $"{button.Title};";
                    newText += button.Description.Contains("csv:") ? button.Description + "\r\n" : "";
                    if (!newText.Contains(openedFile))
                    {
                        newText += openedFile;
                        button.ChangeNameAndDescription(newText);
                    }

                }
            }

            private void buttonReprise10_Click()
            //Lancement station reprise N°10
            {
                _numeroReprise = 10;
                buttonReprise_Click();
            }

            private void buttonReprise9_Click()
            //Lancement station reprise N°9
            {
                _numeroReprise = 9;
                buttonReprise_Click();
            }

            private void buttonReprise8_Click()
            //Lancement station reprise N°8
            {
                _numeroReprise = 8;
                buttonReprise_Click();
            }

            private void buttonReprise7_Click()
            //Lancement station reprise N°7
            {
                _numeroReprise = 7;
                buttonReprise_Click();
            }

            private void buttonReprise6_Click()
            //Lancement station reprise N°6
            {
                _numeroReprise = 6;
                buttonReprise_Click();
            }

            private void buttonReprise5_Click()
            //Lancement station reprise N°5
            {
                _numeroReprise = 5;
                buttonReprise_Click();
            }

            private void buttonReprise4_Click()
            //Lancement station reprise N°4
            {
                _numeroReprise = 4;
                buttonReprise_Click();
            }

            private void buttonReprise3_Click()
            //Lancement station reprise N°3
            {
                _numeroReprise = 3;
                buttonReprise_Click();
            }

            private void buttonReprise2_Click()
            //Lancement station reprise N°2
            {
                _numeroReprise = 2;
                buttonReprise_Click();
            }

            private void buttonReprise1_Click()
            //Lancement station reprise N°1
            {
                _numeroReprise = 1;
                buttonReprise_Click();
            }

            private void buttonReprise_Click()
            //ouvertue station de reprise
            {
                dataGridViewLevel.CancelEdit();
                _AllerRetourRepriseViewStrategy.ShowMessageSave(this);
                Module.ChangeLevelingDirectionToReprise(_numeroReprise);
                _AllerRetourRepriseViewStrategy = new RepriseViewStrategy();
                _AllerRetourRepriseViewStrategy.InitializeTreeviewParameters(this);
                //this._stationLevelModule.UpdateOffsets();
                UpdateView();
            }

            private void buttonAddReprise_Click()
            //ajout d'une station de nivellement reprise
            {
                dataGridViewLevel.CancelEdit();
                _AllerRetourRepriseViewStrategy.ShowMessageSave(this);
                nombreReprise += 1;
                _numeroReprise = nombreReprise;
                Module.AddRepriseStation();
                _AllerRetourRepriseViewStrategy.InitializeTreeviewParameters(this);
                //this._stationLevelModule.UpdateOffsets();
                UpdateView();
            }

            private void buttonAller_Click()
            //Changement du sens de nivellement vers aller
            {
                dataGridViewLevel.CancelEdit();
                _AllerRetourRepriseViewStrategy.ShowMessageSave(this);
                Module.ChangeLevelingDirectionToAller();
                _AllerRetourRepriseViewStrategy.InitializeTreeviewParameters(this);
                // this._AllerRetourRepriseViewStrategy.RefreshContextMenu(this);
                //this._stationLevelModule.UpdateOffsets();
                UpdateView();
            }

            private void buttonRetour_Click()
            //Changement du sens de nivellement vers retour
            {
                dataGridViewLevel.CancelEdit();
                bigButton_AddReprise.Available = true;
                _AllerRetourRepriseViewStrategy.ShowMessageSave(this);
                Module.ChangeLevelingDirectionToRetour();
                _AllerRetourRepriseViewStrategy.InitializeTreeviewParameters(this);
                // this._AllerRetourRepriseViewStrategy.RefreshContextMenu(this);
                //this._stationLevelModule.UpdateOffsets(); //Done inside change levelling direction
                UpdateView();
            }

            internal void Context_OnClick(object sender, EventArgs e)
            //cache le menu context si on clique dessus
            {
                contextMenuStrip.Hide();
            }

            private void button_SFB_Click()
            //changement strategie calcul en SFB
            {
                dataGridViewLevel.CancelEdit();
                Module._LevelingComputationStrategy = new SFBStrategy();
                RedrawTreeviewParameters();
                Module.UpdateOffsets();
            }

            private void button_LGC2_Click()
            //changement strategie calcul en LGC2
            {
                dataGridViewLevel.CancelEdit();
                Module._LevelingComputationStrategy = new LGC2Strategy();
                RedrawTreeviewParameters();
                Module.UpdateOffsets();
            }

            /// <summary>
            /// Select the secondary staff
            /// </summary>
            private void bigButton_SecondaryStaff_Click()
            {
                Module.SelectSecondaryStaff();
            }

            /// <summary>
            /// Cache le context menu si on l'a ouvert par mégarde
            /// </summary>
            private void buttonCancel_Click()
            {
                //this.CloseMenu();
            }

            /// <summary>
            /// Trie par Dcum ou inverse dcum
            /// </summary>
            private void buttonSort_Click()
            {
                switch (Module.sortDirection)
                {
                    case SortDirection.Dcum:
                        Module.sortDirection = SortDirection.InvDcum;
                        break;
                    case SortDirection.InvDcum:
                        Module.sortDirection = SortDirection.Dcum;
                        break;
                    default:
                        break;
                }

                Module.SortMeasure();
                DataGridUtility.SetToDcumSorting(dataGridViewLevel, 4);
                UpdateListMeasure(false);
            }

            /// <summary>
            /// Contrôle des cotes bleues (contrôle de marche)
            /// </summary>
            private void CheckBlueElevation_Click()
            {
                CheckBlueElevation_Click(false);
            }

            /// <summary>
            /// Contrôle des cotes bleues (contrôle de marche)
            /// </summary>
            private string CheckBlueElevation_Click(bool showCancelButton)
            {
                string respond = "";
                if (Module.FinalModule is Level.Module module)
                {
                    respond = module.CheckBlueElevation(showCancelButton);
                }

                if (Module.FinalModule is Common.Guided.Module gm)
                {
                    if (gm.ParentModule is Common.Guided.Group.Module ggm)
                    {
                        respond = ggm.CheckBlueElevation(showCancelButton);
                    }
                }

                return respond;
            }

            /// <summary>
            /// Montre le fichier dat exporté
            /// </summary>
            private void bigButton_ShowDatFile_Click()
            {
                string filepath = Module._WorkingStationLevel.ParametersBasic._GeodeDatFilePath;
                if (filepath == "") filepath = Module._AllerStationLevel.ParametersBasic._GeodeDatFilePath;
                if (filepath == "") filepath = Module._RetourStationLevel.ParametersBasic._GeodeDatFilePath;
                if (filepath == "")
                {
                    int index = Module._RepriseStationLevel.FindIndex(x => x.ParametersBasic._GeodeDatFilePath != "");
                    if (index != -1) filepath = Module._RepriseStationLevel[index].ParametersBasic._GeodeDatFilePath;
                }

                if (System.IO.File.Exists(filepath)) Shell.Run(filepath);
            }

            /// <summary>
            /// Crèe un nouveau point à mesurer
            /// </summary>
            private void buttonCreateNewPoint_Click()
            {
                string newPointName = pointNewPoint;
                if (zone == "" && Module._WorkingStationLevel._MeasureOfLevel.Count != 0)
                {
                    zone = Module._WorkingStationLevel
                        ._MeasureOfLevel[Module._WorkingStationLevel._MeasureOfLevel.Count - 1]._Point._Zone;
                }

                List<string> namePartsTitle = new List<string> { "Zone/Acc", "Class", "Numéro", "Point" };
                List<string> nameParts = new List<string> { zone, classe, number, pointNewPoint };
                if (dataGridViewLevel.IsCurrentCellInEditMode) dataGridViewLevel.EndEdit();

                TableLayoutPanel tlp = CreationHelper.GetTableLayoutForRenamingPoint(nameParts,
                    Module.ElementModule.GetPointsNames(), namePartsTitle);

                MessageInput mi = new MessageInput(MessageType.Choice, R.StringLevel_AddNewPointWindow)
                {
                    ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL },
                    Controls = new List<Control> { tlp }
                };
                List<string> responds;
                using (var result = mi.Show())
                {
                    if (result.TextOfButtonClicked == R.T_CANCEL)
                    {
                        return;
                    }

                    responds = new List<string>();
                    TableLayoutPanel tplr = (TableLayoutPanel)result.ReturnedControls[0];
                    responds.Add(tplr.Controls["Zone/Acc"].Text);
                    responds.Add(tplr.Controls["Class"].Text);
                    responds.Add(tplr.Controls["Numéro"].Text);
                    responds.Add(tplr.Controls["Point"].Text);
                }

                string respond = "";
                if (responds.FindIndex(x => x == "") == -1)
                {
                    //Point alesage au format Geode
                    respond = string.Join(".", responds);
                }
                else
                {
                    if (responds[3] == "" & responds[0] != "" & responds[1] != "" && responds[2] != "")
                    {
                        //Point pilier au format Geode
                        respond = string.Join(".", responds);
                    }
                    else
                    {
                        //Autre format
                        respond = string.Join("", responds);
                    }

                }

                int index = Module.ElementModule.GetPointsInAllElements()
                    .FindIndex(x => x._Name == respond && Module.measGroupName != x._Origin);
                if (index != -1)
                {
                    if (Module._TheoPoint.FindIndex(x => x._Name == respond) == -1)
                    {
                        MessageInput mi1 = new MessageInput(MessageType.Choice,
                            R.StringLevel_PointNameAlreadyExistInElementModule)
                        {
                            ButtonTexts = new List<string> { R.T_OK, R.T_DONT }
                        };
                        string respond1 = mi1.Show().TextOfButtonClicked;
                        if (respond1 == R.T_OK)
                        {
                            E.Point newPoint = Module.ElementModule.GetPointsInAllElements()[index].DeepCopy();
                            Module.SetNewPointAlreadyExisting(newPoint);
                        }

                        return;
                    }
                    else
                    {
                        new MessageInput(MessageType.Warning, R.StringLevel_PointNameAlreadySelected).Show();
                        return;
                    }
                }

                if (Module._TheoPoint.FindIndex(x => x._Name == respond) == -1)
                {
                    string[] nameSplit;
                    char splitChar = '.';
                    respond = respond.ToUpper();
                    nameSplit = respond.Split(splitChar);
                    ///on ne peut pas avoir plus de 15 caractères par champs pour geode
                    foreach (string item in responds)
                    {
                        if (item.Length > 15)
                        {
                            new MessageInput(MessageType.Warning, R.StringLevel_WrongSocketCode1).Show();
                            return;
                        }
                    }

                    //if ((nameSplit.Length == 1 || nameSplit.Length == 4) && System.Text.RegularExpressions.Regex.IsMatch(respond, @"^[a-zA-Z0-9+._-]+$"))
                    if ((nameSplit.Length == 1 || (responds[0] != "" && responds[1] != "" && responds[2] != "")) &&
                        System.Text.RegularExpressions.Regex.IsMatch(respond, @"^[a-zA-Z0-9+._-]+$"))
                    {
                        E.SocketCode.GetSmartCodeFromName(respond, out E.SocketCode preselectedCode,
                            out string message);
                        E.SocketCode.Ask(this, Tsunami2.Preferences.Values.SocketCodes, preselectedCode, "NameForLTH",
                            out E.SocketCode socketCode, out double dummy);
                        if (socketCode != null)
                        {
                            Module.CreateNewPoint(respond, socketCode);
                            UpdateListMeasure();
                            zone = responds[0];
                            classe = responds[1];
                            number = responds[2];
                            pointNewPoint = responds[3];
                            ///incrémentation du point ou du numéro si pas dispo
                            int nb;
                            if (int.TryParse(pointNewPoint, out nb))
                            {
                                nb++;
                                pointNewPoint = nb.ToString();
                            }
                            else
                            {
                                int n;
                                if (int.TryParse(number, out n))
                                {
                                    n++;
                                    number = n.ToString();
                                }
                            }
                            //switch (nameSplit.Length)
                            //{
                            //    case 4:
                            //        ///Format Geode
                            //        this.zone = nameSplit[0];
                            //        this.classe = nameSplit[1];
                            //        this.number = nameSplit[2];
                            //        int n;
                            //        if (Int32.TryParse(this.number, out n))
                            //        {
                            //            n++;
                            //            this.number = n.ToString();
                            //        }
                            //        this.pointNewPoint = nameSplit[3];
                            //        break;
                            //    default:
                            //        //Format personalisé
                            //        this.number = "";
                            //        this.zone = "";
                            //        this.classe = "";
                            //        this.pointNewPoint = nameSplit[0];
                            //        break;
                            //}
                        }
                        else
                        {
                            ShowMessage(R.StringLevel_WrongSocketCode);
                        }
                    }
                    else
                    {
                        ShowMessage(R.StringLevel_WrongPointName);
                    }
                }
                else
                {
                    ShowMessage(R.StringLevel_PointNameAlreadyExist);
                }
            }

            /// <summary>
            /// Affiche la fenêtre pour sélectionner le code
            /// </summary>
            /// <returns></returns>
            private E.SocketCode SelectCode()
            {
                string cancel = R.T_CANCEL;

                BindingSource bindingSource = new BindingSource();
                bindingSource.DataSource = Tsunami2.Preferences.Values.SocketCodes;
                ///Met par défaut le code 10 qui correpond au code crapaud
                E.SocketCode socketCode = Tsunami2.Preferences.Values.SocketCodes.Find(x => x.Id == "10");
                var respond = ShowMessageOfPredefinedInput(
                    string.Format("{0};{1}", R.T_SELECT_SOCKET_CODE,
                        R.T_THE_CODE_DEFINE_THE_VERTICAL_EXTENSION_THAT_TSUNAMI_WILL_AUTOMATICALY_SELECTE),

                    R.T_OK,
                    cancel,
                    bindingSource, "FullName", null, false, R.SocketCode, socketCode);
                if (respond == null) return null;
                if (respond is E.SocketCode)
                {
                    socketCode = (respond as E.SocketCode);
                }
                else if (respond is string)
                {
                    if (respond as string == cancel) return null;
                    int codeId;
                    if (int.TryParse(respond as string, out codeId))
                    {
                        socketCode = Tsunami2.Preferences.Values.SocketCodes.Find(x => x.Id == codeId.ToString());
                    }
                }

                return socketCode;
            }

            /// <summary>
            /// change the compute strategy between LGC2 and SFB
            /// </summary>
            internal void ChangeComputeStrategy()
            {
                ShowPopUpMenu(new List<Control>()
                {
                    new BigButton(R.StringLevel_LGC2Calcul, R.Lgc, button_LGC2_Click),
                    new BigButton(R.StringLevel_SFBCalcul, R.Sfb, button_SFB_Click)
                });
            }

            /// <summary>
            /// change le type de module entre avancé et guidé
            /// Met à jour également le big button top
            /// </summary>
            /// <param name="moduleType"></param>
            internal void ChangeModuleType(ModuleType newModuleType = ModuleType.Advanced)
            {
                moduleType = newModuleType;
                _AllerRetourRepriseViewStrategy.UpdateBigButtonImage(this);
            }

            #endregion

            #region On Changes

            /// <summary>
            /// Affiche ou cache les anciennes mesures dans le datagridview
            /// </summary>
            private void ReverseShowObsoleteMeas()
            {
                Module.ReverseShowObsoleteMeas();
                RedrawTreeviewParameters();
                UpdateDatagridViewLevel();
            }

            /// <summary>
            /// Change l'instrument de mesure
            /// </summary>
            private void ChangeInstrument()
            {
                //choix de l'instrument
                //selection de l'instrument pour les mesures nivellements.
                I.Level instrument = Module.SelectInstrument();
                if (instrument != null)
                {
                    LevelingStaff newStaff = null;
                    while (newStaff == null)
                    {
                        newStaff = Module.SelectDefaultStaff();
                        if (newStaff == null)
                        {
                            new MessageInput(MessageType.Critical, R.StringLevel_ErrorStaff).Show();
                        }
                    }

                    Module.SelectSecondaryStaff();
                    //Message spécial pour Pedro afin qu'il n'oublie pas de faire la mise au point avec le DNA 03
                    if (instrument._levelType == LevelType.DNA03 || instrument._levelType == LevelType.LS15)
                    {
                        new MessageInput(MessageType.Warning, R.StringLevel_FocussingMessage).Show();
                    }

                    HideManualLevelView();
                    //bigButton_Elements.Available = true;
                    //bigButton_selectSequenceNiv.Available = true;
                    RedrawTreeviewParameters();
                }
            }

            /// <summary>
            /// hide the manual instrument panel 
            /// </summary>
            internal void HideManualLevelView()
            {
                var type = Module._WorkingStationLevel._Parameters._Instrument._levelType;
                if (type != LevelType.DNA03 && type != LevelType.LS15)
                {
                    //Hide instrument view except for the RS_auto
                    splitContainer1.Panel2MinSize = 0;
                    splitContainer1.SplitterDistance = splitContainer1.Size.Height - splitContainer1.SplitterWidth;
                }
                else
                {
                    splitContainer1.Panel2MinSize = 0;
                    splitContainer1.SplitterDistance = splitContainer1.Height - 200;
                }
            }

            /// <summary>
            /// Remet le splitter pour voir correctement la vue DNA
            /// </summary>
            internal void ResetSplitterDNA()
            {
                var type = Module._WorkingStationLevel._Parameters._Instrument._levelType;
                if (type == LevelType.DNA03 || type == LevelType.LS15)
                {
                    splitContainer1.Panel2MinSize = 0;
                    int newSplitterDistance = splitContainer1.Height - 200;
                    if (newSplitterDistance >= 0) splitContainer1.SplitterDistance = newSplitterDistance;
                }
            }

            /// <summary>
            /// Utilisation des distances pour le calcul LGC2
            /// </summary>
            private void ReverseUseDistanceForLGC2()
            {
                Module.ReverseUseDistanceForLGC2();
                RedrawTreeviewParameters();
            }

            /// <summary>
            /// Change la tolérance d'alignement
            /// </summary>
            private void ChangeTolerance()
            {
                //changement de la tolérance d'alignement
                MessageTsu.ShowMessageWithTextBox(R.StringLevel_AlignTolerance, new List<string> { R.T_OK, R.T_CANCEL },
                    out string buttonClicked, out string textInput,
                    Math.Round(Module._WorkingStationLevel._Parameters._Tolerance * 1000, 2).ToString());
                if (buttonClicked != R.T_CANCEL)
                {
                    double newTolerance = T.Conversions.Numbers.ToDouble(textInput, true, -9999);
                    if (newTolerance != -9999)
                    {
                        Module.ChangeTolerance(Math.Abs(newTolerance) / 1000);
                        RedrawTreeviewParameters();
                    }
                    else
                    {
                        ShowMessage(R.String_WrongTolerance);
                    }
                }
            }

            /// <summary>
            /// Changement de la temperature de mesure
            /// </summary>
            private void ChangeTemperature()
            {
                dataGridViewLevel.CancelEdit();

                //changement de la temperature de mesure
                MessageTsu.ShowMessageWithTextBox(R.StringLevel_ChangeTemperature,
                    new List<string> { R.T_OK, R.T_CANCEL },
                    out string buttonClicked, out string textInput,
                    Math.Round(Module._WorkingStationLevel._Parameters._Temperature, 1).ToString());
                if (buttonClicked != R.T_CANCEL)
                {
                    double newTemperature = T.Conversions.Numbers.ToDouble(textInput, true, -9999);
                    if (newTemperature != -9999)
                    {
                        Module.ChangeTemperature(newTemperature);
                        RedrawTreeviewParameters();
                        //this._stationLevelModule.UpdateOffsets();
                        UpdateView();
                    }
                    else
                    {
                        ShowMessage(R.String_WrongTemperature);
                    }
                }
            }

            /// <summary>
            /// Change le type de coordonnées utilisées pour le calcul
            /// </summary>
            private void ChangeZType()
            {
                //changement du type de Z pour calcul nivellement
                //Z user defined a été enlevé
                MessageInput mi = new MessageInput(MessageType.Choice,
                    R.T094 + Module._WorkingStationLevel._Parameters._ZType)
                {
                    ButtonTexts = new List<string> { R.StringLevel_HCCS, R.StringLevel_ZSU },
                    Sender = Module._Name
                };
                string MsgResponse = mi.Show().TextOfButtonClicked;
                if (MsgResponse == R.StringLevel_HCCS)
                {
                    Module.ChangeZType(CoordinatesType.CCS);
                    RedrawTreeviewParameters();
                }

                if (MsgResponse == R.StringLevel_ZSU)
                {
                    Module.ChangeZType(CoordinatesType.SU);
                    RedrawTreeviewParameters();
                }
                //if (MsgResponse == "Z User Defined")
                //{
                //    this._stationLevelModule._WorkingStationLevel._Parameters._ZType = TSU.ENUM.CoordinatesType.UserDefined;
                //    this.RedrawTreeviewParameters();
                //    this._stationLevelModule.UpdateOffsets();
                //}
            }

            /// <summary>
            /// Affiche le calcul LGC2
            /// </summary>
            private void RunAndShowLGC2Calculation()
            {
                Module.RunAndShowLGC2Calculation();
                //this.RedrawTreeviewParameters();
                UpdateDatagridViewLevel();
            }

            //Bloque le Hstation pour les calculs lorsqu'on à fait un calcul LGC global
            //private void UseHGlobalLGCCalculation()
            //{
            //    if (this._stationLevelModule.ElementModule.GetPointsInAllElements().FindIndex(x=>x._Name==this._stationLevelModule._WorkingStationLevel._Name)!=-1)
            //    {
            //        string MsgResponse = "";
            //        MsgResponse = this.ShowMessageOfChoice(R.StringLevel_UseHfromLGC2Calculation,
            //           R.T_OK,R.T_DONT,
            //            "");
            //        if (MsgResponse ==R.T_OK)
            //        {
            //            this._stationLevelModule.UseHGlobalLGCCalculation(true);
            //        }
            //        if (MsgResponse ==R.T_DONT)
            //        {
            //            this._stationLevelModule.UseHGlobalLGCCalculation(false);
            //        }
            //    }
            //}
            /// <summary>
            /// Change le nom de l'équipe terrain
            /// </summary>
            private void ChangeTeam()
            {
                MessageTsu.ShowMessageWithTextBox(R.String_TeamChoice, new List<string> { R.T_OK, R.T_CANCEL },
                    out string buttonClicked, out string textInput,
                    Module._WorkingStationLevel._Parameters._Team);
                if (buttonClicked != R.T_CANCEL)
                {
                    //Vérifie que le nom d'équipe contient que des caractères alphanumériques et est de maximum 8 caractères
                    if (System.Text.RegularExpressions.Regex.IsMatch(textInput, @"^[a-zA-Z]+$") &&
                        (textInput.Length < 9 || textInput == R.String_UnknownTeam))
                    {
                        textInput = textInput.ToUpper();
                        Module.ChangeTeam(textInput);
                        RedrawTreeviewParameters();
                    }
                    else
                    {
                        ShowMessage(R.String_WrongTeam);
                    }
                }
            }

            /// <summary>
            /// Permet à l'utilisateur de renommer la station comme il le veut
            /// </summary>
            private void RenameStation()
            {
                MessageTsu.ShowMessageWithTextBox(R.String_StationName, new List<string> { R.T_OK, R.T_CANCEL },
                    out string buttonClicked, out string textInput,
                    Module._WorkingStationLevel.specialPartOfStationName);
                if (buttonClicked != R.T_CANCEL)
                {
                    //Vérifie que le nom d'équipe contient que des caractères alphanumériques et est de maximum 8 caractères
                    if ((System.Text.RegularExpressions.Regex.IsMatch(textInput, @"^[a-zA-Z0-9._-]+$") ||
                         textInput == "") && (textInput.Length < 20))
                    {
                        textInput = textInput.ToUpper();
                        Module.RenameStation(textInput);
                        RedrawTreeviewParameters();
                    }
                    else
                    {
                        ShowMessage(R.String_WrongStationName);
                    }
                }
            }

            public void SelectOperation()
            {
                O.Operation o = Module.FinalModule.OperationManager.SelectOperation();

                if (o != null)
                {
                    Module.ChangeOperationID(o);
                    RedrawTreeviewParameters();
                }
            }

            private void ChangeOperation()
            {
                SelectOperation();
            }

            /// <summary>
            /// Change the view strategy to Aller strategy
            /// </summary>
            internal void ChangeLevelingDirectionToAller()
            {
                _AllerRetourRepriseViewStrategy = new AllerViewStrategy();
                LeveldataGridViewComment.MaxInputLength = 150;
            }

            /// <summary>
            /// Change the view strategy to Retour strategy
            /// </summary>
            internal void ChangeLevelingDirectionToRetour()
            {
                _AllerRetourRepriseViewStrategy = new RetourViewStrategy();
                LeveldataGridViewComment.MaxInputLength = 150;
            }

            /// <summary>
            /// Change the view strategy to Reprise strategy
            /// </summary>
            internal void ChangeLevelingDirectionToReprise(int numeroReprise = 1)
            {
                _AllerRetourRepriseViewStrategy = new RepriseViewStrategy();
                LeveldataGridViewComment.MaxInputLength = 150;
            }

            #endregion

            #region Message

            internal void ShowMessage(string titreAndMessage)
            //Affiche la fenêtre d'erreur si encodage incorrect
            {
                new MessageInput(MessageType.Critical, titreAndMessage).Show();
            }

            private void splitContainer_AlignText_Panel1_Paint(object sender, PaintEventArgs e)
            {

            }


            /// <summary>
            ///  Affiche message pour insérer un numéro d'opération lorsqu'on fait une sauvegarde
            /// </summary>
            /// <returns></returns>
            //internal bool ShowMessageForOperationID()
            //{
            //    this.ShowMessageOfCritical(R.String_OpIDChoice, R.T_OK);
            //    this.SelectOperation();
            //    return true;
            //}

            //internal bool ShowMessageForTeam()
            ////Affiche message pour insérer une équipe lorsqu'on fait une sauvegarde
            //{
            //    bool teamOK = true;
            //    string MsgResponse = "";
            //    MsgResponse = this.ShowMessageOfInput(R.String_TeamChoice, R.T_OK, R.T_CANCEL, this._stationLevelModule._WorkingStationLevel._Parameters._Team.ToString());
            //    if (MsgResponse != R.T_CANCEL)
            //    {
            //        //Vérifie que le nom d'équipe contient que des caractères alphanumériques et est de maximum 8 caractères
            //        if (System.Text.RegularExpressions.Regex.IsMatch(MsgResponse, @"^[a-zA-Z]+$") && (MsgResponse.Length < 9) && (MsgResponse != "Unknown Team"))
            //        {
            //            MsgResponse.ToUpper();
            //            this._stationLevelModule.ChangeTeam(MsgResponse);
            //        }
            //        else
            //        {
            //            this.ShowMessage(R.StringLevel_SavingCanceled);
            //            teamOK = false;
            //        }
            //    }
            //    else
            //    {
            //        this.ShowMessage(R.StringLevel_SavingCanceled);
            //        teamOK = false;
            //    }
            //    return teamOK;
            //}

            #endregion

        }
    }
}
