﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using TSU.Common;
using TSU.Common.Compute.Compensations.BeamOffsets;
using TSU.ENUM;
using TSU.Views;
using TSU.Views.Message;
using R = TSU.Properties.Resources;

namespace TSU.Level
{
    public partial class Station
    {
        [Serializable]
        internal abstract class LevelingDirectionViewStrategy
        {
            internal string _name { get; set; }
            internal double na = Tsunami2.Preferences.Values.na;
            internal abstract void RefreshDatagridView(View stationLevelModuleView);
            internal abstract void RefreshContextMenu(View stationLevelModuleView);
            internal abstract void ShowMessageSave(View stationLevelModuleView);
            internal abstract void UpdateBigButtonImage(View stationLevelModuleView);

            internal void ShowButtonRepriseDone(View stationLevelModuleView)
            //affiche les boutons reprises déjà faites du context menu
            {
                foreach (BigButton button in stationLevelModuleView.allRepriseButtons)
                {
                    int numeroReprise;
                    string stringNumeroReprise = "";
                    stringNumeroReprise = button.Name.Substring(button.Name.Length - 1);
                    if (int.TryParse(stringNumeroReprise, out numeroReprise))
                    {
                        //car ne lit que le dernier caractère
                        if (numeroReprise == 0)
                        {
                            numeroReprise = 10;
                        }

                        if (numeroReprise <= stationLevelModuleView.Module._RepriseStationLevel.Count() &&
                            (numeroReprise != stationLevelModuleView._numeroReprise))
                        {
                            stationLevelModuleView.contextButtons.Add(button);
                        }
                    }
                }
            }

            //internal void HideActualButtonReprise(StationLevelModuleView stationLevelModuleView)
            ////Cache le bouton de reprise actuelle
            //{
            //    foreach (BigButton button in stationLevelModuleView.contextButtons)
            //    {
            //        if (button.name == "Reprise " + (stationLevelModuleView._numeroReprise).ToString())
            //        {
            //            button.Visible = false;
            //        }
            //    }
            //}
            internal void InitializeTreeviewParameters(View stationLevelModuleView)
            {
                if (stationLevelModuleView.treeView_Parameters.Nodes.Count == 0)
                {
                    stationLevelModuleView.treeView_Parameters.BeginUpdate();
                    stationLevelModuleView.treeView_Parameters.Nodes.Clear();
                    //Noeud Parameter
                    stationLevelModuleView.treeView_Parameters.ImageList = TreeNodeImages.ImageListInstance;
                    stationLevelModuleView.treeView_Parameters.SelectedImageKey = "StationParameters";
                    stationLevelModuleView.node_Parameters = new TsuNode();
                    stationLevelModuleView.node_Parameters.SelectedImageKey = "StationParameters";
                    stationLevelModuleView.node_Parameters.ImageKey = "StationParameters";
                    stationLevelModuleView.treeView_Parameters.Nodes.Add(stationLevelModuleView.node_Parameters);
                    stationLevelModuleView.DressNodeParameter();
                    //Noeud Admin
                    stationLevelModuleView.node_Admin = new TsuNode();
                    if (stationLevelModuleView.showAdmin)
                    {

                        stationLevelModuleView.node_Parameters.Nodes.Add(stationLevelModuleView.node_Admin);
                        stationLevelModuleView.node_Admin.BasedOn(stationLevelModuleView);
                    }

                    //Noeud Computation
                    stationLevelModuleView.node_Computation = new TsuNode();
                    if (stationLevelModuleView.showComputation)
                    {
                        stationLevelModuleView.node_Parameters.Nodes.Add(stationLevelModuleView.node_Computation);
                        stationLevelModuleView.node_Computation.ComputationBasedOn(stationLevelModuleView);
                    }

                    stationLevelModuleView.contextMenuStrip = new ContextMenuStrip();
                    stationLevelModuleView.contextMenuStrip.Click +=
                        new EventHandler(stationLevelModuleView.Context_OnClick);
                    stationLevelModuleView.treeView_Parameters.ExpandAll();
                    stationLevelModuleView.treeView_Parameters.EndUpdate();
                }
                else
                {
                    RedrawTreeviewParameters(stationLevelModuleView);
                }

            }

            /// <summary>
            /// Redessine le treeview
            /// </summary>
            /// <param name="stationLevelModuleView"></param>
            internal void RedrawTreeviewParameters(View stationLevelModuleView)
            {
                TsuNode ghostNode_Computation = new TsuNode();
                TsuNode ghostNode_Admin = new TsuNode();
                stationLevelModuleView.treeView_Parameters.BeginUpdate();
                stationLevelModuleView.DressNodeParameter();
                //Noeud Admin
                if (stationLevelModuleView.showAdmin)
                {
                    ghostNode_Admin.BasedOn(stationLevelModuleView);
                    stationLevelModuleView.node_Admin.UpdateTsuNode(ghostNode_Admin);
                }
                else
                {
                    stationLevelModuleView.node_Admin.Nodes.Clear();
                }

                //Noeud Computation
                if (stationLevelModuleView.showComputation)
                {
                    ghostNode_Computation.ComputationBasedOn(stationLevelModuleView);
                    stationLevelModuleView.node_Computation.UpdateTsuNode(ghostNode_Computation);
                }
                else
                {
                    stationLevelModuleView.node_Computation.Nodes.Clear();
                }

                stationLevelModuleView.treeView_Parameters.EndUpdate();
            }
        }

        [Serializable]
        internal class AllerViewStrategy : LevelingDirectionViewStrategy
        {
            internal AllerViewStrategy()
            {
                _name = "Aller";
            }

            internal override void RefreshDatagridView(View stationLevelModuleView)
            //cache la colonne des residus aller et retour
            {
                //if (stationLevelModuleView.moduleType == ModuleType.Advanced) stationLevelModuleView.dataGridViewLevel.Columns[ColumnDelete].Visible = true;
                //else stationLevelModuleView.dataGridViewLevel.Columns[ColumnDelete].Visible = false;
                stationLevelModuleView.dataGridViewLevel.Columns[0].Visible = true;
                stationLevelModuleView.dataGridViewLevel.Columns[15].Visible = false;
                stationLevelModuleView.dataGridViewLevel.Columns[16].Visible = false;
                //Cache les colonnes lect theo et deplacement si pas de points en cala
                if (stationLevelModuleView.Module._AllerStationLevel._MeasureOfLevel.FindIndex(x =>
                        x._Point.LGCFixOption == LgcPointOption.CALA) == -1)
                {
                    stationLevelModuleView.dataGridViewLevel.Columns[11].Visible = false;
                    stationLevelModuleView.dataGridViewLevel.Columns[12].Visible = false;
                    stationLevelModuleView.dataGridViewLevel.Columns[13].Visible = false;
                    stationLevelModuleView.dataGridViewLevel.Columns[14].Visible = false;
                }
                else
                {
                    stationLevelModuleView.dataGridViewLevel.Columns[11].Visible = true;
                    stationLevelModuleView.dataGridViewLevel.Columns[12].Visible = true;
                    stationLevelModuleView.dataGridViewLevel.Columns[13].Visible = true;
                    stationLevelModuleView.dataGridViewLevel.Columns[14].Visible = true;
                }
                if (stationLevelModuleView.Module.FinalModule.RabotStrategy == Common.Compute.Rabot.Strategy.Not_Defined)
                {
                    stationLevelModuleView.dataGridViewLevel.Columns[13].Visible = false;
                    stationLevelModuleView.dataGridViewLevel.Columns[14].Visible = false;
                }
            }

            internal override void RefreshContextMenu(View stationLevelModuleView)
            //Affiche les boutons du context menu lorsqu'on retourne sur la station aller.
            {
                stationLevelModuleView._numeroReprise = 0;
                if (stationLevelModuleView.Module._AllerStationLevel._MeasureOfLevel.Count(
                        x => x._RawLevelReading != na) >=
                    2) stationLevelModuleView.contextButtons.Add(stationLevelModuleView.bigButton_Retour);
                ShowButtonRepriseDone(stationLevelModuleView);
            }

            internal override void ShowMessageSave(View stationLevelModuleView)
            //affiche message de sauvegarde avant changement station aller-retour-reprise
            {
                if (!(stationLevelModuleView.Module._WorkingStationLevel._Parameters._State is State.StationLevelSaved))
                {
                    if (stationLevelModuleView.Module._AllerStationLevel._Parameters._State is State
                            .StationLevelReadyToBeSaved)
                    {
                        MessageInput mi = new MessageInput(MessageType.Choice, R.StringLevel_OutwardSaving)
                        {
                            ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                        };
                        if (mi.Show().TextOfButtonClicked == R.T_YES)
                        {
                            stationLevelModuleView.Module.ExportToGeode();
                        }
                    }
                    else
                    {
                        new MessageInput(MessageType.Warning, R.StringLevel_CannotSave)
                        {
                            ButtonTexts = new List<string> { R.T_OK + "!" }
                        }.Show();
                    }
                }

            }

            internal override void UpdateBigButtonImage(View stationLevelModuleView)
            //met à jour l'image et texte du bigbuttonTop
            {
                string lastSaved = "";
                List<Control> l = new List<Control>();
                if (stationLevelModuleView.Module._AllerStationLevel.ParametersBasic.LastSaved != DateTime.MinValue)
                {
                    lastSaved = string.Format(R.StringLevel_LastSaved,
                        stationLevelModuleView.Module._AllerStationLevel.ParametersBasic.LastSaved.ToString("HH:mm:ss",
                            CultureInfo.InvariantCulture));
                }

                switch (stationLevelModuleView.moduleType)
                {
                    case ModuleType.Guided:
                        stationLevelModuleView._PanelTop.Controls.Clear();
                        if (stationLevelModuleView.Module._WorkingStationLevel._Parameters._State is State
                                .StationLevelReadyToBeSaved)
                        {

                            stationLevelModuleView.bigbuttonTop = new BigButton(string.Format(
                                    R.StringCheminement_OutwardButtonSaving,
                                    stationLevelModuleView.Module._AllerStationLevel._Parameters._Instrument._Name,
                                    lastSaved),
                                R.Level_Save_aller,
                                stationLevelModuleView.buttonSave_Click);
                            stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(
                                    R.StringCheminement_OutwardButtonSaving,
                                    stationLevelModuleView.Module._AllerStationLevel._Parameters._Instrument._Name,
                                    lastSaved),
                                R.Level_Save_aller);
                        }
                        else
                        {
                            stationLevelModuleView.bigbuttonTop = new BigButton(string.Format(
                                    R.StringCheminement_OutwardButton,
                                    stationLevelModuleView.Module._AllerStationLevel._Parameters._Instrument._Name,
                                    lastSaved),
                                R.Level_Aller,
                                stationLevelModuleView.buttonSave_Click);
                            stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(
                                    R.StringCheminement_OutwardButton,
                                    stationLevelModuleView.Module._AllerStationLevel._Parameters._Instrument._Name,
                                    lastSaved),
                                R.Level_Aller);
                        }

                        stationLevelModuleView.bigButton_CreateNewPoint.Visible = true;
                        l.Add(stationLevelModuleView.bigbuttonTop);
                        l.Add(stationLevelModuleView.bigButton_CheckBlueElevation);
                        l.Add(stationLevelModuleView.bigButton_CreateNewPoint);

                        l.Add(WorkFlow.Buttons.GetComputeButton(action: () =>
                        {
                            var fm = stationLevelModuleView._Module.FinalModule;
                            var ids = WorkFlow.DetermineAssemblyIds(fm.MagnetsToBeAligned);
                            foreach (var id in ids)
                            {
                                var modules =
                                    Tsunami2.Properties.MeasurementModules.GetByObservationType(ObservationType.Level,
                                        addRollModules: true);
                                Results r = WorkFlow.Compute(fm, modules, id, ObservationType.Level);
                                if (r.OK)
                                {
                                    WorkFlow.OffsetComputedInAllSystems.WaitOne(7000);
                                    string titleAndMessage = r.ToString("4LEVEL");
                                    new MessageInput(MessageType.FYI, titleAndMessage).Show();
                                }
                            }
                        }));
                        stationLevelModuleView.AddButtonsInPanelTop(l, DockStyle.Fill, 0);
                        break;
                    case ModuleType.Advanced:
                        stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(R.StringLevel_OutwardButton,
                                Math.Round(
                                        stationLevelModuleView.Module._AllerStationLevel._Parameters._Tolerance * 1000,
                                        2)
                                    .ToString("F2", CultureInfo.CreateSpecificCulture("en-US")), lastSaved),
                            R.Level_Aller);
                        break;
                    default:
                        break;
                }
            }
        }

        [Serializable]
        internal class RetourViewStrategy : LevelingDirectionViewStrategy
        {
            internal RetourViewStrategy()
            {
                _name = "Retour";
            }

            internal override void RefreshDatagridView(View stationLevelModuleView)
            //Affiche la colonne des residus aller et cache la colonne des residus retour + lecture theo + deplacement + affiche delete point
            {
                if (stationLevelModuleView.moduleType == ModuleType.Advanced)
                {
                    //Affiche delete point que s'il n'y a pas de mesures encodée dans la station aller
                    stationLevelModuleView.dataGridViewLevel.Columns[0].Visible =
                        stationLevelModuleView.Module._AllerStationLevel._MeasureOfLevel.FindIndex(x =>
                            x._RawLevelReading != na) == -1;
                }
                else stationLevelModuleView.dataGridViewLevel.Columns[0].Visible = false;

                stationLevelModuleView.dataGridViewLevel.Columns[11].Visible = false;
                stationLevelModuleView.dataGridViewLevel.Columns[12].Visible = false;
                if (stationLevelModuleView.Module._WorkingStationLevel._MeasureOfLevel.FindIndex(x =>
                        x._EcartAller != na) == -1)
                {
                    stationLevelModuleView.dataGridViewLevel.Columns[13].Visible = false;
                }
                else
                {
                    stationLevelModuleView.dataGridViewLevel.Columns[14].Visible = false;
                    stationLevelModuleView.dataGridViewLevel.Columns[14].Visible = false;
                }

                stationLevelModuleView.dataGridViewLevel.Columns[15].Visible = true;
            }

            internal override void RefreshContextMenu(View stationLevelModuleView)
            //Affiche les boutons du context menu lorsqu'on est sur la station retour.
            {
                stationLevelModuleView._numeroReprise = 0;
                //Permet de n'afficher le bouton add reprise qu'après un retour avec au moins 2 mesures encodées
                if (stationLevelModuleView.Module._RetourStationLevel._MeasureOfLevel.Count(x =>
                        x._RawLevelReading != na) >= 2)
                {
                    //Ne permet que 10 reprises
                    if (stationLevelModuleView._numeroReprise <= 9)
                        stationLevelModuleView.contextButtons.Add(stationLevelModuleView.bigButton_AddReprise);
                }

                //Si on n'a pas encodé au moins 2 mesures aller, ne permet plus de retourner à l'aller.
                if (stationLevelModuleView.Module._AllerStationLevel._MeasureOfLevel.Count(
                        x => x._RawLevelReading != na) >=
                    2) stationLevelModuleView.contextButtons.Add(stationLevelModuleView.bigButton_Aller);
                //Permet de n'afficher le bouton add reprise qu'après un retour avec au moins 2 mesures encodées
                ShowButtonRepriseDone(stationLevelModuleView);
            }

            internal override void ShowMessageSave(View stationLevelModuleView)
            //affiche message de sauvegarde avant changement station aller-retour-reprise
            {
                if (!(stationLevelModuleView.Module._WorkingStationLevel._Parameters._State is State.StationLevelSaved))
                {
                    if (stationLevelModuleView.Module._WorkingStationLevel._Parameters._State is State
                            .StationLevelReadyToBeSaved)
                    {
                        MessageInput mi = new MessageInput(MessageType.Choice, R.StringLevel_ReturnSaving)
                        {
                            ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                        };
                        if (mi.Show().TextOfButtonClicked == R.T_YES)
                        {
                            stationLevelModuleView.Module.ExportToGeode();
                        }
                    }
                    else
                    {
                        new MessageInput(MessageType.Warning, R.StringLevel_CannotSave)
                        {
                            ButtonTexts = new List<string> { R.T_OK + "!" }
                        }.Show();
                    }
                }

            }

            internal override void UpdateBigButtonImage(View stationLevelModuleView)
            //met à jour l'image et texte du bigbuttonTop
            {
                string lastSaved = "";
                List<Control> l = new List<Control>();
                if (stationLevelModuleView.Module._RetourStationLevel.ParametersBasic.LastSaved != DateTime.MinValue)
                {
                    lastSaved = string.Format(R.StringLevel_LastSaved,
                        stationLevelModuleView.Module._RetourStationLevel.ParametersBasic.LastSaved.ToString("HH:mm:ss",
                            CultureInfo.InvariantCulture));
                }

                switch (stationLevelModuleView.moduleType)
                {
                    case ModuleType.Guided:
                        string emqAller = R.T_NA;
                        stationLevelModuleView._PanelTop.Controls.Clear();
                        if (stationLevelModuleView.Module._RetourStationLevel._Parameters._EmqAller != na)
                        {
                            emqAller = Math
                                .Round(stationLevelModuleView.Module._RetourStationLevel._Parameters._EmqAller * 1000,
                                    2).ToString() + " mm";
                        }

                        stationLevelModuleView._PanelTop.Controls.Clear();
                        if (stationLevelModuleView.Module._WorkingStationLevel._Parameters._State is State
                                .StationLevelReadyToBeSaved)
                        {
                            stationLevelModuleView.bigbuttonTop = new BigButton(string.Format(
                                    R.StringCheminement_ReturnButtonSaving,
                                    emqAller,
                                    stationLevelModuleView.Module._RetourStationLevel._Parameters._Instrument._Name,
                                    lastSaved),
                                R.Level_Save_aller,
                                stationLevelModuleView.buttonSave_Click);
                            stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(
                                    R.StringCheminement_ReturnButtonSaving,
                                    emqAller,
                                    stationLevelModuleView.Module._RetourStationLevel._Parameters._Instrument._Name,
                                    lastSaved),
                                R.Level_Save_aller);
                        }
                        else
                        {
                            stationLevelModuleView.bigbuttonTop = new BigButton(string.Format(
                                    R.StringCheminement_ReturnButton,
                                    emqAller,
                                    stationLevelModuleView.Module._RetourStationLevel._Parameters._Instrument._Name,
                                    lastSaved),
                                R.Level_Retour,
                                stationLevelModuleView.buttonSave_Click);
                            stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(
                                    R.StringCheminement_ReturnButton,
                                    emqAller,
                                    stationLevelModuleView.Module._RetourStationLevel._Parameters._Instrument._Name,
                                    lastSaved),
                                R.Level_Retour);
                        }

                        l.Add(stationLevelModuleView.bigbuttonTop);
                        l.Add(stationLevelModuleView.bigButton_CheckBlueElevation);
                        stationLevelModuleView.AddButtonsInPanelTop(l, DockStyle.Fill, 0);
                        break;
                    case ModuleType.Advanced:
                        if (stationLevelModuleView.Module._AllerStationLevel._MeasureOfLevel.Count(x =>
                                x._RawLevelReading != na) < 2)
                        {
                            stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(R.StringLevel_ReturnButton,
                                    Math.Round(
                                        stationLevelModuleView.Module._RetourStationLevel._Parameters._Tolerance * 1000,
                                        2).ToString("F2", CultureInfo.CreateSpecificCulture("en-US")), lastSaved),
                                R.Level_Retour);
                        }
                        else
                        {
                            stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(
                                    R.StringLevel_ReturnBigButtonDblHeight,
                                    Math.Round(
                                        stationLevelModuleView.Module._RetourStationLevel._Parameters._Tolerance * 1000,
                                        2).ToString("F2", CultureInfo.CreateSpecificCulture("en-US")), lastSaved),
                                R.Level_Retour);
                        }

                        break;
                    default:
                        break;
                }
            }
        }

        [Serializable]
        internal class RepriseViewStrategy : LevelingDirectionViewStrategy
        {
            internal RepriseViewStrategy()
            {
                _name = "Reprise";
            }

            internal override void RefreshDatagridView(View stationLevelModuleView)
            //Affiche la colonne des residus aller et retour, cache la colonne lecture theo + deplacement + affiche delete point
            {
                //La colonne effacer les points n'est jamais visible car une reprise doit être la même qu'une station retour sinon ce n'est plus une reprise.
                stationLevelModuleView.dataGridViewLevel.Columns[0].Visible = false;
                stationLevelModuleView.dataGridViewLevel.Columns[11].Visible = false;
                stationLevelModuleView.dataGridViewLevel.Columns[12].Visible = false;
                if (stationLevelModuleView.Module._WorkingStationLevel._MeasureOfLevel.FindIndex(x =>
                        x._EcartAller != na) == -1)
                {
                    stationLevelModuleView.dataGridViewLevel.Columns[13].Visible = false;
                }
                else
                {
                    stationLevelModuleView.dataGridViewLevel.Columns[13].Visible = false;
                    stationLevelModuleView.dataGridViewLevel.Columns[14].Visible = false;
                }

                if (stationLevelModuleView.Module._WorkingStationLevel._MeasureOfLevel.FindIndex(x =>
                        x._EcartRetour != na) == -1)
                {
                    stationLevelModuleView.dataGridViewLevel.Columns[15].Visible = false;
                }
                else
                {
                    stationLevelModuleView.dataGridViewLevel.Columns[15].Visible = true;
                }
            }

            internal override void RefreshContextMenu(View stationLevelModuleView)
            //Affiche les boutons du context menu lorsqu'on retourne sur la station aller.
            {
                //Permet de n'afficher le bouton add reprise qu'après avoir encodé au moins 2 mesures dans la station reprise actuelle
                if (stationLevelModuleView.Module._RepriseStationLevel[stationLevelModuleView._numeroReprise - 1]
                        ._MeasureOfLevel.Count(x => x._RawLevelReading != na) >= 2)
                {
                    //Ne permet que 10 reprises
                    if (stationLevelModuleView._numeroReprise <= 9)
                        stationLevelModuleView.contextButtons.Add(stationLevelModuleView.bigButton_AddReprise);
                }

                //Si on n'a pas encodé de mesures aller, ne permet plus de retourner à l'aller.
                if (stationLevelModuleView.Module._AllerStationLevel._MeasureOfLevel.Count(
                        x => x._RawLevelReading != na) >=
                    2) stationLevelModuleView.contextButtons.Add(stationLevelModuleView.bigButton_Aller);
                stationLevelModuleView.contextButtons.Add(stationLevelModuleView.bigButton_Retour);
                ShowButtonRepriseDone(stationLevelModuleView);
            }

            internal override void ShowMessageSave(View stationLevelModuleView)
            //affiche message de sauvegarde avant changement station aller-retour-reprise
            {
                if (!(stationLevelModuleView.Module._WorkingStationLevel._Parameters._State is State.StationLevelSaved))
                {
                    if (stationLevelModuleView.Module._WorkingStationLevel._Parameters._State is State
                            .StationLevelReadyToBeSaved)
                    {
                        string titleAndMessage = string.Format(R.StringLevel_RepeatSaving,
                            stationLevelModuleView.Module._NumeroReprise.ToString());
                        MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                        {
                            ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                        };
                        if (mi.Show().TextOfButtonClicked == R.T_YES)
                        {
                            stationLevelModuleView.Module.ExportToGeode();
                        }
                    }
                    else
                    {
                        new MessageInput(MessageType.Warning, R.StringLevel_CannotSave)
                        {
                            ButtonTexts = new List<string> { R.T_OK + "!" }
                        }.Show();
                    }
                }
            }

            internal override void UpdateBigButtonImage(View stationLevelModuleView)
            //met à jour l'image et texte du bigbuttonTop
            {
                string lastSaved = "";
                List<Control> l = new List<Control>();
                switch (stationLevelModuleView.moduleType)
                {
                    case ModuleType.Guided:
                        string emqAller = R.T_NA;
                        string emqRetour = R.T_NA;
                        stationLevelModuleView._PanelTop.Controls.Clear();
                        if (stationLevelModuleView.Module._RepriseStationLevel[0].ParametersBasic.LastSaved !=
                            DateTime.MinValue)
                        {
                            lastSaved = string.Format(R.StringLevel_LastSaved,
                                stationLevelModuleView.Module._RepriseStationLevel[0].ParametersBasic.LastSaved
                                    .ToString("HH:mm:ss", CultureInfo.InvariantCulture));
                        }

                        if (stationLevelModuleView.Module._RepriseStationLevel[0]._Parameters._EmqAller != na)
                        {
                            emqAller = Math
                                .Round(
                                    stationLevelModuleView.Module._RepriseStationLevel[0]._Parameters._EmqAller * 1000,
                                    2).ToString() + " mm";
                        }

                        if (stationLevelModuleView.Module._RepriseStationLevel[0]._Parameters._EmqRetour != na)
                        {
                            emqRetour = Math
                                .Round(
                                    stationLevelModuleView.Module._RepriseStationLevel[0]._Parameters._EmqRetour * 1000,
                                    2).ToString() + " mm";
                        }

                        stationLevelModuleView._PanelTop.Controls.Clear();

                        if (stationLevelModuleView.Module._WorkingStationLevel._Parameters._State is State
                                .StationLevelReadyToBeSaved)
                        {
                            stationLevelModuleView.bigbuttonTop = new BigButton(string.Format(
                                    R.StringCheminement_RepeatButtonSaving,
                                    emqAller,
                                    emqRetour,
                                    stationLevelModuleView.Module._RepriseStationLevel[0]._Parameters._Instrument._Name,
                                    lastSaved),
                                R.Level_Save_reprise,
                                stationLevelModuleView.buttonSave_Click);
                            stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(
                                    R.StringCheminement_RepeatButtonSaving,
                                    emqAller,
                                    emqRetour,
                                    stationLevelModuleView.Module._RepriseStationLevel[0]._Parameters._Instrument._Name,
                                    lastSaved),
                                R.Level_Save_reprise);
                        }
                        else
                        {
                            stationLevelModuleView.bigbuttonTop = new BigButton(string.Format(
                                    R.StringCheminement_RepeatButton,
                                    emqAller,
                                    emqRetour,
                                    stationLevelModuleView.Module._RepriseStationLevel[0]._Parameters._Instrument._Name,
                                    lastSaved),
                                R.Level_Reprise_0,
                                stationLevelModuleView.buttonSave_Click);
                            stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(
                                    R.StringCheminement_RepeatButton,
                                    emqAller,
                                    emqRetour,
                                    stationLevelModuleView.Module._RepriseStationLevel[0]._Parameters._Instrument._Name,
                                    lastSaved),
                                R.Level_Reprise_0);
                        }

                        l.Add(stationLevelModuleView.bigbuttonTop);
                        l.Add(stationLevelModuleView.bigButton_CheckBlueElevation);
                        stationLevelModuleView.AddButtonsInPanelTop(l, DockStyle.Fill, 0);
                        break;
                    case ModuleType.Advanced:
                        if (stationLevelModuleView.Module
                                ._RepriseStationLevel[stationLevelModuleView._numeroReprise - 1].ParametersBasic
                                .LastSaved != DateTime.MinValue)
                        {
                            lastSaved = string.Format(R.StringLevel_LastSaved,
                                stationLevelModuleView.Module
                                    ._RepriseStationLevel[stationLevelModuleView._numeroReprise - 1].ParametersBasic
                                    .LastSaved.ToString("HH:mm:ss", CultureInfo.InvariantCulture));
                        }

                        switch (stationLevelModuleView._numeroReprise)
                        {
                            case 1:
                                stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(
                                        R.StringLevel_RepeatButton1,
                                        Math.Round(
                                            stationLevelModuleView.Module
                                                ._RepriseStationLevel[stationLevelModuleView._numeroReprise - 1]
                                                ._Parameters._Tolerance * 1000, 2).ToString("F2",
                                            CultureInfo.CreateSpecificCulture("en-US")), lastSaved),
                                    R.Level_Reprise_1);
                                break;
                            case 2:
                                stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(
                                        R.StringLevel_RepeatButton2,
                                        Math.Round(
                                            stationLevelModuleView.Module
                                                ._RepriseStationLevel[stationLevelModuleView._numeroReprise - 1]
                                                ._Parameters._Tolerance * 1000, 2).ToString("F2",
                                            CultureInfo.CreateSpecificCulture("en-US")), lastSaved),
                                    R.Level_Reprise_2);
                                break;
                            case 3:
                                stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(
                                        R.StringLevel_RepeatButton3,
                                        Math.Round(
                                            stationLevelModuleView.Module
                                                ._RepriseStationLevel[stationLevelModuleView._numeroReprise - 1]
                                                ._Parameters._Tolerance * 1000, 2).ToString("F2",
                                            CultureInfo.CreateSpecificCulture("en-US")), lastSaved),
                                    R.Level_Reprise_3);
                                break;
                            case 4:
                                stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(
                                        R.StringLevel_RepeatButton4,
                                        Math.Round(
                                            stationLevelModuleView.Module
                                                ._RepriseStationLevel[stationLevelModuleView._numeroReprise - 1]
                                                ._Parameters._Tolerance * 1000, 2).ToString("F2",
                                            CultureInfo.CreateSpecificCulture("en-US")), lastSaved),
                                    R.Level_Reprise_4);
                                break;
                            case 5:
                                stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(
                                        R.StringLevel_RepeatButton5,
                                        Math.Round(
                                            stationLevelModuleView.Module
                                                ._RepriseStationLevel[stationLevelModuleView._numeroReprise - 1]
                                                ._Parameters._Tolerance * 1000, 2).ToString("F2",
                                            CultureInfo.CreateSpecificCulture("en-US")), lastSaved),
                                    R.Level_Reprise_5);
                                break;
                            case 6:
                                stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(
                                        R.StringLevel_RepeatButton6,
                                        Math.Round(
                                            stationLevelModuleView.Module
                                                ._RepriseStationLevel[stationLevelModuleView._numeroReprise - 1]
                                                ._Parameters._Tolerance * 1000, 2).ToString("F2",
                                            CultureInfo.CreateSpecificCulture("en-US")), lastSaved),
                                    R.Level_Reprise_6);
                                break;
                            case 7:
                                stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(
                                        R.StringLevel_RepeatButton7,
                                        Math.Round(
                                            stationLevelModuleView.Module
                                                ._RepriseStationLevel[stationLevelModuleView._numeroReprise - 1]
                                                ._Parameters._Tolerance * 1000, 2).ToString("F2",
                                            CultureInfo.CreateSpecificCulture("en-US")), lastSaved),
                                    R.Level_Reprise_7);
                                break;
                            case 8:
                                stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(
                                        R.StringLevel_RepeatButton8,
                                        Math.Round(
                                            stationLevelModuleView.Module
                                                ._RepriseStationLevel[stationLevelModuleView._numeroReprise - 1]
                                                ._Parameters._Tolerance * 1000, 2).ToString("F2",
                                            CultureInfo.CreateSpecificCulture("en-US")), lastSaved),
                                    R.Level_Reprise_8);
                                break;
                            case 9:
                                stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(
                                        R.StringLevel_RepeatButton9,
                                        Math.Round(
                                            stationLevelModuleView.Module
                                                ._RepriseStationLevel[stationLevelModuleView._numeroReprise - 1]
                                                ._Parameters._Tolerance * 1000, 2).ToString("F2",
                                            CultureInfo.CreateSpecificCulture("en-US")), lastSaved),
                                    R.Level_Reprise_9);
                                break;
                            case 10:
                                stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(
                                        R.StringLevel_RepeatButton10,
                                        Math.Round(
                                            stationLevelModuleView.Module
                                                ._RepriseStationLevel[stationLevelModuleView._numeroReprise - 1]
                                                ._Parameters._Tolerance * 1000, 2).ToString("F2",
                                            CultureInfo.CreateSpecificCulture("en-US")), lastSaved),
                                    R.Level_Reprise_10);
                                break;
                            default:
                                stationLevelModuleView.bigbuttonTop.SetAttributes(string.Format(
                                        R.StringLevel_RepeatButton0,
                                        Math.Round(
                                            stationLevelModuleView.Module
                                                ._RepriseStationLevel[stationLevelModuleView._numeroReprise - 1]
                                                ._Parameters._Tolerance * 1000, 2).ToString("F2",
                                            CultureInfo.CreateSpecificCulture("en-US")), lastSaved),
                                    R.Level_Reprise_0);
                                break;
                        }

                        break;
                    default:
                        break;
                }
            }
        }

    }
}
