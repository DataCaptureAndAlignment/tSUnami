﻿using System;
using System.Xml.Serialization;
using I = TSU.Common.Instruments;

namespace TSU.Level
{
    public partial class Station
    {

        [Serializable]
        [XmlType(TypeName = "Level.Station.Parameters")]
        public class Parameters : Common.Station.Parameters
        //pour les stations de nivellement
        {
            public virtual int _IsCollimationAngleUnknown { get; set; } //bol: Identifies if the collimation angle is an "unknown value" to be determined in the adjustment
            public virtual double _CollimationAngle { get; set; } //flt: Collimation angle value for the instrument [gon]
            public virtual double _InstrumentHeight { get; set; } //flt: Management.Instrument height [m]
            public virtual double _InstrumentHeightDeviation { get; set; } //flt: Standard deviation of the instrument height [mm]
            public virtual double _InstrumentCenteringDeviation { get; set; } //flt: Standard deviation of the instrument centering [mm]
            public virtual double _Tolerance { get; set; } //tolérance pour l'alignement des éléments
            public virtual double _HStation { get; set; } //hauteur calculée de la station de nivellement
            public virtual double _EmqCala { get; set; } //emq calculé de la station sur les points de cala
            public virtual double _EmqAller { get; set; } //emq calculé de la station par rapport à l'aller
            public virtual double _EmqRetour { get; set; } //emq calculé de la station par rapport au retour
            public virtual bool _ShowLGC2Files { get; set; } //montre le fichier résultat et input lors calcul LGC2
            public virtual bool _UseDistanceForLGC2 { get; set; } //utilise les distances horizontales lors du calcul LGC2 (DLEV)
            public virtual ENUM.CoordinatesType _ZType { get; set; } // type de coordonnée à utiliser pour le calcul du H station
            public virtual ENUM.LevelingDirection _LevelingDirection { get; set; } //sens du nivellement pour la station
            public virtual bool HStationLocked { get; set; } //bloque la hauteur station calculée
            public virtual bool showObsoleteMeas { get; set; } //Affiche ou pas les mesures obsoletes
            public virtual double _Temperature { get; set; } // température de mesure
            private double na { get; set; } = Tsunami2.Preferences.Values.na;

            [XmlIgnore]
            public new I.Level _Instrument
            {
                get
                {
                    return base._Instrument as I.Level;
                }
                set
                {
                    base._Instrument = value;
                }
            }
            public Parameters()
                : base()
            //Constructeurs
            {
                this._CollimationAngle = 0;
                this._IsCollimationAngleUnknown = 1;
                this._InstrumentHeight = 0;
                this._InstrumentHeightDeviation = 0;
                this._InstrumentCenteringDeviation = 0;
                this._Tolerance =TSU.Tsunami2.Preferences.Values.Tolerances.Alignment_Radial_mm / 1000;
                this._HStation = na;
                this._ShowLGC2Files = false;
                this._UseDistanceForLGC2 = false;
                this._EmqCala = na;
                this._EmqAller = na;
                this._EmqRetour = na;
                this._ZType = ENUM.CoordinatesType.CCS;
                this._LevelingDirection = ENUM.LevelingDirection.ALLER;
                this._Date = DateTime.Now;
                this._Team = base._Team;
                this._Operation = base._Operation;
                this._IsSetup = false;
                this._Instrument = new I.Level();
                this._State = new State.Opening();
                this.HStationLocked = false;
                this._GeodeDatFilePath = "";
                this.showObsoleteMeas = false;
                this._Temperature = Tsunami2.Preferences.Values.GuiPrefs.ReferenceEcartoTemperature;
            }
        }

    }
}
