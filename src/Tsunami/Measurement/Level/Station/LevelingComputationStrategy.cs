﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using TSU.Common.Compute.Compensations;
using TSU.ENUM;
using E = TSU.Common.Elements;
using M = TSU.Common.Measures;

namespace TSU.Level
{
    public partial class Station
    {
        [Serializable]
        [XmlInclude(typeof(SFBStrategy))]
        [XmlInclude(typeof(LGC2Strategy))]
        public abstract class LevelingComputationStrategy
        {
            public string _name { get; set; }

            internal abstract Result UpdateTheoReadingAndDeviation(Module stationLevelModule);
        }

        [Serializable]
        [XmlType("SFBLevelingComputationStrategy")]
        public class SFBStrategy : LevelingComputationStrategy
        {
            public SFBStrategy()
            {
                _name = "SFB";
            }
            internal override Result UpdateTheoReadingAndDeviation(Module stationLevelModule)
            //Calcule la station nivellement et met à jour les écarts au théorique
            {
                Result result = new Result();
                stationLevelModule._WorkingStationLevel._Parameters._HStation = AverageStationHeightCalculation(stationLevelModule);
                stationLevelModule._WorkingStationLevel.ParametersBasic._LGCInputFilePath = "";
                stationLevelModule._WorkingStationLevel.ParametersBasic._LGCOutputFilePath = "";
                stationLevelModule._WorkingStationLevel._Parameters._LGCInputFilePath = "";
                stationLevelModule._WorkingStationLevel._Parameters._LGCOutputFilePath = "";
                stationLevelModule.TheoReadingAndResidualsCalculation();

                result.Success = true;
                stationLevelModule._WorkingStationLevel._Parameters._State = new State.SFBcalculationSuccessfull();
                return result;
            }

            private static double AverageStationHeightCalculation(Module stationLevelModule)
            //calcule le H moyen de la station
            {
                Debug.WriteInConsole($"Begin AverageStationHeightCalculation for {stationLevelModule._Name}");

                double averageStationHeight = 0;
                List<double> stationHeights = new List<double>();

                // In Smart Levellimg, it's possible to have no cala point
                // So we determine the station by a mean of all points measured both in the current station and a previous one
                if (stationLevelModule.ParentModule is Smart.Module sm)
                {
                    // Previous measurements
                    Dictionary<string, double> hmesForPoint = stationLevelModule.GetMeanHMes(sm);

                    // Valid measurements in current station
                    foreach (M.MeasureOfLevel m in stationLevelModule._WorkingStationLevel._MeasureOfLevel)
                        if (!(m._Status is M.States.Bad)
                            && m._RawLevelReading != stationLevelModule.na
                            && hmesForPoint.ContainsKey(m._PointName))
                        {
                            string pointName = m._PointName;
                            double hMesForPoint = hmesForPoint[pointName];
                            double rawReading = m._RawLevelReading;
                            double corrReading = m._CorrLevelReadingForEtalonnage;
                            double extension = m._Extension;
                            double hStationFromPoint = hMesForPoint + corrReading + extension;
                            Debug.WriteInConsole($"pointName={pointName}, hmesForPoint={hMesForPoint}, rawReading={rawReading}, corrReading={corrReading}, extension={extension}, hStationFromPoint={hStationFromPoint}");
                            stationHeights.Add(hStationFromPoint);
                        }
                }

                if(stationHeights.Count == 0)
                    foreach (M.MeasureOfLevel meas in stationLevelModule._WorkingStationLevel._MeasureOfLevel)
                    {
                        if (meas._Point.LGCFixOption == LgcPointOption.CALA && !(meas._Status is M.States.Bad))
                        {
                            double h;
                            E.Point ptTheo = stationLevelModule._TheoPoint.Find(x => x._Name == meas._PointName);
                            switch (stationLevelModule._WorkingStationLevel._Parameters._ZType)
                            {
                                case CoordinatesType.CCS:
                                    h = ptTheo._Coordinates.Ccs.Z.Value;
                                    break;
                                case CoordinatesType.SU:
                                    h = ptTheo._Coordinates.Su.Z.Value;
                                    break;
                                case CoordinatesType.UserDefined:
                                    h = ptTheo._Coordinates.UserDefined.Z.Value;
                                    break;
                                default:
                                    h = ptTheo._Coordinates.Ccs.Z.Value;
                                    break;
                            }
                            stationHeights.Add(h + meas._CorrLevelReadingForEtalonnage + meas._Extension);
                        }
                    }


                foreach (double item in stationHeights)
                {
                    averageStationHeight += item;
                }

                double stationHeight = averageStationHeight / stationHeights.Count;
                Debug.WriteInConsole($"End AverageStationHeightCalculation for {stationLevelModule._Name}, result={stationHeight}");

                return stationHeight;
            }
        }

        [Serializable]
        [XmlType("LGC2LevelingComputationStrategy")]
        public class LGC2Strategy : LevelingComputationStrategy
        {
            public LGC2Strategy()
            {
                _name = "LGC2";
            }
            internal override Result UpdateTheoReadingAndDeviation(Module stationLevelModule)
            //Calcule la station nivellement et met à jour les écarts au théorique avec LGC2
            {
                //Calcule la lecture theo et l'écart theo pour chaque point en utilisant LGC2.
                Result result = new Result();
                Lgc2 lgc2 = new Lgc2(stationLevelModule);
                lgc2.Run(
                    stationLevelModule._WorkingStationLevel._Parameters._ShowLGC2Files,
                    stationLevelModule._WorkingStationLevel._Parameters._ShowLGC2Files
                );
                if (lgc2._Runned)
                {
                    lgc2._Output.GetResultsForLevelling(stationLevelModule);
                    result.Success = true;
                    stationLevelModule._WorkingStationLevel._Parameters._State = new State.LGC2CalculationSuccessfull();
                }
                else
                {
                    result.Success = false;
                    stationLevelModule._WorkingStationLevel._Parameters._State = new State.LGC2CalculationFailed();
                }
                return result;
            }

        }

    }
}
