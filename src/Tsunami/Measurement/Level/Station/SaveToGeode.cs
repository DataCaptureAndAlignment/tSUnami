﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using TSU.ENUM;
using TSU.IO.SUSoft;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using R = TSU.Properties.Resources;

namespace TSU.Level
{
    public partial class Station
    {
        internal static class SaveToGeode
        {
            internal static double Na => Tsunami2.Preferences.Values.na;

            /// <summary>
            /// Crèe le répertoire Mesures si nécessaire
            /// </summary>
            /// <param name="filepath"></param>
            internal static void CreateDirectoryIfNecessary(string filepath)
            {
                FileInfo fi = new FileInfo(filepath);
                string dir = fi.DirectoryName;
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
            }

            /// <summary>
            /// Sauvegarde la station nivellement dans un fichier dat pour insertion dans Geode, au chemin indiqué
            /// </summary>
            /// <param name="stationLevelModule"></param>
            /// <param name="filePath"></param>
            /// <param name="stationName"></param>
            /// <returns></returns>
            internal static Result SaveDATLevelingStation(Module stationLevelModule, string filePath = null, string stationName = null, bool withEC = true, bool asBad = false)
            {
                // Default parameter values
                if (string.IsNullOrEmpty(filePath))
                    filePath = Tsunami2.Preferences.Values.Paths.MeasureOfTheDay
                             + Geode.GetFileName(stationLevelModule._WorkingStationLevel._Parameters, "LEVEL");
                if (string.IsNullOrEmpty(stationName))
                    stationName = stationLevelModule._WorkingStationLevel._Name;

                Result result = new Result();
                List<string> textToWriteInFile = new List<string>();
                int lineNumber = 1;
                List<string> existingNP = new List<string>();
                string lineText = "";
                List<int> lineNumbersAlreadyRecorded = new List<int>();

                CreateDirectoryIfNecessary(filePath);

                //lecture du fichier existant s'il existe
                try
                {
                    string[] readedTextInFile = File.ReadAllLines(filePath);
                    ///Ajoute la ligne RE en entête si inexistante
                    if (readedTextInFile.Length > 0)
                    {
                        if (!readedTextInFile[0].Contains(";RE;"))
                        {
                            AddLineRE(stationLevelModule, textToWriteInFile, ref lineNumber, ref lineText);
                        }
                    }
                    else
                    {
                        AddLineRE(stationLevelModule, textToWriteInFile, ref lineNumber, ref lineText);
                    }
                    foreach (string line in readedTextInFile)
                    {
                        if (line != "")
                        {
                            ///Lit les ligne NP et les ajoutes aux NP déjà existants
                            char splitChar = ';';
                            string[] lineSplit = line.Split(splitChar);
                            if (lineSplit != null
                                && lineSplit.Length == 14
                                && lineSplit[1].TrimStart(' ') == "NP")
                            {
                                string NP = lineSplit[2].Trim(' ') + "." + lineSplit[3].Trim(' ');
                                if (!existingNP.Contains(NP))
                                {
                                    existingNP.Add(NP);
                                }
                            }
                            textToWriteInFile.Add(line);
                            //int numberOfLine;
                            //incrémentation du numéro de ligne
                            //if (int.TryParse(line.Substring(0, 3), out numberOfLine))
                            //{
                            //    lineNumber = numberOfLine + 1;
                            //}
                            if (line.IndexOf(stationName) != -1)
                            {
                                lineNumbersAlreadyRecorded.Add(lineNumber);
                            }
                            lineNumber++;
                        }
                    }
                }
                catch (Exception e)
                //Exception générée si fichier inexistant
                {
                    AddLineRE(stationLevelModule, textToWriteInFile, ref lineNumber, ref lineText);
                    e.Data.Clear();
                }
                finally
                {
                    //Ecrit les nouveaux points avant la station
                    List<E.Point> createdByUserPoint = stationLevelModule._TheoPoint.FindAll(x => x._Origin == R.T_CREATED_BY_USER);
                    if (createdByUserPoint != null)
                    {
                        foreach (E.Point item in createdByUserPoint)
                        {
                            //Il n'est pas possible de créer un point dans Geode qui n'est pas un pilier
                            if (!existingNP.Contains(item._Name) && item._Point == "")
                            {
                                E.Coordinates c;
                                {
                                    if (item._Coordinates.HasCcs)
                                    {
                                        c = item._Coordinates.Ccs;
                                    }
                                    else
                                    {
                                        c = item._Coordinates.Local;
                                    }
                                }
                                lineText = "";
                                lineText = string.Format("  {0}NP;", WriteLineNumber(lineNumber, lineText));
                                lineNumber++;
                                lineText += string.Format("{0,-10};", item.ZoneOrAcc);
                                lineText += string.Format("{0,-33}", item._ClassAndNumero + ".");
                                lineText += ";p;";
                                lineText += string.Format("{0,-10};", c.X.Value.ToString("F5", CultureInfo.CreateSpecificCulture("en-US")));//"0.00000;";
                                lineText += string.Format("{0,-10};", c.Y.Value.ToString("F5", CultureInfo.CreateSpecificCulture("en-US")));//"0.00000;";
                                lineText += string.Format("{0,-10};", c.Z.Value.ToString("F5", CultureInfo.CreateSpecificCulture("en-US")));//"400.00000;";
                                lineText += item.SocketCode.Id.ToString() + ";";
                                lineText += string.Format("{0,8};", Math.Round(item._Parameters.Cumul, 3).ToString("F3", CultureInfo.CreateSpecificCulture("en-US")));
                                //Date
                                lineText += WriteDate(stationLevelModule._WorkingStationLevel._Parameters._Date);
                                //Heure
                                lineText += string.Format("{0,-8};", DateTime.Now.ToString("T", CultureInfo.CreateSpecificCulture("fr-FR")));
                                lineText += ";";
                                textToWriteInFile.Add(lineText);
                            }
                        }
                    }
                    ///Met le contrôle zero mire si ce n'est pas déjà fait
                    if (!stationLevelModule.FinalModule.GetIsStaffZeroCheckExported()
                        && stationLevelModule.FinalModule.GetStaffUsedList().Find(x => x._zeroCorrectionReading != Na) != null)
                    {
                        AddStaffZeroCheck(stationLevelModule, textToWriteInFile, ref lineNumber);
                    }
                    ///Met le contrôle niveau si ce n'est pas déjà fait
                    I.Level level = stationLevelModule.FinalModule.GetLevelUsedList().Find(x => x._Name == stationLevelModule._WorkingStationLevel.ParametersBasic._Instrument._Name);
                    if (level != null)
                    {
                        if (!level._IsCollimExported & level._ErrorCollim != Na)
                        {
                            AddCtrlColimCheck(level, textToWriteInFile, ref lineNumber);
                        }
                    }
                    else
                    {
                        level = stationLevelModule._WorkingStationLevel.ParametersBasic._Instrument as I.Level;
                        if (!level._IsCollimExported & level._ErrorCollim != Na)
                        {
                            AddCtrlColimCheck(level, textToWriteInFile, ref lineNumber);
                        }
                    }
                    //Ecrit chaque level station dans le fichier
                    //Ecriture entête station
                    AddLineRefStation(textToWriteInFile, ref lineNumber, lineNumbersAlreadyRecorded, stationName);
                    //Ecriture nom station
                    AddLinePH(textToWriteInFile, ref lineNumber, stationLevelModule._WorkingStationLevel, asBad);
                    //Ecriture des mesures de niveau
                    foreach (M.MeasureOfLevel measLevel in stationLevelModule._WorkingStationLevel._MeasureOfLevel)
                        if (measLevel._RawLevelReading != Na)
                            AddLineLE(textToWriteInFile, ref lineNumber, stationLevelModule._WorkingStationLevel, measLevel, asBad);
                    //Ecriture des écarts sur les éléments
                    if (withEC)
                        foreach (M.MeasureOfLevel measLevel in stationLevelModule._WorkingStationLevel._MeasureOfLevel)
                        {
                            if (measLevel._RawLevelReading != Na 
                                && measLevel._TheoReading != Na 
                                && measLevel._Point.LGCFixOption != LgcPointOption.CALA)
                            {
                                AddLineEC(textToWriteInFile, ref lineNumber, measLevel);
                            }
                        }
                    File.WriteAllLines(filePath, textToWriteInFile.ToArray());
                    //Affichage du fichier enregistré et sélection de l'état de la station
                    if (File.Exists(filePath))
                    {
                        result.Success = true;
                        stationLevelModule.FinalModule.SetIsStaffZeroCheckExported(true);
                        if (level != null) level._IsCollimExported = true;
                        stationLevelModule._WorkingStationLevel.ParametersBasic.LastSaved = DateTime.Now;
                        stationLevelModule._WorkingStationLevel._Parameters._State = new State.StationLevelSaved();
                        stationLevelModule._AllerStationLevel._Parameters._GeodeDatFilePath = filePath;
                        stationLevelModule._AllerStationLevel.ParametersBasic._GeodeDatFilePath = filePath;
                        stationLevelModule._RetourStationLevel._Parameters._GeodeDatFilePath = filePath;
                        stationLevelModule._RetourStationLevel.ParametersBasic._GeodeDatFilePath = filePath;
                        stationLevelModule._WorkingStationLevel._Parameters._GeodeDatFilePath = filePath;
                        stationLevelModule._WorkingStationLevel.ParametersBasic._GeodeDatFilePath = filePath;
                        stationLevelModule._Station.ParametersBasic._GeodeDatFilePath = filePath;
                        foreach (Station st in stationLevelModule._RepriseStationLevel)
                        {
                            st.ParametersBasic._GeodeDatFilePath = filePath;
                            st._Parameters._GeodeDatFilePath = filePath;
                        }
                        //if (showMessageOfSuccess == true)
                        //{
                        //    stationLevelModule.View.ShowMessageOfValidation(String.Format(R.StringLevel_StationSaved, filepath), R.T_OK);

                        //} 
                        //Shell.Run(filepath);
                    }
                    else
                    {
                        result.Success = false;
                        result.ErrorMessage = R.StringLevel_DatNotCreated;
                        stationLevelModule._WorkingStationLevel._Parameters._State = new State.StationLevelReadyToBeSaved();
                        string titleAndMessage = string.Format(R.StringLevel_OutwardStationSaved, filePath);
                        new MessageInput(MessageType.Critical, titleAndMessage).Show();
                    }
                }
                return result;
            }

            private static void AddLineEC(List<string> textToWriteInFile, ref int lineNumber, M.MeasureOfLevel measLevel)
            {
                string lineText;
                ///Met un % devant les mesures bad
                if (measLevel._Status is M.States.Bad)
                {
                    lineText = "% ";
                }
                else
                {
                    lineText = "  ";
                }
                lineText = string.Format("{0}EC;", WriteLineNumber(lineNumber, lineText));
                lineNumber++;
                //cas d'un alésage
                if (measLevel._Point._Point != "")
                {
                    lineText += string.Format("{0,-10};", measLevel._Point._Accelerator);
                    lineText += string.Format("{0,-33};", measLevel._Point._ClassAndNumero + "." + measLevel._Point._Point);
                }
                //cas d'un pilier
                else
                {
                    lineText += string.Format("{0,-10};", measLevel._Point._Zone);
                    lineText += string.Format("{0,-33};", measLevel._Point._ClassAndNumero + ".");
                }
                lineText += string.Format("          ;          ;{0,10};          ;dr dl dh dRoll [mm]|[mrad];", Math.Round((measLevel._TheoReading - measLevel._RawLevelReading) * 1000, 2).ToString("F2", CultureInfo.CreateSpecificCulture("en-US")));
                textToWriteInFile.Add(lineText);
            }

            private static void AddLineLE(List<string> textToWriteInFile, ref int lineNumber, Station _WorkingStationLevel, M.MeasureOfLevel measLevel, bool asBad)
            {
                string lineText;
                // Met un % devant les mesures bad
                if (measLevel._Status is M.States.Bad || asBad)
                {
                    lineText = "% ";
                }
                else
                {
                    lineText = "  ";
                }
                //Ecriture de toutes les lectures
                lineText = string.Format("{0}LE;", WriteLineNumber(lineNumber, lineText));
                lineNumber++;
                lineText += string.Format("{0,-9};", measLevel._staff._Model);
                lineText += string.Format("{0,-12};", measLevel._staff._SerialNumber);
                if (measLevel._Point._Point != "")
                //cas d'un alésage
                {
                    lineText += string.Format("{0,-10};", measLevel._Point._Accelerator);
                    lineText += string.Format("{0,-33};", measLevel._Point._ClassAndNumero + "." + measLevel._Point._Point);
                }
                else
                //cas d'un pilier
                {
                    lineText += string.Format("{0,-10};", measLevel._Point._Zone);
                    lineText += string.Format("{0,-33};", measLevel._Point._ClassAndNumero + ".");
                }
                //lecture nivellement
                lineText += string.Format("{0,8};", Math.Round(measLevel._CorrLevelReadingForZero * 100000, 0).ToString("F0", CultureInfo.CreateSpecificCulture("en-US")));
                //rallonge
                lineText += string.Format("{0,8};;;;;;", Math.Round(measLevel._Extension, 5).ToString("F5", CultureInfo.CreateSpecificCulture("en-US")));
                switch (_WorkingStationLevel._Parameters._LevelingDirection)
                {
                    case LevelingDirection.ALLER:
                        lineText += string.Format("{0,1};", "A");
                        break;
                    case LevelingDirection.RETOUR:
                        lineText += string.Format("{0,1};", "R");
                        break;
                    case LevelingDirection.REPRISE:
                        lineText += string.Format("{0,1};", "P");
                        break;
                    default:
                        lineText += string.Format("{0,1};", "A");
                        break;
                }
                //Distance et boussole pour les DNA03 et LS15
                string distance;
                string compass;
                switch (_WorkingStationLevel._Parameters._Instrument._levelType)
                {
                    case I.LevelType.DNA03:
                    case I.LevelType.LS15:
                        distance = Math.Round(measLevel._Distance, 4).ToString("F3", CultureInfo.CreateSpecificCulture("en-US"));
                        compass = Math.Round(measLevel._Compass, 4).ToString("F3", CultureInfo.CreateSpecificCulture("en-US"));
                        break;
                    default:
                        distance = string.Empty;
                        compass = string.Empty;
                        break;
                }
                lineText += string.Format("{0,8};{1,8};;", distance, compass);
                // Rôle
                if (measLevel._Point.LGCFixOption == LgcPointOption.CALA) lineText += "R;;";
                else lineText += "A;;";
                //date
                lineText += string.Format("{0,-8};", measLevel._Date.ToString("T", CultureInfo.CreateSpecificCulture("fr-FR")));
                //commentaire + emq si retour ou reprise
                switch (_WorkingStationLevel._Parameters._LevelingDirection)
                {
                    case LevelingDirection.ALLER:
                        if (measLevel._CorrLevelReadingForZero != measLevel._RawLevelReading)
                        {
                            lineText += string.Format("raw= {0} ", Math.Round(measLevel._RawLevelReading * 100000, 0).ToString("F0", CultureInfo.CreateSpecificCulture("en-US")));
                        }
                        lineText += string.Format("{0};", measLevel.Comment);
                        break;
                    case LevelingDirection.RETOUR:
                        if (measLevel._CorrLevelReadingForZero != measLevel._RawLevelReading)
                        {
                            lineText += string.Format("raw= {0} ", Math.Round(measLevel._RawLevelReading * 100000, 0).ToString("F0", CultureInfo.CreateSpecificCulture("en-US")));
                        }
                        if (_WorkingStationLevel._Parameters._EmqAller != Na)
                        {
                            lineText += string.Format("emqA= {0} ", Math.Round(_WorkingStationLevel._Parameters._EmqAller * 100000, 1).ToString("F1", CultureInfo.CreateSpecificCulture("en-US")));
                        }
                        lineText += string.Format("{0};", measLevel.Comment);
                        break;
                    case LevelingDirection.REPRISE:
                        if (measLevel._CorrLevelReadingForZero != measLevel._RawLevelReading)
                        {
                            lineText += string.Format("raw= {0} ", Math.Round(measLevel._RawLevelReading * 100000, 0).ToString("F0", CultureInfo.CreateSpecificCulture("en-US")));
                        }
                        if (_WorkingStationLevel._Parameters._EmqAller != Na)
                        {
                            lineText += string.Format("emqA= {0} ", Math.Round(_WorkingStationLevel._Parameters._EmqAller * 100000, 1).ToString("F1", CultureInfo.CreateSpecificCulture("en-US")));
                        }
                        if (_WorkingStationLevel._Parameters._EmqRetour != Na)
                        {
                            lineText += string.Format("emqR= {0} ", Math.Round(_WorkingStationLevel._Parameters._EmqRetour * 100000, 1).ToString("F1", CultureInfo.CreateSpecificCulture("en-US")));
                        }
                        lineText += string.Format("{0};", measLevel.Comment);
                        break;
                    default:
                        break;
                }

                // interfaces
                var interfaces = measLevel.Interfaces;
                lineText += interfaces.ToString(1, ';');
                lineText += interfaces.ToString(2, ';');
                lineText += interfaces.ToString(3, ';');

                textToWriteInFile.Add(lineText);
            }

            private static void AddLinePH(List<string> textToWriteInFile, ref int lineNumber, Station _WorkingStationLevel, bool asBad)
            {
                string lineText = asBad ? "% " : "  ";
                lineText = string.Format("{0}PH;", WriteLineNumber(lineNumber, lineText));
                lineNumber++;
                lineText += string.Format("{0,-9};", _WorkingStationLevel._Parameters._Instrument._Model);
                lineText += string.Format("{0,-12};", _WorkingStationLevel._Parameters._Instrument._SerialNumber);
                lineText += string.Format(";;;;;;{0, -7};;;;;;", Math.Round(_WorkingStationLevel._Parameters._Temperature, 1).ToString("F1", CultureInfo.CreateSpecificCulture("en-US")));
                textToWriteInFile.Add(lineText);
            }

            private static void AddCtrlColimCheck(I.Level level, List<string> textToWriteInFile, ref int lineNumber)
            {
                string lineText = "";
                ///Titre
                lineText = string.Format("! {0}----------", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += string.Format("Level {0} collimation check done at {1,-8}", level._Name, level._DateCollim.ToString("T", CultureInfo.CreateSpecificCulture("fr-FR")));
                lineText += "----------";
                textToWriteInFile.Add(lineText);
                //Dessin ligne début entête colonne
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += " _______________________";
                textToWriteInFile.Add(lineText);
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += " |          |          |";
                textToWriteInFile.Add(lineText);
                ///ENtête colonnes
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += string.Format(" |{0,-10}|{1,10}|", "Staff pos ", "  Reading ");
                textToWriteInFile.Add(lineText);
                //Entête colonnes unités
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += string.Format(" |          |{0,10}|", "(1/100 mm)");
                textToWriteInFile.Add(lineText);
                //Dessin ligne fin entête colonne
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += " |----------|----------|";
                textToWriteInFile.Add(lineText);
                //Ajout LA1
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += string.Format(" |    LA1   |{0,-10}|", Math.Round(level._LA1 * 100000, 0).ToString("F0", CultureInfo.CreateSpecificCulture("en-US")));
                textToWriteInFile.Add(lineText);
                //Ajout LB1
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += string.Format(" |    LB1   |{0,-10}|", Math.Round(level._LB1 * 100000, 0).ToString("F0", CultureInfo.CreateSpecificCulture("en-US")));
                textToWriteInFile.Add(lineText);
                //Ajout LB2
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += string.Format(" |    LB2   |{0,-10}|", Math.Round(level._LB2 * 100000, 0).ToString("F0", CultureInfo.CreateSpecificCulture("en-US")));
                textToWriteInFile.Add(lineText);
                //Ajout LA2
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += string.Format(" |    LA2   |{0,-10}|", Math.Round(level._LA2 * 100000, 0).ToString("F0", CultureInfo.CreateSpecificCulture("en-US")));
                textToWriteInFile.Add(lineText);
                //Dessin ligne séparation error
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += " |----------|----------|";
                textToWriteInFile.Add(lineText);
                ///Ligne error
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += string.Format(" |{0,-10}|{1,-10}|",
                    "   Error  ",
                    Math.Round(level._ErrorCollim * 100000, 1).ToString("F1", CultureInfo.CreateSpecificCulture("en-US")));
                textToWriteInFile.Add(lineText);
                //Dessin ligne séparation distance
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += " |----------|----------|";
                textToWriteInFile.Add(lineText);
                //Ajout Distance
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += string.Format(" | Distance |{0,-10}|", Math.Round(level._DistCollim, 1).ToString("F1", CultureInfo.CreateSpecificCulture("en-US")) + " m");
                textToWriteInFile.Add(lineText);
                //Dessin ligne fin Tableau
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += " |__________|__________|";
                textToWriteInFile.Add(lineText);
            }

            /// <summary>
            /// Ajoute la ligne RE en entête fichier
            /// </summary>
            /// <param name="stationLevelModule"></param>
            /// <param name="textToWriteInFile"></param>
            /// <param name="lineNumber"></param>
            /// <param name="lineText"></param>
            private static void AddLineRE(Module stationLevelModule, List<string> textToWriteInFile, ref int lineNumber, ref string lineText)
            {
                //Ligne entête date + team + Opération seulement si fichier existe pas
                lineText = string.Format("  {0}RE;", WriteLineNumber(lineNumber, lineText));
                lineNumber++;
                lineText += WriteDate(stationLevelModule._WorkingStationLevel._Parameters._Date);
                lineText += string.Format("{0,-10};", stationLevelModule._WorkingStationLevel._Parameters._Team);
                lineText += string.Format("{0,6};", stationLevelModule._WorkingStationLevel._Parameters._Operation.value.ToString());
                lineText += $"Tsunami {Tsunami2.Properties.Version};";
                textToWriteInFile.Add(lineText);
            }

            /// <summary>
            /// Ecrit la ligne d'information début de station pour vérifier si station en double
            /// </summary>
            /// <param name="textToWriteInFile"></param>
            /// <param name="lineNumber"></param>
            /// <param name="lineNumberAlreadyRecorded"></param>
            /// <param name="stationName"></param>
            /// 
            private static void AddLineRefStation(List<string> textToWriteInFile, ref int lineNumber, List<int> lineNumberAlreadyRecorded, string stationName)
            {
                string lineText = "";
                lineText = string.Format("! {0}----------", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += string.Format("{0} Levelling recorded at ", stationName);
                lineText += string.Format("{0,-8}", DateTime.Now.ToString("T", CultureInfo.CreateSpecificCulture("fr-FR")));
                if (lineNumberAlreadyRecorded.Count != 0)
                {
                    int i = 1;
                    foreach (int item in lineNumberAlreadyRecorded)
                    {
                        if (i == 1) { lineText += string.Format(" ALREADY RECORDED AT LINE {0}", item); }
                        else { lineText += string.Format(", {0}", item); }
                        i++;
                    }
                }
                lineText += "----------";
                textToWriteInFile.Add(lineText);
            }

            /// <summary>
            /// AJoute le tableau du contrôle du zéro mire
            /// </summary>
            /// <param name="stationLevelModule"></param>
            /// <param name="textToWriteInFile"></param>
            /// <param name="lineNumber"></param>
            private static void AddStaffZeroCheck(Module stationLevelModule, List<string> textToWriteInFile, ref int lineNumber)
            {
                string lineText = "";
                ///Titre
                lineText = string.Format("! {0}----------", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += string.Format("Zero staff check done at {0,-8}", stationLevelModule.FinalModule.GettimeStaffZeroCheck().ToString("T", CultureInfo.CreateSpecificCulture("fr-FR")));
                lineText += "----------";
                textToWriteInFile.Add(lineText);
                //Dessin ligne début entête colonne
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += " __________________________________";
                textToWriteInFile.Add(lineText);
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += " |          |          |          |";
                textToWriteInFile.Add(lineText);
                ///ENtête colonnes
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += string.Format(" |{0,-10}|{1,10}|{2,10}|", "  Staff   ", "  Reading ", "Correction");
                textToWriteInFile.Add(lineText);
                //Entête colonnes unités
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += string.Format(" |          |{0,10}|{0,10}|", "(1/100 mm)");
                textToWriteInFile.Add(lineText);
                //Dessin ligne fin entête colonne
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += " |----------|----------|----------|";
                textToWriteInFile.Add(lineText);
                //Ajout des staff
                double average = 0;
                int number = 0;
                foreach (I.LevelingStaff item in stationLevelModule.FinalModule.GetStaffUsedList())
                {
                    if (item._zeroCorrectionReading != Na)
                    {
                        number++;
                        average += item._zeroCorrectionReading;
                        lineText = "";
                        lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                        lineText = lineText.Replace(';', ' ');
                        lineNumber++;
                        lineText += string.Format(" |{0,-10}|{1,10}|{2,10}|",
                            item._Name,
                            Math.Round(item._zeroCorrectionReading * 100000, 0).ToString("F0", CultureInfo.CreateSpecificCulture("en-US")),
                            Math.Round(item._zeroCorrection * 100000, 1).ToString("F1", CultureInfo.CreateSpecificCulture("en-US")));
                        textToWriteInFile.Add(lineText);
                    }
                }
                if (number != 0) average = average / number;
                //Dessin ligne séparation average
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += " |----------|----------|----------|";
                textToWriteInFile.Add(lineText);
                ///Ligne average
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += string.Format(" |{0,-10}|{1,10}|          |",
                    "  Average ",
                    Math.Round(average * 100000, 1).ToString("F1", CultureInfo.CreateSpecificCulture("en-US")));
                textToWriteInFile.Add(lineText);
                //Dessin ligne fin Tableau
                lineText = "";
                lineText = string.Format("! {0} ", WriteLineNumber(lineNumber, lineText));
                lineText = lineText.Replace(';', ' ');
                lineNumber++;
                lineText += " |__________|__________|__________|";
                textToWriteInFile.Add(lineText);
            }

            /// <summary>
            /// Ecrit la date dans l'entête pour le fichier txt pour Geode
            /// </summary>
            /// <param name="date"></param>
            /// <returns></returns>
            private static string WriteDate(DateTime date)
            {
                //jour
                string dateText = date.Day.ToString() + "-";
                switch (date.Month)
                {
                    //Choix du mois
                    case 1:
                        dateText = dateText + "Jan-";
                        break;
                    case 2:
                        dateText = dateText + "Feb-";
                        break;
                    case 3:
                        dateText = dateText + "Mar-";
                        break;
                    case 4:
                        dateText = dateText + "Apr-";
                        break;
                    case 5:
                        dateText = dateText + "May-";
                        break;
                    case 6:
                        dateText = dateText + "Jun-";
                        break;
                    case 7:
                        dateText = dateText + "Jul-";
                        break;
                    case 8:
                        dateText = dateText + "Aug-";
                        break;
                    case 9:
                        dateText = dateText + "Sep-";
                        break;
                    case 10:
                        dateText = dateText + "Oct-";
                        break;
                    case 11:
                        dateText = dateText + "Nov-";
                        break;
                    case 12:
                        dateText = dateText + "Dec-";
                        break;
                    default:
                        dateText = dateText + "Jan-";
                        break;
                }
                //année
                return dateText + date.Year + ";";
            }

            /// <summary>
            /// écrit le numéro de ligne dans le fichier txt pour Geode
            /// </summary>
            /// <param name="lineNumber"></param>
            /// <param name="lineText"></param>
            /// <returns></returns>
            private static string WriteLineNumber(int lineNumber, string lineText)
            {
                return $"{lineText}{lineNumber:D3};";
            }
        }
    }
}