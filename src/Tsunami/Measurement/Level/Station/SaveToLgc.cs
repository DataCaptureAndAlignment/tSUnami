﻿using System;
using System.Collections.Generic;
using TSU.Common.Compute.Compensations;
using TSU.ENUM;
using TSU.Views.Message;
using R = TSU.Properties.Resources;

namespace TSU.Level
{
    public partial class Station
    {
        internal static class SaveToLgc
        {
            /// <summary>
            /// Sauvegarde la mesure au format LGC inp dans C:\data\mesures\
            /// </summary>
            /// <param name="stationLevelModule"></param>
            /// 
            /// <returns></returns>
            internal static Result SaveLGCFileLevelingStation(Module stationLevelModule)
            {
                Result result = new Result();
                string filepath = string.Format("{0}{1}_{2}_{3}_{4}.inp", Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, stationLevelModule._WorkingStationLevel._Name, DateTime.Now.Day.ToString("00"), DateTime.Now.Month.ToString("00"), (DateTime.Now.Year - 2000).ToString());
                if (!System.IO.Directory.Exists(Tsunami2.Preferences.Values.Paths.MeasureOfTheDay)) { System.IO.Directory.CreateDirectory(Tsunami2.Preferences.Values.Paths.MeasureOfTheDay); }
                if (System.IO.File.Exists(filepath))
                {
                    string respond = R.T_OK;
                    if (stationLevelModule.View.moduleType == ModuleType.Advanced)
                    {
                        string titleAndMessage = String.Format(R.StringLevel_LGCReplaceFile, filepath);
                        MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                        {
                            ButtonTexts = new List<string> { R.T_OK, R.T_DONT }
                        };
                        respond = mi.Show().TextOfButtonClicked;
                    }
                    if (respond == R.T_DONT)
                    {
                        result.Success = false;
                        return result;
                    }
                }
                try
                {
                    new Lgc2(stationLevelModule, filepath);
                    if (System.IO.File.Exists(filepath))
                    {
                        result.Success = true;
                        stationLevelModule._WorkingStationLevel._Parameters._LGCSaveFilePath = filepath;
                        stationLevelModule._WorkingStationLevel.ParametersBasic._LGCSaveFilePath = filepath;
                        stationLevelModule._Station.ParametersBasic._LGCSaveFilePath = filepath;
                        //if (showMessageOfSuccess == true)
                        //{
                        //    stationLevelModule.View.ShowMessageOfValidation(String.Format(R.StringLevel_StationSaved, filepath), R.T_OK);

                        //}
                    }
                    else
                    {
                        result.Success = false;
                        result.ErrorMessage = R.StringLevel_LGCInpNotCreated;
                        stationLevelModule._WorkingStationLevel._Parameters._State = new State.StationLevelReadyToBeSaved();
                        string titleAndMessage = String.Format(R.StringLevel_LGCInpNotCreated, filepath);
                        new MessageInput(MessageType.Critical, titleAndMessage)
                        {
                            ButtonTexts = new List<string> { R.T_OK + "!" }
                        }.Show();
                    }
                    result.Success = true;
                    return result;
                }
                catch (Exception e)
                //Exception générée si fichier inexistant
                {
                    result.Success = false;
                    result.ErrorMessage = e.Message;
                    e.Data.Clear();
                    return result;
                }
            }
        }
    }
}