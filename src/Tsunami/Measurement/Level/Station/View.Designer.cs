﻿
using System;
using System.Windows.Forms;
using TSU.Common;
using R = TSU.Properties.Resources;

namespace TSU.Level
{
    public partial class Station
    {
        partial class View
        {
            /// <summary>
            /// Required designer variable.
            /// </summary>
            private System.ComponentModel.IContainer components = null;

            /// <summary>
            /// Clean up any resources being used.
            /// </summary>
            /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }

            #region Windows Form Designer generated code

            /// <summary>
            /// Required method for Designer support - do not modify
            /// the contents of this method with the code editor.
            /// </summary>
            private void InitializeComponent()
            {
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();

                this.splitContainer1 = new System.Windows.Forms.SplitContainer();
                Tsunami2.Preferences.Theme.ApplyTo(splitContainer1);

                this.splitContainerNivel = new System.Windows.Forms.SplitContainer();
                Tsunami2.Preferences.Theme.ApplyTo(splitContainerNivel);

                this.treeView_Parameters = new System.Windows.Forms.TreeView();
                this.panel_AlignWindow = new System.Windows.Forms.Panel();

                this.splitContainer_AlignWindow = new System.Windows.Forms.SplitContainer();
                Tsunami2.Preferences.Theme.ApplyTo(splitContainer_AlignWindow);

                this.splitContainer_AlignText = new System.Windows.Forms.SplitContainer();
                Tsunami2.Preferences.Theme.ApplyTo(splitContainer_AlignText);

                this.textBox_PointAlign = new System.Windows.Forms.TextBox();
                this.textBox_PointMovement = new System.Windows.Forms.TextBox();
                this.splitContainer_ProgressBar = new System.Windows.Forms.SplitContainer();
                this.progressBar_TooHigh = new TSU.Views.VerticalAlignmentProgressBar();
                this.progressBar_TooLow = new TSU.Views.VerticalAlignmentProgressBar();
                this.dataGridViewLevel = new System.Windows.Forms.DataGridView();
                this.LeveldataGridViewDelete = new System.Windows.Forms.DataGridViewImageColumn();
                this.LeveldataGridViewName = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewCala = new System.Windows.Forms.DataGridViewImageColumn();
                this.LeveldataGridViewUse = new System.Windows.Forms.DataGridViewImageColumn();
                this.LevelDataGridViewDcum = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewH = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewHmes = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewDist = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewRall = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewRawMeas = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewCorrMeas = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewTheo = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewDepla = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewObj = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewMove = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewResidusAller = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewResidusRetour = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewStaff = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewInterfaces = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewComment = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.LeveldataGridViewTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this._PanelBottom.SuspendLayout();
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
                this.splitContainer1.Panel1.SuspendLayout();
                this.splitContainer1.SuspendLayout();
                ((System.ComponentModel.ISupportInitialize)(this.splitContainerNivel)).BeginInit();
                this.splitContainerNivel.Panel1.SuspendLayout();
                this.splitContainerNivel.Panel2.SuspendLayout();
                this.splitContainerNivel.SuspendLayout();
                this.panel_AlignWindow.SuspendLayout();
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer_AlignWindow)).BeginInit();
                this.splitContainer_AlignWindow.Panel1.SuspendLayout();
                this.splitContainer_AlignWindow.Panel2.SuspendLayout();
                this.splitContainer_AlignWindow.SuspendLayout();
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer_AlignText)).BeginInit();
                this.splitContainer_AlignText.Panel1.SuspendLayout();
                this.splitContainer_AlignText.Panel2.SuspendLayout();
                this.splitContainer_AlignText.SuspendLayout();
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer_ProgressBar)).BeginInit();
                this.splitContainer_ProgressBar.Panel1.SuspendLayout();
                this.splitContainer_ProgressBar.Panel2.SuspendLayout();
                this.splitContainer_ProgressBar.SuspendLayout();
                ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLevel)).BeginInit();
                this.SuspendLayout();
                // 
                // instrumentView
                // 
                this.instrumentView.Location = new System.Drawing.Point(182, 182);
                // 
                // _PanelBottom
                // 
                this._PanelBottom.Controls.Add(this.splitContainer1);
                this._PanelBottom.Location = new System.Drawing.Point(5, 84);
                this._PanelBottom.MinimumSize = new System.Drawing.Size(0, 120);
                this._PanelBottom.Size = new System.Drawing.Size(1202, 647);
                // 
                // _PanelTop
                // 
                this._PanelTop.Size = new System.Drawing.Size(1202, 79);
                // 
                // splitContainer1
                // 
                this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
                this.splitContainer1.Location = new System.Drawing.Point(0, 0);
                this.splitContainer1.Name = "splitContainer1";
                this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
                // 
                // splitContainer1.Panel1
                // 
                this.splitContainer1.Panel1.Controls.Add(this.splitContainerNivel);
                // 
                // splitContainer1.Panel2
                // 
                this.splitContainer1.Panel2.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
                this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
                this.splitContainer1.Panel2MinSize = 240;
                this.splitContainer1.Size = new System.Drawing.Size(1202, 647);
                this.splitContainer1.SplitterDistance = 403;
                this.splitContainer1.TabIndex = 1;
                this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
                // 
                // splitContainerNivel
                // 
                this.splitContainerNivel.Dock = System.Windows.Forms.DockStyle.Fill;
                this.splitContainerNivel.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
                this.splitContainerNivel.Location = new System.Drawing.Point(0, 0);
                this.splitContainerNivel.Name = "splitContainerNivel";
                this.splitContainerNivel.Orientation = System.Windows.Forms.Orientation.Horizontal;
                // 
                // splitContainerNivel.Panel1
                // 
                this.splitContainerNivel.Panel1.Controls.Add(this.treeView_Parameters);
                // 
                // splitContainerNivel.Panel2
                // 
                this.splitContainerNivel.Panel2.Controls.Add(this.panel_AlignWindow);
                this.splitContainerNivel.Panel2.Controls.Add(this.dataGridViewLevel);
                this.splitContainerNivel.Size = new System.Drawing.Size(1202, 403);
                this.splitContainerNivel.SplitterDistance = 186;
                this.splitContainerNivel.TabIndex = 0;
                // 
                // treeView_Parameters
                // 
                this.treeView_Parameters.AllowDrop = true;
                this.treeView_Parameters.Dock = System.Windows.Forms.DockStyle.Fill;
                this.treeView_Parameters.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.treeView_Parameters.ItemHeight = 32;
                this.treeView_Parameters.Location = new System.Drawing.Point(0, 0);
                this.treeView_Parameters.Name = "treeView_Parameters";
                this.treeView_Parameters.ShowNodeToolTips = true;
                this.treeView_Parameters.Size = new System.Drawing.Size(1202, 186);
                this.treeView_Parameters.TabIndex = 0;
                this.treeView_Parameters.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView_Parameters_NodeMouseClick);
                this.treeView_Parameters.MouseClick += new System.Windows.Forms.MouseEventHandler(this.treeView_Parameters_MouseClick);
                // 
                // panel_AlignWindow
                // 
                this.panel_AlignWindow.Controls.Add(this.splitContainer_AlignWindow);
                this.panel_AlignWindow.Dock = System.Windows.Forms.DockStyle.Fill;
                this.panel_AlignWindow.Location = new System.Drawing.Point(0, 0);
                this.panel_AlignWindow.Name = "panel_AlignWindow";
                this.panel_AlignWindow.Size = new System.Drawing.Size(1202, 213);
                this.panel_AlignWindow.TabIndex = 19;
                this.panel_AlignWindow.Visible = false;
                // 
                // splitContainer_AlignWindow
                // 
                this.splitContainer_AlignWindow.Dock = System.Windows.Forms.DockStyle.Fill;
                this.splitContainer_AlignWindow.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
                this.splitContainer_AlignWindow.IsSplitterFixed = true;
                this.splitContainer_AlignWindow.Location = new System.Drawing.Point(0, 0);
                this.splitContainer_AlignWindow.Name = "splitContainer_AlignWindow";
                // 
                // splitContainer_AlignWindow.Panel1
                // 
                this.splitContainer_AlignWindow.Panel1.Controls.Add(this.splitContainer_AlignText);
                // 
                // splitContainer_AlignWindow.Panel2
                // 
                this.splitContainer_AlignWindow.Panel2.Controls.Add(this.splitContainer_ProgressBar);
                this.splitContainer_AlignWindow.Size = new System.Drawing.Size(1202, 213);
                this.splitContainer_AlignWindow.SplitterDistance = 1057;
                this.splitContainer_AlignWindow.TabIndex = 0;
                // 
                // splitContainer_AlignText
                // 
                this.splitContainer_AlignText.Dock = System.Windows.Forms.DockStyle.Fill;
                this.splitContainer_AlignText.IsSplitterFixed = true;
                this.splitContainer_AlignText.Location = new System.Drawing.Point(0, 0);
                this.splitContainer_AlignText.Name = "splitContainer_AlignText";
                this.splitContainer_AlignText.Orientation = System.Windows.Forms.Orientation.Horizontal;
                // 
                // splitContainer_AlignText.Panel1
                // 
                this.splitContainer_AlignText.Panel1.Controls.Add(this.textBox_PointAlign);
                this.splitContainer_AlignText.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer_AlignText_Panel1_Paint);
                // 
                // splitContainer_AlignText.Panel2
                // 
                this.splitContainer_AlignText.Panel2.Controls.Add(this.textBox_PointMovement);
                this.splitContainer_AlignText.Size = new System.Drawing.Size(1057, 213);
                this.splitContainer_AlignText.SplitterDistance = 106;
                this.splitContainer_AlignText.TabIndex = 2;
                // 
                // textBox_PointAlign
                // 
                this.textBox_PointAlign.BorderStyle = System.Windows.Forms.BorderStyle.None;
                this.textBox_PointAlign.Dock = System.Windows.Forms.DockStyle.Bottom;
                this.textBox_PointAlign.Font = new System.Drawing.Font("Tahoma", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.textBox_PointAlign.Location = new System.Drawing.Point(0, -10);
                this.textBox_PointAlign.Name = "textBox_PointAlign";
                this.textBox_PointAlign.ReadOnly = true;
                this.textBox_PointAlign.Size = new System.Drawing.Size(1057, 116);
                this.textBox_PointAlign.TabIndex = 0;
                this.textBox_PointAlign.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
                // 
                // textBox_PointMovement
                // 
                this.textBox_PointMovement.BorderStyle = System.Windows.Forms.BorderStyle.None;
                this.textBox_PointMovement.Dock = System.Windows.Forms.DockStyle.Top;
                this.textBox_PointMovement.Font = new System.Drawing.Font("Tahoma", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.textBox_PointMovement.Location = new System.Drawing.Point(0, 0);
                this.textBox_PointMovement.Name = "textBox_PointMovement";
                this.textBox_PointMovement.ReadOnly = true;
                this.textBox_PointMovement.Size = new System.Drawing.Size(1057, 116);
                this.textBox_PointMovement.TabIndex = 1;
                this.textBox_PointMovement.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
                // 
                // splitContainer_ProgressBar
                // 
                this.splitContainer_ProgressBar.Dock = System.Windows.Forms.DockStyle.Fill;
                this.splitContainer_ProgressBar.Location = new System.Drawing.Point(0, 0);
                this.splitContainer_ProgressBar.Name = "splitContainer_ProgressBar";
                this.splitContainer_ProgressBar.Orientation = System.Windows.Forms.Orientation.Horizontal;
                // 
                // splitContainer_ProgressBar.Panel1
                // 
                this.splitContainer_ProgressBar.Panel1.Controls.Add(this.progressBar_TooHigh);
                // 
                // splitContainer_ProgressBar.Panel2
                // 
                this.splitContainer_ProgressBar.Panel2.Controls.Add(this.progressBar_TooLow);
                this.splitContainer_ProgressBar.Size = new System.Drawing.Size(141, 213);
                this.splitContainer_ProgressBar.SplitterDistance = 104;
                this.splitContainer_ProgressBar.TabIndex = 0;
                // 
                // progressBar_TooHigh
                // 
                this.progressBar_TooHigh._doubleValue = 0D;
                this.progressBar_TooHigh._tolerance = 0.2D;
                this.progressBar_TooHigh.Dock = System.Windows.Forms.DockStyle.Fill;
                this.progressBar_TooHigh.Location = new System.Drawing.Point(0, 0);
                this.progressBar_TooHigh.Maximum = 80;
                this.progressBar_TooHigh.Name = "progressBar_TooHigh";
                this.progressBar_TooHigh.Size = new System.Drawing.Size(141, 104);
                this.progressBar_TooHigh.Step = 1;
                this.progressBar_TooHigh.TabIndex = 0;
                // 
                // progressBar_TooLow
                // 
                this.progressBar_TooLow._doubleValue = 0D;
                this.progressBar_TooLow._tolerance = 0.2D;
                this.progressBar_TooLow.Dock = System.Windows.Forms.DockStyle.Fill;
                this.progressBar_TooLow.Location = new System.Drawing.Point(0, 0);
                this.progressBar_TooLow.Maximum = 80;
                this.progressBar_TooLow.Name = "progressBar_TooLow";
                this.progressBar_TooLow.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
                this.progressBar_TooLow.RightToLeftLayout = true;
                this.progressBar_TooLow.Size = new System.Drawing.Size(141, 105);
                this.progressBar_TooLow.Step = 1;
                this.progressBar_TooLow.TabIndex = 0;
                // 
                // dataGridViewLevel
                // 
                this.dataGridViewLevel.AllowUserToAddRows = false;
                this.dataGridViewLevel.AllowUserToDeleteRows = false;
                this.dataGridViewLevel.AllowUserToResizeColumns = true;
                this.dataGridViewLevel.AllowUserToResizeRows = false;
                this.dataGridViewLevel.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
                dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                dataGridViewCellStyle17.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                this.dataGridViewLevel.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
                this.dataGridViewLevel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
                this.dataGridViewLevel.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LeveldataGridViewDelete,
            this.LeveldataGridViewName,
            this.LeveldataGridViewCala,
            this.LeveldataGridViewUse,
            this.LevelDataGridViewDcum,
            this.LeveldataGridViewH,
            this.LeveldataGridViewHmes,
            this.LeveldataGridViewDist,
            this.LeveldataGridViewRall,
            this.LeveldataGridViewRawMeas,
            this.LeveldataGridViewCorrMeas,
            this.LeveldataGridViewTheo,
            this.LeveldataGridViewDepla,
            this.LeveldataGridViewObj,
            this.LeveldataGridViewMove,
            this.LeveldataGridViewResidusAller,
            this.LeveldataGridViewResidusRetour,
            this.LeveldataGridViewCode,
            this.LeveldataGridViewStaff,
            this.LeveldataGridViewInterfaces,
            this.LeveldataGridViewComment,
            this.LeveldataGridViewTime});
                this.dataGridViewLevel.Dock = System.Windows.Forms.DockStyle.Fill;
                this.dataGridViewLevel.EnableHeadersVisualStyles = false;
                this.dataGridViewLevel.Location = new System.Drawing.Point(0, 0);
                this.dataGridViewLevel.MultiSelect = false;
                this.dataGridViewLevel.Name = "dataGridViewLevel";
                this.dataGridViewLevel.RowHeadersVisible = false;
                this.dataGridViewLevel.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                this.dataGridViewLevel.ShowCellErrors = false;
                this.dataGridViewLevel.ShowEditingIcon = false;
                this.dataGridViewLevel.ShowRowErrors = false;
                this.dataGridViewLevel.Size = new System.Drawing.Size(1202, 213);
                this.dataGridViewLevel.TabIndex = 18;
                this.dataGridViewLevel.TabStop = false;
                this.dataGridViewLevel.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewLevel_CellClick);
                this.dataGridViewLevel.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewLevel_CellDoubleClick);
                this.dataGridViewLevel.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewLevel_CellValueChanged);
                this.dataGridViewLevel.CellBeginEdit += new DataGridViewCellCancelEventHandler(this.dataGridViewLevel_CellBeginEdit);
                this.dataGridViewLevel.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewLevel_CellEndEdit);
                this.dataGridViewLevel.CurrentCellChanged += new System.EventHandler(this.dataGridViewLevel_CurrentCellChanged);
                this.dataGridViewLevel.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridViewLevel_RowsAdded);
                this.dataGridViewLevel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridViewLevel_MouseClick);
                this.dataGridViewLevel.MouseEnter += new System.EventHandler(this.dataGridViewLevel_MouseEnter);
                // 
                // LeveldataGridViewDelete
                // 
                this.LeveldataGridViewDelete.HeaderText = "";
                this.LeveldataGridViewDelete.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
                this.LeveldataGridViewDelete.Name = "LeveldataGridViewDelete";
                this.LeveldataGridViewDelete.ReadOnly = true;
                this.LeveldataGridViewDelete.Resizable = System.Windows.Forms.DataGridViewTriState.False;
                this.LeveldataGridViewDelete.Width = 30;
                // 
                // LeveldataGridViewName
                // 
                dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                this.LeveldataGridViewName.DefaultCellStyle = dataGridViewCellStyle18;
                this.LeveldataGridViewName.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Name;
                this.LeveldataGridViewName.Name = "LeveldataGridViewName";
                this.LeveldataGridViewName.ReadOnly = true;
                this.LeveldataGridViewName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                this.LeveldataGridViewName.Width = 200;
                // 
                // LeveldataGridViewCala
                // 
                this.LeveldataGridViewCala.HeaderText = "A";
                this.LeveldataGridViewCala.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
                this.LeveldataGridViewCala.MinimumWidth = 30;
                this.LeveldataGridViewCala.Name = "LeveldataGridViewCala";
                this.LeveldataGridViewCala.ReadOnly = true;
                this.LeveldataGridViewCala.Width = 30;
                // 
                // LeveldataGridViewUse
                // 
                this.LeveldataGridViewUse.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Use;
                this.LeveldataGridViewUse.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
                this.LeveldataGridViewUse.MinimumWidth = 40;
                this.LeveldataGridViewUse.Name = "LeveldataGridViewUse";
                this.LeveldataGridViewUse.ReadOnly = true;
                this.LeveldataGridViewUse.Resizable = System.Windows.Forms.DataGridViewTriState.False;
                this.LeveldataGridViewUse.Width = 40;
                // 
                // LevelDataGridViewDcum
                // 
                dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LevelDataGridViewDcum.DefaultCellStyle = dataGridViewCellStyle19;
                this.LevelDataGridViewDcum.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Dcum;
                this.LevelDataGridViewDcum.Name = "LevelDataGridViewDcum";
                this.LevelDataGridViewDcum.ReadOnly = true;
                this.LevelDataGridViewDcum.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
                // 
                // LeveldataGridViewH
                // 
                dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LeveldataGridViewH.DefaultCellStyle = dataGridViewCellStyle20;
                this.LeveldataGridViewH.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Htheo;
                this.LeveldataGridViewH.Name = "LeveldataGridViewH";
                this.LeveldataGridViewH.ReadOnly = true;
                // 
                // LeveldataGridViewHmes
                // 
                dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LeveldataGridViewHmes.DefaultCellStyle = dataGridViewCellStyle21;
                this.LeveldataGridViewHmes.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Hmes;
                this.LeveldataGridViewHmes.Name = "LeveldataGridViewHmes";
                this.LeveldataGridViewHmes.ReadOnly = true;
                // 
                // LeveldataGridViewDist
                // 
                dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LeveldataGridViewDist.DefaultCellStyle = dataGridViewCellStyle22;
                this.LeveldataGridViewDist.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Dist;
                this.LeveldataGridViewDist.Name = "LeveldataGridViewDist";
                // 
                // LeveldataGridViewRall
                // 
                dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LeveldataGridViewRall.DefaultCellStyle = dataGridViewCellStyle23;
                this.LeveldataGridViewRall.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Extension;
                this.LeveldataGridViewRall.Name = "LeveldataGridViewRall";
                this.LeveldataGridViewRall.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
                // 
                // LeveldataGridViewRawMeas
                // 
                dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LeveldataGridViewRawMeas.DefaultCellStyle = dataGridViewCellStyle24;
                this.LeveldataGridViewRawMeas.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_RawMeas;
                this.LeveldataGridViewRawMeas.Name = "LeveldataGridViewRawMeas";
                this.LeveldataGridViewRawMeas.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
                // 
                // LeveldataGridViewCorrMeas
                // 
                dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LeveldataGridViewCorrMeas.DefaultCellStyle = dataGridViewCellStyle25;
                this.LeveldataGridViewCorrMeas.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_CorrMeas;
                this.LeveldataGridViewCorrMeas.Name = "LeveldataGridViewCorrMeas";
                this.LeveldataGridViewCorrMeas.ReadOnly = true;
                // 
                // LeveldataGridViewTheo
                // 
                dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LeveldataGridViewTheo.DefaultCellStyle = dataGridViewCellStyle26;
                this.LeveldataGridViewTheo.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Theo;
                this.LeveldataGridViewTheo.Name = "LeveldataGridViewTheo";
                this.LeveldataGridViewTheo.ReadOnly = true;
                this.LeveldataGridViewTheo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                // 
                // LeveldataGridViewDepla
                // 
                dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LeveldataGridViewDepla.DefaultCellStyle = dataGridViewCellStyle27;
                this.LeveldataGridViewDepla.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Move;
                this.LeveldataGridViewDepla.Name = "LeveldataGridViewDepla";
                this.LeveldataGridViewDepla.ReadOnly = true;
                this.LeveldataGridViewDepla.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                // 
                // LeveldataGridViewObj
                // 
                dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LeveldataGridViewObj.DefaultCellStyle = dataGridViewCellStyle33;
                this.LeveldataGridViewObj.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Move;
                this.LeveldataGridViewObj.Name = "LeveldataGridViewObj";
                this.LeveldataGridViewObj.ReadOnly = true;
                this.LeveldataGridViewObj.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                // 
                // LeveldataGridViewMove
                // 
                dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LeveldataGridViewMove.DefaultCellStyle = dataGridViewCellStyle34;
                this.LeveldataGridViewMove.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Move;
                this.LeveldataGridViewMove.Name = "LeveldataGridViewMove";
                this.LeveldataGridViewMove.ReadOnly = true;
                this.LeveldataGridViewMove.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                // 
                // LeveldataGridViewResidusAller
                // 
                dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LeveldataGridViewResidusAller.DefaultCellStyle = dataGridViewCellStyle28;
                this.LeveldataGridViewResidusAller.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_ResidusAller;
                this.LeveldataGridViewResidusAller.Name = "LeveldataGridViewResidusAller";
                this.LeveldataGridViewResidusAller.ReadOnly = true;
                // 
                // LeveldataGridViewResidusRetour
                // 
                dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LeveldataGridViewResidusRetour.DefaultCellStyle = dataGridViewCellStyle29;
                this.LeveldataGridViewResidusRetour.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_ResidusRetour;
                this.LeveldataGridViewResidusRetour.Name = "LeveldataGridViewResidusRetour";
                this.LeveldataGridViewResidusRetour.ReadOnly = true;
                // 
                // LeveldataGridViewCode
                // 
                dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LeveldataGridViewCode.DefaultCellStyle = dataGridViewCellStyle30;
                this.LeveldataGridViewCode.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_CodeAlesage;
                this.LeveldataGridViewCode.Name = "LeveldataGridViewCode";
                this.LeveldataGridViewCode.ReadOnly = true;
                this.LeveldataGridViewCode.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                // 
                // LeveldataGridViewStaff
                // 
                dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LeveldataGridViewStaff.DefaultCellStyle = dataGridViewCellStyle31;
                this.LeveldataGridViewStaff.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Staff;
                this.LeveldataGridViewStaff.Name = "LeveldataGridViewStaff";
                this.LeveldataGridViewStaff.ReadOnly = true;
                this.LeveldataGridViewStaff.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                // 
                // LeveldataGridViewInterfaces
                // 
                dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LeveldataGridViewInterfaces.DefaultCellStyle = dataGridViewCellStyle31;
                this.LeveldataGridViewInterfaces.HeaderText = "Interfaces";
                this.LeveldataGridViewInterfaces.Name = "LeveldataGridViewInterfaces";
                this.LeveldataGridViewInterfaces.ReadOnly = true;
                this.LeveldataGridViewInterfaces.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                // 
                // LeveldataGridViewComment
                // 
                this.LeveldataGridViewComment.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Comment;
                this.LeveldataGridViewComment.MaxInputLength = 150;
                this.LeveldataGridViewComment.Name = "LeveldataGridViewComment";
                this.LeveldataGridViewComment.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                this.LeveldataGridViewComment.Width = 300;
                // 
                // LeveldataGridViewTime
                // 
                dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
                this.LeveldataGridViewTime.DefaultCellStyle = dataGridViewCellStyle32;
                this.LeveldataGridViewTime.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Time;
                this.LeveldataGridViewTime.Name = "LeveldataGridViewTime";
                this.LeveldataGridViewTime.ReadOnly = true;
                this.LeveldataGridViewTime.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                // 
                // StationLevelModuleView
                // 
                this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
                this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
                this.ClientSize = new System.Drawing.Size(1212, 736);
                this.Name = "StationLevelModuleView";
                this.Text = "LevellingModule";
                this.Controls.SetChildIndex(this._PanelTop, 0);
                this.Controls.SetChildIndex(this._PanelBottom, 0);
                this._PanelBottom.ResumeLayout(false);
                this.splitContainer1.Panel1.ResumeLayout(false);
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
                this.splitContainer1.ResumeLayout(false);
                this.splitContainerNivel.Panel1.ResumeLayout(false);
                this.splitContainerNivel.Panel2.ResumeLayout(false);
                ((System.ComponentModel.ISupportInitialize)(this.splitContainerNivel)).EndInit();
                this.splitContainerNivel.ResumeLayout(false);
                this.panel_AlignWindow.ResumeLayout(false);
                this.splitContainer_AlignWindow.Panel1.ResumeLayout(false);
                this.splitContainer_AlignWindow.Panel2.ResumeLayout(false);
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer_AlignWindow)).EndInit();
                this.splitContainer_AlignWindow.ResumeLayout(false);
                this.splitContainer_AlignText.Panel1.ResumeLayout(false);
                this.splitContainer_AlignText.Panel1.PerformLayout();
                this.splitContainer_AlignText.Panel2.ResumeLayout(false);
                this.splitContainer_AlignText.Panel2.PerformLayout();
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer_AlignText)).EndInit();
                this.splitContainer_AlignText.ResumeLayout(false);
                this.splitContainer_ProgressBar.Panel1.ResumeLayout(false);
                this.splitContainer_ProgressBar.Panel2.ResumeLayout(false);
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer_ProgressBar)).EndInit();
                this.splitContainer_ProgressBar.ResumeLayout(false);
                ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLevel)).EndInit();
                this.ResumeLayout(false);

            }

            // this is used to trigger the 
            private void dataGridViewLevel_CellEndEdit(object sender, DataGridViewCellEventArgs e)
            {
                if (sender is DataGridView dgv)
                {
                    // If the value hasn't changed, trigger a CellValueChanged event anyway
                    if (valueWhenEntering == dgv[e.ColumnIndex, e.RowIndex].Value)
                    {
                        void DelayedCellValueChanged() => this.dataGridViewLevel_CellValueChanged(sender, e);
                        BeginInvoke(new Action(DelayedCellValueChanged));
                    }
                }
                else
                    valueWhenEntering = null;
            }

            private object valueWhenEntering = null;
            private void dataGridViewLevel_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
            {
                if (sender is DataGridView dgv)
                {
                    // Get the value of the cell before editing
                    valueWhenEntering = dgv[e.ColumnIndex, e.RowIndex].Value;
                }
            }
            #endregion

            private System.Windows.Forms.SplitContainer splitContainer1;
            private System.Windows.Forms.SplitContainer splitContainerNivel;
            internal System.Windows.Forms.TreeView treeView_Parameters;
            internal System.Windows.Forms.DataGridView dataGridViewLevel;
            private System.Windows.Forms.Panel panel_AlignWindow;
            private System.Windows.Forms.SplitContainer splitContainer_AlignWindow;
            private System.Windows.Forms.TextBox textBox_PointAlign;
            private System.Windows.Forms.SplitContainer splitContainer_ProgressBar;
            private TSU.Views.VerticalAlignmentProgressBar progressBar_TooHigh;
            private TSU.Views.VerticalAlignmentProgressBar progressBar_TooLow;
            private System.Windows.Forms.TextBox textBox_PointMovement;
            private System.Windows.Forms.SplitContainer splitContainer_AlignText;
            private System.Windows.Forms.DataGridViewImageColumn LeveldataGridViewDelete;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewName;
            private System.Windows.Forms.DataGridViewImageColumn LeveldataGridViewCala;
            private System.Windows.Forms.DataGridViewImageColumn LeveldataGridViewUse;
            private System.Windows.Forms.DataGridViewTextBoxColumn LevelDataGridViewDcum;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewH;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewHmes;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewDist;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewRall;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewRawMeas;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewCorrMeas;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewTheo;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewDepla;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewObj;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewMove;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewResidusAller;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewResidusRetour;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewCode;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewStaff;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewInterfaces;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewComment;
            private System.Windows.Forms.DataGridViewTextBoxColumn LeveldataGridViewTime;
        }
    }
}