using MathNet.Numerics.Statistics;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Serialization;
using TSU.Common.Instruments.Device;
using TSU.ENUM;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using EC = TSU.Common.Elements.Composites;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;

namespace TSU.Level
{
    public partial class Station
    {
        [Serializable]
        [XmlType(TypeName = "Level.Station.Module")]
        public class Module : Common.Station.Module
        {
            #region files and props

            public LevelingComputationStrategy _LevelingComputationStrategy { get; set; }
            public LevelingDirectionStrategy _LevelingDirectionStrategy { get; set; }

            [XmlIgnore]
            internal new View View
            {
                get
                {
                    return _TsuView as View;
                }
                set
                {
                    _TsuView = value;
                }
            }

            [XmlIgnore]
            public override ReadOnlyCollection<M.Measure> MeasuresReceived
            {
                get
                {
                    var list = new List<M.Measure>();
                    return new ReadOnlyCollection<M.Measure>(list);
                }
            }

            //internal I.Level levelInstrument;
            internal Station _WorkingStationLevel;
            public Station _AllerStationLevel { get; set; }
            public Station _RetourStationLevel { get; set; }
            public bool retourDone { get; set; }
            public CloneableList<Station> _RepriseStationLevel { get; set; }

            public IEnumerable<Station> AllStationLevels
            {
                get
                {
                    if (_AllerStationLevel != null)
                        yield return _AllerStationLevel;

                    if (_RetourStationLevel != null)
                        yield return _RetourStationLevel;

                    if (_RepriseStationLevel != null)
                        foreach (Station st in _RepriseStationLevel)
                            yield return st;
                }
            }
            public int _NumeroReprise { get; set; }
            public CloneableList<E.Point> _TheoPoint { get; set; }
            public CloneableList<E.Point> _MeasPoint { get; set; }
            private I.LevelingStaff defaultStaff;
            public I.LevelingStaff _defaultStaff
            {
                get
                {
                    //Evite de mettre en r�f�rence la defaultstaff
                    if (defaultStaff != null)
                    {
                        I.LevelingStaff staff = FinalModule.GetStaff(defaultStaff);
                        if (staff != null)
                        {
                            return staff.DeepCopy();
                        }
                    }
                    return null;
                }
                set
                {
                    defaultStaff = value;
                    if (FinalModule != null && defaultStaff != null)
                    {
                        FinalModule.AddStaff(defaultStaff);
                    }
                }
            }
            private I.LevelingStaff secondaryStaff;
            public I.LevelingStaff _secondaryStaff
            {
                get
                {
                    //Evite de mettre en r�f�rence la secondary staff
                    if (secondaryStaff != null)
                    {
                        I.LevelingStaff staff = FinalModule.GetStaff(secondaryStaff);
                        if (staff != null)
                        {
                            return staff.DeepCopy();
                        }
                    }
                    return null;
                }
                set
                {
                    secondaryStaff = value;
                    if (FinalModule != null && secondaryStaff != null)
                    {
                        FinalModule.AddStaff(secondaryStaff);
                    }
                }
            }
            [XmlIgnore]
            public bool selectingSecondaryStaff = false;
            public EC.SequenceNiv _SequenceSelected { get; set; }
            public string measGroupName { get; set; }
            #endregion

            #region Construction & init
            public Module()
                : base()
            {

            }
            public Module(Common.FinalModule module, bool createView = true)
                : base(module, $"{R.T_STATION_MODULE_LEVELING};{R.T_MODULE_MANAGING_A_LEVELING_STATION}", createView)
            {

            }
            //Methods
            public override void Initialize() // This method is called by the base class "Module" when this object is created
            {
                // Add a view to the module
                //this._WorkingStationLevel = new Level.Station();
                //this.ShowSaveMessageOfSuccess = new TsuBool(false);
                _AllerStationLevel = new Station();
                _RetourStationLevel = new Station();
                _RepriseStationLevel = new CloneableList<Station>();
                _WorkingStationLevel = _AllerStationLevel;
                _Station = _WorkingStationLevel;
                _LevelingComputationStrategy = new LGC2Strategy();
                _LevelingDirectionStrategy = new AllerStrategy();
                measGroupName = _AllerStationLevel._Name;
                retourDone = false;
                _SequenceSelected = new EC.SequenceNiv();
                _TheoPoint = new CloneableList<E.Point>();
                _MeasPoint = new CloneableList<E.Point>();
                Utility = "A--B";
                base.Initialize();  // this will add a instrument Manager but with no instrument in the list, need to call _InstrumentManager.LoadInstruments()
                _NumeroReprise = 0;

                LoadInstrumentTypes();
                // Add instrument Module for staff
                AddNewInstrumentModules();
            }
            public override string ToString()
            {
                return $"STM of {_WorkingStationLevel._Name}";
            }

            internal override void ReCreateWhatIsNotSerialized(bool saveSomeMemory = false)
            {
                try
                {
                    _LevelingDirectionStrategy = new AllerStrategy();
                    _LevelingDirectionStrategy.ChangeLevellingDirection(this);
                    FinalModule.SetActiveStationModule(this);
                    AddNewInstrumentModules();
                    //Dans le cas ou l'on ouvre une sauvegarde, l'op s�lectionn�e n'est plus coch�e
                    O.Operation oldOp = (FinalModule.OperationManager.AllElements.Find(x => x._Name == _AllerStationLevel.ParametersBasic._Operation._Name) as O.Operation);
                    if (oldOp != null)
                    {
                        FinalModule.OperationManager._SelectedObjectInBlue = oldOp;
                        FinalModule.OperationManager.AddSelectedObjects(oldOp);
                        FinalModule.OperationManager.UpdateView();
                    }
                    base.ReCreateWhatIsNotSerialized(saveSomeMemory);
                    ///Reselection du bon instrument dans l'instrument manager apr�s r�ouverture fichier TSU
                    if (_WorkingStationLevel.ParametersBasic._Instrument.Id != null)
                    {
                        var stp = _WorkingStationLevel.ParametersBasic;

                        I.Instrument oldInstrument = _InstrumentManager.AllElements.OfType<I.Instrument>().FirstOrDefault(
                                   inst => inst._SerialNumber == stp._Instrument._SerialNumber
                                   && (inst._Model == stp._Instrument._Model));
                        I.Sensor i = oldInstrument as I.Sensor;

                        _InstrumentManager._SelectedObjects.Clear();
                        _InstrumentManager._SelectedObjectInBlue = i;
                        _InstrumentManager._SelectedObjects.Add(i);
                        //this._InstrumentManager.ValidateSelection();
                        if (View != null) View.HideManualLevelView();
                    }
                    /// remise que toutes les stations n'ont pas �t� export�es
                    if (_AllerStationLevel != null)
                    {
                        if (_AllerStationLevel.ParametersBasic != null)
                        {
                            _AllerStationLevel.ParametersBasic.LastSaved = DateTime.MinValue;
                            _AllerStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                            CheckIfReadyToBeSaved(_AllerStationLevel);
                        }
                    }
                    if (_RetourStationLevel != null)
                    {
                        if (_RetourStationLevel.ParametersBasic != null)
                        {
                            _RetourStationLevel.ParametersBasic.LastSaved = DateTime.MinValue;
                            _RetourStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                            CheckIfReadyToBeSaved(_RetourStationLevel);
                        }
                    }
                    if (_RepriseStationLevel.Count != 0)
                    {
                        foreach (Station st in _RepriseStationLevel)
                        {
                            if (st != null)
                            {
                                if (st.ParametersBasic != null)
                                {
                                    st.ParametersBasic.LastSaved = DateTime.MinValue;
                                    st.ParametersBasic.LastChanged = DateTime.Now;
                                    CheckIfReadyToBeSaved(st);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"Cannot re-create what was not serialized in {_Name}", ex);
                }
            }
            internal void AddNewInstrumentModules()
            //Ajoute un module Management.Instrument
            {
                FinalModule.staffModule = _InstrumentManager;
                if (FinalModule.ParentModule is Common.Guided.Group.Module groupModule)
                    groupModule.staffModule = _InstrumentManager;
            }

            /// <summary>
            /// Set the Geode file path from the old station level Module into the new station level module
            /// </summary>
            /// <param name="oldStationModule"></param>
            internal void SetGeodeFilePathInNewStationLevelModule(Module oldStationModule)
            {
                _AllerStationLevel.ParametersBasic._GeodeDatFilePath = oldStationModule._AllerStationLevel.ParametersBasic._GeodeDatFilePath;
                _RetourStationLevel.ParametersBasic._GeodeDatFilePath = oldStationModule._RetourStationLevel.ParametersBasic._GeodeDatFilePath;
                if (_RepriseStationLevel.Count > 0 && oldStationModule._RepriseStationLevel.Count > 0)
                {
                    _RepriseStationLevel[0].ParametersBasic._GeodeDatFilePath = oldStationModule._RepriseStationLevel[0].ParametersBasic._GeodeDatFilePath;
                }
                //this.ShowSaveMessageOfSuccess = oldStationModule.ShowSaveMessageOfSuccess;
            }
            /// <summary>
            /// Set the same coordinate system during cloning of station level Module
            /// </summary>
            /// <param name="oldStationModule"></param>
            internal void SetCoordinateSystemInNewStationLevelModule(Module oldStationModule)
            {
                ChangeZType(oldStationModule._WorkingStationLevel._Parameters._ZType, false);
            }
            /// <summary>
            /// Set the elements from the old station module and select the previous sequence
            /// </summary>
            /// <param name="oldStationModule"></param>
            internal void SetPreviousElementSequenceInNewStationModule(Module oldStationModule)
            {
                oldStationModule.SetMeasPointsInElementManager(ElementModule);
                ElementModule._SelectedObjects.Clear();
                ElementModule.SetSelectableToAllSequenceNiv();
                //Coche les points d�j� s�lectionn� dans l'element module
                if (oldStationModule._SequenceSelected.Elements.Count != 0 && ElementModule.SelectableObjects != null)
                {
                    int i = ElementModule.SelectableObjects.FindIndex(x => x._Name == oldStationModule._SequenceSelected._Name);
                    i--;
                    if (i >= 0)
                    {
                        ElementModule._SelectedObjects.Add(ElementModule.SelectableObjects[i]);
                    }
                    else
                    {
                        new MessageInput(MessageType.Warning, R.StringLevel_ErrorPreviousSequence).Show();
                    }
                }
                else
                {
                    new MessageInput(MessageType.Warning, R.StringLevel_ErrorPreviousSequence).Show();
                }
                ElementModule.ValidateSelection();
            }

            /// <summary>
            /// Set the staff from the old station module to the new one 
            /// </summary>
            /// <param name="oldStationModule"></param>
            internal void SetDefaultStaffInNewStationLevelModule(Module oldStationModule)
            {
                if (oldStationModule._defaultStaff != null)
                {
                    TsuObject selectedStaff = _InstrumentManager.AllElements.Find(x => x._Name == oldStationModule._defaultStaff._Name);
                    selectingSecondaryStaff = false;
                    _InstrumentManager._SelectedObjects.Clear();
                    _InstrumentManager.AddSelectedObjects(selectedStaff);
                    _InstrumentManager._SelectedObjectInBlue = selectedStaff;
                    _InstrumentManager.ValidateSelection();
                }
            }
            /// <summary>
            /// Set the secondary staff from the old station module to the new one 
            /// </summary>
            /// <param name="oldStationModule"></param>
            internal void SetSecondaryStaffInNewStationLevelModule(Module oldStationModule)
            {
                if (oldStationModule._secondaryStaff != null)
                {
                    TsuObject selectedStaff = _InstrumentManager.AllElements.Find(x => x._Name == oldStationModule._secondaryStaff._Name);
                    selectingSecondaryStaff = true;
                    _InstrumentManager._SelectedObjects.Clear();
                    _InstrumentManager.AddSelectedObjects(selectedStaff);
                    _InstrumentManager._SelectedObjectInBlue = selectedStaff;
                    _InstrumentManager.ValidateSelection();
                }
            }
            /// <summary>
            /// Met la bonne mire pour chaque mire lors du clonage
            /// </summary>
            /// <param name="oldStationModule"></param>
            internal void SetOldStaffInToNewStationModule(Module oldStationModule)
            {
                foreach (M.MeasureOfLevel meas in _AllerStationLevel._MeasureOfLevel)
                {
                    int index = oldStationModule._AllerStationLevel._MeasureOfLevel.FindIndex(x => x._PointName == meas._PointName);
                    if (index != -1)
                    {
                        I.LevelingStaff newStaff = oldStationModule._AllerStationLevel._MeasureOfLevel[index]._staff;
                        ChangeStaff(meas, newStaff, false);
                    }
                }
                //foreach (M.MeasureOfLevel meas in this._RetourStationLevel._MeasureOfLevel)
                //{
                //    int index = oldStationModule._RetourStationLevel._MeasureOfLevel.FindIndex(x => x._PointName == meas._PointName);
                //    if (index != -1)
                //    {
                //        I.LevelingStaff newStaff = oldStationModule._RetourStationLevel._MeasureOfLevel[index]._staff;
                //        this.ChangeStaff(meas, newStaff, false);
                //    }
                //}
                //int indexReprise=0;
                //foreach (Level.Station reprise in this._RepriseStationLevel)
                //{
                //    foreach (M.MeasureOfLevel meas in reprise._MeasureOfLevel)
                //    {
                //        int index = oldStationModule._RepriseStationLevel[indexReprise]._MeasureOfLevel.FindIndex(x => x._PointName == meas._PointName);
                //        if (index != -1)
                //        {
                //            I.LevelingStaff newStaff = oldStationModule._RepriseStationLevel[indexReprise]._MeasureOfLevel[index]._staff;
                //            this.ChangeStaff(meas, newStaff, false);
                //        }
                //    }
                //    indexReprise++;
                //}
                View.UpdateListMeasure();
                FinalModule.Save();
            }
            /// <summary>
            /// Set the instrument from the old station module to the new one 
            /// </summary>
            /// <param name="oldStationModule"></param>
            internal void SetInstrumentInNewStationLevelModule(Module oldStationModule)
            {
                TsuObject selectedInstrument = _InstrumentManager.AllElements.Find(x => x._Name == oldStationModule._AllerStationLevel.ParametersBasic._Instrument._Name);
                _InstrumentManager._SelectedObjects.Clear();
                _InstrumentManager.AddSelectedObjects(selectedInstrument);
                _InstrumentManager._SelectedObjectInBlue = selectedInstrument;
                _InstrumentManager.ValidateSelection();
                View?.HideManualLevelView();
            }
            /// <summary>
            /// Set the meas Point from the old station level module into the new station level module as CALA points
            /// </summary>
            /// <param name="oldStationModule"></param>
            internal void SetOldStationLevelMeasPointIntoNewStationLevelModule(Module oldStationModule)
            {
                oldStationModule.SetMeasPointsInElementManager(ElementModule);
                ElementModule.ValidateSelection();
                foreach (M.MeasureOfLevel item in _AllerStationLevel._MeasureOfLevel)
                {
                    item._Point.LGCFixOption = LgcPointOption.CALA;
                }
            }
            internal void SetOldStationLevelTheoPointIntoNewStationLevelModule(Module oldStationModule)
            {
                //oldStationModule.SetTheoPointsInElementManager(this.ElementModule);
                //this.ElementModule.ValidateSelection();
                SetPointsToMeasure(oldStationModule._TheoPoint);
            }
            /// <summary>
            /// Set the elements from the old station module and select the next sequence
            /// </summary>
            /// <param name="oldStationModule"></param>
            internal void SetNextElementSequenceInNewStationModule(Module oldStationModule)
            {
                oldStationModule.SetMeasPointsInElementManager(ElementModule);
                ElementModule._SelectedObjects.Clear();
                ElementModule.SetSelectableToAllSequenceNiv();
                //Coche les points d�j� s�lectionn� dans l'element module
                if (oldStationModule._SequenceSelected != null && ElementModule.SelectableObjects != null)
                {
                    int i = ElementModule.SelectableObjects.FindIndex(x => x._Name == oldStationModule._SequenceSelected._Name);
                    i++;
                    if (i < ElementModule.SelectableObjects.Count && i != -1)
                    {
                        ElementModule._SelectedObjects.Add(ElementModule.SelectableObjects[i]);
                    }
                    else
                    {
                        new MessageInput(MessageType.Warning, R.StringLevel_ErrorNextSequence).Show();
                    }
                }
                else
                {
                    new MessageInput(MessageType.Warning, R.StringLevel_ErrorNextSequence).Show();
                }
                ElementModule.ValidateSelection();
            }
            internal void ShowStationLevelModule()
            {
                ShowInParentModule();
            }
            #endregion

            #region updates

            /// <summary>
            /// Mets � jour les coordonn�es de la liste des points mesur�s pour l'element module
            /// </summary>
            internal void UpdateMeasPointList()
            {
                foreach (M.MeasureOfLevel meas in _WorkingStationLevel._MeasureOfLevel)
                {

                    double avg = AverageHmesARP(meas);
                    if (avg != 9999)
                    {
                        int index = -1;
                        switch (_WorkingStationLevel._Parameters._ZType)
                        {
                            case CoordinatesType.CCS:
                                index = _TheoPoint.FindIndex(x => x._Name == meas._PointName);
                                if (index != -1)
                                {
                                    E.Point measPt = _TheoPoint[index].DeepCopy();
                                    measPt._Coordinates.Ccs.Z.Value = avg;
                                    _MeasPoint.Add(measPt);
                                }
                                break;
                            case CoordinatesType.SU:
                                index = _TheoPoint.FindIndex(x => x._Name == meas._PointName);
                                if (index != -1)
                                {
                                    E.Point measPt = _TheoPoint[index].DeepCopy();
                                    E.Coordinates newCoord = measPt._Coordinates.Local;
                                    newCoord.Z.Value = avg;
                                    measPt.SetCoordinates(newCoord, newCoord.ReferenceFrame, newCoord.CoordinateSystem);
                                    _MeasPoint.Add(measPt);
                                }
                                break;
                            //case CoordinatesType.UserDefined:
                            //    this._MeasPoint.Find(x => x._Name == meas._PointName)._Coordinates.UserDefined.H.Value = avg;
                            //    break;
                            default:
                                //this._MeasPoint.Find(x => x._Name == meas._PointName)._Coordinates.Ccs.H.Value = avg;
                                break;
                        }
                    }

                }
            }
            /// <summary>
            /// Calculate the average Hmes for one meas point
            /// </summary>
            /// <param name="meas"></param>
            /// <returns></returns>
            private double AverageHmesARP(M.MeasureOfLevel meas)
            {
                double avg = 0;
                int n = 0;
                foreach (Station st in AllStationLevels)
                {
                    int index = st._MeasureOfLevel.FindIndex(x => x._PointName == meas._PointName);
                    if (index != -1)
                    {
                        if (st._MeasureOfLevel[index]._Hmes != na)
                        {
                            avg += st._MeasureOfLevel[index]._Hmes;
                            n++;
                        }
                    }
                }
                if (n != 0)
                {
                    avg = avg / n;
                }
                else
                {
                    avg = 9999;
                }
                return avg;
            }
            internal void UpdateAllStaff(List<I.LevelingStaff> listStaff)
            {
                _AllerStationLevel.UpdateAllStaff(listStaff);
                _RetourStationLevel.UpdateAllStaff(listStaff);
                foreach (Station st in _RepriseStationLevel)
                {
                    st.UpdateAllStaff(listStaff);
                }
                CorrectMesAllStations();
            }
            internal void ExportToGeode(bool SaveLGC = false)
            //sauvegarde des mesures dans un fichier
            {
                if (_WorkingStationLevel._Parameters._State is State.StationLevelReadyToBeSaved)
                {
                    Result saveDatOK = SaveToGeode.SaveDATLevelingStation(this);
                    Result saveLGCOK = new Result();
                    string titleAndMessage = null;
                    string respond = "";
                    saveLGCOK.Success = false;
                    DsaFlag ShowSaveMessageOfSuccess = DsaFlag.GetByNameOrAdd(FinalModule.DsaFlags, "ShowSaveMessageOfSuccess");
                    if (SaveLGC)
                    {
                        saveLGCOK = SaveToLgc.SaveLGCFileLevelingStation(this);
                        if (saveLGCOK.Success && saveDatOK.Success)
                        {
                            switch (_WorkingStationLevel._Parameters._LevelingDirection)
                            {
                                case LevelingDirection.ALLER:
                                    titleAndMessage = string.Format(R.StringLevel_OutwardStationDatAndLGCSaved);
                                    break;
                                case LevelingDirection.RETOUR:
                                    titleAndMessage = string.Format(R.StringLevel_ReturnStationDatAndLGCSaved);
                                    break;
                                case LevelingDirection.REPRISE:
                                    titleAndMessage = string.Format(R.StringLevel_RepeatStationDatAndLGCSaved);
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                        {
                            if (saveDatOK.Success)
                            {
                                switch (_WorkingStationLevel._Parameters._LevelingDirection)
                                {
                                    case LevelingDirection.ALLER:
                                        titleAndMessage = string.Format(R.StringLevel_OutwardStationSaved, ":");
                                        break;
                                    case LevelingDirection.RETOUR:
                                        titleAndMessage = string.Format(R.StringLevel_ReturnStationSaved, ":");
                                        break;
                                    case LevelingDirection.REPRISE:
                                        titleAndMessage = string.Format(R.StringLevel_RepeatStationSaved, ":");
                                        break;
                                    default:
                                        break;
                                }

                            }
                            if (saveLGCOK.Success)
                            {
                                switch (_WorkingStationLevel._Parameters._LevelingDirection)
                                {
                                    case LevelingDirection.ALLER:
                                        titleAndMessage = string.Format(R.StringLevel_OutwardStationSaved, _WorkingStationLevel.ParametersBasic._LGCSaveFilePath);
                                        break;
                                    case LevelingDirection.RETOUR:
                                        titleAndMessage = string.Format(R.StringLevel_ReturnStationSaved, _WorkingStationLevel.ParametersBasic._LGCSaveFilePath);
                                        break;
                                    case LevelingDirection.REPRISE:
                                        titleAndMessage = string.Format(R.StringLevel_RepeatStationSaved, _WorkingStationLevel.ParametersBasic._LGCSaveFilePath);
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (saveDatOK.Success)
                        {
                            switch (_WorkingStationLevel._Parameters._LevelingDirection)
                            {
                                case LevelingDirection.ALLER:
                                    titleAndMessage = string.Format(R.StringLevel_OutwardStationSaved, ":");
                                    break;
                                case LevelingDirection.RETOUR:
                                    titleAndMessage = string.Format(R.StringLevel_ReturnStationSaved, ":");
                                    break;
                                case LevelingDirection.REPRISE:
                                    titleAndMessage = string.Format(R.StringLevel_RepeatStationSaved, ":");
                                    break;
                                default:
                                    break;
                            }
                        }
                    }


                    base.ShowGeodeExportSuccess(titleAndMessage, _WorkingStationLevel.ParametersBasic._GeodeDatFilePath, saveLGCOK.Success? _WorkingStationLevel.ParametersBasic._LGCSaveFilePath: "");
                    //if (titleAndMessage != null)
                    //{
                    //    MessageInput mi = new MessageInput(MessageType.GoodNews, titleAndMessage)
                    //    {
                    //        ButtonTexts = new List<string> { R.T_OK, R.T_OpenFileLocation },
                    //        DontShowAgain = ShowSaveMessageOfSuccess
                    //    };
                    //    respond = mi.Show().TextOfButtonClicked;
                    //}
                    //if (respond == R.T_OpenFileLocation)
                    //{
                    //    if (System.IO.File.Exists(_WorkingStationLevel.ParametersBasic._GeodeDatFilePath))
                    //    {
                    //        System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo("explorer.exe", " /select, " + _WorkingStationLevel.ParametersBasic._GeodeDatFilePath));
                    //    }
                    //}

                    FinalModule.Save();
                }
                View?.UpdateView();
            }

            #endregion

            #region observations
            public void OnNextBasedOn(I.Instrument TsuObject)
            //already threated in "Management.Instrument.Level SelectionLevel()"
            {
                if (TsuObject is I.Level)
                {
                    ChangeInstrument(TsuObject as I.Level);
                }
                if (TsuObject is I.LevelingStaff && !selectingSecondaryStaff)
                {
                    ChangeDefaultStaff(TsuObject as I.LevelingStaff);
                }
                if (TsuObject is I.LevelingStaff && selectingSecondaryStaff)
                {
                    ChangeSecondaryStaff(TsuObject as I.LevelingStaff);
                }
                selectingSecondaryStaff = false;
            }
            public override void OnNext(TsuObject TsuObject)
            // dispatcheur d'evenement si un type n'est pas connu il est trait� par OnNextBasedOn(TsuObject TsuObject)
            {
                dynamic dynamic = TsuObject;
                OnNextBasedOn(dynamic);
            }
            public void OnNextBasedOn(M.Measure measureFromInstrument)
            //�v�nement li� � la r�ception d'une mesure de l'instrument et lancement de la fonction pour l'ajouter dans le tableau
            {
                if (ParentModule is Smart.Module sm)
                {
                    // The measure is sent to every StationModule, but we only want it to be treated in the current station's StationModule
                    if (this == sm.ViewModel.CurrentStation?.StationModule)
                        sm.ViewModel.CurrentMeasure.CopyMeasureFromInstrument(measureFromInstrument as M.MeasureOfLevel);
                }
                else
                {
                    View?.AddInstrumentMeasure(measureFromInstrument);
                }

            }
            #endregion
            #region SelectionInstrument
            /// <summary>
            /// Changement de l'instrument
            /// </summary>
            /// <param name="newLevel"></param>
            internal void ChangeInstrument(I.Level newLevel = null, bool saveModule = true)
            {
                if (newLevel != null)
                {
                    //Voir TSU-2279
                    if (newLevel._Name != _WorkingStationLevel._Parameters._Instrument._Name
                        || newLevel._SerialNumber != _WorkingStationLevel._Parameters._Instrument._SerialNumber)
                    {
                        _WorkingStationLevel._Parameters._Instrument = newLevel;
                        _WorkingStationLevel._Parameters._Instrument._Model = newLevel._Model;
                        _WorkingStationLevel.ParametersBasic._Instrument = newLevel;
                        _WorkingStationLevel.ParametersBasic._Instrument._Model = newLevel._Model;
                        _WorkingStationLevel._Parameters._State = new State.ElementSelection();
                        _WorkingStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                        //if (this._WorkingStationLevel._MeasureOfLevel.Count != 0)
                        //{
                        //    this.UpdateOffsets();
                        //}
                        _AllerStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                        _AllerStationLevel.ParametersBasic._Instrument = newLevel;
                        _RetourStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                        _RetourStationLevel.ParametersBasic._Instrument = newLevel;
                        foreach (Station st in _RepriseStationLevel)
                        {
                            st.ParametersBasic._Instrument = newLevel;
                            st.ParametersBasic.LastChanged = DateTime.Now;
                        }
                        UpdateAllOffsets(saveModule);
                    }
                    View?.UpdateInstrumentView();
                }
            }
            /// <summary>
            /// S�lection de l'instrument dans l'instrument manager
            /// </summary>
            /// <returns></returns>
            internal I.Level SelectInstrument()
            {
                _InstrumentManager._SelectedObjects.Clear();

                var preselected = new List<TsuObject>();
                if (_WorkingStationLevel._Parameters._Instrument != null)
                {
                    var stp = _WorkingStationLevel._Parameters;

                    I.Instrument oldInstrument = _InstrumentManager.AllElements.OfType<I.Instrument>().FirstOrDefault(
                               inst => inst._SerialNumber == stp._Instrument._SerialNumber
                               && (inst._Model == stp._Instrument._Model));

                    if (oldInstrument != null)
                    {
                        preselected.Add(oldInstrument);
                    }
                }
                var selectables = _InstrumentManager.GetByClass(I.InstrumentClasses.NIVEAU);

                return _InstrumentManager.SelectInstrument(R.StringLevel_InstrumentChoice, selectables, multiSelection: false, preselected) as I.Level;
            }

            /// <summary>
            /// s�lection de la mire par d�faut pour les points mesur�s
            /// </summary>
            /// <param name="newStaff"></param>
            internal void ChangeDefaultStaff(I.LevelingStaff newStaff = null)
            {
                if (newStaff != null) _defaultStaff = newStaff;
            }

            /// <summary>
            /// Changement de la mire seondaire
            /// </summary>
            /// <param name="newStaff"></param>
            internal void ChangeSecondaryStaff(I.LevelingStaff newStaff = null)
            //s�lection de la mire par d�faut pour les points mesur�s
            {
                if (newStaff != null)
                {
                    _secondaryStaff = newStaff;
                    foreach (M.MeasureOfLevel m in _AllerStationLevel._MeasureOfLevel)
                    {
                        if (Tsunami2.Preferences.Values.SecondaryStaffCode.Contains(m._Point.SocketCode.Id))
                        {
                            string respond = "";
                            MessageInput mi = new MessageInput(MessageType.Choice, R.StringLevel_MsgChangeSecondaryStaff)
                            {
                                ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                            };
                            respond = mi.Show().TextOfButtonClicked;
                            if (respond == R.T_YES)
                            {
                                foreach (Station st in AllStationLevels)
                                {
                                    foreach (M.MeasureOfLevel meas in st._MeasureOfLevel)
                                    {
                                        meas._staff = newStaff.DeepCopy();
                                        st.ParametersBasic.LastChanged = DateTime.Now;
                                    }
                                }
                                UpdateAllOffsets();
                            }
                            break;
                        }
                    }
                }
            }
            internal I.LevelingStaff SelectDefaultStaff(bool showCancel = false)
            {
                _InstrumentManager.View.CheckBoxesVisible = false;
                _InstrumentManager.MultiSelection = false;

                List<TsuObject> preselected = null;
                if (defaultStaff != null)
                {
                    preselected = new List<TsuObject>();
                    TsuObject oldStaff = _InstrumentManager.AllElements.OfType<I.LevelingStaff>()
                        .FirstOrDefault(x =>
                            x._Model == defaultStaff._Model && x._SerialNumber == defaultStaff._SerialNumber);
                    preselected.Add(oldStaff);
                    _InstrumentManager.View.currentStrategy.ShowSelectionInButton();
                }
                selectingSecondaryStaff = false;
                var selectable = _InstrumentManager.GetByClassesAndTypes(classesWanted: new List<I.InstrumentClasses>() { I.InstrumentClasses.MIRE });
                return _InstrumentManager.SelectInstrument(R.StringLevel_DefaultStaffChoice, selectable, multiSelection: false, preselected, showCancel) as I.LevelingStaff;
            }
            #endregion

            #region On changes
            internal override void OnInstrumentChanged(I.Sensor i = null)
            {
                base.OnInstrumentChanged(i);
                if (_InstrumentManager != null && View != null) InvokeOnApplicationDispatcher(View.UpdateInstrumentView);
            }
            internal override void CheckAndChangeInstrument(string receivedSerialNumber)
            {
                base.CheckAndChangeInstrument(receivedSerialNumber);
                //permet d'�viter que le dna essaye de se connecter sur d'autres ports
                InvokeOnApplicationDispatcher(View.CancelDnaBackGroundWorker);
                InvokeOnApplicationDispatcher(View.UpdateView);
            }

            /// <summary>
            /// change the staff to a known or new one
            /// </summary>
            /// <param name="measLevel"></param>
            /// <param name="newStaff"></param>
            /// <param name="updateView"></param>
            internal void ChangeStaff(M.MeasureOfLevel measLevel, I.LevelingStaff newStaff = null, bool updateView = true, bool saveModule = true)
            {
                selectingSecondaryStaff = false;
                if (newStaff == null)
                {
                    var selectables = _InstrumentManager.GetByClass(I.InstrumentClasses.MIRE);
                    newStaff = _InstrumentManager.SelectInstrument(R.StringLevel_StaffChoice, selectables, multiSelection: false, preselected: null) as I.LevelingStaff;
                }
                if (newStaff != null)
                {
                    if (measLevel._staff == null || measLevel._staff._SerialNumber != newStaff._SerialNumber || measLevel._staff._Name != newStaff._Name)
                    {
                        FinalModule.AddStaff(newStaff);
                        measLevel._staff = FinalModule.GetStaff(newStaff).DeepCopy();
                        _WorkingStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                        UpdateOffsets(updateView, saveModule);
                    }
                }
            }

            /// <summary>
            /// change la mire Secondaire par default
            /// </summary>
            /// <param name="newStaff"></param>
            /// <param name="updateView"></param>
            internal I.LevelingStaff SelectSecondaryStaff()
            //change la mire Secondaire par default
            {
                _InstrumentManager._SelectedObjects.Clear();
                var preselected = new List<TsuObject>();
                if (_secondaryStaff != null)
                {
                    TsuObject oldStaff = _InstrumentManager.AllElements.OfType<I.LevelingStaff>().FirstOrDefault(x => x._Model == _secondaryStaff._Model
                        && x._SerialNumber == _secondaryStaff._SerialNumber);
                    preselected.Add(oldStaff);
                    _InstrumentManager.AddSelectedObjects(oldStaff);
                    _InstrumentManager.View.currentStrategy.ShowSelectionInButton();
                }
                selectingSecondaryStaff = true;
                var selectables = _InstrumentManager.GetByClass(I.InstrumentClasses.MIRE);

                I.LevelingStaff instr = _InstrumentManager.SelectInstrument(R.StringLevel_SelectSecondaryStaff,
                    selectables, multiSelection: false, preselected, true) as I.LevelingStaff;
                selectingSecondaryStaff = false;
                return instr;
            }
            /// <summary>
            /// Change le commentaire de la mesure
            /// </summary>
            /// <param name="measLevel"></param>
            internal void ChangeComment(M.MeasureOfLevel measLevel, string newComment)
            //change le commentaire
            {
                //newComment = newComment.ToUpper();
                measLevel._Date = DateTime.Now;
                _WorkingStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                measLevel.CommentFromUser = newComment;
                UpdateOffsets();
            }
            /// <summary>
            /// Met � jour toute les station de nivellement (aller-retour-reprise)
            /// si on change point cala ou supprime une mesure
            /// </summary>
            internal void UpdateAllOffsets(bool saveModule = true)
            //met � jour toutes les mesures de nivellement
            {
                LevelingDirectionStrategy actualDirection = _LevelingDirectionStrategy;
                _LevelingDirectionStrategy = new AllerStrategy();
                _WorkingStationLevel = _AllerStationLevel;
                UpdateOffsets(false, false);
                _LevelingDirectionStrategy = new RetourStrategy();
                _WorkingStationLevel = _RetourStationLevel;
                UpdateOffsets(false, false);
                _LevelingDirectionStrategy = new RepriseStrategy();
                int indexReprise = 1;
                foreach (Station st in _RepriseStationLevel)
                {
                    _LevelingDirectionStrategy._name = "P" + indexReprise.ToString();
                    _WorkingStationLevel = st;
                    UpdateOffsets(false, false);
                    indexReprise++;
                }
                _LevelingDirectionStrategy = actualDirection;
                if (actualDirection is AllerStrategy) { _WorkingStationLevel = _AllerStationLevel; }
                if (actualDirection is RetourStrategy) { _WorkingStationLevel = _RetourStationLevel; }
                if (actualDirection is RepriseStrategy) { _WorkingStationLevel = _RepriseStationLevel[_NumeroReprise - 1]; }
                //Sauvegarde du module nivellement
                if (saveModule && FinalModule != null) FinalModule.Save();
                View?.UpdateListMeasure();
            }
            internal void UpdateOffsets(bool updateView = true, bool saveModule = true)
            //met � jour toutes les mesures de nivellement
            {

                if (_WorkingStationLevel._MeasureOfLevel.Count > 0)
                {
                    _MeasPoint.Clear();
                    RemoveFromElementModule(measGroupName);
                    //this.SortMeasure();
                    _WorkingStationLevel.Rename(_LevelingDirectionStrategy);
                    if (FinalModule is Level.Module lm) lm.View.SetStationNameInTopButton(this);
                    if (FinalModule.ParentModule is Common.Guided.Group.Module gm) gm.UpdateBigButtonTop();
                    measGroupName = _AllerStationLevel._Name.Substring(0, 22) + _AllerStationLevel._Name.Substring(24, _AllerStationLevel._Name.Length - 24);
                    Utility = string.Format("{0}=>{1}"
                    , _WorkingStationLevel._MeasureOfLevel[0]._PointName
                    , _WorkingStationLevel._MeasureOfLevel[_WorkingStationLevel._MeasureOfLevel.Count - 1]._PointName);
                    _WorkingStationLevel.CorrectAllMeas();
                    //v�rifie si calcul possible et enl�ve tous les r�sultats si n�cessaire
                    if (CheckIfCalculationPossible())
                    {
                        _LevelingComputationStrategy.UpdateTheoReadingAndDeviation(this);
                    }
                    else
                    {
                        _WorkingStationLevel.CleanAllCalaCalculation();
                    }
                    UpdateMeasPointList();
                    AddAllMeasuresToFinalModule();
                    AddElementsToElementModule();
                    EcartAndEmqAllerRetourCalculation();
                    CheckIfReadyToBeSaved();
                    if (updateView)
                    {
                        View?.UpdateView();
                    }
                }
                //Sauvegarde du module nivellement
                if (saveModule) FinalModule.Save();
            }
            /// <summary>
            /// Corrige les mesures dans toutes les stations du module.
            /// </summary>
            internal void CorrectMesAllStations()
            {
                if (_AllerStationLevel != null) _AllerStationLevel.CorrectAllMeas();
                if (_RetourStationLevel != null) _RetourStationLevel.CorrectAllMeas();
                foreach (Station st in _RepriseStationLevel)
                {
                    if (st != null) st.CorrectAllMeas();
                }
            }
            ///// <summary>
            ///// Met � jour les coordonn�es theoriques en fonction des coordonn�es dans l'element manager
            ///// </summary>
            //private void UpdateTheoCoordinates()
            //{
            //    foreach (M.MeasureOfLevel m in this._WorkingStationLevel._MeasureOfLevel)
            //    {
            //        this.ElementModule.SetSelectableToAllUniquePoint();
            //        E.Point pt = this.ElementModule.SelectableObjects.Find(x=> ((x as E.Point)._Name == m._Point._Name) && ((x as E.Point)._Origin == m._Point._Origin) && ((x as E.Point).State!=E.Element.States.Bad)) as E.Point;
            //        if (pt!=null) 
            //        {
            //            m._Point._Coordinates = pt._Coordinates.DeepCopy();
            //            int index = this._TheoPoint.FindIndex(x => (x._Name == pt._Name) && (x._Origin == pt._Origin));
            //            if (index != -1) this._TheoPoint[index]._Coordinates = pt._Coordinates.DeepCopy();
            //        }
            //    }
            //}
            /// <summary>
            /// Met � jour les coordonn�es theoriques en fonction des coordonn�es dans l'element manager
            /// </summary>
            internal override void UpdateTheoCoordinates(E.Point originalPoint, E.Point pointModified)
            {
                bool update = false;
                int indexAller = _AllerStationLevel._MeasureOfLevel.FindIndex(x => (x._Point._Name == originalPoint._Name) && (x._Point._Origin == originalPoint._Origin));
                if (indexAller != -1)
                {
                    E.Point initialPoint = _AllerStationLevel._MeasureOfLevel[indexAller]._Point.DeepCopy();
                    _AllerStationLevel._MeasureOfLevel[indexAller]._Point.AssignProperties(pointModified);
                    _AllerStationLevel._MeasureOfLevel[indexAller]._Point.LGCFixOption = initialPoint.LGCFixOption;
                    update = true;
                }
                int indexRetour = _RetourStationLevel._MeasureOfLevel.FindIndex(x => (x._Point._Name == originalPoint._Name) && (x._Point._Origin == originalPoint._Origin));
                if (indexRetour != -1)
                {
                    E.Point initialPoint = _RetourStationLevel._MeasureOfLevel[indexRetour]._Point.DeepCopy();
                    _RetourStationLevel._MeasureOfLevel[indexRetour]._Point.AssignProperties(pointModified);
                    _RetourStationLevel._MeasureOfLevel[indexRetour]._Point.LGCFixOption = initialPoint.LGCFixOption;
                    update = true;
                }
                foreach (Station st in _RepriseStationLevel)
                {
                    int indexReprise = st._MeasureOfLevel.FindIndex(x => (x._Point._Name == originalPoint._Name) && (x._Point._Origin == originalPoint._Origin));
                    if (indexReprise != -1)
                    {
                        E.Point initialPoint = st._MeasureOfLevel[indexReprise]._Point.DeepCopy();
                        st._MeasureOfLevel[indexReprise]._Point.AssignProperties(pointModified);
                        st._MeasureOfLevel[indexReprise]._Point.LGCFixOption = initialPoint.LGCFixOption;
                        update = true;
                    }
                }
                int indexTheo = _TheoPoint.FindIndex(x => x._Name == originalPoint._Name && x._Origin == originalPoint._Origin);
                if (indexTheo != -1)
                {
                    E.Point initialPoint = _TheoPoint[indexTheo].DeepCopy();
                    _TheoPoint[indexTheo].AssignProperties(pointModified);
                    _TheoPoint[indexTheo].LGCFixOption = initialPoint.LGCFixOption;
                }
                if (update) UpdateAllOffsets();
            }
            //internal void UpdateOffsets(M.MeasureOfLevel mesure)
            //    //met � jour la mesure de nivellement d�finie
            //{
            //    this.UpdateOffsets(mesure, 1); 
            //}
            //internal void UpdateOffsets(M.MeasureOfLevel mesure, int numeroReprise)
            //    //met � jour la mesure de nivellement, Si reprise met � jour la reprise avec le num�ro de la reprise
            //{
            //    try
            //    {
            //        foreach (M.MeasureOfLevel item in this._WorkingStationLevel._MeasureOfLevel)
            //        {
            //            if (item._Point._Name == mesure._Point._Name)
            //            {
            //                item._Date = mesure._Date;
            //                item._Comment = mesure._Comment;
            //                item._Distance = mesure._Distance;
            //                item._LevelReading = mesure._LevelReading;
            //                item._staff = mesure._staff;
            //            }
            //        }
            //        this._MeasPoint.Clear();
            //        this.RemoveFromElementModule(this.measGroupName);
            //        this.SortMeasure();
            //        this._WorkingStationLevel.Rename(this._LevelingDirectionStrategy);
            //        if (this.FinalModule is  LevelModule)(this.FinalModule as LevelModule).View.SetStationNameInTopButton(this);
            //        if (this.FinalModule.ParentModule is TSU.Guided.Group.Module) (this.FinalModule.ParentModule as TSU.Guided.Group.Module).UpdateBigButtonTop();
            //        this.measGroupName = this._AllerStationLevel._Name.Substring(0, 22) + this._AllerStationLevel._Name.Substring(24, this._AllerStationLevel._Name.Length - 24);
            //        this.utility = String.Format("{0}=>{1}"
            //        , this._WorkingStationLevel._MeasureOfLevel[0]._PointName
            //        , this._WorkingStationLevel._MeasureOfLevel[this._WorkingStationLevel._MeasureOfLevel.Count - 1]._PointName);
            //        //v�rifie si calcul possible et enl�ve tous les r�sultats si n�cessaire
            //        if (CheckIfCalculationPossible())
            //        {
            //            this._LevelingComputationStrategy.UpdateTheoReadingAndDeviation(this);
            //            this.UpdateMeasPointList();
            //            this.AddElementsToElementModule();
            //            this.CheckIfReadyToBeSaved();
            //            this.View.UpdateListMeasure();
            //        }
            //        else
            //        {
            //            this._WorkingStationLevel.CleanAllCalaCalculation();
            //            this.UpdateMeasPointList();
            //            this.AddElementsToElementModule();
            //            this.CheckIfReadyToBeSaved();
            //            this.View.UpdateListMeasure();
            //        }
            //    }
            //    catch (Exception e)
            //    {

            //        throw;
            //    }

            //}
            /// <summary>
            /// Trie les mesures de nivellement dans le sens des Dcum ou inverse Dcum
            /// </summary>
            internal void SortMeasure()
            {
                SortPoints(_TheoPoint);
                SortPoints(_MeasPoint);
                SortPoints(FinalModule.PointsToBeMeasured);
                SortPoints(FinalModule.CalaPoints);
                SortPoints(FinalModule.PointsToBeAligned);
                switch (sortDirection)
                {
                    case SortDirection.Dcum:
                        //this._WorkingStationLevel.SortMeasures();
                        _AllerStationLevel.SortMeasures();
                        _RetourStationLevel.SortMeasures();
                        foreach (Station item in _RepriseStationLevel)
                        {
                            item.SortMeasures();
                        }
                        break;
                    case SortDirection.InvDcum:
                        //this._WorkingStationLevel.ReverseMeasures();
                        _AllerStationLevel.ReverseMeasures();
                        _RetourStationLevel.ReverseMeasures();
                        foreach (Station item in _RepriseStationLevel)
                        {
                            item.ReverseMeasures();
                        }
                        break;
                    default:
                        break;
                }
            }
            /// <summary>
            /// Trie les points et les mesures pour un fil passant par l'origine d'un acc�l�rateur
            /// </summary>
            private void SortMeasAcrossOrigin()
            {
                if (_TheoPoint.FindIndex(X => X._Accelerator == "SPS") != -1)
                {
                    SortPointsForSPSOrigin(_TheoPoint);
                    SortPointsForSPSOrigin(_MeasPoint);
                    SortPointsForSPSOrigin(FinalModule.PointsToBeMeasured);
                    SortPointsForSPSOrigin(FinalModule.CalaPoints);
                    SortPointsForSPSOrigin(FinalModule.PointsToBeAligned);
                    _AllerStationLevel.SortMeasForSPSOrigin();
                    _RetourStationLevel.SortMeasForSPSOrigin();
                    foreach (Station item in _RepriseStationLevel)
                    {
                        item.SortMeasForSPSOrigin();
                    }
                }
                if (_TheoPoint.FindIndex(X => X._Accelerator == "PR") != -1)
                {
                    SortPointsForPROrigin(_TheoPoint);
                    SortPointsForPROrigin(_MeasPoint);
                    SortPointsForPROrigin(FinalModule.PointsToBeMeasured);
                    SortPointsForPROrigin(FinalModule.CalaPoints);
                    SortPointsForPROrigin(FinalModule.PointsToBeAligned);
                    _AllerStationLevel.SortMeasForPROrigin();
                    _RetourStationLevel.SortMeasForPROrigin();
                    foreach (Station item in _RepriseStationLevel)
                    {
                        item.SortMeasForPROrigin();
                    }
                }
                if (_TheoPoint.FindIndex(X => X._Accelerator == "BR") != -1)
                {
                    SortPointsForBROrigin(_TheoPoint);
                    SortPointsForBROrigin(_MeasPoint);
                    SortPointsForBROrigin(FinalModule.PointsToBeMeasured);
                    SortPointsForBROrigin(FinalModule.CalaPoints);
                    SortPointsForBROrigin(FinalModule.PointsToBeAligned);
                    _AllerStationLevel.SortMeasForBROrigin();
                    _RetourStationLevel.SortMeasForBROrigin();
                    foreach (Station item in _RepriseStationLevel)
                    {
                        item.SortMeasForBROrigin();
                    }
                }
                if (_TheoPoint.FindIndex(X => X._Accelerator == "DR") != -1)
                {
                    SortPointsForDROrigin(_TheoPoint);
                    SortPointsForDROrigin(_MeasPoint);
                    SortPointsForDROrigin(FinalModule.PointsToBeMeasured);
                    SortPointsForDROrigin(FinalModule.CalaPoints);
                    SortPointsForDROrigin(FinalModule.PointsToBeAligned);
                    _AllerStationLevel.SortMeasForDROrigin();
                    _RetourStationLevel.SortMeasForDROrigin();
                    foreach (Station item in _RepriseStationLevel)
                    {
                        item.SortMeasForDROrigin();
                    }
                }
            }
            /// <summary>
            /// Check if the wire is crossing the origin of an accelerator
            /// </summary>
            private bool CheckIfAcrossOrigin()
            {
                // Pour le SPS cherche si la liste des points contient un aimant dans le sextant 6 et dans le sextant 1
                if (_TheoPoint.FindIndex(X => X._Accelerator == "SPS" && X._Numero.StartsWith("6")) != -1
                    && _TheoPoint.FindIndex(X => X._Accelerator == "SPS" && X._Numero.StartsWith("1")) != -1)

                    return true;
                //// Pour le PS ring cherche si dans les num�ros, on a des 0 et des 9 en m�me temps
                if (_TheoPoint.FindIndex(X => X._Accelerator == "PR" && X._Numero.StartsWith("0")) != -1
                && _TheoPoint.FindIndex(X => X._Accelerator == "PR" && X._Numero.StartsWith("9")) != -1)

                    return true;
                //// Pour le booster ring cherche si dans les num�ros, on a des 1 et des 16 en m�me temps
                if (_TheoPoint.FindIndex(X => X._Accelerator == "BR" && X._Numero.StartsWith("1")
                && !X._Numero.Contains("16")
                && !X._Numero.Contains("15")
                && !X._Numero.Contains("14")
                && !X._Numero.Contains("13")
                && !X._Numero.Contains("12")
                && !X._Numero.Contains("11")
                && !X._Numero.Contains("10")) != -1
                && _TheoPoint.FindIndex(X => X._Accelerator == "BR" && X._Numero.StartsWith("16")) != -1)

                    return true;
                //// Pour AD ring cherche si dans les num�ros, on a des 0 et des 5 en m�me temps
                if (_TheoPoint.FindIndex(X => X._Accelerator == "DR" && X._Numero.StartsWith("0")) != -1
                && _TheoPoint.FindIndex(X => X._Accelerator == "DR" && X._Numero.StartsWith("5")) != -1)

                    return true;
                return false;
            }
            #endregion

            #region Selection
            internal void ChangeTeam(string newTeam)
            //change l'�quipe dans chaque station level
            {
                _WorkingStationLevel._Parameters._Team = newTeam;
                _WorkingStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                _AllerStationLevel._Parameters._Team = newTeam;
                _AllerStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                _RetourStationLevel._Parameters._Team = newTeam;
                _RetourStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                foreach (Station st in _RepriseStationLevel)
                {
                    st._Parameters._Team = newTeam;
                    st.ParametersBasic.LastChanged = DateTime.Now;
                }
                CheckIfReadyToBeSaved();

                Tsunami2.Preferences.Values.GuiPrefs.AddLastTeamtUsed(newTeam);
            }
            /// <summary>
            /// Inverse le param�tre pour voir ou ne pas voir les calculs LGC2 dans toutes les stations level
            /// </summary>
            internal void RunAndShowLGC2Calculation()
            {
                //bool reverseShow;
                //reverseShow = (this._WorkingStationLevel._Parameters._ShowLGC2Files == true)? false : true;
                //this._WorkingStationLevel._Parameters._ShowLGC2Files = reverseShow;
                //this._AllerStationLevel._Parameters._ShowLGC2Files = reverseShow;
                //this._RetourStationLevel._Parameters._ShowLGC2Files = reverseShow;
                //foreach (Level.Station reprise in this._RepriseStationLevel)
                //{
                //    reprise._Parameters._ShowLGC2Files = reverseShow;
                //}
                _WorkingStationLevel._Parameters._ShowLGC2Files = true;
                UpdateOffsets();
                _WorkingStationLevel._Parameters._ShowLGC2Files = false;
            }
            /// <summary>
            /// Affiche ou cache les anciennes mesures dans le datagridview
            /// </summary>
            internal void ReverseShowObsoleteMeas()
            {
                bool reverseShow;
                reverseShow = !_WorkingStationLevel._Parameters.showObsoleteMeas;
                _WorkingStationLevel._Parameters.showObsoleteMeas = reverseShow;
                _AllerStationLevel._Parameters.showObsoleteMeas = reverseShow;
                _RetourStationLevel._Parameters.showObsoleteMeas = reverseShow;
                foreach (Station reprise in _RepriseStationLevel)
                {
                    reprise._Parameters.showObsoleteMeas = reverseShow;
                }
            }
            /// <summary>
            /// Replace an actual measure by an obsolete measure
            /// </summary>
            /// <param name="v"></param>
            internal void ReplaceActualbyOldMeasure(M.MeasureOfLevel oldMeas)
            {
                int index = _WorkingStationLevel._MeasureOfLevel.FindIndex(x => x._PointName == oldMeas._PointName);
                if (index == -1)
                {
                    // Va chercher le point theo dans l'element module et le rajoute au points � mesurer
                    ElementModule.SetSelectableToAllUniquePoint();
                    if (ElementModule.SelectableObjects != null)
                    {
                        E.Point pointToAdd = ElementModule.FindSelectablePointWithSameNameAndOrigin(oldMeas._Point);
                        if (pointToAdd != null)
                        {
                            pointToAdd.LGCFixOption = LgcPointOption.VZ;
                            _TheoPoint.Add(pointToAdd);
                            AddPoints(_TheoPoint);
                        }
                    }
                    index = _WorkingStationLevel._MeasureOfLevel.FindIndex(x => x._PointName == oldMeas._PointName);
                }
                if (index != -1)
                {
                    //Ajoute la mesure actuelle aux anciennes mesures, remplace la mesure actuelle par oldMeas et enl�ve oldMeas des mesures obsoletes
                    _WorkingStationLevel._MeasureOfLevel[index]._Status = new M.States.Bad();
                    if (_WorkingStationLevel._MeasureOfLevel[index]._RawLevelReading != na)
                    {
                        _WorkingStationLevel._ObsoleteMeasureOfLevel.Add(_WorkingStationLevel._MeasureOfLevel[index].DeepCopy());
                    }
                    oldMeas._Status = new M.States.Unknown();
                    oldMeas._Point.LGCFixOption = _WorkingStationLevel._MeasureOfLevel[index]._Point.LGCFixOption;
                    _WorkingStationLevel._MeasureOfLevel[index] = oldMeas.DeepCopy();
                    int index2 = _WorkingStationLevel._ObsoleteMeasureOfLevel.FindIndex(x => x._PointName == oldMeas._PointName && x._Date == oldMeas._Date);
                    if (index != -1) _WorkingStationLevel._ObsoleteMeasureOfLevel.RemoveAt(index2);
                    _WorkingStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                }
                ///Cas ou le point a �t� effac�
                else
                {

                }
                UpdateOffsets();
            }
            /// <summary>
            /// Inverse l'utilisation des distances pour le calcul LGC2
            /// </summary>
            internal void ReverseUseDistanceForLGC2()
            {
                bool reverseUseDist;
                reverseUseDist = !_WorkingStationLevel._Parameters._UseDistanceForLGC2;
                _WorkingStationLevel._Parameters._UseDistanceForLGC2 = reverseUseDist;
                _AllerStationLevel._Parameters._UseDistanceForLGC2 = reverseUseDist;
                _RetourStationLevel._Parameters._UseDistanceForLGC2 = reverseUseDist;
                foreach (Station reprise in _RepriseStationLevel)
                {
                    reprise._Parameters._UseDistanceForLGC2 = reverseUseDist;
                }
                UpdateOffsets();
            }



            /// <summary>
            /// Change la tolerance dans toutes les stations level
            /// </summary>
            /// <param name="p"></param>
            internal void ChangeTolerance(double p)
            {
                _WorkingStationLevel._Parameters._Tolerance = p;
                _AllerStationLevel._Parameters._Tolerance = p;
                _RetourStationLevel._Parameters._Tolerance = p;
                foreach (Station st in _RepriseStationLevel)
                {
                    st._Parameters._Tolerance = p;
                }
                UpdateOffsets();
            }
            /// <summary>
            /// Changement de la temperature ambiante
            /// </summary>
            /// <param name="newTemperature"></param>
            internal void ChangeTemperature(double newTemperature)
            {
                _WorkingStationLevel._Parameters._Temperature = newTemperature;
                _WorkingStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                CheckIfReadyToBeSaved();
                _AllerStationLevel._Parameters._Temperature = newTemperature;
                _AllerStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                CheckIfReadyToBeSaved(_AllerStationLevel);
                _RetourStationLevel._Parameters._Temperature = newTemperature;
                _RetourStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                CheckIfReadyToBeSaved(_RetourStationLevel);
                foreach (Station item in _RepriseStationLevel)
                {
                    item._Parameters._Temperature = newTemperature;
                    item.ParametersBasic.LastChanged = DateTime.Now;
                    CheckIfReadyToBeSaved(item);
                }
                CorrectMesAllStations();
                UpdateAllOffsets();
            }
            /// <summary>
            /// Change le Z type pour toutes les stations level
            /// </summary>
            /// <param name="coordinatesType"></param>
            internal void ChangeZType(CoordinatesType coordinatesType, bool updateOffset = true)
            {
                _WorkingStationLevel._Parameters._ZType = coordinatesType;
                _AllerStationLevel._Parameters._ZType = coordinatesType;
                _RetourStationLevel._Parameters._ZType = coordinatesType;
                foreach (Station reprise in _RepriseStationLevel)
                {
                    reprise._Parameters._ZType = coordinatesType;
                }
                if (updateOffset) UpdateOffsets();
            }
            /// <summary>
            /// permet � l'utilisateur de changer le nom de la station
            /// </summary>
            /// <param name="MsgResponse"></param>
            internal void RenameStation(string MsgResponse = "NotSet")
            {
                //Toutes les stations level doivent avoir la m�me racine de nom
                _WorkingStationLevel.Rename(_LevelingDirectionStrategy, MsgResponse);
                _AllerStationLevel.Rename(new AllerStrategy(), MsgResponse);
                _RetourStationLevel.Rename(new RetourStrategy(), MsgResponse);
                int r = 1;
                foreach (Station reprise in _RepriseStationLevel)
                {
                    RepriseStrategy repriseStrat = new RepriseStrategy();
                    repriseStrat._name = "P" + r.ToString();
                    reprise.Rename(repriseStrat, MsgResponse);
                }
                RemoveFromElementModule(measGroupName);
                //Change le nom de station en celui de la station level en enlevant le caract�re donnant le sens
                measGroupName = _AllerStationLevel._Name.Substring(0, 22) + _AllerStationLevel._Name.Substring(24, _AllerStationLevel._Name.Length - 24);
                AddElementsToElementModule();
                if (FinalModule is Level.Module) (FinalModule as Level.Module).View.SetStationNameInTopButton(this);
                if (FinalModule.ParentModule is Common.Guided.Group.Module) (FinalModule.ParentModule as Common.Guided.Group.Module).UpdateBigButtonTop();
            }
            /// <summary>
            ///  Change le num�ro d'op�ration dans chaque station level
            /// </summary>
            /// <param name="o"></param>
            internal override void ChangeOperationID(O.Operation op)
            {
                if (_WorkingStationLevel == null) return;
                if (op.value != _WorkingStationLevel._Parameters._Operation.value)
                {
                    _AllerStationLevel.ParametersBasic._Operation = op;
                    _AllerStationLevel._Parameters._Operation = op;
                    _RetourStationLevel.ParametersBasic._Operation = op;
                    _RetourStationLevel._Parameters._Operation = op;
                    _WorkingStationLevel.ParametersBasic._Operation = op;
                    _WorkingStationLevel._Parameters._Operation = op;
                    _WorkingStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                    _RetourStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                    _AllerStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                    foreach (Station st in _RepriseStationLevel)
                    {
                        st.ParametersBasic._Operation = op;
                        st._Parameters._Operation = op;
                        st.ParametersBasic.LastChanged = DateTime.Now;
                    }
                    CheckIfReadyToBeSaved();
                }

                Tsunami2.Preferences.Values.GuiPrefs.LastOpUsed = op;

            }
            internal void SelectPoints()
            //Selectionne des �lements dans un fichier theo
            {
                SetTheoPointsInElementManager();
                ElementModule.View.buttons.HideOnlyButtons(ElementModule.View.buttons.sequence);
                // ouvre la fen�tre du module �lement de s�lection des points 
                ElementModule.SelectPoints(null, R.T_VALID, R.T_CANCEL, selectOnlyPointsForWire: false);
            }
            /// <summary>
            ///  ouvre l'�lement module pour s�lectionner une s�quence de niv
            /// </summary>
            internal void SelectSequence()
            {
                SetSequenceInElementManager();
                ElementModule.SelectOneSequenceNiv();
            }
            /// <summary>
            /// Set the sequence selected in the element module
            /// </summary>
            internal void SetSequenceInElementManager()
            {
                ElementModule.SetSelectableToAllSequenceNiv();
                ElementModule._SelectedObjects.Clear();
                //Coche les points d�j� s�lectionn� dans l'element module
                if (_SequenceSelected != null && ElementModule.SelectableObjects != null)
                {
                    TsuObject selected = ElementModule.SelectableObjects.Find(x => x._Name == _SequenceSelected._Name);
                    if (selected != null)
                    {
                        ElementModule.AddSelectedObjects(selected);
                    }
                }
            }
            /// <summary>
            /// Set the actual theoretical points in the element manager
            /// </summary>
            internal void SetTheoPointsInElementManager(E.Manager.Module elementModule)
            {
                ///Coche les points d�j� s�lectionn� dans l'element module
                elementModule.SetSelectableToAllUniquePoint();
                elementModule._SelectedObjects.Clear();
                if (_TheoPoint != null && elementModule.SelectableObjects != null)
                {
                    foreach (E.Point item in _TheoPoint)
                    {
                        TsuObject objectFound = elementModule.FindSelectablePointWithSameNameAndOrigin(item);
                        if (objectFound != null)
                        {
                            elementModule.AddSelectedObjects(objectFound);
                        }
                    }
                }
            }

            /// <summary>
            /// Set the actual theoretical points in the element manager
            /// </summary>
            internal void SetTheoPointsInElementManager()
            {
                SetTheoPointsInElementManager(ElementModule);
            }
            /// <summary>
            /// Set the actual Cala theoretical points in the element manager
            /// </summary>
            internal void SetTheoCalaPointsInElementManager()
            {
                ///Coche les points d�j� s�lectionn� dans l'element module
                //this.ElementModule.SetSelectableToAllUniquePoint();
                ElementModule._SelectedObjects.Clear();
                if (_TheoPoint != null && ElementModule.SelectableObjects != null)
                {
                    foreach (E.Point item in _TheoPoint)
                    {
                        if (item.LGCFixOption == LgcPointOption.CALA)
                        {
                            TsuObject objectFound = ElementModule.FindSelectablePointWithSameNameAndOrigin(item);
                            if (objectFound != null)
                            {
                                ElementModule.AddSelectedObjects(objectFound);
                            }
                        }
                    }
                }
            }
            /// <summary>
            /// Coche les points d�j� s�lectionn� en VZ dans l'element module
            /// </summary>
            internal void SetVZPointsInElementManager()
            {
                ///Coche les points d�j� s�lectionn� dans l'element module
                //this.ElementModule.SetSelectableToAllUniquePoint();
                ElementModule._SelectedObjects.Clear();
                if (_TheoPoint != null && ElementModule.SelectableObjects != null)
                {
                    foreach (E.Point item in _TheoPoint)
                    {
                        if (item.LGCFixOption == LgcPointOption.CALA)
                        {
                            TsuObject objectToRemoveFound = ElementModule.FindSelectablePointWithSameNameAndOrigin(item);
                            if (objectToRemoveFound != null)
                            {
                                ElementModule.SelectableObjects.Remove(objectToRemoveFound);
                            }
                        }
                        if (item.LGCFixOption == LgcPointOption.VZ)
                        {
                            TsuObject objectToSelectFound = ElementModule.FindSelectablePointWithSameNameAndOrigin(item);
                            if (objectToSelectFound != null)
                            {
                                ElementModule.AddSelectedObjects(objectToSelectFound);
                            }
                        }
                    }
                }
            }
            /// <summary>
            /// Set the measured points in the element manager
            /// </summary>
            internal void SetMeasPointsInElementManager(E.Manager.Module elementModule)
            {
                ///Coche les points d�j� s�lectionn� dans l'element module
                elementModule.SetSelectableToAllUniquePoint();
                elementModule._SelectedObjects.Clear();
                if (_MeasPoint != null && elementModule.SelectableObjects != null)
                {
                    foreach (E.Point item in _MeasPoint)
                    {
                        TsuObject objectFound = elementModule.FindSelectablePointWithSameNameAndOrigin(item);
                        if (objectFound != null)
                        {
                            elementModule.AddSelectedObjects(objectFound);
                        }
                    }
                }
            }
            /// <summary>
            /// Override the method in Final Module when received event to add points to measure
            /// </summary>
            /// <param name="compositeSelected"></param>
            internal override void SetPointsToMeasure(EC.CompositeElement compositeSelected)
            {
                CloneableList<E.Point> pointsSelected = new CloneableList<E.Point>();
                if (compositeSelected != null)
                {
                    if (!(compositeSelected is EC.TheoreticalElement) && (!(compositeSelected is EC.Sequence)))
                    ///permet d'�viter le message de non s�lection de point si ouverture d'un fichier theo
                    {
                        foreach (var item in compositeSelected)
                        {
                            if (item is E.Point)
                            {
                                pointsSelected.Add(item as E.Point);
                            }
                            if (item is EC.Magnet)
                            {
                                pointsSelected.AddRange((item as EC.Magnet).GetAlesages());
                            }
                        }
                        SetPointsToMeasure(pointsSelected);
                    }
                }
            }

            internal void SetPointsToMeasure(List<E.Point> pointsSelected, bool saveModule = true)
            {
                if (pointsSelected != null)
                {
                    if (pointsSelected.Count != 0)
                    {
                        //cr�ation de la station nivellement aller au d�part
                        //if (this._WorkingStationLevel._MeasureOfLevel.Count == 0)
                        //{
                        //    this._AllerStationLevel = this._WorkingStationLevel;
                        //}
                        AddPoints(pointsSelected);
                        if (View != null)
                        {
                            switch (View.moduleType)
                            {
                                case ModuleType.Guided:
                                    if (FinalModule.ParentModule is Common.Guided.Group.Module)
                                    {
                                        switch ((FinalModule.ParentModule as Common.Guided.Group.Module).Type)
                                        {
                                            case GuidedModuleType.Alignment:
                                                if (!CheckIfAcrossOrigin())
                                                {
                                                    SortMeasure();
                                                }
                                                else
                                                {
                                                    SortMeasAcrossOrigin();
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                    break;
                                case ModuleType.Advanced:
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                        {
                            if (!CheckIfAcrossOrigin())
                            {
                                SortMeasure();
                            }
                            else
                            {
                                SortMeasAcrossOrigin();
                            }
                        }
                        //Si on s�lectionne les points directement dans le retour, il faut renommer la station aller pour �viter une erreur dans update offset
                        if (_AllerStationLevel._Name.Length <= 22) _AllerStationLevel.Rename(new AllerStrategy());
                        UpdateOffsets(saveModule);
                    }
                    else
                    {
                        //this.View.ShowMessageOfExclamation(R.StringLevel_ErrorPoint);
                    }
                }
            }
            /// <summary>
            /// met � jour la date de derni�re modification de toutes les stations si on fait une colim niveau
            /// </summary>
            internal void UpdateAllColimCheck()
            {
                _AllerStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                CheckIfReadyToBeSaved(_AllerStationLevel);
                _RetourStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                CheckIfReadyToBeSaved(_RetourStationLevel);
                foreach (Station item in _RepriseStationLevel)
                {
                    item.ParametersBasic.LastChanged = DateTime.Now;
                    CheckIfReadyToBeSaved(item);
                }

            }

            /// <summary>
            /// recupere les points dans la sequence fil et ajoute les points
            /// </summary>
            /// <param name="sequenceFromElementModule"></param>
            internal void SetSequenceToMeasure(EC.SequenceNiv sequenceFromElementModule)
            {
                CloneableList<E.Point> pointsSelected = new CloneableList<E.Point>();
                if (sequenceFromElementModule != null)
                {
                    bool messageOfChoiceAlreadyShown = false;
                    bool usePointsWithoutTheoCoord = true;
                    foreach (var item in sequenceFromElementModule.Elements)
                    {
                        if (item is E.Point)
                        {
                            // Utilisation ou pas des points non-pr�sents dans le fichier th�orique choisi
                            E.Point pt = item as E.Point;
                            if (item._Origin == sequenceFromElementModule._Origin)
                            {
                                if (!messageOfChoiceAlreadyShown)
                                {
                                    MessageInput mi = new MessageInput(MessageType.Choice, R.StringLevel_UseOrNotNewCreatedPoints)
                                    {
                                        ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                                    };
                                    string answer = mi.Show().TextOfButtonClicked;
                                    messageOfChoiceAlreadyShown = true;
                                    if (answer == R.T_YES)
                                    {
                                        usePointsWithoutTheoCoord = true;
                                    }
                                    else
                                    {
                                        usePointsWithoutTheoCoord = false;
                                    }
                                }
                                if (usePointsWithoutTheoCoord)
                                {
                                    pt.LGCFixOption = LgcPointOption.VZ;
                                    pointsSelected.Add(pt);
                                }

                            }
                            else
                            {
                                pt.LGCFixOption = LgcPointOption.VZ;
                                pointsSelected.Add(pt);
                            }
                        }
                    }
                }
                //Rajoute les anciens points theo qui ne sont pas dans les points s�lectionn�s
                if (_SequenceSelected._Name == sequenceFromElementModule._Name)
                {
                    foreach (E.Point theoPoint in _TheoPoint)
                    {
                        if (!pointsSelected.Exists(x => x._Name == theoPoint._Name))
                        {
                            E.Point pointToAdd = ElementModule.GetPointsInAllElements().Find(x => x._Name == theoPoint._Name && x._Origin == theoPoint._Origin);
                            if (pointToAdd != null)
                            {
                                pointsSelected.Add(pointToAdd);
                            }
                        }
                    }
                }
                //enl�vement du point CALA forc�.
                if (pointsSelected.FindIndex(x => x.LGCFixOption == LgcPointOption.CALA) == -1 && pointsSelected.Count > 0)
                {
                    //pointsSelected[0].LGCFixOption = LgcPointOption.CALA;
                }
                ///enleve toutes les mesures si on a s�lectionn� une autre s�quence fil
                if (_SequenceSelected._Name != sequenceFromElementModule._Name && pointsSelected.Count >= 1)
                {
                    _MeasPoint.Clear();
                    _TheoPoint.Clear();
                    _AllerStationLevel._MeasureOfLevel.Clear();
                    _RetourStationLevel._MeasureOfLevel.Clear();
                    foreach (Station st in _RepriseStationLevel)
                    {
                        st._MeasureOfLevel.Clear();
                    }
                    FinalModule.PointsToBeMeasured.Clear();
                }
                _SequenceSelected = sequenceFromElementModule;
                SetPointsToMeasure(pointsSelected);
            }
            //Permet lorsqu'on cr�e un point � partir de l'element manager de mettre l'origine � created by user
            internal override string GetNewPointGroupName()
            {
                return R.T_CREATED_BY_USER;
            }
            /// <summary>
            /// Cr�e un nouveau point dans l'element module et le s�lectionne pour les mesures
            /// </summary>
            /// <param name="newPointName"></param>
            internal void CreateNewPoint(string newPointName, E.SocketCode socketCode)
            {
                ElementModule.SetSelectableToAllUniquePoint();
                if (ElementModule.SelectableObjects.OfType<E.Point>().FirstOrDefault(x => x._Name == newPointName && x._Origin == "CreatedByUser") == null)
                {
                    E.Point newPoint = new E.Point(newPointName);
                    newPoint.StorageStatus = Common.Tsunami.StorageStatusTypes.Keep;
                    newPoint.SocketCode = socketCode;
                    newPoint.Date = DateTime.Now;
                    newPoint._Coordinates.Ccs.X.Value = 0;
                    newPoint._Coordinates.Ccs.Y.Value = 0;
                    newPoint._Coordinates.Ccs.Z.Value = 400;
                    if (newPoint._Point != "" && newPoint._Zone != "" && newPoint._Numero != "" && newPoint._Class != "")
                    {
                        newPoint.fileElementType = ElementType.Alesage;

                    }
                    else
                    {
                        newPoint.fileElementType = ElementType.Pilier;
                    }
                    ElementModule.AddElement(R.T_CREATED_BY_USER, newPoint, false, false, true);
                    ElementModule.SetSelectableToAllUniquePoint();
                    E.Point copyNewPoint = newPoint.DeepCopy();
                    copyNewPoint.LGCFixOption = LgcPointOption.VZ;
                    _TheoPoint.Add(copyNewPoint);
                    FinalModule.PointsToBeAligned.Add(copyNewPoint);
                    //this._MeasPoint.Add(newPoint.DeepCopy());
                    _AllerStationLevel.UpdateListMeasureLevel(this, newPoint);
                    _RetourStationLevel.UpdateListMeasureLevel(this, newPoint);
                    foreach (Station reprise in _RepriseStationLevel)
                    {
                        reprise.UpdateListMeasureLevel(this, newPoint);
                    }
                    SetTheoPointsInElementManager();
                    ElementModule.ValidateSelection();
                }
            }
            internal void AddElementsToElementModule()
            //ajoute tous les points theo s�lectionn� dans element module
            {
                string groupName = measGroupName;
                groupName = groupName.Replace('.', '_');
                string saveName = _Name;
                _Name = groupName;
                foreach (E.Point point in _MeasPoint)
                {
                    point.Type = E.Point.Types.NewPoint;
                    SavePoint(point);
                }
                _Name = saveName;

                //this.ElementModule.Clear(this);
                //foreach (E.Point point in this._MeasPoint)
                //{           
                //    this.ElementModule.AddElement(this, point, false, false, true, false);
                //}
            }
            /// <summary>
            /// Enl�ve les points de l'element module avec l'origine origin
            /// </summary>
            /// <param name="origin"></param>
            internal void RemoveFromElementModule(string origin)
            {
                CloneableList<E.Point> savedMeasPointList = new CloneableList<E.Point>();
                foreach (E.Point pt in _MeasPoint)
                {
                    E.Point copyP = pt.DeepCopy();
                    savedMeasPointList.Add(copyP);
                }
                string saveName = _Name;
                origin = origin.Replace('.', '_');
                _Name = origin;
                ElementModule.Clear(this);
                _Name = saveName;
                _MeasPoint = savedMeasPointList;
            }
            /// <summary>
            /// Ajoute les derni�res mesures dans le final module dans la liste des received measures
            /// </summary>
            private void AddAllMeasuresToFinalModule()
            {
                RemoveAllMeasuresFromFinalModule();
                List<M.MeasureOfLevel> measToAdd = _AllerStationLevel._MeasureOfLevel.FindAll(x => x._TheoReading != na);
                if (measToAdd != null)
                {
                    FinalModule.ReceivedMeasures.AddRange(measToAdd);
                }
            }
            /// <summary>
            /// Enl�ve toutes les mesuresOfOffset du final module dans la liste des received measures
            /// </summary>
            private void RemoveAllMeasuresFromFinalModule()
            {
                FinalModule.ReceivedMeasures.RemoveAll(x => x is M.MeasureOfLevel);
            }
            /// <summary>
            /// V�rifie si le point theo correspondant au point mesur� a des coordonn�es.
            /// </summary>
            /// <param name="measPoint"></param>
            /// <returns></returns>
            //internal bool CheckIfTheoPointHasCoordinate(E.Point measPoint)
            //{
            //    bool hasCoord = true;
            //    E.Point theoPoint = this._TheoPoint.Find(x => x._Name == measPoint._Name);
            //    switch (this._WorkingStationLevel._Parameters._ZType)
            //    {
            //        case CoordinatesType.CCS:
            //            if (theoPoint._Coordinates.Ccs.X.Value ==TSU.Tsunami2.TsunamiPreferences.Values.na || theoPoint._Coordinates.Ccs.Y.Value ==TSU.Tsunami2.TsunamiPreferences.Values.na || theoPoint._Coordinates.Ccs.H.Value ==TSU.Tsunami2.TsunamiPreferences.Values.na)
            //            {
            //                hasCoord = false;
            //            }
            //            break;
            //        case CoordinatesType.SU:
            //            if (theoPoint._Coordinates.Local.X.Value ==TSU.Tsunami2.TsunamiPreferences.Values.na || theoPoint._Coordinates.Local.Y.Value ==TSU.Tsunami2.TsunamiPreferences.Values.na || theoPoint._Coordinates.Local.H.Value ==TSU.Tsunami2.TsunamiPreferences.Values.na)
            //            {
            //                hasCoord = false;
            //            }
            //            break;
            //        default:
            //            break;
            //    }

            //    return hasCoord;
            //}
            private void AddPoints(List<E.Point> selectedPoint)
            //Ajoute des �l�ments points au set des mesures de nivellement
            {
                try
                {
                    CleanMeasureOfLevel(selectedPoint);
                    CloneableList<E.Point> cleanedSelectedPoint = CheckIfPointAlreadyMeasured(selectedPoint);
                    foreach (E.Point pt in cleanedSelectedPoint)
                    //Cr�e une nouvelle mesure dans la liste des mesures et y affecte les points s�lectionn�s
                    {
                        //this._WorkingStationLevel.UpdateListMeasureLevel(this, pt);
                        _RetourStationLevel.UpdateListMeasureLevel(this, pt);
                        _AllerStationLevel.UpdateListMeasureLevel(this, pt);
                        foreach (Station reprise in _RepriseStationLevel)
                        {
                            reprise.UpdateListMeasureLevel(this, pt);
                        }
                        //N'ajoute le nouveau point dans les lectures theo que si le point n'existe pas encore    
                        if (!_TheoPoint.Contains(pt))
                        {
                            _TheoPoint.Add(pt.DeepCopy());
                            //this._MeasPoint.Add(pt.DeepCopy());
                        }
                        else
                        {
                            // doit remplacer les points pour avoir les coordonn�es venant de l'element module � jour.
                            int indexTheo = _TheoPoint.FindIndex(x => x._Name == pt._Name && x._Origin == pt._Origin);
                            if (indexTheo != -1) _TheoPoint[indexTheo] = pt.DeepCopy();
                            int indexMeas = _MeasPoint.FindIndex(x => x._Name == pt._Name && x._Name == pt._Origin);
                            if (indexMeas != -1) _MeasPoint[indexMeas] = pt.DeepCopy();
                            int i = _AllerStationLevel._MeasureOfLevel.FindIndex(x => x._PointName == pt._Name && x._Point._Origin == pt._Origin);
                            if (i != -1) { _AllerStationLevel._MeasureOfLevel[i]._Point = pt.DeepCopy(); }
                            i = _RetourStationLevel._MeasureOfLevel.FindIndex(x => x._PointName == pt._Name && x._Point._Origin == pt._Origin);
                            if (i != -1) { _RetourStationLevel._MeasureOfLevel[i]._Point = pt.DeepCopy(); }
                            foreach (Station st in _RepriseStationLevel)
                            {
                                i = st._MeasureOfLevel.FindIndex(x => x._PointName == pt._Name && x._Point._Origin == pt._Origin);
                                if (i != -1) { st._MeasureOfLevel[i]._Point = pt.DeepCopy(); }
                            }
                        }
                    }
                    //this.CheckPointCalaNumber();
                }
                catch (Exception e)
                {

                    if (e is NullReferenceException) { e.Data.Clear(); }
                    else
                    {
                        throw;
                    }
                }
            }
            /// <summary>
            /// V�rifie qu'il y a au moins un point en cala
            /// </summary>
            private void CheckPointCalaNumber()
            {
                //if (this._WorkingStationLevel._MeasureOfLevel.FindIndex(x => x._Point.LGCFixOption == LgcPointOption.CALA) == -1)
                //{
                //    this.SortMeasure();
                //    this._WorkingStationLevel._MeasureOfLevel[0]._Point.LGCFixOption = LgcPointOption.CALA;
                //}
                if (_AllerStationLevel._MeasureOfLevel.FindIndex(x => x._Point.LGCFixOption == LgcPointOption.CALA) == -1)
                {
                    //this.SortMeasure();
                    _AllerStationLevel._MeasureOfLevel[0]._Point.LGCFixOption = LgcPointOption.CALA;
                    E.Point ptCala = _AllerStationLevel._MeasureOfLevel[0]._Point;
                    _RetourStationLevel._MeasureOfLevel.Find(x => x._Point._Name == ptCala._Name)._Point.LGCFixOption = LgcPointOption.CALA;
                    foreach (Station reprise in _RepriseStationLevel)
                    {
                        reprise._MeasureOfLevel.Find(x => x._Point._Name == ptCala._Name)._Point.LGCFixOption = LgcPointOption.CALA;
                    }
                }

            }
            /// <summary>
            /// Enl�ve les measures of level qui ne sont plus s�lectionn�es dans l'element manager
            /// Enl�ve ces mesures dans toutes les stations level, pts theo et pts mesur�s
            /// </summary>
            /// <param name="selectedPoint"></param>
            private void CleanMeasureOfLevel(List<E.Point> selectedPoint)
            {

                CloneableList<M.MeasureOfLevel> measToRemove = new CloneableList<M.MeasureOfLevel>();
                foreach (M.MeasureOfLevel meas in _AllerStationLevel._MeasureOfLevel)
                {
                    if (selectedPoint.FindIndex(x => x._Name == meas._Point._Name && x._Origin == meas._Point._Origin) == -1)
                    {
                        measToRemove.Add(meas);
                    }
                }
                foreach (M.MeasureOfLevel meas in measToRemove)
                {
                    DeletePointInAllStation(meas._Point._Name, false);
                    //this._AllerStationLevel._MeasureOfLevel.Remove(meas);
                    //this._RetourStationLevel._MeasureOfLevel.Remove(meas);
                    //foreach (Level.Station reprise in this._RepriseStationLevel)
                    //{
                    //    reprise._MeasureOfLevel.Remove(meas);
                    //}
                    //this._TheoPoint.Remove(this._TheoPoint.Find(x => x._Name == meas._Point._Name && x._Origin == meas._Point._Origin));
                    //this._MeasPoint.Remove(this._MeasPoint.Find(x => x._Name == meas._Point._Name));
                    ////N'enl�ve le point theo que s'il n'existe plus dans aucune station aller-retour-reprise
                    //if (!this.FindPointInAllMeasure(meas._Point))
                    //{
                    //    this._TheoPoint.Remove(this._TheoPoint.Find(x => x._Name == meas._Point._Name && x._Origin == meas._Point._Origin));
                    //    this._MeasPoint.Remove(this._MeasPoint.Find(x => x._Name == meas._Point._Name && x._Origin == meas._Point._Origin));
                    //}
                }
            }
            /// <summary>
            /// Enleve le point dans la station level actuelle (aller-retour ou reprise)
            /// </summary>
            /// <param name="pointName"></param>
            //internal void DeletePoint(string pointName)
            //    //supprime les points de la liste de mesure et de la liste d'�l�ment.
            //{
            //    E.Point point = this._TheoPoint.Find(x => x._Name == pointName).DeepCopy();
            //    this._WorkingStationLevel._MeasureOfLevel.RemoveAt(this._WorkingStationLevel._MeasureOfLevel.FindIndex(x => x._PointName == point._Name && x._Point._Origin == point._Origin));
            //    this.ElementModule.Clear(this);
            //    //N'enl�ve le point theo que s'il n'existe plus dans aucune station aller-retour-reprise
            //    if (!this.FindPointInAllMeasure(point))
            //    {
            //        this._TheoPoint.Remove(this._TheoPoint.Find(x => x._Name == point._Name && x._Origin == point._Origin));
            //        this._MeasPoint.Remove(this._MeasPoint.Find(x => x._Name == point._Name && x._Origin == point._Origin));
            //    }         
            //    //this._TheoElement.elements.RemoveAt(this._TheoElement.elements.FindIndex(x => (x as Point)._Name == pointName));
            //    //V�rifie s'il y a au moins un point en cala
            //    if ((this._WorkingStationLevel._MeasureOfLevel.FindAll(x => x._Point.LGCFixOption == TSU.ENUM.LgcPointOption.CALA)).Count < 1)
            //    {
            //        this._WorkingStationLevel._MeasureOfLevel[0]._Point.LGCFixOption = TSU.ENUM.LgcPointOption.CALA;
            //    }
            //    this.AddElementsToElementModule();
            //    this.UpdateOffsets();
            //}
            /// <summary>
            /// Enleve le point toutes les stations (aller-retour et reprise)
            /// </summary>
            /// <param name="pointName"></param>
            internal void DeletePointInAllStation(string pointName, bool refreshOffset = true)
            {
                E.Point point = _TheoPoint.Find(x => x._Name == pointName).DeepCopy();
                foreach (Station st in AllStationLevels)
                {
                    int i = st._MeasureOfLevel.FindIndex(x => x._PointName == point._Name && x._Point._Origin == point._Origin);
                    if (i != -1)
                    {
                        st.CopyAsObsolete(st._MeasureOfLevel[i]);
                        st._MeasureOfLevel.RemoveAt(i);
                        st.ParametersBasic.LastChanged = DateTime.Now;
                    }
                }
                ElementModule.Clear(this);
                _TheoPoint.Remove(_TheoPoint.Find(x => x._Name == point._Name && x._Origin == point._Origin));
                _MeasPoint.Remove(_MeasPoint.Find(x => x._Name == point._Name));
                ////V�rifie s'il y a au moins un point en cala--- Depuis TSU 1839 permet de ne plus avoir de point en cala
                //if ((this._AllerStationLevel._MeasureOfLevel.FindAll(x => x._Point.LGCFixOption == TSU.ENUM.LgcPointOption.CALA)).Count < 1)
                //{
                //    this._AllerStationLevel._MeasureOfLevel[0]._Point.LGCFixOption = TSU.ENUM.LgcPointOption.CALA;
                //    this._RetourStationLevel._MeasureOfLevel[0]._Point.LGCFixOption = TSU.ENUM.LgcPointOption.CALA;
                //    foreach (Stations.StationLevel st in this._RepriseStationLevel)
                //    {
                //        st._MeasureOfLevel[0]._Point.LGCFixOption = TSU.ENUM.LgcPointOption.CALA;
                //    }
                //}
                if (refreshOffset)
                {
                    AddElementsToElementModule();
                    UpdateAllOffsets();
                }
            }
            internal void AddRepriseStation()
            //Ajoute une copie de la station retour si pas encore de reprise, au sinon copie la derni�re station de reprise. Enl�ve toutes les lectures
            {
                if (_RepriseStationLevel.Count == 0)
                {
                    _RepriseStationLevel.Add(_RetourStationLevel.DeepCopy());
                    _RepriseStationLevel[_RepriseStationLevel.Count - 1].specialPartOfStationName = _RetourStationLevel.specialPartOfStationName;
                    //this._RepriseStationLevel[0].ParametersBasic._Instrument = this._RetourStationLevel.ParametersBasic._Instrument;
                }
                else
                {
                    _RepriseStationLevel.Add(_RepriseStationLevel[_RepriseStationLevel.Count - 1].DeepCopy());
                    _RepriseStationLevel[_RepriseStationLevel.Count - 1].specialPartOfStationName = _RepriseStationLevel[_RepriseStationLevel.Count - 2].specialPartOfStationName;
                }
                _RepriseStationLevel[_RepriseStationLevel.Count - 1].CleanAllMeasure();
                _RepriseStationLevel[_RepriseStationLevel.Count - 1]._Parameters._LevelingDirection = LevelingDirection.REPRISE;
                _RepriseStationLevel[_RepriseStationLevel.Count - 1].ParametersBasic._GeodeDatFilePath = "";
                _RepriseStationLevel[_RepriseStationLevel.Count - 1].ParametersBasic.LastChanged = DateTime.MinValue;
                _RepriseStationLevel[_RepriseStationLevel.Count - 1].ParametersBasic.LastSaved = DateTime.MinValue;
                ChangeLevelingDirectionToReprise(_RepriseStationLevel.Count);
            }
            /// <summary>
            /// For the Guided module, Erase and create new retour and reprise station. 
            ///
            /// </summary>
            internal void ResetAndCreateRetourAndReprise()
            {
                retourDone = false;
                List<M.MeasureOfLevel> saveRetourMeas = _RetourStationLevel._MeasureOfLevel;
                List<M.MeasureOfLevel> saveObsoleteRetourMeas = _RetourStationLevel._ObsoleteMeasureOfLevel;
                List<M.MeasureOfLevel> saveRepriseMeas = new List<M.MeasureOfLevel>();
                List<M.MeasureOfLevel> saveObsoleteRepriseMeas = new List<M.MeasureOfLevel>();
                if (_RepriseStationLevel.Count >= 1)
                {
                    saveRepriseMeas = _RepriseStationLevel[0]._MeasureOfLevel;
                    saveObsoleteRepriseMeas = _RepriseStationLevel[0]._ObsoleteMeasureOfLevel;
                }
                ChangeLevelingDirectionToRetour();
                _RepriseStationLevel.Clear();
                AddRepriseStation();
                _RetourStationLevel.CopyMeasAlreadyDone(saveRetourMeas, saveObsoleteRetourMeas);
                _RepriseStationLevel[0].CopyMeasAlreadyDone(saveRepriseMeas, saveObsoleteRepriseMeas);
                ChangeLevelingDirectionToAller();
            }
            #endregion

            #region computes & checks

            /// <summary>
            /// V�rifie si des points s�lectionn� sont d�j� dans la liste des measures of level et les enl�ve pour �viter les doublons
            /// </summary>
            /// <param name="selectedPoint"></param>
            /// <returns></returns>
            private CloneableList<E.Point> CheckIfPointAlreadyMeasured(List<E.Point> selectedPoint)
            {
                CloneableList<E.Point> cleanedSelectedPoint = new CloneableList<E.Point>();
                foreach (E.Point pt in selectedPoint)
                {
                    if (_WorkingStationLevel._MeasureOfLevel.FindIndex(x => x._Point._Name == pt._Name && x._Point._Origin == pt._Origin) == -1)
                    {
                        cleanedSelectedPoint.Add(pt);
                    }
                }
                return cleanedSelectedPoint;
            }

            /// <summary>
            /// Lorsqu'on change les options de cala de points d�j� existant dans les modules guid�s
            /// </summary>
            /// <param name="pointList"></param>
            internal void UpdateCalaVSPointToBeAligned(CloneableList<E.Point> pointList)
            {
                foreach (E.Point pt in pointList)
                {
                    int indexAller = _AllerStationLevel._MeasureOfLevel.FindIndex(x => x._PointName == pt._Name && x._Point._Origin == pt._Origin);
                    if (indexAller != -1) _AllerStationLevel._MeasureOfLevel[indexAller]._Point.LGCFixOption = pt.LGCFixOption;
                    int indexRetour = _RetourStationLevel._MeasureOfLevel.FindIndex(x => x._PointName == pt._Name && x._Point._Origin == pt._Origin);
                    if (indexRetour != -1) _RetourStationLevel._MeasureOfLevel[indexAller]._Point.LGCFixOption = pt.LGCFixOption;
                    foreach (Station reprise in _RepriseStationLevel)
                    {
                        int indexReprise = reprise._MeasureOfLevel.FindIndex(x => x._PointName == pt._Name && x._Point._Origin == pt._Origin);
                        if (indexReprise != -1) reprise._MeasureOfLevel[indexAller]._Point.LGCFixOption = pt.LGCFixOption;
                    }
                }
            }

            //internal bool CheckOperationAndTeam()
            ////V�rifie si un num�ro d'op�ration et une �quipe sont indiqu�s. Ne permet pas la sauvegarde si pas �quipe
            //{
            //    bool check = true;
            //    if (!this._WorkingStationLevel._Parameters._Operation.IsSet)
            //    {
            //        check = this.View.ShowMessageForOperationID();
            //    }
            //    if (this._WorkingStationLevel._Parameters._Team == "UnknownTeam")
            //    {
            //        check = this.View.ShowMessageForTeam();
            //    }
            //    return check;
            //}

            /// <summary>
            /// calcule les �carts, r�sidus, emq en fonction du hstation calcul�
            /// </summary>
            public void TheoReadingAndResidualsCalculation()
            {
                double residualsSum = 0;
                int nombrePointCala = 0;
                foreach (M.MeasureOfLevel meas in _WorkingStationLevel._MeasureOfLevel)
                {
                    double hTheo;
                    // ne fait pas de calcul si level reading n'est pas entr�e
                    if (meas._RawLevelReading != na)
                    {
                        meas._Hmes = _WorkingStationLevel._Parameters._HStation - meas._CorrLevelReadingForEtalonnage - meas._Extension;
                    }
                    else
                    {
                        meas._Hmes = na;
                        meas._TheoReading = na;
                        meas._Residual = na;
                    }
                    ///Affiche les h theo m�me si la mesure n'est pas entr�e
                    E.Point ptTheo = _TheoPoint.Find(x => x._Name == meas._PointName);
                    switch (_WorkingStationLevel._Parameters._ZType)
                    {
                        case CoordinatesType.CCS:
                            hTheo = ptTheo?._Coordinates?.Ccs?.Z?.Value ?? Tsunami2.Preferences.Values.na;
                            break;
                        case CoordinatesType.SU:
                            hTheo = ptTheo?._Coordinates?.Local?.Z?.Value ?? Tsunami2.Preferences.Values.na;
                            break;
                        //case CoordinatesType.UserDefined:
                        //    hTheo = ptTheo?._Coordinates?.UserDefined?.Z?.Value ?? TSU.Tsunami2.Preferences.Values.na;
                        //    break;
                        default:
                            hTheo = ptTheo?._Coordinates?.Ccs?.Z?.Value ?? Tsunami2.Preferences.Values.na;
                            break;
                    }
                    if (hTheo != Tsunami2.Preferences.Values.na)
                    {
                        meas._TheoReading = _WorkingStationLevel._Parameters._HStation - hTheo - meas._Extension + (meas._RawLevelReading - meas._CorrLevelReadingForEtalonnage);
                        if (meas._Point.LGCFixOption == LgcPointOption.CALA && !(meas._Status is M.States.Bad))
                        {
                            meas._Residual = meas._CorrLevelReadingForEtalonnage - meas._TheoReading;
                            nombrePointCala++;
                            residualsSum += Math.Pow(meas._Residual, 2);
                        }
                        else
                        {
                            meas._Residual = 0;
                        }
                    }
                    else
                    {
                        meas._TheoReading = na;
                        meas._Residual = na;
                    }
                }
                //calcul de l'Emq de la station.
                if (nombrePointCala > 1)
                {
                    _WorkingStationLevel._Parameters._EmqCala = Math.Sqrt(residualsSum) / (nombrePointCala - 1);
                }
                else
                {
                    _WorkingStationLevel._Parameters._EmqCala = na;
                }
            }

            /// <summary>
            /// Calcul des Ecart Aller et retour et des r�sidus + emq aller et retour
            /// </summary>
            private void EcartAndEmqAllerRetourCalculation()
            {
                int numberEcartAller = 0;
                double moyEcartAller = 0;
                int numberEcartRetour = 0;
                double moyEcartRetour = 0;
                double residualAllerSum = 0;
                double residualRetourSum = 0;

                foreach (M.MeasureOfLevel meas in _WorkingStationLevel._MeasureOfLevel)
                {
                    if (meas._RawLevelReading == na || meas._Status is M.States.Bad)
                    {
                        meas._EcartAller = na;
                        meas._EcartRetour = na;
                    }
                    else
                    {
                        bool IsMatchingMeasure(M.MeasureOfLevel x)
                        {
                            // V�rifie �galement que la mesure correspondante n'est pas en BAD
                            return x._PointName == meas._PointName
                                   && x._RawLevelReading != na
                                   && !(x._Status is M.States.Bad);
                        }

                        M.MeasureOfLevel allerMeas = _AllerStationLevel._MeasureOfLevel.Find(IsMatchingMeasure);
                        if (allerMeas != null)
                        {
                            numberEcartAller++;
                            meas._EcartAller = allerMeas._CorrLevelReadingForEtalonnage - meas._CorrLevelReadingForEtalonnage;
                            moyEcartAller += meas._EcartAller;
                        }
                        else
                        {
                            meas._EcartAller = na;
                        }

                        M.MeasureOfLevel retourMeas = _RetourStationLevel._MeasureOfLevel.Find(IsMatchingMeasure);
                        if (retourMeas != null)
                        {
                            numberEcartRetour += 1;
                            meas._EcartRetour = retourMeas._CorrLevelReadingForEtalonnage - meas._CorrLevelReadingForEtalonnage;
                            moyEcartRetour += meas._EcartRetour;
                        }
                        else
                        {
                            meas._EcartRetour = na;
                        }
                    }
                }

                //calcul de la moyenne des �carts aller
                if (numberEcartAller > 0)
                {
                    moyEcartAller /= numberEcartAller;
                }
                if (numberEcartRetour > 0)
                {
                    moyEcartRetour /= numberEcartRetour;
                }

                //Calcul des r�sidus et emq
                foreach (M.MeasureOfLevel meas in _WorkingStationLevel._MeasureOfLevel)
                {
                    if (meas._EcartAller != na)
                    {
                        meas._ResidualAller = moyEcartAller - meas._EcartAller;
                        residualAllerSum += Math.Pow(meas._ResidualAller, 2);
                    }
                    else
                    {
                        meas._ResidualAller = na;
                    }
                    if (meas._EcartRetour != na)
                    {
                        meas._ResidualRetour = moyEcartRetour - meas._EcartRetour;
                        residualRetourSum += Math.Pow(meas._ResidualRetour, 2);
                    }
                    else
                    {
                        meas._EcartRetour = na;
                    }
                }

                //Calcul EmqAller
                if (numberEcartAller > 1)
                {
                    _WorkingStationLevel._Parameters._EmqAller = Math.Sqrt(residualAllerSum / (numberEcartAller - 1));
                }
                else
                {
                    _WorkingStationLevel._Parameters._EmqAller = na;
                }

                //Calcul Emq Retour
                if (numberEcartRetour > 1)
                {
                    _WorkingStationLevel._Parameters._EmqRetour = Math.Sqrt(residualRetourSum / (numberEcartRetour - 1));
                }
                else
                {
                    _WorkingStationLevel._Parameters._EmqRetour = na;
                }
            }

            private bool FindPointInAllMeasure(E.Point point)
            //Recherche si un point existe dans toutes les mesures aller-retour-reprise
            {
                bool findPoint = false;
                foreach (M.MeasureOfLevel measLevel in _AllerStationLevel._MeasureOfLevel)
                {
                    if (measLevel._PointName == point._Name && measLevel._Point._Origin == point._Origin) { findPoint = true; }
                }
                foreach (M.MeasureOfLevel measLevel in _RetourStationLevel._MeasureOfLevel)
                {
                    if (measLevel._PointName == point._Name && measLevel._Point._Origin == point._Origin) { findPoint = true; }
                }
                foreach (Station stationLevel in _RepriseStationLevel)
                {
                    foreach (M.MeasureOfLevel measLevel in stationLevel._MeasureOfLevel)
                    {
                        if (measLevel._PointName == point._Name && measLevel._Point._Origin == point._Origin) { findPoint = true; }
                    }
                }
                return findPoint;
            }


            /// <summary>
            /// V�rifie qu'il est possible de lancer le calcul
            /// </summary>
            /// <returns></returns>
            public bool CheckIfCalculationPossible()
            {
                if (ParentModule is Smart.Module sm)
                {
                    // To prevent calculation when the SmartModule is not yet reloaded
                    if (sm.ViewModel?.SmartModule == null)
                        return false;

                    // R�cup�re les mesures valides
                    List<M.MeasureOfLevel> validMeasures = _WorkingStationLevel._MeasureOfLevel.Where(m => !(m._Status is M.States.Bad)).ToList();

                    // Remove all Cala
                    foreach (M.MeasureOfLevel meas in validMeasures)
                        if (meas._Point.LGCFixOption == LgcPointOption.CALA)
                            meas._Point.LGCFixOption = LgcPointOption.POIN;

                    // Autodetermine a CALA point
                    if (AutoDetermineCala(validMeasures))
                        return true;

                    // In Smart Levellimg, it's possible to have no cala point
                    // So we determine the station by a mean of all points measured both in the current station and a previous one
                    Dictionary<string, double> hmesForPoint = GetMeanHMes(sm);

                    // Check if point is in current measures
                    foreach (M.MeasureOfLevel m in validMeasures)
                        if (m._RawLevelReading != na && hmesForPoint.ContainsKey(m._PointName))
                            return true;
                }
                else
                {
                    // V�rifie que toutes les mesures en cala sont entr�es et ont un H theo avant de lancer le calcul
                    bool foundCalaPt = false;
                    foreach (M.MeasureOfLevel meas in _WorkingStationLevel._MeasureOfLevel)
                        if (!(meas._Status is M.States.Bad) && meas._Point.LGCFixOption == LgcPointOption.CALA)
                        {
                            foundCalaPt = true;
                            if (!IsOkForCalaPoint(meas))
                                return false;
                        }
                    return foundCalaPt;
                }

                return false;
            }

            private bool AutoDetermineCala(List<M.MeasureOfLevel> validMeasures)
            {
                foreach (M.MeasureOfLevel meas in validMeasures)
                    if (IsOkForCalaPoint(meas))
                    {
                        meas._Point.LGCFixOption = LgcPointOption.CALA;
                        return true;
                    }

                return false;
            }

            public Dictionary<string, double> GetMeanHMes(Smart.Module sm)
            {
                // Extract the computed height of points in previous stations
                Dictionary<string, List<double>> allHmesForPoint = new Dictionary<string, List<double>>();
                foreach (Smart.StationViewModel s in sm.ViewModel.AllStations)
                {
                    // Continue to use all other stations, break to use the the previous stations
                    // We are not yet 100% sure which one is the best
                    if (s.StationModule == this)
                        break;
                    foreach (Station r in s.AllRounds)
                        if (!(r._Parameters._State is State.Bad))
                            foreach (M.MeasureOfLevel m in r._MeasureOfLevel)
                                if (!m._Hmes.IsNa())
                                    if (allHmesForPoint.ContainsKey(m._PointName))
                                        allHmesForPoint[m._PointName].Add(m._Hmes);
                                    else
                                        allHmesForPoint.Add(m._PointName, new List<double> { m._Hmes });
                }
                // Compute the mean values
                Dictionary<string, double> meanHmesForPoint = new Dictionary<string, double>();
                foreach (KeyValuePair<string, List<double>> kvp in allHmesForPoint)
                {
                    double mean = kvp.Value.Mean();
                    Debug.WriteInConsole($"In GetMeanHMes pointName={kvp.Key} allHmes={string.Join(",", kvp.Value)} mean={mean}");
                    meanHmesForPoint.Add(kvp.Key, mean);
                }

                return meanHmesForPoint;
            }

            private bool IsOkForCalaPoint(M.MeasureOfLevel meas)
            {
                // A cala point must have a reading
                if (meas._RawLevelReading == na)
                    return false;

                // A cala point must be linked to a theoretical point
                E.Point ptTheo = _TheoPoint.Find(x => x._Name == meas._Point._Name);
                if (ptTheo == null)
                    return false;

                // And this theoretical point must have coordinates in the system used
                switch (_WorkingStationLevel._Parameters._ZType)
                {
                    case CoordinatesType.CCS:
                        return ptTheo._Coordinates.Ccs.Z != null
                            && ptTheo._Coordinates.Ccs.Z.Value != Tsunami2.Preferences.Values.na;
                    case CoordinatesType.SU:
                        return ptTheo._Coordinates.Local.Z != null
                            && ptTheo._Coordinates.Local.Z.Value != Tsunami2.Preferences.Values.na;
                    default:
                        return false;
                }
            }

            internal bool CheckIfReadyToBeSaved(Station stationLevel)
            {
                bool a = stationLevel._Parameters._Team != R.String_UnknownTeam;
                bool b = stationLevel._Parameters._Instrument != null;
                bool c = stationLevel._MeasureOfLevel.Count != 0;
                bool d = stationLevel._MeasureOfLevel.FindIndex(x => x._RawLevelReading != na) != -1;
                bool f = stationLevel.ParametersBasic.LastChanged > stationLevel.ParametersBasic.LastSaved;

                if (a && b && c && d && f)
                {
                    stationLevel._Parameters._State = new State.StationLevelReadyToBeSaved();
                    return true;
                }
                else
                {
                    if (a && b && c && d)
                    {
                        stationLevel._Parameters._State = new State.StationLevelSaved();
                        return false;
                    }
                    if (a && b && c)
                    {
                        stationLevel._Parameters._State = new State.MeasureToEnter();
                        return false;
                    }
                }
                return false;
            }
            /// <summary>
            /// Check if the station can be saved and update the state of the working station level
            /// </summary>
            internal bool CheckIfReadyToBeSaved()
            {
                return CheckIfReadyToBeSaved(_WorkingStationLevel);
            }
            internal void ReverseMeasureStatus(string pointName)
            {
                _LevelingDirectionStrategy.ReverseMeasureStatus(pointName, this);

            }
            /// <summary>
            /// Fait la diff�rence de cote bleue (contr�le de marche) entre la station actuelle et le stationLevelModule pr�c�dent 
            /// Fait �ventuellement la diff�rence de cote bleue (contr�le de marche) entre la station actuelle et le stationLevelModule suivant
            /// </summary>
            /// <param name="prevStation"></param>
            /// <param name="nextStation">optionnel</param>
            internal string CheckBlueElevation(Module prevStation, Module nextStation, int actualStationNumber, bool showCancelButton = false)
            {
                List<string> ptNameList = new List<string>();
                List<string> diffStprevA = new List<string>();
                List<string> diffStprevR = new List<string>();
                List<string> diffStprevP = new List<string>();
                List<string> diffStnextA = new List<string>();
                List<string> diffStnextR = new List<string>();
                List<string> diffStnextP = new List<string>();
                bool showResultWindow = false;
                if (_WorkingStationLevel != null)
                {
                    foreach (M.MeasureOfLevel meas in _WorkingStationLevel._MeasureOfLevel)
                    {

                        if (meas._RawLevelReading != na)
                        {
                            int readStRef = Convert.ToInt32(meas._CorrLevelReadingForEtalonnage * 100000 + meas._Extension * 100000);
                            ptNameList.Add(meas._Point._Name);
                            if (prevStation != null)
                            {
                                ///Compare avec station aller pr�c�dente
                                if (prevStation._AllerStationLevel != null)
                                {
                                    int index = prevStation._AllerStationLevel._MeasureOfLevel.FindIndex(x => x._Point._Name == meas._Point._Name && x._RawLevelReading != na);
                                    if (index != -1)
                                    {
                                        int readSt2 = Convert.ToInt32(prevStation._AllerStationLevel._MeasureOfLevel[index]._CorrLevelReadingForEtalonnage * 100000
                                            + prevStation._AllerStationLevel._MeasureOfLevel[index]._Extension * 100000);
                                        diffStprevA.Add((readStRef - readSt2).ToString());
                                        showResultWindow = true;
                                    }
                                    else
                                    {
                                        diffStprevA.Add("");
                                    }
                                }
                                else
                                {
                                    diffStprevA.Add("");
                                }
                                ///Compare avec station retour pr�c�dente
                                if (prevStation._RetourStationLevel != null)
                                {
                                    int index = prevStation._RetourStationLevel._MeasureOfLevel.FindIndex(x => x._Point._Name == meas._Point._Name && x._RawLevelReading != na);
                                    if (index != -1)
                                    {
                                        int readSt2 = Convert.ToInt32(prevStation._RetourStationLevel._MeasureOfLevel[index]._CorrLevelReadingForEtalonnage * 100000
                                            + prevStation._RetourStationLevel._MeasureOfLevel[index]._Extension * 100000);
                                        diffStprevR.Add((readStRef - readSt2).ToString());
                                        showResultWindow = true;
                                    }
                                    else
                                    {
                                        diffStprevR.Add("");
                                    }
                                }
                                else
                                {
                                    diffStprevR.Add("");
                                }
                                ///Compare avec station reprise 1 pr�c�dente
                                if (prevStation._RepriseStationLevel.Count != 0)
                                {
                                    if (prevStation._RepriseStationLevel[0] != null)
                                    {
                                        int index = prevStation._RepriseStationLevel[0]._MeasureOfLevel.FindIndex(x => x._Point._Name == meas._Point._Name && x._RawLevelReading != na);
                                        if (index != -1)
                                        {
                                            int readSt2 = Convert.ToInt32(prevStation._RepriseStationLevel[0]._MeasureOfLevel[index]._CorrLevelReadingForEtalonnage * 100000
                                                + prevStation._RepriseStationLevel[0]._MeasureOfLevel[index]._Extension * 100000);
                                            diffStprevP.Add((readStRef - readSt2).ToString());
                                            showResultWindow = true;
                                        }
                                        else
                                        {
                                            diffStprevP.Add("");
                                        }
                                    }
                                    else
                                    {
                                        diffStprevP.Add("");
                                    }
                                }
                                else
                                {
                                    diffStprevP.Add("");
                                }
                            }
                            else
                            {
                                diffStprevA.Add("");
                                diffStprevR.Add("");
                                diffStprevP.Add("");
                            }
                            if (nextStation != null)
                            {
                                ///Compare avec station aller suivante
                                if (nextStation._AllerStationLevel != null)
                                {
                                    int index = nextStation._AllerStationLevel._MeasureOfLevel.FindIndex(x => x._Point._Name == meas._Point._Name && x._RawLevelReading != na);
                                    if (index != -1)
                                    {
                                        int readSt2 = Convert.ToInt32(nextStation._AllerStationLevel._MeasureOfLevel[index]._CorrLevelReadingForEtalonnage * 100000
                                            + nextStation._AllerStationLevel._MeasureOfLevel[index]._Extension * 100000);
                                        diffStnextA.Add((readStRef - readSt2).ToString());
                                        showResultWindow = true;
                                    }
                                    else
                                    {
                                        diffStnextA.Add("");
                                    }
                                }
                                else
                                {
                                    diffStnextA.Add("");
                                }
                                ///Compare avec station retour pr�c�dente
                                if (nextStation._RetourStationLevel != null)
                                {
                                    int index = nextStation._RetourStationLevel._MeasureOfLevel.FindIndex(x => x._Point._Name == meas._Point._Name && x._RawLevelReading != na);
                                    if (index != -1)
                                    {
                                        int readSt2 = Convert.ToInt32(nextStation._RetourStationLevel._MeasureOfLevel[index]._CorrLevelReadingForEtalonnage * 100000
                                            + nextStation._RetourStationLevel._MeasureOfLevel[index]._Extension * 100000);
                                        diffStnextR.Add((readStRef - readSt2).ToString());
                                        showResultWindow = true;
                                    }
                                    else
                                    {
                                        diffStnextR.Add("");
                                    }
                                }
                                else
                                {
                                    diffStnextR.Add("");
                                }
                                ///Compare avec station reprise 1 pr�c�dente
                                if (nextStation._RepriseStationLevel.Count != 0)
                                {
                                    if (nextStation._RepriseStationLevel[0] != null)
                                    {
                                        int index = nextStation._RepriseStationLevel[0]._MeasureOfLevel.FindIndex(x => x._Point._Name == meas._Point._Name && x._RawLevelReading != na);
                                        if (index != -1)
                                        {
                                            int readSt2 = Convert.ToInt32(nextStation._RepriseStationLevel[0]._MeasureOfLevel[index]._CorrLevelReadingForEtalonnage * 100000
                                                + nextStation._RepriseStationLevel[0]._MeasureOfLevel[index]._Extension * 100000);
                                            diffStnextP.Add((readStRef - readSt2).ToString());
                                            showResultWindow = true;
                                        }
                                        else
                                        {
                                            diffStnextP.Add("");
                                        }
                                    }
                                    else
                                    {
                                        diffStnextP.Add("");
                                    }
                                }
                                else
                                {
                                    diffStnextP.Add("");
                                }
                            }
                            else
                            {
                                diffStnextA.Add("");
                                diffStnextR.Add("");
                                diffStnextP.Add("");
                            }
                        }

                    }
                    if (View != null && showResultWindow)
                    {
                        string title = "";
                        if (_WorkingStationLevel._Parameters._LevelingDirection == LevelingDirection.ALLER) title
                                = string.Format(R.StringDataGridBlueElevation_Title, actualStationNumber) + "\r\n"
                                + string.Format(R.StringDataGridBlueElevation_TitleOutward);
                        if (_WorkingStationLevel._Parameters._LevelingDirection == LevelingDirection.RETOUR)
                        {
                            string emqAller = R.T_NA;
                            if (_RetourStationLevel._Parameters._EmqAller != na)
                            {
                                emqAller = Math.Round(_RetourStationLevel._Parameters._EmqAller * 1000, 2).ToString() + " mm";
                            }
                            title = string.Format(R.StringDataGridBlueElevation_Title, actualStationNumber) + "\r\n"
                                  + string.Format(R.StringDataGridBlueElevation_TitleReturn, emqAller);
                        }
                        if (_WorkingStationLevel._Parameters._LevelingDirection == LevelingDirection.REPRISE)
                        {
                            string emqAller = R.T_NA;
                            if (_RetourStationLevel._Parameters._EmqAller != na)
                            {
                                emqAller = Math.Round(_RetourStationLevel._Parameters._EmqAller * 1000, 2).ToString() + " mm";
                            }
                            string emqRetour = R.T_NA;
                            if (_RepriseStationLevel[0]._Parameters._EmqRetour != na)
                            {
                                emqRetour = Math.Round(_RepriseStationLevel[0]._Parameters._EmqRetour * 1000, 2).ToString() + " mm";
                            }
                            title = string.Format(R.StringDataGridBlueElevation_Title, actualStationNumber) + "\r\n"
                                  + string.Format(R.StringDataGridBlueElevation_TitleReturn, emqAller) + "\r\n"
                                  + string.Format(R.StringDataGridBlueElevation_TitleRepeat, emqRetour);
                        }
                        return View.ShowMessageOfBlueElevationLevelInDatagrid(title,
                            R.T_OK,
                            ptNameList,
                            diffStprevA,
                            diffStprevR,
                            diffStprevP,
                            diffStnextA,
                            diffStnextR,
                            diffStnextP,
                            actualStationNumber,
                            showCancelButton);
                    }
                }
                return "";
            }
            /// <summary>
            /// Use the Hstation calculated during the global LGC2 calculation for that station
            /// </summary>
            /// <param name="use"></param>
            //internal void UseHGlobalLGCCalculation(bool use)
            //{
            //    this._WorkingStationLevel._Parameters.HStationLocked = use;
            //    if (use==true)
            //    {
            //        List<E.Point> pointList = this.ElementModule.GetPointsInAllElements();
            //        if (pointList!=null)
            //        {
            //            int index = pointList.FindIndex(x=>x._Name==this._WorkingStationLevel._Name);
            //            if (index!=-1)
            //            {
            //                double Hstation = -9999;
            //                switch (this._WorkingStationLevel._Parameters._ZType)
            //                {
            //                    case CoordinatesType.CCS:
            //                        Hstation = pointList[index]._Coordinates.Ccs.H.Value;
            //                        this._WorkingStationLevel._Parameters._HStation = Hstation;
            //                        break;
            //                    case CoordinatesType.SU:
            //                        Hstation = pointList[index]._Coordinates.Su.Z.Value;
            //                        this._WorkingStationLevel._Parameters._HStation = Hstation;
            //                        break;
            //                    default:
            //                        break;
            //                }
            //            }
            //        }
            //    }
            //    this.UpdateOffsets();
            //}
            #endregion

            #region strategies

            internal void ChangeLevelingDirectionToAller()
            //change la strat�gie en aller et fait une r�f�rence pour la station nivellement actuelle vers la station aller
            {
                _LevelingDirectionStrategy = new AllerStrategy();
                _LevelingDirectionStrategy.ChangeLevellingDirection(this);
                View?.ChangeLevelingDirectionToAller();
            }
            internal void ChangeLevelingDirectionToRetour()
            //change la strat�gie en retour et fait une r�f�rence pour la station nivellement actuelle vers la station retour
            {
                //cr�ation de la station retour � partir station aller la premi�re fois
                if (!retourDone)
                {
                    _RetourStationLevel = _AllerStationLevel.DeepCopy();
                    _RetourStationLevel.CleanAllMeasure();
                    _RetourStationLevel._Parameters._LevelingDirection = LevelingDirection.RETOUR;
                    _RetourStationLevel.ParametersBasic._GeodeDatFilePath = "";
                    _RetourStationLevel.ParametersBasic._Instrument = _AllerStationLevel.ParametersBasic._Instrument;
                    _RetourStationLevel.ParametersBasic.LastChanged = DateTime.MinValue;
                    _RetourStationLevel.ParametersBasic.LastSaved = DateTime.MinValue;
                    _RetourStationLevel.specialPartOfStationName = _AllerStationLevel.specialPartOfStationName;
                    retourDone = true;
                }
                _LevelingDirectionStrategy = new RetourStrategy();
                _LevelingDirectionStrategy.ChangeLevellingDirection(this);
                View?.ChangeLevelingDirectionToRetour();
            }
            internal void ChangeLevelingDirectionToReprise(int numeroReprise)
            //change la strat�gie en reprise et fait une r�f�rence pour la station nivellement actuelle vers le num�ro de la reprise
            {
                _LevelingDirectionStrategy = new RepriseStrategy();
                _LevelingDirectionStrategy.ChangeLevellingDirection(this, numeroReprise);
                _NumeroReprise = numeroReprise;
                View?.ChangeLevelingDirectionToReprise(numeroReprise);
            }

            #endregion

            /// <summary>
            /// Ajout un point cr�e par l'utilisateur qui est d�j� existant dans l'element module
            /// </summary>
            /// <param name="newPoint"></param>
            internal void SetNewPointAlreadyExisting(E.Point newPoint)
            {
                if (_TheoPoint.FindIndex(x => x._Name == newPoint._Name) == -1)
                {
                    _TheoPoint.Add(newPoint.DeepCopy());
                    //this._MeasPoint.Add(newPoint.DeepCopy());
                    _AllerStationLevel.UpdateListMeasureLevel(this, newPoint);
                    _RetourStationLevel.UpdateListMeasureLevel(this, newPoint);
                    foreach (Station reprise in _RepriseStationLevel)
                    {
                        reprise.UpdateListMeasureLevel(this, newPoint);
                    }
                    SetTheoPointsInElementManager();
                    ElementModule.ValidateSelection();
                }
            }

            /// <summary>
            /// ask the view to show interface choices and then trigger or not the interface cahnge in the module
            /// </summary>
            /// <param name="measure"></param>
            internal void ChangeInterfaces(M.MeasureOfLevel measure)
            {
                View.SelectInterfaces(
                    previousInterFaces: measure.Interfaces,
                    currenObject: measure,
                    model: _AllerStationLevel,
                    fan: _WorkingStationLevel,
                    additionalAction: UpdateView);
            }

            /// <summary>
            /// Replace the interface in a meaure
            /// </summary>
            /// <param name="currenObject"></param>
            /// <param name="newInterFaces"></param>
            internal override void ChangeInterfacesBy(TsuObject currenObject, Interfaces newInterFaces)
            {
                var measure = currenObject as M.MeasureOfLevel;
                measure.Interfaces = newInterFaces;
            }

            internal Station GetStationLevelByGuid(string guid)
            {
                Guid toFind = Guid.Parse(guid);
                return AllStationLevels.FirstOrDefault(st => st.Guid.Equals(toFind));
            }

            internal void SetWorkingStationLevelByGuid(string guid)
            {
                Guid toFind = Guid.Parse(guid);
                int stationLevelIndex = 0;
                foreach (var st in AllStationLevels)
                {
                    if (st.Guid.Equals(toFind))
                    {
                        switch (stationLevelIndex)
                        {
                            case 0:
                                ChangeLevelingDirectionToAller();
                                break;
                            case 1:
                                ChangeLevelingDirectionToRetour();
                                break;
                            default:
                                // Tricky : the 3rd station has index 2 and is the 1st reprise, hence the -1
                                ChangeLevelingDirectionToReprise(stationLevelIndex - 1);
                                break;
                        }
                        return;
                    }
                    stationLevelIndex++;
                }
            }

            public void ChangeMeasure(string pointName, double rawLevelReading)
            {
                Debug.WriteInConsole($"pointName={pointName}, rawLevelReading= {rawLevelReading}");

                M.MeasureOfLevel mesureEntree = _WorkingStationLevel._MeasureOfLevel.Find(x => x._PointName == pointName);

                Debug.WriteInConsole($"mesureEntree._RawLevelReading={mesureEntree._RawLevelReading}, mesureEntree._Status={mesureEntree._Status}");

                //if (mesureEntree._RawLevelReading != rawLevelReading) // I dont know why is what checked, but with BOC obsolete functionnality it is do not work if we cannot enter 2 time the same value
                {
                    _WorkingStationLevel.CopyAsObsolete(mesureEntree);
                    mesureEntree._RawLevelReading = rawLevelReading;
                    mesureEntree._Date = DateTime.Now;
                    mesureEntree._staff._SpatialDistanceTargetHeight = mesureEntree._RawLevelReading;
                    _WorkingStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                    UpdateOffsets();
                }
            }

            public void InitializeMeasure(string pointName)
            {
                Debug.WriteInConsole($"pointName={pointName}");

                M.MeasureOfLevel mesureEntree = _WorkingStationLevel._MeasureOfLevel.Find(x => x._PointName == pointName);

                Debug.WriteInConsole($"mesureEntree._RawLevelReading={mesureEntree._RawLevelReading}, mesureEntree._Status={mesureEntree._Status}");

                //if (mesureEntree._RawLevelReading != na) // I dont know why is what checked, but with BOC obsolete functionnality it is do not work if we cannot enter 2 time the same value
                {
                    _WorkingStationLevel.CopyAsObsolete(mesureEntree);
                    mesureEntree._RawLevelReading = na;
                    mesureEntree._CorrLevelReadingForEtalonnage = na;
                    mesureEntree._CorrLevelReadingForZero = na;
                    mesureEntree._Hmes = na;
                    mesureEntree._EcartAller = na;
                    mesureEntree._EcartRetour = na;
                    mesureEntree._Residual = na;
                    mesureEntree._ResidualAller = na;
                    mesureEntree._ResidualRetour = na;
                    mesureEntree._TheoReading = na;
                    mesureEntree._Date = DateTime.MinValue;
                    mesureEntree._staff._SpatialDistanceTargetHeight = mesureEntree._RawLevelReading;
                    _WorkingStationLevel.ParametersBasic.LastChanged = DateTime.Now;
                    UpdateOffsets();
                }
            }
        }
    }
}
