﻿using MathNet.Numerics.Statistics;
using System.Collections.Generic;
using System.Linq;
using M = TSU.Common.Measures;

namespace TSU.Level.Compute
{
    public class RoundToRoundRms
    {
        public int Round1 { get; set; }

        public int Round2 { get; set; }

        public double Rms { get; set; }

        public RoundToRoundRms() { }

        private static double Na => Tsunami2.Preferences.Values.na;

        public static void Compute(List<Station> rounds, out List<RoundToRoundRms> allR2RRms,
            out RoundToRoundRms bestR2RRms, out double? averageRms)
        {
            // Init out variables
            allR2RRms = new List<RoundToRoundRms>();
            bestR2RRms = null;
            averageRms = null;

            // Do nothing if there are less than 2 rounds
            if (rounds.Count < 2)
                return;

            // Extract the list of useful measure, once per rounds
            List<Dictionary<string, double>> allMeas = new List<Dictionary<string, double>>();
            foreach (Station round in rounds)
                allMeas.Add(round._MeasureOfLevel.Where(m => HasValidReading(m)).ToDictionary(m => m._PointName, m => m._RawLevelReading));

            //to compute the average RMS
            double totalRms = 0;
            int numberOf22RRms = 0;

            // Compute the stations' measures and keep best
            for (int ir1 = 0; ir1 < allMeas.Count; ir1++)
                for (int ir2 = ir1 + 1; ir2 < allMeas.Count; ir2++)
                {
                    double rms = StdDevOfAllDeltas(allMeas[ir1], allMeas[ir2]);
                    if (!rms.IsNa())
                    {
                        RoundToRoundRms newR2RRms = new RoundToRoundRms()
                        {
                            Round1 = ir1,
                            Round2 = ir2,
                            Rms = rms
                        };
                        //Add to the list of RMS
                        allR2RRms.Add(newR2RRms);

                        //For the average RMS calculation
                        totalRms += rms;
                        numberOf22RRms++;
                        //Check if it's the best round-to-round RMS
                        if (bestR2RRms == null || rms < bestR2RRms.Rms)
                            bestR2RRms = newR2RRms;
                    }
                }

            //Compute the average RMS
            if (numberOf22RRms == 0)
                averageRms = null;
            else
                averageRms = totalRms / numberOf22RRms;
        }

        private static bool HasValidReading(M.MeasureOfLevel m)
        {
            return !m._RawLevelReading.IsNa() && !(m._Status is M.States.Bad);
        }

        private static double StdDevOfAllDeltas(Dictionary<string, double> s1meas, Dictionary<string, double> s2Meas)
        {
            List<double> allDeltas = AllDeltas(s1meas, s2Meas).ToList();
            double stdDev = allDeltas.StandardDeviation();
            return double.IsNaN(stdDev) ? Na : stdDev;
        }

        private static IEnumerable<double> AllDeltas(Dictionary<string, double> s1meas, Dictionary<string, double> s2Meas)
        {
            foreach (KeyValuePair<string, double> m1 in s1meas)
                if (s2Meas.TryGetValue(m1.Key, out double m2))
                    yield return m2 - m1.Value;
        }

    }
}
