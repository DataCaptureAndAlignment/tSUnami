﻿namespace TSU.Level.Compute
{
    public class CommonPoint
    {
        public string PointName { get; set; }
        public double MeanVsStation1Aller { get; set; }
        public double MeanVsStation1Retour { get; set; }
        public double MeanVsStation2Aller { get; set; }
        public double MeanVsStation2Retour { get; set; }
    }
}


