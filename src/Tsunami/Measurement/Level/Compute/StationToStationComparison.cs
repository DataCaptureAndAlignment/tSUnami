﻿using MathNet.Numerics.Statistics;
using System.Collections.Generic;
using System.Linq;
using M = TSU.Common.Measures;

namespace TSU.Level.Compute
{
    public partial class StationToStationComparison
    {
        public int Station1 { get; set; }

        public int Station2 { get; set; }

        public List<CommonPoint> CommonPoints { get; set; }

        private static double Na => Tsunami2.Preferences.Values.na;

        internal struct MeasureList
        {
            internal Dictionary<string, double> Aller;
            internal Dictionary<string, double> Retour;
            internal HashSet<string> AllPointNames;

            public MeasureList(IEnumerable<M.MeasureOfLevel> aller, IEnumerable<M.MeasureOfLevel> retour) : this()
            {
                Aller = aller.ToDictionary(KeySelector, ElementSelector);
                Retour = retour.ToDictionary(KeySelector, ElementSelector);
                AllPointNames = Concat.Select(Selector).ToHashSet();
            }

            private static string KeySelector(M.MeasureOfLevel m) => m._PointName;

            private static double ElementSelector(M.MeasureOfLevel m) => HasValidHmes(m) ? m._Hmes : double.NaN;

            internal IEnumerable<KeyValuePair<string, double>> Concat => Aller.Concat(Retour);

            private static string Selector(KeyValuePair<string, double> m) => m.Key;
        }

        public static IEnumerable<StationToStationComparison> Compute(IEnumerable<Station.Module> stations)
        {
            // Extract the list of useful measures
            List<MeasureList> allMeas = ExtractMeasures(stations).ToList();

            // Compute the mean measures for each point which is in at least two consecutive stations
            Dictionary<string, double> meanHeight = ExtractMeanHeights(allMeas);

            // Compute the common points
            for (int ir1 = 0; ir1 < allMeas.Count - 1; ir1++)
                yield return new StationToStationComparison()
                {
                    Station1 = ir1,
                    Station2 = ir1 + 1,
                    CommonPoints = CompareStations(allMeas[ir1], allMeas[ir1 + 1], meanHeight).ToList()
                };
        }

        private static IEnumerable<MeasureList> ExtractMeasures(IEnumerable<Station.Module> stations)
        {
            foreach (Station.Module station in stations)
            {
                Debug.WriteInConsole($"ExtractMeasures for station {station?._Name}");
                Debug.WriteInConsole($"HStation Aller={station?._AllerStationLevel?._Parameters?._HStation}");
                Debug.WriteInConsole($"HStation Retour={station?._RetourStationLevel?._Parameters?._HStation}");
                MeasureList measureList = new MeasureList(
                                station?._AllerStationLevel?._MeasureOfLevel ?? Enumerable.Empty<M.MeasureOfLevel>(),
                                station?._RetourStationLevel?._MeasureOfLevel ?? Enumerable.Empty<M.MeasureOfLevel>()
                            );
                Debug.WriteInConsole($"Aller :");
                foreach (KeyValuePair<string, double> a in measureList.Aller)
                    Debug.WriteInConsole($"{a.Key}=>{a.Value}");
                Debug.WriteInConsole($"Retour :");
                foreach (KeyValuePair<string, double> r in measureList.Retour)
                    Debug.WriteInConsole($"{r.Key}=>{r.Value}");
                Debug.WriteInConsole($"AllPointNames :");
                foreach(string p in measureList.AllPointNames)
                    Debug.WriteInConsole(p);
                yield return measureList;
            }
        }

        private static bool HasValidHmes(M.MeasureOfLevel m)
        {
            return !m._Hmes.IsNa() && !(m._Status is M.States.Bad);
        }

        private static Dictionary<string, double> ExtractMeanHeights(List<MeasureList> allMeas)
        {
            Debug.WriteInConsole($"Begin ExtractMeanHeights, allMeas={string.Join(",", allMeas)}");
            Dictionary<string, double> meanHeight = new Dictionary<string, double>();

            for (int ir1 = 0; ir1 < allMeas.Count - 1; ir1++)
                foreach (string pointName in allMeas[ir1].AllPointNames)
                    if (!meanHeight.ContainsKey(pointName) && allMeas[ir1 + 1].AllPointNames.Contains(pointName))
                    {
                        IEnumerable<double> allHmes = AllHmesForPoint(pointName, allMeas);
                        double mean = allHmes.Mean();
                        meanHeight.Add(pointName, double.IsNaN(mean) ? Na : mean);
                        Debug.WriteInConsole($"ExtractMeanHeights added ir1={ir1}, pointName={pointName}, mean={mean}, allHmes={string.Join(",", allHmes)}");
                    }

            return meanHeight;
        }

        private static IEnumerable<double> AllHmesForPoint(string point, IEnumerable<MeasureList> allMeas)
        {
            foreach (MeasureList ml in allMeas)
                foreach (KeyValuePair<string, double> m in ml.Concat)
                    if (m.Key == point && !m.Value.IsNa())
                        yield return m.Value;
        }

        private static IEnumerable<CommonPoint> CompareStations(MeasureList s1m, MeasureList s2m, Dictionary<string, double> meanHeight)
        {
            foreach (string pointName in s1m.AllPointNames)
                if (s2m.AllPointNames.Contains(pointName))
                {
                    double mean = meanHeight[pointName];

                    CommonPoint commonPoint = new CommonPoint
                    {
                        PointName = pointName,
                        MeanVsStation1Aller = TryDiff(s1m.Aller, pointName, mean),
                        MeanVsStation1Retour = TryDiff(s1m.Retour, pointName, mean),
                        MeanVsStation2Aller = TryDiff(s2m.Aller, pointName, mean),
                        MeanVsStation2Retour = TryDiff(s2m.Retour, pointName, mean)
                    };
                    yield return commonPoint;
                }
        }

        /// <summary>
        /// Compare the value of the point pointName in measure to mean, or returns Na if there's no valid measure
        /// </summary>
        /// <param name="measure"></param>
        /// <param name="pointName"></param>
        /// <param name="mean"></param>
        /// <returns></returns>
        private static double TryDiff(Dictionary<string, double> measure, string pointName, double mean)
        {
            bool v = measure.TryGetValue(pointName, out double s1a);
            double v1 = v && !s1a.IsNa() ? mean - s1a : Na;
            Debug.WriteInConsole($"TryDiff, pointName={pointName}, mean={mean}, v={v}, s1a={s1a}, v1={v1}");
            return v1;
        }
    }
}


