using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using TSU.Common;
using TSU.Common.Compute.Compensations;
using TSU.Common.Elements;
using TSU.ENUM;
using TSU.Views;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using R = TSU.Properties.Resources;

namespace TSU.Level
{
    [Serializable]
    [XmlType(TypeName = "Level.Module")]
    public class Module : FinalModule
    {
        // Fields
        //public StationLevelModule stationLevelModule;
        public override ObservationType ObservationType { get; set; } = ObservationType.Level;
        public TsuBool ShowSaveMessageOfSuccess { get; set; }
        [XmlIgnore]
        public new CompositeView View
        {
            get
            {
                return this._TsuView as CompositeView;
            }
            set
            {
                this._TsuView = value;
            }
        }
        // Constructor
        public Module() // need a parameterless constructor for xml serialazation
            : base()
        {
            ShowSaveMessageOfSuccess = new TsuBool();
        }

        public Module(TSU.Module parentModule, string nameAndDescription)
            : base(parentModule, nameAndDescription)
        {
            ShowSaveMessageOfSuccess = new TsuBool();
        }

        // Methods

        /// <summary>
        /// Cr�e une nouvelle station module en copiant les param�tres admin de l'ancienne
        /// </summary>
        /// <param name="cloneAdminParam"></param>
        internal void CloneAllerStation()
        {
            if (this._ActiveStationModule is Station.Module oldStationModule)
            {
                Station.Module newStationModule = new Station.Module(this, oldStationModule.CreateViewAutmatically);
                newStationModule.ChangeOperationID(oldStationModule._AllerStationLevel.ParametersBasic._Operation);
                newStationModule.ChangeTeam(oldStationModule._AllerStationLevel.ParametersBasic._Team);
                newStationModule.SetGeodeFilePathInNewStationLevelModule(oldStationModule);
                newStationModule.SetInstrumentInNewStationLevelModule(oldStationModule);
                newStationModule.SetDefaultStaffInNewStationLevelModule(oldStationModule);
                newStationModule.SetSecondaryStaffInNewStationLevelModule(oldStationModule);
                newStationModule.ChangeTolerance(oldStationModule._WorkingStationLevel._Parameters._Tolerance);
                newStationModule.ChangeTemperature(oldStationModule._WorkingStationLevel._Parameters._Temperature);
                newStationModule.SetCoordinateSystemInNewStationLevelModule(oldStationModule);
                newStationModule.CreatedOn = DateTime.Now;
                this.SwitchToANewStationModule(newStationModule);
            }
        }

        /// <summary>
        /// Cr�e une nouvelle station module en copiant les param�tres admin de l'ancienne
        /// </summary>
        /// <param name="cloneAdminParam"></param>
        internal void CloneStationWithTheoPoints()
        {
            if (this._ActiveStationModule is Station.Module oldStationModule)
            {
                Station.Module newStationModule = new Station.Module(this, oldStationModule.CreateViewAutmatically);
                newStationModule.ChangeOperationID(oldStationModule._AllerStationLevel.ParametersBasic._Operation);
                newStationModule.ChangeTeam(oldStationModule._AllerStationLevel.ParametersBasic._Team);
                newStationModule.SetGeodeFilePathInNewStationLevelModule(oldStationModule);
                newStationModule.SetInstrumentInNewStationLevelModule(oldStationModule);
                newStationModule.SetDefaultStaffInNewStationLevelModule(oldStationModule);
                newStationModule.SetSecondaryStaffInNewStationLevelModule(oldStationModule);
                newStationModule.SetOldStationLevelTheoPointIntoNewStationLevelModule(oldStationModule);
                newStationModule.SetOldStaffInToNewStationModule(oldStationModule);
                newStationModule.ChangeTolerance(oldStationModule._WorkingStationLevel._Parameters._Tolerance);
                newStationModule.ChangeTemperature(oldStationModule._WorkingStationLevel._Parameters._Temperature);
                newStationModule.SetCoordinateSystemInNewStationLevelModule(oldStationModule);
                newStationModule.CreatedOn = DateTime.Now;
                this.SwitchToANewStationModule(newStationModule);
            }
        }

        /// <summary>
        /// Clone une station de nivellement en gardant les param�tres admin et les points theo, mais en allant directement au retour
        /// </summary>
        internal void CloneStationForReturnWithTheoPoints()
        {
            if (this._ActiveStationModule is Station.Module oldStationModule)
            {
                Station.Module newStationModule = new Station.Module(this, oldStationModule.CreateViewAutmatically);
                newStationModule.ChangeOperationID(oldStationModule._AllerStationLevel.ParametersBasic._Operation);
                newStationModule.ChangeTeam(oldStationModule._AllerStationLevel.ParametersBasic._Team);
                newStationModule.SetGeodeFilePathInNewStationLevelModule(oldStationModule);
                newStationModule.SetInstrumentInNewStationLevelModule(oldStationModule);
                newStationModule.SetDefaultStaffInNewStationLevelModule(oldStationModule);
                newStationModule.SetSecondaryStaffInNewStationLevelModule(oldStationModule);
                newStationModule.SetOldStationLevelTheoPointIntoNewStationLevelModule(oldStationModule);
                newStationModule.SetOldStaffInToNewStationModule(oldStationModule);
                newStationModule.ChangeLevelingDirectionToRetour();
                newStationModule.ChangeTolerance(oldStationModule._WorkingStationLevel._Parameters._Tolerance);
                newStationModule.ChangeTemperature(oldStationModule._WorkingStationLevel._Parameters._Temperature);
                newStationModule.SetCoordinateSystemInNewStationLevelModule(oldStationModule);
                newStationModule.CreatedOn = DateTime.Now;
                this.SwitchToANewStationModule(newStationModule);
            }
        }

        /// <summary>
        /// Clone une station de nivellement en gardant les param�tres admin sans les points theo, mais en allant directement au retour
        /// </summary>
        internal void CloneRetourStation()
        {
            if (this._ActiveStationModule is Station.Module oldStationModule)
            {
                Station.Module newStationModule = new Station.Module(this, oldStationModule.CreateViewAutmatically);
                newStationModule.ChangeOperationID(oldStationModule._AllerStationLevel.ParametersBasic._Operation);
                newStationModule.ChangeTeam(oldStationModule._AllerStationLevel.ParametersBasic._Team);
                newStationModule.SetGeodeFilePathInNewStationLevelModule(oldStationModule);
                newStationModule.SetInstrumentInNewStationLevelModule(oldStationModule);
                newStationModule.SetDefaultStaffInNewStationLevelModule(oldStationModule);
                newStationModule.SetSecondaryStaffInNewStationLevelModule(oldStationModule);
                newStationModule.ChangeLevelingDirectionToRetour();
                newStationModule.ChangeTolerance(oldStationModule._WorkingStationLevel._Parameters._Tolerance);
                newStationModule.ChangeTemperature(oldStationModule._WorkingStationLevel._Parameters._Temperature);
                newStationModule.SetCoordinateSystemInNewStationLevelModule(oldStationModule);
                newStationModule.CreatedOn = DateTime.Now;
                this.SwitchToANewStationModule(newStationModule);
            }
        }

        /// <summary>
        /// Efface la station actuelle et choisit la pr�c�dente station
        /// On ne peut pas effacer la premi�re station
        /// </summary>
        internal void DeleteActualStation()
        {
            if (this._ActiveStationModule != null)
            {
                int index = this.StationModules.FindIndex(x => x._Station._Name == this._ActiveStationModule._Station._Name);
                if (this.StationModules.Count != 1)
                {
                    this.StationModules.Remove(this._ActiveStationModule);
                    if (index > 0) { index -= 1; }
                    else { index = 0; }
                    this.SetActiveStationModule(this.StationModules[index], this.StationModules[index].CreateViewAutmatically);
                }
            }
        }

        /// <summary>
        /// Cr�e une nouvelle station module sans copier les param�tres admin de l'ancienne
        /// </summary>
        internal void NewStation()
        {
            this.SwitchToANewStationModule(new Station.Module(this, CreateViewAutmatically));
        }

        internal override void SetActiveStationModule(Common.Station.Module stationModule, bool createView = true)
        {
            base.SetActiveStationModule(stationModule, createView);
            this.View?.SetStationNameInTopButton(stationModule);
        }

        /// <summary>
        /// dispatcheur d'evenement si un type n'est pas connu il est trait� par OnNextBasedOn(TsuObject TsuObject)
        /// </summary>
        /// <param name="TsuObject"></param>
        public override void OnNext(TsuObject TsuObject)
        {
            dynamic dynamic = TsuObject;
            OnNextBasedOn(dynamic);
        }

        /// <summary>
        /// �v�nement li� � la r�ception de points de l'Element MOdule Manager et ajout � la s�quence de points
        /// </summary>
        /// <param name="elementsFromElementModule"></param>
        public new void OnNextBasedOn(E.Composites.CompositeElement elementsFromElementModule)
        {
            //lance la fonction pour donner les points � mesurer
            if (elementsFromElementModule != null && this._ActiveStationModule is Station.Module lstm)
            {
                if (elementsFromElementModule.Elements.Count > 0)
                {
                    if (elementsFromElementModule.Elements.Count == 1
                        && !(elementsFromElementModule is E.Composites.SequenceNivs)
                        && elementsFromElementModule.Elements[0] is E.Composites.SequenceNiv firstSeq)
                    {
                        //lance la fonction pour integrer les points de la s�quence simple
                        lstm.SetSequenceToMeasure(firstSeq);
                    }
                    else
                    {
                        if (!(elementsFromElementModule is E.Composites.SequenceNivs))
                        {
                            lstm.SetPointsToMeasure(elementsFromElementModule);
                        }
                    }
                }
                else
                {
                    lstm.SetPointsToMeasure(elementsFromElementModule);
                }
            }
        }

        /// <summary>
        /// une current module as input for Beam offset computation
        /// </summary>
        public override void BOC_Compute()
        {
            // identify assembly
            var ids = new List<string>();

            // Add
            if (this._ActiveStationModule is Station.Module lsm)
            {
                foreach (Point point in lsm._MeasPoint)
                {
                    var tempId = Point.GetAssembly(point._Name);
                    ids.AddIfNotExisting(tempId);
                }
            }

            // Remove calas
            foreach (Point point in this.CalaPoints)
            {
                var tempId = Point.GetAssembly(point._Name);
                ids.Remove(tempId);
            }

            if (ids.Count == 0) { throw new Exception("No Su assembly identified for BOC"); }

            // select module: current + roll
            var modules = new List<FinalModule> { this };
            var rollModules = Tsunami2.Properties.MeasurementModules.GetRollModules();
            modules.AddRange(rollModules);

            // run BOC for each assembly
            foreach (string id in ids)
            {
                try
                {
                    var r = Common.Compute.Compensations.BeamOffsets.WorkFlow.Compute(this, modules, id, ObservationType.Level);
                    Common.Compute.Compensations.BeamOffsets.WorkFlow.OffsetComputedInAllSystems.WaitOne(1 * 5000);
                    if (r != null && r.OK)
                    {
                        string titleAndMessage = r.ToString("4LEVEL");
                        new MessageInput(MessageType.FYI, titleAndMessage).Show();
                    }
                }
                catch (Exception ex)
                {
                    MessageTsu.ShowMessage(ex, MessageType.Warning);
                }
            }
        }

        /// <summary>
        /// Export all stations that are ready to be saved in a dat file for Geode
        /// </summary>
        internal void ExportAllToGeode()
        {
            bool saveAllSuccess = true;
            string respond = "";
            string geodeFilePath = "";
            if (this._ActiveStationModule is Station.Module stationModule)
            {
                geodeFilePath = stationModule._WorkingStationLevel.ParametersBasic._GeodeDatFilePath;
            }
            foreach (Station.Module item in this.StationModules.OfType<Station.Module>())
            {
                try
                {
                    string activeRepriseStationName = "";
                    //Pour garder en m�moire la station reprise active
                    if (item._LevelingDirectionStrategy is Station.RepriseStrategy)
                    {
                        activeRepriseStationName = item._WorkingStationLevel._Name;
                    }
                    if (item._AllerStationLevel._Parameters._State is Common.Station.State.StationLevelReadyToBeSaved)
                    {
                        //item.ShowSaveMessageOfSuccess.IsTrue = ShowSaveMessageOfSuccess.IsTrue;
                        item._WorkingStationLevel = item._AllerStationLevel;
                        item.ExportToGeode();
                        if (item._WorkingStationLevel.ParametersBasic._GeodeDatFilePath != "") geodeFilePath = item._WorkingStationLevel.ParametersBasic._GeodeDatFilePath;
                        //if (!ShowSaveMessageOfSuccess.IsTrue) { ShowSaveMessageOfSuccess.IsTrue = item.ShowSaveMessageOfSuccess.IsTrue; }
                    }
                    else
                    {
                        if (item._AllerStationLevel._Parameters._Instrument._Model != null
                            && !(item._AllerStationLevel._Parameters._State is Common.Station.State.StationLevelSaved)
                            && item._AllerStationLevel._Parameters._Team == R.String_UnknownTeam)
                        {
                            string titleAndMessage = string.Format(R.StringAlignment_CannotSaveLevel, item._AllerStationLevel._Name);
                            new MessageInput(MessageType.Warning, titleAndMessage).Show();
                            saveAllSuccess = false;
                        }
                    }
                    if (item._RetourStationLevel._Parameters._State is Common.Station.State.StationLevelReadyToBeSaved)
                    {
                        //item.ShowSaveMessageOfSuccess.IsTrue = ShowSaveMessageOfSuccess.IsTrue;
                        item._WorkingStationLevel = item._RetourStationLevel;
                        item.ExportToGeode();
                        if (item._WorkingStationLevel.ParametersBasic._GeodeDatFilePath != "") geodeFilePath = item._WorkingStationLevel.ParametersBasic._GeodeDatFilePath;
                        //if (!ShowSaveMessageOfSuccess.IsTrue) { ShowSaveMessageOfSuccess.IsTrue = item.ShowSaveMessageOfSuccess.IsTrue; }
                    }
                    else
                    {
                        if (item._RetourStationLevel._Parameters._Instrument._Model != null
                            && !(item._RetourStationLevel._Parameters._State is Common.Station.State.StationLevelSaved)
                            && item._RetourStationLevel._Parameters._Team == R.String_UnknownTeam)
                        {
                            string titleAndMessage = string.Format(R.StringAlignment_CannotSaveLevel, item._RetourStationLevel._Name);
                            new MessageInput(MessageType.Warning, titleAndMessage).Show();
                            saveAllSuccess = false;
                        }
                    }
                    if (item._RepriseStationLevel.Count > 0)
                    {

                        foreach (Station reprise in item._RepriseStationLevel)
                        {
                            if (reprise != null)
                            {
                                if (reprise._Parameters._State is Common.Station.State.StationLevelReadyToBeSaved)
                                {
                                    //item.ShowSaveMessageOfSuccess.IsTrue = ShowSaveMessageOfSuccess.IsTrue;
                                    item._WorkingStationLevel = reprise;
                                    item.ExportToGeode();
                                    if (item._WorkingStationLevel.ParametersBasic._GeodeDatFilePath != "") geodeFilePath = item._WorkingStationLevel.ParametersBasic._GeodeDatFilePath;
                                    //if (!ShowSaveMessageOfSuccess.IsTrue) { ShowSaveMessageOfSuccess.IsTrue = item.ShowSaveMessageOfSuccess.IsTrue; }
                                }
                                else
                                {
                                    if (reprise._Parameters._Instrument._Model != null
                                        && !(reprise._Parameters._State is Common.Station.State.StationLevelSaved)
                                        && reprise._Parameters._Team == R.String_UnknownTeam)
                                    {
                                        string titleAndMessage = string.Format(R.StringAlignment_CannotSaveLevel, reprise._Name);
                                        new MessageInput(MessageType.Warning, titleAndMessage).Show();
                                        saveAllSuccess = false;
                                    }
                                }
                            }
                        }
                    }
                    if (item._LevelingDirectionStrategy is Station.AllerStrategy)
                    {
                        item._WorkingStationLevel = item._AllerStationLevel;
                    }
                    if (item._LevelingDirectionStrategy is Station.RetourStrategy)
                    {
                        item._WorkingStationLevel = item._RetourStationLevel;
                    }
                    if (item._LevelingDirectionStrategy is Station.RepriseStrategy)
                    {
                        int i = item._RepriseStationLevel.FindIndex(x => x._Name == activeRepriseStationName);
                        if (i != -1)
                        {
                            item._WorkingStationLevel = item._RepriseStationLevel[i];
                        }
                        else
                        {
                            item._WorkingStationLevel = item._RepriseStationLevel[0];
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"{R.T_COULD_NOT} {R.T_Export} {R.T_LEVELLING}\r\n\r\n{ex.Message}");
                }

            }
            if (saveAllSuccess && geodeFilePath != "")
            {
                string titleAndMessage = string.Format(R.StringLevel_SaveAllSucces);
                MessageInput mi = new MessageInput(MessageType.GoodNews, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_OK, R.T_OpenFileLocation }
                };
                respond = mi.Show().TextOfButtonClicked;
            }
            else
            {
                if (saveAllSuccess)
                {
                    string titleAndMessage = string.Format(R.StringLevel_SaveAllSucces);
                    new MessageInput(MessageType.GoodNews, titleAndMessage).Show();
                }
            }
            if (respond == R.T_OpenFileLocation && System.IO.File.Exists(geodeFilePath))
            {
                System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo("explorer.exe", " /select, " + geodeFilePath));
            }
        }
        /// <summary>
        /// Lance le contr�le collimation du niveau
        /// </summary>
        internal void CollimCheck()
        {
            if (this._ActiveStationModule is Station.Module slm
                && slm._WorkingStationLevel?.ParametersBasic._Instrument is I.Level asLevel
                && asLevel._Name != R.String_Unknown)
            {
                I.Level level = asLevel.DeepCopy();
                int index = this.levelUsed.FindIndex(x => x._Name == level._Name);
                if (index != -1)
                {
                    level._LA1 = this.levelUsed[index]._LA1;
                    level._LA2 = this.levelUsed[index]._LA2;
                    level._LB1 = this.levelUsed[index]._LB1;
                    level._LB2 = this.levelUsed[index]._LB2;
                    level._ErrorCollim = this.levelUsed[index]._ErrorCollim;
                    level._DistCollim = this.levelUsed[index]._DistCollim;
                    level._IsCollimExported = this.levelUsed[index]._IsCollimExported;
                }
                string respond = this.View.ShowMessageCollimLevelCheck(R.StringMsgColimLevel_Title + "\r\n" + string.Format(R.StringMsgColimLevel_Description, level._Name),
                    R.T_OK,
                    R.T_CANCEL,
                    level);
                if (respond != R.T_CANCEL)
                {
                    if (index != -1)
                    {
                        this.levelUsed.RemoveAt(index);
                    }
                    this.levelUsed.Add(level);
                    asLevel._LA1 = level._LA1;
                    asLevel._LA2 = level._LA2;
                    asLevel._LB1 = level._LB1;
                    asLevel._LB2 = level._LB2;
                    asLevel._ErrorCollim = level._ErrorCollim;
                    asLevel._DistCollim = level._DistCollim;
                    asLevel._IsCollimExported = level._IsCollimExported;
                    foreach (Station.Module item in this.StationModules.OfType<Station.Module>())
                    {
                        if (item._WorkingStationLevel.ParametersBasic._Instrument != null
                            && item._WorkingStationLevel.ParametersBasic._Instrument._Name == level._Name)
                        {
                            item.UpdateAllColimCheck();
                        }
                    }
                    this.Save();
                }
            }
        }

        /// <summary>
        /// Affiche le module pour contr�ler le z�ro des mires
        /// </summary>
        internal void ZeroStaffCheck()
        {
            if (this.staffModule != null)
            {
                string respond = this.View.ShowMessageOfZeroStaffCheck(R.StringLevel_ZeroStaffCheckMsgTitle + "\r\n" + R.StringLevel_ZeroStaffCheckMsgDescription, R.T_OK, R.T_CANCEL, this.staffUsed, this.staffModule);
                if (respond != R.T_CANCEL)
                {
                    this.timeStaffZeroCheck = DateTime.Now;
                    this.SetIsStaffZeroCheckExported(false);
                    this.UpdateAllStaff();
                    foreach (Station.Module item in this.StationModules.OfType<Station.Module>())
                        item.UpdateAllOffsets();
                }

            }
        }
        /// <summary>
        /// Exportes les nouveaux points dans le fichier theo dat
        /// </summary>
        internal void ExportNewPointTheoDat()
        {
            if (this._ActiveStationModule is Station.Module stationModule
                && stationModule.ElementModule != null)
            {
                List<Point> actualStationPoints = new List<Point>();
                List<Point> lgcGlobalPoints = new List<Point>();
                List<Point> allPoints = stationModule.ElementModule.GetPointsInAllElements();
                List<Point> newCreatedByUserPoints = allPoints.FindAll(x => x._Origin == "Created by user");
                if (newCreatedByUserPoints != null)
                {
                    ///Remplace les nouveaux points par les points calcul�s dans la station actuelle si disponible
                    string groupName = stationModule.measGroupName;
                    groupName = groupName.Replace('.', '_');
                    foreach (Point item in newCreatedByUserPoints)
                    {
                        Point pt = allPoints.Find(x => x._Origin == groupName && x._Name == item._Name);
                        if (pt != null)
                        {
                            actualStationPoints.Add(pt);
                        }
                        else
                        {
                            ///il faut un H au point pour r�importer les points dans Tsunami 
                            Point itemWithH = item.DeepCopy();
                            if (item._Coordinates.Ccs.Z.Value == Preferences.Preferences.NotAvailableValueNa)
                            {
                                itemWithH._Coordinates.Ccs.Z.Value = 400;
                            }
                            actualStationPoints.Add(itemWithH);
                        }
                    }
                    ///Remplace les nouveaux points par les points calcul�s dans un calcul LGC global si disponible
                    foreach (Point item in actualStationPoints)
                    {
                        Point pt = allPoints.Find(x => x._Origin == "GlobalLevellingLGC2Calculation" && x._Name == item._Name);
                        if (pt != null)
                        {
                            lgcGlobalPoints.Add(pt);
                        }
                        else
                        {
                            lgcGlobalPoints.Add(item);
                        }
                    }
                    string path = TsuPath.GetFileNameToSave("", string.Format("{0:yyyyMMdd}", DateTime.Now) + "_Elements", "Geode(*.dat) | *.dat", "");
                    if (path != "")
                    {
                        IO.SUSoft.Geode.Export(path, lgcGlobalPoints);
                        Shell.Run(path);
                    }
                }
            }
        }
        internal void DoCalculationWithLGC2()
        {
            string lgcGlobalDir = string.Format(
                "{0}LGC_Global\\",
                Tsunami2.Preferences.Values.Paths.MeasureOfTheDay);
            string filepath = string.Format(
                "{0}GlobalLevelling_{1}.inp",
                lgcGlobalDir,
                Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now));
            if (!System.IO.Directory.Exists(lgcGlobalDir))
            {
                System.IO.Directory.CreateDirectory(lgcGlobalDir);
            }
            if (System.IO.File.Exists(filepath))
            {
                string titleAndMessage = string.Format(R.StringLevel_LGCReplaceFile, filepath);
                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = new List<string> { R.T_OK, R.T_DONT }
                };
                string respond = mi.Show().TextOfButtonClicked;
                if (respond == R.T_DONT)
                    return;
            }
            Lgc2 lgc2 = new Lgc2(this, filepath);
            lgc2.Run(true, true);
            if (lgc2._Runned)
            {
                Station.Module activeStationLevelModule = this._ActiveStationModule as Station.Module;
                //string respond = activeStationLevelModule.View.ShowMessageOfChoice(R.StringLevel_AddHStationToElementManager, R.T_OK, R.T_DONT);
                //if (respond == R.T_OK)
                //{
                List<Point> pointList = lgc2._Output.GetResultsForLevelling(this);
                if (pointList.Count != 0)
                {
                    //string saveName = this._ActiveStationModule._Name;
                    //this._ActiveStationModule._Name = "GlobalLevellingLGC2Calculation";
                    foreach (Point item in pointList)
                    {
                        //item._Origin = "GlobalLevellingLGC2Calculation";
                        this._ElementManager.RemoveElementByNameAndOrigin(item._Name, "GlobalLevellingLGC2Calculation");
                        this._ElementManager.AddElement("GlobalLevellingLGC2Calculation", item);
                    }
                    this._ElementManager.View.UpdateView();
                    this.Save();
                    //this._ActiveStationModule._Name = saveName;
                    //List<string> textToWriteInMessage = new List<string>();
                    List<string> listPointNames = new List<string>();
                    List<string> listDcum = new List<string>();
                    List<string> listMoveToDo = new List<string>();
                    List<string> listTheoReading = new List<string>();
                    bool showMessageWithMove = false;
                    //listPointNames.Add(R.StringLevel_ResultForGlobalLGCCalculation_Name);
                    //listMoveToDo.Add(R.StringLevel_ResultForGlobalLGCCalculation_Move);
                    //listTheoReading.Add(R.StringLevel_ResultForGlobalLGCCalculation_TheoReading);
                    foreach (M.MeasureOfLevel meas in activeStationLevelModule._WorkingStationLevel._MeasureOfLevel)
                    {
                        if (meas._Point.LGCFixOption != LgcPointOption.CALA)
                        {
                            double hMes;
                            double hTheo;
                            Point ptTheo = activeStationLevelModule._TheoPoint.Find(x => x._Name == meas._PointName);
                            switch (activeStationLevelModule._WorkingStationLevel._Parameters._ZType)
                            {
                                case CoordinatesType.CCS:
                                    hTheo = ptTheo._Coordinates.Ccs.Z?.Value ?? na;
                                    break;
                                case CoordinatesType.SU:
                                    hTheo = ptTheo._Coordinates.Local.Z?.Value ?? na;
                                    break;
                                //case CoordinatesType.UserDefined:
                                //    hTheo = ptTheo._Coordinates.UserDefined.Z.Value;
                                //    break;
                                default:
                                    hTheo = ptTheo._Coordinates.Ccs.Z?.Value ?? na;
                                    break;
                            }
                            int index;
                            index = pointList.FindIndex(x => x._Name == meas._PointName);
                            if (index != -1)
                            {
                                switch (activeStationLevelModule._WorkingStationLevel._Parameters._ZType)
                                {
                                    case CoordinatesType.CCS:
                                        hMes = pointList[index]._Coordinates.Ccs.Z?.Value ?? na;
                                        break;
                                    case CoordinatesType.SU:
                                        hMes = pointList[index]._Coordinates.Local.Z?.Value ?? na;
                                        break;
                                    default:
                                        hMes = pointList[index]._Coordinates.Ccs.Z?.Value ?? na;
                                        break;
                                }
                                double move = hTheo - hMes;
                                double theoReading = meas._RawLevelReading - move;
                                listPointNames.Add(meas._PointName + " ");
                                //Ajoute la Dcum
                                if (meas._Point._Parameters.Cumul != Tsunami2.Preferences.Values.na)
                                {
                                    listDcum.Add(meas._Point._Parameters.Cumul.ToString("F3", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                                }
                                else
                                {
                                    listDcum.Add("");
                                }
                                listMoveToDo.Add(" " + Math.Round(move * 100000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " ");
                                listTheoReading.Add(" " + Math.Round(theoReading * 100000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
                                showMessageWithMove = true;
                            }
                        }
                    }
                    if (showMessageWithMove)
                    {
                        this.View.ShowMessageOfMoveLevelInDatagrid(
                            R.StringLevel_ResultForGlobalLGCCalculationTitle,
                            R.T_OK,
                            listPointNames,
                            listDcum,
                            listMoveToDo,
                            listTheoReading);
                    }
                    else
                    {
                        new MessageInput(MessageType.Warning, R.StringLevel_ResultForGlobalLGCCalculationNothingTomove).Show();
                    }

                }
                //}
            }
        }
        /// <summary>
        /// Fait le contr�le de marche avec les cotes bleues (diff�rence de lecture de mire
        /// </summary>
        internal string CheckBlueElevation(bool showCancelButton = false)
        {
            string respond = "";
            if (this._ActiveStationModule is Station.Module levelStationModule)
            {
                int index = this.StationModules.FindIndex(x => x.Guid == levelStationModule.Guid);
                if (index != -1)
                {
                    if (index < this.StationModules.Count - 1)
                    {
                        if (index == 0)
                        {
                            // premi�re station compare uniquement avec la suivante
                            respond = levelStationModule.CheckBlueElevation(null, this.StationModules[index + 1] as Station.Module, index + 1, showCancelButton);
                        }
                        else
                        {
                            //station interm�diaire fait une comparaison avec pr�c�dente et suivante
                            respond = levelStationModule.CheckBlueElevation(this.StationModules[index - 1] as Station.Module, this.StationModules[index + 1] as Station.Module, index + 1, showCancelButton);
                        }
                    }
                    else
                    {
                        //derni�re station, fait une comparaison avec la pr�c�dente
                        if (index != 0)
                        {
                            respond = levelStationModule.CheckBlueElevation(this.StationModules[index - 1] as Station.Module, null, index + 1, showCancelButton);
                        }
                    }
                }
            }
            return respond;
        }
        /// <summary>
        /// mise � jour point qui a �t� modifi� dans autre �l�ment manager
        /// </summary>
        /// <param name="originalPoint"></param>
        /// <param name="pointModified"></param>
        internal override void UpdateTheoCoordinates(Point originalPoint, Point pointModified)
        {
            base.UpdateTheoCoordinates(originalPoint, pointModified);
            foreach (Station.Module stm in this.StationModules.OfType<Station.Module>())
            {
                stm.UpdateTheoCoordinates(originalPoint, pointModified);
            }
        }
        /// <summary>
        /// Met � jour toutes les mires en fonction du z�ro mire
        /// </summary>
        internal override void UpdateAllStaff()
        {
            foreach (Station.Module stm in this.StationModules.OfType<Station.Module>())
            {
                stm.UpdateAllStaff(this.staffUsed);
            }
        }

        internal override IEnumerable<Common.Station> GetStations()
        {
            var stations = new List<Common.Station>();
            foreach (var item in this.StationModules)
            {
                stations.Add(item._Station);
            }
            return stations;
        }


        //internal void AddElementsToElementModule(List<Point> pointList,string origin)
        ////ajoute tous les points theo s�lectionn� dans element module
        //{
        //    string saveName = this._Name;
        //    this._Name = origin;
        //    foreach (Point point in pointList)
        //    {
        //        point.IsTheo = false;
        //        this._ElementManager.AddElement(origin, point, false, false, true, false);
        //    }
        //    this._Name = saveName;
        //}
        ///// <summary>
        ///// Enl�ve les points de l'element module avec l'origine origin
        ///// </summary>
        ///// <param name="origin"></param>
        //internal void RemoveFromElementModule(List<Point> pointList,string origin)
        //{
        //    CloneableList<Point> savedMeasPointList = new CloneableList<Point>();
        //    foreach (Point pt in pointList)
        //    {
        //        Point copyP = pt.DeepCopy();
        //        savedMeasPointList.Add(copyP);
        //    }
        //    string saveName = this._Name;
        //    this._Name = origin;
        //    this._ElementManager.Clear(this);
        //    this._Name = saveName;
        //    pointList = savedMeasPointList;
        //}
    }
}
