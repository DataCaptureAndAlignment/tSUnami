﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSU.Common;
using TSU.Common.Guided;
using TSU.Common.Instruments;
using TSU.Views;
using TSU.Views.Message;

namespace TSU.Tools
{
    /// <summary>
    /// F1 : help
    /// F2 : Refresh / update
    /// F3 : Measure
    /// F4 : Goto/Measure
    /// F5 : Goto
    /// F6 : Lock/unLock
    /// F7 : RenamePoint
    /// F8 : LiveData On/Off
    /// F9 : Open Element Manager
    /// F10 : Change Sub Guided Module / Change advanced modules if not GroupGuidedModuleAvaialble.
    /// F11 : PreviousStep
    /// F12 : NextStep
    /// </summary>
    internal static class KeyboardShortCut
    {
        internal static bool AnalyseAnAct(TsuView control, KeyEventArgs keyEventArgs)
        {
            try
            {
                if (!KeyboardNavigation.IsFunctionKey(keyEventArgs) )
                    return false;
                if (keyEventArgs.Alt || keyEventArgs.Control || keyEventArgs.Shift)
                    return false;

                switch (keyEventArgs.KeyValue)
                {
                    //F1
                    case 112:
                        Help();
                        break;
                    //F2
                    case 113:
                        Refresh();
                        break;
                    //F3
                    case 114:
                        Measure();
                        break;
                    //F4
                    case 115:
                        GotoAndMeasure();
                        break;
                    //F5
                    case 116:
                        Goto();
                        break;
                    //F6
                    case 117:
                        LockUnLcok();
                        break;
                    //F7
                    case 118:
                        RenamePoint();
                        break;
                    //F8
                    case 119:
                        LiveDataOnOff();
                        break;
                    //F9
                    case 120:
                        OpenElementManager();
                        break;
                    //F10
                    case 121:
                        ChangeModule();
                        break;
                    //F11
                    case 122:
                        PreviousStep();
                        break;
                    //F12
                    case 123:
                        NextStep();
                        break;
                    default:
                        break;
                }

                return true;
            }
            catch (Exception ex)
            {
                string titleAndMessage = $"Shortcut failed;{ex}";
                new MessageInput(MessageType.FYI, titleAndMessage).Show();
                return false;
            }
            
        }

        private static void NextStep()
        {
            GetGuidedModule().MoveToNextStep();
            string shortcutName = "Move to next step";
            string titleAndMessage = $"{shortcutName};Shortcut used";
            new MessageInput(MessageType.FYI, titleAndMessage).Show();
        }

        private static Common.Guided.Module GetGuidedModule()
        {
            FinalModule activeFinaleModule = Tsunami2.View.ActiveView._Module as FinalModule;

            if (activeFinaleModule is Common.Guided.Group.Module ggm)
            {
                return ggm.ActiveSubGuidedModule;
            }
            else if (activeFinaleModule is Common.Guided.Module gm)
                return gm;

            throw new Exception("No active guided modules");
        }

        private static void PreviousStep()
        {
            GetGuidedModule().MoveToPreviousStep();
            string shortcutName = "Move to previous step";
            string titleAndMessage = $"{shortcutName};Shortcut used";
            new MessageInput(MessageType.FYI, titleAndMessage).Show();
        }

        private static void ChangeModule()
        {
            FinalModule activeFinaleModule = Tsunami2.View.ActiveView._Module as FinalModule;

            if (activeFinaleModule is Common.Guided.Group.Module ggm) // change sub modules inside current guided modules
            {
                int index = ggm.activeSubGuidedModuleIndex++;
                if (index > ggm.SubGuidedModules.Count - 1)
                    index = 0;
                ggm.SetActiveGuidedModule(ggm.SubGuidedModules[index]);
            }
            else // change main final modules
            {
                Tsunami2.View._Strategy.MoveToNextActiveView();
            }
            string shortcutName = "Change module";
            string titleAndMessage = $"{shortcutName};Shortcut used";
            new MessageInput(MessageType.FYI, titleAndMessage).Show();
        }

        private static void OpenElementManager()
        {
            string shortcutName = "Open element manager";
            string titleAndMessage = $"{shortcutName};Shortcut used";
            new MessageInput(MessageType.FYI, titleAndMessage).Show();
            Polar.Station.Module psm = GetActivePolarStationModule(out _);
            psm.View.SetNextPoints();

        }

        private static void LiveDataOnOff()
        {
            Polar.Station.Module psm = GetActivePolarStationModule(out PolarModule pim);
            if (pim is ILiveData ldpim)
                ldpim.LiveData_StartStop();

            string shortcutName = "Turn ON/OFF liveData";
            string titleAndMessage = $"{shortcutName};Shortcut used";
            new MessageInput(MessageType.FYI, titleAndMessage).Show();
        }

        private static void RenamePoint()
        {
            string shortcutName = "Rename next point";
            string titleAndMessage = $"{shortcutName};Shortcut used";
            new MessageInput(MessageType.FYI, titleAndMessage).Show();
            Polar.Station.Module psm = GetActivePolarStationModule(out _);
            var meas = psm.StationTheodolite.MeasuresToDo;
            if (meas.Count > 0)
            {
                TsuOptionalShowing.Type type = TsuOptionalShowing.Type.None;
                psm.View.RenamePoint(meas[0] as Polar.Measure, ref type);
            }

            
        }

        private static void LockUnLcok()
        {
            Polar.Station.Module psm = GetActivePolarStationModule(out PolarModule pim);
            if (pim is IMotorized mpim)
                mpim.LockUnLock();

            string shortcutName = "(un)Lock";
            string titleAndMessage = $"{shortcutName};Shortcut  used";
            new MessageInput(MessageType.FYI, titleAndMessage).Show();
        }
        private static void Goto()
        {
            GetActivePolarStationModule(out _).Goto();
            string shortcutName = "Goto";
            string titleAndMessage = $"{shortcutName};Shortcut used";
            new MessageInput(MessageType.FYI, titleAndMessage).Show();
        }

        private static void GotoAndMeasure()
        {
            PolarMeasuremment(GotoWanted: true);
            string shortcutName = "Goto And Measure";
            string titleAndMessage = $"{shortcutName};Shortcut used";
            new MessageInput(MessageType.FYI, titleAndMessage).Show();
            
        }

        private static void PolarMeasuremment(bool GotoWanted)
        {
            var psm = GetActivePolarStationModule(out PolarModule pim);
            pim.GotoWanted = GotoWanted;
            psm.MeasureNow();
        }

        private static void Measure()
        {
            PolarMeasuremment(GotoWanted: false);
            string shortcutName = "Measure";
            string titleAndMessage = $"{shortcutName};Shortcut used";
            new MessageInput(MessageType.FYI, titleAndMessage).Show();
        }

        private static Polar.Station.Module GetActivePolarStationModule(out PolarModule PolarInstrumentModule)
        {
            FinalModule activeFinaleModule = Tsunami2.View.ActiveView._Module as FinalModule;
            Polar.Station.Module psm = null;
            if (activeFinaleModule == null) throw new Exception("No polar module found");

            if (activeFinaleModule is Common.Guided.Group.Module)
            {

            }
            else if (activeFinaleModule is IGuidedModule)
            {

            }
            else if (activeFinaleModule._ActiveStationModule is Polar.Station.Module psm2)
            {
                psm = psm2;
            }

            if (psm != null)
            {
                PolarInstrumentModule = (psm._InstrumentManager.SelectedInstrumentModule as PolarModule);
                return psm;
            }
                
            throw new Exception("No polar module found");
        }

        private static void Refresh()
        {
            Polar.Station.Module psm = GetActivePolarStationModule(out PolarModule pim);
            if (pim is IConnectable cpim)
                cpim.Refresh();

        }

        private static void Help()
        {
            Tsunami.ShowHelp();
            string shortcutName = "Help";
            string titleAndMessage = $"{shortcutName}Shortcut used;The read the doc web page should have been opened in your browser as well as local version.";
            new MessageInput(MessageType.FYI, titleAndMessage).Show();

        }
    }

     
}
