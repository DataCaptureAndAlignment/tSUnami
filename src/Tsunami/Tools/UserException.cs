﻿using System;

namespace TSU.Tools
{
    /// <summary>
    /// This class represents an exception caused by the user
    /// It's just meant to identify Exceptions which can be corrected by the user
    /// So they should be displayed without stacktrace and invitation to reprort the issue
    /// </summary>
    internal class UserException : Exception
    {
        public UserException(string message) : base(message)
        {
        }
    }
}
