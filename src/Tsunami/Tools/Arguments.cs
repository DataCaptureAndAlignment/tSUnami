﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using TSU.Common;
using TSU.Common.Zones;
using TSU.Views.Message;
using R = TSU.Properties.Resources;

namespace TSU.Tools
{
    public static class Arguments
    {
        public static bool ContainsMacro {
            get
            {
                if (TSU.Program.Args == null) return false;
                for (int i = 0; i < Program.Args.Length; i++)
                {
                    if (Program.Args[i] == "-m")
                        return true;
                }
                return false;
            }
        }


        public static void Handle()
        {
            if (TSU.Program.Args == null) return;
            string[] args = TSU.Program.Args;

            // Define variables to store the parsed arguments
            List<string> inputFiles = new List<string>();
            List<string> macrosPaths = new List<string>();
            string zonePath = "";
            bool showHelp = false;

            // Parse the command-line arguments
            for (int i = 0; i < args.Length; i++)
            {
                string arg = args[i];

                if (arg.Equals("/?", StringComparison.OrdinalIgnoreCase))
                {
                    showHelp = true;
                }
                else if (arg.Equals("-h", StringComparison.OrdinalIgnoreCase))
                {
                    showHelp = true;
                }
                else if (arg.Equals("-m", StringComparison.OrdinalIgnoreCase))
                {
                    // Collect all subsequent arguments as macros until a different option is encountered
                    for (int j = i + 1; j < args.Length; j++)
                    {
                        string nextArg = args[j];
                        if (nextArg.StartsWith("-"))
                        {
                            break; // Found a different option, stop collecting macros
                        }
                        macrosPaths.Add(nextArg);
                        i = j; // Update the outer loop index to skip processed macro arguments
                    }
                }
                else if (arg.Equals("-z", StringComparison.OrdinalIgnoreCase))
                {
                    // Collect all subsequent arguments as macros until a different option is encountered
                    for (int j = i + 1; j < args.Length; j++)
                    {
                        string nextArg = args[j];
                        zonePath = nextArg;
                        i = j; // Update the outer loop index to skip processed macro arguments
                    }
                }
                else
                {
                    inputFiles.Add(arg);
                }
            }

            // Handle the parsed arguments
            if (showHelp)
            {
                DisplayHelp();
            }
            else
            {
                // Perform your application logic using the inputFiles and macros values
                ProcessInputFiles(inputFiles, macrosPaths, zonePath);
            }
        }

        private static async void ProcessInputFiles(List<string> inputFilesPaths, List<string> macrosPaths, string zonePath)
        {
            var tsunami = Tsunami2.Properties;

            Func<bool> LoadZone = () =>
            {
                if (zonePath != "")
                {
                    FileInfo fi = new FileInfo(zonePath);
                    if (!fi.Exists)
                        throw new Exception($"Zone path: '{zonePath}' not found");
                    if (!(fi.Extension.ToUpper() == ".XML"))
                        throw new Exception($"Zone path: must have .XML extension");
                    tsunami.Zone = TSU.IO.Xml.DeserializeFile(typeof(Zone), zonePath) as Zone;
                    return true;
                }
                return false;
            };
            foreach (string file in inputFilesPaths)
            {
                FileInfo fileInfo = new FileInfo(inputFilesPaths[0]);
                string extension = fileInfo.Extension.ToUpper();
                switch (extension)
                {
                    case ".XML":
                    case ".TSU":
                    case ".TSUT":
                        Logs.Log.AddEntryAsSuccess(tsunami, $"{R.T_LOADING_MODULES}...");
                        tsunami.Menu.View.OpenExistingModule(fileInfo.FullName);
                        LoadZone();
                        break;
                    case ".INP":
                    case ".LGC":
                        bool zoneLoaded = LoadZone();
                        Common.Compute.Compensations.Lgc2.RunInputWithZone(fileInfo.FullName, showInput: !zoneLoaded, exitOnSuccess: true); // if there is a zone probably the goal is to get straight to results
                        break;
                    default:
                        string titleAndMessage = $"{R.T_UNKNOWN_FILE_TYPE};{R.T_KNOWN_FILE_TYPE}";
                        new MessageInput(MessageType.Warning, titleAndMessage).Show();
                        break;
                }
            }
            if (inputFilesPaths.Count == 0)
                Tsunami.TsuLoaded.Set(); // because macro will wait for end of optional .tsu(t) to start

            var timerToStartMacro = new Timer() { Enabled = true, Interval = 100 };
            timerToStartMacro.Elapsed += delegate
            {
                timerToStartMacro.Stop();
                if (!Tsunami.TsuLoaded.WaitOne(60 * 1000))
                    new MessageInput(MessageType.Warning, "Macro didn't run because .tsu end of loading was not detected").Show();
                tsunami.InvokeOnApplicationDispatcher(() => { RunMacros(macrosPaths); }); // must be run only if Tsunami.TsuLoaded is Set();

            };

        }

        private static async Task RunMacros(List<string> macrosPaths)
        {
            foreach (string file in macrosPaths)
            {
                Macro m = Macro.LoadFromFile(file);
                Macro.TsunamiStartingMacro = m;
                Macro.MacroBeingPlayed = m;

                if (m == null)
                    throw new Exception();

                try
                {
                    await Macro.Play(m);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        static void DisplayHelp()
        {
            TSU.Debug.WriteInConsole("Usage: tsunami.exe [input files...] [-m macros] [/?]");
            TSU.Debug.WriteInConsole("");
            TSU.Debug.WriteInConsole("Options:");
            TSU.Debug.WriteInConsole("  [input files...]    Specify one or more input files to process.");
            TSU.Debug.WriteInConsole("  -m macros           Specify one or more macros separated by commas.");
            TSU.Debug.WriteInConsole("  /?                  Show this help message.");
        }
    }
}
