﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using TSU.Functionalities.TestModules;
using static TSU.Tools.Macro;

namespace TSU.Tools
{
    public partial class Macro
    {
        public class MouseSimulator
        {
            [DllImport("user32.dll", SetLastError = true)]
            static extern void mouse_event(uint dwFlags, int dx, int dy, uint dwData, int dwExtraInfo);

            [Flags]
            public enum MouseEventFlags
            {
                LEFTDOWN = 0x0002,
                LEFTUP = 0x0004,
                MIDDLEDOWN = 0x0020,
                MIDDLEUP = 0x0040,
                RIGHTDOWN = 0x0008,
                RIGHTUP = 0x0010,
            }
            /// <summary>
            /// button should be Left, Right or Middle
            /// </summary>
            /// <param name="x"></param>
            /// <param name="y"></param>
            /// <param name="button"></param>
            public static void Click(int x, int y, string button, string direction)
            {
                SetCursorPosition(x, y);
                MouseEventFlags mefDown = (button == "Left") ? MouseEventFlags.LEFTDOWN : (button == "Right") ? MouseEventFlags.RIGHTDOWN: MouseEventFlags.MIDDLEDOWN;
                MouseEventFlags mefUp = (button == "Left") ? MouseEventFlags.LEFTUP : (button == "Right") ? MouseEventFlags.RIGHTUP : MouseEventFlags.MIDDLEUP;

                if (direction == MouseHook.MouseUpAndDown.Up.ToString())
                    mouse_event((uint)mefUp, x, y, 0, 0);
                else if (direction == MouseHook.MouseUpAndDown.Down.ToString())
                    mouse_event((uint)mefDown, x, y, 0, 0);
            }

            public static void SetCursorPosition(int x, int y)
            {
                SetCursorPos(x, y);
            }

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            static extern bool SetCursorPos(int x, int y);
        }

        public static class MouseHook
        {
            private static IntPtr hookID = IntPtr.Zero;
            private static LowLevelMouseProc hookCallback;

            public static event MouseEventHandler OnMouseUp;
            public static event MouseEventHandler OnMouseDown;

            public static void StartHook()
            {
                hookCallback = HookCallback;
                hookID = SetHook(hookCallback);
            }

            public static void StopHook()
            {
                UnhookWindowsHookEx(hookID);
            }

            private delegate IntPtr LowLevelMouseProc(int nCode, IntPtr wParam, IntPtr lParam);

            private static IntPtr SetHook(LowLevelMouseProc proc)
            {
                using (Process curProcess = Process.GetCurrentProcess())
                using (ProcessModule curModule = curProcess.MainModule)
                {
                    return SetWindowsHookEx(WH_MOUSE_LL, proc, GetModuleHandle(curModule.ModuleName), 0);
                }
            }

            public enum MouseUpAndDown
            {
                None,
                Up,
                Down
            }
            private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
            {
                try
                {
                    if (nCode >= 0)
                    {
                        MouseUpAndDown mouseUpOrDown = MouseUpAndDown.Up;
                        MouseButtons mb = MouseButtons.None;
                        switch ((MouseMessages)wParam)
                        {
                            case MouseMessages.WM_LEFTBUTTONDOWN:
                                mouseUpOrDown = MouseUpAndDown.Down;
                                mb = MouseButtons.Left;
                                break;
                            case MouseMessages.WM_LEFTBUTTONUP:
                                mouseUpOrDown = MouseUpAndDown.Up;
                                mb = MouseButtons.Left;
                                break;
                            case MouseMessages.WM_RIGHTBUTTONDOWN:
                                mouseUpOrDown = MouseUpAndDown.Down;
                                mb = MouseButtons.Right;
                                break;
                            case MouseMessages.WM_RIGHTBUTTONUP:
                                mouseUpOrDown = MouseUpAndDown.Up;
                                mb = MouseButtons.Right;
                                break;
                            case MouseMessages.WM_MOUSEWHEEL:
                            case MouseMessages.WM_MOUSEMOVE:
                            default:
                                mb = MouseButtons.None;
                                mouseUpOrDown = MouseUpAndDown.None;
                                break;
                        }
                        if (mb != MouseButtons.None)
                        {
                            MSLLHOOKSTRUCT hookStruct = (MSLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(MSLLHOOKSTRUCT));
                            TSU.Debug.WriteInConsole($"Injection flag: {hookStruct.flags}");
                            if ((hookStruct.flags & (uint)InjectedFlags.LLMHF_INJECTED) != 0)
                            {
                                TSU.Debug.WriteInConsole($"Mouse Click Injected: {hookStruct.flags}");
                            }
                            else
                            {
                                switch (mouseUpOrDown)
                                {
                                    case MouseUpAndDown.Up:
                                        OnMouseUp?.Invoke(null, new MouseEventArgs(mb, 1, hookStruct.pt.x, hookStruct.pt.y, 0));
                                        break;
                                    case MouseUpAndDown.Down:
                                        OnMouseDown?.Invoke(null, new MouseEventArgs(mb, 1, hookStruct.pt.x, hookStruct.pt.y, 0));
                                        break;
                                    case MouseUpAndDown.None:
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                    return CallNextHookEx(hookID, nCode, wParam, lParam);
                }
                catch (Exception ex )
                {
                    Logs.Log.AddEntry(null, ex);
                    return CallNextHookEx(hookID, nCode, wParam, lParam);
                }
            }

            private enum InjectedFlags : uint
            {
                LLMHF_INJECTED = 0x00000001,
                LLMHF_LOWER_IL_INJECTED = 0x00000002
            }

            private const int WH_MOUSE_LL = 14;

            private enum MouseMessages
            {
                WM_MOUSEMOVE = 0x0200,
                WM_LEFTBUTTONDOWN = 0x0201,
                WM_LEFTBUTTONUP =  0x0202,
                WM_RIGHTBUTTONDOWN = 0x0204,
                WM_RIGHTBUTTONUP = 0x0205,
                WM_MOUSEWHEEL = 0x020A,
            }

            [StructLayout(LayoutKind.Sequential)]
            private struct POINT
            {
                public int x;
                public int y;
            }

            [StructLayout(LayoutKind.Sequential)]
            private struct MSLLHOOKSTRUCT
            {
                public POINT pt;
                public uint mouseData;
                public uint flags;
                public uint time;
                public IntPtr dwExtraInfo;
            }

            [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
            private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelMouseProc lpfn, IntPtr hMod, uint dwThreadId);

            [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool UnhookWindowsHookEx(IntPtr hhk);

            [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
            private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

            [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
            private static extern IntPtr GetModuleHandle(string lpModuleName);
        }
    }
}
