﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSU.Tools
{
    public class TimeSpent
    {
        public enum type
        {
            Started, Stopped
        }

        public TimeSpent()
        {
            Reset();
        }

        List<string> names = new List<string>();
        List<type> startOrStops = new List<type>();
        List<DateTime> times = new List<DateTime>();

        List<string> names2 = new List<string>();
        List<TimeSpan> spans = new List<TimeSpan>();

        public void Reset()
        {
            names.Clear();
            startOrStops.Clear();
            times.Clear();
            names2.Clear();
            spans.Clear();
            Add("Global Monitoring", type.Started, DateTime.Now);
        }

        public void Add(string actionName, type type, DateTime dateTime)
        {
            names.Add(actionName);
            startOrStops.Add(type);
            times.Add(dateTime);
        }

        public string DoSynthesis()
        {
            string timespend = "";
            Add("Global Monitoring", type.Stopped, DateTime.Now);

            TimeSpan total = new TimeSpan();
            for (int i = 0; i < names.Count; i++)
            {
                if (startOrStops[i] == type.Started)
                {
                    for (int j = i; j < names.Count; j++)
                    {
                        if (names[i] == names[j] && startOrStops[j] == type.Stopped)
                        {
                            names2.Add(names[i]);
                            TimeSpan diff = (times[i] - times[j]).Duration();
                            spans.Add(diff);
                            total += diff;
                            break;
                        }
                    }
                }
            }
            timespend = $"{spans[0].TotalMilliseconds,10:0} ms (total of sub steps {total.Duration().TotalMilliseconds - spans[0].TotalMilliseconds,10:0} ms)";
            timespend += " (";
            for (int i = 1; i < names2.Count - 1; i++)
            {
                timespend += $"{names2[i]} = {spans[i].TotalMilliseconds,10:0} ms\t";
            }
            timespend += ")";

            return timespend;
        }
    }

}
