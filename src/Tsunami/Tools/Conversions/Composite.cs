﻿ using System;
using System.Collections.Generic;
using TSU;
using System.Text;
using R = TSU.Properties.Resources;
using System.Linq;
using TSU.Views;
using System.Text.RegularExpressions;
using TSU.Common.Instruments;

using TSU.Common.Elements;
using M = TSU.Common.Measures;
using TSU.Common.Elements.Composites;
using TSU.Common.Zones;
using TSU.Common.Operations;

using D = TSU.Common.Instruments.Device;
using TSU.ENUM;

namespace TSU.Tools.Conversions
{
    public static class Composite
    {
        #region Composite
        public static CompositeElement ToCompositeElement(List<Point> points, string name = "default")
        {
            if (name == "default") name = R.T_FEW_POINTS;
            CompositeElement ce = new CompositeElement(name);
            foreach (Point item in points)
            {
                ce.Add(item);
            }
            return ce;
        }
        #endregion

    }
}
