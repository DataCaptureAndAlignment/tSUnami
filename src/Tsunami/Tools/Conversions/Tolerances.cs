﻿using MathNet.Numerics.LinearAlgebra.Complex.Solvers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace TSU.Tools.Conversions
{
    public class Tolerances
    {
        public static bool Check(double value, double tolerance, out string message, string outputUnit = "")
        {
            var pref = Tsunami2.Preferences.Values.GuiPrefs;
            var a = pref != null ? pref.NumberOfDecimalForAngles : 4;
            var d = pref != null ? pref.NumberOfDecimalForDistances : 5;

            bool ok = value < tolerance;
            double multiplier = 1;
            int decimalDigits = 0;
            switch (outputUnit.ToUpper())
            {
                case "MM":
                    multiplier = 1000;
                    decimalDigits = d - 3;
                    break;
                case "M":
                    multiplier = 1;
                    decimalDigits = d;
                    break;
                case "GON":
                    multiplier = 1;
                    decimalDigits = a;
                    break;
                case "CC":
                    multiplier = 10000;
                    decimalDigits = a-4;
                    break;
                case "MRAD":
                    multiplier = 1000;
                    decimalDigits = a-3;
                    break;
                default:
                    break;
            }
            string sOKO = ok ? "OK" : "KO";
            string sValue = (value * multiplier).ToString("F" + decimalDigits);
            string comparator = ok ? "<" : ">=";
            string sTol = (tolerance * multiplier).ToString("F" + decimalDigits);
            message = $"{sOKO}, {sValue} {comparator} {sTol} {outputUnit}";

            return ok;
        }
    }
}
