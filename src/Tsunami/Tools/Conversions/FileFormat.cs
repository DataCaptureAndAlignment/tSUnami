﻿using System;
using System.Collections.Generic;
using System.Text;
using R = TSU.Properties.Resources;
using System.Linq;
using TSU.Views;
using System.Text.RegularExpressions;
using I = TSU.Common.Instruments;

using TSU.Common.Elements;
using M = TSU.Common.Measures;
using TSU.Common.Elements.Composites;
using TSU.Common.Zones;
using TSU.Common.Operations;

using D = TSU.Common.Instruments.Device;
using TSU.ENUM;
using T = TSU.Tools;
using TSU.Common.Instruments;
using TSU.Common.Instruments.Reflector;


namespace TSU.Tools.Conversions
{
    public static class FileFormat
    {

        #region File Format

        public static OperationTree OperationFile2Nodes(System.IO.FileInfo filePath1, out int count, System.IO.FileInfo filePath2 = null)
        {
            OperationTree root = new OperationTree();
            root.Name = "-1";
            root.Text = R.T_AllOperations;
            root.Tag = new Operation(0, "Noperation");

            int count1 = 0;
            int count2 = 0;

            // First high-level node
            OperationTree file1Node = new OperationTree();
            file1Node.Name = "File1";
            file1Node.Text = "Operations from " + filePath1.Name;
            file1Node.Tag = new Operation(1, "File1RootOperation");

            // Add operations from first file to the first high-level node
            file1Node = AddOperationFromChantierFile(filePath1, out count1, file1Node);

            root.Nodes.Add(file1Node);

            if (filePath2 != null)
            {
                // Second high-level node
                OperationTree file2Node = new OperationTree();
                file2Node.Name = "File2";
                file2Node.Text = "Operations from " + filePath2.Name;
                file2Node.Tag = new Operation(2, "File2RootOperation");

                // Add operations from second file to the second high-level node
                file2Node = AddOperationFromChantierFile(filePath2, out count2, file2Node);
                root.Nodes.Add(file2Node);
            }

            // Total count from both files
            count = count1 + count2;

            return root;
        }
        public static OperationTree AddOperationFromChantierFile(System.IO.FileInfo filePath, out int count, OperationTree root)
            {
                count = 0;
                // adding operation from file chantier.dat
                Operation off = new Operation(0, filePath.FullName);
                TsuNode nff = new TsuNode() { Name = off.value.ToString(), Text = off.value.ToString() + ": " + off.Description, Tag = off };
                nff.Text = filePath.FullName;
                root.Nodes.Add(nff);

                foreach (var myString in System.IO.File.ReadAllLines(filePath.FullName, Encoding.GetEncoding("windows-1250")))
                {
                    string line = myString;
                    if (TrimCleanAndCheckForComment(ref line) == true)
                        continue;
                    if (myString != "")
                    {
                        count++;
                        Operation o = GeodeLineToOperation(myString);
                        TsuNode n = new TsuNode() { Name = o.value.ToString(), Text = o.value.ToString() + ": " + o.Description, Tag = o };
                        TsuNode parentNode = FindNodeFromName(o.parentOperationValue.ToString(), nff);

                        if (parentNode != null)
                            parentNode.Nodes.Insert(0, n);
                        else
                            nff.Nodes.Insert(0, n);
                    }
                }

                return root;
            }

        public static List<Operation> OperationFile2List(System.IO.FileInfo filePath)
        {
            List<Operation> l = new List<Operation>();

            Operation o = new Operation(-1, filePath.FullName);
            l.Add(o);
            foreach (var myString in System.IO.File.ReadAllLines(filePath.FullName, Encoding.GetEncoding("windows-1250")))
            {
                if (myString != "")
                {

                    l.Add(GeodeLineToOperation(myString));
                }
            }
            return l;
        }


        private static TsuNode FindNodeFromName(string name, TsuNode root)
        {
            if (root.Name == name)
                return root;
            else
            {
                foreach (TsuNode item in root.Nodes)
                {
                    TsuNode temp = FindNodeFromName(name, item);
                    if (temp != null) return temp;
                }
                return null;
            }
        }

        public static Operation GeodeLineToOperation(string OperationFileLine)
        {
            try
            {
                Operation o = new Operation(1, "");
                //string[] stringSeparators = new string[] { "     " };
                //string[] lineFields = OperationFileLine.Split(stringSeparators, StringSplitOptions.None);

                string temp = "";
                temp = OperationFileLine.Substring(0, 9);
                o.parentOperationValue = Int32.Parse(temp);
                temp = OperationFileLine.Substring(9, 9);
                o.value = Int32.Parse(temp);
                temp = OperationFileLine.Substring(18);
                o.Description = temp;
                o._Name = "Operation " + o.value.ToString();
                return o;
            }
            catch (Exception ex)
            {
                throw new Exception(R.T_CANT_CONVERT_LINE + OperationFileLine + " (" + ex.Message + ")", ex);
            }

        }

        private static Operation FindOperation(int p, Operation operationToLookIn)
        {
            if (operationToLookIn.value == p)
                return operationToLookIn;
            else
            {
                return FindOperation(p, operationToLookIn);
            }
        }


        public static string ToStringWithAllFrame(Point p)
        {
            string line =
                String.Format("{0,-21}", p._Name) +
                String.Format("{0,13}", String.Format("{0:0.000000}", p._Coordinates.Local.X.Value)) +
                String.Format("{0,13}", String.Format("{0:0.000000}", p._Coordinates.Local.Y.Value)) +
                String.Format("{0,13}", String.Format("{0:0.000000}", p._Coordinates.Local.Z.Value)) +
                String.Format("{0,13}", String.Format("{0:0.000000}", p._Coordinates.UserDefined.X.Value)) +
                String.Format("{0,13}", String.Format("{0:0.000000}", p._Coordinates.UserDefined.Y.Value)) +
                String.Format("{0,13}", String.Format("{0:0.000000}", p._Coordinates.UserDefined.Z.Value)) +
                String.Format("{0,13}", String.Format("{0:0.000000}", p._Coordinates.Ccs.X.Value)) +
                String.Format("{0,13}", String.Format("{0:0.000000}", p._Coordinates.Ccs.Y.Value)) +
                String.Format("{0,13}", String.Format("{0:0.000000}", p._Coordinates.Ccs.Z.Value));
            string prefix = p.State == Element.States.Bad ? "%" : "";
            string comment = p.State == Common.Elements.Element.States.Bad ? "%BAD" : "";
            line = prefix + line + comment;
            return line;
        }

        public static bool ToIdXYZString(Point p, Common.CoordinateSystem cs, out string line)
        {
            line = "";
            Coordinates c = p._Coordinates.GetCoordinatesInASystemByName(cs._Name);
            if (c == null || !c.AreKnown)
                return false;

            line =
                String.Format("{0,-21}", p._Name) +
                String.Format("{0,13}", String.Format("{0:0.000000}", c.X.Value)) +
                String.Format("{0,13}", String.Format("{0:0.000000}", c.Y.Value));
            switch (cs.type)
            {
                case Coordinates.CoordinateSystemsTypes._3DCartesian:
                    line += String.Format("{0,13}", String.Format("{0:0.000000}", c.Z.Value));
                    break;
                case Coordinates.CoordinateSystemsTypes._2DPlusH:
                    line += String.Format("{0,13}", String.Format("{0:0.000000}", c.Z.Value));
                    break;
                case Coordinates.CoordinateSystemsTypes.AutomaticBasedOnReferenceFrame:
                case Coordinates.CoordinateSystemsTypes.Geodetic:
                case Coordinates.CoordinateSystemsTypes._2DCartesian:
                case Coordinates.CoordinateSystemsTypes.GeodeticSphere:
                case Coordinates.CoordinateSystemsTypes.Unknown:
                default:
                    throw new Exception($"Don't know what to do woth this coordinate type :{cs.type} from {cs._Name}");
            }
            string prefix = p.State == Element.States.Bad ? "%" : "";
            string comment = p.State == Common.Elements.Element.States.Bad ? "%BAD" : "";
            line = prefix + line + comment;
            return true;
        }

        public static string ToIdXYZString(Point p, Coordinates.ReferenceFrames referenceFrame)
        {
            Coordinates c = new Coordinates();
            c.Set();
            switch (referenceFrame)
            {
                case Coordinates.ReferenceFrames.CCS:
                    c = p._Coordinates.Ccs;
                    break;
                case Coordinates.ReferenceFrames.CernXYHg00Machine:
                case Coordinates.ReferenceFrames.CernXYHg00Topo:
                case Coordinates.ReferenceFrames.CernXYHg1985:
                case Coordinates.ReferenceFrames.CernXYHg85Machine:
                case Coordinates.ReferenceFrames.CERNXYHsSphereSPS:
                    c.X = p._Coordinates.Ccs.X;
                    c.Y = p._Coordinates.Ccs.Y;
                    c.Z = p._Coordinates.Ccs.Z;
                    break;
                case Coordinates.ReferenceFrames.LAp0:
                case Coordinates.ReferenceFrames.LGp0:
                case Coordinates.ReferenceFrames.CERN_GRF:
                case Coordinates.ReferenceFrames.ITRF97:
                case Coordinates.ReferenceFrames.WGS84:
                case Coordinates.ReferenceFrames.ROMA40:
                case Coordinates.ReferenceFrames.ETRF93:
                case Coordinates.ReferenceFrames.CH1903plus:
                case Coordinates.ReferenceFrames.CernXYHe:
                case Coordinates.ReferenceFrames.CernX0Y0He:
                case Coordinates.ReferenceFrames.CGRFSphere:
                case Coordinates.ReferenceFrames.SwissLV95:
                case Coordinates.ReferenceFrames.SwissLV03:
                case Coordinates.ReferenceFrames.FrenchRGF93Zone5:
                case Coordinates.ReferenceFrames.Lambert93:
                case Coordinates.ReferenceFrames.RGF93:
                    // case Coordinates.ReferenceFrames.CHTRF95:  //idem RGF93
                    c = p._Coordinates.UserDefined;
                    break;
                case Coordinates.ReferenceFrames.MLA1985Machine:
                case Coordinates.ReferenceFrames.MLA2000Machine:
                case Coordinates.ReferenceFrames.MLASphere:
                case Coordinates.ReferenceFrames.LA1985:
                case Coordinates.ReferenceFrames.LA2000:
                case Coordinates.ReferenceFrames.LASphere:
                case Coordinates.ReferenceFrames.MLG:
                case Coordinates.ReferenceFrames.MLGSphere:
                case Coordinates.ReferenceFrames.LG:
                case Coordinates.ReferenceFrames.LGSphere:
                    c = p._Coordinates.Local;
                    break;
                default:
                    break;
            }

            return
                String.Format("{0,-21}", p._Name) +
                String.Format("{0,13}", String.Format("{0:0.000000}", c.X.Value)) +
                String.Format("{0,13}", String.Format("{0:0.000000}", c.Y.Value)) +
                String.Format("{0,13}", String.Format("{0:0.000000}", c.Z.Value));
        }


        public static string ToIdXyzString(string name, Coordinates c)
        {
            return
                String.Format("{0,-21}", name) +
                String.Format("{0,13}", String.Format("{0:0.000000}", c.X.Value)) +
                String.Format("{0,13}", String.Format("{0:0.000000}", c.Y.Value)) +
                String.Format("{0,13}", String.Format("{0:0.000000}", c.Z.Value));
        }

        internal static List<Point> ConvertAsciiToPoints(string origin, string ascii, Points.PointAsciiFormat format, Coordinates.ReferenceFrames referenceFrame, Coordinates.CoordinateSystemsTypes coordinateSystem)
        {
            List<Point> t = new List<Point>();

            System.IO.StringReader file = new System.IO.StringReader(ascii);
            string line;
            while ((line = file.ReadLine()) != null)
            {
                line = line.Replace("\t", " ");
                if (line.Replace(" ", "") != "")
                {
                    Point point;
                    switch (format)
                    {
                        case Points.PointAsciiFormat.IDXYZ:
                            point = T.Conversions.FileFormat.IdXYZLineToPoint(line, referenceFrame, coordinateSystem);
                            break;
                        case Points.PointAsciiFormat.Geode:
                            point = TSU.IO.SUSoft.Geode.ReadLine(line);
                            break;
                        case Points.PointAsciiFormat.XML:
                        default:
                            point = new Point();
                            break;
                    }
                    point._Origin = origin;
                    t.Add(point);
                }
            }
            return t;
        }
        public static Point ChabaLineToPoint(string lineInChabaFormat)
        {
            try
            {
                Point p = new Point();
                lineInChabaFormat = System.Text.RegularExpressions.Regex.Replace(lineInChabaFormat.Trim(), @"\s+", " ");
                string[] lineFields = lineInChabaFormat.Split(' ');
                p._Name = lineFields[0];
                T.Conversions.StringManipulation.FullNameToSplitName(ref p);
                p._Coordinates.Su.X.ValueFromString(lineFields[1]);
                p._Coordinates.Su.Y.ValueFromString(lineFields[2]);
                p._Coordinates.Su.Z.ValueFromString(lineFields[3]);

                if (lineFields.Length > 4)
                {
                    p._Coordinates.Su.X.SigmaFromString(lineFields[4], 1000);
                    p._Coordinates.Su.Y.SigmaFromString(lineFields[5], 1000);
                    p._Coordinates.Su.Z.SigmaFromString(lineFields[6], 1000);
                }

                return p;
            }
            catch (Exception ex)
            {
                throw new Exception(R.T_CANT_CONVERT_LINE_OUT + ex.Message, ex);
            }
        }
        public static Point IdXYZLineToPoint(string lineIXYZFormat, Coordinates.ReferenceFrames referenceFrame, Coordinates.CoordinateSystemsTypes coordinateSystem)
        {
            try
            {
                Point p = new Point();
                lineIXYZFormat = System.Text.RegularExpressions.Regex.Replace(lineIXYZFormat.Trim(), @"\s+", " ");
                string[] lineFields = lineIXYZFormat.Split(' ');
                p._Name = lineFields[0].ToUpper();
                T.Conversions.StringManipulation.FullNameToSplitName(ref p);
                Coordinates c = new Coordinates();
                c.Set();
                c._Name = p._Name;
                c.ReferenceFrame = referenceFrame;
                c.CoordinateSystem = coordinateSystem;
                c.X.ValueFromString(lineFields[1]);
                c.Y.ValueFromString(lineFields[2]);
                c.Z.ValueFromString(lineFields[3]);

                p.SetCoordinates(c, referenceFrame, coordinateSystem);
                p.Type = Point.Types.Reference;
                return p;
            }
            catch (Exception ex)
            {
                throw new Exception(R.T007 + ex.Message, ex);
            }
        }
        public static Coordinates IdXYZLineToCoordinates(string lineIXYZFormat, Coordinates.ReferenceFrames referenceFrame, Coordinates.CoordinateSystemsTypes coordinateSystem)
        {
            try
            {

                lineIXYZFormat = System.Text.RegularExpressions.Regex.Replace(lineIXYZFormat.Trim(), @"\s+", " ");
                string[] lineFields = lineIXYZFormat.Split(' ');
                if (lineFields.Length < 2) return null;
                Coordinates c = new Coordinates();
                c.Set();
                c._Name = lineFields[0].ToUpper();
                c.ReferenceFrame = referenceFrame;
                c.CoordinateSystem = coordinateSystem;
                c.X.ValueFromString(lineFields[1]);
                c.Y.ValueFromString(lineFields[2]);
                c.Z.ValueFromString(lineFields[3]);
                return c;
            }
            catch (Exception ex)
            {
                throw new Exception(R.T007 + ex.Message, ex);
            }
        }



        public static int FillListFromGeodeInstrumentFile(ref List<I.Instrument> list, string fileName, string description = "Action")
        {
            string line;
            list = new List<I.Instrument>();

            if (System.IO.File.Exists(fileName))
            {
                System.IO.StreamReader file = new System.IO.StreamReader(fileName);
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Trim() == "")
                        continue;
                    if (TrimCleanAndCheckForComment(ref line) == true)
                        continue;

                    List<string> lineFields = new List<string>(line.Split(' '));

                    //Get classe
                    Enum.TryParse(lineFields[0].Trim(), out I.InstrumentClasses instrumentClass);
                    //Get type
                    var field = lineFields[1].Trim().Replace('.', '_');
                    Enum.TryParse(field, out I.InstrumentTypes instrumentType);

                    //Add to list the new instrument
                    list.Add(CreateRightInstrument(instrumentClass, instrumentType, lineFields));
                }
                file.Close();
                file.Dispose();

                // add a polar simulator
                list.Add(CreateRightInstrument(InstrumentClasses.THEODOLITE, InstrumentTypes.Simulator, new List<string>() { "Simulator", "Simulator", "1" }));

                // add virtual reflector
                list.Add(new Reflector() { _Name = R.T_MANAGED_BY_AT40X });

                return list.Count;
            }
            else throw new Exception(string.Format(R.T482, fileName));
        }


        public static bool TrimCleanAndCheckForComment(ref string line)
        {
            line = Regex.Replace(line, @"(\s)\s+", "$1"); //remove multi tab or space
            line = line.Trim(new char[] { '\t', ' ' }); // trim beginning and end

            return (line[0] == '%' || line[0] == '$' || line[0] == '!' || line[0] == '!');

        }


        public static int FillListFromGeodeEtalonnagesFile(ref List<I.Instrument> instruments, string fileName, out string warningMessage, string description = "Action")
        {
            string line;
            warningMessage = "";
            int count = 0;
            List<string> linesContainingParameters = new List<string>();

            int lineNumber = 0;
            try
            {

                if (System.IO.File.Exists(fileName))
                {
                    System.IO.StreamReader file = new System.IO.StreamReader(fileName);
                    line = file.ReadLine();
                    lineNumber++;
                    while (line != null)
                    {
                        if (TrimCleanAndCheckForComment(ref line) == true)
                            continue;

                        linesContainingParameters.Clear();

                        //selecting the lines of ONE etalonnage
                        linesContainingParameters.Add(line);
                        line = file.ReadLine();
                        lineNumber++;

                        while (line[0] != '*')
                        {
                            linesContainingParameters.Add(line);
                            line = file.ReadLine();
                            while (line != null && line.Trim() == "")
                            {
                                line = file.ReadLine();
                                lineNumber++;
                            }
                            lineNumber++;
                            if (line == null) break;
                        }

                        //Selecting the instrument
                        string line2 = Regex.Replace(linesContainingParameters[0], @"(\s)\s+", "$1"); //remove multi tab or space
                        List<string> lineFields = new List<string>(line2.Split(' '));
                        I.Instrument instrument;
                        string nameAndSn = lineFields[1] + "_" + lineFields[2];
                        switch (lineFields[0])
                        {
                            case "*THEO":
                                instrument = (I.TotalStation)instruments.Where(i => i.Id == nameAndSn).FirstOrDefault();
                                if (instrument != null)
                                {
                                    try
                                    {
                                        ((I.TotalStation)instrument).EtalonnageParameterList.Add(I.EtalonnageParameter.Theodolite.LoadEtalonnageParameterFromLinesBlock(linesContainingParameters, instruments));
                                        count++;
                                    }
                                    catch (Exception ex)
                                    {
                                        warningMessage += $"Problem with etalo.dat;{ex.Message}";
                                        //TsuView temp = new TsuView();
                                        //temp.ShowMessageOfExclamation($"Problem with etalo.dat;{ex.Message}");
                                    }
                                }

                                else
                                {
                                    warningMessage += $"{R.T483} ({nameAndSn} {R.T_MISSING_FROM} {fileName})\r\n";
                                    //throw new Exception($"{R.T483} ({nameAndSn})"); we want not to stop everything for taht  or any etanollage would ba available.
                                }


                                break;
                            case "*OFFSET":
                                instrument = (I.OffsetMeter)instruments.Where(i => i.Id == nameAndSn).FirstOrDefault();
                                if (instrument != null)
                                {
                                    ((I.OffsetMeter)instrument).EtalonnageParameter = (I.EtalonnageParameter.OffsetMeter.LoadEtalonnageParameterFromLinesBlock(linesContainingParameters));
                                    count++;
                                }
                                else
                                {
                                    warningMessage += $"{R.T483} ({nameAndSn} {R.T_MISSING_FROM} {fileName})\r\n";
                                    //throw new Exception($"{R.T483} ({nameAndSn})"); we want not to stop everything for taht or any etanollage would ba available.
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    file.Close();
                    file.Dispose();
                    return count;
                }
                else throw new Exception(string.Format(R.T482, fileName));
            }
            catch (Exception ex)
            {
                var a = lineNumber;
                throw;
            }
        }


        public static I.Instrument CreateRightInstrument(I.InstrumentClasses instrumentClass, I.InstrumentTypes instrumentType, List<string> lineFields)
        {
            I.Instrument newInstrument;
            switch (instrumentClass)
            {
                case I.InstrumentClasses.PRISME:
                    newInstrument = new I.Reflector.Reflector();
                    break;
                case I.InstrumentClasses.TACHEOMETRE:
                    I.TotalStationType totalStationType;
                    Enum.TryParse(lineFields[1].Trim(), out totalStationType);
                    switch (totalStationType)
                    {
                        case I.TotalStationType.TDA5005:
                            newInstrument = new D.TDA5005.Instrument();
                            break;
                        case I.TotalStationType.TC2002:
                            newInstrument = new D.TC2002.Instrument();
                            break;
                        case I.TotalStationType.TCRA1101:
                            newInstrument = new D.TCRA1100.Instrument();
                            break;
                        case I.TotalStationType.TS60:
                            newInstrument = new D.TS60.Instrument();
                            break;
                        default:
                            newInstrument = new I.TotalStation();
                            break;
                    }
                    break;
                case I.InstrumentClasses.LASER_TRACKER:
                    newInstrument = new D.AT40x.Instrument();
                    break;
                case I.InstrumentClasses.THEODOLITE:
                    switch (instrumentType)
                    {
                        case I.InstrumentTypes.T3000:
                            newInstrument = new D.T3000.Instrument();
                            break;
                        case InstrumentTypes.Simulator:
                            newInstrument = new D.Simulator.Instrument();
                            break;

                        default:
                            newInstrument = new D.Manual.Theodolite.Instrument();
                            break;
                    }

                    break;
                case I.InstrumentClasses.TILTMETRE:
                    switch (instrumentType)
                    {
                        case I.InstrumentTypes.WYLER:
                            newInstrument = new D.WYLER.Instrument();
                            break;
                        default:
                            newInstrument = new D.Manual.TiltMeter.Instrument();
                            break;
                    }

                    break;
                case I.InstrumentClasses.TRIPOD:
                    newInstrument = new I.Reflector.Reflector(); break;
                case I.InstrumentClasses.CHAINE:
                    newInstrument = new I.Sensor(); break;
                case I.InstrumentClasses.DISTINVAR:
                    newInstrument = new I.Sensor(); break;
                case I.InstrumentClasses.ECARTOMETRE:
                    I.OffsetMeterType offsetMeterType;
                    Enum.TryParse(lineFields[1].Trim(), out offsetMeterType);
                    switch (offsetMeterType)
                    {
                        case I.OffsetMeterType.PLATINE:
                            newInstrument = new D.Manual.Ecartometer.Instrument();
                            break;
                        case I.OffsetMeterType.R_AUTO:
                            // to change with AutomaticEcartometer when implemented
                            newInstrument = new D.Manual.Ecartometer.Instrument();
                            break;
                        case I.OffsetMeterType.REGLE_LTD:
                            newInstrument = new D.Manual.Ecartometer.Instrument();
                            break;
                        case I.OffsetMeterType.R_MANUEL:
                            newInstrument = new D.Manual.Ecartometer.Instrument();
                            break;
                        case I.OffsetMeterType.R_MIROIR:
                            newInstrument = new D.Manual.Ecartometer.Instrument();
                            break;
                        case I.OffsetMeterType.R_SYLVAC:
                            newInstrument = new D.Manual.Ecartometer.Instrument();
                            break;
                        case I.OffsetMeterType.RS_1000:
                            newInstrument = new D.Manual.Ecartometer.Instrument();
                            break;
                        case I.OffsetMeterType.RS_1480:
                            newInstrument = new D.Manual.Ecartometer.Instrument();
                            break;
                        case I.OffsetMeterType.RS_330:
                            newInstrument = new D.Manual.Ecartometer.Instrument();
                            break;
                        case I.OffsetMeterType.RS_850:
                            newInstrument = new D.Manual.Ecartometer.Instrument();
                            break;
                        default:
                            newInstrument = new I.Sensor();
                            break;
                    }
                    ; break;
                case I.InstrumentClasses.EDM:
                    newInstrument = new I.Sensor(); break;
                case I.InstrumentClasses.FIL:
                    newInstrument = new I.Reflector.Reflector(); break;
                case I.InstrumentClasses.GABARIT:
                    newInstrument = new D.Manual.TiltMeter.Instrument(); break;
                case I.InstrumentClasses.GYROSCOPE:
                    newInstrument = new I.Sensor(); break;
                case I.InstrumentClasses.JAUGE_A_TILT:
                    newInstrument = new D.Manual.TiltMeter.Instrument(); break;
                case I.InstrumentClasses.INTERFACE:
                    I.InstrumentTypes type;
                    Enum.TryParse(lineFields[1].Trim(), out type);
                    switch (type)
                    {
                        case I.InstrumentTypes.JT:
                        case I.InstrumentTypes.GA:
                            newInstrument = new D.Manual.TiltMeter.Instrument(); break;
                        default:
                            // normaly should not be roll sensor because probably a extension/rallonge but better than nothing to avoid bugs
                            newInstrument = new D.Manual.TiltMeter.Instrument(); break;
                    }
                    break;
                case I.InstrumentClasses.MEKOMETRE:
                    newInstrument = new I.Sensor(); break;
                case I.InstrumentClasses.MIRE:
                    I.StaffType staffType;
                    Enum.TryParse(lineFields[1].Trim(), out staffType);
                    newInstrument = new I.LevelingStaff(staffType);
                    break;
                case I.InstrumentClasses.NIVEAU:
                    I.LevelType levelType;
                    Enum.TryParse(lineFields[1].Trim(), out levelType);
                    switch (levelType)
                    {
                        case I.LevelType.DINI11:
                            newInstrument = new D.Manual.Level.Instrument(levelType);
                            break;
                        case I.LevelType.DINI12:
                            newInstrument = new D.Manual.Level.Instrument(levelType);
                            break;
                        case I.LevelType.DNA03:
                            newInstrument = new D.DNA03.Instrument();
                            break;

                        case I.LevelType.LS15:
                            newInstrument = new D.LS15.Instrument();
                            break;
                        case I.LevelType.NA2:
                            newInstrument = new D.Manual.Level.Instrument(levelType);
                            break;
                        case I.LevelType.NA3000:
                            newInstrument = new D.Manual.Level.Instrument(levelType);
                            break;
                        case I.LevelType.N3:
                            newInstrument = new D.Manual.Level.Instrument(levelType);
                            break;
                        case I.LevelType.TRAIN:
                            newInstrument = new D.Manual.Level.Instrument(levelType);
                            break;
                        default:
                            newInstrument = new D.Manual.Level.Instrument(levelType);
                            break;
                    }
                    break;
                default:
                    newInstrument = new I.Sensor();
                    break;
            }
            newInstrument._InstrumentClass = instrumentClass;
            newInstrument._InstrumentType = instrumentType;
            //Get model
            newInstrument._Model = lineFields[1].Trim();
            //Get SerialNumber
            newInstrument._SerialNumber = lineFields[2].Trim();
            //Build ID
            newInstrument.Id = lineFields[1].Trim() + "_" + lineFields[2].Trim();
            //Build Name, to check if OK with theodolite
            newInstrument._Name = newInstrument._Model + "_" + newInstrument._SerialNumber;
            return newInstrument;
        }

        #endregion
    }
}
