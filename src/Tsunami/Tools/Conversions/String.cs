﻿using System;
using System.Collections.Generic;
using TSU;
using System.Text;
using R = TSU.Properties.Resources;
using System.Linq;
using TSU.Views;
using System.Text.RegularExpressions;
using TSU.Common.Instruments;

using TSU.Common.Elements;
using M = TSU.Common.Measures;
using TSU.Common.Elements.Composites;
using TSU.Common.Zones;
using TSU.Common.Operations;
using D = TSU.Common.Instruments.Device;
using TSU.ENUM;
using MathNet.Numerics.Integration;
using TSU.Functionalities.TestModules;
using static TSU.Polar.GuidedModules.Steps.StakeOut;
using static TSU.Polar.GuidedModules.Steps.StakeOut.Tools;
using TSU.Common;
using System.Globalization;

namespace TSU.Tools.Conversions
{
    public static class StringManipulation
    {

        private const double na = TSU.Preferences.Preferences.NotAvailableValueNa;

        #region StringManupilation

        public static string AddCharsInFront(string text, char character, int totalFinalLength)
        {
            string result = text;
            while (result.Length< totalFinalLength)
            {
                result = character + result;
            }
            return result;
        }

        public static string AddCharsAfter(string text, char character, int totalFinalLength)
        {
            string result = text;
            while (result.Length < totalFinalLength)
            {
                result = result+ character;
            }
            return result;
        }

        public static string ToStringOfNChar(int integer, int numberOfChar)
        {
            string zeros = "";
            for (int i = 0; i < numberOfChar; i++)
            {
                zeros += "0";
            }
            return integer.ToString(zeros);
        }
        public static string ToString(Zone o)
        {
            return
                o._Name.Replace(" ", "_") + "  " +
                o.Ccs2MlaInfo.Origin.X.Value + "  " +
                o.Ccs2MlaInfo.Origin.Y.Value + "  " +
                o.Ccs2MlaInfo.Origin.Z.Value + "  " +
                o.Ccs2MlaInfo.Bearing.Value + "  " +
                o.Ccs2MlaInfo.Slope.Value;
        }
        public static string ToString(Coordinates c)
        {
            int i = 0;
            if (c._Name == null) c._Name =R.String_Unknown + i.ToString();
            return
                c._Name + "  " +
                c.X.Value + "  " +
                c.Y.Value + "  " +
                c.Z.Value;
        }
        public static string ToStringWithOneSigma(double Value, double Sigma, int numberOfDecimals)
        {
            string s;
            int multiplicator = 1;
            for (int i = 0; i < numberOfDecimals; i++)
            {
                multiplicator *= 10;
            }
            if (Value != na)
            {
                s = WithGivenNumberOfDecimal(Value, numberOfDecimals);
                if (Sigma != 0 && Sigma != na)
                    s += "(" + string.Format("{0:0}", Sigma * multiplicator) + ")";
            }
            else
                s = WithGivenNumberOfDecimal(Value, 1);

            return s;

        }

        public static string WithGivenNumberOfDecimal(double value, int numberOfDecimals)
        {
            NumberFormatInfo nfi = new  NumberFormatInfo();
            nfi.NumberDecimalDigits = numberOfDecimals;
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberGroupSeparator = "";

            return value.ToString("N", nfi);
        }

        // return a string with the value(sigma) with a given number of decimals
        public static string DoubleValueToStringWithOneSigma(DoubleValue d, int numberOfDecimals, bool showSigma = false)
        {
            if (d.Sigma == na || d.Sigma is double.NaN || showSigma)
                return ToStringWithOneSigma(d.Value , 0, numberOfDecimals);
            else
                return ToStringWithOneSigma(d.Value, d.Sigma, numberOfDecimals);
        }

        // return a string with the value(sigma) with a given number of decimals
        public static string DoubleValueToStringWithOneSigma(DoubleValue d, int numberOfDecimals, double factor, bool showSigma = false)
        {
            if (d.IsNa)
                factor = 1;
            double value = d.Value * factor;
            double sigma;
            if (d.Sigma == na || d.Sigma is double.NaN)
                sigma = na;
            else
                sigma = d.Sigma;
            if (!showSigma)
                sigma = na;
            return ToStringWithOneSigma(value, sigma, numberOfDecimals);
        }
        #endregion



        internal static string LengthOf20String(string p)
        {
            return string.Format("{0,22}", p);
        }
        internal static string LengthOf13String(string p)
        {
            return string.Format("{0,13}", p);
        }

        public static void FullNameToSplitName(ref Point point)
        {
            try
            {
                string[] parts = point._Name.Split('.');

                int l = parts.Length;
                switch (l)
                {
                    case 4: 
                        point._Zone = parts[l - 4];
                        point._Accelerator = point._Zone;
                        point._ClassAndNumero = parts[l - 3]+"."+parts[l - 2];
                        point._Point = parts[l - 1];
                        break;
                    case 3:
                        point._ClassAndNumero = parts[l - 3] + "." + parts[l - 2];
                        point._Point = parts[l - 1];
                        break;
                    case 2:
                        point._ClassAndNumero = "."+parts[l - 2];
                        point._Point = parts[l - 1];
                        break;
                    default:
                        point._Point = parts[l - 1];
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(R.T_CANT_CONVERT_LINE_RES + ex.Message, ex);
            }
        }

        //#region FromLine
        //public static Reflector GetReflectorFromLine(string line)
        //{
        //    Reflector r = null;
        //    if (line[0] != '%')
        //    {
        //        List<string> lineFields = new List<string>(line.Split(' '));
        //        if (lineFields[0] == "*PR")
        //        {
        //            r = new Reflector();
        //            r.CalibrationDate = Convert.ToDateTime(lineFields[1]);
        //            //Get Reflector of reference
        //            InstrumentTypes reflectorType;
        //            lineFields[2].Replace('.', '_'); //because canoot put '.' in the enum "CCR0.5 >> CCR0_5"
        //            Enum.TryParse(lineFields[2].Trim(), out reflectorType);
        //            r.ReferenceReflectorSN = lineFields[3].Trim();
        //            //Get type
        //            lineFields[4].Replace('.', '_'); //because canoot put '.' in the enum "CCR0.5 >> CCR0_5"
        //            Enum.TryParse(lineFields[4].Trim(), out reflectorType);
        //            r._InstrumentType = reflectorType;
        //            r._Id = lineFields[5].Trim();
        //            r._Id = lineFields[5].Trim();
        //            r.Constante = Compute.Conversions.Numbers.ToDouble(lineFields[6].Trim());
        //        }
        //    }
        //    return r;
        //}
        //#endregion

        /// <summary>
        /// Will return the first part of a string containting a ';'
        /// </summary>
        internal static string SeparateTitleFromMessage(string titleAndMessage)
        {
            if (titleAndMessage == "" || titleAndMessage == null) return "";
            titleAndMessage = titleAndMessage.Replace("--->", "\r\n -->");
            int index = titleAndMessage.IndexOf(';');
            if (index > -1)
                return titleAndMessage.Substring(0, index);
            else
            {
                index = titleAndMessage.IndexOf("\r\n");
                if (index > -1)
                    return titleAndMessage.Substring(0, index);
                else
                {
                    if (titleAndMessage.Length > 100)
                        return titleAndMessage.Substring(0, 99);
                    else
                        return titleAndMessage;
                }
            }
        }

        /// <summary>
        /// Will return the second part of a string containting a ';'
        /// </summary>
        internal static string SeparateMessageFromTitle(string titleAndMessage)
        {
            if (titleAndMessage == "" || titleAndMessage == null) return "";
            int index = titleAndMessage.IndexOf(';');
            if (index > -1)
                return titleAndMessage.Substring(index + 1, titleAndMessage.Length - index - 1);
            else
                return titleAndMessage;
                
        }


         /// <summary>
         /// A text showing the default parameters of the station with the list of point to measure
         /// <summary>
        internal static string ToString(CloneableList<Point> list,Common.Station station)
        {
            string text = "";

            text += station.ToString();

            text += $"\r\n{R.T_POINTS_TO_MEASURE}:\r\n";

            foreach (Point p in list)
                text += string.Format("{0}",p._Name) + "\r\n";

            return text;
        }

        internal static IEnumerable<string> GetSimilarNames(string startingName, List<string> existingNames)
        {
            List<string> similarNames = new List<string>();

            similarNames.AddRange(Common.Analysis.Element.Iteration.GetNext(new List<string>() { startingName }, existingNames, Common.Analysis.Element.Iteration.Types.All));

            int parenthesisOpeningPosition = startingName.IndexOf('(');
            if (parenthesisOpeningPosition != -1)
            {
                int parenthesisClosingPosition = startingName.IndexOf(')');
                string insideParenthesis = startingName.Substring(parenthesisOpeningPosition, parenthesisClosingPosition - parenthesisOpeningPosition);
                if (double.TryParse(insideParenthesis, out double result))
                {
                    result++;
                    string tempName = startingName.Replace($"({insideParenthesis})", $"({result})");
                    while (existingNames.Contains(tempName))
                    {
                        result++;
                        tempName = startingName.Replace($"({insideParenthesis})", $"({result})");
                    }
                    similarNames.Add(tempName);
                }
                else
                    similarNames.Add(startingName + "(1)");
            }
            else
                similarNames.Add(startingName + "(1)");

            if (startingName.Contains("["))
            {
                similarNames.AddRange(Common.Analysis.Element.Iteration.GetNext(new List<string>() { startingName }, existingNames, Common.Analysis.Element.Iteration.Types.InBrackets));
            }
            else
            {
                int counter = 1;
                string tempName = startingName + "[" + counter.ToString("D3") + "]";
                while (existingNames.Contains(tempName))
                {
                    counter++;
                    tempName = startingName + "[" + counter.ToString("D3") + "]";
                }
                similarNames.Add(tempName);
            }



            List<string> names;
            names = new List<string>() { startingName };
            DoWeRenameAndAdd(names, new List<string>() { "U", "D" });
            DoWeRenameAndAdd(names, new List<string>() { "R", "L" });
            DoWeRenameAndAdd(names, new List<string>() { "A", "C" });
            DoWeRenameAndAdd(names, new List<string>() { "J", "S" });
            DoWeRenameAndAdd(names, new List<string>() { "T", "B" });
            similarNames.AddRange(names);

            names = new List<string>() { startingName };
            DoWeRenameAndAdd(names, new List<string>() { "NEW", "OLD" });
            similarNames.AddRange(names);

            

            return similarNames;
        }

        private static void DoWeRenameAndAdd(List<string> similarNames, List<string> opposites)
        {
            List<string> separtors = new List<string> { "-", "_", "." };
            List<string> newOnes = new List<string>();
            foreach (var s in separtors)
            {
                foreach (var o in opposites)
                {
                    foreach (string name in similarNames)
                    {
                        if (name.Contains(s + o + s))
                        {
                            foreach (var o2 in opposites)
                            {
                                if (o2!= o)
                                    newOnes.Add(name.Replace(s + o + s, s + o2 + s));
                            }
                        }
                    }
                }
            }
            similarNames.AddRange(newOnes);
        }
    }
}
