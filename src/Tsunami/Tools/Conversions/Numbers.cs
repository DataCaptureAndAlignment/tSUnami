﻿using System;
using System.Collections.Generic;
using System.Text;
using R = TSU.Properties.Resources;
using System.Linq;
using TSU.Views;
using System.Text.RegularExpressions;
using TSU.Common.Instruments;

using TSU.Common.Elements;
using M = TSU.Common.Measures;
using TSU.Common.Elements.Composites;
using TSU.Common.Zones;
using TSU.Common.Operations;

using D = TSU.Common.Instruments.Device;
using TSU.ENUM;


namespace TSU.Tools.Conversions
{
    public static class Numbers
    {
        #region Numbers format
        public static bool IsNumeric(string value)
        {
            double numericValue;
            return double.TryParse(value, out numericValue);
        }

        public static string[] GetNumerics(string[] values)
        {
            return values.Where(s => IsNumeric(s)).ToArray();
        }

        public static double ToDouble(string text, bool useCurrentLocalCulture=false, double defaultValue = -999.99)
        {
            if (defaultValue == -999.99)
            {
                defaultValue =TSU.Tsunami2.Preferences.Values. na;
            }
            double d;
            if (useCurrentLocalCulture)
            {
                if (double.TryParse(text, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture, out d)) return d;
            }
            else
                if (double.TryParse(text, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out d)) return d;


            return defaultValue;
        }

        public static DoubleValue ToDoubleValue(string text, bool useCurrentLocalCulture = false, double defaultValue = -999.99)
        {
            if (defaultValue == -999.99)
            {
                defaultValue =TSU.Tsunami2.Preferences.Values.na;
            }
            double d;
            if (useCurrentLocalCulture)
            {
                if (double.TryParse(text, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture, out d)) return new DoubleValue(d,0);
            }
            else
                if (double.TryParse(text, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out d)) return new DoubleValue(d, 0);


            throw new Exception("Not a number");
        }

        public static bool ToDouble(string text, out double d, bool useCurrentLocalCulture = false, double defaultValue = -999.99)
        {
            if (defaultValue == -999.99)
            {
                defaultValue =TSU.Tsunami2.Preferences.Values.na;
            }
            if (useCurrentLocalCulture)
                return double.TryParse(text, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture, out d) ;
            else
                 return double.TryParse(text, out d);
        }

        public static int ToInt(string text)
        {
            int i;
            if (int.TryParse(text, out i))
                return i;
            else
                return -999;
        }

        public static int ToInt(string text, int defaultValue)
        {
            int i;
            if (int.TryParse(text, out i))
                return i;
            else
                return defaultValue;
        }

        public static char ToChar(string text)
        {
            char c;
            if (char.TryParse(text, out c))
                return c;
            else
                return 'A';
        }
        public static char ToChar(string text, char defaultValue)
        {
            char c;
            if (char.TryParse(text, out c))
                return c;
            else
                return defaultValue;
        }

        internal static void SeperatePrefixFromIterationNumber(string name, out string prefix, out int number, out bool hadBrackets)
        {
            if (name.Contains("["))
            {
                hadBrackets = true;
                string[] parts = name.Split('[', ']');
                if (parts.Length > 1)
                {
                    prefix = parts[0];
                    int.TryParse(parts[1], out number);

                }
                else
                {
                    prefix = name;
                    number = -1;
                }
            }
            else
            {
                hadBrackets = false;
                string n = "";
                int i;
                while (int.TryParse(name.Substring(name.Length - 1), out i))
                {
                    n = name.Substring(name.Length - 1, 1) + n;
                    name = name.Substring(0, name.Length - 1);
                }

                if (n != "")
                    if (int.TryParse(n, out i))
                        number = i;
                    else
                        number = 1;
                else
                    number = -1;
                prefix = name;
            }
        }
        #endregion

        
    }
}
