﻿ using System;
using System.Collections.Generic;
using TSU;
using TSU.Common.Elements;
using E = TSU.Common.Elements;

namespace TSU.Tools.Conversions
{
    public static class Points
    {
        private const double na = TSU.Preferences.Preferences.NotAvailableValueNa;

        internal static E.Element List2Composite(CloneableList<E.Point> referencePointsToMeasure)
        {
            var ce = new E.Composites.CompositeElement();

            foreach (var item in referencePointsToMeasure)
            {
                ce.Elements.Add(item as Element);
            }

            return ce;

        }

        public enum PointAsciiFormat
        {
            IDXYZ, Geode, XML
        }
    }
}
