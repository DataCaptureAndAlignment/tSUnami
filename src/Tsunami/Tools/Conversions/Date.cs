﻿using System;

namespace TSU.Tools.Conversions
{
    public static class Date
    {

        #region Date
        public static string ToDateUpSideDown(DateTime date)
        {
            return $"{date:yyyy-MM-dd_HH-mm-ss}";
        }

        public static string ToDateUpSideDownPrecise(DateTime date)
        {
            return $"{date:yyyy-MM-dd_HH-mm-ss-fffffff}";
        }

        public static DateTime ToDate(string text)
        {
            DateTime c;
            if (DateTime.TryParse(text, out c))
                return c;
            else
                return DateTime.Now; ;
        }
        #endregion

    }
}
