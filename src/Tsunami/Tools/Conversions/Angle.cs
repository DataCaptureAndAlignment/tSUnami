﻿using System;
using System.Collections.Generic;
using TSU;
using System.Text;
using R = TSU.Properties.Resources;
using System.Linq;
using TSU.Views;
using System.Text.RegularExpressions;

using TSU.Common.Elements;
using M = TSU.Common.Measures;
using TSU.Common.Elements.Composites;
using TSU.Common.Zones;
using TSU.Common.Operations;

using D = TSU.Common.Instruments.Device;
using TSU.ENUM;
using TSU.Common.Instruments.Reflector;

namespace TSU.Tools.Conversions
{
    public static class Angles
    {
        public static class Gon
        {
            public static double ToRad(double angle)
            {
                return angle / 200 * Math.PI;
            }

            public static DoubleValue ToRad(DoubleValue dv)
            {
                return new DoubleValue (dv.Value / 200 * Math.PI, dv.Sigma / 200 * Math.PI);
            }
        }

        #region Angles
        public static string DecimalToArbitrarySystem(long decimalNumber, int radix)
        {
            const int BitsInLong = 64;
            const string Digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            if (radix < 2 || radix > Digits.Length)
                throw new ArgumentException(string.Format(R.T_RADIX, Digits.Length.ToString()));

            if (decimalNumber == 0)
                return "0";

            int index = BitsInLong - 1;
            long currentNumber = Math.Abs(decimalNumber);
            char[] charArray = new char[BitsInLong];

            while (currentNumber != 0)
            {
                int remainder = (int)(currentNumber % radix);
                charArray[index--] = Digits[remainder];
                currentNumber = currentNumber / radix;
            }

            string result = new String(charArray, index + 1, BitsInLong - index - 1);
            if (decimalNumber < 0)
            {
                result = "-" + result;
            }

            return result;
        }
        public static double Gon2Rad(double gon)
        {
            return gon / 200 * Math.PI;
        }
        public static double Rad2Gon(double rad)
        {
            return rad * 200 / Math.PI;
        }
        public static double Gon2Deg(double gon)
        {
            return gon / 200 * 180;
        }

        internal static void FillListFromPrismConstanteFile(ref List<Reflector> reflectors, string fileName, string description = "Action")
        {
            string line;
            List<string> linesContainingParameters = new List<string>();

            if (System.IO.File.Exists(fileName))
            {
                System.IO.StreamReader file = new System.IO.StreamReader(fileName);
                line = file.ReadLine();
                while (line != null)
                {
                    if (Conversions.FileFormat.TrimCleanAndCheckForComment(ref line) == true)
                        continue;
                    linesContainingParameters.Clear();
                    //selecting the lines of ONE etalonnage
                    linesContainingParameters.Add(line);
                    line = file.ReadLine();
                    if (line == null) return;
                    while (line[0] != '*')
                    {
                        linesContainingParameters.Add(line);
                        line = file.ReadLine();
                        if (line == null) break;
                    }

                    //Selecting the instrument
                    string line2 = Regex.Replace(linesContainingParameters[0], @"(\s)\s+", "$1"); //remove multi tab or space
                    List<string> lineFields = new List<string>(line2.Split(' '));
                    Reflector r;
                    switch (lineFields[0])
                    {
                        case "*PR":
                            r = reflectors.Find(x => x._Model == lineFields[4] && x._SerialNumber == lineFields[5]);
                            if (r != null)
                            {
                                double c;
                                if (double.TryParse(lineFields[6], out c))
                                    r.Constante = c;
                            }
                            else throw new Exception($"{R.T_PROBLEM_FILLING_FROM_FILE} '{fileName}' {R.T_LINE} : {line2}");



                            break;
                        default:
                            break;
                    }
                }

                file.Close();
                file.Dispose();
            }
            else throw new Exception(string.Format(R.T482, fileName));
        }

        public static double Deg2Gon(double deg)
        {
            return deg * 200 / 180;
        }
        public static double Rad2Deg(double rad)
        {
            return rad * 180 / Math.PI;
        }
        public static double Deg2Rad(double deg)
        {
            return deg * Math.PI / 180;
        }
        #endregion
    }
}
