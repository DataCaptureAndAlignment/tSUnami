﻿using Accord.Video.FFMPEG;
using Microsoft.WindowsAPICodePack.Net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using static TSU.Tools.Macro;
using TSU.Views.Message;
using R = TSU.Properties.Resources;
using System.Management;
using TSU.Common.Compute.Transformation;
using TSU.Logs;
using System.Text.RegularExpressions;

namespace TSU
{
    public static class Debug
    {
        public static void Timing(string message)
        {
            TSU.Debug.WriteInConsole($"T >>> {DateTime.Now.Second} {DateTime.Now.Millisecond} : {message}");
        }

        public static bool IsRunningInATest
        {
            get
            {
                return Globals.UnitTestDetector.RunningInTest;
            }
        }

        class MonitoredEvent
        {
            public int uniqueId = -1;
            public Stopwatch stopwatch = new Stopwatch();
            public MonitoredEvent(int uniqueId)
            {
                this.uniqueId = uniqueId;
                stopwatch.Start();
            }
            public long GetExecutionTime()
            {
                this.stopwatch.Stop();
                return stopwatch.ElapsedMilliseconds;
            }
        }

        internal static void MouseAndKeyboardMonitoring_Unsubscribe()
        {
            if (IsRunningInATest || Debug.Debugging)
                return;

            MouseHook.OnMouseUp -= MouseHook_OnMouseUp;
            MouseHook.OnMouseDown -= MouseHook_OnMouseDown;
            MouseHook.StopHook();
            isMouseHooked = false;


        }

        public static bool isMouseHooked = false;

        public static void MouseAndKeyboardMonitoring_Subscribe()
        {
            if ((Debug.IsRunningInATest))
                return;
            isMouseHooked = true;
            MouseHook.StartHook();
            MouseHook.OnMouseUp += TSU.Tools.Macro.MouseHook_OnMouseUp;
            MouseHook.OnMouseDown += TSU.Tools.Macro.MouseHook_OnMouseDown;
        }

        public static class Screenshots
        {
            private static List<Bitmap> screenshots = new List<Bitmap>();
            private static string directory = "";
            private static int count = 0;
            private static int errorCount = 0;

            public static void Take()
            {
                var tsunamiView = Tsunami2.Properties.View;
                try
                {
                    try
                    {
                        Screenshots.InitializeDirectory();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Cannot create Screenshots directory", ex);
                    }

                    Rectangle bounds;
                    try
                    {
                        // Get the bounds of the form
                        bounds = Screen.FromControl(tsunamiView).Bounds;
                    }
                    catch (Exception ex )
                    {
                        throw new Exception("Cannot get tsunami bounds", ex);
                    }

                    // Create a bitmap to hold the screenshot
                    using (Bitmap originalBitmap = new Bitmap(bounds.Width, bounds.Height))
                    {
                        try
                        {
                            // Use graphics to copy the screen to the bitmap
                            using (Graphics g = Graphics.FromImage(originalBitmap))
                            {
                                g.CopyFromScreen(bounds.Location, Point.Empty, bounds.Size);

                                // Get the mouse position relative to the screen
                                Point mousePosition = Control.MousePosition;

                                // Define the size and position of the red square
                                int squareSize = 50; // Size of the red square
                                Rectangle squareRect = new Rectangle(mousePosition.X + squareSize / 2, mousePosition.Y + squareSize / 2, squareSize, squareSize);

                                // Draw the red square
                                using (Brush brush = new SolidBrush(Color.Red))
                                {
                                    g.FillRectangle(brush, squareRect);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Cannot copying screen for screenshot", ex);
                        }

                        // Define the new size for the resized bitmap (100% of original size)
                        int newWidth = originalBitmap.Width;
                        int newHeight = originalBitmap.Height;

                        // Create a new bitmap with the new size
                        try
                        {
                            using (Bitmap resizedBitmap = new Bitmap(newWidth, newHeight))
                            {
                                // Draw the original bitmap onto the resized bitmap
                                using (Graphics g = Graphics.FromImage(resizedBitmap))
                                {
                                    g.DrawImage(originalBitmap, 0, 0, newWidth, newHeight);
                                }

                                // Save the resized bitmap to disk
                                string screenshotPath = System.IO.Path.Combine(directory, $"screenshot_{count:D4}.jpg");
                                resizedBitmap.Save(screenshotPath, ImageFormat.Jpeg);

                                count++;
                                Console.WriteLine($"Screenshot {count} taken and saved to {screenshotPath}.");
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Cannot Draw and save the bitmap", ex);
                        }
                    }
                }
                catch (Exception ex)
                {
                    errorCount++;
                    if (errorCount < 3)
                    {
                        string message = $"An error occurred while taking a screenshot: {ex.Message}";
                        Logs.Log.AddEntry(null, ex);
                        MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        string message = $"Too much error trying to take screenshots, the video and screenshots are cancelled:\r\n{ex.Message}";
                        Logs.Log.AddEntry(null, ex);
                        Tsunami2.Preferences.Values.GuiPrefs.MakeVideoFromScreenshots.Set(false);
                        Tsunami2.Preferences.Values.GuiPrefs.MakeScreenshotAtEveryMouseClick.Set(false);
                        MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }

            internal static void InitializeDirectory()
            {
                if (directory == "")
                    directory = System.IO.Path.Combine(Tsunami2.Preferences.Values.Paths.MeasureOfTheDay, "Screenshots/");
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
            }

            static bool CheckForCodec(string codecName)
            {
                string query = "SELECT * FROM Win32_CodecFile";

                using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(query))
                using (ManagementObjectCollection codecs = searcher.Get())
                {
                    foreach (ManagementObject codec in codecs)
                    {
                        string name = codec["Name"]?.ToString();
                        string description = codec["Description"]?.ToString();

                        // Use IndexOf for case-insensitive comparison
                        if (!string.IsNullOrEmpty(name) && name.IndexOf(codecName, StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            Logs.Log.AddEntryAsSuccess(Tsunami2.Preferences.Tsunami, $"Codec found: {name}, Description: {description}");
                            return true ;
                        }
                    }
                }
                Logs.Log.AddEntryAsSuccess(Tsunami2.Preferences.Tsunami, $"\"Codec '{codecName}' not found.");
                Console.WriteLine("Codec not found.");
                return false;
            }

            public static bool CreateOrNotAVideoInAThread(out Thread thread, string path = "", bool clean = true)
            {
                thread = null;
                try
                {
                    if (!Tsunami2.Preferences.Values.GuiPrefs.MakeVideoFromScreenshots.IsTrue)
                        return false;

                    if (path == "")
                        path = Regex.Replace(Tsunami2.Properties.PathOfTheSavedFile, "\\.tsu", $"_{DateTime.Now.ToString("HH-mm-ss")}.mp4", RegexOptions.IgnoreCase);
                    else
                        path = $@"{Tsunami2.Preferences.Values.Paths.MeasureOfTheDay}\Video_{DateTime.Now.ToString("HH-mm-ss")}.mp4";

                    if (path == "")
                        return false;

                    if (IsRunningInATest)
                        return false;

                    new MessageInput(MessageType.GoodNews, "Creating a video from the screenShoots").Show();
                    thread = new Thread(() =>
                    {
                        CreateVideo(path, directory, clean);
                    });
                    thread.IsBackground = false; // true Ensure it's a foreground thread, the process will not finish as long as foreground thread are running
                    thread.Start();
                    return true;
                }
                catch (Exception ex)
                {
                    new MessageInput(MessageType.Critical, $"Video no created;{ex.Message}").Show();
                    return false;
                }
            }

            private static void CreateVideo(string videoPath, string screenShotDirectory, bool clean = true)
            {
                try
                {
                    Logs.Log.AddEntryAsFYI(null, "Creating a video.");
                    var screenshotFiles = Directory.GetFiles(screenShotDirectory, "screenshot_*.jpg").OrderBy(f => f).ToList();
                    if (screenshotFiles.Count == 0)
                    {
                        Logs.Log.AddEntryAsFYI(null, "No screenshots available to create a video.");
                        return;
                    }

                    // Define the video file path and the frame rate
                    int frameRate = 1; // 1 frame per second

                    // Create a new video file writer
                    using (VideoFileWriter writer = new VideoFileWriter())
                    {
                        int prefBitRate = Tsunami2.Preferences.Values.GuiPrefs.VideoBitRateKb;

                        // Open the video file with the first screenshot
                        try
                        {
                            using (Bitmap firstImage = (Bitmap)Image.FromFile(screenshotFiles[0]))
                            {
                                Logs.Log.AddEntryAsFYI(null, $"Creating a video with resolution: {firstImage.Width} x {firstImage.Height}");
                                writer.Open(videoPath, firstImage.Width, firstImage.Height, frameRate, VideoCodec.H264, prefBitRate * 1000);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logs.Log.AddEntryAsFYI(null, $"Failed to open video writer: {ex.Message}");
                            return; // Exit if the writer fails to open
                        }

                        // Write each screenshot to the video file
                        Logs.Log.AddEntryAsFYI(null, $"Creating a video from {screenshotFiles.Count} screenshot");
                        foreach (var screenshotFile in screenshotFiles)
                        {
                            using (Bitmap image = (Bitmap)Image.FromFile(screenshotFile))
                            {
                                writer.WriteVideoFrame(image);
                            }
                        }

                        // Close the video file
                        writer.Close();
                    }
                    Logs.Log.AddEntryAsFYI(null, $"Video created successfully at {videoPath}");
                }
                catch (Exception ex)
                {
                    Logs.Log.AddEntryAsFYI(null, $"An error occurred while creating video: {ex.Message} {ex.StackTrace}");
                }

                try
                {
                    if (clean)
                    {
                        System.IO.Directory.Delete(screenShotDirectory, true);
                        Logs.Log.AddEntryAsFYI(null, $"Screenshot Directory cleaned");
                    }
                }
                catch (Exception ex)
                {
                    Logs.Log.AddEntryAsFYI(null, $"An error occurred while cleaning screenhots: {ex.Message}");
                }
            }
        }

        static List<MonitoredEvent> monitoredEvents = new List<MonitoredEvent>();

        static DateTime lastDebufConsoleEntry;


        #region GetNextID
        private static int nextId = 1;
        private static readonly object forLock = new object();
        /// <summary>
        /// Return an unique int id, to be used in WriteInConsole calls for example.
        /// Note : the number will only be unique if you do less than int.MaxValue calls
        /// </summary>
        /// <returns></returns>
        public static int GetNextId()
        {
            int res;

            lock (forLock)
            {
                res = nextId++;
            }

            return res;
        }        
        #endregion

        public static void WriteInConsole(object obj, int uniqueID = -1, [CallerFilePath] string callerFilePath = "?", [CallerLineNumber] int callerLineNumber = -1, [CallerMemberName] string callerMemberName = "?")
        {
            WriteInConsole(obj.ToString(), uniqueID, callerFilePath, callerLineNumber, callerMemberName);
        }

        public static void WriteInConsole(string message, int uniqueID = -1, [CallerFilePath] string callerFilePath = "?", [CallerLineNumber] int callerLineNumber = -1, [CallerMemberName] string callerMemberName = "?")
        {
            bool doFilter = CheckIfFilter(out List<string> consoleFilterKeys);
            var now = DateTime.Now;

            string lasted = "";

            // if execution time wanted
            if (uniqueID != -1)
            {
                var monitoredEvent = monitoredEvents.Find(x => x.uniqueId == uniqueID);

                if (monitoredEvent != null) // the id exist and ths stopwatch have been started
                {
                    lasted = $"------- Lasted: {monitoredEvent.GetExecutionTime()} --------";
                    monitoredEvents.Remove(monitoredEvent);
                }
                else // the id do not exist and ths stopwatch have to be started
                {
                    monitoredEvents.Add(new MonitoredEvent(uniqueID));
                }
            }

            //var elapsed = (now - lastDebufConsoleEntry).TotalMilliseconds;
            //lastDebufConsoleEntry = now;
            //if (elapsed > 100)
            //    WriteInConsole($"/!\\ Since previous task, elapsed time: {elapsed} millis /!\\");

            string finalMessage = $"th:{Thread.CurrentThread.ManagedThreadId.ToString("00")} t:{now:HH.mm.ss.ffffff}: {message} from caller : {callerFilePath}:{callerLineNumber} {callerMemberName} {lasted}";

            if (doFilter)
            {
                bool ok = true;
                foreach (var key in consoleFilterKeys)
                {
                    if (key.StartsWith("&& "))
                        ok = ok && finalMessage.Contains(key.Substring(3));
                    else if (key.StartsWith("|| "))
                        ok = ok || finalMessage.Contains(key.Substring(3));
                    else
                        ok = finalMessage.Contains(key);
                }
                if (!ok)
                    return;
            }

            Console.WriteLine(finalMessage);
        }


        static List<string> _consoleFilterKeys;

        static bool _isFilteringConsole;

        /// <summary>
        /// checking for C:\TEMP\TsunamiConsoleFilterKeys.txt file with keys like "mystring1", "&& mystring2" or "|| mystring3"
        /// </summary>
        /// <param name="consoleFilterKeys"></param>
        /// <returns></returns>
        private static bool CheckIfFilter(out List<string> consoleFilterKeys)
        {
            if (_consoleFilterKeys == null)
            {
                string filePath = @"C:\Data\Tsunami\TsunamiConsoleFilterKeys.txt";
                _isFilteringConsole = System.IO.File.Exists(filePath);
                _consoleFilterKeys = new List<string>();
                if (_isFilteringConsole)
                {
                    try
                    {
                        // Read all lines from the file
                        string[] keys = System.IO.File.ReadAllLines(filePath);

                        // Process each key
                        foreach (string key in keys)
                        {
                            _consoleFilterKeys.Add(key.Trim());
                        }
                    }
                    catch (Exception ex)
                    {
                        TSU.Debug.WriteInConsole($"An error occurred: {ex.Message}");
                    }
                }
            }
            consoleFilterKeys = _consoleFilterKeys;
            return _consoleFilterKeys.Count > 0;
        }

        [DllImport("User32")]
        private static extern int GetGuiResources(IntPtr hProcess, int uiFlags);

        public static void EarlyTest()
        {

        }

        internal static void TestAfterStart()
        {

        }


        private static void testAccessPointAutomaticConnection()
        {
            string serialNumber = "392052";
            Network network = FindAccessPoint(serialNumber, out bool alreadyConnected);
            if (network != null && !alreadyConnected)
            {
                NetworkConnection nc = new NetworkConnection(network.Name, null);
            }
        }

        private static Network FindAccessPoint(string serialNumber, out bool alreadyConnected)
        {
            Network network = GetNetworkContaining(substring: serialNumber);
            if (network != null)
                alreadyConnected = IsNetworkConnect(network);
            else
                alreadyConnected = false;

            return network;
        }

        private static bool IsNetworkConnect(Network network)
        {
            string sConnected = ((network.IsConnected == true) ? " (connected)" : " (disconnected)");
            TSU.Debug.WriteInConsole("Network : " + network.Name + " - Category : " + network.Category.ToString() + sConnected);
            return network.IsConnected == true;
        }

        private static Network GetNetworkContaining(string substring)
        {
            var networks = NetworkListManager.GetNetworks(NetworkConnectivityLevels.All);
            foreach (var network in networks)
            {
                if (network.Name.ToUpper().Contains(substring.ToUpper()))
                {
                    return network;
                }
            }
            return null;
        }


        public class NetworkConnection : IDisposable
        {
            readonly string _networkName;

            public NetworkConnection(string networkName, NetworkCredential credentials)
            {
                _networkName = networkName;


                var netResource = new NetResource
                {
                    Scope = ResourceScope.GlobalNetwork,
                    ResourceType = ResourceType.Disk,
                    DisplayType = ResourceDisplaytype.Share,
                    RemoteName = networkName
                };

                string username;
                string password;
                if (credentials != null)
                {
                    username = string.IsNullOrEmpty(credentials.Domain)
                    ? credentials.UserName
                    : string.Format(@"{0}\{1}", credentials.Domain, credentials.UserName);
                    password = credentials.Password;
                }
                else
                {
                    username = null;
                    password = null;
                }


                var result = WNetAddConnection2(
                    netResource,
                    password,
                    username,
                    0);

                if (result != 0)
                {
                    throw new Win32Exception(result, "Error connecting to remote share");
                }
            }

            ~NetworkConnection()
            {
                Dispose(false);
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            protected virtual void Dispose(bool disposing)
            {
                WNetCancelConnection2(_networkName, 0, true);
            }

            [DllImport("mpr.dll")]
            private static extern int WNetAddConnection2(NetResource netResource,
                string password, string username, int flags);

            [DllImport("mpr.dll")]
            private static extern int WNetCancelConnection2(string name, int flags,
                bool force);

            [StructLayout(LayoutKind.Sequential)]
            public class NetResource
            {
                public ResourceScope Scope;
                public ResourceType ResourceType;
                public ResourceDisplaytype DisplayType;
                public int Usage;
                public string LocalName;
                public string RemoteName;
                public string Comment;
                public string Provider;
            }

            public enum ResourceScope : int
            {
                Connected = 1,
                GlobalNetwork,
                Remembered,
                Recent,
                Context
            };

            public enum ResourceType : int
            {
                Any = 0,
                Disk = 1,
                Print = 2,
                Reserved = 8,
            }

            public enum ResourceDisplaytype : int
            {
                Generic = 0x0,
                Domain = 0x01,
                Server = 0x02,
                Share = 0x03,
                File = 0x04,
                Group = 0x05,
                Network = 0x06,
                Root = 0x07,
                Shareadmin = 0x08,
                Directory = 0x09,
                Tree = 0x0a,
                Ndscontainer = 0x0b
            }
        }









        public static int UserObjects()
        {
            // by default the number max on windows is 10.000
            using (var process = Process.GetCurrentProcess())
            {
                var userHandles = GetGuiResources(process.Handle, 1);
                return Convert.ToInt32(userHandles);
            }
        }

        /// <summary>
        /// by default the number max on windows is 10.000
        /// </summary>
        /// <returns></returns>
        private static int GetUserObjectsCount()
        {
            // 
            using (var process = Process.GetCurrentProcess())
            {
                var userHandles = GetGuiResources(process.Handle, 1);
                return Convert.ToInt32(userHandles);
            }
        }

        private static int GetGdiObjectsCount()
        {
            // 
            using (var process = Process.GetCurrentProcess())
            {
                var gdiHandles = GetGuiResources(process.Handle, 0);
                return Convert.ToInt32(gdiHandles);
            }
        }


        /// <summary>
        /// the mawimum for 32bit app is 2Go.
        /// </summary>
        /// <returns></returns>
        public static long GetMemoryUsage()
        {
            Process currentProcess = Process.GetCurrentProcess();

            // 2. Obtain the used memory by the process
            long usedMemory = currentProcess.PrivateMemorySize64;

            // 3. Display value in the terminal output
            return usedMemory;
        }

        public static void StartTimerToLogRessourcesState()
        {
            System.Timers.Timer TimerToLogTheNumberOfGdIHandles = new System.Timers.Timer()
            {
                Interval = 1000 * 60,
                Enabled = false,
                AutoReset = false
            };

            void OnElapsed(object sender, System.Timers.ElapsedEventArgs e)
            {
                int nUser = GetUserObjectsCount();
                int nGdi = GetGdiObjectsCount();
                long mem = GetMemoryUsage();
                int mc = MessageCount;
                string m = $"There is now '{nUser}/10000' user objects, '{nGdi}/10000' GDI objects";
                string m2 = $" and '{mc}' messages! with memory usage of '{mem:n0}'";
                int n = nUser > nGdi ? nUser : nGdi;
                if (n <= 5000)
                {
                    Logs.Log.AddEntryAsFYI(null, m + m2);
                    TimerToLogTheNumberOfGdIHandles.Start();
                }
                else if (n <= 9500)
                {
                    Logs.Log.AddEntryAsPayAttentionOf(null, m + m2);
                    TimerToLogTheNumberOfGdIHandles.Start();
                }
                else
                {
                    string titleAndMessage = $"{R.T_ATTENTION} it is time to restart; \r\n" +
                                             $"{m}\r\n" +
                                             $" You are getting closer to some limits. Tsunami will probably crash soon.\r\n" +
                                             $"It is adviced to restart.\r\n" +
                                             $"You can use the restart button from the main tsunami menu.";
                    MessageInput mi = new MessageInput(MessageType.Critical, titleAndMessage)
                    {
                        ButtonTexts = new List<string> { R.T_CONTINUE, R.T_RESTART }
                    };
                    if (mi.Show().TextOfButtonClicked == R.T_RESTART)
                    {
                        TimerToLogTheNumberOfGdIHandles.Dispose();
                        Tsunami2.Preferences.Tsunami.Restart();
                    }
                    else
                    {
                        TimerToLogTheNumberOfGdIHandles.Start();
                    }
                }
            }

            TimerToLogTheNumberOfGdIHandles.Elapsed += OnElapsed;
            TimerToLogTheNumberOfGdIHandles.Start();
        }

        #region ATsimulation
        public static int MessageCount = 0;
        public static int ButtonCreated = 0;
        public static int ButtonDisposed = 0;

        public static bool SimulateAt40x = false;
        public static int PolarMeasurementCount = 0;
        public static bool Debugging => Debugger.IsAttached;
        #endregion

        /// <summary>
        /// Runs the action given with a Stopwatch, and writes a message if it lasted motre than ticksTreshold ticks
        /// Warning : the exceptions are not properly catched, so you should use it only in dev
        /// </summary>
        /// <param name="label"></param>
        /// <param name="a"></param>
        /// <param name="ticksTreshold"></param>
        public static void RunWithPerformanceCheck(string label, Action a, double tresholdInMs = 1d)
        {
            Stopwatch sw = Stopwatch.StartNew();
            a();
            sw.Stop();
            double durationInMs = (double)sw.ElapsedTicks / Stopwatch.Frequency * 1000d;
            if (durationInMs > tresholdInMs)
            {
                WriteInConsole($"{label} lasted {durationInMs} ms.");
            }
        }

        internal static void ShowPointsInConsole()
        {
            Console.WriteLine("Points:");
            foreach (var item in Tsunami2.Properties.Points)
            {
                Console.WriteLine($"{item.Guid}     {item._Name}    {item._Origin}");
            }
            Console.WriteLine("Temp:");
            foreach (var item in Tsunami2.Properties.TempPoints)
            {
                Console.WriteLine($"{item.Guid}     {item._Name}    {item._Origin}");
            }
            Console.WriteLine("Hidden:");
            foreach (var item in Tsunami2.Properties.HiddenPoints)
            {
                Console.WriteLine($"{item.Guid}     {item._Name}     {item._Origin}");
            }
        }
    }
}
