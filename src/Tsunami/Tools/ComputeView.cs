﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;using R = TSU.Properties.Resources;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

using TSU.Views;
using TSU;


namespace TSU.Tools
{
    public class ComputeView : ManagerView
    {
        System.Windows.Forms.RichTextBox richTextBox;
        System.Windows.Forms.FontDialog fontDialog;
        System.Windows.Forms.ColorDialog colorDialog;
        System.Windows.Forms.SaveFileDialog saveDialog;
        string path;
        public ComputeView()
        {
            InitializeComponent();

        }
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ComputeView
            // 
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Name = "ComputeView";
            this.path = "";
            richTextBox = new System.Windows.Forms.RichTextBox();
            fontDialog = new FontDialog();
            colorDialog = new ColorDialog();
            saveDialog = new SaveFileDialog();
            richTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Controls.Add(richTextBox);
            richTextBox.MouseUp += new MouseEventHandler(richTextBox1_MouseUp);
            this.ResumeLayout(false);

        }
        private void richTextBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {   //click event
                //Messageox.Show("you got it!");
                ContextMenu contextMenu = new System.Windows.Forms.ContextMenu();
                MenuItem menuItem = new MenuItem("Cut");
                menuItem.Click += new EventHandler(CutAction);
                contextMenu.MenuItems.Add(menuItem);
                menuItem = new MenuItem("Copy");
                menuItem.Click += new EventHandler(CopyAction);
                contextMenu.MenuItems.Add(menuItem);
                menuItem = new MenuItem("Paste");
                menuItem.Click += new EventHandler(PasteAction);
                contextMenu.MenuItems.Add(menuItem);
                menuItem = new MenuItem("Font");
                menuItem.Click += new EventHandler(FontAction);
                contextMenu.MenuItems.Add(menuItem);
                menuItem = new MenuItem("Color");
                menuItem.Click += new EventHandler(ColorAction);
                contextMenu.MenuItems.Add(menuItem);
                menuItem = new MenuItem("Save");
                menuItem.Click += new EventHandler(SaveAction);
                contextMenu.MenuItems.Add(menuItem);
                menuItem = new MenuItem("SaveAs");
                menuItem.Click += new EventHandler(SaveAsAction);
                contextMenu.MenuItems.Add(menuItem);

                richTextBox.ContextMenu = contextMenu;
            }
        }
        public void RichTextBoxFill(string text)
        {
            richTextBox.Text = text;

        }
        void CutAction(object sender, EventArgs e)
        {
            richTextBox.Cut();
        }
        // richtext
        //void CopyAction(object sender, EventArgs e)
        //{
        //    Clipboard.SetData(DataFormats.Rtf, rtb.SelectedRtf);
        //    Clipboard.Clear();
        //}

        //void PasteAction(object sender, EventArgs e)
        //{
        //    if (Clipboard.ContainsText(TextDataFormat.Rtf))
        //    {
        //        rtb.SelectedRtf
        //            = Clipboard.GetData(DataFormats.Rtf).ToString();
        //    }
        //}
        // simpletext
        void CopyAction(object sender, EventArgs e)
        {
            Clipboard.SetText(richTextBox.SelectedText);
        }
        void PasteAction(object sender, EventArgs e)
        {
            if (Clipboard.ContainsText())
            {
                richTextBox.Text
                    += Clipboard.GetText(TextDataFormat.Text).ToString();
            }
        }
        void FontAction(object sender, EventArgs e)
        {
            TSU.Common.TsunamiView.ViewsShownAsModal.Add(fontDialog);
            var res = fontDialog.ShowDialog();
            TSU.Common.TsunamiView.ViewsShownAsModal.Remove(fontDialog);

            if (res == DialogResult.OK)
            {
                richTextBox.SelectionFont = fontDialog.Font;
            }
        }
        void ColorAction(object sender, EventArgs e)
        {
            TSU.Common.TsunamiView.ViewsShownAsModal.Add(colorDialog);
            var res = colorDialog.ShowDialog();
            TSU.Common.TsunamiView.ViewsShownAsModal.Remove(colorDialog);

            if (res == DialogResult.OK)
                richTextBox.SelectionColor = colorDialog.Color;
        }
        void SaveAction(object sender, EventArgs e)
        {
            if (path != "")
            {
                richTextBox.SaveFile(path, RichTextBoxStreamType.RichText);
            }
            else
                SaveAsAction(sender, e);
        }
        void SaveAsAction(object sender, EventArgs e)
        {
            saveDialog.Filter = "Rich Text File (.rtf)|*.rtf|Text File (.txt)|*.txt";

            TSU.Common.TsunamiView.ViewsShownAsModal.Add(saveDialog);
            var res = saveDialog.ShowDialog();
            TSU.Common.TsunamiView.ViewsShownAsModal.Remove(saveDialog);

            if (res == DialogResult.OK)
            {
                switch (saveDialog.FilterIndex)
                {
                    case 1:
                        richTextBox.SaveFile(saveDialog.FileName, RichTextBoxStreamType.RichText);
                        break;
                    case 2:
                        richTextBox.SaveFile(saveDialog.FileName, RichTextBoxStreamType.PlainText);
                        break;
                    default:
                        richTextBox.SaveFile(saveDialog.FileName, RichTextBoxStreamType.PlainText);
                        break;
                }
                
            }
        }
    }
}
