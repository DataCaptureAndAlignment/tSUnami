﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using TSU.Common.Guided.Alignment;

namespace TSU.Tools
{
    internal static class Access
    {
        private class Admin
        {
            public bool IsAdmin { get; private set; }

            public Admin()
            {
                // Start the thread
                string allAdmin = ListAdministrators();
                string accountName = WhoAmI();
                IsAdmin = allAdmin.ToUpper().Contains(accountName.ToUpper());
            }

            private string ListAdministrators()
            {
                Process p = new Process()
                {
                    StartInfo = new ProcessStartInfo()
                    {
                        FileName = "CMD.exe",
                        Arguments = $"/C net localgroup Administrators",
                        UseShellExecute = false,
                        Verb = "runas",
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    }
                };

                p.Start();
                p.WaitForExit();
                return p.StandardOutput.ReadToEnd();
            }

            private string WhoAmI()
            {
                Process p = new Process()
                {
                    StartInfo = new ProcessStartInfo()
                    {
                        FileName = "CMD.exe",
                        Arguments = $"/C whoami",
                        UseShellExecute = false,
                        Verb = "runas",
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    }
                };

                p.Start();
                p.WaitForExit();
                return p.StandardOutput.ReadToEnd();
            }
        }

        private static Admin admin = null;

        public static bool IsAdministrator(bool Async = false)
        {
            // Do the actual check only the first time
            if (admin == null)
            {
                if (!Async)
                {
                    admin = new Admin();
                    return admin.IsAdmin;
                }
                else
                {
                    Thread thread = new Thread(() =>
                    {
                        admin = new Admin();
                    });
                    thread.Start();
                    return false;
                }
            }
            return admin.IsAdmin;
        }

        [DllImport("shell32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool IsUserAnAdmin();

        public static bool IsCurrentProcessAdmin()
        {
            return IsUserAnAdmin();
        }
    }
}
