using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;

using MM = TSU;
using TSU.Views;
using M = TSU;
using TSU.Logs;
using TSU.Common.Compute.Compensations;
using TSU.Common;

namespace TSU.Tools.Gadgets
{
    public partial class View : ModuleView
    {
        internal new Module Module
        {
            get
            {
                return this._Module as Module;
            }
        }

        public Buttons buttons;

        private static readonly object padlock = new object();
        private static View instance;
        public static View Instance(M.IModule module)
        {
            lock (padlock) // to be threat safe, no way to create 2 at the same time
            {
                if (instance == null)
                    instance = new View(module);
                return instance;
            }
        }

        internal View(M.IModule module): base(module)
        {
            this.Image = R.Gadget;
            buttons = new Buttons(this);
            InitializeMenu();

            this.AdaptTopPanelToButtonHeight();
            this._PanelBottom.Padding = new Padding(10);
            this.UpdateView();
        }
        private void ApplyThemeColors()
        {
            this.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
        }

        internal string ChooseFolder()
        {
            OpenFileDialog o = new OpenFileDialog();
            o.Title = "Choose the compute.zip file containing the lgc2 input for the throw points taht you want to compile";
            o.DefaultExt = ".zip";

            TSU.Common.TsunamiView.ViewsShownAsModal.Add(o);
            o.ShowDialog();
            TSU.Common.TsunamiView.ViewsShownAsModal.Remove(o);

            return o.FileName;
        }

        private void InitializeMenu()
        {
            //Creation of buttons
            buttons = new Buttons(this);

            //Main Button
            BigButton bigbuttonTop = new BigButton(
            //    string.Format("{0} {1} '{2}",R.T_GUI_PREF, "Preferences are saved in",TSU.Tsunami2.TsunamiPreferences.Values.Paths.GuiPreferences),
            $"{R.T_SMALL_TOOLS};{R.T_HAVE_A_LOOK_IN_THE_BUTTON_DESCRIPTIONS}",
                R.Gadget,
                this.ShowContextMenuGlobal, null, true);

            this._PanelTop.Controls.Clear();
            this._PanelTop.Controls.Add(bigbuttonTop);

            this.SetContextMenuToGlobal();

            Control[] controls = {

                new Label()
                {
                    Text =R.T_TOOLS_WITH_ACTION_WITHIN_TSUNAMI,
                    Font =TSU.Tsunami2.Preferences.Theme.Fonts.Normal,
                    ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.TextOnDarkBackGround, Dock = DockStyle.Top
                },
                Views.TsuView.CreateLayoutForButtons(new List<Control>(){
                    buttons.MeasureResume,
                    buttons.RenamePoint,
                    buttons.AddTeamToEachStation }, DockStyle.Top, null),
                Views.TsuView.CreateLayoutForButtons(new List<Control>(){
                    buttons.log, buttons.logHtml }, DockStyle.Top, null),
                Views.TsuView.CreateLayoutForButtons(new List<Control>(){
                    buttons.exportWithoutOpening, buttons.exportNewDatFile },
                    DockStyle.Top, null),
                new Label()
                {
                    Text = R.T_USEFUL_LINKS,
                    Font =TSU.Tsunami2.Preferences.Theme.Fonts.Normal,
                    ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.TextOnDarkBackGround, Dock = DockStyle.Top
                },
                Views.TsuView.CreateLayoutForButtons(new List<Control>(){
                    buttons.data, buttons.CurrentTsuFolder}, DockStyle.Top, null),
                Views.TsuView.CreateLayoutForButtons(new List<Control>(){
                    buttons.personalPreferences, buttons.commonPreferences}, DockStyle.Top, null),
                new Label()
                {
                    Text =R.T_EXTERNAL_TOOLS,
                    Font =TSU.Tsunami2.Preferences.Theme.Fonts.Normal,
                    ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.TextOnDarkBackGround, Dock = DockStyle.Top
                },
                    Views.TsuView.CreateLayoutForButtons(new List<Control>(){
                    buttons.FindCorrespondence,
                    buttons.RecreateLGC2InputFromComputeZipFile,
                    buttons.Lgc,
                    buttons.Cala2Poin
                    }, DockStyle.Top, null),
                new Label()
                {
                    Text = "JIRA tickets",
                    Font =TSU.Tsunami2.Preferences.Theme.Fonts.Normal,
                    ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.TextOnDarkBackGround, Dock = DockStyle.Top
                },
                    Views.TsuView.CreateLayoutForButtons(new List<Control>(){
                    buttons.jira,
                    buttons.jiraBug,
                    buttons.jiraImprovement }, DockStyle.Top, null)
            };

            this._PanelBottom.Controls.AddRange(controls.Reverse().ToArray());
        }

        private void SetContextMenuToGlobal()
        {
            contextButtons.Clear();
            contextButtons.Add(buttons.close);
        }

        private void ShowContextMenuGlobal()
        {
            this.SetContextMenuToGlobal();
            this.ShowPopUpMenu(contextButtons);
        }

        public override void UpdateView()
        {
            base.UpdateView();

            this.ResumeLayout();
        }


        public class Buttons
        {
            private View view;
            
            public BigButton close;
            public BigButton Lgc;
            public BigButton Cala2Poin;
            public BigButton RenamePoint;
            public BigButton AddTeamToEachStation;
            public BigButton MeasureResume;
            public BigButton FindCorrespondence;
            public BigButton RecreateLGC2InputFromComputeZipFile; 
            public BigButton log;
            public BigButton logHtml;
            public BigButton exportNewDatFile;
            
            public BigButton exportWithoutOpening;
            public BigButton CurrentTsuFolder;
            public BigButton jira;
            public BigButton jiraBug;
            public BigButton personalPreferences;
            public BigButton commonPreferences;
            public BigButton jiraImprovement;
            public BigButton data;

            public Buttons(View view)
            {
                this.view = view;
                
                Lgc = new BigButton(
                  $"{R.T_RUN_LGC_INPUT};{R.T_RUN_A_SPECIFIC_INPUT_FILE_WITH_LGC_AND_CREATE_ALL_AVAILABLE_COORDINATES_BASED_ON_A_SELECTED_ZONE}",
                  R.Lgc, () => Lgc2.RunInputWithZone(),TSU.Tsunami2.Preferences.Theme.Colors.Object, false);

                Cala2Poin = new BigButton(
                  $"{R.T_CALA2POIN};{R.T_CALA2POIN_details}",
                  R.Element_File, () => Tools.Gadgets.Module.MoveCalaToPoin(),TSU.Tsunami2.Preferences.Theme.Colors.Object, false);

                CurrentTsuFolder = new BigButton(
                 $"'.TSU' {R.T_FOLDER};{R.T_OPEN} {R.T_CURRENT} '.TSU' {R.T_FOLDER}",
                 R.Open, () => Tools.Gadgets.Module.OpenCurrentTsuFolder(), TSU.Tsunami2.Preferences.Theme.Colors.Object, false);

                RenamePoint = new BigButton(
                   $"{R.T_RENAME_POINT};{R.T_CHANGE_THE_NAME_OF_A_POINT_EVERYWHERE}",
                   R.Element_Point, () => Tools.Research.RenamePointEverywhere(),TSU.Tsunami2.Preferences.Theme.Colors.Object, false);

                AddTeamToEachStation = new BigButton(
                   $"{ R.T_ADD_TEAM };{ R.T_CHANGE_THE_TEAM_INITIALS_FOR_EVERY_STATIONS}",
                   R.Team, () => Tools.Research.ChangeTeamInEveryStations(),TSU.Tsunami2.Preferences.Theme.Colors.Object, false);

                MeasureResume = new BigButton(
                   $"{R.T_MEASURE_RESUME};{R.T_WILL_LIST_OCCURENCES_OF_MEASUREMENT}",
                   R.Measurement, () => view.Module.MeasureResume(),TSU.Tsunami2.Preferences.Theme.Colors.Object, false);

                FindCorrespondence = new BigButton(
                   $"{R.T_POINTS_CORRESPONDENCE};{R.T_FIND_CORRESPONDENCE_FROM_2_LIST_OF_POINTS_BASED_ON_THEIR_COORDINATES}",
                   R.Element_Point, () => Common.Analysis.Element.FindCorrespondence(this.view),TSU.Tsunami2.Preferences.Theme.Colors.Object, false);

                RecreateLGC2InputFromComputeZipFile = new BigButton(
                   $"{R.SAVE_THE_DAY};{R.SAVE_THE_DAY_details}",
                   R.Element_File, () => view.Module.RebuildLGC2InputFromPointLanc�Files(),TSU.Tsunami2.Preferences.Theme.Colors.Object, false);


                data = new BigButton(
                    string.Format($"{R.T_DATA_FOLDER};{R.T_OPEN_THE_TSUNAMI_DATA_FOLDER}"),
                   R.Element_File, () => { System.Diagnostics.Process.Start(M.Tsunami2.Preferences.Values.Paths.Data); });

                personalPreferences = new BigButton(
                    string.Format($"{R.T_PERSONAL_PREFERENCES};{R.T_OPEN_THE_FOLDER_WHERE_YOU_PERSONAL_PREFERENCES_ARE_STORED}"),
                   R.I_Settings, () => { System.Diagnostics.Process.Start(M.Tsunami2.Preferences.Values.Paths.PersonnalPreferencesPath); });

                commonPreferences = new BigButton(
                    string.Format($"{R.T_COMMON_PREFERENCES};{R.T_OPEN_THE_FOLDER_WHERE_THE_PREFERENCES_THAT_COME_WITH_THIS_VERSION_ARE_STORED}"),
                   R.I_Settings, () => { System.Diagnostics.Process.Start(M.Tsunami2.Preferences.Values.Paths.CommonPreferencesPath); });

                log = new BigButton(
                  $"{R.T_LOG};{R.T_SHOW_LOG_ENTRIES}",
                 R.Log, view.Module.ShowLog);

                logHtml = new BigButton(
                  R.T_B_OpenHtmlLog,
                 R.Log, view.Module.ShowLogHTML);

                exportNewDatFile = new BigButton(
                    $"{R.T_EXPORT_NEW_DAT};{R.T_EXPORT_NEW_DAT_FILE_DETAILS}",
                    R.Element_File,
                    view.Module.ExportNewDatFile);

                exportWithoutOpening = new BigButton(
                  $"{R.T_EXPORT_WITHOUT_OPENING};{R.T_EXPORT_WITHOUT_OPENING_DETAILS}",
                 R.Log, view.Module.ExportWithoutOpeningView);

                

                jira = new BigButton(
                   R.b_Jira,
                   R.Jira, view.Module.ShowJira);

                jiraBug = new BigButton(
                   $"{R.T_BUG};{R.T_AUTOMATICALLY_GATHER_ACTUAL}",
                   R.JiraBug, view.Module.JiraReportBug);

                jiraImprovement = new BigButton(
                  $"{R.T_IMPROVE};{R.T_AUTOMATICALLY_GATHER_ACTUAL_INF}",
                   R.JiraImprovement, TSU.IO.Jira.AskImprovement);


                close = new BigButton(
                  R.T_B_Close,
                  R.MessageTsu_Problem, this.view.CloseView,TSU.Tsunami2.Preferences.Theme.Colors.Bad);
            }
        }



        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new System.Net.WebClient())
                using (client.OpenRead("http://clients3.google.com/generate_204"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        private void CloseView()
        {
            Tsunami2.Properties.Remove(this.Module);
        }
    }
}
