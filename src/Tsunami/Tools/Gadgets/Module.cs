﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using MM = TSU;
using E = TSU.Common.Elements;
using Z = TSU.Common.Zones;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TSU.Tools.Gadgets;
using TSU.IO.SUSoft;
using TSU.Views;
using TSU.Views.Message;

namespace TSU.Tools.Gadgets
{
    internal class Module : MM.Module
    {

        public new View View
        {
            get
            {
                return _TsuView as View;
            }
        }


        internal Module(MM.Module parentModule) : base(parentModule)
        { }

        internal void MeasureResume()
        {
            List<E.Point> points = new List<E.Point>();
            foreach (E.Element item in Tsunami2.Properties.Elements)
            {
                points.AddRange(item.GetPoints());
            }

            Research.MeasureResume(Tsunami2.Properties.MeasurementModules, points);

        }

        /// <summary>
        /// try to recover data from the log
        /// </summary>
        internal static void LogToDat(string forcedPath = "")
        {
            bool GetTheFile(out string returnedPath)
            {
                returnedPath = "";
                OpenFileDialog openFileDialog1 = new OpenFileDialog
                {
                    InitialDirectory = @"C:\TEMP\",
                    Title = "Browse .html file",
                    CheckFileExists = true,
                    CheckPathExists = true,
                    DefaultExt = "html",
                    RestoreDirectory = true
                };

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    returnedPath = openFileDialog1.FileName;
                    return true;
                }
                return false;
            }

            void ReadTheFile(string pathToRead, out List<string> allLines)
            {
                allLines = File.ReadAllLines(pathToRead).ToList();
            }



            string path;
            if (forcedPath == "")
                GetTheFile(out path);
            else
                path = forcedPath;

            ReadTheFile(path, out List<string> lines);

            List<string> names = new List<string>();
            List<string> accs = new List<string>();
            List<string> states = new List<string>();
            List<string> targets = new List<string>();
            List<double> AHs = new List<double>();
            List<double> AVs = new List<double>();
            List<double> DDs = new List<double>();
            List<string> times = new List<string>();
            List<string> codes = new List<string>();
            List<double> extensions = new List<double>();

            double getValueWithoutSigma(string text)
            {
                int parenthesisPosition = text.IndexOf('(');
                int lengthToRemove = text.Length - parenthesisPosition;
                text = text.Remove(parenthesisPosition);
                double.TryParse(text, out double d);
                return d;
            }

            string getAcceleratorFromName(string name)
            {
                int parenthesisPosition = name.IndexOf('.');
                return name.Substring(1, parenthesisPosition - 1);
            }

            string getNameFromName(string name)
            {
                int parenthesisPosition = name.IndexOf('.');
                return name.Substring(parenthesisPosition + 1, name.Length - parenthesisPosition - 2);
            }

            string previousLine = "";
            bool readNextLine = false;
            string[] parts;
            foreach (string line in lines)
            {
                string trimmed = line.Trim();
                if (readNextLine)
                {
                    readNextLine = false;
                    parts = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    AHs.Add(getValueWithoutSigma(parts[5]));
                    AVs.Add(getValueWithoutSigma(parts[9]));
                    DDs.Add(getValueWithoutSigma(parts[14]));

                }
                if (trimmed.StartsWith("... Finished to"))
                {
                    readNextLine = true;

                    parts = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    states.Add(parts[4]);
                    accs.Add(getAcceleratorFromName(parts[8]));
                    names.Add(getNameFromName(parts[8]));
                    targets.Add(parts[10].Trim('\''));
                    double.TryParse(parts[15], out double d);
                    extensions.Add(d / 1000);
                    if (parts[17] == "(code:")
                        codes.Add(parts[18].TrimEnd(')'));
                    else
                        codes.Add(parts[15].TrimEnd(')'));


                    parts = previousLine.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    times.Add(parts[1]);
                }
                else
                {
                    readNextLine = false;
                }
                previousLine = line;
            }

            // write the dat parts
            string contentAH = "";
            string contentAV = "";
            string contentDD = "";
            for (int i = 0; i < names.Count; i++)
            {
                contentAH += $" {i + 1,4:000};AH;{targets[i],-9};{1,-11};{accs[i],-10};{names[i],-33};{AHs[i],10};{extensions[i],10}; 0.000000;;;;;;;;;C;{codes[i]};{times[i]};{states[i]};\r\n";
                contentAV += $" {i + names.Count + 1,4:000};AV;{targets[i],-9};{1,-11};{accs[i],-10};{names[i],-33};{AVs[i],10};{extensions[i],10}; 0.000000;;;;;;;;;C;{codes[i]};{times[i]};{states[i]};\r\n";
                contentDD += $" {i + names.Count * 2 + 1,4:000};DD;{targets[i],-9};{1,-11};{accs[i],-10};{names[i],-33};{DDs[i],10};{extensions[i],10}; 0.000000;;;;;;;;;C;{codes[i]};{times[i]};{states[i]};\r\n";
            }

            // write the dat 
            string content = contentAH + contentAV + contentDD;



        }

        /// <summary>
        /// move point with obes from cala to point
        /// </summary>
        internal static void MoveCalaToPoin()
        {
            bool GetTheFile(out string path)
            {
                path = "";
                OpenFileDialog openFileDialog1 = new OpenFileDialog
                {
                    InitialDirectory = @"C:\TEMP\",
                    Title = "Browse .txt file",
                    CheckFileExists = true,
                    CheckPathExists = true,
                    DefaultExt = "txt",
                    RestoreDirectory = true
                };

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    path = openFileDialog1.FileName;
                    return true;
                }
                return false;
            }

            void ReadTheFile(string pathToRead, out List<string> allLines)
            {
                allLines = File.ReadAllLines(pathToRead).ToList();
            }

            void StorePointNamesFromStationsAndObservations(List<string> allLines, out List<string> Ss, out List<string> Hs, out List<string> Vs, out List<string> Ds)
            {
                Ss = new List<string>();
                Hs = new List<string>();
                Vs = new List<string>();
                Ds = new List<string>();


                List<string> others = new List<string>();

                List<string> current = others;

                foreach (var line in allLines)
                {
                    if (line.ToUpper().Contains("*ANGL"))
                    {
                        current = Hs;
                        continue;
                    }
                    else if (line.ToUpper().Contains("*ZEND"))
                    {
                        current = Vs;
                        continue;
                    }
                    else if (line.ToUpper().Contains("*DIST"))
                    {
                        current = Ds;
                        continue;
                    }
                    else if (line.ToUpper().Contains("*TSTN"))
                    {
                        string[] partsStation = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        string pointNameStation = partsStation.Length > 0 ? partsStation[1] : "";
                        Ss.Add(pointNameStation);
                        continue;
                    }
                    string[] parts = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    string pointName = parts.Length > 0 ? parts[0] : "";
                    current.Add(pointName);
                }
            }

            void ReOrderPoints(List<string> allLines, List<string> stationsNames, List<string> HorizontalAngleNames,
                List<string> VerticalAngleNames, List<string> DistanceNames, out List<string> finalsLines)
            {
                finalsLines = new List<string>();

                List<string> poins = new List<string>();

                string currentStar = "Unknown";

                bool firstStarPoinOccurance = true;
                foreach (var line in allLines)
                {
                    // detect part of the file
                    if (line.ToUpper().Contains("*CALA")) currentStar = "*CALA";
                    else if (line.ToUpper().Contains("*POIN")) currentStar = "*POIN";
                    else if (line.ToUpper().Contains("*TSTN")) currentStar = "Unknown";

                    // add or not the line
                    // add all line not in *CALA or *POIN
                    if (currentStar == "Unknown")
                    {
                        finalsLines.Add(line);
                    }
                    // add cala only if H+V+D
                    else
                    {
                        string[] parts = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        string pointName = parts.Length > 0 ? parts[0] : " ";
                        int numberOfHorizontalAngle = HorizontalAngleNames.FindAll(x => x == pointName).Count;
                        int numberOfVerticalAngle = VerticalAngleNames.FindAll(x => x == pointName).Count;
                        int numberOfDistance = DistanceNames.FindAll(x => x == pointName).Count;
                        int numberObservations = numberOfHorizontalAngle + numberOfVerticalAngle + numberOfDistance;
                        bool isAStation = stationsNames.Contains(pointName);

                        if (currentStar == "*CALA")
                        {
                            if (pointName[0] == '%' || pointName[0] == ' ')
                                finalsLines.Add(line);
                            // 1 type of obs => go to *POIN
                            else if (numberOfHorizontalAngle > 0 && numberOfVerticalAngle > 0 && numberOfDistance > 0)
                            {
                                poins.Add(line);
                                finalsLines.Add("");
                            }
                            else if (numberObservations > 2)
                            {
                                poins.Add(line + "     % TO CHECK MANUALLY!!");
                                finalsLines.Add("");
                            }
                            else
                            {
                                if (isAStation)
                                    finalsLines.Add(line + "     % WAS A STATION");
                                else if (numberObservations == 0)
                                    finalsLines.Add(line + "     % NOT USED");
                                else
                                    finalsLines.Add(line);
                            }

                        }
                        else if (currentStar == "*POIN")
                        {
                            // write the poin from cala
                            if (firstStarPoinOccurance)
                            {
                                finalsLines.Add(line);
                                foreach (var poin in poins)
                                {
                                    finalsLines.Add(poin + "    % moved from *CALA");
                                }
                                firstStarPoinOccurance = false;
                                continue;
                            }

                            // write or comment the poin
                            if (numberOfHorizontalAngle > 0 && numberOfVerticalAngle > 0 && numberOfDistance > 0)
                                finalsLines.Add(line);
                            else if (numberObservations > 2)
                                finalsLines.Add(line + "     % TO CHECK MANUALLY!!");
                            else if (isAStation)
                                finalsLines.Add(line);
                            else
                                finalsLines.Add("% " + line + " %     SEEMS THERE IS NOT ENOUGH OBS");

                        }
                    }
                }
            }

            void WriteNewFile(string oldPath, List<string> finalsLines, out string newPath)
            {
                newPath = oldPath + ".new.inp";
                File.WriteAllLines(newPath, finalsLines.ToArray());
            }

            void TreatTheLines(List<string> allLines, out List<string> finalsLines)
            {
                StorePointNamesFromStationsAndObservations(allLines, out List<string> stationsNames,
                    out List<string> HorizontalAngleNames, out List<string> VerticalAngleNames, out List<string> DistanceNames);
                ReOrderPoints(allLines, stationsNames, HorizontalAngleNames, VerticalAngleNames, DistanceNames, out finalsLines);
            }

            if (GetTheFile(out string filePath))
            {
                ReadTheFile(filePath, out List<string> allLines);
                TreatTheLines(allLines, out List<string> finalsLines);
                WriteNewFile(filePath, finalsLines, out string newPath);
                Shell.Run(newPath);
            }
        }

        /// <summary>
        /// when tsuanmi bugs and don save the work, you have all the obsrervation in seperated files in the computes/LHC/pint lancé folder, thi is painfullt o rebuild manually, so this fnction will create a songle file with *POIN and *OBS
        /// </summary>
        internal void RebuildLGC2InputFromPointLancéFiles()
        {

            string details = "I could automaticaly retreive the variable points and their observations (c'est deja pas mal). The resection can be used as starting inp file and can be found the 'compute/(P)LGC/station/validated by user. Unfortunatly the closure values can only be found in the Log.HTML.";
            // choose the compute.zip file
            string zipPath = View.ChooseFolder();
            if (zipPath == "") return;
            if (!File.Exists(zipPath)) return;
            FileInfo fi = new FileInfo(zipPath);

            // prepare needed variable
            string starPOIN = "";
            string starANGL = "";
            string starZEND = "";
            string starDIST = "";
            bool foundStarPOIN = false;
            bool foundStarANGL = false;
            bool foundStarZEND = false;
            bool foundStarDIST = false;
            bool foundPname = false;
            bool foundPid = false;

            Action allFalse = () =>
            {
                foundStarPOIN = false;
                foundStarANGL = false;
                foundStarZEND = false;
                foundStarDIST = false;
                foundPname = false;
                foundPid = false;
            };

            // open zip
            Ionic.Zip.ZipFile zip;
            zip = Ionic.Zip.ZipFile.Read(fi.FullName);

            foreach (Ionic.Zip.ZipEntry item in zip.Entries)
            {
                if (item.FileName.Contains("(P)LGC/PointLancer/"))
                {
                    if (item.FileName.EndsWith(".inp"))
                    {
                        // read the file
                        string tempFolder = @"c:\temp\verytemp";
                        string stationName = "unknown";
                        item.Extract(tempFolder, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
                        foreach (string line in File.ReadLines(tempFolder + @"\" + item.FileName))
                        {
                            if (line.Contains("*POIN"))
                            {
                                allFalse();
                                foundStarPOIN = true;
                                continue;
                            }
                            if (line.Contains("*ANGL"))
                            {
                                allFalse();
                                foundStarANGL = true;
                                continue;
                            }
                            if (line.Contains("*ZEND"))
                            {
                                allFalse();
                                foundStarZEND = true;
                                continue;
                            }
                            if (line.Contains("*DIST"))
                            {
                                allFalse();
                                foundStarDIST = true;
                                continue;
                            }
                            if (line.Contains("%Name") & (foundStarPOIN || foundStarANGL || foundStarZEND || foundStarDIST))
                            {
                                foundPname = true;
                                continue;
                            }
                            if (line.Contains("%[id] ") & foundPname & (foundStarPOIN || foundStarANGL || foundStarZEND || foundStarDIST))
                            {
                                foundPid = true;
                                continue;
                            }
                            if (foundPid & foundPname & (foundStarPOIN || foundStarANGL || foundStarZEND || foundStarDIST))
                            {
                                // save  the line
                                string lineToExport = $"{line}        % from station: '{stationName}' in file: '{item.FileName}'\r\n";

                                if (foundStarPOIN) starPOIN += lineToExport;
                                if (foundStarANGL) starANGL += lineToExport;
                                if (foundStarZEND) starZEND += lineToExport;
                                if (foundStarDIST) starDIST += lineToExport;

                                allFalse();
                                continue;
                            }
                            if (line.Contains("*TSTN"))
                            {
                                stationName = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[1];
                            }
                        }
                        File.Delete(tempFolder + "\\" + item.FileName);
                    }
                }
            }

            // write  result
            string exportPath = $@"{fi.Directory}\result.txt";
            File.WriteAllText(exportPath, $"{details}\r\n*POIN\r\n{starPOIN}*ANGL\r\n{starANGL}*ZEND\r\n{starZEND}*DIST\r\n{starDIST}");
            System.Diagnostics.Process.Start(exportPath);
        }


        internal void ShowJira()
        {
            IO.Jira.OpenWebPage();
        }

        internal void ShowLog()
        {
            Logs.LogView lv = new Logs.LogView();
            Preferences.GUI.Theme.ThemeColors colors = Tsunami2.Preferences.Theme.Colors;
            lv.SetColors(beginningOf: colors.Good,
                         logBack: colors.Background,
                         dateAndTime: colors.Choice,
                         error: colors.Bad,
                         fYI: colors.LightForeground,
                         payAttentionTo: colors.Attention,
                         success: colors.Good,
                         finishOf: colors.Good);

            // show all entries
            lv.WantedModule = null; // to insure we got entries from all modules)
            lv.UpdateView();
            lv.Height = 200;
            lv.Dock = DockStyle.Fill;

            View.ShowMessageWithFillControl(control: lv,
                titleAndMessage: string.Format("{0};{1}", R.T_LOG,
                    R.T_ALL_LOG_ENTRIES_DOUBLE_CLICK_THE_TEXT_TO_REDUCE_THE_ENTRIES_BY_SELECTING_THE_SENDERS),
                buttonTexts: new List<string> { R.T_CLOSE }, waitForExit: false);            
        }

        internal void ShowLogHTML()
        {
            Shell.Run(Logs.Log.GetHtmlLogPath());
        }

        internal void JiraReportBug()
        {
            IO.Jira.ReportBug($"In Tsunami {Tsunami2.Properties.Version}");
        }

        internal void ExportWithoutOpeningView()
        {
            throw new NotImplementedException();
        }

        internal void ExportNewDatFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            // Set properties of the dialog
            openFileDialog.Title = "Select the .dat file with duplicate lines";
            openFileDialog.Filter = "DAT Files (*.dat)|*.dat";
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string inputPath = openFileDialog.FileName;
                TSU.Debug.WriteInConsole($"input: {inputPath}");
                string directoryPath = Path.GetDirectoryName(inputPath);
                string newFileName = $"{Path.GetFileNameWithoutExtension(inputPath)}_NoDuplicate{Path.GetExtension(inputPath)}";
                string outputPath = Path.Combine(directoryPath, newFileName);
                TSU.Debug.WriteInConsole($"output: {outputPath}");
                TSU.IO.SUSoft.Geode.RemoveDuplicateSectionsGeode(inputPath, outputPath);
            }
            else { throw new FileNotFoundException(); }
        }

        internal static void OpenCurrentTsuFolder()
        {
            string fullPath = Tsunami2.Properties.PathOfTheSavedFile;
            if (fullPath == "")
            {
                new MessageInput(MessageType.Warning, "No .TSU opened").Show();
                return;
            }
            string folder = Path.GetDirectoryName(fullPath);

            // Open the folder in File Explorer
            System.Diagnostics.Process.Start("explorer.exe", folder);
        }
    }
}
