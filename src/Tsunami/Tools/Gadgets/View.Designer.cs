﻿
using M = TSU;
using TSU.Views;
using TSU.Common;

namespace TSU.Tools.Gadgets
{
    partial class View : ModuleView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            TSU.Preferences.GUI.Theme.ThemeColors themeColors1 = new TSU.Preferences.GUI.Theme.ThemeColors();
            this.SuspendLayout();
            // 
            // _PanelBottom
            // 
            this._PanelBottom.Location = new System.Drawing.Point(5, 50);
            this._PanelBottom.Size = new System.Drawing.Size(738, 211);
            // 
            // _PanelTop
            // 
            this._PanelTop.Size = new System.Drawing.Size(738, 45);
            // 
            // PopUpMenu
            // 
            this.PopUpMenu.BackColor = System.Drawing.Color.LightBlue;
            this.PopUpMenu.Location = new System.Drawing.Point(234, 234);
            // 
            // View
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            themeColors1.Action = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            themeColors1.Attention = System.Drawing.Color.Orange;
            themeColors1.Background = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            themeColors1.Bad = System.Drawing.Color.Red;
            themeColors1.Choice = System.Drawing.Color.DeepSkyBlue;
            themeColors1.DarkBackground = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            themeColors1.FullColorisation = true;
            themeColors1.Good = System.Drawing.Color.Lime;
            themeColors1.Grayout = System.Drawing.Color.LightGray;
            themeColors1.Highlight = System.Drawing.Color.LightBlue;
            themeColors1.LightBackground = System.Drawing.Color.Snow;
            themeColors1.LightForeground = System.Drawing.Color.Snow;
            themeColors1.NormalFore = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            themeColors1.Object = System.Drawing.Color.LightGray;
            themeColors1.TextHighLightBack = System.Drawing.Color.DeepSkyBlue;
            themeColors1.TextHighLightFore = System.Drawing.Color.Snow;
            themeColors1.TextOnDarkBackGround = System.Drawing.Color.Snow;
            themeColors1.Transparent = System.Drawing.Color.Transparent;
            this.BackColor = themeColors1.Background;
            this.ClientSize = new System.Drawing.Size(748, 266);
            this.Name = "View";
            this.Text = "TsuView2";
            this.Controls.SetChildIndex(this.buttonForFocusWithTabIndex0, 0);
            this.Controls.SetChildIndex(this._PanelTop, 0);
            this.Controls.SetChildIndex(this._PanelBottom, 0);
            this.ResumeLayout(false);

        }

        #endregion
    }
}