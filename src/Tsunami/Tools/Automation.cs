﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TSU.Tools
{
    public class Automation
    {
        [XmlAttribute]
        public bool Activated { get; set; } = false;

        [XmlAttribute]
        public string XmlFolderPath { get; set; } = "";

        [XmlAttribute]
        public string ScriptPath = "";

        public Automation() { }


        internal void Run(Polar.Measure measure)
        {
            try
            {
                var xmlPath = XmlFolderPath;
                if (xmlPath == "")
                {
                    xmlPath = Tsunami2.Preferences.Values.Paths.Temporary;
                };
                xmlPath = System.IO.Path.Combine(xmlPath, Conversions.Date.ToDateUpSideDownPrecise(measure._Date).ToString()+".xml");

                // write the measure on the disk
                IO.Xml.CreateFile(measure, xmlPath);

                // run the path
                Process.Start(new ProcessStartInfo
                {
                    FileName = ScriptPath,
                    Arguments = xmlPath,
                    UseShellExecute = true // Ensures the default application is used
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred running the automation path '{ScriptPath}': {ex.Message}");
            }
        }
    }
}
