﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;
using E = TSU.Common.Elements;
using M = TSU.Common.Measures;
using TSU.Views;
using TSU.Common.Instruments.Device.AT40x;
using TSU.Common;
using TSU.Common.Instruments.Reflector;
using TSU.Views.Message;
using Microsoft.Office.Interop.Excel;

namespace TSU.Tools
{
    public static class Research
    {



        public static string FindNextName(string s, List<TsuObject> l)
        {
            string a;
            string b = s.ToUpper() + ".";
            int indexTotal = 0;
            foreach (TsuObject p in l)
            {
                if (p != null)
                {
                    a = p._Name.ToUpper();
                    if (a.Contains(b))
                    {
                        int pos = a.IndexOf(b);
                        if (pos == 0)
                        {
                            int posFinal = pos + b.Length;
                            string endOfString = a.Substring(posFinal);
                            int index;
                            if (int.TryParse(endOfString, out index))
                            {
                                if (index > indexTotal) indexTotal = index;
                            }
                        }
                    }
                }
            }
            if (indexTotal > 0)
            {
                indexTotal += 1;
                a = s + indexTotal.ToString("000");
            }
            else
                a = s + "001";
            return a;

        }
        public static string FindNextName(string s, List<E.Point> l)
        {
            string a;
            string b = s.ToUpper();
            int indexTotal = 0;
            foreach (TsuObject p in l)
            {
                a = p._Name.ToUpper();
                if (a.Contains(b))
                {
                    int pos = a.IndexOf(b);
                    if (pos == 0)
                    {
                        int posFinal = pos + b.Length;
                        string endOfString = a.Substring(posFinal);
                        int index;
                        if (int.TryParse(endOfString, out index))
                        {
                            if (index > indexTotal) indexTotal = index;
                        }
                    }
                }
            }
            if (indexTotal > 0)
            {
                indexTotal += 1;
                a = s + indexTotal.ToString();
            }
            else
                a = s;
            return a;
        }

        public static List<Reflector> GetListOfReflectorUsedIn(Polar.Station station)
        {
            List<Reflector> l = new List<Reflector>();
            var allMeasureDoneAndToBeDone = new List<Common.Measures.Measure>();
            allMeasureDoneAndToBeDone.AddRange(station.MeasuresTaken);
            if (station.MeasuresToDo != null)
                if (station.MeasuresToDo.Count > 0)
                    allMeasureDoneAndToBeDone.Add(station.MeasuresToDo[0]);
            if (station.NextMeasure != null)
                allMeasureDoneAndToBeDone.Add(station.NextMeasure);

            Reflector defautlReflector = Tsunami2.Preferences.Values.Reflectors.FirstOrDefault(x => x._Name == "CCR1.5_1");
            foreach (Polar.Measure m in allMeasureDoneAndToBeDone)
            {
                if (m != null && m.Distance != null && m.Distance.Reflector != null)
                    if (!l.Contains(m.Distance.Reflector))
                        if (m.Distance.Reflector._Name == R.T_MANAGED_BY_AT40X)
                        {
                            if (!l.Contains(defautlReflector))
                                l.Add(defautlReflector);
                        }
                        else
                        {
                            if (!l.Contains(m.Distance.Reflector))
                                l.Add(m.Distance.Reflector);
                        }
            }

            if (l.Count == 0 || l.Count == 1 && l[0]._Name == R.T_MANAGED_BY_AT40X)
            {
                l.Add(Tsunami2.Preferences.Values.Reflectors.FirstOrDefault(x => x._Name == "CCR1.5_1"));
            }
            return l;
        }

        public static TsuNode FindFirstNodeByTag(TsuObject objectToSeekFor, TreeNodeCollection treeNodeCollectionToSeekFrom)
        {
            TsuNode foundItem;
            foreach (TsuNode item in treeNodeCollectionToSeekFrom)
            {
                if (item.Tag == objectToSeekFor)
                    return item;
                else
                {
                    foundItem = FindFirstNodeByTag(objectToSeekFor, item);
                    if (foundItem != null)
                        return foundItem;
                }
            }
            return null;
        }

        public static TsuNode FindFirstNodeByTag(TsuObject objectToSeekFor, List<TsuNode> priorityList, TreeNodeCollection treeNodeCollectionToSeekFrom)
        {
            TsuNode foundItem;

            foreach (TsuNode item in priorityList) //.Reverse<TsuNode>()
            {
                if (item.Tag == objectToSeekFor)
                    return item;
            }

            foundItem = FindFirstNodeByTag(objectToSeekFor, treeNodeCollectionToSeekFrom);

            return foundItem;
        }

        public static TsuNode FindFirstNodeByTag(TsuObject objectToSeekFor, TreeNode treeNodeToSeekFrom)
        {
            TsuNode foundItem;
            foreach (TsuNode item in treeNodeToSeekFrom.Nodes)
            {
                if (item.Tag == objectToSeekFor)
                    return item;
                else
                {
                    foundItem = FindFirstNodeByTag(objectToSeekFor, item);
                    if (foundItem != null)
                        return foundItem;
                }
            }
            return null;
        }

        internal static List<Polar.Measure> FindGoodMeasuresByPointNames(
            CloneableList<M.Measure> measures,
            CloneableList<E.Element> elements,
            M.States.Types type)
        {
            List<Polar.Measure> founds = new List<Polar.Measure>();
            List<M.Measure> measuresGeneric;
            foreach (E.Point p in elements)
            {
                measuresGeneric = measures.FindAll(x => (x._Point._Name == p._Name) && (x._Status.Type == type));
                foreach (var item in measuresGeneric)
                {
                    Polar.Measure good = item as Polar.Measure;
                    if (!founds.Contains(good))
                        founds.Add(good);

                }
            }
            return founds;
        }

        internal static void ReplaceTextInFileFromFileInput(string fileWithNames, string fileToModify)
        {
            string text = System.IO.File.ReadAllText(fileToModify);

            if (System.IO.File.Exists(fileWithNames))
            {
                var lines = System.IO.File.ReadLines(fileWithNames);
                foreach (var line in lines)
                {
                    string cleaned = line.Trim();
                    if (cleaned[0] == '%') continue;
                    cleaned = System.Text.RegularExpressions.Regex.Replace(cleaned, @"(\s)\s+", "$1"); //remove multi tab or space
                    List<string> lineFields = new List<string>(cleaned.Split(' '));
                    if (lineFields.Count == 2) // only if find a line with 2 strings ('old' and 'new')
                    {
                        text = text.Replace(lineFields[1], lineFields[0]);

                    }
                }
                //Tsunami2.Properties.Save();
            }
            System.IO.File.WriteAllText(fileToModify, text);


            return;
        }

        internal static void RenamePointEverywhere(bool nextRenaming = false)
        {
            try
            {
                var v = Tsunami2.Properties.View;
                bool dontShowAgain0 = false;
                bool dontShowAgainAny = false;

                string token = "";


                string message = "What is the full name (ACC/zone included) of the point that you want to change everywhere in this instance of Tsunami?";
                message += "\r\n" + "(macro option: Enter in the textbox the path of a text file containing the pairs 'oldName newName')";
                if (nextRenaming)
                    message = "DONE! rename another point?" + ";" + message;
                else
                    message = "Point name ?" + ";" + message;

                List<E.Point> pointsToRename = FindPointsEverywhere(
                    null,
                    out List<E.Point> allPoints,
                    out List<TSU.Module> allModules,
                    out List<Station> allStations,
                    out List<M.Measure> allMeasures,
                    out string message2);

                BindingSource bs = new BindingSource(pointsToRename, "_Name");

                var objectReturned = (v.ShowMessageOfPredefinedInput(message, R.T_OK, R.T_CANCEL, bs, allowCustomEntry: false));
                if (objectReturned is E.Point p)
                    token = p._Name;
                else if (objectReturned is string s)
                    token = s;
                else
                    return;

                if (token == R.T_CANCEL) return;

                if (System.IO.File.Exists(token))
                {
                    var lines = System.IO.File.ReadLines(token);
                    foreach (var line in lines)
                    {
                        string cleaned = line.Trim();
                        if (cleaned[0] == '%') continue;
                        cleaned = System.Text.RegularExpressions.Regex.Replace(cleaned, @"(\s)\s+", "$1"); //remove multi tab or space
                        List<string> lineFields = new List<string>(cleaned.Split(' '));
                        if (lineFields.Count == 2) // only if find a line with 2 strings ('old' and 'new')
                        {
                            RenameOnePointEverywhere(lineFields[0], ref dontShowAgain0, ref dontShowAgainAny, lineFields[1]);
                        }
                    }
                }
                else
                {
                    RenameOnePointEverywhere(token, ref dontShowAgain0, ref dontShowAgainAny, "");
                    Tsunami2.Properties.Save("", "renaming");
                    //RenamePointEverywhere(nextRenaming: true);
                }
            }
            catch (Exception ex)
            {

                throw new Exception("Failed to rename points", ex);
            }
        }


        public static List<E.Point> FindPointsEverywhere(string name,
            out List<E.Point> allPoints,
            out List<TSU.Module> allModules,
            out List<Station> allStations,
            out List<M.Measure> allMeasures,
            out string message)
        {
            try
            {
                // init
                List<E.Point> points = new List<E.Point>();
                allPoints = new List<E.Point>();
                allModules = new List<TSU.Module>();
                allStations = new List<Station>();
                allMeasures = new List<M.Measure>();

                // from elements
                foreach (E.Element e in Tsunami2.Properties.Elements)
                {
                    foreach (E.Point p in e.GetPoints())
                    {
                        if (!allPoints.Contains(p))
                            allPoints.Add(p);

                        if (name == null)
                            points.Add(p);
                        else if (p._Name.ToUpper() == name.ToUpper())
                            points.Add(p);
                    }
                }

                // from measurements
                foreach (var finalModule in Tsunami2.Properties.MeasurementModules)
                {
                    if (!allModules.Contains(finalModule))
                        allModules.Add(finalModule);

                    foreach (var sm in finalModule.StationModules)
                    {
                        if (!allStations.Contains(sm._Station))
                            allStations.Add(sm._Station);

                        // station point itself
                        if (sm._Station is Polar.Station st)
                        {
                            if (!allPoints.Contains(st.Parameters2._StationPoint))
                                allPoints.Add(st.Parameters2._StationPoint);

                            if (name == null)
                            {
                                if (st.Parameters2._StationPoint != null)
                                    points.Add(st.Parameters2._StationPoint);
                            }
                            else if (st.Parameters2._StationPoint != null)
                            {
                                if (st.Parameters2._StationPoint._Name.ToUpper() == name.ToUpper())
                                    points.Add(st.Parameters2._StationPoint);
                            }
                        }

                        // measure from the station
                        foreach (M.Measure m in sm._MeasureManager.AllElements)
                        {
                            if (!allMeasures.Contains(m))
                                allMeasures.Add(m);

                            if (name == null)
                                points.Add(m._Point);
                            else if (m._Point._Name.ToUpper() == name.ToUpper())
                            {
                                points.Add(m._Point);
                            }

                            if (m._OriginalPoint != null)
                            {
                                if (name == null)
                                    points.Add(m._OriginalPoint);
                                else if (m._OriginalPoint._Name.ToUpper() == name.ToUpper())
                                {
                                    points.Add(m._OriginalPoint);
                                }
                            }
                        }
                    }
                }

                message = $"Tsunami found {points.Count} places containing {name} to be rename;" +
                    $"Tsunami look in {allModules.Count} modules,\r\n{allStations.Count} stations,\r\n{allMeasures.Count} measures and {allPoints.Count} elements (points)\r\n" +
                    $"And found {points.Count} places to rename. \r\n";

                return points;
            }
            catch (Exception ex)
            {
                throw new Exception("Problem when finding all the points", ex);
            }
        }

        internal static int CountPointExistingSomewhere(string value)
        {
            List<E.Point> pointsFound = Research.FindPointsEverywhere(value,
                            out List<E.Point> allPoints,
                            out List<TSU.Module> allModules,
                            out List<Station> allStations,
                            out List<M.Measure> allMeasures,
                            out string message);
            return pointsFound.Count;
        }

        public static void RenameOnePointEverywhere(string oldname, ref bool dontShowAgain0, ref bool dontShowAgainAny, string newname = "")
        {
            var v = Tsunami2.Properties.View;

            List<E.Point> pointsToRename = FindPointsEverywhere(
                oldname,
                out List<E.Point> allPoints,
                out List<TSU.Module> allModules,
                out List<Station> allStations,
                out List<M.Measure> allMeasures,
                out string message);


            if (newname == "")
                newname = oldname + "new";

            // Show message or not?
            bool show = true;
            {
                if (pointsToRename.Count == 0 && dontShowAgain0) show = false;
                if (dontShowAgainAny) show = false;
            }

            if (show)
            {
                DsaFlag proceedRenaming = new DsaFlag("", DsaOptions.Ask_to);
                string titleAndMessage = message +
                                         $"Do you want to proceed?" +
                                         $"What is the full new name (ACC/zone included) of the point that you want to change everywhere in this instance of Tsunami?";
                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = CreationHelper.GetOKCancelButtons(),
                    Controls = new List<Control> { CreationHelper.GetPreparedTextBox(newname) },
                    DontShowAgain = proceedRenaming
                };
                using (var r = mi.Show())
                {
                    if (r.TextOfButtonClicked == R.T_CANCEL)
                        return;
                    newname = r.ReturnedControls[0].Text;
                }

                if (proceedRenaming.State == DsaOptions.Never)
                {
                    if (pointsToRename.Count == 0)
                        dontShowAgain0 = true;
                    else
                        dontShowAgainAny = true;
                }
            }

            foreach (E.Point p in pointsToRename)
            {
                p._Name = newname.ToUpper();
            }

            new MessageInput(MessageType.GoodNews, $"Point renamed;{oldname} => {newname}").Show();
        }

        internal static void ChangeTeamInEveryStations()
        {
            var v = Tsunami2.Properties.View;

            List<Station> stations = new List<Station>();
            int moduleCount = 0;
            int stationCount = 0;

            foreach (var finalModule in Tsunami2.Properties.MeasurementModules)
            {
                moduleCount++;
                foreach (var stationModule in finalModule.StationModules)
                {
                    stationCount++;
                    stations.Add(stationModule._Station);
                }
            }

            string titleAndMessage1 = $"Team initials?;" +
                                      $"Tsunami look in {moduleCount} modules,\r\n{stationCount} stations\r\n" +
                                      $"And found {stations.Count} places to update.\r\n" +
                                      $"What are the initiales that you want to change everywhere in this instance of Tsunami?";
            List<string> buttonTexts = new List<string> { R.T_OK, R.T_CANCEL };

            MessageTsu.ShowMessageWithTextBox(titleAndMessage1, buttonTexts, out string buttonClicked, out string textInput, "AZPS", true);

            if (buttonClicked == R.T_CANCEL) return;


            string titleAndMessage = $"Tsunami found {stations.Count} places to update;" +
                                     $"Tsunami look in {moduleCount} modules,\r\n{stationCount} stations\r\n" +
                                     $"And found {stations.Count} places to update. \r\n" +
                                     $"Do you want to proceed?";
            MessageInput mi = new MessageInput(MessageType.Warning, titleAndMessage)
            {
                ButtonTexts = new List<string> { R.T_OK, R.T_CANCEL }
            };
            string token2 = mi.Show().TextOfButtonClicked;

            if (token2 == R.T_CANCEL) return;

            foreach (Station s in stations)
            {
                s.ParametersBasic._Team = textInput;
            }
        }

        public class MeasureCount : TsuObject
        {
            public E.Point point;
            public int countGood;
            public int countControl;
            public int countQuestionnable;
            public int countBad;
            public static MeasureCount IsContainIn(string name, List<MeasureCount> measures)
            {
                return measures.Find(x => x.point._Name == name);
            }
        }

        internal static void MeasureResume(List<FinalModule> finalModules, List<E.Point> points)
        {
            var v = Tsunami2.Properties.View;

            List<MeasureCount> measureCounts = new List<MeasureCount>();
            int pointCount = 0;
            int moduleCount = 0;
            int stationCount = 0;
            int measureCount = 0;


            foreach (E.Point p in points)
            {
                pointCount++;
                MeasureCount mc = MeasureCount.IsContainIn(p._Name, measureCounts);

                if (mc == null)
                    measureCounts.Add(new MeasureCount() { point = p });
            }

            foreach (var finalModule in finalModules)
            {
                moduleCount++;
                foreach (var stationModule in finalModule.StationModules)
                {
                    stationCount++;
                    if (stationModule._Station.ParametersBasic._State is Station.State.Bad) continue;
                    foreach (M.Measure measure in stationModule._MeasureManager.AllElements)
                    {
                        measureCount++;
                        if (measure._Status is M.States.Bad) continue;

                        MeasureCount mc = MeasureCount.IsContainIn(measure._Point._Name, measureCounts);
                        if (mc != null)
                        {
                            switch (measure._Status.Type)
                            {
                                case M.States.Types.Failed:
                                case M.States.Types.Bad:
                                case M.States.Types.Cancel:
                                case M.States.Types.Replace:
                                    mc.countBad++;
                                    break;
                                case M.States.Types.Unknown:
                                case M.States.Types.Questionnable:
                                case M.States.Types.Temporary:
                                    break;
                                case M.States.Types.Control:
                                case M.States.Types.ControlFromOtherRound:
                                    mc.countControl++;
                                    break;
                                case M.States.Types.Good:
                                case M.States.Types.GoodFromOtherRound:
                                default:
                                    mc.countGood++;
                                    break;
                            }

                        }
                    }
                }
            }

            TSU.Manager manager = new TSU.Manager(
               Tsunami2.Properties, $"{R.T_MESURE_RESUME};{R.T_STATISTIC_RESUME}");
            manager.AllElements = new List<TsuObject>();
            manager.Initialize();
            manager.AllElements.AddRange(measureCounts);
            manager.View.ListviewColumnHeaderTexts = new List<string>() { "Point name", "Cumul", "Good", "?", "CTRL", "Bad" };
            manager.View.HideEmptyColumns = false;
            manager.View.currentStrategy.Initialize(); // to apply headers


            manager.View.ShowInMessageTsu(R.T_MESURE_RESUME, R.T_THANKS, null);
        }

        internal static string GetNFirstToString(int n, List<E.Point> missingPoints)
        {
            StringBuilder s = new StringBuilder();
            bool first = true;

            for (int i = 0; i < missingPoints.Count; i++)
            {
                if (i >= n)
                {
                    s.Append("...");
                    break;
                }

                if (first)
                    first = false;
                else
                    s.Append(", ");

                s.Append(missingPoints[i]._Name);
            }

            return s.ToString();
        }
    }
}
