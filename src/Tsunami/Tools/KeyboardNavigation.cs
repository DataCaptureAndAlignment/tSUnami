﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSU.Common;
using TSU.Views;

namespace TSU.Tools
{

    /// <summary>
    /// 
    /// ALT+Fx : alterate macro
    /// CTRL+Fx : 
    /// </summary>
    internal static class KeyboardNavigation
    {
        internal static void AnalyseAnAct(TsuView control, KeyEventArgs keyEventArgs)
        {
            KeyboardNavigation.MoveBetweenTsuViewBasedOnKey(control: control, ShiftKeyPressed: keyEventArgs.Shift, keyCode: keyEventArgs.KeyCode);
        }

        internal static bool IsFunctionKey(KeyEventArgs e)
        {
            return 112 <= e.KeyValue && e.KeyValue <= 123;
        }

        public static void MoveBetweenTsuViewBasedOnKey(TsuView control, bool ShiftKeyPressed, Keys keyCode)
        {
            if (ShiftKeyPressed)
            {
                if (keyCode == Keys.Escape) MoveToMenuView();
                if (keyCode == Keys.Up) MoveToParentView(control);
                if (keyCode == Keys.Down) MoveToFirstChildView(control);
                if (keyCode == Keys.Left) MoveToSisterVisibleView(control);
                if (keyCode == Keys.Right) MoveToSisterVisibleView(control);
            }
        }

        public static void MoveToMenuView()
        {
            Tsunami2.Properties.Menu.View.buttonForFocusWithTabIndex0.Focus();
        }

        internal static bool IsAlreadyCatchbyChildren(TsuView control)
        {

            // check if a child form already catched the key
            if (ChildFormAlreadyCatchedTheKey(control))
                return true;
            return false;
        }

        public static bool MoveToSisterVisibleView(Control control)
        {
            Debug.WriteInConsole($"KEY_UP: KeyUp_MoveToSisterView");
            var parent = control.Parent;
            if (parent == null) return false;

            if (parent is TsuView tsuView)
            {
                int index = parent.Controls.IndexOf(control);
                // going from current control to the next
                for (int i = index + 1; i < parent.Controls.Count; i++)
                {
                    if (parent.Controls[i] is TsuView sisterView)
                    {
                        if (sisterView.Visible == true)
                        {
                            sisterView.buttonForFocusWithTabIndex0.Focus();
                            Debug.WriteInConsole($"KEY_UP: Focusing on {sisterView._Name}");
                            return true;
                        }
                    }
                }
                // if not found looking from the beginning to the currrent control
                for (int i = 0; i < index; i++)
                {
                    if (parent.Controls[i] is TsuView sisterView)
                    {
                        sisterView.buttonForFocusWithTabIndex0.Focus();
                        Debug.WriteInConsole($"KEY_UP: Focusing on {sisterView._Name}");
                        return true;
                    }
                }
            }
            return false;
        }

        public static void MoveToParentView(Control control)
        {
            Debug.WriteInConsole($"KEY_UP: KeyUp_MoveToParentView");
            var parent = control.Parent;
            if (parent != null)
            {
                if (parent is TsuView tsuView)
                {
                    tsuView.buttonForFocusWithTabIndex0.Focus();
                    Debug.WriteInConsole($"KEY_UP: Focusing on {tsuView._Name}");
                }
                else
                {
                    MoveToParentView(parent);
                }
                if (parent is TsunamiView)
                    MoveToSisterVisibleView(control);
            }
        }

        private static bool MoveToFirstChildView(Control control)
        {
            Debug.WriteInConsole($"KEY_UP: KeyUp_MoveToFirstChildView");
            foreach (var item in control.Controls)
            {
                if (item is TsuView tsuView)
                {
                    tsuView.buttonForFocusWithTabIndex0.Focus();
                    Debug.WriteInConsole($"KEY_UP: Focusing on {tsuView._Name}");
                    return true;
                }
                if (item is Panel panel)
                {
                    if (MoveToFirstChildView(panel))
                        return true;
                }
                if (item is SplitContainer sc)
                {
                    if (MoveToFirstChildView(sc))
                        return true;
                }
            }
            return false;
        }

        private static bool ChildFormAlreadyCatchedTheKey(Control control)
        {
            foreach (var item in control.Controls)
            {
                if (item is TsuView tsuView)
                {
                    if (tsuView.KeyCatched != Keys.Cancel)
                        return true;
                    if (ChildFormAlreadyCatchedTheKey(tsuView))
                        return true;
                }
                if (item is Panel panel)
                {
                    if (ChildFormAlreadyCatchedTheKey(panel))
                        return true;
                }
                if (item is SplitContainer sc)
                {
                    if (ChildFormAlreadyCatchedTheKey(sc))
                        return true;
                }
            }
            return false;
        }

        internal static void ResetKeyCatchedInChildForms(Control control)
        {
            foreach (var item in control.Controls)
            {
                if (item is TsuView tsuView)
                {
                    tsuView.KeyCatched = Keys.Cancel;
                    ResetKeyCatchedInChildForms(tsuView);
                }
                if (item is Panel panel)
                {
                    ResetKeyCatchedInChildForms(panel);
                }
                if (item is SplitContainer sc)
                {
                    ResetKeyCatchedInChildForms(sc);
                }
            }
        }


        private static bool FocusIsOnEditableControl(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if (control is TextBox) return true;
                if (control is ComboBox) return true;
                if (FocusIsOnEditableControl(control.Controls)) return true;
            }
            return false;
        }
    }


}
