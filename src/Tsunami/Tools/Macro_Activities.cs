﻿using System;
using System.Security.Policy;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using R = TSU.Properties.Resources;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using static TSU.Tools.Macro;
using System.Text.RegularExpressions;

namespace TSU.Tools
{
    public partial class Macro
    {
        [XmlInclude(typeof(KeyStroke))]
        [XmlInclude(typeof(MouseActivity))]
        [XmlInclude(typeof(Trace))]
        public class IInputActivity
        {
            [XmlAttribute("Pos.")]
            public int Position { get; set; }
            [XmlAttribute("Delay")]
            public int DelayInMillis { get; set; } = 0;
            public override string ToString()
            {
                return $"[{Position}/$count$]";
            }
            [XmlAttribute("trace")]
            public string Trace { get; set; }
        }
        [XmlType("Trace________")]
        public class Trace : IInputActivity
        {
            [XmlAttribute("Action")]
            public string message { get; set; } = "";

            public override string ToString()
            {
                return  $"{base.ToString()} Trace: {message}";
            }
        }

        [XmlType("KeyStroke____")]
        public class KeyStroke : IInputActivity
        {
            [XmlAttribute("CTRL")]
            public bool Control;
            [XmlAttribute("SHIFT")]
            public bool Shift;
            [XmlAttribute("ALT")]
            public bool Alt;
            [XmlAttribute]
            public string KeyData;


            [XmlAttribute("Target")]
            public string TargetControl;


            public KeyStroke()
            {

            }

            public override string ToString()
            {
                string mod = "";
                mod += Control ? "CTRL" : "";
                mod += Shift ? "Shift" : "";
                mod += Alt ? "Alt" : "";
                return $"{base.ToString()} Keyboard stroke: {mod}+{KeyData}";
            }
        }

        public class MouseActivity : IInputActivity
        {
            [XmlAttribute("Button")]
            public string Button { get; set; }
            [XmlAttribute("Direction")]
            public string Direction { get; set; }
            [XmlAttribute("Xpos")]
            public int X { get; set; }
            [XmlAttribute("Ypos")]
            public int Y { get; set; }
            [XmlAttribute("Delta")]
            public int Delta { get; set; }
            public override string ToString()
            {
                return $"{base.ToString()} Mouse Click: {Button} {Direction}";
            }
            public static void Record(MouseHook.MouseUpAndDown d, MouseEventArgs e, Control controlUnderCursor)
            {
                string trace = "none";
                if (controlUnderCursor != null)
                {
                    trace =  $"{controlUnderCursor.Name}+{controlUnderCursor.Text}";
                    trace = Regex.Replace(trace, @"\d{1,2}/\d{1,2}/\d{4}\s\d{1,2}:\d{2}:\d{2}", "");
                    // because of Module.CreatedOn.ToString("G", CultureInfo.CreateSpecificCulture("nl-BE")) that shown in buttons
                    // Created on: 21/05/2024 13:59:39
                }

                Debug.WriteInConsole($"you clicked on {trace}");
                TimeSpan t = DateTime.Now - lastStrokeDate;
                lastStrokeDate = DateTime.Now;
                if (e.Clicks == 0)
                    throw new Exception();
                MouseActivity ma = new MouseActivity()
                {
                    Button = e.Button.ToString(),
                    Direction = d.ToString(),
                    X = e.Location.X,
                    Y = e.Location.Y,
                    Delta = e.Delta,
                    DelayInMillis = (int)((t.TotalMilliseconds) + TICK_INTERVAL_MILLIS),
                    Trace = trace
                };
                MacroBeingRecorded.KeyStrokesAndMouseClicks.Add2(ma);
            }
        }
    }
}