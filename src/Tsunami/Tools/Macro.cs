﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using TSU.Common;
using TSU.Views;
using TSU.Views.Message;
using R = TSU.Properties.Resources;

namespace TSU.Tools
{
    public static class MacroExtensions
    {
        // extension that fills the list position insode the macro activity to be able to show up in the xml file
        public static void Add2<T>(this T src, Macro.IInputActivity activity) where T : List<Macro.IInputActivity>
        {
            activity.Position = src.Count + 1;
            src.Add(activity);
        }
    }

    
    /// <summary>
    /// CTRL+FX : Run existing macro X
    /// ALT+FX : Change Macro FX settings
    /// Shift+FX : Record/Stop/Override/ContinueRecording macro FX
    /// </summary>
    public partial class Macro
    {

        public static readonly ManualResetEvent MacroStarted = new ManualResetEvent(false);
        public static readonly ManualResetEvent MacroFinished2SecondsAgo = new ManualResetEvent(false);
        public static bool MacroFinishedSuccessfully;

        public class CustomSet
        {
            [XmlIgnore]
            public bool IsRecording = false;
            [XmlIgnore]
            public Macro BeingRecorded = null;
            public List<Macro> ExistingMacros = new List<Macro>();

            public CustomSet()
            {

            }
        }


        [XmlIgnore]
        public int playedStep = 0;

        public static bool IsRecording
        {
            get
            {
                return Tsunami2.Preferences.Values.GuiPrefs!=null && Tsunami2.Preferences.Values.GuiPrefs.Macros.IsRecording;
            }
            set
            {
                Tsunami2.Preferences.Values.GuiPrefs.Macros.IsRecording = value;
            }
        }

        public static Macro TsunamiStartingMacro { get; internal set; }

        public static Macro MacroBeingRecorded
        {
            get
            {
                return Tsunami2.Preferences.Values.GuiPrefs.Macros.BeingRecorded;
            }
            set
            {
                Tsunami2.Preferences.Values.GuiPrefs.Macros.BeingRecorded = value;
            }
        }

        public static Macro MacroBeingPlayed;

        public static List<Macro> Macros
        {
            get
            {
                return Tsunami2.Preferences.Values.GuiPrefs.Macros.ExistingMacros;
            }
            set
            {
                Tsunami2.Preferences.Values.GuiPrefs.Macros.ExistingMacros = value;
            }
        }

        [XmlAttribute]
        public string Name { get; set; }

        [XmlType(Namespace = "TSU.Macro", TypeName = "Parameters")]
        public class Parameters
        {
            [XmlAttribute]
            public Keys FunctionKey { get; set; }
            [XmlAttribute]
            public double SpeedFactor { get; set; } = 1;

            [XmlIgnore]
            public Rectangle Area
            {
                get
                {
                    return new Rectangle(0, 0, this.Width, this.Height);
                }
                set
                {
                    this.Width = value.Width;
                    this.Height = value.Height;
                }
            }
            [XmlAttribute]
            public int Height = 0;
            [XmlAttribute]
            public int Width = 0;
            public Parameters()
            {

            }
        }

        public Parameters WorkingParameters { get; set; } = new Parameters();

        public List<IInputActivity> KeyStrokesAndMouseClicks { get; set; } = new List<IInputActivity>();
        public static bool IsPlaying { get; set; } = false;

        public static bool IsPaused { get; set; } = false;

        /// <summary>
        /// Total Duration in second including the speed factor
        /// </summary>
        [XmlIgnore]
        public int Duration
        {
            get
            {
                double dDuration = 0;
                foreach (var item in KeyStrokesAndMouseClicks)
                {
                    dDuration += item.DelayInMillis + 100;
                }
                int iDuration = (int)((dDuration / WorkingParameters.SpeedFactor / 1000) + 1);
                return iDuration;
            }
        }

        public static int Resolution_Screen_H { get; internal set; }
        public static int Resolution_Screen_W { get; internal set; }
        public static string MacroFinishedReason { get; private set; }

        internal Label MessageLabel;

        public Macro()
        {

        }

        public delegate void MacroEventHandler(object source, MacroEventArgs args);

        internal static async void Play(Keys keyCode)
        {
            Macro macro = GetExistingMacroByFKey(keyCode);

            if (macro == null)
            {
                WarnNoMacroForThisKey(keyCode);
                return;
            }
            else
            {
                MacroBeingPlayed = macro;
            }


            KeyboardNavigation.MoveToMenuView();

            try
            {
                await Macro.Play(macro);
            }
            catch (Exception ex)
            {
                string titleAndMessage = $"{ex.Message}";
                new MessageInput(MessageType.Critical, titleAndMessage).Show();
            }
        }

        //public static void PlaySync(Macro macro, Action preAction = null, bool PlayIfNotActive = false)
        //{
        //    bool tsunamisIsActive = ApplicationIsActivated();
        //    bool continueMacro = tsunamisIsActive || PlayIfNotActive;

        //    // launch pre action once at beginning
        //    if (preAction != null && macro.playedStep == 0 && continueMacro)
        //    {
        //        preAction();
        //        preAction = null;
        //    }

        //    // run keystrokes until all striked.
        //    foreach (KeyStroke stroke in macro.KeyStrokesAndMouseClicks)
        //    {
        //        string modifier = stroke.Control ? "^" : stroke.Alt ? "%" : stroke.Shift ? "+" : "";
        //        string toSend = $"{modifier}{ToString(stroke.Key)}";
        //        SendKeys.Send(toSend);
        //        Thread.Sleep((int)(stroke.DelayInMillis / macro.speedFactor));
        //    }
        //}

        static int TICK_INTERVAL_MILLIS = 10;
        static int PRE_MOVE_MOUSE_MILLIS = 100;

        public static async Task Play(Macro macro, Action preAction = null, bool careAboutResolution = true)
        {
            if (IsPlaying == true)
            {
                string titleAndMessage = $"Another macro is already been played";
                new MessageInput(MessageType.Important, titleAndMessage).Show();
            }

            var tsunamiView = Tsunami2.Properties.View;
            if (tsunamiView.Width > tsunamiView.Height) // looks like tsunami and not smartnami
                tsunamiView.SetSizeForMacro(1038, 1920);
            else
                tsunamiView.SetSizeForMacro(1038, 720);


            if (!Macro.Resolution_Check(macro, out string message))
            {
                throw new Exception($"{message} => STOP");
            }

            MacroStarted.Set();
            Macro.MacroFinishedSuccessfully = false;

            // now listen at start of tsunami
            //// listen mouse activity, to stop the macro, if the mouse is clicked
            //MouseHook.StartHook();
            //MouseHook.OnMouseUp += MouseHook_OnMouseUp;
            //MouseHook.OnMouseDown += MouseHook_OnMouseDown;

            IsPlaying = true;
            TSU.Debug.WriteInConsole($"---Macro--- {macro.Name} started");
            macro.playedStep = 0;
            // System.Timers.Timer t = new System.Timers.Timer() { Interval = 500, Enabled = false };
            System.Windows.Forms.Timer t = new System.Windows.Forms.Timer() { Interval = 500, Enabled = false };
            t.Tick += async delegate
            {
                if (IsPaused)
                {
                    t.Interval = 10000;
                    new MessageInput(MessageType.FYI, $"x: {Cursor.Position.X} y: {Cursor.Position.Y}").Show();
                    return;
                }
                else
                    t.Interval = TICK_INTERVAL_MILLIS;
                bool tsunamisIsActive = ApplicationIsActivated();
                bool PlayIfNotActive = Globals.UnitTestDetector.RunningInTest;
               
                bool continueMacro = tsunamisIsActive || PlayIfNotActive;

                // launch pre action once at beginning
                if (preAction != null && macro.playedStep == 0 && continueMacro)
                {
                    t.Stop();
                    preAction();
                    preAction = null;
                    t.Start();
                    return;
                }


                // run keystrokes until all striked.
                bool moreActivityToCome = macro.playedStep < macro.KeyStrokesAndMouseClicks.Count;
                if (!moreActivityToCome || !IsPlaying || !continueMacro)
                {
                    t.Stop();

                    if (!moreActivityToCome)
                        Macro.MacroFinishedSuccessfully = true;

                    t.Dispose();
                    if (!continueMacro)
                    {
                        StopPlaying("lost the focus");
                        string titleAndMessage = $"Macro '{macro.Name}' has been stopped because tsunami lost the focus";
                        new MessageInput(MessageType.Critical, titleAndMessage).Show();
                    }
                    else
                    {
                        StopPlaying("macro finished");
                        string titleAndMessage = $"Macro '{macro.Name}' has been played";
                        new MessageInput(MessageType.FYI, titleAndMessage).Show();
                    }
                }
                else
                {
                    IInputActivity input = macro.KeyStrokesAndMouseClicks[macro.playedStep];
                    DisplayStepMessage(macro, input);

                    t.Stop();
                    int waitingTime = (int)(input.DelayInMillis / macro.WorkingParameters.SpeedFactor);

                    bool preMoveMouse = false;
                    if (input is MouseActivity mouseInput)

                        if (preMoveMouse)
                            waitingTime -= PRE_MOVE_MOUSE_MILLIS;


                    await Task.Delay(waitingTime);

                    TSU.Debug.WriteInConsole($"---Macro--- {input} will happen");
                    if (input is KeyStroke ks)
                    {
                        PlayOneKeyStroke(ks);
                    }
                    else if (input is MouseActivity ma)
                    {
                        ChangeMousePositionToFitTrace(ma);

                        Action actionToBePlayedInGuiThread;
                        if (preMoveMouse)
                        {
                            actionToBePlayedInGuiThread = () => { MouseSimulator.SetCursorPosition(ma.X, ma.Y); };
                            await Task.Delay(PRE_MOVE_MOUSE_MILLIS);
                        }
                        actionToBePlayedInGuiThread = () => { MouseSimulator.Click(ma.X, ma.Y, ma.Button, ma.Direction); };

                        Tsunami2.Properties.InvokeOnApplicationDispatcher(actionToBePlayedInGuiThread);
                    }

                    TSU.Debug.WriteInConsole($"---Macro--- {input} happened");


                    t.Start();
                    macro.playedStep++;
                }
            };

            if (Tsunami2.Properties != null)
                macro.MessageLabel = Tsunami2.View.ShowTopMessage($"Macro '{macro.Name}' being played:", System.Drawing.Color.DarkSeaGreen);
            t.Start();
        }

        private static void ChangeMousePositionToFitTrace(MouseActivity ma)
        {
            if (ma.Trace == null || ma.Trace == "") 
                return;
            string[] parts = ma.Trace.Split('+');
            if (!ChangeMousePositionToFitTrace(ma, parts, System.Windows.Forms.Application.OpenForms.Cast<Control>().ToList()))
                Debug.WriteInConsole($"Control {ma.Trace} not found");
        }
        private static bool ChangeMousePositionToFitTrace(MouseActivity ma, string[] parts, List<Control> controls)
        {
            var trace = ma.Trace;
            if (trace == "") return false;

            string controlName = parts[0];

            string controlText = (parts.Length > 1) ? parts[1] : "" ;

            foreach (Control item in controls)
            {
                Debug.WriteInConsole($"Looking for {trace} in {item.Name}");

                // 1. control itself
                if (!item.Visible)
                    continue;

                var a = Regex.Replace(item.Name, @"\d{1,2}/\d{1,2}/\d{4}\s\d{1,2}:\d{2}:\d{2}", "Replaced_Date_And_Time");
                if (a != item.Name)
                {
                }

                var name = Regex.Replace(item.Name, @"\d{1,2}/\d{1,2}/\d{4}\s\d{1,2}:\d{2}:\d{2}", "Replaced_Date_And_Time");
                if (name == controlName)
                {
                    var text = Regex.Replace(item.Text, @"\d{1,2}/\d{1,2}/\d{4}\s\d{1,2}:\d{2}:\d{2}", "Replaced_Date_And_Time");
                    if (text == controlText)
                    {
                        TSU.Debug.WriteInConsole("Looking for button names= {text}");
                        System.Drawing.Point buttonScreenLocation = item.PointToScreen(System.Drawing.Point.Empty);
                        ma.X = buttonScreenLocation.X + item.Width / 2;
                        ma.Y = buttonScreenLocation.Y + item.Height / 2;
                        return true;
                    }
                }

                // 2 in popupmenu
                if (item is TsuView tsuView)
                {
                    if (tsuView.PopUpMenu != null && tsuView.PopUpMenu.Visible)
                        if (ChangeMousePositionToFitTrace(ma, parts, tsuView.PopUpMenu.Controls.Cast<Control>().ToList()))
                            return true;
                }

                // 3. look in childs
                if (ChangeMousePositionToFitTrace(ma, parts, item.Controls.Cast<Control>().ToList()))
                    return true;
            }

            return false;
        }

        public static void PlayOneKeyStroke(KeyStroke ks)
        {
            var toSend = ks.KeyData.ToUpper();
            Debug.WriteInConsole($"will sendkeys.send '{toSend}'");
            Tsunami2.Properties.InvokeOnApplicationDispatcher(() => { SendKeys.Send(toSend); });
        }

        private static void DisplayStepMessage(Macro macro, IInputActivity input)
        {
            // current step
            string message = $"Macro '{macro.Name}' Playing: {input.ToString()}";
            message = message.Replace("$count$", macro.KeyStrokesAndMouseClicks.Count.ToString());

            // is next step a trace ? (for mouse up events)
            IInputActivity nextInput = macro.playedStep < macro.KeyStrokesAndMouseClicks.Count - 1 ? macro.KeyStrokesAndMouseClicks[macro.playedStep + 1] : null;
            if (nextInput != null && nextInput is Trace trace)
                message += $"on {trace.message}";

            // is next-next step a trace ? (for mouse down events)
            IInputActivity nextnextInput = macro.playedStep < macro.KeyStrokesAndMouseClicks.Count - 2 ? macro.KeyStrokesAndMouseClicks[macro.playedStep + 2] : null;
            if (nextnextInput != null && nextnextInput is Trace trace2)
                message += $"on {trace2.message}";

            macro.MessageLabel.Text = message;
        }

        /// <summary>
        /// Returns true if the current application has focus, false otherwise
        /// </summary>
        public static bool ApplicationIsActivated(bool log = false)
        {
            IntPtr activatedHandle = GetForegroundWindow();
            if (log) Logs.Log.AddEntry(null, $"activatedHandle: {activatedHandle}", Logs.LogEntry.LogEntryTypes.Error);
            if (activatedHandle == IntPtr.Zero)
            {
                return false;       // No window is currently activated
            }

            int procId = Process.GetCurrentProcess().Id;
            if (log) Logs.Log.AddEntry(null, $"procId: {procId}", Logs.LogEntry.LogEntryTypes.Error);
            int activeProcId;
            GetWindowThreadProcessId(activatedHandle, out activeProcId);

            if (log) Logs.Log.AddEntry(null, $"activeProcId: {activeProcId}", Logs.LogEntry.LogEntryTypes.Error);
            bool active = activeProcId == procId;
            if (log) Logs.Log.AddEntry(null, $"ApplicationIsActive: {active}", Logs.LogEntry.LogEntryTypes.Error);
            return active;
        }


        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowThreadProcessId(IntPtr handle, out int processId);

        private static void WarnNoMacroForThisKey(Keys keyCode)
        {
            string titleAndMessage = $"No macro recorded for '{keyCode}'; You can set Macro to the 12 function keys, " +
                                     $"you can start recording it with CTRL+FX and stop recording it with same FX key." +
                                     $"You can use Macro inside macro if you wish.";
            new MessageInput(MessageType.Warning, titleAndMessage).Show();
        }

        public override string ToString()
        {
            int extensionCount = 0;
            foreach (var item in KeyStrokesAndMouseClicks)
            {
                if (item is Trace t)
                    if (t.message == "Macro extension")
                        extensionCount++;
            }
            return $"CTRL-{this.WorkingParameters.FunctionKey}: {this.Name} ({this.KeyStrokesAndMouseClicks.Count} step(s) and {extensionCount} extension(s))";
        }

        public static string KeyToString(Keys key)
        {
            switch (key)
            {
                case Keys.Back:
                    return "{BACKSPACE}";
                case Keys.Separator:
                    return "{BREAK}";
                case Keys.Escape:
                    return "{ESC}";
                case Keys.PageDown:
                    return "{PGDN}";
                case Keys.PageUp:
                    return "{PGUP}";
                case Keys.Return:
                    return "{ENTER}";
                case Keys.PrintScreen:
                    return "{PRTSC}";
                case Keys.Scroll:
                    return "{SCROLLLOCK}";
                case Keys.ShiftKey:
                    return "+";
                case Keys.ControlKey:
                    return "^";
                case Keys.Alt:
                    return "%";
                case Keys.Space:
                    return " ";
                case Keys.Decimal:
                    return ".";
                case Keys.Menu:
                    return "";
                default:
                    string keyData = "{" + key.ToString().ToUpper() + "}";
                    keyData = keyData.Replace("NUMPAD", "");
                    return keyData;
            }
        }

        internal static DateTime lastStrokeDate;

        internal static void ContinueRecording(KeyEventArgs e, Control controlThatGotTheStroke)
        {
            if (!IsRecording)
                throw new Exception("IsRecording not set to true");
            if (MacroBeingRecorded == null)
                throw new Exception("No macro being recording now, 'MacroBeingRecorded == null' ");

            if (MacroBeingRecorded.WorkingParameters.FunctionKey == e.KeyCode)
            {
                if (e.Shift)
                    throw new Exception($"Macro for '{e.KeyCode}' already being recorded");
                else
                    StopRecording();
            }

            Keys correctedKey = e.KeyCode;
            MacroBeingRecorded.KeyStrokesAndMouseClicks.Add2(CreateKeyStrokeData(e, controlThatGotTheStroke));
        }

        private static IInputActivity CreateKeyStrokeData(KeyEventArgs e, Control control)
        {
            // target name:
            string targetControlName = control is TsuView ? ((TsuView)control)._Name : control.GetType().Name;

            // delay
            TimeSpan t = DateTime.Now - lastStrokeDate;
            lastStrokeDate = DateTime.Now;
            int delay = (int)(t.TotalMilliseconds + TICK_INTERVAL_MILLIS);

            // keyData (to sendKey later)
            string keyData = "";
            keyData += e.Control ? "^" : "";
            keyData += e.Shift ? "+" : "";
            keyData += e.Alt ? "%" : "";


            string sKey = KeyToString(e.KeyCode);

            bool isA_D_Key = sKey.Length == 4 && sKey[1] == 'D';
            bool isInputBox = isA_D_Key && (control is TextBox || control is ComboBox || control is DataGridView);
            bool isVerySpecialCase = isA_D_Key && isInputBox;
            if (!isVerySpecialCase)
            {
                keyData += sKey;
            }
            else
            {
                if (control is TextBox tb)
                {
                    keyData += tb.Text[tb.SelectionStart - 1];
                }
                else if (control is ComboBox cb)
                {
                    keyData += cb.Text[cb.SelectionStart - 1];
                }
                else if (control is DataGridView dgv)
                {
                    keyData += "errorDGV";
                }
            }


            var ks = new KeyStroke()
            {
                TargetControl = targetControlName,
                DelayInMillis = delay,
                KeyData = keyData,
            };
            return ks;
        }

        private static Macro GetExistingMacroByFKey(Keys fKey)
        {
            return Macros.Find(x => x.WorkingParameters.FunctionKey == fKey);
        }

        private static bool IsExistingMacroByFKey(Keys fKey)
        {
            return Macros.Find(x => x.WorkingParameters.FunctionKey == fKey) != null;
        }

        private static void StopRecording()
        {
            IsRecording = false;

            // now done when leaving tsunami
            //MouseHook.OnMouseUp -= MouseHook_OnMouseUp;
            //MouseHook.OnMouseDown -= MouseHook_OnMouseDown;
            //MouseHook.StopHook();

            if (MacroBeingRecorded != null)
            {
                Tsunami2.View.RemoveTopMessage(MacroBeingRecorded.MessageLabel);
                EditExistingMacroProperties(MacroBeingRecorded.WorkingParameters.FunctionKey, isJustRecordedMacro: true);
            }

            //DsaFlag dsa = DsaFlag.GetByNameOrAdd(Tsunami2.Properties.DsaFlag, $"Macro_{MacroBeingRecorded.FunctionKey}");
            //var r = Tsunami2.View.ShowMessage(Views.MessageTsu.Types.Choice,
            //    "Macro name;How do you want to (re)name this macro?",
            //    TSU.Views.Message.CreationHelper.GetOKCancelButtons(),
            //    new List<Control>() { TSU.Views.Message.CreationHelper.GetPreparedTextBox(
            //        (MacroBeingRecorded!=null && MacroBeingRecorded.Name != "" )? MacroBeingRecorded.Name: "My Macro") }, dsa);
            //if (r.TextOfButtonClicked == R.T_CANCEL)
            //{
            //    MacroBeingRecorded = null;
            //    IsRecording = false;
            //    return;
            //}
            //MacroBeingRecorded.Name = r.ReturnedControls[0].Text;
            if (MacroBeingRecorded != null)
            {
                Macros.Remove(GetExistingMacroByFKey(MacroBeingRecorded.WorkingParameters.FunctionKey));
                Macros.Add(MacroBeingRecorded);
                MacroBeingRecorded = null;
                Tsunami2.Preferences.Values.SaveGuiPrefs();
            }
        }
        /// <summary>
        /// check what to do, between start stop or continu to recoed teh macro, also the
        /// </summary>
        /// <param name="e"></param>
        /// <param name="alreadyCatchedByChild"></param>
        /// <param name="control"></param>
        internal static void AnalyseAndAct(KeyEventArgs e, Control controlThatGotTheStroke)
        {
            bool isFunctionKey = KeyboardNavigation.IsFunctionKey(e);

            bool pauseCombo = e.KeyCode == Keys.P && e.Control == true;
            IsPaused = IsPaused ? !pauseCombo : pauseCombo;


            if (IsPlaying && isFunctionKey && e.Control) // this is a combo that have no chance exist in a playing macro
            {
                string message = $"Macro '{MacroBeingPlayed.Name}' has been stopped by a key press";
                StopPlaying("KeyPressed");
                new MessageInput(MessageType.FYI, message).Show();
                return;
            }

            // If not recording and the key is not a fkey, there is nothing to do
            if (!IsRecording && !isFunctionKey) return;

            // with shift we record
            if (!IsRecording && isFunctionKey && e.Shift) { StartRecording(e.KeyCode); return; }

            // with alt we alter
            if (!IsRecording && isFunctionKey && e.Alt) { EditExistingMacroProperties(e.KeyCode, isJustRecordedMacro: false); return; }

            // with control we play
            if (!IsRecording && isFunctionKey && e.Control) { Play(e.KeyCode); return; }

            // with shift and the current recoodrinf fkey we stop the recording
            if (IsRecording && isFunctionKey && e.Shift && MacroBeingRecorded.WorkingParameters.FunctionKey == e.KeyCode) { StopRecording(); return; }

            // if recording and not 
            if (IsRecording) { ContinueRecording(e, controlThatGotTheStroke); return; }
        }

        private static async void StopPlaying(string reason)
        {
            if (MacroBeingPlayed != null)
            {
                Macro.MacroFinishedReason = reason;
                TSU.Debug.WriteInConsole($"---Macro--- {MacroBeingPlayed.Name} stopped");

                // now done when leaving tsunami
                //MouseHook.OnMouseUp -= MouseHook_OnMouseUp;
                //MouseHook.OnMouseDown -= MouseHook_OnMouseDown;
                //MouseHook.StopHook();

                lastStrokeDate = DateTime.Now;
                IsPlaying = false;


                Tsunami2.View.RemoveTopMessage(MacroBeingPlayed.MessageLabel);
                MacroBeingPlayed = null;

                await Task.Delay(2000);
                Macro.MacroFinished2SecondsAgo.Set();
            }
        }

        private static void EditExistingMacroProperties(Keys keyCode, bool isJustRecordedMacro)
        {
            Macro selectedMacro = isJustRecordedMacro ? MacroBeingRecorded : GetExistingMacroByFKey(keyCode);
            if (selectedMacro != null)
            {
                //combobox with all macros
                TextBox nameBox = CreationHelper.GetPreparedTextBox(selectedMacro.Name.ToString()) as TextBox;
                TextBox speedBox = CreationHelper.GetPreparedTextBox(selectedMacro.WorkingParameters.SpeedFactor.ToString()) as TextBox;
                var macros = new List<Macro>(Macros);
                if (isJustRecordedMacro)
                    // add the justRecorded macro because it is not yet in the saved list
                    macros.Add(selectedMacro);
                BindingSource bs = new BindingSource() { DataSource = macros };
                bs.Position = bs.IndexOf(selectedMacro);
                ComboBox comboBox = CreationHelper.GetComboBox(bs);
                comboBox.Enabled = !isJustRecordedMacro;
                comboBox.DropDownStyle = ComboBoxStyle.DropDownList;

                comboBox.SelectedValueChanged += delegate (object sender, EventArgs e)
                {
                    selectedMacro = ((ComboBox)sender).SelectedItem as Macro;
                    if (selectedMacro != null)
                    {
                        nameBox.Text = selectedMacro.Name;
                        speedBox.Text = selectedMacro.WorkingParameters.SpeedFactor.ToString();
                    }
                };


                // Button remove Extension
                Button RemoveButton = MessageTsu.GetAButton("rm", "Remove last Extension", Tsunami2.Preferences.Theme.Colors.NormalFore, Tsunami2.Preferences.Theme.Colors.Object);
                RemoveButton.Click += delegate
                {
                    selectedMacro.RemoveLastExtension();
                    Macro temp = selectedMacro;
                    comboBox.DataSource = null;

                    bs = new BindingSource() { DataSource = Macros };
                    bs.Position = bs.IndexOf(temp);
                    comboBox.DataSource = bs;
                };

                // Button Show
                Button showButton = MessageTsu.GetAButton("show", "Show in .XML", Tsunami2.Preferences.Theme.Colors.NormalFore, Tsunami2.Preferences.Theme.Colors.Object);
                showButton.Click += delegate
                {
                    IO.Xml.CreateFile(selectedMacro, "temp.xml");
                    Process.Start("temp.xml");
                };

                // Button export
                Button exportButton = MessageTsu.GetAButton("export", "Export to .XML", Tsunami2.Preferences.Theme.Colors.NormalFore, Tsunami2.Preferences.Theme.Colors.Object);
                exportButton.Click += delegate
                {
                    string path = TSU.IO.Path.AskFileLocation($"{selectedMacro.Name}.macro.xml");
                    if (path != "")
                        IO.Xml.CreateFile(selectedMacro, path);
                };

                // Button import
                Button importButton = MessageTsu.GetAButton("import", "Import from .XML", Tsunami2.Preferences.Theme.Colors.NormalFore, Tsunami2.Preferences.Theme.Colors.Object);
                importButton.Click += delegate
                {
                    if (TSU.IO.Xml.BrowseAnXml(out string path))
                    {
                        object o = IO.Xml.DeserializeFile(typeof(Macro), path);
                        if (o != null && o is Macro m)
                        {
                            selectedMacro.DoCopy(m);
                        }
                    }
                };

                // ask
                string titleAndMessage = $"Editing macro;Select a existing macro and modify its name or speed factor.";
                MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
                {
                    ButtonTexts = CreationHelper.GetOKCancelButtons(),
                    Controls = new List<Control>()
                    {
                        comboBox,
                        RemoveButton,
                        showButton,
                        exportButton,
                        importButton,
                        CreationHelper.GetPreparedLabel("Macro Name:"),
                        nameBox,
                        CreationHelper.GetPreparedLabel("Speed factor:"),
                        speedBox,
                    }
                };
                using (var r = mi.Show())
                {
                    if (r.TextOfButtonClicked == R.T_CANCEL)
                    {
                        Tsunami2.Preferences.Values.LoadGuiPref(); // reload to cancel changes
                        return;
                    }
                }

                // get the modified macro
                selectedMacro = comboBox.SelectedItem as Macro;
                if (selectedMacro != null)
                {
                    selectedMacro.Name = nameBox.Text;
                    if (double.TryParse(speedBox.Text, out double s))
                        selectedMacro.WorkingParameters.SpeedFactor = s;
                }

                Tsunami2.Preferences.Values.SaveGuiPrefs();
            }
            else
                WarnNoMacroForThisKey(keyCode);
        }

        private void DoCopy(Macro m)
        {
            this.KeyStrokesAndMouseClicks = m.KeyStrokesAndMouseClicks;
            this.Name = m.Name;
            this.WorkingParameters = m.WorkingParameters;
        }

        private void RemoveLastExtension()
        {
            IInputActivity lastExtension = this.KeyStrokesAndMouseClicks.LastOrDefault(x => x.GetType() == typeof(Trace) && ((Trace)x).message.Contains("Macro extension"));

            if (lastExtension != null)
            {
                int index = KeyStrokesAndMouseClicks.IndexOf(lastExtension);

                // Check if the specific object is not the last item in the list
                if (index < KeyStrokesAndMouseClicks.Count - 1)
                {
                    // Remove all entries after the specific object
                    KeyStrokesAndMouseClicks.RemoveRange(index, KeyStrokesAndMouseClicks.Count - index);
                }
            }
        }


        private static void StartRecording(Keys keyCode)
        {
            if (!TSU.Debug.isMouseHooked)
                TSU.Debug.MouseAndKeyboardMonitoring_Subscribe();

            bool createANewOne = true;
            Macro existing = GetExistingMacroByFKey(keyCode);
            if (existing != null)
            {
                string replace = "Override";
                string extend = "Extend";
                DsaFlag dsa = DsaFlag.GetByNameOrAdd(Tsunami2.Properties.DsaFlag, $"Macro_Overriding_Warning");
                string titleAndMessage = $"Replace the existing '{keyCode}' macro or extend it?; The '{keyCode}' key is currently assigned to '{existing.Name}'.";
                var r = new MessageInput(MessageType.Critical, titleAndMessage)
                {
                    ButtonTexts = new List<string> { replace, extend, R.T_CANCEL },
                    DontShowAgain = dsa
                }.Show();
                if (r.TextOfButtonClicked == R.T_CANCEL)
                    return;
                if (r.TextOfButtonClicked == extend)
                    createANewOne = false;
            }
            else
            {
                DsaFlag dsa = DsaFlag.GetByNameOrAdd(Tsunami2.Properties.DsaFlag, $"Macro_Recoding_Warning");
                string titleAndMessage = $"Recording a macro for '{keyCode}'?;The macro then can be re-played by pression the '{keyCode}'. Ctrl-'{keyCode}' will override the macro with a new one.";
                var r = new MessageInput(MessageType.FYI, titleAndMessage)
                {
                    ButtonTexts = CreationHelper.GetOKCancelButtons(),
                    DontShowAgain = dsa
                }.Show();
                if (r.TextOfButtonClicked == R.T_CANCEL)
                    return;
            }

            if (createANewOne)
                MacroBeingRecorded = new Macro() { Name = keyCode.ToString(), WorkingParameters = new Parameters() { FunctionKey = keyCode } };
            else
            {
                MacroBeingRecorded = existing;
                MacroBeingRecorded.KeyStrokesAndMouseClicks.Add2(new Macro.Trace() { message = "Macro extension" });
            }

            IsRecording = true;
            lastStrokeDate = DateTime.Now;

            Resolution_SaveValues(MacroBeingRecorded);

            MacroBeingRecorded.MessageLabel = Tsunami2.View.ShowTopMessage("Macro being recorded", System.Drawing.Color.DarkOrange);

            // now done when starting tsunami
            //// Hook up mouse event handlers
            //MouseHook.StartHook();
            //MouseHook.OnMouseUp += MouseHook_OnMouseUp;
            //MouseHook.OnMouseDown += MouseHook_OnMouseDown;

            //Focus to tsunami menu
            KeyboardNavigation.MoveToMenuView();
        }

        private static void Resolution_SaveValues(Macro macro)
        {
            var mainView = Tsunami2.Properties.View;
            if (mainView.Width > mainView.Height) // looks like tsunami and not smartnami
                mainView.SetSizeForMacro(1038, 1920);
            else
                mainView.SetSizeForMacro(1038, 720);

            //var screenBounds = System.Windows.Forms.Screen.FromControl(mainView).WorkingArea;
            macro.WorkingParameters.Area = new Rectangle(mainView.Left, mainView.Top, mainView.Width, mainView.Height);
        }

        public static bool Resolution_Check(Macro macro, out string message)
        {
            message = "OK";

            bool rightH = Math.Abs(macro.WorkingParameters.Height - Macro.Resolution_Screen_H) < 20;
            bool rightV = Math.Abs(macro.WorkingParameters.Width - Macro.Resolution_Screen_W) < 20;
            bool oko = rightH && rightV;
            if (!(oko))
                message = $"Macro was recorded in resolution: {macro.WorkingParameters.Width}x{macro.WorkingParameters.Height} and you have {Macro.Resolution_Screen_W}x{Macro.Resolution_Screen_H}";
            return oko;
        }

        public static void MouseHook_OnMouseUp(object sender, MouseEventArgs e)
        {
            if (IsRecording)
                MouseActivity.Record(MouseHook.MouseUpAndDown.Up, e, GetControlUnderCursor());
            else if (IsPlaying)
            {
                string message = $"Macro '{MacroBeingPlayed.Name}' has been stopped by a mouse click";
                StopPlaying("user mouse click up");
                new MessageInput(MessageType.FYI, message).Show();
            }

        }
        public static void MouseHook_OnMouseDown(object sender, MouseEventArgs e)
        {
            if (Tsunami2.Preferences.Values.GuiPrefs.MakeVideoFromScreenshots.IsTrue ||
                Tsunami2.Preferences.Values.GuiPrefs.MakeScreenshotAtEveryMouseClick.IsTrue)
                TSU.Debug.Screenshots.Take();

            if (IsRecording)
                MouseActivity.Record(MouseHook.MouseUpAndDown.Down, e, GetControlUnderCursor());

            else if (IsPlaying)
            {
                string message = $"Macro '{MacroBeingPlayed.Name}' has been stopped by a mouse click";
                StopPlaying("user mouse click up");
                new MessageInput(MessageType.FYI, message).Show();
            }
        }

        public static Control GetControlUnderCursor()
        {
            System.Drawing.Point cursorPos = Cursor.Position;
            IntPtr hWnd = WindowFromPoint(cursorPos);
            Control control = Control.FromHandle(hWnd);
            if (control!=null && control.Parent!=null && control.Parent is BigButton bb)
                return bb;
            if (control is Button b)
                return b;
            //if (control != null)
            //{
            //    System.Drawing.Point clientPos = control.PointToClient(cursorPos);
            //    Control childControl = control.GetChildAtPoint(clientPos);

            //    return childControl ?? control;
            //}
            return null;
        }

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern IntPtr WindowFromPoint(System.Drawing.Point point);

        public static void WaitFor(Macro macro)
        {
            double millis = 0;
            foreach (var item in macro.KeyStrokesAndMouseClicks)
            {
                millis += item.DelayInMillis / macro.WorkingParameters.SpeedFactor;
            }

            // wait
            Thread.Sleep(((int)millis + 500) * 20);
        }

        public static Macro LoadFromFile(string filePath)
        {
            object obj = TSU.IO.Xml.DeserializeFile(typeof(Macro), filePath);
            if (obj is Macro macro)
            {
                return macro;
            }
            else
            {
                throw new Exception($"Couldn't open {filePath} as a macro XML");
            }
        }

        internal static void DoTrace(string message)
        {
            if (Macro.IsRecording)
            {
                MacroBeingRecorded.KeyStrokesAndMouseClicks.Add2(new Macro.Trace() { message = message });
            }
        }
    }
    public class MacroEventArgs : EventArgs
    {
        public MacroEventArgs()
        {
        }
    }
}