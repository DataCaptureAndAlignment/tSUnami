﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using TSU.Common;
using F = System.Windows.Forms;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;

namespace TSU.Tools.Exploration
{

    public class Explorator : Manager
    {
        // singleton with parameters
        private static volatile Explorator instance;
        private static readonly object syncRoot = new object();
        public static Explorator Instance(Module parentModule)
        {

            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                    {

                        instance = new Explorator(parentModule);
                        instance.LoadModules();
                    }
                }
            }
            if (instance.ParentModule == null) instance.ParentModule = parentModule;
            instance.Update();
            return instance;
        }

        [XmlIgnore]
        internal new View View
        {
            get
            {
                return _TsuView as View;
            }
        }

        private void LoadModules()
        {
            AllElements.Clear();
            SelectableObjects.Clear();

            // properties
            Debug.WriteInConsole("AllElements.Add(Tsunami2.Properties)");
            AllElements.Add(Tsunami2.Properties);


            // measurement modules
            foreach (IModule entry in Tsunami2.Properties.childModules)
            {
                if (!(entry is Explorator || entry is Functionalities.Menu || entry is O.Manager || entry is Preferences.Preferences))
                {
                    Debug.WriteInConsole($"AllElements.Add({entry.GetType().FullName})");
                    AllElements.Add(entry as Module);
                }
            }


        }

        public Explorator() // this parameterless constructor is normaly used only by the serializer, we put here the initialization of what is not serialized (like views for example)
            : base()
        {
        }
        public Explorator(Module parentModule)
            : base(parentModule, R.T_B_Explore)
        {
        }
        public override void Initialize()
        {
            base.Initialize();

            MultiSelection = false;

            AllElements = new List<TsuObject>();

            NodeClicked += On_NodeClicked;
        }

        private void On_NodeClicked(object sender, ManagerEventArgs e)
        {
            F.TreeNodeMouseClickEventArgs eventTag = e.Tag as F.TreeNodeMouseClickEventArgs;
            View.ShowOptions(eventTag.Node.Tag);
        }

        internal void Update()
        {
            LoadModules();
            SelectableObjects.Clear();
        }

        internal void Clear()
        {
            AllElements.Clear();
        }
        internal void Open()
        {
            Tsunami nT = null;

            // deserialize

            string fileName = View.GetSavingName(
               Tsunami2.Preferences.Values.Paths.Data,
                TSU.Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now) + "_" + _Name,
                "xml (*.xml)|*.xml");

            if (fileName != "")
                nT = IO.Xml.DeserializeFile(typeof(Module), fileName) as Tsunami; // Add modules
            if (nT != null)
            {
                Tsunami oT = ParentModule as Tsunami;
                foreach (Module item in nT.childModules)
                {
                    if (!(item is Functionalities.Menu || item is Explorator))
                    {
                        oT.Add(item);
                        item._TsuView.UpdateView();
                    }
                }
            }
        }
        public void Save(string fileName = null)
        {
            IO.Xml.CreateFile(ParentModule, fileName);
        }
        public new void Dispose()
        {
            _TsuView.Dispose();
        }

        internal void RemoveModule(Module item)
        {
            (ParentModule as Tsunami).Remove(item);
            if (item is FinalModule finalModule)
            {
                foreach (var stm in finalModule.StationModules)
                {
                    stm.DecoupleFromInstrument(null);
                    stm.Dispose();
                }
                Tsunami2.Preferences.Tsunami.MeasurementModules.Remove(finalModule);
                Tsunami2.Properties.childModules.Remove(finalModule);
            }
            item.Dispose();
            UpdateView();
        }

        internal void ExploreTsu()
        {
            string tsuPath = Tsunami2.Preferences.Tsunami.PathOfTheSavedFile;
            if (tsuPath == "")
            {
                // open a file dialog
            }

            // extract xml from tsu

            string xmlPath = IO.Tsu.ExtractXmlFromTsu(tsuPath);

            // Load the XML document.
            XmlDocument xml_doc = IO.Xml.OpenXmlDocument(xmlPath);

            // Add the root node's children to the TreeView.
            F.TreeView tv = View.treeview;
            tv.BeginUpdate();
            tv.Nodes.Clear();
            Views.TsuNode main = new Views.TsuNode(tsuPath);
            tv.Nodes.Add(main);
            AddTreeViewChildNodes(main, xml_doc.DocumentElement);
            tv.CollapseAll();
            tv.EndUpdate();
            tv.Refresh();
        }

        // Add the children of this XML node 
        // to this child nodes collection.
        private void AddTreeViewChildNodes(
            Views.TsuNode parent, XmlNode xml_node)
        {
            if (xml_node.Attributes != null)
            {
                foreach (XmlAttribute item in xml_node.Attributes)
                {
                    if (item.Name != "xmlns")
                        parent.Nodes.Add(new Views.TsuNode($"{item.Name}: {item.Value}"));
                }
            }

            foreach (XmlNode item in xml_node.ChildNodes)
            {
                if (item.Name != "#text")
                {
                    // Make the new TreeView node.
                    Views.TsuNode new_node = new Views.TsuNode($"{item.Name}: {item.InnerText}");
                    parent.Nodes.Add(new_node);

                    // Recursively make this node's descendants.
                    AddTreeViewChildNodes(new_node, item);

                    // If this is a leaf node, make sure it's visible.
                    if (new_node.Nodes.Count == 0) new_node.EnsureVisible();
                }
            }
        }

        /// <summary>
        /// Will tell if the object is ok to be selected; always false
        /// </summary>
        /// <param name="tsuObject"></param>
        /// <returns>false</returns>
        internal override bool IsItemSelectable(TsuObject tsuObject) => false;
    }
}
