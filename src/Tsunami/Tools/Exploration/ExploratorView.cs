﻿using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Windows.Forms;
using TSU.Common;
using TSU.Views;
using TSU.Views.Message;
using M = TSU;
using R = TSU.Properties.Resources;
using V = TSU.Common.Strategies.Views;

namespace TSU.Tools.Exploration
{
    public partial class View : ManagerView
    {
        // Fields
        internal new Explorator Module
        {
            get
            {
                return base._Module as Explorator;
            }
            set
            {
                base._Module = value;
            }
        }

        public TreeView treeview
        {
            get
            {
                return this.currentStrategy.GetControl() as TreeView;
            }
            private set
            {

            }
        }

        //Constructor
        public View(M.IModule module)
            : base(module) // Taking form already in controllerModuleView
        {
            _Name = R.T367;
            this.Image = R.Explore;
            InitializeComponent();
            this.ApplyThemeColors();
            InitializeMenu();
            this.AdaptTopPanelToButtonHeight();

            this.currentStrategy.Update();
            this.currentStrategy.Show();

            this.VisibleChanged += new EventHandler(OnHide);

        }
        private void ApplyThemeColors()
        {
            this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
        }
        internal override void InitializeStrategy()
        {
            this.AvailableViewStrategies = new List<V.Types>() { V.Types.List, V.Types.Tree };
            this.SetStrategy(V.Types.Tree);
        }

        internal override IEnumerable<Control> GetOptions(object tag)
        {
            foreach (Control c in base.GetOptions(tag))
                yield return c;

            if (tag is DsaFlag dsa)
            {
                yield return new BigButton(DsaOptions.Always.ToString(), R.StatusGood, () => ChangeDsa(DsaOptions.Always, dsa));
                yield return new BigButton(DsaOptions.Never.ToString(), R.StatusBad, () => ChangeDsa(DsaOptions.Never, dsa));
                yield return new BigButton(DsaOptions.Ask_to.ToString(), R.StatusQuestionnable, () => ChangeDsa(DsaOptions.Ask_to, dsa));
                yield return new BigButton(DsaOptions.Ask_to_with_pre_checked_dont_show_again.ToString(), R.StatusQuestionnable, () => ChangeDsa(DsaOptions.Ask_to_with_pre_checked_dont_show_again, dsa));
            }

            if (tag is TsuBool tb)
            {
                yield return new BigButton(R.T_YES, R.StatusGood, () => ChangeBool(true, tb));
                yield return new BigButton(R.T_NO, R.StatusBad, () => ChangeBool(false, tb));
            }

            if (tag is FinalModule m)
            {
                yield return new BigButton(R.T455, R.Delete_Ancrage, ()=> DeleteModuleWithConfirmation(m));
            }
        }

        private void DeleteModuleWithConfirmation(FinalModule m)
        {
            string question = string.Format(R.T_ARE_YOU_SURE_YOU_WANT_TO_REMOVE_THE_MODULE, m._Name);
            MessageInput mi = new MessageInput(MessageType.Warning, question)
            {
                ButtonTexts = new List<string> { R.T_YES, R.T_NO }
            };
            string answer = mi.Show().TextOfButtonClicked;
            if (answer == R.T_YES)
                Module.RemoveModule(m);
        }

        private void ChangeBool(bool newValue, TsuBool tb)
        {
            tb.Set(newValue);
            Tsunami2.Preferences.Values.SaveGuiPrefs();
            UpdateView();
        }

        private void ChangeDsa(DsaOptions newDsa, DsaFlag dsa)
        {
            dsa.State = newDsa;
            UpdateView();
        }

        private void OnHide(object sender, EventArgs e)
        {
            if (this.Visible)
                Module.Update();
            //else                    
            //ExplorationModule.Clear(); why?
        }

        // Menu
        internal override void InitializeMenu()
        {
            base.InitializeMenu();
            bigbuttonTop.ChangeNameAndDescription(R.T451);
            bigbuttonTop.ChangeImage(R.Module);
        }

        internal override void ShowContextMenuGlobal()
        {
            contextButtons = new List<Control>();

            contextButtons.Add(new BigButton(
               R.T_B_Close,
               R.MessageTsu_Problem,
                this.CloseView));

            //contextButtons.Add(new BigButton(
            //   R.T453,
            //   R.Open,
            //    this.Open));

            //contextButtons.Add(new BigButton(
            //   R.T454,
            //   R.Save,
            //    this.Save));

            contextButtons.Add(new BigButton(
               $"Explore *.TSU;Explore the structure of TSU file", R.Tsunami_Alpha
               ,
                this.Module.ExploreTsu));

            this.ShowPopUpMenu(contextButtons);
            this.UpdateView();
        }

        private void CloseView()
        {
            Tsunami2.Properties.Remove(this.Module);

        }

        private void Open()
        {
            this.TryAction(this.Module.Open);
        }

        private void Save()
        {
            this.Module.Save();
        }

        // Strategy
        // define an interface for different strategy of showing the elements
        public override void UpdateView()
        {
            (this.Module).Update();
            base.UpdateView();
        }

        public override List<string> ListviewColumnHeaderTexts
        {

            get
            {
                return new List<string>() { "Name" };
            }
            set { }
        }


        private void ExplorationModuleView_FormClosing(object sender, FormClosingEventArgs e)
        {
            //e.Cancel = true;
            this.Hide();
        }
    }

}
