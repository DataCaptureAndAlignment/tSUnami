﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text;
using R = TSU.Properties.Resources;
using System.Diagnostics;
using UnitTestGenerator;

namespace TSU.Views
{
    public partial class TsuPopUpMenu : TsuView
    {
        public bool MouseIsInOrInChild
        {
            get
            {
                // check this popup
                Rectangle r = new Rectangle(this.Bounds.Location, this.Bounds.Size);
                r.Inflate(50, 50);
                bool isIn = r.Contains(MousePosition);
                if (!isIn)
                {
                    if (this.subMenu != null)
                        isIn = this.subMenu.MouseIsInOrInChild;
                }
                // TSU.Debug.WriteInConsole($"MouseIsInOrInChild =  {isIn}");
                return isIn;
            }
        }

        Timer t;

        public TsuPopUpMenu subMenu;
        public TsuPopUpMenu(Control parent, string name)
        {
            this._Name = name;
            this.parent = parent;
            InitializeComponent();

            t = new Timer() { Interval = 300, Enabled = true };
            t.Tick += OnTick;
        }

        private void OnTick(object sender, EventArgs e)
        {
            // if the mouse is not around we hide
            bool mustHide = !MouseIsInOrInChild;
            // TSU.Debug.WriteInConsole($"{this} ticked and  tsupopup used ={mustHide} ");
            if (mustHide)
                Hide();
        }

        private Control parent;

        internal void Add(List<Control> l)
        {
            if (l == null) return;


            Rectangle TsunamiSize = TSU.Tsunami2.Preferences.Tsunami.View !=null ? TSU.Tsunami2.Preferences.Tsunami.View.Bounds : new Rectangle(100,100, 500,500);

            int existingTabIndex = this.Controls.Count;
            foreach (Control item in l)
            {
                if (item == null)
                    continue;

                existingTabIndex++;
                item.TabIndex = existingTabIndex;

                // check if the size is not bigger than Tsunami
                if (item.Bounds.Height > TsunamiSize.Height)
                    item.Size = new Size(item.Width / 2, item.Height / 2);

                item.Dock = DockStyle.Bottom;
                this.Controls.Add(item);

                if (item is BigButton bb)
                {
                    if (!bb.HasSubButtons)
                    {
                        bb.BigButtonPreClicked -= OnActionButtonClicked;
                        bb.BigButtonPreClicked += OnActionButtonClicked;
                    }
                }
            }
        }

        protected override void OnKeyUp(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) { this.Hide(); }
            base.OnKeyUp(e);
        }
        internal void Clear()
        {
            this.Controls.Clear();
            this.Refresh();
        }
        
        public override string ToString()
        {
            return $"PopupMenu: {this._Name}";
        }
        public override void SelectNextButton(BigButton current, bool forward)
        {
            Control actual = this.ActiveControl;
            List<BigButton> allButtons = BigButton.GetButtonsFrom(this);
            int numButtons = allButtons.Count;

            for (int i = 0; i < numButtons; i++)
            {
                if (allButtons[i] == actual)
                {
                    if (!forward)
                    {
                        if (i == 0)
                            i = numButtons - 1;
                        else
                            i--;
                    }
                    else
                    {
                        if (i == numButtons - 1)
                            i = 0;
                        else
                            i++;
                    }
                    allButtons[i].Focus();
                    break;
                }
            }
        }

        public override void SelectOtherView()
        {
            this.Hide();
        }

        private void Adapt()
        {
            // Maximal heigth
            int maxH = TSU.Tsunami2.Preferences.Values.WorkingArea.Height - 150;

            // Actual Heigth
            int actualH = 0;
            int count = 0;
            bool tooTall = false;
            int maxWidthFound = 0;
            foreach (Control item in this.Controls)
            {
                if (item is BigButton bb)
                {
                    while (!bb.IsFullyVisible && bb.Width < 5000)
                    {
                        bb.Width += 100;
                        this.Width += 100;
                    }

                    if (maxWidthFound < bb.Width)
                        if (bb.Width < 500)
                            maxWidthFound = bb.Width;
                        else maxWidthFound = 500;

                    if (actualH + bb.Height > maxH)
                    {
                        tooTall = true;
                        break;
                    }
                    else
                        actualH += bb.Height;
                    count += 1;
                }
                else if (item is PictureBox pb)
                {
                    actualH += pb.Image.Height;
                    if (maxWidthFound < pb.Image.Width)
                        maxWidthFound = pb.Image.Width;
                }
                else
                {
                    actualH += item.Height;
                    maxWidthFound = Math.Max(maxWidthFound, item.Width);
                }


            }
            actualH += this.Padding.Vertical;
            TSU.Debug.WriteInConsole(this.ToString() + " too tall:" + tooTall.ToString());
            this.Height = actualH;
            this.Width = maxWidthFound;

            int numberOfButtonToMove = tooTall? this.Controls.Count - count : 0;

            if (tooTall)
            {
                if (numberOfButtonToMove > 1) // becasue we dont want to hide one button in a submenu that also is a button
                    MoveControlsTotheMoreSubPopUp(numberOfButtonToMove, width: maxWidthFound);
                else
                    this.Height += TSU.Tsunami2.Preferences.Theme.ButtonHeight;
            }
        }

        public void ShowInPlace(int offsetX = 40, int offsetY = 0, bool showWithoutActivation = false)
        {
            t.Start();
            SuspendLayout();
            if (showWithoutActivation)
            {
                this.TopMost = false;
                //this.Enabled = false;
            }
            else
            {
                this.TopMost = true;
                //this.Enabled = true;
            }
            this.Visible = false;
            Debug.Timing("this.Adapt(");
            this.Adapt();

            

            Debug.Timing($"adapted");
            this.TopMost = true;
            Tsunami2.Properties.View.visiblePopUpMenu = this;
            //Ne montre pas le menu si rien dedans (carré noir)
            //Place();
            ResumeLayout(true);
            if (this.Controls.Count != 0)
                this.Show();
            this.Location = MousePosition;
            //this.Top = MousePosition.X + offsetX;
            //this.Left = MousePosition.Y + offsetY;
            Visible = true;
            MoveToStayFullyVisibleOnTheCurrentScreen();
        }

        private void MoveToStayFullyVisibleOnTheCurrentScreen()
        {
            while (!Screen.FromPoint(Cursor.Position).Bounds.Contains(this.Bounds.Left, this.Bounds.Bottom))
            {
                if (this.Top > 0)
                    this.Top -= 20;
                else
                    break;
            }
            while (!Screen.FromPoint(Cursor.Position).Bounds.Contains(this.Bounds.Right, this.Bounds.Top))
            {
                if (this.Left > 0)
                    this.Left -= 20;
                else
                    break;
            }
        }

        private void MoveControlsTotheMoreSubPopUp(int numberOfControlTOMove, bool multiColumns = true, int width = 200)
        {
            if (multiColumns)
            {
                SplitIn2Columns(width);
            }
            else
            {
                if (numberOfControlTOMove == 0)
                    return;
                TsuPopUpMenu SubPopup = new TsuPopUpMenu(this, "secondary ");
                BigButton more = new BigButton(R.BT_More, R.Add, TSU.Tsunami2.Preferences.Theme.Colors.Action, hasSubButtons: true);

                List<Control> controlsToGoToMoreMenu = new List<Control>();
                more.BigButtonClicked += delegate
                {
                    //initialViewShowingThePopup.ShowPopUpSubMenu(controlsToGoToMoreMenu, "secondary menu ");
                    ShowSubMenu(controlsToGoToMoreMenu, "secondary menu ");
                };

                // move control to the 'more' list
                int originalcount = this.Controls.Count;
                int startingPointToRemove = this.Controls.Count - numberOfControlTOMove;
                for (int i = 0; i < numberOfControlTOMove; i++)
                {
                    Control control = this.Controls[startingPointToRemove];
                    controlsToGoToMoreMenu.Add(control);
                    this.Controls.Remove(control);
                    //this.Height -= control.Height;
                }

                more.Dock = DockStyle.Bottom;
                this.Controls.Add(more);
                this.Height += more.Height;
            }
        }

        private void SplitIn2Columns(int width)
        {

            this.SuspendLayout();
            int itemsCount = this.Controls.Count;
            // Calculate rows needed for 2 columns
            int rows = (itemsCount + 1) / 2;
            bool odd = itemsCount % 2 != 0;
            List<Control> controls = new List<Control>();
            foreach (Control item in this.Controls)
            {
                controls.Add(item);
            }

            TableLayoutPanel tableLayoutPanel = new TableLayoutPanel
            {
                ColumnCount = 2,
                RowCount = rows,
                Dock = DockStyle.Fill,
                AutoSize = true,
                Padding = new Padding(2),
                //Margin = new Padding(0),
                CellBorderStyle = TableLayoutPanelCellBorderStyle.None,
                //BackColor = Color.BurlyWood,
            };
            tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            for (int i = 0; i < rows; i++)
            {
                var leftControl = controls[i];
                leftControl.Margin = new Padding(0);
                leftControl.Dock = DockStyle.Fill;
                tableLayoutPanel.Controls.Add(leftControl, 0, i);
                if (i == rows - 1 && odd)
                    continue;

                var rightControl = controls[rows+i];
                rightControl.Margin = new Padding(0);
                rightControl.Dock = DockStyle.Fill;
                tableLayoutPanel.Controls.Add(rightControl, 1, i);
            }
            this.Controls.Clear();
            this.Controls.Add(tableLayoutPanel);
            this.Height = tableLayoutPanel.PreferredSize.Height;
            this.Width = tableLayoutPanel.PreferredSize.Width;
            tableLayoutPanel.Height = this.Height;
            tableLayoutPanel.Width = this.Width;
            this.ResumeLayout(false);
        }


        // creata e and show a sub pop menu with the button in given in param
        internal void ShowSubMenu(List<Control> l, string name)
        {
            if (this.subMenu == null)
                this.subMenu = new TsuPopUpMenu(this,"");
            this.subMenu._Name =  name;
            subMenu.Clear();
            subMenu.Add(l);
            subMenu.ShowInPlace(10,10,true);
        }

        internal void ShowSubSubMenu(List<Control> l, string name)
        {
            var sub = this.subMenu;
            var subsub = sub.subMenu;
            if (subsub == null)
                subsub = new TsuPopUpMenu(sub, "");
            subsub._Name = name;
            subsub.Clear();
            subsub.Add(l);
            subsub.ShowInPlace(10, 10, true);
        }

        private void OnActionButtonClicked(object source, TsuObjectEventArgs e)
        {
            // button from a menu has been clcked to all menu should desappear
            this.Hide(hideAllParent : true);
        }

        public new void Hide(bool hideAllParent = false, bool hideAllChild = true)
        {
            t.Stop();
            foreach (Control item in this.Controls)
            {
                if (item is BigButton bb)
                {
                    bb.BigButtonPreClicked -= OnActionButtonClicked;
                }
            }
            if (hideAllChild && subMenu != null)
                this.subMenu.Hide();

            if (hideAllParent)
                HideParents();
            
            base.Hide();

            TSU.Debug.WriteInConsole(this.ToString() + " Hide");
        }

        private void HideParents()
        {
            var parent = this.parent;
            while (parent is TsuPopUpMenu parentMenu)
            {
                parentMenu.subMenu = null;
                parentMenu.Hide();
                parent = parent.Parent;
            }
        }
    }
}
