﻿using EmScon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TSU.Common;
using TSU.Common.Compute;
using TSU.Common.Compute.Compensations;
using TSU.Common.Elements;
using TSU.Common.Instruments.Reflector;
using TSU.ENUM;
using TSU.Preferences.GUI;
using TSU.Views.Message;
using C = TSU.Common.Compute.Compensations;
using CS = TSU.Common.Elements.Coordinates.CoordinateSystemsTsunamiTypes;
using E = TSU.Common.Elements;
using EC = TSU.Common.Elements.Composites;
using I = TSU.Common.Instruments;
using L = TSU.Logs;
using M = TSU.Common.Measures;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;
using SO = TSU.Polar.GuidedModules.Steps.StakeOut;
using T = TSU.Tools;
using Z = TSU.Common.Zones;

namespace TSU.Views
{
    public class TsuNode : TreeNode, ITsuObject
    {
        // Properties
        public string _Name { get; set; }
        public Guid Guid { get; set; }
        public ENUM.ElementType _fileElementType { get; set; }
        public ENUM.MeasureProperties _MeasureProperties { get; set; }
        public I.InstrumentClasses _InstrumentType { get; set; }
        public NodeDetailsType DetailsType;
        public E.Element element { get; set; }
        public ENUM.StationProperties _StationParameters { get; set; }
        public Common.Station _Station { get; set; }
        List<TsuObject> selectables;
        private double na { get; set; } = TSU.Tsunami2.Preferences.Values.na;
        public bool HasChildNodes
        {
            get
            {
                return (this.Nodes.Count > 0);
            }
        }

        [Flags]
        public enum NodeDetailsType
        {
            NameOnly,
            Detailed,
            Serialized,
            Special,
            Shorted,
            ShortedForMeasure_LocalCoordinates,
            ShortedForMeasure_VerticalizedDisplacement,
            ShortedForMeasure_Displacement
        }

        internal ENUM.StateType State;

        #region Constructor
        public TsuNode()
        {
            Guid = Guid.NewGuid();
        }

        public TsuNode(string name, object tag)
        {
            Guid = Guid.NewGuid();
            Name = name;
            Tag = tag;
            Text = name;
            if (tag is bool)
                Text += tag.ToString();
            this.NodeFont = TSU.Tsunami2.Preferences.Theme.Fonts.Normal;
        }
        public TsuNode(Object o, NodeDetailsType detailsType = NodeDetailsType.Detailed, List<TsuObject> selectables = null)
        {
            TsuObject to = o as TsuObject;
            try
            {
                this.selectables = selectables;
                Guid = new Guid();
                DetailsType = detailsType;
                if (o != null)
                {
                    dynamic d = o;
                    this.BasedOn(d);
                    ColorNodeBaseOnState(this);
                }
                _Name = Name;
            }
            catch (Exception ex)
            {
                string name = (to != null) ? to._Name : R.T_UNKNOWN_OBJECT;
                throw new Exception($"{R.T_IMPOSSIBLE_TO_CREATE_A_TREE_NODE_FOR} {name};\r\n{ex.Message}", ex);
            }
        }
        public override object Clone()
        {
            var obj = (TsuNode)base.Clone();
            obj.Guid = this.Guid;
            if (this.IsExpanded) obj.Expand();
            return obj;
        }
        /// <summary>
        /// Met à jour le TsuNode comme le referenceTsuNode
        /// </summary>
        /// <param name="referenceTsuNode"></param>
        public void UpdateTsuNode(TsuNode referenceTsuNode, bool orderAlsoNode = false)
        {

            this.BackColor = referenceTsuNode.BackColor;
            this.ForeColor = referenceTsuNode.ForeColor;
            this.ImageIndex = referenceTsuNode.ImageIndex;
            this.ImageKey = referenceTsuNode.ImageKey;
            this.Name = referenceTsuNode.Name;
            this.NodeFont = referenceTsuNode.NodeFont;
            this.SelectedImageIndex = referenceTsuNode.SelectedImageIndex;
            this.SelectedImageKey = referenceTsuNode.SelectedImageKey;
            this.StateImageIndex = referenceTsuNode.StateImageIndex;
            this.StateImageKey = referenceTsuNode.StateImageKey;
            this.Tag = referenceTsuNode.Tag;
            this.Text = referenceTsuNode.Text;
            this.ToolTipText = referenceTsuNode.ToolTipText;
            this.State = referenceTsuNode.State;
            this.StateImageIndex = referenceTsuNode.StateImageIndex;
            this.StateImageKey = referenceTsuNode.StateImageKey;
            List<TsuNode> TsuNodeToRemove = new List<TsuNode>();
            foreach (TsuNode item in this.Nodes)
            {
                if (referenceTsuNode.Nodes.ContainsKey(item.Name))
                {
                    item.UpdateTsuNode(referenceTsuNode.Nodes.Find(item.Name, false)[0] as TsuNode, orderAlsoNode);
                }
                else
                {
                    TsuNodeToRemove.Add(item);
                }
            }
            foreach (TsuNode item in TsuNodeToRemove)
            {
                this.Nodes.Remove(item);
            }
            int index = 0;
            foreach (TsuNode item in referenceTsuNode.Nodes)
            {
                if (!this.Nodes.ContainsKey(item.Name))
                {
                    TsuNode n = new TsuNode();
                    n.UpdateTsuNode(item, orderAlsoNode);
                    n.ExpandAll();
                    this.Nodes.Insert(index, n);
                }
                if (orderAlsoNode)
                {
                    // permet de mettre les noeuds dans le même ordre que le reference node,
                    // fait déplacer le vertical scroll du treeview
                    TsuNode findNode = this.Nodes.Find(item.Name, false)[0] as TsuNode;
                    this.Nodes.Remove(findNode);
                    this.Nodes.Insert(index, findNode);
                }

                index++;
            }
        }
        internal void ColorNodeBaseOnState(TsuNode n)
        {
            foreach (TsuNode item in n.Nodes)
            {
                ColorNodeBaseOnState(item);
            }
            switch (n.State)
            {
                case ENUM.StateType.Ready:
                    n.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Good;
                    n.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
                    break;
                case ENUM.StateType.NotReady:
                    if (TSU.Tsunami2.Preferences.Values.GuiPrefs.daltonien.IsTrue)
                    {
                        n.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
                        n.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
                    }
                    else
                    {
                        n.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
                        n.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
                    }
                    break;
                case ENUM.StateType.Acceptable:
                    n.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Attention;
                    n.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
                    break;
                case ENUM.StateType.ToBeFilled:
                    n.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
                    n.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Action;
                    break;
                default:
                    n.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
                    n.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
                    break;
            }
        }

        public TsuNode(CloneableList<M.Measure> o, NodeDetailsType detailsType = NodeDetailsType.Detailed)
        {
            DetailsType = detailsType;
            if (o != null)
            {
                dynamic d = o;
                this.BasedOn(d);
            }
        }
        public TsuNode(string name)
        {
            Name = name;
            Text = name;
        }
        public TsuNode(string name, ElementType fileElementType)
        {
            Name = name;
            Text = name;
            _fileElementType = fileElementType;
        }

        object lastObject = "ééé";

        public void BasedOn(Object o)
        {
            if (o == null) return;
            if (o == lastObject)
                throw new Exception("A TsuNode is being created BasedOn an unknown type which will create a stack overflow exception if we do not stop here...");
            dynamic d = o;
            lastObject = o;
            this.BasedOn(d);
        }
        #endregion

        #region static methods

        public static List<TsuNode> GetCheckedNodes(TreeNodeCollection nodes)
        {
            List<TsuNode> found = new List<TsuNode>();
            foreach (TsuNode aNode in nodes)
            {
                //edit
                if (aNode.Checked)
                    found.Add(aNode);

                if (aNode.Nodes.Count != 0)
                    found.AddRange(GetCheckedNodes(aNode.Nodes));
            }
            return found;
        }

        public static IEnumerable<TsuNode> EnumerateAllNodes(TreeNodeCollection nodes)
        {
            foreach (TsuNode aNode in nodes)
            {
                yield return aNode;
                foreach (TsuNode ret in EnumerateAllNodes(aNode.Nodes))
                    yield return ret;
            }
        }

        #endregion


        public void AssignImageIndexBasedOnFileElementype()
        {
            switch (_fileElementType)
            {

                default:
                    break;
            }
            SelectedImageIndex = ImageIndex;
        }

        #region Expand/Collapse

        public void ExpandToPoint()
        {
            if (_fileElementType != ElementType.Point)
            {
                Expand();
                foreach (TsuNode item in Nodes)
                {
                    item.ExpandToPoint();
                }
            }
            else
            {
                Collapse();
                foreach (TsuNode item in Nodes)
                {
                    item.CollapseChilds();
                }
            }
        }
        public void CollapseChilds()
        {
            foreach (TsuNode item in Nodes)
            {
                item.CollapseChilds();
            }
        }

        #endregion

        #region TreeState
        ///// <summary>
        ///// Sauvegarde de l'état du treeview parameter
        ///// /// <param name="nodeNameSelected"></param>
        ///// </summary>
        //public static Dictionary<string, bool> SaveTreeExpandedState(System.Windows.Forms.TreeView tree, ref string nodeNameSelected)
        //{
        //    Dictionary<string, bool> nodeStates = new Dictionary<string, bool>();
        //    for (int i = 0; i < tree.Nodes.Count; i++)
        //    {
        //        if (tree.Nodes.Count > 0)
        //        {
        //            nodeStates.Add(tree.Nodes[i].Name, tree.Nodes[i].IsExpanded);
        //        }
        //        for (int j = 0; j < tree.Nodes[i].Nodes.Count; j++)
        //        {
        //            if (tree.Nodes[i].Nodes.Count > 0)
        //            {
        //                nodeStates.Add(tree.Nodes[i].Nodes[j].Name, tree.Nodes[i].Nodes[j].IsExpanded);
        //            }
        //            for (int k = 0; k < tree.Nodes[i].Nodes[j].Nodes.Count; k++)
        //            {
        //                if (tree.Nodes[i].Nodes[j].Nodes.Count > 0)
        //                {
        //                    nodeStates.Add(tree.Nodes[i].Nodes[j].Nodes[k].Name, tree.Nodes[i].Nodes[j].Nodes[k].IsExpanded);
        //                }
        //            }
        //        }
        //    }
        //    if (tree.SelectedNode != null) nodeNameSelected = tree.SelectedNode.Name;
        //    return nodeStates;
        //}
        //public static Dictionary<string, bool> SaveTreeVisibleState(System.Windows.Forms.TreeView tree, ref string nodeNameSelected)
        //{
        //    Dictionary<string, bool> nodeStates = new Dictionary<string, bool>();
        //    for (int i = 0; i < tree.Nodes.Count; i++)
        //    {
        //        if (tree.Nodes.Count > 0)
        //        {
        //            nodeStates.Add(tree.Nodes[i].Name, tree.Nodes[i].IsVisible);
        //        }
        //        for (int j = 0; j < tree.Nodes[i].Nodes.Count; j++)
        //        {
        //            if (tree.Nodes[i].Nodes.Count > 0)
        //            {
        //                nodeStates.Add(tree.Nodes[i].Nodes[j].Name, tree.Nodes[i].Nodes[j].IsVisible);
        //            }
        //            for (int k = 0; k < tree.Nodes[i].Nodes[j].Nodes.Count; k++)
        //            {
        //                if (tree.Nodes[i].Nodes[j].Nodes.Count > 0)
        //                {
        //                    nodeStates.Add(tree.Nodes[i].Nodes[j].Nodes[k].Name, tree.Nodes[i].Nodes[j].Nodes[k].IsVisible);
        //                }
        //            }
        //        }
        //    }
        //    if (tree.SelectedNode != null) nodeNameSelected = tree.SelectedNode.Name;
        //    return nodeStates;
        //}
        ///// <summary>
        ///// Restore l'état du treeview parameter
        ///// </summary>
        ///// <param name="tree"></param>
        ///// <param name="treeState"></param>
        ///// <param name="nodeNameSelected"></param>
        //public static void RestoreTreeExpandedState(System.Windows.Forms.TreeView tree, Dictionary<string, bool> treeState, string nodeNameSelected)
        //{
        //    TreeNode selectedNode = new TreeNode();
        //    for (int i = 0; i < tree.Nodes.Count; i++)
        //    {
        //        if (treeState.ContainsKey(tree.Nodes[i].Name))
        //        {
        //            if (treeState[tree.Nodes[i].Name])
        //                tree.Nodes[i].Expand();
        //            else
        //                tree.Nodes[i].Collapse();
        //        }
        //        else
        //        {
        //            //Nouveau noeud à afficher
        //            tree.Nodes[i].Expand();
        //        }
        //        if (nodeNameSelected == tree.Nodes[i].Name)
        //        {
        //            tree.SelectedNode = tree.Nodes[i];
        //        }
        //        for (int j = 0; j < tree.Nodes[i].Nodes.Count; j++)
        //        {
        //            if (treeState.ContainsKey(tree.Nodes[i].Nodes[j].Name))
        //            {
        //                if (treeState[tree.Nodes[i].Nodes[j].Name])
        //                    tree.Nodes[i].Nodes[j].Expand();
        //                else
        //                    tree.Nodes[i].Nodes[j].Collapse();
        //            }
        //            else
        //            {
        //                //Nouveau noeud à afficher
        //                tree.Nodes[i].Nodes[j].Expand();
        //            }
        //            if (nodeNameSelected == tree.Nodes[i].Nodes[j].Name)
        //            {
        //                tree.SelectedNode = tree.Nodes[i].Nodes[j];
        //            }
        //            for (int k = 0; k < tree.Nodes[i].Nodes[j].Nodes.Count; k++)
        //            {
        //                if (treeState.ContainsKey(tree.Nodes[i].Nodes[j].Nodes[k].Name))
        //                {
        //                    if (treeState[tree.Nodes[i].Nodes[j].Nodes[k].Name])
        //                        tree.Nodes[i].Nodes[j].Nodes[k].Expand();
        //                    else
        //                        tree.Nodes[i].Nodes[j].Nodes[k].Collapse();
        //                }
        //                else
        //                {
        //                    tree.Nodes[i].Nodes[j].Nodes[k].Expand();
        //                }
        //                if (tree.Nodes[i].Nodes[j].Nodes[k].Name == nodeNameSelected)
        //                {
        //                    tree.SelectedNode = tree.Nodes[i].Nodes[j].Nodes[k];
        //                }
        //            }
        //        }
        //    }
        //    tree.TopNode = selectedNode;
        //    selectedNode.EnsureVisible();
        //    tree.Refresh();

        //}
        ///// <summary>
        ///// Restore the treeview visible state
        ///// </summary>
        ///// <param name="tree"></param>
        ///// <param name="treeState"></param>
        ///// <param name="nodeNameSelected"></param>
        //public static void RestoreTreeVisibleState(System.Windows.Forms.TreeView tree, Dictionary<string, bool> treeState, string nodeNameSelected)
        //{
        //    TreeNode selectedNode = new TreeNode();
        //    for (int i = 0; i < tree.Nodes.Count; i++)
        //    {
        //        if (treeState.ContainsKey(tree.Nodes[i].Name))
        //        {
        //            if (treeState[tree.Nodes[i].Name])
        //                tree.Nodes[i].EnsureVisible();
        //        }
        //        else
        //        {
        //            tree.Nodes[i].EnsureVisible();
        //        }
        //        if (nodeNameSelected == tree.Nodes[i].Name)
        //        {
        //            tree.SelectedNode = tree.Nodes[i];
        //            selectedNode = tree.Nodes[i];
        //        }
        //        for (int j = 0; j < tree.Nodes[i].Nodes.Count; j++)
        //        {
        //            if (treeState.ContainsKey(tree.Nodes[i].Nodes[j].Name))
        //            {
        //                if (treeState[tree.Nodes[i].Nodes[j].Name])
        //                    tree.Nodes[i].Nodes[j].EnsureVisible();
        //            }
        //            else
        //            {
        //                tree.Nodes[i].Nodes[j].EnsureVisible();
        //            }
        //            if (nodeNameSelected == tree.Nodes[i].Nodes[j].Name)
        //            {
        //                tree.SelectedNode = tree.Nodes[i].Nodes[j];
        //                selectedNode = tree.Nodes[i].Nodes[j];
        //            }
        //            for (int k = 0; k < tree.Nodes[i].Nodes[j].Nodes.Count ; k++)
        //            {
        //                if (treeState.ContainsKey(tree.Nodes[i].Nodes[j].Nodes[k].Name))
        //                {
        //                    if (treeState[tree.Nodes[i].Nodes[j].Nodes[k].Name])
        //                        tree.Nodes[i].Nodes[j].Nodes[k].EnsureVisible();
        //                }
        //                else
        //                {
        //                    tree.Nodes[i].Nodes[j].Nodes[k].EnsureVisible();
        //                }
        //                if (tree.Nodes[i].Nodes[j].Nodes[k].Name == nodeNameSelected)
        //                {
        //                    tree.SelectedNode = tree.Nodes[i].Nodes[j].Nodes[k];
        //                    selectedNode = tree.Nodes[i].Nodes[j].Nodes[k];
        //                }
        //            }
        //        }
        //    }
        //    tree.TopNode = selectedNode;
        //    selectedNode.EnsureVisible();
        //    tree.Refresh();
        //}
        #endregion

        #region Points
        // Methods
        public EC.CompositeElement GetPointsFromCheckedNodes(bool parentChecked)
        {
            EC.CompositeElement list = new EC.CompositeElement();
            bool isChecked = (this.Checked || parentChecked);
            if (isChecked) // if selected himself or from over him we have to get all points under this node
            {
                if (this._fileElementType == ElementType.SequenceTH)
                {
                    list.Add(this.Tag as E.Element);
                }
                if (this._fileElementType == ElementType.Point)
                {
                    list.Add(this.Tag as E.Element);
                }
                else if (this.HasChildNodes)
                {
                    foreach (TsuNode node in this.Nodes)
                    {
                        list.AddRange(node.GetPointsFromCheckedNodes(isChecked).Elements);
                    }
                }
                return list;
            }
            else // if not selected we have to llok for check node under this one
            {
                if (this._fileElementType != ElementType.Point && this._fileElementType != ElementType.SequenceTH)
                {
                    foreach (TsuNode node in this.Nodes)
                    {
                        list.AddRange(node.GetPointsFromCheckedNodes(false).Elements);
                    }
                }
                return list;
            }
        }
        public void GetPrimitiveElements(List<E.IElement> primitiveElements)
        {
            if (_fileElementType == ElementType.Point)
            {
                primitiveElements.Add(element);
            }
            else
            {
                foreach (TsuNode item in Nodes)
                {
                    item.GetPrimitiveElements(primitiveElements);
                }
            }
        }
        #endregion

        #region Build Node

        #region Dressing
        public void DressAs(ElementType p, string additionalText = "", Object o = null)
        {
            DressAs(p.ToString(), additionalText, o);
            _fileElementType = p;
        }
        public void DressAs(StationProperties p, string additionalText, Object o)
        {
            this.DressAs(p.ToString(), additionalText, o);
            this._StationParameters = p;
        }
        public void DressAs(I.InstrumentClasses t, string additionalText, Object o)
        {
            DressAs(t.ToString(), additionalText, o);
            _InstrumentType = t;
        }
        public void DressAs(MeasureProperties p, string additionalText, Object o)
        {
            DressAs(p.ToString(), additionalText, o);
            _MeasureProperties = p;
        }
        public void DressAs(string PropertiesAsString, string additionalPreText, Object o, string additionalPostText = "")
        {
            if (o != null)
            {
                this.Tag = o;
            }
            this.Text = PropertiesAsString;
            if (additionalPreText != "") this.Text += " " + additionalPreText;
            this.Name = this.Text;
            this.ImageKey = PropertiesAsString;
            this.SelectedImageKey = PropertiesAsString;
        }
        public void DressAs(string PropertiesAsString)
        {
            this.Name = PropertiesAsString;
            this.Text = PropertiesAsString + " ";
            this.ImageKey = PropertiesAsString;
            this.SelectedImageKey = PropertiesAsString;
        }
        #endregion

        #region AddSubNode
        private void AddNodeAs(StationProperties p, string additionalText, Object o, string additionalPostText = "")
        {
            TsuNode n = new TsuNode();
            n.DressAs(p, "", o);
            n.Tag = o;
            n.Text = p.ToString() + ": " + additionalText + additionalPostText;
            n.State = ((additionalText == "-999") ||
                (additionalText == R.String_Unknown) ||
                (additionalText == "UnknownTeam") ||
                (additionalText == "Not Available") ||
                (additionalText == "9999")) ?
                ENUM.StateType.Acceptable : n.State;
            n.State =
                ((additionalText == "UnknownFace") ||
                (additionalText == "UnknownFace")) ?
                ENUM.StateType.NotReady : n.State;
            if (p == StationProperties.NextReflector && additionalText.ToUpper() == "UNKNOWN")
                n.State = ENUM.StateType.NotReady;
            this.Nodes.Add(n);
        }
        private void AddNodeAs(StationProperties p, string additionalPreText, ref Object o, string additionalPostText = "")
        {
            TsuNode n = new TsuNode();
            n.DressAs(p, additionalPreText, o);
            n.Tag = o;
            n.Text = p.ToString() + ": " + additionalPreText;
            n.State = ((additionalPreText == "-999") || (additionalPreText == R.String_Unknown) || (additionalPreText == "UnknownTeam") || (additionalPreText == "Not Available")) ? ENUM.StateType.Acceptable : ENUM.StateType.Normal;
            this.Nodes.Add(n);
        }
        private void AddNodeAs(MeasureProperties p, string additionalPreText, Object o)
        {
            TsuNode n = new TsuNode();
            n.DressAs(p, additionalPreText, o);
            this.Nodes.Add(n);
        }
        private void AddNodeAs(ElementType p, string additionalPreText, double d, string additionalPostText = "")
        {
            TsuNode n = new TsuNode();

            n.DressAs(p, additionalPreText, null);
            n.Text += " " + d.ToString() + additionalPostText;
            n.Name = n.Text;
            this.Nodes.Add(n);
        }
        private TsuNode AddNode(Object o, string text = null, NodeDetailsType type = NodeDetailsType.Detailed)
        {
            if (o == null) return null;
            TsuNode n = new TsuNode(o, type);
            if (text != null)
            {
                n.Text = text + ": " + n.Text;
                n.ImageKey = text;
                n.SelectedImageKey = text;
            }
            this.Nodes.Add(n);
            return n;
        }

        private void AddNode(DoubleValue o, MeasureProperties p, string additionalPreText, int precision, string additionalPostText)
        {
            TsuNode n = new TsuNode();
            n.Tag = o;
            n.DressAs(p, additionalPreText, o);
            n.Text += T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(o, precision) + additionalPostText;
            n.Name = this.Text;
            this.Nodes.Add(n);
        }

        private void AddNodeAs(bool useDistanceForLGC2)
        {
            //ajout de l'option calcul LGC2 avec DLEV
            TsuNode n = new TsuNode();
            if (useDistanceForLGC2 == true)
            {
                n.ImageKey = "Checked";
                n.SelectedImageKey = "Checked";
            }
            else
            {
                n.ImageKey = "NotChecked";
                n.SelectedImageKey = "NotChecked";
            }
            n.Name = "Admin_ShowLGC_useDistance";

            n.State = ENUM.StateType.Normal;
            this.ColorNodeBaseOnState(n);
            n.Text = R.StringTsuNode_Level_UseDlev;
            this.Nodes.Add(n);
        }

        private void AddNodeAs(TsuBool anyBool, string name)
        {
            TsuNode n = new TsuNode();
            if (anyBool == null)
            {
                n.ImageKey = "StatusBad";
                n.SelectedImageKey = "StatusBad";
                n.State = StateType.ToBeFilled;
                this.Nodes.Add(n);
            }
            else
            {
                if (anyBool.IsTrue)
                {
                    n.ImageKey = "Checked";
                    n.SelectedImageKey = "Checked";
                }
                else
                {
                    n.ImageKey = "NotChecked";
                    n.SelectedImageKey = "NotChecked";
                }
                n.Tag = anyBool;
                n.State = ENUM.StateType.Normal;
            }
            n.Name = name;
            this.ColorNodeBaseOnState(n);
            n.Text = name;
            this.Nodes.Add(n);
        }


        private void AddNodeAs(TSU.ENUM.CoordinatesType coordinatesType)
        //Ajout Noeud pour sélection type coordonnées à utiliser pour calcul
        {
            TsuNode n = new TsuNode()
            {
                ImageKey = "CoordType",
                SelectedImageKey = "CoordType",
                Name = "Admin_CoordType_",
                State = ENUM.StateType.Normal
            };
            ColorNodeBaseOnState(n);

            switch (coordinatesType)
            {
                case CoordinatesType.CCS:
                    n.Text = R.StringTsuNode_Line_CoordTypeCCS;
                    break;
                case CoordinatesType.SU:
                    n.Text = R.StringTsuNode_Line_CoordTypeLocal;
                    break;
                case CoordinatesType.UserDefined:
                    n.Text = R.StringTsuNode_Line_CoordTypeUserDef;
                    break;
                default:
                    n.Text = R.StringTsuNode_Line_CoordTypeCCS;
                    break;
            }
            this.Nodes.Add(n);
        }
        private void AddNodeAs(double tolerance)
        //ajoude un node avec la tolérance d'alignement
        {
            TsuNode n = new TsuNode()
            {
                Name = "tolerance",
                Text = "Tolerance : " + Math.Round(tolerance * 1000, 2).ToString() + " mm",
                State = ENUM.StateType.Normal
            };
            ColorNodeBaseOnState(n);
            n.ImageKey = "Tolerance";
            n.SelectedImageKey = "Tolerance";

            this.Nodes.Add(n);
        }
        private void AddNodeAs(string textName, string additionalText, Object o, ENUM.StateType state = ENUM.StateType.Unknown)
        {
            TsuNode n = new TsuNode();
            n.DressAs(textName, additionalText, o);
            n.Tag = o;
            //n.Text = textName;
            if (state == ENUM.StateType.Unknown)
            {
                n.State = ((additionalText == "-999") || (additionalText == R.String_Unknown) || (additionalText == R.String_UnknownTeam) || (additionalText == R.T_NA) || (additionalText == "0")) ? ENUM.StateType.ToBeFilled : ENUM.StateType.Normal;
            }
            else
            {
                n.State = state;
            }
            ColorNodeBaseOnState(n);
            this.Nodes.Add(n);
        }
        //
        // Visitor pattern
        //

        #endregion

        #region Modules

        // -Manager
        public void BasedOn(Manager m)
        {
            this.DressAs("Module");
            this.Tag = m;
            this.Text = m._Name + "(" + m.Guid + ")";
            if (m.AllElements != null)
                foreach (var child in m.AllElements)
                    this.Nodes.Add(new TsuNode(child));
        }


        // -Module
        public void BasedOn(Module m)
        {
            this.DressAs(m._Name);
            this.Tag = m;
            this.Text = m._Name;
            this.ImageKey = "Module";
            this.SelectedImageKey = "Module";
            foreach (IModule entry in m.childModules)
            {
                this.Nodes.Add(new TsuNode(entry as Module));
            }
        }

        // -FinalModule
        public void BasedOn(Common.FinalModule fm)
        {
            this.DressAs(fm._Name);
            this.Tag = fm;
            this.Text = $"{fm._Name} ({fm.Guid})";
            this.ImageKey = "Module";
            this.SelectedImageKey = "Module";
            this.Nodes.Add(new TsuNode(fm.DsaFlags));
            this.Nodes.Add(new TsuNode(fm._ElementManager as TSU.Manager));
            this.Nodes.Add(new TsuNode(fm.ZoneManager as TSU.Manager));
            foreach (IModule entry in fm.childModules)
            {
                this.Nodes.Add(new TsuNode(entry));
            }
        }

        // -Guided.Group.Module
        public void BasedOn(Common.Guided.Group.Module m)
        {
            BasedOn(m as FinalModule);
            this.DressAs(m._Name);
            this.Tag = m;
            this.Text = m._Name;
            this.ImageKey = "Module";
            this.SelectedImageKey = "Module";
            foreach (IModule entry in m.SubGuidedModules)
            {
                this.Nodes.Add(new TsuNode(entry as Common.Guided.Module));
            }
        }

        // -Guided.module
        public void BasedOn(Common.Guided.Module m)
        {
            this.BasedOn(m as FinalModule);
        }

        // -station.module
        public void BasedOn(Station.Module m)
        {
            this.DressAs(m._Name);
            this.Tag = m;
            this.Text = $"{m._Name} ({m.Guid})";
            this.ImageKey = MeasureProperties.Theodolite.ToString();
            this.SelectedImageKey = MeasureProperties.Theodolite.ToString();

            this.Nodes.Add(new TsuNode(m._Station.ParametersBasic));
            this.Nodes.Add(new TsuNode(m._MeasureManager));

            if (m._InstrumentManager != null)
            {
                this.Nodes.Add(new TsuNode(m._InstrumentManager as I.Manager.Module));
                if (m._InstrumentManager.SelectedInstrumentModule != null)
                {
                    this.Nodes.Add(new TsuNode(m._InstrumentManager.SelectedInstrumentModule));
                }
            }
        }

        // -Polar.station.module.Flags
        public void BasedOn(DsaFlag dsaFlag)
        {
            switch (dsaFlag.State)
            {
                case DsaOptions.Always:

                    this.ImageKey = "StatusGood";
                    this.SelectedImageKey = "StatusGood";
                    break;
                case DsaOptions.Never:
                    this.ImageKey = "StatusBad";
                    this.SelectedImageKey = "StatusBad";
                    break;
                case DsaOptions.Ask_to:
                case DsaOptions.Ask_to_with_pre_checked_dont_show_again:
                default:
                    this.ImageKey = "StatusQuestionable";
                    this.SelectedImageKey = "StatusQuestionable";
                    break;
            }

            this.Text = dsaFlag.ToString();
            this.Tag = dsaFlag;
        }

        public void BasedOn(List<DsaFlag> dsaFlags)
        {
            if (dsaFlags != null)
            {
                this.Tag = dsaFlags;
                this.Text = $"Visibility Option";
                this.ImageKey = MeasureProperties.Mode.ToString();
                this.SelectedImageKey = MeasureProperties.Mode.ToString();

                foreach (DsaFlag dsaFlag in dsaFlags)
                {
                    this.Nodes.Add(new TsuNode(dsaFlag));
                }
            }
        }

        // -Polar.station.module
        public void BasedOn(TSU.Polar.Station.Module m)
        {
            BasedOn(m as Station.Module);
        }


        // -Measure.Manager.Module
        public void BasedOn(M.Manager m)
        {

            StationProperties p;
            if (m.AllElements != null)
            {
                p = StationProperties.Measures;
                this.DressAs(p, "", m.AllElements);
                this.Text = m.AllElements.Count + " Measured";
                if (m.AllElements.Count > 0)
                {
                    this.Text += $" ({m.AllElements[m.AllElements.Count - 1]._Name})";
                }


                bool isShorted = DetailsType == NodeDetailsType.Shorted;

                if (DetailsType == NodeDetailsType.Shorted)
                    DetailsType = NodeDetailsType.NameOnly;

                if (isShorted && m.AllElements.Count > 5)
                {
                    TsuNode olders = new TsuNode("Others (older)");
                    this.Nodes.Add(olders);
                    int count = 1;
                    for (int i = 0; i < m.AllElements.Count; i++)
                    {
                        TsuNode n = new TsuNode();
                        if (m.AllElements[i] is Polar.Measure)
                            n.BasedOn(m.AllElements[i] as Polar.Measure, DetailsType);
                        else
                            n.BasedOn(m.AllElements[i]);
                        n.Text = count.ToString() + ": " + n.Text;
                        count++;
                        if (i > m.AllElements.Count - 5)
                            this.Nodes.Insert(0, n);
                        else
                            olders.Nodes.Insert(0, n);
                    }
                }
                else
                {
                    for (int i = 0; i < m.AllElements.Count; i++)
                    {
                        // Add measures
                        //if ((m.AllElements[i] as Measure)._Status is MeasureStateGood)
                        {
                            TsuNode n = new TsuNode();
                            if (m.AllElements[i] is Polar.Measure)
                                n.BasedOn(m.AllElements[i] as Polar.Measure, DetailsType);
                            else
                                n.BasedOn(m.AllElements[i]);
                            this.Nodes.Insert(0, n);
                        }
                    }
                }
            }
        }


        // -Measure.Manager.Module // Measured list from Adv Polar
        public void BasedOn(M.Manager m, string typeS, SO.DisplayTypes typeD)
        {
            try
            {
                StationProperties p;

                if (m.AllElements != null)
                {
                    p = StationProperties.Measures;
                    this.DressAs(p, "", m.AllElements);
                    this.Text = $"{m._Name} ({m.Guid})";
                    bool rabotUsed = (m.FinalModule != null && !(m.FinalModule.RabotStrategy == Common.Compute.Rabot.Strategy.Not_Defined || typeS != "BEAMV"));
                    string chosenStrategy = !rabotUsed ? string.Empty : " (Curve smooth alignment strategy " + m.FinalModule.RabotStrategy + ")";
                    this.Text = $"Measured ({typeD} in {typeS} CS" + chosenStrategy +")";


                    TsuNode olders = new TsuNode("Others (olders)");
                    this.Nodes.Add(olders);
                    int count = 1;
                    for (int i = 0; i < m.AllElements.Count; i++)
                    {
                        // create a measurement node based on selection of details

                        TsuNode n = new TsuNode()
                        {
                            NodeFont = TSU.Tsunami2.Preferences.Theme.Fonts.Normal,
                            ImageKey = StationProperties.Measures.ToString(),
                            _StationParameters = StationProperties.Measure,
                            SelectedImageKey = StationProperties.Measures.ToString()
                        };
                        Polar.Measure mt = m.AllElements[i] as Polar.Measure;
                        n.BasedOn(mt);
                        n.Tag = mt;

                        E.Coordinates c = SO.Tools.GetRightCoordinates(typeS, typeD, out string sType)(mt);

                        if (mt._Point == null)
                            throw new Exception($"point {mt.PointGUID} not found");

                        string additionalPrefix = mt._Status is M.States.Control ? "CTRL on " : "";

                        string additionalInfo = c.AreKnown ? c.ToString("mm") : "";

                        n.Text = $"{count}: {additionalPrefix}{mt._Point._Name,-30} {additionalInfo}";

                        count++;

                        // put in subfolder if older than 5
                        if (i > m.AllElements.Count - 5)
                            this.Nodes.Insert(0, n);
                        else
                            olders.Nodes.Insert(0, n);
                    }
                }
                ColorNodeBaseOnState(this);
            }
            catch (Exception ex)
            {
                string titleAndMessage = $"Problem with Creating the treeview node;Of the next points list: {ex}";
                new MessageInput(MessageType.Warning, titleAndMessage).Show();
            }
        }

        // -Element.Manager.Module
        public void BasedOn(E.Manager.Module m)
        {
            this.Tag = m;
            this.Text = $"{m._Name} ({m.Guid})";
            this.ImageKey = ElementType.Class.ToString();
            this.SelectedImageKey = ElementType.Class.ToString();
            foreach (E.Element entry in m.AllElements)
            {
                this.Nodes.Add(new TsuNode(entry));
            }
        }

        // -Instruments.Manager.Module
        public void BasedOn(I.Manager.Module m)
        {
            this.DressAs(StationProperties.Instrument, "", m);
            this.Text = $"{m._Name} ({m.Guid})";

            if (m.SelectedInstrument != null) this.AddNode(m.SelectedInstrument);
        }

        // -Instruments.Module
        public void BasedOn(I.Module m)
        {
            this.DressAs(StationProperties.Instrument, m.Instrument._Name, m);
            this.Text = $"{m._Name} ({m.Guid})";
            this.AddNode(m.InstrumentIsOn, "Is ON");
            this.AddNode(m.ToBeMeasuredData);
        }

        public void BasedOn(M.Extension e)
        {
            this.Tag = e;
            this.Text = $"Ext = {e}";
            this.ImageKey = StationProperties.Extension.ToString();
            this.SelectedImageKey = ImageKey;
        }




        public void BasedOn(Tools.Research.MeasureCount mc)
        {
            this.Tag = mc;
            this.Text = mc.point._Name;
            this.Nodes.Add(new TsuNode("Number of good measure: " + mc.countGood.ToString()));
            this.Nodes.Add(new TsuNode("Number of Questionnable measure: " + mc.countQuestionnable.ToString()));
            this.Nodes.Add(new TsuNode("Number of Bad measure: " + mc.countBad.ToString()));
            this.Nodes.Add(new TsuNode("Number of Control measure: " + mc.countControl.ToString()));

            switch (mc.countGood)
            {
                case 0: this.State = ENUM.StateType.NotReady; break;
                case 1: this.State = ENUM.StateType.Acceptable; break;
                case 2: this.State = ENUM.StateType.Normal; break;
                default: this.State = ENUM.StateType.Ready; break;
            }

        }

        #endregion

        #region Others

        public void BasedOn(CompensationItem item)
        {
            DressAs(MeasureProperties.MeasureState.ToString());
            Text = item.Measure._PointName;
        }

        public void BasedOn(TSU.Common.Dependencies.Application e)
        {
            bool showPaths = false;
            bool showVersion = false;
            bool showDependencies = false;
            bool showDetailedDependencies = false;
            //bool skipInstallerPath = false;
            //string dependenciesNodeName;

            switch (this.DetailsType)
            {
                case NodeDetailsType.Detailed: showPaths = true; showVersion = true; showDetailedDependencies = true; break;
                case NodeDetailsType.Serialized: showPaths = true; showVersion = true; ; break;
                case NodeDetailsType.Special: showPaths = true; showVersion = true; showDependencies = true; break; //skipInstallerPath = true;
                case NodeDetailsType.Shorted: showVersion = true; showDependencies = true; break;
                case NodeDetailsType.NameOnly:
                default: break;
            }

            if (e._Name == "SU sotfware already installed")
            {
                showPaths = false; showVersion = false; showDependencies = true; showDetailedDependencies = false; //dependenciesNodeName = "Program Files";
            }

            this.Tag = e;
            this.Text = e._Name;
            if (showVersion) this.Text += " v " + e._VersionNumbers.Full;
            this.Name = this.Text;
            this.ImageKey = "SUSoft";

            if (showPaths)
            {
                if (e.StoredApplicationPath != "")
                {
                    this.AddNode(e.ApplicationFileInfo);
                    this.LastNode.Text = "Application path: " + this.LastNode.Text;

                }
                if (e.StoredInstallerPath != "")
                {


                    TsuNode n = new TsuNode(e.InstallerFileInfo);
                    if (n.State == ENUM.StateType.NotReady) n.State = ENUM.StateType.Acceptable; // having the installer not available is acceptable, the parent node will be red because of application path missing anyway
                    n.Text = "Installer path: " + n.Text;
                    this.Nodes.Add(n);
                }
            }

            if (showDependencies || showDetailedDependencies)
            {
                if (e.Dependencies.Count > 0)
                {
                    this.ImageKey = "ComposedApp";
                    //TsuNode dep = new TsuNode(dependenciesNodeName);
                    //dep.ImageKey = "Dependencies";
                    //dep.SelectedImageKey = this.ImageKey;
                    if (!showDetailedDependencies)
                        foreach (TSU.Common.Dependencies.Application item in e.Dependencies)
                            this.AddNode(item, null, NodeDetailsType.Serialized);
                    else
                        foreach (TSU.Common.Dependencies.Application item in e.Dependencies)
                        {
                            if (this.DetailsType == NodeDetailsType.Special)
                                this.AddNode(item, null, NodeDetailsType.Shorted);
                            else
                                this.AddNode(item, null, NodeDetailsType.Special);
                        }
                    //this.Nodes.Add(dep);
                }
                else
                    this.ImageKey = "SingleApp";
            }


            this.SelectedImageKey = this.ImageKey;
            this.State = this.GetChildrenWorstState(this);
        }

        public void BasedOn(bool b)
        {
            this.Tag = b;
            this.Text = b.ToString();
            this.Name = this.Text;
            this.ImageKey = "";
            this.SelectedImageKey = this.ImageKey;
        }

        public void BasedOn(L.LogEntry e)
        {
            this.Tag = e;
            this.Text = e.sender._Name + " at " + e.date + ": " + e.entry;
            this.Name = this.Text;
            this.ImageKey = "";
            this.SelectedImageKey = this.ImageKey;
        }



        public void BasedOn(System.IO.FileInfo e)
        {
            this.Tag = e;
            this.Text = e.FullName;
            this.Name = this.Text;
            this.ImageKey = "File";
            this.SelectedImageKey = this.ImageKey;
            if (e.FullName.ToUpper().Contains("\\CERN.CH"))
            {
                if (TSU.Tsunami2.Preferences.Values.DfsAvailable)
                {
                    if (e.Exists)
                        this.State = ENUM.StateType.Ready;
                    else
                        this.State = ENUM.StateType.NotReady;
                }
                else
                    this.State = ENUM.StateType.NotReady;
            }
            else
            {
                if (e.Exists)
                    this.State = ENUM.StateType.Ready;
                else
                    this.State = ENUM.StateType.NotReady;
            }
        }

        public void BasedOn(Z.Zone z)
        {
            this.Tag = z;
            this.DressAs(ElementType.Accelerator, "", z);
            this.Text = z.ToString();
            this.Name = this.Text;
            this.Nodes.Add(new TsuNode() { Text = "Source: " + z.Source, ImageKey = "comment", SelectedImageKey = "comment" });
            this.Nodes.Add(new TsuNode() { Text = "Path Source: " + z.PathSource, ImageKey = "comment", SelectedImageKey = "comment" });
            this.Nodes.Add(new TsuNode() { Text = "Date: " + z.Date.ToShortDateString(), ImageKey = "Date", SelectedImageKey = "Date" });

            if (z.Ccs2MlaInfo != null)
            {
                TsuNode ccs2Mla = new TsuNode("CCS > MLA", ElementType.Rotations);
                this.Nodes.Add(ccs2Mla);

                TsuNode referecenSurface = new TsuNode()
                {
                    Text = "Reference surface is " + z.Ccs2MlaInfo.ReferenceSurface.ToString(),
                    Tag = z.Ccs2MlaInfo.ReferenceSurface
                };
                ccs2Mla.Nodes.Add(referecenSurface);


                if (z.Ccs2MlaInfo.Origin != null)
                {
                    TsuNode ccs = new TsuNode("CCS", ElementType.CoordinateSystem);
                    ccs.DressAs(ElementType.CoordinateSystem, "Origin in CCS", z.Ccs2MlaInfo.Origin);
                    ccs.AddNodeAs(ElementType.X, " = ", z.Ccs2MlaInfo.Origin.X.Value, " m");
                    ccs.AddNodeAs(ElementType.Y, " = ", z.Ccs2MlaInfo.Origin.Y.Value, " m");
                    ccs.AddNodeAs(ElementType.Z, " = ", z.Ccs2MlaInfo.Origin.Z.Value, " m");
                    ccs2Mla.Nodes.Add(ccs);
                }
                if (z.Ccs2MlaInfo.Bearing != null)
                    ccs2Mla.AddNodeAs(ElementType.Bearing, " = ", z.Ccs2MlaInfo.Bearing.Value, " gon");
                if (z.Ccs2MlaInfo.Slope != null)
                    ccs2Mla.AddNodeAs(ElementType.Slope, " = ", z.Ccs2MlaInfo.Slope.Value, " rad");
            }
            if (z.Mla2SuInGon != null)
            {
                TsuNode mla2SuInGon = new TsuNode("Rots", ElementType.Rotations);
                mla2SuInGon.DressAs(ElementType.Rotations, "between Mla and Su", z.Mla2SuInGon);
                mla2SuInGon.AddNodeAs(ElementType.X, " = ", z.Mla2SuInGon.RotX, " gon");
                mla2SuInGon.AddNodeAs(ElementType.Y, " = ", z.Mla2SuInGon.RotY, " gon");
                mla2SuInGon.AddNodeAs(ElementType.Z, " = ", z.Mla2SuInGon.RotZ, " gon");
                this.Nodes.Add(mla2SuInGon);
            }
            if (z.Su2PhysInGon != null)
            {
                TsuNode su2PhysInGon = new TsuNode("Rots", ElementType.Rotations);
                su2PhysInGon.DressAs(ElementType.Rotations, "between SU and Phys", z.Su2PhysInGon);
                su2PhysInGon.AddNodeAs(ElementType.X, " = ", z.Su2PhysInGon.RotX, " gon");
                su2PhysInGon.AddNodeAs(ElementType.Y, " = ", z.Su2PhysInGon.RotY, " gon");
                su2PhysInGon.AddNodeAs(ElementType.Z, " = ", z.Su2PhysInGon.RotZ, " gon");
                this.Nodes.Add(su2PhysInGon);
            }
            if (z.Su2PhysInM != null)
            {
                TsuNode su2PhysInM = new TsuNode("Trans", ElementType.Translations);
                su2PhysInM.DressAs(ElementType.Translations, "between SU and Phys", z.Su2PhysInM);
                su2PhysInM.AddNodeAs(ElementType.X, " = ", z.Su2PhysInM.AlongX, " m");
                su2PhysInM.AddNodeAs(ElementType.Y, " = ", z.Su2PhysInM.AlongY, " m");
                su2PhysInM.AddNodeAs(ElementType.Z, " = ", z.Su2PhysInM.AlongZ, " m");
                this.Nodes.Add(su2PhysInM);
            }
        }

        public void BasedOn(DoubleValue v, int numberOfDecimals = -1)
        {
            if (numberOfDecimals == -1)
            {
                this.Text = v.Value.ToString();
                if (v.Sigma != na)
                    this.Text += " (" + v.Sigma.ToString() + ")";
            }
            else
                this.Text = v.ToStringInJcgm200Way(numberOfDecimals: numberOfDecimals);


            this.Name = this.Text;
        }

        public void BasedOn(M.DistanceHorizontal d)
        {
            Text = "Horizontal distance = " + d.ToString("m-m") + " m";

            this.Name = this.Text;
        }

        #endregion

        #region Measures
        public void BasedOn(M.State s)
        {
            DressAs(MeasureProperties.MeasureState.ToString());
            Text = s._Name;
        }

        public void BasedOn(Polar.Measure m, NodeDetailsType type = NodeDetailsType.Detailed)
        {
            //Root node containing only the name of the point only
            Name = m._Point._Name;
            Text = m._Point._Name;
            ImageKey = StationProperties.Measures.ToString();
            SelectedImageKey = StationProperties.Measures.ToString();
            Tag = m;
            this._StationParameters = StationProperties.Measure;
            //let see what we add as node child
            switch (type)
            {
                case NodeDetailsType.NameOnly:
                    if (m is Polar.Measure) Text += " in " + (m as Polar.Measure).Face.ToString();
                    if (m.Comment != R.T_NO_COMMENT) Text += " (" + m.Comment + ")";
                    break;
                case NodeDetailsType.Detailed:

                    AddNodeAs(MeasureProperties.Date, m._Date.ToString(), m as TsuObject);
                    AddNode(m._Point);
                    LastNode.Text = "Point name: " + m._Point._Name;
                    AddNode(m._Status);
                    AddNodeAs(MeasureProperties.Comment, m.Comment.ToString(), m as TsuObject);

                    break;
                case NodeDetailsType.Serialized:
                    break;
                case NodeDetailsType.ShortedForMeasure_LocalCoordinates:
                    Text += " in " + (m as Polar.Measure)._Point._Coordinates.Local.ToString();
                    break;
                case NodeDetailsType.ShortedForMeasure_Displacement:
                    break;
                case NodeDetailsType.ShortedForMeasure_VerticalizedDisplacement:
                    break;
                default:
                    break;
            }
            if (m._Status is M.States.Bad) this.State = ENUM.StateType.NotReady;
            else if (m._Status is M.States.Questionnable) this.State = ENUM.StateType.Acceptable;
            else if (m._Status is M.States.Temporary) this.State = ENUM.StateType.Normal;
            else if (m._Status is M.States.Control) this.State = ENUM.StateType.Normal;
            else this.State = ENUM.StateType.Ready;
        }


        public void BasedOn(M.Measure m)
        {
            //Root node containing only the name of the point only
            Name = m._Name;
            Text = m._Name;
            ImageKey = StationProperties.Measures.ToString();
            SelectedImageKey = StationProperties.Measures.ToString();
            Tag = m;
            this._StationParameters = StationProperties.Measure;


            this.Nodes.Add(new TsuNode() { Text = $"Date {m._Date.ToString()}" });
            this.Nodes.Add(new TsuNode() { Text = $"State {m._Status._Name}" });

            // Point
            TsuNode p = new TsuNode();
            p.BasedOn(m._Point, showFullName: true);
            p.Text = "Point:" + p.Text;
            this.Nodes.Add(p);

            // Original Point
            if (m._OriginalPoint != null)
            {
                TsuNode op = new TsuNode();
                op.BasedOn(m._OriginalPoint, showFullName: true);
                op.Text = "(Original Point:" + m._OriginalPoint._Name + ")";
                this.Nodes.Add(op);
            }

            // Original Point modified
            if (m._OriginalPointModifiedInH != null)
            {
                TsuNode opm = new TsuNode();
                opm.BasedOn(m._OriginalPointModifiedInH, showFullName: true);
                opm.Text = "(Original Point modified by 'theo H':" + p.Text + ")";
                this.Nodes.Add(opm);
            }

            if (m._Status is M.States.Bad) this.State = ENUM.StateType.NotReady;
            else if (m._Status is M.States.Questionnable) this.State = ENUM.StateType.Acceptable;
            else if (m._Status is M.States.Temporary) this.State = ENUM.StateType.Normal;
            else if (m._Status is M.States.Control) this.State = ENUM.StateType.Normal;
            else this.State = ENUM.StateType.Ready;
        }

        public void BasedOn(M.MeasureOfLevel m)
        {
            BasedOn(m as M.Measure);


            this.Nodes.Add(new TsuNode() { Text = $"Ext {m._Extension.ToString()} m" });
            this.Nodes.Add(new TsuNode() { Text = $"Distance {m._Distance.ToString()} m" });
            this.Nodes.Add(new TsuNode() { Text = $"Reading {m._RawLevelReading.ToString()} m" });
            this.Nodes.Add(new TsuNode() { Text = $"Point {m._Point.ToString()} m" });
        }

        public void BasedOn(Tilt.Measure m)
        {
            BasedOn(m as M.Measure);

            this.Nodes.Add(new TsuNode() { Text = $"Distance {m._ReadingBeamDirection.ToString()} m" });
            this.Nodes.Add(new TsuNode() { Text = $"Reading {m._ReadingOppositeBeam.ToString()} m" });
        }

        public void BasedOn(M.MeasureOfOffset m)
        {
            BasedOn(m as M.Measure);

            this.Nodes.Add(new TsuNode() { Text = $"Distance {m._RawReading.ToString()} m" });
            this.Nodes.Add(new TsuNode() { Text = $"Reading {m._ReductedReading.ToString()} m" });
            this.Nodes.Add(new TsuNode() { Text = $"Point {m._Point.ToString()} m" });
        }

        public void BasedOn(Polar.Measure m)
        {
            BasedOn(m as M.Measure);
            Text += " (" + m.Face.ToString() + ")";

            // theo dH
            this.Nodes.Add(new TsuNode() { Text = $"Theo dH {m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates.ToString("m-m")} m" });

            // Ext
            M.Extension e = new M.Extension() { Value = m.Extension.Value, Sigma = m.Extension.Sigma };
            AddNode(e);

            TsuNode a = AddNode(m.Angles);
            if (m.AnglesF1 != null) a.AddNode(m.AnglesF1, "Face1");
            if (m.AnglesF2 != null) a.AddNode(m.AnglesF2, "Face2");

            TsuNode d = AddNode(m.Distance);
            if (m.DistanceF1 != null) d.AddNode(m.DistanceF1, "Face1");
            if (m.DistanceF2 != null) d.AddNode(m.DistanceF2, "Face2");
            if (m.distanceHorizontal != null) d.AddNode(m.distanceHorizontal);


            TsuNode param = new TsuNode() { Text = "Parameters" };
            //AddNodeAs(StationProperties.Extension,m.Extension.ToString() + " m (" + m._Point.SocketCode, m.Extension, ")");
            param.AddNodeAs(MeasureProperties.NumberOfMeasureToAverage, m.NumberOfMeasureToAverage.ToString(), m.NumberOfMeasureToAverage);
            if (m.ModeOfMeasure != null) param.AddNodeAs(MeasureProperties.Mode, m.ModeOfMeasure, m.ModeOfMeasure);
            param.AddNodeAs(StationProperties.Verticalisation, m.StationVerticalisationState.ToString(), m.StationVerticalisationState);
            //AddNodeAs(StationProperties.NumberOfMeasurement, m.NumberOfMeasureToAverage.ToString(), m.NumberOfMeasureToAverage);
            param.AddNodeAs(MeasureProperties.Face, m.Face.ToString(), m.Face);
            //param.AddNodeAs(MeasureProperties.Face, m.Face.ToString(), m.Face);

            this.Nodes.Add(param);

            //offsets?
            if (m.Offsets != null)
            {
                TsuNode offsets = this.AddNode(m.Offsets);
                offsets.Text = R.T_CST_OFFSETS;
            }

        }
        public void BasedOn(M.MeasureOfAngles m)
        {
            DressAs(MeasureProperties.Angles.ToString());
            Text = "Angles";
            AddNode(m.Raw, MeasureProperties.Raw.ToString());
            AddNode(m.Corrected, MeasureProperties.Corrected.ToString());
        }

        public void BasedOn(E.Angles a)
        {
            DressAs(MeasureProperties.Angles.ToString());
            Text = "Angle";

            int precision = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;

            // get face2 corrected in to face1 range
            E.Angles f2toF1 = null;
            if (a.isFace2) f2toF1 = Survey.TransformToOppositeFace(a.Clone() as E.Angles);

            // Ah
            TsuNode n = new TsuNode()
            {
                Tag = a
            };
            this.ImageKey = MeasureProperties.Horizontal.ToString();
            this.SelectedImageKey = MeasureProperties.Horizontal.ToString();

            n.Text += "H = " + T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(a.Horizontal, precision);
            if (a.isFace2) n.Text += " => f1 " + T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(f2toF1.Horizontal, precision);
            n.Text += " gon";
            n.Name = n.Text;
            this.Nodes.Add(n);

            // Av
            n = new TsuNode()
            {
                Tag = a
            };
            this.ImageKey = MeasureProperties.Vertical.ToString();
            this.SelectedImageKey = MeasureProperties.Vertical.ToString();

            n.Text += "V = " + T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(a.Vertical, precision);
            if (a.isFace2) n.Text += " => f1 " + T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(f2toF1.Vertical, precision);
            n.Text += " gon";
            n.Name = n.Text;
            this.Nodes.Add(n);
        }

        public void BasedOn(M.MeasureOfDistance m)
        {
            int precisionDigits = (TSU.Tsunami2.Preferences.Values.GuiPrefs == null) ? 7 : TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
            DressAs(MeasureProperties.Distances.ToString());
            Text = "Distances";
            AddNode(m.Raw, MeasureProperties.Raw, " = ", precisionDigits, " m");
            AddNode(m.Corrected, MeasureProperties.Corrected, " = ", precisionDigits, " m");
            AddNode(m.Reflector, MeasureProperties.Reflector.ToString());
            AddNode(m.WeatherConditions, "Weather conditions");
        }

        public void BasedOn(List<M.Measure> l)
        {
            switch (DetailsType)
            {
                case NodeDetailsType.Special:
                    NextPointsList(l);
                    break;
                default:
                    StationProperties p;
                    if (l != null)
                    {
                        p = StationProperties.Measures;
                        this.DressAs(p, "", l);
                        this.Text = "Measures";
                        for (int i = l.Count - 1; i >= 0; i--)
                        {
                            AddNode(l[i], null, DetailsType);
                        }
                    }
                    break;
            }
        }
        private void NextPointsList(List<M.Measure> l)
        {
            try
            {
                StationProperties p;
                if (l != null)
                {
                    p = StationProperties.NextPoints;
                    this.DressAs(p, "", null);

                    this.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
                    this.Text = "Next " + l.Count + " Point(s)";
                    if (l.Count > 0 && l[0]._Point!=null)
                        this.Text += " (" + l[0]._Point._Name + ")";

                    this.Tag = l;

                    bool couldBeManagedByAt40x = l.Count > 0 && l[0].SendingStationModule != null &&
                        l[0].SendingStationModule._InstrumentManager?.SelectedInstrument?._Model?.Contains("AT40") == true;
                    string reflectorSelectedInAT40x = R.String_Unknown;
                    if (couldBeManagedByAt40x)
                    {
                        var at = l[0].SendingStationModule?._InstrumentManager?.SelectedInstrumentModule as I.Device.AT40x.Module;
                        if (at != null && at.ActualReflector != null)
                            reflectorSelectedInAT40x = at.ActualReflector._Name;
                    }
                    

                    foreach (Polar.Measure m in l)
                    {
                        TsuNode n = new TsuNode();
                        p = StationProperties.NextPoint;
                        n.DressAs(p, "", null);
                        if (m._Status.Type == M.States.Types.Good || m.DirectMeasurementWanted)
                        {
                            n.ImageKey = "KnownPoint";
                            n.SelectedImageKey = "KnownPoint";
                        }
                        else if (m._Status.Type == M.States.Types.Questionnable)
                        {
                            n.ImageKey = "QuestionnablePoint";
                            n.SelectedImageKey = "QuestionnablePoint";
                        }
                        else if (m._Status.Type == M.States.Types.Control)
                        {
                            n.ImageKey = "ControlPoint";
                            n.SelectedImageKey = "ControlPoint";
                        }
                        n.Text = m._Point?._Name;
                        n.Tag = m;
                        n.ToolTipText = "Next point = " + m;

                        // reflector
                        bool reflectorKnown = m.Distance.Reflector != null && m.Distance.Reflector._Name != R.String_Unknown;
                        string text = " = ";
                        if (reflectorKnown)
                            text += m.Reflector._Name;
                        else
                        {
                            if (couldBeManagedByAt40x)
                                text += $"{R.T_MANAGED_BY_AT40X} ({reflectorSelectedInAT40x})";
                            else
                                text += "To be selected";
                        }

                        n.AddNodeAs(StationProperties.NextReflector, text, n.Tag as TsuObject);

                        // rallonge based (and code or not)
                        {
                            string display = "";

                            if (m.Extension.Value != TSU.Tsunami2.Preferences.Values.na)
                            {
                                if (TSU.Tsunami2.Preferences.Values.GuiPrefs.UnitForExtension == TSU.Preferences.Preferences.DistanceUnit.mm)
                                    display = (m.Extension.Value * 1000).ToString() + " mm";
                                else
                                    display = m.Extension.Value.ToString() + " m";
                            }
                            else
                            {
                                E.SocketCode c = m._Point.SocketCode;
                                if (c != null)
                                    display = $"{R.T_MANAGED_BY_POINT_CODE} ({c.DefaultExtensionForPolarMeasurement * 1000} mm)";
                                else
                                    display = "0 mm";

                            }

                            n.AddNodeAs(StationProperties.Extension, " = " + display, n.Tag as TsuObject);
                        }

                        // socket
                        {
                            string display = "";
                            if (m._OriginalPoint == null || m._OriginalPoint.SocketCode == null)
                                display = "Not defined";
                            else
                                display = m._OriginalPoint.SocketCode.ToString();
                            n.AddNodeAs(StationProperties.SocketCode, display, m);
                        }

                        // Interfaces
                        {
                            n.AddNodeAs(StationProperties.Interfaces, m.Interfaces._Name, m);
                        }
                        // CCS H difference
                        {
                            n.Nodes.Add(new TsuNode() { Tag = m, _StationParameters = StationProperties.dHStakeOut, Text = $"dH for CCS stakeout = {m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates.Value} m" });
                        }

                        n.AddNodeAs(StationProperties.NextFace, " = " + (n.Tag as Polar.Measure).Face.ToString(), n.Tag as TsuObject);
                        n.AddNodeAs(StationProperties.NextComment, " = " + (n.Tag as Polar.Measure).Comment.ToString(), n.Tag as TsuObject);
                        n.AddNodeAs(StationProperties.Status, " = " + (n.Tag as Polar.Measure)._Status._Name, n.Tag as TsuObject);
                        n.AddNodeAs(StationProperties.NumberOfMeasurement, " = " + (n.Tag as Polar.Measure).NumberOfMeasureToAverage.ToString(), n.Tag as TsuObject);
                        this.Nodes.Add(n);
                    }
                }
            }
            catch (Exception)
            {
                string titleAndMessage = $"Problem with Creating the treeview node;Of the next points list";
                new MessageInput(MessageType.Warning, titleAndMessage).Show();
                this.Nodes.Add("Problem");

            }
        }


        #endregion

        #region Elements
        public void BasedOn(E.Point pt, bool showFullName = false)
        {
            if (pt == null)
            {
                this.Text = "Missing point";
                if (Tsunami2.View != null)
                {
                    string titleAndMessage = $"There is a missing point;Required to fill a treeview node";
                    new MessageInput(MessageType.Warning, titleAndMessage).Show();
                }

                return;
            }

            if (this.selectables != null)
                if (!this.selectables.Contains(pt))
                {
                    this.Text = R.T_NOT_AVAILABLE;
                    return;
                }

            this.DressAs(pt.fileElementType);
            this.Tag = pt;
            if (showFullName || pt._Point == "")
                this.Text = pt._Name;
            else
                this.Text = pt._Point;

            this.Name = this.Text;

            this.AddNode(pt._Coordinates);


            TsuNode parameters = new TsuNode("Parameters", ElementType.Parameters);
            parameters.DressAs(ElementType.Parameters);
            parameters.AddNodeAs(ElementType.Cumul, "", pt._Parameters.Cumul);
            parameters.AddNodeAs(ElementType.BeamBearing, "", pt._Parameters.GisementFaisceau);
            parameters.AddNodeAs(ElementType.Length, "", pt._Parameters.L);
            parameters.AddNodeAs(ElementType.Slope, "", pt._Parameters.Slope);
            parameters.AddNodeAs(ElementType.Tilt, "", pt._Parameters.Tilt);
            this.Nodes.Add(parameters);


            string type = pt.Type.ToString();
            TsuNode theoOrNP = new TsuNode() { Text = $"Point known as '{type}'" };
            this.Nodes.Add(theoOrNP);

            if (pt.SocketCode != null)
            {
                TsuNode socket = new TsuNode() { Text = "Socket code: " + pt.SocketCode.ToString(), ImageKey = ElementType.SocketCode.ToString(), SelectedImageKey = ElementType.SocketCode.ToString() };
                this.Nodes.Add(socket);
            }

            TsuNode comment = new TsuNode() { Text = "Comment: " + pt.Comment };
            this.Nodes.Add(comment);

            switch (pt.State)
            {
                case E.Element.States.Good:
                case E.Element.States.ModifiedByUser:
                    this.State = ENUM.StateType.Normal;
                    break;
                case E.Element.States.Questionnable:
                case E.Element.States.Approximate:
                    this.State = ENUM.StateType.Acceptable;
                    break;
                case E.Element.States.Bad:
                default:
                    this.State = ENUM.StateType.NotReady;
                    break;
            }

        }



        public void BasedOn(TSU.Preferences.Preferences p)
        {
            this.Tag = p;
            this.Name = $"{R.T_PREFERENCES_ON_THIS_COMPUTER}";
            this.Text = this.Name;

            AddNode(p.GuiPrefs);


        }

        public void BasedOn(Global p)
        {
            this.Tag = p;
            this.Name = "Options";
            this.Text = this.Name;

            AddNodeAs(p.HideUnusedCalaPoint, R.T_LGC_INPUT_HIDE_UNUSED_CALA);
            AddNodeAs(p.daltonien, R.T_COLORBLIND);
            AddNodeAs(p.ShowCommentedLineInGeodeExport, R.T_SHOW_COMMENTED_LINES_IN_GEODEEXPORT);
            AddNodeAs(p.DontEverShowTheDeclarationStepOfGuidedModule, R.T_DONT_EVER_SHOW_THE_DECLARATION_STEP_OF_GUIDED_MODULE);
            AddNodeAs(p.UseDotsInNamesAndCodesForThepoints, R.T_USE_DOTS);
            AddNodeAs(p.ShowDescriptionWhenHooveringAListView, R.T_SHOW_DESCRIPTION_WHEN_HOOVERING_A_LIST_VIEW);
            AddNodeAs(p.NotificationLeavingOnTheLeftSide, "Notification Leaving On The Left Side");
            AddNodeAs(p.AddPointsToTheEndOfTheNextPointsList, "Add points to the end of the APM 'next points' list");
            AddNodeAs(p.MakeScreenshotAtEveryMouseClick, "Screenshot each mouse click");
            AddNodeAs(p.MakeVideoFromScreenshots, "Do video from Screenshots");
            AddNodeAs(p.ShowOneButtonSubMenu, "Show popup menu even if it contains a single button");
            AddNodeAs(p.LogAT40xMeasurementSpeed, "Log AT40x Measurement Speed");
            
            AddNodeAs(p.AllowExportOfPolarStationsWithoutClosure, "Allow Export Of Polar Stations Without Closure");
            AddNodeAs(p.AskForTsuFileNameAndLocation, "Ask For Tsu File Name And Location");
            AddNodeAs(p.ExportBadPolarStationsAsCommentedLines, "Export Bad Polar Stations As Commented Lines");
            AddNodeAs(p.ShowCodeProposition, "Show Code Proposition");
            AddNodeAs(p.HideMachineParameters, "Hide Machine Parameters");
            AddNodeAs(p.MoveBetweenSameFaceMeasurements, "Move Between Same Face Measurements");
            
            AddNodeAs(p.MoveOneEdgeOnlyWhenResizingWindows, "Move one edge only (instead of both opposite) when resizing windows");
            AddNodeAs(p.BocMeasuresAreNeverExpired, "BOC Measures Never Expired");
            AddNodeAs(p.BOC_Run_on_measure_update, "BOC runs automatically on new polar measurement with pintable");
            AddNodeAs(p.BOC_from_ADMIN_step_mixes_all_observations, "BOC runs with mixed observations in admin GEM step");

        }

        public void BasedOn(M.WeatherConditions wc)
        {
            this.Tag = wc;
            this.Name = R.T_WEATHER_CONDITIONS;
            this.Text = this.Name;


            if (wc.WetTemperature != TSU.Tsunami2.Preferences.Values.na)
                this.Nodes.Add(new TsuNode() { Text = $"{R.T_WEATHER_CONDITIONS}: {wc.WetTemperature} °C", ImageKey = "Temperature", SelectedImageKey = "Temperature" });
            this.Nodes.Add(new TsuNode() { Text = $"{R.T_HUMIDITY}: {wc.humidity} %", ImageKey = "Humidity", SelectedImageKey = "Humidity" });
            this.Nodes.Add(new TsuNode() { Text = $"{R.T_PRESSURE}: {wc.pressure} mBar", ImageKey = "Pressure", SelectedImageKey = "Pressure" });
            this.Nodes.Add(new TsuNode() { Text = $"{R.T_DRY_TEMPERATURE}: {wc.dryTemperature} °C", ImageKey = "Temperature", SelectedImageKey = "Temperature" });

        }

        public void BasedOn(E.Coordinates c)
        {
            this.DressAs(ElementType.CoordinateSystem);
            this.Tag = c;
            if (c.SystemName == R.String_Unknown)
                this.Text = R.String_Unknown;
            else
                this.Text = "Coordinates in " + c.SystemName;
            this.Name = this.Text;
            int d = Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;

            this.Nodes.Add(new TsuNode($"X: {c.X.ToStringInJcgm200Way(d)} m"));
            this.Nodes.Add(new TsuNode($"Y: {c.Y.ToStringInJcgm200Way(d)} m"));
            this.Nodes.Add(new TsuNode($"Z: {c.Z.ToStringInJcgm200Way(d)} m"));
        }

        public void BasedOn(E.Line3D c)
        {
            this.Tag = c;
            this.Text = "Line Parameters";

            this.Nodes.Add(new TsuNode(c._Coordinates, NodeDetailsType.Detailed));
            this.Nodes.Add(new TsuNode($"Bearing = {c.Bearing.ToString("gon-gon")}"));
            this.Nodes.Add(new TsuNode($"Slope = {c.Slope.ToString("gon-gon")}"));
        }

        public void BasedOn(E.CoordinatesInAllSystems cs)
        {
            this.DressAs(ElementType.CoordinateSystem);
            this.Tag = cs;
            this.Text = "Coordinates";
            this.Name = this.Text;
            this.Nodes.Add(new TsuNode($"Approximate = {cs.AreApproximate}"));
            foreach (var coord in cs.ListOfAllCoordinates)
            {
                if (coord.AreKnown)
                    this.Nodes.Add(new TsuNode(coord));
            }
        }

        public void BasedOn(E.CoordinateSystems css)
        {
            this.DressAs(ElementType.CoordinateSystem);
            this.Tag = css;
            this.Text = "Coordinates systems";
            this.Name = this.Text;

            this.Nodes.Add(new TsuNode($"Selected type = {css.sSelectedType}"));

            TsuNode all = new TsuNode($"All");
            this.Nodes.Add(all);
            foreach (CoordinateSystem cs in css.AllTypes)
            {
                TsuNode csn = new TsuNode(cs);
                all.Nodes.Add(csn);
            }
        }

        public void BasedOn(CoordinateSystem cs)
        {
            this.DressAs(ElementType.CoordinateSystem);
            this.Tag = cs;
            this.Text = $"{cs._Name}: {cs.LongName}";
        }


        public void BasedOn(EC.CompositeElement e)
        {
            this.DressAs(e.fileElementType);
            this.Tag = e;
            if (e.fileElementType == ElementType.Class)
                this.Name = e._ClassAndNumero;
            else
                this.Name = e._Name;
            this.Text = this.Name;
            foreach (E.Element child in e.Elements)
            {
                TsuNode childNode = new TsuNode(child, NodeDetailsType.Detailed, this.selectables);
                if (childNode.Text != R.T_NOT_AVAILABLE)
                    if (childNode.Text == "")
                        childNode.Text = "empty";
                this.Nodes.Add(childNode);
            }
        }

        public void BasedOn(E.Circle c)
        {
            this.DressAs(c.fileElementType);
            this.Tag = c;
            this.Name = c._Name;
            this.Text = c._Name;
            this.AddNode(c._Coordinates, "Center");
            this.AddNode(c.Vector, "Normal Vector");
            this.AddNode(c.Radius, MeasureProperties.Value, "Radius", 5, "m");

        }

        public void BasedOn(EC.TheoreticalElement e)
        {
            this.DressAs(e.fileElementType);
            this.Tag = e;
            this.Name = e._Name;
            this.Text = e._Name;
            foreach (E.Element child in e.Elements)
            {
                TsuNode childNode = new TsuNode(child, NodeDetailsType.Detailed, this.selectables);
                if (childNode.Text != R.T_NOT_AVAILABLE)
                    this.Nodes.Add(childNode);
            }
        }


        public void BasedOn(Common.Tsunami t)
        {
            this.Tag = t;
            this.Name = R.T_CURRENT_TSUNAMI_INSTANCE;
            this.Text = R.T_CURRENT_TSUNAMI_INSTANCE;

            //propss
            TsuNode props = new TsuNode(R.T_PROPERTIES_OF_THE__TSU_FILE);
            this.Nodes.Add(props);



            // ccs
            TsuNode css = new TsuNode(R.T_COORDINATES_SYSTEMS);
            css.BasedOn(t.CoordinatesSystems);
            props.Nodes.Add(css);

            //zone
            if (t.Zone != null)
            {
                TsuNode zone = new TsuNode(R.T_COORDINATES_SYSTEMS);
                zone.BasedOn(t.Zone);
                props.Nodes.Add(zone);
            }

            //dsa
            TsuNode DsaFlagForAdvancedModule = new TsuNode();
            DsaFlagForAdvancedModule.BasedOn(t.DsaFlagForAdvancedModule);
            DsaFlagForAdvancedModule.Text = $"{R.T_VISIBILITY_OPTIONS_FOR} {R.T_ADVANCED_MODULES}";
            props.Nodes.Add(DsaFlagForAdvancedModule);
            TsuNode DsaFlagForGuidedModule = new TsuNode();
            DsaFlagForGuidedModule.BasedOn(t.DsaFlagForGuidedModule);
            DsaFlagForGuidedModule.Text = $"{R.T_VISIBILITY_OPTIONS_FOR} {R.T_GUIDED_MODULES}";
            props.Nodes.Add(DsaFlagForGuidedModule);


            //prefs
            TsuNode prefs = new TsuNode($"{R.T_PREFERENCES_ON_THIS_COMPUTER}");
            prefs.BasedOn(Tsunami2.Preferences.Values);
            this.Nodes.Add(prefs);
            TsuNode macros = new TsuNode() { Text = "Macros" };
            prefs.Nodes.Add(macros);
            foreach (var item in Tsunami2.Preferences.Values.GuiPrefs.Macros.ExistingMacros)
            {
                TsuNode node = new TsuNode() { Text = item.ToString() };
                macros.Nodes.Add(node);
            }

        }

        public void BasedOn(EC.FittedShape e)
        {
            this.DressAs(e.fileElementType);
            this.Tag = e;
            this.Name = e._Name;
            this.Text = e._Name;
            this.Nodes.Add(new TsuNode(e.parametricShape));
            foreach (E.Element child in e.Elements)
            {
                TsuNode childNode = new TsuNode(child, NodeDetailsType.Detailed, this.selectables);
                if (childNode.Text != R.T_NOT_AVAILABLE)
                    this.Nodes.Add(childNode);
            }
            TsuNode file = new TsuNode("", ElementType.File);

            string source = (e.FittingOutputFile == null) ? "Shape" : e.FittingOutputFile.FullName;
            file.DressAs(ElementType.File, source, e.FittingOutputFile);
            this.Nodes.Add(file);
        }
        public void BasedOn(EC.Sequence e)
        {
            this.DressAs(ENUM.ElementType.SequenceTH);
            this.Tag = e;
            this.Name = e._Name;
            this.Text = e._Name;


            bool first = true;

            foreach (E.Element child in e.Elements)
            {
                TsuNode n = new TsuNode();
                dynamic d = child;
                n.BasedOn(d, true);
                this.Nodes.Add(n);
                if (first && !(child is EC.SequenceTH))
                {
                    n.ImageKey = "THEODOLITE";
                    n.SelectedImageKey = "THEODOLITE";
                    first = false;
                }
            }
        }
        public void BasedOn(EC.SequenceTH e, bool fullname)
        {
            this.DressAs(ENUM.ElementType.SequenceTH);
            this.Name = e._Name;
            this.Tag = e;
            this.Text = e._Name;
            bool first = true;

            foreach (E.Element child in e.Elements)
            {
                TsuNode n = new TsuNode();
                dynamic d = child;
                n.BasedOn(d, fullname);
                this.Nodes.Add(n);
                if (first && !(child is EC.SequenceTH))
                {
                    n.ImageKey = "THEODOLITE";
                    n.SelectedImageKey = "THEODOLITE";
                    first = false;
                }
            }
        }

        public void BasedOn(EC.SequenceFil e, bool fullname)
        {
            this.DressAs(ENUM.ElementType.SequenceTH);
            this.Name = e._Name;
            this.Tag = e;
            this.Text = e._Name;
            foreach (E.Element child in e.Elements)
            {
                TsuNode n = new TsuNode();
                dynamic d = child;
                n.BasedOn(d, fullname);
                this.Nodes.Add(n);
            }
        }
        public void BasedOn(EC.SequenceNiv e, bool fullname)
        {
            this.DressAs(ENUM.ElementType.SequenceTH);
            this.Name = e._Name;
            this.Tag = e;
            this.Text = e._Name;
            foreach (E.Element child in e.Elements)
            {
                TsuNode n = new TsuNode();
                dynamic d = child;
                n.BasedOn(d, fullname);
                this.Nodes.Add(n);
            }
        }


        public void BasedOn(E.ParametricShape e)
        {
            this.DressAs(e.fileElementType);
            this.Name = e._Name;
            this.Tag = e;
            this.Text = e._Name;

            TsuNode ccs = new TsuNode("CCS", ElementType.CoordinateSystem);
            ccs.DressAs(ElementType.CoordinateSystem, "CERN", e._Coordinates.Ccs);
            ccs.AddNodeAs(ElementType.X, "", e._Coordinates.Ccs.X.Value);
            ccs.AddNodeAs(ElementType.Y, "", e._Coordinates.Ccs.Y.Value);
            ccs.AddNodeAs(ElementType.Z, "", e._Coordinates.Ccs.Z.Value);
            this.Nodes.Add(ccs);

            TsuNode phy = new TsuNode("Physicist", ElementType.CoordinateSystem);
            phy.DressAs(ElementType.CoordinateSystem, "Physicist", e._Coordinates.UserDefined);
            phy.AddNodeAs(ElementType.X, "", e._Coordinates.UserDefined.X.Value);
            phy.AddNodeAs(ElementType.Y, "", e._Coordinates.UserDefined.Y.Value);
            phy.AddNodeAs(ElementType.Z, "", e._Coordinates.UserDefined.Z.Value);
            this.Nodes.Add(phy);

            TsuNode local = new TsuNode("Local", ElementType.CoordinateSystem);
            local.DressAs(ElementType.CoordinateSystem, "Local", e._Coordinates.Local);
            local.AddNodeAs(ElementType.X, "", e._Coordinates.Local.X.Value);
            local.AddNodeAs(ElementType.Y, "", e._Coordinates.Local.Y.Value);
            local.AddNodeAs(ElementType.Z, "", e._Coordinates.Local.Z.Value);
            this.Nodes.Add(local);


            TsuNode parameters = new TsuNode("Parameters", ElementType.Parameters);
            parameters.DressAs(ElementType.Parameters);
            if (e.Vector != null)
            {
                parameters.AddNodeAs(ElementType.Line, "Vector", e.Vector.X.Value);
            }
            if (e.parameter3 != null) parameters.AddNodeAs(ElementType.Parameters, "Parameter", e.parameter3.Value);
            this.Nodes.Add(parameters);
        }
        #endregion

        #region Management.Instrument
        public void BasedOn(I.Sensor e)
        {
            this.DressAs(e._InstrumentClass, e._Model + " " + e._SerialNumber, e);
        }
        public void BasedOn(Reflector e)
        {
            this.DressAs(e._InstrumentClass, "", e);
            this.Name = e._InstrumentType.ToString() + " " + e.Id;
            this.Text = this.Name;

        }

        public void BasedOn(I.LevelingStaff e)
        {
            //this.DressWith(e._InstrumentType);
            this.DressAs(e._InstrumentClass, e._Model + " " + e._SerialNumber, e);
            //this.Name = e._Brand.ToString() + " " + e._Id;
            //this.Tag = e;
            //this.Text = this.Name;

        }
        #endregion

        #region Management.Operations
        public void BasedOn(O.Operation o)
        {
            if (o != null)
            {
                this.Tag = o;
            }
            //this.Text = o._Name + ": " +o.Description;
            this.Text = o.ToString();
            this.Name = o._Name;
            this.ImageKey = "Operation";
            this.SelectedImageKey = "Operation";
        }
        #endregion

        #region Stations

        public void BasedOn(Polar.Station.Parameters theodoliteParameters)
        {
            Polar.Station.Parameters param = theodoliteParameters;
            // Admin and instrument
            BasedOn(param as Station.Parameters);

            //
            // Setup Parameters
            //

            string temp;
            temp = StationProperties.Setup.ToString();
            TsuNode setupNode = new TsuNode() { Name = temp, ImageKey = temp, SelectedImageKey = temp, Text = "Setup Parameters", Tag = param, _StationParameters = StationProperties.Setup };
            if (param._IsSetup)
            {
                setupNode.State = ENUM.StateType.Ready;

                setupNode.Text += " " + string.Format(R.T_TH_PARAM_FINAL);
            }
            else
                if (param.Setups.ActualState == Polar.Station.Parameters.Setup.States.Approximated || param.Setups.ActualState == Polar.Station.Parameters.Setup.States.VerifiedApproximation)
            {
                setupNode.State = ENUM.StateType.Acceptable;
                setupNode.Text += " " + string.Format(R.T_TH_PARAM_TEMP);
            }
            else
            {
                setupNode.State = ENUM.StateType.Acceptable;
                setupNode.Text += " " + string.Format("(Not compensated yet)");
            }

            // Verticalisation
            temp = StationProperties.Verticalisation.ToString();
            TsuNode Verticalisation = new TsuNode() { Name = temp, ImageKey = temp, SelectedImageKey = temp, Tag = param.Setups.InitialValues.VerticalisationState, _StationParameters = StationProperties.Verticalisation };
            switch (param.Setups.InitialValues.VerticalisationState)
            {
                case Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation.Verticalised:
                    Verticalisation.Text = R.T_NODE_Verticalised;
                    Verticalisation.State = ENUM.StateType.Normal;
                    break;
                case Polar.Station.Parameters.Setup.Values.InstrumentVerticalisation.NotVerticalised:
                    Verticalisation.Text = R.T_NODE_NOT_Verticalised;
                    Verticalisation.State = ENUM.StateType.Acceptable;
                    break;
                default:
                    break;
            }

            // Tolerances
            temp = "Tolerances";
            TsuNode Tolerances = new TsuNode() { Name = temp, Text = temp, ImageKey = temp, SelectedImageKey = temp, Tag = param.Tolerance, _StationParameters = StationProperties.Tolerance };
            TsuNode sameFace = new TsuNode()
            {
                //    Text = string.Format("{0}: {1} CC, {2} mm", "Between same face measurements", param.Tolerance.Same_Face.CC, param.Tolerance.Same_Face.mm),
                Text = $"{R.T_BETWEEN_SAME_FACE_MEASUREMENTS}: H:{param.Tolerance.Same_Face.H_CC} CC, V:{param.Tolerance.Same_Face.V_CC} CC, {param.Tolerance.Same_Face.D_mm} mm",
                Name = "Face1",
                ImageKey = "Face1",
                SelectedImageKey = "Face1",
                Tag = param.Tolerance.Same_Face,
                _StationParameters = StationProperties.Theo_Tolerances
            };
            TsuNode opposite = new TsuNode()
            {
                // Text = string.Format("{0}: {1} CC, {2} mm", "Between opposite face measurements", param.Tolerance.Opposite_Face.CC, param.Tolerance.Opposite_Face.mm),
                Text = $"{R.T_BETWEEN_OPPOSITE_FACE_MEASUREMENTS}: H:{param.Tolerance.Opposite_Face.H_CC} CC, V:{param.Tolerance.Same_Face.V_CC} CC, {param.Tolerance.Opposite_Face.D_mm} mm",
                Name = "Face2",
                ImageKey = "Face2",
                SelectedImageKey = "Face2",
                Tag = param.Tolerance.Opposite_Face,
                _StationParameters = StationProperties.Theo_Tolerances

            };
            TsuNode closure = new TsuNode()
            {
                // Text = string.Format("{0}: {1} CC, {2} mm", "For station closure", param.Tolerance.Closure.CC, param.Tolerance.Closure.mm),
                Text = $"{R.T_FOR_STATION_CLOSURE}: H:{param.Tolerance.Closure.H_CC} CC, V:{param.Tolerance.Closure.V_CC} CC, {param.Tolerance.Closure.D_mm} mm",
                Name = "Face",
                ImageKey = "Face",
                SelectedImageKey = "Face",
                Tag = param.Tolerance.Closure,
                _StationParameters = StationProperties.Theo_Tolerances
            };
            Tolerances.Nodes.Add(sameFace);
            Tolerances.Nodes.Add(opposite);
            Tolerances.Nodes.Add(closure);

            // A-priori Sigmas
            temp = $"A-priori Sigmas set for {param.Condition} condition";
            TsuNode apriori = new TsuNode() { Name = "apriori", Text = temp, ImageKey = "Sigma", SelectedImageKey = "Sigma", Tag = param.AprioriSigmaForTheSelectedInstrumentAndConditon, _StationParameters = StationProperties.Theo_Sigmas };

            foreach (var item in param.AprioriSigmaForTheSelectedInstrumentAndConditon)
            {
                TsuNode couple = new TsuNode()
                {
                    Text = $"{item.Target}: sHA='{item.sigmaAnglCc}'cc,  sVA='{item.sigmaZenDCc}'cc, sD='{item.sigmaDistMm}'mm, rCentering='{item.rCenteringMm} ({item.Condition})",
                    Name = item.Target,
                    ImageKey = StationProperties.NextReflector.ToString(),
                    SelectedImageKey = StationProperties.NextReflector.ToString(),
                    Tag = item,
                    _StationParameters = StationProperties.Theo_Sigma
                };
                apriori.Nodes.Add(couple);
            }

            // Stationned Point
            temp = StationProperties.Point.ToString();

            TsuNode point = new TsuNode(param._StationPoint);
            point._StationParameters = StationProperties.Point;
            if (param._StationPoint == null || param._StationPoint._Name == R.String_Unknown)
            {
                point.Text = string.Format(R.T_NODE_ST_PT_NS);
                point.State = ENUM.StateType.NotReady;
            }
            else
            {
                point.Text = string.Format(R.T_NODE_ST_PT, param._StationPoint._Name);
                point.State = ENUM.StateType.Ready;
                E.Point ps = param._StationPoint;
                E.Coordinates c = (ps._Coordinates.HasLocal) ? c = ps._Coordinates.Ccs : c = ps._Coordinates.Local;

                if (c.AreKnown && !(c.X.Sigma == TSU.Tsunami2.Preferences.Values.na))
                {
                    point.Text += $" sX = {c.X.GetSigma("mm")}, sY = {c.Y.GetSigma("mm")}, sZ = {c.Z.GetSigma("mm")} mm";
                }
            }

            // Instrument height
            temp = StationProperties.InstrumentHeight.ToString();
            TsuNode instHeight = new TsuNode() { Name = temp, ImageKey = temp, SelectedImageKey = temp, Tag = param.Setups.InitialValues.IsInstrumentHeightKnown, _StationParameters = StationProperties.InstrumentHeight };
            switch (param.Setups.InitialValues.IsInstrumentHeightKnown)
            {
                case Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight.NotDefinedYet:
                    instHeight.Text = R.T_NODE_INSTHEIGHT_NOT;
                    instHeight.State = ENUM.StateType.NotReady;
                    break;
                case Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight.Known:
                    if (param._InstrumentHeight != null)
                        instHeight.Text = string.Format(R.T_NODE_INSTHEIGHT_VALUE, param._InstrumentHeight.Value.ToString());
                    else
                        instHeight.Text = R.String_Unknown;
                    instHeight.State = ENUM.StateType.Ready;
                    break;
                case Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight.Unknown:
                    if (param._IsSetup)
                    {
                        instHeight.Text = string.Format(R.T_NODE_INSTHEIGHT_COMP) + " = " + param._InstrumentHeight.ToString(5);
                        instHeight.State = ENUM.StateType.Ready;
                    }
                    else
                    {
                        instHeight.Text = string.Format(R.T_NODE_INSTHEIGHT_COMP);
                        instHeight.State = ENUM.StateType.Ready;
                    }
                    break;
                default:
                    break;
            }


            // Orientation
            temp = StationProperties.Orientation.ToString();
            TsuNode orientation = new TsuNode() { Name = temp, ImageKey = temp, SelectedImageKey = temp, Tag = param.vZero, _StationParameters = StationProperties.Orientation };
            if (param.vZero == null)
            {
                orientation.Text = R.T_NODE_V0_NOT_SET;
                orientation.State = ENUM.StateType.Acceptable;
            }
            else
            {
                orientation.Text = string.Format(R.T_NODE_V0, param.vZero.ToString(5));

                if (param._IsSetup)
                    orientation.State = ENUM.StateType.Ready;
                else
                {
                    orientation.State = ENUM.StateType.Acceptable;
                    orientation.Text += " (Approximated to allow goto)";
                }
            }

            setupNode.Nodes.Add(Verticalisation);
            setupNode.Nodes.Add(Tolerances);
            setupNode.Nodes.Add(apriori);
            setupNode.Nodes.Add(point);
            setupNode.Nodes.Add(instHeight);
            setupNode.Nodes.Add(orientation);
            setupNode.State = GetChildrenWorstState(setupNode);



            //
            // Default Parameters
            //

            TsuNode defaultNode = new TsuNode();
            defaultNode.DressAs(StationProperties.NextDefault, "", param.DefaultMeasure);
            defaultNode.Text = R.T_DEFAULT_MEASUREMENT_PARAMETERS;
            defaultNode.AddNodeAs(StationProperties.DefaultPointName, param.DefaultMeasure._PointName.ToString(), param.DefaultMeasure);
            //socket
            {
                string display = "";
                if (param.DefaultMeasure._Point.SocketCode == null)
                    display = "Not defined";
                else
                    display = param.DefaultMeasure._Point.SocketCode.ToString();
                defaultNode.AddNodeAs(StationProperties.DefaultPointCode, display, param.DefaultMeasure);
            }
            // reflector
            {
                if (param.DefaultMeasure.Distance.Reflector != null)
                {
                    defaultNode.AddNodeAs(StationProperties.NextReflector, param.DefaultMeasure.Distance.Reflector._Name, param.DefaultMeasure);
                    // exception At40x that want to manage the reflector alone
                    if (param._Instrument is TSU.Common.Instruments.Device.AT40x.Instrument && param.DefaultMeasure.Distance.Reflector._Name == R.String_Unknown)
                    {
                        (defaultNode.LastNode as TsuNode).Text = (defaultNode.LastNode as TsuNode).Text.Replace(R.String_Unknown, R.T_MANAGED_BY_AT40X);
                        (defaultNode.LastNode as TsuNode).State = ENUM.StateType.Normal;
                    }
                }
                else
                    defaultNode.AddNodeAs(StationProperties.NextReflector, "No default reflector", param.DefaultMeasure);
            }

            // Extension
            {
                bool usingMM = TSU.Tsunami2.Preferences.Values.GuiPrefs.UnitForExtension == TSU.Preferences.Preferences.DistanceUnit.mm;
                int factor = usingMM ? 1000 : 1;
                double value = param.DefaultMeasure.Extension.Value;
                bool valueSet = value != TSU.Tsunami2.Preferences.Values.na;
                value *= factor;

                string valueToShow = valueSet ? value.ToString() : R.T_MANAGED_BY_POINT_CODE;
                string unitToShow = valueSet ? " " + TSU.Tsunami2.Preferences.Values.GuiPrefs.UnitForExtension.ToString() : "";

                defaultNode.AddNodeAs(StationProperties.Extension, valueToShow, param.DefaultMeasure, unitToShow);
            }

            defaultNode.AddNodeAs(StationProperties.Interfaces, param.DefaultMeasure.Interfaces.ToString(), param.DefaultMeasure);
            defaultNode.AddNodeAs(StationProperties.NextFace, param.DefaultMeasure.Face.ToString(), param.DefaultMeasure);

            defaultNode.AddNodeAs(StationProperties.NumberOfMeasurement, param.DefaultMeasure.NumberOfMeasureToAverage.ToString(), param.DefaultMeasure);
            defaultNode.AddNodeAs(StationProperties.NextComment, param.DefaultMeasure.Comment.ToString(), param.DefaultMeasure);

            defaultNode.State = GetChildrenWorstState(defaultNode);

            // Add to main
            this.Nodes.Add(setupNode);
            this.Nodes.Add(defaultNode);
        }

        public void BasedOn(Station.Parameters offsetParameters)
        {
            Name = offsetParameters._StationName;
            if (offsetParameters._Station != null)
                Text = "Parameters of station '" + offsetParameters._Station._Name + "' (Status: " + offsetParameters._State._Description + ")";
            else
                Text = "Unknown station";

            this.State = (offsetParameters._State is Station.State.Measuring) ? ENUM.StateType.Normal : ENUM.StateType.Acceptable;
            this.State = (offsetParameters._State is Station.State.SFBcalculationSuccessfull) ? ENUM.StateType.Ready : this.State;
            this.State = (offsetParameters._State is Station.State.LGC2CalculationSuccessfull) ? ENUM.StateType.Ready : this.State;
            this.State = (offsetParameters._State is Station.State.WireSaved) ? ENUM.StateType.Ready : this.State;
            ImageKey = "StationParameters";
            SelectedImageKey = "StationParameters";
            Tag = offsetParameters._Station;

            TsuNode adminNode = new TsuNode("Administration");
            adminNode.DressAs(StationProperties.Admin, "", offsetParameters);
            adminNode.AddNodeAs(StationProperties.Date, offsetParameters._Date.ToString(), offsetParameters);
            adminNode.AddNodeAs(StationProperties.Team, offsetParameters._Team.ToString(), offsetParameters);
            adminNode.AddNodeAs(StationProperties.Operation, offsetParameters._Operation.value.ToString(), offsetParameters);

            adminNode.State = GetChildrenWorstState(adminNode);

            this.Nodes.Add(adminNode);

            this.Nodes.Add(new TsuNode()
            {
                Text = R.T_IMPORT_POINTS_FROM_FILE,
                _StationParameters = StationProperties.TheoFile,
                Tag = offsetParameters,
            });
            if (Tsunami2.Properties.Elements.Count == 0)
                (this.LastNode as TsuNode).State = ENUM.StateType.NotReady;
            else
                (this.LastNode as TsuNode).State = ENUM.StateType.Normal;

            //AddNodeAs(StationProperties.TheoFile, "Choose Theoretical file", p);

            if (offsetParameters._Instrument != null && offsetParameters._Instrument._Model != null)
            {
                AddNodeAs(StationProperties.Instrument, offsetParameters._Instrument._Model + " " + offsetParameters._Instrument._SerialNumber, offsetParameters);
            }
            else
            {
                AddNodeAs(StationProperties.Instrument, "To be selected", offsetParameters);
                (this.LastNode as TsuNode).State = ENUM.StateType.NotReady;
            }
        }

        private ENUM.StateType GetChildrenWorstState(TsuNode adminNode)
        {
            ENUM.StateType worst = adminNode.State;
            foreach (TsuNode item in adminNode.Nodes)
            {
                item.State = GetChildrenWorstState(item);
                if (item.State > worst) worst = item.State;
            }
            return worst;
        }


        /// <summary>
        /// Construit le noeud admin pour un station line module
        /// </summary>
        /// <param name="p"></param>
        /// <param name="showCoordType"></param>
        public void BasedOn(Line.Station.Parameters offsetParameters, Line.Station.OffsetStrategy strat, bool showCoordType)
        {
            Line.Station.Parameters p = offsetParameters;
            this.DressAs(R.StringTsuNode_Line_Admin, "", p);
            this.Name = "Admin_Main_";
            this.AddNodeAs(R.StringTsuNode_Line_Date + ": ", p._Date.ToString(), p, ENUM.StateType.Normal);
            this.LastNode.Name = "Admin_Date_";
            this.LastNode.ImageKey = "Date";
            this.LastNode.SelectedImageKey = "Date";
            ENUM.StateType stateTeam = (p._Team == R.String_UnknownTeam) ? ENUM.StateType.ToBeFilled : ENUM.StateType.Normal;
            this.AddNodeAs(R.StringTsuNode_Line_Team + ": ", p._Team.ToString(), p);
            this.LastNode.Name = "Admin_Team_";
            this.LastNode.ImageKey = "Team";
            this.LastNode.SelectedImageKey = "Team";
            ENUM.StateType stateOp = (!p._Operation.IsSet) ? ENUM.StateType.ToBeFilled : ENUM.StateType.Normal;
            this.AddNodeAs(R.StringTsuNode_Line_Operation + ":", p._Operation.ToString(), p, stateOp);
            this.LastNode.Name = "Admin_OperationID_";
            this.LastNode.ImageKey = "Operation";
            this.LastNode.SelectedImageKey = "Operation";
            if (p._Instrument != null && p._Instrument._Model != null)
                this.AddNodeAs(R.StringTsuNode_Line_Instrument + ":", p._Instrument._Model + " sn" + p._Instrument._SerialNumber, null, ENUM.StateType.Normal);
            else
            {
                this.AddNodeAs(R.StringTsuNode_Line_Instrument + ":", R.StringTsuNode_Line_ToBeSelected, null, ENUM.StateType.ToBeFilled);
                //(this.LastNode as TsuNode).State = ENUM.StateType.ToBeFilled;
            }
            //ColorNodeBaseOnState(this.LastNode as TsuNode);
            this.LastNode.Name = "Admin_Instrument_";
            this.LastNode.ImageKey = p._Instrument._InstrumentClass.ToString();
            this.LastNode.SelectedImageKey = p._Instrument._InstrumentClass.ToString();
            this.AddNodeAs(R.StringTsuNode_Line_Temperature + ":", p._Temperature.ToString("F1", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " °C", this.Tag as TsuObject, ENUM.StateType.Normal);
            this.LastNode.ImageKey = "Temperature";
            this.LastNode.SelectedImageKey = "Temperature";
            this.LastNode.Name = "Admin_Temperature_";
            this.AddNodeAs(R.StringTsuNode_Line_ToleranceNode + ":", Math.Round(p._Tolerance * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " mm", this.Tag as TsuObject, ENUM.StateType.Normal);
            this.LastNode.ImageKey = "Tolerance";
            this.LastNode.SelectedImageKey = "Tolerance";
            this.LastNode.Name = "Admin_Tolerance_";
            if (showCoordType)
            {
                this.AddNodeAs(p._CoordType);
                this.LastNode.Name = "Admin_CoordType_";
            }
        }
        /// <summary>
        ///  add the node computation in the treeview for Ecartometry with the computation option
        /// </summary>
        /// <param name="st"></param>
        public void ComputationBasedOn(Line.Station.View st)
        {
            Line.Station.Parameters p = st.stationLineModule.WorkingAverageStationLine._Parameters;
            this.Name = "Comp__";
            this.Text = string.Format("{0}", R.StringTsuNode_Computation);
            this.State = ENUM.StateType.Normal;
            ColorNodeBaseOnState(this);
            if (st.showCalculationOption)
            {
                TsuNode node_CalculationOption = new TsuNode();
                this.Nodes.Add(node_CalculationOption);
                node_CalculationOption.Name = "Comp_Strategy_";
                node_CalculationOption.Text = string.Format("{0} {1}", R.StringTsuNode_Line_CalculationStrategy, st.stationLineModule._ComputeStrategy._name);
                node_CalculationOption.State = ENUM.StateType.Normal;
                ColorNodeBaseOnState(node_CalculationOption);
                if (st.stationLineModule._ComputeStrategy is Line.Station.LGC2Strategy)
                {
                    node_CalculationOption.ImageKey = "LGC2CalculationOption";
                    node_CalculationOption.SelectedImageKey = "LGC2CalculationOption";
                    ////Desactivation du node show LGC output JIRA TSU-2642 22-01-2020
                    //TsuNode nodeShowOutput = new TsuNode();
                    //nodeShowOutput.ImageKey = "Run";
                    //nodeShowOutput.SelectedImageKey = "Run";
                    //nodeShowOutput.Name = "Comp_Strategy_ShowLGC";
                    //nodeShowOutput.State = ENUM.StateType.Normal;
                    //ColorNodeBaseOnState(nodeShowOutput);
                    //nodeShowOutput.Text = R.StringTsuNode_Line_ShowLGCFile;
                    //this.Nodes.Add(nodeShowOutput);
                }
                if (st.stationLineModule._ComputeStrategy is Line.Station.SFBStrategy)
                {
                    node_CalculationOption.ImageKey = "SFBCalculationOption";
                    node_CalculationOption.SelectedImageKey = "SFBCalculationOption";
                }
            }
        }
        public void BasedOn(Length.Station.View st)
        //ajout des propriétés pour les stations de nivellement
        {
            Length.Station.Parameters p = st._stationLengthModule._LengthStation._Parameters;
            this.DressAs(R.StringTsuNode_Length_Admin, "", p);
            this.Name = "Admin_Main_";
            this.AddNodeAs(R.StringTsuNode_Length_Date + ": ", p._Date.ToString(), p, ENUM.StateType.Normal);
            this.LastNode.Name = "Admin_Date_";
            this.LastNode.ImageKey = "Date";
            this.LastNode.SelectedImageKey = "Date";
            ENUM.StateType stateTeam = (p._Team == R.String_UnknownTeam) ? ENUM.StateType.ToBeFilled : ENUM.StateType.Normal;
            this.AddNodeAs(R.StringTsuNode_Length_Team + ": ", p._Team.ToString(), p, stateTeam);
            this.LastNode.Name = "Admin_Team_";
            this.LastNode.ImageKey = "Team";
            this.LastNode.SelectedImageKey = "Team";
            ENUM.StateType stateOp = (!p._Operation.IsSet) ? ENUM.StateType.ToBeFilled : ENUM.StateType.Normal;
            this.AddNodeAs(R.StringTsuNode_Length_Operation + ":", p._Operation.ToString(), p, stateOp);
            this.LastNode.Name = "Admin_OperationID_";
            this.LastNode.ImageKey = "Operation";
            this.LastNode.SelectedImageKey = "Operation";
            if (p._Instrument != null && p._Instrument._Model != null)
                this.AddNodeAs(R.StringTsuNode_Length_Instrument + ":", p._Instrument._Model + " sn" + p._Instrument._SerialNumber, null, ENUM.StateType.Normal);
            else
            {
                this.AddNodeAs(R.StringTsuNode_Length_Instrument + ":", R.StringTsuNode_Level_NA, null, ENUM.StateType.ToBeFilled);
                //(this.LastNode as TsuNode).State = ENUM.StateType.ToBeFilled;
            }
            //ColorNodeBaseOnState(this.LastNode as TsuNode);
            this.LastNode.Name = "Admin_Instrument_";
            this.LastNode.ImageKey = p._Instrument._InstrumentClass.ToString();
            this.LastNode.SelectedImageKey = p._Instrument._InstrumentClass.ToString();
            this.AddNodeAs(R.StringTsuNode_Length_ToleranceNode + ":", Math.Round(p._Tolerance * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " mm", this.Tag as TsuObject);
            this.LastNode.ImageKey = "Tolerance";
            this.LastNode.SelectedImageKey = "Tolerance";
            this.LastNode.Name = "Admin_Tolerance_";
            //this.AddNodeAs(p._ZType);
            //this.LastNode.Name = "Admin_CoordType_";
        }
        /// <summary>
        /// Crèe le noeud avec les paramètres par défaut pour une mesure de mise en longueur
        /// </summary>
        /// <param name="parameters"></param>
        internal void DefaultMeasBasedOn(Length.Station.Parameters p)
        {
            this.DressAs(R.StringTsuNode_Length_DefaultMeasParameters, "", p);
            this.Name = "DefaultMeas_Main_";
            this.AddNodeAs(R.StringTsuNode_Length_Reflector + ": ", p._DefaultReflector.ToString(), p, ENUM.StateType.Normal);
            this.LastNode.Name = "DefaultMeas_Reflector_";
            this.LastNode.ImageKey = "Reflector";
            this.LastNode.SelectedImageKey = "Reflector";
            TsuNode nodeDoubleFace = new TsuNode();
            nodeDoubleFace.ImageKey = "Face";
            nodeDoubleFace.SelectedImageKey = "Face";
            nodeDoubleFace.Name = "DefaultMeas_DoubleFace_";
            nodeDoubleFace.State = ENUM.StateType.Normal;
            ColorNodeBaseOnState(nodeDoubleFace);
            if (p._DefaultDoubleFace)
            {
                nodeDoubleFace.Text = R.StringTsuNode_Length_DoubleFace;
            }
            else
            {
                nodeDoubleFace.Text = R.StringTsuNode_Length_OneFace;
            }
            this.Nodes.Add(nodeDoubleFace);
            // measure number
            {
                TsuNode nodeNbreMeas = new TsuNode();
                nodeNbreMeas.ImageKey = "NbreMeas";
                nodeNbreMeas.SelectedImageKey = "NbreMeas";
                nodeNbreMeas.Name = "DefaultMeas_NbreMeas_";
                nodeNbreMeas.State = ENUM.StateType.Normal;
                ColorNodeBaseOnState(nodeNbreMeas);
                nodeNbreMeas.Text = R.StringTsuNode_Length_NbreMeas + p._DefaultNumberMeas.ToString();
                this.Nodes.Add(nodeNbreMeas);
            }
        }
        public void BasedOn(Level.Station.View st)
        //ajout des propriétés pour les stations de nivellement
        {
            Level.Station.Parameters p = st.Module._WorkingStationLevel._Parameters;
            this.DressAs(R.StringTsuNode_Level_Admin, "", p);
            this.Name = "Admin_Main_";
            this.AddNodeAs(R.StringTsuNode_Level_Date + ": ", p._Date.ToString(), p, ENUM.StateType.Normal);
            this.LastNode.Name = "Admin_Date_";
            this.LastNode.ImageKey = "Date";
            this.LastNode.SelectedImageKey = "Date";
            ENUM.StateType stateTeam = (p._Team == R.String_UnknownTeam) ? ENUM.StateType.ToBeFilled : ENUM.StateType.Normal;
            this.AddNodeAs(R.StringTsuNode_Level_Team + ": ", p._Team.ToString(), p, stateTeam);
            this.LastNode.Name = "Admin_Team_";
            this.LastNode.ImageKey = "Team";
            this.LastNode.SelectedImageKey = "Team";
            ENUM.StateType stateOp = (!p._Operation.IsSet) ? ENUM.StateType.ToBeFilled : ENUM.StateType.Normal;
            this.AddNodeAs(R.StringTsuNode_Level_Operation + ":", p._Operation.ToString(), p, stateOp);
            this.LastNode.Name = "Admin_OperationID_";
            this.LastNode.ImageKey = "Operation";
            this.LastNode.SelectedImageKey = "Operation";
            if (p._Instrument != null && p._Instrument._Model != null)
                this.AddNodeAs(R.StringTsuNode_Level_Instrument + ":", p._Instrument._Model + " sn" + p._Instrument._SerialNumber, null, ENUM.StateType.Normal);
            else
            {
                this.AddNodeAs(R.StringTsuNode_Level_Instrument + ":", R.StringTsuNode_Level_NA, null, ENUM.StateType.ToBeFilled);
                //(this.LastNode as TsuNode).State = ENUM.StateType.ToBeFilled;
            }
            //ColorNodeBaseOnState(this.LastNode as TsuNode);
            this.LastNode.Name = "Admin_Instrument_";
            this.LastNode.ImageKey = p._Instrument._InstrumentClass.ToString();
            this.LastNode.SelectedImageKey = p._Instrument._InstrumentClass.ToString();
            this.AddNodeAs(R.StringTsuNode_Line_Temperature + ":", p._Temperature.ToString("F1", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " °C", this.Tag as TsuObject, ENUM.StateType.Normal);
            this.LastNode.ImageKey = "Temperature";
            this.LastNode.SelectedImageKey = "Temperature";
            this.LastNode.Name = "Admin_Temperature_";
            this.AddNodeAs(R.StringTsuNode_Level_ToleranceNode + ":", Math.Round(p._Tolerance * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " mm", this.Tag as TsuObject);
            this.LastNode.ImageKey = "Tolerance";
            this.LastNode.SelectedImageKey = "Tolerance";
            this.LastNode.Name = "Admin_Tolerance_";
            if (st.showCoordType)
            {
                this.AddNodeAs(p._ZType);
                this.LastNode.Name = "Admin_CoordType_";
            }
            if (st.showNodeObsoleteMeas)
            {
                TsuNode nodeShowObsoleteMeas = new TsuNode();
                if (p.showObsoleteMeas == true)
                {
                    nodeShowObsoleteMeas.ImageKey = "Checked";
                    nodeShowObsoleteMeas.SelectedImageKey = "Checked";
                }
                else
                {
                    nodeShowObsoleteMeas.ImageKey = "NotChecked";
                    nodeShowObsoleteMeas.SelectedImageKey = "NotChecked";
                }
                nodeShowObsoleteMeas.Name = "Admin_ShowObsoleteMeas_";
                nodeShowObsoleteMeas.State = ENUM.StateType.Normal;
                ColorNodeBaseOnState(nodeShowObsoleteMeas);
                nodeShowObsoleteMeas.Text = R.StringTsuNode_Level_ShowObsoleteMeas;
                this.Nodes.Add(nodeShowObsoleteMeas);
            }

        }
        /// <summary>
        /// Crée le noeud résultat computation pour le treeview du nivellement
        /// </summary>
        /// <param name="st"></param>
        public void ComputationBasedOn(Level.Station.View st)
        {
            Level.Station.Parameters p = st.Module._WorkingStationLevel._Parameters;
            this.Name = "Emq__";
            this.Text = string.Format("{0}", R.StringTsuNode_Computation);
            this.State = ENUM.StateType.Normal;
            ColorNodeBaseOnState(this);
            if (p._HStation != na)
            {
                AddNodeAs(R.StringTsuNode_Level_HStation + ":", Math.Round(p._HStation, 5).ToString("F5", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " m", this.Tag as TsuObject, ENUM.StateType.Normal);
            }
            else
            {
                AddNodeAs(R.StringTsuNode_Level_HStation + ":", R.StringTsuNode_Level_NA, this.Tag as TsuObject, ENUM.StateType.Normal);
            }
            this.LastNode.Name = "Emq_HStation_";
            // Emq station sur point de CALA
            if (p._EmqCala != na)
            {
                this.AddNodeAs(R.StringTsuNode_Level_EmqStation + ":", Math.Round(p._EmqCala * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " mm", this.Tag as TsuObject, ENUM.StateType.Normal);
            }
            else
            {
                this.AddNodeAs(R.StringTsuNode_Level_EmqStation + ":", R.StringTsuNode_Level_NA, this.Tag as TsuObject, ENUM.StateType.Normal);
            }
            this.LastNode.Name = "Emq_Station_";
            if (Math.Abs(p._EmqCala * 1000) <= 0.07)
            {
                (this.LastNode as TsuNode).State = ENUM.StateType.Ready;
            }
            else
            {
                if (p._EmqCala != na)
                {
                    (this.LastNode as TsuNode).State = ENUM.StateType.NotReady;
                }
            }
            ColorNodeBaseOnState(this.LastNode as TsuNode);
            if (p._LevelingDirection == TSU.ENUM.LevelingDirection.RETOUR || p._LevelingDirection == TSU.ENUM.LevelingDirection.REPRISE)
            {
                //Ajout Emq Aller dans le treeview
                if (p._EmqAller != na)
                {
                    this.AddNodeAs(R.StringTsuNode_Level_EmqAller + ":", Math.Round(p._EmqAller * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " mm", this.Tag as TsuObject, ENUM.StateType.Normal);
                }
                else
                {
                    this.AddNodeAs(R.StringTsuNode_Level_EmqAller + ":", R.StringTsuNode_Level_NA, this.Tag as TsuObject, ENUM.StateType.Normal);
                }
                this.LastNode.Name = "Emq_Aller_";
                if (Math.Abs(p._EmqAller * 1000) <= 0.07)
                {
                    (this.LastNode as TsuNode).State = ENUM.StateType.Ready;
                }
                else
                {
                    if (p._EmqAller != na)
                    {
                        (this.LastNode as TsuNode).State = ENUM.StateType.NotReady;
                    }
                }
                ColorNodeBaseOnState(this.LastNode as TsuNode);
            }
            if (p._LevelingDirection == TSU.ENUM.LevelingDirection.REPRISE)
            {
                //Ajout Emq Retour dans le treeview
                if (p._EmqRetour != na)
                {
                    this.AddNodeAs(R.StringTsuNode_Level_EmqRetour + ":", Math.Round(p._EmqRetour * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " mm", this.Tag as TsuObject, ENUM.StateType.Normal);
                }
                else
                {
                    this.AddNodeAs(R.StringTsuNode_Level_EmqRetour + ":", R.StringTsuNode_Level_NA, this.Tag as TsuObject, ENUM.StateType.Normal);
                }
                this.LastNode.Name = "Emq_Retour_";
                if (Math.Abs(p._EmqRetour * 1000) <= 0.07)
                {
                    (this.LastNode as TsuNode).State = ENUM.StateType.Ready;
                }
                else
                {
                    if (p._EmqRetour != na)
                    {
                        (this.LastNode as TsuNode).State = ENUM.StateType.NotReady;
                    }
                }
                ColorNodeBaseOnState(this.LastNode as TsuNode);
            }
            if (st.showCalculationOption)
            {
                TsuNode node_CalculationOption = new TsuNode();
                this.Nodes.Add(node_CalculationOption);
                node_CalculationOption.Name = "Emq_Strategy_";
                node_CalculationOption.Text = string.Format("{0} {1}", R.StringTsuNode_Line_CalculationStrategy, st.Module._LevelingComputationStrategy._name);
                node_CalculationOption.State = ENUM.StateType.Normal;
                ColorNodeBaseOnState(node_CalculationOption);
                if (st.Module._LevelingComputationStrategy is Level.Station.LGC2Strategy)
                {
                    node_CalculationOption.ImageKey = "LGC2CalculationOption";
                    node_CalculationOption.SelectedImageKey = "LGC2CalculationOption";
                    TsuNode nodeShowOutput = new TsuNode()
                    {
                        ImageKey = "Run",
                        SelectedImageKey = "Run",
                        Name = "Emq_Strategy_ShowLGC",
                        State = ENUM.StateType.Normal
                    };

                    //if (p._ShowLGC2Files == true)
                    //{
                    //    nodeShowOutput.ImageKey = "Checked";
                    //    nodeShowOutput.SelectedImageKey = "Checked";
                    //}
                    //else
                    //{
                    //    nodeShowOutput.ImageKey = "NotChecked";
                    //    nodeShowOutput.SelectedImageKey = "NotChecked";
                    //}
                    ColorNodeBaseOnState(nodeShowOutput);
                    nodeShowOutput.Text = R.StringTsuNode_Level_ShowLGCFile;
                    node_CalculationOption.Nodes.Add(nodeShowOutput);
                    // option qui ne fonctionne pas encore.
                    //TsuNode nodeUseDist = new TsuNode();
                    //if (p._UseDistanceForLGC2 == true)
                    //{
                    //    nodeUseDist.ImageKey = "Checked";
                    //    nodeUseDist.SelectedImageKey = "Checked";
                    //}
                    //else
                    //{
                    //    nodeUseDist.ImageKey = "NotChecked";
                    //    nodeUseDist.SelectedImageKey = "NotChecked";
                    //}
                    //nodeUseDist.Name = "Emq_Strategy_UseDist";
                    //nodeUseDist.State = ENUM.StateType.Normal;
                    //ColorNodeBaseOnState(nodeUseDist);
                    //nodeUseDist.Text = R.StringTsuNode_Level_UseDist;
                    //node_CalculationOption.Nodes.Add(nodeUseDist);
                }
                if (st.Module._LevelingComputationStrategy is Level.Station.SFBStrategy)
                {
                    node_CalculationOption.ImageKey = "SFBCalculationOption";
                    node_CalculationOption.SelectedImageKey = "SFBCalculationOption";
                }
            }
        }
        /// <summary>
        /// Ajout parametre pour station tilt
        /// </summary>
        /// <param name="tiltParam"></param>
        public void BasedOn(Tilt.Station.Parameters tiltParam, TSU.Common.Operations.Operation initOpID, TSU.Common.Operations.Operation adjustingOpID, bool showInitOP = true, bool showAdjustingOP = true)

        {
            this.DressAs(R.StringTsuNode_Tilt_Admin, "", tiltParam);
            this.Name = "Admin_Main_";
            this.AddNodeAs(R.StringTsuNode_Tilt_Date + ": ", tiltParam._Date.ToString(), tiltParam, ENUM.StateType.Normal);
            this.LastNode.Name = "Admin_Date_";
            this.LastNode.ImageKey = "Date";
            this.LastNode.SelectedImageKey = "Date";
            ENUM.StateType stateTeam = (tiltParam._Team == R.String_UnknownTeam) ? ENUM.StateType.ToBeFilled : ENUM.StateType.Normal;
            this.AddNodeAs(R.StringTsuNode_Tilt_Team + ": ", tiltParam._Team.ToString(), tiltParam, stateTeam);
            this.LastNode.Name = "Admin_Team_";
            this.LastNode.ImageKey = "Team";
            this.LastNode.SelectedImageKey = "Team";
            if (showInitOP)
            {
                ENUM.StateType stateInitOp = (!initOpID.IsSet) ? ENUM.StateType.ToBeFilled : ENUM.StateType.Normal;
                this.AddNodeAs(R.StringTsuNode_Tilt_OpInit + ":", initOpID.ToString(), tiltParam, stateInitOp);
                this.LastNode.Name = "Initial_OperationID_";
                this.LastNode.ImageKey = "Operation";
                this.LastNode.SelectedImageKey = "Operation";
            }
            if (showAdjustingOP)
            {
                ENUM.StateType stateAdjOp = (!adjustingOpID.IsSet) ? ENUM.StateType.ToBeFilled : ENUM.StateType.Normal;
                this.AddNodeAs(R.StringTsuNode_Tilt_OpAjusting + ":", adjustingOpID.ToString(), tiltParam, stateAdjOp);
                this.LastNode.Name = "Adjusting_OperationID_";
                this.LastNode.ImageKey = "Operation";
                this.LastNode.SelectedImageKey = "Operation";
            }
            if (tiltParam._Instrument != null && tiltParam._Instrument._Model != null)
                this.AddNodeAs(R.StringTsuNode_Tilt_Instrument + ":", tiltParam._Instrument._Model + " sn" + tiltParam._Instrument._SerialNumber, null, ENUM.StateType.Normal);
            else
            {
                this.AddNodeAs(R.StringTsuNode_Tilt_Instrument + ": ", R.StringTsuNode_Tilt_ToBeSelected, null, ENUM.StateType.ToBeFilled);
                //(this.LastNode as TsuNode).State = ENUM.StateType.ToBeFilled;
            }
            //ColorNodeBaseOnState(this.LastNode as TsuNode);
            this.LastNode.Name = "Admin_Instrument_";
            this.LastNode.ImageKey = tiltParam._Instrument._InstrumentClass.ToString();
            this.LastNode.SelectedImageKey = tiltParam._Instrument._InstrumentClass.ToString();
            this.AddNodeAs(R.StringTsuNode_Tilt_ToleranceNode + ":", Math.Round(tiltParam._Tolerance * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " mRad", this.Tag as TsuObject, ENUM.StateType.Normal);
            this.LastNode.Name = "Admin_Tolerance_";
            this.LastNode.ImageKey = "Tolerance";
            this.LastNode.SelectedImageKey = "Tolerance";
        }
        public void BasedOn(Common.Station s)
        {
            if (s != null)
            {
                this.Tag = s;
            }
            this.Name = s._Name;
            this.Text = this.Name;
            this.ImageKey = "Station";
            this.SelectedImageKey = "Station";
            foreach (var item in s.MeasuresTaken)
            {
                this.Nodes.Add(new TsuNode(item));
            }

        }
        public void BasedOn(Polar.Station s)
        {
            this.BasedOn(s as Common.Station);
            foreach (TsuNode n in this.Nodes)
            {
                if ((n.Tag as Polar.Measure).Distance.Reflector != null)
                    n.AddNodeAs(StationProperties.NextReflector, " = " + (n.Tag as Polar.Measure).Distance.Reflector.Id, n.Tag as TsuObject);
                else
                    n.AddNodeAs(StationProperties.NextReflector, " = To be selected", n.Tag as TsuObject);
                n.AddNodeAs(StationProperties.Extension, " = " + (n.Tag as Polar.Measure).Extension.Value.ToString() + " m", n.Tag as TsuObject);
                n.AddNodeAs(StationProperties.NextFace, " = " + (n.Tag as Polar.Measure).Face.ToString(), n.Tag as TsuObject);
                n.AddNodeAs(StationProperties.NextComment, " = " + (n.Tag as Polar.Measure).Comment.ToString(), n.Tag as TsuObject);
                n.AddNodeAs(StationProperties.Status, " = " + (n.Tag as Polar.Measure)._Status._Name, n.Tag as TsuObject);
            }
        }

        #region treeview tilt
        ///// <summary>
        ///// Cree le noeud avec tous les paramètres pour la station tilt
        ///// </summary>
        ///// <param name="stationTiltParameters"></param>
        ///// <param name="point"></param>
        //internal void BasedOn(StationTiltParameters stationTiltParameters, E.Point point)
        ////Cree le noeud avec tous les paramètres pour la station tilt
        //{
        //    this.BasedOn(stationTiltParameters);
        //}
        /// <summary>
        /// cree le noeud pour la station tilt
        /// </summary>
        /// <param name="stationTilt"></param>
        //internal void BasedOn(StationTilt stationTilt)
        //// cree le noeud pour la station tilt
        //{
        //    string prefix;
        //    switch (stationTilt.TiltParameters._TiltInitOrAdjusting)
        //    {
        //        case TiltInitOrAdjusting.Initial:
        //            prefix = "Initial_";
        //            Name = prefix+"NodeMeasure_";
        //            Text =R.StringTsuNode_Tilt_MesInit;
        //            Tag = prefix+"NodeMeasure";
        //            break;
        //        case TiltInitOrAdjusting.Adjusting:
        //            prefix = "Adjusting_";
        //            Name = prefix+"NodeMeasure_";
        //            Text =R.StringTsuNode_Tilt_MesAdj;
        //            Tag = prefix+"NodeMeasure";
        //            break;
        //        default:
        //            prefix = "Initial_";
        //            Name = prefix+"NodeMeasure_";
        //            Text =R.StringTsuNode_Tilt_MesInit;
        //            Tag = prefix+"NodeMeasure";
        //            break;
        //    }
        //    /// N'affiche la fin du treeview de mesure que si un point theo est sélectionné
        //    if (stationTilt.TheoPoint._Name !=R.String_Unknown)
        //    {
        //        this.AddNodeAs(R.StringTsuNode_Tilt_Comment, ": " + stationTilt.TiltParameters._Comment, this.Tag as TsuObject, ENUM.StateType.Normal);
        //        this.Nodes[0].Name = prefix+"Comment_ " ;
        //        string tiltMoy = "";
        //        string emqTiltMoy = "";
        //        string ecartTilt = "";
        //        string theoBeamReading = "";
        //        if (stationTilt._MeasureAverageBothDirection.Value == 9999)
        //        {
        //            tiltMoy =R.T_NA;
        //            emqTiltMoy =R.T_NA;
        //        }
        //        else
        //        {
        //            tiltMoy = Math.Round(stationTilt._MeasureAverageBothDirection.Value * 1000, 3).ToString("F3", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " mRad";
        //            emqTiltMoy = Math.Round(stationTilt._EcartTheo.Sigma * 1000, 3).ToString("F3", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " mRad";
        //        }
        //        if (stationTilt._EcartTheo.Value == 9999||stationTilt._MeasuresTilt.Count>1)
        //        {
        //            theoBeamReading =R.T_NA;
        //        }
        //        else
        //        {
        //            theoBeamReading = Math.Round((stationTilt._MeasureAverageBeamDirection.Value - stationTilt._EcartTheo.Value) * 1000, 3).ToString("F3", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " mRad";
        //        }
        //        if (stationTilt._EcartTheo.Value == 9999)
        //        {
        //            ecartTilt =R.T_NA;
        //        }
        //        else
        //        {
        //            ecartTilt = Math.Round(stationTilt._EcartTheo.Value * 1000, 3).ToString("F3", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " mRad";
        //        }
        //        if (stationTilt._MeasuresTilt.Count>1)
        //        {
        //            this.AddNodeAs(R.StringTsuNode_Tilt_Average + ":", tiltMoy, null, ENUM.StateType.Normal);
        //            this.LastNode.Name = prefix + "AverageTilt_ ";
        //        }
        //        this.AddNodeAs(R.StringTsuNode_Tilt_Ecart+":", ecartTilt, null, ENUM.StateType.Normal);
        //        this.LastNode.Name = prefix+"EcartTilt_ ";
        //        if (stationTilt._MeasureAverageBothDirection.Value == 9999)
        //        {
        //        }
        //        else
        //        {
        //            if (Math.Abs(stationTilt._EcartTheo.Value) >= stationTilt.TiltParameters._Tolerance)
        //            {
        //                (this.LastNode as TsuNode).State = ENUM.StateType.NotReady;
        //            }
        //            else
        //            {
        //                (this.LastNode as TsuNode).State = ENUM.StateType.Ready;
        //            }
        //        }
        //        ColorNodeBaseOnState(this.LastNode as TsuNode);
        //        //Met en rouge ou vert l'emq moyen de la station tilt
        //        ENUM.StateType stateEmq = ENUM.StateType.Normal;
        //        if (emqTiltMoy!=R.T_NA)
        //        {
        //                stateEmq = (stationTilt._EcartTheo.Sigma * 1000 <= 0.05) ? ENUM.StateType.Ready : ENUM.StateType.NotReady;
        //        }
        //        if (stationTilt._MeasuresTilt.Count>1)
        //        {
        //            AddNodeAs(R.StringTsuNode_Tilt_emq + ":", emqTiltMoy, null, stateEmq);
        //            this.LastNode.Name = prefix + "EmqTilt_ ";
        //        }
        //        AddNodeAs(R.StringTsuNode_Tilt_TheoBeamDirection + ":", theoBeamReading, null, ENUM.StateType.Normal);
        //        this.LastNode.Name = prefix + "TheoBeam_ ";
        //        int nbMes = 0;
        //        foreach (MeasureOfTilt mesTilt in stationTilt._MeasuresTilt)
        //        {
        //            AddNodeAs(mesTilt, nbMes, prefix);
        //            this.LastNode.Name = prefix+"Measure_" + nbMes;
        //            this.LastNode.Text = String.Format("{0} {1}",R.StringTsuNode_Tilt_Mesure,(nbMes + 1));
        //            nbMes++;
        //        }
        //    }

        //}
        /// <summary>
        /// Ajoute le noeud pour une mesure de tilt
        /// </summary>
        /// <param name="measureOfTilt"></param>
        //private void AddNodeAs(MeasureOfTilt measureOfTilt, int nbMes, string prefix="")
        ////Ajoute le noeud pour une mesure de tilt
        //{
        //    TsuNode nodeMesTilt = new TsuNode();
        //    string tiltBeam = "";
        //    string tiltOpposite = "";
        //    string averageBothDirection = "";
        //    tiltBeam = (measureOfTilt._ReadingBeamDirection.Value == na) ?R.StringTsuNode_Tilt_DoubleClickToEnterValue : Math.Round(measureOfTilt._ReadingBeamDirection.Value * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " mRad";
        //    tiltOpposite = (measureOfTilt._ReadingOppositeBeam.Value == na) ?R.StringTsuNode_Tilt_DoubleClickToEnterValue : Math.Round(measureOfTilt._ReadingOppositeBeam.Value * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " mRad";
        //    averageBothDirection = (measureOfTilt._AverageBothDirection.Value == 9999) ?R.T_NA : Math.Round(measureOfTilt._AverageBothDirection.Value * 1000, 3).ToString("F3", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " mRad";
        //    nodeMesTilt.AddNodeAs(R.StringTsuNode_Tilt_BeamDirection,": "+ tiltBeam, null, ENUM.StateType.Normal);
        //    nodeMesTilt.Nodes[0].Name = prefix+"MeasTiltBeam_"+nbMes;
        //    nodeMesTilt.LastNode.BackColor = Tsunami2.Preferences.Theme._Colors.ActionToDo;// ( measureOfTilt._ReadingBeamDirection.Value == na) ? Tsunami2.Preferences.Theme._Colors.ActionToDo: Tsunami2.Preferences.Theme._Colors.Good ;
        //    nodeMesTilt.AddNodeAs(R.StringTsuNode_Tilt_OppositeBeam, ": " + tiltOpposite, null, ENUM.StateType.Normal);
        //    nodeMesTilt.LastNode.BackColor = Tsunami2.Preferences.Theme._Colors.ActionToDo; // (measureOfTilt._ReadingOppositeBeam.Value == na) ? Tsunami2.Preferences.Theme._Colors.ActionToDo : Tsunami2.Preferences.Theme._Colors.Good;
        //    nodeMesTilt.Nodes[1].Name = prefix+"MeasTiltOpposite_"+nbMes;
        //    nodeMesTilt.AddNodeAs(R.StringTsuNode_Tilt_Average_Both_Direction, ": " + averageBothDirection, null, ENUM.StateType.Normal);
        //    nodeMesTilt.Nodes[2].Name = prefix+"MeasAverageBothDirection_"+nbMes;
        //    this.Nodes.Add(nodeMesTilt);
        //}
        /// <summary>
        /// Crée un noeud avec toute la séquence de station tilt sélectionnée
        /// </summary>
        /// <param name="stationTilt"></param>
        //internal void BaseOn(List<StationTilt> listStationTilt, TSU.Common.Elements.Point actualPoint)
        //{
        //    double na =TSU.Tsunami2.TsunamiPreferences.Values.na;
        //    Name = "Node_Element_";
        //    Text = "Element";
        //    Tag = "NodeElement";
        //    TsuNode nameNode = new TsuNode();
        //    nameNode.Name = "Node_Element_Point";

        //    nameNode.State = (actualPoint._Name ==R.String_Unknown) ? ENUM.StateType.ToBeFilled : ENUM.StateType.Normal;
        //    nameNode.ImageKey = "Point";
        //    nameNode.SelectedImageKey = "Point";
        //    nameNode.Text = String.Format("{0}: {1}",R.StringTsuNode_Tilt_Point, actualPoint._Name);
        //    ColorNodeBaseOnState(nameNode);
        //    this.Nodes.Add(nameNode);
        //    this.Nodes[0].Name = "Node_Element_PointName";
        //    TsuNode tiltTheoNode = new TsuNode();
        //    tiltTheoNode.Name = "Node_Element_TiltTheo";
        //    tiltTheoNode.State = (tiltTheoNode.Text == "Unknown") ? ENUM.StateType.NotReady : ENUM.StateType.Normal; 
        //    ColorNodeBaseOnState(tiltTheoNode);
        //    tiltTheoNode.ImageKey = "TiltTheo";
        //    tiltTheoNode.SelectedImageKey = "TiltTheo";
        //    if (actualPoint._Parameters.Tilt == na)
        //    {
        //        tiltTheoNode.Text = String.Format("{0}: {1}",R.StringTsuNode_Tilt_Theo,R.T_NA);
        //    }
        //    else
        //    {
        //        tiltTheoNode.Text = String.Format("{0}: {1} mRad",R.StringTsuNode_Tilt_Theo, Math.Round(actualPoint._Parameters.Tilt * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")));
        //    }
        //    this.Nodes.Add(tiltTheoNode);
        //    this.Nodes[1].Name = "Node_Element_TiltTheo";
        //    TsuNode sequenceNode = new TsuNode();
        //    sequenceNode.Name = "Node_Sequence_Main";
        //    sequenceNode.Text = R.StringTsuNode_Sequence;
        //    sequenceNode.Tag = "NodeSequence";
        //    foreach (StationTilt stationTilt in listStationTilt)
        //    {
        //            if (stationTilt.TiltParameters._TiltInitOrAdjusting == TSU.ENUM.TiltInitOrAdjusting.Initial)
        //        {
        //            TsuNode newNode = new TsuNode();

        //            ColorNodeBaseOnState(newNode);
        //            newNode.ImageKey = "Point";
        //            newNode.SelectedImageKey = "Point";
        //            newNode.Text = stationTilt.TheoPoint._Name;
        //            newNode.State = ENUM.StateType.Normal;
        //            newNode.Name = "Node_Sequence_" + stationTilt.TheoPoint._Name;
        //            CultureInfo ci = CultureInfo.InvariantCulture;
        //            if (stationTilt.ParametersBasic.LastSaved!=DateTime.MinValue)
        //            {
        //                newNode.Text += String.Format("{0} {1}",R.StringTsuNode_Tilt_Init, stationTilt.ParametersBasic.LastSaved.ToString("HH:mm:ss", ci));
        //                newNode.ImageKey = "TiltInitSaved";
        //                newNode.SelectedImageKey = "TiltInitSaved";
        //                if (actualPoint._Name==stationTilt.TheoPoint._Name)
        //                {
        //                    nameNode.ImageKey = "TiltInitSaved";
        //                    nameNode.SelectedImageKey = "TiltInitSaved";
        //                    nameNode.Text += String.Format("{0} {1}",R.StringTsuNode_Tilt_Init, stationTilt.ParametersBasic.LastSaved.ToString("HH:mm:ss", ci));
        //                }
        //            }
        //            StationTilt stationTiltAdj = new StationTilt();
        //            stationTiltAdj = listStationTilt.Find(x => x.TheoPoint._Name == stationTilt.TheoPoint._Name && x.TiltParameters._TiltInitOrAdjusting == TiltInitOrAdjusting.Adjusting);
        //            if (stationTiltAdj != null)
        //            {
        //                if (stationTiltAdj.ParametersBasic.LastSaved != DateTime.MinValue)
        //                {

        //                    newNode.Text += String.Format("{0} {1}",R.StringTsuNode_Tilt_Regl, stationTiltAdj.ParametersBasic.LastSaved.ToString("HH:mm:ss", ci));
        //                    newNode.ImageKey = stationTilt.ParametersBasic.LastSaved != DateTime.MinValue ? "TiltBothSaved" : "TiltAdjSaved";
        //                    newNode.SelectedImageKey = stationTilt.ParametersBasic.LastSaved != DateTime.MinValue ? "TiltBothSaved" : "TiltAdjSaved";
        //                    if (actualPoint._Name == stationTilt.TheoPoint._Name)
        //                    {
        //                        nameNode.Text += String.Format("{0} {1}",R.StringTsuNode_Tilt_Regl, stationTiltAdj.ParametersBasic.LastSaved.ToString("HH:mm:ss", ci));
        //                        nameNode.ImageKey = stationTilt.ParametersBasic.LastSaved != DateTime.MinValue ? "TiltBothSaved" : "TiltAdjSaved";
        //                        nameNode.SelectedImageKey = stationTilt.ParametersBasic.LastSaved != DateTime.MinValue ? "TiltBothSaved" : "TiltAdjSaved";
        //                    }
        //                }
        //            }  
        //            sequenceNode.Nodes.Add(newNode);
        //        }  
        //    }
        //    this.Nodes.Add(sequenceNode);
        //}
        #endregion
        #endregion

        #endregion
    }
}
