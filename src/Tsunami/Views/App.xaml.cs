﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace TSU.Views
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();
            this.DispatcherUnhandledException += OnException;
            this.Exit += OnExit;
        }

        private void OnException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            System.Windows.Forms.MessageBox.Show(e.Exception.Message + "\r\n" + "\r\n" + e.Exception.StackTrace);
            e.Handled = true;
        }

        private void OnExit(object sender, ExitEventArgs e)
        {
            Debug.WriteInConsole($"WPF Application exited exitCode={e.ApplicationExitCode}.");
        }

        public static void EnsureApplicationResources()
        {
            if (Current == null)
            {
                Debug.WriteInConsole($"WPF Application created.");
                // create the Application object
                new App() { ShutdownMode = ShutdownMode.OnExplicitShutdown };
            }
            else
            {
                Debug.WriteInConsole($"WPF Application was already existing.");
                Debug.WriteInConsole($"HasShutdownFinished={Current.Dispatcher.HasShutdownFinished}");
                Debug.WriteInConsole($"HasShutdownStarted={Current.Dispatcher.HasShutdownStarted}");
            }
        }
    }
}
