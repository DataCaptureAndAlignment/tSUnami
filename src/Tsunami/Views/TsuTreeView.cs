﻿using System;
using System.Windows.Forms;

class TsuTreeView : TreeView
{
    //public bool IsUpdating = true;
    public TsuTreeView()
    {
        this.Dock = DockStyle.Fill;
        this.ItemHeight = 32;
        this.Font = TSU.Tsunami2.Preferences.Theme.Fonts.Normal;
        this.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
        this.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
    }
}
