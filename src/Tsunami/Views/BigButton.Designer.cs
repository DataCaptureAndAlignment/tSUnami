﻿using System;

namespace TSU.Views
{
    partial class BigButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(() =>
            {
                try
                {
                    if (this.BB_Title != null)
                    {
                        this.BB_Title.Font = null;
                    }

                    if (this.BB_Description != null)
                    {
                        this.BB_Description.Font = null;
                    }
                }
                catch (Exception)
                {
                    //Ignore all Exceptions
                    ;
                }
            });


            this.Paint -= OnPaint;
            this.BigButtonClicked -= bigButtonClickedEventHandler;

            RemoveEventsToControl(BB_Picture);
            RemoveEventsToControl(BB_Title);
            RemoveEventsToControl(BB_Description);
            if (BB_subMenuArrow != null) RemoveEventsToControl(BB_subMenuArrow);


            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BB_Picture = new System.Windows.Forms.PictureBox();
            this.BB_Title = new System.Windows.Forms.Label();
            this.BB_Description = new System.Windows.Forms.Label();
            this.buttonForFocus = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.BB_Picture)).BeginInit();
            this.SuspendLayout();
            // 
            // BB_Picture
            // 
            this.BB_Picture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BB_Picture.Dock = System.Windows.Forms.DockStyle.Left;
            this.BB_Picture.Location = new System.Drawing.Point(3, 3);
            this.BB_Picture.Margin = new System.Windows.Forms.Padding(5);
            this.BB_Picture.Name = "BB_Picture";
            this.BB_Picture.Size = new System.Drawing.Size(62, 61);
            this.BB_Picture.TabIndex = 1;
            this.BB_Picture.TabStop = false;
            // 
            // BB_Title
            // 
            this.BB_Title.AutoSize = true;
            this.BB_Title.BackColor = System.Drawing.Color.Transparent;
            this.BB_Title.Dock = System.Windows.Forms.DockStyle.Top;
            this.BB_Title.ForeColor = System.Drawing.Color.Black;
            this.BB_Title.Location = new System.Drawing.Point(65, 3);
            this.BB_Title.Margin = new System.Windows.Forms.Padding(5);
            this.BB_Title.Name = "BB_Title";
            this.BB_Title.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BB_Title.Size = new System.Drawing.Size(39, 13);
            this.BB_Title.TabIndex = 2;
            this.BB_Title.Text = "Title p";
            // 
            // BB_Description
            // 
            this.BB_Description.BackColor = System.Drawing.Color.Transparent;
            this.BB_Description.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BB_Description.ForeColor = System.Drawing.Color.Black;
            this.BB_Description.Location = new System.Drawing.Point(65, 16);
            this.BB_Description.Margin = new System.Windows.Forms.Padding(5);
            this.BB_Description.Name = "BB_Description";
            this.BB_Description.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.BB_Description.Size = new System.Drawing.Size(255, 48);
            this.BB_Description.TabIndex = 3;
            this.BB_Description.Text = "description description description description description description descripti" +
    "on description description description description description description ";
            // 
            // buttonForFocus
            // 
            this.buttonForFocus.Location = new System.Drawing.Point(14, -27);
            this.buttonForFocus.Name = "buttonForFocus";
            this.buttonForFocus.Size = new System.Drawing.Size(51, 23);
            this.buttonForFocus.TabIndex = 4;
            this.buttonForFocus.Text = "button4Focus";
            this.buttonForFocus.UseVisualStyleBackColor = true;
            this.buttonForFocus.Click += new System.EventHandler(this.buttonForFocus_Click);
            this.buttonForFocus.Enter += new System.EventHandler(this.buttonForFocus_Enter);
            this.buttonForFocus.KeyUp += new System.Windows.Forms.KeyEventHandler(this.buttonForFocus_KeyUp);
            this.buttonForFocus.Leave += new System.EventHandler(this.buttonForFocus_Leave);
            // 
            // BigButton
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.BackColor = System.Drawing.Color.White;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.buttonForFocus);
            this.Controls.Add(this.BB_Description);
            this.Controls.Add(this.BB_Title);
            this.Controls.Add(this.BB_Picture);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.ImeMode = System.Windows.Forms.ImeMode.On;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "BigButton";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.Size = new System.Drawing.Size(323, 67);
            this.SizeChanged += new System.EventHandler(this.BigButton_SizeChanged);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.BigButton_MouseDown);
            this.MouseEnter += new System.EventHandler(this.BigButton_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.BigButton_MouseLeave);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.BigButton_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.BB_Picture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal System.Windows.Forms.Label BB_Title;
        internal System.Windows.Forms.Label BB_Description;
        internal System.Windows.Forms.PictureBox BB_Picture;
        private System.Windows.Forms.Button buttonForFocus;
    }
}
