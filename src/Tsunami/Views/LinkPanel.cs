﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace TSU.Views
{
    /// <summary>
    /// LinkPanel p = new LinkPanel() {TSU.Tsunami2.TsunamiPreferences.Theme.Fonts.Large, Dock = DockStyle.Top };
    /// p.Add("you can change reflector $here%0$ this is a test $bye%2$.", new List<Action>() { a, b, c
    /// </summary>
    public class LinkPanel : Panel
    {
        List<Label> Labels;
        bool firstPass = true;

        public LinkPanel():base()
        {
            Labels = new List<Label>();
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            if (firstPass)
            {
                int neededHeight = PlaceTheLabels();
                firstPass = false;
                this.Height = neededHeight;
            }
            else
            {
                firstPass = true;
            }

        }

        /// <summary>
        /// return the panel needed height
        /// </summary>
        /// <returns></returns>
        private int PlaceTheLabels()
        {
            int actualLeft = 0;
            int actualLine = 0;
            int panelNewWidth = this.Width;

            int ActualTop = 0;
            int lineHeight = 0;


            int uselessOffset = (int) this.Font.Size / 4;


            // lets replaced the labels
            foreach (var item in Labels)
            {
                Size size = TextRenderer.MeasureText(item.Text, item.Font);
                if (item.Text!="")
                    lineHeight = (size.Height + 5);

                if (actualLeft != 0 && actualLeft + size.Width - uselessOffset > panelNewWidth)
                {
                    actualLine++;
                    actualLeft = 0;
                }

                ActualTop = lineHeight * (actualLine);

                item.Top = ActualTop;
                item.Left = actualLeft;

                actualLeft += size.Width -uselessOffset;
            }
            return ActualTop + lineHeight;
        }

        public void Add(string s, List<Action> actions)
        {
            List<string> texts = new List<string>();
            List<int> actionIndexes = new List<int>();

            string[] parts = s.Split('$');
            //if (parts.Length % 2 !=0)
            foreach (var item in parts)
            {
                string[] elements = item.Split('%');
                if (elements.Length != 2)
                {
                    texts.Add(elements[0]);
                    actionIndexes.Add(-1);
                }
                else
                {
                    texts.Add(elements[0]);
                    if (int.TryParse(elements[1], out int i))
                        actionIndexes.Add(i);
                    else
                        actionIndexes.Add(-1);
                }
            }
            
            for (int i = 0; i < texts.Count; i++)
            {
                if (actionIndexes[i] == -1)
                {
                    Label l = new Label() { Text = texts[i].Trim(), AutoSize = true, Font = this.Font };
                    Labels.Add(l);
                    this.Controls.Add(l);
                }
                else
                {
                    LinkLabel l = new LinkLabel() { Text = texts[i].Trim(), AutoSize=true, Font = this.Font };
                    int actionIndex = actionIndexes[i];
                    l.Tag = actions[actionIndex];
                    l.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_LinkClicked);
                    Labels.Add(l);
                    this.Controls.Add(l);
                }
            }

        }

        private void linkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            (((sender as Label).Tag) as Action)();
            if (sender is LinkLabel l)
                l.LinkVisited = true;
        }
    }
}
