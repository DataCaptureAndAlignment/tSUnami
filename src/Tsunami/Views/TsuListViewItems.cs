﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TSU.Common;
using TSU.Common.Compute.Compensations;
using TSU.Common.Elements.Composites;
using TSU.Common.Instruments;
using TSU.Common.Instruments.Reflector;
using TSU.Common.Measures;
using static TSU.Common.Compute.Compensations.Frame.FrameMeasurement;
using E = TSU.Common.Elements;
using M = TSU.Common.Measures;
using MMMM = TSU;
using O = TSU.Common.Operations;
using R = TSU.Properties.Resources;
using T = TSU.Tools;
using Zones = TSU.Common.Zones;

namespace TSU.Views
{
    public class TsuListViewItem : ListViewItem
    {
        internal ENUM.StateType State;

        #region constructors
        public bool Choosen;

        public TsuListViewItem()
        {
        }

        public TsuListViewItem(TsuObject o) : this()
        {
            try
            {
                dynamic dynamicMeasure = o;
                //if (o is TheoreticalElement) return;
                BasedOn(dynamicMeasure);
                TSU.Tsunami2.Preferences.Values.ElementManagerPositionCounter++;
                ColorNodeBaseOnState(this);
            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T538} {o.GetType().ToString()}: {ex.Message}", ex);
            }

        }
        //public TsuListViewItem(E.Point pt) 
        //{
        //    try
        //    {
        //        BasedOn(pt);
        //       TSU.Tsunami2.TsunamiPreferences.Values.ElementManagerPositionCounter++;
        //        ColorNodeBaseOnState(this);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception($"{R.T538} {o.GetType().ToString()}: {ex.Message}", ex);
        //    }
        //}

        public void BasedOn(TsuObject o)
        {
            this.Name = o._Name;
            this.Tag = o;
            this.Text = o._Name + R.T539 + o.GetType().ToString();
        }

        internal void ColorNodeBaseOnState(TsuListViewItem n)
        {
            switch (n.State)
            {
                case ENUM.StateType.Ready:
                    n.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Good;
                    n.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
                    break;
                case ENUM.StateType.NotReady:
                    if (TSU.Tsunami2.Preferences.Values.GuiPrefs.daltonien.IsTrue)
                    {
                        n.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
                        n.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.DarkBackground;
                    }
                    else
                    {
                        n.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Bad;
                        n.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
                    }
                    break;
                case ENUM.StateType.Acceptable:
                    n.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.Attention;
                    n.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
                    break;
                case ENUM.StateType.ToBeFilled:
                    n.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
                    n.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Action;
                    break;
                case ENUM.StateType.Unknown:
                    n.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
                    n.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
                    break;
                default:
                    n.ForeColor = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
                    n.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
                    break;
            }
        }
        #endregion

        #region modules
        public void BasedOn(MMMM.Manager m)
        {
            var AddObjectAsSubItem = new Action<object>(o =>
            {
                if (o != null) this.SubItems.Add(o.ToString());
            });

            this.Name = m._Name;
            this.Tag = m;
            this.Text = this.Name;
            foreach (var e in m.AllElements)
            {
                AddObjectAsSubItem(e);
            }
        }
        #endregion

        #region measures
        public void BasedOn(Polar.Measure m)
        {
            int precisionDigitsA = (TSU.Tsunami2.Preferences.Values.GuiPrefs == null) ? 5 : TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;
            int precisionDigitsD = (TSU.Tsunami2.Preferences.Values.GuiPrefs == null) ? 7 : TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
            this.Name = m._Name;
            this.Tag = m;
            this.Text = m._Point._Name;// +" " + m.Origin + " " + m.Face.ToString();
            this.SubItems.Add(m._Status.Type.ToString());
            this.SubItems.Add(m._Date.ToString("hh:mm"));
            string faceNumberMode = string.Format("{0} #/f={1}", m.Face.ToString(), m.NumberOfMeasureToAverage);
            this.SubItems.Add(faceNumberMode);
            this.SubItems.Add(T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(m.Angles.Corrected.Horizontal, precisionDigitsA));
            this.SubItems.Add(T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(m.Angles.Corrected.Vertical, precisionDigitsA));
            this.SubItems.Add(T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(m.Distance.Corrected, precisionDigitsD));
            this.SubItems.Add(T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(m.distanceHorizontal, precisionDigitsD));
            // Extension et code
            string extCode = (m.Extension.Value * 1000).ToString("F" + (precisionDigitsD - 3));
            if (m._OriginalPoint.SocketCode != null)
                extCode += " (Code: " + m._OriginalPoint.SocketCode.Id + ")";
            this.SubItems.Add(extCode);
            this.SubItems.Add(m.Distance.Reflector._Name);
            this.SubItems.Add(m.UsedAsReference.ToString());
            this.SubItems.Add(m.Comment);
            this.SubItems.Add(m.Origin);
            this.SubItems.Add($"{m.Distance.WeatherConditions.humidity:0}");
            this.SubItems.Add($"{m.Distance.WeatherConditions.dryTemperature:0.0}");
            this.SubItems.Add($"{m.Distance.WeatherConditions.pressure}");


            if (m._Status is M.States.Bad) this.State = ENUM.StateType.NotReady;
            else if (m._Status is M.States.Questionnable) this.State = ENUM.StateType.Acceptable;
            else if (m._Status is M.States.Temporary) this.State = ENUM.StateType.Acceptable;
            else if (m._Status is M.States.Control) this.State = ENUM.StateType.Unknown;
            else this.State = ENUM.StateType.Normal;
        }

        public void BasedOn(M.MeasureOfLevel m)
        {
            int precisionDigitsD = (TSU.Tsunami2.Preferences.Values.GuiPrefs == null) ? 7 : TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
            this.Name = m._Name;
            this.Tag = m;
            this.Text = m._Name;// +" " + m.Origin + " " + m.Face.ToString();
            this.SubItems.Add(m._Status.Type.ToString());
            this.SubItems.Add(m._Date.ToString());
            this.SubItems.Add(m.Origin);
            this.SubItems.Add(m._Point._Name);

            this.SubItems.Add("");
            this.SubItems.Add(m._RawLevelReading.ToString());
            this.SubItems.Add(m._Distance.ToString());
            this.SubItems.Add("");
            this.SubItems.Add("");
            // Extension et code
            string extCode = (m._Extension * 1000).ToString("F" + (precisionDigitsD - 3));
            this.SubItems.Add(extCode);
            this.SubItems.Add(m._staff._Name);
            this.SubItems.Add("");
            this.SubItems.Add(m.Comment);


            if (m._Status is M.States.Bad) this.State = ENUM.StateType.NotReady;
            else if (m._Status is M.States.Questionnable) this.State = ENUM.StateType.Acceptable;
            else if (m._Status is M.States.Temporary) this.State = ENUM.StateType.Acceptable;
            else if (m._Status is M.States.Control) this.State = ENUM.StateType.Unknown;
            else this.State = ENUM.StateType.Normal;
        }

        public void BasedOn(Tilt.Measure m)
        {
            int precisionDigitsA = (TSU.Tsunami2.Preferences.Values.GuiPrefs == null) ? 5 : TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;
            this.Name = m._Name;
            this.Tag = m;
            this.Text = m._Name;// +" " + m.Origin + " " + m.Face.ToString();
            this.SubItems.Add(m._Status.Type.ToString());
            this.SubItems.Add(m._Date.ToString());
            this.SubItems.Add(m.Origin);
            this.SubItems.Add(m._PointName);

            this.SubItems.Add("");
            this.SubItems.Add(m._ReadingBeamDirection.ToString());
            this.SubItems.Add(m._ReadingOppositeBeam.ToString());
            this.SubItems.Add("");
            this.SubItems.Add("");
            this.SubItems.Add("");
            this.SubItems.Add("");
            this.SubItems.Add("");
            this.SubItems.Add(m.Comment);


            if (m._Status is M.States.Bad) this.State = ENUM.StateType.NotReady;
            else if (m._Status is M.States.Questionnable) this.State = ENUM.StateType.Acceptable;
            else if (m._Status is M.States.Temporary) this.State = ENUM.StateType.Acceptable;
            else if (m._Status is M.States.Control) this.State = ENUM.StateType.Unknown;
            else this.State = ENUM.StateType.Normal;
        }

        public void BasedOn(M.MeasureOfOffset m)
        {
            int precisionDigitsA = (TSU.Tsunami2.Preferences.Values.GuiPrefs == null) ? 5 : TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;
            this.Name = m._Name;
            this.Tag = m;
            this.Text = m._Name;// +" " + m.Origin + " " + m.Face.ToString();
            this.SubItems.Add(m._Status.Type.ToString());
            this.SubItems.Add(m._Date.ToString());
            this.SubItems.Add(m.Origin);
            this.SubItems.Add(m._Point._Name);

            this.SubItems.Add("");
            this.SubItems.Add(m._RawReading.ToString());
            this.SubItems.Add(m._ReductedReading.ToString());
            this.SubItems.Add("");
            this.SubItems.Add("");
            this.SubItems.Add("");
            this.SubItems.Add("");
            this.SubItems.Add("");
            this.SubItems.Add(m.Comment);


            if (m._Status is M.States.Bad) this.State = ENUM.StateType.NotReady;
            else if (m._Status is M.States.Questionnable) this.State = ENUM.StateType.Acceptable;
            else if (m._Status is M.States.Temporary) this.State = ENUM.StateType.Acceptable;
            else if (m._Status is M.States.Control) this.State = ENUM.StateType.Unknown;
            else this.State = ENUM.StateType.Normal;
        }

        #endregion

        #region isntrument
        public void BasedOn(Sensor i)
        {
            this.Name = i._Name;
            this.Tag = i;
            this.Text = i._InstrumentClass.ToString();
            this.SubItems.Add(i._Brand);
            this.SubItems.Add(i._Model);
            this.SubItems.Add("# " + i._SerialNumber); // '#' to avoid the column to be soo small that it is threated as empty
        }

        public void BasedOn(OffsetMeter i)
        {
            BasedOn(i as Sensor);
            if (i.EtalonnageParameter != null)
                this.SubItems.Add(string.Format("{0}: {1} m", "Coefficient A0", i.EtalonnageParameter.fourrierA0Constant.ToString("F5")));
        }

        public void BasedOn(Reflector i)
        {
            this.Name = i._Name;
            this.Tag = i;
            this.Text = i._InstrumentClass.ToString();
            this.SubItems.Add(i._Brand);
            this.SubItems.Add(i._Model);
            this.SubItems.Add("# " + i._SerialNumber); // '#' to avoid the column to be soo small that it is threated as empty
        }
        public void BasedOn(LevelingStaff i)
        {
            this.Name = i._Name;
            this.Tag = i;
            this.Text = i._InstrumentClass.ToString();
            this.SubItems.Add(i._Brand);
            this.SubItems.Add(i._Model);
            this.SubItems.Add("# " + i._SerialNumber); // '#' to avoid the column to be soo small that it is threated as empty
        }
        #endregion

        #region elements
        public void BasedOn(CompositeElement e)
        {
            this.Name = e._Name;
            this.Tag = e;
            this.Text = this.Name;
            this.SubItems.Add("n/a"); // for the pos in theo file
            this.SubItems.Add(CheckedString(e.fileElementType.ToString()));
            this.SubItems.Add(CheckedString(e._Origin));
            this.SubItems.Add(CheckedString(e._Accelerator));
            this.SubItems.Add(CheckedString(e._Class));
            this.SubItems.Add(CheckedString(e._Numero));

        }

        public void BasedOn(Sequence s)
        {
            this.Name = s._Name;
            this.Tag = s;
            this.Text = this.Name;
            this.SubItems.Add("n/a"); // for the pos in theo file
            this.SubItems.Add(CheckedString(s.fileElementType.ToString()));
            this.SubItems.Add(CheckedString(s._Origin));
            this.SubItems.Add(CheckedString(s._Accelerator));
            this.SubItems.Add(CheckedString(s._Class));
            this.SubItems.Add(CheckedString(s._Numero));
        }

        public void BasedOn(Common.Station s)
        {
            this.Name = s._Name;
            this.Tag = s;
            this.Text = this.Name;
            this.SubItems.Add(s.ParametersBasic._Date.ToString());
            this.SubItems.Add(s.ParametersBasic._IsSetup.ToString());
            this.SubItems.Add(s.ParametersBasic._State.ToString());
        }

        public void BasedOn(T.Research.MeasureCount rmc)
        {
            this.Name = rmc.point._Name;
            this.Tag = rmc;
            this.Text = this.Name;
            this.SubItems.Add(rmc.point._Parameters.Cumul.ToString());
            this.SubItems.Add(rmc.countGood.ToString());
            this.SubItems.Add(rmc.countQuestionnable.ToString());
            this.SubItems.Add(rmc.countControl.ToString());
            this.SubItems.Add(rmc.countBad.ToString());

            switch (rmc.countGood)
            {
                case 0: this.State = ENUM.StateType.NotReady; break;
                case 1: this.State = ENUM.StateType.Acceptable; break;
                case 2: this.State = ENUM.StateType.Normal; break;
                default: this.State = ENUM.StateType.Ready; break; ;
            }
        }

        //public void BasedOn(SequenceTH s)
        //{
        //    this.Name = s._Name;
        //    this.Tag = s;
        //    this.Text = this.Name;
        //    this.SubItems.Add(CheckedString(s.fileElementType.ToString()));
        //    this.SubItems.Add(CheckedString(s._Origin));
        //    this.SubItems.Add(CheckedString(s._Accelerator));
        //    this.SubItems.Add(CheckedString(s._Class));
        //    this.SubItems.Add(CheckedString(s._Numero));
        //}
        //public void BasedOn(SequenceNiv s)
        //{
        //    this.Name = s._Name;
        //    this.Tag = s;
        //    this.Text = this.Name;
        //    this.SubItems.Add(CheckedString(s.fileElementType.ToString()));
        //    this.SubItems.Add(CheckedString(s._Origin));
        //    this.SubItems.Add(CheckedString(s._Accelerator));
        //    this.SubItems.Add(CheckedString(s._Class));
        //    this.SubItems.Add(CheckedString(s._Numero));
        //}
        //public void BasedOn(SequenceFil s)
        //{
        //    this.Name = s._Name;
        //    this.Tag = s;
        //    this.Text = this.Name;
        //    this.SubItems.Add(CheckedString(s.fileElementType.ToString()));
        //    this.SubItems.Add(CheckedString(s._Origin));
        //    this.SubItems.Add(CheckedString(s._Accelerator));
        //    this.SubItems.Add(CheckedString(s._Class));
        //    this.SubItems.Add(CheckedString(s._Numero));
        //}

        public void BasedOn(TSU.Common.Elements.Point pt)
        {
            int dd = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
            int da = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;

            this.Name = pt._Name;
            this.Tag = pt;
            this.Text = this.Name;                                                              //00
            this.SubItems.Add(MMMM.Tsunami2.Preferences.Values.ElementManagerPositionCounter.ToString()); //01
            this.SubItems.Add(CheckedString("*" + pt.fileElementType.ToString()));                    //02

            if (TSU.Tsunami2.Preferences.Values.GuiPrefs.HideMachineParameters.IsFalse)
                this.SubItems.Add(DoubleToFormatedString(pt._Parameters.Cumul));                    //

            this.SubItems.Add(CheckedString(pt._Origin));                                       //

            if (TSU.Tsunami2.Preferences.Values.GuiPrefs.HideMachineParameters.IsFalse)
            {
                this.SubItems.Add("*" + pt.Type.ToString());                                         //35
                this.SubItems.Add(CheckedString(pt._Accelerator));                                  //04
                this.SubItems.Add(CheckedString(pt._Class));                                        //05
                this.SubItems.Add(CheckedString(pt._Numero));                                       //06
                this.SubItems.Add(CheckedString(pt._Point));                                        //07
                
            }

            this.AddCoordinatesToShow(this.SubItems, pt, TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedTypes, dd);

            if (TSU.Tsunami2.Preferences.Values.GuiPrefs.HideMachineParameters.IsFalse)
            {
                this.SubItems.Add(DoubleToFormatedString(pt._Parameters.L, dd));                        //28
                this.SubItems.Add(DoubleToFormatedString(pt._Parameters.GisementFaisceau, da));         //29
                this.SubItems.Add(DoubleToFormatedString(pt._Parameters.Slope, da + 2));                    //30
                this.SubItems.Add(DoubleToFormatedString(pt._Parameters.T));                        //31
                this.SubItems.Add(DoubleToFormatedString(pt._Parameters.Tilt, da + 2));                     //32
                if (pt.SocketCode != null)                                                          //33
                    this.SubItems.Add(pt.SocketCode._Name);
                else
                    this.SubItems.Add("N/A");
            }

            switch (pt.State)                                                                   //34
            {
                case E.Element.States.Good:
                    this.State = ENUM.StateType.Normal;
                    this.SubItems.Add("*Good");
                    break;
                case E.Element.States.Bad:
                    this.State = ENUM.StateType.NotReady;
                    this.SubItems.Add("*Bad");
                    break;
                case E.Element.States.Questionnable:
                    this.State = ENUM.StateType.Acceptable;
                    this.SubItems.Add("*???");
                    break;
                case E.Element.States.ModifiedByUser:
                    this.State = ENUM.StateType.Acceptable;
                    this.SubItems.Add("*Mod.user");
                    break;
                case E.Element.States.FromStation:
                    this.State = ENUM.StateType.Acceptable;
                    this.SubItems.Add("*Mod.St");
                    break;
                case E.Element.States.Approximate:
                default:
                    this.State = ENUM.StateType.Acceptable;
                    this.SubItems.Add("*Approx");
                    break;
            }

            this.SubItems.Add(pt.Date.ToString("yy/MM/dd HH:mm:ss"));                                              //36
            this.SubItems.Add(pt.Comment);                                                      //37
        }

        private void AddCoordinatesToShow(ListViewSubItemCollection subItems, E.Point pt, List<CoordinateSystem> selectedCSs, int digits)
        {
            bool showAll = selectedCSs.Count == 0;
            var csToshow = showAll ? TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.AllTypes : selectedCSs;
            Color color1 = TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            Color color2 = TSU.Tsunami2.Preferences.Theme.Colors.LightForeground;
            Color currentColor = color1;
            Color bakColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            Font f = TSU.Tsunami2.Preferences.Theme.Fonts.MonospaceFont;
            foreach (var cs in csToshow)
            {
                currentColor = currentColor == color1 ? color2 : color1;
                if (cs == null) continue;
                if (pt._Coordinates.CoordinatesExistInAGivenSystem(cs._Name))
                {
                    var c = pt._Coordinates.GetCoordinatesInASystemByName(cs._Name);
                    SubItems.Add(DoubleValueToFormatedString(c.X, digits), currentColor, bakColor, f);          //09
                    SubItems.Add(DoubleValueToFormatedString(c.Y, digits), currentColor, bakColor, f);          //10
                    SubItems.Add(DoubleValueToFormatedString(c.Z, digits), currentColor, bakColor, f);          //11
                }
                else
                {
                    SubItems.Add("-");          //09
                    SubItems.Add("-");          //10
                    SubItems.Add("-");          //11
                }
            }
        }

        public void BasedOn(Accelerator e)
        {
            this.Name = e._Name;
            this.Tag = e;
            this.Text = this.Name;
            this.SubItems.Add(MMMM.Tsunami2.Preferences.Values.ElementManagerPositionCounter.ToString());
            this.SubItems.Add(CheckedString(e.fileElementType.ToString()));
            this.SubItems.Add(CheckedString(e._Origin));
            this.SubItems.Add(CheckedString(e._Accelerator));
        }

        public void BasedOn(ElementClass e)
        {
            this.Name = e._Name;
            this.Tag = e;
            this.Text = this.Name;
            this.SubItems.Add(MMMM.Tsunami2.Preferences.Values.ElementManagerPositionCounter.ToString());
            this.SubItems.Add(CheckedString(e.fileElementType.ToString()));
            this.SubItems.Add(CheckedString(e._Origin));
            this.SubItems.Add(CheckedString(e._Accelerator));
            this.SubItems.Add(CheckedString(e._Class));
        }

        public void BasedOn(Magnet e)
        {
            this.Name = e._Name;
            this.Tag = e;
            this.Text = this.Name;
            this.SubItems.Add(MMMM.Tsunami2.Preferences.Values.ElementManagerPositionCounter.ToString());
            this.SubItems.Add(CheckedString(R.T_MAGNET));
            this.SubItems.Add(CheckedString(e._Origin));

            if (TSU.Tsunami2.Preferences.Values.GuiPrefs.HideMachineParameters.IsFalse)
            {
                this.SubItems.Add(CheckedString("TH"));
                this.SubItems.Add(CheckedString(e._Accelerator));
                this.SubItems.Add(CheckedString(e._Class));
                this.SubItems.Add(CheckedString(e._Numero));
                this.SubItems.Add(new ListViewSubItem()); // hide element
                this.SubItems.Add(CheckedString(e._Parameters.Cumul.ToString()));
            }
        }

        public void BasedOn(TheoreticalElement e)
        {
            this.Name = e._Name;
            this.Tag = e;
            this.Text = this.Name;
            this.SubItems.Add("n/a"); // for the pos in theo file
            this.SubItems.Add(CheckedString(e.fileElementType.ToString()));
            this.SubItems.Add(CheckedString(e._Origin));
        }

        public void BasedOn(FittedShape e)
        {
            this.Name = e._Name;
            this.Tag = e;
            this.Text = e._Name;

            if (e.parametricShape is E.Line3D l)
            {
                if (e.Elements.Count < 2)
                {
                    this.SubItems.Add("no points!");
                    return;
                }

                int nd = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;
                E.Point p = e.Elements[0] as E.Point;

                this.SubItems.Add(MMMM.Tsunami2.Preferences.Values.ElementManagerPositionCounter.ToString()); //01
                this.SubItems.Add(CheckedString(e.fileElementType.ToString()));                    //02
                this.SubItems.Add(CheckedString(e._Origin));                                       //03
                this.SubItems.Add(CheckedString(p._Accelerator));                                  //04
                this.SubItems.Add(CheckedString(p._Class));                                        //05
                this.SubItems.Add(CheckedString(p._Numero));                                       //06
                this.SubItems.Add(CheckedString(p._Point));                                        //07
                this.SubItems.Add(DoubleToFormatedString(p._Parameters.Cumul));                    //08
                this.AddCoordinatesToShow(this.SubItems, p, TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedTypes, nd);

                this.SubItems.Add(new ListViewSubItem());                        //28
                this.SubItems.Add(DoubleValueToFormatedString(l.Bearing, nd));        //29
                this.SubItems.Add(DoubleValueToFormatedString(l.Slope, nd));                    //30
            }
        }

        public void BasedOn(E.Line3D e)
        {
            int nd = TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;

            this.Name = e._Name;
            this.Tag = e;
            this.Text = e._Name;
            this.SubItems.Add(CheckedString(e.fileElementType.ToString()));

            E.Point p = e.Elements[0] as E.Point;

            this.SubItems.Add(MMMM.Tsunami2.Preferences.Values.ElementManagerPositionCounter.ToString()); //01
            this.SubItems.Add(CheckedString(e.fileElementType.ToString()));                    //02
            this.SubItems.Add(CheckedString(e._Origin));                                       //03
            this.SubItems.Add(CheckedString(p._Accelerator));                                  //04
            this.SubItems.Add(CheckedString(p._Class));                                        //05
            this.SubItems.Add(CheckedString(p._Numero));                                       //06
            this.SubItems.Add(CheckedString(p._Point));                                        //07
            this.SubItems.Add(DoubleToFormatedString(p._Parameters.Cumul));                    //08
            this.AddCoordinatesToShow(this.SubItems, p, TSU.Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedTypes, nd);
            this.SubItems.Add(new ListViewSubItem());                        //28
            this.SubItems.Add(DoubleValueToFormatedString(e.Bearing, nd));        //29
            this.SubItems.Add(DoubleValueToFormatedString(e.Slope, nd));                    //30
        }

        public void BasedOn(E.ParametricShape e)
        {

        }
        #endregion

        #region tools

        private string DoubleToFormatedString(double d, int numberDecimal = 5)
        {
            string s;
            if (d == TSU.Tsunami2.Preferences.Values.na)
                s = "";
            else
                s = d.ToString($"F{numberDecimal}");
            return s;
        }
        private string DoubleValueToFormatedString(DoubleValue value, int numberDecimal)
        {
            if (value == null || value.Value == TSU.Tsunami2.Preferences.Values.na)
                return "";
            else
                return T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(value, numberDecimal);
        }
        private string CheckedString(string s)
        {
            if (s == null) return "";
            int n = 3;
            if (s.Length >= n)
                if (s.Substring(0, n).ToUpper() == "UNK")
                    s = "";
            return s;
        }
        #endregion

        #region Management.Operations
        public void BasedOn(O.Operation i)
        {
            this.Name = i._Name;
            this.Tag = i;
            this.Text = i.value.ToString();
            this.SubItems.Add(i.Description);
            this.SubItems.Add(i.parentOperationValue.ToString());
        }
        #endregion

        #region Zones
        public void BasedOn(Zones.Zone i)
        {
            this.Name = i._Name;
            this.Tag = i;
            this.Text = i._Name;

            if (i.Mla2SuInGon == null)
            {
                this.SubItems.Add("");
                this.SubItems.Add("");
                this.SubItems.Add("");
            }
            else
            {
                this.SubItems.Add(i.Mla2SuInGon.RotX.ToString());
                this.SubItems.Add(i.Mla2SuInGon.RotY.ToString());
                this.SubItems.Add(i.Mla2SuInGon.RotZ.ToString());
            }

            if (i.Ccs2MlaInfo != null)
            {
                if (i.Ccs2MlaInfo.Origin == null)
                {
                    this.SubItems.Add("");
                    this.SubItems.Add("");
                    this.SubItems.Add("");
                    this.SubItems.Add("");
                }
                else
                {
                    this.SubItems.Add(i.Ccs2MlaInfo.Origin._Name.ToString());
                    this.SubItems.Add(T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(i.Ccs2MlaInfo.Origin.X, 5));
                    this.SubItems.Add(T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(i.Ccs2MlaInfo.Origin.Y, 5));
                    this.SubItems.Add(T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(i.Ccs2MlaInfo.Origin.Z, 5));
                }

                if (i.Ccs2MlaInfo.Bearing == null)
                    this.SubItems.Add("");
                else
                    this.SubItems.Add(T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(i.Ccs2MlaInfo.Bearing, 5));

                if (i.Ccs2MlaInfo.Slope == null)
                    this.SubItems.Add("");
                else
                    this.SubItems.Add(T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(i.Ccs2MlaInfo.Slope, 5));
            }
            else
            {
                this.SubItems.Add("");
                this.SubItems.Add("");
                this.SubItems.Add("");
                this.SubItems.Add("");
                this.SubItems.Add("");
                this.SubItems.Add("");
            }
            if (i.Su2PhysInGon == null)
            {
                this.SubItems.Add("");
                this.SubItems.Add("");
                this.SubItems.Add("");
            }
            else
            {
                this.SubItems.Add(i.Su2PhysInGon.RotX.ToString());
                this.SubItems.Add(i.Su2PhysInGon.RotY.ToString());
                this.SubItems.Add(i.Su2PhysInGon.RotZ.ToString());
            }
            if (i.Su2PhysInM == null)
            {
                this.SubItems.Add("");
                this.SubItems.Add("");
                this.SubItems.Add("");
            }
            else
            {
                this.SubItems.Add(i.Su2PhysInM.AlongX.ToString());
                this.SubItems.Add(i.Su2PhysInM.AlongY.ToString());
                this.SubItems.Add(i.Su2PhysInM.AlongZ.ToString());
            }

            if (i.BeamInclinaisonInRad == null)
                this.SubItems.Add("");
            else
                this.SubItems.Add(i.BeamInclinaisonInRad.Rot.ToString());
        }


        #endregion


        public void BasedOn(TSU.Common.Compute.Compensations.BeamOffsets.CompensationItem item)
        {
            string naString = "-";
            int precisionDigitsA = (TSU.Tsunami2.Preferences.Values.GuiPrefs == null) ? 5 : TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;
            int precisionDigitsD = (TSU.Tsunami2.Preferences.Values.GuiPrefs == null) ? 7 : TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;

            this.Tag = item;
            var meas = item.StateAndMeasure?.Measure;
            bool isPolar = false;
            bool isLevel = false;
            bool isLine = false;
            bool isTilt = false;
            bool isComputedTilt = false;
            if (meas != null)
            {
                this.Name = meas._PointName;
                this.Text = meas._PointName;
                if (meas is Polar.Measure)
                    isPolar = true;
                else if (meas is MeasureOfLevel)
                    isLevel = true;
                else if (meas is MeasureOfOffset)
                    isLine = true;
                else if (meas is Tilt.Measure)
                    isTilt = true;
            }
            else
            // must be a computed dRoll
            {
                isTilt = true;
                isComputedTilt = true;
                string assemblyName = item.Incly.Point;
                this.Name = assemblyName;
                this.Text = assemblyName;
            }


            // measure type
            string type = naString;
            {
                if (isPolar)
                    type = "POLAR";
                else if (isLevel)
                    type = "LEVEL";
                else if (isLine)
                    type = "LINE";
                else if (isTilt)
                {
                    type = "R.";
                    if (isComputedTilt)
                        type += "COMP";
                    else
                        type += "MEAS";
                }
            }
            this.SubItems.Add(type);

            // state
            if (!isComputedTilt)
                this.SubItems.Add(new ListViewSubItem() { Text = meas._Status.Type.ToString(), Tag = meas._Status });
            else
                this.SubItems.Add(new ListViewSubItem() { Text = "-" });

            // Extension
            ListViewSubItem extension = new ListViewSubItem() { Text = naString };
            if ((isPolar || isLevel))
            {
                string extCode = "";
                if (meas is Polar.Measure pm)
                {
                    extCode = pm.Extension != null ? pm.Extension.ToString("m-mm"): "-";
                    extension.Tag = pm.Extension;
                }
                else if (meas is MeasureOfLevel lm)
                {
                    extCode =  (lm._Extension*1000).ToString("F2");
                    extension.Tag = lm._Extension;
                }
                extension.Text = extCode;
            }
            this.SubItems.Add(extension);

            // usage
            this.SubItems.Add(new ListViewSubItem() { Text = item.Used ? "X" : naString, Tag = item.Used });

            // Time
            //this.SubItems.Add(meas._Date.ToString("hh:mm:ss"));

            // Residuals 
            if (item.Used)
            {
                if (item.ObsXyz != null)
                {
                    this.SubItems.Add((item.ObsXyz.X.Residual * 1000).ToString("F2"));
                    this.SubItems.Add((item.ObsXyz.Y.Residual * 1000).ToString("F2"));
                    this.SubItems.Add((item.ObsXyz.Z.Residual * 1000).ToString("F2"));
                }
                else
                {
                    this.SubItems.Add(naString);
                    this.SubItems.Add(naString);
                    this.SubItems.Add(naString);
                }
            }
            else
            {
                this.SubItems.Add("-");
                this.SubItems.Add("-");
                this.SubItems.Add("-");
            }

            //dRoll
            if (isTilt)
            {
                double dRollToShow_mrad = 0;
                if (item.Incly.IsAResultFromComputation)
                    dRollToShow_mrad = item.Incly.dRoll.Value / 200 * Math.PI * 1000;
                else
                {
                    var tm = (Tilt.Measure)item.StateAndMeasure.Measure;
                    dRollToShow_mrad = tm._Ecart.Value * 1000;
                }
                this.SubItems.Add(item.Incly == null ? "-" : Math.Round(dRollToShow_mrad, 3).ToString("F3"));
            }
            else
                this.SubItems.Add("-");

            // Sigma a priori
            if (item.Used)
            {
                if (item.ObsXyz != null)
                {
                    this.SubItems.Add(new ListViewSubItem() { Text = (item.ObsXyz.X.Sigma * 1000).ToString("F2"), Tag = item.ObsXyz.X });
                    this.SubItems.Add(new ListViewSubItem() { Text = (item.ObsXyz.Y.Sigma * 1000).ToString("F2"), Tag = item.ObsXyz.Y });
                    this.SubItems.Add(new ListViewSubItem() { Text = (item.ObsXyz.Z.Sigma * 1000).ToString("F2"), Tag = item.ObsXyz.Z });
                }
                else
                {
                    this.SubItems.Add(naString);
                    this.SubItems.Add(naString);
                    this.SubItems.Add(naString);
                    
                }
            }
            else
            {
                this.SubItems.Add("-");
                this.SubItems.Add("-");
                this.SubItems.Add("-");
            }

            // sRoll
            if (isTilt)
            {
                Observation dRollToShow_gon = null;
                double dRollToShow_mrad = 0;
                if (item.Incly.IsAResultFromComputation)
                {
                    dRollToShow_mrad = item.Incly.dRoll.Sigma / 200 * Math.PI * 1000;
                }
                else
                {
                    var tm = (Tilt.Measure)item.StateAndMeasure.Measure;
                    dRollToShow_mrad = tm._Ecart.Sigma * 1000;
                }
                this.SubItems.Add(item.Incly == null ? "-" : Math.Round(dRollToShow_mrad, 3).ToString("F3"));
            }
            else
                this.SubItems.Add("-");
        }

        public void BasedOn(CompensationItem item)
        {
            int precisionDigitsA = (TSU.Tsunami2.Preferences.Values.GuiPrefs == null) ? 5 : TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForAngles;
            int precisionDigitsD = (TSU.Tsunami2.Preferences.Values.GuiPrefs == null) ? 7 : TSU.Tsunami2.Preferences.Values.GuiPrefs.NumberOfDecimalForDistances;

            this.Name = item.Measure._PointName;
            this.Tag = item;
            this.Text = item.Measure._PointName;

            //1
            ListViewSubItem status = new ListViewSubItem() { Text = item.Measure._Status.Type.ToString(), Tag = item.Measure._Status };
            this.SubItems.Add(status);

            //2 role
            string role = item.Measure.GeodeRole.ToString();
            if (item.AHDisabled || item.AVDisabled || item.DistanceDisabled)
                role += $" {R.T_WITHOUT} ";
            if (item.AHDisabled)
                role += "AH ";
            if (item.AVDisabled)
                role += "AV ";
            if (item.DistanceDisabled)
                role += "D";
            ListViewSubItem roleItem = new ListViewSubItem() { Text = role, Tag = item.Measure.GeodeRole };
            this.SubItems.Add(roleItem);

            Polar.Measure m = item.Measure as Polar.Measure;
            //3,4
            if (m != null)
            {
                ListViewSubItem reflector = new ListViewSubItem() { Text = m.Distance.Reflector._Name, Tag = m.Distance.Reflector };
                this.SubItems.Add(reflector);
                this.SubItems.Add(m.Distance.Corrected.ToString("m-m"));
            }
            else
            {
                this.SubItems.Add("N/A");
                this.SubItems.Add("N/A");
            }

            // 5 Extension et code

            string extCode = item.Measure.Extension.ToString("m-mm");
            ListViewSubItem extension = new ListViewSubItem() { Text = extCode, Tag = item.Measure.Extension };
            this.SubItems.Add(extension);

            // 6 time 
            this.SubItems.Add(item.Measure._Date.ToString("hh:mm:ss"));

            // 7 vo
            if (item.V0 != null)
                this.SubItems.Add(T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(item.V0, precisionDigitsA));
            else
                this.SubItems.Add("N/A");

            // 8 Zt
            if (item.Zt != null)
                this.SubItems.Add(T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(item.Zt, precisionDigitsD));
            else
                this.SubItems.Add("N/A");




            // 9,10,11 rH rV rD
            if (m != null)
            {
                if (item.LgcResults is Polar.Measure r)
                {
                    string textAH = item.AHDisabled ? "disabled" : r.Angles.Corrected.Horizontal.GetSigma("cc");
                    ListViewSubItem ah = new ListViewSubItem() { Text = textAH, Tag = r.Angles.Corrected.Horizontal };
                    this.SubItems.Add(ah);

                    string textAV = item.AVDisabled ? "disabled" : r.Angles.Corrected.Vertical.GetSigma("cc");
                    ListViewSubItem vh = new ListViewSubItem() { Text = textAV, Tag = r.Angles.Corrected.Vertical };
                    this.SubItems.Add(vh);

                    string textDD = item.DistanceDisabled ? "disabled" : r.Distance.Corrected.GetSigma("mm");
                    ListViewSubItem dd = new ListViewSubItem() { Text = textDD, Tag = r.Distance.Corrected };
                    this.SubItems.Add(dd);
                }
                else
                {
                    this.SubItems.Add("N/A");
                    this.SubItems.Add("N/A");
                    this.SubItems.Add("N/A");
                }
            }

            // 12 ,13 14 dl dr dz
            this.SubItems.Add(CheckForNullOrNa(item.dR));
            this.SubItems.Add(CheckForNullOrNa(item.dL));
            this.SubItems.Add(CheckForNullOrNa(item.dZ));

            var comment = item.Measure.CommentFromUser;
            ListViewSubItem commentSubItem = new ListViewSubItem() { Text = comment == ""? "Add comment": comment, Tag = "COMMENT" };
            this.SubItems.Add(commentSubItem);
        }

        private string CheckForNullOrNa(DoubleValue value)
        {
            string s = "N/A";

            if (value != null)
                if (value.Value != TSU.Tsunami2.Preferences.Values.na)
                    s = value.ToString("m-mm");
            return s;
        }
    }
}
