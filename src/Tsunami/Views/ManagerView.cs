﻿using Ploeh.AutoFixture;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TSU.Common;
using TSU.Common.Elements;
using TSU.Preferences.GUI;
using TSU.Tools.Exploration;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using F = TSU.Functionalities;
using M = TSU;
using R = TSU.Properties.Resources;
using V = TSU.Common.Strategies.Views;

namespace TSU.Views
{
    public class ManagerView : ModuleView
    {

        public bool HideEmptyColumns { get; set; } = true;

        #region Fields
        private Color originalColor;

        private Buttons buttons;
        internal Buttons _Buttons
        {
            get
            {
                if (buttons == null)
                    buttons = new Buttons(this);
                return buttons;
            }
            set
            {
                buttons = value;
            }
        }

        /// <summary>
        /// Show or Hide checkboxes in front of the element
        /// </summary>
        public bool CheckBoxesVisible { get; set; }

        internal bool selectionChanged;

        public bool AutoCheckChildren
        { 
            get
            {
                    return this.Module.AutoCheckChildren;
            }
            set
            {
                if (this.Module != null)
                    this.Module.AutoCheckChildren = value;
            }
        }

        public V.Interface currentStrategy;
        public List<V.Types> AvailableViewStrategies;

        private Label label1;
        
        private bool StopShowMessageAboutHiding;

        internal new M.Manager Module
        {
            get
            {
                return base._Module as M.Manager;
            }
            set
            {
                base._Module = value;
            }
        }

        internal List<TsuObject> AllElements
        {
            get
            {
                List<TsuObject> list = new List<TsuObject>();
                foreach (Element item in this.Module.AllElements)
                {
                    list.Add(item);
                    list.AddRange(item.GetAllElements());
                }
                return list;
            }
        }

        #endregion

        #region Constructor
        public ManagerView():base()
        {
            SuspendLayout();

            this.AdaptTopPanelToButtonHeight();
            this.AutoScroll = true;
            ResumeLayout();
        }
        public ManagerView(M.IModule linkedModule)
            : base(linkedModule)
        {
            this.icon =R.TSU1;
            SuspendLayout();
            this.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.AllowTransparency = false;
            this.InitializeStrategy();

            this.AdaptTopPanelToButtonHeight();
            this.contextButtons = new List<Control>();
            this._Buttons = new Buttons(this);
            this.originalColor = this.BackColor;
            this._PanelBottom.AutoScroll = true;
            this.MinimumSize = new Size( 300, 200);
            
            this.ResumeLayout();

            this.AdaptTopPanelToButtonHeight();
            //this.InitializeMenu();
        }

        protected override void Dispose(bool disposing)
        {
            if (this.existingStrategies != null)
            {
                existingStrategies.Clear();
            }
            this.existingStrategies = null;

            this.currentStrategy.Dispose();
            if (this._Buttons != null)
            {
                this._Buttons.Dispose();
                this._Buttons = null;
            }
            if (this.contextButtons != null)
            {
                for (int i = 0; i < this.contextButtons.Count; i++)
                {
                    if (this.contextButtons[i] != null)
                        this.contextButtons[i].Dispose();
                    this.contextButtons[i] = null;
                }
                this.contextButtons.Clear();
                this.contextButtons = null;
            }

            base.Dispose(disposing);
        }

        internal virtual IEnumerable<Control> GetOptions(object tag)
        {
            if (tag is TsuObject to)
            {
                foreach(Control c in to.GetOptions())
                    yield return c;
            }
        }

        internal void ShowOptions(object tag)
        {
            List<Control> controls = GetOptions(tag).ToList();
            if (controls.Count != 0)
                base.ShowPopUpMenu(controls, "Manager options ");
        }

        internal virtual void InitializeStrategy()
        {
            // Design Pattern Strategy for the view
            this.AvailableViewStrategies = new List<V.Types>() { V.Types.List, V.Types.Tree };

            this.SetStrategy(V.Types.List);
        }

        #endregion

        #region buttons


        internal BigButton bigbuttonTop;

        public class Buttons 
        {
            public List<BigButton> all;

            public ManagerView View;
            public BigButton TreeView;
            public BigButton ListView;
            public BigButton ThreeDView;
            public BigButton ShowSelection;

            public Buttons(ManagerView view)
            {
                View = view;
                all = new List<BigButton>();

                // Views
                TreeView = new BigButton(R.T530, R.Tree, () => { View.ShowAsTreeView(saveInGuiPref: true); }); all.Add(TreeView);
                ListView = new BigButton(R.T535, R.List, () => { View.ShowAsListView(saveInGuiPref: true); }); all.Add(ListView);
                ThreeDView = new BigButton(R.T531, R.Element_Code, () => { View.ShowAs3DView(saveInGuiPref: true); }); all.Add(ThreeDView);

                ShowSelection = new BigButton($"{R.T_SHOW_SELECTION};{R.T_FOR_TEST_PURPOSE}", R.Search, View.ShowSelectedElements,TSU.Tsunami2.Preferences.Theme.Colors.Highlight); all.Add(ShowSelection);
            }

            internal void Dispose()
            {
                this.View = null;

                for (int i = 0; i < all.Count; i++)
                {
                    if (all[i] != null)
                        all[i].Dispose();
                    all[i] = null;
                }
                if (all != null)
                    all.Clear();
                all = null;
            }
        }

        #endregion

        #region Specific
        private List<string> listviewColumnHeaderTexts;
        public virtual List<string> ListviewColumnHeaderTexts
        {
            get
            {
                if (listviewColumnHeaderTexts == null)
                {
                    listviewColumnHeaderTexts = new List<string>() { "TobeFilled", "TobeFilled", "TobeFilled", "TobeFilled", "TobeFilled", "TobeFilled" };
                }
                return listviewColumnHeaderTexts;
            }
            set
            {
                listviewColumnHeaderTexts  = value;

            }

        }

        /// <summary>
        /// Do nothing for the moment
        /// </summary>
        /// <param name="t"></param>
        public virtual void ArrangeNodes(TreeView t) { }

        #endregion

        #region object selection

        public virtual List<Control> GetViewOptionButtons()
        {
            return new List<Control>();
        }

        public void ValidateSelection()
        {
            this.currentStrategy.ValidateSelection();
        }

        public void CancelSelection()
        {
            this.currentStrategy.CancelSelection();
        }


        //internal void SetSelectableList(List<TsuObject> selectableItemList)
        //{
        //    this._Selectable.Clear();
        //    if (selectableItemList != null)
        //        this._Selectable.AddRange(selectableItemList);
        //    else
        //        this._Selectable.AddRange(this.ManagerModule.Elements);
        //    this.UpdateView();
        //}

        #endregion

        #region Showing and update strategies

        #region Interface

        #endregion


        #region Methods

        public void ShowAsListView(bool saveInGuiPref = false)
        {
            ShowAs(V.Types.List, saveInGuiPref);
        }

        public void ShowAs3DView(bool saveInGuiPref = false)
        {
            ShowAs(V.Types.In3D, saveInGuiPref);
        }

        public void ShowAsTreeView(bool saveInGuiPref = false)
        {
            ShowAs(V.Types.Tree, saveInGuiPref);
        }

        private void ShowAs(V.Types viewStrategyType, bool saveInGuiPref=false)
        {
            this.Show(viewStrategyType);


            if (saveInGuiPref)
            {
                this.SavePreference(viewStrategyType);
            }
        }

        internal virtual void SavePreference(V.Types viewStrategyType)
        {
            // overrided

            M.Tsunami2.Preferences.Values.SaveGuiPrefs();
        }

        internal virtual void SetStrategy(V.Types viewStrategyType)
        {
            if (!AvailableViewStrategies.Contains(viewStrategyType)) return;

            ChangeStrategy(viewStrategyType);

            if (this is TSU.Common.Instruments.Manager.View)
            {
                if (this is Common.Instruments.Reflector.Manager.View)
                   TSU.Tsunami2.Preferences.Values.GuiPrefs.InstrumentViewType = viewStrategyType;
                else
                   TSU.Tsunami2.Preferences.Values.GuiPrefs.ReflectorViewType = viewStrategyType;
            }

        }
        private List<V.Interface> existingStrategies = new List<V.Interface>();

        internal void ChangeStrategy(V.Types type)
        {
            this.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this._PanelBottom.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this._PanelTop.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.AllowTransparency = false;

            this.currentStrategy = existingStrategies.Find(x => x.Type == type);
            if (this.currentStrategy == null)
            {
                switch (type)
                {
                    case V.Types.Tree: this.currentStrategy = new V.Tree(this); break;
                    case V.Types.List: this.currentStrategy = new V.List(this); break;
                    case V.Types.EditableList: this.currentStrategy = new V.EditableList(this); break;
                    case V.Types.In3D: this.currentStrategy = new V.ThreeDimension(this); break;
                    default:
                        break;
                }
                existingStrategies.Add(this.currentStrategy);
            }
            else
                this.currentStrategy.ManagerView = this;


        }



        internal  void Show(V.Types viewStrategyType)
        {
            if (!AvailableViewStrategies.Contains(viewStrategyType)) return;

            ChangeStrategy(viewStrategyType);

            if (this is TSU.Common.Instruments.Manager.View)
            {
                if (this is Common.Instruments.Reflector.Manager.View)
                   TSU.Tsunami2.Preferences.Values.GuiPrefs.InstrumentViewType = viewStrategyType;
                else
                   TSU.Tsunami2.Preferences.Values.GuiPrefs.ReflectorViewType = viewStrategyType;
            }

            // this.currentStrategy.Update(); is update in show()
            currentStrategy.Show();
        }

        #endregion

        #endregion

        #region Show


        internal void ShowTsuObject(object o)
        {
            if (o != null)
            {
                dynamic d = o;
                this.BasedOn(d);
            }
        }

        private void BasedOn(object o)
        {
            // by default , do nothing if type not known
        }
        private void BasedOn(TsuBool b)
        {
            b.IsTrue= !b.IsTrue;
            this.UpdateView();
        }

        private void BasedOn(Global x)
        {
            M.Tsunami2.Preferences.Values.SaveGuiPrefs();
        }

        private void BasedOn(TsuObject o)
        {
            // by default , do nothing if type not known
        }

        private void BasedOn(Tsunami m)
        {
            // click in the explorer module on a tsunami node should do nothing
        }

        private void BasedOn(Preferences.Preferences m)
        {
            // click in the explorer module on a preference node should do nothing
        }

        private void BasedOn(M.Module m)
        {
            this.Hide(); 

            m.Finished = false;
            if (m._TsuView != null)
            {
                m._TsuView.WindowState = FormWindowState.Normal;
                m._TsuView.TopMost = true;
                m._TsuView.BringToFront();
                m._TsuView.Visible = true;
                m._TsuView.WindowState = FormWindowState.Normal;
                m._TsuView.ShowDockedFill();
            }
        }

        private void BasedOn(System.IO.FileInfo m)
        {
            if (!m.Exists) return;

            string titleAndMessage = string.Format(R.T_RunPath, m.FullName);
            MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
            {
                ButtonTexts = new List<string> { R.T_YES, R.T_NO }
            };
            string resp = mi.Show().TextOfButtonClicked;
            if (resp == R.T_YES)
                Shell.Run(m.FullName);
        }

        public virtual void ShowSelected()
        {
            if (this.currentStrategy!=null)
            {
                this.currentStrategy.ShowSelection();
                this.currentStrategy.Update();
            }
        }

        public override void FocusView()
        {
            this.currentStrategy.Focus();
            this.currentStrategy.SetSelectedInBlue();
        }

        // define an interface for different strategy of showing the elements
        public override void UpdateView()
        {
            try
            {
                if (this._Module is M.Manager)
                    if ((this._Module as M.Manager).isMainManager)
                        (this._Module as M.Manager).ShowAllElementIncludedInParentModule();

                base.UpdateView();

                if (this.IsHandleCreated)
                {
                        this.currentStrategy.Update();
                }
            }
            catch (Exception ex)
            {
                this.ShowMessageOfBug(string.Format(R.T536, this._Module._Name, ":\r\n"), ex, R.T_OK + "!");
            }
        }

        #endregion

        #region Events

        public virtual void AddFromFilter()
        {

        }

        private void ModuleView_Load(object sender, EventArgs e)
        {

        }
        private void ModuleView_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;

            if (this._Module is F.Menu) return;
            if (this._Module is Explorator)
            {
                this.Hide();
                return;
            }

            if (!this.StopShowMessageAboutHiding)
            {
                string cancel = R.T_CANCEL;
                string titleAndMessage = string.Format(
                    R.T537, this._Module.GetType().ToString());
                MessageInput mi = new MessageInput(MessageType.Warning, titleAndMessage)
                {
                    ButtonTexts = new List<string> { "Hide", cancel }
                };
                if (cancel != mi.Show().TextOfButtonClicked
                   )
                {
                    this.StopShowMessageAboutHiding = true;
                    this.Hide();
                }
            }
            else
                this.Hide();


        }
        private void ModuleView_ResizeBegin(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
            {
                this.Dock = DockStyle.Fill;
            }
            else
                this.Dock = DockStyle.None;
        }
        #endregion

        internal TableLayoutPanel tableLayoutForTopButton;
        internal BigButton ViewChoiceButton;

        internal virtual void InitializeMenu()
        {

            bigbuttonTop = new BigButton("Manager",
               R.Module,
                this.ShowContextMenuGlobal);

            ViewChoiceButton = new BigButton("Options;Display and Selection options",R.imanust, ShowChoiceView);
            ViewChoiceButton.HasSubButtons = true;

            tableLayoutForTopButton = new TableLayoutPanel() { ColumnCount = 2, Dock = DockStyle.Fill };
            tableLayoutForTopButton.Margin = new Padding(0);
            tableLayoutForTopButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60));
            tableLayoutForTopButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20));

            bigbuttonTop.Margin = new Padding(0);
            tableLayoutForTopButton.Controls.Add(bigbuttonTop);

            ViewChoiceButton.Margin = new Padding(0);
            tableLayoutForTopButton.Controls.Add(ViewChoiceButton);

            this._PanelTop.Controls.Add(tableLayoutForTopButton);
        }

        internal virtual void ShowContextMenuGlobal()
        {
            contextButtons = new List<Control>(); ;
            
            this.ShowPopUpMenu(contextButtons, "Manager Context Menu");
            this.UpdateView();
        }

        internal void ShowChoiceView()
        {
            this.currentStrategy.CreateContextMenuItemCollection();
            this.ShowPopUpMenu(this.currentStrategy.ContextMenu, "Manager choices ");
        }


        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _PanelBottom
            // 
            this._PanelBottom.Location = new System.Drawing.Point(5, 82);
            this._PanelBottom.Size = new System.Drawing.Size(738, 336);
            // 
            // _PanelTop
            // 
            this._PanelTop.Size = new System.Drawing.Size(738, 77);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.label1.Location = new System.Drawing.Point(5, 403);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(362, 15);
            this.label1.TabIndex = 9;
            this.label1.Text = "label1";
            // 
            // ManagerView
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(748, 423);
            this.Controls.Add(this.label1);
            this.Name = "ManagerView";
            this.Controls.SetChildIndex(this.buttonForFocusWithTabIndex0, 0);
            this.Controls.SetChildIndex(this._PanelTop, 0);
            this.Controls.SetChildIndex(this._PanelBottom, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.ResumeLayout(false);

        }

        private void ApplyThemeColors()
        {
            this.label1.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
        }

        internal void ShowSelectedElements()
        {
            string t = "Actual selection is :";
            string m = "Empty";
            bool first = true;
            foreach (TsuObject item in this.Module._SelectedObjects)
	        {
                if (first) {m = ""; first = false;  }
                m += item.ToString() + "\r\n";
	        }

            m += "\r\n"+"Element with a 'v' in front : " + "\r\n";
            foreach (TsuObject item in this.currentStrategy.GetChecked())
            {
                m += item.ToString() + "\r\n";
            }

            m += "\r\n"+"Element selected in blue : " + "\r\n";
            foreach (TsuObject item in this.currentStrategy.GetSelectedInBlue())
            {
                m += item.ToString() + "\r\n";
            }

            string titleAndMessage = t+";"+m;
            new MessageInput(MessageType.GoodNews, titleAndMessage)
            {
                ButtonTexts = new List<string> { "Thanks" }
            }.Show();

            if (this is E.Manager.View view) view.buttons.ReleaseAll();
        }

        virtual internal void DealWithClickedItem(object tag)
        {
            
        }
    }
}
