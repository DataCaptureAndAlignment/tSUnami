﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using TSU.Logs;
using TSU.Preferences;
using MM = TSU;
using D = TSU.Common.Instruments.Device;
using E = TSU.Common.Elements;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using O = TSU.Common.Operations;
using Z = TSU.Common.Zones;
using X = TSU.Tools.Exploration;
using F = TSU.Functionalities;
using T = TSU.Connectivity_Tests;
using TSU.Common.Elements.Manager;
using TSU.Common.Dependencies.Manager;
using TSU.Common.Instruments;
using TSU.Common.Instruments.Reflector.Manager;
using TSU.Common;
using TSU.Tools.Exploration;

namespace TSU.Views
{
    /// <summary>
    /// Class that create the proper view for a module.
    /// </summary>
    public class Creator
    {
        private static Creator instance;
        public static TsuView Do(Module module)
        {
            HourGlass.Set();
            TsuView v = null;
            try
            {
                if (instance == null) instance = new Creator();

                if (module != null)
                {
                    dynamic d = module;
                    // if an exception is created here, it could be because th BAseOn mthod doesnt exist yet for the wanted module
                    v = instance.BasedOn(d);
                }
                return v;
            }
            catch (Exception ex )
            {
                throw;
            }
            finally
            {
                HourGlass.Remove();
            }
        }

        #region Managers


        private TsuView BasedOn(Common.Tsunami m)
        {
            return new TsunamiView(m) ;
        }

        private TsuView BasedOn(Module m)
        {
            return new TsuView(m);
        }

        private TsuView BasedOn(Tools.Gadgets.Module m)
        {
            return new TSU.Tools.Gadgets.View(m);
        }

        private TsuView BasedOn(TSU.Splash.Module m)
        {
            return new TSU.Splash.ScreenView(m);
        }
        private TsuView BasedOn(Common.Compute.Compensations.Manager.Module m)
        {
            TSU.Common.Compute.Compensations.Manager.View v = new TSU.Common.Compute.Compensations.Manager.View(m);
            MM.Tsunami2.Preferences.Values.Managers.Add(v);
            return v;
        }

        private TsuView BasedOn(Common.Dependencies.Manager.Module m)
        {
            TSU.Common.Dependencies.Manager.View v = new TSU.Common.Dependencies.Manager.View(m);
            MM.Tsunami2.Preferences.Values.Managers.Add(v);
            return v;
        }
        private TsuView BasedOn(TSU.Preferences.Preferences m)
        {
            return TSU.Preferences.View.Instance(m);
        }
        private TsuView BasedOn(Explorator m)
        {
            X.View v = new X.View(m){icon =R.Explore};

            MM.Tsunami2.Preferences.Values.Managers.Add(v);
            return v;
        }
        private TsuView BasedOn(E.Manager.Module m)
        {
            ManagerView v;
            v = MM.Tsunami2.Preferences.Values.Managers.Find(x => x is E.Manager.View);
            if (v == null || m.CreateUniqueView)
            {
                v = new E.Manager.View(m) { icon = R.Element_Aimant };
                MM.Tsunami2.Preferences.Values.Managers.Add(v);
            }
            return v;
        }
        private TsuView BasedOn(TSU.Manager m)
        {
            return new TSU.Views.ManagerView(m) { icon = R.Element_File};
        }
        private TsuView BasedOn(I.Manager.Module m)
        {
            TSU.Views.ManagerView v;
            v = MM.Tsunami2.Preferences.Values.Managers.Find(x => x is MM.Common.Instruments.Manager.View);
            if (v == null)
            {
                v = new TSU.Common.Instruments.Manager.View(m) { icon = R.Instrument };
                MM.Tsunami2.Preferences.Values.Managers.Add(v);
            }
            return v;
        }

        private TsuView BasedOn(I.Reflector.Manager.Module m)
        {
            I.Reflector.Manager.View v = new MM.Common.Instruments.Reflector.Manager.View(m) { icon = R.Reflector };
            MM.Tsunami2.Preferences.Values.Managers.Add(v);
            return v;
        }

        private TsuView BasedOn(I.Module m)
        {
            D.View v = new D.View(m) { icon =R.Instrument };
            return v;
        }
        private TsuView BasedOn(M.Manager m)
        {
            M.View v =  new M.View(m) { icon =R.Measurement };
            MM.Tsunami2.Preferences.Values.Managers.Add(v);
            return v;
        }
        private TsuView BasedOn(O.Manager m)
        {
            O.View v =  new O.View(m){icon =R.Explore};
            MM.Tsunami2.Preferences.Values.Managers.Add(v);
            return v;
        }
        private TsuView BasedOn(Z.Manager m)
        {
            Z.View v =  new Z.View(m) { icon =R.Explore };
            MM.Tsunami2.Preferences.Values.Managers.Add(v);
            return v;
        }
        private TsuView BasedOn(F.Menu m)
        {
            F.MenuView v =  new F.MenuView(m) { icon =R.TSU1 };
            return v;
        }

        #endregion

        #region Stations

        private TsuView BasedOn(Polar.Station.Module m)
        {
            return new Polar.Station.View(m) { icon =R.Theodolite };
        }
        private TsuView BasedOn(Level.Station.Module m)
        {
            return new Level.Station.View(m) { icon = R.Level };
        }
        private TsuView BasedOn(Line.Station.Module m)
        {
            return new Line.Station.View(m) { icon =R.Line };
        }
        private TsuView BasedOn(Tilt.Station.Module m)
        {
            return new Tilt.Station.View(m) { icon =R.Tilt };
        }
        private TsuView BasedOn(Length.Station.Module m)
        {
            return new Length.Station.View(m) { icon = R.LengthSetting };
        }
        #endregion

        #region Devices

        private TsuView BasedOn(D.AT40x.Module m)
        {
            return new D.AT40x.View(m) {  };
        }
        private TsuView BasedOn(D.DNA03.Module m)
        {
            return new D.DNA03.View(m) { };
        }

        private TsuView BasedOn(D.LS15.Module m)
        {
            return new D.LS15.View(m) { };
        }

        private TsuView BasedOn(D.WYLER.Module m)
        {
            return new D.WYLER.View(m) { };
        }

        private TsuView BasedOn(D.Manual.Ecartometer.Module m)
        {
            return new D.Manual.Ecartometer.View(m) { };
        }
        private TsuView BasedOn(D.Manual.Level.Module m)
        {
            return new D.Manual.Level.View(m) { };
        }
        private TsuView BasedOn(D.Manual.Theodolite.Module m)
        {
            return new D.Manual.Theodolite.ManualTheodoliteView(m) { };
        }
        private TsuView BasedOn(D.Simulator.Module m)
        {
            return new D.Simulator.SimulatorView(m) { };
        }

        private TsuView BasedOn(D.Manual.TiltMeter.Module m)
        {
            return new D.Manual.TiltMeter.View(m) { };
        }
        private TsuView BasedOn(D.TC2002.Module m)
        {
            return new D.TC2002.View(m) { };
        }
        private TsuView BasedOn(D.T3000.Module m)
        {
            return new D.T3000.View(m) { };
        }
        private TsuView BasedOn(D.TCRA1100.Module m)
        {
            return new D.TCRA1100.View(m) { };
        }
        private TsuView BasedOn(D.TDA5005.Module m)
        {
            return new D.TDA5005.View(m) { };
        }
        private TsuView BasedOn(D.TS60.Module m)
        {
            return new D.TS60.View(m) { };
        }


        #endregion

        #region Advanced Modules

        private TsuView BasedOn(Line.Module m)
        {
            return new Line.View(m) { icon = R.Line };
        }
        private TsuView BasedOn(Tilt.Module m)
        {
            return new Tilt.View(m) { icon = R.Tilt};
        }
        private TsuView BasedOn(Polar.Module m)
        {
            return new Polar.View(m);
        }
        private TsuView BasedOn(Level.Module m)
        {
            return new Level.View(m) { icon = R.Level};
        }
        private TsuView BasedOn(Length.Module m)
        {
            return new Length.View(m) { icon = R.LengthSetting };
        }
        #endregion

        #region Guided Modules

        private TsuView BasedOn(Common.Guided.Module m )
        {
            return new Common.Guided.View(m) { };
        }
        private TsuView BasedOn(Common.Guided.Group.Module m)
        {
            return new Common.Guided.Group.View(m) { Image =R.GuidedModule };
        }
        #endregion

        #region Test Modules

        private TsuView BasedOn(T.TestModule m)
        {
            return new T.ToBeImplemented(m) { icon =R.GuidedModule };
        }

        private TsuView BasedOn(T.DllTestModule m)
        {
            return new T.TestDllView();
        }

        private TsuView BasedOn(T.TcpTestModule m)
        {
            return new T.Client();
        }
        private TsuView BasedOn(T.InstrumentTestModule m)
        {
            return new Connectivity_Tests.TestInstrument();
        }
        private TsuView BasedOn(T.MessageTestModule m)
        {
            return new T.FormTestMessage();
        }
        #endregion

        #region Smart Modules
        private TsuView BasedOn(Tilt.Smart.Module m)
        {
            return new Tilt.Smart.View.Main(m) { };
        }

        private TsuView BasedOn(Level.Smart.Module m)
        {
            return new Level.Smart.View.Main(m) { };
        }

        private TsuView BasedOn(Line.Smart.Module m)
        {
            return new Line.Smart.View.Main(m) { };
        }
        #endregion

    }
}
