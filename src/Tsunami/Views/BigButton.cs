﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TSU.Common.Analysis;
using TSU.Logs;
using TSU.Tools.Conversions;
using TSU.Views.Message;
using P = TSU.Preferences;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Views
{
    public partial class BigButton : UserControl
    {

        #region delegate
        public event EventHandler<TsuObjectEventArgs> BigButtonClicked;
        public event EventHandler<TsuObjectEventArgs> BigButtonPreClicked;
        public event EventHandler<TsuObjectEventArgs> BigButtonSpecialClicked;
        #endregion

        #region field
        private TsuObject tag;
        //private ICommand _action;  // those are duplicate, i create my own then realize it exist in the framework
        private bool available;
        public bool Available
        {
            get
            {
                return available;
            }
            set
            {
                available = value;
                BB_Title.Enabled = value;
                BB_Description.Enabled = value;
                BB_Picture.Visible = value;
            }
        }
        private List<Action> Actions;
        private event EventHandler<TsuObjectEventArgs> AdditionalAction;
        public bool HasSubButtons { get; set; }
        private bool MouseIsIn;
        public bool isSpecial = false;
        private Point startingLocation;
        public Color ButtonColor { get; set; }
        public Color FontColor { get; set; }

        public Color OriginalColor { get; set; }

        public string name;
        private bool mouseIsDown;
        private PictureBox BB_subMenuArrow;

        public Guid Guid = Guid.NewGuid();

        private TsuView ParentView
        {
            get
            {
                return TsuView.GetTsuViewParentFrom(this, null);
            }
        }

        public bool IsFullyVisible
        {
            get
            {
                bool titleVisible = BB_Title.Width + BB_Picture.Width + 50 < Width;
                int CalculatedHeight = 0;
                var label = BB_Description;
                
                using (Graphics g = CreateGraphics())
                {
                    if (label != null && !string.IsNullOrEmpty(label.Text) && label.Font != null && label.Width > 0 && label.Height > 0)
                    {
                        SizeF size = g.MeasureString(label.Text, label.Font, label.Width);
                        CalculatedHeight = (int)Math.Ceiling(size.Height);
                    }
                }
                bool descriptionVisible = label.Height > CalculatedHeight;
                return titleVisible && descriptionVisible;
            }
        }

        public string Title
        {
            get
            {
                return BB_Title.Text;
            }
            set
            {
                BB_Title.Text = value;
            }
        }

        public string Description
        {
            get
            {
                return BB_Description.Text;
            }
            set
            {
                BB_Description.Text = value;
            }
        }
        #endregion

        protected override CreateParams CreateParams
        {
            get
            {
                // Activate double buffering at the form level.  All child controls will be double buffered as well.
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED
                return cp;
            }
        }

        #region Construtor
        private ToolTip toolTip;


        public BigButton()
        {
            InitializeComponent();
            InitializeComponent2();
    }

    public void InitializeComponent2()
        {
            Debug.ButtonCreated++;
            Debug.ButtonDisposed++;

            ApplyThemeColors();
            SetTextSize();
            BigButton_SizeChanged(this, null);
            GiveEventsToControl(this);
            foreach (Control item in Controls)
            {
                GiveEventsToControl(item);
            }
        }

        private void SetUpToolTip()
        {
            toolTip = new ToolTip();
            toolTip.AutoPopDelay = 5000;
            toolTip.InitialDelay = 1000;
            toolTip.ReshowDelay = 1000;
            toolTip.ShowAlways = true;
            toolTip.SetToolTip(this, BB_Title.Text + "\n" + BB_Description.Text);
            toolTip.SetToolTip(this.BB_Description, BB_Title.Text + "\n" + BB_Description.Text);
            toolTip.SetToolTip(this.BB_Title, BB_Title.Text + "\n" + BB_Description.Text);
            toolTip.SetToolTip(this.BB_Picture, BB_Title.Text + "\n" + BB_Description.Text);
            if(this.BB_subMenuArrow!=null)
                toolTip.SetToolTip(this.BB_subMenuArrow, BB_Title.Text + "\n" + BB_Description.Text);

        }

        private void On_InvisblePictureBox_Enter(object sender, EventArgs e)
        {
            Focus();

        }
        private void ApplyThemeColors()
        {
            BB_Title.BackColor = Tsunami2.Preferences.Theme.Colors.Transparent;
            BB_Title.ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore;
            BB_Description.BackColor = Tsunami2.Preferences.Theme.Colors.Object;
            BackColor = Tsunami2.Preferences.Theme.Colors.Background;
        }
        public BigButton(string titleAndDescription, Image image, Color? color = null, bool hasSubButtons = false)
        {
            available = true;
            SuspendLayout();

            HasSubButtons = hasSubButtons;
            InitializeComponent();
            InitializeComponent2();
            InitializeMoreComponent(titleAndDescription, image, color, hasSubButtons);


            Height = Tsunami2.Preferences.Theme.ButtonHeight;
            Name = StringManipulation.SeparateTitleFromMessage(titleAndDescription);

            //this.CreateToolTip(titleAndDescription);

            ResumeLayout(true);
            PerformLayout();

            Actions = new List<Action>();
            Paint += OnPaint;

        }

        private void InitializeMoreComponent(string titleAndDescription, Image image, Color? color = null, bool hasSubButtons = false)
        {
            if (P.Preferences.Instance.GuiPrefs != null)
                ButtonColor = P.Preferences.Instance.GuiPrefs.Theme.Colors.Object;

            ButtonColor = (color == null) ? ButtonColor : (Color)color;
            OriginalColor = ButtonColor;


            if (hasSubButtons)
            {
                BB_subMenuArrow = new PictureBox
                {
                    Image = R.Arrow,
                    Dock = DockStyle.Right,
                    Width = 20,
                    BackgroundImageLayout = ImageLayout.Stretch,
                    Height = Height,
                    SizeMode = PictureBoxSizeMode.StretchImage
                };
                Controls.Add(BB_subMenuArrow);
                GiveEventsToControl(BB_subMenuArrow);

                BB_subMenuArrow.SendToBack();
            }

            SetAttributes(titleAndDescription, image, color);
            SetUpToolTip();


            // Dock style
            Dock = DockStyle.Top;

            startingLocation.X = Top;
            startingLocation.Y = Left;
        }

        public BigButton(string titleAndDescription, Image image, Action action, Color? color = null, bool hasSubButtons = false)
            : this(titleAndDescription, image, color, hasSubButtons)
        {
            Actions.Add(action);
        }

        public BigButton(string titleAndDescription, Image image, EventHandler<TsuObjectEventArgs> eventHandler, TsuObject tag = null, Color? color = null, bool hasSubButtons = false)
            : this(titleAndDescription, image, color, hasSubButtons)
        {
            bigButtonClickedEventHandler = eventHandler;
            this.tag = tag;
            BigButtonClicked += bigButtonClickedEventHandler;

        }

        EventHandler<TsuObjectEventArgs> bigButtonClickedEventHandler;


        public void OverrideAction(Action newAction)
        {
            foreach (Delegate d in BigButtonClicked.GetInvocationList())
            {
                BigButtonClicked -= (EventHandler<TsuObjectEventArgs>)d;
            }

            Actions.Clear();
            Actions.Add(newAction);
        }

        public override string ToString()
        {
            return $"BigB {BB_Title.Text}, {BB_Description.Text} ({Guid})";
        }

        public BigButton GetClone()
        {
            BigButton b = new BigButton(BB_Title.Text + ";" + BB_Description.Text, BB_Picture.BackgroundImage, BackColor);
            if (BigButtonClicked != null)
                foreach (EventHandler<TsuObjectEventArgs> item in BigButtonClicked.GetInvocationList())
                {
                    b.BigButtonClicked += item.Clone() as EventHandler<TsuObjectEventArgs>;
                }

            b.Actions.AddRange(Actions);
            return b;
        }

        #endregion

        #region Action


        public void Do()
        {
            try
            {
                T.Macro.DoTrace($"BB: {Title} (beginning)");

                if (Actions.Count != 0)
                {
                    foreach (Action item in Actions)
                    {
                        if (item.Target is TsuView v)
                            v.TryAction(item);
                        else
                            item();
                    }

                    AdditionalAction?.Invoke(this, new TsuObjectEventArgs { TsuObject = tag });

                }

                Log.AddEntryAsFYI(null, $"Button Clicked: '{Title}'");

                OnBigButtonClicked(tag);
            }

            catch (TsuException ex)
            {
                new MessageInput( MessageType.Critical, ex.Message).Show();
                Cursor.Current = Cursors.Default;
            }

            catch (Exception ex)
            {
                MessageTsu.ShowMessage(ex);
            }
            HourGlass.Remove();
        }

        public void AddAction(Action action)
        {
            Actions.Add(action);

        }

        public void AddAction(EventHandler<TsuObjectEventArgs> action, TsuObject Tag)
        {
            AdditionalAction = action;
            tag = Tag;
        }
        #endregion


        public static List<BigButton> GetButtonsFrom(Control control)
        {
            List<BigButton> allControls = new List<BigButton>();
            foreach (Control item in control.Controls)
            {
                if (item is BigButton bb)
                    allControls.Add(bb);
                else
                    allControls.AddRange(GetButtonsFrom(item));
            }
            return allControls;
        }

        private void FindButtonStartingWithLetterInParent(char firstLetter)
        {
            foreach (Control item in Parent.Controls)
            {
                if (item is BigButton button)
                {
                    if (button.Title[0] == firstLetter)
                        button.Focus();
                }
            }
        }

        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);
            buttonForFocus.Focus();

        }

        //protected override void OnLostFocus(EventArgs e)
        //{
        //    base.OnLostFocus(e);
        //    this.ShowButtonAsNotHoovered();
        //}

        #region design

        /// permet de changer l'image du bigbutton
        internal void ChangeImage(Image image)
        {
            BB_Picture.BackgroundImage = image;
        }

        public Color GetLighterColor(Color c, double factor)
        {
            double r = c.R + (255 - c.R) * factor;
            double b = c.B + (255 - c.B) * factor;
            double g = c.G + (255 - c.G) * factor;

            // for darker it need to be implemented
            return Color.FromArgb(255, (int)r, (int)g, (int)b);
        }

        private string title;
        private string description;

        internal void SetAttributes(string titleAndDescription, Image image = null, Color? color = null)
        //permet de mettre à jour les attributs visible du bigbutton.
        {
            Action a = () =>
            {
                AutoSize = false;
                title = StringManipulation.SeparateTitleFromMessage(titleAndDescription);
                BB_Title.Text = title;
                description = StringManipulation.SeparateMessageFromTitle(titleAndDescription);
                BB_Description.Text = description;
                if (image != null) BB_Picture.BackgroundImage = image;
                Top = 15;
                OriginalColor = color ?? ButtonColor;
                ApplyColor(OriginalColor);

            };
            if (Tsunami2.Properties != null)
                Tsunami2.Properties.InvokeOnApplicationDispatcher(a);
            else
                a();

        }
        private string dynamicCommentInTitle = "";

        public void AddCommentToTitle(string comment)
        {
            dynamicCommentInTitle = comment;
            ReplaceTitle(title);
        }

        public void ReplaceTitle(string newTitle, bool keepDynamicComment = true)
        {
            title = newTitle;
            BB_Title.Text = newTitle;
            if (keepDynamicComment)
            {
                if (dynamicCommentInTitle != "")
                    BB_Title.Text += ": ";

                BB_Title.Text += dynamicCommentInTitle;
            }
            TSU.Debug.WriteInConsole($"'{Name}' title changed to '{BB_Title.Text}'");
        }

        public void SetColors(Color color)
        {
            if (color != null)
            {
                OriginalColor = color;
                ButtonColor = color;
            }
            ApplyColor(color);
        }

        /// <summary>
        /// by default we dont set teh font tu use single instance for all tsunai text (in the past we have memory leak problems)
        /// </summary>
        /// <param name="font1"></param>
        /// <param name="font2"></param>
        public void SetTextSize(Font font1 = null, Font font2 = null)
        {
            if (font1 == null || font2 == null)
            {
                BB_Title.Font = Tsunami2.Preferences.Theme.Fonts.Normal;
                BB_Description.Font = Tsunami2.Preferences.Theme.Fonts.Tiny;
            }
            else
            {
                BB_Title.Font = font1;
                BB_Description.Font = font2;
            }

            BB_Picture.Width = Tsunami2.Preferences.Theme.ButtonHeight - 6;
            if (BB_subMenuArrow != null)
                BB_subMenuArrow.Height = Tsunami2.Preferences.Theme.ButtonHeight - 6;
        }

        private void ApplyColor(Color? color)
        {
            Color c = color ?? OriginalColor;
            BackColor = c;
            BB_Title.BackColor = c;
            BB_Description.BackColor = c;
            if (BB_subMenuArrow != null) BB_subMenuArrow.BackColor = c;

            // Font Color
            FontColor = Colors.ChooseContrastedColorBetweenBlackAndWhite(c);

            if (BB_subMenuArrow != null) BB_subMenuArrow.ForeColor = FontColor;
            BB_Description.ForeColor = FontColor;
            BB_Title.ForeColor = FontColor;

        }
        /// <summary>
        /// Met e bouton en surbrillance temporairement sans remplacer la couleur originale
        /// </summary>
        /// <param name="highlightColor"></param>
        public void HighLightWithColor(Color? highlightColor = null)
        {
            highlightColor = highlightColor ?? Tsunami2.Preferences.Theme.Colors.Highlight;
            ButtonColor = (Color)highlightColor;
            ApplyColor(highlightColor);
        }

        public void RestoreColor()
        {
            ApplyColor(OriginalColor);

            ButtonColor = OriginalColor;
        }

        ///// <summary>
        ///// Change the color of the button in light to highlight it in a menu of several buttons
        ///// </summary>
        //public void HighlightButton()
        //{
        //    this.SetColors(Tsunami2.Preferences.Theme._Colors.ButtonHighlight);
        //}
        /// <summary>
        /// Change le texte du bouton
        /// </summary>
        /// <param name="newUtility"></param>
        public void ChangeNameAndDescription(string newNameAndDescription)
        {
            Title = StringManipulation.SeparateTitleFromMessage(newNameAndDescription); ;
            BB_Title.Text = Title;
            Description = StringManipulation.SeparateMessageFromTitle(newNameAndDescription);
            BB_Description.Text = Description;
            newNameAndDescription = newNameAndDescription.Replace(";", "\n");
            //Tsunami2.View.MainToolTip.SetToolTip(this, newNameAndDescription);
            toolTip.SetToolTip(this, newNameAndDescription);
            toolTip.SetToolTip(this.BB_Description, newNameAndDescription);
            toolTip.SetToolTip(this.BB_Title, newNameAndDescription);
            toolTip.SetToolTip(this.BB_Picture, newNameAndDescription);
            if (this.BB_subMenuArrow != null)
                toolTip.SetToolTip(this.BB_subMenuArrow, newNameAndDescription);
            Refresh();
        }


        Timer timer = new Timer { Interval = 1000 };
        //ToolTip tooltip = new ToolTip();

        internal void ShowButtonAsNotHoovered()
        {
            if (available)
            {
                ApplyColor(ButtonColor);
                Padding = new Padding(2);
            }

        }
        internal void ShowButtonAsHoovered()
        {
            if (available)
            {
                ApplyColor(GetLighterColor(ButtonColor, 0.4));
                Padding = new Padding(1);
            }
        }
        private void ShowButtonAsPressedDown()
        {
            if (available)
            {
                ApplyColor(GetLighterColor(ButtonColor, 0.6));
                Padding = new Padding(0);
            }
        }
        #endregion

        #region events
        private void OnPaint(object sender, PaintEventArgs e)
        {
            if (P.Preferences.Instance.GuiPrefs == null) return;
            if (P.Preferences.Instance.GuiPrefs.UserType == P.Preferences.UserTypes.Guided && available == false)
                Hide();
        }

        private void GiveEventsToControl(Control c)
        {
            c.MouseDown += BigButton_MouseDown;
            c.MouseMove += BigButton_MouseMove;
            c.MouseUp += BigButton_MouseUp;
            c.MouseEnter += BigButton_MouseEnter;
            c.MouseLeave += BigButton_MouseLeave;
        }

        private void RemoveEventsToControl(Control c)
        {
            c.MouseDown -= BigButton_MouseDown;
            c.MouseMove -= BigButton_MouseMove;
            c.MouseUp -= BigButton_MouseUp;
            c.MouseEnter -= BigButton_MouseEnter;
            c.MouseLeave -= BigButton_MouseLeave;
        }

        private void BigButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ShowButtonAsPressedDown();
                mouseIsDown = true;
            }
            else if (e.Button == MouseButtons.Right)
            {
                if (mouseIsDown)
                    OnBigButtonSpecialClicked(null);
            }
        }

        private void BigButton_MouseMove(object sender, MouseEventArgs e)
        {

            if (MouseIsIn)
                return;
            // if me miss the mous is in show as inside the button
            ShowButtonAsHoovered();
            MouseIsIn = true;
        }


        private void BigButton_MouseUp(object sender, MouseEventArgs e)
        {
            ShowButtonAsNotHoovered();
            if (mouseIsDown) // yes or not to cancel this condition
            {
                switch (e.Button)
                {
                    case MouseButtons.Left:
                        if (available)
                        {
                            Point pos = Cursor.Position;
                            Point pos2 = PointToClient(pos);

                            if (pos2.X < 0) break;
                            if (pos2.Y < 0) break;
                            if (pos2.X > Width) break;
                            if (pos2.Y > Height) break;

                            if (!HasSubButtons)
                            {
                                if (Parent is ContextMenuStrip || Parent is TsuPopUpMenu)
                                    Parent.Hide();
                            }

                            Do();
                        }
                        break;
                    case MouseButtons.Middle:
                        Available = true;
                        break;
                    case MouseButtons.None:
                        break;
                    case MouseButtons.Right:
                        Available = false;
                        break;
                    case MouseButtons.XButton1:
                        break;
                    case MouseButtons.XButton2:
                        break;
                }
                mouseIsDown = false;

            }
            //TSU.Debug.WriteInConsole("Mouse up on " + this.BB_Title.Text);
            Debug.Timing("Action button ends");
        }

        private void BigButton_MouseEnter(object sender, EventArgs e)
        {
            ShowButtonAsHoovered();
            if (!MouseIsIn)
            {
                if (Tsunami2.Properties.View != null && Tsunami2.Properties.View.visiblePopUpMenu != null)
                    if (!Tsunami2.Properties.View.visiblePopUpMenu.Controls.Contains(this))
                        return;

                var a = Form.ActiveForm;
                if (!(a is MessageTsu))
                    Focus();
                timer.Stop();
                timer.Start();                
            }
            MouseIsIn = true;
        }
        private void BigButton_MouseLeave(object sender, EventArgs e)
        {
            // if still in the button, we just left on of the subcontrols
            if (ClientRectangle.Contains(PointToClient(MousePosition)))
                return;
            OnLeave(e);

            ShowButtonAsNotHoovered();

            mouseIsDown = false;
            MouseIsIn = false;
        }

        protected virtual void OnBigButtonClicked(TsuObject o)
        {
            BigButtonPreClicked?.Invoke(this, new TsuObjectEventArgs { TsuObject = o });
            BigButtonClicked?.Invoke(this, new TsuObjectEventArgs { TsuObject = o });

            T.Macro.DoTrace($"BB: {Title} end");
        }

        protected virtual void OnBigButtonSpecialClicked(TsuObject o)
        {
            BigButtonSpecialClicked?.Invoke(this, new TsuObjectEventArgs { TsuObject = o });
        }

        #endregion

        public override int GetHashCode()
        {
            return BB_Description.Text.GetHashCode();
        }
        public void DisableButton()
        {
            if (Tsunami2.Properties != null)
                Tsunami2.Properties.InvokeOnApplicationDispatcher(Disable);
            else
                Disable();
            return;

            void Disable()
            {
                Enabled = false;
            }
        }
        public void EnableButton()
        {
            if (Tsunami2.Properties != null)
                Tsunami2.Properties.InvokeOnApplicationDispatcher(Enable);
            else
                Enable();
            return;

            void Enable()
            {
                Enabled = true;
            }
        }

        int LastHeight;
        private void BigButton_SizeChanged(object sender, EventArgs e)
        {
            if (Tsunami2.Preferences.Theme.ButtonHeight != LastHeight)
            {
                Height = Tsunami2.Preferences.Theme.ButtonHeight;
                SetTextSize();
            }

            if (Width < Height) Width = Height;
            BB_Picture.Width = BB_Picture.Height;
            LastHeight = Height;
        }

        private void buttonForFocus_Enter(object sender, EventArgs e)
        {
            ShowButtonAsHoovered();
        }

        private void buttonForFocus_Leave(object sender, EventArgs e)
        {
            ShowButtonAsNotHoovered();
        }

        private void buttonForFocus_Click(object sender, EventArgs e)
        {
            //this.Do();
        }

        private void buttonForFocus_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyData)
            {
                case Keys.Right:
                    if (HasChildren)
                        Do();
                    return;
                case Keys.Space:
                    Do();
                    return;
                case Keys.Down:
                    ParentView.SelectNextButton(this, forward: true);
                    return;
                case Keys.Left:
                    if (Parent is TsuPopUpMenu tpum)
                        tpum.Hide();
                    return;
                case Keys.Up:
                    ParentView.SelectNextButton(this, forward: false);
                    return;
                case Keys.Shift | Keys.Right:
                    if (HasSubButtons)
                        Invoke(Actions[0]);
                    return;
                case Keys.Shift | Keys.Down:
                case Keys.Shift | Keys.Up:
                    T.KeyboardNavigation.MoveToParentView(this);
                    return;
                case Keys.Shift | Keys.Tab:
                    return;
                case Keys.Tab:
                    return;
                case Keys.Escape:
                    T.KeyboardNavigation.MoveToParentView(this);
                    return;

                default:
                    FindButtonStartingWithLetterInParent(e.KeyData.ToString()[0]);
                    //Tsunami2.Properties.View.FindButton(keyData.ToString()[0]);
                    return;
            }
        }
    }

    #region as menu item

    public class BigButtonMenuItem : ToolStripControlHost
    {
        public BigButton bigButton;

        public BigButtonMenuItem(BigButton button)
            : base(button)
        {
            bigButton = Control as BigButton;
            Name = button.name;
        }

        public override string ToString()
        {
            return "BigButton:" + Name;
        }


        //public BigButtonMenuItem(string title, string description, Image image, ICommand action, Color? color = null)
        //    : base(new BigButton(title, description, image, action, color))
        //{
        //    this.bigbutton = this.Control as BigButton;
        //    this.bigbutton.IsContextMenuItem = true;
        //    this.Name = title;
        //}

        public BigButtonMenuItem(string titleAndDescription, Image image, Action action, Color? color = null)
            : base(new BigButton(titleAndDescription, image, action, color))
        {
            bigButton = Control as BigButton;
            Name = StringManipulation.SeparateTitleFromMessage(titleAndDescription);
        }

        public BigButtonMenuItem(string titleAndDescription, Image image, EventHandler<TsuObjectEventArgs> eventHandler, TsuObject tag = null, Color? color = null)
            : base(new BigButton(
                titleAndDescription,
                image, eventHandler, tag, color))
        {
            bigButton = Control as BigButton;
            Name = StringManipulation.SeparateTitleFromMessage(titleAndDescription);
        }

    }
    #endregion

    #region events

    public class TsuObjectEventArgs : EventArgs
    {
        public TsuObject TsuObject { get; set; }
    }

    #endregion
}
