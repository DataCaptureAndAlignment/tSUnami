﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSU.ENUM;
using TSU.Views;
using MM = TSU;
using M = TSU;
using TSU.Common.Instruments;
using TSU.Common.Measures;
using TSU.Logs;
using Accord.IO;
using TSU.Tools;

namespace TSU.Views
{
    public class CompositeView : ModuleView
    {
        public new string _Name
        {
            get
            {
                string name = "";
                foreach (TsuView item in _Views)
                {
                    name += item._Name + " ";
                }
                return name;
            }
            set
            {

            }
        }
        internal List<TsuView> _Views;
        protected CompositeViewStrategy compositeViewStrategy;

        internal SplitContainer splitContainerStationVsInstrument;


        protected Panel lowerPanel;
        public CompositeView() : base()
        {

        }

        public bool isInitializaling = false;

        public CompositeView(M.IModule module, Orientation splitOrientation, ModuleType? TopMenuType = null, Bitmap icon = null)
            : base(module)
        {
            InitializeComponent();
            this.AdaptTopPanelToButtonHeight();

            this.compositeViewStrategy = new CompositeViewStrategy.FullDockSplit();

            this.TopLevel = false;
            this.SuspendLayout();
            this._Views = new List<TsuView>();
            this._Name = _Module._Name;


            // Create the ToolTip and associate with the Form container.
            this.ToolTip = new ToolTip();

            // Set up the delays for the ToolTip.
            this.ToolTip.AutoPopDelay = 5000;
            this.ToolTip.InitialDelay = 1000;
            this.ToolTip.ReshowDelay = 500;

            this.ResumeLayout();
        }

        protected override void Dispose(bool disposing)
        {
            try
            {
                if (_Views != null)
                    _Views.Clear();
                _Views = null;

                compositeViewStrategy = null;

                if (lowerPanel != null)
                    lowerPanel.Controls.Clear();

                var split = this.splitContainerStationVsInstrument;
                if (split != null)
                {
                    split.Panel1.Controls.Clear();
                    split.Panel2.Controls.Clear();
                }
                base.Dispose(disposing);
            }
            catch { }
        }

        public override void SelectNextButton(BigButton current, bool forward)
        {
            if (this._PanelTop.Controls.Count > 0)
                if (current == this._PanelTop.Controls[0])
                {
                    if (this._PanelBottom.Controls.Count > 0)
                        if (this._PanelBottom.Controls[0] is SplitContainer sc)
                        {
                            if (sc.Panel1.Controls.Count > 0)
                                sc.Panel1.Controls[0].Focus();

                        }
                }
            base.SelectNextButton(current, forward);
        }



        protected override void OnKeyUp(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Tab)
            {
                var next = this.GetNextControl(this.ActiveControl, forward: true);
                if (next != null) next.Focus();
            }
            base.OnKeyUp(e);
        }
        protected override void OnGotFocus(EventArgs e)
        {
            Invalidate();
            base.OnGotFocus(e);
        }

        protected override void OnLostFocus(EventArgs e)
        {
            base.OnLostFocus(e);
            Invalidate();
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CompositeView));
            this.SuspendLayout();
            // 
            // _PanelBottom
            // 
            resources.ApplyResources(this._PanelBottom, "_PanelBottom");
            // 
            // _PanelTop
            // 
            resources.ApplyResources(this._PanelTop, "_PanelTop");
            // 
            // CompositeView
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Name = "CompositeView";
            this.ResizeBegin += new System.EventHandler(this.CompositeView_ResizeBegin);
            this.Controls.SetChildIndex(this._PanelTop, 0);
            this.Controls.SetChildIndex(this._PanelBottom, 0);
            this.ResumeLayout(false);

        }

        /// <summary>
        /// Add a view with a plus image to add a new station to  the module
        /// </summary>
        internal void AddPlusView(BigButton buttonNew)
        {
            TsuView v = new TsuView();
            //var a = v._Module;
            v.Name = "AddingViewPlus";
            v.BackgroundImage = R.Station;
            v.BackColor = TSU.Tsunami2.Preferences.Theme.Colors.Background;
            //BigButton newStationButton = buttonNew.GetClone();
            //newStationButton.Dock = DockStyle.None;
            //newStationButton.Anchor = AnchorStyles.None;
            //v.Controls.Add(newStationButton);

            v.BackgroundImageLayout = ImageLayout.Center;
            v.Click += delegate
            {
                this.contextButtons.Clear();
                this.contextButtons.Add(buttonNew);
                this.ShowPopUpMenu(contextButtons);
            };
            v.MouseHover += delegate { v.Cursor = Cursors.Hand; };

            v.ShowDockedFill();
            this.AddView(v);
        }

        public override void AddView(TsuView view)
        {
            if (!(view._Module is M.Manager))
            {
                view.TopLevel = false;
                if (!(this._Views.Contains(view)))
                    this._Views.Add(view);
            }
        }

        internal override Orientation SetSplitContainerOrientation(Orientation orientation, bool invert = false)
        {
            var split_S_I = this.splitContainerStationVsInstrument;

            Orientation o = orientation;
            double factor = 1;
            if (invert)
            {
                if (orientation == Orientation.Horizontal)
                {
                    o = Orientation.Vertical;
                    factor = (double)split_S_I.Width / (double)split_S_I.Height;
                }
                else
                {
                    o = Orientation.Horizontal;
                    factor = (double)split_S_I.Height / (double)split_S_I.Width;
                }
            }

            // set new orientation
            split_S_I.Orientation = o;

            //split_S_I.SplitterDistance = (int)(factor * split_S_I.SplitterDistance);

            // set half distance of paretn
            //if (split_S_I.Parent != null)
            //{
            //    if (o == Orientation.Horizontal)
            //        split_S_I.SplitterDistance = 3 * split_S_I.Parent.Height / 5;
            //    else

            //        split_S_I.SplitterDistance = split_S_I.Parent.Width / 2;
            //}

            Tsunami2.Preferences.Values.GuiPrefs.InstrumentSplitDistance = split_S_I.SplitterDistance;
            Tsunami2.Preferences.Values.GuiPrefs.InstrumentSplitOrientation = split_S_I.Orientation;
            Tsunami2.Preferences.Values.SaveGuiPrefs();

            // deal (invert) with inside containers
            {
                List<Control> compositeViews = new List<Control>();

                // Collect inside composite viewws
                foreach (var item in split_S_I.Panel1.Controls)
                    if (item is TsuView)
                        compositeViews.Add(item as TsuView);

                foreach (var item in split_S_I.Panel2.Controls)
                {
                    if (item is TsuView)
                        compositeViews.Add(item as TsuView);
                }

                // inverts them
                foreach (TsuView item in compositeViews)
                {
                    item.SetSplitContainerOrientation(o, invert: true);
                }
            }
            return o;
        }

        public override void UpdateView()
        {
            try
            {
                isInitializaling = true;
                if (this.Visible && (!(this is Common.Station.View)))
                {
                    base.UpdateView();
                    this._PanelBottom.Controls.Clear();

                    //select the wanted views
                    List<TsuView> wantedList = new List<TsuView>();

                    Guid activeStation;
                    if (this._Module is Common.FinalModule)
                        activeStation = (this._Module as Common.FinalModule).idOfActiveStation;
                    else
                        activeStation = Guid.Empty;
                    if (activeStation != Guid.Empty)
                        foreach (TsuView item in this._Views)
                        {
                            if (item._Module is Common.Station.Module)
                            {
                                if (item._Module.Guid == activeStation)
                                    wantedList.Add(item);
                            }
                            else
                            {
                                //wantedList.Add(item);
                            }
                        }
                    else wantedList = this._Views.ToList();


                    // add the wanted views
                    bool first = true;
                    foreach (TsuView item in wantedList)
                    {
                        if (first)
                        {
                            
                            if (item is Polar.Station.View polarView)
                            {
                                polarView.isInitializaling = true;
                                this._PanelBottom.Controls.Add(polarView);

                                
                                var a = polarView.splitContainerStationVsInstrument.SplitterDistance;
                                var b = polarView._PanelBottom.Controls[0] as SplitContainer;
                                var c = b.Panel1.Height;
                                var d1 = b.Panel2.Height;
                                int distance = Tsunami2.Preferences.Values.GuiPrefs.InstrumentSplitDistance;

                                if (Macro.IsPlaying || Macro.IsRecording || Debug.IsRunningInATest)
                                    distance -= 50;
                                polarView.splitContainerStationVsInstrument.SplitterDistance = distance;
                                polarView.isInitializaling = false;
                            }
                            else
                                this._PanelBottom.Controls.Add(item);
                            first = false;
                        }
                        else
                        {
                            this.compositeViewStrategy.SplitAndAdd(this, item);
                        }
                        item.UpdateView();
                        item.ShowDockedFill();
                    }
                }
                this.Update();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            { 
                isInitializaling = false;
            }
        }
        private void CompositeView_ResizeBegin(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
            {
                this.Dock = DockStyle.Fill;
            }
            else
                this.Dock = DockStyle.None;
        }
        //Change le texte dans le top button pour afficher la station active
        internal virtual void SetStationNameInTopButton(Common.Station.Module stationModule)
        {

        }
    }
    public abstract class CompositeViewStrategy
    {
        public abstract void SplitAndAdd(CompositeView subject, TsuView view);

        public class FullDockSplit : CompositeViewStrategy
        {
            public override void SplitAndAdd(CompositeView subject, TsuView view)
            {
                SplitContainer splitContainer = new SplitContainer()
                {
                    Dock = DockStyle.Fill,
                    Orientation = Orientation.Horizontal,
                };
                M.Tsunami2.Preferences.Theme.ApplyTo(splitContainer);

                splitContainer.DoubleClick += delegate
                {
                    splitContainer.SplitterDistance = TSU.Tsunami2.Preferences.Theme.ButtonHeight + 10;
                };
                splitContainer.Panel1.Controls.Add(subject._PanelBottom.Controls[0]);
                splitContainer.Panel2.Controls.Add(view);


                subject._PanelBottom.Controls.Clear();
                subject._PanelBottom.Controls.Add(splitContainer);
            }
        }
    }
}
