﻿namespace TSU.Views
{
    partial class TsuView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            Globals.createdViews.Remove(this);
            this.ParentView = null;

            this._Module = null;
            this._SelectionView = null;

            if (this.observers != null)
                this.observers.Clear();
            this.observers = null;

            if (this.PopUpMenu != null)
                this.PopUpMenu.Dispose();
            this.PopUpMenu = null;

            if (this.WaitingForm != null)
                this.WaitingForm.Dispose();
            this.WaitingForm = null;

            if (this.contextButtons != null)
            {
                for (int i = 0; i < contextButtons.Count; i++)
                {
                    if (contextButtons[i] != null)
                        contextButtons[i].Dispose();
                    contextButtons[i] = null;
                }
                contextButtons.Clear();
                contextButtons = null;
            }
            this.contextButtons = null;

            if (this.ContextMenuStrip != null)
                this.ContextMenuStrip.Dispose();
            this.ContextMenuStrip = null;

            if (this._SelectionView != null)
                this._SelectionView.Dispose();
            this._SelectionView = null;

            if (this.ToolTip != null)
                this.ToolTip.Dispose();
            this.ToolTip = null;


            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTipTsuView = new System.Windows.Forms.ToolTip(this.components);
            this.buttonForFocusWithTabIndex0 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonForFocusWithTabIndex0
            // 
            this.buttonForFocusWithTabIndex0.Location = new System.Drawing.Point(8, -24);
            this.buttonForFocusWithTabIndex0.Name = "buttonForFocusWithTabIndex0";
            this.buttonForFocusWithTabIndex0.Size = new System.Drawing.Size(61, 19);
            this.buttonForFocusWithTabIndex0.TabIndex = 0;
            this.buttonForFocusWithTabIndex0.Text = "Iman hidden button to give focus to the tsuview";
            this.buttonForFocusWithTabIndex0.UseVisualStyleBackColor = true;
            this.buttonForFocusWithTabIndex0.Enter += new System.EventHandler(this.buttonForFocusWithTabIndex0_Enter);
            this.buttonForFocusWithTabIndex0.KeyUp += new System.Windows.Forms.KeyEventHandler(this.buttonForFocusWithTabIndex0_KeyUp);
            this.buttonForFocusWithTabIndex0.Leave += new System.EventHandler(this.buttonForFocusWithTabIndex0_Leave);
            // 
            // TsuView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(419, 266);
            this.Controls.Add(this.buttonForFocusWithTabIndex0);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "TsuView";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Text = "TsuView2";
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TsuView_KeyUp);
            this.ResumeLayout(false);
        }

        #endregion

        internal System.Windows.Forms.ToolTip toolTipTsuView;
        public System.Windows.Forms.Button buttonForFocusWithTabIndex0;
    }
}