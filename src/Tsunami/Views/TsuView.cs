using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Windows.Forms;
using TSU.Common;
using TSU.Common.Instruments;
using TSU.Logs;
using TSU.Tilt.Smart.View;
using TSU.Tools.Conversions;
using TSU.Views.Message;
using static TSU.Views.Message.MessageTsu;
using I = TSU.Common.Instruments.Device.WYLER;
using P = TSU.Tsunami2.Preferences;
using R = TSU.Properties.Resources;
using Station = TSU.Tilt.Station;
using T = TSU.Tools;
using View = TSU.Common.Elements.Manager.View;

namespace TSU.Views
{
    public partial class TsuView : Form, IObserver<TsuObject>, ITsuObject, IChildItem<TsuView>
    {
        #region Fields

        private ProgressMessage waitingForm;
        public ProgressMessage WaitingForm
        {
            get
            {
                if (waitingForm == null)
                    waitingForm = new ProgressMessage();
                return waitingForm;
            }
            set
            {
                waitingForm?.Dispose();// probably overkill that do the same as GC.
                waitingForm = value;
            }
        }

        public Guid Guid { get; set; }
        public bool MustBeUpdated;
        public MessageModuleView _SelectionView;
        protected ContextMenuStrip contextMenuStrip;
        public List<Control> contextButtons;
        internal TsuPopUpMenu PopUpMenu;

        #endregion

        #region Properties

        public ChildItemCollection<TsuView, TsuView> childViews { get; private set; }
        public Module _Module { get; set; }
        public string _Name { get; set; }
        public bool IsWaiting;

        public TsuView ParentView { get; internal set; }

        TsuView IChildItem<TsuView>.Parent
        {
            get
            {
                return ParentView;
            }
            set
            {
                ParentView = value;
            }
        }
        private bool resizable;

        public bool Resizable
        {
            get { return resizable; }
            set
            {
                resizable = value;
                AllowMovement(this);
            }
        }

        public bool isShownInMessageTsu;
        public Bitmap icon;
        #endregion

        internal void HidePopupMenu()
        {
            PopUpMenu.Clear();
            PopUpMenu.Hide();
        }

        #region  Contructor
        public TsuView()
        {
            int id = Debug.GetNextId();
            InitializeComponent();
            AllowTransparency = false;
            Init();

            if (!(this is TsuPopUpMenu))
                PopUpMenu = new TsuPopUpMenu(this, "main ");
        }

        public TsuView(IModule linkedModule) : this()
        {
            int id = Debug.GetNextId();
            if (linkedModule != null)
            {
                _Module = linkedModule as Module;
                _Name = string.Format(R.T459, linkedModule._Name);
            }
            Subscribe(linkedModule as Module);
        }

        private void Init()
        {
            childViews = new ChildItemCollection<TsuView, TsuView>(this);
            observers = new List<IObserver<TsuObject>>();
            contextMenuStrip = new ContextMenuStrip();
            Guid = Guid.NewGuid();
            Icon = R.tSUnami_32;
            Validating += test;
            Paint += OnPaint;
        }

        public static TsuView GetTsuViewParentFrom(Control control, Type typeOfView)
        {
            if (control.Parent == null) return Tsunami2.Properties.View;
            if (typeOfView == null) // any TSU view is ok
            {
                if (control.Parent is TsuView tv)
                    return tv;
            }
            else
            {
                if (control.Parent.GetType() == typeOfView)
                    return control.Parent as TsuView;
            }
            return GetTsuViewParentFrom(control.Parent, typeOfView);
        }

        public virtual void SelectNextButton(BigButton current, bool forward)
        {
            // to be override in menuview, popupMenu, steps, etc...
        }

        public virtual void SelectOtherView()
        {
            // to be override in menuview, popupMenu, steps, etc...
        }



        private void OnPaint(object sender, PaintEventArgs e)
        {
            if (MustBeUpdated) UpdateView();
        }

        private void test(object sender, CancelEventArgs e)
        {
            Debug.WriteInConsole("validating");
        }


        #endregion

        #region  Update
        /// <summary>
        /// Will force old childViews to update
        /// </summary>
        /// 
        public virtual void UpdateView()
        {
            foreach (TsuView item in childViews)
            {
                try
                {
                    item.UpdateView();
                }
                catch (Exception ex)
                {
                    ShowMessageOfBug(string.Format(R.T541, item._Name, ":\r\n"), ex, R.T_OK + "!");
                }
            }
            Debug.WriteInConsole("TSU.VIEW.UPDATE " + _Name + " " + R.T_HAS_BEEN_UPDATED);
            MustBeUpdated = false;
        }
        public virtual void AddView(TsuView view)
        {
            if (!(childViews.Contains(view)))
            {
                try
                {
                    childViews.Add(view);
                }
                catch (Exception ex)
                {
                    ShowMessageOfBug(string.Format(R.T543, view._Name, _Name, ":\r\n"), ex, R.T_OK + "!");
                }
            }
            else
                view.BringToFront();
        }

        public virtual void FocusView()
        {

        }
        #endregion

        #region  Docking
        public void ShowDockedNone() { Dock = DockStyle.None; ShowFormAsUserControl(); }
        public void ShowDockedLeft() { Dock = DockStyle.Left; ShowFormAsUserControl(); }
        public void ShowDockedTop() { Dock = DockStyle.Top; ShowFormAsUserControl(); }
        public void ShowDockedRight() { Dock = DockStyle.Right; ShowFormAsUserControl(); }
        public void ShowDockedFill() { Dock = DockStyle.Fill; ShowFormAsUserControl(); }
        public void ShowDockedBottom() { Dock = DockStyle.Bottom; ShowFormAsUserControl(); }
        protected void ShowFormAsUserControl()
        {
            TopLevel = false;
            FormBorderStyle = FormBorderStyle.None;
            Visible = true;
            Show();
        }
        public void ShowOnTop()
        {
            if (this is View v && v._Module is Common.Elements.Manager.Module m)
                m.ShowAllElementIncludedInParentModule();
            Show();
            UpdateView();
            BringToFront();
            //this.ShowDialog();
        }
        /// <summary>
        /// Show the TSU View and set the active measurement module in the menu
        /// </summary>
        /// <param name="menu"></param>
        public void ShowActiveModule()
        {
            Module m = null;
            if (this is Common.Guided.View cgView) m = cgView.Module;
            if (this is Common.Guided.Group.View cggView) m = cggView.Module;
            if (this is Line.View lnView) m = lnView.Module;
            if (this is Level.View lvView) m = lvView.Module;
            if (this is Polar.View pView) m = pView.Module;
            if (this is Tilt.View tView) m = tView.Module;
            if (this is Length.View lgView) m = lgView.Module;
            if (this is Main tsvMain) m = tsvMain.Module;
            if (this is Level.Smart.View.Main lsvMain) m = lsvMain.Module;

            if (m?.ParentModule is Tsunami t)
            {
                m.Finished = false;
                t.Menu._activeMeasurementModule = m;
                if (m is Line.Module || m is Level.Module || m is Tilt.Module
                    || m is Common.Guided.Module || m is Length.Module || m is Polar.Module)
                    ((FinalModule)m).SetInstrumentInActiveStationModule();
                if (m is Common.Guided.Group.Module grGm && grGm.ActiveSubGuidedModule != null)
                    grGm.ActiveSubGuidedModule.SetInstrumentInActiveStationModule();
                if (m is Tilt.Smart.Module srm)
                    srm.ViewModel.LoadModule(srm);
                if (m is Level.Smart.Module slm)
                    slm.ViewModel.LoadModule(slm);

                t.Menu.RefreshOpenedAndFinishedModule();
            }
            ShowOnTop();
        }
        #endregion

        #region  Message to show modules

        public virtual string ShowInMessageTsu(string titleAndMessage, string middleButton, Action middleAction,
            string rightButton = null, Action rightAction = null, string leftButton = null, Action leftAction = null,
            Color? background = null, Color? foreground = null, bool showdialog = true, bool fullSize = false,
            DsaFlag flagDont = null, EventHandler onShowingMessage = null)
        {
            if (CancelBasedOnDsaFlag(flagDont, middleButton, rightButton, out string buttonTextToReturn))
                return buttonTextToReturn;
            string s = null;
            P.Values.InvokeOnApplicationDispatcher(Action);
            return s;

            void Action()
            {
                const int SPACE = 200;

                Name = titleAndMessage;
                TopLevel = false;
                isShownInMessageTsu = true;
                Visible = true;
                resizable = false;
                FormBorderStyle = FormBorderStyle.None;

                // Define the initial size of the control so that the MessageModuleView takes enough space.
                // At this point it may be bigger than the screen size, but this will be fixed when we call AdjustToScreenSize
                if (this is ManagerView mv)
                {
                    int h = mv.currentStrategy.GetRecommandedHeight();
                    if (h == -1)
                    {
                        var i = Screen.FromPoint(Cursor.Position).WorkingArea.Height;
                        Height = i;
                    }
                    else if (h < 350)
                        Height = 350;
                    else
                        Height = h;
                }
                else
                {
                    Height = Screen.FromPoint(Cursor.Position).WorkingArea.Height;
                }

                TsuView parentView;
                if (this is ModuleView || this is Common.Instruments.Device.View)
                {
                    // make the window centered and fitted on the main tsunami view
                    parentView = Tsunami2.View;
                }
                else
                {
                    // make the window centered and fitted on the parent module's view or the one given
                    parentView = _Module?.ParentModule?.View ?? ParentView;
                }
                List<string> buttons = new List<string> { leftButton, middleButton, rightButton };

                _SelectionView = new MessageModuleView(this, parentView, titleAndMessage, buttons, flagDont, background, foreground);
                _SelectionView.SetButtonClick(leftButton, leftAction);
                _SelectionView.SetButtonClick(middleButton, middleAction);
                _SelectionView.SetButtonClick(rightButton, rightAction);
                if (onShowingMessage != null) _SelectionView.Shown += onShowingMessage;

                _SelectionView.Width = Screen.FromPoint(Cursor.Position).WorkingArea.Width - SPACE;
                _SelectionView.Left = SPACE / 2;
                _SelectionView.AdjustToScreenSize(fullSize);

                ShowSelectionView(showdialog);
                _SelectionView.Refresh();
                s = _SelectionView.Respond;
            }
        }

        protected static TableLayoutPanel CreateLayoutForButtons(List<Control> existingButtons, DockStyle dockstyle, List<int> forcedSizesInPourcent)
        {
            int PADDING = 5;


            // Find Height
            int maxHeight = 0;
            foreach (var item in existingButtons)
            {
                if (item != null)
                {
                    if (item.Height > maxHeight)
                        maxHeight = item.Height;
                }
            }

            // Create Layout of one line 
            TableLayoutPanel TLP = new TableLayoutPanel
            {
                RowCount = 1,
                ColumnCount = existingButtons.Count,

                Dock = dockstyle,
                Height = maxHeight + 5 + 2 * PADDING,
                Padding = new Padding(PADDING)
            };

            float columnSizeInPercent = 100f / existingButtons.Count;
            int count = 0;
            foreach (Control item in existingButtons)
            {
                if (forcedSizesInPourcent != null)
                {
                    columnSizeInPercent = forcedSizesInPourcent[count];
                }
                TLP.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, columnSizeInPercent));
                //if (item is BigButton bb) 
                //bb.HasSubButtons = false;  // why? i comment it because then ust opening a menu trigger step cahnge and update in GM
                TLP.Controls.Add(item);
                count++;
            }
            TLP.BringToFront();
            return TLP;
        }

        internal Control controlToGiveFocusTo;


        private void ShowSelectionView(bool showdialog)
        {
            if (showdialog)
            {
                _SelectionView.Shown += OnSelectionViewShown;
                _SelectionView.Show2();
            }
            else
            {
                if (_Module?.ParentModule?.View != null)
                    _SelectionView.Show(_Module.ParentModule.View);
                else
                    _SelectionView.Show();
            }
            isShownInMessageTsu = false;
        }

        private void OnSelectionViewShown(object sender, EventArgs e)
        {
            if (controlToGiveFocusTo == null)
            {
                foreach (Control item in Controls)
                {
                    if (item.CanFocus)
                    {
                        item.Focus();
                        break;
                    }
                }
            }
            else
                controlToGiveFocusTo.Focus();
            ///To focus the correct element : example the selected object in blue in a list view
            FocusView();
        }
        public virtual bool ShowPopUpSubSubSubMenu(List<Control> controls, string name = "unknown sub-sub-sub ")
        {
            var pop = PopUpMenu.subMenu.subMenu;
            pop.ShowSubMenu(controls, name);
            return true;
        }
        public virtual bool ShowPopUpSubSubMenu(List<Control> controls, string name = "unknown sub-sub ")
        {
            var pop = PopUpMenu.subMenu;
            pop.ShowSubMenu(controls, name);
            return true;
        }

        public virtual bool ShowPopUpSubMenu(List<Control> controls, string name = "unknown sub ")
        {
            var pop = PopUpMenu;
            pop.ShowSubMenu(controls, name);
            return true;
        }

        public virtual bool ShowPopUpMenu(List<Control> controls, string name = "unknown main")
        {
            var pop = PopUpMenu;
            if (pop != null)
            {
                pop.Hide();
                pop._Name = name;
            }
            else
            {
                PopUpMenu = new TsuPopUpMenu(this, name);
                pop = PopUpMenu;
            }

            pop.Clear();
            pop.Add(controls);

            pop.ShowInPlace();
            return true;
        }

        #endregion

        #region  Messages
        public bool GeNumericaltValueFromUser(string message, out double value, double defaultValue = 0, double minValue = -999.9, double maxValue = -999.9)
        {
            value = -999.9;

            // Ask
            string cancel = R.T_CANCEL;
            List<string> buttonTexts = new List<string> { R.T_OK, cancel };

            ShowMessageWithTextBox(message, buttonTexts, out string buttonClicked, out string textInput, defaultValue.ToString());

            // if Cancel
            if (buttonClicked == cancel) return false;

            string problemMessage = "";
            if (double.TryParse(textInput, out value))
            {
                // check allowed interval
                bool problem = false;
                if (minValue != -999.9)
                {
                    problem = value < minValue;
                    problemMessage += $"{R.T_MUST_BE_LARGER_THAN} {minValue}";
                }
                if (maxValue != -999.9)
                {
                    problem = value > maxValue;
                    if (problemMessage != "") problemMessage += " and ";
                    problemMessage += $"{R.T_MUST_BE_SMALLER_THAN} {maxValue}";
                }

                if (!problem)
                    return true;
                problemMessage = $"{R.T_VALUE_IS_OUT_OF_TOLERANCE};{problemMessage}.)";
            }
            else
                problemMessage = $"{R.T_VALUE_MUST_BE_NUMERIC};{R.T_IT_IS_NOT_RECOGNIZED_AS_A_NUMBER_CHECK_YOUR_SEPARATOR_AND_TRY_AGAIN}";

            // show problem
            new MessageInput(MessageType.Warning, problemMessage).Show();

            // recursively ask again
            return GeNumericaltValueFromUser(message, out value, defaultValue, minValue, maxValue);
        }

        [Obsolete("rather 'use public MessageResult ShowMessage()'")]
        public string ShowMessageOfBug(string titleAndMessage, Exception ex, string buttonMiddle = "OK", string buttonRight = "", string buttonLeft = "", string sender = "")
        {
            MessageTsu m = null;
            P.Values.InvokeOnApplicationDispatcher(Action);
            if (m == null)
                throw new Exception("Tsunami cannot create a message.");
            return m.Respond;

            void Action()
            {
                string version = Tsunami2.Properties == null ? "Version" : Tsunami2.Properties.Version;
                string message = $"{titleAndMessage}\r\n\r\n" +
                                 $"{R.T_TSUNAMI_BUGS_VOUS_FERIEZ_MIEUX_D�TRE_TR�S_PRUDENT}\r\n\r\n" +
                                 $"{R.T_PLEASE_REPORT_WHAT_YOU_HAVE_DONE_IN_TO_JIRA_AND_COPY_THE_FOLLOWING_SELECTABLE_LINES_TO_THE_ISSUE_DESCRIPTION}\r\n\r\n" +
                                 $"Tsunami version: {version}\r\n\r\n" + ex;
                SystemSounds.Exclamation.Play();
                m = new MessageTsu(this, P.Theme.Colors.DarkBackground, P.Theme.Colors.LightForeground, "A�e",
                    "Oulala...", message, buttonMiddle, buttonRight, buttonLeft, "", sender);
            }
        }

        public string ShowProgress(TsuView destinationView, Action action, int estimatedTimeInSeconds = 2,
            string titleAndMessage = null, string buttonMiddle = "", Action cancelAction = null)
        {
            P.Values.InvokeOnApplicationDispatcher(ShowProgressMessage);

            return WaitingForm.respond;

            void ShowProgressMessage()
            {
                if (string.IsNullOrEmpty(buttonMiddle)) buttonMiddle = R.T_CANCEL;
                if (string.IsNullOrEmpty(titleAndMessage)) titleAndMessage = R.T544;
                string title = StringManipulation.SeparateTitleFromMessage(titleAndMessage);
                string message = StringManipulation.SeparateMessageFromTitle(titleAndMessage);
                if (title == message) title = R.T544;
                WaitingForm = new ProgressMessage(destinationView, title, message, 1, estimatedTimeInSeconds * 1000,
                    true, cancelAction, buttonMiddle);
                action?.Invoke();
                WaitingForm.Stop();
            }
        }

        internal ToolTip ToolTip;

        public MessageResult ShowMessageWithFillControl(Control control, MessageType type = MessageType.FYI,
            string titleAndMessage = "No Title; No description", List<string> buttonTexts = null, DsaFlag dontShowAgain = null,
            bool coverParentView = false, string textOfButtonToBeLocked = "",
            string sender = "", bool waitForExit = true)
        {
            if (CancelBasedOnDsaFlag(dontShowAgain, buttonTexts, out string buttonTextToReturn))
                return new MessageResult { TextOfButtonClicked = buttonTextToReturn };

            MessageResult r = null;
            P.Values.InvokeOnApplicationDispatcher(Action);
            return r;

            void Action()
            {
                MesssageWithFillControl messageTsu = new MesssageWithFillControl(this, type, titleAndMessage, buttonTexts, control, dontShowAgain, coverParentView, textOfButtonToBeLocked, sender);
                if (waitForExit)
                    r = messageTsu.Show2();
                else
                    messageTsu.Show();
            }
        }


        [Obsolete("rather 'use public MessageResult ShowMessage()'")]
        /// <summary>
        /// title and message should be a single string and separated by ";"
        /// binding source shoudl be a list of object, display is a string containting the name of the property to show, valuemember is a string containtg thename of the property to return, if set to null, the object will be returned.
        /// </summary>
        /// <param name="titleAndMessage"></param>
        /// <param name="buttonMiddle"></param>
        /// <param name="cancelButton"></param>
        /// <param name="initialTextBoxText"></param>
        /// <returns></returns>
        public object ShowMessageOfPredefinedInput(
            string titleAndMessage,
            string buttonMiddle,
            string cancelButton,
            BindingSource bindingSource, string displayMember = "_Name", string valueMember = null, bool selectFirst = false, Image image = null, object preselectedObject = null, DsaFlag dontShowAgain = null, bool allowCustomEntry = true)
        {
            if (CancelBasedOnDsaFlag(dontShowAgain, buttonMiddle, cancelButton, out string buttonTextToReturn)) 
                return buttonTextToReturn;
            MessageTsu m = null;
            object o = null;
            P.Values.InvokeOnApplicationDispatcher(Action);
            return o;

            void Action()
            {
                m = new MessageTsu { ParentView = this };
                m.Colorize(P.Theme.Colors.Object, P.Theme.Colors.NormalFore);
                m.CancelString = cancelButton;
                if (dontShowAgain != null) m.AddCheckBoxDontShowAgain(dontShowAgain);
                m.FillTexts("?", titleAndMessage, buttonMiddle, m.CancelString);
                m._textBoxRespondNeeded = false;
                m.textBox.Visible = false;
                m.titleAndMessageRichBox.BackColor = Color.White;


                PictureBox pb = null;

                // Add dropbox
                ComboBox combo = new ComboBox
                {
                    Top = m.titleAndMessageRichBox.Top + m.titleAndMessageRichBox.Height,
                    Left = 10,
                    Width = m.textBox.Width - 10,
                    Font = P.Theme.Fonts.Large,
                    Visible = true,
                    Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top,
                    
                };
                if (!allowCustomEntry)
                    combo.DropDownStyle = ComboBoxStyle.DropDownList;
                m.Controls.Add(combo);
                combo.BringToFront();



                combo.DataSource = bindingSource.DataSource;
                combo.DisplayMember = displayMember;
                combo.ValueMember = valueMember;

                m.Load += OnMessageLoad;

                if (preselectedObject != null)
                {
                    if (combo.Items.Contains(preselectedObject))
                        combo.SelectedItem = preselectedObject;
                    else
                        combo.SelectedItem = preselectedObject.ToString();
                }

                m.button2.Click += OnButton2OnClick;
                combo.KeyPress += OnComboOnKeyPress;

                //dropping when click
                //combo.GotFocus += delegate { combo.DroppedDown = true; };

                // select  and focus combotext
                combo.SelectAll();

                m.Shown += OnMOnShown;


                // add image 
                if (image != null)
                {
                    pb = new PictureBox
                    {
                        BackColor = P.Theme.Colors.Transparent,
                        Image = image,
                        SizeMode = PictureBoxSizeMode.AutoSize,
                        Visible = true,
                        Top = combo.Top + 2 * combo.Height,
                        Left = 10,
                    };
                    m.Controls.Add(pb);
                    pb.BringToFront();
                    m.Height += pb.Height + 50;
                    m.Top = (Screen.FromRectangle(m.Bounds).WorkingArea.Height - m.Height) / 2;
                }

                // show
                m.ShowDialog(this);
                if (combo != null)
                {
                    if (combo.SelectedValue != null)
                        o = combo.SelectedValue;
                    else
                        o = combo.Text;
                }
                else
                    o = R.T_CANCEL;

                return;

                // setup enter button
                void OnComboOnKeyPress(object sender, KeyPressEventArgs e)
                {
                    combo.DroppedDown = false;
                    if (e.KeyChar == (char)Keys.Return || e.KeyChar == (char)Keys.Enter)
                    {
                        m.button1.PerformClick();
                    }
                }

                //combo.Focus();
                void OnMOnShown(object sender, EventArgs e)
                {
                    combo.Focus();
                }

                //setup cancel button
                void OnButton2OnClick(object sender, EventArgs e)
                {
                    combo = null;
                }

                // spread to combo size
                void OnMessageLoad(object sender, EventArgs e)
                {
                    int maxWidth = 0;
                    int temp = 0;
                    Label label1 = new Label();

                    foreach (var obj in combo.Items)
                    {
                        label1.Text = obj.ToString();
                        temp = label1.PreferredWidth;
                        if (temp > maxWidth)
                        {
                            maxWidth = temp;
                        }
                    }

                    label1.Dispose();
                    m.Width = maxWidth + 400;

                    int titleH = m.TitlePreferedSize.Height * ((m.TitlePreferedSize.Width / m.Width)+1);
                    int descriptionH = m.DescriptionPreferedSize.Height * ((m.DescriptionPreferedSize.Width / m.Width)+1);
                    int tdH =  titleH + descriptionH;
                    int oldH = m.titleAndMessageRichBox.Height;

                    if (combo != null)
                    {
                        combo.Top = m.titleAndMessageRichBox.Top + tdH + 2;
                        m.Height = combo.Top + 2 * combo.Height;
                    }
                    if (pb != null)
                    {
                        pb.Top = combo.Top + 2 * combo.Height;
                        m.Height = pb.Top + pb.Height + 2;
                    }
                    m.Height += m.button1.Height + 20;
                    m.titleAndMessageRichBox.Height = tdH;
                }
            }
        }

        [Obsolete]
        /// <summary>
        /// title and message should be a single string and separated by ";"
        /// binding source shoudl be a list of object, display is a string containting the name of the property to show, valuemember is a string containtg thename of the property to return, if set to null, the object will be returned.
        /// </summary>
        /// <param name="titleAndMessage"></param>
        /// <param name="buttonMiddle"></param>
        /// <param name="cancelButton"></param>
        /// <param name="initialTextBoxText"></param>
        /// <returns></returns>
        public void ShowMessageOfImage(string titleAndMessage, string buttonMiddle, Image image, string toolTipTxt = "")
        {
            P.Values.InvokeOnApplicationDispatcher(Action);
            return;

            void Action()
            {
                MessageTsu m = new MessageTsu();
                m.button1.Text = buttonMiddle;
                m.ParentView = this;
                m.Colorize(P.Theme.Colors.Object, P.Theme.Colors.NormalFore);
                m.SetTitleAndMessage(titleAndMessage, "?", out Size preferedSize);
                m.titleAndMessageRichBox.Size = preferedSize;
                m.Controls.Clear();
                m.Height = Screen.FromRectangle(m.Bounds).WorkingArea.Height - 60;
                m.Width = Screen.FromRectangle(m.Bounds).WorkingArea.Width - 60;

                TableLayoutPanel tableLayoutPanel = new TableLayoutPanel();
                tableLayoutPanel.RowCount = 3;
                tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, m.titleAndMessageRichBox.Height + 20));
                tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, m.Height - m.titleAndMessageRichBox.Height - m.button1.Height - 40));
                tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, m.button1.Height + 20));
                tableLayoutPanel.Dock = DockStyle.Fill;
                tableLayoutPanel.GrowStyle = TableLayoutPanelGrowStyle.FixedSize;
                tableLayoutPanel.Location = new Point(0, 0);
                tableLayoutPanel.Name = "tableLayoutPanel";
                tableLayoutPanel.ColumnCount = 1;
                tableLayoutPanel.TabIndex = 2;
                m.Left = 30;
                m.Top = 30;
                m.Controls.Add(tableLayoutPanel);
                TableLayoutPanel tableLayoutPanelButton = new TableLayoutPanel();
                tableLayoutPanelButton.ColumnCount = 3;
                tableLayoutPanelButton.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, (tableLayoutPanel.Width - m.button1.Width) / 2 - 10));
                tableLayoutPanelButton.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, m.button1.Width + 20));
                tableLayoutPanelButton.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, (tableLayoutPanel.Width - m.button1.Width) / 2 - 10));
                tableLayoutPanelButton.Dock = DockStyle.Fill;
                tableLayoutPanelButton.GrowStyle = TableLayoutPanelGrowStyle.FixedSize;
                tableLayoutPanelButton.Location = new Point(0, 0);
                tableLayoutPanelButton.Name = "tableLayoutPanelButton";
                tableLayoutPanelButton.RowCount = 1;
                tableLayoutPanelButton.TabIndex = 1;
                tableLayoutPanel.Controls.Add(m.titleAndMessageRichBox, 0, 0);
                tableLayoutPanel.Controls.Add(tableLayoutPanelButton, 0, 1);
                tableLayoutPanelButton.Controls.Add(m.button1, 1, 0);
                // add image 
                if (image != null)
                {
                    PictureBox pb = new PictureBox
                    {
                        BackColor = P.Theme.Colors.Transparent,
                        SizeMode = PictureBoxSizeMode.Zoom,
                        Image = image,
                        Visible = true,
                    };
                    ///Met un tool tip pour aide
                    if (toolTipTxt != "")
                    {
                        ToolTip tooltip = new ToolTip { InitialDelay = 50, AutoPopDelay = 20000, ReshowDelay = 50 };
                        tooltip.SetToolTip(pb, toolTipTxt);
                    }

                    pb.Cursor = Cursors.Arrow;
                    pb.Dock = DockStyle.Fill;
                    pb.BringToFront();
                    tableLayoutPanel.Controls.Add(pb, 0, 1);
                }

                // show
                m.ShowDialog(this);
            }
        }

        /// <summary>
        /// title and message should be a single string and separated by ";"
        /// </summary>
        /// <param name="titleAndMessage"></param>
        /// <param name="buttonMiddle"></param>
        /// <param name="cancelButton"></param>
        /// <param name="initialTextBoxText"></param>
        /// <returns></returns>
        public string ShowMessageOfEnterTiltInDatagrid(string title, string buttonLeft, string cancelButton, Station stationTilt, bool showToleranceColor, Instrument instrument = null, I.Module instrumentModule = null)
        {
            MessageRollDataGridTSU m = null;

            P.Values.InvokeOnApplicationDispatcher(Action);
            return m.Respond;

            void Action()
            {
                m = new MessageRollDataGridTSU();
                m.Colorize(P.Theme.Colors.Object, P.Theme.Colors.NormalFore);
                m.CancelString = cancelButton;
                m.showToleranceColors = showToleranceColor;
                m.oldStationTilt = stationTilt;
                m.newStationTilt = (Station)stationTilt.Clone();
                if (instrument != null) m._usedInstrument = instrument;
                if (instrumentModule != null) m._instrumentModule = instrumentModule;
                m.FillTitleAndAdjustSize(title, buttonLeft, m.CancelString);
                TryAction(m.RedrawDataGridViewMesTilt);
                m.ShowDialog(this);
            }
        }
        /// <summary>
        /// Affiche le tableau qui permet de faire un contr�le de z�ro mire
        /// </summary>
        /// <param name="title"></param>
        /// <param name="buttonLeft"></param>
        /// <param name="cancelButton"></param>
        /// <param name="listLevellingStaff"></param>
        /// <returns></returns>
        public string ShowMessageOfZeroStaffCheck(string title, string buttonLeft, string cancelButton, List<LevelingStaff> listLevellingStaff, Common.Instruments.Manager.Module staffModule)
        {
            MessageZeroStaffCheck m = null;

            P.Values.InvokeOnApplicationDispatcher(Action);
            return m.Respond;

            void Action()
            {
                m = new MessageZeroStaffCheck();
                m.Colorize(P.Theme.Colors.Object, P.Theme.Colors.NormalFore);
                m.staffModule = staffModule;
                m.CancelString = cancelButton;
                m.oldListStaff = listLevellingStaff;
                m.DeepCopyOldToNewListStaff();
                m.FillTitleAndAdjustSize(title, buttonLeft, m.CancelString);
                TryAction(m.RedrawDataGridViewZeroStaff);
                m.ShowDialog(this);
            }
        }
        public string ShowMessageCollimLevelCheck(string title, string buttonLeft, string cancelButton, Common.Instruments.Level level)
        {
            MessageLevelColimCheck m = null;

            P.Values.InvokeOnApplicationDispatcher(Action);
            return m.Respond;

            void Action()
            {
                m = new MessageLevelColimCheck();
                m.Colorize(P.Theme.Colors.Object, P.Theme.Colors.NormalFore);
                m.level = level;
                m.LA1 = level._LA1;
                m.LA2 = level._LA2;
                m.LB1 = level._LB1;
                m.LB2 = level._LB2;
                m.errorColim = level._ErrorCollim;
                m.distColim = level._DistCollim;
                m.CancelString = cancelButton;
                m.FillTitleAndAdjustSize(title, buttonLeft, m.CancelString);
                TryAction(m.RedrawDataGridViewColim);
                m.ShowDialog(this);
            }
        }
        /// <summary>
        /// title and message should be a single string and separated by ";"
        /// </summary>
        /// <param name="titleAndMessage"></param>
        /// <param name="buttonMiddle"></param>
        /// <param name="cancelButton"></param>
        /// <param name="initialTextBoxText"></param>
        /// <returns></returns>
        public string ShowMessageOfMoveLevelInDatagrid(string title, string buttonLeft, List<string> listPointNames, List<string> listDcum, List<string> listMoveToDo, List<string> listTheoReading)
        {
            MesssageMoveLevelDatagrid m = null;

            P.Values.InvokeOnApplicationDispatcher(Action);
            return m.Respond;

            void Action()
            {
                m = new MesssageMoveLevelDatagrid();
                m.Colorize(P.Theme.Colors.Object, P.Theme.Colors.NormalFore);
                m.listPointNames = listPointNames;
                m.listDcum = listDcum;
                m.listMoveToDo = listMoveToDo;
                m.listTheoReading = listTheoReading;
                m.FillTitleAndAdjustSize(title, buttonLeft);
                TryAction(m.RedrawDataGridViewMoveLevel);
                m.ShowDialog(this);
            }
        }
        /// <summary>
        /// Affiche une fen�tre avec les r�sultats de v�rification de la cote bleue entre station de cheminement( contr�le de marche)
        /// </summary>
        /// <param name="title"></param>
        /// <param name="buttonLeft"></param>
        /// <param name="ptNameList"></param>
        /// <param name="diffStprevA"></param>
        /// <param name="diffStprevR"></param>
        /// <param name="diffStprevP"></param>
        /// <param name="diffStnextA"></param>
        /// <param name="diffStnextR"></param>
        /// <param name="diffStnextP"></param>
        /// <returns></returns>
        public string ShowMessageOfBlueElevationLevelInDatagrid(string title, string buttonLeft, List<string> ptNameList,
            List<string> diffStprevA, List<string> diffStprevR, List<string> diffStprevP, List<string> diffStnextA, List<string> diffStnextR, List<string> diffStnextP, int actualStationNumber, bool showCancelButton = false)
        {
            MesssageBlueElevationDataGrid m = null;

            P.Values.InvokeOnApplicationDispatcher(Action);
            return m.Respond;

            void Action()
            {
                m = new MesssageBlueElevationDataGrid();
                m.Colorize(P.Theme.Colors.Object, P.Theme.Colors.NormalFore);
                m.ptNameList = ptNameList;
                m.diffStprevA = diffStprevA;
                m.diffStprevR = diffStprevR;
                m.diffStprevP = diffStprevP;
                m.diffStnextA = diffStnextA;
                m.diffStnextR = diffStnextR;
                m.diffStnextP = diffStnextP;
                m.Outward1.HeaderText = string.Format(R.StringDataGridBlueElevation_Outward, actualStationNumber - 1);
                m.Return1.HeaderText = string.Format(R.StringDataGridBlueElevation_Return, actualStationNumber - 1);
                m.Repeat1.HeaderText = string.Format(R.StringDataGridBlueElevation_Repeat, actualStationNumber - 1);
                m.Outward3.HeaderText = string.Format(R.StringDataGridBlueElevation_Outward, actualStationNumber + 1);
                m.Return3.HeaderText = string.Format(R.StringDataGridBlueElevation_Return, actualStationNumber + 1);
                m.Repeat3.HeaderText = string.Format(R.StringDataGridBlueElevation_Repeat, actualStationNumber + 1);
                m.FillTitleAndAdjustSize(title, buttonLeft);
                if (!showCancelButton) m.RemoveButtonRight();
                TryAction(m.RedrawDataGridViewMoveLevel);
                m.ShowDialog(this);
            }
        }
        #endregion

        #region Moving and Resizing
        internal bool resizingInProgress;
        protected bool movingInProgress;
        private int xDiff;
        private int yDiff;
        private Point startingLocationOfMouse;
        private Rectangle startingWindow;
        private Rectangle endingWindow;
        private ResizingDirections direction;
        private void TsuView_MouseDown(object sender, MouseEventArgs e)
        {
            RecordInitialPosition();
            CheckResizeDirection(PointToClient(MousePosition));

            // if filled then we dont move or resize
            if (Dock == DockStyle.Fill) return;

            if (direction == ResizingDirections.Move)
            {
                movingInProgress = true;
                Debug.WriteInConsole(R.T546);
            }
            else
            {
                resizingInProgress = true;
                Debug.WriteInConsole(R.T547);
            }
        }
        private void TsuView_MouseUp(object sender, MouseEventArgs e)
        {
            resizingInProgress = false;
            movingInProgress = false;
        }

        internal virtual void On_TsuView_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void RecordInitialPosition()
        {
            startingWindow = ClientRectangle;
            startingWindow.Location = Location;
            endingWindow = ClientRectangle;
            endingWindow.Location = Location;
            startingLocationOfMouse = Cursor.Position;
        }
        private void TsuView_MouseLeave(object sender, EventArgs e)
        {
            // this.Cursor = Cursors.Arrow;
        }

        DateTime lastRunnedTime;
        internal void TsuView_MouseMove(object sender, MouseEventArgs e)
        {

            TimeSpan span = (DateTime.Now - lastRunnedTime);
            if (span.TotalMilliseconds > 5)
            {
                // if filled then we dont move or resize
                if (Dock == DockStyle.Fill) return;

                if (!movingInProgress && !resizingInProgress)
                {
                    CheckResizeDirection(PointToClient(MousePosition));
                    ShowGoodCursor(sender);
                }
                else
                {
                    if (movingInProgress)
                        MoveWindow();
                    if (resizable && resizingInProgress)
                        ResizeWindow();
                }
            }

            lastRunnedTime = DateTime.Now;

        }
        private void ResizeWindow()
        {
            if (resizingInProgress)
            {
                ComputeNewWindowRectangle();
                ApplyMovement();
            }
        }
        private void MoveWindow()
        {
            if (movingInProgress)
            {
                // Get the difference between the two points
                xDiff = startingLocationOfMouse.X - Cursor.Position.X;
                yDiff = startingLocationOfMouse.Y - Cursor.Position.Y;

                endingWindow.Location = new Point(
                    startingWindow.X - xDiff,
                    startingWindow.Y - yDiff);
                ApplyMovement();
            }
        }
        private void ComputeNewWindowRectangle()
        {
            // Get the difference between the two points
            xDiff = startingLocationOfMouse.X - Cursor.Position.X;
            yDiff = startingLocationOfMouse.Y - Cursor.Position.Y;

            if (Dock == DockStyle.None)
            {
                if (P.Values.GuiPrefs.MoveOneEdgeOnlyWhenResizingWindows.IsTrue)
                    MoveOneEdgeOrAngle();
                else
                    MoveBothOppositeEdgesOrAngles();
            }
            else
            {
                switch (direction)
                {
                    case ResizingDirections.N:
                        endingWindow.Y = startingWindow.Y - yDiff;
                        endingWindow.Height = startingWindow.Height + yDiff;
                        break;
                    case ResizingDirections.NE:
                        endingWindow.Height = startingWindow.Height + yDiff;
                        endingWindow.Width = startingWindow.Width - xDiff;
                        break;
                    case ResizingDirections.E:
                        endingWindow.Width = startingWindow.Width - xDiff;
                        break;
                    case ResizingDirections.SE:
                        endingWindow.Height = startingWindow.Height - yDiff;
                        endingWindow.Width = startingWindow.Width - xDiff;
                        break;
                    case ResizingDirections.S:
                        endingWindow.Height = startingWindow.Height - yDiff;
                        break;
                    case ResizingDirections.SW:
                        endingWindow.Height = startingWindow.Height - yDiff;
                        endingWindow.X = startingWindow.X - xDiff;
                        endingWindow.Width = startingWindow.Width + xDiff;
                        break;
                    case ResizingDirections.W:
                        endingWindow.X = startingWindow.X - xDiff;
                        endingWindow.Width = startingWindow.Width + xDiff;
                        break;
                    case ResizingDirections.NW:
                        endingWindow.Y = startingWindow.Y - yDiff;
                        endingWindow.Height = startingWindow.Height + yDiff;
                        endingWindow.X = startingWindow.X - xDiff;
                        endingWindow.Width = startingWindow.Width + xDiff;
                        break;
                }
            }
        }

        private void MoveBothOppositeEdgesOrAngles()
        {
            switch (direction)
            {
                case ResizingDirections.N:
                    endingWindow.Height = startingWindow.Height + 2 * yDiff;
                    if (endingWindow.Height < MinimumSize.Height) return;
                    if (endingWindow.Width < MinimumSize.Width) return;
                    endingWindow.Y = startingWindow.Y - yDiff;
                    break;
                case ResizingDirections.NE:
                    endingWindow.Height = startingWindow.Height + 2 * yDiff;
                    endingWindow.Width = startingWindow.Width - 2 * xDiff;
                    if (endingWindow.Height < MinimumSize.Height) return;
                    if (endingWindow.Width < MinimumSize.Width) return;
                    endingWindow.X = startingWindow.X + xDiff;
                    endingWindow.Y = startingWindow.Y - yDiff;
                    break;
                case ResizingDirections.E:
                    endingWindow.Width = startingWindow.Width - 2 * xDiff;
                    if (endingWindow.Height < MinimumSize.Height) return;
                    if (endingWindow.Width < MinimumSize.Width) return;
                    endingWindow.X = startingWindow.X + xDiff;
                    break;
                case ResizingDirections.SE:
                    endingWindow.Height = startingWindow.Height - 2 * yDiff;
                    endingWindow.Width = startingWindow.Width - 2 * xDiff;
                    if (endingWindow.Height < MinimumSize.Height) return;
                    if (endingWindow.Width < MinimumSize.Width) return;
                    endingWindow.X = startingWindow.X + xDiff;
                    endingWindow.Y = startingWindow.Y + yDiff;
                    break;
                case ResizingDirections.S:
                    endingWindow.Height = startingWindow.Height - 2 * yDiff;
                    if (endingWindow.Height < MinimumSize.Height) return;
                    if (endingWindow.Width < MinimumSize.Width) return;
                    endingWindow.Y = startingWindow.Y + yDiff;
                    break;
                case ResizingDirections.SW:
                    endingWindow.Height = startingWindow.Height - 2 * yDiff;
                    endingWindow.Width = startingWindow.Width + 2 * xDiff;
                    if (endingWindow.Height < MinimumSize.Height) return;
                    if (endingWindow.Width < MinimumSize.Width) return;
                    endingWindow.X = startingWindow.X - xDiff;
                    endingWindow.Y = startingWindow.Y + yDiff;
                    break;
                case ResizingDirections.W:
                    endingWindow.Width = startingWindow.Width + 2 * xDiff;
                    if (endingWindow.Height < MinimumSize.Height) return;
                    if (endingWindow.Width < MinimumSize.Width) return;
                    endingWindow.X = startingWindow.X - xDiff;
                    break;
                case ResizingDirections.NW:
                    endingWindow.Height = startingWindow.Height + 2 * yDiff;
                    endingWindow.Width = startingWindow.Width + 2 * xDiff;
                    if (endingWindow.Height < MinimumSize.Height) return;
                    if (endingWindow.Width < MinimumSize.Width) return;
                    endingWindow.X = startingWindow.X - xDiff;
                    endingWindow.Y = startingWindow.Y - yDiff;
                    break;
            }
        }

        private void MoveOneEdgeOrAngle()
        {
            // Vertical resize
            if (direction == ResizingDirections.N || direction == ResizingDirections.NE || direction == ResizingDirections.NW)
            {
                int newHeight = startingWindow.Height + yDiff;
                if (newHeight >= MinimumSize.Height)
                {
                    endingWindow.Height = newHeight;
                    endingWindow.Y = startingWindow.Y - yDiff;
                }
            }
            else if (direction == ResizingDirections.S || direction == ResizingDirections.SE || direction == ResizingDirections.SW)
            {
                int newHeight = startingWindow.Height - yDiff;
                if (newHeight >= MinimumSize.Height)
                    endingWindow.Height = newHeight;
            }

            // Horizontal resize
            if (direction == ResizingDirections.W || direction == ResizingDirections.NW || direction == ResizingDirections.SW)
            {
                int newWidth = startingWindow.Width + xDiff;
                if (newWidth >= MinimumSize.Width)
                {
                    endingWindow.Width = newWidth;
                    endingWindow.X = startingWindow.X - xDiff;
                }
            }
            else if (direction == ResizingDirections.E || direction == ResizingDirections.NE || direction == ResizingDirections.SE)
            {
                int newWidth = startingWindow.Width - xDiff;
                if (newWidth >= MinimumSize.Width)
                    endingWindow.Width = newWidth;
            }
        }

        private void ShowGoodCursor(object sender)
        {
            switch (direction)
            {
                case ResizingDirections.NW:
                case ResizingDirections.SE:
                    Cursor = Cursors.SizeNWSE;
                    //this.toolTipTsuView.SetToolTip(sender as Control, "Drag to change the size of the window");
                    break;
                case ResizingDirections.N:
                case ResizingDirections.S:
                    Cursor = Cursors.SizeNS;
                    //this.toolTipTsuView.SetToolTip(sender as Control, "Drag to change the size of the window");
                    break;
                case ResizingDirections.NE:
                case ResizingDirections.SW:
                    Cursor = Cursors.SizeNESW;
                    // this.toolTipTsuView.SetToolTip(sender as Control, "Drag to change the size of the window");
                    break;
                case ResizingDirections.W:
                case ResizingDirections.E:
                    Cursor = Cursors.SizeWE;
                    //this.toolTipTsuView.SetToolTip(sender as Control, "Drag to change the size of the window");
                    break;
                case ResizingDirections.Move:
                    Cursor = Cursors.SizeAll;
                    //this.toolTipTsuView.SetToolTip(sender as Control, "Drag to change the location of the window");
                    break;
            }
        }
        private enum ResizingDirections
        {
            N, NE, E, SE, S, SW, W, NW, Move
        }
        private void CheckResizeDirection(Point mousePositionInView)
        {
            if (resizable)
            {
                //bool N = (e.Y < this.Height / 10);
                //bool W = (e.X < this.Width / 10);
                //bool E = (e.X > this.Width * 9 / 10);
                //bool S = (e.Y > this.Height * 9 / 10);

                const int RESIZING_BORDER_SIZE = 30;
                bool N = (mousePositionInView.Y < RESIZING_BORDER_SIZE);
                bool W = (mousePositionInView.X < RESIZING_BORDER_SIZE);
                bool E = (mousePositionInView.X > Width - RESIZING_BORDER_SIZE);
                bool S = (mousePositionInView.Y > Height - RESIZING_BORDER_SIZE);

                string s = "";
                if (N)
                    s += 'N';
                else
                    if (S)
                    s += 'S';

                if (E)
                    s += 'E';
                else
                    if (W)
                    s += 'W';
                switch (s)
                {
                    case "SE": direction = ResizingDirections.SE; break;
                    case "E": direction = ResizingDirections.E; break;
                    case "S": direction = ResizingDirections.S; break;
                    case "SW": direction = ResizingDirections.SW; break;
                    case "W": direction = ResizingDirections.W; break;
                    case "NW": direction = ResizingDirections.NW; break;
                    case "N": direction = ResizingDirections.N; break;
                    case "NE": direction = ResizingDirections.NE; break;
                    default: direction = ResizingDirections.Move; break;
                }
            }
            else
                direction = ResizingDirections.Move;
        }
        private void ApplyMovement()
        {
            Location = endingWindow.Location;
            Size = endingWindow.Size;
        }
        protected void AllowMovement(Control c)
        {
            c.MouseMove += TsuView_MouseMove;
            c.MouseLeave += TsuView_MouseLeave;
            c.MouseDown += TsuView_MouseDown;
            c.MouseUp += TsuView_MouseUp;
        }


        public void HideMessage()
        {
            WaitingForm.EndAll();
        }
        private void Context_OnClick(object sender, EventArgs e)
        {
            contextMenuStrip.Hide(); //cache le menu context si on clique dessus
        }

        internal void ShowContextMenuAboveControlClicked(Control controlClicked)
        {
            contextMenuStrip.Location = new Point(-1000,-1000);
            contextMenuStrip.Show(); // neeed to show up invisible (see above) to compute the real height used below
            Point ptLowerLeft = new Point(0, -contextMenuStrip.Height);
            ptLowerLeft = controlClicked.PointToScreen(ptLowerLeft);
            contextMenuStrip.Location = ptLowerLeft;
        }
        #endregion

        #region  Exception

        public static DataGridView FindVisibleDataGridView(Control parentControl)
        {
            // Traverse all child controls of the parent control
            foreach (Control control in parentControl.Controls)
            {
                // If the control is a DataGridView and it is visible
                if (control is DataGridView gridView && gridView.Visible)
                {
                    return gridView;  // Return the first visible DataGridView found
                }

                // Recursively check for nested controls, such as panels, group boxes, or even forms within forms
                if (control.HasChildren)
                {
                    var foundGridView = FindVisibleDataGridView(control);
                    if (foundGridView != null)
                    {
                        return foundGridView;  // Return if a visible DataGridView is found in a child
                    }
                }
            }
            return null;  // No visible DataGridView found in this container
        }

        public bool TryActionWithMessage(Action action, string actionName = null, int totalEstimatedTimeMs = 5000)
        {
            try
            {
                string name = actionName ?? action.ToString();
                WaitingForm = new ProgressMessage(this, name, "In progress", 1, totalEstimatedTimeMs);
                bool result = TryAction(action, actionName);
                WaitingForm.EndAll();
                return result;
            }
            catch (Exception ex)
            {
                new MessageInput(MessageType.Critical, ex.Message).Show();
                WaitingForm.EndAll();
                return false;
            }
        }

        public override string ToString()
        {
            return _Module != null ? _Module._Name : _Name;
        }

        public bool TryAction(Action action, string actionName = null)
        {
            if (InvokeRequired)
            {
                return (bool)Invoke(new Func<bool>(Func));

                bool Func()
                {
                    return TryAction(action, actionName);
                }
            }

            string message;

            if (actionName != null)
                message = actionName;
            else
            {
                message = action.Method.ToString();
                var type = action.Method.DeclaringType;
                while (type != null)
                {
                    message += " in " + type.ToString();
                    type = type.DeclaringType;
                }
            }

            try
            {
                action();
                Log.AddEntryAsFYI(_Module, $"Action ({message}) requested");
                return true;
            }
            catch (NotImplementedException e)
            {
                Log.AddEntryAsError(_Module, $"{message} {R.T_FAILED}, {e.Message}");
                string titleAndMessage = string.Format(R.T_REQUEST_NOT_IMLEMENTED, e.StackTrace);
                new MessageInput(MessageType.Warning, titleAndMessage)
                {
                    Sender = action.Method + " in '" + _Module._Name + "'"
                }.Show();

                return false;
            }

            catch (OutOfMemoryException e)
            {
                return WarnForOutOfMemory(_Module, message, e);
            }

            catch (OperationCanceledException e)
            {
                Log.AddEntryAsError(_Module, message + " " + e.Message);

                return false;
            }

            catch (CancelException)
            {
                return false;
            }

            catch (T.UserException e)
            {
                new MessageInput(MessageType.Warning, e.Message).Show();

                return false;
            }

            catch (TsuException e)
            {
                new MessageInput(MessageType.Warning, e.Message).Show();

                return false;
            }

            catch (Exception e)
            {
                string exceptionDetails = e.Message;
                Exception temp = e;
                while (temp.InnerException != null)
                {
                    exceptionDetails += ", " + R.T_BECAUSE + ":\r\n" + temp.InnerException.Message;
                    temp = temp.InnerException;
                }
                string newMessage = "'" + message + "'" + " failed;" + exceptionDetails + "\r\n\r\nThis Stack Trace may help your developper:\r\n" + e.StackTrace;
                Log.AddEntryAsError(_Module, newMessage);

                if (!newMessage.Contains(';'))
                {
                    //Cut the message into title + message
                    int i = newMessage.IndexOf(':');
                    if (i < 30)
                        newMessage = newMessage.Replace(':', ';');
                    else
                    {
                        newMessage = newMessage.Substring(0, 30) + ';' + newMessage.Substring(30);
                    }
                }

                new MessageInput(MessageType.Warning, newMessage)
                {
                    Sender = action.Method + " in '" + _Module._Name + "'"
                }.Show();

                return false;
            }
        }

        public static bool WarnForOutOfMemory(Module module, string message, OutOfMemoryException e)
        {
            string customMessage = $"{R.T_OUT_OF_MEMORY};{R.T_OUT_OF_MEMORY_DETAILS}\r\n\r\n{message}\r\n$red&{e.Message}";
            Log.AddEntryAsError(module, customMessage);
            new MessageInput(MessageType.Critical, customMessage).Show();

            return false;
        }
        #endregion

        #region  Observer-Observable

        public virtual void OnNext(TsuObject TsuObject)
        {
            dynamic dynamic = TsuObject;
            OnNextBasedOn(dynamic);
        }

        internal void OnNextBasedOn(TsuObject TsuObject)
        {
        }

        internal virtual void SendMessage(TsuObject o)
        {
            if (observers != null)
                foreach (var observer in observers)
                {
                    if (o == null)
                        observer.OnError(new NotImplementedException());
                    else
                        observer.OnNext(o);
                }
        }

        internal List<IObserver<TsuObject>> observers;
        public IDisposable Subscribe(IObserver<TsuObject> observer)
        {
            if (!observers.Contains(observer)) observers.Add(observer);
            return new Unsubscriber(observers, observer);
        }
        public IDisposable SubscribeAsFirst(IObserver<TsuObject> observer)
        {
            if (!observers.Contains(observer)) observers.Insert(0, observer);
            return new Unsubscriber(observers, observer);
        }
        public virtual void OnCompleted()
        {
            throw new Exception(R.T552 + this);
        }
        public virtual void OnError(Exception error)
        {
            throw new Exception(R.T552 + this);
        }

        internal virtual void CheckOrientation()
        {
        }


        /// <summary>
        /// will optional reverse the orientation or set to given one, and will set the distacen to half of the parent, will return the orientation choosed, and will invert tthe orientation of the composit view in the 2 panels
        /// <param name="splitContainer"></param>
        /// <param name="orientation"></param>
        /// <param name="useOpposite"></param>
        internal virtual Orientation SetSplitContainerOrientation(Orientation orientation, bool invert = false)
        {
            return orientation;
        }

        protected IDisposable unsubscriber;
        internal class Unsubscriber : IDisposable
        {
            private List<IObserver<TsuObject>> _observers;
            private IObserver<TsuObject> _observer;
            public Unsubscriber(List<IObserver<TsuObject>> observers, IObserver<TsuObject> observer)
            {
                _observers = observers;
                _observer = observer;
            }
            public void Dispose()
            {
                if (_observer != null && _observers.Contains(_observer))
                    _observers.Remove(_observer);
            }
        }

        #endregion

        #region BackGroungWorker

        internal BackgroundWorker _BackgroundWorker;
        internal bool BackgroundWorkerRunning;
        internal int estimatedTimeInMs;
        internal string actionNameToDisplay;
        public TsuPopUpMenu visiblePopUpMenu;

        /// <summary>
        /// Will create a worker that show a message of progress over the view, you have to override OnDoWork() and you can overide OnProgressChanged and OnFinishWork
        /// Ba carrefully, a backgroundworker is  a new thread, you will not be able to access view and everything to start asynchrounously could be problematic.
        /// So i use it for synchrone work only.
        /// </summary>
        /// <param name="actionNameToDisplay"></param>
        /// <param name="estimatedTimeInMs"></param>
        internal void SetupBackGroundWorker(
            string actionNameToDisplay,
            Action<object, DoWorkEventArgs> WorkToDo,
            Action<object, ProgressChangedEventArgs> ToDoOnProgress = null,
            Action<object, RunWorkerCompletedEventArgs> ToDoWhenFinished = null,
            int stepNumber = 1, int estimatedTimeInMs = 1000, bool Start = true)
        {
            this.estimatedTimeInMs = estimatedTimeInMs;
            this.actionNameToDisplay = actionNameToDisplay;
            _BackgroundWorker = new BackgroundWorker
            {
                WorkerReportsProgress = true
            };
            _BackgroundWorker.DoWork += OnDoWork;
            _BackgroundWorker.DoWork += new DoWorkEventHandler(WorkToDo);
            _BackgroundWorker.ProgressChanged += OnProgressChanged;
            if (ToDoOnProgress != null) _BackgroundWorker.ProgressChanged += new ProgressChangedEventHandler(ToDoOnProgress);
            _BackgroundWorker.RunWorkerCompleted += OnFinishWork;
            if (ToDoWhenFinished != null) _BackgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(ToDoWhenFinished);

            WaitingForm = new ProgressMessage(this, actionNameToDisplay, actionNameToDisplay, stepNumber, estimatedTimeInMs);
            if (Start) StartBackGroundWorker();
        }
        internal void StartBackGroundWorker()
        {
            WaitingForm.BeginAstep(actionNameToDisplay);
            _BackgroundWorker.RunWorkerAsync();

        }

        internal void OnDoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorkerRunning = true;
        }

        internal void OnProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ProgressMessageState state = (ProgressMessageState)e.UserState;
            string description = state.Status ?? "in progress...";

            switch (state.Type)
            {
                case ProgressMessage.StepTypes.Begin: WaitingForm.BeginAstep(description); break;
                case ProgressMessage.StepTypes.End: WaitingForm.EndCurrentStep(description); break;
                case ProgressMessage.StepTypes.EndAll: WaitingForm.EndAll(); break;
            }
        }

        internal void OnFinishWork(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                if (e.Error is CancelException || e.Error.InnerException is CancelException)
                {

                }
                else
                {
                    //this.ShowMessageOfCritical(e.Error.Message, R.T_OK + "!");
                    new MessageInput(MessageType.Critical, e.Error.Message)
                    {
                        ButtonTexts = new List<string> { R.T_OK + "!" }
                    }.Show();
                }
            }
            //waitingForm.Stop();
            BackgroundWorkerRunning = false;
        }


        public void PossibleActions(string description, ref DsaOptions choiceFlag, string doName, Action doAction,
            MessageType messageType = MessageType.Choice, bool neverIsAllowed = false, string dontName = null,
            Action dontAction = null)
        {
            switch (choiceFlag)
            {
                case DsaOptions.Always:
                    doAction?.Invoke();
                    break;
                case DsaOptions.Never:
                    dontAction?.Invoke();
                    break;
                case DsaOptions.Ask_to:
                default:
                    // by default
                    if (dontName == null) dontName = R.T_DONT;
                    if (dontName == "") dontName = R.T_DONT;
                    if (doName == "") doName = R.T_OK;

                    // dont show again option
                    DsaFlag dsa = new DsaFlag("dontShowAgain", DsaOptions.Ask_to);

                    // ask
                    MessageInput mi = new MessageInput(messageType, description)
                    {
                        ButtonTexts = new List<string> { doName, dontName },
                        DontShowAgain = dsa
                    };
                    string r = mi.Show().TextOfButtonClicked;

                    // treat answer
                    if (r == doName)
                    {
                        // Do
                        if (dsa.State == DsaOptions.Ask_to_with_pre_checked_dont_show_again)
                            choiceFlag = DsaOptions.Always;
                        doAction?.Invoke();
                    }
                    else
                    {
                        // don't Do
                        if (dsa.State == DsaOptions.Ask_to_with_pre_checked_dont_show_again && neverIsAllowed)
                            choiceFlag = DsaOptions.Never;
                    }
                    break;
            }
        }

        #endregion

        internal virtual void CancelAction()
        {
            //throw new NotImplementedException();
        }

        Color UnactivatedColor;


        public Keys KeyCatched = Keys.Cancel;

        internal virtual void TsuView_KeyUp(object sender, KeyEventArgs e)
        {
            if (CheckIfHandledAlready(control: this, keyEventArgs: e))
                return;

            // trying t use tab to move from tsuviews
            // if(this.ActiveControl != this.buttonForFocusWithTabIndex0)
            //    previousTabIndex = this.ActiveControl.TabIndex;

            // Navigation between tsuViews with keybard
            T.KeyboardNavigation.AnalyseAnAct(control: this, keyEventArgs: e);

            // HardCoded ShortCuts
            T.KeyboardShortCut.AnalyseAnAct(control: this, keyEventArgs: e);

            // check function keys for macro recording or macro playing
            Control c = ActiveControl ?? this;
            T.Macro.AnalyseAndAct(e, c);
        }

        internal static bool CheckIfHandledAlready(TsuView control, KeyEventArgs keyEventArgs)
        {
            if (T.KeyboardNavigation.IsAlreadyCatchbyChildren(control: control))
            {
                // already catch so I should do nothing except if im the main form I shoudl reset the KeyCatched
                bool ImTheMainTsunamiForm = control.Parent == null;
                if (ImTheMainTsunamiForm)
                    T.KeyboardNavigation.ResetKeyCatchedInChildForms(control);
                return true;
            }

            control.KeyCatched = keyEventArgs.KeyCode; /// here every tsuview will declare that he receive this stroke so that parent receiving also the key will not treat it.

            return false;
        }


        private void buttonForFocusWithTabIndex0_Enter(object sender, EventArgs e)
        {
            Debug.WriteInConsole($"ENTER in button with tab index 0: TSUVIEW.ACTIVATED {_Name}");
            UnactivatedColor = BackColor;
            BackColor = P.Theme.Colors.Highlight;
        }

        private void buttonForFocusWithTabIndex0_Leave(object sender, EventArgs e)
        {
            Debug.WriteInConsole($"LEAVE in button with tab index 0: TSUVIEW.DE-ACTIVATED {_Name}");
            BackColor = UnactivatedColor;
        }

        int previousTabIndex = -1;
        private void buttonForFocusWithTabIndex0_KeyUp(object sender, KeyEventArgs e)
        {
            Debug.WriteInConsole($"KEY_UP 0  {buttonForFocusWithTabIndex0.TabIndex} of {_Name}");
            if (e.KeyCode == Keys.Tab)
            {
                bool weDidAfullControlCycleWithTab = previousTabIndex > 1;
                if (weDidAfullControlCycleWithTab)
                {
                    e.Handled = true; // does it do th same as my tsu.view.KeyCatched variable?
                    if (!T.KeyboardNavigation.MoveToSisterVisibleView(this))
                        T.KeyboardNavigation.MoveToParentView(this);
                    KeyCatched = e.KeyCode;
                    previousTabIndex = buttonForFocusWithTabIndex0.TabIndex;
                }
            }
        }

        //private void buttonForFocusWithTabIndex100_Enter(object sender, EventArgs e)
        //{
        //    Debug.WriteInConsole($"ENTER 100 with {buttonForFocusWithTabIndex100.TabIndex} of {this._Name}");
        //    UnactivatedColor = this.BackColor;
        //    this.BackColor = Tsunami2.Preferences.Theme.Colors.Choice;
        //}

        //private void buttonForFocusWithTabIndex100_KeyUp(object sender, KeyEventArgs e)
        //{
        //    Debug.WriteInConsole($"KEY_UP 100  {buttonForFocusWithTabIndex100.TabIndex} of {this._Name}");
        //    if (e.KeyCode == Keys.Tab)
        //    {
        //            if (!KeyboardNavigation.MoveToSisterVisibleView(this))
        //            KeyboardNavigation.MoveToParentView(this);
        //        this.KeyCatched = e.KeyCode;
        //    }
        //}

        //private void buttonForFocusWithTabIndex100_Leave(object sender, EventArgs e)
        //{
        //    Debug.WriteInConsole($"LEAVE 100 with {buttonForFocusWithTabIndex100.TabIndex} of {this._Name}");
        //    this.BackColor = UnactivatedColor;
        //}

        //private void buttonForFocusWithTabIndex100_KeyDown(object sender, KeyEventArgs e)
        //{
        //    Debug.WriteInConsole($"KEY_DOWN 100  {buttonForFocusWithTabIndex100.TabIndex} of {this._Name}");
        //}

        //private void buttonForFocusWithTabIndex0_KeyDown(object sender, KeyEventArgs e)
        //{
        //    Debug.WriteInConsole($"KEY_DOWN 0  {buttonForFocusWithTabIndex100.TabIndex} of {this._Name}");
        //}

        //private void buttonForFocusWithTabIndex0_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    Debug.WriteInConsole($"KEY_Pressd 0  {buttonForFocusWithTabIndex100.TabIndex} of {this._Name}");
        //}

        internal T FindParentOfType<T>() where T : Control
        {
            Control c = this;

            while (c != null)
            {
                if (c is T ret)
                    return ret;
                c = c.Parent;
            }

            return null;
        }
    }
}
