﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using static TSU.Polar.GuidedModules.Steps.StakeOut.Tools;
using static TSU.Polar.GuidedModules.Steps.StakeOut;
using DT = TSU.Polar.GuidedModules.Steps.StakeOut.DisplayTypes;

namespace TSU.Views
{
    public partial class TreeGrid : UserControl
    {
        public DataGridView GridPart
        {
            get
            {
                return this.dataGridOfTreeGrid;
            }
        }
        public Color BackColor
        {
            get
            {
                return dataGridOfTreeGrid.BackgroundColor;
            }
            set
            {
                // ignore;
            }
        }

        ITreeGridCategory dataSource;
        public ITreeGridCategory DataSource
        {
            get
            {
                return dataSource;
            }

            set
            {
                dataSource = value;
                Populate(dataSource);
            }
        }

        public Func<List<ITreeGridCategory>, object> CastToRelevantType { get; set; }
        public Func<Color, object> BackgroundColoring { get; set; }
        public Func<Color, object> ForegroundColoring { get; set; }

        public List<string> HiddenColumns { get; set; }
        public DataGridViewCellEventHandler DatagridView1_CellMouseEnter { get; set; }
        public DataGridViewCellEventHandler DatagridView1_CellMouseLeave { get; set; }
        public DataGridViewCellPaintingEventHandler DatagridView1_CellPaint { get; set; }
        public DataGridViewCellEventHandler DatagridView1_CellDoubleClick { get; set; }

        public TreeGrid()
        {
            InitializeComponent();
            ((ISupportInitialize)(this.GridPart)).BeginInit();
            Tsunami2.Preferences.Theme.ApplyTo(this.GridPart);
            this.GridPart.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            //this.GridPart.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            ((ISupportInitialize)(this.GridPart)).EndInit();
        }

        private void TreeGrid_Load(object sender, EventArgs e)
        {
            var dgv = dataGridOfTreeGrid;
            dgv.CellClick += DatagridView1_CellClick;
            dgv.AllowUserToResizeColumns = true;
            dgv.RowPrePaint += DatagridView1_RowPrePaint;
            dgv.CellPainting += DatagridView1_CellPaint;
            dgv.CellFormatting += DataGridView1_CellFormatting;
            dgv.CellDoubleClick += DatagridView1_CellDoubleClick;
            dgv.CellMouseEnter += DatagridView1_CellMouseEnter;
            dgv.CellMouseLeave += DatagridView1_CellMouseLeave;
        }

        List<int> selectedRowIndexes = new List<int>();

        private void DatagridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
                return;
            if (e.ColumnIndex == -1)
                return;

            NodeState_ScrollPosition_Save();

            NodeState_Selection_Save();

            // tree looking column clicked
            if (e.ColumnIndex == 0)
            {
                NodeState_Collapsed_Save(e);

                Update();
            }
        }

        // we save only the collapse that has child
        private void NodeState_Collapsed_Save(DataGridViewCellEventArgs e)
        {
            if (FlattenCategories[e.RowIndex] is ITreeGridCategory node)
            {
                if (!node.IsLeaf)
                {
                    var name = node.FullStructureName;
                    bool collapsed = !node.IsCollapsed;

                    NodeState_ListOfCollapsedNodes.Remove(name);
                    if (collapsed)
                        NodeState_ListOfCollapsedNodes.Add(name);
                    node.IsCollapsed = collapsed;
                }
            }
        }

        private void NodeState_ScrollPosition_Save()
        {
            lastVisibleFirstRow = dataGridOfTreeGrid.FirstDisplayedScrollingRowIndex;
        }

        private void NodeState_Selection_Save()
        {
            // Clear the existing list of selected indexes
            selectedRowIndexes.Clear();

            // Record the selected row indexes
            foreach (DataGridViewRow row in dataGridOfTreeGrid.SelectedRows)
            {
                selectedRowIndexes.Add(row.Index);
            }
        }

        private void DataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dataGridOfTreeGrid.Columns[e.ColumnIndex].HeaderText == "Categories")
            {
                // Do not change the default header style
                if (e.CellStyle != dataGridOfTreeGrid.ColumnHeadersDefaultCellStyle)
                {
                    e.CellStyle.WrapMode = DataGridViewTriState.False;
                    e.CellStyle.Font = Tsunami2.Preferences.Theme.Fonts.MonospaceFont;
                    e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                }
                else
                    Debug.WriteInConsole($"On Column Header, e.RowIndex={e.RowIndex}, e.ColumnIndex={e.ColumnIndex}");
                if (FlattenCategories[e.RowIndex] is ITreeGridCategory node)
                {
                    bool hasNodes = node.SubCategories.Count > 0;
                    string header = "";

                    var parent = node.Parent;
                    while (parent != null)
                    {
                        if (parent.Name == "Categories")
                            header = header;
                        else if (parent.IsLastNodeOfALevel)
                            header = $"   " + header;
                        else
                            header = $" │ " + header; //  \u2502 
                        parent = parent.Parent;
                    }

                    if (hasNodes)
                    {
                        if (node.IsCollapsed)
                            header += "[+]";
                        else
                            header += "[-]";
                    }
                    else
                    {
                        if (node.IsLastNodeOfALevel)
                            header += $" └ "; //  \u2514
                        else
                            header += $" ├ "; //  \u251C
                    }
                    e.Value = header + e.Value;
                    e.FormattingApplied = true;
                }
                else
                {
                    e.FormattingApplied = false;
                }
            }
        }

        private void DatagridView1_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            // TSU.Debug.WriteInConsole($"Pre DatagridView1_RowPrePaint {e.RowIndex}");

            var row = dataGridOfTreeGrid.Rows[e.RowIndex];
            ITreeGridCategory tgc = FlattenCategories[e.RowIndex];
            var theme = Tsunami2.Preferences.Theme;
            var informationColor = theme.Colors.Grayout;
            {
                if (tgc.Grayout)
                    row.DefaultCellStyle.BackColor = informationColor;
                if (tgc.Highlighted)
                    row.DefaultCellStyle.ForeColor = theme.Colors.Highlight;
                else if (tgc.Obsolete)
                    row.DefaultCellStyle.ForeColor = informationColor;
            }

            // TSU.Debug.WriteInConsole($"After DatagridView1_RowPrePaint {e.RowIndex}");
        }


        private void Populate(ITreeGridCategory categories)
        {
            IsPopulating = true;
            dataGridOfTreeGrid.Tag = categories;

            //Update();
            IsPopulating = false;

        }
        private bool Test(TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                if (node == null)
                    return false;
                if (node.Nodes.Count > 0)
                    Test(node.Nodes);
            }
            return true;
        }

        bool IsPopulating = false;
        public List<ITreeGridCategory> FlattenCategories { get; set; }

        private List<ITreeGridCategory> ToFlatCategories(List<ITreeGridCategory> categories)
        {
            List<ITreeGridCategory> flattenElements = new List<ITreeGridCategory>();
            foreach (ITreeGridCategory item in categories)
            {
                flattenElements.Add(item);

                // IF collapsed we stop
                if (NodeState_ListOfCollapsedNodes.Contains(item.FullStructureName))
                {
                    item.IsCollapsed = true;
                    continue;
                }

                // flatten children
                flattenElements.AddRange(ToFlatCategories(item.SubCategories));

            }
            return flattenElements;
        }

        public class TestCategory : BaseTreeGridCategory, ITreeGridCategory
        {
            public string Name { get; set; }
            public string Categories { get; set; }
            public object Tag { get; set; }
            public List<ITreeGridCategory> SubCategories { get; set; }
            public ITreeGridCategory Parent { get; set; }
            public bool Obsolete { get; set; } = false;
            public bool Highlighted { get; set; } = false;
            public bool Grayout { get; set; } = false;
            public bool IsCollapsed { get; set; }

            public bool IsLeaf
            {
                get
                {
                    if (SubCategories == null) return true;
                    return SubCategories.Count == 0;
                }
            }

            int depth;


            public bool IsLastNodeOfALevel
            {
                get
                {
                    if (Parent != null)
                        if (Parent.SubCategories[Parent.SubCategories.Count - 1] == this)
                            return true;
                    return false;
                }
            }
            public int Depth
            {
                get
                {
                    if (depth == 0)
                    // compute it
                    {
                        var parent = this.Parent;
                        while (parent != null)
                        {
                            depth++;
                            parent = parent.Parent;
                        }

                    }
                    return depth;
                }
            }

            public Guid Guid { get; set; } = new Guid();

            public TestCategory(string name, object tag)
            {
                Name = name;
                Tag = tag;
                SubCategories = new List<ITreeGridCategory>();
            }


            public void Add(ITreeGridCategory subNode)
            {
                this.SubCategories.Add(subNode);
                subNode.Parent = this;
            }
        }

        private List<ITreeGridCategory> GetTestCategories()
        {
            List<ITreeGridCategory> categories = new List<ITreeGridCategory>();
            categories.Add(new TestCategory("R1", "R1"));
            categories.Add(new TestCategory("A", "")
            {
                SubCategories = new List<ITreeGridCategory>() {
                    new TestCategory("E", "E"),
                    new TestCategory("S", "s"),
                    new TestCategory("Other sockets", "o") {
                        SubCategories = new List<ITreeGridCategory>() {
                            new TestCategory("Beam_E", "E"),
                            new TestCategory("Beam_S", "s"),
                        }
                    },
                    new TestCategory("Beam points",  "bp") {
                        SubCategories = new List<ITreeGridCategory>() {
                            new TestCategory("Beam_E", "E"),
                            new TestCategory("Beam_S", "s"),
                            new TestCategory("Other beam points", "o") {
                                SubCategories = new List<ITreeGridCategory>() {
                                    new TestCategory("Beam_A", "A"),
                                    new TestCategory("Beam_B", "B"),
                                }
                            }
                        }
                    }
                }
            });
            categories.Add(new TestCategory("B", "")
            {
                SubCategories = new List<ITreeGridCategory>() {
                    new TestCategory("E", "E"),
                    new TestCategory("S", "S"),
                    new TestCategory("Other sockets", "o") {
                        SubCategories = new List<ITreeGridCategory>() {
                            new TestCategory("Beam_E", "E"),
                            new TestCategory("Beam_S", "\"S\""),
                        }
                    },
                    new TestCategory("Beam point", "bp") {
                        SubCategories = new List<ITreeGridCategory>() {
                            new TestCategory("Beam_E", "E"),
                            new TestCategory("Beam_S", "S"),
                            new TestCategory("Other beam points", "o") {
                                SubCategories = new List<ITreeGridCategory>() {
                                    new TestCategory("Beam_A",  "A"),
                                    new TestCategory("Beam_B", "B"),
                                }
                            }
                        }
                    }
                }
            });
            categories.Add(new TestCategory("R2", "R2"));
            return categories;

        }


        List<Panel> panels = new List<Panel>();


        static List<string> NodeState_ListOfCollapsedNodes = new List<string>();
        static int lastVisibleFirstRow = 0;

        // show rows based on the treeview collapse/expand pattern
        internal void Update(bool rabotIsUsed = false)
        {
            int id = Debug.GetNextId();
            Debug.WriteInConsole("Begin TreeGrid.Update", id);
            var dgv = dataGridOfTreeGrid;
            if (dgv.Tag is ITreeGridCategory categories)
            {
                int subId = Debug.GetNextId();
                Debug.WriteInConsole("Begin TreeGrid.Update ToFlatCategories", subId);
                this.FlattenCategories = ToFlatCategories(categories.SubCategories);
                Debug.WriteInConsole("End TreeGrid.Update ToFlatCategories", subId);

                subId = Debug.GetNextId();
                Debug.WriteInConsole("Begin TreeGrid.Update CastToRelevantType", subId);
                object selectedObjects = CastToRelevantType(this.FlattenCategories);
                Debug.WriteInConsole("End TreeGrid.Update CastToRelevantType", subId);

                subId = Debug.GetNextId();
                Debug.WriteInConsole("Begin TreeGrid.Update DataGridToDataTable", subId);
                // Copy everything to a DataTable
                DataTable dt = ToDataTable((IList)selectedObjects);
                Debug.WriteInConsole("End TreeGrid.Update DataGridToDataTable", subId);

                foreach (DataRow line in dt.Rows)
                    Debug.WriteInConsole(string.Join(";", line.ItemArray));

                subId = Debug.GetNextId();
                Debug.WriteInConsole("Begin TreeGrid.Update HideHiddenOrEmptyColumns", subId);
                // hide empty column r the one specifies in .HiddenColumns property
                HideHiddenOrEmptyColumns(dt);
                Debug.WriteInConsole("End TreeGrid.Update HideHiddenOrEmptyColumns", subId);

                subId = Debug.GetNextId();
                Debug.WriteInConsole("Begin TreeGrid.Update SetCaptions", subId);
                // make better title
                SetCaptions(dt);
                Debug.WriteInConsole("End TreeGrid.Update SetCaptions", subId);

                subId = Debug.GetNextId();
                Debug.WriteInConsole("Begin TreeGrid.Update 2nd set DataSource", subId);
                // set the filtered dataset as datasoruce
                dgv.DataSource = dt;
                Debug.WriteInConsole("End TreeGrid.Update 2nd set DataSource", subId);

                subId = Debug.GetNextId();
                Debug.WriteInConsole("Begin TreeGrid.Update style all columns", subId);
                // style all columns first
                foreach (DataGridViewColumn column in dgv.Columns)
                {
                    column.Visible = true;
                    column.SortMode = DataGridViewColumnSortMode.NotSortable;
                }
                Debug.WriteInConsole("End TreeGrid.Update style all columns", subId);

                subId = Debug.GetNextId();
                Debug.WriteInConsole("Begin TreeGrid.Update NodeState_ScrollPosition_Restore", subId);
                if (lastVisibleFirstRow > 0)
                    dgv.FirstDisplayedScrollingRowIndex = lastVisibleFirstRow;
                Debug.WriteInConsole("End TreeGrid.Update NodeState_ScrollPosition_Restore", subId);

                subId = Debug.GetNextId();
                Debug.WriteInConsole("Begin TreeGrid.Update ClearSelection", subId);
                // Deselect all rows before restoring the selection
                dgv.ClearSelection();
                Debug.WriteInConsole("End TreeGrid.Update ClearSelection", subId);

                subId = Debug.GetNextId();
                Debug.WriteInConsole("Begin TreeGrid.Update NodeState_Selection_Restore", subId);
                // Restore the selection based on the recorded indexes
                foreach (int index in selectedRowIndexes)
                {
                    if (index >= 0 && index < dgv.Rows.Count)
                    {
                        dgv.Rows[index].Selected = true;
                    }
                }
                Debug.WriteInConsole("End TreeGrid.Update NodeState_Selection_Restore", subId);
            }

            Debug.WriteInConsole("End TreeGrid.Update", id);
            if (rabotIsUsed && Tsunami2.Properties.ValueToDisplayType == DisplayTypes.Displacement)
            {
                var x = this.GridPart.Columns["X"];
                x.HeaderText = x.HeaderText.Replace("d", "o");
                var y = this.GridPart.Columns["Y"];
                y.HeaderText = y.HeaderText.Replace("d", "o");
                var z = this.GridPart.Columns["Z"];
                z.HeaderText = z.HeaderText.Replace("d", "o");
            }
        }

        private DataTable ToDataTable(IList selectedObjects)
        {
            DataTable table = new DataTable();
            InitMustBeHidden();
            foreach (object item in selectedObjects)
            {
                List<object> newValues = new List<object>(table.Columns.Count);
                foreach (DataColumn _ in table.Columns)
                    newValues.Add(null);
                PropertyDescriptorCollection props = TypeDescriptor.GetProperties(item.GetType());
                foreach (PropertyDescriptor prop in props)
                {
                    string propName = prop.Name;
                    if(!MustBeHidden(propName))
                    { 
                        int index = table.Columns.IndexOf(propName);
                        object val = prop.GetValue(item) ?? null;
                        if (index == -1)
                        {
                            table.Columns.Add(propName, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                            newValues.Add(val);
                        }
                        else
                        {
                            newValues[index] = val;
                        }
                    }
                }
                table.Rows.Add(newValues.ToArray());
            }
            return table;
        }

        private static void SetCaptions(DataTable dt)
        {
            foreach (DataColumn column in dt.Columns)
            {
                var header = column.ColumnName;
                header = header.Replace("_M_", "\r\n[m]");
                header = header.Replace("_MM_", "\r\n[mm]");
                header = header.Replace("_MRAD_", "\r\n[mRad]");
                header = header.Replace("_", "\r\n");
                column.Caption = header;
            }
        }

        private void HideHiddenOrEmptyColumns(DataTable dt)
        {
            if (!MustBeHidden_HideEmpty) 
                return;

            DataColumnCollection columns = dt.Columns;
            List<DataColumn> toRemove = new List<DataColumn>();
            foreach (DataColumn column in columns)
            {
                bool isEmpty = true;
                foreach (DataRow row in dt.Rows)
                {
                    if (row[column.Ordinal] != null && !string.IsNullOrWhiteSpace(row[column.Ordinal].ToString()))
                    {
                        // Cell is not empty, do not hide the column
                        isEmpty = false;
                        break;
                    }
                }

                // Hide the column if all cells are empty
                if (isEmpty)
                    toRemove.Add(column);
            }
            foreach (var col in toRemove)
                dt.Columns.Remove(col);
        }

        private bool MustBeHidden_HideVert;

        private static readonly string[] MustBeHidden_beamVertColumns = {
            "Exp_Rbv_MM_",
            "Exp_Lbv_MM_",
            "Exp_Hbv_MM_",
            "Move_Rbv_MM_",
            "Move_Lbv_MM_",
            "Move_Hbv_MM_",
        };

        private static readonly string[] MustBeHidden_technicalColumns = {
            "Tag",
            "IsLeaf",
            "Guid",
            "IsCollapsed",
            //"Categories",
            "Obsolete",
            "Highlighted",
            "Grayout",
            "IsLastNodeOfALevel",
            "Parent",
            "Depth",
            "FullStructureName",
            "Unit",
            "SubCategories",
            "Measure"
        };

        private bool MustBeHidden_HideEmpty;

        private void InitMustBeHidden()
        {
            string typeS = Tsunami2.Preferences.Tsunami.CoordinatesSystems.SelectedType._Name;
            DT typeD = Tsunami2.Properties.ValueToDisplayType;
            MustBeHidden_HideVert = ((typeD != DT.Offsets && typeD != DT.Displacement) || typeS != "BEAMV");
            MustBeHidden_HideEmpty = HiddenColumns == null || HiddenColumns.Count == 0;
        }

        private bool MustBeHidden(string columnName)
        {
            if (MustBeHidden_HideVert && MustBeHidden_beamVertColumns.Contains(columnName))
                return true;

            if (MustBeHidden_technicalColumns.Contains(columnName))
                return true;

            if (!MustBeHidden_HideEmpty && HiddenColumns.Contains(columnName))
                return true;

            return false;
        }

        // base on the list of node name that are collpased, we set the temporpry treenode
        private void NodeState_Collapsed_Restore(TreeNode treeNode, ITreeGridCategory currentnodes)
        {
            if (currentnodes.IsCollapsed)
                treeNode.Collapse();
            else
                treeNode.Expand();
            for (int i = 0; i < currentnodes.SubCategories.Count; i++)
            {
                NodeState_Collapsed_Restore(treeNode.Nodes[i], currentnodes.SubCategories[i]);
            }
        }

        bool resizingInProgress = false;

        private void TreeGrid_Resize(object sender, EventArgs e)
        {
            Debug.WriteInConsole("Tree Grid resize");
            if (resizingInProgress) return;
            //Update();
        }
    }

    public interface ITreeGridCategory
    {
        string Name { get; set; }
        string Categories { get; set; }
        object Tag { get; set; }
        List<ITreeGridCategory> SubCategories { get; set; }

        bool Obsolete { get; set; }
        bool Highlighted { get; set; }
        bool Grayout { get; set; }

        ITreeGridCategory Parent { get; set; }

        bool IsCollapsed { get; set; }

        string FullStructureName { get; }

        bool IsLeaf { get; }
        Guid Guid { get; set; }

        int Depth { get; }
        bool IsLastNodeOfALevel { get; }

        void Add(ITreeGridCategory subNode);
    }

    public class BaseTreeGridCategory : ITreeGridCategory
    {

        public virtual string Categories { get; set; }
        public virtual string Name { get; set; }

        public object Tag { get; set; }
        public List<ITreeGridCategory> SubCategories { get; set; } = new List<ITreeGridCategory>();
        public bool IsCollapsed { get; set; }
        public bool Obsolete { get; set; }
        public bool Grayout { get; set; }
        public bool Highlighted { get; set; } = false;
        public Guid Guid { get; set; } = Guid.NewGuid();

        public string FullStructureName
        {
            get
            {
                var parentFullStructureName = Parent != null ? Parent.FullStructureName : "";
                var name = Name != "" ? Name : Categories;
                return parentFullStructureName + ":" + name;
            }
        }

        public bool IsLastNodeOfALevel
        {
            get
            {
                if (Parent != null)
                    if (Parent.SubCategories[Parent.SubCategories.Count - 1] == this)
                        return true;
                return false;
            }
        }
        public ITreeGridCategory Parent { get; set; }

        public bool IsLeaf
        {
            get
            {
                if (SubCategories == null) return true;
                return SubCategories.Count == 0;
            }
        }

        int depth = 0;
        public int Depth
        {
            get
            {
                if (depth == 0)
                // compute it
                {
                    var parent = this.Parent;
                    while (parent != null)
                    {
                        depth++;
                        parent = parent.Parent;
                    }

                }
                return depth;
            }
        }
        public void Add(ITreeGridCategory subNode)
        {
            this.SubCategories.Add(subNode);
            subNode.Parent = this;
        }
    }

    public static class CategorizedResultExtensions
    {
        public static bool Contains<T>(this T src, Guid guid) where T : List<ITreeGridCategory>
        {
            return !(src.Find(x => x.Guid == guid) == null);
        }
        public static ITreeGridCategory GetByName<T>(this T src, string name, string treeName) where T : List<ITreeGridCategory>
        {
            return src.Find(x => x.Name == name && x.Categories == treeName);
        }
    }
}
