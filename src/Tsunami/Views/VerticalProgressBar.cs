﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Security.Permissions;


namespace TSU.Views
{
    //public static class ModifyProgressBarColor
    //{
    //    [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
    //    static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr w, IntPtr l);
    //    public static void SetState(this ProgressBar pBar, int state)
    //    {
    //        SendMessage(pBar.Handle, 1040, (IntPtr)state, IntPtr.Zero);
    //    }
    //}
    public class VerticalAlignmentProgressBar : ProgressBar
    
    {
        private double tolerance;
        public double _tolerance
            //tolerance d'alignement en mm
        {
            get
            {
                return this.tolerance;
            }
            set
            {
                //force la tolerance d'alignment à être positive
                tolerance = Math.Abs(value);
                //permet d'éviter l'overflowException
                if (400*tolerance >= Int32.MaxValue)
                {
                    this.Maximum = Int32.MaxValue;
                }
                else
                {
                    if (400*tolerance <= Int32.MinValue)
                    {
                        this.Maximum = Int32.MinValue;
                    }
                    else
                    {
                        this.Maximum = Convert.ToInt32(400 * this.tolerance);
                    }
                }
                
            }
        }
        protected override CreateParams CreateParams
            //Force le style de la progress bar en vertical : sets the PBS_VERTICAL flag in Style
        {
            get
            {
                // Avoid CA2122
                new SecurityPermission(SecurityPermissionFlag.UnmanagedCode).Demand();

                CreateParams cp = base.CreateParams;
                cp.Style |= 0x04;
                return cp;
            }
        }
        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
            // permet de dessiner la progress bar vertical et avec les couleurs désirées en fonction de la valeur
        {
            if (ProgressBarRenderer.IsSupported)
            {
                ProgressBarRenderer.DrawVerticalBar(e.Graphics, e.ClipRectangle);

                const int HORIZ_OFFSET = 3;
                const int VERT_OFFSET = 2;

                if (this.Minimum == this.Maximum || (this.Value - Minimum) == 0 ||
                        this.Height < 2 * VERT_OFFSET || this.Width < 2 * VERT_OFFSET)
                    return;

                int barHeight = (this.Value - this.Minimum) * this.Height / (this.Maximum - this.Minimum);
                barHeight = Math.Min(barHeight, this.Height - 2 * VERT_OFFSET);
                int barWidth = this.Width - 2 * HORIZ_OFFSET;
                //Choisi la couleur de la progress bar en fonction de la tolérance
                SolidBrush rectBrush = new SolidBrush(TSU.Tsunami2.Preferences.Theme.Colors.Good); ;
                if (this.doubleValue < tolerance)
                {
                    rectBrush = new SolidBrush(TSU.Tsunami2.Preferences.Theme.Colors.Good);
                }
                if (this.doubleValue >= tolerance)
                {
                    rectBrush = new SolidBrush(TSU.Tsunami2.Preferences.Theme.Colors.Attention);
                }
                if (this.doubleValue > 1.5 * tolerance)
                {
                    rectBrush = new SolidBrush(TSU.Tsunami2.Preferences.Theme.Colors.Bad);
                }
                //Dessine la progress bar de bas en haut (right to left = no) ou de haut en bas (right to left = yes)
                if (this.RightToLeftLayout && this.RightToLeft == System.Windows.Forms.RightToLeft.Yes)
                {
                    Rectangle rect = new Rectangle(HORIZ_OFFSET, VERT_OFFSET, barWidth, barHeight);
                    ProgressBarRenderer.DrawVerticalChunks(e.Graphics,rect);
                    e.Graphics.FillRectangle(rectBrush, rect);
                }
                else
                {
                    int blockHeight = 10;
                    int wholeBarHeight = Convert.ToInt32(barHeight / blockHeight) * blockHeight;
                    int wholeBarY = this.Height - wholeBarHeight - VERT_OFFSET;
                    int restBarHeight = barHeight % blockHeight;
                    int restBarY = this.Height - barHeight - VERT_OFFSET;
                    Rectangle rect1 = new Rectangle(HORIZ_OFFSET, wholeBarY, barWidth, wholeBarHeight);
                    Rectangle rect2 = new Rectangle(HORIZ_OFFSET, restBarY, barWidth, restBarHeight);
                    ProgressBarRenderer.DrawVerticalChunks(e.Graphics,rect1);
                    ProgressBarRenderer.DrawVerticalChunks(e.Graphics,rect2);
                    e.Graphics.FillRectangle(rectBrush, rect1);
                    e.Graphics.FillRectangle(rectBrush, rect2);
                }
            }
            base.OnPaint(e);
        }
        private double doubleValue;
        public double _doubleValue
            //remplace la propriété value pour pouvoir mettre directement des doubles sans faire de conversion - unité mm
        {
            get
            {
                return this.doubleValue;
            }
            set
            {
                this.doubleValue = value;
                int intValue = 0;
                //permet d'éviter l'overflowException
                if (value*100>=Int32.MaxValue)
                {
                    intValue = Math.Abs(Int32.MaxValue);
                }
                else
                {
                    if (value*100<=Int32.MinValue)
                    {
                        intValue = Math.Abs(Int32.MinValue);
                    }
                    else
                    {
                        intValue = Math.Abs(Convert.ToInt32(100*value));
                    }
                }
                //N'autorise pas de valeur > maximum pour la valeur value
                if (intValue > this.Maximum)
                {
                    this.Value = this.Maximum;
                }
                else
                {
                    this.Value = intValue;
                }
            }
        }
        public VerticalAlignmentProgressBar ()
            //constructeur en tenant compte d'une tolérance définie
        {
            // Enable OnPaint overriding
            this.SetStyle(ControlStyles.UserPaint, true);
            this._tolerance = 0.2;
            this.doubleValue = 0;
        }
    }
}
