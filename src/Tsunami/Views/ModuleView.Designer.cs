﻿namespace TSU.Views
{
    partial class ModuleView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._PanelBottom = new System.Windows.Forms.Panel();
            this._PanelTop = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // _ContextMenuStrip
            // 
            this._ContextMenuStrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this._ContextMenuStrip.Name = "_ContextMenuStrip";
            this._ContextMenuStrip.Size = new System.Drawing.Size(61, 4);
            // 
            // _PanelBottom
            // 
            this._PanelBottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this._PanelBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this._PanelBottom.Location = new System.Drawing.Point(5, 71);
            this._PanelBottom.Name = "_PanelBottom";
            this._PanelBottom.Size = new System.Drawing.Size(738, 130);
            this._PanelBottom.TabIndex = 5;
            // 
            // _PanelTop
            // 
            this._PanelTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this._PanelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this._PanelTop.Location = new System.Drawing.Point(5, 5);
            this._PanelTop.MinimumSize = new System.Drawing.Size(0, 20);
            this._PanelTop.Name = "_PanelTop";
            this._PanelTop.Size = new System.Drawing.Size(738, 66);
            this._PanelTop.TabIndex = 4;
            // 
            // ModuleView
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.ClientSize = new System.Drawing.Size(748, 206);
            this.Controls.Add(this._PanelBottom);
            this.Controls.Add(this._PanelTop);
            this.MinimumSize = new System.Drawing.Size(700, 187);
            this.Name = "ModuleView";
            this.Text = "ModuleView";
            this.Activated += new System.EventHandler(this.ModuleView_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ModuleView_FormClosing);
            this.ResizeBegin += new System.EventHandler(this.ModuleView_ResizeBegin);
            this.Controls.SetChildIndex(this._PanelTop, 0);
            this.Controls.SetChildIndex(this._PanelBottom, 0);
            this.Controls.SetChildIndex(this.buttonForFocusWithTabIndex0, 0);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ContextMenuStrip _ContextMenuStrip;
        /// <summary>
        /// The bottom panel is the amin panel under the main big button of the view
        /// </summary>
        public System.Windows.Forms.Panel _PanelBottom;
        /// <summary>
        /// The top panel is generally a small on containg the top big button
        /// </summary>
        public System.Windows.Forms.Panel _PanelTop;
    }
}