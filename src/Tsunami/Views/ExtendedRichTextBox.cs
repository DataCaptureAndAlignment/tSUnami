﻿using System;
using System.Drawing.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace TSU.Views
{
    public class ExtendedRichTextBox : RichTextBox
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int SendMessage(IntPtr hWnd, int wMsg, IntPtr wParam, IntPtr lParam);

        private const int WM_VSCROLL = 277;
        private const int SB_PAGEBOTTOM = 7;
        private const int WM_SETREDRAW = 0xB;
        private const int WM_USER = 0x400;
        private const int EM_GETEVENTMASK = WM_USER + 59;
        private const int EM_SETEVENTMASK = WM_USER + 69;

        private IntPtr eventMask;

        public bool IsUpdating { get; private set; } = false;

        /// <summary>
        /// /!\ use of a trick to avoid to use scrollToCaret, which sometime create a acces memory violation
        /// </summary>
        public void ScrollToBottom()
        {
            SendMessage(Handle, WM_VSCROLL, (IntPtr)SB_PAGEBOTTOM, IntPtr.Zero);
        }

        public void BeginUpdate()
        {
            IsUpdating = true;
            // Stop redrawing:
            SendMessage(Handle, WM_SETREDRAW, IntPtr.Zero, IntPtr.Zero);
            // Stop sending of events:
            eventMask = (IntPtr)SendMessage(Handle, EM_GETEVENTMASK, IntPtr.Zero, IntPtr.Zero);
        }

        public void EndUpdate()
        {
            IsUpdating = false;
            // turn on events
            SendMessage(Handle, EM_SETEVENTMASK, IntPtr.Zero, eventMask);
            // turn on redrawing
            SendMessage(Handle, WM_SETREDRAW, (IntPtr)1, IntPtr.Zero);
            // this forces a repaint, which for some reason is necessary in some cases.
            Invalidate();
        }
    }
}
