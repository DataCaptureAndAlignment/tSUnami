﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TSU.Common;
using TSU.Tools;
using TSU.Tools.Exploration;
using TSU.Views.Message;
using F = TSU.Functionalities;
using R = TSU.Properties.Resources;

namespace TSU.Views
{
    public partial class ModuleView : TsuView
    {
        #region Fields
        private Color originalColor;
        private int blinkingCount;
        public Image Image;

        public bool popUpReady = false;
        private bool StopShowMessageAboutHiding;
        internal Module Module
        {
            get
            {
                return _Module as Module;
            }
            set
            {
                _Module = value;
            }
        }
        #endregion

        #region Constructor
        public ModuleView() : base()
        {
            InitializeComponent();
            AdaptTopPanelToButtonHeight();

            SuspendLayout();
            AutoScroll = true;
            ResumeLayout();
        }
        public ModuleView(IModule linkedModule)
                    : base(linkedModule)
        {
            icon = R.TSU1;
            Image = R.TSU1;
            InitializeComponent();
            ApplyThemeColors();
            AdaptTopPanelToButtonHeight();
            SuspendLayout();

            contextButtons = new List<Control>();
            originalColor = BackColor;
            _PanelBottom.AutoScroll = true;
            ResumeLayout();

            buttonForFocusWithTabIndex0.KeyUp += On_FocusButton_KeyUp;
        }


        private void On_FocusButton_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Down)
                if (_PanelTop.Controls.Count > 0)
                    _PanelTop.Controls[0].Focus();
        }

        public void ApplyThemeColors()
        {
            _ContextMenuStrip.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
            _PanelBottom.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
            _PanelTop.BackColor = Tsunami2.Preferences.Theme.Colors.Background;
            BackColor = Tsunami2.Preferences.Theme.Colors.Background;
            AllowTransparency = false;
        }
        protected override void OnGotFocus(EventArgs e)
        {
            buttonForFocusWithTabIndex0.Focus();
            //Invalidate();
            //base.OnGotFocus(e);
        }

        internal MessageTsu FindMessageModuleViewInParents()
        {
            Control current = this;
            while (current != null)
            {
                if (current is MessageTsu tsu)
                    return tsu;
                current = current.Parent;
            }
            return null;
        }

        protected override void OnLostFocus(EventArgs e)
        {
            base.OnLostFocus(e);
            Invalidate();
        }

        public override void SelectNextButton(BigButton current, bool forward)
        {
            if (_PanelTop.Controls.Count > 0)
                if (current == _PanelTop.Controls[0])
                {
                    if (forward)
                    {
                        if (_PanelBottom.Controls.Count > 0)
                            _PanelBottom.Controls[0].Focus();
                    }
                    else
                    {
                        KeyboardNavigation.MoveToParentView(this);
                    }
                }

        }

        internal void AdaptTopPanelToButtonHeight()
        {
            if (Tsunami2.Preferences.Values.GuiPrefs != null)
            {
                _PanelTop.Height = Tsunami2.Preferences.Theme.ButtonHeight;
            }
        }

        #endregion


        #region Button & menu
        internal void AddTopButton(string titleAndDescription, Bitmap bitmap, Action action)
        {
            Color c = (_Module is FinalModule) ? Tsunami2.Preferences.Theme.Colors.Action : Tsunami2.Preferences.Theme.Colors.Choice;
            //Color c =TSU.Tsunami2.TsunamiPreferences.Theme._Colors.Action;
            BigButton bigbuttonTop = new BigButton(titleAndDescription, bitmap, action, c, true);
            bigbuttonTop.Margin = new Padding(0);
            _PanelTop.Controls.Add(bigbuttonTop);
        }

        private void PutColorBack(object sender, EventArgs e)
        {
            if (blinkingCount > 5) ((System.Timers.Timer)sender).Dispose();
            BackColor = (BackColor == originalColor) ? Tsunami2.Preferences.Theme.Colors.Good : originalColor;
            blinkingCount += 1;
        }
        public void Blink()
        {
            BackColor = Tsunami2.Preferences.Theme.Colors.Good;
            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new System.Timers.ElapsedEventHandler(PutColorBack);
            aTimer.Interval = 200;
            blinkingCount = 0;
            aTimer.Enabled = true;
        }
        internal void AddButtonsInPanelTop(List<Control> existingControls, DockStyle dockstyle = DockStyle.Bottom, int PADDING = 0)
        {
            foreach (BigButton bb in existingControls)
            {
                bb.HasSubButtons = true;
            }
            AddControlsLineInControl(_PanelTop, existingControls, dockstyle, PADDING);
        }

        internal static void AddControlsLineInControl(Control controlWhereToAddButtons, List<Control> existingControl, DockStyle dockstyle = DockStyle.Bottom, int PADDING = 0)
        {
            int highestControlHeight = 0;
            foreach (Control c in existingControl)
            {
                if (c?.Height > highestControlHeight)
                    highestControlHeight = c.Height;
            }

            TableLayoutPanel TLP = new TableLayoutPanel()
            {
                RowCount = 1,
                ColumnCount = existingControl.Count,
                Dock = dockstyle,
                Height = highestControlHeight + 2 * PADDING,
                Padding = new Padding(PADDING)
            };

            float columnSizeInPercent = 100 / existingControl.Count;
            foreach (Control item in existingControl)
            {
                TLP.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, columnSizeInPercent));

                TLP.Controls.Add(item);
            }

            controlWhereToAddButtons.Controls.Add(TLP);
            TLP.BringToFront();
        }

        internal void CloseMenu()
        {
            // this is automatic
        }
        #endregion

        // will return choose path in the view or "" if cancel
        internal string GetSavingName(string initialDirectory, string defaultFileName, string filter)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Title = "Choose the name of the file to save to";
            saveFileDialog1.InitialDirectory = initialDirectory;
            saveFileDialog1.CheckFileExists = false;
            saveFileDialog1.FileName = defaultFileName;
            saveFileDialog1.Filter = filter;
            saveFileDialog1.FilterIndex = 1;
            //saveFileDialog1.FileOk += OnSaveDialogButtonClick;

            if (saveFileDialog1.ShowDialog(this) == DialogResult.OK) return saveFileDialog1.FileName; else return "";
        }

        // will return choose path in the view or "" if cancel
        internal string GetSavingNameForAppend(string initialDirectory, string defaultFileName, string filter)
        {
            OpenFileDialog saveFileDialog1 = new OpenFileDialog();
            saveFileDialog1.Title = "Choose the name of the file to  append (*.dat) to";
            saveFileDialog1.InitialDirectory = initialDirectory;
            saveFileDialog1.CheckFileExists = false;
            saveFileDialog1.FileName = defaultFileName;
            saveFileDialog1.Filter = filter;
            saveFileDialog1.FilterIndex = 1;
            // saveFileDialog1.FileOk += OnSaveDialogButtonClick;

            if (saveFileDialog1.ShowDialog(this) == DialogResult.OK) return saveFileDialog1.FileName; else return "";
        }

        private void OnSaveDialogButtonClick(object sender, CancelEventArgs e)
        {
            OpenFileDialog saveFileDialog1 = sender as OpenFileDialog;
            if (System.IO.Path.GetExtension(saveFileDialog1.FileName) != ".dat")
            {
                if (System.IO.File.Exists(saveFileDialog1.InitialDirectory + saveFileDialog1.FileName))
                {
                    MessageInput mi = new MessageInput(MessageType.Critical,
                        $"{R.T_FILE_EXIST};{R.T_DO_YOU_WANT_TO_CONTINUE_AND_LOOSE_THE_CONTENT_OF_THE_EXISTING_FILE}")
                    {
                        ButtonTexts = new List<string> { R.T_YES, R.T_NO }
                    };
                    if (mi.Show().TextOfButtonClicked== R.T_NO)
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        /// <summary>
        /// show the waiting form
        /// </summary>
        /// <param name="name"></param>
        /// <param name="totalEstimatedTimeInMilliSeconds"></param>
        //internal void ShowWaitingForm(TSU.Module initiator, string name, int totalEstimatedTimeInMilliSeconds)
        //{
        //    if (this.waitingForm != null) this.waitingForm.EndAll();
        //    this.waitingForm = new Views.Message.ProgressMessage(
        //        this,
        //        name,
        //        "Starting " + name,
        //        1,
        //        totalEstimatedTimeInMilliSeconds
        //        );
        //    initiator.MessageBroadcast += OnMessageBroadcast;
        //    this.waitingForm.BeginAstep(name);
        //}




        /// <summary>
        /// hide the waiting form
        /// </summary>
        /// <param name="action"></param>
        /// <param name="name"></param>
        /// <param name="totalEstimatedTimeInMilliSeconds"></param>
        internal void HideWaitingForm()
        {
            if (WaitingForm != null) WaitingForm.EndAll();
        }
        #region Show

        public override void UpdateView()
        {
            try
            {
                base.UpdateView();
            }
            catch (Exception ex)
            {
                ShowMessageOfBug(string.Format(R.T536, _Module._Name, ":\r\n"), ex, R.T_OK + "!");
            }
        }

        #endregion

        #region Events

        private void ModuleView_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;

            if (_Module is F.Menu) return;
            if (_Module is Tsunami) return;
            if (_Module is Explorator)
            {
                Hide();
                return;
            }

            if (!StopShowMessageAboutHiding)
            {
                string cancel = R.T_CANCEL;
                string titleAndMessage = string.Format(R.T537, _Module.GetType());
                MessageInput mi = new MessageInput(MessageType.Warning, titleAndMessage)
                {
                    ButtonTexts = new List<string> { "Hide", cancel},
                };
                if (cancel != mi.Show().TextOfButtonClicked)
                {
                    StopShowMessageAboutHiding = true;
                    Hide();
                }
            }
            else
                Hide();


        }
        private void ModuleView_ResizeBegin(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
            {
                Dock = DockStyle.Fill;
            }
            else
                Dock = DockStyle.None;
        }
        #endregion

        public override string ToString()
        {
            return $"{GetType().ToString()} of {_Module._Name} ({Guid})";
        }

        private void ModuleView_Activated(object sender, EventArgs e)
        {
            // focus on the oppanel button
            foreach (var item in _PanelTop.Controls)
            {
                if (item is BigButton bb)
                    bb.Focus();
            }

        }
    }
}
