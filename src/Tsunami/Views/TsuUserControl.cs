﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using T = TSU.Tools;
using WF = System.Windows.Forms;

namespace TSU.Views
{
    /// <summary>
    /// Interaction logic for TsuUserControl.xaml
    /// </summary>
    public class TsuUserControl : UserControl
    {
        public TsuUserControl()
        {
            App.EnsureApplicationResources();
            Unloaded += UserControl_Unloaded;
            KeyUp += UserControl_KeyUp;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            CommandBindings.Clear();
            DataContext = null;
        }

        /// <summary>
        /// All subclasses should redefine this to return the parent TsuView
        /// </summary>
        public virtual TsuView MainView => null;

        private void UserControl_KeyUp(object sender, KeyEventArgs e)
        {
            var e2 = new WF.KeyEventArgs((WF.Keys)KeyInterop.VirtualKeyFromKey(e.Key));
            var ctrl = MainView;

            if (TsuView.CheckIfHandledAlready(control: ctrl, keyEventArgs: e2))
                return;

            // trying t use tab to move from tsuviews
            // if(this.ActiveControl != this.buttonForFocusWithTabIndex0)
            //    previousTabIndex = this.ActiveControl.TabIndex;

            // Navigation between tsuViews with keybard
            T.KeyboardNavigation.AnalyseAnAct(control: ctrl, keyEventArgs: e2);

            // HardCoded ShortCuts
            T.KeyboardShortCut.AnalyseAnAct(control: ctrl, keyEventArgs: e2);

            // check function keys for macro recording or macro playing
            T.Macro.AnalyseAndAct(e2, ctrl);
        }

    }
}
