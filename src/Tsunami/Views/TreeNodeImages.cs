﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using I = TSU.Common.Instruments;
using TSU.ENUM;

namespace TSU.Views
{
    public static class TreeNodeImages
    {
        //private static ImageList imageListInstance = CreateImageList();

        private static Lazy<ImageList> lazy = new Lazy<ImageList>(CreateImageList);

        public static ImageList ImageListInstance
        {
            get
            {
                return lazy.Value;
            }
        }
       

        private static ImageList CreateImageList()
        {
            ImageList l = new ImageList();
            l.ColorDepth = ColorDepth.Depth32Bit;
            l.ImageSize = new Size(32, 32);
            l.Images.Add(ElementType.Unknown.ToString(),R.Tsunami);
            l.Images.Add(ElementType.Point.ToString(),R.Element_Point);       // 0
            l.Images.Add(ElementType.CoordinateSystem.ToString(),R.Element_CS);       // 1
            //l.Images.Add(.ToString(),global::Tsunami.R.Element_Phys);       // 2
            l.Images.Add(ElementType.Parameters.ToString(),R.Element_Parameters);       // 3
            l.Images.Add(ElementType.X.ToString(),R.Element_X);       // 4
            l.Images.Add(ElementType.Y.ToString(),R.Element_Y);       // 5
            l.Images.Add(ElementType.Z.ToString(),R.Element_Z);       // 6
            l.Images.Add(ElementType.SocketCode.ToString(),R.Element_Code);       // 7
            l.Images.Add(ElementType.BeamBearing.ToString(),R.Element_Gisement);       // 8
            l.Images.Add(ElementType.Length.ToString(),R.Element_L);       // 9
            //l.Images.Add(ElementType.ToString(),global::Tsunami.R.Element_R);       // 10
            //l.Images.Add(.ToString(),global::Tsunami.R.Element_S);       // 11
            l.Images.Add(ElementType.Slope.ToString(),R.Element_Slope);       // 12
            //l.Images.Add(.ToString(),global::Tsunami.R.Element_T);       // 13
            l.Images.Add(ElementType.Tilt.ToString(),R.Element_Tilt);       // 14
            l.Images.Add(ElementType.Class.ToString(),R.Element_Aimant);       // 15
            l.Images.Add(ElementType.Numero.ToString(),R.Element_Aimant);
            l.Images.Add(ElementType.Accelerator.ToString(),R.Element_Zone);       // 16
            l.Images.Add(ElementType.Cumul.ToString(),R.Element_Cumul);       // 17
            //l.Images.Add(.ToString(),global::Tsunami.R.Element_File);       // 18
            l.Images.Add(ElementType.H.ToString(),R.Element_C);       // 19 H
            l.Images.Add(ElementType.SocketCode.ToString(),R.Element_Code);       // 20 socketcode
            l.Images.Add(ElementType.SocketType.ToString(),R.Element_Code);       // 21 socketype
            l.Images.Add(ElementType.Date.ToString(),R.Element_Date);       // 22 Date
            l.Images.Add(ElementType.SerialNumber.ToString(),R.Element_Serial);       // 23 serialnumber
            l.Images.Add(ElementType.Comment.ToString(),R.Comment);       // 24 comment
            l.Images.Add(ElementType.File.ToString(),R.Element_File);       // 25 File
            l.Images.Add(ElementType.SequenceTH.ToString(),R.Element_Sequence);       // 26
            l.Images.Add(ElementType.Line.ToString(),R.Line1);
            l.Images.Add(ElementType.Plane.ToString(),R.Plane);
            l.Images.Add(ElementType.Circle.ToString(),R.Circle);
            l.Images.Add(ElementType.Sphere.ToString(),R.Sphere);
            l.Images.Add(ElementType.Cylinder.ToString(),R.Cylinder);
            l.Images.Add(ElementType.Fit.ToString(),R.GeoFit);
            l.Images.Add(ElementType.Bearing.ToString(),R.Bearing);
            l.Images.Add(ElementType.TheoreticalFile.ToString(),R.Element_File);
            l.Images.Add(ElementType.Rotations.ToString(),R.Rotation);

            l.Images.Add(ElementType.Translations.ToString(),R.Translation);
            l.Images.Add(ElementType.Alesage.ToString(), R.Element_PointAlesage);

            l.Images.Add(StationProperties.Date.ToString(),R.Element_Date);
            l.Images.Add("Date",R.Element_Date);
            l.Images.Add(StationProperties.v0.ToString(),R.v0);       // 1
            l.Images.Add(StationProperties.Instrument.ToString(),R.Instrument);       // 2
            l.Images.Add(StationProperties.InstrumentHeight.ToString(),R.InstrumentHeight_Unknown);
            l.Images.Add(StationProperties.Operation.ToString(),R.Operation);
            l.Images.Add("Operation",R.Operation);
            l.Images.Add(StationProperties.Team.ToString(),R.Team);
            l.Images.Add("Team",R.Team);
            l.Images.Add(StationProperties.Admin.ToString(),R.Admin);
            l.Images.Add(StationProperties.Next.ToString(),R.Next);
            l.Images.Add(StationProperties.NextDefault.ToString(),R.Next);
            l.Images.Add("StationParameters",R.StationParameters);
            l.Images.Add("StationLevelAller",R.Level_Aller);
            l.Images.Add("StationLevelRetour",R.Level_Retour);
            l.Images.Add("StationLevelReprise0",R.Level_Reprise_0);
            l.Images.Add("StationLevelReprise1",R.Level_Reprise_1);
            l.Images.Add("StationLevelReprise2",R.Level_Reprise_2);
            l.Images.Add("StationLevelReprise3",R.Level_Reprise_3);
            l.Images.Add("StationLevelReprise4",R.Level_Reprise_4);
            l.Images.Add("StationLevelReprise5",R.Level_Reprise_5);
            l.Images.Add("StationLevelReprise6",R.Level_Reprise_6);
            l.Images.Add("StationLevelReprise7",R.Level_Reprise_7);
            l.Images.Add("StationLevelReprise8",R.Level_Reprise_8);
            l.Images.Add("StationLevelReprise9",R.Level_Reprise_9);
            l.Images.Add("StationLevelReprise10",R.Level_Reprise_10);
            l.Images.Add("Sigma", R.Sigma);
            l.Images.Add("PointParameters",R.Station_PointParameters);
            l.Images.Add(StationProperties.NextPoint.ToString(),R.Element_Point);
            l.Images.Add(StationProperties.Point.ToString(),R.Element_Point);
            l.Images.Add(StationProperties.Extension.ToString(),R.Rallonge);
            l.Images.Add(StationProperties.NextReflector.ToString(),R.Reflector);
            l.Images.Add(StationProperties.DefaultPointName.ToString(),R.Element_Point);
            l.Images.Add(StationProperties.DefaultPointCode.ToString(),R.Element_Code);
            l.Images.Add(StationProperties.NextComment.ToString(),R.Comment);
            l.Images.Add(StationProperties.Measures.ToString(),R.Measurement);
            l.Images.Add(StationProperties.Orientation.ToString(),R.Station_Orientated);
            l.Images.Add(StationProperties.Status.ToString(),R.Status);
            l.Images.Add(StationProperties.NextFace.ToString(),R.Face);

            l.Images.Add("Tolerances", R.Tolerances);
            l.Images.Add(StationProperties.Verticalisation.ToString(),R.Tilt);
            l.Images.Add(StationProperties.Interfaces.ToString(), R.Interfaces);
            l.Images.Add(StationProperties.NumberOfMeasurement.ToString(),R.I_Number);

            l.Images.Add(StationProperties.Setup.ToString(),R.Compute);
            l.Images.Add(I.InstrumentClasses.CHAINE.ToString(),R.Instrument);
            l.Images.Add(I.InstrumentClasses.DISTINVAR.ToString(),R.Instrument);
            l.Images.Add(I.InstrumentClasses.ECARTOMETRE.ToString(),R.Instrument);
            l.Images.Add(I.InstrumentClasses.EDM.ToString(),R.Instrument);
            l.Images.Add(I.InstrumentClasses.FIL.ToString(),R.Instrument);
            l.Images.Add(I.InstrumentClasses.GABARIT.ToString(),R.Instrument);
            l.Images.Add(I.InstrumentClasses.GYROSCOPE.ToString(),R.Instrument);
            l.Images.Add(I.InstrumentClasses.JAUGE_A_TILT.ToString(),R.Element_Tilt);
            l.Images.Add(I.InstrumentClasses.INTERFACE.ToString(), R.Element_Tilt);
            l.Images.Add(I.InstrumentClasses.LASER_TRACKER.ToString(),R.Theodolite);
            l.Images.Add(I.InstrumentClasses.MEKOMETRE.ToString(),R.Instrument);
            l.Images.Add(I.InstrumentClasses.MIRE.ToString(),R.Staff);
            l.Images.Add(I.InstrumentClasses.NIVEAU.ToString(),R.Level);
            l.Images.Add(I.InstrumentClasses.PRISME.ToString(),R.Reflector);
            l.Images.Add(I.InstrumentClasses.TACHEOMETRE.ToString(),R.Theodolite);
            l.Images.Add(I.InstrumentClasses.THEODOLITE.ToString(),R.Theodolite);
            l.Images.Add(I.InstrumentClasses.TILTMETRE.ToString(),R.Element_Tilt);
            l.Images.Add(I.InstrumentClasses.TRIPOD.ToString(),R.Instrument);
            l.Images.Add("Instruments",R.Instruments);
            l.Images.Add("Accessories",R.I_Accessories);
            l.Images.Add("Module",R.Module);
            l.Images.Add("Operation",R.Operation);
            l.Images.Add("SUSoft",R.Susoft);

            l.Images.Add("Dependencies",R.AdvancedModule);
            l.Images.Add("SingleApp",R.ManagerModule);
            l.Images.Add("ComposedApp",R.GuidedModule);

            l.Images.Add("Temperature", R.At40x_Temperature);
            l.Images.Add("Humidity", R.At40x_Humidity);
            l.Images.Add("Pressure", R.At40x_Pressure);

            l.Images.Add("Advanced",R.AdvancedModule);
            l.Images.Add("Guided",R.GuidedModule);
            l.Images.Add("Manager",R.ManagerModule);

            l.Images.Add("StatusGood", R.StatusGood);
            l.Images.Add("StatusQuestionable", R.StatusQuestionnable);
            l.Images.Add("StatusBad", R.StatusBad);

            l.Images.Add(MeasureProperties.Angles.ToString(),R.Angle);
            l.Images.Add(MeasureProperties.Comment.ToString(),R.Comment);
            l.Images.Add(MeasureProperties.Corrected.ToString(),R.Corrected);
            l.Images.Add(MeasureProperties.Distances.ToString(),R.Distance);
            l.Images.Add(MeasureProperties.DoubleFace.ToString(),R.Face);
            l.Images.Add(MeasureProperties.Face.ToString(),R.Face);
            l.Images.Add(MeasureProperties.Face1.ToString(),R.Face);
            l.Images.Add(MeasureProperties.Face2.ToString(),R.Face);
            l.Images.Add(MeasureProperties.MeasureState.ToString(),R.Status);
            l.Images.Add(MeasureProperties.Raw.ToString(),R.Raw);
            l.Images.Add(MeasureProperties.Station.ToString(),R.StationParameters);
            l.Images.Add(MeasureProperties.Theodolite.ToString(),R.Theodolite);
            l.Images.Add(MeasureProperties.Horizontal.ToString(),R.Horizontal);
            l.Images.Add(MeasureProperties.Vertical.ToString(),R.Vertical);
            l.Images.Add(MeasureProperties.NumberOfMeasureToAverage.ToString(),R.I_Number);
            l.Images.Add(MeasureProperties.Mode.ToString(),R.At40x_Precision_Fast);
            l.Images.Add(MeasureProperties.Reflector.ToString(),R.Reflector);
            l.Images.Add("LGC2CalculationOption",R.Lgc);
            l.Images.Add("SFBCalculationOption",R.Sfb);
            l.Images.Add("Checked",R.Checked_box);
            l.Images.Add("NotChecked",R.Unchecked_box);
            l.Images.Add("Tolerance",R.Tolerance);
            l.Images.Add("Ztype",R.Element_Z);
            l.Images.Add("CoordType",R.Element_Manager);
            l.Images.Add("Log",R.Log);
            l.Images.Add("DefaultFace",R.Face);
            l.Images.Add("TiltTheo",R.Angle);
            l.Images.Add(StationProperties.HStation.ToString(),R.Level_H);
            l.Images.Add(StationProperties.EmqStation.ToString(),R.Level_Emq);
            l.Images.Add(StationProperties.EmqAller.ToString(),R.Level_Emq_Aller);
            l.Images.Add(StationProperties.EmqRetour.ToString(),R.Level_Emq_Retour);
            //images pour tilt
            l.Images.Add(StationProperties.Average_Tilt.ToString(),R.Tilt_Angle);
            l.Images.Add(StationProperties.Emq_Tilt.ToString(),R.Tilt_Angle_Emq);
            l.Images.Add(StationProperties.Ecart_Tilt.ToString(),R.Tilt_Angle_Ecart);
            l.Images.Add(MeasureProperties.Beam_Direction.ToString(),R.Tilt_Angle_Beam);
            l.Images.Add(MeasureProperties.Opposite_Beam.ToString(),R.Tilt_Angle_Opposite);
            l.Images.Add(MeasureProperties.Average_Both_Direction.ToString(),R.Tilt_Angle_Both);
            l.Images.Add(StationProperties.Tilt_Tolerance.ToString(),R.Tilt_Angle_Max);
            l.Images.Add("TiltInitSaved",R.Tilt_Init_Saved);
            l.Images.Add("TiltBothSaved",R.Tilt_Both_Saved);
            l.Images.Add("TiltAdjSaved",R.Tilt_Adj_Saved);
            l.Images.Add("KnownPoint",R.Element_PointKnown);
            l.Images.Add("QuestionnablePoint",R.Element_PointUnKnown);
            l.Images.Add("ControlPoint", R.element_control);
            l.Images.Add("Run", R.Run);
            l.Images.Add("Reflector", R.Reflector);
            l.Images.Add("Face", R.Face);
            l.Images.Add("NbreMeas", R.I_Number);
            l.Images.Add("Temperature", R.At40x_Temperature);
            return l;
        }
    }
}
