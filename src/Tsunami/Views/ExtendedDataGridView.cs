﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace TSU.Views
{
    internal class ExtendedDataGridView : DataGridView
    {
        // *** API Declarations ***
        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int wMsg, bool wParam, int lParam);
        private const int WM_SETREDRAW = 11;

        public void SuspendRedraw()
        {
            SendMessage(Handle, WM_SETREDRAW, false, 0);
        }

        public void ResumeRedraw()
        {
            SendMessage(Handle, WM_SETREDRAW, true, 0);
            Refresh();
        }

        public ExtendedDataGridView() 
            : base()
        {
            DoubleBuffered = true;
        }
    }
}
