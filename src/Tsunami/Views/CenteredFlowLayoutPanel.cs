﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace TSU.Views
{
    internal class CenteredFlowLayoutPanel : FlowLayoutPanel
    {
        public CenteredFlowLayoutPanel()
            : base()
        {
        }

        protected override void OnLayout(LayoutEventArgs levent)
        {
            //Debug.WriteInConsole($"Begin OnLayout Size={this.Size},ClientRectangle={this.ClientRectangle},Margin={this.Margin},Padding={this.Padding}");

            base.OnLayout(levent);

            if (Controls.Count == 0)
            {
                //Debug.WriteInConsole($"OnLayout : No controls");
                return;
            }

            SuspendLayout();

            CheckWidthAndHeight(ClientSize.Width,
                                out int maxWidth,
                                out int maxHeight,
                                out int totalLines,
                                out int numberOfControlsOnLongLines);

            // Adjust the Height, only if different, to prevent infinite loop
            int newHeight = totalLines * (maxHeight + Margin.Top + Margin.Bottom) + Padding.Top + Padding.Bottom;
            //Debug.WriteInConsole($"OnLayout : newHeight={newHeight}");
            if (newHeight != this.Height)
            {
                //Debug.WriteInConsole($"OnLayout : Before Height change");
                this.Height = newHeight;
                //Debug.WriteInConsole($"OnLayout : After Height change");
            }

            // number of controls per line
            int count = numberOfControlsOnLongLines;

            // number of short and long lines 
            int shortLines = totalLines * count - Controls.Count;
            int fullLines = totalLines - shortLines;

            //Debug.WriteInConsole($"OnLayout : count={count}, shortLines={shortLines}, fullLines={fullLines}");

            // used portion, width is for for full lines
            Rectangle usedPortion = new Rectangle
            {
                Width = count * (maxWidth + Margin.Right + Margin.Left),
                Height = totalLines * (maxHeight + Margin.Top + Margin.Bottom)
            };
            //Debug.WriteInConsole($"OnLayout : ClientRectangle={ClientRectangle}, usedPortion={usedPortion}");
            usedPortion.X = (ClientRectangle.Width - usedPortion.Width) / 2;
            usedPortion.Y = (ClientRectangle.Height - usedPortion.Height) / 2;
            //Debug.WriteInConsole($"OnLayout : usedPortion={usedPortion}");

            int startIndex = 0;

            // place the controls on full lines
            for (int currentLine = 0; currentLine < fullLines; currentLine++)
            {
                PlaceControlsInLine(maxWidth, maxHeight, usedPortion, currentLine, startIndex, count);
                startIndex += count;
            }

            if (shortLines > 0)
            {
                // One control less per line
                count--;
                // update used portion for short lines (Height and Y don't change)
                usedPortion.Width = count * maxWidth;
                usedPortion.X = (ClientRectangle.Width - usedPortion.Width) / 2;

                // place the controls on short lines
                for (int currentLine = fullLines; currentLine < totalLines; currentLine++)
                {
                    PlaceControlsInLine(maxWidth, maxHeight, usedPortion, currentLine, startIndex, count);
                    startIndex += count;
                }
            }

            ResumeLayout();

            //System.Collections.IList list = Controls;
            //for (int i = 0; i < list.Count; i++)
            //{
            //Control control = (Control)list[i];
            //Debug.WriteInConsole($"Control {i} is at {control.Bounds}.");
            //}

            //Debug.WriteInConsole($"End OnLayout Size={this.Size},ClientRectangle={this.ClientRectangle},Margin={this.Margin},Padding={this.Padding}");
        }

        private void CheckWidthAndHeight(
            int maxWidthAllowed,
            out int widthOfAControl,
            out int heigthOfAControl,
            out int totalLines,
            out int numberOfControlsOnLongLines)
        {
            widthOfAControl = 0;
            heigthOfAControl = 0;

            // get the min and max Width and Height
            foreach (Control c in Controls)
            {
                if (c.Width > widthOfAControl) widthOfAControl = c.Width;
                if (c.Height > heigthOfAControl) heigthOfAControl = c.Height;
            }

            //Debug.WriteInConsole($"OnLayout : maxWidth={maxWidth}, maxHeight={maxHeight}");

            int totalWidth = Controls.Count * (widthOfAControl + Margin.Left + Margin.Right);

            totalLines = (int)Math.Ceiling((double)totalWidth / maxWidthAllowed);

            numberOfControlsOnLongLines = (int)Math.Ceiling((double)Controls.Count / totalLines);
        }

        internal int GetOptimumWidth(int maxWidth)
        {
            CheckWidthAndHeight(maxWidth,
                                out int widhtOfAControl,
                                out _,
                                out _,
                                out int numberOfControlsOnLongLines);

            return numberOfControlsOnLongLines * (widhtOfAControl + Margin.Left + Margin.Right) + Padding.Left + Padding.Right;
        }

        private void PlaceControlsInLine(int width, int height, Rectangle usedPortion, int currentLine, int startIndex, int count)
        {
            for (int currentColumn = 0; currentColumn < count; currentColumn++)
            {
                Rectangle controlRectangle = new Rectangle()
                {
                    Width = width,
                    Height = height
                };
                controlRectangle.X = usedPortion.X + Margin.Right + currentColumn * (width + Margin.Right + Margin.Left);
                controlRectangle.Y = usedPortion.Y + Margin.Bottom + currentLine * (height + Margin.Bottom + Margin.Top);
                int controlIndex = startIndex + currentColumn;
                //Debug.WriteInConsole($"Placing Control {controlIndex} at {controlRectangle}.");
                Controls[controlIndex].Bounds = controlRectangle;
            }
        }
    }
}
