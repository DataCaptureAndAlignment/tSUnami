﻿using System;
using System.Windows.Forms;
using TSU;

class TsuListView : ListView
{
    //public bool IsUpdating = true;
    public event ScrollEventHandler Scroll;
    protected virtual void OnScroll(ScrollEventArgs e)
    {
        ScrollEventHandler handler = this.Scroll;
        if (handler != null) handler(this, e);
    }

    public TsuListView()
    {
        Dock = DockStyle.Fill;
        Font = Tsunami2.Preferences.Theme.Fonts.Normal;
        BackColor = Tsunami2.Preferences.Theme.Colors.LightBackground;
    }
}
