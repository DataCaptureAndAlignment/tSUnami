﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using P = TSU.Tsunami2.Preferences;

namespace TSU.Views.Message
{
    /// <summary>
    /// This class is used to give the settings for a mesage
    /// Call the Show method after setting the properties to show the message, it returns a MessageResult
    /// If you have set the Controls property with controls you created, you should put a using block around the messageResult, to dispose the returned controls.
    /// </summary>
    public class MessageInput
    {
        public MessageInput(MessageType type, string titleAndMessage)
        {
            Type = type;
            TitleAndMessage = titleAndMessage;
            ButtonTexts = null;
            Controls = null;
            DontShowAgain = null;
            AsNotification = true;
            TextOfButtonToBeLocked = "";
            Sender = "";
            UseMonoSpacedFont = false;
            MinWidth = -1;
        }

        public MessageType Type { get; set; }
        public string TitleAndMessage { get; set; }
        public List<string> ButtonTexts { get; set; }
        public List<Control> Controls { get; set; }
        public DsaFlag DontShowAgain { get; set; }
        public bool AsNotification { get; set; }
        public string TextOfButtonToBeLocked { get; set; }
        public string Sender { get; set; }
        public bool UseMonoSpacedFont { get; set; }
        public int MinWidth { get; set; }
        public EventHandler Shown { get; set; }
        public KeyPressEventHandler On_KeyPressed { get; set; }
        public Color Color { get; set; } = Color.Transparent;

        public MessageResult Show()
        {
            // Remove empty buttons and add an OK button if no button is set
            ButtonTexts = MessageTsu.CheckAndFilterButtons(ButtonTexts);

            if (MessageTsu.CancelBasedOnDsaFlag(DontShowAgain, ButtonTexts, out string buttonTextToReturn))
                return new MessageResult { TextOfButtonClicked = buttonTextToReturn };

            MessageResult r = null;

            P.Values.InvokeOnApplicationDispatcher(() =>
            {
                TsuView parentView = Tsunami2.Properties == null ? null : Tsunami2.View;
                MessageTsu messageTsu = new MessageTsu(parentView, Type, TitleAndMessage, ButtonTexts, Controls,
                                                       DontShowAgain, false, UseNotification(), TextOfButtonToBeLocked,
                                                       Sender, UseMonoSpacedFont, MinWidth, DockStyle.Bottom, Color);
                // When the message is shown, also call the input's shown event
                messageTsu.Shown += this.Shown;
                this.On_KeyPressed += messageTsu.On_KeyPressed;
                r = messageTsu.Show2();
            });

            return r;
        }

        private bool UseNotification()
        {
            // Never use notification if we have a DontShowAgain to ask
            if (DontShowAgain != null && (DontShowAgain.State == DsaOptions.Ask_to || DontShowAgain.State == DsaOptions.Ask_to_with_pre_checked_dont_show_again))
                return false;

            // Never use notification if we have a Control to show
            if (Controls != null && Controls.Count > 0)
                return false;

            // If we have different buttons, the user must chose one
            if (ButtonTexts.Count > 1)
                return false;

            // The usage of OK! is the old way to avoid notifications
            if (ButtonTexts.Count == 1 && ButtonTexts[0] == "OK!")
                return false;

            // If we have one button only, use the setting
            return AsNotification;
        }


    }
}