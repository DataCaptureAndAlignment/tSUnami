﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Drawing;
using System.Threading.Tasks;
using P = TSU.Tsunami2.Preferences;
using System.Windows.Forms;
using TSU.Common.Dependencies;


namespace TSU.Views.Message
{


    public class ProgressMessage
    {
        public enum StepTypes
        {
            Begin, End, EndAll
        }
        public event EventHandler Finished;

        internal MessageTsu MessageView;
        internal TsuView CoveredView;
        Progress progress;
        string title;
        public string respond;
        internal int currentStepValue;
        internal int stepsCount;
        internal bool IsCancelled;

        internal void Dispose()
        {
            TSU.Common.TsunamiView.ExistingMessages.Remove(MessageView);
            if (this.MessageView != null)
            {
                MessageView.Hide();
                MessageView.Dispose();
            }
            this.MessageView = null;


            this.CoveredView = null;

            if (this.progress != null)
                this.progress.Dispose();

            this.progress = null;
            Finished = null;

        }


        public bool Visible
        {
            get
            {
                if (this.MessageView == null)
                    return false;
                return this.MessageView.Visible;
            }
        }

        public ProgressMessage()
        {

        }
        public ProgressMessage(TsuView TsuViewToCover, string title, string startingMessage, int stepsCount, int totalEstimatedTimeMs, bool ShowCancelButton=true, Action cancelAction=null, string textButton ="")
        {
            Init(TsuViewToCover, title, startingMessage, stepsCount, totalEstimatedTimeMs, ShowCancelButton, cancelAction, textButton);
        }

        private void Init(TsuView TsuViewToCover, string title, string startingMessage, int stepsCount, int totalEstimatedTimeMs, bool ShowCancelButton = true, Action cancelAction = null, string textButton = "")
            {
            Action a = delegate
            {
                // Setup values
                this.currentStepValue = 1;
                this.stepsCount = stepsCount;
                this.CoveredView = TsuViewToCover;
                this.title = title;

                // Create messageView
                MessageView = new MessageTsu()
                {
                    ParentView = TsuViewToCover,
                    Text = "Progress...",
                };
                MessageView.Colorize(P.Theme.Colors.Object, P.Theme.Colors.TextOnDarkBackGround);
                MessageView.coveringMessage = true;
                MessageView.CoverParentView(0);
                MessageView.FillTexts("P", title + ";" + startingMessage);

                // Create and add Bar
                progress = new Progress(this, totalEstimatedTimeMs);
                MessageView.panel1.Controls.Add(progress.bar);
                MessageView.panel1.Visible = true;

                // Add Cancel Button
                MessageView.button2.Text = (textButton == "") ? R.T_CANCEL : textButton;
                MessageView.button2.Visible = true;
                MessageView.button2.Click += OnCancel;
                if (cancelAction != null) MessageView.button2.Click += delegate
                {
                    cancelAction();
                };

                // rename cancel by close if cancel is not possible
                if (!ShowCancelButton)
                {
                    if (MessageView.button2.Text == R.T_CANCEL)
                        MessageView.button2.Text = R.T_CLOSE;
                }

                Show();
            };
            P.Values.InvokeOnApplicationDispatcher(a);

        }

        public Action CancelAction = null;

        private void OnCancel(object sender, EventArgs e)
        {
            //EndAll();
            this.CoveredView.CancelAction();
            if (this.CancelAction !=null) this.CancelAction();
            this.IsCancelled = true;
        }

        public void Show()
        {
            
            if (MessageView == null)
                return;
            if (CoveredView != null)
            {
                MessageView.TopLevel = false;
                CoveredView.Controls.Add(MessageView);
            }
            else
            {
                MessageView.TopLevel = true ;
            }
            MessageView.BringToFront();
            MessageView.Show();
            CoveredView.Refresh();
        }

        public void Stop()
        {
            OnFinished(this, null);
        }

        bool isPaused = false; 
        public void Pause()
        {
            isPaused = true;
        }



        private void OnFinished(object sender, EventArgs e)
        {
            if (this.progress == null)
                return;
            this.Finish();

            this.progress.timer.Stop();
            this.MessageView.Hide();

            //this.Dispose();
        }

        internal void Finish()
        {
            if (this.Finished != null) Finished(this, new EventArgs());
        }

        /// <summary>
        /// Update the waiting view with a new text, specified a number of step to advance, reminder: SetupProgressBar() should be step before using ShowOnTopOfTheView(), the windows will hide when the number of step is reached
        /// </summary>
        internal void BeginAstep(string NewDescription = "in progress ...")
        {
            if (this.MessageView == null || this.isPaused)
                return;
            // Checks
            if (this.MessageView.Visible == false) this.Show();

            // Change description
            UpdateText(NewDescription);

            // Progress
            this.progress.BeginAStep();
            this.MessageView.Refresh();
        }

        internal void EndCurrentStep(string NewDescription = "... Done")
        {

            // Checks
            if (currentStepValue >= stepsCount || (this.MessageView!=null && this.MessageView.IsDisposed))
            {
                EndAll();
                return;
            }

            // Move one step
            currentStepValue += 1;

            // Change description
            UpdateText(NewDescription);

            // Progress
            if(this.progress!=null)
                this.progress.EndCurrentStep();
            
        }

        private void UpdateText(string newDescription)
        {
            if (this.MessageView == null) return;
            string text="";
            if (this.stepsCount>1) text+= "(Step " + (this.currentStepValue).ToString() + "/" + this.stepsCount.ToString() + "): ";
            text += newDescription;
            MessageView.FillTexts("P", title + ";" + text, this.MessageView.button1.Text, MessageView.button2.Text, MessageView.button3.Text);
        }


        public void EndAll()
        {
            if (MessageView!=null)
                respond = MessageView.button2.Text;
            if (progress!=null)
                progress.EndAll();
            TSU.Tsunami2.Preferences.Values.InvokeOnApplicationDispatcher(this.Dispose);
        }

        internal void Show(TSU.Module initiator, TSU.Views.TsuView coverview=null, string name="Unknown process", int totalEstimatedTimeInMilliSeconds= 1000)
        {
            this.EndAll();

            this.Init(coverview,
                name,
                "Starting " + name,
                1,
                totalEstimatedTimeInMilliSeconds
                );
            initiator.MessageBroadcast += this.OnMessageBroadcast;
            this.BeginAstep(name);
        }

        private void OnMessageBroadcast(object sender, Module.MessageEventArgs e)
        {
            string s = "";
            if (e.Module != null)
                s += e.Module._Name + ": ";
            s += e.Message;
            if (this.currentStepValue < this.stepsCount)
                this.currentStepValue++;
            this.currentStepValue++;
            this.BeginAstep(s);
        }

        internal void Suspend()
        {
            this.progress.Suspend();
        }

        internal void Resume()
        {
            isPaused = false;
            this.progress?.Resume();
        }

    }

    internal class Progress
    {
        public ProgressMessage progressMessage;
        internal ProgressBar bar;
        internal int CurrentMaxValue;
        internal Timer timer;
        const int FACTOR = 20;

        internal void Dispose()
        {
            progressMessage = null;
            if (bar != null)
                bar.Dispose();
            bar = null;

            if (timer != null)
                timer.Dispose();
            timer = null;
        }

        internal Progress(ProgressMessage progressMessage, int totalEstimatedTimeMs = 1000)
        {
            this.progressMessage = progressMessage;

            // Setup bar
            bar = new ProgressBar()
            {
                Step = 1,
                Value = 1,
                Maximum = progressMessage.stepsCount * FACTOR,
                Visible = true,
                Dock = DockStyle.Fill
            };

            // Start timer that will show progresses
            timer = new Timer()
            {
                Interval = totalEstimatedTimeMs / (FACTOR * progressMessage.stepsCount),
                Enabled = true
            };
            timer.Tick += delegate
            {
                if (bar != null && bar.Value < CurrentMaxValue - 1)
                {
                    bar.PerformStep();
                    this.progressMessage.MessageView.Refresh();
                }
                else
                {
                    if (timer !=null) 
                    {
                        timer.Stop();
                        timer.Dispose();
                    }
                    
                }
            };
        }

        internal void BeginAStep()
        {
            CurrentMaxValue = progressMessage.currentStepValue * FACTOR;
            timer.Enabled = true;
        }

        internal void EndCurrentStep()
        {
            bar.Value = CurrentMaxValue;
            if (progressMessage.currentStepValue >= progressMessage.stepsCount)
            {
                timer.Stop();
                timer.Dispose();
            }
        }

        internal void EndAll()
        {
            timer.Stop();
            timer.Dispose();
        }

        internal void Suspend()
        {
            this.timer.Stop();
        }

        internal void Resume()
        {
            this.timer.Start();
        }
    }


    public class ProgressMessageState
    {
        public string Status;
        public ProgressMessage.StepTypes Type;
        public ProgressMessageState(string Status, ProgressMessage.StepTypes Type)
        {
            this.Status = Status;
            this.Type = Type;
        }
    }
}
