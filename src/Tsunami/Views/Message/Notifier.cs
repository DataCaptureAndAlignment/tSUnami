﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TSU.Tools;

namespace TSU.Views.Message
{
    internal class Notifier
    {
        const int DELAY_MOVING = 2000;
        const int HIDING_FREQUENCY = 20;
        const int SPACE_BETWEEN_MESSAGES = 5;

        public bool mouseIsIn = false;

        List<(MessageTsu, DateTime)> messagesWithCreationTime = new List<(MessageTsu message, DateTime dateTime)>();

        System.Windows.Forms.Timer t = new System.Windows.Forms.Timer();

        public DateTime MouseDownTime { get; internal set; }

        public Notifier()
        {
            bool restored = false;
            t.Interval = DELAY_MOVING;
            t.Enabled = false;
            t.Tick += delegate
            {
                t.Interval = HIDING_FREQUENCY;
                if (mouseIsIn)
                {
                    if (!restored)
                    {
                        RestorePositions();
                        t.Interval = DELAY_MOVING;
                        restored = true;
                    }
                    return;
                }
                restored = false;

                if (!TSU.Common.TsunamiView.IsShownAsModalInPlay)
                {
                    if (Tsunami2.Preferences.Values.GuiPrefs.NotificationLeavingOnTheLeftSide.IsTrue)
                        MoveLeft(pixel: 4);
                    else
                        MoveDown(pixel: 4);
                }
            };
        }

        private int GetScreenHeight()
        {
            var tsunamiView = TSU.Tsunami2.Preferences.Tsunami.View;
            return System.Windows.Forms.Screen.FromControl(tsunamiView).Bounds.Height;
        }
        private int GetWorkingAreaHeight()
        {
            var tsunamiView = TSU.Tsunami2.Preferences.Tsunami.View;
            return System.Windows.Forms.Screen.FromControl(tsunamiView).WorkingArea.Height;
        }

        private void MoveDown(int pixel)
        {
            for (int i = 0; i < messagesWithCreationTime.Count; i++)
            {
                (MessageTsu, DateTime) md = messagesWithCreationTime[i];

                var message = md.Item1 as MessageTsu;
                DateTime previousDate = md.Item2;

                // do not show above other apps
                if (!Macro.ApplicationIsActivated())
                {
                    messagesWithCreationTime[i] = (message, DateTime.Now);
                }
                else
                {
                    TimeSpan difference = DateTime.Now - previousDate;

                    if (difference.TotalMilliseconds < DELAY_MOVING)
                        return;

                    var m = md.Item1 as MessageTsu;
                    m.Top += pixel;

                    if (m.Top > GetScreenHeight())
                    {
                        RemoveMessage(m); i--;
                        m.Dispose();
                    }
                }
            }
            if (messagesWithCreationTime.Count == 0)
                t.Stop();
        }
        private void MoveLeft(int pixel)
        {
            for (int i = 0; i < messagesWithCreationTime.Count; i++)
            {
                (MessageTsu, DateTime) md = messagesWithCreationTime[i];

                var message = md.Item1 as MessageTsu;
                DateTime previousDate = md.Item2;

                // do not show above other apps
                if (!Macro.ApplicationIsActivated())
                {
                    messagesWithCreationTime[i] = (message, DateTime.Now);
                }
                else
                {
                    TimeSpan difference = DateTime.Now - previousDate;

                    if (difference.TotalMilliseconds < DELAY_MOVING)
                        return;

                    message.Left -= pixel;

                    if (message.Left < -(message.Width + 200))
                    {
                        RemoveMessage(message); i--;
                        message.Dispose();
                    }
                }
            }
            if (messagesWithCreationTime.Count == 0)
                t.Stop();
        }

        private void InitialisePosition(MessageTsu message)
        {
            int bottomPosition = GetWorkingAreaHeight();
            int leftPosition = SPACE_BETWEEN_MESSAGES;

            foreach ((MessageTsu, DateTime) md in messagesWithCreationTime)
            {
                var m = md.Item1 as MessageTsu;
                bottomPosition = bottomPosition - SPACE_BETWEEN_MESSAGES - m.Height;
            }

            message.Top = bottomPosition;
            message.Left = leftPosition;
        }

        private void RestorePositions()
        {
            int bottomPosition = GetWorkingAreaHeight();
            int leftPosition = SPACE_BETWEEN_MESSAGES;

            for (int i = 0; i < messagesWithCreationTime.Count; i++)
            {
                var m = messagesWithCreationTime[i].Item1 as MessageTsu;

                int topPosition = bottomPosition - SPACE_BETWEEN_MESSAGES - m.Height;
                bottomPosition = topPosition;
                if (Tsunami2.Preferences.Values.GuiPrefs.NotificationLeavingOnTheLeftSide.IsTrue)
                    m.Left = leftPosition;
                else
                    m.Top = topPosition;
                messagesWithCreationTime[i] = (m, DateTime.Now);
            }
        }

        public void AddMessage(MessageTsu message)
        {
            messagesWithCreationTime.Add((message, DateTime.Now));
            message.Owner = (Tsunami2.View);
            message.TopMost = false;
            InitialisePosition(message);
            t.Interval = DELAY_MOVING;
            t.Start();

        }
        public void RemoveMessage(MessageTsu message)
        {
            var m = messagesWithCreationTime.FirstOrDefault(x => x.Item1 == message);
            messagesWithCreationTime.Remove(m);
            CheckForRearrangement(pixel: 15);
        }

        private void CheckForRearrangement(int pixel)
        {
            if (mouseIsIn)
            {
                for (int i = 0; i < messagesWithCreationTime.Count; i++)
                {
                    messagesWithCreationTime[i] = (messagesWithCreationTime[i].Item1, DateTime.Now);
                }
            }

            System.Windows.Forms.Timer rearrangementTimer = new System.Windows.Forms.Timer();

            rearrangementTimer.Interval = HIDING_FREQUENCY;
            rearrangementTimer.Enabled = true;
            rearrangementTimer.Tick += delegate
            {
                bool oneMoved = false;
                for (int i = 0; i < messagesWithCreationTime.Count; i++)
                {
                    (MessageTsu, DateTime) md = messagesWithCreationTime[i];
                    MessageTsu message = md.Item1;
                    DateTime previousDate = md.Item2;

                    bool shouldMoveDown = false;

                    int previousNotificationIndex = i - 1;

                    int messageBottom = message.Top + message.Height + SPACE_BETWEEN_MESSAGES;

                    if (previousNotificationIndex == -1) // no previous
                        shouldMoveDown = messageBottom < GetWorkingAreaHeight(); // not at the bottom yet?
                    else
                        shouldMoveDown = messageBottom < messagesWithCreationTime[previousNotificationIndex].Item1.Top; // not at the top of the previous yet?

                    if (shouldMoveDown)
                    {
                        oneMoved = true;
                        message.Top += pixel;
                    }
                }

                if (!oneMoved)
                {
                    rearrangementTimer.Stop();
                    rearrangementTimer.Dispose();
                }
            };
        }
    }
}
