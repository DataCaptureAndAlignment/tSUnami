﻿using R = TSU.Properties.Resources;
namespace TSU.Views
{
    partial class MesssageMoveLevelDatagrid
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonLeft = new System.Windows.Forms.Button();
            this.titleAndMessageRichBox = new System.Windows.Forms.RichTextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridViewMoveLevel = new System.Windows.Forms.DataGridView();
            this.PointName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dcum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TheoReading = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Move = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMoveLevel)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonLeft
            // 
            this.buttonLeft.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonLeft.BackColor = System.Drawing.Color.LightGray;
            this.buttonLeft.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.buttonLeft.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLeft.Location = new System.Drawing.Point(185, 7);
            this.buttonLeft.MaximumSize = new System.Drawing.Size(140, 0);
            this.buttonLeft.MinimumSize = new System.Drawing.Size(140, 60);
            this.buttonLeft.Name = "buttonLeft";
            this.buttonLeft.Size = new System.Drawing.Size(140, 60);
            this.buttonLeft.TabIndex = 8;
            this.buttonLeft.Text = "buttonLeft";
            this.buttonLeft.UseVisualStyleBackColor = false;
            this.buttonLeft.Click += new System.EventHandler(this.On_buttonClick);
            this.buttonLeft.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonLeft_KeyPress);
            // 
            // titleAndMessageRichBox
            // 
            this.titleAndMessageRichBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.titleAndMessageRichBox.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.titleAndMessageRichBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.titleAndMessageRichBox.Location = new System.Drawing.Point(3, 3);
            this.titleAndMessageRichBox.Name = "titleAndMessageRichBox";
            this.titleAndMessageRichBox.ReadOnly = true;
            this.titleAndMessageRichBox.Size = new System.Drawing.Size(511, 94);
            this.titleAndMessageRichBox.TabIndex = 25;
            this.titleAndMessageRichBox.TabStop = false;
            this.titleAndMessageRichBox.Text = "Title\n";
            this.titleAndMessageRichBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.titleAndMessageRichBox_KeyPress);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(8, 8);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(517, 666);
            this.panel2.TabIndex = 27;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.dataGridViewMoveLevel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.titleAndMessageRichBox, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(517, 666);
            this.tableLayoutPanel1.TabIndex = 26;
            // 
            // dataGridViewMoveLevel
            // 
            this.dataGridViewMoveLevel.AllowUserToAddRows = false;
            this.dataGridViewMoveLevel.AllowUserToDeleteRows = false;
            this.dataGridViewMoveLevel.AllowUserToResizeColumns = false;
            this.dataGridViewMoveLevel.AllowUserToResizeRows = false;
            this.dataGridViewMoveLevel.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewMoveLevel.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewMoveLevel.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewMoveLevel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMoveLevel.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PointName,
            this.Dcum,
            this.TheoReading,
            this.Move});
            this.dataGridViewMoveLevel.Cursor = System.Windows.Forms.Cursors.Arrow;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewMoveLevel.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewMoveLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewMoveLevel.EnableHeadersVisualStyles = false;
            this.dataGridViewMoveLevel.Location = new System.Drawing.Point(3, 103);
            this.dataGridViewMoveLevel.MultiSelect = false;
            this.dataGridViewMoveLevel.Name = "dataGridViewMoveLevel";
            this.dataGridViewMoveLevel.ReadOnly = true;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewMoveLevel.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewMoveLevel.RowHeadersVisible = false;
            this.dataGridViewMoveLevel.Size = new System.Drawing.Size(511, 480);
            this.dataGridViewMoveLevel.TabIndex = 0;
            this.dataGridViewMoveLevel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dataGridViewMoveLevel_KeyPress);
            this.dataGridViewMoveLevel.MouseEnter += new System.EventHandler(this.dataGridViewMoveLevel_MouseEnter);
            // 
            // PointName
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.PointName.DefaultCellStyle = dataGridViewCellStyle9;
            this.PointName.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Name;
            this.PointName.MinimumWidth = 200;
            this.PointName.Name = "PointName";
            this.PointName.ReadOnly = true;
            this.PointName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Dcum
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Dcum.DefaultCellStyle = dataGridViewCellStyle10;
            this.Dcum.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Dcum;
            this.Dcum.Name = "Dcum";
            this.Dcum.ReadOnly = true;
            // 
            // TheoReading
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TheoReading.DefaultCellStyle = dataGridViewCellStyle11;
            this.TheoReading.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Theo;
            this.TheoReading.Name = "TheoReading";
            this.TheoReading.ReadOnly = true;
            this.TheoReading.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Move
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            this.Move.DefaultCellStyle = dataGridViewCellStyle12;
            this.Move.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Move;
            this.Move.Name = "Move";
            this.Move.ReadOnly = true;
            this.Move.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.62622F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.37378F));
            this.tableLayoutPanel2.Controls.Add(this.buttonLeft, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 589);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(511, 74);
            this.tableLayoutPanel2.TabIndex = 26;
            // 
            // MesssageMoveLevelDatagrid
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(533, 682);
            this.Controls.Add(this.panel2);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MinimumSize = new System.Drawing.Size(450, 200);
            this.Name = "MesssageMoveLevelDatagrid";
            this.Padding = new System.Windows.Forms.Padding(8);
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Message...";
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMoveLevel)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button buttonLeft;
        private System.Windows.Forms.RichTextBox titleAndMessageRichBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView dataGridViewMoveLevel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PointName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dcum;
        private System.Windows.Forms.DataGridViewTextBoxColumn TheoReading;
        private System.Windows.Forms.DataGridViewTextBoxColumn Move;
    }
}