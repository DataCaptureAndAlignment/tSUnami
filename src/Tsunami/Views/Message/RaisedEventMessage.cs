﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using M = TSU;
using TSU.Common.Guided;
using TSU.Common.Guided.Group;

namespace TSU.Views.Message
{
    class RaisedEventMessage: MessageTsu
    {
        M. Module module;
        public RaisedEventMessage()
        {
            button3.Visible = false;

            button1.Visible = false;

            button2.Visible = false;
            
            ShowInTaskbar = true;// becaue canot be showdilaog
        }

        public void OnGuidedModuleEvent(object source, GuidedModuleEventArgs e)
        {
            module = e.step.GuidedModule;
            
            this.AddToLabel(e.step._Name, number: 3);
            this.Refresh();
        }

        internal void OnSubModuleAdded(object source, Common.ModuleEventArgs e)
        {
            this.AddToLabel(e.Module._Name, number :2);
            this.Refresh();
        }

        internal void OnSubModuleAdding(object source, Common.ModuleEventArgs e)
        {
            this.AddToLabel(e.Module._Name, number: 2);
            this.Refresh();
        }

        internal void OnModuleStarted(object source, Common.ModuleEventArgs args)
        {

            this.Dispose();
        }

        internal void OnGuidedModuleAddedEvent(object source, GroupGuidedModuleEventArgs e)
        {
            this.AddToLabel(e.GuidedModule._Name, number: 3);
            this.Refresh();
        }

        internal void OnActiveGuidedModuleChanged(object source, GroupGuidedModuleEventArgs args)
        {
            this.Dispose();
        }
    }
}
