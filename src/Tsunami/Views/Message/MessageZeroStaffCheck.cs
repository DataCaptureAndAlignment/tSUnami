﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using R = TSU.Properties.Resources;
using System.Windows.Forms;
using I = TSU.Common.Instruments;
using T = TSU.Tools;
using TSU.Common;
using TSU.Views.Message;

namespace TSU.Views
{
    public partial class MessageZeroStaffCheck : TsuView
    {
        private string _respond;
        public string Respond { get { return _respond; }   }
        public string CancelString { get; set; }
        public List<I.LevelingStaff> oldListStaff;
        public List<I.LevelingStaff> newListStaff;
        private double averageReading;
        internal int rowActiveCell = 0;
        internal int columnActiveCell = 1;
        internal bool setCurrentCell = false;
        private double na { get; set; } =TSU.Tsunami2.Preferences.Values.na;
        public I.Manager.Module staffModule { get; set; }
        public MessageZeroStaffCheck()
        {
            this.oldListStaff = new List<I.LevelingStaff>();
            this.newListStaff = new List<I.LevelingStaff>();
            averageReading = na;
            this.InitializeComponent();
            this.ApplyThemeColors();
            this.Resizable = true;
            this.Location = new System.Drawing.Point((TSU.Tsunami2.Preferences.Values.WorkingArea.Width - this.Width) / 2, (TSU.Tsunami2.Preferences.Values.WorkingArea.Height - this.Height )/ 2);
            this.AllowMovement(this);
        }
        private void ApplyThemeColors()
        {
            this.buttonLeft.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.buttonRight.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.dataGridViewZeroStaff.ColumnHeadersDefaultCellStyle.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.dataGridViewZeroStaff.ColumnHeadersDefaultCellStyle.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.dataGridViewZeroStaff.ColumnHeadersDefaultCellStyle.SelectionBackColor =TSU.Tsunami2.Preferences.Theme.Colors.TextHighLightBack;
            this.dataGridViewZeroStaff.ColumnHeadersDefaultCellStyle.SelectionForeColor =TSU.Tsunami2.Preferences.Theme.Colors.TextHighLightFore;
            this.dataGridViewZeroStaff.DefaultCellStyle.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.LightBackground;
            this.dataGridViewZeroStaff.DefaultCellStyle.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            this.dataGridViewZeroStaff.DefaultCellStyle.SelectionBackColor =TSU.Tsunami2.Preferences.Theme.Colors.TextHighLightBack;
            this.dataGridViewZeroStaff.DefaultCellStyle.SelectionForeColor =TSU.Tsunami2.Preferences.Theme.Colors.TextHighLightFore;
            this.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            this.dataGridViewZeroStaff.BackgroundColor =TSU.Tsunami2.Preferences.Theme.Colors.Background;
            this.Correction.HeaderText = R.StringMsgZeroStaff_Correction;
            this.Reading.HeaderText = R.StringMsgZeroStaff_Reading;
            this.Staff.HeaderText = R.StringMsgZeroStaff_Staff;
        }


        // Tools
        public void Colorize( Color back, Color fore)
        {
            this.BackColor = back;
            this.ForeColor = fore;
            this.titleAndMessageRichBox.BackColor = back;
            
            Color darkerColor = Common.Analysis.Colors.GetDarkerColor(BackColor);
            Color lighterColor = Common.Analysis.Colors.GetLighterColor(BackColor);
            this.BackColor = darkerColor;
            this.panel2.BackColor = back;
            this.titleAndMessageRichBox.BackColor = back;
            this.buttonRight.BackColor = darkerColor;
            this.buttonLeft.BackColor = darkerColor;
            this.titleAndMessageRichBox.ForeColor = fore;
            this.buttonRight.ForeColor = fore;
            this.buttonLeft.ForeColor = fore;
        }



        public void FillTitleAndAdjustSize(string title = "", string left = "", string right = "")
        {
            SetTitle(title);
            this.buttonRight.Text = right;
            this.buttonLeft.Text = left;
            this.buttonRight.Visible = (this.buttonRight.Text == "") ? false : true;
            this.buttonLeft.Visible = (this.buttonLeft.Text == "") ? false : true;
            this.CreateToolTip();
        }
        /// <summary>
        /// Cree les tooltips pour tous les boutons
        /// </summary>
        private void CreateToolTip()
        {
            ToolTip tooltip = new ToolTip();
            tooltip.InitialDelay = 50;
            tooltip.AutoPopDelay = 5000;
            tooltip.ReshowDelay = 50;
            if (titleAndMessageRichBox != null) tooltip.SetToolTip(this.titleAndMessageRichBox, this.titleAndMessageRichBox.Text);
            tooltip.SetToolTip(this, this.Text);
            if (buttonLeft != null) tooltip.SetToolTip(this.buttonLeft, this.buttonLeft.Text);
            if (buttonRight != null) tooltip.SetToolTip(this.buttonRight, this.buttonRight.Text);
        }


        public void SetTitle(string title)
        {
            this.titleAndMessageRichBox.Text = "";
            Rectangle screenSize =Tsunami2.Properties.View.Bounds;
            this.titleAndMessageRichBox.Text = title;
            this.titleAndMessageRichBox.SelectAll();
            Font titleFont =TSU.Tsunami2.Preferences.Theme.Fonts.Large;
            this.titleAndMessageRichBox.SelectionFont = titleFont;
            this.titleAndMessageRichBox.SelectionAlignment = HorizontalAlignment.Center;
            this.titleAndMessageRichBox.SelectionLength = 0;
            this.Top = (screenSize.Height - this.Height) / 2;
            this.Left = (screenSize.Width - this.Width) / 2;
            this.AdjustToScreenSize();
        }
        // Return
        internal void SetRespond(Button button)
        {
            _respond = button.Text;
        }

        protected virtual void On_buttonClick(object sender, EventArgs e)
        {
            Button button = ((Button)sender);
            SetRespond(button);
            if (button == buttonLeft)
            {
                this.CheckDatagridIsInEditMode();
                this.TryAction(this.UpdateOldListStaff);
            }
            this.Close();
        }
        /// <summary>
        /// Check if datagrid is in edit mode and validate it before leaving
        /// </summary>
        internal void CheckDatagridIsInEditMode()
        {
            if (dataGridViewZeroStaff.IsCurrentCellInEditMode)
            {
                //this.dataGridViewLevel.CellValueChanged -= this.dataGridViewLevel_CellValueChanged;
                int saveRow = rowActiveCell;
                int saveColumn = columnActiveCell;
                //Try car de temps en temps endEdit peut faire une exception
                try
                {
                    dataGridViewZeroStaff.EndEdit();

                }
                catch (Exception ex)
                {
                    ex.Data.Clear();
                }
                //this.dataGridViewLevel.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewLevel_CellValueChanged);
                rowActiveCell = saveRow;
                columnActiveCell = saveColumn;
            }
        }
        /// <summary>
        /// Met à jour la liste des staff dans old list staff
        /// </summary>
        private void UpdateOldListStaff()
        {
            foreach (I.LevelingStaff item in this.newListStaff)
            {
                int index = this.oldListStaff.FindIndex(x => x._Name == item._Name);
                if (index != -1)
                {
                    this.oldListStaff[index]._zeroCorrection = item._zeroCorrection;
                    this.oldListStaff[index]._zeroCorrectionReading = item._zeroCorrectionReading;
                }
                else
                {
                    this.oldListStaff.Add(item);
                }
            }
        }
        /// <summary>
        /// Deep copy toutes les staffs de la old list staff dans la new
        /// </summary>
        public void DeepCopyOldToNewListStaff()
        {
            foreach (I.LevelingStaff item in this.oldListStaff)
            {
                this.newListStaff.Add(item.DeepCopy());
            }
        }

        internal void AdjustToScreenSize(bool full=false)
        {
            const int DIFF = 50;
            Rectangle screenSize =Tsunami2.Properties.View.Bounds;

            if (full)
            {
                this.Height = screenSize.Height - DIFF;
                this.Width = screenSize.Width - DIFF;
                this.Top = DIFF / 3;
                this.Left = DIFF / 3;
            }
            else
            {
                if ((this.Top + this.Height) > (screenSize.Height - DIFF))
                {
                    this.Top = DIFF / 3;
                    this.Height = screenSize.Height - DIFF;
                }
                if ((this.Left + this.Width) > (screenSize.Width - DIFF))
                {
                    this.Width = screenSize.Width - 50;
                }
            }
            this.Top = (screenSize.Height - this.Height) / 2;
            this.Left = (screenSize.Width - this.Width) / 2;
        }
        /// <summary>
        /// Redraw the datagridview
        /// </summary>
        public void RedrawDataGridViewZeroStaff()
        {
            this.dataGridViewZeroStaff.CellValueChanged -= this.DataGridViewZeroStaff_CellValueChanged;
            this.dataGridViewZeroStaff.CurrentCellChanged -= this.DataGridViewZeroStaff_CurrentCellChanged;
            this.ZeroCorrectionCalculation();
            dataGridViewZeroStaff.Rows.Clear();
            List<List<object>> listRow = new List<List<object>>();
            foreach (I.LevelingStaff item in this.newListStaff)
            {
                AddNewStaffRowInDataGridView(listRow, item);
            }
            AddAverageResultRowInDataGridView(listRow);
            foreach (List<Object> ligne in listRow)
            {
                dataGridViewZeroStaff.Rows.Add(ligne.ToArray());
            }
            for (int i = 0; i < dataGridViewZeroStaff.RowCount-1; i++)
            {
                dataGridViewZeroStaff.Rows[i].Height = 30; // met toutes les lignes en hauteur de 30 pour afficher correctement l'image de suppression.
                ///Met en jaune les cases de mesure à encoder
                if (dataGridViewZeroStaff[1, i].Value.ToString() != "")
                {
                    dataGridViewZeroStaff[2, i].Style.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Action;
                }
            }
            DataGridViewCellStyle dataGridViewCellStyleLastRow = new DataGridViewCellStyle();
            dataGridViewCellStyleLastRow.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyleLastRow.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.DarkBackground;
            dataGridViewCellStyleLastRow.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Underline);
            dataGridViewCellStyleLastRow.ForeColor =TSU.Tsunami2.Preferences.Theme.Colors.NormalFore;
            dataGridViewCellStyleLastRow.SelectionBackColor =TSU.Tsunami2.Preferences.Theme.Colors.TextHighLightBack;
            dataGridViewCellStyleLastRow.SelectionForeColor =TSU.Tsunami2.Preferences.Theme.Colors.TextHighLightFore;
            dataGridViewCellStyleLastRow.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            dataGridViewCellStyleLastRow.BackColor =TSU.Tsunami2.Preferences.Theme.Colors.Object;
            dataGridViewZeroStaff.Rows[dataGridViewZeroStaff.RowCount - 1].DefaultCellStyle = dataGridViewCellStyleLastRow;
            dataGridViewZeroStaff.Rows[dataGridViewZeroStaff.RowCount - 1].Height = 30;
            dataGridViewZeroStaff[2, dataGridViewZeroStaff.RowCount - 1].ReadOnly = true;
            dataGridViewZeroStaff[3, dataGridViewZeroStaff.RowCount - 1].ReadOnly = true;
            this.dataGridViewZeroStaff.Refresh();
            this.dataGridViewZeroStaff.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewZeroStaff_CellValueChanged);
            this.dataGridViewZeroStaff.CurrentCellChanged += new System.EventHandler(this.DataGridViewZeroStaff_CurrentCellChanged);
        }
        /// <summary>
        /// Add a new row of tilt meaure in the datagridview
        /// </summary>
        /// <param name="listRow"></param>
        /// <param name="item"></param>
        private  void AddNewStaffRowInDataGridView(List<List<object>> listRow, I.LevelingStaff item)
        {
            List<object> row = new List<object>();
            row.Add(R.Tilt_Delete); //0 Delete mes
            row.Add(""); // 1 Staff
            row.Add(""); // 2 Reading
            row.Add(""); // 3 Correction
            listRow.Add(row);
            //Ne permet pas d'effacer une mesure non vide ou si on n'a qu'une seule mesure
            listRow[listRow.Count - 1][1] = item._Name;
            if (item._zeroCorrectionReading != na) { listRow[listRow.Count - 1][2] = Math.Round(item._zeroCorrectionReading * 100000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")); }
            if (item._zeroCorrection != na) { listRow[listRow.Count - 1][3] = Math.Round(item._zeroCorrection * 100000, 1).ToString("F1", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")); }
        }
        private  void AddAverageResultRowInDataGridView(List<List<object>> listRow)
        {
            List<object> row = new List<object>();
            row.Add(R.Add_Tilt); //0 Add Mesure
            row.Add(""); // 1 Staff
            row.Add(""); // 2 Average reading
            row.Add(""); // 3 Correction
            listRow.Add(row);
            listRow[listRow.Count - 1][1] = R.StringMsgZeroStaff_Average;
            if (averageReading != na) { listRow[listRow.Count - 1][2] = Math.Round(averageReading * 100000, 1).ToString("F1", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")); }
        }
        private void DataGridViewZeroStaff_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < dataGridViewZeroStaff.Rows.Count && e.RowIndex != -1)
            {
                rowActiveCell = e.RowIndex;
            }
            if (e.ColumnIndex < dataGridViewZeroStaff.Columns.Count && e.ColumnIndex != -1)
            {
                columnActiveCell = e.ColumnIndex;
            }
            /// mesure staff zero reading entree

            if (e.RowIndex >= 0 && e.ColumnIndex == 2 && (e.RowIndex <= dataGridViewZeroStaff.Rows.Count - 2))
            {
                if (dataGridViewZeroStaff[e.ColumnIndex, e.RowIndex].Value != null)
                {
                    double datagridNumber = T.Conversions.Numbers.ToDouble(dataGridViewZeroStaff[e.ColumnIndex, e.RowIndex].Value.ToString(), true, -9999);
                    if (datagridNumber != -9999)
                    {
                        if (this.newListStaff[e.RowIndex]._zeroCorrectionReading != datagridNumber / 100000)
                        {
                            //encode la mesure zero reading de la staff en cours
                            this.newListStaff[e.RowIndex]._zeroCorrectionReading = datagridNumber / 100000;
                            //sélectionne la case staff suivante pour encoder le reading suivant
                            if (rowActiveCell < dataGridViewZeroStaff.RowCount - 2)
                            {
                                columnActiveCell = 2;
                                rowActiveCell++;
                            }
                            else
                            {
                                //passe à la dernière ligne du datagrid qui n'est pas editable pour pouvoir cliquer sur les raccourci bouton
                                columnActiveCell = 2;
                                rowActiveCell = dataGridViewZeroStaff.RowCount - 1;
                            }
                            this.setCurrentCell = true;
                            this.TryAction(this.RedrawDataGridViewZeroStaff);
                            this.TryAction(SetCurrentCell);
                            return;
                        }
                        else
                        {
                            //remet l'ancienne valeur dans la case au bon format
                            this.dataGridViewZeroStaff.CellValueChanged -= this.DataGridViewZeroStaff_CellValueChanged;
                            this.dataGridViewZeroStaff[e.ColumnIndex, e.RowIndex].Value = Math.Round(datagridNumber, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                            this.dataGridViewZeroStaff.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewZeroStaff_CellValueChanged);
                            return;
                        }
                    }
                    else
                    {
                        //message erreur, valeur encodée incorrecte
                        new MessageInput(MessageType.Warning, R.StringMsgZeroStaff_WrongReading).Show();
                        //remet l'ancienne valeur dans la case au bon format
                        this.dataGridViewZeroStaff.CellValueChanged -= this.DataGridViewZeroStaff_CellValueChanged;
                        if (this.newListStaff[e.RowIndex]._zeroCorrectionReading != na)
                        {
                            this.dataGridViewZeroStaff[e.ColumnIndex, e.RowIndex].Value = Math.Round(this.newListStaff[e.RowIndex]._zeroCorrectionReading * 100000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                        }
                        else
                        {
                            this.dataGridViewZeroStaff[e.ColumnIndex, e.RowIndex].Value = "";
                        }
                        this.dataGridViewZeroStaff.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewZeroStaff_CellValueChanged);
                        return;
                    }
                }
                else
                {
                    //contenu de la case effacé
                    this.newListStaff[e.RowIndex]._zeroCorrectionReading = na;
                    if (rowActiveCell < dataGridViewZeroStaff.RowCount - 2)
                    {
                        columnActiveCell = 2;
                        rowActiveCell++;
                    }
                    this.setCurrentCell = true;
                    this.TryAction(this.RedrawDataGridViewZeroStaff);
                    this.TryAction(SetCurrentCell);
                    return;
                }
            }
        }

        private void ZeroCorrectionCalculation()
        {
            double avg = 0;
            int nb = 0;
            foreach (I.LevelingStaff item in this.newListStaff)
            {
                item._zeroCorrection = na;
                if (item._zeroCorrectionReading != na)
                {
                    avg += item._zeroCorrectionReading;
                    nb++;
                }
            }
            if (nb>0)
            {
                avg = avg / nb;
                this.averageReading = avg;
                foreach (I.LevelingStaff item in this.newListStaff)
                {
                    if (item._zeroCorrectionReading != na)
                    {
                        item._zeroCorrection = avg - item._zeroCorrectionReading;
                    }
                }
            }
            else
            {
                this.averageReading = na;
            }
        }

        /// <summary>
        /// Set the current cell in datagridview as the columnactivecell and rowactivecell
        /// </summary>
        internal void SetCurrentCell()
        {
            Action a = () =>
            {
                this.dataGridViewZeroStaff.CurrentCellChanged -= this.DataGridViewZeroStaff_CurrentCellChanged;
                dataGridViewZeroStaff.Focus();
                dataGridViewZeroStaff.ClearSelection();
                if ((columnActiveCell < this.dataGridViewZeroStaff.ColumnCount) && (columnActiveCell >= 0) && (rowActiveCell < this.dataGridViewZeroStaff.RowCount) && (rowActiveCell >= 0))
                {
                    dataGridViewZeroStaff.CurrentCell = this.dataGridViewZeroStaff[columnActiveCell, rowActiveCell];
                    dataGridViewZeroStaff.Rows[rowActiveCell].Cells[columnActiveCell].Selected = true;
                }
                this.dataGridViewZeroStaff.CurrentCellChanged += new System.EventHandler(this.DataGridViewZeroStaff_CurrentCellChanged);
            };
            Tsunami2.Properties.InvokeOnApplicationDispatcher(a);
        }
        private void DataGridViewZeroStaff_CurrentCellChanged(object sender, EventArgs e)
        {
            //permet de détecter quand le datagridview change automatiquement la ligne courante après avoir entré une valeur dans une case.  
            if (this.setCurrentCell == true && this.dataGridViewZeroStaff.CurrentCell != null)
            { 
                ////BeginInvoke crèe un processus asynchrone qui empêche de tourner en boucle lorsqu'on change la current cell
                Delegate delegateCurrentCell = new MethodInvoker(() =>
                {
                    this.SetCurrentCell();
                });
                IAsyncResult async = this.BeginInvoke(delegateCurrentCell);
                this.setCurrentCell = false;
            }
        }
        /// <summary>
        /// Evénement si on clique pour ajouter ou effacer une mesure
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGridViewZeroStaff_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewZeroStaff.IsCurrentCellInEditMode) return;
            //Ajout d'une mesure
            if (e.RowIndex == dataGridViewZeroStaff.RowCount - 1 && e.ColumnIndex == 0)
            {
                this.AddStaffs();
                this.TryAction(RedrawDataGridViewZeroStaff);
                this.dataGridViewZeroStaff.Focus();
                return;
            }
            //Efface une staff du tableau, 
            if (e.RowIndex <= dataGridViewZeroStaff.RowCount - 2 && e.ColumnIndex == 0)
            {
                this.newListStaff.RemoveAt(e.RowIndex);
                this.TryAction(RedrawDataGridViewZeroStaff);
                this.dataGridViewZeroStaff.Focus();
                return;
            }
        }

        private void AddStaffs()
        {
            this.staffModule.View.CheckBoxesVisible = false;
            this.staffModule.MultiSelection = true;
            this.staffModule._SelectedObjects.Clear();
            foreach (I.LevelingStaff item in this.newListStaff)
            {
                TsuObject staff = this.staffModule.AllElements.OfType<I.LevelingStaff>()
                    .FirstOrDefault(x => x._Model == item._Model && x._SerialNumber == item._SerialNumber);
                this.staffModule._SelectedObjectInBlue = staff;
                this.staffModule.AddSelectedObjects(staff);
                this.staffModule.View.currentStrategy.ShowSelectionInButton();
            }
            List<I.LevelingStaff> staffsChosed =   this.staffModule.SelectStaffs(R.StringMsgZeroStaff_SelectStaffs, I.InstrumentClasses.MIRE, false);
            List<I.LevelingStaff> staffChosedDeepCopy = new List<I.LevelingStaff>();
            foreach (I.LevelingStaff item in staffsChosed)
            {
                staffChosedDeepCopy.Add(item.DeepCopy());
                int index = this.newListStaff.FindIndex(x => x._Name == item._Name);
                if (index != -1)
                {
                    staffChosedDeepCopy[staffChosedDeepCopy.Count-1]._zeroCorrectionReading = this.newListStaff[index]._zeroCorrectionReading;
                }
            }
            this.newListStaff.Clear();
            this.newListStaff = staffChosedDeepCopy;
        }

        /// <summary>
        /// Force le datagrid à reprendre le focus si on passe la souris dessus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGridViewZeroStaff_MouseEnter(object sender, EventArgs e)
        {
            Action a = () =>
            {
                dataGridViewZeroStaff.Focus();
            };
            Tsunami2.Properties.InvokeOnApplicationDispatcher(a);
        }
        
        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            this.rowActiveCell = 0;
            this.columnActiveCell = 2;
            this.SetCurrentCell();
        }

        private void MessageDataGridTSU_Shown(object sender, EventArgs e)
        {
            timer1.Interval = 50;
            timer1.Enabled = true;
            timer1.Start();
        }

        private void titleAndMessageRichBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonLeft.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonLeft();
            }
            if (e.KeyChar == this.buttonRight.Text.ToLower().ToCharArray()[0])
            {
                SetRespond(this.buttonRight);
                this.Close();
            }
            if (dataGridViewZeroStaff.CurrentCell != null)
            {
                if ((dataGridViewZeroStaff.CurrentCell.RowIndex == dataGridViewZeroStaff.RowCount - 1) && e.KeyChar == 13)
                {
                    ActionButtonLeft();
                }
            }
        }

        private void DataGridViewZeroStaff_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonLeft.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonLeft();
            }
            if (e.KeyChar == this.buttonRight.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonRight();
            }
            if (dataGridViewZeroStaff.CurrentCell != null)
            {
                if ((dataGridViewZeroStaff.CurrentCell.RowIndex == dataGridViewZeroStaff.RowCount - 1) && e.KeyChar == 13)
                {
                    ActionButtonLeft();
                }
            }
        }

        private void buttonLeft_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonLeft.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonLeft();
            }
            if (e.KeyChar == this.buttonRight.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonRight();
            }
            if (dataGridViewZeroStaff.CurrentCell != null)
            {
                if ((dataGridViewZeroStaff.CurrentCell.RowIndex == dataGridViewZeroStaff.RowCount - 1) && e.KeyChar == 13)
                {
                    ActionButtonLeft();
                }
            }
        }

        private void buttonRight_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonLeft.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonLeft();
            }
            if (e.KeyChar == this.buttonRight.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonRight();
            }
            if (dataGridViewZeroStaff.CurrentCell != null)
            {
                if ((dataGridViewZeroStaff.CurrentCell.RowIndex == dataGridViewZeroStaff.RowCount - 1) && e.KeyChar == 13)
                {
                    ActionButtonLeft();
                }
            }
        }

        private void ActionButtonRight()
        {
            SetRespond(this.buttonRight);
            this.Close();
        }

        private void ActionButtonLeft()
        {
            SetRespond(this.buttonLeft);
            this.CheckDatagridIsInEditMode();
            this.TryAction(this.UpdateOldListStaff);
            this.Close();
        }
    }
}
