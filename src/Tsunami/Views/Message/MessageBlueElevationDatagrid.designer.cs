﻿using R = TSU.Properties.Resources;
namespace TSU.Views
{
    partial class MesssageBlueElevationDataGrid
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonLeft = new System.Windows.Forms.Button();
            this.titleAndMessageRichBox = new System.Windows.Forms.RichTextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridViewBlueElevation = new System.Windows.Forms.DataGridView();
            this.PointName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Outward1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Return1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Repeat1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Outward3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Return3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Repeat3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonRight = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBlueElevation)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonLeft
            // 
            this.buttonLeft.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonLeft.BackColor = System.Drawing.Color.LightGray;
            this.buttonLeft.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLeft.Location = new System.Drawing.Point(255, 7);
            this.buttonLeft.MaximumSize = new System.Drawing.Size(140, 0);
            this.buttonLeft.MinimumSize = new System.Drawing.Size(140, 60);
            this.buttonLeft.Name = "buttonLeft";
            this.buttonLeft.Size = new System.Drawing.Size(140, 60);
            this.buttonLeft.TabIndex = 8;
            this.buttonLeft.Text = "buttonLeft";
            this.buttonLeft.UseVisualStyleBackColor = false;
            this.buttonLeft.Click += new System.EventHandler(this.On_buttonClick);
            this.buttonLeft.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonLeft_KeyPress);
            // 
            // titleAndMessageRichBox
            // 
            this.titleAndMessageRichBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.titleAndMessageRichBox.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.titleAndMessageRichBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.titleAndMessageRichBox.Location = new System.Drawing.Point(3, 3);
            this.titleAndMessageRichBox.Name = "titleAndMessageRichBox";
            this.titleAndMessageRichBox.ReadOnly = true;
            this.titleAndMessageRichBox.Size = new System.Drawing.Size(1302, 94);
            this.titleAndMessageRichBox.TabIndex = 25;
            this.titleAndMessageRichBox.TabStop = false;
            this.titleAndMessageRichBox.Text = "Title\n";
            this.titleAndMessageRichBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.titleAndMessageRichBox_KeyPress);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(8, 8);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1308, 666);
            this.panel2.TabIndex = 27;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.dataGridViewBlueElevation, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.titleAndMessageRichBox, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1308, 666);
            this.tableLayoutPanel1.TabIndex = 26;
            // 
            // dataGridViewBlueElevation
            // 
            this.dataGridViewBlueElevation.AllowUserToAddRows = false;
            this.dataGridViewBlueElevation.AllowUserToDeleteRows = false;
            this.dataGridViewBlueElevation.AllowUserToResizeColumns = false;
            this.dataGridViewBlueElevation.AllowUserToResizeRows = false;
            this.dataGridViewBlueElevation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewBlueElevation.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewBlueElevation.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewBlueElevation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBlueElevation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PointName,
            this.Outward1,
            this.Return1,
            this.Repeat1,
            this.Outward3,
            this.Return3,
            this.Repeat3});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewBlueElevation.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewBlueElevation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewBlueElevation.EnableHeadersVisualStyles = false;
            this.dataGridViewBlueElevation.Location = new System.Drawing.Point(3, 103);
            this.dataGridViewBlueElevation.MultiSelect = false;
            this.dataGridViewBlueElevation.Name = "dataGridViewBlueElevation";
            this.dataGridViewBlueElevation.ReadOnly = true;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewBlueElevation.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewBlueElevation.RowHeadersVisible = false;
            this.dataGridViewBlueElevation.Size = new System.Drawing.Size(1302, 480);
            this.dataGridViewBlueElevation.TabIndex = 0;
            this.dataGridViewBlueElevation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dataGridViewBlueElevation_KeyPress);
            this.dataGridViewBlueElevation.MouseEnter += new System.EventHandler(this.dataGridViewBlueElevation_MouseEnter);
            // 
            // PointName
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.PointName.DefaultCellStyle = dataGridViewCellStyle2;
            this.PointName.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Name;
            this.PointName.MinimumWidth = 200;
            this.PointName.Name = "PointName";
            this.PointName.ReadOnly = true;
            this.PointName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PointName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Outward1
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Outward1.DefaultCellStyle = dataGridViewCellStyle3;
            this.Outward1.HeaderText = "ST1 Outward (1/100 mm)";
            this.Outward1.Name = "Outward1";
            this.Outward1.ReadOnly = true;
            this.Outward1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Outward1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Return1
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            this.Return1.DefaultCellStyle = dataGridViewCellStyle4;
            this.Return1.HeaderText = "ST1 Return (1/100mm)";
            this.Return1.Name = "Return1";
            this.Return1.ReadOnly = true;
            this.Return1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Return1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Repeat1
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            this.Repeat1.DefaultCellStyle = dataGridViewCellStyle5;
            this.Repeat1.HeaderText = "ST1 Repeat (1/100mm)";
            this.Repeat1.Name = "Repeat1";
            this.Repeat1.ReadOnly = true;
            this.Repeat1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Outward3
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            this.Outward3.DefaultCellStyle = dataGridViewCellStyle6;
            this.Outward3.HeaderText = "ST3 Outward (1/100mm)";
            this.Outward3.Name = "Outward3";
            this.Outward3.ReadOnly = true;
            this.Outward3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Return3
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            this.Return3.DefaultCellStyle = dataGridViewCellStyle7;
            this.Return3.HeaderText = "ST3 Return (1/100 mm)";
            this.Return3.Name = "Return3";
            this.Return3.ReadOnly = true;
            this.Return3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Repeat3
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            this.Repeat3.DefaultCellStyle = dataGridViewCellStyle8;
            this.Repeat3.HeaderText = "ST3 Repeat (1/100mm)";
            this.Repeat3.Name = "Repeat3";
            this.Repeat3.ReadOnly = true;
            this.Repeat3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.buttonRight, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.buttonLeft, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 589);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1302, 74);
            this.tableLayoutPanel2.TabIndex = 26;
            // 
            // buttonRight
            // 
            this.buttonRight.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonRight.BackColor = System.Drawing.Color.LightGray;
            this.buttonRight.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRight.Location = new System.Drawing.Point(906, 7);
            this.buttonRight.MaximumSize = new System.Drawing.Size(140, 0);
            this.buttonRight.MinimumSize = new System.Drawing.Size(140, 60);
            this.buttonRight.Name = "buttonRight";
            this.buttonRight.Size = new System.Drawing.Size(140, 60);
            this.buttonRight.TabIndex = 9;
            this.buttonRight.Text = global::TSU.Properties.Resources.StringDataGridBlueElevation_CancelExport;
            this.buttonRight.UseVisualStyleBackColor = false;
            this.buttonRight.Click += new System.EventHandler(this.On_buttonClick);
            this.buttonRight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonRight_KeyPress);
            // 
            // MesssageBlueElevationDataGrid
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(1324, 682);
            this.Controls.Add(this.panel2);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MinimumSize = new System.Drawing.Size(450, 200);
            this.Name = "MesssageBlueElevationDataGrid";
            this.Padding = new System.Windows.Forms.Padding(8);
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Message...";
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBlueElevation)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button buttonLeft;
        private System.Windows.Forms.RichTextBox titleAndMessageRichBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView dataGridViewBlueElevation;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.Button buttonRight;
        public System.Windows.Forms.DataGridViewTextBoxColumn PointName;
        public System.Windows.Forms.DataGridViewTextBoxColumn Outward1;
        public System.Windows.Forms.DataGridViewTextBoxColumn Return1;
        public System.Windows.Forms.DataGridViewTextBoxColumn Repeat1;
        public System.Windows.Forms.DataGridViewTextBoxColumn Outward3;
        public System.Windows.Forms.DataGridViewTextBoxColumn Return3;
        public System.Windows.Forms.DataGridViewTextBoxColumn Repeat3;
    }
}