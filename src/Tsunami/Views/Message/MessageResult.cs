﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace TSU.Views.Message
{
    public class MessageResult : IDisposable
    {
        public string TextOfButtonClicked = "";

        public List<Control> ReturnedControls = new List<Control>();

        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    foreach (Control control in ReturnedControls)
                    {
                        if (control.InvokeRequired)
                            control.BeginInvoke(new Action(() => control.Dispose()));
                        else
                            control.Dispose();
                    }
                    ReturnedControls.Clear();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}