﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;

namespace TSU.Views
{
    //public interface IMessageTsu
    //{
    //    void ShowInMessageTsu(
    //        string title,
    //        string message,
    //        string middleButton,
    //        Action middleAction,
    //        string rightButton = null,
    //        Action rightAction = null,
    //        string leftButton = null,
    //        Action leftAction = null,
    //        Color? background = null,
    //        Color? foreground = null);
    //    List<string> GetTextListForMessage();
    //}

    // Version ShowDialog

    // Comparation
    // Implements the manual sorting of items by columns.
    class ListViewItemComparer : IComparer
    {
        private int col;
        bool _descendantSorting;
        public ListViewItemComparer()
        {
            col = 0;
        }
        public ListViewItemComparer(int column, bool descendantSorting)
        {
            col = column;
            _descendantSorting = descendantSorting;
        }
        public int Compare(object x, object y)
        {
            int i;

            Analyse(x, out string itemX, out double dX, out bool xNum);
            Analyse(y, out string itemY, out double dY, out bool yNum);


            if (_descendantSorting)
            {
                if (xNum && yNum)
                {
                    if (dX > dY)
                        i= 1;
                    else if (dX < dY)
                        i=-1;
                    else
                        i= 0;
                }
                else
                    i= String.Compare(itemX, itemY);
            }
            else
            {
                if (xNum && yNum)
                {
                    if (dX < dY)
                        i = 1;
                    else if (dX > dY)
                        i = -1;
                    else
                        i = 0;
                }
                else
                    i =String.Compare(itemY, itemX);
            }
            
            return i;
        }

        private void Analyse(object o, out string itemO, out double dO, out bool oNum)
        {
            dO = 0;
            oNum = false;
            if (((ListViewItem)o).SubItems.Count < col + 1)
            {
                itemO = "@";
            }
            else
            {
                itemO = ((ListViewItem)o).SubItems[col].Text;
                if (itemO == "")
                    itemO = "@";
                else
                {
                    oNum = Double.TryParse(itemO, out dO);
                }
            }
        }
    }
}
