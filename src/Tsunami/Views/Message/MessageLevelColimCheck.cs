﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using TSU.Views;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using P = TSU.Tsunami2.Preferences;

using M = TSU.Common.Measures;
using I = TSU.Common.Instruments;
using T = TSU.Tools;
using System.Globalization;
using TSU.Common;
using TSU.Views.Message;

namespace TSU.Views
{
    public partial class MessageLevelColimCheck : TsuView
    {
        private string _respond;
        public string Respond { get { return _respond; }   }
        public string CancelString { get; set; }
        public I.Level level;
        internal int rowActiveCell = 0;
        internal int columnActiveCell = 1;
        internal bool setCurrentCell = false;
        private double na { get; set; } =TSU.Tsunami2.Preferences.Values.na;
        public double errorColim;
        public double LA1;
        public double LB1;
        public double LA2;
        public double LB2;
        public double distColim;
        double theoLB2;
        double theoLA2;
        double theoLB1;
        double theoLA1;
        string helptext;
        public MessageLevelColimCheck()
        {
            this.InitializeComponent();
            this.ApplyThemeColors();
            this.Resizable = true;
            this.Location = new System.Drawing.Point((TSU.Tsunami2.Preferences.Values.WorkingArea.Width - this.Width) / 2, (TSU.Tsunami2.Preferences.Values.WorkingArea.Height - this.Height )/ 2);
            this.AllowMovement(this);
            LA1 = na;
            LA2 = na;
            LB1 = na;
            LB2 = na;
            distColim = na;
            errorColim = na;
            theoLA1 = na;
            theoLA2 = na;
            theoLB1 = na;
            theoLB2 = na;
            helptext = R.StringMsgColimLevel_HelpDescription1 + "\r\n";
            helptext += R.StringMsgColimLevel_HelpDescription2 + "\r\n";
            helptext += R.StringMsgColimLevel_HelpDescription3 + "\r\n";
            helptext += R.StringMsgColimLevel_HelpDescription4 + "\r\n";
            helptext += R.StringMsgColimLevel_HelpDescription5 + "\r\n";
            helptext += R.StringMsgColimLevel_HelpDescription6 + "\r\n";
            helptext += R.StringMsgColimLevel_HelpDescription7 + "\r\n";
            helptext += R.StringMsgColimLevel_HelpDescription8;
        }
        private void ApplyThemeColors()
        {
            this.buttonLeft.BackColor = P.Theme.Colors.Object;
            this.buttonRight.BackColor = P.Theme.Colors.Object;
            this.dataGridViewColimLevel.ColumnHeadersDefaultCellStyle.BackColor = P.Theme.Colors.Object;
            this.dataGridViewColimLevel.ColumnHeadersDefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.dataGridViewColimLevel.ColumnHeadersDefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            this.dataGridViewColimLevel.ColumnHeadersDefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.dataGridViewColimLevel.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.dataGridViewColimLevel.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.dataGridViewColimLevel.DefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            this.dataGridViewColimLevel.DefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.BackColor = P.Theme.Colors.Object;
            this.dataGridViewColimLevel.BackgroundColor = P.Theme.Colors.Background;
            this.StaffPos.HeaderText = R.StringMsgColimLevel_StaffPos;
            this.Reading.HeaderText = R.StringMsgColimLevel_Reading;
            this.Theo.HeaderText = R.StringMsgColimLevel_Theo;
        }


        // Tools
        public void Colorize( Color back, Color fore)
        {
            this.BackColor = back;
            this.ForeColor = fore;
            this.titleAndMessageRichBox.BackColor = back;
            Color darkerColor = Common.Analysis.Colors.GetDarkerColor(BackColor);
            Color lighterColor = Common.Analysis.Colors.GetLighterColor(BackColor);
            this.BackColor = darkerColor;
            this.panel2.BackColor = back;
            this.titleAndMessageRichBox.BackColor = back;
            this.buttonRight.BackColor = darkerColor;
            this.buttonLeft.BackColor = darkerColor;
            this.buttonHelp.BackColor = darkerColor;
            this.titleAndMessageRichBox.ForeColor = fore;
            this.buttonRight.ForeColor = fore;
            this.buttonLeft.ForeColor = fore;
            this.buttonHelp.ForeColor = fore;
        }



        public void FillTitleAndAdjustSize(string title = "", string left = "", string right = "")
        {
            SetTitle(title);
            this.buttonRight.Text = right;
            this.buttonLeft.Text = left;
            this.buttonHelp.Text = R.StringMsgColimLevel_Help;
            this.buttonRight.Visible = (this.buttonRight.Text == "") ? false : true;
            this.buttonLeft.Visible = (this.buttonLeft.Text == "") ? false : true;
            this.CreateToolTip();
        }
        /// <summary>
        /// Cree les tooltips pour tous les boutons
        /// </summary>
        private void CreateToolTip()
        {
            ToolTip tooltip = new ToolTip();
            tooltip.InitialDelay = 50;
            tooltip.AutoPopDelay = 5000;
            tooltip.ReshowDelay = 50;
            if (titleAndMessageRichBox != null) tooltip.SetToolTip(this.titleAndMessageRichBox, this.titleAndMessageRichBox.Text);
            tooltip.SetToolTip(this, this.Text);
            if (buttonLeft != null) tooltip.SetToolTip(this.buttonLeft, this.buttonLeft.Text);
            if (buttonRight != null) tooltip.SetToolTip(this.buttonRight, this.buttonRight.Text);
            if (buttonHelp != null) tooltip.SetToolTip(this.buttonHelp, helptext);
        }


        public void SetTitle(string title)
        {
            this.titleAndMessageRichBox.Text = "";
            Rectangle screenSize =Tsunami2.Properties.View.Bounds;
            this.titleAndMessageRichBox.Text = title;
            this.titleAndMessageRichBox.SelectAll();
            Font titleFont =TSU.Tsunami2.Preferences.Theme.Fonts.Large;
            this.titleAndMessageRichBox.SelectionFont = titleFont;
            this.titleAndMessageRichBox.SelectionAlignment = HorizontalAlignment.Center;
            this.titleAndMessageRichBox.SelectionLength = 0;
            this.Top = (screenSize.Height - this.Height) / 2;
            this.Left = (screenSize.Width - this.Width) / 2;
            this.AdjustToScreenSize();
        }
        // Return
        internal void SetRespond(Button button)
        {
            _respond = button.Text;
        }

        protected virtual void On_buttonClick(object sender, EventArgs e)
        {
            Button button = ((Button)sender);
            SetRespond(button);
            if (button == buttonLeft)
            {
                this.CheckDatagridIsInEditMode();
                this.TryAction(this.UpdateColimParameter);
            }
            if (button == buttonLeft || button == buttonRight) this.Close();
            if (button == buttonHelp) this.ShowMessageOfImage(R.StringMsgColimLevel_DrawingTitleDescription,R.T_OK,R.Level_Collimation_Help,helptext);
        }
        /// <summary>
        /// Check if datagrid is in edit mode and validate it before leaving
        /// </summary>
        internal void CheckDatagridIsInEditMode()
        {
            if (dataGridViewColimLevel.IsCurrentCellInEditMode)
            {
                //this.dataGridViewLevel.CellValueChanged -= this.dataGridViewLevel_CellValueChanged;
                int saveRow = rowActiveCell;
                int saveColumn = columnActiveCell;
                //Try car de temps en temps endEdit peut faire une exception
                try
                {
                    dataGridViewColimLevel.EndEdit();

                }
                catch (Exception ex)
                {
                    ex.Data.Clear();
                }
                //this.dataGridViewLevel.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewLevel_CellValueChanged);
                rowActiveCell = saveRow;
                columnActiveCell = saveColumn;
            }
        }
        /// <summary>
        /// Met à jour la liste des staff dans old list staff
        /// </summary>
        private void UpdateColimParameter()
        {
            if (errorColim != na)
            {
                level._LA1 = LA1;
                level._LA2 = LA2;
                level._LB1 = LB1;
                level._LB2 = LB2;
                level._ErrorCollim = errorColim;
                level._DistCollim = distColim;
                level._IsCollimExported = false;
                level._DateCollim = DateTime.Now;
            }
            else
            {
                level._LA1 = na;
                level._LA2 = na;
                level._LB1 = na;
                level._LB2 = na;
                level._ErrorCollim = na;
                level._DistCollim = na;
                level._IsCollimExported = false;
                level._DateCollim = DateTime.MinValue;
            }
        }

        internal void AdjustToScreenSize(bool full=false)
        {
            const int DIFF = 50;
            Rectangle screenSize =Tsunami2.Properties.View.Bounds;

            if (full)
            {
                this.Height = screenSize.Height - DIFF;
                this.Width = screenSize.Width - DIFF;
                this.Top = DIFF / 3;
                this.Left = DIFF / 3;
            }
            else
            {
                if ((this.Top + this.Height) > (screenSize.Height - DIFF))
                {
                    this.Top = DIFF / 3;
                    this.Height = screenSize.Height - DIFF;
                }
                if ((this.Left + this.Width) > (screenSize.Width - DIFF))
                {
                    this.Width = screenSize.Width - 50;
                }
            }
            this.Top = (screenSize.Height - this.Height) / 2;
            this.Left = (screenSize.Width - this.Width) / 2;
        }
        /// <summary>
        /// Redraw the datagridview
        /// </summary>
        public void RedrawDataGridViewColim()
        {
            this.dataGridViewColimLevel.CellValueChanged -= this.DataGridViewColimLevel_CellValueChanged;
            this.dataGridViewColimLevel.CurrentCellChanged -= this.DataGridViewColimLevel_CurrentCellChanged;
            this.ErrorColimCalculation();
            dataGridViewColimLevel.Rows.Clear();
            List<List<object>> listRow = new List<List<object>>();
            AddNewMeasLineInDataGridView(listRow, "LA1");
            AddNewMeasLineInDataGridView(listRow, "LB1");
            AddNewMeasLineInDataGridView(listRow, "LB2");
            AddNewMeasLineInDataGridView(listRow, "LA2");
            AddDistRowInDataGridView(listRow);
            AddErrorResultRowInDataGridView(listRow);
            foreach (List<Object> ligne in listRow)
            {
                dataGridViewColimLevel.Rows.Add(ligne.ToArray());
            }
            for (int i = 0; i < dataGridViewColimLevel.RowCount-1; i++)
            {
                dataGridViewColimLevel.Rows[i].Height = 30; // met toutes les lignes en hauteur de 30 pour afficher correctement l'image de suppression.
                ///Met en jaune les cases de mesure à encoder
                if (dataGridViewColimLevel[0, i].Value.ToString() != "")
                {
                    dataGridViewColimLevel[1, i].Style.BackColor = P.Theme.Colors.Action;
                }
            }
            DataGridViewCellStyle dataGridViewCellStyleLastRow = new DataGridViewCellStyle();
            dataGridViewCellStyleLastRow.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyleLastRow.BackColor = P.Theme.Colors.DarkBackground;
            dataGridViewCellStyleLastRow.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Underline);
            dataGridViewCellStyleLastRow.ForeColor = P.Theme.Colors.NormalFore;
            dataGridViewCellStyleLastRow.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            dataGridViewCellStyleLastRow.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            dataGridViewCellStyleLastRow.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            dataGridViewCellStyleLastRow.BackColor = P.Theme.Colors.Object;
            dataGridViewColimLevel.Rows[dataGridViewColimLevel.RowCount - 1].DefaultCellStyle = dataGridViewCellStyleLastRow;
            dataGridViewColimLevel.Rows[dataGridViewColimLevel.RowCount - 1].Height = 30;
            dataGridViewColimLevel[1, dataGridViewColimLevel.RowCount - 1].ReadOnly = true;
            dataGridViewColimLevel[2, dataGridViewColimLevel.RowCount - 1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewColimLevel[2, dataGridViewColimLevel.RowCount - 2].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewColimLevel.Refresh();
            this.dataGridViewColimLevel.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewColimLevel_CellValueChanged);
            this.dataGridViewColimLevel.CurrentCellChanged += new System.EventHandler(this.DataGridViewColimLevel_CurrentCellChanged);
        }
        /// <summary>
        /// Add a new row of tilt meaure in the datagridview
        /// </summary>
        /// <param name="listRow"></param>
        /// <param name="item"></param>
        private  void AddNewMeasLineInDataGridView(List<List<object>> listRow, string staffPosition)
        {
            List<object> row = new List<object>();
            row.Add(""); // 0 Staff position
            row.Add(""); // 2 Reading
            row.Add(""); // 3 Theo
            listRow.Add(row);
            listRow[listRow.Count - 1][0] = staffPosition;
            //Colonne reading
            if (staffPosition == "LA1" && LA1 != na) listRow[listRow.Count - 1][1] = Math.Round(LA1 * 100000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            if (staffPosition == "LA2" && LA2 != na) listRow[listRow.Count - 1][1] = Math.Round(LA2 * 100000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            if (staffPosition == "LB1" && LB1 != na) listRow[listRow.Count - 1][1] = Math.Round(LB1 * 100000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            if (staffPosition == "LB2" && LB2 != na) listRow[listRow.Count - 1][1] = Math.Round(LB2 * 100000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            //Colonne theo
            if (staffPosition == "LA1" && theoLA1 != na) listRow[listRow.Count - 1][2] = Math.Round(theoLA1 * 100000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            if (staffPosition == "LA2" && theoLA2 != na) listRow[listRow.Count - 1][2] = Math.Round(theoLA2 * 100000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            if (staffPosition == "LB1" && theoLB1 != na) listRow[listRow.Count - 1][2] = Math.Round(theoLB1 * 100000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            if (staffPosition == "LB2" && theoLB2 != na) listRow[listRow.Count - 1][2] = Math.Round(theoLB2 * 100000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
        }
        /// <summary>
        /// Ajoute la ligne contenant le calcul d'erreur de collimation
        /// </summary>
        /// <param name="listRow"></param>
        private  void AddErrorResultRowInDataGridView(List<List<object>> listRow)
        {
            List<object> row = new List<object>();
            row.Add(""); // 0 Staff position - Error title
            row.Add(""); // 2 Reading - error
            row.Add(""); // 3 Theo - empty
            listRow.Add(row);
            row[0] = R.StringMsgColimLevel_Error;
            //listRow[listRow.Count - 1][0] = R.StringMsgColimLevel_Error;
            if (errorColim != na)
            {
                row[1] = Math.Round(errorColim * 100000, 1).ToString("F1", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")); 
            }
            row[2] = "1/100mm";

            List<object> rowAngle = new List<object>();
            rowAngle.Add(""); // 0 Staff position - Error title
            rowAngle.Add(""); // 2 Reading - error
            rowAngle.Add(""); // 3 Theo - empty
            listRow.Add(rowAngle);
            // rowAngle[0] = R.StringMsgColimLevel_Error;
            //listRow[listRow.Count - 1][0] = R.StringMsgColimLevel_Error;
            if (errorColim != na)
            {
                double angle = Math.Asin(errorColim / distColim);
                rowAngle[1] = Math.Round(angle * 1000, 3).ToString("F3", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            }
            rowAngle[2] = "mrad";
        }
        /// <summary>
        /// Ajoute la ligne pour encoder la distance entre les points
        /// </summary>
        /// <param name="listRow"></param>
        private void AddDistRowInDataGridView(List<List<object>> listRow)
        {
            List<object> row = new List<object>();
            row.Add(""); // 0 Staff position - Dist AB title
            row.Add(""); // 2 Reading - dist
            row.Add(""); // 3 Theo - empty
            listRow.Add(row);
            listRow[listRow.Count - 1][0] = R.StringMsgColimLevel_Distance;
            if (distColim != na) { listRow[listRow.Count - 1][1] = Math.Round(distColim, 1).ToString("F1", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")); }
            listRow[listRow.Count - 1][2] = "m";
        }
        private void DataGridViewColimLevel_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < dataGridViewColimLevel.Rows.Count && e.RowIndex != -1)
            {
                rowActiveCell = e.RowIndex;
            }
            if (e.ColumnIndex < dataGridViewColimLevel.Columns.Count && e.ColumnIndex != -1)
            {
                columnActiveCell = e.ColumnIndex;
            }
            /// mesure entrée lecture mire

            
            if (e.RowIndex >= 0 && e.ColumnIndex == 1 && (e.RowIndex <= dataGridViewColimLevel.Rows.Count - 4))
            {
                if (dataGridViewColimLevel[e.ColumnIndex, e.RowIndex].Value != null)
                {
                    double datagridNumber = T.Conversions.Numbers.ToDouble(dataGridViewColimLevel[e.ColumnIndex, e.RowIndex].Value.ToString(), true, -9999);
                    if (datagridNumber != -9999)
                    {
                        ///ENcode la lecture faite
                        if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LA1") LA1 = datagridNumber / 100000;
                        if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LA2") LA2 = datagridNumber / 100000;
                        if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LB1") LB1 = datagridNumber / 100000;
                        if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LB2") LB2 = datagridNumber / 100000;
                        //sélectionne la case row suivante pour encoder le reading suivant
                        if (rowActiveCell < dataGridViewColimLevel.RowCount - 3)
                        {
                            columnActiveCell = 1;
                            rowActiveCell++;
                        }
                        else
                        {
                            //passe à la dernière ligne du datagrid qui n'est pas editable pour pouvoir cliquer sur les raccourci bouton
                            columnActiveCell = 1;
                            rowActiveCell = dataGridViewColimLevel.RowCount - 2;
                        }
                        this.setCurrentCell = true;
                        this.TryAction(this.RedrawDataGridViewColim);
                        this.TryAction(SetCurrentCell);
                        return;
                    }
                    else
                    {
                        //message erreur, valeur encodée incorrecte
                        new MessageInput(MessageType.Warning, R.StringMsgColimLevel_WrongReading).Show();
                        //remet l'ancienne valeur dans la case au bon format
                        this.dataGridViewColimLevel.CellValueChanged -= this.DataGridViewColimLevel_CellValueChanged;
                        if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LA1" && LA1 != na) this.dataGridViewColimLevel[e.ColumnIndex, e.RowIndex].Value = Math.Round(LA1 * 100000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                        if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LA2" && LA2 != na) this.dataGridViewColimLevel[e.ColumnIndex, e.RowIndex].Value = Math.Round(LA2 * 100000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                        if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LB1" && LB1 != na) this.dataGridViewColimLevel[e.ColumnIndex, e.RowIndex].Value = Math.Round(LB1 * 100000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                        if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LB2" && LB2 != na) this.dataGridViewColimLevel[e.ColumnIndex, e.RowIndex].Value = Math.Round(LB2 * 100000, 0).ToString("F0", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                        if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LA1" && LA1 == na) this.dataGridViewColimLevel[e.ColumnIndex, e.RowIndex].Value = "";
                        if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LA2" && LA2 == na) this.dataGridViewColimLevel[e.ColumnIndex, e.RowIndex].Value = "";
                        if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LB1" && LB1 == na) this.dataGridViewColimLevel[e.ColumnIndex, e.RowIndex].Value = "";
                        if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LB2" && LB2 == na) this.dataGridViewColimLevel[e.ColumnIndex, e.RowIndex].Value = "";
                        this.dataGridViewColimLevel.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewColimLevel_CellValueChanged);
                        return;
                    }
                }
                else
                {
                    //contenu de la case effacé
                    if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LA1") LA1 = na;
                    if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LA2") LA2 = na;
                    if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LB1") LB1 = na;
                    if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LB2") LB2 = na;
                    if (rowActiveCell < dataGridViewColimLevel.RowCount - 3)
                    {
                        columnActiveCell = 1;
                        rowActiveCell++;
                    }
                    this.setCurrentCell = true;
                    this.TryAction(this.RedrawDataGridViewColim);
                    this.TryAction(SetCurrentCell);
                    return;
                }
            }
            /// distance entrée
            if ((e.RowIndex == dataGridViewColimLevel.Rows.Count - 3) && e.ColumnIndex == 1)
            {
                if (dataGridViewColimLevel[e.ColumnIndex, e.RowIndex].Value != null)
                {
                    double datagridNumber = T.Conversions.Numbers.ToDouble(dataGridViewColimLevel[e.ColumnIndex, e.RowIndex].Value.ToString(), true, -9999);
                    if (datagridNumber != -9999)
                    {
                        ///ENcode la distance
                        distColim = datagridNumber;
                        if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LA1") LA1 = datagridNumber / 100000;
                        if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LA2") LA2 = datagridNumber / 100000;
                        if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LB1") LB1 = datagridNumber / 100000;
                        if (dataGridViewColimLevel[0, e.RowIndex].Value.ToString() == "LB2") LB2 = datagridNumber / 100000;
                        //passe à la dernière ligne du datagrid qui n'est pas editable pour pouvoir cliquer sur les raccourci bouton
                        columnActiveCell = 1;
                        rowActiveCell = dataGridViewColimLevel.RowCount - 2;
                        this.setCurrentCell = true;
                        this.TryAction(this.RedrawDataGridViewColim);
                        this.TryAction(SetCurrentCell);
                        return;
                    }
                    else
                    {
                        //message erreur, valeur encodée incorrecte
                        new MessageInput(MessageType.Warning, R.StringMsgColimLevel_WrongReading).Show();
                        //remet l'ancienne valeur dans la case au bon format
                        this.dataGridViewColimLevel.CellValueChanged -= this.DataGridViewColimLevel_CellValueChanged;
                        if (distColim != na)
                        {
                            this.dataGridViewColimLevel[e.ColumnIndex, e.RowIndex].Value = Math.Round(distColim, 1).ToString("F1", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                        }
                        else
                        {
                            this.dataGridViewColimLevel[e.ColumnIndex, e.RowIndex].Value = "";
                        }
                        this.dataGridViewColimLevel.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewColimLevel_CellValueChanged);
                        return;
                    }
                }
                else
                {
                    //contenu de la case effacé
                    distColim = na;
                    //passe à la dernière ligne du datagrid qui n'est pas editable pour pouvoir cliquer sur les raccourci bouton
                    columnActiveCell = 1;
                    rowActiveCell = dataGridViewColimLevel.RowCount - 2;
                    this.setCurrentCell = true;
                    this.TryAction(this.RedrawDataGridViewColim);
                    this.TryAction(SetCurrentCell);
                    return;
                }
            }
        }
        /// <summary>
        /// Calcule l'erreur de collimation
        /// </summary>
        private void ErrorColimCalculation()
        {
            this.errorColim = na;
            theoLA1 = na;
            theoLA2 = na;
            theoLB1 = na;
            theoLB2 = na;
            if (LA1 != na && LA2 != na && LB1 != na && LB2 != na)
            {
                double diffS1 = LA1 - LB1;
                double diffS2 = LA2 - LB2;
                errorColim = (diffS2 - diffS1) / 2;
                theoLB2 = LB2 - this.errorColim;
                theoLA2 = LA2 - 2 * this.errorColim;
                theoLA1 = LA1 - this.errorColim;
                theoLB1 = LB1 - 2 * this.errorColim;
            }
        }

        /// <summary>
        /// Set the current cell in datagridview as the columnactivecell and rowactivecell
        /// </summary>
        internal void SetCurrentCell()
        {
            Action a = () =>
            {
                this.dataGridViewColimLevel.CurrentCellChanged -= this.DataGridViewColimLevel_CurrentCellChanged;
                dataGridViewColimLevel.Focus();
                dataGridViewColimLevel.ClearSelection();
                if ((columnActiveCell < this.dataGridViewColimLevel.ColumnCount) && (columnActiveCell >= 0) && (rowActiveCell < this.dataGridViewColimLevel.RowCount) && (rowActiveCell >= 0))
                {
                    dataGridViewColimLevel.CurrentCell = this.dataGridViewColimLevel[columnActiveCell, rowActiveCell];
                    dataGridViewColimLevel.Rows[rowActiveCell].Cells[columnActiveCell].Selected = true;
                }
                this.dataGridViewColimLevel.CurrentCellChanged += new System.EventHandler(this.DataGridViewColimLevel_CurrentCellChanged);
            };
            Tsunami2.Properties.InvokeOnApplicationDispatcher(a);
        }
        private void DataGridViewColimLevel_CurrentCellChanged(object sender, EventArgs e)
        {
            //permet de détecter quand le datagridview change automatiquement la ligne courante après avoir entré une valeur dans une case.  
            if (this.setCurrentCell == true && this.dataGridViewColimLevel.CurrentCell != null)
            { 
                ////BeginInvoke crèe un processus asynchrone qui empêche de tourner en boucle lorsqu'on change la current cell
                Delegate delegateCurrentCell = new MethodInvoker(() =>
                {
                    this.SetCurrentCell();
                });
                IAsyncResult async = this.BeginInvoke(delegateCurrentCell);
                this.setCurrentCell = false;
            }
        }
        /// <summary>
        /// Evénement si on clique pour ajouter ou effacer une mesure
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGridViewColimLevel_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }
        /// <summary>
        /// Force le datagrid à reprendre le focus si on passe la souris dessus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGridViewZeroStaff_MouseEnter(object sender, EventArgs e)
        {
            Action a = () =>
            {
                dataGridViewColimLevel.Focus();
            };
            Tsunami2.Properties.InvokeOnApplicationDispatcher(a);
        }
        
        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            this.rowActiveCell = 0;
            this.columnActiveCell = 2;
            this.SetCurrentCell();
        }

        private void MessageDataGridTSU_Shown(object sender, EventArgs e)
        {
            timer1.Interval = 50;
            timer1.Enabled = true;
            timer1.Start();
        }

        private void titleAndMessageRichBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonLeft.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonLeft();
            }
            if (e.KeyChar == this.buttonRight.Text.ToLower().ToCharArray()[0])
            {
                SetRespond(this.buttonRight);
                this.Close();
            }
            if (e.KeyChar == this.buttonHelp.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonHelp();
            }
            if (dataGridViewColimLevel.CurrentCell != null)
            {
                if ((dataGridViewColimLevel.CurrentCell.RowIndex == dataGridViewColimLevel.RowCount - 1) && e.KeyChar == 13)
                {
                    ActionButtonLeft();
                }
            }
        }

        private void DataGridViewColimLevel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonLeft.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonLeft();
            }
            if (e.KeyChar == this.buttonRight.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonRight();
            }
            if (e.KeyChar == this.buttonHelp.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonHelp();
            }
            if (dataGridViewColimLevel.CurrentCell != null)
            {
                if ((dataGridViewColimLevel.CurrentCell.RowIndex == dataGridViewColimLevel.RowCount - 1) && e.KeyChar == 13)
                {
                    ActionButtonLeft();
                }
            }
        }

        private void buttonLeft_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonLeft.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonLeft();
            }
            if (e.KeyChar == this.buttonRight.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonRight();
            }
            if (e.KeyChar == this.buttonHelp.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonHelp();
            }
            if (dataGridViewColimLevel.CurrentCell != null)
            {
                if ((dataGridViewColimLevel.CurrentCell.RowIndex == dataGridViewColimLevel.RowCount - 1) && e.KeyChar == 13)
                {
                    ActionButtonLeft();
                }
            }
        }

        private void buttonRight_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonLeft.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonLeft();
            }
            if (e.KeyChar == this.buttonRight.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonRight();
            }
            if (e.KeyChar == this.buttonHelp.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonHelp();
            }
            if (dataGridViewColimLevel.CurrentCell != null)
            {
                if ((dataGridViewColimLevel.CurrentCell.RowIndex == dataGridViewColimLevel.RowCount - 1) && e.KeyChar == 13)
                {
                    ActionButtonLeft();
                }
            }
        }
        private void buttonHelp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonLeft.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonLeft();
            }
            if (e.KeyChar == this.buttonRight.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonRight();
            }
            if (e.KeyChar == this.buttonHelp.Text.ToLower().ToCharArray()[0])
            {
                ActionButtonHelp();
            }
            if (dataGridViewColimLevel.CurrentCell != null)
            {
                if ((dataGridViewColimLevel.CurrentCell.RowIndex == dataGridViewColimLevel.RowCount - 1) && e.KeyChar == 13)
                {
                    ActionButtonLeft();
                }
            }
        }
        private void ActionButtonRight()
        {
            SetRespond(this.buttonRight);
            this.Close();
        }

        private void ActionButtonLeft()
        {
            SetRespond(this.buttonLeft);
            this.CheckDatagridIsInEditMode();
            this.TryAction(this.UpdateColimParameter);
            this.Close();
        }
        private void ActionButtonHelp()
        {
            SetRespond(this.buttonHelp);
            this.CheckDatagridIsInEditMode();
            this.ShowMessageOfImage(R.StringMsgColimLevel_DrawingTitleDescription, R.T_OK, R.Level_Collimation_Help, helptext);
        }

        private void buttonHelp_Click(object sender, EventArgs e)
        {
            this.ShowMessageOfImage(R.StringMsgColimLevel_DrawingTitleDescription, R.T_OK, R.Level_Collimation_Help, helptext);
        }
    }
}
