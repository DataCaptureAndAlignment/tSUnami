﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TSU.Views.Message;
using P = TSU.Tsunami2.Preferences;

namespace TSU.Views
{
    public class MessageModuleView : MesssageWithFillControl
    {

        public MessageModuleView(Control view, TsuView parentView = null, string titleAndMessage = null,
            List<string> buttons = null, DsaFlag dsa = null, Color? background = null, Color? foreground = null)
            : base(parentView, MessageType.Choice, titleAndMessage, buttons, view, dsa, false, null, null)
        {
            this.ApplyThemeColors();

            this.SuspendLayout();

            this.Name = "MessageModuleView";
            this.DoubleClick += new EventHandler(this.MessageModuleView_DoubleClick);
            this.TopLevel = true;

            panel1.Visible = true;
            StartPosition = FormStartPosition.WindowsDefaultLocation;

            Colorize(background ?? P.Theme.Colors.Object,
                     foreground ?? P.Theme.Colors.NormalFore);

            // Depending of the type of view
            if (view is TsuView tv)
            {
                this._Module = tv._Module;
                if (tv is ManagerView mv)
                {
                    if (mv.currentStrategy == null)
                        mv.ShowAsListView();
                    mv.currentStrategy.Show();
                }
            }
            else
            {
                this._Module = null;
            }

            // Add Events
            KeyPress += new KeyPressEventHandler(On_KeyPressed);
            this.button2.Click += new EventHandler(ButtonRight_OnClick);
            this.button1.Click += new EventHandler(ButtonMiddle_OnClick);
            this.button3.Click += new EventHandler(ButtonLeft_OnClick);

            this.MaximumSize = new Size(P.Values.WorkingArea.Width - 100, P.Values.WorkingArea.Height - 100);
            if(Debug.IsRunningInATest && !PositionOnContainerViewInTest)
                this.StartPosition = FormStartPosition.CenterScreen;
            else
                this.StartPosition = FormStartPosition.Manual;

            if (labelPicture.Visible && !string.IsNullOrEmpty(titleAndMessage))
                SetPicture("M");

            this.ResumeLayout(false);

            this.PerformLayout();

        }

        private void ApplyThemeColors()
        {
            this.button1.BackColor = P.Theme.Colors.Object;
            this.button3.BackColor = P.Theme.Colors.Object;
            this.button2.BackColor = P.Theme.Colors.Object;
            this.BackColor = P.Theme.Colors.Object;
            this.button1.ForeColor = P.Theme.Colors.NormalFore;
            this.button3.ForeColor = P.Theme.Colors.NormalFore;
            this.button2.ForeColor = P.Theme.Colors.NormalFore;
            this.ForeColor = P.Theme.Colors.NormalFore;
        }

        public void SetPicture(string imageLetter)
        {
            this.labelPicture.Text = imageLetter;
            this.labelPicture.Visible = true;
        }

        protected override void On_buttonClick(object sender, EventArgs e)
        {
            SetRespond((Button)sender, this);
        }

        private void ButtonMiddle_OnClick(object sender, EventArgs e)
        {
            try
            {
                GetButtonClick(button1?.Text)?.Invoke();                
                if (this.dontShowAgain != null && this.dontShowAgain.State == DsaOptions.Ask_to_with_pre_checked_dont_show_again)
                    this.dontShowAgain.State = DsaOptions.Always;
            }
            catch (TsuCancelException)
            {
                CancelClosing = true;
            }
            catch (Exception)
            {
                throw;
            }

            TestClosure();
        }

        private void ButtonRight_OnClick(object sender, EventArgs e)
        {
            GetButtonClick(button2?.Text)?.Invoke();
            if (this.dontShowAgain != null && this.dontShowAgain.State == DsaOptions.Ask_to_with_pre_checked_dont_show_again)
                this.dontShowAgain.State = DsaOptions.Never;
            TestClosure();
        }

        private void ButtonLeft_OnClick(object sender, EventArgs e)
        {
            GetButtonClick(button3?.Text)?.Invoke();
            if (this.dontShowAgain != null && this.dontShowAgain.State == DsaOptions.Ask_to_with_pre_checked_dont_show_again)
                throw new Exception("dont show again box shouldn't be available when 3 choices exist");
            TestClosure();
        }

        

        private void MessageModuleView_DoubleClick(object sender, EventArgs e)
        {

            if (this.WindowState == FormWindowState.Maximized)
                this.WindowState = FormWindowState.Normal;
            else
                this.WindowState = FormWindowState.Maximized;
        }
    }
}
