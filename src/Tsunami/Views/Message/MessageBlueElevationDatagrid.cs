﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using TSU.Views;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using P = TSU.Tsunami2.Preferences;

using M = TSU.Common.Measures;
using T = TSU.Tools;
using System.Globalization;
using Cac = TSU.Common.Analysis.Colors;


namespace TSU.Views
{
    public partial class MesssageBlueElevationDataGrid : TsuView
    {
        private string _respond;
        public string Respond { get { return _respond; }   }
        public string CancelString { get; set; }
        public List<string> ptNameList;
        public List<string> diffStprevA;
        public List<string> diffStprevR;
        public List<string> diffStprevP;
        public List<string> diffStnextA;
        public List<string> diffStnextR;
        public List<string> diffStnextP;
        bool showColumnPrevA;
        bool showColumnPrevR;
        bool showColumnPrevP;
        bool showColumnNextA;
        bool showColumnNextR;
        bool showColumnNextP;
        bool showCancelButton;
        List<bool> showLines;
        public MesssageBlueElevationDataGrid()
        {
            ptNameList = new List<string>();
            diffStprevA = new List<string>();
            diffStprevR = new List<string>();
            diffStprevP = new List<string>();
            diffStnextA = new List<string>();
            diffStnextR = new List<string>();
            diffStnextP = new List<string>();
            showLines = new List<bool>();
            showColumnPrevA = false;
            showColumnPrevR = false;
            showColumnPrevP = false;
            showColumnNextA = false;
            showColumnNextR = false;
            showColumnNextP = false;
            this.InitializeComponent();
            this.ApplyThemeColors();
            this.Resizable = true;
            this.Location = new System.Drawing.Point((TSU.Tsunami2.Preferences.Values.WorkingArea.Width - this.Width) / 2, (TSU.Tsunami2.Preferences.Values.WorkingArea.Height - this.Height )/ 2);
            this.AllowMovement(this);
        }
        private void ApplyThemeColors()
        {
            this.buttonLeft.BackColor = P.Theme.Colors.Object;
            this.dataGridViewBlueElevation.ColumnHeadersDefaultCellStyle.BackColor = P.Theme.Colors.Object;
            this.dataGridViewBlueElevation.ColumnHeadersDefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.dataGridViewBlueElevation.ColumnHeadersDefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            this.dataGridViewBlueElevation.ColumnHeadersDefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.dataGridViewBlueElevation.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.dataGridViewBlueElevation.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.dataGridViewBlueElevation.DefaultCellStyle.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            this.dataGridViewBlueElevation.DefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.buttonRight.BackColor = P.Theme.Colors.Object;
            this.PointName.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.PointName.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.Outward1.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.Outward1.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.Return1.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.Return1.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.Repeat1.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.Repeat1.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.Outward3.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.Outward3.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.Return3.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.Return3.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.Repeat3.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.Repeat3.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.BackColor = P.Theme.Colors.Object;
            this.PointName.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Name;
            this.Outward1.HeaderText = R.StringBlueElevation_Outward1;
            this.Return1.HeaderText = R.StringBlueElevation_Return1;
            this.Repeat1.HeaderText = R.StringBlueElevation_Repeat1;
            this.Outward3.HeaderText = R.StringBlueElevation_Outward3;
            this.Return3.HeaderText = R.StringBlueElevation_Return3;
            this.Repeat3.HeaderText = R.StringBlueElevation_Repeat3;
        }
        // Tools
        public void Colorize( Color back, Color fore)
        {
            this.BackColor = back;
            this.ForeColor = fore;
            this.titleAndMessageRichBox.BackColor = back;
            
            Color darkerColor = Cac.GetDarkerColor(BackColor);
            Color lighterColor = Cac.GetLighterColor(BackColor);
            this.BackColor = darkerColor;
            this.panel2.BackColor = back;
            this.titleAndMessageRichBox.BackColor = back;
            this.buttonLeft.BackColor = darkerColor;
            this.buttonRight.BackColor = darkerColor;
            this.titleAndMessageRichBox.ForeColor = fore;
            this.buttonLeft.ForeColor = fore;
            this.buttonRight.ForeColor = fore;
        }



        public void FillTitleAndAdjustSize(string title = "", string left = "")
        {
            SetTitle(title);
            this.buttonLeft.Text = left;
            this.buttonLeft.Visible = (this.buttonLeft.Text == "") ? false : true;
            this.CreateToolTip();
        }

        public void RemoveButtonRight()
        {
            this.tableLayoutPanel2.ColumnCount = 1;

        }


        public void SetTitle(string title)
        {
            this.titleAndMessageRichBox.Text = "";
            Rectangle screenSize =Tsunami2.Properties.View.Bounds;
            this.titleAndMessageRichBox.Text = title;
            this.titleAndMessageRichBox.SelectAll();
            Font titleFont =TSU.Tsunami2.Preferences.Theme.Fonts.Large;
            this.titleAndMessageRichBox.SelectionFont = titleFont;
            this.titleAndMessageRichBox.SelectionAlignment = HorizontalAlignment.Center;
            this.titleAndMessageRichBox.SelectionLength = 0;
            this.Top = (screenSize.Height - this.Height) / 2;
            this.Left = (screenSize.Width - this.Width) / 2;
            this.AdjustToScreenSize();
        }
        // Return
        internal void SetRespond(Button button)
        {
            _respond = button.Text;
        }

        protected virtual void On_buttonClick(object sender, EventArgs e)
        {
            Button button = ((Button)sender);
            SetRespond(button);
            this.Close();
        }

        /// <summary>
        /// Cree les tooltips pour tous les boutons
        /// </summary>
        private void CreateToolTip()
        {
            ToolTip tooltip = new ToolTip();
            tooltip.InitialDelay = 50;
            tooltip.AutoPopDelay = 5000;
            tooltip.ReshowDelay = 50;
            if (titleAndMessageRichBox != null) tooltip.SetToolTip(this.titleAndMessageRichBox, this.titleAndMessageRichBox.Text);
            tooltip.SetToolTip(this, this.Text);
            if (buttonLeft != null) tooltip.SetToolTip(this.buttonLeft, this.buttonLeft.Text);
            if (buttonRight != null) tooltip.SetToolTip(this.buttonRight, this.buttonRight.Text);
        }
        internal void AdjustToScreenSize(bool full=false)
        {
            const int DIFF = 50;
            Rectangle screenSize =Tsunami2.Properties.View.Bounds;

            if (full)
            {
                this.Height = screenSize.Height - DIFF;
                this.Width = screenSize.Width - DIFF;
                this.Top = DIFF / 3;
                this.Left = DIFF / 3;
            }
            else
            {
                if ((this.Top + this.Height) > (screenSize.Height - DIFF))
                {
                    this.Top = DIFF / 3;
                    this.Height = screenSize.Height - DIFF;
                }
                if ((this.Left + this.Width) > (screenSize.Width - DIFF))
                {
                    this.Width = screenSize.Width - 50;
                }
            }
            this.Top = (screenSize.Height - this.Height) / 2;
            this.Left = (screenSize.Width - this.Width) / 2;
        }
        /// <summary>
        /// Redraw the datagridview
        /// </summary>
        public void RedrawDataGridViewMoveLevel()
        {
            dataGridViewBlueElevation.Rows.Clear();
            List<List<object>> listRow = new List<List<object>>();
            int actualLineIndex = 0;
            List<int> index = new List<int>();
            foreach (string item in this.ptNameList)
            {
                if (this.diffStprevA[actualLineIndex] != ""
                   || this.diffStprevR[actualLineIndex] != ""
                   || this.diffStprevP[actualLineIndex] != ""
                   || this.diffStnextA[actualLineIndex] != ""
                   || this.diffStnextR[actualLineIndex] != ""
                   || this.diffStnextP[actualLineIndex] != "")
                {
                    AddNewBlueElevationInDataGridView(listRow, actualLineIndex);
                    if (actualLineIndex != ptNameList.Count - 1) AddBlankDifferenceInDataGridView(listRow);
                }
                actualLineIndex++;
            }
            int lineIndex = 0;
            foreach (List<Object> ligne in listRow)
            {
                if (lineIndex != listRow.Count-1)
                {
                    dataGridViewBlueElevation.Rows.Add(ligne.ToArray());
                }
                else
                {
                    if (ligne[0].ToString() != R.StringDataGridBlueElevation_Difference)
                    {
                        dataGridViewBlueElevation.Rows.Add(ligne.ToArray());
                    }
                }   
                lineIndex++;
            }
            for (int i = 0; i < dataGridViewBlueElevation.RowCount; i++)
            {
                dataGridViewBlueElevation.Rows[i].Height = 30; // met toutes les lignes en hauteur de 30 pour afficher correctement l'image de suppression.
                if (dataGridViewBlueElevation[0, i].Value.ToString() == R.StringDataGridBlueElevation_Difference)
                {
                    //Fait la différence entre la ligne avant et après
                    bool showLineDifference = false;
                    dataGridViewBlueElevation[0, i].Style.BackColor = P.Theme.Colors.Highlight;
                    for (int c = 1; c < dataGridViewBlueElevation.ColumnCount; c++)
                    {
                        if (dataGridViewBlueElevation[c, i - 1].Value.ToString() != "" && dataGridViewBlueElevation[c, i + 1].Value.ToString() != "")
                        {
                            int a = Convert.ToInt32(dataGridViewBlueElevation[c, i - 1].Value);
                            int b = Convert.ToInt32(dataGridViewBlueElevation[c, i + 1].Value);
                            dataGridViewBlueElevation[c, i].Value = (b - a).ToString();
                            showLineDifference = true;
                            //Met en rouge la ligne si différence > tolérance
                            if (Math.Abs(b - a) >= P.Values.GuiPrefs.ToleranceLevelBlueElevationMM * 100)
                            {
                                dataGridViewBlueElevation[c, i].Style.BackColor = P.Theme.Colors.Attention;
                            }
                            else
                            {
                                dataGridViewBlueElevation[c, i].Style.BackColor = P.Theme.Colors.Highlight;
                            }
                        }
                        else
                        {
                            dataGridViewBlueElevation[c, i].Style.BackColor = P.Theme.Colors.Highlight;
                        }
                    }
                    if (!showLineDifference) dataGridViewBlueElevation.Rows[i].Visible = false;
                }
                else
                {
                    ///Cache les lignes ou il n'y a rien
                    dataGridViewBlueElevation.Rows[i].Visible = this.showLines[i];
                }
            }
            if (showColumnPrevA == false) dataGridViewBlueElevation.Columns[1].Visible = false;
            if (showColumnPrevR == false) dataGridViewBlueElevation.Columns[2].Visible = false;
            if (showColumnPrevP == false) dataGridViewBlueElevation.Columns[3].Visible = false;
            if (showColumnNextA == false) dataGridViewBlueElevation.Columns[4].Visible = false;
            if (showColumnNextR == false) dataGridViewBlueElevation.Columns[5].Visible = false;
            if (showColumnNextP == false) dataGridViewBlueElevation.Columns[6].Visible = false;
            this.dataGridViewBlueElevation.Refresh();
        }
        /// <summary>
        /// Add a new row of move measure in the datagridview
        /// </summary>
        /// <param name="listRow"></param>
        /// <param name="item"></param>
        private void AddNewBlueElevationInDataGridView(List<List<object>> listRow, int index)
        {
            List<object> row = new List<object>();
            row.Add(""); // 0 Point Name
            row.Add(""); // 1 ST previous Outward
            row.Add(""); // 2 St previous return
            row.Add(""); // 3 ST previous repeat
            row.Add(""); // 4 ST next Outward
            row.Add(""); // 5 ST next return
            row.Add(""); // 6 ST next repeat
            row[0] = this.ptNameList[index];
            row[1] = this.diffStprevA[index];
            row[2] = this.diffStprevR[index];
            row[3] = this.diffStprevP[index];
            row[4] = this.diffStnextA[index];
            row[5] = this.diffStnextR[index];
            row[6] = this.diffStnextP[index];
            listRow.Add(row);
            if (this.diffStprevA[index] != "") showColumnPrevA = true;
            if (this.diffStprevR[index] != "") showColumnPrevR = true;
            if (this.diffStprevP[index] != "") showColumnPrevP = true;
            if (this.diffStnextA[index] != "") showColumnNextA = true;
            if (this.diffStnextR[index] != "") showColumnNextR = true;
            if (this.diffStnextP[index] != "") showColumnNextP = true;
            if (this.diffStprevA[index] != "" || this.diffStprevR[index] != "" || this.diffStprevP[index] != "" 
                || this.diffStnextA[index] != "" || this.diffStnextR[index] != "" || this.diffStnextP[index] != "")
            {
                this.showLines.Add(true);
            }
            else
            {
                this.showLines.Add(false);
            }
        }
        /// <summary>
        /// Add a new blank row of differences in the datagridview
        /// </summary>
        /// <param name="listRow"></param>
        /// <param name="item"></param>
        private void AddBlankDifferenceInDataGridView(List<List<object>> listRow)
        {
            List<object> row = new List<object>();
            row.Add(""); // 0 Point Name
            row.Add(""); // 1 ST previous Outward
            row.Add(""); // 2 St previous return
            row.Add(""); // 3 ST previous repeat
            row.Add(""); // 4 ST next Outward
            row.Add(""); // 5 ST next return
            row.Add(""); // 6 ST next repeat
            row[0] = R.StringDataGridBlueElevation_Difference;
            listRow.Add(row);
            this.showLines.Add(true);
        }
        /// <summary>
        /// Force le datagrid à reprendre le focus si on passe la souris dessus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewBlueElevation_MouseEnter(object sender, EventArgs e)
        {
            Action a = () =>
            {
                dataGridViewBlueElevation.Focus();
            };
            Tsunami2.Properties.InvokeOnApplicationDispatcher(a);
        }

        private void dataGridViewBlueElevation_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonLeft.Text.ToLower().ToCharArray()[0] || e.KeyChar == 13)
            {
                SetRespond(this.buttonLeft);
                this.Close();
            }
            if (e.KeyChar == this.buttonRight.Text.ToLower().ToCharArray()[0])
            {
                SetRespond(this.buttonRight);
                this.Close();
            }
        }

        private void buttonLeft_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonLeft.Text.ToLower().ToCharArray()[0] || e.KeyChar == 13)
            {
                SetRespond(this.buttonLeft);
                this.Close();
            }
            if (e.KeyChar == this.buttonRight.Text.ToLower().ToCharArray()[0])
            {
                SetRespond(this.buttonRight);
                this.Close();
            }
        }

        private void titleAndMessageRichBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonLeft.Text.ToLower().ToCharArray()[0] || e.KeyChar == 13)
            {
                SetRespond(this.buttonLeft);
                this.Close();
            }
            if (e.KeyChar == this.buttonRight.Text.ToLower().ToCharArray()[0])
            {
                SetRespond(this.buttonRight);
                this.Close();
            }
        }

        private void buttonRight_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonRight.Text.ToLower().ToCharArray()[0])
            {
                SetRespond(this.buttonRight);
                this.Close();
            }
            if (e.KeyChar == this.buttonLeft.Text.ToLower().ToCharArray()[0] || e.KeyChar == 13)
            {
                SetRespond(this.buttonLeft);
                this.Close();
            }
        }
    }
}
