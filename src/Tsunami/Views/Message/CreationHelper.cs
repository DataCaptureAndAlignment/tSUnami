﻿using Castle.Core.Smtp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Windows.Forms;
using TSU.Common.Analysis;
using TSU.Common.Elements;
using R = TSU.Properties.Resources;

namespace TSU.Views.Message
{
    internal static class CreationHelper
    {
        internal static Control GetPreparedDsa(DsaFlag dontShowAgain, FlowLayoutPanel flowLayoutPanel1)
        {
            CheckBox cb = new CheckBox()
            {
                Text = "Don't show again",
                Checked = dontShowAgain.State == DsaOptions.Ask_to_with_pre_checked_dont_show_again,
                Font = Tsunami2.Preferences.Theme.Fonts.Normal,
                Height = Tsunami2.Preferences.Theme.ButtonHeight,
                AutoSize = true
            };

            RenameButtonsIfDsaChecked(cb.Checked, flowLayoutPanel1); //to do it straight forward if the precheck is set

            cb.CheckedChanged += delegate
            {
                if (cb.Checked)
                    dontShowAgain.State = DsaOptions.Ask_to_with_pre_checked_dont_show_again;

                RenameButtonsIfDsaChecked(cb.Checked, flowLayoutPanel1);

            };
            return cb;
        }

        private static void RenameButtonsIfDsaChecked(bool @checked, FlowLayoutPanel flowLayoutPanel1)
        {
            var b1 = flowLayoutPanel1.Controls["button1"];
            var b2 = flowLayoutPanel1.Controls["button2"];

            if (b1 is Button but1)
            {
                if (but1.Text == R.T_YES && @checked)
                    but1.Text = R.T_ALWAYS;

                if (but1.Text == R.T_ALWAYS && !@checked)
                    but1.Text = R.T_YES;

                if (b2 is Button but2)
                {
                    if (but2.Text == R.T_NO && @checked)
                        but2.Text = R.T_NEVER;

                    if (but2.Text == R.T_NEVER && !@checked)
                        but2.Text = R.T_NO;
                }
            }
        }

        internal static Control GetPreparedTextBox(string text)
        {
            TextBox tb = new TextBox()
            {
                Text = text,
                Font = Tsunami2.Preferences.Theme.Fonts.Large,
                Margin = new Padding(5)
            };
            tb.Enter += delegate
            {
                tb.Focus();
                tb.SelectionStart = tb.Text.Length; // add some logic if length is 0
                tb.SelectionLength = 0;
            };
            return tb;
        }

        internal static TableLayoutPanel GetTableLayoutForRenamingPoint(string nameByDefault, List<string> existingNames, List<string> titles = null)
        {
            List<string> nameParts = nameByDefault.Split('.').ToList();
            TableLayoutPanel TPL = GetTableLayoutForRenamingPoint(nameParts, existingNames, titles);
            return TPL;
        }

        internal static TableLayoutPanel GetTableLayoutForRenamingPoint(List<string> names, List<string> existingNames, List<string> titles = null)
        {
            MessageTsu GetMessageView(Control control)
            {
                while (control != null && !(control is MessageTsu))
                    control = control.Parent;

                if (control != null)
                    return (MessageTsu)control;
                else
                    return null;
            }

            List<string> nameParts = names;
            string name = string.Join(".", nameParts);
            int forcedPartsCout = 4;
            titles = titles ?? new List<string> { "Acc/Zone", "Class", "Numero", "Point" };


            // create input boxes
            TableLayoutPanel TLP = GetATableLayout(numberOfRows: 3, numberOfColumns: forcedPartsCout);



            // add label for separated boxes
            Label[] labels = new Label[forcedPartsCout];
            for (int i = 0; i < forcedPartsCout; i++)
            {
                labels[i] = GetALabel(text: titles[i]);
                TLP.Controls.Add(labels[i]);
            }

            // fill separated textboxes
            TextBox[] textBoxes = new TextBox[forcedPartsCout];
            for (int i = forcedPartsCout - 1; i > -1; i--)
            {
                string part = nameParts.Count > i ? nameParts[i] : "";
                textBoxes[i] = GetATextBox(part, titles[i]);
            }

            // add separated textboxes
            for (int i = 0; i < forcedPartsCout; i++)
            {
                TLP.Controls.Add(textBoxes[i]);
            }

            // add MergedBoxes and its title
            Label mergedLabel = GetALabel("Merged point name:");
            TLP.Controls.Add(mergedLabel);

            ComboBox mergedBox = GetAComboBox(text: name, name: "MergedPointName");
            TLP.Controls.Add(mergedBox);
            TLP.SetColumnSpan(mergedBox, forcedPartsCout - 1);// spread the box on 3 cells

            mergedBox.Items.Add(name);
            foreach (string item in Tools.Conversions.StringManipulation.GetSimilarNames(name, existingNames))
            {
                mergedBox.Items.Add(item);
            }

            // set merged-box behaviour
            mergedBox.TextChanged += delegate (object sender, EventArgs e)
            {
                if (!mergedBox.Enabled)
                    return;
                List<string> newNameIn4Parts = Names.Get4AsgParts(mergedBox.Text);

                // Fill boxes
                for (int i = 3; i > -1; i--) // fill boxes from last to first
                {
                    if (newNameIn4Parts.Count > 0)
                    {
                        textBoxes[i].Text = newNameIn4Parts[newNameIn4Parts.Count - 1].ToUpper();
                        newNameIn4Parts.Remove(newNameIn4Parts[newNameIn4Parts.Count-1]);
                    }
                    else
                        textBoxes[i].Text = "";
                }
            };

            // Set seperated boxes behaviours
            foreach (var tb in textBoxes)
            {
                tb.TextChanged += delegate (object sender, EventArgs e)
                {
                    if (((TextBox)sender).Modified)
                    {
                        string s = "";
                        for (int j = 0; j < textBoxes.Length; j++)
                        {
                            if (textBoxes[j].Text.Length > 15)
                            {
                                new MessageInput(MessageType.Warning, "Geode allows maximum 15 characters per field, you may have problems later").Show();
                            }

                            if (s != "" && textBoxes[j].Enabled) s += ".";
                            s += textBoxes[j].Text;
                        }
                        mergedBox.Enabled = false;
                        mergedBox.Text = s;
                        mergedBox.Enabled = true;
                    }
                };
            }

            // On enter press, validate messageTsu
            foreach (Control item in TLP.Controls)
            {
                if (item is TextBox || item is ComboBox)
                {
                    item.KeyPress += delegate (object sender, KeyPressEventArgs e)
                    {
                        // on enter press, validate messageTsu
                        if (e.KeyChar == '\r')
                        {
                            MessageTsu m = GetMessageView((Control)sender);
                            if (mergedBox.Text.Replace(".", "").Trim() == "")
                            {
                                new MessageInput(MessageType.Critical, "Wrong Name").Show();
                                return;
                            }

                            if (m != null)
                            {
                                m.SetRespond(null, m);
                                m.Hide();
                            }
                        }
                    };
                }
            }
            return TLP;
        }



        private static TextBox GetATextBox(string text, string name = "DefaultName")
        {
            TextBox t = new TextBox()
            {
                ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore,
                BackColor = Tsunami2.Preferences.Theme.Colors.LightBackground,
                TextAlign = HorizontalAlignment.Center,
                Font = Tsunami2.Preferences.Theme.Fonts.Normal,
                Dock = DockStyle.Fill,
                Text = text,
                Name = name
            };
            return t;
        }

        internal static ComboBox GetAComboBox(string text, string name = "DefaultName")
        {
            ComboBox t = new ComboBox()
            {
                ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore,
                BackColor = Tsunami2.Preferences.Theme.Colors.LightBackground,
                Font = Tsunami2.Preferences.Theme.Fonts.MonospaceFont,
                Dock = DockStyle.Fill,
                Text = text,
                Name = name
            };
            return t;
        }


        internal static ComboBox GetComboBox(BindingSource bs)
        {
            ComboBox t = new ComboBox()
            {
                ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore,
                BackColor = Tsunami2.Preferences.Theme.Colors.LightBackground,
                Font = Tsunami2.Preferences.Theme.Fonts.Normal,
                Dock = DockStyle.Fill,
                DataSource = bs
            };
            return t;
        }

        private static Label GetALabel(string text)
        {
            Label l = new Label()
            {
                Text = text,
                ForeColor = Tsunami2.Preferences.Theme.Colors.NormalFore,
                BackColor = Tsunami2.Preferences.Theme.Colors.Transparent,
                Dock = DockStyle.Fill,
                TextAlign = ContentAlignment.MiddleCenter,
                Font = Tsunami2.Preferences.Theme.Fonts.Normal
            };
            return l;
        }

        private static TableLayoutPanel GetATableLayout(int numberOfRows, int numberOfColumns)
        {
            TableLayoutPanel TLP = new TableLayoutPanel()
            {
                BackColor = Tsunami2.Preferences.Theme.Colors.Object,
                AutoSizeMode = AutoSizeMode.GrowOnly,
                GrowStyle = TableLayoutPanelGrowStyle.AddRows,
                RowCount = numberOfRows,
                ColumnCount = numberOfColumns,
                AutoSize = true
            };

            float columnSizeInPercent = 100f / numberOfColumns;
            float rowSizeInPercent = 100f / numberOfRows;

            for (int i = 0; i < numberOfColumns; i++)
            {
                TLP.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, columnSizeInPercent));
            }
            TLP.RowStyles.Add(new RowStyle(SizeType.Percent, rowSizeInPercent));
            TLP.RowStyles.Add(new RowStyle(SizeType.Percent, rowSizeInPercent));
            TLP.RowStyles.Add(new RowStyle(SizeType.Percent, rowSizeInPercent));
            TLP.RowStyles.Add(new RowStyle(SizeType.Percent, rowSizeInPercent));

            return TLP;
        }

        internal static TableLayoutPanel GetTableLayoutForSigmas(List<string> sigmaTitles, List<string> sigmaValues)
        {

            int forcedPartsCout = sigmaValues.Count;

            TableLayoutPanel TLP = GetATableLayout(2, forcedPartsCout);

            // add label for separated boxes
            Label[] labels = new Label[forcedPartsCout];
            for (int i = 0; i < forcedPartsCout; i++)
            {
                labels[i] = GetALabel(sigmaTitles[i]);
                TLP.Controls.Add(labels[i]);
            }

            // add separated textboxes
            TextBox[] textBoxes = new TextBox[forcedPartsCout];
            for (int i = 0; i < forcedPartsCout; i++)
            {
                textBoxes[i] = GetATextBox(sigmaValues[i], sigmaTitles[i]);
                TLP.Controls.Add(textBoxes[i]);
            }

            // On enter press, validate messageTsu
            foreach (var item in TLP.Controls)
            {
                if (item is TextBox tb)
                    tb.KeyPress += delegate (object sender, KeyPressEventArgs e)
                    {
                        // on enter press, validate messageTsu
                        if (e.KeyChar == '\r')
                        {
                            MessageTsu m = GetMessageView((TextBox)sender);

                            if (m != null)
                            {
                                m.SetRespond(null, m);
                                m.Hide();
                            }
                        }
                    };
            }

            return TLP;
        }

        private static MessageTsu GetMessageView(Control control)
        {
            while (control != null || control is MessageTsu)
                control = control.Parent;

            if (control != null)
                return (MessageTsu)control;
            else
                return null;
        }

        internal static List<Control> GetSocketCodeComboBox(List<SocketCode> codes, SocketCode preSelected, string displayMember, string valueMember)
        {
            List<Control> controls = new List<Control>();
            BindingSource bs = new BindingSource
            {
                DataSource = codes
            };
            if (preSelected == null) preSelected = codes[0];
            var combo = GetAComboBox(preSelected.NameForIH, name: "CodesCombo");
            combo.DataSource = bs;
            combo.DisplayMember = displayMember;
            combo.ValueMember = valueMember;

            bs.ResetBindings(false); // Apply data binding
            combo.SelectedItem = preSelected;

            // find the good width
            int minWidth;
            bool imageWanted = false;
            switch (displayMember)
            {
                case "NameForIH":
                    minWidth = 1300;
                    break;
                case "NameForPTH":
                    minWidth = 1300;
                    imageWanted = true;
                    break;
                default:
                    minWidth = 1300;
                    break;
            }
            combo.MinimumSize = new Size(minWidth, 0);

            bool selectionDone = false;
            combo.SelectedIndexChanged += delegate
            {
                selectionDone = true; // because visible change is raised for unknwon reason several time even after selection
            };
            combo.VisibleChanged += delegate
            {
                if (!selectionDone)
                    combo.SelectedItem = preSelected;
            };
            combo.KeyUp += delegate (object sender, KeyEventArgs e)
            {
                selectionDone = true;// because visible change is raised for unknwon reason several time even after selection
                if (e.KeyCode == Keys.Enter )
                {
                    var input = combo.Text;
                    foreach (var code in codes)
                    {
                        if (code.Id == input)
                        {
                            combo.SelectedItem = code;
                        }
                    }
                }
            };
            controls.Add(combo);

            if (imageWanted)
            {
                PictureBox pb = new PictureBox()
                {
                    BackColor = Tsunami2.Preferences.Theme.Colors.Transparent,
                    Image = R.SocketCode,
                    SizeMode = PictureBoxSizeMode.AutoSize,
                    Visible = true,
                };
                pb.MinimumSize = pb.PreferredSize;
                controls.Add(pb);
            }
            return controls;
        }

        internal static List<string> GetYesNoButtons()
        {
            return new List<string>() { R.T_YES, R.T_NO };
        }

        internal static List<string> GetOKCancelButtons()
        {
            return new List<string>() { R.T_OK, R.T_CANCEL };
        }

        internal static Control GetPreparedLabel(string text)
        {
            Label l = new Label()
            {
                Text = "\r\n" + text,
                Font = Tsunami2.Preferences.Theme.Fonts.Normal,
                AutoSize = true
            };
            return l;
        }

        internal static string GetNiceMessageFromInnerExceptions(string message, Exception ex)
        {
            string title = message;
            string causedBy = " -> ";
            Exception exception = ex;
            string description = causedBy + exception.Message;
            while (exception.InnerException != null)
            {
                description = causedBy + exception.InnerException.Message + description;
                exception = exception.InnerException;
            }
            description = $"Initially caused by: " + "\r\n" + description;
            return $"{title};{description}";
        }

        internal static Size GetPreferredHeight(string text, Font font, int maxWidth)
        {
            Size proposedSize = new Size(maxWidth, int.MaxValue);
            Size rectangle = TextRenderer.MeasureText(text, font, proposedSize, TextFormatFlags.WordBreak);
            return rectangle;
        }
    }
}
