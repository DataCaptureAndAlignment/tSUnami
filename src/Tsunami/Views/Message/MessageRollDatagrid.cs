﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TSU.Common.Instruments;
using TSU.Views.Message;
using P = TSU.Tsunami2.Preferences;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Views
{
    public partial class MessageRollDataGridTSU : TsuView
    {
        private string _respond;
        public string Respond { get { return _respond; } }
        public string CancelString { get; set; }
        public Tilt.Station newStationTilt;
        public Tilt.Station oldStationTilt;
        internal int rowActiveCell = 0;
        internal int columnActiveCell = 1;
        internal bool setCurrentCell = false;
        internal bool showToleranceColors = true;
        internal Instrument _usedInstrument = null;
        internal Common.Instruments.Device.WYLER.Module _instrumentModule = null;
        private double na { get; set; } = P.Values.na;
        public MessageRollDataGridTSU()
        {
            this.newStationTilt = new Tilt.Station();
            this.oldStationTilt = new Tilt.Station();
            this.InitializeComponent();
            this.ApplyThemeColors();
            this.Resizable = true;
            this.Location = new Point((P.Values.WorkingArea.Width - this.Width) / 2, (P.Values.WorkingArea.Height - this.Height) / 2);
            this.AllowMovement(this);
        }
        private void ApplyThemeColors()
        {
            this.buttonLeft.BackColor = P.Theme.Colors.Object;
            this.buttonRight.BackColor = P.Theme.Colors.Object;
            this.buttonMeasure.BackColor = P.Theme.Colors.Object;
            this.dataGridViewMesTilt.ColumnHeadersDefaultCellStyle.BackColor = P.Theme.Colors.Object;
            this.dataGridViewMesTilt.ColumnHeadersDefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.dataGridViewMesTilt.ColumnHeadersDefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            this.dataGridViewMesTilt.ColumnHeadersDefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.dataGridViewMesTilt.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.dataGridViewMesTilt.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.dataGridViewMesTilt.DefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            this.dataGridViewMesTilt.DefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.MeasureNumber.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.MeasureNumber.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.MeasureNumber.DefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            this.MeasureNumber.DefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.MeasureNumber.HeaderText = R.StringTilt_DataGrid_MesureNumber;
            this.BeamDirection.HeaderText = R.StringTilt_DataGrid_TiltBeam;
            this.BeamDirection.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.BeamDirection.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.BeamDirection.DefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            this.BeamDirection.DefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.BeamOpposite.HeaderText = R.StringTilt_DataGrid_TiltOpposite;
            this.BeamOpposite.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.BeamOpposite.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.BeamOpposite.DefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            this.BeamOpposite.DefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.Average.HeaderText = R.StringTilt_DataGrid_TiltAverage;
            this.Average.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.Average.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.Average.DefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            this.Average.DefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.Offset.HeaderText = R.StringTilt_DataGrid_Offset;
            this.Offset.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.Offset.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.Offset.DefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            this.Offset.DefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.BackColor = P.Theme.Colors.Object;
            this.dataGridViewMesTilt.BackgroundColor = P.Theme.Colors.Background;
        }


        // Tools
        public void Colorize(Color back, Color fore)
        {
            this.BackColor = back;
            this.ForeColor = fore;
            this.titleAndMessageRichBox.BackColor = back;

            Color darkerColor = Common.Analysis.Colors.GetDarkerColor(BackColor);
            Color lighterColor = Common.Analysis.Colors.GetLighterColor(BackColor);
            this.BackColor = darkerColor;
            this.panel2.BackColor = back;
            this.titleAndMessageRichBox.BackColor = back;
            this.buttonRight.BackColor = darkerColor;
            this.buttonLeft.BackColor = darkerColor;
            this.buttonMeasure.BackColor = darkerColor;
            this.titleAndMessageRichBox.ForeColor = fore;
            this.buttonRight.ForeColor = fore;
            this.buttonLeft.ForeColor = fore;
            this.buttonMeasure.ForeColor = fore;
        }



        public void FillTitleAndAdjustSize(string title = "", string left = "", string right = "")
        {
            SetTitle(title);
            this.buttonRight.Text = right;
            this.buttonLeft.Text = left;
            this.buttonMeasure.Text = "Measure";
            this.buttonMeasure.Visible = (this._usedInstrument is Common.Instruments.Device.WYLER.Instrument) ? true : false;
            this.buttonRight.Visible = (this.buttonRight.Text == "") ? false : true;
            this.buttonLeft.Visible = (this.buttonLeft.Text == "") ? false : true;
            this.CreateToolTip();
        }
        /// <summary>
        /// Cree les tooltips pour tous les boutons
        /// </summary>
        private void CreateToolTip()
        {
            ToolTip tooltip = new ToolTip();
            tooltip.InitialDelay = 50;
            tooltip.AutoPopDelay = 5000;
            tooltip.ReshowDelay = 50;
            if (titleAndMessageRichBox != null) tooltip.SetToolTip(this.titleAndMessageRichBox, this.titleAndMessageRichBox.Text);
            tooltip.SetToolTip(this, this.Text);
            if (buttonLeft != null) tooltip.SetToolTip(this.buttonLeft, this.buttonLeft.Text);
            if (buttonRight != null) tooltip.SetToolTip(this.buttonRight, this.buttonRight.Text);
            if (buttonMeasure != null) tooltip.SetToolTip(this.buttonMeasure, "Measure");

        }


        public void SetTitle(string title)
        {
            this.titleAndMessageRichBox.Text = "";
            Rectangle screenSize = Tsunami2.Properties.View.Bounds;
            this.titleAndMessageRichBox.Text = title;
            this.titleAndMessageRichBox.SelectAll();
            Font titleFont = P.Theme.Fonts.Large;
            this.titleAndMessageRichBox.SelectionFont = titleFont;
            this.titleAndMessageRichBox.SelectionAlignment = HorizontalAlignment.Center;
            this.titleAndMessageRichBox.SelectionLength = 0;
            this.Top = (screenSize.Height - this.Height) / 2;
            this.Left = (screenSize.Width - this.Width) / 2;
            this.AdjustToScreenSize();
        }
        // Return
        internal void SetRespond(Button button)
        {
            _respond = button.Text;
        }

        protected virtual void On_buttonClick(object sender, EventArgs e)
        {
            Button button = ((Button)sender);
            SetRespond(button);
            if (button == buttonLeft)
            {
                this.CheckDatagridIsInEditMode();
                this.TryAction(this.UpdateOldStationTilt);
            }
            if (this._instrumentModule != null && this._instrumentModule.wyBusConnected)
            {
                this._instrumentModule._wyBus?.Dispose();
                this._instrumentModule._wyBus = null;
                this._instrumentModule.wyBusConnected = false;
            }
            this.Close();
        }
        /// <summary>
        /// Check if datagrid is in edit mode and validate it before leaving
        /// </summary>
        internal void CheckDatagridIsInEditMode()
        {
            if (dataGridViewMesTilt.IsCurrentCellInEditMode)
            {
                //this.dataGridViewLevel.CellValueChanged -= this.dataGridViewLevel_CellValueChanged;
                int saveRow = rowActiveCell;
                int saveColumn = columnActiveCell;
                //Try car de temps en temps endEdit peut faire une exception
                try
                {
                    dataGridViewMesTilt.EndEdit();

                }
                catch (Exception ex)
                {
                    ex.Data.Clear();
                }
                //this.dataGridViewLevel.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewLevel_CellValueChanged);
                rowActiveCell = saveRow;
                columnActiveCell = saveColumn;
            }
        }
        /// <summary>
        /// Met à jour les mesures dans l'ancienne station tilt
        /// </summary>
        private void UpdateOldStationTilt()
        {
            //int index = 0;
            //foreach (Tilt.Measure item in newStationTilt._MeasuresTilt)
            //{
            //    oldStationTilt._MeasuresTilt[index].._ReadingBeamDirection.Value = item._ReadingBeamDirection.Value;
            //    oldStationTilt._MeasuresTilt[index]._ReadingOppositeBeam.Value = item._ReadingOppositeBeam.Value;
            //    index++;
            //}
            oldStationTilt._MeasuresTilt = newStationTilt._MeasuresTilt;
            oldStationTilt.ParametersBasic.LastChanged = DateTime.Now;
        }

        internal void AdjustToScreenSize(bool full = false)
        {
            const int DIFF = 50;
            Rectangle screenSize = Tsunami2.Properties.View.Bounds;

            if (full)
            {
                this.Height = screenSize.Height - DIFF;
                this.Width = screenSize.Width - DIFF;
                this.Top = DIFF / 3;
                this.Left = DIFF / 3;
            }
            else
            {
                if ((this.Top + this.Height) > (screenSize.Height - DIFF))
                {
                    this.Top = DIFF / 3;
                    this.Height = screenSize.Height - DIFF;
                }
                if ((this.Left + this.Width) > (screenSize.Width - DIFF))
                {
                    this.Width = screenSize.Width - 50;
                }
            }
            this.Top = (screenSize.Height - this.Height) / 2;
            this.Left = (screenSize.Width - this.Width) / 2;
        }
        /// <summary>
        /// Redraw the datagridview
        /// </summary>
        public void RedrawDataGridViewMesTilt()
        {
            this.dataGridViewMesTilt.CellValueChanged -= this.dataGridViewMesTilt_CellValueChanged;
            this.dataGridViewMesTilt.CurrentCellChanged -= this.dataGridViewMesTilt_CurrentCellChanged;
            dataGridViewMesTilt.Rows.Clear();
            List<List<object>> listRow = new List<List<object>>();
            int actualLineIndex = 0;
            foreach (Tilt.Measure item in this.newStationTilt._MeasuresTilt)
            {
                AddNewMeasureRowInDataGridView(listRow, item, actualLineIndex);
                actualLineIndex++;
            }
            //AddEmptyRowInDataGridView(listRow);
            AddAverageResultRowInDataGridView(listRow, this.newStationTilt);
            foreach (List<Object> ligne in listRow)
            {
                dataGridViewMesTilt.Rows.Add(ligne.ToArray());
            }
            for (int i = 0; i < dataGridViewMesTilt.RowCount - 1; i++)
            {
                dataGridViewMesTilt.Rows[i].Height = 30; // met toutes les lignes en hauteur de 30 pour afficher correctement l'image de suppression.
                ///Met en jaune les cases de mesure à encoder
                if (dataGridViewMesTilt[1, i].Value.ToString() != "")
                {
                    dataGridViewMesTilt[2, i].Style.BackColor = P.Theme.Colors.Action;
                    dataGridViewMesTilt[3, i].Style.BackColor = P.Theme.Colors.Action;
                }
                ///N'affiche plus temporairement les couleurs pour les tolérances en module guidé
                double p = T.Conversions.Numbers.ToDouble(dataGridViewMesTilt[5, i].Value.ToString(), true, -9999);
                if (p != -9999 && showToleranceColors)
                {
                    dataGridViewMesTilt[5, i].Style.BackColor = (Math.Abs(p) > this.newStationTilt.TiltParameters._Tolerance * 1000) ? P.Theme.Colors.Bad : dataGridViewMesTilt.Rows[i].DefaultCellStyle.BackColor;
                    dataGridViewMesTilt[5, i].Style.ForeColor = (Math.Abs(p) > this.newStationTilt.TiltParameters._Tolerance * 1000) ? dataGridViewMesTilt.Rows[i].DefaultCellStyle.ForeColor : P.Theme.Colors.Good;
                }
            }
            DataGridViewCellStyle dataGridViewCellStyleLastRow = new DataGridViewCellStyle();
            dataGridViewCellStyleLastRow.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyleLastRow.BackColor = P.Theme.Colors.DarkBackground;
            dataGridViewCellStyleLastRow.Font = new Font("Calibri", 15F, FontStyle.Underline);
            dataGridViewCellStyleLastRow.ForeColor = P.Theme.Colors.NormalFore;
            dataGridViewCellStyleLastRow.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            dataGridViewCellStyleLastRow.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            dataGridViewCellStyleLastRow.WrapMode = DataGridViewTriState.False;
            dataGridViewCellStyleLastRow.BackColor = P.Theme.Colors.Object;
            dataGridViewMesTilt.Rows[dataGridViewMesTilt.RowCount - 1].DefaultCellStyle = dataGridViewCellStyleLastRow;
            dataGridViewMesTilt.Rows[dataGridViewMesTilt.RowCount - 1].Height = 30;
            // Met en couleur et en read only les moyennes sur la dernière ligne TSU-1664-tout vert ou tout rouge
            ///N'affiche plus temporairement les couleurs pour les tolérances en module guidé
            double avg = T.Conversions.Numbers.ToDouble(dataGridViewMesTilt[5, dataGridViewMesTilt.RowCount - 1].Value.ToString(), true, -9999);
            //if (avg != -9999) dataGridViewMesTilt[2, dataGridViewMesTilt.RowCount - 1].Style.ForeColor = 
            //        (Math.Abs(avg) > this.newStationTilt.TiltParameters._Tolerance * 1000) ? 
            //        dataGridViewMesTilt.Rows[dataGridViewMesTilt.RowCount - 1].DefaultCellStyle.ForeColor : Tsunami2.Preferences.Theme._Colors.Good;
            //if (avg != -9999) dataGridViewMesTilt[2, dataGridViewMesTilt.RowCount - 1].Style.BackColor = 
            //        (Math.Abs(avg) > this.newStationTilt.TiltParameters._Tolerance * 1000) ? 
            //        Tsunami2.Preferences.Theme._Colors.Bad : dataGridViewMesTilt.Rows[dataGridViewMesTilt.RowCount - 1].DefaultCellStyle.BackColor;
            //if (avg != -9999) dataGridViewMesTilt[3, dataGridViewMesTilt.RowCount - 1].Style.ForeColor = 
            //        (Math.Abs(avg) > this.newStationTilt.TiltParameters._Tolerance * 1000) ? 
            //        dataGridViewMesTilt.Rows[dataGridViewMesTilt.RowCount - 1].DefaultCellStyle.ForeColor : Tsunami2.Preferences.Theme._Colors.Good;
            //if (avg != -9999) dataGridViewMesTilt[3, dataGridViewMesTilt.RowCount - 1].Style.BackColor = 
            //        (Math.Abs(avg) > this.newStationTilt.TiltParameters._Tolerance * 1000) ? 
            //        Tsunami2.Preferences.Theme._Colors.Bad : dataGridViewMesTilt.Rows[dataGridViewMesTilt.RowCount - 1].DefaultCellStyle.BackColor;
            //if (avg != -9999) dataGridViewMesTilt[4, dataGridViewMesTilt.RowCount - 1].Style.ForeColor =
            //        (Math.Abs(avg) > this.newStationTilt.TiltParameters._Tolerance * 1000) ?
            //        dataGridViewMesTilt.Rows[dataGridViewMesTilt.RowCount - 1].DefaultCellStyle.ForeColor : Tsunami2.Preferences.Theme._Colors.Good;
            //if (avg != -9999) dataGridViewMesTilt[4, dataGridViewMesTilt.RowCount - 1].Style.BackColor =
            //        (Math.Abs(avg) > this.newStationTilt.TiltParameters._Tolerance * 1000) ?
            //        Tsunami2.Preferences.Theme._Colors.Bad : dataGridViewMesTilt.Rows[dataGridViewMesTilt.RowCount - 1].DefaultCellStyle.BackColor;
            if (avg != -9999 && showToleranceColors) dataGridViewMesTilt[5, dataGridViewMesTilt.RowCount - 1].Style.ForeColor =
                    (Math.Abs(avg) > this.newStationTilt.TiltParameters._Tolerance * 1000) ?
                    dataGridViewMesTilt.Rows[dataGridViewMesTilt.RowCount - 1].DefaultCellStyle.ForeColor : P.Theme.Colors.Good;
            if (avg != -9999 && showToleranceColors) dataGridViewMesTilt[5, dataGridViewMesTilt.RowCount - 1].Style.BackColor =
                    (Math.Abs(avg) > this.newStationTilt.TiltParameters._Tolerance * 1000) ?
                    P.Theme.Colors.Bad : dataGridViewMesTilt.Rows[dataGridViewMesTilt.RowCount - 1].DefaultCellStyle.BackColor;

            for (int colIndex = 0; colIndex < dataGridViewMesTilt.Columns.Count; colIndex++)
            {
                dataGridViewMesTilt[colIndex, dataGridViewMesTilt.RowCount - 1].ReadOnly = true;
            }
            for (int rowIndex = 0; rowIndex < dataGridViewMesTilt.Rows.Count; rowIndex++)
            {
                dataGridViewMesTilt[1, rowIndex].ReadOnly = true;
            }

            dataGridViewMesTilt.Columns[1].ReadOnly = true;
            this.dataGridViewMesTilt.Refresh();
            this.dataGridViewMesTilt.CellValueChanged += new DataGridViewCellEventHandler(this.dataGridViewMesTilt_CellValueChanged);
            this.dataGridViewMesTilt.CurrentCellChanged += new EventHandler(this.dataGridViewMesTilt_CurrentCellChanged);
        }
        /// <summary>
        /// Add a new row of tilt meaure in the datagridview
        /// </summary>
        /// <param name="listRow"></param>
        /// <param name="item"></param>
        private void AddNewMeasureRowInDataGridView(List<List<object>> listRow, Tilt.Measure item, int index)
        {
            List<object> row = new List<object>();
            row.Add(R.Tilt_Case_Vide); //0 Delete mes
            row.Add(""); // 1 MesureNumber
            row.Add(""); // 2 BeamDirection
            row.Add(""); // 3 BeamOpposite
            row.Add(""); // 4 Average
            row.Add(""); // 5 Offset
            listRow.Add(row);
            //Ne permet pas d'effacer une mesure non vide ou si on n'a qu'une seule mesure
            if (this.newStationTilt._MeasuresTilt.Count > 1)
                listRow[listRow.Count - 1][0] = R.Tilt_Delete;
            listRow[listRow.Count - 1][1] = index + 1;
            if (item._ReadingBeamDirection.Value != na)
                listRow[listRow.Count - 1][2] = Tilt.Module.FormatAngleInMrad(item._ReadingBeamDirection.Value);
            if (item._ReadingOppositeBeam.Value != na)
                listRow[listRow.Count - 1][3] = Tilt.Module.FormatAngleInMrad(item._ReadingOppositeBeam.Value);
            if (item._AverageBothDirection.Value != na)
                listRow[listRow.Count - 1][4] = Tilt.Module.FormatAngleInMrad(item._AverageBothDirection.Value);
            if (item._Ecart.Value != na)
                listRow[listRow.Count - 1][5] = Tilt.Module.FormatAngleInMrad(item._Ecart.Value);
        }
        private void AddEmptyRowInDataGridView(List<List<object>> listRow)
        {
            List<object> row = new List<object>();
            row.Add(R.Tilt_Case_Vide); //0 Delete mes
            row.Add(""); // 1 MesureNumber
            row.Add(""); // 2 BeamDirection
            row.Add(""); // 3 BeamOpposite
            row.Add(""); // 4 Average
            row.Add(""); // 5 Offset
            listRow.Add(row);
        }
        private void AddAverageResultRowInDataGridView(List<List<object>> listRow, Tilt.Station item)
        {
            List<object> row = new List<object>();
            row.Add(R.Add_Tilt); //0 Add Mesure
            row.Add(""); // 1 MesureNumber
            row.Add(""); // 2 BeamDirection
            row.Add(""); // 3 BeamOpposite
            row.Add(""); // 4 Average
            row.Add(""); // 5 Offset
            listRow.Add(row);
            listRow[listRow.Count - 1][1] = R.StringTilt_Datagrid_Average;

            if (item._MeasureAverageBeamDirection.Value != 9999)
                listRow[listRow.Count - 1][2] = Tilt.Module.FormatAngleInMrad(item._MeasureAverageBeamDirection.Value);
            if (item._MeasureAverageOppositeDirection.Value != 9999)
                listRow[listRow.Count - 1][3] = Tilt.Module.FormatAngleInMrad(item._MeasureAverageOppositeDirection.Value);
            if (item._MeasureAverageBothDirection.Value != 9999)
                listRow[listRow.Count - 1][4] = Tilt.Module.FormatAngleInMrad(item._MeasureAverageBothDirection.Value);
            if (item._EcartTheo.Value != 9999)
                listRow[listRow.Count - 1][5] = Tilt.Module.FormatAngleInMrad(item._EcartTheo.Value);
        }
        private void dataGridViewMesTilt_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < dataGridViewMesTilt.Rows.Count && e.RowIndex != -1)
            {
                rowActiveCell = e.RowIndex;
            }
            if (e.ColumnIndex < dataGridViewMesTilt.Columns.Count && e.ColumnIndex != -1)
            {
                columnActiveCell = e.ColumnIndex;
            }
            /// mesure sens faisceau entree

            if (e.RowIndex >= 0 && e.ColumnIndex == 2 && (e.RowIndex <= dataGridViewMesTilt.Rows.Count - 2))
            {
                if (dataGridViewMesTilt[e.ColumnIndex, e.RowIndex].Value != null)
                {
                    double datagridNumber = T.Conversions.Numbers.ToDouble(dataGridViewMesTilt[e.ColumnIndex, e.RowIndex].Value.ToString(), true, -9999);
                    if (datagridNumber != -9999)
                    {
                        if (this.newStationTilt._MeasuresTilt[e.RowIndex]._ReadingBeamDirection.Value != datagridNumber / 1000)
                        {
                            // encode la mesure du tilt sens faisceau 
                            this.newStationTilt._MeasuresTilt[e.RowIndex]._ReadingBeamDirection.Value = datagridNumber / 1000;
                            // Récupère le sigma pour les mesures faites par un Wyler
                            if (dataGridViewMesTilt[e.ColumnIndex, e.RowIndex].Tag is DoubleValue dv
                                && Math.Abs(dv.Value - datagridNumber) < 0.001d)
                                this.newStationTilt._MeasuresTilt[e.RowIndex]._ReadingBeamDirection.Sigma = dv.Sigma / 1000;
                            //sélectionne la case tilt inverse pour encoder le tilt
                            columnActiveCell++;
                            this.newStationTilt.AverageTiltCalculation();
                            this.setCurrentCell = true;
                            this.TryAction(this.RedrawDataGridViewMesTilt);
                            this.TryAction(SetCurrentCell);
                            return;
                        }
                        else
                        {
                            //remet l'ancienne valeur dans la case au bon format
                            this.dataGridViewMesTilt.CellValueChanged -= this.dataGridViewMesTilt_CellValueChanged;
                            this.dataGridViewMesTilt[e.ColumnIndex, e.RowIndex].Value = Math.Round(datagridNumber, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                            this.dataGridViewMesTilt.CellValueChanged += new DataGridViewCellEventHandler(this.dataGridViewMesTilt_CellValueChanged);
                            return;
                        }
                    }
                    else
                    {
                        //message erreur, valeur encodée incorrecte
                        new MessageInput(MessageType.Warning, R.StringTilt_WrongRoll).Show();
                        //remet l'ancienne valeur dans la case au bon format
                        this.dataGridViewMesTilt.CellValueChanged -= this.dataGridViewMesTilt_CellValueChanged;
                        if (this.newStationTilt._MeasuresTilt[e.RowIndex]._ReadingBeamDirection.Value != na)
                        {
                            this.dataGridViewMesTilt[e.ColumnIndex, e.RowIndex].Value = Math.Round(this.newStationTilt._MeasuresTilt[e.RowIndex]._ReadingBeamDirection.Value * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                        }
                        else
                        {
                            this.dataGridViewMesTilt[e.ColumnIndex, e.RowIndex].Value = "";
                        }
                        //this.dataGridViewMesTilt[e.ColumnIndex, e.RowIndex].Value = Math.Round(datagridNumber, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                        this.dataGridViewMesTilt.CellValueChanged += new DataGridViewCellEventHandler(this.dataGridViewMesTilt_CellValueChanged);
                        return;
                    }
                }
                else
                {
                    //contenu de la case effacé
                    this.newStationTilt.ResetMeasForBeamDirectionReadingDeleted(e.RowIndex);
                    columnActiveCell++;
                    this.newStationTilt.AverageTiltCalculation();
                    this.setCurrentCell = true;
                    this.TryAction(this.RedrawDataGridViewMesTilt);
                    this.TryAction(SetCurrentCell);
                    return;
                }
            }
            /// mesure sens inverse faisceau entree
            if (e.RowIndex >= 0 && e.ColumnIndex == 3 && (e.RowIndex <= dataGridViewMesTilt.Rows.Count - 2))
            {
                if (dataGridViewMesTilt[e.ColumnIndex, e.RowIndex].Value != null)
                {
                    double datagridNumber = T.Conversions.Numbers.ToDouble(dataGridViewMesTilt[e.ColumnIndex, e.RowIndex].Value.ToString(), true, -9999);
                    if (datagridNumber != -9999)
                    {
                        if (this.newStationTilt._MeasuresTilt[e.RowIndex]._ReadingOppositeBeam.Value != datagridNumber / 1000)
                        {
                            //encode la mesure du tilt sens faisceau 
                            this.newStationTilt._MeasuresTilt[e.RowIndex]._ReadingOppositeBeam.Value = datagridNumber / 1000;
                            // Récupère le sigma pour les mesures faites par un Wyler
                            if (dataGridViewMesTilt[e.ColumnIndex, e.RowIndex].Tag is DoubleValue dv
                                && Math.Abs(dv.Value - datagridNumber) < 0.001d)
                                this.newStationTilt._MeasuresTilt[e.RowIndex]._ReadingOppositeBeam.Sigma = dv.Sigma / 1000;
                            //Sélectionne le tilt faisceau de l'élément suivant
                            if (rowActiveCell < dataGridViewMesTilt.RowCount - 2)
                            {
                                columnActiveCell = 2;
                                rowActiveCell++;
                            }
                            else
                            {
                                //passe à la dernière ligne du datagrid qui n'est pas editable pour pouvoir cliquer sur les raccourci bouton
                                columnActiveCell = 2;
                                rowActiveCell = dataGridViewMesTilt.RowCount - 1;
                            }
                            this.newStationTilt.AverageTiltCalculation();
                            this.setCurrentCell = true;
                            this.TryAction(this.RedrawDataGridViewMesTilt);
                            this.TryAction(SetCurrentCell);
                            return;
                        }
                        else
                        {
                            //remet l'ancienne valeur dans la case au bon format
                            this.dataGridViewMesTilt.CellValueChanged -= this.dataGridViewMesTilt_CellValueChanged;
                            this.dataGridViewMesTilt[e.ColumnIndex, e.RowIndex].Value = Math.Round(datagridNumber, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                            this.dataGridViewMesTilt.CellValueChanged += new DataGridViewCellEventHandler(this.dataGridViewMesTilt_CellValueChanged);
                            return;
                        }

                    }
                    else
                    {
                        //message erreur, valeur encodée incorrecte
                        new MessageInput(MessageType.Warning, R.StringTilt_WrongRoll).Show();
                        //remet l'ancienne valeur dans la case au bon format
                        this.dataGridViewMesTilt.CellValueChanged -= this.dataGridViewMesTilt_CellValueChanged;
                        if (this.newStationTilt._MeasuresTilt[e.RowIndex]._ReadingOppositeBeam.Value != na)
                        {
                            this.dataGridViewMesTilt[e.ColumnIndex, e.RowIndex].Value = Math.Round(this.newStationTilt._MeasuresTilt[e.RowIndex]._ReadingOppositeBeam.Value * 1000, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                        }
                        else
                        {
                            this.dataGridViewMesTilt[e.ColumnIndex, e.RowIndex].Value = "";
                        }
                        //this.dataGridViewMesTilt[e.ColumnIndex, e.RowIndex].Value = Math.Round(datagridNumber, 2).ToString("F2", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                        this.dataGridViewMesTilt.CellValueChanged += new DataGridViewCellEventHandler(this.dataGridViewMesTilt_CellValueChanged);
                        return;
                    }
                }
                else
                {
                    //contenu de la case effacé
                    this.newStationTilt.ResetMeasForBeamOppositeDirectionReadingDeleted(e.RowIndex);
                    //Sélectionne le tilt faisceau de l'élément suivant
                    if (rowActiveCell < dataGridViewMesTilt.RowCount - 2)
                    {
                        columnActiveCell = 2;
                        rowActiveCell++;
                    }
                    this.newStationTilt.AverageTiltCalculation();
                    this.setCurrentCell = true;
                    this.TryAction(this.RedrawDataGridViewMesTilt);
                    this.TryAction(SetCurrentCell);
                    return;
                }
            }

        }
        /// <summary>
        /// Set the current cell in datagridview as the columnactivecell and rowactivecell
        /// </summary>
        internal void SetCurrentCell()
        {
            Action a = () =>
            {
                this.dataGridViewMesTilt.CurrentCellChanged -= this.dataGridViewMesTilt_CurrentCellChanged;
                dataGridViewMesTilt.Focus();
                dataGridViewMesTilt.ClearSelection();
                if ((columnActiveCell < this.dataGridViewMesTilt.ColumnCount) && (columnActiveCell >= 0) && (rowActiveCell < this.dataGridViewMesTilt.RowCount) && (rowActiveCell >= 0))
                {
                    dataGridViewMesTilt.CurrentCell = this.dataGridViewMesTilt[columnActiveCell, rowActiveCell];
                    dataGridViewMesTilt.Rows[rowActiveCell].Cells[columnActiveCell].Selected = true;
                }
                this.dataGridViewMesTilt.CurrentCellChanged += new EventHandler(this.dataGridViewMesTilt_CurrentCellChanged);
            };
            Tsunami2.Properties.InvokeOnApplicationDispatcher(a);
        }
        private void dataGridViewMesTilt_CurrentCellChanged(object sender, EventArgs e)
        {
            //permet de détecter quand le datagridview change automatiquement la ligne courante après avoir entré une valeur dans une case.  
            if (this.setCurrentCell == true && this.dataGridViewMesTilt.CurrentCell != null)
            {
                ////BeginInvoke crèe un processus asynchrone qui empêche de tourner en boucle lorsqu'on change la current cell
                Delegate delegateCurrentCell = new MethodInvoker(() =>
                {
                    this.SetCurrentCell();
                });
                IAsyncResult async = this.BeginInvoke(delegateCurrentCell);
                this.setCurrentCell = false;
            }
        }
        /// <summary>
        /// Evénement si on clique pour ajouter ou effacer une mesure
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewMesTilt_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewMesTilt.IsCurrentCellInEditMode) return;
            //Ajout d'une mesure
            if (e.RowIndex == dataGridViewMesTilt.RowCount - 1 && e.ColumnIndex == 0)
            {
                Tilt.Measure newMeas = new Tilt.Measure();
                this.newStationTilt._MeasuresTilt.Add(newMeas);
                this.newStationTilt.AverageTiltCalculation();
                this.TryAction(RedrawDataGridViewMesTilt);
                this.dataGridViewMesTilt.Focus();
                return;
            }
            //Efface une mesure vide si pas de mesures encodées
            if (e.RowIndex <= dataGridViewMesTilt.RowCount - 2 && e.ColumnIndex == 0)
            {
                if (dataGridViewMesTilt.RowCount > 2)
                {
                    this.newStationTilt.DeleteMeas(e.RowIndex);
                    this.newStationTilt.AverageTiltCalculation();
                    this.TryAction(RedrawDataGridViewMesTilt);
                    this.dataGridViewMesTilt.Focus();
                    return;
                }
            }
        }
        /// <summary>
        /// Force le datagrid à reprendre le focus si on passe la souris dessus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewMesTilt_MouseEnter(object sender, EventArgs e)
        {
            Action a = () =>
            {
                dataGridViewMesTilt.Focus();
            };
            Tsunami2.Properties.InvokeOnApplicationDispatcher(a);
        }
        /// <summary>
        /// Timer qui permet lors du démarrage de sélectionner la current cell après l'affichage de la fenêtre qui fait perdre le focus du datagrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            this.rowActiveCell = 0;
            this.columnActiveCell = 2;
            this.SetCurrentCell();
        }

        private void MessageDataGridTSU_Shown(object sender, EventArgs e)
        {
            timer1.Interval = 50;
            timer1.Enabled = true;
            timer1.Start();
        }

        private void titleAndMessageRichBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonLeft.Text.ToLower()[0])
            {
                ActionButtonLeft();
            }
            if (e.KeyChar == this.buttonRight.Text.ToLower()[0])
            {
                SetRespond(this.buttonRight);
                this.Close();
            }
            if (dataGridViewMesTilt.CurrentCell != null)
            {
                if ((dataGridViewMesTilt.CurrentCell.RowIndex == dataGridViewMesTilt.RowCount - 1) && e.KeyChar == 13)
                {
                    ActionButtonLeft();
                }
            }
        }

        private void dataGridViewMesTilt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonLeft.Text.ToLower()[0])
            {
                ActionButtonLeft();
            }
            if (e.KeyChar == this.buttonRight.Text.ToLower()[0])
            {
                ActionButtonRight();
            }
            if (dataGridViewMesTilt.CurrentCell != null)
            {
                if ((dataGridViewMesTilt.CurrentCell.RowIndex == dataGridViewMesTilt.RowCount - 1) && e.KeyChar == 13)
                {
                    ActionButtonLeft();
                }
            }
        }

        private void buttonLeft_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonLeft.Text.ToLower()[0])
            {
                ActionButtonLeft();
            }
            if (e.KeyChar == this.buttonRight.Text.ToLower()[0])
            {
                ActionButtonRight();
            }
            if (dataGridViewMesTilt.CurrentCell != null)
            {
                if ((dataGridViewMesTilt.CurrentCell.RowIndex == dataGridViewMesTilt.RowCount - 1) && e.KeyChar == 13)
                {
                    ActionButtonLeft();
                }
            }
        }

        private void buttonRight_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonLeft.Text.ToLower()[0])
            {
                ActionButtonLeft();
            }
            if (e.KeyChar == this.buttonRight.Text.ToLower()[0])
            {
                ActionButtonRight();
            }
            if (e.KeyChar == "measure".ToLower()[0])
            {
                ActionButtonMeasure();
            }
            if (dataGridViewMesTilt.CurrentCell != null)
            {
                if ((dataGridViewMesTilt.CurrentCell.RowIndex == dataGridViewMesTilt.RowCount - 1) && e.KeyChar == 13)
                {
                    ActionButtonLeft();
                }
            }
        }

        private void ActionButtonRight()
        {
            SetRespond(this.buttonRight);
            this.Close();
        }

        private void ActionButtonMeasure()
        {
            SetRespond(this.buttonMeasure);
            TSU.Debug.WriteInConsole("Action Measure");
            //this.Close();
        }

        private void ActionButtonLeft()
        {
            SetRespond(this.buttonLeft);
            this.CheckDatagridIsInEditMode();
            this.TryAction(this.UpdateOldStationTilt);
            this.Close();
        }

        private void buttonMeasure_Click(object sender, EventArgs e)
        {
            if (!this._instrumentModule.wyBusConnected)
            {
                Result connectionResult = this._instrumentModule.Connect();
                if (!connectionResult.Success)
                    throw new Exception(connectionResult.ErrorMessage);
            }

            if (!dataGridViewMesTilt.CurrentCell.ReadOnly && this._instrumentModule.ReadingAngle(out DoubleValue dv))
            {
                dataGridViewMesTilt.CurrentCell.Tag = dv;
                dataGridViewMesTilt.CurrentCell.Value = dv.Value;
            }
        }
    }
}
