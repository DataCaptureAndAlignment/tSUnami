﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace TSU.Views.Message
{
    partial class MessageTsu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            this.Font = null;

            this.ParentView = null;
            this._SelectionView = null;
            this._Module = null;

            if (this.titleAndMessageRichBox != null)
            {
                this.titleAndMessageRichBox.Font = null;
            }


            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);

            this.Controls?.Clear();

        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox = new System.Windows.Forms.TextBox();
            this.MessageBy = new System.Windows.Forms.Label();
            this.hScrollBarLock = new System.Windows.Forms.HScrollBar();
            this.labelPicture = new System.Windows.Forms.Label();
            this.titleAndMessageRichBox = new System.Windows.Forms.RichTextBox();
            this.panelContainer = new System.Windows.Forms.Panel();
            this.panelContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // PopUpMenu
            // 
            this.PopUpMenu.BackColor = System.Drawing.Color.LightBlue;
            this.PopUpMenu.Location = new System.Drawing.Point(182, 182);
            // 
            // button3
            // 
            this.button3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button3.BackColor = System.Drawing.Color.LightGray;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(260, 325);
            this.button3.MaximumSize = new System.Drawing.Size(140, 0);
            this.button3.MinimumSize = new System.Drawing.Size(140, 60);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(140, 60);
            this.button3.TabIndex = 8;
            this.button3.Text = "buttonLeft";
            this.button3.UseCompatibleTextRendering = true;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.On_buttonClick);
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button2.BackColor = System.Drawing.Color.LightGray;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(552, 325);
            this.button2.MaximumSize = new System.Drawing.Size(140, 0);
            this.button2.MinimumSize = new System.Drawing.Size(140, 60);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(140, 60);
            this.button2.TabIndex = 7;
            this.button2.Text = "buttonRight";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.On_buttonClick);
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button1.BackColor = System.Drawing.Color.LightGray;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))));
            this.button1.Location = new System.Drawing.Point(406, 325);
            this.button1.MaximumSize = new System.Drawing.Size(140, 200);
            this.button1.MinimumSize = new System.Drawing.Size(140, 60);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(140, 60);
            this.button1.TabIndex = 6;
            this.button1.Text = "buttonMiddle";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.On_buttonClick);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.Location = new System.Drawing.Point(8, 111);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(904, 196);
            this.panel1.TabIndex = 17;
            this.panel1.Visible = false;
            // 
            // textBox
            // 
            this.textBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox.Location = new System.Drawing.Point(5, 266);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(910, 38);
            this.textBox.TabIndex = 23;
            this.textBox.Visible = false;
            this.textBox.WordWrap = false;
            // 
            // MessageBy
            // 
            this.MessageBy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.MessageBy.AutoSize = true;
            this.MessageBy.BackColor = System.Drawing.Color.LightGray;
            this.MessageBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MessageBy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))));
            this.MessageBy.Location = new System.Drawing.Point(9, 369);
            this.MessageBy.Name = "MessageBy";
            this.MessageBy.Size = new System.Drawing.Size(70, 12);
            this.MessageBy.TabIndex = 24;
            this.MessageBy.Text = "Message from: ";
            this.MessageBy.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.MessageBy.DoubleClick += new System.EventHandler(this.On_MessageTsu_DoubleClick);
            // 
            // hScrollBarLock
            // 
            this.hScrollBarLock.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.hScrollBarLock.Cursor = System.Windows.Forms.Cursors.Hand;
            this.hScrollBarLock.LargeChange = 1;
            this.hScrollBarLock.Location = new System.Drawing.Point(552, 326);
            this.hScrollBarLock.Maximum = 5;
            this.hScrollBarLock.Name = "hScrollBarLock";
            this.hScrollBarLock.Size = new System.Drawing.Size(140, 59);
            this.hScrollBarLock.TabIndex = 26;
            this.hScrollBarLock.Visible = false;
            this.hScrollBarLock.Scroll += new System.Windows.Forms.ScrollEventHandler(this.On_hScrollBar1_Scroll);
            // 
            // labelPicture
            // 
            this.labelPicture.BackColor = System.Drawing.Color.LightGray;
            this.labelPicture.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelPicture.Font = new System.Drawing.Font("Lucida Sans Unicode", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPicture.ForeColor = System.Drawing.Color.Red;
            this.labelPicture.Location = new System.Drawing.Point(14, 16);
            this.labelPicture.Margin = new System.Windows.Forms.Padding(0);
            this.labelPicture.Name = "labelPicture";
            this.labelPicture.Size = new System.Drawing.Size(101, 92);
            this.labelPicture.TabIndex = 15;
            this.labelPicture.Text = "X";
            this.labelPicture.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelPicture.DoubleClick += new System.EventHandler(this.On_MessageTsu_DoubleClick);
            // 
            // titleAndMessageRichBox
            // 
            this.titleAndMessageRichBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.titleAndMessageRichBox.BackColor = System.Drawing.Color.LightGray;
            this.titleAndMessageRichBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.titleAndMessageRichBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))));
            this.titleAndMessageRichBox.Location = new System.Drawing.Point(128, 16);
            this.titleAndMessageRichBox.MinimumSize = new System.Drawing.Size(100, 50);
            this.titleAndMessageRichBox.Name = "titleAndMessageRichBox";
            this.titleAndMessageRichBox.ReadOnly = true;
            this.titleAndMessageRichBox.Size = new System.Drawing.Size(784, 288);
            this.titleAndMessageRichBox.TabIndex = 25;
            this.titleAndMessageRichBox.TabStop = false;
            this.titleAndMessageRichBox.Text = "Title\nMessage\n";
            this.titleAndMessageRichBox.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.On_TitleAndMessageRichBox_LinkClicked);
            this.titleAndMessageRichBox.VisibleChanged += new System.EventHandler(this.On_TitleAndMessageRichBox_VisibleChanged);
            // 
            // panelContainer
            // 
            this.panelContainer.Controls.Add(this.textBox);
            this.panelContainer.Controls.Add(this.hScrollBarLock);
            this.panelContainer.Controls.Add(this.labelPicture);
            this.panelContainer.Controls.Add(this.panel1);
            this.panelContainer.Controls.Add(this.button1);
            this.panelContainer.Controls.Add(this.button3);
            this.panelContainer.Controls.Add(this.titleAndMessageRichBox);
            this.panelContainer.Controls.Add(this.button2);
            this.panelContainer.Controls.Add(this.MessageBy);
            this.panelContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContainer.Location = new System.Drawing.Point(1, 1);
            this.panelContainer.Name = "panelContainer";
            this.panelContainer.Size = new System.Drawing.Size(918, 389);
            this.panelContainer.TabIndex = 29;
            this.panelContainer.DoubleClick += new System.EventHandler(this.On_MessageTsu_DoubleClick);
            // 
            // MessageTsu
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(920, 391);
            this.Controls.Add(this.panelContainer);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))));
            this.MinimumSize = new System.Drawing.Size(450, 200);
            this.Name = "MessageTsu";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Message...";
            this.Load += new System.EventHandler(this.MessageTsu_Load);
            this.DoubleClick += new System.EventHandler(this.On_MessageTsu_DoubleClick);
            this.Controls.SetChildIndex(this.panelContainer, 0);
            this.Controls.SetChildIndex(this.buttonForFocusWithTabIndex0, 0);
            this.panelContainer.ResumeLayout(false);
            this.panelContainer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button button3;
        public System.Windows.Forms.Button button2;
        public System.Windows.Forms.Button button1;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.TextBox textBox;
        public System.Windows.Forms.Label MessageBy;
        private System.Windows.Forms.HScrollBar hScrollBarLock;
        private System.Windows.Forms.DataGridView tiltDataGrid;
        internal System.Windows.Forms.Panel panelContainer;
        internal System.Windows.Forms.RichTextBox titleAndMessageRichBox;
        protected System.Windows.Forms.Label labelPicture;

        public static void ShowMessageWithTextBox(string titleAndMessage, List<string> buttonTexts, out string buttonClicked, out string textInput,
            string initialTextBoxText = "", bool SelectText = true, MessageType type = MessageType.Choice, KeyPressEventHandler onKeyPress = null)
        {
            TextBox tb = Message.CreationHelper.GetPreparedTextBox(initialTextBoxText) as TextBox;

            MessageInput mi = new MessageInput(type, titleAndMessage)
            {
                ButtonTexts = buttonTexts,
                Controls = new List<Control> { tb }
            };

            if (SelectText)
            {
                // Select all text
                mi.Shown += delegate
                {
                    tb.Focus();
                    tb.SelectAll();
                };
            }
            else
            {
                // Put the cursor at the end of the text
                mi.Shown += delegate
                {
                    tb.Focus();
                    tb.SelectionStart = tb.Text.Length; // add some logic if length is 0
                    tb.SelectionLength = 0;
                };
            }

            // Validate the message by pressing enter
            tb.KeyPress += mi.On_KeyPressed;

            // If the onKeyPress delegate is provided, attach it to the TextBox's KeyPress event
            if (onKeyPress != null)
            {
                tb.KeyPress += onKeyPress;
            }


            using (MessageResult mr = mi.Show())
            {
                buttonClicked = mr.TextOfButtonClicked;
                textInput = ((TextBox)mr.ReturnedControls[0]).Text;
            }
        }
    }
}