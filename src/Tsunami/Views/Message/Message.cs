﻿using IWshRuntimeLibrary;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Windows.Forms;
using TSU.Common;
using static System.Net.Mime.MediaTypeNames;
using C = TSU.Common.Analysis.Colors;
using P = TSU.Tsunami2.Preferences;
using R = TSU.Properties.Resources;
using T = TSU.Tools;

namespace TSU.Views.Message
{
    public partial class MessageTsu : TsuView
    {
        public static bool PositionOnContainerViewInTest = false;

        private string _respond;

        public string Respond => _respond;
        public bool _textBoxRespondNeeded { get; set; }
        public string CancelString { get; set; }

        public bool coveringMessage = false;

        /// <summary>
        /// Cree les tooltips pour tout les boutons
        /// </summary>
        /// <param name="titleAndDescription"></param>
        public void CreateToolTip()
        {
            ToolTip tooltip = new ToolTip
            {
                InitialDelay = 2000,
                AutoPopDelay = 5000,
                ReshowDelay = 2000
            };
            //if (this.titleAndMessageRichBox != null) tooltip.SetToolTip(this.titleAndMessageRichBox, this.titleAndMessageRichBox.Text);
            //tooltip.SetToolTip(this, this.Text);
            if (MessageBy != null) tooltip.SetToolTip(MessageBy, MessageBy.Text);
            if (button3 != null) tooltip.SetToolTip(button3, button3.Text);
            if (button1 != null) tooltip.SetToolTip(button1, button1.Text);
            if (button2 != null) tooltip.SetToolTip(button2, button2.Text);
        }

        public MessageTsu()
        {
            Init();
        }

        private void Init()
        {
            UpdateMessageCount();
            TsunamiView.ExistingMessages.Add(this);

            InitializeComponent();
            ApplyThemeColors();

            textBox.Visible = false;

            if (!coveringMessage)
                PlaceAndAllowMovement();
            else
                CoverParentView();

            foreach (Control item in Controls)
            {
                if (!(item is TextBox))
                    item.KeyUp += On_KeyUp;
            }
        }


        public override string ToString()
        {
            return title;
        }

        public static bool CancelBasedOnDsaFlag(DsaFlag dontShowAgain, string buttonMiddle, string buttonRight, out string buttonTextToReturn)
        {
            buttonTextToReturn = "";
            if (dontShowAgain == null) return false;
            switch (dontShowAgain.State)
            {
                case DsaOptions.Always:
                    buttonTextToReturn = buttonMiddle;
                    return true;
                case DsaOptions.Never:
                    buttonTextToReturn = buttonRight;
                    return true;
                default:
                    buttonTextToReturn = "";
                    return false;
            }
        }

        public static bool CancelBasedOnDsaFlag(DsaFlag dontShowAgain, List<string> buttonTexts, out string buttonTextToReturn)
        {
            buttonTextToReturn = "";
            if (dontShowAgain == null) return false;

            string buttonMiddle = "";
            string buttonRight = "";

            if (buttonTexts != null && buttonTexts.Count > 0)
                buttonMiddle = buttonTexts[0];
            if (buttonTexts != null && buttonTexts.Count > 1)
                buttonRight = buttonTexts[1];

            bool cancel = CancelBasedOnDsaFlag(dontShowAgain, buttonMiddle, buttonRight, out buttonTextToReturn);
            return cancel;
        }

        internal void CoverParentView(int border = 30)
        {            
            Height = ParentView.Height - border * 2;
            Width = ParentView.Width - border * 2;
            Top = border;
            Left = border;
        }

        internal void PerformButtonClick(Button button)
        {
            On_buttonClick(button, null);
        }


        internal void PlaceAndAllowMovement()
        {
            Resizable = true;

            int height = (P.Values.WorkingArea.Height - Height) / 2;
            int width = (P.Values.WorkingArea.Width - Width) / 2;
            Location = new Point(width, height);

            AllowMovement(this);
            AllowMovement(panelContainer);
            AllowMovement(labelPicture);
            AllowMovement(MessageBy);
            AllowMovement(panel1);
            MessageBy.MouseMove += TsuView_MouseMove;
        }

        MessageType currentType;

        public MessageTsu(TsuView parentView, MessageType type, string titleAndMessage, List<string> buttonTexts,
                          List<Control> controls, DsaFlag dontShowAgain, bool coverParent, bool asNotification,
                          string textOfButtonToBeLocked, string sender, bool useMonospacedFont, int minWdith,
                          DockStyle dockStyle, Color color)
        {
            currentType = type;
            newMessageType = true;
            useNotifationInstead = asNotification;
            coveringMessage = coverParent;
            this.dontShowAgain = dontShowAgain;
            ParentView = parentView;
            Init();
            if (minWdith > 450)
                MinimumSize = new Size(minWdith, MinimumSize.Height);
            CleanDepreciatedControls();
            Colorize(type, color);
            SetTitleAndMessage(titleAndMessage, GetLetterBasedOnType(type), out _, textOfButtonToBeLocked, useMonospacedFont);
            FillPanelWithControls(controls, dockStyle);
            if (asNotification)
            {
                CleanControlsForNotification();
                TransformInNotification();
            }
            else
            {
                FillPanelWithButtons(buttonTexts, dontShowAgain, textOfButtonToBeLocked);
                //sender = "test";
                FillPanelWithSender(sender);
            }
            panel1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
        }

        readonly bool newMessageType = false;

        readonly MessageResult Results = new MessageResult();

        public MessageResult Show2()
        {
            if (!useNotifationInstead)
            {
                TsuView containerView = ContainerView;
                if (containerView != null)
                    containerView.Resize += OnContainerResize;
                ShowDialog(ParentView);
                if (containerView != null)
                    containerView.Resize -= OnContainerResize;
                MoveControlsForNewMessageToResults();
                Dispose(true);
                return Results;
            }
            else
            {
                Show();
                return null;
            }

        }

        /// <summary>
        /// The controls given by the caller must be returned and removed form panel1 to prevent them from being disposed
        /// The controls created internally must not be returnd and must remain in panel1 to be disposed
        /// </summary>
        protected void MoveControlsForNewMessageToResults()
        {
            foreach (Control item in panel1.Controls)
                if (item != buttonPanel && item != MessageBy)
                    Results.ReturnedControls.Add(item);
            foreach (Control item in Results.ReturnedControls)
                panel1.Controls.Remove(item);
        }

        private string GetLetterBasedOnType(MessageType type)
        {
            switch (type)
            {
                case MessageType.Bug:
                    return ":(";
                case MessageType.Choice:
                case MessageType.ImportantChoice:
                    return "?";
                case MessageType.FYI:
                    return "...";
                case MessageType.GoodNews:
                    return ":)";
                case MessageType.Warning:
                case MessageType.Critical:
                case MessageType.Important:
                    return "/!\\";
                default:
                    return "M";
            }
        }

        private void FillPanelWithControls(List<Control> controls, DockStyle dockStyle)
        {
            int minimumHeight = GetMinimumTextHeight();
            panel1.Padding = new Padding(5);
            panel1.Margin = new Padding(5);
            // Add the Padding and Margin of the different levels of containers
            Height = minimumHeight
                     + Padding.Top + Padding.Bottom
                     + panelContainer.Padding.Top + panelContainer.Padding.Bottom
                     + panelContainer.Margin.Top + panelContainer.Margin.Bottom
                     + panel1.Padding.Top + panel1.Padding.Bottom
                     + panel1.Margin.Top + panel1.Margin.Bottom;
            panel1.Top = minimumHeight;
            panel1.Height = 0;
            panel1.Visible = true;

            //this.panel1.BackColor = Color.Aqua;
            if (controls != null)
                foreach (Control item in controls)
                {
                    int itemHeight = 0;
                    // If DockStyle.Fill use the height of the control before adding it, because Add sets it to 0
                    if (dockStyle == DockStyle.Fill) itemHeight = item.Height;
                    item.Dock = dockStyle;
                    panel1.Controls.Add(item);
                    // Else use the height after because the control is fitted after being added
                    if (dockStyle != DockStyle.Fill) itemHeight = item.Height;
                    Height += itemHeight;
                    var minWidth = item.MinimumSize.Width + 40;
                    if (Width < minWidth)
                    {
                        Left = (Screen.PrimaryScreen.WorkingArea.Width - minWidth) / 2;
                        Width = minWidth;
                    }
                }

            this.panel1.Cursor = Cursors.Default;
            this.MessageBy.BringToFront();
        }

        protected int GetMinimumTextHeight()
        {
            int minimumHeight;

            // When there's no title part to show
            if (title == "View" & description == "" & letter == "")
                minimumHeight = labelPicture.Top;
            else
            {
                int titleH = CreationHelper.GetPreferredHeight(title, P.Theme.Fonts.Large, titleAndMessageRichBox.Width).Height;
                int descriptionH = CreationHelper.GetPreferredHeight(description, P.Theme.Fonts.Normal, titleAndMessageRichBox.Width).Height;
                int textHeightAndSpace = titleAndMessageRichBox.Top + titleH + descriptionH;
                int pictureHisghtAndSpace = labelPicture.Top + labelPicture.Height;
                if (textHeightAndSpace <= pictureHisghtAndSpace)
                    minimumHeight = pictureHisghtAndSpace;
                else
                    minimumHeight = textHeightAndSpace;
            }

            MinimumSize = new Size(MinimumSize.Width, minimumHeight);
            return minimumHeight;
        }

        public class MessageResponseButton
        {
            public Action OnClickBeforeClosing;
            public Button Button;
        }

        protected List<MessageResponseButton> buttons = new List<MessageResponseButton>();
        private CenteredFlowLayoutPanel buttonPanel = null;

        private void FillPanelWithButtons(List<string> buttonTexts, DsaFlag dontShowAgain, string textOfButtonToBeLocked)
        {
            buttonPanel = new CenteredFlowLayoutPanel
            {
                FlowDirection = FlowDirection.LeftToRight,
                Dock = DockStyle.Bottom,
                AutoSize = true,
                AutoSizeMode = AutoSizeMode.GrowOnly,
            };

            //Allow to use the leftover space in the CenteredFlowLayoutPanel to move
            //Particulary useful for MessageModuleView, because they don't have image and title
            if (!coveringMessage)
                AllowMovement(buttonPanel);

            int widthUsedByControls = 0;
            if (dontShowAgain != null)
            {
                var c = CreationHelper.GetPreparedDsa(dontShowAgain, buttonPanel);
                buttonPanel.Controls.Add(c);
                widthUsedByControls += c.Width;
            }

            buttonTexts = CheckAndFilterButtons(buttonTexts);

            Button lockedButton = null;
            int count = 0;
            foreach (string item in buttonTexts)
            {
                // to create compatibility with all style of message that had 3 buttons max
                count++;
                if (count == 1) button1.Text = item;
                if (count == 2) button2.Text = item;
                if (count == 3) button3.Text = item;

                if (item != "")
                {
                    Button b = GetAButton("button" + count, item, foreColor, BackColor);
                    if (count == 1) button1 = b;
                    if (count == 2) button2 = b;
                    if (count == 3) button3 = b;
                    buttons.Add(new MessageResponseButton() { Button = b });
                    b.Click += On_buttonClick;
                    widthUsedByControls += b.Width;
                    if (item != textOfButtonToBeLocked)
                        buttonPanel.Controls.Add(b);
                    else
                    {
                        lockedButton = b;
                        // put a lockbox instead
                        HScrollBar hsb = new HScrollBar()
                        {
                            Width = 170,
                            Height = 70,
                            Maximum = 10,
                            LargeChange = 2,
                            Value = 0,
                            TabStop = true
                        };
                        hsb.ValueChanged += delegate
                        {
                            if (hsb.Value == hsb.Maximum * 0.8)
                            {
                                hsb.Hide();
                                buttonPanel.Controls.Remove(hsb);
                                buttonPanel.Controls.Add(lockedButton);
                            }
                        };
                        buttonPanel.Controls.Add(hsb);
                    }
                }
            }

            panel1.Controls.Add(buttonPanel);
            buttonPanel.SendToBack();

            // Try to make the window wider to have all controls on as few lines as possible
            int outerWidth = Width - buttonPanel.Width;
            int maxInnerWidth = P.Values.WorkingArea.Width - 200 - outerWidth;
            int optimumWidth = buttonPanel.GetOptimumWidth(maxInnerWidth) + outerWidth;
            if (optimumWidth > Width)
                AdjustWidth(optimumWidth);

            // Adjust for the actual height
            // the flowLayoutPanel1 may have one line, or more if the screen is not wide enough, like on a tablet
            Height += buttonPanel.Height;
            Top = (Screen.PrimaryScreen.WorkingArea.Height - Height) / 2;
        }

        internal static List<string> CheckAndFilterButtons(List<string> buttonTextsBefore)
        {
            if (buttonTextsBefore == null)
                return new List<string> { R.T_OK };

            //Filter null or empty buttons
            List<string> buttonTextsAfter = new List<string>();
            foreach (string buttonName in buttonTextsBefore)
                if (!string.IsNullOrEmpty(buttonName))
                    buttonTextsAfter.Add(buttonName);

            //Make a button if there's none left
            if (buttonTextsAfter.Count == 0)
                return new List<string> { R.T_OK };
            else
                return buttonTextsAfter;
        }

        internal void SetButtonClick(string textOfThebutton, Action action)
        {
            if (string.IsNullOrEmpty(textOfThebutton) || action == null)
                return;

            MessageResponseButton messageResponseButton = GetButtonByName(textOfThebutton);

            if (messageResponseButton != null)
                messageResponseButton.OnClickBeforeClosing = action;
        }

        internal Action GetButtonClick(string textOfThebutton)
        {
            if (string.IsNullOrEmpty(textOfThebutton))
                return null;

            return GetButtonByName(textOfThebutton)?.OnClickBeforeClosing;
        }

        internal MessageResponseButton GetButtonByName(string textOfThebutton)
        {
            foreach (MessageResponseButton button in buttons)
                if (button.Button.Text == textOfThebutton)
                    return button;

            return null;
        }

        private void FillPanelWithSender(string sender)
        {
            if (string.IsNullOrWhiteSpace(sender))
                return;

            MessageBy = new Label()
            {
                TextAlign = ContentAlignment.MiddleLeft,
                Text = "Message from: " + sender,
                Dock = DockStyle.Bottom,
                Width = panel1.Width,
                Height = 0,
                AutoSize = true,
            };

            panel1.Controls.Add(MessageBy);
            Height += MessageBy.Height;
        }

        public static Button GetAButton(string name, string text, Color foreColor, Color darkerColor)
        {
            return new Button()
            {
                Name = name,
                Text = text,
                Width = 170,
                Height = 70,
                Cursor = Cursors.Hand,
                Font = P.Theme.Fonts.Normal,
                ForeColor = foreColor,
                BackColor = darkerColor
            };
        }

        private void CleanDepreciatedControls()
        {
            Clean(button3);
            Clean(button1);
            Clean(button2);
            Clean(textBox);
            Clean(MessageBy);
        }

        private static void Clean(Control c)
        {
            if (c != null)
            {
                c.Parent?.Controls.Remove(c);
                c.Hide();
                c.Dispose();
            }
        }

        private void CleanControlsForNotification()
        {
            Clean(hScrollBarLock);
            Clean(panel1);
            Clean(labelPicture);
        }

        private void UpdateMessageCount()
        {
            Debug.MessageCount += 1;
        }

        private void ApplyThemeColors()
        {
            button3.BackColor = P.Theme.Colors.Object;
            button2.BackColor = P.Theme.Colors.Object;
            button1.BackColor = P.Theme.Colors.Object;
            button1.ForeColor = P.Theme.Colors.NormalFore;
            button2.ForeColor = P.Theme.Colors.NormalFore;
            button3.ForeColor = P.Theme.Colors.NormalFore;
            MessageBy.BackColor = P.Theme.Colors.Object;
            MessageBy.ForeColor = P.Theme.Colors.NormalFore;
            labelPicture.BackColor = P.Theme.Colors.Object;
            labelPicture.ForeColor = P.Theme.Colors.Bad;
            titleAndMessageRichBox.ForeColor = P.Theme.Colors.NormalFore;
            titleAndMessageRichBox.BackColor = P.Theme.Colors.Object;
            BackColor = P.Theme.Colors.Object;
            ForeColor = P.Theme.Colors.NormalFore;
        }
        private void On_KeyUp(object sender, KeyEventArgs e)
        {
            LookForButtonShortCut(e);
        }

        public static Color ColorBasedOnType(MessageType color)
        {
            switch (color)
            {
                case MessageType.Bug:
                    return P.Theme.Colors.DarkBackground;
                case MessageType.Choice:
                    return P.Theme.Colors.Highlight;
                case MessageType.Critical:
                    return P.Theme.Colors.Bad;
                case MessageType.FYI:
                case MessageType.GoodNews:
                    return P.Theme.Colors.Good;
                case MessageType.Warning:
                    return P.Theme.Colors.Action;
                case MessageType.Important:
                    return P.Theme.Colors.Attention;
                default:
                    return P.Theme.Colors.Highlight;
            }
        }

        readonly bool leftButtonAvailableForHelp = false;

        readonly string helpButtonText = "(help)";

        internal DsaFlag dontShowAgain = null;

        [Obsolete("rather 'use public MessageResult ShowMessage()'")]
        public MessageTsu(TsuView parentView, Color backColor, Color foreColor, string pictureChar, string defaultTitle,
            string titleAndMessage, string buttonMiddle, string buttonRight, string buttonLeft, string defaultInput,
            string sender, bool showNow = true, DsaFlag dontShowAgain = null, Bitmap helpImage = null)
            : this()
        {
            this.dontShowAgain = dontShowAgain;

            useNotifationInstead = CheckBoxIfNotificationisNeeded(buttonMiddle, buttonRight, buttonLeft, dontShowAgain);

            ParentView = parentView;
            Text = defaultTitle;
            Colorize(backColor, foreColor);

            // replace left button with help button if picture avaialble
            leftButtonAvailableForHelp = buttonLeft == "";
            if (helpImage != null && leftButtonAvailableForHelp)
            {
                buttonLeft = helpButtonText;
            }

            FillTexts(pictureChar, titleAndMessage, buttonMiddle, buttonRight, buttonLeft, defaultInput, sender);
            if (helpImage != null)
            {
                if (leftButtonAvailableForHelp)
                {
                    button3.Click += delegate
                    {
                        parentView.ShowMessageOfImage("", R.T_OK, helpImage);
                    };
                }
                labelPicture.Text += " (?)";

                labelPicture.Click += delegate
                {
                    parentView.ShowMessageOfImage("", R.T_OK, helpImage);
                };
            }

            ResizeHeight();

            // log the message received by the user.
            try
            {
                Module module = null;
                if (parentView != null && parentView._Module != null)
                {
                    module = parentView._Module;
                }

                Logs.Log.AddEntryAsFYI(module, $"Message to user: {titleAndMessage} / {buttonMiddle} / {buttonRight} / {buttonLeft} ");
            }
            catch (Exception ex)
            {
                ShowMessageOfBug("problem to Log the message received by the user", ex);
            }

            if (dontShowAgain != null) AddCheckBoxDontShowAgain(dontShowAgain);

            if (parentView == null) parentView = Tsunami2.Properties.View;

            Logs.Log.AddEntryAsFYI(parentView._Module, $"Message: {titleAndMessage}");

            if (useNotifationInstead)
            {
                TransformInNotification();
                Show();

            }
            else
            {
                if (showNow)
                {
                    TopMost = true;
                    if (ParentView != null)
                        ShowDialog(ParentView);
                    else
                        ShowDialog();
                }
            }
        }

        public bool CheckBoxIfNotificationisNeeded(string buttonMiddle, string buttonRight, string buttonLeft, DsaFlag dontShowAgain)
        {
            bool useNotif = false;
            if (buttonMiddle == "" && buttonRight == "") useNotif = true;
            if (buttonMiddle == "" && buttonLeft == "") useNotif = true;
            if (buttonRight == "" && buttonLeft == "") useNotif = true;
            if (buttonRight == "JIRA") useNotif = true;
            if (buttonMiddle == "OK!") useNotif = false;
            if (dontShowAgain != null && (dontShowAgain.State == DsaOptions.Ask_to || dontShowAgain.State == DsaOptions.Ask_to_with_pre_checked_dont_show_again)) useNotif = false;
            return useNotif;
        }

        public bool useNotifationInstead = false;

        internal static Notifier Notifier = new Notifier();

        private void TransformInNotification()
        {
            LogTheMessage("Notification");

            labelPicture.Hide();
            titleAndMessageRichBox.Left -= labelPicture.Width;
            titleAndMessageRichBox.Width += labelPicture.Width;

            TopLevel = true;
            TopMost = true;
            ShowInTaskbar = false;
            AllowTransparency = true;
            Opacity = 50;
            int width = Tsunami2.Properties.Menu != null ? Tsunami2.Properties.Menu.View.Width : 200;
            MinimumSize = new Size(width, 50);
            MaximumSize = new Size((int)(Tsunami2.Properties.View.Width / 2*0.8), (int)(Tsunami2.Properties.View.Height/2));
            Width = width * 2;

            // Expand to see full text
            var titlePreferedSize = CreationHelper.GetPreferredHeight(title, P.Theme.Fonts.Normal, titleAndMessageRichBox.Width);
            int titlePreferedHeight = titlePreferedSize.Height;
            int titlePreferedWidth = titlePreferedSize.Width;

            var descriptionPreferedSize = CreationHelper.GetPreferredHeight(description, P.Theme.Fonts.Tiny, titleAndMessageRichBox.Width);
            int descrPreferedHeight = descriptionPreferedSize.Height;
            int descrPreferedWidth = descriptionPreferedSize.Width;

            int textBoxHeight = titlePreferedHeight + descrPreferedHeight;
            // Adjust the window size first, so that the anchoring/docking does not move the RichTextBox
            this.Height = textBoxHeight
                     + Padding.Top + Padding.Bottom
                     + panelContainer.Padding.Top + panelContainer.Padding.Bottom
                     + panelContainer.Margin.Top + panelContainer.Margin.Bottom;
            titleAndMessageRichBox.Top = panelContainer.Padding.Top + panelContainer.Margin.Top;

            //Make it bigger first, to remove the vertical scrollbar
            //titleAndMessageRichBox.Height = textBoxHeight + 1000;

            //Give the right size
            titleAndMessageRichBox.Height = textBoxHeight;

            Visible = false;
            var preferedWidth = Math.Max(titlePreferedWidth, descrPreferedWidth);
            this.MinimumSize = new Size(preferedWidth+100, this.Height);
            this.Width = preferedWidth+100;

            // hide unecessary button, for old style only, because in the new style they're already disposed 
            if (!button1.IsDisposed && button2.Text == "" && button3.Text == "")
            {
                button1.Hide();
                titleAndMessageRichBox.Height += button1.Height;
            }

            if (button2.Text == "JIRA")
            {
                button2.Hide();
                button1.Hide();
                titleAndMessageRichBox.Height += button1.Height;
            }

            // move dont show again
            foreach (var item in Controls)
            {
                if (item is CheckBox dsa)
                {
                    dsa.Height /= 2;
                    dsa.Left = 10;
                    dsa.Top += 50;
                    dsa.BringToFront();
                }
            }

            // detect mouse inside to stop the move down
            List<Control> controls = new List<Control>() { titleAndMessageRichBox, button1, this, panelContainer, panel1 };
            foreach (var item in controls)
            {
                item.MouseDown += delegate
                {
                    Notifier.MouseDownTime = DateTime.UtcNow;
                };
                item.MouseUp += delegate
                {

                    if (doNotHideBecauseALinkWasClicked)
                    {
                        doNotHideBecauseALinkWasClicked = false;
                        return;
                    }

                    if ((DateTime.UtcNow - Notifier.MouseDownTime).TotalMilliseconds < 500)
                    {
                        Notifier.RemoveMessage(this);
                        Hide();
                        Dispose();
                    }
                };
                item.MouseEnter += delegate
                {
                    Notifier.mouseIsIn = true;
                    titleAndMessageRichBox.Focus();
                };
                item.MouseLeave += delegate
                {
                    Notifier.mouseIsIn = false;
                };
            }

            Notifier.AddMessage(this);
        }

        /// <summary>
        /// overrided to include main ui dispatcher and to store the modal form to beused by notifier
        /// </summary>
        /// <returns></returns>
        public new DialogResult ShowDialog()
        {
            DialogResult dr = DialogResult.Cancel;
            try
            {
                TsunamiView.ViewsShownAsModal.Add(this);
                P.Values.InvokeOnApplicationDispatcher(() => { dr = base.ShowDialog(); });
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                TsunamiView.ViewsShownAsModal.Remove(this);
            }
            return dr;
        }

        /// <summary>
        /// overrided to include main ui dispatcher and to store the modal form to beused by notifier
        /// </summary>
        /// <returns></returns>
        public new DialogResult ShowDialog(IWin32Window iww)
        {
            DialogResult dr = DialogResult.Cancel;
            try
            {
                TsunamiView.ViewsShownAsModal.Add(this);
                void Action()
                {
                    dr = base.ShowDialog(iww);
                }
                P.Values.InvokeOnApplicationDispatcher(Action);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                TsunamiView.ViewsShownAsModal.Remove(this);
            }
            return dr;
        }

        internal void LookForButtonShortCut(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Menu)
            { // show accessible shortcut

            }
            else
            {
                if (e.Modifiers == Keys.Alt)
                {
                    // check if there is a button statring withthis letter
                    foreach (var item in Controls)
                    {
                        if (item is Button b)
                        {
                            if (b.Text == "") continue;
                            KeysConverter kc = new KeysConverter();
                            char keyChar = kc.ConvertToString(e.KeyCode)[0];
                            if (b.Text[0] == keyChar)
                            {
                                b.Focus();
                                Refresh();
                                SendKeys.Send(" ");
                                //b.PerformClick();
                                //this.buttonClick(b, null);
                                break;
                            }
                        }
                        if (item is CheckBox cb && e.KeyCode == Keys.D && cb.Text == R.T_DONT_SHOW_ANYMORE)
                        {
                            cb.Checked = !cb.Checked;
                        }
                    }
                }
            }
        }

        internal void AddCheckBoxDontShowAgain(DsaFlag dontShowAgain)
        {
            if (dontShowAgain == null) return;
            this.dontShowAgain = dontShowAgain;
            if (button3.Text != "")
            {
                Width = (button3.Width + 20) * 4;
            }
            int left = button1.Left - button3.Width - 30;


            CheckBox cb = new CheckBox()
            {
                Text = R.T_DONT_SHOW_ANYMORE,
                Top = button3.Top,
                Left = left,
                Font = button3.Font,
                Size = button3.Size,
                Anchor = button1.Anchor,
                BackColor = panelContainer.BackColor,
                Checked = dontShowAgain.State == DsaOptions.Ask_to_with_pre_checked_dont_show_again,
                Width = 170
            };
            ToolTip tooltip = new ToolTip
            {
                InitialDelay = 800,
                AutoPopDelay = 5000,
                ReshowDelay = 800
            };
            tooltip.SetToolTip(cb, cb.Text);
            cb.CheckStateChanged += delegate
            {
                if (cb.Checked)
                    this.dontShowAgain.State = DsaOptions.Ask_to_with_pre_checked_dont_show_again;
            };
            Controls.Add(cb);

            cb.BringToFront();
        }

        internal void ResizeHeight(int additionnalPixels = 0)
        {
            int buttonSize = button1.Height;
            int totalLineCount = titleAndMessageRichBox.Lines.Count();
            int titleLineCount = 0;
            int descriptionLineCount = 0;

            // check line length
            titleLineCount += (titleAndMessageRichBox.Lines[0].Count() / 70) + 1;

            int totalLineCountToCount = totalLineCount;
            if (totalLineCountToCount > 50) totalLineCountToCount = 50;
            for (int i = 1; i < totalLineCountToCount; i++)

            {
                int charCount = titleAndMessageRichBox.Lines[i].Count();
                descriptionLineCount += (charCount / 180) + 1;
            }

            int titleTextSize = (P.Theme.Fonts.NormalTextSize * 3 / 2 + 25) * titleLineCount;
            int textSize = ((P.Theme.Fonts.TinyTextSize * 3 / 2) + 15) * descriptionLineCount;
            int optimalRichBoxHeight = titleTextSize + textSize;

            Height = optimalRichBoxHeight + buttonSize + 20;
            if (_textBoxRespondNeeded)
                Height += textBox.Height + 10;

            if (Tsunami2.Properties != null)
            {
                while (Top > 150)
                {
                    if (!P.Values.WorkingArea.Contains(Bounds))
                    {
                        Top -= 50;
                        //Messageox.Show(this.Top.ToString());
                    }
                    else
                        break;
                }
                bool test1 = true, test2 = true, test3 = true, test4 = true;
                bool firstTime = true;
                while (test1 && test2 && test3 && test4)
                {
                    test1 = !P.Values.WorkingArea.Contains(x: Left + Width, y: Top + Height + 150);
                    test2 = Tsunami2.Properties.View.Top > -100;
                    test3 = Tsunami2.Properties.View.Left > -100;
                    test4 = Height > MinimumSize.Height;
                    //Messageox.Show(this.Height.ToString() + test1 + test2+ test3+ test4 + this.MinimumSize.Height);
                    if (!firstTime)
                        Height -= 50;
                    firstTime = false;
                }
            }
            Height += additionnalPixels; // additional pixels height received from the caller
        }

        Color darkerColor;
        Color foreColor;

        // Tools, use fore transparent to choose blow or white based on the background color
        public void Colorize(Color back, Color fore)
        {

            if (fore == P.Theme.Colors.Transparent)
                fore = C.ChooseContrastedColorBetweenBlackAndWhite(back);
            foreColor = fore;
            // Change what change color depending of the theme
            if (P.Values.GuiPrefs != null && P.Values.GuiPrefs.Theme.Colors.FullColorisation)
            {
                BackColor = back;
                labelPicture.BackColor = fore;
                labelPicture.ForeColor = back; ForeColor = fore;
                titleAndMessageRichBox.BackColor = back;
                labelPicture.ForeColor = back;

                darkerColor = C.GetDarkerColor(BackColor);
                MessageBy.BackColor = back;
                BackColor = darkerColor;
                panelContainer.BackColor = back;
                panel1.BackColor = back;
                titleAndMessageRichBox.BackColor = back;
                button1.BackColor = darkerColor;
                button2.BackColor = darkerColor;
                button3.BackColor = darkerColor;
                titleAndMessageRichBox.ForeColor = fore;
                button1.ForeColor = fore;
                button2.ForeColor = fore;
                button3.ForeColor = fore;
            }
            else
            {
                BackColor = P.Theme.Colors.Object;
                ForeColor = P.Theme.Colors.NormalFore;
                labelPicture.BackColor = P.Theme.Colors.Object;
                labelPicture.ForeColor = BackColor;
                titleAndMessageRichBox.BackColor = ForeColor;

                darkerColor = C.GetDarkerColor(BackColor);
                MessageBy.BackColor = BackColor;
                panelContainer.BackColor = BackColor;
                titleAndMessageRichBox.BackColor = BackColor;
                button1.BackColor = darkerColor;
                button2.BackColor = darkerColor;
                button3.BackColor = darkerColor;
                titleAndMessageRichBox.ForeColor = ForeColor;
                button1.ForeColor = ForeColor;
                button2.ForeColor = ForeColor;
                button3.ForeColor = ForeColor;
                BackColor = darkerColor; // last
            }
        }

        internal void Colorize(MessageType type, Color color)
        {
            if (color != Color.Transparent)
            {
                Colorize(color, P.Theme.Colors.LightForeground);
            }
            else
            {
                switch (type)
                {
                    case MessageType.Bug:
                        Colorize(P.Theme.Colors.DarkBackground, P.Theme.Colors.NormalFore);
                        break;
                    case MessageType.Choice:
                    case MessageType.FYI:
                        Colorize(P.Theme.Colors.Object, P.Theme.Colors.NormalFore);
                        break;
                    case MessageType.Critical:
                        Colorize(P.Theme.Colors.Bad, P.Theme.Colors.LightForeground);
                        break;
                    case MessageType.GoodNews:
                        Colorize(P.Theme.Colors.Good, P.Theme.Colors.NormalFore);
                        break;
                    case MessageType.Warning:
                    case MessageType.Important:
                    case MessageType.ImportantChoice:
                        Colorize(P.Theme.Colors.Attention, P.Theme.Colors.NormalFore);
                        break;
                    default:
                        break;
                }
            }
        }

        public void LockLeftButton()
        {
            hScrollBarLock.Visible = true;
            hScrollBarLock.TabStop = true;
            titleAndMessageRichBox.SelectionFont = P.Theme.Fonts.Normal;
            titleAndMessageRichBox.AppendText(string.Format("\r\n" + "\r\n" + R.T_Locked, button2.Text));
        }

        public void FillTexts(string picture, string titleAndMessage = "", string middle = "", string right = "", string left = "", string box = "", string sender = "")
        {
            SetTitleAndMessage(titleAndMessage, picture, out Size preferedSize);

            button1.Text = middle;
            button1.Visible = button1.Text != "";
            button2.Text = right;
            button2.Visible = button2.Text != "";
            button3.Text = left;
            button3.Visible = button3.Text != "";
            textBox.Text = box;
            MessageBy.Text += sender;
            MessageBy.Visible = sender != "";
            //this.CreateToolTip();
        }

        string title;
        string description;
        string letter;


        internal Size TitlePreferedSize;
        internal Size DescriptionPreferedSize;

        public void SetTitleAndMessage(string titleAndMessage, string BigLetter, out Size preferedSize, string textOfButtonToBeLocked = "", bool useMonospacedFont = false)
        {
            preferedSize = titleAndMessageRichBox.Size;

            if (titleAndMessage == "View")
            {
                titleAndMessageRichBox.Visible = false;
                labelPicture.Visible = false;
                int increase = panel1.Top - labelPicture.Top;
                panel1.Top = labelPicture.Top;
                panel1.Height += increase;
                panel1.BackColor = Color.Black;
                title = "View";
                description = "";
                letter = "";
            }
            else
            {

                titleAndMessageRichBox.Text = "";
                letter = BigLetter;

                // Title
                title = T.Conversions.StringManipulation.SeparateTitleFromMessage(titleAndMessage);
                Text = title;
                Font titleFont = useNotifationInstead ? P.Theme.Fonts.Normal : P.Theme.Fonts.Large;

                titleAndMessageRichBox.SelectionFont = titleFont;

                ThreatStringColorForRTB(title + "\r\n", titleAndMessageRichBox, ForeColor);

                var w = this.Width;
                preferedSize = TextRenderer.MeasureText(title, titleFont);
                TitlePreferedSize = preferedSize;

                // Description
                Font descriptionFont;
                if (useNotifationInstead)
                    descriptionFont = P.Theme.Fonts.Tiny;
                else if (useMonospacedFont)
                    descriptionFont = P.Theme.Fonts.MonospaceFont;
                else
                    descriptionFont = P.Theme.Fonts.Normal;

                titleAndMessageRichBox.SelectionFont = descriptionFont;

                description = T.Conversions.StringManipulation.SeparateMessageFromTitle(titleAndMessage);
                description = description.Replace(";", "\r\n");

                if (!string.IsNullOrEmpty(textOfButtonToBeLocked))
                    description += string.Format("\r\n" + R.T_Locked, textOfButtonToBeLocked);

                Size descriptionSize = TextRenderer.MeasureText(description, descriptionFont);
                DescriptionPreferedSize = descriptionSize;


                ThreatStringColorForRTB(description, titleAndMessageRichBox, ForeColor);


                labelPicture.Text = BigLetter;

                // get autosize
                if (!coveringMessage)
                {
                    int longerWidth = 0;
                    longerWidth = (longerWidth < preferedSize.Width) ? preferedSize.Width : longerWidth;
                    longerWidth = (longerWidth < descriptionSize.Width) ? descriptionSize.Width : longerWidth;
                    longerWidth += labelPicture.Width + 100;
                    AdjustWidth(longerWidth);
                }
                preferedSize += descriptionSize;
            }
        }

        /// <summary>
        /// Adjust the window to fit to the given content width
        /// </summary>
        /// <param name="longerWidth"></param>
        private void AdjustWidth(int longerWidth)
        {
            int maxWidthAcceptable = P.Values.WorkingArea.Width - 200;
            longerWidth = (longerWidth > maxWidthAcceptable) ? maxWidthAcceptable : longerWidth;
            Width = longerWidth;
            Left = (P.Values.WorkingArea.Width - longerWidth) / 2;
        }

        private static class FormatTypes
        {
            public const string Regular = "$REGULAR$";
            public const string Bold = "$BOLD$";
            public const string Italic = "$ITALIC$";
            public const string Green_Good = "$GREEN$";
            public const string Red_Bad = "$RED";
            public const string Average_Orange = "$ORANGE$";
            public const string End_All = "$END$";
        }

        public static void ThreatStringColorForRTB(string description, RichTextBox rtb, Color normalColor)
        {
            string[] parts = description.Split('$');
            foreach (var item in parts)
            {
                string key = '$' + item.ToUpper() + '$';
                if (key == FormatTypes.End_All)
                {
                    rtb.SelectionColor = normalColor;
                    rtb.SelectionFont = P.Theme.Fonts.Normal;
                }
                else if (key == FormatTypes.Red_Bad)
                {
                    rtb.SelectionColor = P.Theme.Colors.Bad;
                }
                else if (key == FormatTypes.Average_Orange)
                {
                    rtb.SelectionColor = P.Theme.Colors.Attention;
                }
                else if (key == FormatTypes.Green_Good)
                {
                    rtb.SelectionColor = P.Theme.Colors.Good;
                }
                else if (key == FormatTypes.Regular)
                {
                    rtb.SelectionFont = P.Theme.Fonts.Normal;
                }
                else if (key == FormatTypes.Bold)
                {
                    rtb.SelectionFont = P.Theme.Fonts.NormalBold;
                }
                else if (key == FormatTypes.Italic)
                {
                    rtb.SelectionFont = P.Theme.Fonts.NormalBold;
                }
                else
                    rtb.AppendText(item);
            }
        }

        // Calls
        public void On_KeyPressed(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar == (char)Keys.Return || e.KeyChar == (char)Keys.Enter) && sender is TextBox b)
            {
                MessageTsu form = (MessageTsu)b.Parent.Parent;
                if (form.textBox.Visible) form._respond = form.textBox.Text;
                form.Close();
            }
        }

        public string buttonClickedName;
        // Return
        internal void SetRespond(Button button, MessageTsu form)
        {

            string buttonText = (button == null) ? "VALIDATE" : button.Text;

            T.Macro.DoTrace($"MB: {title}: {buttonText}");
            if (button != null)
            {
                if (!_textBoxRespondNeeded)
                {
                    _respond = buttonText;
                }
                else
                {
                    buttonClickedName = buttonText;
                    if (buttonText != form.CancelString)
                        _respond = textBox.Text;
                    else
                        _respond = form.CancelString;
                }
            }

            // todo, ugly way to send multi textbox results tsu3151
            if (form.Controls["texts"] is TableLayoutPanel tlp)
            {
                _respond = "";
                bool first = true;
                foreach (var item in tlp.Controls)
                {
                    if (item is TextBox box)
                    {
                        if (box.Enabled) // if deisable the fields was suppressed on purose in the merge text box
                        {
                            if (!first)
                            {
                                _respond += "£";
                            }
                            _respond += box.Text;

                        }
                        first = false;
                    }
                }
            }

            if (ParentView != null && ParentView._Module != null)
                Logs.Log.AddEntryAsFYI(ParentView._Module, $"User selected '{buttonText}'");
        }
        protected override void OnGotFocus(EventArgs e)
        {
            // do nothing
        }
        protected virtual void On_buttonClick(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            // if 'help button' do nothing
            if (button.Text == helpButtonText)
                return;
            // looking for the MessageView... why?
            Control currentParent = button.Parent;
            while (currentParent!= null && !(currentParent is MessageTsu))
            {
                currentParent = currentParent.Parent;
            }
            MessageTsu messageTsu = (MessageTsu)currentParent;

            LogTheMessage(button.Text);

            SetRespond(button, messageTsu);

            Results.TextOfButtonClicked = button.Text;

            AdaptTheDsatoReturn(ref Results.TextOfButtonClicked);


            // oops if we remove the textboxes here, the tsu view asking for strin[] with not received anything anymore

            //this.panel1.Controls.Clear(); // to not didpose the possible TSUVIEW inside the message
            TsunamiView.ExistingMessages.Remove(this);


            Close();
            if (newMessageType)
                MoveControlsForNewMessageToResults();
            Dispose(true); //cannot be dispose or the result object from the new show2() will be empty
        }
        private void AdaptTheDsatoReturn(ref string textOfTheClickedButton)
        {
            if (dontShowAgain == null || dontShowAgain.State != DsaOptions.Ask_to_with_pre_checked_dont_show_again) // means the dsa is not checked
                return;

            // adapt the dsa
            if (textOfTheClickedButton != R.T_CANCEL // cancel should not record the doesnt show again
                && dontShowAgain.State != DsaOptions.Always
                && dontShowAgain.State != DsaOptions.Never)
            {
                if (button2.Text != "") // if 2 buttons, the first is always, the second never
                {
                    if (textOfTheClickedButton == button1.Text)
                        dontShowAgain.State = DsaOptions.Always;
                    else if (textOfTheClickedButton == button2.Text)
                        dontShowAgain.State = DsaOptions.Never;
                }
                else // if only one button first oen probably "ok" means "dont show again
                {
                    if (textOfTheClickedButton == button1.Text)
                        dontShowAgain.State = DsaOptions.Never;
                }
            }


            if (textOfTheClickedButton == R.T_ALWAYS && dontShowAgain.State == DsaOptions.Always)
            {
                textOfTheClickedButton = R.T_YES;
            }

            if (textOfTheClickedButton == R.T_NEVER && dontShowAgain.State == DsaOptions.Never)
                textOfTheClickedButton = R.T_NO;
        }

        private void LogTheMessage(string buttonClicked)
        {
            try
            {
                Module module = ParentView?._Module;

                Logs.LogEntry.LogEntryTypes type;
                switch (this.currentType)
                {
                    case MessageType.Bug:
                        type = Logs.LogEntry.LogEntryTypes.Error;
                        break;
                    case MessageType.Choice:
                        type = Logs.LogEntry.LogEntryTypes.FYI;
                        break;
                    case MessageType.Critical:
                        type = Logs.LogEntry.LogEntryTypes.Error;
                        break;
                    case MessageType.FYI:
                        type = Logs.LogEntry.LogEntryTypes.FYI;
                        break;
                    case MessageType.GoodNews:
                        type = Logs.LogEntry.LogEntryTypes.Success;
                        break;
                    case MessageType.Warning:
                        type = Logs.LogEntry.LogEntryTypes.PayAttention;
                        break;
                    case MessageType.Important:
                        type = Logs.LogEntry.LogEntryTypes.PayAttention;
                        break;
                    case MessageType.ImportantChoice:
                        type = Logs.LogEntry.LogEntryTypes.PayAttention;
                        break;
                    default:
                        type = Logs.LogEntry.LogEntryTypes.FYI;
                        break;
                }
                Logs.Log.AddEntry(module, $"'{this.currentType}' message to user: {titleAndMessageRichBox.Text}\r\n{button1}\r\n{button2}\r\n{button3}", type);
                Logs.Log.AddEntryAsFYI(module, $"User clicked: {buttonClicked}");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Problem to Log the message received by the user, {ex}");
            }
        }
        private void On_labelPicture_Click(object sender, EventArgs e)
        {
            Control c = this;
            Bitmap bmp = new Bitmap(c.Width, c.Height);
            c.DrawToBitmap(bmp, new Rectangle(Point.Empty, bmp.Size));
            string path = P.Values.Paths.Temporary + T.Conversions.Date.ToDateUpSideDown(DateTime.Now) + ".bmp";
            bmp.Save(path);
            Shell.Run(path);
        }

        internal void ShowInputBox()
        {
            titleAndMessageRichBox.Height = textBox.Top - titleAndMessageRichBox.Top;
            textBox.Visible = true;
            textBox.Show();
            textBox.Left = 10;
            BringToFront();

        }

        /// <summary>
        /// Get a link to open the file from a message, if the path is too long, a shortcut is created in 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        internal static string GetLink(string filePath)
        {
            string lnkPath = filePath;

            if (lnkPath.Length > 100)
            {
                // Chemin du fichier pour lequel vous voulez créer le raccourci

                // Créez une instance de WshShell pour accéder aux méthodes de création de raccourcis
                WshShell shell = new WshShell();

                // Créez un objet ShortcutClass pour représenter le raccourci
                FileInfo fi = new FileInfo(filePath);
                lnkPath = Path.Combine(Tsunami2.Preferences.Values.Paths.Links, $"{fi.Name}.lnk");
                IWshShortcut raccourci = (IWshShortcut)shell.CreateShortcut(lnkPath);

                // Définissez le chemin de destination du raccourci (le fichier cible)
                raccourci.TargetPath = filePath;

                // Facultatif : spécifiez d'autres propriétés du raccourci, comme l'icône ou les arguments
                // raccourci.IconLocation = "chemin\\de\\licone.ico";
                // raccourci.Arguments = "/arg1 /arg2";

                // Enregistrez le raccourci
                raccourci.Save();
            }

            TSU.Debug.WriteInConsole("Raccourci créé avec succès !");

            return $"\"file:///\\{lnkPath.Replace(' ', (char)160)}\"";
        }

        internal static string GetLinkIfExist(string fileNameTemp)
        {
            if (fileNameTemp == null)
                return "n/a";
            if (!System.IO.File.Exists(fileNameTemp) && !System.IO.Directory.Exists(fileNameTemp))
                return "n/a";

            return GetLink(fileNameTemp);
        }

        internal void ChangeLabel(string p)
        {
            SetTitleAndMessage(title + ";" + p, letter, out _);
        }

        internal void AddToLabel(string text, int number)
        {
            List<string> lines = titleAndMessageRichBox.Text.Split('\r', '\n').ToList();
            while (lines.Count > number)
            {
                lines.RemoveAt(1);
            }
            lines.Add(text);
            string s = "";
            foreach (var line in lines)
            {
                s += "\r\n" + line;
            }
            ChangeLabel(s);
        }

        internal void ShowOnTopOfTheView(TsuView destinationView)
        {
            destinationView.Controls.Add(this);
            ShowDockedFill();
            ShowOnTop();

        }

        private void On_hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            if (sender is HScrollBar hScrollBar
                && e.NewValue == hScrollBar.Maximum)
                hScrollBar.Hide();
        }

        private void On_MessageTsu_DoubleClick(object sender, EventArgs e)
        {
            AdjustToScreenSize(true);
        }

        internal void OnContainerResize(object sender, EventArgs e)
        {
            AdjustToScreenSize(true);
        }

        internal void AdjustToScreenSize(bool full = false)
        {
            const int DIFF = 50;

            if (Debug.IsRunningInATest && !PositionOnContainerViewInTest)
            {
                Rectangle screenSize = P.Values.WorkingArea;

                if (full)
                {
                    Height = screenSize.Height - DIFF;
                    Width = screenSize.Width - DIFF;
                    Top = DIFF / 3;
                    Left = DIFF / 3;
                }
                else if ((Top + Height) > (screenSize.Height - DIFF))
                {
                    Top = DIFF / 3;
                    Height = screenSize.Height - DIFF;
                    Width = screenSize.Width - 50;
                }
            }
            else
            {
                TsuView containerView = ContainerView;
                Rectangle containerRectangle;
                if (containerView != null)
                {
                    containerRectangle = new Rectangle(
                        containerView.PointToScreen(Point.Empty), 
                        containerView.Size
                    );
                }
                else
                {
                    containerRectangle = P.Values.WorkingArea;
                }
                Rectangle targetRectangle = new Rectangle(
                    containerRectangle.Left + (DIFF / 2),
                    containerRectangle.Top + (DIFF / 2),
                    containerRectangle.Width - DIFF,
                    containerRectangle.Height - DIFF
                );

                Bounds = targetRectangle;
            }
        }

        private TsuView ContainerView => ParentView
                                      ?? (Tsunami2.Properties?.View)
                                      ?? (TsuView)(Splash.Module.Instance?.View);

        private void On_TitleAndMessageRichBox_VisibleChanged(object sender, EventArgs e)
        {
            if (sender is RichTextBox rtb && rtb.ScrollBars == RichTextBoxScrollBars.Vertical)
            {
                Height += 100;
            }
        }

        bool doNotHideBecauseALinkWasClicked = false;
        private void On_TitleAndMessageRichBox_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            doNotHideBecauseALinkWasClicked = true;
            OpenLink(e.LinkText);
        }

        public static void OpenLink(string LinkText)
        {
            try
            {
                string linkText = LinkText.Replace((char)160, ' ');

                // For some reason rich text boxes strip off the 
                // trailing ')' character for URL's which end in a 
                // ')' character, so if we had a '(' opening bracket
                // but no ')' closing bracket, we'll assume there was
                // meant to be one at the end and add it back on. This 
                // problem is commonly encountered with wikipedia links!

                if ((linkText.IndexOf('(') > -1) && (linkText.IndexOf(')') == -1))
                    linkText += ")";

                System.Diagnostics.Process.Start(linkText);
            }
            catch (Exception ex)
            {
                string titleAndMessage = $"Cannot start '{LinkText}';{ex.Message}   ";
                new MessageInput(MessageType.Critical, titleAndMessage).Show();
            }
        }

        private void MessageTsu_Load(object sender, EventArgs e)
        {
            // focus on the first button
            if (TrySearchFirstButton(panel1, out Button b))
            {
                b.Select();
            }
        }

        /// <summary>
        /// Search the first button in the parent control, searching all the subcontrols
        /// </summary>
        /// <param name="p">The control containing the button</param>
        /// <returns>true if a button was found, false otherwise</returns>
        private static bool TrySearchFirstButton(Control p, out Button buttonFound)
        {
            foreach (Control c in p.Controls)
            {
                // if the control is a Button, return true to end the current stack of calls
                if (c is Button b)
                {
                    buttonFound = b;
                    return true;
                }

                // Otherwise, we check the child control.
                else
                {
                    // If there's a button inside, end the stack of calls.
                    if (TrySearchFirstButton(c, out buttonFound))
                        return true;
                    // Otherwise, we will check the next item in the foreach
                }
            }

            // If no subcontrol is a button, keep going for the next control
            buttonFound = null;
            return false;
        }

        public bool CancelClosing = false;

        public void TestClosure()
        {
            if (!CancelClosing)
            {
                MoveControlsForNewMessageToResults();
                this.Close();
                this.Hide();
                this.Dispose();
            }
            else
            {
                CancelClosing = false;
            }
        }

        internal static void ShowMessage(Exception ex, MessageType type = MessageType.Critical)
        {
            string bestLookingException = $"{ex.Message};";
            if (ex.InnerException != null)
                bestLookingException += GetInnerExceptions(ex.InnerException);
            if (!(ex is TsuException))
                bestLookingException += $"\r\n {ex.StackTrace}";
            new MessageInput(type, bestLookingException).Show();
        }

        private static string GetInnerExceptions(Exception ex)
        {
            string text = $"{ex.Message}";
            if (ex.InnerException != null)
                text += ". <= " + GetInnerExceptions(ex.InnerException);
            return text + "\r\n";
        }
    }
}