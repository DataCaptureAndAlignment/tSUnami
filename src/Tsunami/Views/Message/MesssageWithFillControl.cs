﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace TSU.Views.Message
{
    public class MesssageWithFillControl : MessageTsu
    {
        public MesssageWithFillControl(TsuView parentView, MessageType type, string titleAndMessage, List<string> buttonTexts,
            Control control, DsaFlag dontShowAgain, bool coverParent, string textOfButtonToBeLocked, string sender)
            : base(parentView, type, titleAndMessage, buttonTexts, new List<Control> { control }, dontShowAgain,
                  coverParent, false, textOfButtonToBeLocked, sender, false, -1, DockStyle.Fill, color: Color.Transparent)
        {
            Resize += OnResize;
        }

        private void OnResize(object sender, EventArgs e)
        {
            // Gets the minimum text height considering the current width; updates MinimumSize too
            int textHeight = GetMinimumTextHeight();

            // The panel should start after the text, and use the available height
            panel1.SetBounds(0, textHeight, 0, this.Height - textHeight - 10, BoundsSpecified.Y | BoundsSpecified.Height);
        }
    }
}
