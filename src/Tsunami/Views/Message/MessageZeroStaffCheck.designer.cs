﻿using R = TSU.Properties.Resources;
namespace TSU.Views
{
    partial class MessageZeroStaffCheck
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonLeft = new System.Windows.Forms.Button();
            this.buttonRight = new System.Windows.Forms.Button();
            this.titleAndMessageRichBox = new System.Windows.Forms.RichTextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridViewZeroStaff = new System.Windows.Forms.DataGridView();
            this.Delete = new System.Windows.Forms.DataGridViewImageColumn();
            this.Staff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Reading = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Correction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewZeroStaff)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonLeft
            // 
            this.buttonLeft.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonLeft.BackColor = System.Drawing.Color.LightGray;
            this.buttonLeft.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.buttonLeft.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLeft.Location = new System.Drawing.Point(57, 7);
            this.buttonLeft.MaximumSize = new System.Drawing.Size(140, 0);
            this.buttonLeft.MinimumSize = new System.Drawing.Size(140, 60);
            this.buttonLeft.Name = "buttonLeft";
            this.buttonLeft.Size = new System.Drawing.Size(140, 60);
            this.buttonLeft.TabIndex = 8;
            this.buttonLeft.Text = "buttonLeft";
            this.buttonLeft.UseVisualStyleBackColor = false;
            this.buttonLeft.Click += new System.EventHandler(this.On_buttonClick);
            this.buttonLeft.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonLeft_KeyPress);
            // 
            // buttonRight
            // 
            this.buttonRight.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonRight.BackColor = System.Drawing.Color.LightGray;
            this.buttonRight.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.buttonRight.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRight.ForeColor = System.Drawing.Color.Black;
            this.buttonRight.Location = new System.Drawing.Point(313, 7);
            this.buttonRight.MaximumSize = new System.Drawing.Size(140, 200);
            this.buttonRight.MinimumSize = new System.Drawing.Size(140, 60);
            this.buttonRight.Name = "buttonRight";
            this.buttonRight.Size = new System.Drawing.Size(140, 60);
            this.buttonRight.TabIndex = 6;
            this.buttonRight.Text = "buttonRight";
            this.buttonRight.UseVisualStyleBackColor = false;
            this.buttonRight.Click += new System.EventHandler(this.On_buttonClick);
            this.buttonRight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonRight_KeyPress);
            // 
            // titleAndMessageRichBox
            // 
            this.titleAndMessageRichBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.titleAndMessageRichBox.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.titleAndMessageRichBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.titleAndMessageRichBox.Location = new System.Drawing.Point(3, 3);
            this.titleAndMessageRichBox.Name = "titleAndMessageRichBox";
            this.titleAndMessageRichBox.ReadOnly = true;
            this.titleAndMessageRichBox.Size = new System.Drawing.Size(511, 94);
            this.titleAndMessageRichBox.TabIndex = 25;
            this.titleAndMessageRichBox.TabStop = false;
            this.titleAndMessageRichBox.Text = "Title\n";
            this.titleAndMessageRichBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.titleAndMessageRichBox_KeyPress);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(8, 8);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(517, 666);
            this.panel2.TabIndex = 27;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.dataGridViewZeroStaff, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.titleAndMessageRichBox, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(517, 666);
            this.tableLayoutPanel1.TabIndex = 26;
            // 
            // dataGridViewZeroStaff
            // 
            this.dataGridViewZeroStaff.AllowUserToAddRows = false;
            this.dataGridViewZeroStaff.AllowUserToDeleteRows = false;
            this.dataGridViewZeroStaff.AllowUserToResizeColumns = false;
            this.dataGridViewZeroStaff.AllowUserToResizeRows = false;
            this.dataGridViewZeroStaff.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewZeroStaff.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.dataGridViewZeroStaff.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewZeroStaff.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewZeroStaff.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewZeroStaff.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Delete,
            this.Staff,
            this.Reading,
            this.Correction});
            this.dataGridViewZeroStaff.Cursor = System.Windows.Forms.Cursors.Arrow;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewZeroStaff.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewZeroStaff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewZeroStaff.EnableHeadersVisualStyles = false;
            this.dataGridViewZeroStaff.Location = new System.Drawing.Point(3, 103);
            this.dataGridViewZeroStaff.MultiSelect = false;
            this.dataGridViewZeroStaff.Name = "dataGridViewZeroStaff";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewZeroStaff.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewZeroStaff.RowHeadersVisible = false;
            this.dataGridViewZeroStaff.Size = new System.Drawing.Size(511, 480);
            this.dataGridViewZeroStaff.TabIndex = 0;
            this.dataGridViewZeroStaff.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewZeroStaff_CellClick);
            this.dataGridViewZeroStaff.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewZeroStaff_CellValueChanged);
            this.dataGridViewZeroStaff.CurrentCellChanged += new System.EventHandler(this.DataGridViewZeroStaff_CurrentCellChanged);
            this.dataGridViewZeroStaff.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DataGridViewZeroStaff_KeyPress);
            this.dataGridViewZeroStaff.MouseEnter += new System.EventHandler(this.DataGridViewZeroStaff_MouseEnter);
            // 
            // Delete
            // 
            this.Delete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Delete.FillWeight = 60.9137F;
            this.Delete.HeaderText = "";
            this.Delete.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.Delete.MinimumWidth = 30;
            this.Delete.Name = "Delete";
            this.Delete.ReadOnly = true;
            this.Delete.Width = 30;
            // 
            // Staff
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Staff.DefaultCellStyle = dataGridViewCellStyle14;
            this.Staff.FillWeight = 112.6286F;
            this.Staff.HeaderText = R.StringMsgZeroStaff_Staff;
            this.Staff.MinimumWidth = 60;
            this.Staff.Name = "Staff";
            this.Staff.ReadOnly = true;
            this.Staff.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Staff.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Reading
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Reading.DefaultCellStyle = dataGridViewCellStyle15;
            this.Reading.FillWeight = 116.2103F;
            this.Reading.HeaderText = R.StringMsgZeroStaff_Reading;
            this.Reading.MinimumWidth = 60;
            this.Reading.Name = "Reading";
            this.Reading.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Correction
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Correction.DefaultCellStyle = dataGridViewCellStyle16;
            this.Correction.FillWeight = 94.61308F;
            this.Correction.HeaderText = R.StringMsgZeroStaff_Correction;
            this.Correction.Name = "Correction";
            this.Correction.ReadOnly = true;
            this.Correction.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.buttonLeft, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.buttonRight, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 589);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(511, 74);
            this.tableLayoutPanel2.TabIndex = 26;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MessageZeroStaffCheck
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(533, 682);
            this.Controls.Add(this.panel2);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MinimumSize = new System.Drawing.Size(450, 200);
            this.Name = "MessageZeroStaffCheck";
            this.Padding = new System.Windows.Forms.Padding(8);
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Message...";
            this.Shown += new System.EventHandler(this.MessageDataGridTSU_Shown);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewZeroStaff)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Button buttonLeft;
        public System.Windows.Forms.Button buttonRight;
        private System.Windows.Forms.RichTextBox titleAndMessageRichBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView dataGridViewZeroStaff;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridViewImageColumn Delete;
        private System.Windows.Forms.DataGridViewTextBoxColumn Staff;
        private System.Windows.Forms.DataGridViewTextBoxColumn Reading;
        private System.Windows.Forms.DataGridViewTextBoxColumn Correction;
    }
}