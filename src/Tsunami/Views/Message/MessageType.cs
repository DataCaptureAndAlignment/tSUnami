﻿namespace TSU.Views.Message
{
    public enum MessageType
    {
        Bug, 
        Choice, 
        Critical, 
        FYI, 
        GoodNews, 
        Warning, 
        Important, 
        ImportantChoice
    }
}