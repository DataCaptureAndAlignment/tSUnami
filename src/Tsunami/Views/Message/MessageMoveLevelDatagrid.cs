﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using TSU.Views;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using P = TSU.Tsunami2.Preferences;

using M = TSU.Common.Measures;
using T = TSU.Tools;
using System.Globalization;
using Cac = TSU.Common.Analysis.Colors;


namespace TSU.Views
{
    public partial class MesssageMoveLevelDatagrid : TsuView
    {
        private string _respond;
        public string Respond { get { return _respond; }   }
        public string CancelString { get; set; }
        public List<string> listPointNames;
        public List<string> listDcum;
        public List<string> listMoveToDo;
        public List<string> listTheoReading;
        public MesssageMoveLevelDatagrid()
        {
            this.listPointNames = new List<string>();
            this.listDcum = new List<string>();
            this.listMoveToDo = new List<string>();
            this.listTheoReading = new List<string>();
            this.InitializeComponent();
            this.ApplyThemeColors();
            this.Resizable = true;
            this.Location = new System.Drawing.Point((TSU.Tsunami2.Preferences.Values.WorkingArea.Width - this.Width) / 2, (TSU.Tsunami2.Preferences.Values.WorkingArea.Height - this.Height )/ 2);
            this.AllowMovement(this);
        }
        private void ApplyThemeColors()
        {
            this.buttonLeft.BackColor = P.Theme.Colors.Object;
            this.buttonLeft.ForeColor = P.Theme.Colors.NormalFore;
            this.dataGridViewMoveLevel.ColumnHeadersDefaultCellStyle.BackColor = P.Theme.Colors.Object;
            this.dataGridViewMoveLevel.ColumnHeadersDefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.dataGridViewMoveLevel.ColumnHeadersDefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            this.dataGridViewMoveLevel.ColumnHeadersDefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.dataGridViewMoveLevel.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.dataGridViewMoveLevel.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.dataGridViewMoveLevel.DefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            this.dataGridViewMoveLevel.DefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.PointName.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.PointName.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.PointName.DefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            this.PointName.DefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.Dcum.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.Dcum.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.Dcum.DefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            this.Dcum.DefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.TheoReading.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.TheoReading.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.TheoReading.DefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            this.TheoReading.DefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.Move.DefaultCellStyle.BackColor = P.Theme.Colors.LightBackground;
            this.Move.DefaultCellStyle.ForeColor = P.Theme.Colors.NormalFore;
            this.Move.DefaultCellStyle.SelectionBackColor = P.Theme.Colors.TextHighLightBack;
            this.Move.DefaultCellStyle.SelectionForeColor = P.Theme.Colors.TextHighLightFore;
            this.BackColor = P.Theme.Colors.Object;
            this.PointName.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Name;
            this.Dcum.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Dcum;
            this.TheoReading.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Theo;
            this.Move.HeaderText = global::TSU.Properties.Resources.StringDataGridLevel_Move;
        }
        // Tools
        public void Colorize( Color back, Color fore)
        {
            this.BackColor = back;
            this.ForeColor = fore;
            this.titleAndMessageRichBox.BackColor = back;
            
            Color darkerColor = Cac.GetDarkerColor(BackColor);
            Color lighterColor = Cac.GetLighterColor(BackColor);
            this.BackColor = darkerColor;
            this.panel2.BackColor = back;
            this.titleAndMessageRichBox.BackColor = back;
            this.buttonLeft.BackColor = darkerColor;
            this.titleAndMessageRichBox.ForeColor = fore;
            this.buttonLeft.ForeColor = fore;
        }



        public void FillTitleAndAdjustSize(string title = "", string left = "")
        {
            SetTitle(title);
            this.buttonLeft.Text = left;
            this.buttonLeft.Visible = (this.buttonLeft.Text == "") ? false : true;
            this.CreateToolTip();
        }



        public void SetTitle(string title)
        {
            this.titleAndMessageRichBox.Text = "";
            Rectangle screenSize =Tsunami2.Properties.View.Bounds;
            this.titleAndMessageRichBox.Text = title;
            this.titleAndMessageRichBox.SelectAll();
            Font titleFont =TSU.Tsunami2.Preferences.Theme.Fonts.Large;
            this.titleAndMessageRichBox.SelectionFont = titleFont;
            this.titleAndMessageRichBox.SelectionAlignment = HorizontalAlignment.Center;
            this.titleAndMessageRichBox.SelectionLength = 0;
            this.Top = (screenSize.Height - this.Height) / 2;
            this.Left = (screenSize.Width - this.Width) / 2;
            this.AdjustToScreenSize();
        }
        // Return
        internal void SetRespond(Button button)
        {
            _respond = button.Text;
        }

        protected virtual void On_buttonClick(object sender, EventArgs e)
        {
            Button button = ((Button)sender);
            SetRespond(button);
            this.Close();
        }

        /// <summary>
        /// Cree les tooltips pour tous les boutons
        /// </summary>
        private void CreateToolTip()
        {
            ToolTip tooltip = new ToolTip();
            tooltip.InitialDelay = 50;
            tooltip.AutoPopDelay = 5000;
            tooltip.ReshowDelay = 50;
            if (titleAndMessageRichBox != null) tooltip.SetToolTip(this.titleAndMessageRichBox, this.titleAndMessageRichBox.Text);
            tooltip.SetToolTip(this, this.Text);
            if (buttonLeft != null) tooltip.SetToolTip(this.buttonLeft, this.buttonLeft.Text);
        }
        internal void AdjustToScreenSize(bool full=false)
        {
            const int DIFF = 50;
            Rectangle screenSize =Tsunami2.Properties.View.Bounds;

            if (full)
            {
                this.Height = screenSize.Height - DIFF;
                this.Width = screenSize.Width - DIFF;
                this.Top = DIFF / 3;
                this.Left = DIFF / 3;
            }
            else
            {
                if ((this.Top + this.Height) > (screenSize.Height - DIFF))
                {
                    this.Top = DIFF / 3;
                    this.Height = screenSize.Height - DIFF;
                }
                if ((this.Left + this.Width) > (screenSize.Width - DIFF))
                {
                    this.Width = screenSize.Width - 50;
                }
            }
            this.Top = (screenSize.Height - this.Height) / 2;
            this.Left = (screenSize.Width - this.Width) / 2;
        }
        /// <summary>
        /// Redraw the datagridview
        /// </summary>
        public void RedrawDataGridViewMoveLevel()
        {
            TSU.DataGridUtility.SaveSorting(dataGridViewMoveLevel);
            dataGridViewMoveLevel.Rows.Clear();
            List<List<object>> listRow = new List<List<object>>();
            int actualLineIndex = 0;
            List<int> index = new List<int>();
            foreach (string item in this.listPointNames)
            {
                AddNewMoveRowInDataGridView(listRow, actualLineIndex);
                actualLineIndex++;
            }
            foreach (List<Object> ligne in listRow)
            {
                dataGridViewMoveLevel.Rows.Add(ligne.ToArray());
            }
            for (int i = 0; i < dataGridViewMoveLevel.RowCount; i++)
            {
                dataGridViewMoveLevel.Rows[i].Height = 30; // met toutes les lignes en hauteur de 30 pour afficher correctement l'image de suppression.
            }
            TSU.DataGridUtility.RestoreSorting(dataGridViewMoveLevel);
            this.dataGridViewMoveLevel.Refresh();
        }
        /// <summary>
        /// Add a new row of move measure in the datagridview
        /// </summary>
        /// <param name="listRow"></param>
        /// <param name="item"></param>
        private void AddNewMoveRowInDataGridView(List<List<object>> listRow, int index)
        {
            List<object> row = new List<object>();
            row.Add(""); // 0 Point Name
            row.Add(""); // 1 Dcum
            row.Add(""); // 2 Move
            row.Add(""); // 3 Theo Reading
            listRow.Add(row);
            listRow[listRow.Count - 1][0] = this.listPointNames[index];
            listRow[listRow.Count - 1][1] = this.listDcum[index];
            listRow[listRow.Count - 1][2] = this.listTheoReading[index];
            listRow[listRow.Count - 1][3] = this.listMoveToDo[index];
        }
        /// <summary>
        /// Force le datagrid à reprendre le focus si on passe la souris dessus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewMoveLevel_MouseEnter(object sender, EventArgs e)
        {
            Action a = () =>
            {
                dataGridViewMoveLevel.Focus();
            };
            Tsunami2.Properties.InvokeOnApplicationDispatcher(a);
        }

        private void dataGridViewMoveLevel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonLeft.Text.ToLower().ToCharArray()[0] || e.KeyChar == 13)
            {
                SetRespond(this.buttonLeft);
                this.Close();
            }
        }

        private void buttonLeft_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonLeft.Text.ToLower().ToCharArray()[0] || e.KeyChar == 13)
            {
                SetRespond(this.buttonLeft);
                this.Close();
            }
        }

        private void titleAndMessageRichBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == this.buttonLeft.Text.ToLower().ToCharArray()[0] || e.KeyChar == 13)
            {
                SetRespond(this.buttonLeft);
                this.Close();
            }
        }
    }
}
