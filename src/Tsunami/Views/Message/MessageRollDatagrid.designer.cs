﻿using R = TSU.Properties.Resources;
namespace TSU.Views
{
    partial class MessageRollDataGridTSU
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.titleAndMessageRichBox = new System.Windows.Forms.RichTextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonMeasure = new System.Windows.Forms.Button();
            this.dataGridViewMesTilt = new System.Windows.Forms.DataGridView();
            this.Delete = new System.Windows.Forms.DataGridViewImageColumn();
            this.MeasureNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BeamDirection = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BeamOpposite = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Average = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Offset = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonLeft = new System.Windows.Forms.Button();
            this.buttonRight = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMesTilt)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // PopUpMenu
            // 
            this.PopUpMenu.BackColor = System.Drawing.Color.LightBlue;
            this.PopUpMenu.Location = new System.Drawing.Point(130, 130);
            // 
            // titleAndMessageRichBox
            // 
            this.titleAndMessageRichBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.titleAndMessageRichBox.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.titleAndMessageRichBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.titleAndMessageRichBox.Location = new System.Drawing.Point(3, 3);
            this.titleAndMessageRichBox.Name = "titleAndMessageRichBox";
            this.titleAndMessageRichBox.ReadOnly = true;
            this.titleAndMessageRichBox.Size = new System.Drawing.Size(511, 110);
            this.titleAndMessageRichBox.TabIndex = 25;
            this.titleAndMessageRichBox.TabStop = false;
            this.titleAndMessageRichBox.Text = "Title\n";
            this.titleAndMessageRichBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.titleAndMessageRichBox_KeyPress);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(8, 8);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(517, 666);
            this.panel2.TabIndex = 27;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.buttonMeasure, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dataGridViewMesTilt, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.titleAndMessageRichBox, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 83.33334F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(517, 666);
            this.tableLayoutPanel1.TabIndex = 26;
            // 
            // buttonMeasure
            // 
            this.buttonMeasure.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonMeasure.BackColor = System.Drawing.Color.LightGray;
            this.buttonMeasure.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.buttonMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonMeasure.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMeasure.Location = new System.Drawing.Point(188, 124);
            this.buttonMeasure.MaximumSize = new System.Drawing.Size(140, 0);
            this.buttonMeasure.MinimumSize = new System.Drawing.Size(140, 60);
            this.buttonMeasure.Name = "buttonMeasure";
            this.buttonMeasure.Size = new System.Drawing.Size(140, 61);
            this.buttonMeasure.TabIndex = 28;
            this.buttonMeasure.Text = "measure";
            this.buttonMeasure.UseVisualStyleBackColor = false;
            this.buttonMeasure.Click += new System.EventHandler(this.buttonMeasure_Click);
            this.buttonMeasure.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonRight_KeyPress);
            // 
            // dataGridViewMesTilt
            // 
            this.dataGridViewMesTilt.AllowUserToAddRows = false;
            this.dataGridViewMesTilt.AllowUserToDeleteRows = false;
            this.dataGridViewMesTilt.AllowUserToResizeColumns = false;
            this.dataGridViewMesTilt.AllowUserToResizeRows = false;
            this.dataGridViewMesTilt.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewMesTilt.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.dataGridViewMesTilt.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewMesTilt.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewMesTilt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMesTilt.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Delete,
            this.MeasureNumber,
            this.BeamDirection,
            this.BeamOpposite,
            this.Average,
            this.Offset});
            this.dataGridViewMesTilt.Cursor = System.Windows.Forms.Cursors.Arrow;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewMesTilt.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewMesTilt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewMesTilt.EnableHeadersVisualStyles = false;
            this.dataGridViewMesTilt.Location = new System.Drawing.Point(3, 197);
            this.dataGridViewMesTilt.MultiSelect = false;
            this.dataGridViewMesTilt.Name = "dataGridViewMesTilt";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewMesTilt.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewMesTilt.RowHeadersVisible = false;
            this.dataGridViewMesTilt.Size = new System.Drawing.Size(511, 383);
            this.dataGridViewMesTilt.TabIndex = 0;
            this.dataGridViewMesTilt.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewMesTilt_CellClick);
            this.dataGridViewMesTilt.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewMesTilt_CellValueChanged);
            this.dataGridViewMesTilt.CurrentCellChanged += new System.EventHandler(this.dataGridViewMesTilt_CurrentCellChanged);
            this.dataGridViewMesTilt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dataGridViewMesTilt_KeyPress);
            this.dataGridViewMesTilt.MouseEnter += new System.EventHandler(this.dataGridViewMesTilt_MouseEnter);
            // 
            // Delete
            // 
            this.Delete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Delete.FillWeight = 60.9137F;
            this.Delete.HeaderText = "";
            this.Delete.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.Delete.MinimumWidth = 30;
            this.Delete.Name = "Delete";
            this.Delete.ReadOnly = true;
            this.Delete.Width = 30;
            // 
            // MeasureNumber
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.MeasureNumber.DefaultCellStyle = dataGridViewCellStyle2;
            this.MeasureNumber.FillWeight = 107.8173F;
            this.MeasureNumber.HeaderText = global::TSU.Properties.Resources.StringTilt_DataGrid_MesureNumber;
            this.MeasureNumber.Name = "MeasureNumber";
            this.MeasureNumber.ReadOnly = true;
            this.MeasureNumber.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.MeasureNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // BeamDirection
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.BeamDirection.DefaultCellStyle = dataGridViewCellStyle3;
            this.BeamDirection.FillWeight = 107.8173F;
            this.BeamDirection.HeaderText = global::TSU.Properties.Resources.StringTilt_DataGrid_TiltBeam;
            this.BeamDirection.Name = "BeamDirection";
            this.BeamDirection.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // BeamOpposite
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.BeamOpposite.DefaultCellStyle = dataGridViewCellStyle4;
            this.BeamOpposite.FillWeight = 107.8173F;
            this.BeamOpposite.HeaderText = global::TSU.Properties.Resources.StringTilt_DataGrid_TiltOpposite;
            this.BeamOpposite.Name = "BeamOpposite";
            this.BeamOpposite.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Average
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Average.DefaultCellStyle = dataGridViewCellStyle5;
            this.Average.FillWeight = 107.8173F;
            this.Average.HeaderText = global::TSU.Properties.Resources.StringTilt_DataGrid_TiltAverage;
            this.Average.Name = "Average";
            this.Average.ReadOnly = true;
            this.Average.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Offset
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Offset.DefaultCellStyle = dataGridViewCellStyle6;
            this.Offset.FillWeight = 107.8173F;
            this.Offset.HeaderText = global::TSU.Properties.Resources.StringTilt_DataGrid_Offset;
            this.Offset.Name = "Offset";
            this.Offset.ReadOnly = true;
            this.Offset.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.93877F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 252F));
            this.tableLayoutPanel2.Controls.Add(this.buttonLeft, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.buttonRight, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 586);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(511, 77);
            this.tableLayoutPanel2.TabIndex = 26;
            // 
            // buttonLeft
            // 
            this.buttonLeft.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonLeft.BackColor = System.Drawing.Color.LightGray;
            this.buttonLeft.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.buttonLeft.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLeft.Location = new System.Drawing.Point(59, 8);
            this.buttonLeft.MaximumSize = new System.Drawing.Size(140, 0);
            this.buttonLeft.MinimumSize = new System.Drawing.Size(140, 60);
            this.buttonLeft.Name = "buttonLeft";
            this.buttonLeft.Size = new System.Drawing.Size(140, 60);
            this.buttonLeft.TabIndex = 8;
            this.buttonLeft.Text = "buttonLeft";
            this.buttonLeft.UseVisualStyleBackColor = false;
            this.buttonLeft.Click += new System.EventHandler(this.On_buttonClick);
            this.buttonLeft.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonLeft_KeyPress);
            // 
            // buttonRight
            // 
            this.buttonRight.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonRight.BackColor = System.Drawing.Color.LightGray;
            this.buttonRight.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.buttonRight.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRight.ForeColor = System.Drawing.Color.Black;
            this.buttonRight.Location = new System.Drawing.Point(315, 8);
            this.buttonRight.MaximumSize = new System.Drawing.Size(140, 200);
            this.buttonRight.MinimumSize = new System.Drawing.Size(140, 60);
            this.buttonRight.Name = "buttonRight";
            this.buttonRight.Size = new System.Drawing.Size(140, 60);
            this.buttonRight.TabIndex = 6;
            this.buttonRight.Text = "buttonRight";
            this.buttonRight.UseVisualStyleBackColor = false;
            this.buttonRight.Click += new System.EventHandler(this.On_buttonClick);
            this.buttonRight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonRight_KeyPress);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MessageRollDataGridTSU
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(533, 682);
            this.Controls.Add(this.panel2);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MinimumSize = new System.Drawing.Size(450, 200);
            this.Name = "MessageRollDataGridTSU";
            this.Padding = new System.Windows.Forms.Padding(8);
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Message...";
            this.Shown += new System.EventHandler(this.MessageDataGridTSU_Shown);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.buttonForFocusWithTabIndex0, 0);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMesTilt)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.RichTextBox titleAndMessageRichBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView dataGridViewMesTilt;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridViewImageColumn Delete;
        private System.Windows.Forms.DataGridViewTextBoxColumn MeasureNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn BeamDirection;
        private System.Windows.Forms.DataGridViewTextBoxColumn BeamOpposite;
        private System.Windows.Forms.DataGridViewTextBoxColumn Average;
        private System.Windows.Forms.DataGridViewTextBoxColumn Offset;
        public System.Windows.Forms.Button buttonMeasure;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.Button buttonLeft;
        public System.Windows.Forms.Button buttonRight;
    }
}