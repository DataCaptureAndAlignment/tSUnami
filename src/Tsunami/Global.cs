﻿using System;
using System.Collections.Generic;
using System.Linq;
using TSU.Common;

namespace TSU
{


    public static class Globals
    {
        public static List<Views.TsuView> createdViews = new List<Views.TsuView>();
        public static int[] numberCreated = { 0, 0, 0, 0 }; // [0] view create, [1] total view, [2] log create, [3] total log

        public static string AppName;
        public static string AppNameDescription;
        public static System.Drawing.Color AppColor;
        public static System.Drawing.Image AppImage;

        /// <summary>
        /// this is used in lgc to identify a measurement from input to output, the id must be unique.
        /// </summary>
        internal static int MeasureIdNumberCount;

        internal static bool IsBeta=false;


        /// <summary>
        /// Detects if we are running inside a unit test.
        /// </summary>
        public static class UnitTestDetector
        {
            static UnitTestDetector()
            {
                string testAssemblyV1Name = "Microsoft.VisualStudio.QualityTools.UnitTestFramework";
                string testAssemblyV2Name = "Microsoft.VisualStudio.TestPlatform.TestFramework";
                var assemblies = AppDomain.CurrentDomain.GetAssemblies();
                UnitTestDetector.RunningInTest = assemblies
                    .Any(a => a.FullName.StartsWith(testAssemblyV1Name) || a.FullName.StartsWith(testAssemblyV2Name));
            }

            public static bool RunningInTest { get; private set; }
        }


    }
    public static class Tsunami2
    {
        public static Common.TsunamiView View
        {
            get
            {
                if (Tsunami2.Properties == null)
                    throw new TsunamiException("Tsunami.View is not available to display messages.");
                return Tsunami2.Properties.View;
            }
        }

        public static Tsunami Properties { get; set; }

        public static void InitializePropertiesForUnitTesting()
        {
            if (Properties == null) // this should happen only in test environnement as tsunami is created/set in  main() in ths tsunami project
            {
                Tsunami t = new Tsunami(null);
                Properties = t;
                var test = Properties.Gadgets;
                TSU.Preferences.Preferences.Instance.tsunami = t;
            }
        }
        public static void InitializeTsunamiViewForUnitTesting()
        {
            if (Properties == null)
                InitializePropertiesForUnitTesting();

            
            Splash.Module sm = new Splash.Module(Properties);
            Properties.View.SplashScreenView = sm.View;
            
        }

        public static class Preferences
        {
            public static Common.Tsunami Instance
            {
                get
                {
                    return TSU.Preferences.Preferences.Instance.tsunami;
                }
            }

            public static Common.Tsunami Tsunami
            {
                get
                {
                    return TSU.Preferences.Preferences.Instance.tsunami;
                }
            }

            public static TSU.Preferences.Preferences Values
            {
                get
                {
                    return TSU.Preferences.Preferences.Instance;
                }
            }

            public static TSU.Preferences.GUI.Theme Theme
            {
                get
                {
                    if (TSU.Preferences.Preferences.Instance.GuiPrefs != null)
                    {
                        return TSU.Preferences.Preferences.Instance.GuiPrefs.Theme;
                    }
                    else
                    {
                        return new TSU.Preferences.GUI.Theme();
                    }
                }
            }
        }

    }
}