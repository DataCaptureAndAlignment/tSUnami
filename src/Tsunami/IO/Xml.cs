﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using TSU.Common;
using R = TSU.Properties.Resources;
using System.Collections.Generic;
using System.Xml.XPath;

namespace TSU.IO
{
    public static class Xml
    {
        public static void CreateFile(object o, string filePath)
        {
            try
            {
                //write xml
                string xmlPath = System.IO.Path.ChangeExtension(filePath, "xml");

                string directoryPath = System.IO.Path.GetDirectoryName(xmlPath);
                if (!string.IsNullOrEmpty(directoryPath) && !Directory.Exists(directoryPath))
                {
                    // Create the directory if it doesn't exist
                    Directory.CreateDirectory(directoryPath);
                    Console.WriteLine($"Directory created: {directoryPath}");
                }
                string tmpPath = xmlPath + ".tmp";
                using (StreamWriter file = File.CreateText(tmpPath))
                {
                    Serialize(o, file);
                }
                // If everything worked fine, overwrite the xml with the xml.tmp
                if (File.Exists(xmlPath))
                    File.Delete(xmlPath);
                File.Move(tmpPath, xmlPath);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(R.T011, filePath, ex.Message), ex);
            }
        }

        public static bool BrowseAnXml(out string path)
        {
            OpenFileDialog theDialog = new OpenFileDialog();
            theDialog.Title = R.T_OPEN_TEXT_FILE;
            theDialog.Filter = "Macro|*.XML";
            theDialog.InitialDirectory = TSU.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay;
            TsunamiView.ViewsShownAsModal.Add(theDialog);
            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                path = theDialog.FileName.ToString();
                return true;
            }
            else
            {
                path = "";
                return false;
            }
        }

        public static void Serialize(object o, TextWriter file)
        {
            XmlSerializerNamespaces nameSpace = GetNameSpaces(o.GetType());
            XmlSerializer serializer = new XmlSerializer(o.GetType());

            //if (Debug.GetDebugging())
            //    AnalyzeXmlForDebug(o, nameSpace, serializer);

            serializer.Serialize(file, o, nameSpace);
        }

        //private static void AnalyzeXmlForDebug(object o, XmlSerializerNamespaces nameSpace, XmlSerializer serializer)
        //{
        //    using (var sw = new StringWriter())
        //    {
        //        // Serialize the xml to memory, without the NameSpaces object
        //        serializer.Serialize(sw, o);

        //        // Get the defined namespaces; we will ignore them
        //        HashSet<string> namespacesDefined = new HashSet<string>();
        //        foreach (XmlQualifiedName ns in nameSpace.ToArray())
        //            namespacesDefined.Add(ns.Namespace);

        //        // Get the list of used namespaces
        //        Dictionary<string, HashSet<string>> namespacesUsed = new Dictionary<string, HashSet<string>>();
        //        using (StringReader sr = new StringReader(sw.ToString()))
        //        {
        //            XPathDocument x = new XPathDocument(sr);
        //            XPathNavigator foo = x.CreateNavigator();
        //            while (foo.MoveToFollowing(XPathNodeType.Element))
        //                foreach (KeyValuePair<string, string> v in foo.GetNamespacesInScope(XmlNamespaceScope.Local))
        //                    if (!namespacesDefined.Contains(v.Value))
        //                        if (namespacesUsed.ContainsKey(v.Value))
        //                            namespacesUsed[v.Value].Add(v.Key);
        //                        else
        //                            namespacesUsed.Add(v.Value, new HashSet<string>() { v.Key });
        //        }

        //        // Display what's left
        //        if (namespacesUsed.Count > 0)
        //        {
        //            Debug.WriteInConsole($"XML serialization for {o.GetType().FullName} has {namespacesUsed.Count} undefined namespaces used.");
        //            foreach (var v in namespacesUsed)
        //                Debug.WriteInConsole($"Namespace {v.Key}, auto-aliases {string.Join(",", v.Value)}");
        //        }
        //    }
        //}

        private static XmlSerializerNamespaces GetNameSpaces(Type t)
        {
            XmlSerializerNamespaces nameSpace = new XmlSerializerNamespaces();

            //Standard headers
            nameSpace.Add("xml", "http://www.w3.org/XML/1998/namespace");
            nameSpace.Add("xsd", "http://www.w3.org/2001/XMLSchema");
            nameSpace.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");

            //Our default namespace has no name
            nameSpace.Add("", "");

            //For TSU files, we define some we often use
            if (t == typeof(Tsunami))
            {
                nameSpace.Add("t", "TSU");
                nameSpace.Add("p", "TSU.Polar");
                nameSpace.Add("m", "Measures");
                nameSpace.Add("ms", "Measures.States");
            }

            return nameSpace;
        }

        public static string ToText(object o)
        {
            StringWriter writer = new StringWriter();
            Serialize(o, writer);
            writer.Close();
            return writer.ToString();
        }

        public static object DeserializeFile(Type type, string path)
        {
            try
            {
                if (!System.IO.File.Exists(path)) throw new Exception(R.T480 + path + " " + R.T479);
                using (StreamReader fileReader = File.OpenText(path))
                {
                    XmlSerializer serializer = new XmlSerializer(type);
                    serializer.UnknownAttribute += OnUnknownAttribute;
                    serializer.UnknownElement += OnUnknownElement;
                    return serializer.Deserialize(fileReader);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(R.T037, ex.Message), ex);
            }
        }

        private static void OnUnknownElement(object sender, XmlElementEventArgs e)
        {
            Type t = e.ObjectBeingDeserialized.GetType();
            System.Reflection.PropertyInfo propertyInfo = t.GetProperty(e.Element.Name);
            if (propertyInfo != null && propertyInfo.CanWrite)
            {
                Type t2 = propertyInfo.PropertyType;
                if (t2 == typeof(double) && double.TryParse(e.Element.InnerText, out double d))
                    propertyInfo.SetValue(e.ObjectBeingDeserialized, d);
                else if (t2 == typeof(string))
                    propertyInfo.SetValue(e.ObjectBeingDeserialized, e.Element.InnerText);
            }
        }

        private static void OnUnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            Type t = e.ObjectBeingDeserialized.GetType();
            System.Reflection.PropertyInfo propertyInfo = t.GetProperty(e.Attr.Name);
            if (propertyInfo != null && propertyInfo.CanWrite)
            {
                Type t2 = propertyInfo.PropertyType;
                if (t2 == typeof(double) && double.TryParse(e.Attr.InnerText, out double d))
                    propertyInfo.SetValue(e.ObjectBeingDeserialized, d);
                else if (t2 == typeof(string))
                    propertyInfo.SetValue(e.ObjectBeingDeserialized, e.Attr.InnerText);
            }
        }

        public static Type CheckType(string xmlFilePath)
        {
            if (xmlFilePath == "") return null;

            XmlDocument doc = OpenXmlDocument(xmlFilePath);

            switch (doc.DocumentElement.Name)
            {
                case "TheodoliteModule": return typeof(Polar.Module);
                case "LevelModule": return typeof(Level.Module);
                case "TiltModule": return typeof(Tilt.Module);
                case "LineModule": return typeof(Line.Module);
                case "GuidedModule": return typeof(Common.Guided.Module);
                case "TdH": return typeof(Polar.GuidedModules.TdH);
                case "GuidedGroupModule": return typeof(Common.Guided.Group.Module);
                case "Tsunami": return typeof(Common.Tsunami);
                default: throw new NotImplementedException();
            }
        }

        public static XmlDocument OpenXmlDocument(string xmlFilePath)
        {
            XmlDocument doc = new XmlDocument();

            try
            {
                doc.Load(xmlFilePath);
            }
            catch (Exception)
            {
                // Try to read the file to memory, it helps for files with the wrong encoding in the xml header
                doc.LoadXml(File.ReadAllText(xmlFilePath));
            }

            return doc;
        }
    }
}
