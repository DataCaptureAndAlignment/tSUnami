﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using R = TSU.Properties.Resources;
using E = TSU.Common.Elements;
using MM = TSU.Common.Measures;
using I = TSU.Common.Instruments;

using TSU.Preferences;
using TSU.Views;
using M = TSU;
using System.Globalization;


namespace TSU.IO
{
    public static class Path
    {
        public static string AskFileLocation(string fileName, string directoryPath = "", string defaultExtention = ".dat")
        {
            try
            {
                directoryPath = (directoryPath == "") ?TSU.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay : directoryPath;
                SaveFileDialog sfd = new SaveFileDialog()
                {
                    FileName = fileName,
                    Title = $"{R.T_SELECT_THE_PLACE_WHERE_TO_EXPORT_YOUR_DATA}, { R.T_THE_FILE_WILL_BE_APPEND_IF_IT_EXISTS_OR_WILL_BE_CREATED_IF_NOT}.",
                    DefaultExt = defaultExtention,
                    InitialDirectory = directoryPath,
                    CheckFileExists = false,
                    OverwritePrompt = false
                };

                TSU.Common.TsunamiView.ViewsShownAsModal.Add(sfd);
                if (sfd.ShowDialog() != DialogResult.OK) return "";
                TSU.Common.TsunamiView.ViewsShownAsModal.Remove(sfd);
                return sfd.FileName;
            }
            catch (Exception ex)
            {
                // throw new Exception(string.Format("Could not receive a path from user" + ";" + ex.Message));
                throw new Exception($"{R.T_COULD_NOT_RECEIVE_A_PATH_FROM_USER};{ex.Message}", ex);
            }
        }
    }
}
