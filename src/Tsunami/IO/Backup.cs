﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

using R = TSU.Properties.Resources;

namespace TSU.IO
{
    public static class Backup
    {
        public static string Create(string filePath, string triggerMessage = "", bool keepOriginalPlace = false)
        {
            //
            // create name of the back up
            //

            Logs.Log.AddEntryAsBeginningOf(TSU.Tsunami2.Preferences.Tsunami, $"Backup started (triggered by '{triggerMessage}')");
            // get file name without path and extension
            FileInfo originalFileInfo = new FileInfo(filePath);
            string fileName = System.IO.Path.GetFileNameWithoutExtension(filePath);

            // replace previosu backup comment
            string pattern = @"_Backup of \d+ \d+h\d+m\d+ version [\d\.]+";
            fileName = Regex.Replace(fileName, pattern, "");

            // change path to backup
            string backupDir;
            if (keepOriginalPlace)
            {
                backupDir = originalFileInfo.DirectoryName + "\\";
            }
            else
            {
                backupDir = TSU.Preferences.Preferences.Instance.Paths.Backups;
                if (!Directory.Exists(backupDir))
                    Directory.CreateDirectory(backupDir);
            }
               

            string backUpFullPath = backupDir + fileName;

            // add bacup detail to the name
            backUpFullPath += $"_{R.T_BACKUP_OF} {DateTime.Now:dd HH}h{DateTime.Now:mm}m{DateTime.Now:ss} {triggerMessage}";

            // add extension 
            backUpFullPath += originalFileInfo.Extension;

            //
            // Move the file
            //
            string backupNameTest = backUpFullPath;
            int i = 1;
            while (File.Exists(backUpFullPath))
            {
                backUpFullPath = backupNameTest + "(" + i.ToString() + ")";
            }
            File.Copy(filePath, backUpFullPath);
            Logs.Log.AddEntryAsFinishOf(TSU.Tsunami2.Preferences.Tsunami, $"{backUpFullPath} backup.");
            return backUpFullPath;
        }
    }
}
