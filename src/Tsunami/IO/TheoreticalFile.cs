﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using TSU.Common.Elements;
using TSU.Views;
using TSU.Views.Message;
using C = TSU.Common.Elements.Composites;
using E = TSU.Common.Elements;
using R = TSU.Properties.Resources;
using T = TSU.Tools;
using Z = TSU.Common.Zones;

namespace TSU.IO
{
    public static class TheoreticalFile
    {
        public static C.TheoreticalElement TheoreticalElement(TSU.Views.TsuView view, FileInfo filePath,
            E.Coordinates.CoordinateSystemsTsunamiTypes type, Z.Zone proposedZone, bool isDotDat)
        {
            // set the thing by default, that can be cahnge after if local zone selected
            C.TheoreticalElement t = new C.TheoreticalElement();

            t.Definition.InitialTsunamiSystems = type;

            if (type == E.Coordinates.CoordinateSystemsTsunamiTypes.CCS)
                t.Definition.InitialCoordinateSystems = E.Coordinates.CoordinateSystemsTypes._2DPlusH;
            else
                t.Definition.InitialCoordinateSystems = E.Coordinates.CoordinateSystemsTypes._3DCartesian;

            // Choose zone
            if (filePath.Extension.ToUpper() != ".DAT")
            {
                if (!t.Definition.Validity)
                {
                    string yes = R.T_YES;
                    if (t.Definition.LocalZone == null)
                    {
                        if (proposedZone != null)
                        {
                            string titleAndMessage = string.Format(R.EM_ExistingZone, proposedZone);
                            new MessageInput(MessageType.Choice, titleAndMessage).Show();
                            t.Definition.LocalZone = proposedZone;
                        }
                        else
                        {
                            MessageInput mi = new MessageInput(MessageType.Choice, R.EM_NoZone)
                            {
                                ButtonTexts = new List<string> { yes, R.T_NO },
                            };
                            if (mi.Show().TextOfButtonClicked == yes)
                            {
                                t.Definition.LocalZone = Z.Zone.FixDefinition(view, filePath);
                                if (t.Definition.LocalZone == null)
                                    t.Definition.LocalZone = new Z.Zone() { Ccs2MlaInfo = new Z.Zone.Ccs2Mla() { ReferenceSurface = E.Coordinates.ReferenceSurfaces.RS2K } };
                            }
                            else
                                t.Definition.LocalZone = new Z.Zone() { Ccs2MlaInfo = new Z.Zone.Ccs2Mla() { ReferenceSurface = E.Coordinates.ReferenceSurfaces.RS2K } };
                        }
                    }
                }
            }
            else
                t.Definition.LocalZone = new Z.Zone() { Ccs2MlaInfo = new Z.Zone.Ccs2Mla() { ReferenceSurface = E.Coordinates.ReferenceSurfaces.RS2K } };

            if (t.Definition.LocalZone.Ccs2MlaInfo == null)
                t.Definition.LocalZone.Ccs2MlaInfo = new Z.Zone.Ccs2Mla() { ReferenceSurface = E.Coordinates.ReferenceSurfaces.RS2K };

            t.Definition.InitialReferenceFrames = E.Coordinates.CorrespondingReferenceFrame(type, t.Definition.LocalZone.Ccs2MlaInfo.ReferenceSurface);

            // Read and fill
            using (StreamReader reader = new StreamReader(filePath.FullName, Encoding.Default))
            {
                FillTheoreticalElementFromstring(ref t, reader, filePath.FullName, isDotDat);
            }

            return t;
        }



        /// <summary>
        /// Get point and zone definition from xml file
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        public static C.TheoreticalElement TheoreticalElementFromXml(FileInfo filePath)
        {
            C.TheoreticalElement n = Xml.DeserializeFile(typeof(C.TheoreticalElement), filePath.FullName) as C.TheoreticalElement;

            n.SetNetworkFilePath(filePath.FullName);

            if (n.Definition.LocalZone == null) n.Definition.LocalZone = new Z.Zone() { Ccs2MlaInfo = new Z.Zone.Ccs2Mla() { ReferenceSurface = E.Coordinates.ReferenceSurfaces.RS2K } };

            List<E.Point> l = null;
            if (n.PointInIdXYZFormat != null)
                l = (T.Conversions.FileFormat.ConvertAsciiToPoints(filePath.FullName, n.PointInIdXYZFormat, T.Conversions.Points.PointAsciiFormat.IDXYZ, n.Definition.InitialReferenceFrames, n.Definition.InitialCoordinateSystems));
            else
                if (n.PointInGeodeFormat != null)
                l = (T.Conversions.FileFormat.ConvertAsciiToPoints(filePath.FullName, n.PointInGeodeFormat, T.Conversions.Points.PointAsciiFormat.Geode, n.Definition.InitialReferenceFrames, n.Definition.InitialCoordinateSystems));

            if (l != null)
                foreach (E.Point element in l)
                {
                    n.Add(element);
                }
            return n;
        }

        public static void FillTheoreticalElementFromstring(ref C.TheoreticalElement t, TextReader StringReader,
            string origin, bool isDotDat)
        {
            string line;
            t._Name = origin;
            t._Origin = origin;
            t.fileElementType = ENUM.ElementType.File;
            t._OriginType = ENUM.ElementType.TheoreticalFile;
            DateTime fileModificationDate = File.GetLastWriteTime(origin);
            DateTime lastDateTime = fileModificationDate;

            List<E.Point> temporaryList = new List<E.Point>();
            while ((line = StringReader.ReadLine()) != null)
            {
                line = line.Trim();
                if (line != "")
                {
                    var firstChar = line.Substring(0, 1);
                    if (firstChar != "*" && firstChar != "#")
                    {
                        if (line.ToUpper() == "%FRAME TRANSFORMATION PARAMETERS") // "*.coo" detection to ignore frame data
                            break;

                        if (firstChar != "%")
                        {
                            E.Point point = null;
                            if (isDotDat)
                            {
                                // here we check when to stop the reading because we dont want to read all 'BEAM_' points and *FRAME there is too much of them, and we need only some
                                string temp = line.TrimStart();
                                if (temp.StartsWith("BEAM_") || temp.StartsWith("*FRAME"))
                                    break;

                                Coordinates.ReferenceSurfaces rs = (t.Definition.LocalZone == null) ? Coordinates.ReferenceSurfaces.RS2K : t.Definition.LocalZone.Ccs2MlaInfo.ReferenceSurface;
                                t.Definition.InitialTsunamiSystems = Coordinates.CoordinateSystemsTsunamiTypes.CCS;
                                t.Definition.InitialReferenceFrames = Coordinates.CorrespondingReferenceFrame(t.Definition.InitialTsunamiSystems, rs);
                                point = IO.SUSoft.Geode.ReadLine(line);
                                point._Origin = origin;
                                point.Date = lastDateTime.AddSeconds(1);
                                lastDateTime = point.Date;
                                temporaryList.Add(point);
                            }
                            else
                            {
                                switch (t.Definition.InitialTsunamiSystems)
                                {

                                    case Coordinates.CoordinateSystemsTsunamiTypes.Unknown:
                                    case Coordinates.CoordinateSystemsTsunamiTypes.CCS:
                                    case Coordinates.CoordinateSystemsTsunamiTypes.PHYS:
                                    case Coordinates.CoordinateSystemsTsunamiTypes.SU:
                                    case Coordinates.CoordinateSystemsTsunamiTypes.MLA:
                                        point = T.Conversions.FileFormat.IdXYZLineToPoint(line, t.Definition.InitialReferenceFrames, t.Definition.InitialCoordinateSystems);
                                        if (point._Zone == R.String_Unknown && t.Definition.LocalZone != null)
                                        {
                                            point._Zone = t.Definition.LocalZone._Name;
                                            point._Accelerator = point._Zone;
                                        }
                                        point._Origin = origin;
                                        point.Date = lastDateTime.AddSeconds(1);
                                        lastDateTime = point.Date;
                                        t.Add(point);
                                        break;
                                    case Coordinates.CoordinateSystemsTsunamiTypes.UserDefined:
                                    case Coordinates.CoordinateSystemsTsunamiTypes.ToStation:
                                    case Coordinates.CoordinateSystemsTsunamiTypes.Beam:
                                    case Coordinates.CoordinateSystemsTsunamiTypes.Displacement:
                                    default:

                                        break;
                                }
                                point.StorageStatus = TSU.Common.Tsunami.StorageStatusTypes.Keep; // this does : //Tsunami2.Properties.Points.Add(point);

                            }
                        }
                    }
                }
            }
            if (temporaryList.Count > 0) // check if it was used
            {
                if (temporaryList.Count > Tsunami2.Preferences.Values.GuiPrefs.MaxNumberOfPointsPerFileBeforeWarning) // check if it was used
                {
                    Reduce(temporaryList);

                }
                foreach (var item in temporaryList)
                {
                    t.AddInHierarchy(item, E.Element.HowToRename.AddParenthesis);
                    item.StorageStatus = TSU.Common.Tsunami.StorageStatusTypes.Keep; // this does : //Tsunami2.Properties.Points.Add(item);
                }
            }
        }

        private static void Reduce(List<E.Point> l)
        {
            if (Debug.IsRunningInATest)
                return; 

            string thresholdMessage = R.MINIMUM_NUMBER_OF_POINTS_TO_TRIGGER_THIS_MESSAGE;
            string thresholdMessage2 = thresholdMessage + " " +
                                       Tsunami2.Preferences.Values.GuiPrefs.MaxNumberOfPointsPerFileBeforeWarning;
            string titleAndMessage = string.Format(R.T_A_LOT_OF_POINTS_THERE, l.Count);
            MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
            {
                ButtonTexts = new List<string> { R.T_YES, R.T_NO },
                Controls = new List<Control> { CreationHelper.GetPreparedTextBox(thresholdMessage2) }
            };
            using (var r = mi.Show())
            {
                // change the threhold in the guipref
                string s = (r.ReturnedControls[0] as TextBox).Text;
                s = s.Replace(thresholdMessage, "").Trim();
                if (int.TryParse(s, out int threshold))
                {
                    Tsunami2.Preferences.Values.GuiPrefs.MaxNumberOfPointsPerFileBeforeWarning = threshold;
                    Tsunami2.Preferences.Values.SaveGuiPrefs();
                }

                if (r.TextOfButtonClicked == R.T_YES)
                {
                    E.Manager.Module em = new E.Manager.Module();
                    em.AllElements = new List<TsuObject>();
                    em._SelectedObjects = new List<TsuObject>();
                    em.SelectableObjects = new List<TsuObject>();

                    em.CreateView();

                    em.AllElements.AddRange(l);
                    C.CompositeElement c = em.SelectPoints(R.T_SELECT_POINT_YOU_WANT_TO_KEEP);
                    if (c != null)
                    {
                        List<E.Point> keeps = c.GetPoints();
                        l.Clear();
                        l.AddRange(keeps);
                        em.View.Dispose();
                        em.Dispose();
                    }
                }
            }
        }

        public static C.CompositeElement TheoreticalElement(string stream, string p, bool isDotDat)
        {
            C.TheoreticalElement t = new C.TheoreticalElement();
            System.IO.StringReader reader = new System.IO.StringReader(stream);
            FillTheoreticalElementFromstring(ref t, reader, p, isDotDat);
            return t;
        }
    }
}
