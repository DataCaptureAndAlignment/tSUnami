﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using R = TSU.Properties.Resources;
using E = TSU.Common.Elements;
using MM = TSU.Common.Measures;
using I = TSU.Common.Instruments;

using TSU;
using TSU.Preferences;
using TSU.Views;
using M = TSU;
using System.Globalization;

namespace TSU.IO
{
    public static class Csv
    {
        public static void ToCsv(object o, string completeDefinitivePath) // polymorphisme used asVisitor pattern
        {
            string p = completeDefinitivePath;

            List<String> l = new List<string>();

            Csv.Visit(o, ref l, ref p);

            WriteToFile(l, p);
        }

        private static void WriteToFile(List<string> s, string completeDefinitivePath)
        {
            string p = completeDefinitivePath;
            try
            {
                if (s.Count == 0) throw new Exception(R.T_NOTHING_TO_EXPORT);

                if (File.Exists(p)) File.Delete(p);
                File.WriteAllLines(p, s);
            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_COULD_NOT_WRITE_FILE} '{p}'.\r\n{ex.Message}", ex);
            }

        }

        public static void Visit(object o, ref List<String> l, ref string path)
        {
            try
            {
                dynamic dynamicMeasure = o; //notice the 'dynamic' type. this is the key to dynamic dispatch
                VisitCore(dynamicMeasure, ref l, ref path);
            }
            catch (Exception ex)
            {

                //  throw new Exception( string.Format(R.T_CANT_VISIT_GEODE, o.GetType().ToString(), e.Message));
                throw new Exception($"{R.T_CANT_VISIT_CSV} {o.GetType().ToString()}\r\n{ex.Message}", ex);
            }

        }

        public static void VisitCore(M.Manager o, ref List<String> l, ref string path)
        {
            // getting the column name from the listview
            string s = "";
            foreach (string item in (o._TsuView as ManagerView).ListviewColumnHeaderTexts)
            {
                if (s != "") s += ";";
                s += item;
            }
            l.Add(s);

            // getting the listview data
            foreach (TsuObject item in o._SelectedObjects)
            {
                Visit(item, ref l, ref path);
            }

        }
        public static void VisitCore(MM.Measure o, ref List<String> l, ref string path)
        {
            l.Add(GetCsvLineFromAnListViewItemLine(o));
        }

        public static void VisitCore(E.Element o, ref List<String> l, ref string path)
        {
            l.Add(GetCsvLineFromAnListViewItemLine(o));
        }

        public static void VisitCore(MM.Manager o, ref List<String> l, ref string path)
        {
            foreach (var item in o.AllElements)
            {
                if (item is MM.Measure)
                    l.Add(GetCsvLineFromAnListViewItemLine(item));
            }

        }

        private static string GetCsvLineFromAnListViewItemLine(TsuObject o) // my be the opposite should be done???
        {
            // create the item
            TsuListViewItem temp = new TsuListViewItem(o);

            // changeit into a line
            string s = "";

            // next line if it is not the first one
            if (s != "") s += "\r\n";
            int count = 0;
            // add all columln to the line
            for (int i = 0; i < temp.SubItems.Count; i++)
            {
                if (count > 0) s += ';';
                s += temp.SubItems[i].Text;
                count++;
            }
            return s;
        }

        private static string GetCsvHeaderLineFromAnListViewItemLine(TsuObject o) // my be the opposite should be done???
        {
            // create the item
            TsuListViewItem temp = new TsuListViewItem(o);

            // changeit into a line
            string s = "";

            if (s != "") s += ';';
            s += temp.Name;

            return s;
        }
    }
}
