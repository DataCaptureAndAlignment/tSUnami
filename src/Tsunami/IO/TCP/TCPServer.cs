﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;using R = TSU.Properties.Resources;
using System.Threading;

namespace TSU.IO.TCP
{
    // State object for reading client data asynchronously
    public class StateObject
    {
        // Client  socket.
        public Socket workSocket = null;
        // Size of receive buffer.
        public const int BufferSize = 1024;
        // Receive buffer.
        public byte[] buffer = new byte[BufferSize];
        // Received data string.
        public StringBuilder sb = new StringBuilder();

    }

    public class AsynchronousServer
    {

        public enum TransmissionSate
        {
            Waiting,
            Receiving,
            Received,
            Sending,
            Sent,
            Off
        }

        // Thread signal.
        public ManualResetEvent allDone = new ManualResetEvent(false);

        public  IPHostEntry ipHostInfo;
        public  IPAddress ipAddress;
        public  IPEndPoint localEndPoint;
        public  Socket listener;
        public  Socket handler;

        public int port = 11000;
        public String nextMessage = String.Empty;

        //flags
        public static TransmissionSate currentState = TransmissionSate.Off;


        public AsynchronousServer()
        {
            Thread tcpServerThread = new Thread(delegate()
            {
                //socket opening
                this.StartListening();
            });
            tcpServerThread.IsBackground = true;
            tcpServerThread.Start();
        }

        //Start Listening for Client
        public  void StartListening()
        {
            // Data buffer for incoming data.
            byte[] bytes = new Byte[1024];

            // Establish the local endpoint for the socket.
            // The DNS name of the computer
            ipHostInfo = Dns.Resolve(Dns.GetHostName());
            ipAddress = ipHostInfo.AddressList[0];
            localEndPoint = new IPEndPoint(ipAddress, 11000);

            // Create a TCP/IP socket.
            listener = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and listen for incoming connections.
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(100);

                while (true)
                {
                    // Set the event to nonsignaled state.
                    allDone.Reset();

                    // Start an asynchronous socket to listen for connections.
                    TSU.Debug.WriteInConsole(R.T512);
                    listener.BeginAccept(
                        new AsyncCallback(AcceptCallback),
                        listener);

                    //flag
                    currentState = TransmissionSate.Waiting;

                    // Wait until a connection is made before continuing.
                    allDone.WaitOne();
                }

            }
            catch (Exception e)
            {
                TSU.Debug.WriteInConsole(e.ToString());
            }


            TSU.Debug.WriteInConsole("TSU.TCPIP "+R.T513);
            //Console.Read();

        }

        //Accept communication and Start reading
        public  void AcceptCallback(IAsyncResult ar)
        {
            // Signal the main thread to continue.
            allDone.Set();

            // Get the socket that handles the client request.
            listener = (Socket)ar.AsyncState;
            handler = listener.EndAccept(ar);

            //flag
            currentState = TransmissionSate.Receiving;

            // Create the state object.
            StateObject state = new StateObject();
            state.workSocket = handler;
            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReadCallback), state);
        }

        //Read and handle the information
        public  void ReadCallback(IAsyncResult ar)
        {
            try
            {
                String content = String.Empty;

                // Retrieve the state object and the handler socket
                // from the asynchronous state object.
                StateObject state = (StateObject)ar.AsyncState;
                handler = state.workSocket;

                // Read data from the client socket. 
                int bytesRead = handler.EndReceive(ar);

                if (bytesRead > 0)
                {
                    // There  might be more data, so store the data received so far.
                    state.sb.Append(Encoding.ASCII.GetString(
                        state.buffer, 0, bytesRead));

                    // Check for end-of-file tag. If it is not there, read 
                    // more data.
                    content = state.sb.ToString();
                    if (content.IndexOf("<EOF>") > -1)
                    {

                        //flag 
                        currentState = TransmissionSate.Received;

                        // All the data has been read from the 
                        // client. Display it on the console.
                        TSU.Debug.WriteInConsole(string.Format(R.T514,
                            content.Length, content));

                        //reference
                        Data.lastResponse = content;

                        //react
                        TSU.Debug.WriteInConsole(R.T515);
                        //trim
                        content = content.Substring(0, content.Length - 5);
                        TSU.Debug.WriteInConsole(R.T516 + content);
                        switch (content)
                        {
                            case "DataRequest":
                                TSU.Debug.WriteInConsole(R.T517);
                                SendThroughTCPSocket(Data.xmlToSend);
                                break;

                            default:
                                SendThroughTCPSocket(R.T518 + Data.endOfFile);
                                TSU.Debug.WriteInConsole(R.T519);
                                break;
                        }
                        handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReadCallback), state);
                    }
                    else
                    {
                        // Not all data received. Get more.
                        handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReadCallback), state);
                    }
                }
            }
            catch (SocketException e)
            {
                throw new Exception(R.T520 + e.Message, e);
            }
            
        }

        //Send data to a socket
        private  void Send(Socket handler, String data)
        {
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            //flag
            currentState = TransmissionSate.Sending;

            // Begin sending the data to the remote device.
            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        //Sending data and closing handler
        private  void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket handler = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = handler.EndSend(ar);
                TSU.Debug.WriteInConsole(string.Format(R.T521, bytesSent));

                //flag
                currentState = TransmissionSate.Sent;

            }
            catch (Exception e)
            {
                TSU.Debug.WriteInConsole(e.ToString());
            }
        }


        public  void SendThroughTCPSocket(String data)
        {
            try
            {
                // Send data to the client
                Send(handler, data + Data.endOfFile);
                TSU.Debug.WriteInConsole("'" + data + "' sent");
            }
            catch (Exception e)
            {
                TSU.Debug.WriteInConsole(e.ToString());
            }

        }

        //Close Sever  
        public  void StopServer()
        {
            try
            {
                // Release the socket.
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();

                //flag
                currentState = TransmissionSate.Off;

            }
            catch (Exception e)
            {
                TSU.Debug.WriteInConsole(e.ToString());
            }
        }
    }
}
