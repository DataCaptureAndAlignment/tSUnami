﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;using R = TSU.Properties.Resources;
using System.Threading;

namespace TSU.IO.TCP
{
    
    public class AsynchronousClient
    {
        public enum TransmissionSate
        {
            Waiting,
            Receiving,
            Received,
            Sending,
            Sent,
            Off
        }

        // The port number for the remote device.
        public int port = 11000;

        // ManualResetEvent instances signal completion.
        private  ManualResetEvent connectDone =new ManualResetEvent(false);
        private  ManualResetEvent sendDone =  new ManualResetEvent(false);
        private  ManualResetEvent receiveDone = new ManualResetEvent(false);

        // The response from the remote device.
        private  String response = String.Empty;
        public  String nextMessage = String.Empty;
        public  IPHostEntry ipHostInfo;
        public  IPAddress ipAddress;
        public  IPEndPoint remoteEP;
        public  Socket client;

        //flags
        public  TransmissionSate currentState = TransmissionSate.Off;
        public  bool isConnected = false;


        public AsynchronousClient()
        {
            Thread tcpClientThread = new Thread(delegate()
            {
                //start client
                this.StartClient();

            });
            tcpClientThread.IsBackground = true;
            tcpClientThread.Start();
        }

        //Start Client and use it 
        public  void StartClient()
        {
            // Connect to a remote device.
            try
            {
                //flag 
                isConnected = false;

                // Establish the remote endpoint for the socket.
                // The name of the host
                // remote device is the current device
                //ipHostInfo = Dns.Resolve(Dns.GetHostName());
                //ipAddress = ipHostInfo.AddressList[0];
                remoteEP = new IPEndPoint(ipAddress, port);

                // Create a TCP/IP socket.
                client = new Socket(AddressFamily.InterNetwork,  SocketType.Stream, ProtocolType.Tcp);

                // Connect to the remote endpoint.
                client.BeginConnect(remoteEP,  new AsyncCallback(ConnectCallback), client);
                connectDone.WaitOne();

                //flag
                currentState = TransmissionSate.Waiting;

            }
            catch (Exception e)
            {
                TSU.Debug.WriteInConsole("TSU.TCPIP "+ e.ToString());
            }

            //switch to listening state
            //BackToSocketListening();
        }


        //Close Client 
        public  void StopClient()
        {
            try
            {
                // Release the socket.
                client.Shutdown(SocketShutdown.Both);
                client.Close();

                //flag
                currentState = TransmissionSate.Off;
                isConnected = false;

            }
            catch (Exception e)
            {
                TSU.Debug.WriteInConsole(e.ToString());
            }
        }

        //callback for connection
        private  void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket client = (Socket)ar.AsyncState;

                // Complete the connection.
                client.EndConnect(ar);

                TSU.Debug.WriteInConsole(string.Format(R.T503,   client.RemoteEndPoint.ToString()));

                // Signal that the connection has been made.
                connectDone.Set();

                //flag
                isConnected = true;
            }
            catch (Exception e)
            {
                TSU.Debug.WriteInConsole(string.Format("/!\\ {0} ", e.ToString()));
            }
        }


        //Receive data
        private  void Receive(Socket client)
        {
            try
            {
                // Create the state object.
                StateObject state = new StateObject();
                state.workSocket = client;

                //flag
                currentState = TransmissionSate.Receiving;

                // Begin receiving the data from the remote device.
                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state);
            }
            catch (Exception e)
            {
                TSU.Debug.WriteInConsole(string.Format("/!\\ {0} ", e.ToString()));
            }
        }

        //callback Receive
        private  void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the client socket 
                // from the asynchronous state object.
                StateObject state = (StateObject)ar.AsyncState;
                Socket client = state.workSocket;

                    // Read data from the remote device.
                    int bytesRead = client.EndReceive(ar);

                    if (bytesRead > 0)
                    {
                        // There might be more data, so store the data received so far.
                        state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));
                        var endPos = state.sb.ToString().IndexOf("<EOF>");
                        if (endPos > -1) //we have a complete message. huzzah!
                        {
                            response = state.sb.ToString().Substring(0, endPos);
                            // Signal that all bytes have been received.
                            receiveDone.Set();
                            //flag 
                            currentState = TransmissionSate.Received;
                            //BackToSocketListening();
                        }
                        else
                            // Get the rest of the data.
                            client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                            new AsyncCallback(ReceiveCallback), state);
                    }
            }
            catch (Exception e)
            {
                TSU.Debug.WriteInConsole(string.Format("/!\\ {0} ", e.ToString()));
            }
        }


        //Send data
        private  void Send(Socket client, String data)
        {
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            //flag
            currentState = TransmissionSate.Sending;

            // Begin sending the data to the remote device.
            TSU.Debug.WriteInConsole(string.Format(R.T504, data));
            client.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), client);
        }

        //Callback send
        private  void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket client = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = client.EndSend(ar);
                TSU.Debug.WriteInConsole(string.Format(R.T505, bytesSent));

                // Signal that all bytes have been sent.
                sendDone.Set();

                //flag
                currentState = TransmissionSate.Sent;
            }
            catch (Exception e)
            {
                TSU.Debug.WriteInConsole(string.Format("/!\\ {0} ", e.ToString()));
            }
        }


        public  string SendThroughTCPSocket(String data)
        {
            try
            {
                //response = string.Empty;
                // Send data to the remote device.
                Send(client, data + Data.endOfFile);
                sendDone.WaitOne();

                // Receive the response from the remote device.
                Receive(client);
                receiveDone.WaitOne();

                // Write the response to the console.
                TSU.Debug.WriteInConsole(string.Format(R.T506, response));

                return response;
            }
            catch (Exception e)
            {
                TSU.Debug.WriteInConsole(string.Format("/!\\ {0} ", e.ToString()));
                return "<No response>";
            }
            
        }


        public  void BackToSocketListening()
        {
            try
            {
                while (true)
                {
                    // Receive the response from the remote device.
                    Receive(client);
                    receiveDone.WaitOne();

                    // Write the response to the console.
                    TSU.Debug.WriteInConsole(string.Format(R.T507, response));

                    //react
                    
                    switch (response)
                    {
                        case "ShouldRequest":
                            TSU.Debug.WriteInConsole(R.T509);
                            //proceed to the requested action 
                            response = SendThroughTCPSocket(R.T396);
                            if (response != null)
                            {
                                //Manage the response
                                TSU.Debug.WriteInConsole(R.T509 + response);
                                Data.lastResponse = response;
                            }
                            else
                            {
                                TSU.Debug.WriteInConsole(R.T510);
                            }
                            break;

                        default:
                            TSU.Debug.WriteInConsole(R.T511);
                            break;
                    }
                }


            }
            catch (Exception e)
            {
                TSU.Debug.WriteInConsole(string.Format("/!\\ {0} ", e.ToString()));
            }

        }


    }
}
