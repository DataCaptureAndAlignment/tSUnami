﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using R = TSU.Properties.Resources;


namespace TSU.IO
{
    public static class Batch
    {
        
        public static void Create(TsuObject o, string batchPath) // polymorphisme used as Visitor pattern
        {
            dynamic d = o; //notice the 'dynamic' type. this is the key to dynamic dispatch
            VisitCore(d, batchPath);
        }

        public static void VisitCore(Polar.Measure measure, string batchPath) // polymorphisme used as Visitor pattern
        {
            string getValueInALine(Polar.Measure m)
            {
                List<object> objects = new List<object>();

                Common.Elements.Point p = m._Point;
                objects.Add(m._Point._Name);

                objects.Add(p._Coordinates.Su.X.Value);
                objects.Add(p._Coordinates.Su.Y.Value);
                objects.Add(p._Coordinates.Su.Z.Value);

                objects.Add(p._Coordinates.Su.X.Sigma);
                objects.Add(p._Coordinates.Su.Y.Sigma);
                objects.Add(p._Coordinates.Su.Z.Sigma);
                            
                objects.Add(p._Coordinates.Physicist.X.Value);
                objects.Add(p._Coordinates.Physicist.Y.Value);
                objects.Add(p._Coordinates.Physicist.Z.Value);
                            
                objects.Add(p._Coordinates.Physicist.X.Sigma);
                objects.Add(p._Coordinates.Physicist.Y.Sigma);
                objects.Add(p._Coordinates.Physicist.Z.Sigma);
                            
                objects.Add(p._Coordinates.Ccs.X.Value);
                objects.Add(p._Coordinates.Ccs.Y.Value);
                objects.Add(p._Coordinates.Ccs.Z.Value);
                            
                objects.Add(p._Coordinates.Ccs.X.Sigma);
                objects.Add(p._Coordinates.Ccs.Y.Sigma);
                objects.Add(p._Coordinates.Ccs.Z.Sigma);

                objects.Add(m.Angles.Corrected.Horizontal.Value);
                objects.Add(m.Angles.Corrected.Vertical.Value);
                objects.Add(m.Distance.Corrected.Value);

                objects.Add(m.Angles.Corrected.Horizontal.Sigma);
                objects.Add(m.Angles.Corrected.Vertical.Sigma);
                objects.Add(m.Distance.Corrected.Sigma);
                
                string s = "";
                foreach (var item in objects)
                {
                    s += $"{item.ToString()} ";
                }

                return s;
            }

            string listOfparameters = getValueInALine(measure);

            Shell.Run(batchPath, wait:false, arguments: listOfparameters);
        }
    }
}
