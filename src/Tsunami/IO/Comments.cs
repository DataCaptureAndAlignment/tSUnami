﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using R = TSU.Properties.Resources;
using E = TSU.Common.Elements;
using M = TSU.Common.Measures;
using I = TSU.Common.Instruments;


namespace TSU.IO
{
    public static class Comments
    {

        internal static void CompleteCommentFromTsunami(Polar.Measure m, Polar.Measure closure, bool modifyExtention = false)
        {
            string seperator = (m.CommentFromTsunami == "") ? "" : " - ";

            string mode = (m.ModeOfMeasure != null) ? $"{m.ModeOfMeasure}" : "";
            string precisionHA = (m.Angles != null) ? $"sH={m.Angles.Corrected.Horizontal.GetSigma("cc")} cc, " : "";
            string precisionVA = (m.Angles != null) ? $"sV={m.Angles.Corrected.Vertical.GetSigma("cc")} cc, " : "";
            string precisionD = (m.Angles != null) ? $"sD={m.Distance.Corrected.GetSigma("mm")} mm" : "";
            string precisions = precisionHA + precisionVA + precisionD;
            if (precisions != "") precisions = "=> " + precisions;

            string f;
            switch (m.Face)
            {
                case I.FaceType.UnknownFace:
                    f = "?";
                    break;
                case I.FaceType.Face1:
                    f = "F1";
                    break;
                case I.FaceType.Face2:
                    f = "F2";
                    break;
                case I.FaceType.DoubleFace:
                    f = "2F";
                    break;
                case I.FaceType.Face1Reducted:
                    f = "F1r";
                    break;
                case I.FaceType.DoubleFaceReducted:
                    f = "F2r";
                    break;
                default:
                    f = "?";
                    break;
            }

            m.CommentPrecisionFromTsunami = $"{f} / {m.NumberOfMeasureToAverage} {mode}: {precisions}";

            // new adding to show offset on the control measurement
            {
                try
                {
                    if (m._Status is M.States.Control)
                    {
                        string closureOffset = "Off.: ";
                        if (closure._Point._Name == m._Point._Name)
                        {
                            closureOffset += $"dH={Math.Round((m.Angles.Corrected.Horizontal.Value - closure.Angles.Corrected.Horizontal.Value) * 10000 * 10) / 10}cc, ";
                            closureOffset += $"dV={Math.Round((m.Angles.Corrected.Vertical.Value - closure.Angles.Corrected.Vertical.Value) * 10000 * 10) / 10}cc, ";
                            closureOffset += $"dD={Math.Round((m.Distance.Corrected.Value - closure.Distance.Corrected.Value) * 1000 * 100) / 100}mm";

                            m.CommentPrecisionFromTsunami += "," + seperator + closureOffset;
                        }
                    }
                }
                catch
                {
                    // do nothing it is just a plus if it works
                }
            }
        }
    }
}
