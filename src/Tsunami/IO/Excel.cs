﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

using R = TSU.Properties.Resources;


namespace TSU.IO
{
    public static class Excel
    {


        public static void ToExcel(TsuObject o) // polymorphisme used asVisitor pattern
        {
            //Microsoft.Office.Interop.Excel.Application oXL;
            //Microsoft.Office.Interop.Excel._Workbook oWB;
            //if (oXL == null ) oXL = new Microsoft.Office.Interop.Excel.Application();
            //if (oWB == null) oWB = oXL.Workbooks.Open("c:\\DATA\\test505.xls");
            //Microsoft.Office.Interop.Excel._Worksheet oSheet;
            //Microsoft.Office.Interop.Excel.Range oRng;
            //object misvalue = System.Reflection.Missing.Value;


            //    oXL.Visible = true;

            ////Get a new workbook.

            //oSheet = (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet;

            //bool found = false;
            //int i = 0;
            //while (!found)
            //{
            //    i++;
            //    if (oSheet.Cells[i, 1].Value == null) break;
            //    string s = oSheet.Cells[i, 1].Value.ToString();
            //    if (s=="") found = true;

            //    if (i > 1000) break;
            //}
            ////Add table headers going cell by cell.
            //oSheet.Cells[i, 1] = "First Name"; oSheet.Cells[i, 1].Font.Bold = true;
            //oSheet.Cells[i, 2] = "Last Name";
            //oSheet.Cells[i, 3] = "Full Name";
            //oSheet.Cells[i, 4] = "Salary";
            ////oSheet.get_Range(oSheet.Cells[i, 1], oSheet.Cells[i, 4]).Font.Bold = true;
        }

        /// <summary>
        /// return true with excel et workbook ref if the workbook is open and existing in the given path
        /// </summary>
        /// <param name="excel"></param>
        /// <param name="workbook"></param>
        /// <param name="pathWhereTheWorkBookShouldExit"></param>
        /// <returns></returns>
        public static bool IsAnWorkBookOpen(out Microsoft.Office.Interop.Excel.Application excel,
            out Microsoft.Office.Interop.Excel.Workbook workbook, string pathWhereTheWorkBookShouldExit = "")
        {
            excel = null;
            workbook = null;
            try
            {


                var app = System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application");
                if (!(app is Microsoft.Office.Interop.Excel.Application))
                {
                    Logs.Log.AddEntryAsFYI(Tsunami2.Properties, "Excel is not open");
                    return false;
                }

                excel = app as Microsoft.Office.Interop.Excel.Application;

                var wb = (app as Microsoft.Office.Interop.Excel.Application).ActiveWorkbook;
                int count = (app as Microsoft.Office.Interop.Excel.Application).Workbooks.Count;
                if (!(wb is Microsoft.Office.Interop.Excel.Workbook))
                {
                    Logs.Log.AddEntryAsFYI(Tsunami2.Properties, "Excel open with no workbook");
                    return false;
                }

                // workbook = wb as Microsoft.Office.Interop.Excel.Workbook;

                if (pathWhereTheWorkBookShouldExit != "")
                {
                    string directoryPath = new System.IO.FileInfo(pathWhereTheWorkBookShouldExit).DirectoryName;
                    if (workbook.Path != directoryPath)
                    {
                        Logs.Log.AddEntryAsFYI(Tsunami2.Properties, $"'{workbook.Path}'  is different from '{directoryPath}'");
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Logs.Log.AddEntryAsFYI(Tsunami2.Properties, $"Tsunami was not able to check if a excel workbook was open, {ex.Message}");
                return false;
            }
        }

        public static void WriteInWorkBook(List<object> values)
        {
            var daypath =TSU.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay;
            try
            {
                excel.Application.ScreenUpdating = false;
                var oSheet = workbook.ActiveSheet;
                int numberOfCellToWrite = values.Count;
                if (FindNextEmptyCells(excel, workbook, numberOfCellToWrite, out int row, out int column))
                {
                    // Get dimensions of the 2-d array
                    int rowCount = 1;
                    int columnCount = numberOfCellToWrite;
                    // Get an Excel Range of the same dimensions
                    Microsoft.Office.Interop.Excel.Range range = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[row, column];
                    range = range.get_Resize(rowCount, columnCount);

                    // create an array
                    object[,] array2 = new object[1, numberOfCellToWrite];
                    for (int i = 0; i < numberOfCellToWrite; i++)
                    {
                        array2[0, i] = values[i];
                    }

                    // Assign the 2-d array to the Excel Range
                    range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, array2);
                }

            }
            catch (Exception ex)
            {
                // Excel is not running.
            }
            finally
            {
                excel.Application.ScreenUpdating = true;
                if (excel != null)
                    excel.Application.ScreenUpdating = true;
            }

        }

        static bool FindNextEmptyCells(
            Microsoft.Office.Interop.Excel.Application excel,
            Microsoft.Office.Interop.Excel.Workbook wb,
            int numberOfCellToWrite,
            out int row, out int column)
        {
            try
            {
                bool ok = false;
                var oSheet = wb.ActiveSheet;

                int startingRow = excel.ActiveCell.Row;
                int startingColumn = excel.ActiveCell.Column;
                int rowOffset = -1;
                while (!ok)
                {

                    rowOffset++;
                    ok = true;
                    for (int i = 0; i < numberOfCellToWrite; i++)
                    {
                        if (oSheet.Cells[startingRow + rowOffset, startingColumn + i].Value != null)
                        {
                            ok = false;
                            break;
                        }
                    }
                    if (rowOffset > 100) throw new Exception("Looked for empty space in 100 rows without success...");
                }
                row = startingRow + rowOffset;
                column = startingColumn;
                excel.ActiveSheet.Cells(row, column).Select();
                return true;
            }

            catch (Exception ex)
            {
                throw;
            }
        }

        internal static void PossiblyExport(string pathWithOpenExcelWB, TSU.Polar.Measure n)
        {
            if (TSU.IO.Excel.IsAnWorkBookOpen(out var excel, out var wooknook, pathWithOpenExcelWB))
            {
                List<object> objects = new List<object>() { };

                Common.Elements.Point p = n._Point;

                objects.Add(p._Name);

                Common.Elements.CoordinatesInAllSystems acs = p._Coordinates;
                if (p._Coordinates.HasSu)
                {
                    objects.Add(acs.Su.X.Value);
                    objects.Add(acs.Su.Y.Value);
                    objects.Add(acs.Su.Z.Value);
                    objects.Add(acs.Su.X.Sigma);
                    objects.Add(acs.Su.Y.Sigma);
                    objects.Add(acs.Su.Z.Sigma);
                }
                if (n._Point._Coordinates.HasPhysicist)
                {
                    objects.Add(acs.Physicist.X.Value);
                    objects.Add(acs.Physicist.Y.Value);
                    objects.Add(acs.Physicist.Z.Value);
                    objects.Add(acs.Physicist.X.Sigma);
                    objects.Add(acs.Physicist.Y.Sigma);
                    objects.Add(acs.Physicist.Z.Sigma);
                }
                if (n._Point._Coordinates.HasCcs)
                {
                    objects.Add(acs.Ccs.X.Value);
                    objects.Add(acs.Ccs.Y.Value);
                    objects.Add(acs.Ccs.Z.Value);
                    objects.Add(acs.Ccs.X.Sigma);
                    objects.Add(acs.Ccs.Y.Sigma);
                    objects.Add(acs.Ccs.Z.Sigma);
                }
                objects.Add(n.Angles.Corrected.Horizontal.Value);
                objects.Add(n.Angles.Corrected.Vertical.Value);
                objects.Add(n.Distance.Corrected.Value);
                objects.Add(n.Angles.Corrected.Horizontal.Sigma);
                objects.Add(n.Angles.Corrected.Vertical.Sigma);
                objects.Add(n.Distance.Corrected.Sigma);


               TSU.IO.Excel.WriteInWorkBook(objects);
            }
        }

        internal static List<object> CreateObjectsFrom(Polar.Measure m)
        {
            List<object> objects = new List<object>() { };

            objects.Add(m._Point._Name);
            if (m._Point._Coordinates.HasSu)
            {
                objects.Add(m._Point._Coordinates.Su.X.Value);
                objects.Add(m._Point._Coordinates.Su.Y.Value);
                objects.Add(m._Point._Coordinates.Su.Z.Value);
                objects.Add(m._Point._Coordinates.Su.X.Sigma);
                objects.Add(m._Point._Coordinates.Su.Y.Sigma);
                objects.Add(m._Point._Coordinates.Su.Z.Sigma);
            }
            else
            {
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
            }

            if (m._Point._Coordinates.HasPhysicist)
            {
                objects.Add(m._Point._Coordinates.Physicist.X.Value);
                objects.Add(m._Point._Coordinates.Physicist.Y.Value);
                objects.Add(m._Point._Coordinates.Physicist.Z.Value);
                objects.Add(m._Point._Coordinates.Physicist.X.Sigma);
                objects.Add(m._Point._Coordinates.Physicist.Y.Sigma);
                objects.Add(m._Point._Coordinates.Physicist.Z.Sigma);
            }
            else
            {
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
            }

            if (m._Point._Coordinates.HasCcs)
            {
                objects.Add(m._Point._Coordinates.Ccs.X.Value);
                objects.Add(m._Point._Coordinates.Ccs.Y.Value);
                objects.Add(m._Point._Coordinates.Ccs.Z.Value);
                objects.Add(m._Point._Coordinates.Ccs.X.Sigma);
                objects.Add(m._Point._Coordinates.Ccs.Y.Sigma);
                objects.Add(m._Point._Coordinates.Ccs.Z.Sigma);
            }
            else
            {
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
            }

            if (m._Point._Coordinates.HasBeam)
            {
                objects.Add(m._Point._Coordinates.Beam.X.Value);
                objects.Add(m._Point._Coordinates.Beam.Y.Value);
                objects.Add(m._Point._Coordinates.Beam.Z.Value);
                objects.Add(m._Point._Coordinates.Beam.X.Sigma);
                objects.Add(m._Point._Coordinates.Beam.Y.Sigma);
                objects.Add(m._Point._Coordinates.Beam.Z.Sigma);
            }
            else
            {
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
            }
            if (m._Point._Coordinates.HasBeamV)
            {
                objects.Add(m._Point._Coordinates.BeamV.X.Value);
                objects.Add(m._Point._Coordinates.BeamV.Y.Value);
                objects.Add(m._Point._Coordinates.BeamV.Z.Value);
                objects.Add(m._Point._Coordinates.BeamV.X.Sigma);
                objects.Add(m._Point._Coordinates.BeamV.Y.Sigma);
                objects.Add(m._Point._Coordinates.BeamV.Z.Sigma);
            }
            else
            {
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
            }
            if (m._Point._Coordinates.HasStationCs)
            {
                objects.Add(m._Point._Coordinates.StationCs.X.Value);
                objects.Add(m._Point._Coordinates.StationCs.Y.Value);
                objects.Add(m._Point._Coordinates.StationCs.Z.Value);
                objects.Add(m._Point._Coordinates.StationCs.X.Sigma);
                objects.Add(m._Point._Coordinates.StationCs.Y.Sigma);
                objects.Add(m._Point._Coordinates.StationCs.Z.Sigma);
            }
            else
            {
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
                objects.Add("-");
            }

            objects.Add(m.Angles.Corrected.Horizontal.Value);
            objects.Add(m.Angles.Corrected.Vertical.Value);
            objects.Add(m.Distance.Corrected.Value);
            objects.Add(m.Angles.Corrected.Horizontal.Sigma);
            objects.Add(m.Angles.Corrected.Vertical.Sigma);
            objects.Add(m.Distance.Corrected.Sigma);

            return objects;
        }

        static Microsoft.Office.Interop.Excel.Application excel = null;
        static Microsoft.Office.Interop.Excel.Workbook workbook = null;

        internal static string FindOpenWorkbook()
        {
            try
            {
                var app = System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application");
                if (!(app is Microsoft.Office.Interop.Excel.Application))
                {
                    Logs.Log.AddEntryAsFYI(Tsunami2.Properties, "Excel is not open");
                    return "";
                }

                excel = app as Microsoft.Office.Interop.Excel.Application;

                var wb = (app as Microsoft.Office.Interop.Excel.Application).ActiveWorkbook;
                int count = (app as Microsoft.Office.Interop.Excel.Application).Workbooks.Count;
                if (!(wb is Microsoft.Office.Interop.Excel.Workbook))
                {
                    Logs.Log.AddEntryAsFYI(Tsunami2.Properties, "Excel open with no workbook");
                    return "";
                }

                workbook = wb as Microsoft.Office.Interop.Excel.Workbook;

                return workbook.FullName;
            }
            catch (Exception ex)
            {
                Logs.Log.AddEntryAsFYI(Tsunami2.Properties, $"Tsunami was not able to check if a excel workbook was open, {ex.Message}");
                return "";
            }
        }
    }
}
