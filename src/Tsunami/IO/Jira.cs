﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using System.Text;
using System.Threading.Tasks;
using R = TSU.Properties.Resources;
using T = TSU.Tools;
using TSU.Common;
using TSU.Views;
using TSU.Views.Message;
using Ionic.Zip;
using Ionic.Zlib;
using System.IO;

namespace TSU.IO
{
    public static class Jira
    {
        public static void AskImprovement()
        {
            string folderPath = PrepareInformation("Improvement");
            if (folderPath == R.T_CANCEL)
                return;

            if (!ProposeToOpenJira(folderPath)) return;

            System.Diagnostics.Process.Start(folderPath);

            string jira = "https://its.cern.ch/jira/secure/CreateIssueDetails!init.jspa?";

            string projectId = "pid=19446";
            List<string> arguments = new List<string>();
            arguments.Add("issuetype=4");
            arguments.Add($"description=In%20Tsunami%20{Tsunami2.Properties.Version},%20I%20Want...");

            string link = jira + projectId;
            foreach (var arg in arguments)
            {
                link += "&" + arg;
            }

            System.Diagnostics.Process.Start(link);
        }

        public static void ReportBug(string description)
        {
            string folderPath = PrepareInformation("BUG");
            if (folderPath == R.T_CANCEL)
                return;

            if (!ProposeToOpenJira(folderPath)) return;

            System.Diagnostics.Process.Start(folderPath);

            string jira = "https://its.cern.ch/jira/secure/CreateIssueDetails!init.jspa?";

            string projectId = "pid=19446";
            List<string> arguments = new List<string>();
            arguments.Add("issuetype=1");

            arguments.Add($"description={description.Replace(" ", "%20")}");

            string link = jira + projectId;
            foreach (var arg in arguments)
            {
                link += "&" + arg;
            }

            System.Diagnostics.Process.Start(link);
        }

        private static bool ProposeToOpenJira(string folderPath)
        {
            string titleAndMessage = "Thank you;" +
                                     $"A JIRA folder with helping information has been created: {MessageTsu.GetLink(folderPath)}";
            MessageInput mi = new MessageInput(MessageType.Choice, titleAndMessage)
            {
                ButtonTexts = new List<string> { R.T_OK, "Create ticket now" }
            };
            string respond = mi.Show().TextOfButtonClicked;
            return (respond != R.T_OK);
        }

        public static void OpenWebPage()
        {
            System.Diagnostics.Process.Start("https://its.cern.ch/jira/projects/TSU/issues/TSU-3?filter=allopenissues");
        }

        /// <summary>
        /// Create a folder name with time, containing, printscreen, tsu, log, and message from the user, and return the folder path
        /// </summary>
        public static string PrepareInformation(string type)
        {
            string folder = "";
            try
            {
                Tsunami tsunami = Tsunami2.Properties;
                string pathOfThday = Tsunami2.Properties.PathOfTheSavedFile;

                List<string> buttonTexts = new List<string> { R.T_OK, R.T_CANCEL };
                string titleAndMessage1 = "Let's report the problem;Please enter a short description of the problem.\r\n" +
                    "Tsunami will create a folder containing the *.tsu file, a printscreen and the log file that you can join when you create the Jira ticket.\r\n" +
                    "The log file will contains the error messages that you have received.\r\n";

                MessageTsu.ShowMessageWithTextBox(titleAndMessage1, buttonTexts, out string buttonClicked, out string textInput, "My problem is...", true);

                if (buttonClicked == R.T_CANCEL)
                    return R.T_CANCEL;

                string sourceDirectory = "";
                string destination = "";
                folder = Prepare_CreateFolder(type, textInput);
                string message = Prepare_CreateInfoFile(tsunami, folder, textInput);
                Prepare_CopyTsuFile(pathOfThday, ref sourceDirectory, ref destination, folder);
                Prepare_ScreenshotsAndVideo(folder);
                Prepare_CopyLog(ref sourceDirectory, ref destination, folder);
                Prepare_ZipCompute(ref sourceDirectory, ref destination, folder);
                Prepare_CopyBackups(ref sourceDirectory, ref destination, folder);
                sourceDirectory = Prepare_ZipBackup(destination);

                return folder;
            }
            catch (Exception ex)
            {
                string titleAndMessage = $"Cannot prepare information for Jira Ticket;{ex.Message}";
                new MessageInput(MessageType.Warning, titleAndMessage).Show();
                return R.T_CANCEL;
            }
        }

        private static void Prepare_ScreenshotsAndVideo(string folder)
        {
            string path = folder + "\\" + T.Conversions.Date.ToDateUpSideDown(DateTime.Now);
            try
            {
                // Make print screen with message
                using (Bitmap bitmap = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height))
                {
                    Graphics graphics = Graphics.FromImage(bitmap as Image);
                    graphics.CopyFromScreen(0, 0, 0, 0, bitmap.Size);
                    bitmap.Save(path + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Problem during making screenshots", ex);
            }

            try
            {
                // video
                Debug.Screenshots.CreateOrNotAVideoInAThread(out _, path + ".mp4", clean: false);
            }
            catch (Exception ex)
            {
                throw new Exception("Problem during Video creation", ex);
            }
        }

        private static void Prepare_CopyTsuFile(string pathOfThday, ref string sourceDirectory, ref string destination, string folder)
        {
            try
            {
                //copy tsu file
                if (pathOfThday != "")
                {
                    sourceDirectory = pathOfThday;
                    if (new FileInfo(sourceDirectory).Extension.ToUpper() != ".TSU")
                        sourceDirectory += ".tsu";
                    destination = folder + "\\" + new FileInfo(sourceDirectory).Name;
                    if (File.Exists(sourceDirectory))
                        File.Copy(sourceDirectory, destination);

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot copy the tsu file", ex);
            }
        }

        private static string Prepare_CreateInfoFile(Tsunami tsunami, string folder, string textInput)
        {
            try
            {
                //create text file
                string message = "";
                message += "Version: " + tsunami.Version + "\r\n";
                message += "Opened Modules: \r\n";
                foreach (var item in tsunami.MeasurementModules)
                {
                    message += item._Name + "\r\n";
                }
                message += "Message from user: \r\n";
                message += textInput;

                File.AppendAllText(folder + "\\" + "info.txt", message);

                return message;
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot create the text information file", ex);
            }
        }

        private static string Prepare_CreateFolder(string type, string textInput)
        {
            try
            {
                // create a folder
                int length = Math.Min(textInput.Length, 10);
                string textInput10 = textInput.Substring(0, length);
                string folder = Tsunami2.Preferences.Values.Paths.MeasureOfTheDay + "JIRA\\" + T.Conversions.Date.ToDateUpSideDown(DateTime.Now) + "_" + type + "_" + textInput10;
                Directory.CreateDirectory(folder);
                return folder;
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot create Jira folder locally", ex);
            }
        }

        private static string Prepare_ZipBackup(string destination)
        {
            try
            {
                // zip the backups
                string sourceDirectory = destination;
                if (Directory.Exists(sourceDirectory))
                {
                    ZipFile zip;
                    zip = new ZipFile();
                    zip.Name = "backups";

                    using (zip)
                    {
                        zip.CompressionLevel = CompressionLevel.BestSpeed;
                        zip.IgnoreDuplicateFiles = true;
                        zip.AddDirectory(sourceDirectory);
                        zip.Save(destination + ".zip");
                    }
                    Console.WriteLine("Folder successfully compressed to " + sourceDirectory);
                }
                if (Directory.Exists(destination))
                    Directory.Delete(destination, true);
                return sourceDirectory;
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot zip the backup folder", ex);
            }
        }

        private static void Prepare_CopyBackups(ref string sourceDirectory, ref string destination, string folder)
        {
            try
            {
                // copy backup files
                DirectoryInfo info = new DirectoryInfo(Tsunami2.Preferences.Values.Paths.Backups);
                FileInfo[] files = info.GetFiles("*", SearchOption.AllDirectories).OrderBy(p => p.CreationTime).Reverse().ToArray();
                int number_of_file_to_export = 20;
                int count = 0;
                int count_of_backup = 0;
                foreach (FileInfo file in files)
                {
                    if (count > number_of_file_to_export)
                        break;
                    sourceDirectory = file.FullName;
                    string originFolder = file.DirectoryName;
                    int l_tempo = Tsunami2.Preferences.Values.Paths.Temporary.Length;
                    if (originFolder.Length < l_tempo)
                        continue;

                    string sub_folder = originFolder.Substring(l_tempo);

                    if (sub_folder == "Backup")
                        count_of_backup++;


                    if (sub_folder != "Backup" || count_of_backup < 3)
                    {
                        destination = folder + "\\" + sub_folder;
                        if (!Directory.Exists(destination))
                            Directory.CreateDirectory(destination);

                        File.Copy(file.FullName, destination + "\\" + file.Name);

                        count++;
                    }
                    else
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot create copy the backups", ex);
            }
        }

        private static void Prepare_ZipCompute(ref string sourceDirectory, ref string destination, string folder)
        {
            try
            {
                // zip compiute
                sourceDirectory = Tsunami2.Preferences.Values.Paths.MeasureOfTheDay + "Computes\\";

                destination = folder + "\\" + new DirectoryInfo(sourceDirectory).Name;
                if (Directory.Exists(sourceDirectory))
                {
                    using (var zip = new ZipFile())
                    {
                        zip.Name = "computes";
                        zip.CompressionLevel = CompressionLevel.BestSpeed;
                        zip.IgnoreDuplicateFiles = true;
                        zip.AddDirectory(sourceDirectory);
                        zip.Save(destination + ".zip");
                    }
                    Console.WriteLine("Folder successfully compressed to " + sourceDirectory);
                }
            }
            catch (Exception ex)
            {
                string titleAndMessage = $"Cannot copy computation files;{ex.Message}";
                new MessageInput(MessageType.Warning, titleAndMessage).Show();
            }
        }

        private static void Prepare_CopyLog(ref string sourceDirectory, ref string destination, string folder)
        {
            try
            {
                // copy log
                sourceDirectory = Tsunami2.Preferences.Values.Paths.MeasureOfTheDay + "Logs\\";
                if (Directory.Exists(sourceDirectory))
                {
                    destination = folder + "\\" + new DirectoryInfo(sourceDirectory).Name;
                    Copy(sourceDirectory, destination);
                }
            }
            catch (Exception ex)
            {
                string titleAndMessage = $"Cannot copy log files;{ex.Message}";
                new MessageInput(MessageType.Warning, titleAndMessage).Show();
            }
        }

        public static void Copy(string sourceDirectory, string targetDirectory)
        {
            var diSource = new DirectoryInfo(sourceDirectory);
            var diTarget = new DirectoryInfo(targetDirectory);

            CopyAll(diSource, diTarget);
        }

        public static void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            Directory.CreateDirectory(target.FullName);

            // Copy each file into the new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                Debug.WriteInConsole(string.Format(@"Copying {0}\{1}", target.FullName, fi.Name));
                fi.CopyTo(System.IO.Path.Combine(target.FullName, fi.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir);
            }
        }
    }
}
