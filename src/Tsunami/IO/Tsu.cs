﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using TSU.Views.Message;
using M = TSU;
using P = TSU.Preferences;
using R = TSU.Properties.Resources;

namespace TSU.IO
{
    public static class Tsu
    {
        static bool SavingInProgress = false;
        static bool SavingTriggered = false;
        static bool SavingNeeded = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="o"></param>
        /// <param name="filePath"></param>
        public static void Create(object o, string filePath, string triggerMessage = "")
        {
            try
            {
                if (SavingInProgress)
                {
                    Debug.WriteInConsole("Saving Requested during previous one writing");
                    SavingNeeded = true;
                }
                else
                {
                    if (!SavingTriggered)
                    {
                        Debug.WriteInConsole("Saving triggered");
                        SavingTriggered = true;
                        Action a = () =>
                        {
                            Timer t = new Timer() { Enabled = true, Interval = 1000 };
                            t.Tick += delegate
                            {
                                if (!SavingInProgress)
                                {
                                    SavingNeeded = false;
                                    ToXmlToTsuAndBackupAsync(o, filePath, triggerMessage);
                                    SavingTriggered = false;

                                    if (!SavingNeeded)
                                    {
                                        t.Stop();
                                        t.Dispose();
                                    }
                                }
                            };
                            t.Start();
                        };
                        Tsunami2.Properties.InvokeOnApplicationDispatcher(a);
                    }
                    else
                    {
                        Debug.WriteInConsole("Saving skipped cause already triggereg");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(R.T011, filePath, ex.Message), ex);
            }
        }

        public static void ToXmlToTsuAndBackupSync(object o, string filePath, string triggerMessage = "")
        {
            SavingInProgress = true;
            string tsuPath = filePath;
            bool isTsu = System.IO.Path.GetExtension(tsuPath).ToUpper() == ".TSU";
            bool isTsut = System.IO.Path.GetExtension(tsuPath).ToUpper() == ".TSUT";
            bool isXml = System.IO.Path.GetExtension(tsuPath).ToUpper() == ".XML";
            if (!isTsu && !isTsut && !isXml)
                tsuPath = tsuPath + ".TSU";

            string xmlPath = System.IO.Path.ChangeExtension(tsuPath, "xml");

            var fileInfo = new FileInfo(xmlPath);
            if (!System.IO.File.Exists(fileInfo.DirectoryName))
                System.IO.Directory.CreateDirectory(fileInfo.DirectoryName);

            // make backup if file exist
            if (System.IO.File.Exists(tsuPath))
                Backup.Create(tsuPath, triggerMessage);


            //write xml
            try
            {
                Logs.Log.AddEntryAsBeginningOf(null, R.T_WRITING_TSUNAMI_STATE_TO_AN_XML_FILE);
                Xml.CreateFile(o, xmlPath);
                Logs.Log.AddEntryAsFinishOf(null, "XML written");
            }
            catch (Exception ex)
            {
                string message = $"Writing xml for {tsuPath} failed;{ex.ToString()}";
                Logs.Log.AddEntryAsError(null, message);
                new MessageInput(MessageType.Critical, message).Show();
            }

            // set curretn dir to the one where si the file to avoid to have the path compressed in the zip file
            string CuDi = System.IO.Directory.GetCurrentDirectory();
            System.IO.Directory.SetCurrentDirectory(System.IO.Path.GetDirectoryName(tsuPath));

            if (!isXml)
            {
                try
                {
                    //compress and encrypt to TSU
                    Logs.Log.AddEntryAsBeginningOf(null, "Compressing xml to TSU");
                    using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile())
                    {
                        //zip.Password = "tsunami";
                        zip.CompressionLevel = Ionic.Zlib.CompressionLevel.BestSpeed;
                        zip.AddFile(System.IO.Path.GetFileName(xmlPath));
                        zip.Save(tsuPath);
                    }
                    Logs.Log.AddEntryAsFinishOf(null, "Compressing done.");
                }
                catch (Exception ex)
                {
                    string message = $"Compression of {tsuPath} failed;{ex.ToString()}";
                    Logs.Log.AddEntryAsError(null, message);
                    new MessageInput(MessageType.Critical, message).Show();
                }
            }

            // delete xml 
            if (!TSU.Debug.Debugging && !isXml)
                System.IO.File.Delete(xmlPath);

            // put directory back
            System.IO.Directory.SetCurrentDirectory(CuDi);

            M.Tsunami2.Preferences.Values.GuiPrefs.AddToRecentPaths(tsuPath);
            SavingInProgress = false;
        }
        private static void ToXmlToTsuAndBackupAsync(object o, string filePath, string triggerMessage = "")
        {
            var worker = new System.ComponentModel.BackgroundWorker();
            worker.DoWork += delegate { ToXmlToTsuAndBackupSync(o, filePath, triggerMessage); };
            worker.RunWorkerCompleted += delegate
            { 
                (o as Common.Tsunami).DeclareSavingFinished();
            };
            worker.RunWorkerAsync();
        }

        public static object OpenFile(string filePath)
        {
            try
            {
                string xmlFilePath;
                string extension = System.IO.Path.GetExtension(filePath).ToUpper();
                if (extension == ".TSU")
                    xmlFilePath = ExtractXmlFromTsu(filePath);
                else if (extension == ".XML")
                    xmlFilePath = filePath;
                else
                    throw new Exception("Extension must be TSU");

                if (xmlFilePath == "") return null;

                // Analyse what module
                Type type = Xml.CheckType(xmlFilePath);

                object obj = Xml.DeserializeFile(type, xmlFilePath);

                if (obj is TSU.Module tsuMod)
                {
                    tsuMod.PathOfTheSavedFile = filePath;
                }

                return obj;
            }
            catch (Exception ex)
            {
                new MessageInput(MessageType.Critical, string.Format(R.T037, ex.Message)).Show();
                return null;
            }
        }

        public static string ExtractXmlFromTsu(string TsuPath)
        {

            try
            {
                return ExtractWithPassword("", TsuPath);
            }
            catch (Exception)
            {
                return ExtractWithPassword("tsunami", TsuPath);
            }
        }

        private static string ExtractWithPassword(string password, string TsuPath)
        {
            string xmlPath = "";
            string pathTempXml = P.Preferences.Instance.Paths.Temporary;
            // extract and decrypt xml from TSU
            using (Ionic.Zip.ZipFile zip1 = Ionic.Zip.ZipFile.Read(TsuPath))
            {
                zip1.Password = password;
                // here, we extract every entry, but we could extract conditionally
                // based on entry name, size, date, checkbox status, etc.  
                string path = System.IO.Path.GetDirectoryName(TsuPath);
                foreach (Ionic.Zip.ZipEntry e in zip1)
                {
                    e.Extract(pathTempXml, Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
                    if (System.IO.Path.GetExtension(e.FileName).ToUpper() == ".XML")
                    {
                        xmlPath = System.IO.Path.ChangeExtension(TsuPath, "xml");
                        if (File.Exists(xmlPath)) File.Delete(xmlPath);
                        System.IO.File.Move(pathTempXml + "//" + e.FileName, xmlPath);
                    }
                }
            }
            return xmlPath;
        }
    }
}
