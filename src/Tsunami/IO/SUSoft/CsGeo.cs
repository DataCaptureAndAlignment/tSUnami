﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using TSU.Common.Zones;
using P = TSU.Preferences;
using T = TSU.Tools;
using D = TSU.Common.Dependencies;
using TSU.Common;
using TSU.Common.Compute.Transformation;

namespace TSU.IO.SUSoft
{
    public static class CsGeo
    {
        #region enums
        public enum RunningOptions
        {
            GUI,
            CLI_Visible,
            CLI_Invisible
        }
        public enum AngleUnitsSystems
        {
            Radians = 0,
            Gon = 1,
            _100MicroGon = 2,
            DMS = 3,
            CC = 4,
        }

        #endregion

        private const double na = TSU.Preferences.Preferences.NotAvailableValueNa;

        #region Static classes available from users

        #region usefull

        //public static void XYH2MLAorOpposite(TheoreticalElement n)
        //{
        //    if (n.Definition.LocalZone!=null)
        //    Any((List<Point>)n.Elements.Cast<Point>().ToList(), n.Definition.InitialReferenceFrames, Coordinates.GetRelativeSystemMlaAndXyh(n.Definition.InitialReferenceFrames), n.Definition.LocalZone);
        //}

        //public static void XYH2MLAorOpposite(ref List<Point> points, Coordinates.ReferenceFrames inputReferenceFrames, Zone mlaZone)
        //{
        //    Any(points, inputReferenceFrames, Coordinates.GetRelativeSystemMlaAndXyh(inputReferenceFrames), mlaZone);
        //}

        //public static void XYH2MLAorOpposite(Point point, Coordinates.ReferenceFrames inputReferenceFrames, Zone mlaZone)
        //{
        //    List<Point> points = new List<Point>();
        //    points.Add(point);
        //    Any(points, inputReferenceFrames, Coordinates.GetRelativeSystemMlaAndXyh(inputReferenceFrames), mlaZone);
        //}

        public static void GUI(List<Point> points, Coordinates.CoordinateSystemsTsunamiTypes tsunamiTypes, Action<string[]> whatToDoAfterUIClosure, Zone mlaZone = null)
        {
            Coordinates.ReferenceFrames inputReferenceFrames = Common.Analysis.ReferenceFrames.GetIfConstant(points, tsunamiTypes);
            List<Point> newPoints = points;
            if (inputReferenceFrames == Coordinates.ReferenceFrames.Unknown)
            {
                if (tsunamiTypes == Coordinates.CoordinateSystemsTsunamiTypes.CCS)
                    inputReferenceFrames = Coordinates.ReferenceFrames.CernXYHg00Machine;
                else if (tsunamiTypes == Coordinates.CoordinateSystemsTsunamiTypes.SU)
                    inputReferenceFrames = Coordinates.ReferenceFrames.MLA2000Machine;
            }
            Any(newPoints, inputReferenceFrames, Coordinates.ReferenceFrames.Unknown, whatToDoAfterUIClosure, mlaZone);
        }

        public static void Any(List<Point> points,
            Coordinates.ReferenceFrames inputReferenceFrames, Coordinates.ReferenceFrames outputReferenceFrames,
            Action<string[]> whatToDoAfterUIClosure, Zone mlaZone,
            Coordinates.CoordinateSystemsTypes inputCoordinateSystems = Coordinates.CoordinateSystemsTypes.AutomaticBasedOnReferenceFrame,
            Coordinates.CoordinateSystemsTypes outputCoordinateSystems = Coordinates.CoordinateSystemsTypes.AutomaticBasedOnReferenceFrame)
        {
            Project project;
            Add addInput, addOutput;
            Insert insertCoordinates;
            Prepare(out project, out addInput, out addOutput, out insertCoordinates, inputReferenceFrames, outputReferenceFrames);

            addInput(project, points, inputReferenceFrames, inputCoordinateSystems, mlaZone);
            // addOutput(project, null, outputReferenceFrames, outputCoordinateSystems, mlaZone); only use to send to UI for the user to do whatever he want so we dont have to pre set the output

            
            project.RunInUI(whatToDoAfterUIClosure);
        }



        #endregion

        #region Insert coordinate from csgeo to points
        private static void InsertCoordinates(TheoreticalElement n, Project project, Coordinates.CoordinateSystemsTsunamiTypes CoordinateType)
        {
            InsertCoordinates(n.Elements.Cast<Point>().ToList(), project, CoordinateType);
        }
        private static void InsertCoordinates(List<Point> points, Project project, Coordinates.CoordinateSystemsTsunamiTypes type)
        {
            int i = 0;
            if (project.outputFiles.Count < 1) throw new Exception(R.T032);
            if ((project.outputFiles[0].coordinates).Count == points.Count)
            {
                foreach (Coordinates item in project.outputFiles[0].coordinates)
                {
                    switch (type)
                    {
                        case Coordinates.CoordinateSystemsTsunamiTypes.CCS:
                            (points[i] as Point)._Coordinates.Ccs = item;
                            break;
                        case TSU.Common.Elements.Coordinates.CoordinateSystemsTsunamiTypes.PHYS:
                        case TSU.Common.Elements.Coordinates.CoordinateSystemsTsunamiTypes.SU:
                        case TSU.Common.Elements.Coordinates.CoordinateSystemsTsunamiTypes.MLA:
                            (points[i] as Point)._Coordinates.Local = item;
                            break;
                        case Coordinates.CoordinateSystemsTsunamiTypes.UserDefined:
                        default:
                            (points[i] as Point)._Coordinates.UserDefined = item;
                            break;
                    }
                    i += 1;
                }
            }
        }

        #endregion

        #region tools
        private static void Prepare(out Project project, out Add addInput, out Add addOutput, out Insert insertCoordinates, Coordinates.ReferenceFrames inputReferenceFrames, Coordinates.ReferenceFrames outputReferenceFrames)
        {
            if (outputReferenceFrames == Coordinates.ReferenceFrames.Unknown)
                project = new Project(RunningOptions.GUI);
            else
                project = new Project(RunningOptions.CLI_Visible);

            switch (Coordinates.GetCoordinateTsunamiType(inputReferenceFrames))
            {
                case Coordinates.CoordinateSystemsTsunamiTypes.CCS: addInput = new Add(AddCcsInput); break;
                case Coordinates.CoordinateSystemsTsunamiTypes.MLA:
                case Coordinates.CoordinateSystemsTsunamiTypes.SU:
                case Coordinates.CoordinateSystemsTsunamiTypes.PHYS:
                    addInput = new Add(AddLocalInput); break;
                case Coordinates.CoordinateSystemsTsunamiTypes.UserDefined: addInput = new Add(AddUserDefinedInput); break;
                case Coordinates.CoordinateSystemsTsunamiTypes.Unknown:
                default: addInput = null; break;
            }

            switch (Coordinates.GetCoordinateTsunamiType(outputReferenceFrames))
            {
                case Coordinates.CoordinateSystemsTsunamiTypes.CCS: addOutput = new Add(AddOutput); insertCoordinates = new Insert(InsertCcsCoordinates); break;
                case Coordinates.CoordinateSystemsTsunamiTypes.MLA:
                case Coordinates.CoordinateSystemsTsunamiTypes.SU:
                case Coordinates.CoordinateSystemsTsunamiTypes.PHYS:
                    addOutput = new Add(AddOutput); insertCoordinates = new Insert(InsertMlaCoordinates); break;
                case Coordinates.CoordinateSystemsTsunamiTypes.UserDefined: addOutput = new Add(AddOutput); insertCoordinates = new Insert(InsertUserDefinedCoordinates); break;
                case Coordinates.CoordinateSystemsTsunamiTypes.Unknown:
                default:
                    addOutput = null; insertCoordinates = null;
                    break;
            }
        }

        private delegate void Add(Project project, List<Point> points, Coordinates.ReferenceFrames referenceFrame, Coordinates.CoordinateSystemsTypes coordinateSystems = Coordinates.CoordinateSystemsTypes.AutomaticBasedOnReferenceFrame, Zone mlaZone = null);
        private delegate void Insert(List<Point> points, Project project, Coordinates.ReferenceFrames outputReferenceFrames);

        private static void AddCcsInput(Project project, List<Point> points, Coordinates.ReferenceFrames referenceFrame, Coordinates.CoordinateSystemsTypes coordinateSystems = Coordinates.CoordinateSystemsTypes.AutomaticBasedOnReferenceFrame, Zone mlaZone = null)
        {
            List<Coordinates> coords = new List<Coordinates>();

            foreach (Point point in points)
            {
                var coordinates = point._Coordinates.Ccs.Clone() as Coordinates;
                coordinates._Name = point._Name;
                coords.Add(coordinates);
                mlaZone = null;
            }

            project.AddInput(coords, referenceFrame, coordinateSystems, mlaZone);
        }
        private static void AddLocalInput(Project project, List<Point> points, Coordinates.ReferenceFrames referenceFrame, Coordinates.CoordinateSystemsTypes coordinateSystems = Coordinates.CoordinateSystemsTypes.AutomaticBasedOnReferenceFrame, Zone mlaZone = null)
        {
            List<Coordinates> coords = new List<Coordinates>();
            foreach (Point point in points)
                coords.Add(point._Coordinates.Mla);
            project.AddInput(coords, referenceFrame, coordinateSystems, mlaZone);
        }
        private static void AddUserDefinedInput(Project project, List<Point> points, Coordinates.ReferenceFrames referenceFrame, Coordinates.CoordinateSystemsTypes coordinateSystems = Coordinates.CoordinateSystemsTypes.AutomaticBasedOnReferenceFrame, Zone mlaZone = null)
        {
            List<Coordinates> coords = new List<Coordinates>();
            foreach (Point point in points)
                coords.Add(point._Coordinates.UserDefined);
            project.AddInput(coords, referenceFrame, coordinateSystems, mlaZone);
        }
        private static void AddOutput(Project project, List<Point> points, Coordinates.ReferenceFrames referenceFrame, Coordinates.CoordinateSystemsTypes coordinateSystems = Coordinates.CoordinateSystemsTypes.AutomaticBasedOnReferenceFrame, Zone mlaZone = null)
        {
            project.AddOutput(null, referenceFrame, coordinateSystems, mlaZone);
        }

        private static void InsertCcsCoordinates(List<Point> points, Project project, Coordinates.ReferenceFrames outputReferenceFrames)
        {
            int i = 0;
            if (project.outputFiles.Count < 1) throw new Exception(R.T032);
            if ((project.outputFiles[0].coordinates).Count == points.Count)
            {
                foreach (Coordinates item in project.outputFiles[0].coordinates)
                {
                    (points[i] as Point)._Coordinates.Ccs = item;
                    if (outputReferenceFrames != Coordinates.ReferenceFrames.CCS)
                    {
                        (points[i] as Point)._Coordinates.Ccs.Z = item.Z;
                    }
                    i += 1;
                }
            }
        }
        private static void InsertMlaCoordinates(List<Point> points, Project project, Coordinates.ReferenceFrames outputReferenceFrames)
        {
            int i = 0;
            if (project.outputFiles.Count < 1)
                throw new Exception(R.T032);
            if ((project.outputFiles[0].coordinates).Count != points.Count)
                throw new Exception("The number of points is different in the input and output, in csgeo 5.01 it happened that cegeo removes some points et rewrite the input file...");



            foreach (Coordinates item in project.outputFiles[0].coordinates)
            {
                (points[i] as Point)._Coordinates.Mla = item;
                i += 1;
            }

        }
        private static void InsertUserDefinedCoordinates(List<Point> points, Project project, Coordinates.ReferenceFrames outputReferenceFrames)
        {
            int i = 0;
            if (project.outputFiles.Count < 1) throw new Exception(R.T032);
            if ((project.outputFiles[0].coordinates).Count == points.Count)
            {
                foreach (Coordinates item in project.outputFiles[0].coordinates)
                {
                    (points[i] as Point)._Coordinates.UserDefined = item;
                    i += 1;
                }
            }
        }
        #endregion

        #endregion


        private class Project
        {
            #region  new usage of csgeo 5.00.00
            //
            // now the command line is suppressed, and allow only "csgeo.Exe <projectfile>" to open the GUI with the input describe in the project already open in the GUI
            // or "csgeo.Exe -x <projectfile>" to automaticaly compute in any output that outputfile specified in the project file. 
            // It means that the project file input file and/or not output file should be created manually and filled with the needed parameters.
            #endregion

            #region  old usage of Csgeo:
            //
            //   C:\Program Files (x86)\SuSoft\CSGeo\4.03.01\csgeoCLI.exe  [-i <path>] -s
            //                                        <number> [-c <number>]
            //                                        [--input-angle <number>] [-O
            //                                        <path>] [-F] -S <number> [-C
            //                                        <number>] [--output-angle <number>]
            //                                        [--origin <path>]
            //                                        [--output-precision <number>] [--]
            //                                        [--version] [-h]
            //Where:

            //   -i <path>,  --input-file <path>
            //     Input filename (default = stdin)

            //   -s <number>,  --input-refframe <number>
            //     (required)  Input file reference system

            //   -c <number>,  --input-coordsys <number>
            //     Input coordinate system (if not present will use the default for the
            //     reference frame)

            //   --input-angle <number>
            //     Input angle units (if not present will default to gon)

            //   -O <path>,  --output-file <path>
            //     Output filename (default = stdout)

            //   -F,  --
            //     Overwrite the output file

            //   -S <number>,  --output-refframe <number>
            //     (required)  Output file reference system

            //   -C <number>,  --output-coordsys <number>
            //     Output coordinate system (if not present will use the default for the
            //     reference frame)

            //   --output-angle <number>
            //     Output angle units (if not present will default to gon)

            //   --origin <path>
            //     Origin information file for the output reference frame

            //   --output-precision <number>
            //     How many digits after the decimal point should be PRINTED in the
            //     output

            //   --,  --ignore_rest
            //     Ignores the rest of the labeled arguments following this flag.

            //   --version
            //     Displays version information and exits.
            //   -h,  --help

            //     Displays usage information and exits.
            //   CSGEO Command Line Application (v4.03.01)
            //   Compiled against SurveyLib v3.03.03
            //   Copyright 2003-2012, CERN BE/ABP. All rights reserved.
            //   List of supported reference frames:

            //        Number                                   Name
            //            -2................LocalRefFrame (RESERVED)
            //             0..........................CCS - CERN XYZ
            //             1....................................LAp0
            //             2....................................LGp0
            //             3................................CERN GRF
            //             4...........................ITRF97 ep98.5
            //             5...................................WGS84
            //             6..................................ROMA40
            //             7..................................ETRF93
            //             8.................................CH1903+
            //           100...............................CERN XYHe
            //           101..............CERN XoYoHe (Map Transfer)
            //           102.............................CERN XYHg00
            //           103.........................CERN XYHg00Topo
            //           104........................CERN XYHg (RS2K)
            //           105............CERN XYHg1985 (Surface Topo)
            //           106.....................CERN XYHg1985 (LHC)
            //           107.........................CERN XYHs (SPS)
            //           108.........................CERN GRF sphere
            //           109.........Swiss LV95 (ellipsoidal height)
            //           110.........Swiss LV03 (ellipsoidal height)
            //           205..French RGF93zone5 (ellipsoidal height)
            //           206........................French Lambert93
            //           207...................................RGF93
            //           208.................................CHTRF95
            //          1000..............................MLA (1985)
            //          1001..............................MLA (2000)
            //          1002............................MLA (Sphere)
            //          1010...............................LA (1985)
            //          1011...............................LA (2000)
            //          1012.............................LA (Sphere)
            //          2000.....................................MLG
            //          2001............................MLG (Sphere)
            //          2010.....................................LG
            //          2011.............................LG (Sphere)

            //   List of supported coordinate systems:
            //        Number                                   Name
            //             1............................3D Cartesian
            //             2................................Geodetic
            //             4...............................2D plus H
            //             8............................2D Cartesian
            //            16.........................Geodetic Sphere

            //   List of supported angle units systems:
            //        Number                                   Name
            //             0.................................Radians
            //             1....................................gon
            //             2............................100MicroGon
            //             3.....................................DMS
            //             4......................................CC";
            #endregion


            public RunningOptions options;
            public string directoryPath;
            public string fileName;
            public bool runned;
            public PointFile inputFile;
            public List<PointFile> outputFiles;
            public string content;
            public int numberOfOutputFiles = 0;

            public Project(RunningOptions option = RunningOptions.CLI_Invisible, string fileName = "default", string pathOfDirectory = "default")
            {
                this.directoryPath = (pathOfDirectory == "default") ? P.Preferences.Instance.Paths.MeasureOfTheDay + @"Computes\CsGeo\" : pathOfDirectory;
                if (!System.IO.Directory.Exists(directoryPath.TrimEnd('\\'))) System.IO.Directory.CreateDirectory(directoryPath.TrimEnd('\\'));
                this.fileName = (fileName == "default") ? T.Conversions.Date.ToDateUpSideDown(DateTime.Now) + ".csgeo" : fileName;
                this.options = option;

                this.outputFiles = new List<PointFile>();

                this.content = "*PREC 6" + "\r\n" + "projectFileVersion	2" + "\r\n";
            }

            internal void AddInput(List<Coordinates> points, Coordinates.ReferenceFrames referenceFrame, Coordinates.CoordinateSystemsTypes coordinateSystems, Zone origin = null)
            {
                this.inputFile = new PointFile(this, points, referenceFrame, origin, T.Conversions.Date.ToDateUpSideDown(DateTime.Now) + "_input.txt", AngleUnitsSystems.Gon, 7, coordinateSystems);
                this.content += "inputFile	" + this.inputFile.fileName + "\r\n";
            }

            internal void AddOutput(List<Coordinates> points, Coordinates.ReferenceFrames referenceFrame, Coordinates.CoordinateSystemsTypes coordinateSystems = Coordinates.CoordinateSystemsTypes.AutomaticBasedOnReferenceFrame, Zone origin = null)
            {
                outputFiles.Add(new PointFile(this, points, referenceFrame, origin, T.Conversions.Date.ToDateUpSideDown(DateTime.Now) + "_output.txt"));
                foreach (PointFile item in outputFiles)
                {
                    this.content += "outputFile	" + item.fileName + "\r\n";
                    this.numberOfOutputFiles += 1;
                }
            }
            internal void CreateFile()
            {
                System.IO.File.WriteAllText(this.directoryPath + this.fileName, this.content);
            }
            public void RunInUI(Action<string[]> whatToDoAfterUIClosure)
            {
                System.Diagnostics.ProcessStartInfo cmdsi = null;
                System.Diagnostics.Process cmd = null;

                this.CreateFile();
                try
                {
                    if (!TSU.Preferences.Preferences.Dependencies.GetByBame("SURVEYPAD", out Common.Dependencies.Application app))
                        throw new Exception("SURVEYPAD for CSGEO plugin not found");

                    string process = app.Path;

                    if (!File.Exists(process))
                        throw new Exception($"{process} does not exist");

                    cmdsi = new System.Diagnostics.ProcessStartInfo(process);

                    //options
                    cmdsi.Arguments += (options != RunningOptions.GUI) ? " -x " : " ";
                    cmdsi.CreateNoWindow = (options != RunningOptions.CLI_Visible) ? true : false;

                    //path
                    cmdsi.Arguments += directoryPath + this.fileName;
                    //cmdsi.WorkingDirectory = System.IO.Path.GetDirectoryName(directoryPath); // surveypad has problem if not working in his directory

                    //run
                    cmd = System.Diagnostics.Process.Start(cmdsi);

                    TSU.Tsunami2.Preferences.Values.Processes.Add(new TSU.TsuProcess() { Application = app, Process = cmd, Input = this.fileName });

                    cmd.EnableRaisingEvents = true;
                    cmd.Exited += (o, e) =>
                    {
                        runned = true;

                        string filePath = directoryPath + this.fileName;
                        string directory = System.IO.Path.GetDirectoryName(filePath);
                        string fileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(filePath);
                        string fullPathWithoutExtension = System.IO.Path.Combine(directory, fileNameWithoutExtension);
                        string[] matchingFiles = Directory.GetFiles(directoryPath, fileNameWithoutExtension + "*");


                        whatToDoAfterUIClosure(matchingFiles);
                    };
                }
                catch (Exception ex)
                {
                    runned = false;
                    throw new Exception(R.T033, ex);
                }
            }


            //private int GetReferenceFrameNumber(Coordinates.ReferenceFrames referenceFrames)
            //{
            //    return (int)((Coordinates.ReferenceFrames)Enum.Parse(typeof(Coordinates.ReferenceFrames), referenceFrames.ToString()));
            //}
            //private int GetCoordinateSystemsNumber(Element.Coordinates.CoordinateSystems coordinateSystems)
            //{
            //    return (int)((Element.Coordinates.CoordinateSystems)Enum.Parse(typeof(Element.Coordinates.CoordinateSystems), coordinateSystems.ToString()));
            //}
            //private int GetAngleUnitsSystemsNumber(AngleUnitsSystems angleUnitsSystems)
            //{
            //    return (int)((AngleUnitsSystems)Enum.Parse(typeof(AngleUnitsSystems), angleUnitsSystems.ToString()));
            //}

            public class PointFile
            {
                //fields
                internal Project project;
                public List<Coordinates> coordinates;
                public System.IO.StreamReader file;
                internal string fileName;
                internal TSU.Common.Elements.Coordinates.ReferenceFrames referenceFrame;
                internal TSU.Common.Elements.Coordinates.CoordinateSystemsTypes coordinateSystem;
                internal AngleUnitsSystems angleUnitsSystems;
                internal Zone origin;
                internal int precision;

                //methods
                public PointFile(
                    Project project,
                    List<Coordinates> points,
                    Coordinates.ReferenceFrames referenceFrame,
                    Zone origin = null,
                    string fileName = "default",
                    AngleUnitsSystems angleUnitsSystems = AngleUnitsSystems.Gon,
                    int precision = 7,
                    TSU.Common.Elements.Coordinates.CoordinateSystemsTypes coordinateSystem = TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.AutomaticBasedOnReferenceFrame)
                {
                    //cheking1
                    if (Coordinates.IsLocalSystem(referenceFrame) && origin == null)
                        throw new Exception(R.T034);

                    //Field filling
                    this.project = project;
                    this.coordinates = (points == null) ? new List<Coordinates>() : points;
                    this.referenceFrame = referenceFrame;
                    this.angleUnitsSystems = angleUnitsSystems;
                    this.origin = origin;
                    this.precision = precision;
                    this.fileName =
                        (fileName == "default") ?
                        T.Conversions.Date.ToDateUpSideDown(DateTime.Now) + ".txt" :
                        fileName;
                    this.coordinateSystem =
                        (coordinateSystem == TSU.Common.Elements.Coordinates.CoordinateSystemsTypes.AutomaticBasedOnReferenceFrame) ?
                        TSU.Common.Elements.Coordinates.GetCoordinatesSystemsTypeBasedOn(referenceFrame) :
                        coordinateSystem;
                    this.CreateFile();
                }

                public void CreateFile()
                {
                    string fileContent = "";
                    try
                    {
                        string originLine = (this.origin != null) ? "ORIGIN " + T.Conversions.StringManipulation.ToString(this.origin) : "";
                        string referenceFrame = this.referenceFrame.ToString().Replace("_", "");
                        string coordinateSystem = this.coordinateSystem.ToString().Replace("_", "");
                        string precision = this.precision.ToString();


                        fileContent += @"#
#   Created by TSUNAMI " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + @" for CSGEO v5.00.00 compiled against SurveyLib v3.03.07 on Aug 12 2016
#   Copyright 2003-2016, CERN ACE/SU. All rights reserved.
#   " + DateTime.Now.ToString() + @"

#
#
#" + this.fileName + @"
#
#; ReferenceFrame = " + referenceFrame + @"
#; CoordinateSystem = " + coordinateSystem + @"
#; AngularUnits = NA
#; Precision = " + precision.ToString() + @"
#
#
#; <Errors> 
#; </Errors>

" + originLine + @"
#          Name                       X                        Y                        Z
#         Units                       M                        M                        M
";
                        if (coordinates != null)
                        {
                            foreach (Coordinates coordinate in coordinates)
                            {
                                //TSU-3131 ORIGIN not ALLOWED in point names for CSGEO
                                string s = T.Conversions.StringManipulation.ToString(coordinate);
                                s = s.Replace("ORIGIN", "0R1G1N");

                                fileContent += s + "\r\n";

                            }
                        }
                        System.IO.File.WriteAllText(this.project.directoryPath + this.fileName, fileContent);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"{R.T_IMPOSSIBLE_TO_CREATE_FILE} '{this.project.directoryPath}' {R.T_WITH_THIS_DATA}:{this.fileName} {fileContent}\r\n{ex.Message}", ex);
                    }
                }

                public void ReadFile()
                {
                    string path = project.directoryPath + fileName;
                    if (!System.IO.File.Exists(path)) throw new Exception(string.Format(R.T035, path));

                    foreach (var line in GetLinesContainingPoints(path))
                    {
                        //TSU-3131 ORIGIN not ALLOWED in point names for CSGEO
                        string lineMod = line.Replace("0R1G1N", "ORIGIN");

                        Coordinates c = T.Conversions.FileFormat.IdXYZLineToCoordinates(lineMod, referenceFrame, coordinateSystem);

                        this.coordinates.Add(c);
                    }
                }

                private List<string> GetLinesContainingPoints(string path)
                {
                    string line;
                    List<string> pointLines = new List<string>();

                    file = new System.IO.StreamReader(path);

                    FindAtPosition(file, " Units", 9, 6);
                    while ((line = file.ReadLine()) != null)
                        pointLines.Add(CleanFromDoubleSpace(line));
                    file.Close();
                    file.Dispose();
                    return pointLines;
                }
                private void SkipLines(int p)
                {
                    for (int i = 0; i < p; i++)
                    {
                        file.ReadLine();
                    }
                }
                private void FindAtPosition(System.IO.StreamReader file, string word, int start, int length)
                {
                    string line;
                    bool found = false;
                    while (!found)
                    {
                        line = file.ReadLine();
                        if (line.Length > start + length)
                            if (line.Substring(start, length) == word)
                                found = true;
                    }
                }
                private string CleanFromDoubleSpace(string line)
                {
                    RegexOptions options = RegexOptions.None;
                    Regex regex = new Regex("[ ]{2,}", options);
                    return regex.Replace(line, " ");
                }
            }
        }


    }
}
