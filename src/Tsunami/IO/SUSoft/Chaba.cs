﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using TSU.Common.Elements;
using P = TSU.Preferences;
using D = TSU.Common.Dependencies;
using T = TSU.Tools;
using TSU.Common;

namespace TSU.IO.SUSoft
{
    public class Chaba
    {
        /// <summary>
        /// For chaba 5.00.02 which is working in cli only
        /// </summary>
        private Output output;
        public Chaba(List<Point> pointsInOriginalFrame, List<Point> pointsInDestinationFrame, 
            out List<string> createdFileFullPaths, bool silent = false, string filesName =null, bool edit = false)
        {
            try
            {
                createdFileFullPaths = new List<string>();
                //filesName = (filesName == null)? "Chaba_"+Conversions.Date.ToDateUpSideDown(DateTime.Now): filesName;
                filesName = filesName ?? "Chaba_" + T.Conversions.Date.ToDateUpSideDown(DateTime.Now);
                //string dirName = Preferences.Instance.Paths.Temporary + @"Chaba\";
                string dirName =TSU.Tsunami2.Preferences.Values.Paths.MeasureOfTheDay;
                dirName += @"computes\CHABA\";
                if (!System.IO.Directory.Exists(dirName))
                    System.IO.Directory.CreateDirectory(dirName);

                // try to rename points for name fit
                // for the moment just remove brackets
                //Rename(pointsInOriginalFrame, pointsInDestinationFrame, out List<Point> pointsInOriginalFrameRenammed, out List<Point> pointsInDestinationFrameRenammed);
                // !! no renaming should be done before using chaba for example in TSU.Compute.Transformation.system.BasedOnCommonPoints

                Input input = new Input(pointsInOriginalFrame, pointsInDestinationFrame,
                    dirName, filesName, silent, edit: edit);
                if (input.runned)
                {
                    output = new Output(dirName, filesName, ".out");
                    createdFileFullPaths.Add(dirName + filesName + ".chaba");
                    createdFileFullPaths.Add(dirName + filesName + ".act");
                    createdFileFullPaths.Add(dirName + filesName + ".pas");
                    createdFileFullPaths.Add(dirName + filesName + "LOG.log");
                    createdFileFullPaths.Add(dirName + filesName + ".coo");
                    createdFileFullPaths.Add(dirName + filesName + ".out");
                }
                else
                    throw new Exception("Seems like chaba did not run properly");
            }
            catch (Exception ex)
            {
                throw new Exception("Could not transform with Chaba", ex);
            }
        }

        private void Rename(List<Point> pointsInOriginalFrame, List<Point> pointsInDestinationFrame, out List<Point> pointsInOriginalFrameRenammed, out List<Point> pointsInDestinationFrameRenammed)
        {
            pointsInOriginalFrameRenammed = new List<Point>();
            pointsInDestinationFrameRenammed = new List<Point>();

            foreach (var item in pointsInOriginalFrame)
            {
                Point p = item.Clone() as Point;
                int pos = p._Name.FirstOrDefault(x => x == '[');
                if (pos != 0)
                    p._Name = p._Name.Remove(pos);
                pointsInOriginalFrameRenammed.Add(p);
            }
            foreach (var item in pointsInDestinationFrame)
            {
                Point p = item.Clone() as Point;
                int pos = p._Name.FirstOrDefault(x => x == '[');
                if (pos != 0)
                    p._Name = p._Name.Remove(pos);
                pointsInDestinationFrameRenammed.Add(p);
            }
        }

        public List<Point> GetOutputPoints()
        {
            return output.results;
        }
        public class Input
        {
            public bool runned;
            public Input(List<Point> pointsInOriginalFrame, List<Point> pointsInDestinationFrame, 
                string fullDirectoryPath,  string filesName ,bool silent = false, bool edit = false)
            {
                try
                {
                    string projectFileName = filesName + ".chaba";
                    string activeFileName = CreateOrSelect(pointsInDestinationFrame, fullDirectoryPath, filesName + ".act");
                    string passiveFileName = CreateOrSelect(pointsInOriginalFrame, fullDirectoryPath, filesName + ".pas");

                    CreateProjectFile(fullDirectoryPath, filesName, activeFileName, passiveFileName);

                    if (!silent)
                        TSU.Shell.ExecutePathInDialogView(fullDirectoryPath + projectFileName, "Chaba", R.T_CANCEL, launchSurveyPadPlugin: edit);
                    else
                        Run(fullDirectoryPath + projectFileName, silent);
                    
                    runned = System.IO.File.Exists(fullDirectoryPath + filesName + ".out");
                }
                catch (Exception ex)
                {
                    runned = false;
                    throw new Exception(string.Format(R.T030,ex.Message), ex);
                }
                
            }
            private string CreateOrSelect(List<Point> pointsInOriginalFrame, string fullDirectoryPath, string fileNameWithExtension)
            {
                if (pointsInOriginalFrame != null)
                {
                    CreatePointsFile(pointsInOriginalFrame, fullDirectoryPath, fileNameWithExtension);
                }
                else
                {
                    OpenFileDialog theDialog = new OpenFileDialog();
                    theDialog.Title = R.T_OPEN_TEXT_FILE;
                    theDialog.Filter = "Passif files|*.pas | Active files|*.act";
                    theDialog.InitialDirectory =TSU.Tsunami2.Preferences.Values.Paths.Data;
                    TsunamiView.ViewsShownAsModal.Add(theDialog);  
                    if (theDialog.ShowDialog() == DialogResult.OK)
                    {
                        fileNameWithExtension = theDialog.FileName.ToString();
                    }
                    TsunamiView.ViewsShownAsModal.Remove(theDialog);
                }
                return fileNameWithExtension;
            }
            public void CreateProjectFile(string fullDirectoryPath, string projectFileName, string activeFileName, string passiveFileName)
            {
                string time = T.Conversions.Date.ToDateUpSideDown(DateTime.Now);
                string title = $"{R.T_CREATED_BY_TSUNAMI_ON} {time}";
                string projectFile = fullDirectoryPath + projectFileName + ".chaba";
                // we use 10m as tolerance so taht any pint is ever exclude by default and user have to understand what is goingon.
                string content =
@"*PREC 6
#projectFileVersion	1
title	Test
tolerance	10.0
scale	fixed 1
TrX	variable 0
TrY	variable 0
TrZ	variable 0
RotX	variable 0
RotY	variable 0
RotZ	variable 0
passiveFile	""" + passiveFileName + @"""
activeFile	""" + activeFileName  + @"""
cooFile	""" + projectFileName + ".coo" + @"""
punchFile	""" + projectFileName + ".pun" + @"""
resultFile	""" + projectFileName + ".out" + @"""";

                System.IO.Directory.CreateDirectory(fullDirectoryPath);
                System.IO.File.WriteAllText(projectFile, content);
            }

            public void CreatePointsFile(List<Point> points, string fullDirectoryPath, string fileNameWithExtension)
            {
                string content;
                content = @"*OLOC" + "\r\n";
                foreach (Point item in points)
                {
                    content +=
                        item._Name
                        + "    "
                        + item._Coordinates.Local.X.Value
                        + "    "
                        + item._Coordinates.Local.Y.Value
                        + "    "
                        + item._Coordinates.Local.Z.Value
                        + "\r\n";
                }

                System.IO.File.WriteAllText(fullDirectoryPath + fileNameWithExtension, content);
            }
            public void Run(string ProjectFilePath, bool silent = false)
            {

                const int MAX_RUNNING_TIME = 20000;
                string process = "";
                try
                {
                    if (!TSU.Preferences.Preferences.Dependencies.GetByBame("CHABA", out Common.Dependencies.Application app))
                        throw new Exception("CHABA not found");
                    process = app.Path; 
                
                    System.Diagnostics.ProcessStartInfo cmdsi = new System.Diagnostics.ProcessStartInfo(process);
                    cmdsi.CreateNoWindow = false;
                    //if (silent) cmdsi.Arguments += " -X ";
                    cmdsi.Arguments += "\""+ ProjectFilePath +"\"";
                    cmdsi.WorkingDirectory = System.IO.Path.GetDirectoryName(ProjectFilePath);
                    System.Diagnostics.Process cmd = System.Diagnostics.Process.Start(cmdsi);

                    TSU.Tsunami2.Preferences.Values.Processes.Add(new TSU.TsuProcess() { Application = app, Process = cmd, Input = ProjectFilePath });

                    if (!cmd.WaitForExit(MAX_RUNNING_TIME))
                    {
                        if (!cmd.HasExited)
                        {
                            cmd.Kill();
                            cmd.Dispose();
                        }
                        throw new Exception($"Chaba did not end in 2 seconds");
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception($"Could not execute Chaba ({ProjectFilePath} with {process})", ex);
                }
            }
        }
        public class Output
        {
            public List<Point> results;
            internal System.IO.StreamReader file;
            public Output(string dir, string fileName, string extension)
            {
                // test
                string path = dir + fileName + extension;
                if (!System.IO.File.Exists(path))
                {
                    //can be because chaba use a differetn output name then expected .chaba.res instead of .out
                    if (!System.IO.File.Exists(path)) throw new Exception(string.Format(R.T031, path , dir + fileName + ".chaba.res"));
                }
                    
                results = new List<Point>();
                foreach (var line in ReadPointLines(path))
	            {
                    Point p = T.Conversions.FileFormat.ChabaLineToPoint(line);
                    p._Origin = fileName;
                    this.results.Add(p);
	            }
            }

            private List<string> ReadPointLines(string path)
            {
                string line;
                List<string> pointLines = new List<string>();

                file =new System.IO.StreamReader(path);

                FindAtPosition(file, "ANCIEN", 41, 6); //   COORDONNÉES POINTS REFERENCE (ACTIFS) (41)ANCIEN SYSTÈME DANS LE NOUVEAU SYSTÈME
                SkipLines(2);
                while ((line = file.ReadLine()) != "")
                    pointLines.Add(CleanFromDoubleSpace(line));
                try
                {
                    FindAtPosition(file, "PASSIFS", 32, 7); //   COORDONNÉES POINTS MODIFIES ((32)PASSIFS) DANS LE NOUVEAU SYSTÈME
                    SkipLines(2);
                    while ((line = file.ReadLine()) != "" && line != null)
                        pointLines.Add(CleanFromDoubleSpace(line));
                }
                catch (Exception ex)
                {
                    Logs.Log.AddEntryAsFYI(null, "No passifs points that are not common with the actif ones.");
                }
                

                file.Close();
                file.Dispose();
                return pointLines;
            }
            private void SkipLines(int p)
            {
                for (int i = 0; i < p; i++)
                {
                    file.ReadLine();
                }
            }
            private void FindAtPosition(System.IO.StreamReader file, string word, int start, int length)
            {
                string line;
                bool found =false;
                while (!found)
                {
                    line = file.ReadLine();
                    if (line.Length>start+length)
                        if (line.Substring(start, length) == word) 
                            found = true;
                }
            }
            private string CleanFromDoubleSpace(string line)
            {
                RegexOptions options = RegexOptions.None;
                Regex regex = new Regex("[ ]{2,}", options);
                return  regex.Replace(line, " ");
            }
        }
    }
}
