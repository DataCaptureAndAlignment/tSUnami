﻿using LibVLCSharp.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using TSU.Views.Message;
using E = TSU.Common.Elements;
using R = TSU.Properties.Resources;


namespace TSU.IO.SUSoft
{

    public static class Lgc2
    {
        public static bool ToLgc2(object o, string path, int forcedPrecision = 7, bool consiLibre = false) // polymorphisme used as Visitor pattern
        {
            if (path == "" && o is TsuObject tsuo) path = tsuo._Name;

            Visit(o, path, forcedPrecision, consiLibre);
            return true;
        }

        public static void Visit(object o, string path, int forcedPrecision = 7, bool consiLibre = false)
        {
            try
            {
                //    if (o == null) throw new Exception("Object to visit is null");
                //    if (path == null || path =="") throw new Exception("Path is null or empty");
                if (o == null) throw new Exception(R.T_OBJECT_TO_VISIT_IS_NULL);
                if (path == null || path == "") throw new Exception(R.T_PATH_IS_NULL_OR_EMPTY);
                dynamic dynamic = o; //notice the 'dynamic' type. this is the key to dynamic dispatch
                VisitCore(dynamic, path, forcedPrecision, consiLibre);
            }
            catch (Exception ex)
            {
                string custom = $"{R.T_ERR_VISIT_LGC} {o.GetType()} {R.T_FAILED}\r\n{ex.Message}";
                throw new Exception($"{custom}", ex);
            }

        }

        public static void VisitCore(Polar.Station.Module stm, string path, int forcedPrecision = 7, bool consiLibre = false)
        {
            Common.Compute.Compensations.Strategies.Common setupStrategy;
            if (stm.stationParameters.Setups.InitialValues.CompensationStrategy == null)
                setupStrategy = new Polar.Compensations.AllCala();
            else
                setupStrategy = stm.stationParameters.Setups.InitialValues.CompensationStrategy;

            setupStrategy.CheckIfGeoidalComputeIsPossible(stm.StationTheodolite);

            Common.Compute.Compensations.Lgc2 lgc = new Common.Compute.Compensations.Lgc2(
                stm.StationTheodolite,
                setupStrategy,
                setupStrategy.getTheRightCoordinates,
                setupStrategy.getTheRightOption,
                setupStrategy.coordianteSystemType,
                path,
                $"Input by Tsunami {Tsunami2.Properties.Version} {DateTime.Now}");

            Shell.Run(path);
        }

        public static void VisitCore(Polar.Module tm, string path, int forcedPrecision = 7, bool consiLibre = false)
        {
            try
            {
                List<Common.Station.Module> l = new List<Common.Station.Module>();
                l.AddRange(tm.StationModules);

                string title1 = "";
                string title2 = "";
                if (consiLibre)
                {
                    path = $"{Tsunami2.Preferences.Values.Paths.MeasureOfTheDay}{path}.consi.libr.inp";
                    title1 = $"CONSI LIBR input exported";
                    title2 = $"by Tsunami for Polar station(s)";
                }
                else
                {
                    title1 = "Input Exported by Tsunami";
                    title2 = "from Polar station(s)";
                }

                Common.Compute.Compensations.Lgc2 lgc = new Common.Compute.Compensations.Lgc2(
                    l,
                    tm._ElementManager.GetPointsInAllElements(),
                    path,
                    $"{title1} {title2} {Tsunami2.Properties.Version} {DateTime.Now}",
                    forcedPrecision,
                    useControls: true,
                    consiLibre);

                var fi = new FileInfo(path);
                new MessageInput(MessageType.GoodNews, 
                    $"{title1};{title2} \r\n" +
                    $"File: {MessageTsu.GetLink(path)}\r\n" +
                    $"Folder: {MessageTsu.GetLink(fi.Directory.ToString())}").Show();

            }
            catch (Exception ex)
            {
                string custom = "Visit of Advanced Theodolite final module";
                throw new Exception($"{custom}\r\n{ex}", ex);
            }

        }

        public static void VisitCore(List<Common.FinalModule> finals, string path, int forcedPrecision = 7, bool consiLibre = false)
        {
            try
            {
                List<Common.Station.Module> l = new List<Common.Station.Module>();
                finals.ForEach(x => l.AddRange(x.StationModules));
                List<E.Point> pts = new List<E.Point>();
                Tsunami2.Properties.Elements.ForEach(x => pts.AddRange((x as E.Element).GetPoints()));
                Common.Compute.Compensations.Lgc2 lgc = new Common.Compute.Compensations.Lgc2(
                    l,
                    pts,
                    path,
                    $"Input by Tsunami {Tsunami2.Properties.Version} {DateTime.Now}",
                    forcedPrecision);
            }
            catch (Exception ex)
            {
                string custom = "Visit of 'module list' failed";
                throw new Exception($"{custom}", ex);
            }

        }

        public static void VisitCore(List<Common.Station.Module> stationModules, string path, int forcedPrecision = 7, bool consiLibre = false)
        {
            try
            {
                List<Common.Station.Module> l = new List<Common.Station.Module>();
                stationModules.ForEach(x => l.Add(x));
                List<E.Point> pts = new List<E.Point>();
                Tsunami2.Properties.Elements.ForEach(x => pts.AddRange((x as E.Element).GetPoints()));
                Common.Compute.Compensations.Lgc2 lgc = new Common.Compute.Compensations.Lgc2(
                    l,
                    pts,
                    path,
                    $"Input by Tsunami {Tsunami2.Properties.Version} {DateTime.Now}",
                    forcedPrecision, consiLibre);
            }
            catch (Exception ex)
            {
                string custom = "Visit of 'module list' failed";
                throw new Exception($"{custom}", ex);
            }

        }

        public static void VisitCore(Common.Guided.Module gm, string path, int forcedPrecision = 7, bool consiLibre = false)
        {
            try
             { 
                string title1 = "";
                string title2 = "";
                if (consiLibre)
                {
                    path = $"{Tsunami2.Preferences.Values.Paths.MeasureOfTheDay}{path}.consi.libr";
                    title1 = $"CONSI LIBR input exported";
                    title2 = $"by Tsunami for Polar station(s)";
                }
                else
                {
                    title1 = "Input Exported by Tsunami";
                    title2 = "from Polar station(s)";
                }

                List<Common.Station.Module> l = new List<Common.Station.Module>();
                l.AddRange(gm.StationModules);
                Common.Compute.Compensations.Lgc2 lgc = new Common.Compute.Compensations.Lgc2(
                    l,
                    gm._ElementManager.GetPointsInAllElements(),
                    path,
                    $"{title1} {title2} {Tsunami2.Properties.Version} {DateTime.Now}",
                    forcedPrecision, 
                    consiLibre: consiLibre);

           

                var fi = new FileInfo(path);
                new MessageInput(MessageType.GoodNews,
                    $"{title1};{title2}\r\n" +
                    $"File: {MessageTsu.GetLink(path + ".inp")}\r\n" +
                    $"Folder: {MessageTsu.GetLink(fi.Directory.ToString())}").Show();
            }
            catch (Exception ex)
            {
                string custom = "Visit of Advanced Theodolite final module";
                throw new Exception($"{custom}\r\n{ex}", ex);
            }
        }
    }
}
