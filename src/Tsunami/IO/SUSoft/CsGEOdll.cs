using System;
using System.Collections.Generic;
using System.Text;
using R = TSU.Properties.Resources;
using System.Runtime.InteropServices;
using System.Windows.Controls;

namespace TSU.IO.SUSoft
{
    public class CsGEoException : Exception
    {
        public CsGEoException()
        {
        }

        public CsGEoException(string message)
            : base(message)
        {
        }

        public CsGEoException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }

    public static class CSGEOdll
    {
        const string dllPath = @"libs\SuSoft\CSGeoDLLx86\CSGeoDLL.dll";

        [StructLayout(LayoutKind.Sequential)]
        public class TPlainDataParameters
        {
            public int fRefFrame; /*!< Check lists of code below for valid values */
            public int fCoordSys; /*!< Check lists of code below for valid values */
            public int fAngUnits; /*!< Check lists of code below for valid values */
            public double fEpoch; /*!< Epoch of the coordinates in decimal years  */
            public string fSolution; /*!< Specific solution for terrestrial reference frame */
        }

        [StructLayout(LayoutKind.Sequential)]
        public class TPlainLocalSystemOrigin
        {
            public double fX;
            public double fY;
            public double fZ;
            public double fGisement; /*!< in gons */
            public double fSlope;    /*!< in radians */
        }

        public enum CSTypes
        {
            _3D_Cartesian = 1,
            Geodetic = 2,
            _2D_H = 4,
            _2D_Cartesian = 8,
            Geodetic_Sphere = 16
        }

        public enum AngularUnits
        {
            Radians = 0,
            Gons = 1,
            Decimal_Degrees = 5
        }

        public enum ReferenceFrames
        {
            kCCS = 0,  //   3D Cartesian.              // CERN coordinate system: 3 Cartesian coordinate systembased on the CGRF. Use for the positioning of the accelerator lines.
            kLAp0 = 1,  //   3D Cartesian.              // A local astronomical system (LA) at P0 has his vertical axis in the same direction of the vertical of P and the Y axis oriented in the north direction.
            kLGp0 = 2,  //   3D Cartesian.              // The local geodetic (LG) system is topocentric and a right-handed Cartesian coordinate system at P0.
            kCGRF = 3,  //   3D Cartesian or Geodetic.  // CERN geodetic reference frame based on the ellipsoidal reference surface
            kITRF97 = 4,  //   3D Cartesian or Geodetic.  // ITRF97@1998.5
            kWGS84 = 5,  //   3D Cartesian or Geodetic.  // Geocentric system based on the WSG84 ellipsoid. Beware: WGS84 is not aligned with the latest ITRF realization
            kROMA40 = 6,  //   3D Cartesian or Geodetic.  // Italian Geodetic system based on Hayford ellipsoid.
            kETRF93 = 7,  //   3D Cartesian or Geodetic.  // ETRF93@1993.0     
            kCH1903plus = 8,  //   3D Cartesian or Geodetic.  // New local reference system for the Swiss national survey LV95. Based on the Bessel 1941 ellipsoid.
            kCGRF_new = 9,  //do not use
            kCCS_new = 10,  //do not use
            kLAp0_new = 11,  //do not use
            kLGp0_new = 12,  //do not use
            kITRFin = 13,  //   3D Cartesian or Geodetic.  // ITRF as input coordinate system. Epoch and solution must be specified. Supported solution are: "ITRF 88", "ITRF 89", "ITRF 90", "ITRF 91", "ITRF 92", "ITRF 93", "ITRF 94", "ITRF 96", "ITRF 97", "ITRF 2000", "ITRF 2005", "ITRF 2008", "ITRF 2014"
            kITRFout = 14,  //   3D Cartesian or Geodetic.  // ITRF as output coordinate system. Epoch and solution must be specified. Supported solution are: "ITRF 88", "ITRF 89", "ITRF 90", "ITRF 91", "ITRF 92", "ITRF 93", "ITRF 94", "ITRF 96", "ITRF 97", "ITRF 2000", "ITRF 2005", "ITRF 2008", "ITRF 2014"
            kETRFin = 15,  //   3D Cartesian or Geodetic.  // ETRF as input coordinate system. Epoch and solution must be specified. Supported solution are: "ETRF 89", "ETRF 90", "ETRF 91", "ETRF 92", "ETRF 93", "ETRF 94", "ETRF 96", "ETRF 97", "ETRF 2000", "ETRF 2005", "ETRF 2014"
            kETRFout = 16,  //   3D Cartesian or Geodetic.  // ETRF as output coordinate system. Epoch and solution must be specified. Supported solution are: "ETRF 89", "ETRF 90", "ETRF 91", "ETRF 92", "ETRF 93", "ETRF 94", "ETRF 96", "ETRF 97", "ETRF 2000", "ETRF 2005", "ETRF 2014"
            kCGRFSphere = 108,  //   Geodetic Sphere.           // CERN geodetic reference frame based on spherical reference surface

            // Projections:
            kCernXYHe = 100, //   2D + H. //X, Y coordinates from the CCS, the height is calculated with the ellipsoid.
            kCernX0Y0He = 101, //   2D + H. //X, Y coordinates from the CCS, the height is calculated with the ellipsoid at P0.
            kCernXYHg00 = 102, //   2D + H. //X and Y coordinates come from the CCS. The H coordinate represents the height of geoid CG2000 defined in 2000 at the ellipsoid (H=0)
            kCernXYHg00Topo = 103, //   2D + H. //X and Y coordinates come from the CCS. The H coordinate represents the height of geoid CG2000 defined in 2000 on the top
            kCernXYHg00Machine = 104, //   2D + H. //X and Y coordinates come from the CCS. The H coordinate represents the height of geoid CG2000 defined in 2000 in the machine
            kCernXYHg85 = 105, //   2D + H. //X and Y coordinates come from the CCS. The H coordinate represents the height of geoid CG1985 defined in 1985 on the top
            kCernXYHg85Machine = 106, //   2D + H. //X and Y coordinates come from the CCS. The H coordinate represents the height of geoid CG1985 defined in 1985 in the tunnel
            kCERNXYHsSphereSPS = 107, //   2D + H. //X and Y coordinates come from the CCS. The H coordinate represents the height of the sphere defined for the SPS
            kCGRFMercator_eh = 115, //   2D + H. //CERN geodetic reference frame based on the ellipsoidal reference surface, transverse mercator projection. The central meridian is 6.14�E

            //Swiss system                                 
            kSwissLV95_eh = 109, //   2D + H.                     //New triangulation based on CHTRS95 (global positioning) and CH1903+ (local positioning). Ellipsoidal heights are relative to the Bessel 1941 ellispoid
            kSwissLV03_eh = 110, //   2D + H.                     //Triangulation network of 1903 (Bessel 1941 ellipsoidal height)
            kSwissLV95_lhn95 = 111, //   2D + H.                     //New triangulation based on CHTRS95 (global positioning) and CH1903+ (local positioning). CHGeo 2004 orthometric height
            kSwissLV03_ln02 = 112, //   2D + H.                     //Triangulation network of 1903 (LN02 leveled height)
            kSwissLV95_ln02 = 113, //   2D + H.                     //New triangulation based on CHTRS95 (global positioning) and CH1903+ (local positioning). LN02 leveled height
            kSwissLV03_lhn95 = 114, //   2D + H.                     //Triangulation network of 1903 (CHGeo 2004 orthometric height)
            kCHTRF95 = 210, //   3D Cartesian or Geodetic.   //New global reference system for the Swiss national survey LV95. Based on ETRF93 at epoch 1993.0.

            //French system
            kFrenchRGF93_CC46_eh = 205, //   2D + H.                    //Projection and ellipsoidal height of the RGF93 to minimize the linear alteration (9 projections to define the French areas. The 5th (CC46) is relative to the Geneva latitude). Ellipsoidal heights are relative to the ellispoid GRS80.
            kFrenchRGF93_CC46_ign69 = 206, //   2D + H.                  //Projection and altitude NGF-IGN69 of the RGF93 to minimize the linear alteration (9 projections to define the French areas. The 5th (CC46) is relative to the Geneva latitude). The RAF20 altimetric conversion grid is used.
            kLambert93_eh = 207, //   2D + H.                    //Conique conforme projection of the RGF93. Ellipsoidal heights are relative to the ellispoid GRS80.
            kLambert93_ign69 = 208, //   2D + H.                    //Conique conforme projection of the RGF93 (altitude NGF-IGN69). The RAF20 altimetric conversion grid is used.     
            kRGF93 = 209, //   3D Cartesian or Geodetic.  // Geocentric system based on the GRS80 ellipsoid. It is based on ETRF2000 at epoch 2019.0.

            // Local Reference Frames:
            kMLA1985Machine = 1000, //   3D Cartesian. //A modified local astronomical system(MLA) is a transformed LA (based on the geoid CG1985 at the machine level) by a Helmert transformation(describe by a bearing and slope angle).
            kMLA2000Machine = 1001, //   3D Cartesian. //A modified local astronomical system(MLA) is a transformed LA (based on the geoid CG2000 at the machine level) by a Helmert transformation(describe by a bearing and slope angle).
            kMLASphere = 1002, //   3D Cartesian. //A modified local astronomical system(MLA) is a transformed LA (based on the sphere) by a Helmert transformation(describe by a bearing and slope angle).
            kMLA2000Topo = 1003, //   3D Cartesian. //A modified local astronomical system(MLA) is a transformed LA (based on the geoid CG2000 at the topographic surface) by a Helmert transformation(describe by a bearing and slope angle).
            kMLA2000H0 = 1004, //   3D Cartesian. //A modified local astronomical system(MLA) is a transformed LA (based on the geoid CG2000 at level 0) by a Helmert transformation(describe by a bearing and slope angle).
            kMLA1985H0 = 1005, //   3D Cartesian. //A modified local astronomical system(MLA) is a transformed LA (based on the geoid CG1985 at level 0) by a Helmert transformation(describe by a bearing and slope angle).

            kLA1985Machine = 1010, //   3D Cartesian. //A local astronomical system (LA) at P has his vertical axis in the same direction of the vertical of P and the Y axis oriented in the north direction. The geodetic reference system is the geoid CG1985 at the machine level.
            kLA2000Machine = 1011, //   3D Cartesian. //A local astronomical system (LA) at P has his vertical axis in the same direction of the vertical of P and the Y axis oriented in the north direction. The geodetic reference system is the geoid CG2000 at the machine level.
            kLASphere = 1012, //   3D Cartesian. //A local astronomical system (LA) at P has his vertical axis in the same direction of the vertical of P and the Y axis oriented in the north direction. The geodetic reference system is the sphere.
            kLA2000Topo = 1013, //   3D Cartesian. //A local astronomical system (LA) at P has his vertical axis in the same direction of the vertical of P and the Y axis oriented in the north direction. The geodetic reference system is the geoid CG2000 at the topographic surface.
            kLA2000H0 = 1014, //   3D Cartesian. //A local astronomical system (LA) at P has his vertical axis in the same direction of the vertical of P and the Y axis oriented in the north direction. The geodetic reference system is the geoid CG2000 at level 0.
            kLA1985H0 = 1015, //   3D Cartesian. //A local astronomical system (LA) at P has his vertical axis in the same direction of the vertical of P and the Y axis oriented in the north direction. The geodetic reference system is the geoid CG1985 at the level 0.

            kMLGGRS80 = 2000, //   3D Cartesian. //The MLG is an LG (base on the ellipsoid GRs80) modified by a Helmert transformation (describe by a bearing and slope angle)
            kMLGSphere = 2001, //   3D Cartesian. //The MLG is an LG (based on the sphere) modified by a Helmert transformation (describe by a bearing and slope angle)
            kLGGRS80 = 2010, //   3D Cartesian. //The local geodetic (LG) system is topocentric and a right-handed Cartesian coordinate system. The geodetic reference is the ellipsoid GRS80.
            kLGSphere = 2011, //   3D Cartesian. //The local geodetic (LG) system is topocentric and a right-handed Cartesian coordinate system. The geodetic reference is the sphere.

        }

        // Call DLL and declare function
        [DllImport(dllPath, EntryPoint = "transformPoint", CallingConvention = CallingConvention.Cdecl)]
        public static extern int transformPoint(
            ref double c1, ref double c2, ref double c3,
            TPlainDataParameters inputParams, TPlainLocalSystemOrigin inputLocalSystemOrigin,
            TPlainDataParameters outputParams, TPlainLocalSystemOrigin outputLocalSystemOrigin);

        public static (double, double, double) TransformPoint(double X, double Y, double Z,
            TPlainDataParameters inputParams, TPlainLocalSystemOrigin inputLocalOrigin,
            TPlainDataParameters outputParams, TPlainLocalSystemOrigin outputLocalOrigin)
        {
            
            try
            {
                int result = transformPoint(
                ref X, ref Y, ref Z, inputParams, inputLocalOrigin, outputParams, outputLocalOrigin);

                string message = "";
                switch (result)
                {
                    case 0:
                        message = "CsGEO transform point is a Success";
                        break;
                    case 1:
                        message = "Invalid Input RefFrame";
                        throw new CsGEoException("Invalid Input RefFrame");
                    case 2:
                        message = "Invalid output RefFrame";
                        throw new CsGEoException("Invalid Output RefFrame");
                    case 3:
                        message = "Invalid Input CoordSys";
                        throw new CsGEoException("Invalid Input CoordSys");
                    case 4:
                        message = "Invalid Output CoordSys";
                        throw new CsGEoException("Invalid Output CoordSys");
                    case 5:
                        message = "Missing input LSO";
                        throw new CsGEoException("Missing Input Local System Origin");
                    case 6:
                        message = "Missing output LSO";
                        throw new CsGEoException("Missing Output Local System Origin");
                    case 7:
                        message = "Superfluous input LSO";
                        throw new CsGEoException("Superfluous Input Local System Origin");
                    case 8:
                        message = "Superfluous Output LSO";
                        throw new CsGEoException("Superfluous Output Local System Origin");
                    case 9:
                        message = "Invalid Input AngUnits";
                        throw new CsGEoException("Invalid Input AngUnits");
                    case 10:
                        message = "Invalid Output AngUnits";
                        throw new CsGEoException("Invalid Output AngUnits");
                    case 11:
                        message = "Cannot Setup TDAtaparameters";
                        throw new CsGEoException("Cannot Setup TDataParameters");
                    case 12:
                        message = "Transform Error";
                        throw new CsGEoException("Transform Error");
                    case 13:
                        message = "Missing Input Epoch";
                        throw new CsGEoException("Missing Input Epoch");
                    case 14:
                        message = "Missing input Solution";
                        throw new CsGEoException("Missing Input Solution");
                    case 15:
                        message = "Missing output Epoch";
                        throw new CsGEoException("Missing Output Epoch");
                    case 16:
                        message = "Missing output solution";
                        throw new CsGEoException("Missing Output Solution");
                    case 17:
                        message = "Bad input TRF";
                        throw new CsGEoException("Bad Input TRF");
                    case 18:
                        message = "Bad output TRF";
                        throw new CsGEoException("Bad Output TRF");
                    default:
                        break;
                }
                TSU.Debug.WriteInConsole(message);
            }
            catch (Exception ex)
            {
                throw new CsGEoException("Dll call failed here", ex);
            }
            return (X,Y,Z);
        }

        public static float TransformPointTest()
        {
            // PJ01 in MLA 1985              -8.8109000         2.0567200        -1.0946900
            // ORIGIN     IP8_2014_SU     4472.525917     5005.972899     2330.049057     324.734623     0.00017239
            // PJ01 in CERNxyH1985              4467.28426                 4998.59735                  330.08705

            TPlainDataParameters inputParams = new TPlainDataParameters();
            inputParams.fRefFrame = (int)ReferenceFrames.kMLA1985Machine;
            inputParams.fCoordSys = (int)CSTypes._3D_Cartesian;
            inputParams.fAngUnits = (int)AngularUnits.Gons;
            inputParams.fEpoch = 0.0;
            //inputParams.fSolution = '\0';

            TPlainLocalSystemOrigin inputLocalSystemOrigin = new TPlainLocalSystemOrigin();
            inputLocalSystemOrigin.fX = 4472.525917;
            inputLocalSystemOrigin.fY = 5005.972899;
            inputLocalSystemOrigin.fZ = 2330.049057;
            inputLocalSystemOrigin.fGisement = 324.734623;
            inputLocalSystemOrigin.fSlope = 0.00017239;


            TPlainDataParameters outputParams = new TPlainDataParameters();
            outputParams.fRefFrame = (int)ReferenceFrames.kCernXYHg85Machine;
            outputParams.fCoordSys = (int)CSTypes._2D_H;
            outputParams.fAngUnits = (int)AngularUnits.Gons;
            outputParams.fEpoch = 0.0;
            outputParams.fSolution = "\0";

            TPlainLocalSystemOrigin outputLocalSystemOrigin = new TPlainLocalSystemOrigin();

            double c1 = -8.8109000;
            double c2 = 2.0567200;
            double c3 = -1.0946900;

            int result = -1;
            try
            {
                result = transformPoint(
                ref c1, ref c2, ref c3, inputParams, inputLocalSystemOrigin, outputParams, null);

                switch (result)
                {
                    case 0:
                        TSU.Debug.WriteInConsole("Sucess");
                        break;
                    case 1:
                        TSU.Debug.WriteInConsole("Invalid Input RefFrame");
                        throw new CsGEoException("Invalid Input RefFrame");
                    case 2:
                        TSU.Debug.WriteInConsole("Invalid output RefFrame");
                        throw new CsGEoException("Invalid Output RefFrame");
                    case 3:
                        TSU.Debug.WriteInConsole("Invalid Input CoordSys");
                        throw new CsGEoException("Invalid Input CoordSys");
                    case 4:
                        TSU.Debug.WriteInConsole("Invalid Output CoordSys");
                        throw new CsGEoException("Invalid Output CoordSys");
                    case 5:
                        TSU.Debug.WriteInConsole("Missing input LSO");
                        throw new CsGEoException("Missing Input Local System Origin");
                    case 6:
                        TSU.Debug.WriteInConsole("Missing output LSO");
                        throw new CsGEoException("Missing Output Local System Origin");
                    case 7:
                        TSU.Debug.WriteInConsole("Suprefluous input LSO");
                        throw new CsGEoException("Superfluous Input Local System Origin");
                    case 8:
                        TSU.Debug.WriteInConsole("Superfluous Output LSO");
                        throw new CsGEoException("Superfluous Output Local System Origin");
                    case 9:
                        TSU.Debug.WriteInConsole("Invalid Input AngUnits");
                        throw new CsGEoException("Invalid Input AngUnits");
                    case 10:
                        TSU.Debug.WriteInConsole("Invalid Ouptut AngUnits");
                        throw new CsGEoException("Invalid Output AngUnits");
                    case 11:
                        TSU.Debug.WriteInConsole("Cannot Setup TDAtaparameters");
                        throw new CsGEoException("Cannot Setup TDataParameters");
                    case 12:
                        TSU.Debug.WriteInConsole("Transform Erro");
                        throw new CsGEoException("Transform Error");
                    case 13:
                        TSU.Debug.WriteInConsole("Missing Input Epoch");
                        throw new CsGEoException("Missing Input Epoch");
                    case 14:
                        TSU.Debug.WriteInConsole("Missing input Solution");
                        throw new CsGEoException("Missing Input Solution");
                    case 15:
                        TSU.Debug.WriteInConsole("Missing output Epoch");
                        throw new CsGEoException("Missing Output Epoch");
                    case 16:
                        TSU.Debug.WriteInConsole("Missing output solution");
                        throw new CsGEoException("Missing Output Solution");
                    case 17:
                        TSU.Debug.WriteInConsole("Bad input TRF");
                        throw new CsGEoException("Bad Input TRF");
                    case 18:
                        TSU.Debug.WriteInConsole("Bad output TRF");
                        throw new CsGEoException("Bad Output TRF");
                    default:
                        break;
                }
                TSU.Debug.WriteInConsole(c1);
                TSU.Debug.WriteInConsole(c2);
                TSU.Debug.WriteInConsole(c3);
            }
            catch (Exception ex)
            {
                throw new CsGEoException("Dll call failed here", ex);
            }


            return result;
        }

    }

}
