﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using System.Threading.Tasks;
using P = TSU.Preferences;
using T = TSU.Tools;


namespace TSU.IO.SUSoft
{
    public static class GeoFit
    {
        public static FittedShape Line3D(CompositeElement toBeFit)
        {
            // Write input
            string input = "";
            string shapeName = "Line";
            string fitName = "tsunamiFitting"+shapeName;
            input += fitName+" BEGINS:	-f 1" + "\r\n";
            input += WritePoints(toBeFit, fitName);

            //Run and read output
            string outputFilePath = Run(input);
            string[] vector = ReadOutputVector(outputFilePath);

                
            // Create Result
            Line3D shape =
                new Line3D(
                    new Coordinates(vector, 0),
                    new Coordinates(vector, 3));
            shape._Name = shapeName;

            shape.Elements.AddRange(toBeFit.Elements);
            return new FittedShape(shape, toBeFit, outputFilePath);
        }

        public static FittedShape Plane(CompositeElement toBeFit)
        {
            // Write input
            string input = "";
            string shapeName = "Plane";
            string fitName = "tsunamiFitting" + shapeName;
            input += fitName + " BEGINS:	-f 2" + "\r\n";
            input += WritePoints(toBeFit, fitName);

            //Run and read output
            string outputFilePath = Run(input);
            string[] vector = ReadOutputVector(outputFilePath);

            // Create Result
            Plane shape = new Plane(
                new Coordinates(vector, 0),
                new DoubleValue(Tools.Conversions.Numbers.ToDouble(vector[3]), 0.00));
            shape._Name = shapeName;

            shape.Elements.AddRange(toBeFit.Elements);
            return new FittedShape(shape, toBeFit,outputFilePath);
        }
        public static FittedShape Sphere(CompositeElement toBeFit)
        {
            // Write input
            string input = "";
            string shapeName = "Sphere";
            string fitName = "tsunamiFitting" + shapeName;
            input += fitName + " BEGINS:	-f 3" + "\r\n";
            input += WritePoints(toBeFit, fitName);

            //Run and read output
            string outputFilePath = Run(input);
            string[] vector = ReadOutputVector(outputFilePath);

            // Create Result
            Sphere shape = 
                new Sphere(
                    new Coordinates(vector, 0),
                    new DoubleValue(Tools.Conversions.Numbers.ToDouble(vector[3]), 0.00));
            shape._Name = shapeName;

            shape.Elements.AddRange(toBeFit.Elements);
            return new FittedShape(shape, toBeFit, outputFilePath);
        }
        public static FittedShape Circle(CompositeElement toBeFit)
        {
            // Write input
            string input = "";
            string shapeName = "Circle";
            string fitName = "tsunamiFitting" + shapeName;
            input += fitName + " BEGINS:	-f 4" + "\r\n";
            input += WritePoints(toBeFit, fitName);

            //Run and read output
            string outputFilePath = Run(input);
            string[] vector = ReadOutputVector(outputFilePath);

            // Create Result
            Circle shape = 
                new Circle(
                    new Coordinates(vector, 0), 
                    new Coordinates(vector, 4),
                    new DoubleValue(Tools.Conversions.Numbers.ToDouble(vector[3]), 0.00));
            shape._Name = shapeName;
            shape.Elements.AddRange(toBeFit.Elements);
            return new FittedShape(shape, toBeFit, outputFilePath);
        }
        public static FittedShape Cylinder(CompositeElement toBeFit)
        {
            // Write input
            string input = "";
            string shapeName = "Cylinder";
            string fitName = "tsunamiFitting" + shapeName;
            input += fitName + " BEGINS:	-f 6" + "\r\n";
            // It will crashes if the line are present and nothing fixed..
            //input += "tsunamiFitting INIVAL:  0.00    0.00    0.00    0.00    0.00    0.00" + "\r\n";
            //input += "tsunamiFitting INIFIX:  0       0       0       0       0       0" + "\r\n";  
            input += WritePoints(toBeFit, fitName);

            //Run and read output
            string outputFilePath = Run(input);
            string[] vector = ReadOutputVector(outputFilePath);

            // Create Result
            Cylinder shape =
                new Cylinder(
                    new Coordinates(vector, 0),
                    new Coordinates(vector, 3),
                    new DoubleValue(Tools.Conversions.Numbers.ToDouble(vector[6]), 0.00));
            shape._Name = shapeName;

            shape.Elements.AddRange(toBeFit.Elements);
            return new FittedShape(shape, toBeFit, outputFilePath);
        }

        private static string Run(string input)
        {

            const int MAX_RUNNING_TIME = 2000;
            // create input file
            string dirName =TSU.Tsunami2.Preferences.Values.Paths.Temporary + @"GeoFit\";
            System.IO.Directory.CreateDirectory(dirName);
            string time = T.Conversions.Date.ToDateUpSideDown(DateTime.Now);
            string inputPath = dirName + time + "_GeoFit.inp";
            string outputPath = dirName + time + "_GeoFit.out";
            System.IO.File.WriteAllText(inputPath, input);

            // run it
            if (!TSU.Preferences.Preferences.Dependencies.GetByBame("GEOFIT", out Common.Dependencies.Application app))
                throw new Exception("GEOFIT not found");
            string process = app.Path; 
            System.Diagnostics.ProcessStartInfo cmdsi = new System.Diagnostics.ProcessStartInfo(process);
            cmdsi.CreateNoWindow = false;
            cmdsi.Arguments += " -i " + inputPath;
            cmdsi.Arguments += " -o " + outputPath;
            cmdsi.Arguments += " -v ";
            cmdsi.WorkingDirectory = System.IO.Path.GetDirectoryName(dirName);
            System.Diagnostics.Process cmd = System.Diagnostics.Process.Start(cmdsi);


            if (!cmd.WaitForExit(MAX_RUNNING_TIME))
            {
                if (!cmd.HasExited)
                {
                    cmd.Kill();
                    cmd.Dispose();
                }
                throw new Exception($"GeoFit did not end in 2 seconds");
            }


            return outputPath;
        }
        private static string[] ReadOutputVector(string outputPath)
        {
            try
            {


                string output = System.IO.File.ReadAllText(outputPath);
                if (output.Contains("Process ERROR")) throw new Exception("Process ERROR found in the GeoFitOutput");

                System.IO.StringReader vectorStream = new System.IO.StringReader(output);
                string line;
                string searchWords = "Solution Vector : ";
                while ((line = vectorStream.ReadLine()) != null)
                    if (line.Contains(searchWords))
                    {
                        line = System.Text.RegularExpressions.Regex.Replace(line, @"(\s)\s+", "$1");
                        return line.Substring(line.IndexOf(searchWords) + searchWords.Length).Split(' ');
                    }
                //   throw new Exception("No Solution Vector found");
                throw new Exception(R.T_NO_SOLUTION_VECTOR_FOUND);
            }
            catch (Exception ex )
            {
                throw new Exception(string.Format(R.T036, ex.Message ), ex);
            }
            
        }
        private static string WritePoints(CompositeElement toBeFit, string fitName)
        {
            string input = "";
            double weight = 1.00;
            foreach (Point p in toBeFit.Elements)
            {
                input += fitName+" DATAPT: " +
                    p._Name + " " +
                    p._Coordinates.Local.X.Value + " " +
                    p._Coordinates.Local.Y.Value + " " +
                    p._Coordinates.Local.Z.Value + " " +
                    weight +
                    "\r\n";
            }
            input += fitName+" COMPLT: " + toBeFit.Elements.Count + " " + "0";
            return input;
        }
    }
}
