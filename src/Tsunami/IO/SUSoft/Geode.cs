﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Shapes;
using TC = TSU.Common;
using TSU.Common.Analysis;
using TSU.Common.Compute;
using TSU.Common.Elements;
using TSU.Common.Instruments.Device;
using TSU.Common.Measures.States;
using TSU.Common.Strategies.Views;
using TSU.Polar;
using TSU.Views;
using TSU.Views.Message;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using R = TSU.Properties.Resources;
using T = TSU.Tools;
using System.Activities.Expressions;

namespace TSU.IO.SUSoft
{
    public static class Geode
    {
        [Flags]
        public enum Roles
        {
            Unknown = 0,
            R_as_Cala = 1,          //for compatibility
            Cala = 1,
            C_as_Radi = 2,           //for compatibility
            Radi = 2,
            P_as_Ray = 4,             //for compatibility
            A_as_Ray = 4,            //for compatibility
            A_as_Alignment = 4,     //for compatibility
            Poin = 4,
            Unused = 8,
            Pdor = 16,
        }

        private static string separator = ";";
        private static List<int> lineNumbersAlreadyRecorded = new List<int>();

        static int lineNumber = 1;

        /// <summary>
        /// Convert an object to a GEODE file
        /// </summary>
        public static bool ToGeode(object o, out string content, string fileName = "", string directoryPath = "", bool AskForFilePathConfirmation = false, bool bShowFile = true) // polymorphisme used as Visitor pattern
        {
            try
            {
                if (directoryPath == "")
                    directoryPath = GetBestDirectory();

                if (fileName == "")
                {
                    if (o is Station st)
                    {
                        fileName = GetFileName(st.Parameters2);
                    }
                }

                if (fileName == "")
                    fileName = directoryPath + "GeodeExport.dat";
                else
                    fileName = directoryPath + fileName;

                ///Pour avoir le bon numéro de ligne
                List<String> l = new List<string>();
                if (File.Exists(fileName))
                {
                    string[] readedTextInFile = File.ReadAllLines(fileName);
                    lineNumber = readedTextInFile.Count() + 1;
                }
                else
                {
                    lineNumber = 1;
                }
                Visit(o, ref l, ref fileName, ref lineNumber, "  ");

                l = l.Select(s => s == "" ? "!" : s).ToList();

                AppendToFile(l, fileName, bShowFile);

                content = string.Join("\r\n", l);
                return true;
            }
            catch (Exception ex)
            {
                throw new TsuException($"{R.T_COULD_NOT} {R.T_EXPORT_TO} GEODE;{ex.Message}", ex);
            }
        }

        public static string GetNpLine(Point p)
        {
            string line = string.Format("{0:D3}", lineNumber);
            string prefix = p.State == Common.Elements.Element.States.Bad ? "%" : "";
            string comment = p.State == Common.Elements.Element.States.Bad ? "%BAD" : "";
            line = prefix + line + comment;
            lineNumber++;
            AddToLine(ref line, ToNpLine(p, p.Date));
            return line;
        }

        private static string GetBestDirectory()
        {
            string directory = Tsunami2.Properties.PathOfTheSavedFile;
            directory = System.IO.Path.GetDirectoryName(directory);

            if (directory == null || directory == "")
                directory = Tsunami2.Preferences.Values.Paths.MeasureOfTheDay;
            else
                directory += "\\";

            return directory;
        }

        private static void AppendToFile(List<string> s, string finalPath, bool showFile = true)
        {
            try
            {
                if (s.Count == 0) throw new Exception(R.T_NOTHING_TO_EXPORT);
                File.AppendAllLines(finalPath, s);
                if (showFile)
                    Shell.Run(finalPath);
            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_COULD_NOT} {R.T_APPEND} {R.T_FILE} '{finalPath}'.\r\n{ex.Message}", ex);
            }
        }

        internal static string GetFileName(Common.Station.Parameters parameters, string measTypeName = "")
        {
            bool newNameForASG = true; // tsu 2844
            bool useModuleType = false; // tsu 2844

            string Name;
            if (newNameForASG)
            {
                string op = parameters._Operation.value.ToString("00000");
                string Date = parameters._Date.ToString("yyyy-MM-dd");
                string team = parameters._Team.Length > 6 ? parameters._Team.Substring(0, 6) : parameters._Team;
                string type = useModuleType && measTypeName != "" ? "_" + measTypeName : "";
                Name = $"{op}_{Date}_{team}{type}.dat";
            }
            else
            {
                string op = parameters._Operation.value.ToString();
                string team = parameters._Team;
                string Date = parameters._Date.ToString("yy_MM_dd");
                Name = $"{op}_{Date}_{team}.dat";
            }

            return Name;
        }

        public static void Visit(object o, ref List<String> lines, ref string path, ref int lineNumber, string prefix)
        {
            try
            {
                dynamic dynamicMeasure = o; //notice the 'dynamic' type. this is the key to dynamic dispatch
                VisitCore(dynamicMeasure, ref lines, ref path, ref lineNumber, prefix);
            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_ERR_VISIT_GEODE} {o.GetType()}", ex);
            }

        }

        internal static string AddToLine(ref string line, params string[] additionalTexts)
        {
            foreach (string s in additionalTexts)
            {
                line = line + separator + s;
            }
            line += ";";
            return line;
        }

        /// <summary>
        /// Create a array of line containing lines of a geode file
        /// </summary>
        // 001;RE;19-Oct-2016;ELPA      ; 12448;
        // 002;OB;AT401    ;390769      ;TT41      ;STL.19101601.                    ;; 0.00000;;19.8;19.8; 977;45;00;;



        public static void VisitCore(List<Common.Station> stations, ref List<String> lines, ref string path, ref int lineNumber, string prefix)
        {
            foreach (Station item in stations.OfType<Station>())
            {
                VisitCore(item, ref lines, ref path, ref lineNumber, prefix);
            }
        }

        public static void VisitCore(List<Station> stations, ref List<String> lines, ref string path, ref int lineNumber, string prefix)
        {
            foreach (Station item in stations)
            {
                VisitCore(item, ref lines, ref path, ref lineNumber, prefix);
            }
        }

        public static void VisitCore(Station.Parameters param, ref List<String> lines, ref string path, ref int lineNumber, string prefix)
        {
            ToNpLine(param._StationPoint, param._Date);
        }

        public static string GetStationPrefix(Common.Station st, string actualPrefix)
        {
            if (st.ParametersBasic._State is Common.Station.State.Bad)
            {
                if (Tsunami2.Preferences.Values.GuiPrefs.ExportBadPolarStationsAsCommentedLines.IsTrue)
                    return "% "; // we dont want geode to readit
            }

            return actualPrefix;
        }

        public static List<int> GetLineNumbersAlreadyRecorded(Station st, string tempPath)
        {
            List<int> lineNumbersAlreadyRecorded = new List<int>();
            string search_text = "----------" + st._Name + " Polar recorded at";
            try
            {
                using (StreamReader reader = new StreamReader(tempPath))
                {
                    string line;
                    int lineNumber = 0;

                    while ((line = reader.ReadLine()) != null)
                    {
                        lineNumber++;
                        if (line.Contains(search_text))
                        {
                            lineNumbersAlreadyRecorded.Add(lineNumber);
                        }
                    }
                }
                return lineNumbersAlreadyRecorded;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
            return lineNumbersAlreadyRecorded;
        }

        public static void AddLineRefStation(Station st, List<int> lineNumbersAlreadyRecorded, ref List<String> lines, int lineNumber)
        {
            string lineText = string.Format("! {0} ----------", string.Format("{0:D3}", lineNumber));
            lineText += string.Format("{0} Polar recorded at ", st._Name);
            lineText += string.Format("{0,-8}", DateTime.Now.ToString("T", CultureInfo.CreateSpecificCulture("fr-FR")));
            if (lineNumbersAlreadyRecorded.Count != 0)
            {
                for (int i = 0; i < lineNumbersAlreadyRecorded.Count; i++)
                {
                    int item = lineNumbersAlreadyRecorded[i];
                    if (i == 0)
                        lineText += string.Format(" ALREADY RECORDED AT LINE {0}", item);
                    else
                        lineText += string.Format(", {0}", item);
                }
            }
            lineText += "----------";
            lines.Add(lineText);
        }

        public static void VisitCore(Station st, ref List<String> lines, ref string path, ref int lineNumber, string prefix)
        {
            try
            {
                if (st.Parameters2._StationPoint == null) return;

                if (st.ParametersBasic._Instrument._Name == R.String_Unknown) return;

                prefix = GetStationPrefix(st, prefix);



                bool appendTheStationAlone = false;
                string tempPath = path;

                if (tempPath.Substring(tempPath.Length - 1, 1) == "\\") // there si no specific file just a directory so each station should defnie the dat file name
                {
                    appendTheStationAlone = true;
                    lines.Clear();
                    tempPath = path + GetFileName(st.Parameters2);
                }

                if (lineNumber == 1)
                {
                    // RE line
                    string line = "  " + string.Format("{0:D3}", lineNumber);
                    lineNumber++;


                    AddToLine(ref line,
                              "RE",
                              string.Format(new CultureInfo("en-US"), "{0:dd-MMM-yyyy}", st.ParametersBasic._Date),
                              string.Format("{0,-10}", st.ParametersBasic._Team),
                              string.Format("{0,6}", st.ParametersBasic._Operation.value),
                              $"Tsunami {Tsunami2.Properties.Version}");
                    lines.Add(line);
                }


                // new point ?

                GetNewPoints(st, ref lines, ref lineNumber, prefix);
                lineNumbersAlreadyRecorded = GetLineNumbersAlreadyRecorded(st, tempPath);
                AddLineRefStation(st, lineNumbersAlreadyRecorded, ref lines, lineNumber);
                lineNumber++;

                // OB line
                AddObLine(st, lines, ref lineNumber);

                // Measure Lines

                // find measure to export
                List<Polar.Measure> toExport = new List<Polar.Measure>();
                List<M.Measure> measureToExportDefinedInTheStation = st.MeasuresToExport;
                if (measureToExportDefinedInTheStation.Count != 0)
                {
                    foreach (var item in measureToExportDefinedInTheStation)
                    {
                        toExport.Add(item as Polar.Measure);
                    }
                }
                else
                {
                    foreach (var item in st.MeasuresTaken)
                    {
                        toExport.Add(item as Polar.Measure);
                    }
                }

                bool putObversationTogether = true;
                if (putObversationTogether)
                {
                    VisitCore(toExport, st, ref lines, ref tempPath, ref lineNumber, prefix);
                }
                else
                {
                    foreach (Polar.Measure mes in toExport)
                        VisitCore(mes, st, ref lines, ref tempPath, ref lineNumber, prefix);
                }

                // Offsets
                foreach (Polar.Measure mes in toExport)
                    GetOffsets(mes, st, ref lines, ref lineNumber);

                if (appendTheStationAlone)
                    AppendToFile(lines, tempPath);
            }
            catch (Exception ex)
            {
                throw new Exception($"Cannot visit '{st._Name}'", ex);
            }
        }

        public static bool PolarModuleToGeode(TC.FinalModule finalModule, bool askForFilePathConfirmation, ref string filename, ref string folder, bool bShowFile)
        {
            List<TC.Station> stations = finalModule.GetListOfStationsClosedOrBad();

            foreach (var s in stations)
            {
                if (!(s.ParametersBasic._State is TC.Station.State.Closed || s.ParametersBasic._State is TC.Station.State.Bad))
                    throw new TsuException(string.Format($"{R.T_STATION_IN_PROGRESS};{R.T_YOU_SHOULD_CLOSE_THE_IN_ORDER_TO_EXPORT_IT}", s._Name));
            }


            if (stations.Count > 0)
            {
                if (filename == "")
                    filename = IO.SUSoft.Geode.GetFileName(stations[stations.Count - 1].ParametersBasic);
                if (folder == "")
                    folder = Tsunami2.Preferences.Values.Paths.MeasureOfTheDay;

                // also export a *CONSI-LIBR lgc to check validity before geode insertion of the dat
                IO.SUSoft.Lgc2.ToLgc2(finalModule, filename, forcedPrecision: 5, consiLibre: true);

                return IO.SUSoft.Geode.ToGeode(stations, out _, filename, folder, AskForFilePathConfirmation: askForFilePathConfirmation, bShowFile: bShowFile);
            }
            return false;
        }


        private static void AddObLine(Station st, List<string> lines, ref int lineNumber)
        {
            // no prefix for RE and OB, because AVN, will not see then and the next bas measure will associated to previosu station
            string line = "  " + string.Format("{0:D3}", lineNumber);

            // 1
            lineNumber++;

            // Compute non-trivial properties
            Station.Parameters p2 = st.Parameters2;
            Station.Parameters.Setup setups = p2.Setups;
            Station.Parameters.Setup.Values best = setups.BestValues;
            Station.Parameters.Setup.Values init = setups.InitialValues;
            Point stationPoint = p2._StationPoint;
            M.WeatherConditions weatherConditions = st.WeatherConditions;

            string RouA;
            if (best == null)
            {
                if (stationPoint._Coordinates.HasAny)
                    RouA = "R";
                else
                    RouA = "A";
            }
            else
            {
                if (best.Strategy == Common.Compute.Compensations.Strategies.List.OrientatedOnly_LGC2)
                    RouA = "R";
                else
                    RouA = "A";
            }

            string wetTemp;
            if (weatherConditions.WetTemperature != Tsunami2.Preferences.Values.na)
                wetTemp = weatherConditions.WetTemperature.ToString("F1", new CultureInfo("en-US"));
            else
                wetTemp = "";

            string hi;
            if (init.IsInstrumentHeightKnown == Station.Parameters.Setup.Values.InstrumentTypeHeight.Known)
                hi = "HI_FIX";
            else
                hi = "HI_CALC";

            string sigma0;
            if (best == null)
                sigma0 = "?";
            else
                sigma0 = best.SigmaZero.ToString();

            //Ajoute le texte avec la date de compensation du TS60
            string checkAndAdjust = "";
            if (st.ParametersBasic._Instrument is I.Device.TS60.Instrument TS60
             && TS60._LastCompensationDate != DateTime.MinValue)
            {
                checkAndAdjust = " Check and Adjust ";
                if (TS60._Comp_L) checkAndAdjust += "l, t,";
                if (TS60._Comp_i_indexV) checkAndAdjust += "i, c,";
                if (TS60._Comp_a_AxeA) checkAndAdjust += "a,";
                if (TS60._Comp_ATRHz) checkAndAdjust += "ATR";
                checkAndAdjust += " done the ";
                checkAndAdjust += TS60._LastCompensationDate.Day + "-";
                checkAndAdjust += TS60._LastCompensationDate.Month + "-";
                checkAndAdjust += TS60._LastCompensationDate.Year;
            }

            string verti;
            if (init.VerticalisationState == Station.Parameters.Setup.Values.InstrumentVerticalisation.NotVerticalised)
                verti = "ROT3D";
            else
                verti = "";

            AddToLine(ref line,
                      "OB", //2
                      string.Format("{0,-9}", st.ParametersBasic._Instrument._Model),
                      string.Format("{0,-11}", st.ParametersBasic._Instrument._SerialNumber),
                      string.Format("{0,-10}", stationPoint._Accelerator), //5
                      string.Format("{0,-33}", stationPoint.FullNameWithoutZone),
                      "",
                      string.Format("{0,8}", p2._InstrumentHeight.Value.ToString("F5", new CultureInfo("en-US"))),
                      "", //9
                      string.Format("{0,6}", wetTemp), //10
                      string.Format("{0,6}", weatherConditions.dryTemperature.ToString("F1", new CultureInfo("en-US"))),
                      string.Format("{0,6}", weatherConditions.pressure.ToString("F1", new CultureInfo("en-US"))),
                      string.Format("{0,6}", weatherConditions.humidity),
                      "00",//14
                      "", // 15 measure gyro?
                      RouA,
                      hi,
                      $"s0={sigma0}{checkAndAdjust}", //sigma0 + checkAndAdjust
                      verti);
            lines.Add(line);
        }

        public static void VisitCore(List<TsuObject> s, ref List<String> l, ref string path, ref int lineNumber, string prefix)
        {
            foreach (var item in s)
            {
                Visit(item, ref l, ref path, ref lineNumber, prefix);
            }
        }
        public static void VisitCore(List<Common.FinalModule> measuringModules, ref List<String> l, ref string path, ref int lineNumber, string prefix)
        {
            foreach (var finalModule in measuringModules)
            {
                VisitCore(finalModule, ref l, ref path, ref lineNumber, prefix);
            }
        }

        public static void VisitCore(Common.FinalModule tm, ref List<String> l, ref string path, ref int lineNumber, string prefix)
        {
            foreach (var stationModule in tm.StationModules)
            {
                if (stationModule is Station.Module)
                    VisitCore(stationModule._Station as Station, ref l, ref path, ref lineNumber, prefix);
            }
        }


        public static void VisitCore(Polar.Measure m, ref List<String> l, ref string path, ref int lineNumber, string prefix)
        {
            VisitCore(m, new Station(), ref l, ref path, ref lineNumber, prefix);
        }

        public static void VisitCore(List<Polar.Measure> list, Station station, ref List<String> l, ref string path, ref int lineNumber, string prefix)
        {


            // string space8 = ";;;;;;;;";
            // string space9 = ";;;;;;;;;";

            double decalage = 0.0;
            string operationTerrain = "01";

            prefix = GetStationPrefix(station, prefix);



            string line;


            string observationHeader = "! ###;MT;target.n ;target.sn  ;acc/zone  ;class.numero.point               ;       obs; target.h;  offset;8 empty;X;OT;    Time;             comment;   i1.type;     i1.sn;   i2.type;     i2.sn;   i3.type;     i3.sn;";
            string underlyingHeaders = "!----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------";
            // AH
            bool isFirstIteration = true;
            foreach (var m in list)
            {
                Polar.Measure mf1 = Survey.GetObservationInFace1Type(m);
                if (isFirstIteration)
                {
                    l.Add("");
                    l.Add(observationHeader);
                    l.Add(underlyingHeaders);
                    isFirstIteration = false;
                }
                line = GetHeadedLine(GetAH(mf1, station, decalage, operationTerrain), mf1._Status, ref lineNumber, prefix);
                if (line != null) l.Add(line);
            }

            // AV
            foreach (var m in list)
            {
                Polar.Measure mf1 = Survey.GetObservationInFace1Type(m);
                line = GetHeadedLine(GetAV(mf1, station, decalage, operationTerrain), mf1._Status, ref lineNumber, prefix);
                if (line != null) l.Add(line);
            }

            // DD
            foreach (var m in list)
            {

                Polar.Measure mf1 = Survey.GetObservationInFace1Type(m);
                line = GetHeadedLine(GetDD(mf1, station, decalage, operationTerrain), mf1._Status, ref lineNumber, prefix);
                if (line != null) l.Add(line);
            }

            if (path == "")
            {
                path = GetFileName(station.ParametersBasic);
            }
        }

        // 003;AH;CCR1.5   ;1143L       ;TT41      ;ST.324E.                         ;200.2885; 0.07000; 0.00000;;;;;;;;;R;02;11:05:20;                                                                                ;
        // 004;AV;CCR1.5   ;1143L       ;TT41      ;ST.324E.                         ; 98.4418; 0.07000;;;;;;;;;;R;02;11:05:20;                                                                                ;
        // 005;DD;CCR1.5   ;1143L       ;TT41      ;ST.324E.                         ; 11.029107; 0.07000; 0.00000;;;;;;;;;R;02;11:05:20;    
        public static void VisitCore(Polar.Measure m, Station station, ref List<String> l, ref string path, ref int lineNumber, string prefix)
        {

            // string space8 = ";;;;;;;;";
            // string space9 = ";;;;;;;;;";

            double decalage = 0.0;
            string operationTerrain = "01";

            prefix = GetStationPrefix(station, prefix);


            Polar.Measure mf1 = Survey.GetObservationInFace1Type(m);

            string line;
            // AH
            line = GetHeadedLine(GetAH(mf1, station, decalage, operationTerrain), mf1._Status, ref lineNumber, prefix);
            if (line != null) l.Add(line);
            // AV
            line = GetHeadedLine(GetAV(mf1, station, decalage, operationTerrain), mf1._Status, ref lineNumber, prefix);
            if (line != null) l.Add(line);
            // DD
            line = GetHeadedLine(GetDD(mf1, station, decalage, operationTerrain), mf1._Status, ref lineNumber, prefix);
            if (line != null) l.Add(line);


            if (path == "")
            {
                GetFileName(station.ParametersBasic);
            }
        }

        private static string GetHeadedLine(string obsLine, M.State measureState, ref int lineNumber, string prefix)
        {
            string suffix = " - Was " + measureState._Name + " according to Tsunami";
            bool show = Tsunami2.Preferences.Values.GuiPrefs.ShowCommentedLineInGeodeExport.IsTrue;

            if (measureState is M.States.Good)
            {
                suffix = "";
            }
            else if (measureState is M.States.Control)
            {
                prefix = "% ";
            }
            else if (measureState is M.States.Bad)
            {
                prefix = "% ";
            }
            else
            {
                if (!show) return null;
                if (prefix != "% ")
                    prefix = "! ";
            }

            string lineNum = string.Format("{0:D3}", lineNumber);
            lineNumber++;

            string toReturn = prefix + lineNum + obsLine;

            AddCommentinCommentField(toReturn, suffix);
            return toReturn;


        }

        private static string AddCommentinCommentField(string originalString, string newComment)
        {
            string[] parts = originalString.Split(';');
            string resultString = "";
            if (parts.Length > 19)
            {
                // Insert the string just before the 20th instance
                parts[19] = newComment + parts[19];

                resultString = string.Join(";", parts);
            }
            else
            {
                if (originalString.Substring(originalString.Length - 1, 1) == ";")
                    return originalString.Substring(0, originalString.Length - 1) + newComment + ';';
                else
                    return originalString + newComment;
            }

            return resultString;
        }

        static bool useTheoreticalHeightOfTheBeamInTheExtentionValue = false;

        private static string GetAH(Polar.Measure m, Station station, double decalage, string operationTerrain)
        {
            //AH
            string line = "";



            string role = GetGeodeRole(m, station);

            double extentionValue = m.Extension.Value;

            // selection of the right Extension
            // 'old, not anymore tsu3101, they want back Htarget=Hriton' If an offset was given in the theoretical value Geode require to get it mixed with the real Extension


            if (useTheoreticalHeightOfTheBeamInTheExtentionValue)
            {
                if (m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates != null)
                {
                    if (m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates.Value != Tsunami2.Preferences.Values.na)
                    {
                        extentionValue = m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates.Value + m.Extension.Value;
                    }
                }
            }


            Comments.CompleteCommentFromTsunami(m, station.ClosureMeasure);
            string comment = ReduceCommentLenghtTo150CharForGeode(m.Comment);

            var interfaces = m.Interfaces;

            AddToLine(ref line,
                      "AH",
                      string.Format("{0,-9}", m.Distance.Reflector._Model),
                      string.Format("{0,-11}", m.Distance.Reflector._SerialNumber),
                      GetAccelerator(m._Point, out string NameWithoutZone),
                      string.Format("{0,-33}", NameWithoutZone),
                      string.Format("{0,10}", m.Angles.Raw.Horizontal.Value.ToString("F6", new CultureInfo("en-US"))),
                      string.Format("{0,9}", extentionValue.ToString("F6", new CultureInfo("en-US"))),
                      string.Format("{0,8}", decalage.ToString("F6", new CultureInfo("en-US"))),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0,1}", role),
                      string.Format("{0,2}", operationTerrain),
                      string.Format("{0:H:mm:ss}", m._Date),
                      string.Format("{0}", comment),
                      interfaces.ToString(1, ';'),
                      interfaces.ToString(2, ';'),
                      interfaces.ToString(3, ';')
                      );

            return line;
        }


        private static string GetAccelerator(Point p, out string restOfThename)
        {
            List<string> newNameIn4Parts = Names.Get4AsgParts(p._Name);
            restOfThename = $"{newNameIn4Parts[1]}.{newNameIn4Parts[2]}.{newNameIn4Parts[3]}";
            return string.Format("{0,-10}", newNameIn4Parts[0]);
        }

        private static string GetGeodeRole(Polar.Measure m, Station station)
        {
            // old style{ //if (station.AllReferences.Contains(m)) return "R";//else if (station.AllRays.Contains(m)) return "A"; //return " ";}
            // new style tsu-2334
            switch (m.GeodeRole)
            {
                case Roles.Poin: return "A";
                case Roles.Radi: return "C";
                case Roles.Cala: return "R";
                default:
                    if (m._Status is Control)
                    {
                        // TSU-2809 control should have the same role as the first measurement (radi or cala) in case it is reactivated
                        var firstMeasureOfThisPoint = station.MeasuresTaken.First(
                            x => x._Point._Name == m._Point._Name &&
                            x._Status is Good);
                        if (firstMeasureOfThisPoint is Polar.Measure pm)
                            return GetGeodeRole(pm, station);
                        else // take a guess
                            return "R";
                    }
                    else
                        return " ";
            }
        }

        private static string ReduceCommentLenghtTo150CharForGeode(string comment)
        {
            string nComment = comment;
            if (nComment.Length > 150)
                nComment = nComment.Replace("according to Tsunami", "");
            if (nComment.Length > 150)
                nComment = nComment.Replace(" = ", "=");
            if (nComment.Length > 150)
                nComment = nComment.Replace("meas.", "m.");
            if (nComment.Length > 150)
                nComment = nComment.Replace(", ", ",");

            if (nComment.Length > 150)
                nComment = nComment.Substring(0, 120);

            return nComment;
        }

        private static string GetAV(Polar.Measure m, Station station, double decalage, string operationTerrain)
        {
            //AV
            string line = "";


            string role = GetGeodeRole(m, station);

            // selection of the right Extension
            // If an offset was given in the theoretical value Geode require to get it mixed with the real Extension
            double extentionValue = m.Extension.Value;
            if (m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates != null)
            {
                if (m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates.Value != Tsunami2.Preferences.Values.na)
                {
                    extentionValue = m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates.Value + m.Extension.Value;
                }
            }

            Comments.CompleteCommentFromTsunami(m, station.ClosureMeasure);
            string comment = ReduceCommentLenghtTo150CharForGeode(m.Comment);

            var interfaces = m.Interfaces;

            AddToLine(ref line,
                      "AV",
                      string.Format("{0,-9}", m.Distance.Reflector._Model),
                      string.Format("{0,-11}", m.Distance.Reflector._SerialNumber),
                      GetAccelerator(m._Point, out string NameWithoutZone),
                      string.Format("{0,-33}", NameWithoutZone),
                      string.Format("{0,10}", m.Angles.Raw.Vertical.Value.ToString("F6", new CultureInfo("en-US"))),
                      string.Format("{0,9}", extentionValue.ToString("F6", new CultureInfo("en-US"))),
                      string.Format("{0,8}", decalage.ToString("F6", new CultureInfo("en-US"))),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0,1}", role),
                      string.Format("{0,2}", operationTerrain),
                      string.Format("{0:H:mm:ss}", m._Date),
                      string.Format("{0,20}", comment),


                      interfaces.ToString(1, ';'),
                      interfaces.ToString(2, ';'),
                      interfaces.ToString(3, ';')
                      );
            ;

            return line;
        }

        private static string GetDD(Polar.Measure m, Station station, double decalage, string operationTerrain)
        {
            //AV
            string line = "";



            string role = GetGeodeRole(m, station);

            // selection of the right Extension
            // If an offset was given in the theoretical value Geode require to get it mixed with the real Extension
            double extentionValue = m.Extension.Value;
            if (m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates != null)
            {
                if (m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates.Value != Tsunami2.Preferences.Values.na)
                {
                    extentionValue = m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates.Value + m.Extension.Value;
                }
            }

            Comments.CompleteCommentFromTsunami(m, station.ClosureMeasure);
            string comment = ReduceCommentLenghtTo150CharForGeode(m.Comment);

            var interfaces = m.Interfaces;

            AddToLine(ref line,
                      "DD",
                      string.Format("{0,-9}", m.Distance.Reflector._Model),
                      string.Format("{0,-11}", m.Distance.Reflector._SerialNumber),
                      GetAccelerator(m._Point, out string NameWithoutZone),
                      string.Format("{0,-33}", NameWithoutZone),
                      string.Format("{0,10}", m.Distance.Raw.Value.ToString("F6", new CultureInfo("en-US"))),
                      string.Format("{0,9}", extentionValue.ToString("F6", new CultureInfo("en-US"))),
                      string.Format("{0,8}", decalage.ToString("F6", new CultureInfo("en-US"))),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0}", ""),
                      string.Format("{0,1}", role),
                      string.Format("{0,2}", operationTerrain),
                      string.Format("{0:H:mm:ss}", m._Date),
                      string.Format("{0}", comment),


                      interfaces.ToString(1, ';'),
                      interfaces.ToString(2, ';'),
                      interfaces.ToString(3, ';')
                      );
            return line;
        }



        //040;EC;TT43      ;MBAWH.412343.S                   ;     -0.05;     -0.60;          ;          ;dr dl dh mm;
        //041;EC;TT43      ;MBAWH.412343.E                   ;      0.03;     -0.61;          ;          ;dr dl dh mm;
        //042;EC;TT43      ;MBAWH.430300.E                   ;      0.06;     -0.05;          ;          ;dr dl dh mm;
        //043;EC;TT43      ;MBAWH.430300.S                   ;     -0.03;     -0.07;          ;          ;dr dl dh mm;
        public static void GetOffsets(Polar.Measure m, Station station, ref List<String> l, ref int lineNumber)
        {
            bool mustBeCommented = !(m._Status is M.States.Good);

            bool mustBeAddedIfFile = !(mustBeCommented && Tsunami2.Preferences.Values.GuiPrefs.ShowCommentedLineInGeodeExport.IsFalse);

            // measure needs coordinates in StationCS or Beam Cs, if not, we skip
            if (m._Point._Coordinates == null) return;
            if (!(m._Point._Coordinates.HasBeam && m._Point._Coordinates.HasStationCs)) return;

            // set prefix
            string prefix;
            if (mustBeCommented)
                prefix = "% ";
            else
                prefix = "  ";
            prefix = GetStationPrefix(station, prefix);


            string line = prefix;
            line += string.Format("{0:D3}", lineNumber);
            lineNumber++;

            //EC
            AddToLine(ref line,
                      "EC",
                      GetAccelerator(m._Point, out string NameWithoutZone),
                      string.Format("{0,-33}", NameWithoutZone),
                      (m._Point._Coordinates.HasBeamV) ? string.Format("{0,10}", (m._Point._Coordinates.BeamV.X.Value * 1000).ToString("F2", new CultureInfo("en-US"))) : "          ",
                      (m._Point._Coordinates.HasBeamV) ? string.Format("{0,10}", (m._Point._Coordinates.BeamV.Y.Value * 1000).ToString("F2", new CultureInfo("en-US"))) : "          ",
                      (m._Point._Coordinates.HasStationCs) ? string.Format("{0,10}", (m._Point._Coordinates.BeamV.Z.Value * 1000).ToString("F2", new CultureInfo("en-US"))) : "          ",
                      string.Format("{0}", ""),
                      string.Format("{0}", "dr dl dh mm (offset in Beam-Line CS verticalized)") +
                         ((m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates != null) ?
                          $"(HFT={m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates.ToString("m-mm")}mm, HR={m.Extension.ToString("m-mm")}mm)"
                          : "") +
                      $"(t:{m._Date})");

            if (mustBeCommented) line += "   $ was " + m._Status._Name + " according to Tsunami";
            if (mustBeAddedIfFile)
                l.Add(line);
        }

        public static void GetNewPoints(Station station, ref List<String> l, ref int lineNumber, string prefix)
        {
            try
            {
                List<string> namesAlreadyAdded = new List<string>();

                CloneableList<M.Measure> MeasuresToPlayWith;

                MeasuresToPlayWith = station.MeasuresToExport;

                if (MeasuresToPlayWith.Count == 0)
                    MeasuresToPlayWith = station.MeasuresTaken;


                foreach (var m in MeasuresToPlayWith)
                {
                    string prefix2 = prefix;
                    bool mustBeCommented = !(m._Status is M.States.Good);
                    bool mustBeAddedIfFile = !(mustBeCommented && Tsunami2.Preferences.Values.GuiPrefs.ShowCommentedLineInGeodeExport.IsFalse);


                    if (namesAlreadyAdded.Contains(m._Point._Name)) continue;

                    // of original point have coordinate it is not a new point! (?) NOT TRUE ANYMORE, WE PUT THE NEW AS ORIGINAL IN CASE WHE REPRODUCE THE SAME MEASUREMENT TO GET GOTO OR SHOW DIFFERENCES

                    //if (m._OriginalPoint?.Type != NewPn)
                    //if (m._OriginalPoint == null) continue;
                    ////if (m._OriginalPoint?._Coordinates == null) continue;
                    ////if (m._OriginalPoint._Coordinates.HasAny) continue;
                    //if (m._Point == null) continue;
                    //if (m._Point._Coordinates == null || !m._Point._Coordinates.HasAny) continue;

                    bool originalPointIsFlaggedAsNewPoint = m._OriginalPoint?.Type == Point.Types.NewPoint;

                    if (!originalPointIsFlaggedAsNewPoint)
                        continue;

                    string line = "";
                    if (mustBeCommented)
                        prefix2 += "% ";

                    line += prefix2 + string.Format("{0:D3}", lineNumber);
                    lineNumber++;

                    AddToLine(ref line, ToNpLine(m._Point, m._Date));

                    namesAlreadyAdded.Add(m._Point._Name);
                    if (mustBeCommented) line += "   $ was " + m._Status._Name + " according to " + Globals.AppName;
                    if (mustBeAddedIfFile)
                        l.Add(line);
                }
                if (station.Parameters2.Setups.InitialValues.IsPositionKnown == false && station.Parameters2._StationPoint != null)// then station linre
                {
                    string line = prefix + string.Format("{0:D3}", lineNumber);
                    AddToLine(ref line, ToNpLine(station.Parameters2._StationPoint, station.Parameters2._Date));
                    l.Add(line);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Could not get new points from the station", ex);
            }
        }


        public static string[] ToNpLine(Point p, DateTime date)
        {
            // NP   267;NP;TCC4      ;STL.BTV.                         ;P; 3089.00957; 4217.98592;380.07825; 8;  829.10000;01-mar-2017;15:23:25;;

            Coordinates c;
            {
                if (p._Coordinates.HasCcs)
                {
                    c = p._Coordinates.Ccs;
                }
                else
                {
                    c = p._Coordinates.Local;
                }
            }
            string code = "0";
            string comment = " !!! Code 0 is not supported by GEODE, please change it! (9th fields)";
            if (p.SocketCode != null)
            {
                if (p.SocketCode.Id != "0")
                {
                    code = p.SocketCode.Id;
                    comment = "";
                }
            }

            string[] t = new string[] {
                    "NP",
                    GetAccelerator(p, out string NameWithoutZone),
                    string.Format("{0,-33}",NameWithoutZone),
                    "p",

                    string.Format("{0,-10}",c.X.Value.ToString("F5", new CultureInfo("en-US"))),
                    string.Format("{0,-10}",c.Y.Value.ToString("F5", new CultureInfo("en-US"))),
                    string.Format("{0,-10}",c.Z.Value.ToString("F5", new CultureInfo("en-US"))),
                    string.Format("{0,2}", code),
                    string.Format("{0,10}",Math.Round(p._Parameters.Cumul, 3).ToString("F3", CultureInfo.CreateSpecificCulture("en-US"))),
                    string.Format("{0}",date.ToString("dd-MMM-yyyy", CultureInfo.CreateSpecificCulture("en-US"))),
                    string.Format("{0,-8}",date.ToString("T", CultureInfo.CreateSpecificCulture("fr-FR"))),
                    string.Format("{0}",comment)
                };
            return t;
        }


        # region import

        private const double na = Preferences.Preferences.NotAvailableValueNa;


        public static Point ReadLine(string lineGeodeFormat)
        {
            try
            {

                // Split the geode string
                List<string> lineFields = new List<string>(lineGeodeFormat.Split(';'));
                if (lineFields.Count > 1)
                {
                    // Create point with name been Zone+NameWithPoints
                    //Point p = new Point(lineFields[0].Trim() + "." + lineFields[1].Trim()); bad because we loose the acc/zon from the calss-num-point and a acc can have a '.'
                    Point p = new Point();
                    p._Name = lineFields[0].Trim().ToUpper() + "." + lineFields[1].Trim().ToUpper();
                    p._Accelerator = lineFields[0].Trim();
                    p._Zone = p._Accelerator;
                    p._ClassAndNumero = lineFields[1].Substring(0, lineFields[1].LastIndexOf('.')).Trim();
                    p._Point = lineFields[1].Substring(lineFields[1].LastIndexOf('.') + 1).Trim();

                    p._Parameters.Cumul = T.Conversions.Numbers.ToDouble(lineFields[2], true, na);
                    var c = new Coordinates();
                    c.Set();
                    c.X.ValueFromString(lineFields[3].Trim());
                    c.Y.ValueFromString(lineFields[4].Trim());
                    c.Z.ValueFromString(lineFields[5].Trim());
                    c.Z.ValueFromString(lineFields[6].Trim());
                    p._Coordinates.Ccs = c;

                    p._Parameters.L = T.Conversions.Numbers.ToDouble(lineFields[7].Trim());
                    p._Parameters.Tilt = T.Conversions.Numbers.ToDouble(lineFields[8].Trim());
                    p._Parameters.Slope = T.Conversions.Numbers.ToDouble(lineFields[9].Trim());
                    p._Parameters.GisementFaisceau = T.Conversions.Numbers.ToDouble(lineFields[10].Trim());
                    p.SocketCode = Tsunami2.Preferences.Values.SocketCodes.Find(x => x.Id == lineFields[11].Trim());

                    p.SocketType = T.Conversions.Numbers.ToChar(lineFields[12].Trim());
                    p.Date = T.Conversions.Date.ToDate(lineFields[13].Trim());
                    p.SerialNumber = lineFields[14].Trim();

                    if (lineFields.Count > 17)
                        p.CommentFromUser = lineFields[16];
                    else
                        p.CommentFromUser = "";
                    p.Type = Point.Types.Reference;

                    if (p._Point == "") p.fileElementType = ENUM.ElementType.Pilier;
                    else if (p.SocketType == 'A' | p.SocketType == 'O') p.fileElementType = ENUM.ElementType.Alesage;
                    else if (p.SocketType == 'T') p.fileElementType = ENUM.ElementType.PointATracer;
                    else p.fileElementType = ENUM.ElementType.Point;


                    return p;
                }
                else
                    throw new Exception();
            }
            catch (Exception ex)
            {
                throw new Exception($"Cannot convert from the Geode Line:\r\n{lineGeodeFormat.Replace(';', ':')}\r\n{ex.Message}");

            }
        }
        #endregion


        internal static void Export(string path, List<Point> points)
        {
            if (!AllPointsHaveCcs(points))
                new MessageInput(MessageType.Important, "Geode Export supports Only CCS coordinates, all points must have CCS").Show();

            using (StreamWriter sw = File.AppendText(path))
                WriteFile(sw, points);
        }

        internal static void ExportInNewPointFormat(string path, List<Point> points)
        {
            if (!AllPointsHaveCcs(points))
                new MessageInput(MessageType.Important, "").Show();

            using (StreamWriter sw = File.AppendText(path))
                WriteFileInNewPointFormat(sw, points);
        }

        private static bool AllPointsHaveCcs(List<Point> points)
        {
            foreach (Point x in points)
                if (!x._Coordinates.HasCcs)
                    return false;
            return true;
        }


        public static string ToLineInNewPointFormat(Point p)
        {
            Coordinates c = p._Coordinates.Ccs ?? new Coordinates();

            return GetNpLine(p);
        }

        public static string ToLine(Point point)
        {
            var coords = point._Coordinates;
            Coordinates ccsH = coords.GetCoordinatesInASystemByName("CCS-H") ?? new Coordinates();
            Coordinates ccsZ = coords.GetCoordinatesInASystemByName("CCS-Z") ?? new Coordinates();
            string line = "";
            if (ccsZ.AreKnown && ccsH.AreKnown)
            {
                line += GetAccelerator(point, out string NameWithoutZone) + ";" +
               string.Format("{0,-33}", NameWithoutZone) + ";" +
               String.Format("{0,11}", point._Parameters.Cumul) + ";" +
               String.Format("{0,11}", ccsZ.X.Value.ToString("F6")) + ";" +
               String.Format("{0,11}", ccsZ.Y.Value.ToString("F6")) + ";" +
               String.Format("{0,11}", ccsZ.Z.Value.ToString("F6")) + ";" +
               String.Format("{0,11}", ccsH.Z.Value.ToString("F6")) + ";" +
               String.Format("{0,6}", point._Parameters.L.ToString("F4")) + ";" +
               String.Format("{0,8}", point._Parameters.Tilt.ToString("F5")) + ";" +
               String.Format("{0,9}", point._Parameters.Slope.ToString("F6")) + ";" +
               String.Format("{0,8}", point._Parameters.GisementFaisceau.ToString("F5")) + ";" +
               String.Format("{0,2}", point.SocketCode == null ? "" : point.SocketCode.Id) + ";" +
               String.Format("{0,1}", point.SocketType) + ";" +
               String.Format("{0,11}", point.Date.ToString("dd-MMM-yyyy", CultureInfo.CreateSpecificCulture("en-US"))) + ";" +
               String.Format("{0,20}", point.SerialNumber) + ";" +
               String.Format("{0,0}", "") + ";" +
               String.Format("{0,0}", "");
                line = line.Replace(Tsunami2.Preferences.Values.na.ToString(), "      ");
            }
            string prefix = point.State == Common.Elements.Element.States.Bad ? "%" : "";
            string comment = point.State == Common.Elements.Element.States.Bad ? "%BAD" : "";
            line = prefix + line + comment;
            return line;
        }


        private static void WriteFile(StreamWriter sw, List<Point> points)
        {
            sw.WriteLine(R.T275);
            string group = "";
            foreach (Point item in points)
            {
                if (item._Origin != group)
                {
                    sw.WriteLine($"% Tsunami 'group/origin': {item._Origin}");
                }
                group = item._Origin;
                string s = ToLine(item);
                sw.WriteLine(s);
            }
        }

        private static void WriteFileInNewPointFormat(StreamWriter sw, List<Point> points)
        {
            sw.WriteLine(R.T562);
            string group = "";
            foreach (Point item in points)
            {
                if (item._Origin != group)
                {
                    sw.WriteLine($"! Tsunami 'group/origin': {item._Origin}");
                }
                group = item._Origin;
                string s = GetNpLine(item);
                sw.WriteLine(s);
            }
        }

        /// <summary>
        /// Given a .dat file, this method will create a new clean one without the duplicate sections based oon the detection of the station metadata line.
        /// This will generate a new file in the outputPath which should be the same name as the inputPath, with an increment of NoDuplicate at the end.
        /// </summary>
        /// <param name="inputPath"></param>
        /// <param name="outputPath"></param>
        public static void RemoveDuplicateSectionsGeode(string inputPath, string outputPath)
        {
            Dictionary<string, List<Tuple<int, int>>> stations_blocks = new Dictionary<string, List<Tuple<int, int>>>();

            GetDuplicatedBlocks(ref stations_blocks, inputPath);
            try
            {
                bool firstLineWritten = false;
                using (StreamWriter writer = new StreamWriter(outputPath))
                {
                    foreach (var station in stations_blocks)
                    {
                        var lastBlock = station.Value[station.Value.Count - 1];
                        int startLine = lastBlock.Item1;
                        int endLine = lastBlock.Item2;

                        using (StreamReader reader = new StreamReader(inputPath))
                        {
                            for (int i = 1; i <= endLine; i++)
                            {
                                string line = reader.ReadLine();
                                if (i == 1 && !firstLineWritten)
                                {
                                    writer.WriteLine(line);
                                    firstLineWritten = true;
                                }
                                if (i >= startLine && i <= endLine)
                                {
                                    string pattern = @"ALREADY RECORDED AT [^-]*";
                                    line = Regex.Replace(line, pattern, "");
                                    writer.WriteLine(line);
                                }
                            }
                        }

                    }
                }
                LineReorder(outputPath);

                TSU.Debug.WriteInConsole($"New file {outputPath} generated.");
            }
            catch (IOException exception)
            {
                TSU.Debug.WriteInConsole($"An error occurred: {exception.Message}");
            }
        }

        private static void LineReorder(string filePath)
        {
            //Reordering the line numbers
            string[] lines = File.ReadAllLines(filePath);
            int lineNumber = 1;
            for (int i = 0; i < lines.Length; i++)
            {
                // Match the line number pattern
                Match match = Regex.Match(lines[i], @"^!?%?\s*\d+");
                if (match.Success)
                {
                    string newValue = string.Format("  {0:D3}", lineNumber);
                    if (match.Value.Contains("!"))
                    {
                        newValue = "! " + newValue.TrimStart();
                    }
                    else if (match.Value.Contains("%"))
                    {
                        newValue = "% " + newValue.TrimStart();
                    }
                    lines[i] = lines[i].Replace(match.Value, newValue);
                    lineNumber++;
                }
            }

            // Write the modified lines back to the file with the correct line numbers
            File.WriteAllLines(filePath, lines);
        }

        private static void ReplaceEndLineNumber(ref Dictionary<string, List<Tuple<int, int>>> stationDictionary, string previousMatch, int lineNumber)
        {
            int firstLineNumber = stationDictionary[previousMatch][stationDictionary[previousMatch].Count - 1].Item1;
            stationDictionary[previousMatch].RemoveAt(stationDictionary[previousMatch].Count - 1);
            stationDictionary[previousMatch].Add(Tuple.Create(firstLineNumber, lineNumber - 1));
        }
        private static void GetDuplicatedBlocks(ref Dictionary<string, List<Tuple<int, int>>> stationDictionary, string inputPath)
        {
            string pattern = @"-+ST.*_\d+_\d+_[\w.]+ ";
            int lineNumber = 1;
            string previousMatch = "";
            foreach (string line in File.ReadLines(inputPath))
            {

                // Match the pattern in the line
                Match match = Regex.Match(line, pattern);

                if (match.Success)
                {
                    string matchedStationName = match.Value;
                    if (previousMatch != "")
                    {
                        ReplaceEndLineNumber(ref stationDictionary, previousMatch, lineNumber);
                    }
                    if (!stationDictionary.ContainsKey(matchedStationName))
                    {
                        previousMatch = matchedStationName;
                        stationDictionary[matchedStationName] = new List<Tuple<int, int>> { Tuple.Create(lineNumber, -1) };
                    }
                    else
                    {
                        stationDictionary[matchedStationName].Add(Tuple.Create(lineNumber, -1));
                    }
                }
                lineNumber++;
            }
            ReplaceEndLineNumber(ref stationDictionary, previousMatch, lineNumber);
        }
    }

}
