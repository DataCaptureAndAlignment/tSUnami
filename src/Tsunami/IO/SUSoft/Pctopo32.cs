﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;using R = TSU.Properties.Resources;
using System.Threading.Tasks;

namespace TSU.IO.SUSoft
{
    public static class Pctopo32
    {
        public static void OpenWithLGC2(string inputPath)
        {
            try
            {
                //lance LGC2
                string pathPctopo = getPctopoApplicationPath();
                System.Diagnostics.ProcessStartInfo cmdsi = new System.Diagnostics.ProcessStartInfo(pathPctopo);
                cmdsi.Arguments = " -m -s \"" + inputPath + "\"";

                //Pour la cacher la fenêtre application console lorsque LGC2 tourne
                cmdsi.UseShellExecute = false;
                cmdsi.CreateNoWindow = true;
                System.Diagnostics.Process cmd = System.Diagnostics.Process.Start(cmdsi);
            }
            catch (Exception ex)
            {
                throw new Exception($"{R.T_OPENING_WITH_PCTOPO32_FAILED}:\r\n{ex.Message}", ex);
            }
        }

        private static string getPctopoApplicationPath()
        {
            if (!TSU.Preferences.Preferences.Dependencies.GetByBame("PCTOPO32",  out Common.Dependencies.Application app))
                throw new Exception ("PCTopo32 not found");

            return app.Path;
        }
    }
}
