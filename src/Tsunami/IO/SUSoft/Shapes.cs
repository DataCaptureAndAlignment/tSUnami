﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using R = TSU.Properties.Resources;
using System.Threading.Tasks;
using E = TSU.Common.Elements;
using XL = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;


namespace TSU.IO.SUSoft
{
    public static class Shapes
    {
        // MacroPathTSU.Tsunami2.TsunamiPreferences.Values.Dependencies.Find(x => x._Name.ToUpper() == "SHAPES").ApplicationPathCorrected;
        public static void Launch(List<E.Coordinates> Liste_Points, string folderPath, string macroPath)
        {

            // Create excel file
            string filePath = CreateCoordinatesSheet(Liste_Points, folderPath);

            // Find Excel
            string excelPath = GetExcelApplicationPath();
            if (excelPath == "") throw new Exception("Excel applciation not found.");

           // string sCommande = excelPath + "\"" + GetMacro() + "\" \"" + fullPath + "\"";
                //'Lancement dune nouvelle app excel, ouverture de shapes et du workbook fraicheéent cree
                //    If SHAPES_EXE<> "" Then
                //           sCommande = repExcel & "\excel.exe """ & SHAPES_EXE & """ """ & repCalculs & "\" & sNomFichier & """"
                //            'MsgBox sCommande
                //            Shell sCommande, vbMaximizedFocus
                //    Else
                //            MsgBox "La version de SHAPES recommandée, n'est pas installée sur votre ordinateur, veuillez l'installer pour utiliser cette fonction.", vbCritical, "Alerte CARNET4000"
                //    End If

            System.Diagnostics.ProcessStartInfo cmdsi = new System.Diagnostics.ProcessStartInfo(excelPath);
            cmdsi.CreateNoWindow = false;
            string argMacro = "\"" + macroPath + "\"";
            cmdsi.Arguments += argMacro;

            string argFile = "\"" + filePath + "\"";
            cmdsi.Arguments += argFile;
            // cmdsi.WorkingDirectory = System.IO.Path.GetDirectoryName(dirName);
            System.Diagnostics.Process cmd = System.Diagnostics.Process.Start(cmdsi);


        }

        private static string GetExcelApplicationPath()
        {
            string folder;

            folder = @"C:\Program Files\Microsoft Office";
            if (System.IO.Directory.Exists(folder))
            {
                string[] found = System.IO.Directory.GetFiles(folder, "Excel.exe", System.IO.SearchOption.AllDirectories);
                if (found.Length == 1) return found[0];
            }
            
            folder = @"C:\Program Files (x86)\Microsoft Office";
            if (System.IO.Directory.Exists(folder))
            {
                string[] found = System.IO.Directory.GetFiles(folder, "Excel.exe", System.IO.SearchOption.AllDirectories);
                if (found.Length == 1) return found[0];
            }

            return "";
        }

        private static string CreateCoordinatesSheet(List<E.Coordinates> Liste_Points, string path)
        {
            XL.Application xlApp = new Microsoft.Office.Interop.Excel.Application();

            if (xlApp == null)
            {
                throw new Exception("Excel is not properly installed!!");
            }

           // xlApp.Visible = true;
            XL.Workbook xlWorkBook = null;
            XL.Worksheet xlWorkSheet = null;
            object misValue = System.Reflection.Missing.Value;

            xlWorkBook = xlApp.Workbooks.Add(misValue);
            //xlWorkBook.Sheets.Add();
            xlWorkSheet = (XL.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlWorkSheet.Name = "Co-ordinates";
            // Add Header
            xlWorkSheet.Cells[1, 1] = "Sheet Containing the Co-ordinates of the points coming from Tsunami";
            xlWorkSheet.Cells[2, 1] = "name";
            xlWorkSheet.Cells[2, 2] = "X";
            xlWorkSheet.Cells[2, 3] = "Y";
            xlWorkSheet.Cells[2, 4] = "Z";
            xlWorkSheet.Cells[2, 5] = "Sigma - X";
            xlWorkSheet.Cells[2, 6] = "Sigma - Y";
            xlWorkSheet.Cells[2, 7] = "Sigma - Z";
            xlWorkSheet.Cells[2, 8] = "Weight";

            // Add coordinates
            int row = 2;
            double na =TSU.Tsunami2.Preferences.Values.na;
            foreach (var item in Liste_Points)
            {
                E.Coordinates rightCoordinates = item;
                row++;
                xlWorkSheet.Cells[row, 1] = item._Name;
                xlWorkSheet.Cells[row, 2] = rightCoordinates.X.Value;
                xlWorkSheet.Cells[row, 3] = rightCoordinates.Y.Value;
                xlWorkSheet.Cells[row, 4] = rightCoordinates.Z.Value;
                xlWorkSheet.Cells[row, 5] = (rightCoordinates.X.Sigma != na) ? rightCoordinates.X.Sigma.ToString() : "";
                xlWorkSheet.Cells[row, 6] = (rightCoordinates.Y.Sigma != na) ? rightCoordinates.Y.Sigma.ToString() : "";
                xlWorkSheet.Cells[row, 7] = (rightCoordinates.Z.Sigma != na) ? rightCoordinates.Z.Sigma.ToString() : "";
            }
            xlWorkSheet.Range[xlWorkSheet.Cells[3, 1], xlWorkSheet.Cells[row, 1]].Select();


            string fullPath = path + TSU.Tools.Conversions.Date.ToDateUpSideDown(DateTime.Now) + "_Shapes.xlsx";
            xlWorkBook.SaveAs(fullPath, XL.XlFileFormat.xlWorkbookDefault, misValue, misValue, misValue, misValue, XL.XlSaveAsAccessMode.xlNoChange, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();

            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);

            return fullPath;
        }
        
    }
}
