﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using TSU.Common;
using TSU.Tools;
using static TSU.Tools.Macro;
using R = TSU.Properties.Resources;

namespace TSU
{
    public static class Program
    {
        public static string[] Args;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        [STAThread]
        public static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (args.Length > 0)
            {
                Args = args;
            }

            Application.ThreadException += Application_ThreadException;

            try
            {
                if (Debugger.IsAttached)
                    Debug.EarlyTest();

                //if (!(Debug.IsRunningInATest || Debug.Debugging))
                //    TSU.Debug.MouseAndKeyboardMonitoring_Subscribe(); // must be on only if we want screenshots

                Tsunami tsunami = new Tsunami(null);
                Tsunami2.Properties = tsunami;
                Tsunami.TsunamiExists.Set();

                Splash.Module splashScreenModule = new Splash.Module(tsunami);
                tsunami.View.SplashScreenView = splashScreenModule.View; // this is because the slash view is used to invoke the UI thread from the Module.InvokeOnApplicationDispatcher

                Preferences.Preferences.Instance.tsunami = tsunami;
                //var e = tsunami.MeasurementModules[0].RadialRabot;
                //var f = tsunami.MeasurementModules[0].VerticalRabot;
                //var g = tsunami.MeasurementModules[0].RabotDisplayType;

                Application.Run(splashScreenModule.View);

                var a = Tsunami2.Properties.Points;
                var b = Tsunami2.Properties.TempPoints;
                var c = Tsunami2.Properties.HiddenPoints;
            }

            catch (Exception ex)
            {
                if (Tsunami2.Properties != null && Tsunami2.Properties.View != null)
                    Tsunami2.Properties.View.ShowMessageOfBug("Bug at the MAIN level", ex);
                else
                    MessageBox.Show($"Bug at the MAIN level \r\n {ex}");
            }
            finally
            {
                TSU.Debug.MouseAndKeyboardMonitoring_Unsubscribe();
                TSU.Debug.Screenshots.CreateOrNotAVideoInAThread(out System.Threading.Thread videoThread);
                if (videoThread != null)
                    videoThread.Join(5 * 60 * 1000); // wait 5 minutes max for the video to be created
            }
        }

        private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            // if the innest exception is a TsunamiException it means a message  or log could not be threated because tsunami or its view is null
            if (e.Exception.GetBaseException() is TsunamiException te)
            {
                MessageBox.Show(te.Message + "\r\n" + "\r\n" + te.StackTrace);
                return;
            }

            Views.TsuView v = Tsunami2.Properties?.View;

            if (v == null)
            {
                v = Splash.Module.Instance.View;
            }

            const string jira = "JIRA";
            string r = v.ShowMessageOfBug(
                $"{R.T_SORRY_THERE_IS_AN_UNEXPECTED_PROBLEM};{e.Exception.Message}",
                e.Exception,
                jira);

            if (r == jira)
            {
                string modules = "";
                foreach (var mm in Tsunami2.Properties.MeasurementModules)
                {
                    modules += mm._Name + ", ";
                }
                IO.Jira.ReportBug($"In Tsunami {Tsunami2.Properties.Version} a bug appearred in module {modules} and return the following error: {e.Exception}");
            }
        }
    }





}
