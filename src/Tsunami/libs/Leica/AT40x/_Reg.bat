@echo OFF

set choice=
:start
ECHO.
set /p choice=[I]nfo, [R]egistering, [U]nregistering or [Q]uit ?
if not '%choice%'=='' set choice=%choice:~0,1%
if '%choice%'=='I' goto info
if '%choice%'=='i' goto info
if '%choice%'=='R' goto reg
if '%choice%'=='r' goto reg
if '%choice%'=='U' goto unreg
if '%choice%'=='u' goto unreg
if '%choice%'=='Q' goto end
if '%choice%'=='q' goto end
ECHO "%choice%" is not valid, try again
ECHO.
goto start

:info
ECHO.
ECHO INFO:
ECHO -----
ECHO This should be run as ADMINISTRATOR
ECHO Registering of Atl.dll is a pre-requised for the installation of Leica dll.
ECHO LTControl.dll and LTVideo2.OCX, they should come from the last Leica SDK...
ECHO From LTVideo2.OCX, c# wrappers must be created and should be add as references in visual studio...
ECHO If you chose x64 registering, the visual studio project should be set to x64 only.
ECHO If you chose x86 registering, you should provide information on the machine (x86 or x64).
goto start

:reg
ECHO.
set /p choice=Register the x64 libs [6], the x86 libs on a x86 machine [8] or th x86 libs on a [x64] machine [0] ?
if not '%choice%'=='' set choice=%choice:~0,1%
if '%choice%'=='6' goto reg64
if '%choice%'=='8' goto reg86
if '%choice%'=='0' goto reg86on64
ECHO "%choice%" is not valid, try again
ECHO.
goto reg

:reg64
C:\Windows\SysWOW64\regsvr32.exe %0\..\Atl.dll
C:\Windows\SysWOW64\regsvr32.exe %0\..\LTControl_x64_V3.8rev7.dll
C:\Windows\SysWOW64\regsvr32.exe %0\..\LTVideo2_x64_V3.8rev7.ocx
%0\..\AxImp.exe %0\..\LTVideo2_x64_V3.8rev7.ocx
goto start

:reg86
rem WOW64 stand for 'windows on windows64' it allows to run 32bits app on a 64 machine.
C:\Windows\system32\regsvr32.exe %0\..\Atl.dll
C:\Windows\system32\regsvr32.exe %0\..\LTControl_x86_V3.8rev7.dll
C:\Windows\system32\regsvr32.exe %0\..\LTVideo2_x86_V3.8rev7.ocx
%0\..\AxImp.exe %0\..\LTVideo2_x86_V3.8rev7.ocx
goto start

:reg86on64
rem system32 reste le dossier par defaut de windows qu'il soit 32 ou 64 bit
C:\Windows\SysWOW64\regsvr32.exe %0\..\Atl.dll
C:\Windows\SysWOW64\regsvr32.exe %0\..\LTControl_x86_V3.8rev7.dll
C:\Windows\SysWOW64\regsvr32.exe %0\..\LTVideo2_x86_V3.8rev7.ocx
%0\..\AxImp.exe %0\..\LTVideo2_x86_V3.8rev7.ocx
goto start

:unreg
ECHO.
set /p choice=Unregister the x64 libs [6], the x86 libs on a x86 machine [8] or th x86 libs on a [x64] machine [0] ?
if not '%choice%'=='' set choice=%choice:~0,1%
if '%choice%'=='6' goto unreg64
if '%choice%'=='8' goto unreg86
if '%choice%'=='0' goto unreg86on64
ECHO "%choice%" is not valid, try again
ECHO.
goto unreg

:unreg64
C:\Windows\SysWOW64\regsvr32.exe -u %0\..\Atl.dll
C:\Windows\SysWOW64\regsvr32.exe -u %0\..\LTControl_x64_V3.8rev7.dll
C:\Windows\SysWOW64\regsvr32.exe -u %0\..\LTVideo2_x64_V3.8rev7.ocx
goto start

:unreg86
C:\Windows\system32\regsvr32.exe -u %0\..\Atl.dll
C:\Windows\system32\regsvr32.exe -u %0\..\LTControl_x86_V3.8rev7.dll
C:\Windows\system32\regsvr32.exe -u %0\..\LTVideo2_x86_V3.8rev7.ocx
goto start

:unreg86on64
C:\Windows\SysWOW64\regsvr32.exe -u %0\..\Atl.dll
C:\Windows\SysWOW64\regsvr32.exe -u %0\..\LTControl_x86_V3.8rev7.dll
C:\Windows\SysWOW64\regsvr32.exe -u %0\..\LTVideo2_x86_V3.8rev7.ocx
goto start

:end