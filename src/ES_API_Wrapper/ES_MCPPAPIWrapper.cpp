//
// Copyright (C) Leica Geosystems AG.  All rights reserved.
//
// Filename:    ES_MCPPAPIWrapper.cpp
//
// Description:
//   This is the 'Main Module' for the Managed C++ Wrapper of the emScon API.
//   This module compiles into a library 'ES_MCPP_API_Wrapper.dll'. This DLL 
//   can be referenced from C# application.
//
// Author:      Urs Wigger

// Note: the library 'EmsyMCPPApiWrapper.dll' only needs to be rebuilt when one
// of the header files changes. An already built library is distributed with
// the emScon SDK. This sample just shows how to build this library. 
// C# applications may direclty reference the EmsyMCPPApiWrapper DLL in the
// SDK's 'lib' directory. This sample is intended for documentation only.

//  Include the MCPP header file - that's all!
#include "ES_MCPP_API_Def.h"
