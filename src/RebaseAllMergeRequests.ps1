#!/bin/bash

# Variables
GITLAB_URL="https://gitlab.cern.ch/"       # Replace with your GitLab instance URL
PROJECT_ID="14585"                    # Replace with your project ID
PRIVATE_TOKEN="glpat-aCJsd3EseyqKsMseUDq-"     # Replace with your personal access token

# Fetch all open merge requests
merge_requests=$(curl --silent --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" \
  "${GITLAB_URL}/api/v4/projects/${PROJECT_ID}/merge_requests?state=opened")

# Loop through each merge request
echo "$merge_requests" | jq -c '.[]' | while read -r mr; do
  mr_id=$(echo "$mr" | jq '.iid')
  merge_status=$(echo "$mr" | jq -r '.merge_status')

  if [ "$merge_status" == "can_be_merged" ]; then
    echo "Rebasing merge request #$mr_id..."
    curl --request PUT \
      --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" \
      "${GITLAB_URL}/api/v4/projects/${PROJECT_ID}/merge_requests/${mr_id}/rebase"
  else
    echo "Merge request #$mr_id does not require a rebase or cannot be merged."
  fi
done