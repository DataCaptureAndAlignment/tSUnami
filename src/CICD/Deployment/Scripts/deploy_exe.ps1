param(
	[string] $JobDate,
	[string] $JobTag	
)

# NSI app to make .exe 
$NSIPath = "C:\Program Files (x86)\NSIS\makensis.exe"

# These files will be updated automatically by GitLab (every stage fetch repository of branch which runs pipeline)
$VersionFilePath = "src\Version.nsh"
$DependenciesPath = "src\Tsunami\Preferences\TSUNAMIDependencies.xml"
$NSIFile = @('src\CICD\Deployment\Installer_Builder\tSUnami.nsi')
$InstallVMPath = "src\CICD\Deployment\Installer_Builder\"

# These files will be added from GitLab as artifacts from previous stage of current pipeline
$BinDependenciesPath = "src\Tsunami\bin\Debug\Preferences\TSUNAMIDependencies.xml"

# DFS path to save Tsunami .exe
$DFSInstallFolder = "\\cern.ch\dfs\Support\SurveyingEng\Computing\Software\Install\SUSOFT\Tsunami\"
$DFSTestInstallFolder = "\\cern.ch\dfs\Support\SurveyingEng\Computing\Software\test\tSUnami\Installers\"

# Variables for text color
$esc="$([char]27)" 
$RED="$esc[31m" 
$GREEN="$esc[92m"
$BLUE="$esc[96m"
$WHITE="$esc[0m"
$EXC="$esc[101m"

# Function to define what to show at the begging of the process
function StartMessage()
{
	param([string]$message)	
	Write-Host $BLUE"$message" 
}

# Function to define what to show when error occurs during process
function ErrorMessage()
{
	param([string]$message)
	Write-Host $RED"$message failed" 
}

# Function to define what to show at the end of process
function EndMessage()
{
	param([string]$message)
	Write-Host $GREEN"$message succeed" 		
}

# Function to define what to show at normal process
function NormalMessage()
{
	param([string]$message)
	Write-Host $WHITE"$message"	
}

function ExceptionMessage()
{
	param([string]$message)
	Write-Host $RED"Exception output: "
	Write-Host $EXC"$message" 
	
	exit 1
}


## Read version number and date from NSH file
# $path - path to .NSI file
# Return:
# $version - version number read from .NSI file
# $date  - date of release read from .NSI file
function ReadVersionAndDateFromNSH
{
	param([string] $path)
	try{
		$spaceSep = " "
		$nshFile = Get-Content -Path $path -ErrorAction Stop
		$versionLine = $nshFile | Select-String -Pattern 'VERSION' -CaseSensitive -ErrorAction Stop	
		$version = "9.99.99"
		
		if ($null -eq $versionLine){ 
			ErrorMessage "Finding tag for version number in NSH file"
			$lineBefore = Get-Content -Path $path | Select-String -Pattern '; Change version and date here:' | Select-Object -ExpandProperty Line		
			$lineToAdd = '!define VERSION "9.99.99"'
			$nshFile | ForEach-Object {$_ -replace $lineBefore, "; Change version and date here: `r`n$lineToAdd"} | Set-Content -Path $path -ErrorAction Stop
			NormalMessage "NSH file updated with default tag for version number"					
			}
		elseif ($versionLine.ToString().TrimEnd().Length -lt 16){
			ErrorMessage "Finding version number in NSH file"
			$lineBefore = Get-Content -Path $path | Select-String -Pattern '!define VERSION' | Select-Object -ExpandProperty Line		
			$nshFile | ForEach-Object {$_ -replace $lineBefore, '!define VERSION "9.99.99" '} | Set-Content -Path $path -ErrorAction Stop			
			NormalMessage "NSH file updated with default version number $version"
		}
		else{
			$version = $versionLine.Line.split($spaceSep)[2]			
		}
		
		$nshFile = Get-Content -Path $path -ErrorAction Stop
		$dateLine = $nshFile | Select-String -Pattern 'DATE' -CaseSensitive -ErrorAction Stop
		$date = '2023_01_01'
		
		if ($null -eq $dateLine){ 
			ErrorMessage "Finding tag for date in NSH file"
			$lineBefore = Get-Content -Path $path | Select-String -Pattern '!define VERSION' | Select-Object -ExpandProperty Line		
			$lineToAdd = '!define DATE 2023_01_01'
			$nshFile | ForEach-Object {$_ -replace $lineBefore, "$lineBefore `r`n$lineToAdd"} | Set-Content -Path $path -ErrorAction Stop			
			NormalMessage "NSH file updated with default tag for date"	
			}
		elseif ($dateLine.ToString().TrimEnd().Length -lt 13){
			ErrorMessage "Finding date in NSH file"
			$lineBefore = Get-Content -Path $path | Select-String -Pattern '!define DATE' | Select-Object -ExpandProperty Line		
			$nshFile | ForEach-Object {$_ -replace $lineBefore, '!define DATE 2023_01_01'} | Set-Content -Path $path -ErrorAction Stop
			Write-Output $GREEN"NSH file updated with default version number"			
		}
		else{
			$date = $dateLine.Line.split($spaceSep)[2]
		}
		return $version, $date
	}catch{
		ErrorMessage "Reading NSH file"
		ExceptionMessage $_.Exception
		Exit 1
	}
}

## Return new version number depending on JobTag/JobDate
# $jobTag - tag of job read from GitLab (where there is tag - it is release version)
# $jobDate - date of job read from GitLab (where there is date - it is temporary version)
# Return:
# $newVersion - returns Tsunami version which depends on jobTag/jobDate 
function ChangeVersionNumber()
{
	param([string]$jobTag,[string]$jobDate)
	
	$newVersion = ""
	if ($jobTag -eq ""){
		if ($JobDate -ne ""){
			$tempSplit = $jobDate.Split("+")[0]
			$newVersion = $tempSplit.replace(":", "-")
			NormalMessage "Runner didn't catch job tag. It uses job date: $jobDate"
		}
		else{
			$newVersion = "9.99.99"
			NormalMessage "Runner didn't catch job tag and job date. It uses default one: 9.99.99"
		}
	}else{
		$newVersion = $jobTag
		NormalMessage "Runner catches job tag: $jobTag"
	}
	return $newVersion 	
}

## Return version number depending on GIT tag
function ChangeVersionNumberFromGIT()
{
	#### Old version - tag from git --dirty
	$git_command = 'git describe --dirty'
	$separator = "-"
	$versionFull = Invoke-expression $git_command
	$version = $versionFull.split($separator)
	$newVersion = $version[0]
	return $newVersion			
}

## Return new date of release
# Return:
# $newDate - returns date of release in Tsunami data format (yyyy_mm_dd)
function ChangeReleaseDate()
{
	$newDate = Get-Date -Format "yyyy_MM_dd"
	$newDate = $newDate.ToString()
	NormalMessage "Format of release date: yyyy_mm_dd"
	return $newDate
		
}

## Update version number and date in NSH file 
# $versionFilePath - path to NSH file
# $version - old version number 
# $newVersion - new version number
# $date - old release date
# $newDate - new release date 
# Return:
# $ErrorOccured - true/false - if error occured or not
function UpdateNSHFile()
{
	param([string] $versionFilePath,
		  [string] $version,
		  [string] $newVersion,
		  [string] $date,
		  [string] $newDate
		 )
	$ErrorOccured = $false
	try{
		$nshFile1 = Get-Content -Path $versionFilePath 
		$stringNewVersion = '"' + $newVersion + '"'
		$updateFile = $nshFile1 -replace  $version, $stringNewVersion
		$updateFile | Set-Content -Path $versionFilePath 
		NormalMessage "Version number updated in file."
		
		$nshFile2 = Get-Content -Path $versionFilePath
		$updateFile = $nshFile2 -replace $date, $newDate
		$updateFile | Set-Content -Path $versionFilePath 
		NormalMessage "Release date updated in file."
	}catch {	
		ExceptionMessage $_.Exception
		$ErrorOccured = $true
	}	
	return $ErrorOccured		
}

## Update TSUNAMIdependencies file with new version number, release date and installer path
# $dependenciesPath - path to Tsunami Dependencies file
# $newVersion - new version number
# $newDate - new release date  
# $installPath - path to Tsunami installator 
# Return:
# $ErrorOccured - true/false - if error occured or not

function UpdateDependenciesFile()
{
	param([string] $dependenciesPath,
			[string] $newVersion,
			[string] $newDate, 
			[string] $installPath)	
			
	$ErrorOccured = $false	
	try{
		NormalMessage "TSUnami dependencies file: $dependenciesPath"
		[xml]$xmlElm = Get-Content -Path $dependenciesPath -ErrorAction Stop
		$xmlElm.Application.V = $newVersion
		$xmlElm.Application.StringDate = $newDate
		
		 # Check if InstallerPath contains "{SUTest}"
		 if ($xmlElm.Application.InstallerPath -like '*{SUTest}*') {
            $installPath = $DFSTestInstallFolder  # "\\cern.ch\dfs\Support\SurveyingEng\Computing\Software\test\tSUnami\Installers\"
        } else {
            $installPath = $DFSInstallFolder    # "\\cern.ch\dfs\Support\SurveyingEng\Computing\Software\Install\SUSOFT\Tsunami\"
        }

		$installPath = $installPath + "$newVersion\tSUnami-Installer-$newVersion-win32.exe";
		NormalMessage "Installer path: $installPath"
		$xmlElm.Application.InstallerPath = $installPath
		$xmlElm.Save($dependenciesPath)	
	}catch{
		ExceptionMessage $_.Exception
		$ErrorOccured = $true
	}
	
	return $ErrorOccured
}

## Copy dependencies file to BIN
# $dependenciesPath - path to TSUNAMIDependencies.xml 
# $binDependenciesPath - path to TSUNAMIDependencies.xml in bin folder
# Return:
# $ErrorOccured - true/false - if error occured or not
function CopyUpdatedDependenciesFile()
{
	param ([string] $dependenciesPath,
			[string] $binDependenciesPath)
			
	$ErrorOccured = $false
	try{
		NormalMessage "Path to copy TSUnami dependencies file: $binDependenciesPath"
		Copy-Item $dependenciesPath -Destination $binDependenciesPath -ErrorAction Stop
		
	}catch{		
		ExceptionMessage $_.Exception
		$ErrorOccured = $true
	}
	return $ErrorOccured
	
	
}

## Prepare .exe file 
# $nsiPath - path to NSIS\makensis.exe 
# $nsiFile - path Tsunami NSI file 
# Return:
# $ErrorOccured - true/false - if error occured or not
function MakeEXE()
{
	param([string] $nsiPath,
		  [string] $nsiFile)
		
	$ErrorOccured = $false
	try{
		NormalMessage "NSI use $nsiFile"
		$b = & $nsiPath $nsiFile	
	}catch{
		ExceptionMessage $_.Exception
		$ErrorOccured = $true
	}
	return $ErrorOccured		
}

## Return path to DFS folder to copy after
# $jobTag - job Tag from GitLab
# $dfsInstallFolder - path to DFS installer folder
# $dfsTestInstallFolder - path to DFS test folder
# $newVersion - updated new version
# Return:
# $dfsFolder - path to DFS folder where Tsunami .exe will be copy (install or test folder)
# $folderName - name of new folder which be created for new Tsunami .exe 
#
function FindDFSPath()
{
	param ([string] $jobTag,
			[string] $dfsInstallFolder,
			[string] $dfsTestInstallFolder,
			[string] $newVersion)

	if ($jobTag -ne ""){
		$first = [int]$JobTag.SubString(0,1)
	}else{
		$first = 2
	}
	
	if ($jobTag -ne "" -and $first -lt 2){
		$folderName = "$dfsInstallFolder$newVersion"
		$dfsFolder = $dfsInstallFolder
	}else{
		$folderName = "$dfsTestInstallFolder$newVersion"
		$dfsFolder = $dfsTestInstallFolder
	}
	return $dfsFolder, $folderName
			
}

## Copy exe file from VM to DFS folder
# $folderName - folder name for new Tsunami .exe
# $dfsFolder - path to DFS folder 
# $newVersion - updated new version 
# $installPath - path to Tsunami installator
# Return:
# $ErrorOccured - true/false - if error occured or not
function CopyEXEFile()
{
	param ([string] $folderName, 
			[string] $dfsFolder,
			[string] $newVersion,
			[string] $installPath)
			
	$ErrorOccured = $false
	try{	
		if (Test-Path $folderName){
			Remove-Item $folderName -Recurse -Force -Confirm:$false	-ErrorAction Stop	
			NormalMessage "Tsunami version $newVersion already find in Installer directory. It will be overwritten."
		}	
		New-Item -Path $dfsFolder -Name $newVersion -ItemType directory | Out-Null
		Copy-Item $installPath -Destination $folderName -ErrorAction Stop
		NormalMessage "TSUnami *exe will be copy into: $folderName"
	}catch{
		ExceptionMessage $_.Exception
		$ErrorOccured = $true		
	}
	return $ErrorOccured
}

##### Read Version File and Date #####
$readNSH = "Reading latest version file and date from Version.nsh"
StartMessage $readNSH
NormalMessage "Reading information from $VersionFilePath"
$Version, $Date = ReadVersionAndDateFromNSH($VersionFilePath)
if ($Version -eq ""){
		ErrorMessage "Reading version number"	
}else {
		NormalMessage "Version number: $Version"		
	}
if ($Date -eq ""){
	ErrorMessage "Reading date"
}else {
	NormalMessage "Release date: $Date"
    }
EndMessage "Reading version number and release date"

##### Change Version Number #####
$newVN = "Setting new version number from GitLab commit"	
StartMessage $newVN
$NewVersion = ChangeVersionNumber $JobTag $JobDate
if ($NewVersion -eq ""){
	ErrorMessage $newVN
	Exit 1
} 
NormalMessage "New version number: $newVersion"
EndMessage $newVN

##### Change date of release  #####
$releaseDate = "Setting new release date"
StartMessage $releaseDate
$NewDate = ChangeReleaseDate
if ($NewDate -eq ""){
	ErrorMessage $releaseDate
	Exit 1	
} 
NormalMessage "New release date: $newDate"
EndMessage "$releaseDate"


##### Modify Version File with updated values #####
$updateNSH = "Updating Version.nsh file"
StartMessage $updateNSH
$errorOccured = UpdateNSHFile $VersionFilePath $Version $NewVersion $Date $NewDate
if ($errorOccured -eq $true){
	ErrorMessage $updateNSH
	Exit 1
}
EndMessage $updateNSH

##### Modify dependencies File #####
$modDep = "Updating dependencies file"
StartMessage $modDep
$InstallPath = $DFSTestInstallFolder
$errorOccured = UpdateDependenciesFile $DependenciesPath $NewVersion $NewDate $InstallPath
if ($errorOccured -eq $true){
	ErrorMessage $modDep
	Exit 1
}
EndMessage $modDep

##### Copy dependencies File to bin folder #####
$copyDep = "Copying dependencies file"
StartMessage $copyDep
$errorOccured = CopyUpdatedDependenciesFile $DependenciesPath $BinDependenciesPath
if ($errorOccured -eq $true){
	ErrorMessage $copyDep
	Exit 1   
}
EndMessage $copyDep


##### Run NSIS to make .exe #####
$makeNSIS = "Making .exe with NSIS."
StartMessage $makeNSIS
Write-Output $WHITE"It may take time....."
$errorOccured = MakeEXE $NSIPath $NSIFile
if ($errorOccured -eq $true){
	ErrorMessage $makeNSIS
	Exit 1
}
EndMessage $makeNSIS

##### Copy .exe to Installer or Test directory on DFS #####
$copyExe = "Copying TSUnami installer on DFS"
StartMessage $copyExe
$fileName = "tSUnami-Installer-$newVersion-win32.exe"
NormalMessage "TSUnami installer name: $fileName"
$DfsFolder, $FolderName = FindDFSPath $JobTag $DFSInstallFolder $dfsTestInstallFolder $NewVersion
$path = $InstallVMPath + $fileName
$errorOccured = CopyEXEFile $FolderName $DfsFolder $NewVersion $path

if ($errorOccured -eq $true){
	ErrorMessage $copyExe
	Remove-Item $FolderName -Recurse -Force -Confirm:$false	-ErrorAction Stop
	Exit 1
}
EndMessage $copyExe
Exit 0

