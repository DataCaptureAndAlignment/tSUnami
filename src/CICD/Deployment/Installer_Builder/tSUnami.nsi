﻿!define SOLUTION  "..\..\..\.."
!include /NONFATAL "${SOLUTION}\src\Version.nsh"
!include "x64.nsh"

# Default value in case no version.nsh is present


!ifndef DATE
    !define /date DATE "%Y-%m-%d %H:%M:%S"
!endif
!ifndef VERSION
    !define /string VERSION "00.00.00"
!endif

!define ARCHITECTURE "x86"

!define DESCRIPTION "TSUNAMI"
!define COPYRIGHT "CERN"


!include "LogicLib.nsh" ;pour les if
!include "MUI2.nsh"

!define APPLICATION  			"tSUnami"
!define EXENAME      			"${APPLICATION}.exe"
!define EXENAMEWITHVERSION      "${APPLICATION} ${VERSION}.exe"
!define DEPENDENCIES  			"$PROGRAMFILES64\SUSOFT\SUSoftDependencies.ini"
!define InstallerIcon 			"installer.ico"

Name "${APPLICATION} ${VERSION} from ${DATE}"	
OutFile "${APPLICATION}-Installer-${VERSION}-win32.exe"
InstallDir "$PROGRAMFILES64\SuSoft\${APPLICATION}\${VERSION}"

Icon ${InstallerIcon}
RequestExecutionLevel admin 

!define MUI_FINISHPAGE_RUN 
!define MUI_FINISHPAGE_RUN_TEXT "Launch ${APPLICATION}"
!define MUI_FINISHPAGE_RUN_NOTCHECKED
!define MUI_FINISHPAGE_RUN_FUNCTION "LaunchLink"
!define MUI_INSTFILESPAGE_COLORS "00FF00 000000"
!define MUI_FINISHPAGE_NOAUTOCLOSE
;!define MUI_FINISHPAGE_RUN_CHECKED
!define MUI_ICON ${InstallerIcon}
!define MUI_UNICON ${InstallerIcon}
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH
!insertmacro MUI_LANGUAGE "English"

UninstPage uninstConfirm
UninstPage instfiles



Section "${APPLICATION}"

	SetOverwrite on
	SectionIn RO	;section required lors de l'install
	Call CheckVCRedist
	Call CopyNecessaryFiles
	Call REGISTRATION
	Call SHORTCUTS
	
	Call ETALO_INSTALLATION
	Call RegisterAt40xDll
	Call Give_accese
	Call TS60_TRICK
	WriteUninstaller "uninstall.exe"

SectionEnd 		


Section "Uninstall"
  
  	DeleteRegKey HKLM "SOFTWARE\CERN SUSOFT\${APPLICATION}\${VERSION}"
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPLICATION} ${VERSION}"
	
	
  	Delete "$INSTDIR\*.*"
	RMDir /r "$INSTDIR\"

	SetShellVarContext all
  	Delete "$SMPROGRAMS\SUSOFT\${APPLICATION}\${VERSION}\*.*"
	
  	RMDir /r "$SMPROGRAMS\${APPLICATION}\${VERSION}"
	
	Delete  "$DESKTOP\${APPLICATION} Frequent folders\${APPLICATION} ${VERSION}.lnk"
	Delete  "$DESKTOP\${APPLICATION} ${VERSION}.lnk"
	
	
SectionEnd

;Function .onInit ; this will run at the beginning of the setup
;	IfFileExists $PROGRAMFILES64\SUSOFT\tsunami\* continue firstInstallMessage
;	firstInstallMessage:
;	
;	MessageBox MB_OKCANCEL \
;		"First installation?$\r$\n$\r$\n\
;		\
;		Please make sure that:$\r$\n\
;		G:\Support\SurveyingEng\Computing\Software\Install\Microsoft\x86\vcredist_x86_2015.exe$\r$\n\
;		is installed for SurveyLib to work.$\r$\n$\r$\n\
;		\
;		Please make sure that:$\r$\n\
;		G:\Support\SurveyingEng\Computing\Software\Install\Microsoft\x86\vcredist_x86_2008.exe$\r$\n\
;		is installed for At40x to work.$\r$\n$\r$\n"\
;	IDOK continue IDCANCEL cancel
;	cancel:
;		Abort
;	continue:
;FunctionEnd

Function LaunchLink
  ExecShell "" "$INSTDIR\${EXENAME}"
FunctionEnd

Function RegisterAt40xDll ; this will run at the beginning of the setup
	DetailPrint ">>> Register LT_CONTROL"
	DetailPrint ">>> Registering AT40x DLLs"
	IfFileExists "$INSTDIR\libs\Leica\At40x\atl.dll" 0 +3
	UnRegDLL "$INSTDIR\libs\Leica\At40x\atl.dll"
	RegDLL "$INSTDIR\libs\Leica\At40x\atl.dll"
	IfFileExists   "$INSTDIR\libs\Leica\At40x\LTVideo2_x86_V3.8rev7.ocx" 0 +3
	UnRegDLL "$INSTDIR\libs\Leica\At40x\LTVideo2_x86_V3.8rev7.ocx"
	RegDLL "$INSTDIR\libs\Leica\At40x\LTVideo2_x86_V3.8rev7.ocx"
FunctionEnd	
	
Function TS60_TRICK
  ;TS60: install protocol to be able to acces shared file from the ts60-----
  DetailPrint ">>> ENABLING SMB1 for TS60 communication"
		${If} ${RunningX64}
		   ${DisableX64FSRedirection}
		   DetailPrint ">>> Disabling Windows 64-bit file system redirection"
		${EndIf}
		
		;map un nouveau disque pour pouvoir CD sur le reseau
		ExecWait "@pushd %~dp0"
		;Enable SMB1
		ExecWait "cmd /c Dism /online /Enable-Feature /FeatureName:SMB1Protocol -All /NoRestart"
		;demap le disque cree precedemment
		ExecWait "popd"

		${If} ${RunningX64}
		   ${EnableX64FSRedirection}
		   DetailPrint ">>> Re-enabling Windows 64-bit file system redirection"
		${EndIf}
FunctionEnd

Function CheckVCRedist

	ExecWait '"\\cern.ch\dfs\Support\SurveyingEng\Computing\Software\Install\Microsoft\x86\vcredist_x86_2008.exe" /q'
	ExecWait '"\\cern.ch\dfs\Support\SurveyingEng\Computing\Software\Install\Microsoft\x86\others\vcredist_x86_2010.exe" /passive /norestart'
	ExecWait '"\\cern.ch\dfs\Support\SurveyingEng\Computing\Software\Install\Microsoft\x86\vcredist_x86_2015.exe" /passive /norestart'
	
FunctionEnd
 
Function CopyNecessaryFiles
	;Copy Files: path to the installation directory $INSTDIR=path given in the "InstallDir"
		DetailPrint ">>> Copying files"
		SetOutPath $INSTDIR		
		File /r "${SOLUTION}\src\Tsunami\bin\debug\"	;Copy Files		
		SetOverwrite on
		SetShellVarContext all
		SetFileAttributes $INSTDIR\${EXENAME}	 READONLY
		
		
FunctionEnd
 
Function ETALO_INSTALLATION
		MessageBox MB_YESNO \
		"Etalo.dat, the file containing the instrument calibrations is needed by tsunami.$\r$\n$\r$\n\
		Normally, you should create one by yourself with GEODE to make sure that you are working with the last version of the calibrations. $\r$\n\
		However, tsunami can install an old version with the related Instrum.dat for you if you don't have access to geode.$\r$\n$\r$\n\
		Do you want to install it? (this is not recommended)"\
		IDNO dontInstallEtalo IDYES installEtalo
			installEtalo:
				DetailPrint ">>> Installing Etalo.dat"
				SetOutPath "c:\data\tsunami\preferences\Instruments and Calibrations\"
				File "${SOLUTION}\src\Tsunami\Preferences\Instruments and Calibrations\etalo.dat"	;Copy File	
				File "${SOLUTION}\src\Tsunami\Preferences\Instruments and Calibrations\instrum.dat"	;Copy File
				Goto continue
			dontInstallEtalo:
				DetailPrint ">>> Not installing Etalo.dat"
			continue:
FunctionEnd
  
Function Give_accese
	DetailPrint ">>> Giving acceses"
	;ACCESS: give full acces to tsunami folder so that not only the guy that install tsunami can work there
		AccessControl::GrantOnFile "c:\data\tsunami\preferences\Instruments and Calibrations\" "(BU)" "FullAccess"
		AccessControl::GrantOnFile "c:\data\tsunami\Measures\" "(BU)" "FullAccess"
		AccessControl::GrantOnFile "c:\data\tsunami\Temp\" "(BU)" "FullAccess"
		AccessControl::GrantOnFile "c:\data\tsunami\Temp\Backup" "(BU)" "FullAccess"
		AccessControl::GrantOnFile "c:\data\tsunami\preferences\" "(BU)" "FullAccess"
		AccessControl::GrantOnFile "c:\data\tsunami" "(BU)" "FullAccess"
		AccessControl::GrantOnFile "c:\data" "(BU)" "FullAccess"
		AccessControl::GrantOnFile "c:\temp" "(BU)" "FullAccess"
		AccessControl::GrantOnFile "c:\temp\tsunami" "(BU)" "FullAccess"
FunctionEnd
  
Function SHORTCUTS ;create shorcuts
		DetailPrint ">>> Creating shorcuts"
		CreateDirectory "$SMPROGRAMS\${APPLICATION}\${VERSION}"
		CreateShortCut  "$SMPROGRAMS\${APPLICATION}\${VERSION}\Uninstall ${APPLICATION} ${VERSION}.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
		CreateShortCut  "$SMPROGRAMS\${APPLICATION}\${VERSION}\${APPLICATION} ${VERSION}.lnk" "$INSTDIR\${EXENAME}" "" "$INSTDIR\${EXENAME}" 0
		CreateShortCut  "$SMPROGRAMS\${APPLICATION}\${VERSION}\Data.lnk" "c:\Data\Tsunami" "" "$INSTDIR\${EXENAME}" 0
		CreateShortCut  "$SMPROGRAMS\${APPLICATION}\${VERSION}\Aide.lnk" "$INSTDIR\aide\${APPLICATION}.HLP" "" "$INSTDIR\aide\Aide.ico" 0
		
		CreateShortCut  "c:\data\tsunami\preferences\Common Preferences.lnk" "$INSTDIR\Preferences\" "" $INSTDIR\${EXENAME}" 0
		
		CreateShortCut  "$DESKTOP\${APPLICATION} ${VERSION}.lnk" "$INSTDIR\${EXENAME}" "" $INSTDIR\${EXENAME}" 0
		
		CreateDirectory "$DESKTOP\${APPLICATION} Frequent folders"
		CreateShortCut  "$DESKTOP\${APPLICATION} Frequent folders\${APPLICATION} ${VERSION}.lnk" "$INSTDIR\${EXENAME}" "" $INSTDIR\${EXENAME}" 0
		CreateShortCut  "$DESKTOP\${APPLICATION} Frequent folders\My Preferences.lnk" "c:\data\tsunami\preferences\" "" $INSTDIR\${EXENAME}" 0
		CreateShortCut  "$DESKTOP\${APPLICATION} Frequent folders\Common Preferences.lnk" "$INSTDIR\Preferences\" "" $INSTDIR\${EXENAME}" 0
		CreateShortCut  "$DESKTOP\${APPLICATION} Frequent folders\Measures.lnk" "c:\data\tsunami\Measures\" "" $INSTDIR\${EXENAME}" 0
		CreateShortCut  "$DESKTOP\${APPLICATION} Frequent folders\Theoretical files.lnk" "c:\data\tsunami\Theoretical Files\" "" $INSTDIR\${EXENAME}" 0
FunctionEnd	
  
Function	REGISTRATION
		DetailPrint ">>> Windows registering"
		;APP
			WriteRegStr HKLM "SOFTWARE\CERN SUSOFT\${APPLICATION}\${VERSION}" "Path" "$INSTDIR"
			WriteRegStr HKLM "SOFTWARE\CERN SUSOFT\${APPLICATION}\${VERSION}" "Product Name" "${APPLICATION} ${VERSION}"
			WriteRegStr HKLM "SOFTWARE\CERN SUSOFT\${APPLICATION}\${VERSION}" "Start Menu Folder" "SUSOFT\${APPLICATION}\${VERSION}\"
		;Extentions
			WriteRegStr HKCR ".tsu" "" "Tsunami"
			WriteRegStr HKCR ".tsut" "" "Tsunami"
			WriteRegStr HKCR "Tsunami\shell\open\command" "" '"$INSTDIR\${EXENAME}" "%1"'
		;UNINSTALLER
			WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPLICATION} ${VERSION}"  "DisplayName" "${APPLICATION} ${VERSION}"
			WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPLICATION} ${VERSION}"  "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
			WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPLICATION} ${VERSION}"  "Publisher" "SUSOFT"
			WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPLICATION} ${VERSION}"  "DisplayVersion" "${VERSION}"
			WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPLICATION} ${VERSION}"  "DisplayIcon" "$INSTDIR\graphic\${APPLICATION}.ico"
FunctionEnd	