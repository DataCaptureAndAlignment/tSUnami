﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml;


namespace Installer
{
    static class Program
    {
        static void Main()
        {
            string src = @"..\..\..\..\..";
            string VERSION_FILE = $@"{src}\version.nsh";

            string DEPENDENCIES_FILE = $@"{src}\Tsunami\Preferences\TSUNAMIDependencies.xml";
            string NSIS_SCRIPT = @"..\..\Tsunami.nsi";
            string BIN_INSTALLER = @"..\Installers";

            ReadVersionFile(VERSION_FILE, out string lastVersion, out string lastDate);
            string version = GetVersion(lastVersion);
            string date = GetDate(lastDate, lastVersion);
            ModifyVersionFile(VERSION_FILE, lastDate, date, lastVersion, version);
            ModifyDependenciesFile(DEPENDENCIES_FILE, date, version);
            BuildTsunamiProject();
            RunNsiScript(NSIS_SCRIPT);
            string newSetupDir = MoveArtefacts(NSIS_SCRIPT, BIN_INSTALLER, version);
            OpenSetupFolder(newSetupDir);
        }

        private static void OpenSetupFolder(string newSetupDir)
        {
            Console.WriteLine("Done!");
            System.Diagnostics.Process.Start("explorer.exe", newSetupDir);
            Console.ReadLine();
        }

        private static string MoveArtefacts(string NSIS_SCRIPT, string BIN_INSTALLER, string newVersion)
        {
            string setupDir = new FileInfo(NSIS_SCRIPT).DirectoryName;
            string newSetupDir = $@"{BIN_INSTALLER}\{newVersion}";

            Directory.CreateDirectory(newSetupDir);
            string setupName = $"Tsunami-Installer-{newVersion}-win32.exe";
            if (File.Exists($@"{newSetupDir}\{setupName}"))
            {
                string r = "";
                while (r.ToUpper() != "Y" && r.ToUpper() != "N")
                {
                    Console.WriteLine($"'{$@"{newSetupDir}\{setupName}"}' already exist do you want to delete?");
                    r = Console.ReadLine();
                }
                if (r.ToUpper() == "Y")
                    File.Delete($@"{newSetupDir}\{setupName}");
                else
                    return newSetupDir;
            }
            File.Move($@"{setupDir}\{setupName}", $@"{newSetupDir}\{setupName}");
            return newSetupDir;
        }

        private static void RunNsiScript(string NSIS_SCRIPT)
        {
            Console.WriteLine("Step 6: Will now execute Tsunami.nsi to create the setup.exe (you need NSIS installed) (press 'enter')");
            //Console.ReadLine();
            ProcessStartInfo psi = new ProcessStartInfo(NSIS_SCRIPT);
            Process p = Process.Start(psi);
            p.WaitForExit();
        }

        private static void BuildTsunamiProject()
        {
            Console.WriteLine("Step 5: We have to BUILD the tsunami project NOW to apply the new numbers. To build it press 'enter'.");
            //Console.ReadLine();

            var process = Process.Start(@"C:\Program Files\Microsoft Visual Studio\2022\Community\MSBuild\Current\Bin\MSBUILD.exe", @"C:\PASCAL\tsunami\src\Tsunami\TSUnami.csproj");
            process.WaitForExit();
        }

        private static void ModifyDependenciesFile(string DEPENDENCIES_FILE, string newDate, string newVersion)
        {
            Console.WriteLine($"Step 4: These values will modify the file: '{DEPENDENCIES_FILE}' press 'enter'.");
            
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(DEPENDENCIES_FILE);

            XmlNode node = xmlDoc["Application"];
            node.Attributes["V"].Value = newVersion;
            node.Attributes["StringDate"].Value = newDate;

            //InstallerPath="\\cern.ch\dfs\Support\SurveyingEng\Computing\Software\test\tSUnami\Installers\beta (test versions)\1.02.00_beta12\Setup.exe" 

            string variablePart;
            if (newVersion.ToUpper().Contains("BETA"))
                variablePart = $@"test\tSUnami\Installers\beta(test versions)";
            else if (newVersion.ToUpper().Contains("ALPHA"))
                variablePart = $@"test\tSUnami\Installers\alpha (pre-test versions)";
            else
                variablePart = "Tsunami";

            node.Attributes["InstallerPath"].Value = $@"\\cern.ch\dfs\Support\SurveyingEng\Computing\Software\{variablePart}\{newVersion}\Setup.exe";
            xmlDoc.Save(DEPENDENCIES_FILE);
        }

        private static void ModifyVersionFile(string VERSION_FILE, string date, string newDate, string version, string newVersion)
        {
            // warning
            Console.WriteLine($"Step 3: These values will modify the file: '{VERSION_FILE}' press 'enter'.");
            //Console.ReadLine();
            string text = File.ReadAllText(VERSION_FILE);
            text = text.Replace(date, newDate);
            text = text.Replace(version, newVersion);
            File.WriteAllText(VERSION_FILE, text);
        }

        private static string GetDate(string date, string version)
        {
            // date 
            Console.WriteLine($"Step 2: Version.nsh define '{date}' as the date of the release, enter a new one to change it then press 'enter'");

            string actualDate = $"{DateTime.Now:yyyy_MM_dd}";
            string actualDateWithStamp = $"{actualDate}_{DateTime.Now:HHmm}";

            bool isBetaOrAlpha = version.ToUpper().Contains('A');
            string newDate;
            if (isBetaOrAlpha)
                newDate = actualDateWithStamp;
            else
                newDate = actualDate;

            //Console.Write(newDate);
            string input = Console.ReadLine();
            if (input != "") newDate = input;
            return newDate;
        }

        private const string DefineVersion = "!define VERSION ";
        private const string DefineDate = "!define DATE ";

        private static void ReadVersionFile(string VERSION_FILE, out string version, out string date)
        {
            version = "";
            date = "";
            foreach (var line in File.ReadLines(VERSION_FILE))
            {
                if (TryReadValue(line, DefineVersion, out string versionValue))
                {
                    string[] parts = versionValue.Split('_');
                    if (parts.Length > 1)
                        version = parts[0] + "_" + parts[1];
                    else
                        version = versionValue;
                }
                if (TryReadValue(line, DefineDate, out string dateValue))
                {
                    date = dateValue;
                }
            }

            if (version == "" || date == "")
                Console.WriteLine($"Cannot read version number and date in {VERSION_FILE}");
        }

        private static bool TryReadValue(string line, string header, out string value)
        {
            // if the header is not found, return false
            int found = line.IndexOf(header);
            if (found == -1)
            {
                value = "";
                return false;
            }

            //If the value is between ", return the value inside
            var endOfLine = line.Substring(found + header.Length);
            string[] strings = endOfLine.Split('"');
            if (strings.Length >= 2)
                value = strings[1];
            //else return all the value without the spaces
            else
                value = endOfLine.Trim();

            return true;
        }

        private static string GetVersion(string version)
        {
            // version number
            Console.WriteLine($"Step 1: Version.nsh define '{version}' as the version  number, enter a new one to change it then press 'enter'");
            string newVersion = version;

            //Console.Write(newVersion);
            string input = Console.ReadLine();
            if (input != "") newVersion = input;
            return newVersion;
        }
    }
}
