﻿!define SOLUTION  "..\..\..\.."
!include /NONFATAL "${SOLUTION}\src\Version.nsh"
!include "x64.nsh"

!ifndef DATE
    !define /date DATE "%Y-%m-%d %H:%M:%S"
!endif
!ifndef VERSION
    !define /string VERSION "00.00.00"
!endif

!define ARCHITECTURE "x86"
!define DESCRIPTION "TSUNAMI"
!define COPYRIGHT "CERN"

!include "LogicLib.nsh" ; For if conditions
!include "MUI2.nsh"
!include "nsDialogs.nsh"

!define APPLICATION "tSUnami"
!define EXENAME "${APPLICATION}.exe"
!define EXENAMEWITHVERSION "${APPLICATION} ${VERSION}.exe"
!define DEPENDENCIES "$PROGRAMFILES64\SUSOFT\SUSoftDependencies.ini"
!define InstallerIcon "installer.ico"

Name "${APPLICATION} ${VERSION} from ${DATE}"    
OutFile "${APPLICATION}-Installer-${VERSION}-win32.exe"
InstallDir "$PROGRAMFILES64\SuSoft\${APPLICATION}\${VERSION}"

Icon ${InstallerIcon}
RequestExecutionLevel admin 

!define MUI_FINISHPAGE_RUN 
!define MUI_FINISHPAGE_RUN_TEXT "Launch ${APPLICATION}"
!define MUI_FINISHPAGE_RUN_FUNCTION "Launch"
;!define MUI_FINISHPAGE_RUN_NOTCHECKED

!define MUI_INSTFILESPAGE_COLORS "00FF00 000000"
!define MUI_FINISHPAGE_NOAUTOCLOSE
!define MUI_ICON ${InstallerIcon}
!define MUI_UNICON ${InstallerIcon}
!insertmacro MUI_PAGE_DIRECTORY
Page custom InitCheckBoxPage RetrieveCheckboxStates
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH
!insertmacro MUI_LANGUAGE "English"

UninstPage uninstConfirm
UninstPage instfiles

; Variables to hold checkbox states
Var Dialog

;Var Checkbox_Bin
Var Checkbox_Etalo

;Var Checkbox_Shortcuts
;Var Checkbox_Registration

Var Checkbox_Master

Var Checkbox_Access
Var Checkbox_VCRedist
Var Checkbox_AT40x
Var Checkbox_TS60

;Var Do_Bin
Var Do_Etalo

;ar Do_Access
;Var Do_Shortcuts
;Var Do_Registration

Var Do_Access
Var Do_VCRedist
Var Do_AT40x
Var Do_TS60

; Function to retrieve checkbox states after user interaction
Function RetrieveCheckboxStates
    ;${NSD_GetState} $Checkbox_Bin $Do_Bin
    ${NSD_GetState} $Checkbox_Etalo $Do_Etalo
	
    ;${NSD_GetState} $Checkbox_Registration $Do_Registration
    ;${NSD_GetState} $Checkbox_Shortcuts $Do_Shortcuts
	
    ${NSD_GetState} $Checkbox_Access $Do_Access
    ${NSD_GetState} $Checkbox_VCRedist $Do_VCRedist
    ${NSD_GetState} $Checkbox_AT40x $Do_AT40x
    ${NSD_GetState} $Checkbox_TS60 $Do_TS60
FunctionEnd

Function OnMasterCheckboxClick
    ${NSD_GetState} $Checkbox_Master $0
    StrCmp $0 ${BST_CHECKED} 0 uncheckAll

    ; If master is checked, check all other checkboxes
    ${NSD_Check} $Checkbox_Access
    ${NSD_Check} $Checkbox_VCRedist
    ${NSD_Check} $Checkbox_AT40x
    ${NSD_Check} $Checkbox_TS60
    Goto end

    ; If master is unchecked, uncheck all other checkboxes
    uncheckAll:
    ${NSD_Uncheck} $Checkbox_Access
    ${NSD_Uncheck} $Checkbox_VCRedist
    ${NSD_Uncheck} $Checkbox_AT40x
    ${NSD_Uncheck} $Checkbox_TS60

    end:
FunctionEnd

; Page to show checkboxes for optional steps
Function InitCheckBoxPage
    nsDialogs::Create 1018
    Pop $Dialog
    
    ;${NSD_CreateCheckbox} 20u 0u 100% 12u "Copy Tsunami Necessary Files"
    ;Pop $Checkbox_Bin
    ;${NSD_Check} $Checkbox_Bin

    ${NSD_CreateCheckbox} 20u 15u 100% 12u "Install (probably outdated) 'Etalo.dat' and 'Instrum.dat'"
    Pop $Checkbox_Etalo
    ;${NSD_Check} $Checkbox_InstallEtalo

	;${NSD_CreateCheckbox} 20u 35u 100% 12u "Registration in Windows"
    ;Pop $Checkbox_Registration
    ;${NSD_Check} $Checkbox_Registration
	;
    ;${NSD_CreateCheckbox} 20u 50u 100% 12u "Create Shortcuts"
    ;Pop $Checkbox_Shortcuts
    ;${NSD_Check} $Checkbox_Shortcuts
	;
	
	
	; Master checkbox to control the other three
    ${NSD_CreateCheckbox} 20u 55u 100% 12u "Enable All"
    Pop $Checkbox_Master
    ${NSD_Check} $Checkbox_Master ; Optionally, check it by default
	${NSD_OnClick} $Checkbox_Master OnMasterCheckboxClick
	
	; Set up event handler for the master checkbox
    ;nsDialogs::OnClick $Checkbox_Master OnMasterCheckboxClick
    ; Optionally invoke OnMasterCheckboxClick once to ensure it’s recognized
    ;Call OnMasterCheckboxClick
	
	${NSD_CreateCheckbox} 40u 70u 100% 12u "Give access to necessary folders"
    Pop $Checkbox_Access
    ${NSD_Check} $Checkbox_Access
	
	${NSD_CreateCheckbox} 40u 85u 100% 12u "Install 'Visual C++ Redistributions'"
    Pop $Checkbox_VCRedist
    ${NSD_Check} $Checkbox_VCRedist ; Checked by default
	
	${NSD_CreateCheckbox} 40u 100u 100% 12u "Register 'Leica - AT40x' libraries"
    Pop $Checkbox_AT40x
    ${NSD_Check} $Checkbox_AT40x

	${NSD_CreateCheckbox} 40u 115u 100% 12u "Give access to 'Leica - TS60' memory"
    Pop $Checkbox_TS60
    ${NSD_Check} $Checkbox_TS60

    nsDialogs::Show
FunctionEnd



Section "${APPLICATION}"
    SetOverwrite on
    SectionIn RO
    
    ; Conditional installation steps based on checkboxes
	Call Bin
	Call Etalo
	
    Call Registration
    Call Shorcuts
    Call Access
	
    Call VCRedist
	Call AT40x
	Call TS60
    
    WriteUninstaller "uninstall.exe"
SectionEnd         

Function Bin
	;StrCmp $Do_Bin ${BST_CHECKED} 0 skip_Bin

		DetailPrint ">>> Copying files"
		SetOutPath $INSTDIR
		File /r "${SOLUTION}\src\Tsunami\bin\debug\"
		SetOverwrite on
		SetShellVarContext all
		SetFileAttributes $INSTDIR\${EXENAME} READONLY

		DetailPrint ">>> copy in into c:\data for user to now how to use this folder"
		
		SetOutPath "c:\data\tsunami\preferences\\"
		File "${SOLUTION}\src\Tsunami\Preferences\Read me.txt"
			
		; create temp folder for PLGC and LGC1
		CreateDirectory "C:\TEMP" 
		CreateDirectory "C:\Data\Tsunami\Measures\Templates" 
		
	;skip_Bin:
FunctionEnd

Function Etalo
	StrCmp $Do_Etalo ${BST_CHECKED} 0 skip_Etalo
		DetailPrint ">>> Installing Etalo.dat"
		SetOutPath "c:\data\tsunami\preferences\Instruments and Calibrations\"
		File "${SOLUTION}\src\Tsunami\Preferences\Instruments and Calibrations\etalo.dat"
		File "${SOLUTION}\src\Tsunami\Preferences\Instruments and Calibrations\instrum.dat"
	skip_Etalo:
FunctionEnd


Function Registration
	;StrCmp $Do_Registration ${BST_CHECKED} 0 skip_Registration
		DetailPrint ">>> Performing registration"
		;APP
			WriteRegStr HKLM "SOFTWARE\CERN SUSOFT\${APPLICATION}\${VERSION}" "Path" "$INSTDIR"
			WriteRegStr HKLM "SOFTWARE\CERN SUSOFT\${APPLICATION}\${VERSION}" "Product Name" "${APPLICATION} ${VERSION}"
			WriteRegStr HKLM "SOFTWARE\CERN SUSOFT\${APPLICATION}\${VERSION}" "Start Menu Folder" "SUSOFT\${APPLICATION}\${VERSION}\"
		;Extentions
			WriteRegStr HKCR ".tsu" "" "Tsunami"
			WriteRegStr HKCR ".tsut" "" "Tsunami"
			WriteRegStr HKCR "Tsunami\shell\open\command" "" '"$INSTDIR\${EXENAME}" "%1"'
		;UNINSTALLER
			WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPLICATION} ${VERSION}"  "DisplayName" "${APPLICATION} ${VERSION}"
			WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPLICATION} ${VERSION}"  "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
			WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPLICATION} ${VERSION}"  "Publisher" "SUSOFT"
			WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPLICATION} ${VERSION}"  "DisplayVersion" "${VERSION}"
			WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPLICATION} ${VERSION}"  "DisplayIcon" "$INSTDIR\graphic\${APPLICATION}.ico"
	;skip_Registration:
FunctionEnd

Function Shorcuts
	;StrCmp $Do_Shortcuts ${BST_CHECKED} 0 skip_Shortcuts
		CreateDirectory "$SMPROGRAMS\${APPLICATION}\${VERSION}"
		CreateShortCut  "$SMPROGRAMS\${APPLICATION}\${VERSION}\Uninstall ${APPLICATION} ${VERSION}.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
		CreateShortCut  "$SMPROGRAMS\${APPLICATION}\${VERSION}\${APPLICATION} ${VERSION}.lnk" "$INSTDIR\${EXENAME}" "" "$INSTDIR\${EXENAME}" 0
		CreateShortCut  "$SMPROGRAMS\${APPLICATION}\${VERSION}\Data.lnk" "c:\Data\Tsunami" "" "$INSTDIR\${EXENAME}" 0
		CreateShortCut  "$SMPROGRAMS\${APPLICATION}\${VERSION}\Aide.lnk" "$INSTDIR\aide\${APPLICATION}.HLP" "" "$INSTDIR\aide\Aide.ico" 0
		
		CreateShortCut  "c:\data\tsunami\preferences\Common Preferences.lnk" "$INSTDIR\Preferences\" "" $INSTDIR\${EXENAME}" 0
		
		CreateShortCut  "$DESKTOP\${APPLICATION} ${VERSION}.lnk" "$INSTDIR\${EXENAME}" "" $INSTDIR\${EXENAME}" 0
		
		CreateDirectory "$DESKTOP\${APPLICATION} Frequent folders"
		CreateShortCut  "$DESKTOP\${APPLICATION} Frequent folders\${APPLICATION} ${VERSION}.lnk" "$INSTDIR\${EXENAME}" "" $INSTDIR\${EXENAME}" 0
		CreateShortCut  "$DESKTOP\${APPLICATION} Frequent folders\My Preferences.lnk" "c:\data\tsunami\preferences\" "" $INSTDIR\${EXENAME}" 0
		CreateShortCut  "$DESKTOP\${APPLICATION} Frequent folders\Common Preferences.lnk" "$INSTDIR\Preferences\" "" $INSTDIR\${EXENAME}" 0
		CreateShortCut  "$DESKTOP\${APPLICATION} Frequent folders\Measures.lnk" "c:\data\tsunami\Measures\" "" $INSTDIR\${EXENAME}" 0
		CreateShortCut  "$DESKTOP\${APPLICATION} Frequent folders\Theoretical files.lnk" "c:\data\tsunami\Theoretical Files\" "" $INSTDIR\${EXENAME}" 0
	;skip_Shortcuts:
FunctionEnd

Function Access
	StrCmp $Do_Access ${BST_CHECKED} 0 skip_Access
		DetailPrint ">>> Giving access to 'c:\data\'"
		AccessControl::GrantOnFile "c:\data" "(BU)" "FullAccess"
		
		DetailPrint ">>> Giving access to 'c:\data\tsunami\'"
		AccessControl::GrantOnFile "c:\data\tsunami" "(BU)" "FullAccess"
		
		DetailPrint ">>> Giving access to 'c:\data\tsunami\preferences\'"
		AccessControl::GrantOnFile "c:\data\tsunami\preferences\" "(BU)" "FullAccess"
		
		DetailPrint ">>> Giving access to 'c:\data\tsunami\preferences\Instruments and Calibrations\'"
		AccessControl::GrantOnFile "c:\data\tsunami\preferences\Instruments and Calibrations\" "(BU)" "FullAccess"
		
		DetailPrint ">>> Giving access to 'c:\data\tsunami\Measures\'"
		AccessControl::GrantOnFile "c:\data\tsunami\Measures\" "(BU)" "FullAccess"
		
		DetailPrint ">>> Giving access to 'c:\data\tsunami\Temp\'"
		AccessControl::GrantOnFile "c:\data\tsunami\Temp\" "(BU)" "FullAccess"
		
		DetailPrint ">>> Giving access to 'c:\data\tsunami\Temp\Backup'"
		AccessControl::GrantOnFile "c:\data\tsunami\Temp\Backup" "(BU)" "FullAccess"
		
		DetailPrint ">>> Giving access to 'c:\temp\'"
		AccessControl::GrantOnFile "c:\temp" "(BU)" "FullAccess"
		
		DetailPrint ">>> Giving access to 'c:\temp\tsunami'"
		AccessControl::GrantOnFile "c:\temp\tsunami" "(BU)" "FullAccess"
	skip_Access:
FunctionEnd


Function VCRedist
	StrCmp $Do_VCRedist ${BST_CHECKED} 0 skip_VCRedist
		ExecWait '"\\cern.ch\dfs\Support\SurveyingEng\Computing\Software\Install\Microsoft\x86\vcredist_x86_2008.exe" /q'
		ExecWait '"\\cern.ch\dfs\Support\SurveyingEng\Computing\Software\Install\Microsoft\x86\others\vcredist_x86_2010.exe" /passive /norestart'
		ExecWait '"\\cern.ch\dfs\Support\SurveyingEng\Computing\Software\Install\Microsoft\x86\vcredist_x86_2015.exe" /passive /norestart'
	skip_VCRedist:
FunctionEnd

Function AT40x
	StrCmp $Do_AT40x ${BST_CHECKED} 0 skip_AT40x
		DetailPrint ">>> Register LT_CONTROL"
		DetailPrint ">>> Registering AT40x DLLs"
		IfFileExists "$INSTDIR\libs\Leica\At40x\atl.dll" 0 +3
		UnRegDLL "$INSTDIR\libs\Leica\At40x\atl.dll"
		RegDLL "$INSTDIR\libs\Leica\At40x\atl.dll"
		IfFileExists "$INSTDIR\libs\Leica\At40x\LTVideo2_x86_V3.8rev7.ocx" 0 +3
		UnRegDLL "$INSTDIR\libs\Leica\At40x\LTVideo2_x86_V3.8rev7.ocx"
		RegDLL "$INSTDIR\libs\Leica\At40x\LTVideo2_x86_V3.8rev7.ocx"
	skip_AT40x:
FunctionEnd    

Function TS60
	StrCmp $Do_TS60 ${BST_CHECKED} 0 skip_TS60
	  ;TS60: install protocol to be able to acces shared file from the ts60-----
	  DetailPrint ">>> ENABLING SMB1 for TS60 communication"
		${If} ${RunningX64}
		   ${DisableX64FSRedirection}
		   DetailPrint ">>> Disabling Windows 64-bit file system redirection"
		${EndIf}
		
		;map un nouveau disque pour pouvoir CD sur le reseau
		ExecWait "@pushd %~dp0"
		;Enable SMB1
		ExecWait "cmd /c Dism /online /Enable-Feature /FeatureName:SMB1Protocol -All /NoRestart"
		;demap le disque cree precedemment
		ExecWait "popd"

		${If} ${RunningX64}
		   ${EnableX64FSRedirection}
		   DetailPrint ">>> Re-enabling Windows 64-bit file system redirection"
		${EndIf}
	skip_TS60:
FunctionEnd


Function Launch
    ExecShell "" "$INSTDIR\${EXENAME}"
FunctionEnd

Section "Uninstall"
    DeleteRegKey HKLM "SOFTWARE\CERN SUSOFT\${APPLICATION}\${VERSION}"
    DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPLICATION} ${VERSION}"
    
	Delete "$INSTDIR\*.*"
    RMDir /r "$INSTDIR\"
	
    SetShellVarContext all
    Delete "$SMPROGRAMS\SUSOFT\${APPLICATION}\${VERSION}\*.*"
	
    RMDir /r "$SMPROGRAMS\${APPLICATION}\${VERSION}"
    Delete "$DESKTOP\${APPLICATION} Frequent folders\${APPLICATION} ${VERSION}.lnk"
    Delete "$DESKTOP\${APPLICATION} ${VERSION}.lnk"
SectionEnd
