# Script to run unit test and upload test results reports in .XML to GitLab

# Global variables for command and options
$global:vstest = "C:\Program Files\Microsoft Visual Studio\2022\Community\Common7\IDE\CommonExtensions\Microsoft\TestWindow\vstest.console.exe"
$global:options = "/logger:""junit;verbosity=minimal"" /PlatForm:x86"

# Path to files 
$global:pathToXMLReports = "TestResults\TestResults.xml"
$unitTestDLL = @('src\CICD\Test\Projects\Unit\bin\Debug\Unit.dll')
$pathToCopyXMLReports = "TestResults\IntegrationTestsResults"

# Variables for text color
$esc="$([char]27)" 
$RED="$esc[31m" 
$GREEN="$esc[92m"
$BLUE="$esc[96m"
$WHITE="$esc[0m"
$EXC="$esc[101m"

# Function to define what to show at the begging of the process
function StartMessage()
{
	param([string]$message)	
	Write-Host $BLUE"$message" 
}

# Function to define what to show when error occurs during process
function ErrorMessage()
{
	param([string]$message)
	Write-Host $RED"$message failed" 
}

# Function to define what to show at the end of successful process
function EndMessage()
{
	param([string]$message)
	Write-Host $GREEN"$message succeed" 		
}

# Function to define what to show at normal process
function NormalMessage()
{
	param([string]$message)
	Write-Host $WHITE"$message"	
}

# Function to show exception
function ExceptionMessage()
{
	param([string]$message)
	Write-Host $RED"Exception output: "
	Write-Host $EXC"$message" 
	
	exit 1
}

# Copy xml reports from default directory to new directory
# $pathToCopyXMLReports - path where XML should be copy (to avoid overwriting)
# $pathToXMLReports - default path for XML report in GitLab Runner 
# Return:
# $ErrorOccured - true/false - if error occured or not
function CopyXMLReports()
{
	param([string]$pathToCopyXMLReports)
	
	$ErrorOccured = $false
	try{	
		NormalMessage "Default GitLab Runner directory for XML report is: $pathToXMLReports"			
		if (!(Test-Path $pathToCopyXMLReports)){ 
			New-Item $pathToCopyXMLReports -ItemType Directory | Out-Null 
			NormalMessage "New directory was created: $pathToCopyXMLReports"
		}										
		Copy-Item $pathToXMLReports -Destination $pathToCopyXMLReports"\IntegrationTestsResults.xml" -ErrorAction Stop
		NormalMessage "XML report copied into: $pathToCopyXMLReports"
	}catch{
		ExceptionMessage $_.Exception
		$ErrorOccured = $true
	}
	return $ErrorOccured	
}

#Check number of failures in all tests
# $XMLReportPath - path to XML reporst created during the running the tests
# Return:
# failureNumber - number of failures counted in the XML reports
# $ErrorOccured - true/false - if error occured or not
function CheckFailuresInTests()
{	
	param([string]$XMLReportPath)

	$ErrorOccured = $false	
	try{
		$failureNumber = Select-Xml -Path $XMLReportPath -XPath 'testsuites/ testsuite' | ForEach-Object { $_.Node.failures }-ErrorAction Stop
		NormalMessage "$failureNumber errors found in XML reports"
	}catch{	
		ExceptionMessage $_.Exception
		$ErrorOccured = $true
	}
	return $failureNumber, $ErrorOccured	
}

# Run vstest
$test = "Running unit tests"
$ErrorTest = $false
StartMessage $test
NormalMessage ""
try{
	& $vstest $unitTestDLL $options
}catch{	
	ExceptionMessage $_.Exception
	$ErrorTest = $true
}

if ($error -eq $true){
	ErrorMessage $test
	ExceptionMessage "Unit tests sometimes failed. Still under construction... "
	Exit 1
}else{
	NormalMessage "IntegrationTestsResults.xml was created"
	EndMessage $test
}


# Copy TestResults from default directory to another path, to avoid overwritting XML report in next job
$copy = "Copying XML report"
StartMessage $copy
$error = CopyXMLReports $pathToCopyXMLReports
if ($error -eq $true){
	ErrorMessage $copy
	Exit 1
}
EndMessage $copy

# Read XML tests report to find number of failures
# GitLab Runner is not checking if there are failures in tests. 
# GitLab Runner fails only when it is an error

$fail = "Checking test failures in XML report"
StartMessage $fail
$failureNumber, $error = CheckFailuresInTests $pathToCopyXMLReports"\IntegrationTestsResults.xml"
if ($error -eq $true){
	ErrorMessage $fail
	Exit 1
}
EndMessage $fail

# Exit code depend on the number of failures in tests
# Exit codes - 0 - success, 1 - failure
$unit = "Unit tests"
if($failureNumber -eq 0){
	EndMessage $unit
	Exit 0	
}
else{	
	ErrorMessage $unit
	Exit 1
}



##################################################################################################################
##### Funcionality for run tests one by one - slow but more safe ######
# Function to show result of test 
function TestMessage()
{
	param([string]$message)
	$lines = $output.Split([System.Environment]::NewLine,[System.StringSplitOptions]::RemoveEmptyEntries)
	foreach ($line in $lines){
		Write-Host $WHITE"$line"	
	}	
}

# Function to show if test passed/failed
function ResultMessage()
{
	param([string]$message, [string]$testName)
	if ($output -contains "Test Run Successful."){
		EndMessage "$testName test"			
	}else{
		ErrorMessage "$testName test"	
	}
}

# Run tests one by one using vstest.console.exe with checking errors
function RunTest()
{
	param([string]$unitTestDLL)
	$allTests = & $vstest $unitTestDLL $options /ListTests:$unitTestDLL
	
	$ErrorOccured = $false
	$testNumber = 1
	try{
		foreach ($test in $allTests)
		{					
			if($test -match '\s{3,}(?<TestName>.+)'){
				$testName = $Matches.TestName
				$criteria = "/Tests:" + $testName					
				StartMessage "Starting $testName test" 
				$output = & $vstest $unitTestDLL $options $criteria 
				TestMessage $output
				CopyXMLReports $pathToCopyXMLReports $pathToXMLReports $testNumber
				$testNumber++
				NormalMessage "XML report created"
				ResultMessage $output "$testName"			
			}							
		}
	}catch{	
		ExceptionMessage $_.Exception
		$ErrorOccured = $true
		Exit 1
	}
	return $ErrorOccured
}
##################################################################################################################



