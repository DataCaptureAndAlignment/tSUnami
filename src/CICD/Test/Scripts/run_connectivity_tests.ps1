param(
     [Parameter()]
     [string]$instrumentTag
 )

 # Variables for text color
$esc="$([char]27)" 
$RED="$esc[31m" 
$GREEN="$esc[92m"
$BLUE="$esc[96m"
$WHITE="$esc[0m"
$EXC="$esc[101m"

 if ($instrumentTag -eq "") {
    Write-Host $RED"No instrument S/N provided in the GIT TAG message. the 'instrumentsForTests.txt' from the repo will be used".
} else{
	Write-Host $GREEN$instrumentTag" will be used".
}


# Script to run instrument test and upload test results reports in .XML to GitLab
# Global variables for command and options
$global:vstest = "C:\Program Files\Microsoft Visual Studio\2022\Community\Common7\IDE\CommonExtensions\Microsoft\TestWindow\vstest.console.exe"
$global:options = "/logger:""junit;verbosity=minimal"" /PlatForm:x86"

# Path to files
$global:pathToXMLReports = "TestResults\TestResults.xml"
$connectivityTestDLL = @('src\CICD\Test\Projects\Connectivity\bin\Debug\Connectivity.dll')
$pathToCopyXMLReports = "TestResults\ConnectivityTestResults"



# Function to define what to show at the begging of the process
function StartMessage()
{
	param([string]$message)	
	Write-Host $BLUE"$message" 
}

# Function to define what to show when error occurs during process
function ErrorMessage()
{
	param([string]$message)
	Write-Host $RED"$message failed" 
}

# Function to define what to show at the end of process
function EndMessage()
{
	param([string]$message)
	Write-Host $GREEN"$message succeed" 		
}

# Function to define what to show at normal process
function NormalMessage()
{
	param([string]$message)
	Write-Host $WHITE"$message"	
}

function ExceptionMessage()
{
	param([string]$message)
	Write-Host $RED"Exception output: "
	Write-Host $EXC"$message" 
	exit 1
}

# Copy xml reports from default directory to new directory
# $pathToCopyXMLReports - path where XML should be copy (to avoid overwriting)
# $pathToXMLReports - default path for XML report in GitLab Runner 
# Return:
# $ErrorOccured - true/false - if error occured or not
function CopyXMLReports()
{
	param([string]$pathToCopyXMLReports)
	
	$ErrorOccured = $false
	try{				
		if (!(Test-Path $pathToCopyXMLReports)){ 
			New-Item $pathToCopyXMLReports -ItemType Directory | Out-Null 
		}
		NormalMessage "XML report copied into: $pathToCopyXMLReports"
		Copy-Item $pathToXMLReports -Destination $pathToCopyXMLReports"\ConnectivityTestResults.xml" -ErrorAction Stop				
	}catch{
		ExceptionMessage $_.Exception		
		$ErrorOccured = $true	
	}
	return $ErrorOccured	
}

# Check number of failures in tests
# $XMLReportPath - path to XML reporst created during the running the tests
# Return:
# failureNumber - number of failures counted in the XML reports
# $ErrorOccured - true/false - if error occured or not
function CheckFailuresInTests()
{	
	param([string]$XMLReportPath)

	$ErrorOccured = $false
	try{
		$failureNumber = Select-Xml -Path $XMLReportPath -XPath 'testsuites/ testsuite' | ForEach-Object { $_.Node.failures} -ErrorAction Stop
		NormalMessage "$failureNumber errors found in XML reports"
	}catch{
		ExceptionMessage $_.Exception	
		$ErrorOccured = $true
	}
	return $failureNumber, $ErrorOccured
	
}

if ($instrumentTag -ne "") {
	# Save tag with info about instrument S/N for connectivity test
	$save = "Saving instruments tags for run tests"
	StartMessage $save
	Write-Output $WHITE""
	Write-Host $BLUE$instrumentTag

	$instrumentsForTests = "src\CICD\Test\Projects\Connectivity\bin\Debug\Preferences\Instruments and Calibrations\instrumentsForTests.txt"
	$instrumentTag | Out-File $$instrumentsForTests
	if (Test-Path $$instrumentsForTests) {
		EndMessage $save
	} else {
		ErrorMessage $save
	}
}


# Run vstest
$test = "Running Connectivity Tests"
$ErrorTest = $false
StartMessage $test

try
{	
	$allTests = & $vstest $connectivityTestDLL $options 
	$testNumber = 0

	$global:testTotalCount = $allTests.Count - 4
		
	if ($global:testTotalCount -lt 1){
		Write-Host $Red"No tests found!"
		Exit 1
	}
}
catch
{
	ExceptionMessage $_.Exception	
}


# Copy TestResults from default directory to another path, to avoid overwritting report
$copy = "Copying XML report"
StartMessage $copy
$error = CopyXMLReports $pathToCopyXMLReports $pathToXMLReports
if ($error -eq $true){
	ErrorMessage $copy
	Exit 1
}
EndMessage $copy

# Read XML tests report to find number of failures
# GitLab Runner is not checking if there are failures in tests. 
# GitLab Runner fails only when it is an error

$check = "Checking test failures in XML report"
StartMessage $check
$failureNumber, $error = CheckFailuresInTests $pathToCopyXMLReports"\ConnectivityTestResults.xml"
if ($error -eq $true){
	ErrorMessage $check
	Exit 1
}
EndMessage $check


# Exit code depend on the number of failures in tests
# Exit codes - 0 - success, 1 - failure
$instr = "Instrument tests"
if($failureNumber -eq 0){
	EndMessage $instr
	Exit 0	
}
else{	
	ErrorMessage $instr
	Exit 1
}
