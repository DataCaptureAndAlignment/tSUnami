# Script to run functional test and upload separate test results reports in .XML to GitLab

# Global variables for command and options
$global:vstest_path = "C:\Program Files\Microsoft Visual Studio\2022\Community\Common7\IDE\CommonExtensions\Microsoft\TestWindow\vstest.console.exe"
$global:options = "/logger:""junit;verbosity=minimal"" /PlatForm:x86"
$global:testTotalCount = 0
$global:testFailureCount = 0
$global:testNotRunnedCount = 0

# Path to files
$global:pathToXMLReports = "TestResults\TestResults.xml"
$functionalTestDLL = @('src\CICD\Test\Projects\Functional\bin\Debug\Functional_Tests.dll')
$pathToCopyXMLReports = "TestResults\FunctionalTestResults"

# Variables for text color
$esc="$([char]27)" 
$RED="$esc[31m" 
$GREEN="$esc[92m"
$BLUE="$esc[96m"
$BACKBLUE="$esc[44m"
$WHITE="$esc[0m"
$YELLOW="$esc[33m"
$PURPLE="$esc[35m"
$CYAN="$esc[36m"
$EXC="$esc[101m"
$EXC="$esc[101m"

# Function to define what to show at the begging of the process
function StartMessage()
{
	param([string]$message)	
	Write-Host $BLUE"$message" 
}

# Function to define what to show when error occurs during process
function ErrorMessage()
{
	param([string]$message)
	Write-Host $RED"$message failed" 
}

# Function to define what to show at the end of process
function SuccessMessage()
{
	param([string]$message)
	Write-Host $GREEN"$message succeed" 		
}

# Function to define what to show at normal process
function NormalMessage()
{
	param([string]$message)
	Write-Host $WHITE"$message"	
}

function ExceptionMessage()
{
	param([string]$message)
	Write-Host $RED"Exception output: "
	Write-Host $EXC"$message" 
	
	exit 1
}





# Copy xml reports from default directory to new directory
# $pathToCopyXMLReports - path where XML should be copy (to avoid overwriting)
# $pathToXMLReports - default path for XML report in GitLab Runner 
# $testNumber - number of test which is already runs
# Return:
# $ErrorOccured - true/false - if error occured or not
function CopyXMLReports()
{
	param([string]$pathToCopyXMLReports, [int]$testNumber)
		
	$ErrorOccured = $false
	try{				
		if (!(Test-Path $pathToCopyXMLReports)){ 
			New-Item $pathToCopyXMLReports -ItemType Directory | Out-Null 
		}
		
		$nameTestResults = "\FunctionalTestResults"+$testNumber+".xml"
		Copy-Item $pathToXMLReports -Destination $pathToCopyXMLReports$nameTestResults -ErrorAction Stop		
		Write-Host $YELLOW "Tests results copied into: $nameTestResults" 		
	}catch{
		ExceptionMessage $_.Exception
		$ErrorOccured = $true
	}
	return $ErrorOccured	
}

# Run test using vstest.console.exe
# $functionalTestDLL - path to *DLL file with functional test (created during the build process)
# $pathToCopyXMLReports - path where XML should be copy (to avoid overwriting)
# $pathToXMLReports - default path for XML report in GitLab Runner 
# $testNumber - number of test which is already runs
# Return:
# $ErrorOccured - true/false - if error occured or not

function RunTest()
{
	param(	[string]$testDLL,
			[string]$pathXMLReports)

	try{	
		$return = & $vstest_path $testDLL $options /ListTests:$testDLL
		$testNumber = 0
		$allTests = $return[4..($return.Length - 1)]
		$global:testTotalCount = $allTests.Count
		
		if ($global:testTotalCount -lt 1){
			Write-Host $Red"No tests found!"
			Exit 1
		}
		$sortedTests = $alltests | Sort-Object 

		Write-Host $Yellow$sortedTests

		foreach ($test in $sortedTests)
		{
			if($test -match '\s{3,}(?<TestName>.+)') {
				
				$testNumber++
				$criteria = "/Tests:" + $Matches.TestName
				$testName = "$BLUE Testing $testNumber/$testTotalCount : $($Matches.TestName)"
				$fullOutputAsList = & $vstest_path $testDLL $options $criteria 
				# remove 5 first useless lines
				$newStringList = $fullOutputAsList[5..($fullOutputAsList.Count - 1)]
				# trasnforme into a single string
				$multiLineFullOutput = $newStringList -join "`r`n"
				# Remove the long trace Split the string based on the ignore text and take the first part
				$reducedString = $multiLineFullOutput -replace '(?s)Stack Trace:.*$', ''
				# check if the text contains "Failed"
				$bContainsFailed = $reducedString -imatch "Failed"
				$bContainsMissingArtefact = $reducedString -imatch "missing artefact"
				$bContainsPassed = $reducedString -imatch "Passed"

				if ($bContainsMissingArtefact){
					Write-Host $testName$YELLOW" NOT RUNNED"$WHITE="$esc[0m"
					$global:testNotRunnedCount++
				} elseif ($bContainsFailed) {
					Write-Host $testName
					ErrorMessage $reducedString
					CopyXMLReports $pathXMLReports $testNumber
					$global:testFailureCount++
				} elseif ($bContainsPassed){
					Write-Host $testName$GREEN" OK"$WHITE="$esc[0m"
				}
			}
		}
	}catch{
		ExceptionMessage $_.Exception	
	}
}

# Run vstest
$test = "Running functional tests one by one with run_functional_tests.ps1"

Write-Host $BLUE $functionalTestDLL "started"

#$error = RunTest $functionalTestDLL $pathToCopyXMLReports 
#if ($error -eq $true){
#	ErrorMessage $test
#	Exit 1
#}
#SuccessMessage $test

try
{			
	$dummy = RunTest $functionalTestDLL $pathToCopyXMLReports 

	Write-Host $Blue"Finally:"
	if ($testFailureCount -eq 0){
		Write-Host $GREEN"All good!"
		Exit 0
	}else{
		
		Write-Host $GREEN""($global:testTotalCount-$global:testFailureCount-$global:testNotRunnedCount)"/"$global:testTotalCount "test(s) passed" 
		Write-Host $YELLOW""($global:testNotRunnedCount)"/"$global:testTotalCount "test(s) not runned" 
		Write-Host $RED""($global:testFailureCount)"/"$global:testTotalCount "test(s) failed" 
		Exit 1
	}
}
catch
{
	ExceptionMessage $_.Exception	
	Exit 1
}