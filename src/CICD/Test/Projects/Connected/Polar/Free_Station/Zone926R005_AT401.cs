﻿using Connected_Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Functional_Tests;
using TSU;
using I = TSU.Common.Instruments;
using TSU.Polar;
using System.Linq;
using System.Threading;
using D = TSU.Common.Instruments.Device;
using TSU.Connectivity_Tests.Instruments.Device.AT40x.Tests;
using ManagedNativeWifi;
using System.Text.RegularExpressions;
using TSU.Common.Instruments;
using System.Configuration;

namespace Connected_Tests.Polar.Free_Station
{
    [TestClass]
    public class Zone926R005_AT401
    {
        [TestInitialize()]
        public void ConnectedInit()
        {
            Functional_Tests.Tools.Artefact.RelativeArtefactFolderPath = "Connected//Artefacts//";

        }

        static D.AT40x.Module moduleAT
        {
            get
            {
                return ConnectionTests.moduleAt;
            }
        }


        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            //ConnectToAvailableSsidOfGivenModel("AT40");
        }

        private static void ConnectToAvailableSsidOfGivenModel(string model, bool renewIP = true)
        {
            try
            {
                TSU.Preferences.Preferences.Instance.InitializeWithAllXmlParametersFiles();

                bool ssidFound = false;
                int trialCount = 0;
                List<string> ssids4AT40xs = null;
                while (!ssidFound && trialCount < 4)
                {
                    ssidFound = WiFi.GetAvailableSsids(model, out ssids4AT40xs, out List<Instrument> instrumets);
                    if (!ssidFound) { System.Threading.Thread.Sleep(2 * 1000); }
                    trialCount++;
                }
                if (!ssidFound)
                    throw new Exception($"No SSID available for {model}");

                TSU.WiFi.Connect(ssids4AT40xs[0], test: true);
                if (renewIP)
                {
                    var wifiInterfaces = WiFi.GetWifiInteraceWithNetsh();
                    var wifiInterface = wifiInterfaces.FirstOrDefault();
                    TSU.WiFi.RenewIpConfig(wifiInterface.InterfaceName);
                }
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        public static void CreateTheoFile()
        {
            string content = @"  926R005.REF.001.       4470.354881  5006.861962   331.790800
926R005.REF.002.       4470.722578  5005.153284   331.554672
926R005.REF.003.       4474.034842  5005.993100   331.619608
926R005.REF.004.       4473.781488  5007.038613   331.740976
926R005.REF.005.       4473.736749  5007.216566   331.735140
";
            File.WriteAllText($@"c:\Data\tsunami\theoretical files\926-R005-ccsH.txt", content);
        }



        const string FUNCTIONAL_ARTEFACTS = @"../../../Functional/bin/debug/Functional_Tests/Artefacts/";

        //private static I.Sensor GetTestInstrument()
        //{
        //    var current = Directory.GetCurrentDirectory();
        //    string filePath = Path.Combine(current, @"Preferences\Instruments and Calibrations\instrumentsForTests.txt");

        //    using (StreamReader reader = new StreamReader(filePath))
        //    {
        //        // Read the first line of the file
        //        string firstLine = reader.ReadLine();
        //        string[] numbers = firstLine.Split(' ');
        //        string at40xNumber = numbers[0];
        //        return Tsunami2.Preferences.Values.Instruments.Find(x => x._SerialNumber == at40xNumber) as I.Sensor;
        //    }
        //}


        [Priority(0)]
        [TestMethod()]
        public void L2_P_FS_AT40X_Step00_Get_instrument_behind_SSID_Test()
        {
            CreateTheoFile();
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {

                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>

  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");

                TSU.Common.Instruments.Manager.Module.GetAvailableSsids("AT40", out var ssids, out var instruments);

                Assert.IsTrue(instruments.Count > 0, "No instrument ssid found");
                Tools.Artefact.CreateArtefact(instruments[0]._Name, $"{testMethod.DeclaringType}.instrument.txt");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

            });
        }

        /// <summary>
        /// keywords: AT401 AT402 AT403 CCS dat file init
        /// </summary>
        [Priority(1)]
        [TestMethod()]
        public void L2_P_FS_AT40X_Step01_new_AT_station_instrument_selection_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            var instrumentName = File.ReadAllText(Tools.Artefact.GetArtefactFullPath($"{testMethod.DeclaringType}.instrument.txt")).Replace("\r\n", "") ;
            CreateTheoFile();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_P_FS_AT40X_Step00_Get_instrument_behind_SSID_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""229"" trace=""Advanced modules+"" Button=""Left"" Direction=""Down"" Xpos=""251"" Ypos=""358"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""7"" trace=""Advanced modules+"" Button=""Left"" Direction=""Up"" Xpos=""251"" Ypos=""358"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Advanced modules (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Advanced modules end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""286"" trace=""Polar Module+"" Button=""Left"" Direction=""Down"" Xpos=""382"" Ypos=""383"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""7"" trace=""Polar Module+"" Button=""Left"" Direction=""Up"" Xpos=""382"" Ypos=""383"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Polar Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Polar Module end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""214"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""530"" Ypos=""318"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""41"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""530"" Ypos=""318"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""74"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""530"" Ypos=""318"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""41"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""530"" Ypos=""318"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""262"" trace=""New Station+"" Button=""Left"" Direction=""Down"" Xpos=""595"" Ypos=""134"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""73"" trace=""New Station+"" Button=""Left"" Direction=""Up"" Xpos=""595"" Ypos=""134"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: New Station (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""16"" Delay=""0"" Action=""BB: New Station end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""245"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""603"" Ypos=""355"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""603"" Ypos=""355"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""746"" trace=""{instrumentName}+"" Button=""Left"" Direction=""Down"" Xpos=""653"" Ypos=""252"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""105"" trace=""{instrumentName}+"" Button=""Left"" Direction=""Up"" Xpos=""653"" Ypos=""252"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""16"" Delay=""0"" Action=""BB: {instrumentName} (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""BB: {instrumentName} end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""1518"" trace=""buttonConnect+"" Button=""Left"" Direction=""Down"" Xpos=""1286"" Ypos=""965"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""97"" trace=""buttonConnect+"" Button=""Left"" Direction=""Up"" Xpos=""1286"" Ypos=""965"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""249"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1345"" Ypos=""919"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""122"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1345"" Ypos=""919"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""1675"" trace=""buttonInit+"" Button=""Left"" Direction=""Down"" Xpos=""1225"" Ypos=""972"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""136"" trace=""buttonInit+"" Button=""Left"" Direction=""Up"" Xpos=""1225"" Ypos=""972"" Delta=""0"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var stm = tsunami.MeasurementModules[0]?.StationModules[0];
                Assert.IsTrue(stm != null, "station module object is NULL");
                var station = stm?._Station as TSU.Polar.Station;
                Assert.IsTrue(station != null, "station object is NULL");

                var instrument = station.Parameters2._Instrument;
                Assert.IsTrue(instrument != null, "instrument object is NULL");
                Assert.IsTrue(instrument._Model.Contains("AT40"), "failed to connect to AT40x ssid");

                var im = stm._InstrumentManager.SelectedInstrumentModule as D.AT40x.Module;
                Assert.IsTrue(im != null, "instrument module  object is NULL");

                Assert.IsTrue(im.InstrumentIsOn, "AT40x did not turn ON");

                bool isOk = false;
                int i;
                for (i = 0; i < 5 * 60; i++)
                {
                    if (im.IsInitialized)
                    {
                        isOk = true;
                        break;
                    }
                    else
                        Thread.Sleep(1000);
                }
                Assert.IsTrue(isOk, $"AT40x not init in {i} seconds");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                //Thread.Sleep(60 * 1000); // let me interact with tsunami
            });
        }


        /// <summary>
        /// keywords: AT401 AT402 AT403 CCS dat file init
        /// </summary>
        [Priority(2)]
        [TestMethod()]
        public void L2_P_FS_AT40X_Step02_points_file_selection_Test()
        {
            CreateTheoFile();
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_P_FS_AT40X_Step01_new_AT_station_instrument_selection_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
   
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""222"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""638"" Ypos=""326"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""638"" Ypos=""326"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""254"" trace=""Import Points from file(s)+"" Button=""Left"" Direction=""Down"" Xpos=""777"" Ypos=""361"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""113"" trace=""Import Points from file(s)+"" Button=""Left"" Direction=""Up"" Xpos=""777"" Ypos=""361"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""21"" Delay=""0"" Action=""BB: Import Points from file(s) (beginning)"" />

    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{-}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{R}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{-}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{c}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{C}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{s}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{H}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{.}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{t}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{x}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{t}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""335"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""Button"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""745"" trace=""button1+CCS"" Button=""Left"" Direction=""Down"" Xpos=""617"" Ypos=""566"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""145"" trace=""button1+CCS"" Button=""Left"" Direction=""Up"" Xpos=""617"" Ypos=""566"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""28"" Delay=""0"" Action=""MB: Coordinate system?: CCS"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""29"" trace=""button2+No"" Button=""Left"" Direction=""Down"" Xpos=""1031"" Ypos=""565"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""130"" trace=""button2+No"" Button=""Left"" Direction=""Up"" Xpos=""1031"" Ypos=""565"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""31"" Delay=""0"" Action=""MB: Zone definition: No"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""234"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""950"" Ypos=""760"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""154"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""950"" Ypos=""760"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""MB: Elements available in this instance of tsunami: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""35"" Delay=""0"" Action=""BB: Import Points from file(s) end"" />

  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var stm = tsunami.MeasurementModules[0]?.StationModules[0];
                var station = stm?._Station as TSU.Polar.Station;
                Assert.IsTrue(tsunami.Points.Count == 5, "Failed to open the theo file");


                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                //Thread.Sleep(60 * 1000); // let me interact with tsunami
            });
        }

        /// <summary>
        /// to move from ref1 to ref2 we need do H-51.8924 V+4.3193
        /// </summary>
        [TestMethod()]
        [Priority(3)]
        public void L2_P_FS_AT40X_Step03_Prepare_with_AT40X_REF1_Change_prisme_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_P_FS_AT40X_Step02_points_file_selection_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" FunctionKey=""F1"" speedFactor=""1"">
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />    
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""319"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""629"" Ypos=""404"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""11"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""629"" Ypos=""404"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""393"" trace=""Set as a 'Free-Station'+"" Button=""Left"" Direction=""Down"" Xpos=""738"" Ypos=""453"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""11"" trace=""Set as a 'Free-Station'+"" Button=""Left"" Direction=""Up"" Xpos=""738"" Ypos=""453"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""29"" Delay=""0"" Action=""BB: Set as a 'Free-Station' (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""214"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""1084"" Ypos=""634"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""9"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""1084"" Ypos=""634"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""32"" Delay=""0"" Action=""MB: Station point name?: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""33"" Delay=""0"" Action=""BB: Set as a 'Free-Station' end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""301"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""561"" Ypos=""423"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""561"" Ypos=""423"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""358"" trace=""Quick measure+"" Button=""Left"" Direction=""Down"" Xpos=""674"" Ypos=""750"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""8"" trace=""Quick measure+"" Button=""Left"" Direction=""Up"" Xpos=""674"" Ypos=""750"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""BB: Quick measure (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""39"" Delay=""0"" Action=""BB: Quick measure end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""210"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""548"" Ypos=""520"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""7"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""548"" Ypos=""520"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""270"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Down"" Xpos=""650"" Ypos=""538"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""8"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Up"" Xpos=""650"" Ypos=""538"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""44"" Delay=""0"" Action=""BB: Add known Point(s) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""282"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""205"" Ypos=""327"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""205"" Ypos=""327"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""218"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""199"" Ypos=""350"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""7"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""199"" Ypos=""350"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""49"" Delay=""289"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""857"" Ypos=""912"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""50"" Delay=""8"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""857"" Ypos=""912"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""51"" Delay=""0"" Action=""MB: Elements Selection: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""52"" Delay=""0"" Action=""BB: Add known Point(s) end"" />

  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var stm = tsunami.MeasurementModules[0]?.StationModules[0];
                var station = stm?._Station as TSU.Polar.Station;


                var mt = station.MeasuresToDo;
                Assert.IsTrue(mt != null, "measure taken is null");
                Assert.IsTrue(mt.Count == 2, $"{mt.Count} measures taken instead of 2");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        [Priority(4)]
        public void L2_P_FS_AT40X_Step04_Acquire_with_AT40X_REF1_Change_prisme_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_P_FS_AT40X_Step03_Prepare_with_AT40X_REF1_Change_prisme_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" FunctionKey=""F1"" speedFactor=""1"">
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />    
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""322"" trace=""buttonConnect+"" Button=""Left"" Direction=""Down"" Xpos=""1292"" Ypos=""976"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""12"" trace=""buttonConnect+"" Button=""Left"" Direction=""Up"" Xpos=""1292"" Ypos=""976"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""368"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1305"" Ypos=""930"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1305"" Ypos=""930"" Delta=""0"" />


    <IInputActivity xsi:type=""MouseActivity"" Pos.=""53"" Delay=""4285"" trace=""buttonPrism+"" Button=""Left"" Direction=""Down"" Xpos=""662"" Ypos=""978"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""54"" Delay=""89"" trace=""buttonPrism+"" Button=""Left"" Direction=""Up"" Xpos=""662"" Ypos=""978"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""55"" Delay=""253"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""737"" Ypos=""706"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""56"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""737"" Ypos=""706"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""57"" Delay=""3466"" trace=""buttonMeasure+"" Button=""Left"" Direction=""Down"" Xpos=""497"" Ypos=""975"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""58"" Delay=""146"" trace=""buttonMeasure+"" Button=""Left"" Direction=""Up"" Xpos=""497"" Ypos=""975"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""59"" Delay=""777"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""947"" Ypos=""733"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""60"" Delay=""105"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""947"" Ypos=""733"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""61"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK"" />


    <IInputActivity xsi:type=""MouseActivity"" Pos.=""105"" Delay=""7505"" trace=""continue"" Button=""Left"" Direction=""Down"" Xpos=""792"" Ypos=""564"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""106"" Delay=""114"" trace=""continue"" Button=""Left"" Direction=""Up"" Xpos=""792"" Ypos=""564"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""368"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""854"" Ypos=""647"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""854"" Ypos=""647"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""241"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""1030"" Ypos=""647"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""5"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""1030"" Ypos=""647"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""27"" Delay=""0"" Action=""MB: Point successfully measured: OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""62"" Delay=""258"" trace=""buttonPrism+"" Button=""Left"" Direction=""Down"" Xpos=""660"" Ypos=""968"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""63"" Delay=""73"" trace=""buttonPrism+"" Button=""Left"" Direction=""Up"" Xpos=""660"" Ypos=""968"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""64"" Delay=""218"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""682"" Ypos=""816"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""65"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""682"" Ypos=""816"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""256"" trace=""buttonCamera+"" Button=""Left"" Direction=""Down"" Xpos=""1465"" Ypos=""961"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""111"" trace=""buttonCamera+"" Button=""Left"" Direction=""Up"" Xpos=""1465"" Ypos=""961"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""930"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1530"" Ypos=""813"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""121"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1530"" Ypos=""813"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""389"" trace=""buttonCamera+"" Button=""Left"" Direction=""Down"" Xpos=""1469"" Ypos=""967"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""97"" trace=""buttonCamera+"" Button=""Left"" Direction=""Up"" Xpos=""1469"" Ypos=""967"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""370"" trace=""buttonJog+"" Button=""Left"" Direction=""Down"" Xpos=""1354"" Ypos=""962"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""146"" trace=""buttonJog+"" Button=""Left"" Direction=""Up"" Xpos=""1354"" Ypos=""963"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""342"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1404"" Ypos=""714"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""54"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1404"" Ypos=""714"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""11"" Delay=""82"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""12"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""14"" Delay=""26"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{DELETE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{DELETE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{DELETE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{DELETE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{DELETE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{DELETE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{DELETE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{DELETE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""16"" Delay=""51"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""17"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""329"" trace=""buttonPlusLeft+"" Button=""Left"" Direction=""Down"" Xpos=""1423"" Ypos=""772"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""105"" trace=""buttonPlusLeft+"" Button=""Left"" Direction=""Up"" Xpos=""1423"" Ypos=""772"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""210"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1393"" Ypos=""713"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1393"" Ypos=""713"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""22"" Delay=""42"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""23"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""21"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""25"" Delay=""28"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""26"" Delay=""32"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""27"" Delay=""56"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""28"" Delay=""24"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""68"" trace=""buttonPlusDown+"" Button=""Left"" Direction=""Down"" Xpos=""1614"" Ypos=""849"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""11"" trace=""buttonPlusDown+"" Button=""Left"" Direction=""Up"" Xpos=""1614"" Ypos=""849"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""3026"" trace=""buttonCamera+"" Button=""Left"" Direction=""Down"" Xpos=""1475"" Ypos=""973"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""9"" trace=""buttonCamera+"" Button=""Left"" Direction=""Up"" Xpos=""1475"" Ypos=""973"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""2737"" trace=""buttonCamera+"" Button=""Left"" Direction=""Down"" Xpos=""1475"" Ypos=""973"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""12"" trace=""buttonCamera+"" Button=""Left"" Direction=""Up"" Xpos=""1475"" Ypos=""973"" Delta=""0"" />   

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""90"" Delay=""1018"" trace=""buttonMeasure+"" Button=""Left"" Direction=""Down"" Xpos=""493"" Ypos=""965"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""91"" Delay=""8"" trace=""buttonMeasure+"" Button=""Left"" Direction=""Up"" Xpos=""493"" Ypos=""965"" Delta=""0"" />


    <IInputActivity xsi:type=""MouseActivity"" Pos.=""105"" Delay=""3666"" trace=""continue"" Button=""Left"" Direction=""Down"" Xpos=""792"" Ypos=""564"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""106"" Delay=""114"" trace=""continue"" Button=""Left"" Direction=""Up"" Xpos=""792"" Ypos=""564"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""92"" Delay=""366"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""770"" Ypos=""712"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""93"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""770"" Ypos=""712"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""94"" Delay=""465"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""965"" Ypos=""725"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""95"" Delay=""9"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""965"" Ypos=""725"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""96"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""97"" Delay=""5346"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""538"" Ypos=""579"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""98"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""538"" Ypos=""579"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""99"" Delay=""1098"" trace=""Control Opening+"" Button=""Left"" Direction=""Down"" Xpos=""657"" Ypos=""804"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""100"" Delay=""11"" trace=""Control Opening+"" Button=""Left"" Direction=""Up"" Xpos=""657"" Ypos=""804"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""101"" Delay=""0"" Action=""BB: Control Opening (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""102"" Delay=""0"" Action=""BB: Control Opening end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""103"" Delay=""3574"" trace=""buttonGotoAll+"" Button=""Left"" Direction=""Down"" Xpos=""440"" Ypos=""977"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""104"" Delay=""10"" trace=""buttonGotoAll+"" Button=""Left"" Direction=""Up"" Xpos=""440"" Ypos=""977"" Delta=""0"" />
    

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""105"" Delay=""7505"" trace=""continue"" Button=""Left"" Direction=""Down"" Xpos=""792"" Ypos=""564"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""106"" Delay=""11"" trace=""continue"" Button=""Left"" Direction=""Up"" Xpos=""792"" Ypos=""564"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""105"" Delay=""305"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""755"" Ypos=""935"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""106"" Delay=""11"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""755"" Ypos=""935"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""105"" Delay=""305"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""1030"" Ypos=""935"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""106"" Delay=""11"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""1030"" Ypos=""935"" Delta=""0"" />
  
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""105"" Delay=""505"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""982"" Ypos=""614"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""106"" Delay=""11"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""982"" Ypos=""614"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""107"" Delay=""0"" Action=""MB: Closure OK: OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""901"" trace=""button2+No"" Button=""Left"" Direction=""Down"" Xpos=""1173"" Ypos=""557"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""122"" trace=""button2+No"" Button=""Left"" Direction=""Up"" Xpos=""1173"" Ypos=""557"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""MB: Do you want to start the station setup compensation?: No"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""108"" Delay=""346"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""639"" Ypos=""404"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""109"" Delay=""114"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""639"" Ypos=""404"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""110"" Delay=""245"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Down"" Xpos=""784"" Ypos=""410"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""111"" Delay=""97"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Up"" Xpos=""784"" Ypos=""410"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""112"" Delay=""0"" Action=""BB: Computing (LGC2) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""113"" Delay=""569"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""944"" Ypos=""748"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""114"" Delay=""106"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""944"" Ypos=""748"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""115"" Delay=""0"" Action=""MB: Computing results (LGC adjustment): OK!"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""116"" Delay=""210"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""870"" Ypos=""738"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""117"" Delay=""162"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""870"" Ypos=""738"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""118"" Delay=""0"" Action=""MB: Compensation: Valid"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""119"" Delay=""289"" trace=""button1+Yes"" Button=""Left"" Direction=""Down"" Xpos=""871"" Ypos=""564"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""120"" Delay=""130"" trace=""button1+Yes"" Button=""Left"" Direction=""Up"" Xpos=""871"" Ypos=""564"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""121"" Delay=""0"" Action=""MB: Are you sure?: Yes"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""122"" Delay=""0"" Action=""BB: Computing (LGC2) end"" />

  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var stm = tsunami.MeasurementModules[0]?.StationModules[0];
                var station = stm?._Station as TSU.Polar.Station;

                var instrument = station.Parameters2._Instrument;
                Assert.IsTrue(instrument._Model.Contains("AT40"), "not a AT40x");

                var im = stm._InstrumentManager.SelectedInstrumentModule as D.AT40x.Module;
                Assert.IsTrue(im != null, "instru not selected");
                Assert.IsTrue(im.InstrumentIsOn, "AT40x did not turn ON");
                Assert.IsTrue(im.IsInitialized, "AT40x not init");

                var mt = station.MeasuresTaken;
                Assert.IsTrue(mt != null, "measure taken is null");
                Assert.IsTrue(mt.Count == 3, $"{mt.Count} measures taken instead of 3");

                Assert.IsTrue(station.Parameters2.Setups.FinalValues.Verified, "last station not verified");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        [Priority(5)]
        public void L2_P_FS_AT40X_Step05_Select_a_zone_4_MLA_CSS_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_P_FS_AT40X_Step04_Acquire_with_AT40X_REF1_Change_prisme_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" FunctionKey=""F1"" speedFactor=""1"">
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />    
  <KeyStrokesAndMouseClicks>

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""205"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""427"" Ypos=""265"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""427"" Ypos=""265"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""221"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""429"" Ypos=""299"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""429"" Ypos=""299"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""234"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""276"" Ypos=""412"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""276"" Ypos=""412"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Manager modules (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Manager modules end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""233"" trace=""Zone Manager+"" Button=""Left"" Direction=""Down"" Xpos=""427"" Ypos=""761"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""106"" trace=""Zone Manager+"" Button=""Left"" Direction=""Up"" Xpos=""427"" Ypos=""761"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Zone Manager (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Zone Manager end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""293"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""538"" Ypos=""305"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""538"" Ypos=""305"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""274"" trace=""button1+LHCb"" Button=""Left"" Direction=""Down"" Xpos=""861"" Ypos=""551"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""96"" trace=""button1+LHCb"" Button=""Left"" Direction=""Up"" Xpos=""862"" Ypos=""552"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""MB: New zone!: LHCb"" />
    
  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.Zone != null, "zone null");
                Assert.IsTrue(tsunami.Zone._Name == "LHCb", "zone not LHCb");
                Assert.AreEqual(0, tsunami.Points.Last(x => x._Name == "926R005.REF.001.")._Coordinates.Su.X.Value, 0.005, "wrong station Xsu");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }


        [TestMethod()]
        [Priority(6)]
        public void L2_P_FS_AT40X_Step06_Stakeout_a_Line_setup_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_P_FS_AT40X_Step05_Select_a_zone_4_MLA_CSS_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" FunctionKey=""F1"" speedFactor=""1"">
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />    
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""256"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""428"" Ypos=""265"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""73"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""428"" Ypos=""265"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""962"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""429"" Ypos=""298"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""113"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""429"" Ypos=""298"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""294"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""551"" Ypos=""322"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""551"" Ypos=""322"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""285"" trace=""Stake-out menu+"" Button=""Left"" Direction=""Down"" Xpos=""674"" Ypos=""580"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""82"" trace=""Stake-out menu+"" Button=""Left"" Direction=""Up"" Xpos=""674"" Ypos=""580"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Stake-out menu (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Stake-out menu end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""298"" trace=""Create a line+"" Button=""Left"" Direction=""Down"" Xpos=""861"" Ypos=""774"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""89"" trace=""Create a line+"" Button=""Left"" Direction=""Up"" Xpos=""861"" Ypos=""774"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""13"" Delay=""0"" Action=""BB: Create a line (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""282"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""240"" Ypos=""406"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""240"" Ypos=""406"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""234"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""227"" Ypos=""431"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""65"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""227"" Ypos=""431"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""258"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""861"" Ypos=""915"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""57"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""861"" Ypos=""915"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""20"" Delay=""0"" Action=""MB: Choose points to fit with a line: Select"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""201"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""921"" Ypos=""564"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""921"" Ypos=""564"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""274"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""1066"" Ypos=""565"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""82"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""1066"" Ypos=""565"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""25"" Delay=""0"" Action=""MB: Previous reflector use: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""26"" Delay=""0"" Action=""BB: Create a line end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""297"" trace=""buttonConnect+"" Button=""Left"" Direction=""Down"" Xpos=""1275"" Ypos=""968"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""87"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1275"" Ypos=""968"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""524"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1316"" Ypos=""923"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""74"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1316"" Ypos=""923"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""5338"" trace=""buttonGoto+"" Button=""Left"" Direction=""Down"" Xpos=""557"" Ypos=""963"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""73"" trace=""buttonGoto+"" Button=""Left"" Direction=""Up"" Xpos=""557"" Ypos=""963"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""5570"" trace=""buttonMeasure+"" Button=""Left"" Direction=""Down"" Xpos=""503"" Ypos=""965"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""106"" trace=""buttonMeasure+"" Button=""Left"" Direction=""Up"" Xpos=""503"" Ypos=""965"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""6226"" trace=""button1+Keep"" Button=""Left"" Direction=""Down"" Xpos=""846"" Ypos=""745"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""129"" trace=""button1+Keep"" Button=""Left"" Direction=""Up"" Xpos=""846"" Ypos=""745"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""MB: Keep or reject?: Keep"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""270"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""593"" Ypos=""331"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""593"" Ypos=""331"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""284"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Down"" Xpos=""729"" Ypos=""362"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""114"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Up"" Xpos=""729"" Ypos=""362"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""42"" Delay=""0"" Action=""BB: Add known Point(s) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""238"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""200"" Ypos=""441"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""44"" Delay=""42"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""200"" Ypos=""441"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""45"" Delay=""273"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""876"" Ypos=""936"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""46"" Delay=""90"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""876"" Ypos=""936"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""47"" Delay=""0"" Action=""MB: Elements Selection: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""48"" Delay=""0"" Action=""BB: Add known Point(s) end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""49"" Delay=""218"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""595"" Ypos=""400"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""50"" Delay=""37"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""595"" Ypos=""393"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""51"" Delay=""226"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""595"" Ypos=""394"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""52"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""595"" Ypos=""394"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""53"" Delay=""246"" trace=""Move to top+"" Button=""Left"" Direction=""Down"" Xpos=""784"" Ypos=""796"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""54"" Delay=""97"" trace=""Move to top+"" Button=""Left"" Direction=""Up"" Xpos=""784"" Ypos=""796"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""55"" Delay=""0"" Action=""BB: Move to top (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""56"" Delay=""0"" Action=""BB: Move to top end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""57"" Delay=""226"" trace=""buttonGoto+"" Button=""Left"" Direction=""Down"" Xpos=""546"" Ypos=""970"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""58"" Delay=""90"" trace=""buttonGoto+"" Button=""Left"" Direction=""Up"" Xpos=""546"" Ypos=""970"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""59"" Delay=""4722"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""592"" Ypos=""356"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""60"" Delay=""113"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""592"" Ypos=""356"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""61"" Delay=""282"" trace=""Remove+"" Button=""Left"" Direction=""Down"" Xpos=""687"" Ypos=""463"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""62"" Delay=""57"" trace=""Remove+"" Button=""Left"" Direction=""Up"" Xpos=""687"" Ypos=""463"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""63"" Delay=""0"" Action=""BB: Remove (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""64"" Delay=""0"" Action=""BB: Remove end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""65"" Delay=""290"" trace=""buttonMeasure+"" Button=""Left"" Direction=""Down"" Xpos=""490"" Ypos=""984"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""66"" Delay=""106"" trace=""buttonMeasure+"" Button=""Left"" Direction=""Up"" Xpos=""490"" Ypos=""984"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""67"" Delay=""6202"" trace=""button1+Keep"" Button=""Left"" Direction=""Down"" Xpos=""858"" Ypos=""744"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""68"" Delay=""73"" trace=""button1+Keep"" Button=""Left"" Direction=""Up"" Xpos=""858"" Ypos=""744"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""69"" Delay=""0"" Action=""MB: Keep or reject?: Keep"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""70"" Delay=""185"" trace=""button2+No"" Button=""Left"" Direction=""Down"" Xpos=""1161"" Ypos=""567"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""71"" Delay=""122"" trace=""button2+No"" Button=""Left"" Direction=""Up"" Xpos=""1161"" Ypos=""567"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""72"" Delay=""0"" Action=""MB: Already Measured: No"" />
    
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""225"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""594"" Ypos=""322"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""594"" Ypos=""322"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""281"" trace=""Control Opening+"" Button=""Left"" Direction=""Down"" Xpos=""756"" Ypos=""660"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""74"" trace=""Control Opening+"" Button=""Left"" Direction=""Up"" Xpos=""756"" Ypos=""660"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Control Opening (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Control Opening end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""233"" trace=""buttonGoto+"" Button=""Left"" Direction=""Down"" Xpos=""557"" Ypos=""972"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""114"" trace=""buttonGoto+"" Button=""Left"" Direction=""Up"" Xpos=""557"" Ypos=""972"" Delta=""0"" />

    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""2000"" Action=""BB: end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {

                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");

                var stm = tsunami.MeasurementModules[0]?.StationModules[0];
                var station = stm?._Station as TSU.Polar.Station;

                var instrument = station.Parameters2._Instrument;
                Assert.IsTrue(instrument._Model.Contains("AT"), "Failed to select an At40x");
                var lastmeasuer = station.MeasuresTaken[3] as TSU.Polar.Measure;
                Assert.AreEqual(0, lastmeasuer._Point._Coordinates.Beam.X.Value, 0.01, "dXbm too big");
                Assert.AreEqual(0, lastmeasuer._Point._Coordinates.Beam.Y.Value, 0.01, "dYbm too big");
                Assert.AreEqual(0, lastmeasuer._Point._Coordinates.Beam.Z.Value, 0.01, "dZbm too big");
                var lastmeasuer2 = station.MeasuresTaken[4] as TSU.Polar.Measure;
                Assert.AreEqual(0, lastmeasuer2._Point._Coordinates.Beam.X.Value, 0.01, "dXbm too big");
                Assert.AreEqual(0.19, lastmeasuer2._Point._Coordinates.Beam.Y.Value, 0.01, "dYbm wrong");
                Assert.AreEqual(0, lastmeasuer2._Point._Coordinates.Beam.Z.Value, 0.01, "dZbm too big");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }
    }
}
