﻿using Connected_Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Functional_Tests;
using TSU;
using TSU.Common.Instruments;
using CT = TSU.Connectivity_Tests;
using TSU.Polar;
using D = TSU.Common.Instruments.Device;
using System.Linq;
using System.Threading;

namespace Connected_Tests.Polar.Free_Station
{
    [TestClass]
    public class Zone926R005_TS60
    {
        [TestClass()]
        public class Zone926R005
        {
            [TestInitialize()]
            public void ConnectedInit()
            {
                Functional_Tests.Tools.Artefact.RelativeArtefactFolderPath = "Connected//Artefacts//";

            }

            const string FUNCTIONAL_ARTEFACTS = @"../../../Functional/bin/debug/Functional_Tests/Artefacts/";

            private void GetBackToStartingPosition()
            {
                CT.InstrumentsTools.ReadOnDiskTheOrientation(filePathForTS60StartingPosition, out var firstMeasure);
                if (ts60module != null)
                {
                    //var angles = TSU.Common.Compute.Survey.TransformToOppositeFace(firstMeasure.Angles);
                    ts60module.MoveTo(firstMeasure.Angles.Corrected.Horizontal.Value, firstMeasure.Angles.Corrected.Vertical.Value);
                }
            }

            /// <summary>
            /// if ref1:
            /// AH = 281.48433 gon
            /// AV = 78.87584 gon
            /// D = 2.027894 m
            /// 
            /// ref2 :
            /// AH 213.74015
            /// AV = 81.64077
            /// D = 1.49508
            /// 
            /// difference 
            /// ΔAH = -67.74418 gon
            /// ΔAV = +2.76493 gon
            /// ΔD = 0.532814 m
            /// 

            /// 
            [TestMethod()]
            [Priority(1)]
            public void L1_P_FS_TS60_Step0_Choose_ts60_Test()
            {
                Connected_Tests.Polar.Free_Station.Zone926R005_AT401.CreateTheoFile();
                var testMethod = MethodBase.GetCurrentMethod();
                F_Test test = new F_Test(testMethod)
                {
                    MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""229"" trace=""Advanced modules+"" Button=""Left"" Direction=""Down"" Xpos=""251"" Ypos=""358"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""7"" trace=""Advanced modules+"" Button=""Left"" Direction=""Up"" Xpos=""251"" Ypos=""358"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Advanced modules (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Advanced modules end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""286"" trace=""Polar Module+"" Button=""Left"" Direction=""Down"" Xpos=""382"" Ypos=""383"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""7"" trace=""Polar Module+"" Button=""Left"" Direction=""Up"" Xpos=""382"" Ypos=""383"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Polar Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Polar Module end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""214"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""530"" Ypos=""318"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""41"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""530"" Ypos=""318"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""74"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""530"" Ypos=""318"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""41"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""530"" Ypos=""318"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""262"" trace=""New Station+"" Button=""Left"" Direction=""Down"" Xpos=""595"" Ypos=""334"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""73"" trace=""New Station+"" Button=""Left"" Direction=""Up"" Xpos=""595"" Ypos=""334"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: New Station (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""16"" Delay=""0"" Action=""BB: New Station end"" />
   
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""202"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""593"" Ypos=""352"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""73"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""593"" Ypos=""352"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""246"" trace=""TS60_888094+"" Button=""Left"" Direction=""Down"" Xpos=""784"" Ypos=""551"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""98"" trace=""TS60_888094+"" Button=""Left"" Direction=""Up"" Xpos=""784"" Ypos=""551"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""19"" Delay=""0"" Action=""BB: TS60_888094 (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""20"" Delay=""0"" Action=""BB: TS60_888094 end"" />

  </KeyStrokesAndMouseClicks>
</Macro>"
                };

                test.Execute(testAction: (tsunami) =>
                {
                    Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                    var mms = tsunami.MeasurementModules;
                    Assert.IsTrue(mms != null, "measurement modules object is NULL");
                    Assert.IsTrue(mms.Count == 1, "no measurement module available");


                    var mm1 = tsunami.MeasurementModules[0];
                    Assert.IsTrue(mm1 != null, "measurement modules 1 object is NULL");

                    var sms = mm1.StationModules;
                    Assert.IsTrue(sms != null, "station modules object is NULL");
                    Assert.IsTrue(sms.Count == 1, "no station module available");

                    var stm = sms[0];
                    Assert.IsTrue(stm != null, "station module object is NULL");

                    var station = stm?._Station as TSU.Polar.Station;
                    Assert.IsTrue(station != null, "station object is NULL");

                    var instrument = station.Parameters2._Instrument;
                    Assert.IsTrue(instrument != null, "instrument object is NULL");
                    Assert.IsTrue(instrument._Model.Contains("TS60"), "Failed to select an TS60 instrument");

                    var im = stm._InstrumentManager.SelectedInstrumentModule as D.TS60.Module;
                    Assert.IsTrue(im != null, "instrument module  object is NULL");
                    test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
                    test.LookForBugMessagesInTsunamiLog();
                });
            }

            /// <summary>
            /// if ref1:
            /// AH = 281.48433 gon
            /// AV = 78.87584 gon
            /// D = 2.027894 m
            /// 
            /// ref2 :
            /// AH 213.74015
            /// AV = 81.64077
            /// D = 1.49508
            /// 
            /// difference 
            /// ΔAH = -67.74418 gon
            /// ΔAV = +2.76493 gon
            /// ΔD = 0.532814 m
            /// 

            /// 
            [TestMethod()]
            [Priority(2)]
            public void L1_P_FS_TS60_Step1_start_acquire_Test()
            {
                Connected_Tests.Polar.Free_Station.Zone926R005_AT401.CreateTheoFile();
                var testMethod = MethodBase.GetCurrentMethod();
                F_Test test = new F_Test(testMethod)
                {
                    TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L1_P_FS_TS60_Step0_Choose_ts60_Test) + ".tsut"),
                    MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""222"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""638"" Ypos=""326"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""638"" Ypos=""326"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""254"" trace=""Import Points from file(s)+"" Button=""Left"" Direction=""Down"" Xpos=""777"" Ypos=""361"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""113"" trace=""Import Points from file(s)+"" Button=""Left"" Direction=""Up"" Xpos=""777"" Ypos=""361"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""21"" Delay=""0"" Action=""BB: Import Points from file(s) (beginning)"" />

    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{-}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{R}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{-}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{c}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{C}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{s}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{H}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{.}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{t}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{x}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{t}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""335"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""Button"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""745"" trace=""button1+CCS"" Button=""Left"" Direction=""Down"" Xpos=""617"" Ypos=""566"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""145"" trace=""button1+CCS"" Button=""Left"" Direction=""Up"" Xpos=""617"" Ypos=""566"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""28"" Delay=""0"" Action=""MB: Coordinate system?: CCS"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""29"" trace=""button2+No"" Button=""Left"" Direction=""Down"" Xpos=""1031"" Ypos=""565"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""130"" trace=""button2+No"" Button=""Left"" Direction=""Up"" Xpos=""1031"" Ypos=""565"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""31"" Delay=""0"" Action=""MB: Zone definition: No"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""234"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""950"" Ypos=""760"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""154"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""950"" Ypos=""760"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""MB: Elements available in this instance of tsunami: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""35"" Delay=""0"" Action=""BB: Import Points from file(s) end"" />


    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""277"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""585"" Ypos=""394"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""585"" Ypos=""394"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""281"" trace=""Set as a 'Free-Station'+"" Button=""Left"" Direction=""Down"" Xpos=""682"" Ypos=""419"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""89"" trace=""Set as a 'Free-Station'+"" Button=""Left"" Direction=""Up"" Xpos=""682"" Ypos=""419"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""25"" Delay=""0"" Action=""BB: Set as a 'Free-Station' (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""254"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""1065"" Ypos=""619"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""65"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""1065"" Ypos=""619"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""28"" Delay=""0"" Action=""MB: Station point name?: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""29"" Delay=""0"" Action=""BB: Set as a 'Free-Station' end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""226"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""664"" Ypos=""429"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""664"" Ypos=""429"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""913"" trace=""Quick measure+"" Button=""Left"" Direction=""Down"" Xpos=""791"" Ypos=""750"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""66"" trace=""Quick measure+"" Button=""Left"" Direction=""Up"" Xpos=""791"" Ypos=""750"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Quick measure (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""35"" Delay=""0"" Action=""BB: Quick measure end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""202"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""581"" Ypos=""517"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""581"" Ypos=""517"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""209"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Down"" Xpos=""726"" Ypos=""546"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""74"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Up"" Xpos=""726"" Ypos=""546"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""40"" Delay=""0"" Action=""BB: Add known Point(s) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""346"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""237"" Ypos=""325"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""237"" Ypos=""325"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""237"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""226"" Ypos=""350"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""44"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""226"" Ypos=""350"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""45"" Delay=""277"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""867"" Ypos=""917"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""46"" Delay=""129"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""867"" Ypos=""917"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""47"" Delay=""0"" Action=""MB: Elements Selection: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""48"" Delay=""0"" Action=""BB: Add known Point(s) end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
                };

                test.Execute(testAction: (tsunami) =>
                {
                    Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                    var stm = tsunami.MeasurementModules[0]?.StationModules[0];
                    var station = stm?._Station as TSU.Polar.Station;

                    var instrument = station.Parameters2._Instrument;
                    Assert.IsTrue(instrument._Model.Contains("TS60"), "not a TS60");

                    Assert.IsTrue(station.MeasuresToDo.Count == 2, "should 2 measures to come");

                    Assert.IsTrue(station.Parameters2.DefaultMeasure.NumberOfMeasureToAverage == 1, "deafult number of measure per afce should be1");
                    Assert.IsTrue(station.Parameters2.DefaultMeasure.Face == FaceType.Face1, "shoudl face1 by default");

                    test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
                });
            }

            static string filePathForTS60StartingPosition = "TS60_STARTING_POSITION.TXT";
            static D.TS60.Module ts60module;

           

            [TestMethod()]
            [Priority(3)]
            public void L1_P_FS_TS60_Step2_record_FIRST_POSITION_Test()
            {
                var testMethod = MethodBase.GetCurrentMethod();
                F_Test test = new F_Test(testMethod)
                {
                    TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L1_P_FS_TS60_Step1_start_acquire_Test) + ".tsut"),
                    FinalAction = GetBackToStartingPosition,
                    MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""49"" Delay=""290"" trace=""Button_OnOff+"" Button=""Left"" Direction=""Down"" Xpos=""1173"" Ypos=""978"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""50"" Delay=""114"" trace=""Button_OnOff+"" Button=""Left"" Direction=""Up"" Xpos=""1173"" Ypos=""978"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""036"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""883"" Ypos=""590"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""105"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""883"" Ypos=""590"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""MB: Temperature?: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""233"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""883"" Ypos=""590"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""121"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""883"" Ypos=""590"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""MB: Pressure?: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""242"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""883"" Ypos=""590"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""74"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""883"" Ypos=""590"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""MB: Humidity?: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""2593"" trace=""button_All+"" Button=""Left"" Direction=""Down"" Xpos=""450"" Ypos=""980"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""80"" trace=""button_All+"" Button=""Left"" Direction=""Up"" Xpos=""450"" Ypos=""980"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""281"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""941"" Ypos=""709"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""82"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""941"" Ypos=""709"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK"" />.

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""7911"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""889"" Ypos=""649"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""63"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""889"" Ypos=""649"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""552"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""1056"" Ypos=""651"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""58"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""1056"" Ypos=""651"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""MB: Point successfully measured: OK!"" />


  </KeyStrokesAndMouseClicks>
</Macro>"
                };

                test.Execute(testAction: (tsunami) =>
                    {
                        Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                        var stm = tsunami.MeasurementModules[0]?.StationModules[0];

                        ts60module = stm._InstrumentManager.SelectedInstrumentModule as D.TS60.Module;
                        Assert.IsTrue(ts60module.InstrumentIsOn, "instrument off");

                        var station = stm?._Station as TSU.Polar.Station;

                        var lastMeasure = station.MeasuresTaken[0] as TSU.Polar.Measure;
                        Assert.IsTrue(lastMeasure != null, "no measure found");
                        CT.InstrumentsTools.RecordOnDiskTheOrientation(filePathForTS60StartingPosition, lastMeasure);

                        test.CreateArtefact(filePathForTS60StartingPosition);
                        test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                    });
            }

            [TestMethod()]
            [Priority(4)]
            public void L1_P_FS_TS60_Step3_acquisition_REF2_AND_COMPUTE_Test()
            {
                var testMethod = MethodBase.GetCurrentMethod();
                F_Test test = new F_Test(testMethod)
                {
                    TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L1_P_FS_TS60_Step2_record_FIRST_POSITION_Test) + ".tsut"),
                    FinalAction = GetBackToStartingPosition,
                    MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""49"" Delay=""290"" trace=""Button_OnOff+"" Button=""Left"" Direction=""Down"" Xpos=""1173"" Ypos=""978"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""50"" Delay=""114"" trace=""Button_OnOff+"" Button=""Left"" Direction=""Up"" Xpos=""1173"" Ypos=""978"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""036"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""883"" Ypos=""590"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""105"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""883"" Ypos=""590"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""MB: Temperature?: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""233"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""883"" Ypos=""590"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""121"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""883"" Ypos=""590"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""MB: Pressure?: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""242"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""883"" Ypos=""590"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""74"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""883"" Ypos=""590"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""MB: Humidity?: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""1889"" trace=""buttonJog+"" Button=""Left"" Direction=""Down"" Xpos=""1257"" Ypos=""987"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""98"" trace=""buttonJog+"" Button=""Left"" Direction=""Up"" Xpos=""1257"" Ypos=""987"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""1209"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1547"" Ypos=""914"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1547"" Ypos=""914"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""953"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""919"" Ypos=""511"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""65"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""919"" Ypos=""511"" Delta=""0"" />

    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""12"" Delay=""02"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""61"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""14"" Delay=""54"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""57"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""16"" Delay=""56"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""17"" Delay=""53"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""18"" Delay=""52"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""19"" Delay=""54"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""51"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""21"" Delay=""77"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{SUBTRACT}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""22"" Delay=""39"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""23"" Delay=""88"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""373"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""948"" Ypos=""613"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""948"" Ypos=""613"" Delta=""0"" />

    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""26"" Delay=""29"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""27"" Delay=""46"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""28"" Delay=""45"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""29"" Delay=""45"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""30"" Delay=""54"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""31"" Delay=""54"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""32"" Delay=""61"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""33"" Delay=""89"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ADD}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""34"" Delay=""02"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""261"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""884"" Ypos=""659"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""512"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""884"" Ypos=""659"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""MB: Move by Angle: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""3060"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""650"" Ypos=""587"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""137"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""650"" Ypos=""587"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""217"" trace=""Move to top+"" Button=""Left"" Direction=""Down"" Xpos=""810"" Ypos=""779"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""106"" trace=""Move to top+"" Button=""Left"" Direction=""Up"" Xpos=""810"" Ypos=""779"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Move to top (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Move to top end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""1353"" trace=""button_All+"" Button=""Left"" Direction=""Down"" Xpos=""470"" Ypos=""978"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""98"" trace=""button_All+"" Button=""Left"" Direction=""Up"" Xpos=""470"" Ypos=""978"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""857"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""722"" Ypos=""722"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""115"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""722"" Ypos=""722"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""464"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""946"" Ypos=""721"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""97"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""946"" Ypos=""721"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""13"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""19642"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""591"" Ypos=""584"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""121"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""591"" Ypos=""584"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""874"" trace=""Control Opening+"" Button=""Left"" Direction=""Down"" Xpos=""740"" Ypos=""784"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""73"" trace=""Control Opening+"" Button=""Left"" Direction=""Up"" Xpos=""740"" Ypos=""784"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""BB: Control Opening (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""19"" Delay=""0"" Action=""BB: Control Opening end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""1706"" trace=""buttonGoto+"" Button=""Left"" Direction=""Down"" Xpos=""599"" Ypos=""976"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""98"" trace=""buttonGoto+"" Button=""Left"" Direction=""Up"" Xpos=""599"" Ypos=""976"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""5809"" trace=""button_All+"" Button=""Left"" Direction=""Down"" Xpos=""450"" Ypos=""976"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""137"" trace=""button_All+"" Button=""Left"" Direction=""Up"" Xpos=""450"" Ypos=""976"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""4898"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""967"" Ypos=""606"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""169"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""967"" Ypos=""606"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""26"" Delay=""0"" Action=""MB: Closure OK: OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""248"" trace=""button2+Keep"" Button=""Left"" Direction=""Down"" Xpos=""1057"" Ypos=""644"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""71"" trace=""button2+Keep"" Button=""Left"" Direction=""Up"" Xpos=""1057"" Ypos=""644"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""MB: Closure out of tolerance: Keep"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""2152"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""846"" Ypos=""565"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""116"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""846"" Ypos=""565"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""266"" trace=""button2+Never"" Button=""Left"" Direction=""Down"" Xpos=""1141"" Ypos=""561"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""68"" trace=""button2+Never"" Button=""Left"" Direction=""Up"" Xpos=""1141"" Ypos=""561"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""MB: Do you want to start the station setup compensation?: Never"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""246"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""676"" Ypos=""361"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""129"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""676"" Ypos=""361"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""210"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Down"" Xpos=""836"" Ypos=""462"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""81"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Up"" Xpos=""836"" Ypos=""462"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""31"" Delay=""0"" Action=""BB: Computing (LGC2) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""1626"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""965"" Ypos=""759"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""146"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""965"" Ypos=""759"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""MB: Computing results (LGC adjustment): OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""1177"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""882"" Ypos=""749"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""162"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""882"" Ypos=""749"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""MB: Compensation: Valid"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""1081"" trace=""button1+Yes"" Button=""Left"" Direction=""Down"" Xpos=""892"" Ypos=""562"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""122"" trace=""button1+Yes"" Button=""Left"" Direction=""Up"" Xpos=""892"" Ypos=""562"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""40"" Delay=""0"" Action=""MB: Are you sure?: Yes"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""41"" Delay=""0"" Action=""BB: Computing (LGC2) end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
                };

                test.Execute(testAction: (tsunami) =>
                {
                    Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                    var stm = tsunami.MeasurementModules[0]?.StationModules[0];
                    var station = stm?._Station as TSU.Polar.Station;

                    var instrument = station.Parameters2._Instrument;
                    Assert.IsTrue(instrument._Model.Contains("TS60"), "not a AT40x");

                    var im = stm._InstrumentManager.SelectedInstrumentModule as D.TS60.Module;
                    Assert.IsTrue(im.InstrumentIsOn, "TS60 did not turn ON");

                    var final = station.Parameters2.Setups.FinalValues;
                    Assert.IsTrue(Math.Abs(final.SigmaZero)<30, "s0>30");
                    Assert.AreEqual(4471.97,final.StationPoint._Coordinates.Ccs.X.Value, 0.02, "stX != 4471.97");
                    Assert.AreEqual(5005.84, final.StationPoint._Coordinates.Ccs.Y.Value, 0.002, "stX != 5005.84");
                    Assert.AreEqual(331.129, final.StationPoint._Coordinates.Ccs.Z.Value, 0.02, "stX != 331.129");

                    test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
                });
            }

            [TestMethod()]
            [Priority(5)]
            public void L1_P_FS_TS60_Step4_STAKEOUT_REF3_station_Test()
            {
                var testMethod = MethodBase.GetCurrentMethod();

                F_Test test = new F_Test(testMethod)
                {
                    TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L1_P_FS_TS60_Step3_acquisition_REF2_AND_COMPUTE_Test) + ".tsut"),
                    FinalAction = GetBackToStartingPosition,
                    MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""204"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""422"" Ypos=""263"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""36"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""422"" Ypos=""263"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""250"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""424"" Ypos=""299"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""69"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""424"" Ypos=""299"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""217"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""532"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""30"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""532"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""291"" trace=""Control Opening+"" Button=""Left"" Direction=""Down"" Xpos=""699"" Ypos=""680"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""13"" trace=""Control Opening+"" Button=""Left"" Direction=""Up"" Xpos=""699"" Ypos=""680"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Control Opening (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Control Opening end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""262"" trace=""Button_OnOff+"" Button=""Left"" Direction=""Down"" Xpos=""1169"" Ypos=""993"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""69"" trace=""Button_OnOff+"" Button=""Left"" Direction=""Up"" Xpos=""1169"" Ypos=""993"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""200"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""871"" Ypos=""603"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""43"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""871"" Ypos=""603"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""MB: Temperature?: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""228"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""855"" Ypos=""578"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""31"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""855"" Ypos=""578"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""MB: Pressure?: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""238"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""876"" Ypos=""585"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""28"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""876"" Ypos=""585"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""21"" Delay=""0"" Action=""MB: Humidity?: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""260"" trace=""buttonGoto+"" Button=""Left"" Direction=""Down"" Xpos=""600"" Ypos=""984"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""84"" trace=""buttonGoto+"" Button=""Left"" Direction=""Up"" Xpos=""600"" Ypos=""984"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""3352"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""566"" Ypos=""338"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""43"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""566"" Ypos=""338"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""245"" trace=""Remove all points+"" Button=""Left"" Direction=""Down"" Xpos=""674"" Ypos=""846"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""78"" trace=""Remove all points+"" Button=""Left"" Direction=""Up"" Xpos=""674"" Ypos=""846"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Remove all points (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Remove all points end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""244"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""576"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""39"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""576"" Ypos=""330"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""294"" trace=""Stake-out menu+"" Button=""Left"" Direction=""Down"" Xpos=""719"" Ypos=""593"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""49"" trace=""Stake-out menu+"" Button=""Left"" Direction=""Up"" Xpos=""719"" Ypos=""593"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Stake-out menu (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Stake-out menu end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""222"" trace=""Add known Point(s) to stake out+"" Button=""Left"" Direction=""Down"" Xpos=""929"" Ypos=""641"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""65"" trace=""Add known Point(s) to stake out+"" Button=""Left"" Direction=""Up"" Xpos=""929"" Ypos=""641"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Add known Point(s) to stake out (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""214"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""266"" Ypos=""385"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""54"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""266"" Ypos=""385"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""325"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""865"" Ypos=""926"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""81"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""865"" Ypos=""926"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""MB: Elements Selection: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""35"" Delay=""0"" Action=""BB: Add known Point(s) to stake out end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""234"" trace=""buttonGotoAll+"" Button=""Left"" Direction=""Down"" Xpos=""673"" Ypos=""977"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""14"" trace=""buttonGotoAll+"" Button=""Left"" Direction=""Up"" Xpos=""673"" Ypos=""977"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""7535"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""655"" Ypos=""704"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""67"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""655"" Ypos=""704"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""432"" trace=""button1+Keep"" Button=""Left"" Direction=""Down"" Xpos=""861"" Ypos=""700"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""91"" trace=""button1+Keep"" Button=""Left"" Direction=""Up"" Xpos=""861"" Ypos=""700"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""MB: Keep or reject?: Keep"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""217"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""532"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""30"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""532"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""291"" trace=""Control Opening+"" Button=""Left"" Direction=""Down"" Xpos=""699"" Ypos=""680"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""13"" trace=""Control Opening+"" Button=""Left"" Direction=""Up"" Xpos=""699"" Ypos=""680"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Control Opening (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Control Opening end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""517"" trace=""buttonGoto+"" Button=""Left"" Direction=""Down"" Xpos=""609"" Ypos=""987"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""97"" trace=""buttonGoto+"" Button=""Left"" Direction=""Up"" Xpos=""609"" Ypos=""987"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""4974"" trace=""button_All+"" Button=""Left"" Direction=""Down"" Xpos=""441"" Ypos=""987"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""104"" trace=""button_All+"" Button=""Left"" Direction=""Up"" Xpos=""441"" Ypos=""987"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""7089"" trace=""button2+Keep"" Button=""Left"" Direction=""Down"" Xpos=""1044"" Ypos=""641"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""75"" trace=""button2+Keep"" Button=""Left"" Direction=""Up"" Xpos=""1044"" Ypos=""641"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""MB: Closure out of tolerance: Keep"" />

  </KeyStrokesAndMouseClicks>
</Macro>"
                };

                test.Execute(testAction: (tsunami) =>
                {
                    Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                    var stm = tsunami.MeasurementModules[0]?.StationModules[0];
                    var station = stm?._Station as TSU.Polar.Station;

                    var instrument = station.Parameters2._Instrument;
                    Assert.IsTrue(instrument._Model.Contains("TS60"), "not a AT40x");

                    var lastmeasuer = station.MeasuresTaken[3] as TSU.Polar.Measure;
                    Assert.AreEqual("926R005.REF.003.", lastmeasuer._Point._Name, $"926R005.REF.003. wanted, instead: {lastmeasuer._Point._Name}");
                    Assert.AreEqual(0, lastmeasuer._Point._Coordinates.StationCs.X.Value, 0.01, "dX too big");
                    Assert.AreEqual(0, lastmeasuer._Point._Coordinates.StationCs.Y.Value, 0.01, "dY too big");
                    Assert.AreEqual(0, lastmeasuer._Point._Coordinates.StationCs.Z.Value, 0.01, "dZ too big");
                    Assert.AreEqual(0, lastmeasuer.Offsets.Local.X.Value, 0.01, "dX too big");
                    Assert.AreEqual(0, lastmeasuer.Offsets.Local.Y.Value, 0.01, "dY too big");
                    Assert.AreEqual(0, lastmeasuer.Offsets.Local.Z.Value, 0.01, "dZ too big");
                    test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
                });
            }

            /// scotch extention 10mm:
            /// AH 19.0052
            /// AV = 112.3552
            /// D = 2.10790
            /// 
            /// ΔAH = 262.47913 gon
            /// ΔAV = -33.47936 gon
            /// ΔD = -0.080006 m
            /// 
         
            /// 
            [TestMethod()]
            [Priority(6)]
            public void L1_P_FS_TS60_Step5_SETUP_SCOTCH_TARGET_EXT_10_mm_Test()
            {
                var testMethod = MethodBase.GetCurrentMethod();
                F_Test test = new F_Test(testMethod)
                {
                    TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L1_P_FS_TS60_Step4_STAKEOUT_REF3_station_Test) + ".tsut"),
                    MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>

   <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""969"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""419"" Ypos=""259"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""96"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""419"" Ypos=""259"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""602"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""426"" Ypos=""295"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""426"" Ypos=""295"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""521"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""510"" Ypos=""331"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""510"" Ypos=""331"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""586"" trace=""Add New point+"" Button=""Left"" Direction=""Down"" Xpos=""622"" Ypos=""444"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""90"" trace=""Add New point+"" Button=""Left"" Direction=""Up"" Xpos=""622"" Ypos=""444"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Add New point (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Add New point end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""634"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""467"" Ypos=""361"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""467"" Ypos=""361"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""561"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""780"" Ypos=""325"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""113"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""780"" Ypos=""325"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""546"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1203"" Ypos=""360"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1203"" Ypos=""360"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""705"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""766"" Ypos=""328"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""766"" Ypos=""328"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""706"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1038"" Ypos=""356"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1038"" Ypos=""356"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""21"" Delay=""0"" Action=""BB: Change Reflector (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""690"" trace=""Options+"" Button=""Left"" Direction=""Down"" Xpos=""1514"" Ypos=""234"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""89"" trace=""Options+"" Button=""Left"" Direction=""Up"" Xpos=""1514"" Ypos=""234"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""24"" Delay=""0"" Action=""BB: Options (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""25"" Delay=""0"" Action=""BB: Options end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""705"" trace=""List View+"" Button=""Left"" Direction=""Down"" Xpos=""1663"" Ypos=""366"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""66"" trace=""List View+"" Button=""Left"" Direction=""Up"" Xpos=""1663"" Ypos=""366"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""28"" Delay=""0"" Action=""BB: List View (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""29"" Delay=""0"" Action=""BB: List View end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""602"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""454"" Ypos=""254"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""454"" Ypos=""254"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""32"" Delay=""713"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{S}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""33"" Delay=""129"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{C}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""34"" Delay=""74"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{S}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""35"" Delay=""3265"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""1490"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""179"" Ypos=""350"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""73"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""179"" Ypos=""350"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""994"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""879"" Ypos=""921"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""90"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""879"" Ypos=""922"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""40"" Delay=""0"" Action=""MB: Choose a Reflector: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""41"" Delay=""0"" Action=""BB: Change Reflector end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""1737"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""652"" Ypos=""432"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""652"" Ypos=""432"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""44"" Delay=""505"" trace=""Change Extension+"" Button=""Left"" Direction=""Down"" Xpos=""782"" Ypos=""475"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""45"" Delay=""97"" trace=""Change Extension+"" Button=""Left"" Direction=""Up"" Xpos=""782"" Ypos=""475"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""46"" Delay=""0"" Action=""BB: Change Extension (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""47"" Delay=""689"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""542"" Ypos=""515"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""48"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""542"" Ypos=""515"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""49"" Delay=""674"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""50"" Delay=""202"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""51"" Delay=""977"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""841"" Ypos=""578"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""52"" Delay=""66"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""841"" Ypos=""578"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""53"" Delay=""0"" Action=""MB: Extension: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""54"" Delay=""0"" Action=""BB: Change Extension end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
                };

                test.Execute(testAction: (tsunami) =>
                {
                    Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                    var stm = tsunami.MeasurementModules[0]?.StationModules[0];
                    var station = stm?._Station as TSU.Polar.Station;

                    var im = stm._InstrumentManager.SelectedInstrumentModule as D.TS60.Module;
                    var todos = station.MeasuresToDo;
                    Assert.IsTrue(todos.Count>0, "nothing in the todo list");
                    var todo = todos[0] as TSU.Polar.Measure;
                    Assert.IsTrue(todo.Extension.Value == 0.010, "etension should have been 10mm");
                    Assert.IsTrue(todo.Reflector._Name == "CSCOTCH_1", "CSCOTCH_1 not selected");

                    test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
                });
            }

            /// Laser:
            /// AH 18.3914
            /// AV = 109.6738
            /// D = 2.13625
            /// 
            /// ΔAH = 263.09293 gon
            /// ΔAV = -30.79796 gon
            /// ΔD = -0.108356 m
            /// </summary>
            /// 
            [TestMethod()]
            [Priority(7)]
            public void L1_P_FS_TS60_Step6_SETUP_LASER_TARGET_Test()
            {
                var testMethod = MethodBase.GetCurrentMethod();
                F_Test test = new F_Test(testMethod)
                {
                    TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L1_P_FS_TS60_Step5_SETUP_SCOTCH_TARGET_EXT_10_mm_Test) + ".tsut"),
                    MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""951"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""432"" Ypos=""459"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""432"" Ypos=""459"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""1889"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""521"" Ypos=""497"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""521"" Ypos=""497"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""1569"" trace=""Add New point+"" Button=""Left"" Direction=""Down"" Xpos=""663"" Ypos=""501"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""74"" trace=""Add New point+"" Button=""Left"" Direction=""Up"" Xpos=""663"" Ypos=""501"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Add New point (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Add New point end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""841"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""463"" Ypos=""552"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""463"" Ypos=""552"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""905"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""666"" Ypos=""326"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""666"" Ypos=""326"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""506"" trace=""Change Reflector+"" Button=""Left"" Direction=""Down"" Xpos=""904"" Ypos=""341"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""82"" trace=""Change Reflector+"" Button=""Left"" Direction=""Up"" Xpos=""904"" Ypos=""341"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Change Reflector (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""793"" trace=""Options+"" Button=""Left"" Direction=""Down"" Xpos=""1484"" Ypos=""205"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""49"" trace=""Options+"" Button=""Left"" Direction=""Up"" Xpos=""1484"" Ypos=""205"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""BB: Options (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""19"" Delay=""0"" Action=""BB: Options end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""489"" trace=""List View+"" Button=""Left"" Direction=""Down"" Xpos=""1649"" Ypos=""326"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""74"" trace=""List View+"" Button=""Left"" Direction=""Up"" Xpos=""1649"" Ypos=""326"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""22"" Delay=""0"" Action=""BB: List View (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""23"" Delay=""0"" Action=""BB: List View end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""617"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""231"" Ypos=""255"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""231"" Ypos=""255"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""26"" Delay=""946"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{L}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""27"" Delay=""264"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{A}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""28"" Delay=""90"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{S}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""1345"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""168"" Ypos=""363"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""168"" Ypos=""363"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""842"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""898"" Ypos=""926"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""89"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""898"" Ypos=""926"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""33"" Delay=""0"" Action=""MB: Choose a Reflector: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Change Reflector end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
                };

                test.Execute(testAction: (tsunami) =>
                {
                    Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                    var stm = tsunami.MeasurementModules[0]?.StationModules[0];
                    var station = stm?._Station as TSU.Polar.Station;

                    var im = stm._InstrumentManager.SelectedInstrumentModule as D.TS60.Module;
                    var todos = station.MeasuresToDo;
                    Assert.IsTrue(todos.Count > 1, "not enough in the todo list");
                    var todo = todos[1] as TSU.Polar.Measure;
                    Assert.IsTrue(todo.Extension.Value == -999.9, "etension should have been 0mm");
                    Assert.IsTrue(todo.Reflector._Name == "LASER_1", "CSCOTCH_1 not selected");

                    test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
                });
            }


            [TestMethod()]
            [Priority(8)]
            public void L1_P_FS_TS60_Step7_Measure_Test()
            {
                var testMethod = MethodBase.GetCurrentMethod();
                F_Test test = new F_Test(testMethod)
                {
                    TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L1_P_FS_TS60_Step6_SETUP_LASER_TARGET_Test) + ".tsut"),
                    FinalAction = GetBackToStartingPosition,
                    MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
            <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""1404"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""430"" Ypos=""273"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""52"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""430"" Ypos=""273"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""551"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""425"" Ypos=""292"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""122"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""425"" Ypos=""292"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""929"" trace=""Button_OnOff+"" Button=""Left"" Direction=""Down"" Xpos=""1162"" Ypos=""987"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""98"" trace=""Button_OnOff+"" Button=""Left"" Direction=""Up"" Xpos=""1163"" Ypos=""987"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""665"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""888"" Ypos=""588"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""82"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""888"" Ypos=""588"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""MB: Temperature?: OK"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""305"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""887"" Ypos=""586"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""89"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""887"" Ypos=""586"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""MB: Pressure?: OK"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""282"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""887"" Ypos=""586"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""114"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""887"" Ypos=""586"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""MB: Humidity?: OK"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""394"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""672"" Ypos=""352"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""113"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""672"" Ypos=""352"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""641"" trace=""Move to top+"" Button=""Left"" Direction=""Down"" Xpos=""845"" Ypos=""760"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""113"" trace=""Move to top+"" Button=""Left"" Direction=""Up"" Xpos=""845"" Ypos=""760"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""20"" Delay=""0"" Action=""BB: Move to top (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""21"" Delay=""0"" Action=""BB: Move to top end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""225"" trace=""buttonJog+"" Button=""Left"" Direction=""Down"" Xpos=""1248"" Ypos=""990"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""131"" trace=""buttonJog+"" Button=""Left"" Direction=""Up"" Xpos=""1248"" Ypos=""990"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""649"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1565"" Ypos=""908"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1565"" Ypos=""908"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""217"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""945"" Ypos=""501"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""945"" Ypos=""501"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""28"" Delay=""67"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""29"" Delay=""36"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""30"" Delay=""14"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""31"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""32"" Delay=""29"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""33"" Delay=""37"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""34"" Delay=""46"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""35"" Delay=""53"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""36"" Delay=""54"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""37"" Delay=""93"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""38"" Delay=""113"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{SUBTRACT}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""39"" Delay=""26"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""40"" Delay=""93"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""41"" Delay=""86"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""42"" Delay=""73"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""43"" Delay=""53"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""44"" Delay=""21"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""45"" Delay=""56"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""46"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""47"" Delay=""37"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""936"" Ypos=""593"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""48"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""936"" Ypos=""593"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""49"" Delay=""98"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""50"" Delay=""30"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""51"" Delay=""37"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""52"" Delay=""38"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""53"" Delay=""45"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""37"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""55"" Delay=""46"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""56"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""57"" Delay=""20"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""58"" Delay=""77"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""59"" Delay=""46"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""60"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""61"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""62"" Delay=""77"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""63"" Delay=""550"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""880"" Ypos=""669"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""64"" Delay=""89"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""880"" Ypos=""669"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""65"" Delay=""0"" Action=""MB: Move by Angle: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""66"" Delay=""3562"" trace=""button_All+"" Button=""Left"" Direction=""Down"" Xpos=""455"" Ypos=""981"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""67"" Delay=""121"" trace=""button_All+"" Button=""Left"" Direction=""Up"" Xpos=""455"" Ypos=""981"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""68"" Delay=""5306"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""974"" Ypos=""551"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""69"" Delay=""122"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""974"" Ypos=""551"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""70"" Delay=""0"" Action=""MB: Manual target aim: OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""71"" Delay=""7093"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""604"" Ypos=""334"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""72"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""604"" Ypos=""334"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""73"" Delay=""313"" trace=""Control Opening+"" Button=""Left"" Direction=""Down"" Xpos=""747"" Ypos=""679"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""74"" Delay=""106"" trace=""Control Opening+"" Button=""Left"" Direction=""Up"" Xpos=""747"" Ypos=""679"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""75"" Delay=""0"" Action=""BB: Control Opening (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""76"" Delay=""0"" Action=""BB: Control Opening end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""77"" Delay=""1250"" trace=""buttonGotoAll+"" Button=""Left"" Direction=""Down"" Xpos=""675"" Ypos=""984"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""78"" Delay=""98"" trace=""buttonGotoAll+"" Button=""Left"" Direction=""Up"" Xpos=""675"" Ypos=""984"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""8590"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{RIGHT}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""14"" Delay=""297"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData="" "" Target=""Button"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""MB: Closure out of tolerance: Keep"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""82"" Delay=""2050"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1540"" Ypos=""917"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""83"" Delay=""114"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1540"" Ypos=""917"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""84"" Delay=""202"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""955"" Ypos=""499"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""85"" Delay=""153"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""949"" Ypos=""499"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""86"" Delay=""46"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""87"" Delay=""37"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""88"" Delay=""29"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""89"" Delay=""37"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""90"" Delay=""46"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""91"" Delay=""37"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""92"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""93"" Delay=""29"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""94"" Delay=""54"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""95"" Delay=""53"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""38"" Delay=""33"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{SUBTRACT}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""96"" Delay=""51"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""97"" Delay=""78"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""98"" Delay=""94"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""99"" Delay=""57"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""100"" Delay=""44"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""101"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""102"" Delay=""58"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""103"" Delay=""45"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""104"" Delay=""217"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""945"" Ypos=""598"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""105"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""945"" Ypos=""598"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""106"" Delay=""54"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""107"" Delay=""38"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""108"" Delay=""45"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""109"" Delay=""45"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""110"" Delay=""46"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""111"" Delay=""37"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""112"" Delay=""49"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""113"" Delay=""53"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""114"" Delay=""69"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""115"" Delay=""70"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""116"" Delay=""48"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""117"" Delay=""53"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""118"" Delay=""32"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""119"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""120"" Delay=""218"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""870"" Ypos=""661"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""121"" Delay=""129"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""870"" Ypos=""661"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""122"" Delay=""0"" Action=""MB: Move by Angle: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""123"" Delay=""7938"" trace=""button_All+"" Button=""Left"" Direction=""Down"" Xpos=""450"" Ypos=""978"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""124"" Delay=""113"" trace=""button_All+"" Button=""Left"" Direction=""Up"" Xpos=""450"" Ypos=""978"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""125"" Delay=""533"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""1009"" Ypos=""564"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""126"" Delay=""98"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""1009"" Ypos=""564"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""127"" Delay=""0"" Action=""MB: Manual target aim: OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""7224"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""536"" Ypos=""325"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""104"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""536"" Ypos=""325"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""205"" trace=""Control Opening+"" Button=""Left"" Direction=""Down"" Xpos=""721"" Ypos=""645"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""105"" trace=""Control Opening+"" Button=""Left"" Direction=""Up"" Xpos=""721"" Ypos=""645"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Control Opening (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Control Opening end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""250"" trace=""buttonGotoAll+"" Button=""Left"" Direction=""Down"" Xpos=""676"" Ypos=""994"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""144"" trace=""buttonGotoAll+"" Button=""Left"" Direction=""Up"" Xpos=""677"" Ypos=""993"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""7590"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{RIGHT}}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""14"" Delay=""297"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData="" "" Target=""Button"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""MB: Closure out of tolerance: Keep"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
                };

                test.Execute(testAction: (tsunami) =>
                {
                    Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                    var stm = tsunami.MeasurementModules[0]?.StationModules[0];
                    var station = stm?._Station as TSU.Polar.Station;

                    var instrument = station.Parameters2._Instrument;
                    Assert.IsTrue(instrument._Model.Contains("TS60"), "not a AT40x");

                    var scotchM = station.MeasuresTaken[5] as TSU.Polar.Measure;
                    //Assert.AreEqual(1.1644, scotchM._Point._Coordinates.Su.X.Value, 0.01, "wrong X for scotch point");
                    //Assert.AreEqual(-0.9700, scotchM._Point._Coordinates.Su.Y.Value, 0.01, "wrong Y for scotch point");
                    //Assert.AreEqual(-0.4675, scotchM._Point._Coordinates.Su.Z.Value, 0.01, "wrong Z for scotch point");
                    Assert.AreEqual(4473.867, scotchM._Point._Coordinates.Ccs.X.Value, 0.01, "wrong X for scotch point");
                    Assert.AreEqual(5006.685, scotchM._Point._Coordinates.Ccs.Y.Value, 0.01, "wrong Y for scotch point");
                    Assert.AreEqual(330.713, scotchM._Point._Coordinates.Ccs.Z.Value, 0.01, "wrong Z for scotch point");
                    Assert.IsTrue(scotchM.Reflector._Model == "CSCOTCH", "not a Cscotch");

                    var laserM = station.MeasuresTaken[7] as TSU.Polar.Measure;

                    //Assert.AreEqual(1.210, laserM._Point._Coordinates.Su.X.Value, 0.01, "wrong X for scotch point");
                    //Assert.AreEqual(-0.9850, laserM._Point._Coordinates.Su.Y.Value, 0.01, "wrong Y for scotch point");
                    //Assert.AreEqual(-0.3744, laserM._Point._Coordinates.Su.Z.Value, 0.01, "wrong Z for scotch point");
                    //Assert.AreEqual(4473.928, laserM._Point._Coordinates.Ccs.X.Value, 0.01, "wrong X for scotch point");
                    //Assert.AreEqual(5006.734, laserM._Point._Coordinates.Ccs.Y.Value, 0.01, "wrong Y for scotch point");
                    //Assert.AreEqual(330.8019, laserM._Point._Coordinates.Ccs.Z.Value, 0.01, "wrong Z for scotch point");
                    Assert.AreEqual(4473.896, laserM._Point._Coordinates.Ccs.X.Value, 0.01, "wrong X for scotch point");
                    Assert.AreEqual(5006.721, laserM._Point._Coordinates.Ccs.Y.Value, 0.01, "wrong Y for scotch point");
                    Assert.AreEqual(330.8073, laserM._Point._Coordinates.Ccs.Z.Value, 0.01, "wrong Z for scotch point");
                    Assert.AreEqual(2.136, laserM.Distance.Corrected.Value, 0.05, "wrong corrected distance");
                    Assert.IsTrue(laserM.Distance.Corrected.Value - laserM.Distance.Raw.Value >0 ,"no distance correction detected");
                    test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
                });
            }
        }
    }
}
