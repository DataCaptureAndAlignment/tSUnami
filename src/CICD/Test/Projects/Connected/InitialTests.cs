﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using SWA = System.Windows.Automation;
using TSU;
using System.Diagnostics.Eventing.Reader;
using TSU.Tools;
using System.Reflection;
using System.Linq;
using Functional_Tests;

using D = TSU.Common.Instruments.Device;
using System.Reflection.Metadata;
using TSU.Connectivity_Tests;
using static TSU.Common.Instruments.Device.AT40x.Target;
using TSU.Common.Measures;
using TSU.Common.Compute.Compensations.BeamOffsets;
using TSU.Connectivity_Tests.Instruments.Device.AT40x.Tests;

namespace Connected_Tests
{
    [TestClass]
    public class Initialization
    {
        [TestInitialize()]
        public void ConnectedInit()
        {
            Functional_Tests.Tools.Artefact.RelativeArtefactFolderPath = "Connected//Artefact//";

        }
    }
}
