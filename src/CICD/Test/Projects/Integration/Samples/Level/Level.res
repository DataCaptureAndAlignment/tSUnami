

                          ****               ******              *****           ***********                                     
                          ****            ************        ***********        ***********                                     
                          ****           *****     ****      *****    ****              ****                                     
                          ****          ****        ****    ****       ****             ****                                     
                          ****          ****                ****                        ****                                     
                          ****          ****                ****                 ***********                                     
 ***********************  ****          ****      *******   ****                 ***********    ***********************             
 ***********************  ****          ****      *******   ****                 ****            ***********************             
                          ****          ****       ******   ****       ****      ****                                            
                          ***********    ****     **** **    ****     ****       ****                                            
                          ***********     ***********         ***********        ***********                                     
                          ***********        *****               *****           ***********                                     



*********************************************************************************************************************************** 

LGC2 v2.6.beta_4-OBSXYZ_Upgrade, compiled on Apr 17 2023
Copyright 2022, CERN SU. All rights reserved.
*********************************************************************************************************************************** 
Input by Tsunami

CALCUL DU 17 May 2023 16:18:16
*********************************************************************************************************************************** 

DATA SET -  INFO GENERAL:

	FRAMES :1

	POINTS : 8
	 INCONNUES INTRODUITES POINTS: 5

POINTS : 

	LECTURE DES POINTS DE CALAGE : 3
	LECTURE DES POINTS VARIABLES EN Z UNIQUEMENT : 5

MESURES :

	LECTURE DES DISTANCES VERTICALES (DVER) : 7

*** STATISTIQUES ***

	NOMBRE D'OBSERVATIONS =  7
	NOMBRE D'INCONNUES =     5
	NOMBRE D'ITERATIONS =    2

SIGMA ZERO A POSTERIORI =1.1239810, VALEUR CRITIQUE = ( 0.15912,  1.92065)
LES ECARTS-TYPES SONT CALCULES PAR RAPPORT AU SIGMA ZERO A PRIORI (EGAL A 1)



FRAME	ROOT  ID(1)                         

POINTS DE CALAGE   (NB. = 3,  REFERENTIEL = OLOC )
SFP                                       NOM               X                Y                Z         SX         SY         SZ         DX         DY         DZ    
                                                           (M)              (M)              (M)       (MM)       (MM)       (MM)       (MM)       (MM)       (MM)   

                               LHC.MB.A12R1.E     1792.3519600     2543.5211200      364.9722600                                                                     
                               LHC.MB.A12R1.S     1782.2201200     2539.7829500      365.1121300                                                                     
                               LHC.MB.C12R1.E     1762.9381200     2532.7786100      365.3709300                                                                     

POINTS VARIABLES EN Z UNIQUEMENT   (NB. = 5,  REFERENTIEL = OLOC )
SFP                                       NOM               X                Y                Z         SX         SY         SZ         DX         DY         DZ    
                                                           (M)              (M)              (M)       (MM)       (MM)       (MM)       (MM)       (MM)       (MM)   

                               LHC.MB.B12R1.E     1777.6595000     2538.1121500      365.1722033                           0.1155                          -0.1067   
                               LHC.MB.B12R1.M     1772.5846100     2536.2687000      365.2388533                           0.1155                          -0.0067   
                               LHC.MB.B12R1.T     1767.6888200     2533.9260700      365.3079233                           0.1155                          -0.0667   
                               LHC.MB.B12R1.S     1767.5074600     2534.4246400      365.3106733                           0.1155                          -0.0967   
       STLEV_20190515_092456_A_LHC.MB.A12R1.E        0.0000000        0.0000000      365.6468233                           0.0577                           0.0003   

SFP = Sub-Frame Point; * = TRUE


*** RESUME DES MESURES ***

DVER                                      
	INSTR_POS                                  RES_MAX    RES_MIN    RES_MOY   ECART_TYPE   
	                                              (MM)       (MM)       (MM)       (MM)   
	ROOT                                        0.0967    -0.1233     0.0000     0.0649   
RESIDU MOYEN =    0.0000    MM :  LIMITES DE CONFIANCE A 95.0 = (-0.0600, 0.0600)
ECART-TYPE   =    0.0649    MM :  LIMITES DE CONFIANCE A 95.0 = (0.0418, 0.1429)   


REPARTITION DES DVER   (1/100 MM)

-14.0000
 +    
-13.0000
 +    *
-12.0000
 +    
-11.0000
 +    
-10.0000
 +    
-9.0000
 +    
-8.0000
 +    
-7.0000
 +    
-6.0000
 +    
-5.0000
 +    
-4.0000
 +    
-3.0000
 +    
-2.0000
 +    
-1.0000
 +    ****
0.0000
 +    
1.0000
 +    
2.0000
 +    *
3.0000
 +    
4.0000
 +    
5.0000
 +    
6.0000
 +    
7.0000
 +    
8.0000
 +    
9.0000
 +    *
10.0000
 +    
11.0000

NOMBRE DE POINTS HORS-HISTOGRAMME   0




*** MESURES ***


	DVER
		DISTANCES VERTICALES (DVER)   (NB. = 7 )   
		POINT 1                                   POINT 2                                         OBSERVE      SIGMA         CALCULE     RESIDU    RES/SIG            DCOR   
		                                                                                              (M)       (MM)             (M)       (MM)                        (M)   
		STLEV_20190515_092456_A_LHC.MB.A12R1.E    LHC.MB.A12R1.E                               -0.6045900     0.1000      -0.6045633     0.0267     0.2667      -0.0700000      
		STLEV_20190515_092456_A_LHC.MB.A12R1.E    LHC.MB.A12R1.S                               -0.4647900     0.1000      -0.4646933     0.0967     0.9667      -0.0700000      
		STLEV_20190515_092456_A_LHC.MB.A12R1.E    LHC.MB.B12R1.E                               -0.4046200     0.1000      -0.4046200     0.0000     0.0000      -0.0700000      
		STLEV_20190515_092456_A_LHC.MB.A12R1.E    LHC.MB.B12R1.M                               -0.3379700     0.1000      -0.3379700     0.0000     0.0000      -0.0700000      
		STLEV_20190515_092456_A_LHC.MB.A12R1.E    LHC.MB.B12R1.T                               -0.2689000     0.1000      -0.2689000     0.0000     0.0000      -0.0700000      
		STLEV_20190515_092456_A_LHC.MB.A12R1.E    LHC.MB.B12R1.S                               -0.2661500     0.1000      -0.2661500     0.0000     0.0000      -0.0700000      
		STLEV_20190515_092456_A_LHC.MB.A12R1.E    LHC.MB.C12R1.E                               -0.2057700     0.1000      -0.2058933    -0.1233    -1.2333      -0.0700000      

*** FIN DE FICHIER ***
