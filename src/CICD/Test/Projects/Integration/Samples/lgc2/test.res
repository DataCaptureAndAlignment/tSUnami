
                          ****               ******              *****           ***********                                     
                          ****            ************        ***********        ***********                                     
                          ****           *****     ****      *****    ****              ****                                     
                          ****          ****        ****    ****       ****             ****                                     
                          ****          ****                ****                        ****                                     
                          ****          ****                ****                 ***********                                     
 ***********************  ****          ****      *******   ****                 ***********    ***********************             
 ***********************  ****          ****      *******   ****                 ****            ***********************             
                          ****          ****       ******   ****       ****      ****                                            
                          ***********    ****     **** **    ****     ****       ****                                            
                          ***********     ***********         ***********        ***********                                     
                          ***********        *****               *****           ***********                                     



*********************************************************************************************************************************** 

LGC2 v2.0.7, version was not compiled against SurveyLib, compiled on Mar 29 2017
Copyright 2015, CERN SU. All rights reserved.
*********************************************************************************************************************************** 
%Input by Tsunami

CALCUL DU 26 June 2017 15:45:22
*********************************************************************************************************************************** 

DATA SET -  INFO GENERAL:

	FRAMES :1

	POINTS : 5
	 Inconuues introduites POINTS: 9

	ANGLES : 1
	 Inconuues introduites ANGLES: 1

	DISTANCES : 2
	 Inconuues introduites DISTANCES: 0

POINTS : 

	LECTURE DES POINTS DE CALAGE : 2
	LECTURE DES POINTS VARIABLES EN XYZ : 3

MESURES :

	LECTURE DES ANGLES HORIZONTAUX (ANGL) : 5
	LECTURE DES DISTANCES ZENITHALES (ZEND) : 5
	LECTURE DES DISTANCES MESUREES (DIST) : 5

*** STATISTIQUES ***

	NOMBRE D'OBSERVATIONS =  15
	NOMBRE D'INCONNUES =     10
	NUMBER OF ITERATIONS =     4

SIGMA ZERO A POSTERIORI =0.0000000, VALEUR CRITIQUE = ( 0.40773,  1.60203)
LES ECARTS-TYPES SONT CALCULES PAR RAPPORT AU SIGMA ZERO A POSTERIORI



FRAME	ROOT  ID(1)    

POINTS DE CALAGE   (NB. = 2,  REFERENTIEL = OLOC )
SFP                  NOM               X                Y                Z         SX         SY         SZ         DX         DY         DZ    
                                      (M)              (M)              (M)       (MM)       (MM)       (MM)       (MM)       (MM)       (MM)   

      ST_UnknownTeam_001        0.0000000        0.0000000        0.0000000                                                                     
                    Fake        0.0000000        1.0000000        0.0000000                                                                     

POINTS VARIABLE EN XYZ   (NB. = 3,  REFERENTIEL = OLOC )
SFP                  NOM               X                Y                Z         SX         SY         SZ         DX         DY         DZ    
                                      (M)              (M)              (M)       (MM)       (MM)       (MM)       (MM)       (MM)       (MM)   

               Point_001       -0.0000000        9.3456510        0.4524580     0.0000     0.0000     0.0000    -0.0000    -0.3490   -239.5420   
               Point_002        0.6543421       -6.0383738       -0.5260953     0.0000     0.0000     0.0000     0.3421    -0.3738   -240.0953   
               Point_003       -0.8013938       -2.9955392        0.7324386     0.0000     0.0000     0.0000    -0.3938     0.4608   -239.5614   

SFP = Sub-Frame Point; * = TRUE


*** RESUME DES MESURES ***

ANGL                 
	TSTN_POS              RES_MAX    RES_MIN    RES_MOY   ECART_TYPE   
	                         (CC)       (CC)       (CC)       (CC)   
	ST_UnknownTeam_001     0.0000    -0.0000    -0.0000     0.0000   
RESIDU MOYEN =   -0.0000    CC :  LIMITES DE CONFIANCE A 95.0 = (-0.0000, 0.0000)
ECART-TYPE   =    0.0000    CC :  LIMITES DE CONFIANCE A 95.0 = (0.0000, 0.0000)   


ZEND                 
	TSTN_POS              RES_MAX    RES_MIN    RES_MOY   ECART_TYPE   
	                         (CC)       (CC)       (CC)       (CC)   
	ST_UnknownTeam_001     0.0000    -0.0000    -0.0000     0.0000   
RESIDU MOYEN =   -0.0000    CC :  LIMITES DE CONFIANCE A 95.0 = (-0.0000, 0.0000)
ECART-TYPE   =    0.0000    CC :  LIMITES DE CONFIANCE A 95.0 = (0.0000, 0.0000)   


DIST                 
	TSTN_POS              RES_MAX    RES_MIN    RES_MOY   ECART_TYPE   
	                         (MM)       (MM)       (MM)       (MM)   
	ST_UnknownTeam_001     0.0000     0.0000     0.0000     0.0000   
RESIDU MOYEN =   0.00000    MM :  LIMITES DE CONFIANCE A 95.0 = (-0.00000, 0.00000)
ECART-TYPE   =   0.00000    MM :  LIMITES DE CONFIANCE A 95.0 = (0.00000, 0.00000)   



*** MESURES ***


	INSTRUMENT POLAIRE: AT401_390858   
	POSITION             ST_UnknownTeam_001   HI (M)              0.0000000   SHI (MM)     0.0000           ROT3D   FALSE           
	ROM                                       ACST (GON)          0.0000000   V0 (GON)           65.8157100   SV0 (CC)      0.000   

		ANGLES HORIZONTAUX (ANGL)   (NB. = 5 )   
		POSITION                   OBSERVE      SIGMA         CALCULE     RESIDU      ECART        RES                 TRGT           H_TGT   
		                             (GON)       (CC)           (GON)       (CC)       (MM)       /SIG                                  (M)   
		Point_001              334.1842900      1.000       0.0000000      0.000     0.0000       0.00          RRR0.5_2885       0.0000000   
		Fake                   334.1842900      1.000       0.0000000     -0.000    -0.0000      -0.00          RRR0.5_2885       0.0000000   
		Point_002              127.3124400      1.000     193.1281500     -0.000    -0.0000      -0.00          RRR0.5_2885       0.0000000   
		Point_003              150.8260000      1.000     216.6417100     -0.000    -0.0000      -0.00          RRR0.5_2885       0.0000000   
		Point_001              334.1842900      1.000       0.0000000      0.000     0.0000       0.00          RRR0.5_2885       0.0000000   

		DISTANCES ZENITHALES (ZEND)   (NB. = 5 )   
		POSITION                   OBSERVE      SIGMA         CALCULE     RESIDU      ECART        RES                 TRGT           H_TGT   
		                             (GON)       (CC)           (GON)       (CC)       (MM)       /SIG                                  (M)   
		Point_001               96.9202900      1.000      96.9202900      0.000     0.0000      0.000          RRR0.5_2885       0.0000000   
		Fake                   100.0000000      1.000     100.0000000     -0.000    -0.0000     -0.000          RRR0.5_2885       0.0000000   
		Point_002              105.5005600      1.000     105.5005600     -0.000    -0.0000     -0.000          RRR0.5_2885       0.0000000   
		Point_003               85.2334900      1.000      85.2334900      0.000     0.0000      0.000          RRR0.5_2885       0.0000000   
		Point_001               96.9202900      1.000      96.9202900      0.000     0.0000      0.000          RRR0.5_2885       0.0000000   

		DISTANCES MESUREES (DIST)   (NB. = 5 )   
		POSITION                   OBSERVE      SIGMA         CALCULE     RESIDU      SENSI        RES           CONST     SCONST                 TRGT           H_TGT   
		                               (M)       (MM)             (M)       (MM)    (MM/CM)       /SIG             (M)       (MM)                                  (M)   
		Point_001                9.3565971     0.0010       9.3565971     0.0000     0.4836       0.00       0.0000000      FIXED          RRR0.5_2885       0.0000000   
		Fake                     1.0000000     0.0010       1.0000000     0.0000     0.0000       0.00       0.0000000      FIXED          RRR0.5_2885       0.0000000   
		Point_002                6.0964661     0.0010       6.0964661     0.0000    -0.8630       0.00       0.0000000      FIXED          RRR0.5_2885       0.0000000   
		Point_003                3.1862130     0.0010       3.1862130     0.0000     2.2988       0.00       0.0000000      FIXED          RRR0.5_2885       0.0000000   
		Point_001                9.3565971     0.0010       9.3565971     0.0000     0.4836       0.00       0.0000000      FIXED          RRR0.5_2885       0.0000000   

*** FIN DE FICHIER ***
