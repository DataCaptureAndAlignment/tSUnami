

                          ****               ******              *****           ***********                                     
                          ****            ************        ***********        ***********                                     
                          ****           *****     ****      *****    ****              ****                                     
                          ****          ****        ****    ****       ****             ****                                     
                          ****          ****                ****                        ****                                     
                          ****          ****                ****                 ***********                                     
 ***********************  ****          ****      *******   ****                 ***********    ***********************             
 ***********************  ****          ****      *******   ****                 ****            ***********************             
                          ****          ****       ******   ****       ****      ****                                            
                          ***********    ****     **** **    ****     ****       ****                                            
                          ***********     ***********         ***********        ***********                                     
                          ***********        *****               *****           ***********                                     



*********************************************************************************************************************************** 

LGC2 v2.6.0, compiled on Jun 12 2023
Copyright 2022, CERN SU. All rights reserved.
*********************************************************************************************************************************** 
LGC Input created by Tsunami for a free Station as a free station

CALCUL DU 03 August 2023 14:27:40
*********************************************************************************************************************************** 

DATA SET -  INFO GENERAL:

	FRAMES :1

	POINTS : 4
	 INCONNUES INTRODUITES POINTS: 3

	ANGLES : 1
	 INCONNUES INTRODUITES ANGLES: 1

	DISTANCES : 2
	 INCONNUES INTRODUITES DISTANCES: 0

POINTS : 

	LECTURE DES POINTS DE CALAGE : 3
	LECTURE DES POINTS VARIABLES EN XYZ : 1

MESURES :

	LECTURE DES ANGLES HORIZONTAUX (ANGL) : 3
	LECTURE DES DISTANCES ZENITHALES (ZEND) : 3
	LECTURE DES DISTANCES MESUREES (DIST) : 3

*** STATISTIQUES ***

	NOMBRE D'OBSERVATIONS =  9
	NOMBRE D'INCONNUES =     4
	NOMBRE D'ITERATIONS =    7

SIGMA ZERO A POSTERIORI =2048.908812, VALEUR CRITIQUE = (0.40773, 1.60203)
LES ECARTS-TYPES SONT CALCULES PAR RAPPORT AU SIGMA ZERO A POSTERIORI



FRAME	ROOT  ID(1)          

POINTS DE CALAGE   (NB. = 3,  REFERENTIEL = RS2K )
SFP                        NOM              X               Y               Z               H        SX        SY        SZ        DX        DY        DZ    
                                           (M)             (M)             (M)             (M)      (MM)      (MM)      (MM)      (MM)      (MM)      (MM)   

                926.R008.REF2.     4472.409661     5008.201679     2329.874965      331.005054                                                               
                926.R008.REF1.     4472.759716     5008.414703     2329.893138      331.023456                                                               
                926.R008.REF3.     4470.271429     5006.895787     2330.117225      331.245909                                                               

POINTS VARIABLES EN XYZ   (NB. = 1,  REFERENTIEL = RS2K )
SFP                        NOM              X               Y               Z               H        SX        SY        SZ        DX        DY        DZ    
                                           (M)             (M)             (M)             (M)      (MM)      (MM)      (MM)      (MM)      (MM)      (MM)   

       926.STL.20230803ELPA01.     4482.977526     5014.653114     2328.668333      329.805379   1441.175   2238.802   247.297   -780.624   -454.193    81.294   

SFP = Sub-Frame Point; * = TRUE


*** RESUME DES MESURES ***

ANGL                       
	TSTN_POS                   RES_MAX   RES_MIN   RES_MOY   ECART_TYPE   
	                              (CC)      (CC)      (CC)      (CC)   
	926.STL.20230803ELPA01.     17.135   -14.167    -0.000    15.861   
RESIDU MOYEN =   -0.000    CC :  LIMITES DE CONFIANCE A 95.0 = (-39.401, 39.401)
ECART-TYPE   =   15.861    CC :  LIMITES DE CONFIANCE A 95.0 = (8.258, 99.682)   

ZEND                       
	TSTN_POS                   RES_MAX   RES_MIN   RES_MOY   ECART_TYPE   
	                              (CC)      (CC)      (CC)      (CC)   
	926.STL.20230803ELPA01.    1607.875   -2250.137    95.102   2059.324   
RESIDU MOYEN =   95.102    CC :  LIMITES DE CONFIANCE A 95.0 = (-5115.643, 5115.643)
ECART-TYPE   =  2059.324    CC :  LIMITES DE CONFIANCE A 95.0 = (1072.204, 12942.302)   

DIST                       
	TSTN_POS                   RES_MAX   RES_MIN   RES_MOY   ECART_TYPE   
	                              (MM)      (MM)      (MM)      (MM)   
	926.STL.20230803ELPA01.    1813.806   -1108.911     0.529   1583.385   
RESIDU MOYEN =    0.529    MM :  LIMITES DE CONFIANCE A 95.0 = (-3933.347, 3933.347)
ECART-TYPE   =  1583.385    MM :  LIMITES DE CONFIANCE A 95.0 = (824.403, 9951.157)   



*** MESURES ***


	INSTRUMENT POLAIRE: Simulator_1   
	POSITION                   926.STL.20230803ELPA01.    HI (M)             0.000000   SHI (MM)     0.000          ROT3D   FALSE          
	ROM                                                   ACST (GON)         0.000000   V0 (GON)         165.637360   SV0 (CC)   128217.85   

		ANGLES HORIZONTAUX (ANGL)   (NB. = 3 )   
		POSITION                        OBSERVE     SIGMA        CALCULE    RESIDU     ECART       RES                       TRGT      OBSE      TCSE   
		                                  (GON)      (CC)          (GON)      (CC)      (MM)      /SIG                                 (CC)      (MM)   
		926.R008.REF2.                99.467400     10.00      99.469114     17.14     0.335      1.71                     PBTJ_2     10.00     0.000      
		926.R008.REF1.                99.467400     10.00      99.465983    -14.17    -0.268     -1.42                     PBTJ_2     10.00     0.000      
		926.R008.REF3.                99.467400     10.00      99.467103     -2.97    -0.070     -0.30                     PBTJ_2     10.00     0.000      

		DISTANCES ZENITHALES (ZEND)   (NB. = 3 )   
		POSITION                        OBSERVE     SIGMA        CALCULE    RESIDU     ECART       RES                       TRGT         H_TRGT      OBSE      TCSE      THSE   
		                                  (GON)      (CC)          (GON)      (CC)      (MM)      /SIG                                       (M)      (CC)      (MM)      (MM)   
		926.R008.REF2.                93.402100     10.00      93.494857    927.57    18.125     92.76                     PBTJ_2       0.070000     10.00     0.000     0.000      
		926.R008.REF1.                93.402100     10.00      93.177086   -2250.14   -42.535   -225.01                     PBTJ_2       0.070000     10.00     0.000     0.000      
		926.R008.REF3.                93.402100     10.00      93.562888   1607.88    37.777    160.79                     PBTJ_2       0.070000     10.00     0.000     0.000      

		DISTANCES MESUREES (DIST)   (NB. = 3 )   
		POSITION                        OBSERVE     SIGMA        CALCULE    RESIDU     SENSI       RES          CONST    SCONST                       TRGT         H_TRGT      OBSE       PPM      TCSE      THSE   
		                                    (M)      (MM)            (M)      (MM)   (MM/CM)      /SIG            (M)      (MM)                                       (M)      (MM)   (MM/KM)      (MM)      (MM)   
		926.R008.REF2.                13.150380     0.490      12.447071   -703.309     1.026   -1436.051       0.000000     FIXED                     PBTJ_2       0.070000     0.424     5.000     0.000     0.000      
		926.R008.REF1.                13.150380     0.490      12.041469   -1108.911     1.075   -2264.230       0.000000     FIXED                     PBTJ_2       0.070000     0.424     5.000     0.000     0.000      
		926.R008.REF3.                13.150380     0.490      14.964186   1813.806     1.015   3703.521       0.000000     FIXED                     PBTJ_2       0.070000     0.424     5.000     0.000     0.000      

*** FIN DE FICHIER ***
