﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Text.RegularExpressions;
using TSU;
using TSU.Common.Instruments.Reflector;
using TSU.Polar;
using E = TSU.Common.Elements;

namespace Integration_Tests.Dependencies
{
    [TestClass()]
    public class Geode
    {
        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [TestMethod()]
        public void ToGeodeExportMeasuretheodolite()
        {
            File.Delete(@"ToGeodeExportMeasuretheodolite.dat");

            Measure m = new Measure();
            m._Point = new E.Point() { _Name = "TT41.STL.324E.", Guid = Guid.NewGuid() };
            m.Angles.Corrected.Horizontal = new DoubleValue(200.2885, 0.0001);
            m.Angles.Corrected.Vertical = new DoubleValue(98.4418, 0.0001);
            m.Angles.Raw.Horizontal = new DoubleValue(200.2885, 0.0001);// geode shoudl export raw value taht are coorected by geode
            m.Angles.Raw.Vertical = new DoubleValue(98.4418, 0.0001);
            m.Distance.Corrected = new DoubleValue(11.029107, 0.0001);
            m.Distance.Raw = new DoubleValue(11.029107, 0.0001);
            m.GeodeRole = TSU.IO.SUSoft.Geode.Roles.Poin;
            m.NumberOfMeasureToAverage = 2;
            m.Face = TSU.Common.Instruments.FaceType.DoubleFace;
            m._Status = new TSU.Common.Measures.States.Good();
            var r = Tsunami2.Preferences.Values.Instruments.Find(x => x._Model == "CCR1.5" && x._SerialNumber == "1");
            m.Distance.Reflector = r as Reflector;
            m.Extension.Value = 0.07;
            m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates.Value = 0;
            m._Date = new DateTime(2016, 10, 19, 11, 05, 20);

            Measure m2 = new Measure();
            m2._Point = new E.Point() { _Name = "TT41.MDSH.412338.E", Guid = Guid.NewGuid() };
            m2.Angles.Corrected.Horizontal = new DoubleValue(189.8505, 0.0001);
            m2.Angles.Corrected.Vertical = new DoubleValue(102.6535, 0.0001);
            m2.Distance.Corrected = new DoubleValue(4.733571, 0.0001);
            m2.Angles.Raw.Horizontal = new DoubleValue(189.8505, 0.0001);
            m2.Angles.Raw.Vertical = new DoubleValue(102.6535, 0.0001);
            m2.Distance.Raw = new DoubleValue(4.733571, 0.0001);
            m2.Distance.Reflector = r as Reflector;
            m2.Extension.Value = 0.07;
            m2.NumberOfMeasureToAverage = 2;
            m2._Status = new TSU.Common.Measures.States.Good();
            m2.Face = TSU.Common.Instruments.FaceType.DoubleFace;
            m2.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates.Value = 0;
            m2.GeodeRole = TSU.IO.SUSoft.Geode.Roles.Cala;
            m2._Date = new DateTime(2016, 10, 19, 11, 05, 20);

            Measure m3 = new Measure();
            m3._Point = new E.Point() { _Name = "TT41.MDSH.412338.S", Guid = Guid.NewGuid() };
            m3.Angles.Corrected.Horizontal = new DoubleValue(189.8505, 0.0001);
            m3.Angles.Corrected.Vertical = new DoubleValue(102.6535, 0.0001);
            m3.Distance.Corrected = new DoubleValue(4.733571, 0.0001);
            m3.Angles.Raw.Horizontal = new DoubleValue(189.8505, 0.0001);
            m3.Angles.Raw.Vertical = new DoubleValue(102.6535, 0.0001);
            m3.Distance.Raw = new DoubleValue(4.733571, 0.0001);
            m3.Distance.Reflector = r as Reflector;
            m3.Extension.Value = 0.07;
            m3.NumberOfMeasureToAverage = 2;
            m3._Status = new TSU.Common.Measures.States.Good();
            m3.Face = TSU.Common.Instruments.FaceType.DoubleFace;
            m3.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates.Value = 0;
            m3.GeodeRole = TSU.IO.SUSoft.Geode.Roles.Radi;
            m3._Date = new DateTime(2016, 10, 19, 11, 05, 20);

            Station s = new Station();

            var stationPoint = new E.Point() { _Name = "TT41.STL.19101601.", Guid = Guid.NewGuid() };
            stationPoint._Coordinates.Ccs = new E.Coordinates("TT41.STL.19101601.", 2666, 3666, 366);
            stationPoint._Parameters.Cumul = 666;
            s.Parameters2._StationPoint = stationPoint;
            s.Parameters2.Setups.InitialValues.IsInstrumentHeightKnown = Station.Parameters.Setup.Values.InstrumentTypeHeight.Known;
            s.Parameters2.Setups.InitialValues.InstrumentHeight = new DoubleValue(0, 0);
            s.MeasuresTaken.Add(m);
            s.MeasuresTaken.Add(m2);
            s.MeasuresTaken.Add(m3);
            s.ParametersBasic._Date = new DateTime(2016, 10, 19);
            s.ParametersBasic._Team = "ELPA";
            s.ParametersBasic._Operation.value = 12448;
            s.ParametersBasic._Instrument = new TSU.Common.Instruments.Device.AT40x.Instrument() { _SerialNumber = "390769", _Model = "AT401", _Name = "AT401- 390769" };

            TSU.IO.SUSoft.Geode.ToGeode(s, out string result, "ToGeodeExportMeasuretheodolite.dat", Directory.GetCurrentDirectory(), false, false);

            Assert_that_StringShouldBeIdenticalLineByLine(expected, result);
        }

        private const string expected = @"  001;RE;19-Oct-2016;ELPA      ; 12448;Tsunami ;
  002;NP;TT41      ;STL\.19101601\.                    ;p;2666\.00000;3666\.00000;366\.00000 ; 0;   666\.000;19-Oct-2016;00:00:00; !!! Code 0 is not supported by GEODE, please change it! \(9th fields\);
! 002 ----------STtheo_161019_000000_TT41_STL_19101601_ Polar recorded at [0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}----------
  003;OB;AT401    ;390769     ;TT41      ;STL\.19101601\.                    ;; 0\.00000;;      ;  20\.0;1013\.0;    60;00;;R;HI_FIX;s0=\?;;
!
! ###;MT;target\.n ;target\.sn  ;acc/zone  ;class\.numero\.point               ;       obs; target\.h;  offset;8 empty;X;OT;    Time;             comment;   i1\.type;     i1\.sn;   i2\.type;     i2\.sn;   i3\.type;     i3\.sn;
!----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  004;AH;CCR1\.5   ;1          ;TT41      ;STL\.324E\.                        ;200\.288500; 0\.070000;0\.000000;;;;;;;;;A;01;11:05:20;TSU: 'Good 2F / 2 : => sH=1 cc, sV=1 cc, sD=0\.10 mm';          ;          ;          ;          ;          ;          ;
  005;AH;CCR1\.5   ;1          ;TT41      ;MDSH\.412338\.E                    ;189\.850500; 0\.070000;0\.000000;;;;;;;;;R;01;11:05:20;TSU: 'Good 2F / 2 : => sH=1 cc, sV=1 cc, sD=0\.10 mm';          ;          ;          ;          ;          ;          ;
  006;AH;CCR1\.5   ;1          ;TT41      ;MDSH\.412338\.S                    ;189\.850500; 0\.070000;0\.000000;;;;;;;;;C;01;11:05:20;TSU: 'Good 2F / 2 : => sH=1 cc, sV=1 cc, sD=0\.10 mm';          ;          ;          ;          ;          ;          ;
  007;AV;CCR1\.5   ;1          ;TT41      ;STL\.324E\.                        ; 98\.441800; 0\.070000;0\.000000;;;;;;;;;A;01;11:05:20;TSU: 'Good 2F / 2 : => sH=1 cc, sV=1 cc, sD=0\.10 mm';          ;          ;          ;          ;          ;          ;
  008;AV;CCR1\.5   ;1          ;TT41      ;MDSH\.412338\.E                    ;102\.653500; 0\.070000;0\.000000;;;;;;;;;R;01;11:05:20;TSU: 'Good 2F / 2 : => sH=1 cc, sV=1 cc, sD=0\.10 mm';          ;          ;          ;          ;          ;          ;
  009;AV;CCR1\.5   ;1          ;TT41      ;MDSH\.412338\.S                    ;102\.653500; 0\.070000;0\.000000;;;;;;;;;C;01;11:05:20;TSU: 'Good 2F / 2 : => sH=1 cc, sV=1 cc, sD=0\.10 mm';          ;          ;          ;          ;          ;          ;
  010;DD;CCR1\.5   ;1          ;TT41      ;STL\.324E\.                        ; 11\.029107; 0\.070000;0\.000000;;;;;;;;;A;01;11:05:20;TSU: 'Good 2F / 2 : => sH=1 cc, sV=1 cc, sD=0\.10 mm';          ;          ;          ;          ;          ;          ;
  011;DD;CCR1\.5   ;1          ;TT41      ;MDSH\.412338\.E                    ;  4\.733571; 0\.070000;0\.000000;;;;;;;;;R;01;11:05:20;TSU: 'Good 2F / 2 : => sH=1 cc, sV=1 cc, sD=0\.10 mm';          ;          ;          ;          ;          ;          ;
  012;DD;CCR1\.5   ;1          ;TT41      ;MDSH\.412338\.S                    ;  4\.733571; 0\.070000;0\.000000;;;;;;;;;C;01;11:05:20;TSU: 'Good 2F / 2 : => sH=1 cc, sV=1 cc, sD=0\.10 mm';          ;          ;          ;          ;          ;          ;";

        public void Assert_that_StringShouldBeIdenticalLineByLine(string expected, string result)
        {
            // Read all lines from both files
            string[] expectedSplitted = expected.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
            string[] resultSplitted = result.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

            LinesArraysShouldBeIdenticalLineByLine(expectedSplitted, resultSplitted);
        }

        public void LinesArraysShouldBeIdenticalLineByLine(string[] expected, string[] result)
        {
            // Assert that the number of lines is the same
            Assert.AreEqual(expected.Length, result.Length, "not same number of lines in both feeds");

            // Compare each line
            for (int i = 0; i < expected.Length; i++)
            {
                var m = Regex.Matches(result[i], expected[i]);
                Assert.IsTrue(m.Count == 1 && m[0].Index == 0 && m[0].Length == result[i].Length, $"Line {i} \"{result[i]}\" does not match the expected pattern \"{expected[i]}\"");
            }
        }

        public void Assert_that_FilesShouldBeIdenticalLineByLine(string filePath1, string filePath2)
        {
            // Read all lines from both files
            string[] file1Lines = File.ReadAllLines(filePath1);
            string[] file2Lines = File.ReadAllLines(filePath2);

            LinesArraysShouldBeIdenticalLineByLine(file1Lines, file2Lines);
        }

        //        /// <summary>
        //        /// To test new patterns
        //        /// </summary>
        //        [TestMethod]
        //        public void Test_Assert_that_StringShouldBeIdenticalLineByLine()
        //        {
        //            string result = @"  001;RE;19-Oct-2016;ELPA      ; 12448;Tsunami ;
        //  002;NP;TT41      ;STL.19101601.                    ;p;2666.00000;3666.00000;366.00000 ; 0;   666.000;19-Oct-2016;00:00:00; !!! Code 0 is not supported by GEODE, please change it! (9th fields);
        //! 002 ----------STtheo_161019_000000_TT41_STL_19101601_ Polar recorded at 10:10:10----------
        //  003;OB;AT401    ;390769     ;TT41      ;STL.19101601.                    ;; 0.00000;;      ;  20.0;1013.0;    60;00;;R;HI_FIX;s0=?;;

        //! ###;MT;target.n ;target.sn  ;acc/zone  ;class.numero.point               ;       obs; target.h;  offset;8 empty;X;OT;    Time;             comment;   i1.type;     i1.sn;   i2.type;     i2.sn;   i3.type;     i3.sn;
        //!----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //  004;AH;CCR1.5   ;1          ;TT41      ;STL.324E.                        ;200.288500; 0.070000;0.000000;;;;;;;;;A;01;11:05:20;TSU: 'Good 2F / 2 : => sH=1 cc, sV=1 cc, sD=0.10 mm';          ;          ;          ;          ;          ;          ;
        //  005;AH;CCR1.5   ;1          ;TT41      ;MDSH.412338.E                    ;189.850500; 0.070000;0.000000;;;;;;;;;R;01;11:05:20;TSU: 'Good 2F / 2 : => sH=1 cc, sV=1 cc, sD=0.10 mm';          ;          ;          ;          ;          ;          ;
        //  006;AH;CCR1.5   ;1          ;TT41      ;MDSH.412338.S                    ;189.850500; 0.070000;0.000000;;;;;;;;;C;01;11:05:20;TSU: 'Good 2F / 2 : => sH=1 cc, sV=1 cc, sD=0.10 mm';          ;          ;          ;          ;          ;          ;
        //  007;AV;CCR1.5   ;1          ;TT41      ;STL.324E.                        ; 98.441800; 0.070000;0.000000;;;;;;;;;A;01;11:05:20;TSU: 'Good 2F / 2 : => sH=1 cc, sV=1 cc, sD=0.10 mm';          ;          ;          ;          ;          ;          ;
        //  008;AV;CCR1.5   ;1          ;TT41      ;MDSH.412338.E                    ;102.653500; 0.070000;0.000000;;;;;;;;;R;01;11:05:20;TSU: 'Good 2F / 2 : => sH=1 cc, sV=1 cc, sD=0.10 mm';          ;          ;          ;          ;          ;          ;
        //  009;AV;CCR1.5   ;1          ;TT41      ;MDSH.412338.S                    ;102.653500; 0.070000;0.000000;;;;;;;;;C;01;11:05:20;TSU: 'Good 2F / 2 : => sH=1 cc, sV=1 cc, sD=0.10 mm';          ;          ;          ;          ;          ;          ;
        //  010;DD;CCR1.5   ;1          ;TT41      ;STL.324E.                        ; 11.029107; 0.070000;0.000000;;;;;;;;;A;01;11:05:20;TSU: 'Good 2F / 2 : => sH=1 cc, sV=1 cc, sD=0.10 mm';          ;          ;          ;          ;          ;          ;
        //  011;DD;CCR1.5   ;1          ;TT41      ;MDSH.412338.E                    ;  4.733571; 0.070000;0.000000;;;;;;;;;R;01;11:05:20;TSU: 'Good 2F / 2 : => sH=1 cc, sV=1 cc, sD=0.10 mm';          ;          ;          ;          ;          ;          ;
        //  012;DD;CCR1.5   ;1          ;TT41      ;MDSH.412338.S                    ;  4.733571; 0.070000;0.000000;;;;;;;;;C;01;11:05:20;TSU: 'Good 2F / 2 : => sH=1 cc, sV=1 cc, sD=0.10 mm';          ;          ;          ;          ;          ;          ;";

        //            Assert_that_StringShouldBeIdenticalLineByLine(expected, result);
        //        }
    }
}
