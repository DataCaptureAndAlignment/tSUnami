﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Compute.Compensations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Integration_Tests.LGC2

{
    [TestClass()]
    public class Lgc2Test
    {
        [TestMethod()]
        public void COmputeWireWithLGC2_Test()
        {
            string fileContent = @"*TITR 
Input by Tsunami
*OLOC
*HIST 
*JSON
*PUNC E 
*FAUT     .01     .10 
*PREC 7
*INSTR 
   *SCALE RS_1000_16 0.1 0 0 0 0
   *EDM DISTTHEO TARGETTHEO 0 0 0 
      TARGETTHEO 1 0 0 0 0 0 0 0 

*CALA 
   LHC.MB.A12R1.E 0.000000 0.000000 0.000000 
   LHC.MB.A12R1.S -0.064851 10.799253 0.139853 
   LHC.MB.C12R1.E -0.085023 31.314024 0.398572 
   LHC.MB.C12R1.S -0.040619 42.114499 0.536313 

*VXY 
   LHC.MB.B12R1.E -0.082897 15.656258 0.200020 
   LHC.MB.B12R1.M -0.088164 21.055590 0.266551 
   LHC.MB.B12R1.T -0.623767 26.456498 0.335657 
   LHC.MB.B12R1.S -0.093236 26.457255 0.338437 

*ECHO RS_1000_16 
   LHC.MB.A12R1.E 0.00000 $Debut du fil
   LHC.MB.A12R1.S 0.06472 $ 
   LHC.MB.B12R1.E 0.08294 $ 
   LHC.MB.B12R1.M 0.00000 $ 
   LHC.MB.B12R1.T 0.00000 $ 
   LHC.MB.B12R1.S 0.09332 $ 
   LHC.MB.C12R1.E 0.08507 $ 
   LHC.MB.C12R1.S 0.04062 $Fin du fil

*DSPT LHC.MB.A12R1.E DISTTHEO
   LHC.MB.A12R1.S 10.800354
   LHC.MB.B12R1.E 15.657755
   LHC.MB.B12R1.M 21.057462
   LHC.MB.B12R1.T 26.465979
   LHC.MB.B12R1.S 26.459583
   LHC.MB.C12R1.E 31.316676
   LHC.MB.C12R1.S 42.117933
*END 
";
            string filepath = @"testJsonOnWire.inp";
            System.IO.File.WriteAllText(filepath, fileContent);
            var lgc2 = Lgc2.RunInput(filepath);
            Lgc2.Output output = lgc2._Output;

            // special treatement as Philippe do not fill the output with result but has direct method to fill the station objects...
            //Lgc2 lgc2 = new Lgc2("testJsonOnWire.inp");
            lgc2._FileName = "testJsonOnWire";
            lgc2._Output = output;
            TSU.Line.Station stationLine = new TSU.Line.Station();
            TSU.Line.Module lineModule = new TSU.Line.Module();
            //List<Point> a  =lgc2._Output.GetCoordinatesResultsForWire(lineModule);

            //Assert.AreEqual(output._Result.SigmaZero, 0.5392593, 0.001);
        }
    }
}