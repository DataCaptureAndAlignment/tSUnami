﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Common.Elements;
using TSU.Common.Elements.Composites;
using TSU.Connectivity_Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Integration_Tests.GeoFit
{
    [TestClass()]
    public class GeoFitTests
    {
        [TestMethod()]
        public void CylinderTest()
        {
            string stream = @"1  35.988803  30.491070  175.33369
2  37.799574  -9.391037  160.50364
3  19.166165  -15.07568  148.18585
4  -7.748851  9.344080  143.32739
5  3.335899  39.368865  161.21561
6  91.781775  89.452080  45.967023
7  111.69559  66.935824  47.798389
8  59.763589  73.971409  22.536780
9  80.340195  36.857289  18.939971";
            CompositeElement p = TSU.IO.TheoreticalFile.TheoreticalElement(stream, "test", false);
            FittedShape shape = TSU.IO.SUSoft.GeoFit.Cylinder(p);
            Assert.AreEqual(0.448735, (shape.parametricShape as Cylinder).Vector.X.Value);
            Assert.AreEqual(-30.1147104, (shape.parametricShape as Cylinder).Radius.Value);

        }

        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [TestMethod()]
        public void FitL3dLineTest()
        {
            string stream = @"pt1	10.00	20.00	0.00	1.000
pt5	50.00	100.00	0.00	1.000
pt2	90.00	180.00	0.00	1.000
pt3	8.00	22.00	0.00	1.000
pt4	48.00	102.00	0.00	1.000
pt6	88.00	182.00	0.00	1.000";
            CompositeElement p = TSU.IO.TheoreticalFile.TheoreticalElement(stream, "test", false);
            FittedShape shape = TSU.IO.SUSoft.GeoFit.Line3D(p);
            Assert.AreEqual(0.4471406, (shape.parametricShape as Line3D).Vector.X.Value);
        }

        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [TestMethod()]
        public void FitPlaneTest()
        {
            string stream = @"CIE1	1815.661	635.083	    -576.336	1.000
CIE2	2000.018	807.089	    -576.090	1.000
CIE3	2252.335	798.133	    -575.892	1.000
CIE4	2424.419	613.597	    -576.307	1.000
CIE5	2415.301	361.424	    -575.890	1.000
CIE6	2231.014	189.565	    -575.922	1.000
CIE7	1978.620	198.510	    -576.263	1.000
CIE8	1806.529	382.834	    -576.188	1.000";
            CompositeElement p = TSU.IO.TheoreticalFile.TheoreticalElement(stream, "test", false);
            FittedShape shape = TSU.IO.SUSoft.GeoFit.Plane(p);
            Assert.AreEqual(577.6572665, (shape.parametricShape as Plane).d.Value);
        }

        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [TestMethod()]
        public void FitSphereTest()
        {
            
            string stream = @"1	2114.644	498.215	    -351.900	
2	2054.600	497.102	    -373.268	
3	2021.712	496.144	    -444.389	
4	2034.805	496.921	    -491.171	
5	2056.012	497.971	    -515.408	
6	2090.324	538.465	    -363.662	
7	2072.409	567.259	    -397.429	
8	2065.801	578.432	    -443.149	
9	2055.958	554.334	    -490.141	
10	2073.486	520.952	    -363.839	
11	2043.712	536.897	    -397.933	
12	2032.562	542.826	    -443.899			
13	2025.608	520.011	    -460.434			
14	2044.830	515.470	    -385.123			
15	2050.656	558.560	    -412.670			
16	2047.775	561.711	    -459.833			
17	2062.694	527.882	    -514.945			
18	2084.092	489.724	    -357.321";
            CompositeElement p = TSU.IO.TheoreticalFile.TheoreticalElement(stream, "test", false);
            FittedShape shape = TSU.IO.SUSoft.GeoFit.Sphere(p);
            Assert.AreEqual(2113.6862404, shape.parametricShape._Coordinates.Local.X.Value);
            Assert.AreEqual(91.9959635, (shape.parametricShape as Sphere).Radius.Value);
        }

        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [TestMethod()]
        public void FitCircle3DTest()
        {
            
            string stream = @" CCL1	1691.53357	-865.0046	-0.0440882	
CCL2	1679.06117	-843.3677	-0.08883209	
CCL3	1672.81182	-820.32613	-0.0297501	
CCL4	1672.84727	-795.9027	-0.00349477	
CCL5	1679.18421	-772.20171	-0.0565818	
CCL6	1691.31572	-751.12764	-0.03878051	";
            CompositeElement p = TSU.IO.TheoreticalFile.TheoreticalElement(stream, "test", false);
            FittedShape shape = TSU.IO.SUSoft.GeoFit.Circle(p);
            Assert.AreEqual(-807.9376944, (shape.parametricShape as Circle)._Coordinates.Local.Y.Value);
            Assert.AreEqual(93.2866528, (shape.parametricShape as Circle).Radius.Value);
            Assert.AreEqual(0.9999997, (shape.parametricShape as Circle).Vector.Z.Value);
        }
       
    }
}
