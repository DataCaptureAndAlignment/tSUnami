﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU;

using TSU.Common.Elements;
using M = TSU.Common.Measures;
using I = TSU.Common.Instruments;
using TSU.Common.Compute.Compensations;
using D = TSU.Common.Instruments.Device;
using TSU.ENUM;

using TSU.Line;
using TSU.Preferences;
using System.Runtime.InteropServices;
using static TSU.Common.TsunamiView;
using System;
using System.Collections.ObjectModel;
using TSU.Common.Instruments;
using TSU.Common.Measures;
using TSU.Common.Instruments.Reflector;

namespace Integration_Tests.LGC2
{
    [TestClass()]
    public class Lgc2Tests
    {
        [TestMethod()]
        public void Lgc2_Freestation_mla_Test()
        {
            TSU.Polar.Station station = Get4Test_PolarStation(haveNoccs: true, freestation: true);
            var strategy = new TSU.Polar.Compensations.FreeStation_LGC2();

            Lgc2 lgc2 = new Lgc2(station, strategy);
            lgc2.Run(false, false, true, 30);
            strategy.ParseOutPutAsTheodoliteResultsAsJSON(lgc2._Output);

            //
            //if was local
            Assert.AreEqual(lgc2._Result.vZero.Value, 352.854254, 0.001);
            Assert.AreEqual(lgc2._Result.vZero.Sigma, 0.003821, 0.001);
            Assert.AreEqual(lgc2._Result.VariablePoints[0]._Coordinates.Su.X.Value, 1.404452, 0.0001);

            Assert.AreEqual(lgc2._Result.VariablePoints[0]._Coordinates.Su.Z.Value, 0.46492, 0.0001);
            Assert.AreEqual(lgc2._Result.VariablePoints[0]._Coordinates.Su.X.Sigma, 0.000141, 0.1);
        }

        [TestMethod()]
        public void Lgc2_Freestation_ccs_Test()
        {
            TSU.Polar.Station station = Get4Test_PolarStation(haveNoccs: false, freestation: true);
            var strategy = new TSU.Polar.Compensations.FreeStation_LGC2();

            Lgc2 lgc2 = new Lgc2(station, strategy);
            lgc2.Run(false, false, true, 30);
            strategy.ParseOutPutAsTheodoliteResultsAsJSON(lgc2._Output);

            Assert.AreEqual(lgc2._Result.vZero.Value, 177.5884779, 0.001);
            Assert.AreEqual(lgc2._Result.vZero.Sigma, 0.0038181, 0.001);
            Assert.AreEqual(lgc2._Result.VariablePoints[0]._Coordinates.Ccs.X.Value, 4472.64026, 0.0001);
            Assert.AreEqual(lgc2._Result.VariablePoints[0]._Coordinates.Ccs.Z.Value, 331.64674, 0.0001);
            Assert.AreEqual(lgc2._Result.VariablePoints[0]._Coordinates.Ccs.X.Sigma, 0.000139, 0.0001);
        }

        [TestMethod()]
        public void Lgc2_stationOrientationConnue_ccs_Test_French()
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("fr-FR");

            TSU.Polar.Station station = Get4Test_PolarStation(haveNoccs: false, freestation: false);
            var strategy = new TSU.Polar.Compensations.Orientation_LGC2();

            Lgc2 lgc2 = new Lgc2(station, strategy);
            lgc2.Run(false, false, true, 30);
            strategy.ParseOutPutAsTheodoliteResultsAsJSON(lgc2._Output);

            Assert.AreEqual(177.5786, lgc2._Result.vZero.Value, 0.0001);
            Assert.AreEqual(0.0028, lgc2._Result.vZero.Sigma, 0.0001);
            Assert.AreEqual(0.24, lgc2._Result.InstrumentHeight.Value, 0.01);
        }

        [TestMethod()]
        public void Lgc2_stationOrientationConnue_ccs_Test_english()
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-EN");

            TSU.Polar.Station station = Get4Test_PolarStation(haveNoccs: false, freestation: false);

            var strategy = new TSU.Polar.Compensations.Orientation_LGC2();
            Lgc2 lgc2 = new Lgc2(station, strategy);
            lgc2.Run(false, false, true, 30);
            strategy.ParseOutPutAsTheodoliteResultsAsJSON(lgc2._Output);

            Assert.AreEqual(177.5786, lgc2._Result.vZero.Value, 0.0001);
            Assert.AreEqual(0.0028, lgc2._Result.vZero.Sigma, 0.0001);
            Assert.AreEqual(0.24, lgc2._Result.InstrumentHeight.Value, 0.01);
        }

        [TestMethod()]
        public void Lgc2_stationOrientationConnue_rs2k_Test()
        {
            TSU.Polar.Station station = Get4Test_PolarStation(haveNoccs: false, freestation: false);

            var strategy = new TSU.Polar.Compensations.Orientation_LGC2();
            Lgc2 lgc2 = new Lgc2(station, strategy);
            lgc2.Run(false, false, true, 30);
            strategy.ParseOutPutAsTheodoliteResultsAsJSON(lgc2._Output);

            Assert.AreEqual(177.5786, lgc2._Result.vZero.Value, 0.0001);
            Assert.AreEqual(0.0028, lgc2._Result.vZero.Sigma, 0.0001);
            Assert.AreEqual(0.24, lgc2._Result.InstrumentHeight.Value, 0.01);
        }

        [TestMethod()]
        public void Lgc2_stationOrientationConnue_mla_Test()
        {
            TSU.Polar.Station station = Get4Test_PolarStation(haveNoccs: true, freestation: false);

            var strategy = new TSU.Polar.Compensations.Orientation_LGC2();
            Lgc2 lgc2 = new Lgc2(station, strategy);
            lgc2.Run(false, false, true, 30);
            strategy.ParseOutPutAsTheodoliteResultsAsJSON(lgc2._Output);

            Assert.AreEqual(352.8440023, lgc2._Result.vZero.Value, 0.0001);
            Assert.AreEqual(0.002854, lgc2._Result.vZero.Sigma, 0.0001);
            Assert.AreEqual(0.2400975, lgc2._Result.InstrumentHeight.Value, 0.01);
        }

        private TSU.Polar.Station Get4Test_PolarStation(bool haveNoccs, bool freestation)
        {
            TSU.Polar.Station pst = new TSU.Polar.Station();
            pst.Parameters2 = Get4Test_FilledMockStationsParameters(haveNoccs, freestation);

            pst.MeasuresTaken.Add(Get4Test_FilledMockMeasureTheodolite(1, haveNoccs));
            pst.MeasuresTaken.Add(Get4Test_FilledMockMeasureTheodolite(2, haveNoccs));
            pst.MeasuresTaken.Add(Get4Test_FilledMockMeasureTheodolite(3, haveNoccs));

            return pst;
        }

        private M.Measure Get4Test_FilledMockMeasureTheodolite(int refencePointNumber, bool haveNoccs, TSU.IO.SUSoft.Geode.Roles roles = TSU.IO.SUSoft.Geode.Roles.Cala)
        {
            TSU.Polar.Measure pm = new TSU.Polar.Measure();
            pm.CommentFromUser = "Blabla";
            pm._Date = DateTime.Now;
            //pm._Station._Parameters._Instrument = (Sensor)new FilledMockInstrument().Object);
            pm.GeodeRole = TSU.IO.SUSoft.Geode.Roles.Cala;
            pm._OriginalPoint = Get4Test_FilledMockPoint(refencePointNumber, haveNoccs);
            pm._Point = Get4Test_FilledMockPoint(refencePointNumber, haveNoccs);
            pm._Status = new M.States.Good();
            pm.Extension = new DoubleValue(0, 0);
            pm.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates = new DoubleValue(0, 0);
            pm.Angles = Get4Test_FilledMockMeasureOfAngles(refencePointNumber, I.FaceType.DoubleFace);
            pm.Distance = Get4Test_FilledMockMeasureOfDistance(refencePointNumber, haveNoccs);

            return pm;
        }

        private MeasureOfDistance Get4Test_FilledMockMeasureOfDistance(int refencePointNumber, bool haveNoccs)
        {
            MeasureOfDistance m = new MeasureOfDistance();

            m._Date = (DateTime.Now);
            m._InstrumentSN = ("123");
            m._Point = Get4Test_FilledMockPoint(0, haveNoccs);
            m._Status = new M.States.Good();

            switch (refencePointNumber)
            {
                case 1:
                    m.Raw = Get4Test_FilledMockDoubleValue(3.089418, 0.0005);
                    break;
                case 2:
                    m.Raw = Get4Test_FilledMockDoubleValue(2.493948, 0.0005);
                    break;
                case 3:
                    m.Raw = Get4Test_FilledMockDoubleValue(9.580441, 0.0005);
                    break;
                default:
                    break;
            }
            m.Corrected = m.Raw;

            m.Reflector = Get4Test_FilledMockReflector();
            m.WeatherConditions = Get4Test_FilledMockWeatherConditions();
            m.MeasurementMode = DistanceMeasurementMode.WithReflector;
            m.IsCorrectedForEtalonnage = (true);
            m.IsCorrectedForPrismConstanteValue = (true);
            m.IsCorrectedForWeatherConditions = (true);

            return m;
        }

        private WeatherConditions Get4Test_FilledMockWeatherConditions()
        {
            WeatherConditions wc = new WeatherConditions();
            wc.dryTemperature = (21);
            wc.humidity = (65);
            wc.pressure = (980);
            wc.WetTemperature = (22);
            return wc;
        }

        private Reflector Get4Test_FilledMockReflector()
        {
            Reflector relector = new Reflector();
            relector._Brand = ("Leica");
            relector.Id = ("RRR15");
            relector._Name = ("RRR15");
            relector._InstrumentType = (I.InstrumentTypes.PBT);
            relector._Model = ("RRR");
            relector._SerialNumber = ("3");
            return relector;
        }

        private MeasureOfAngles Get4Test_FilledMockMeasureOfAngles(int refencePointNumber, FaceType face)
        {
            MeasureOfAngles m = new MeasureOfAngles();
            m.Compensator = (new I.Compensator());
            m.Raw = Get4Test_FilledMockAngles(refencePointNumber, face);
            m.Corrected = m.Raw;
            //m.CorrectForCollimation()).Returns(new Result());
            //Mock<M.MeasureOfAngles> copyMock = this;
            //this.Setup(m => m.Clone()).Returns(copyMock.Object);
            return m;
        }

        private Angles Get4Test_FilledMockAngles(int refencePointNumber, FaceType face)
        {
            Angles angles = new Angles();

            DoubleValue h;
            DoubleValue v;
            DoubleValue hm;
            ////DoubleValue vm;

            switch (refencePointNumber)
            {
                case 1:
                    if (face == I.FaceType.Face2)
                    {
                        h = new DoubleValue(96.03283, 0.0003);
                        v = new DoubleValue(267.52213, 0.0003);
                    }
                    else
                    {
                        h = new DoubleValue(296.03281, 0.0003);
                        v = new DoubleValue(132.47785, 0.0003);
                    }
                    hm = new DoubleValue(296.03282, 0.0003);
                    hm = new DoubleValue(132.47786, 0.0003);
                    break;
                case 2:
                    if (face == I.FaceType.Face2)
                    {
                        h = new DoubleValue(50.85947, 0.0003);
                        v = new DoubleValue(857.08639, 0.0003);
                    }
                    else
                    {
                        h = new DoubleValue(250.85947, 0.0003);
                        v = new DoubleValue(142.91361, 0.0003);
                    }
                    break;
                case 3:
                    if (face == I.FaceType.Face2)
                    {
                        h = new DoubleValue(59.13208, 0.0003);
                        v = new DoubleValue(897.29585, 0.0003);
                    }
                    else
                    {
                        h = new DoubleValue(359.13208, 0.0003);
                        v = new DoubleValue(102.70415, 0.0003);
                    }

                    break;
                default:
                    h = new DoubleValue(100, 0.00001);
                    v = new DoubleValue(100, 0.00001);
                    break;
            }
            angles.Horizontal = h;
            angles.Vertical = v;
            return angles;
        }

        private Point Get4Test_FilledMockPoint(int refencePointNumber, bool haveNoccs)
        {
            Point point = new Point();
            point._Name = "LHC.GGPSO.3R8.E";
            point._Accelerator = "LHC";
            point.fileElementType = ElementType.Point;
            point._Zone = "LHC";
            point._Coordinates.Local = Get4Test_FilledMockMlaCoordinates(refencePointNumber);
            if (!haveNoccs)
                point._Coordinates.Ccs = Get4Test_FilledMockCcsCoordinates(refencePointNumber);
            point._Coordinates.UserDefined = Get4Test_FilledMockMlaCoordinates(refencePointNumber);
            point._Coordinates.Su = Get4Test_FilledMockMlaCoordinates(refencePointNumber);
            point._Coordinates.Physicist = Get4Test_FilledMockMlaCoordinates(refencePointNumber);
            point._Zone = "LHC";
            switch (refencePointNumber)
            {
                case 1:
                    point._Name = "PS03";
                    break;
                case 2:
                    point._Name = "PS11";
                    break;
                case 3:
                    point._Name = "PS30";
                    break;
                case 4:
                    point._Name = "ST04";
                    break;
                default:
                    break;
            }
            return point;
        }

        private Coordinates Get4Test_FilledMockCcsCoordinates(int refencePointNumber)
        {
            Coordinates coordinates = new Coordinates();
            switch (refencePointNumber)
            {

                case 0:
                    Random rnd = new Random(Guid.NewGuid().GetHashCode());
                    coordinates.X = Get4Test_FilledMockDoubleValue(rnd.Next(-10000, 10000) / 1000, rnd.Next(-10000, 10000) / 1000);
                    coordinates.Y = Get4Test_FilledMockDoubleValue(rnd.Next(-10000, 10000) / 1000, rnd.Next(-10000, 10000) / 1000);
                    coordinates.Z = Get4Test_FilledMockDoubleValue(rnd.Next(-10000, 10000) / 1000, rnd.Next(-10000, 10000) / 1000);
                    break;
                case 1:
                    coordinates.X = Get4Test_FilledMockDoubleValue(4475.1077099, 0.0000);
                    coordinates.Y = Get4Test_FilledMockDoubleValue(5011.0441434, 0.0000);
                    coordinates.Z = Get4Test_FilledMockDoubleValue(330.1378852, 0.00005);
                    break;
                case 2:
                    coordinates.X = Get4Test_FilledMockDoubleValue(4473.4818009, 0.00005);
                    coordinates.Y = Get4Test_FilledMockDoubleValue(5011.7159601, 0.00005);
                    coordinates.Z = Get4Test_FilledMockDoubleValue(330.0898154, 0.00005);
                    break;
                case 3:
                    coordinates.X = Get4Test_FilledMockDoubleValue(4480.6630608, 0.00005);
                    coordinates.Y = Get4Test_FilledMockDoubleValue(5004.7392708, 0.00005);
                    coordinates.Z = Get4Test_FilledMockDoubleValue(331.2409478, 0.00005);
                    break;
                case 4:
                    coordinates.X = Get4Test_FilledMockDoubleValue(4472.6404349, 0.00005);
                    coordinates.Y = Get4Test_FilledMockDoubleValue(5009.9587199, 0.00005);
                    coordinates.Z = Get4Test_FilledMockDoubleValue(331.4067339, 0.00005);
                    break;
                default:
                    break;
            }
            return coordinates;
        }

        private Coordinates Get4Test_FilledMockMlaCoordinates(int refencePointNumber)
        {
            Coordinates coordinates = new Coordinates();
            switch (refencePointNumber)
            {

                case 0:
                    Random rnd = new Random(Guid.NewGuid().GetHashCode());
                    coordinates.X = Get4Test_FilledMockDoubleValue(rnd.Next(-10000, 10000) / 1000, rnd.Next(-10000, 10000) / 1000);
                    coordinates.Y = Get4Test_FilledMockDoubleValue(rnd.Next(-10000, 10000) / 1000, rnd.Next(-10000, 10000) / 1000);
                    coordinates.Z = Get4Test_FilledMockDoubleValue(rnd.Next(-10000, 10000) / 1000, rnd.Next(-10000, 10000) / 1000);
                    break;
                case 1:
                    coordinates.X = Get4Test_FilledMockDoubleValue(-0.468410, 0.00005);
                    coordinates.Y = Get4Test_FilledMockDoubleValue(-5.671920, 0.00005);
                    coordinates.Z = Get4Test_FilledMockDoubleValue(-1.043850, 0.00005);
                    break;
                case 2:
                    coordinates.X = Get4Test_FilledMockDoubleValue(1.290810, 0.00005);
                    coordinates.Y = Get4Test_FilledMockDoubleValue(-5.677750, 0.00005);
                    coordinates.Z = Get4Test_FilledMockDoubleValue(-1.091920, 0.00005);
                    break;
                case 3:
                    coordinates.X = Get4Test_FilledMockDoubleValue(-7.997980, 0.00005);
                    coordinates.Y = Get4Test_FilledMockDoubleValue(-1.940870, 0.00005);
                    coordinates.Z = Get4Test_FilledMockDoubleValue(0.059210, 0.00005);
                    break;
                case 4:
                    coordinates.X = Get4Test_FilledMockDoubleValue(1.404, 0.00005);
                    coordinates.Y = Get4Test_FilledMockDoubleValue(-3.732, 0.00005);
                    coordinates.Z = Get4Test_FilledMockDoubleValue(0.225, 0.00005);
                    break;
                default:
                    break;
            }
            return coordinates;
        }

        private DoubleValue Get4Test_FilledMockDoubleValue(double v1, double v2)
        {
            DoubleValue dv = new DoubleValue(v1, v2);
            return dv;
        }

        private TSU.Polar.Station.Parameters Get4Test_FilledMockStationsParameters(bool haveNoccs, bool freestation)
        {
            TSU.Polar.Station.Parameters @params = new TSU.Polar.Station.Parameters();
            @params._Date = DateTime.Now;
            @params._Instrument = (Sensor)Get4Test_FilledMockPolarInstrument();
            @params._IsSetup = true;
            @params._Operation = new TSU.Common.Operations.Operation(10356, "");
            @params._Team = "PS-DEP";
            Point p = Get4Test_FilledMockPoint(4, haveNoccs); ;
            var setup = new TSU.Polar.Station.Parameters.Setup.Values()
            {
                InstrumentHeight = new DoubleValue(0, 0),
                IsInstrumentHeightKnown = freestation ? TSU.Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight.Known :
                TSU.Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight.Unknown,
                StationPoint = p
            };
            @params.Setups = new TSU.Polar.Station.Parameters.Setup();
            @params.Setups.InitialValues = setup;
            return @params;
        }

        private IPolarInstrument Get4Test_FilledMockPolarInstrument()
        {
            TSU.Common.Instruments.Device.AT40x.Instrument m = new TSU.Common.Instruments.Device.AT40x.Instrument();
            m._Brand = ("Leica");
            m.Id = ("AT402-359056");
            m._Name = ("AT402-359056");
            m._InstrumentClass = I.InstrumentClasses.THEODOLITE;
            m._Model = "At402";
            m._SerialNumber = "359056";
            return m;
        }

        [TestMethod()]
        public void Lgc2_PointLancé_mla_Test()
        {
            TSU.Polar.Station s = new TSU.Polar.Station();
            s.Parameters2._StationPoint = new Point("lastation");
            s.Parameters2._StationPoint._Coordinates.Local = new Coordinates("Unknwon", 0, 0, 0);
            s.Parameters2.Setups.FinalValues = new TSU.Polar.Station.Parameters.Setup.Values();
            s.Parameters2.Setups.FinalValues.vZero = new DoubleValue(120.0, 0.0003);
            s.Parameters2._Instrument = new D.AT40x.Instrument() { _Model = "At401" };
            s.Parameters2.Setups.InitialValues.IsInstrumentHeightKnown = TSU.Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight.Known;
            s.Parameters2.Setups.InitialValues.InstrumentHeight = new DoubleValue(0, 0);
            var strategy = new TSU.Polar.Compensations.PointLancéLGC2();

            //create measure
            TSU.Polar.Measure m = new TSU.Polar.Measure()
            {
                _Status = new M.States.Good(),
                Angles = new M.MeasureOfAngles()
                {
                    Corrected = new Angles()
                    {
                        Horizontal = new DoubleValue(150, 0.0001),
                        Vertical = new DoubleValue(90, 0.0002)
                    }
                },
                Distance = new M.MeasureOfDistance()
                {
                    Corrected = new DoubleValue(100, 0.001),
                    Reflector = new I.Reflector.Reflector() { _Model = "RRR" }
                },
            };


            // create pointlancé
            Point Lancé = new Point("pointlancé")
            {
                _Coordinates = new CoordinatesInAllSystems()
                {
                    Local = TSU.Common.Compute.Survey.GetPointLancé(m, s)
                }
            };
            m._Point = Lancé;

            s.MeasuresTaken.Add(m);

            Lgc2 lgc2 = new Lgc2(s, strategy);
            lgc2.Run(false, false, true, 30);

            strategy.ParseOutPutAsTheodoliteResultsAsJSON(lgc2._Output);
            Assert.AreEqual(lgc2._Result.vZero.Value, 120.0000, 0.001);
            Assert.AreEqual(lgc2._Result.InstrumentHeight.Value, 0.0, 0.001);
            Assert.AreEqual(-88.0036755, lgc2._Result.VariablePoints[0]._Coordinates.Su.X.Value, 0.00001);
            Assert.AreEqual(0.0, lgc2._Result.VariablePoints[0]._Coordinates.Su.X.Sigma, 0.00001);
            Assert.AreEqual(-44.84011235, lgc2._Result.VariablePoints[0]._Coordinates.Su.Y.Value, 0.00001);
            Assert.AreEqual(0.0, lgc2._Result.VariablePoints[0]._Coordinates.Su.Y.Sigma, 0.00001);
            Assert.AreEqual(15.6434465, lgc2._Result.VariablePoints[0]._Coordinates.Su.Z.Value, 0.00001);
            Assert.AreEqual(0.0, lgc2._Result.VariablePoints[0]._Coordinates.Su.Z.Sigma, 0.00001);
        }
    }
}
