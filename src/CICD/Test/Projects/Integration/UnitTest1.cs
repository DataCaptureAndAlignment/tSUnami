﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TSU;

namespace Integration_Tests
{
    [TestClass]
    public class UnitTest1
    {
        [AssemblyInitialize]
        //[ClassInitialize]
        public static void CreateTsunamiObject(TestContext testContext)
        {
            Tsunami2.InitializePropertiesForUnitTesting();
            Tsunami2.InitializeTsunamiViewForUnitTesting();
            TSU.Preferences.Preferences.Instance.InitializeWithAllXmlParametersFiles();
            var prop = Tsunami2.Properties;
            var pref = Tsunami2.Preferences.Values;
        }


        [AssemblyCleanup]
        public static void AssemblyCleanup()
        {

        }
    }
}
