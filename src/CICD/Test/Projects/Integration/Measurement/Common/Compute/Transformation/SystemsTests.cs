﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Compute.Transformation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TSU.Common.Compute.Compensations.Strategies;
using static TSU.Common.Compute.Transformation.Systems;
using TSU.Common.Elements;

namespace TSU.Common.Compute.Transformation.Tests2
{
    [TestClass()]
    public class SystemsTests
    {
        [DeploymentItem(@"libs\SuSoft\", @"libs\SuSoft\")]
        [TestMethod()]
        public void HtoZTestWithOldSpatialObjectDll()
        {
            double z = 0;

            //z = HtoZ(2000, 2000, 300, "CG2000");    Assert.AreEqual(2300.0000349844845, z, 0.00001);
            //z = HtoZ(2000, 2000, 300, "CG1985");    Assert.AreEqual(2300.0000306471925, z, 0.00001);
            //z = HtoZ(2000, 2000, 300, "CG1985N0");  Assert.AreEqual(2300.0000311127224, z, 0.00001);
            //z = HtoZ(2000, 2000, 300, "CG2000N0");  Assert.AreEqual(2300.0000249844843, z, 0.00001);
            //z = HtoZ(2000, 2000, 300, "SPHERE");    Assert.AreEqual(2300.0000349844845, z, 0.00001);

            //z = HtoZ(-1000, 10000, 300, "CG2000");   Assert.AreEqual(2294.4218894317492, z, 0.00001);
            //z = HtoZ(-1000, 10000, 300, "CG1985");   Assert.AreEqual(2294.4162117998994, z, 0.00001);
            //z = HtoZ(-1000, 10000, 300, "CG1985N0"); Assert.AreEqual(2294.4139070630513, z, 0.00001);
            //z = HtoZ(-1000, 10000, 300, "CG2000N0"); Assert.AreEqual(2294.4217694316435, z, 0.00001);
            //z = HtoZ(-1000, 10000, 300, "SPHERE");   Assert.AreEqual(2294.3929794062988, z, 0.00001);

            z = HtoZ(-1000, 10000, 300, Coordinates.ReferenceSurfaces.RS2K, out _);             Assert.AreEqual(2294.4218894317492, z, 0.00001);
            z = HtoZ(-1000, 10000, 300, Coordinates.ReferenceSurfaces.Machine1985, out _);      Assert.AreEqual(2294.4162117998994, z, 0.00001);
            z = HtoZ(-1000, 10000, 300, Coordinates.ReferenceSurfaces.SurfaceTopo1985, out _);  Assert.AreEqual(2294.4139070630513, z, 0.00001);
            // m                                                                                            2294.4217694316435
            z = HtoZ(-1000, 10000, 300, Coordinates.ReferenceSurfaces.Sphere, out _);           Assert.AreEqual(2294.39401332661, z, 0.00001);
        }

        [DeploymentItem(@"libs\SuSoft\", @"libs\SuSoft\")]
        [TestMethod()]
        public void HtoZtoHTestWithOldSpatialObjectDll()
        {
            double z = 0;

            // old way with a
            //z = ZToH(-1000, 10000, HtoZ(-1000, 10000, 300, "CG2000"), "CG2000"); Assert.AreEqual(300, z, 0.00001);
            //z = ZToH(-1000, 10000, HtoZ(-1000, 10000, 300, "CG1985"), "CG1985"); Assert.AreEqual(300, z, 0.00001);
            //z = ZToH(-1000, 10000, HtoZ(-1000, 10000, 300, "CG1985N0"), "CG1985N0"); Assert.AreEqual(300, z, 0.00001);
            //z = ZToH(-1000, 10000, HtoZ(-1000, 10000, 300, "CG2000N0"), "CG2000N0"); Assert.AreEqual(300, z, 0.00001);
            //z = ZToH(-1000, 10000, HtoZ(-1000, 10000, 300, "SPHERE"), "SPHERE"); Assert.AreEqual(300, z, 0.00001);


            z = ZToH(-1000, 10000, HtoZ(-1000, 10000, 300, Coordinates.ReferenceSurfaces.RS2K, out _), Coordinates.ReferenceSurfaces.RS2K); Assert.AreEqual(300, z, 0.00001);
            z = ZToH(-1000, 10000, HtoZ(-1000, 10000, 300, Coordinates.ReferenceSurfaces.Machine1985, out _), Coordinates.ReferenceSurfaces.Machine1985); Assert.AreEqual(300, z, 0.00001);
            z = ZToH(-1000, 10000, HtoZ(-1000, 10000, 300, Coordinates.ReferenceSurfaces.SurfaceTopo1985, out _), Coordinates.ReferenceSurfaces.SurfaceTopo1985); Assert.AreEqual(300, z, 0.00001);
            // missing
            z = ZToH(-1000, 10000, HtoZ(-1000, 10000, 300, Coordinates.ReferenceSurfaces.Sphere, out _), Coordinates.ReferenceSurfaces.Sphere); Assert.AreEqual(300, z, 0.00001);

        }

        [DeploymentItem(@"libs\SuSoft\", @"libs\SuSoft\")]
        [TestMethod()]
        public void ZtoHTestWithOldSpatialObjectDll()
        {
            double z = 0;

            //old
            //z = ZToH(2000, 2000, 2300, "CG2000");   Assert.AreEqual(299.99996001551517, z, 0.00001);
            //z = ZToH(2000, 2000, 2300, "CG1985");   Assert.AreEqual(299.99996435280735, z, 0.00001);
            //z = ZToH(2000, 2000, 2300, "CG1985N0"); Assert.AreEqual(299.99996388727732, z, 0.00001);
            //z = ZToH(2000, 2000, 2300, "CG2000N0"); Assert.AreEqual(299.99997001551515, z, 0.00001);
            //z = ZToH(2000, 2000, 2300, "SPHERE");   Assert.AreEqual(299.999960506364, z, 0.00001);

            //z = ZToH(-1000, 10000, 2294.4218894317492, "CG2000");   Assert.AreEqual(300.0, z, 0.00001);
            //z = ZToH(-1000, 10000, 2294.4162117998994, "CG1985");   Assert.AreEqual(300.0, z, 0.00001);
            //z = ZToH(-1000, 10000, 2294.4139070630513, "CG1985N0"); Assert.AreEqual(300.0, z, 0.00001);
            //z = ZToH(-1000, 10000, 2294.4217694316435, "CG2000N0"); Assert.AreEqual(300.0, z, 0.00001);
            //z = ZToH(-1000, 10000, 2294.3929794062988, "SPHERE");   Assert.AreEqual(300.0, z, 0.00001);

            z = ZToH(-1000, 10000, 2294.4218894317492, Coordinates.ReferenceSurfaces.RS2K); Assert.AreEqual(300, z, 0.00001);
            z = ZToH(-1000, 10000, 2294.4162117998994, Coordinates.ReferenceSurfaces.Machine1985); Assert.AreEqual(300, z, 0.00001);
            z = ZToH(-1000, 10000, 2294.4139070630513, Coordinates.ReferenceSurfaces.SurfaceTopo1985); Assert.AreEqual(300, z, 0.00001);
            // missing                2294.4217694316435
            z = ZToH(-1000, 10000, 2294.39401332661, Coordinates.ReferenceSurfaces.Sphere); Assert.AreEqual(300, z, 0.00001);

        }
    }
}