using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Compute.Compensations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static TSU.Common.Compute.Compensations.Frame.FrameMeasurement;
using System.Net;
using TSU.Common.Compute.Transformation;

namespace TSU.Common.Compute.Compensations.Tests
{
    [TestClass()]
    public class FrameTests
    {
        [TestMethod()]
        public void GetFromStringContainingOnlyOneFrameTest()
        {
            string source = @"*FRAME RSTI_103035_LHC.MQML.5L5  -1231.090090  10389.569100   2416.394520 0 0   74.733794 1
*FRAME RST_103035_LHC.MQML.5L5 0 0 0 .7911910531 -.41577637333 0 1
*FRAME RSTRI_103035_LHC.MQML.5L5     -0.001198      0.001174     -0.000592 .00074272462 0 -.00045093994 1 TX TY TZ RX RZ 
*FRAME RSTR_103035_LHC.MQML.5L5 0 0 0 0 0.001336902 0 1 RY
*CALA
BEAM_LHC.MQML.5L5.S                                             -0.000005      4.800002      0.000003   $13135.200 693826 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.BPMR.5L5.E                                             -0.000003     -0.745000      0.000001   $13129.655 683395 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.BPMR.5L5.S                                             -0.000003     -0.745000      0.000001   $13129.655 683396 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MQML.5L5.E                                              0.000000      0.000000      0.000000   $13130.400 693825 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCV.5L5.E                                            -0.000004      4.990007      0.000003   $13135.390 804768 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCV.5L5.S                                            -0.000005      5.894007      0.000003   $13136.294 804769 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCA.5L5.E                                            -0.000004      4.990007      0.000003   $13135.390 686523 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCA.5L5.S                                            -0.000005      5.894007      0.000003   $13136.294 686524 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
LHC.MQML.5L5.T     -0.360470      4.306890      0.420950 0.1 0.1 0.1   $13134.707 516196 paramètres RST 23-FEB-2023 00:00:00
*OBSXYZ
LHC.MQML.5L5.E      0.168800      0.601580      0.421630 0.1 0.1 0.1   $13131.001 516195 paramètres RST 23-FEB-2023 00:00:00
LHC.MQML.5L5.S      0.167070      4.306810      0.420700 0.1 0.1 0.2   $13134.706 516197 paramètres RST 23-FEB-2023 00:00:00
%LHC.MQML.5L5.T     -0.360470      4.306890      0.420950 0.1 0.1 0.1   $13134.707 516196 paramètres RST 23-FEB-2023 00:00:00
*INCLY W1015
LHC.MQML.5L5.E  -0.533487369 RF -0.030169411
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*END
";
            Frame frame = Frame.GetFromStringContainingOnlyOneFrame(source);
            Assert.AreEqual("RSTI_103035_LHC.MQML.5L5", frame._Name);

            Assert.AreEqual("RSTR_103035_LHC.MQML.5L5", frame.DeeperFrame._Name);
            Assert.AreEqual(0.000003, frame.DeeperFrame.CalagePoints[0].Z.Value, 0.00001);
            Assert.AreEqual(0.420950, frame.DeeperFrame.CalagePoints[8].Z.Value, 0.00001);
            var obsxyz = frame.DeeperFrame.Measurements.GetObsXyzByName("LHC.MQML.5L5.S");
            Assert.AreEqual(0.0002, obsxyz.Z.Sigma, 0.0001);

        }

        [TestMethod()]
        public void CreateAFullRootFrameFromLGCInputFormatStringTest()
        {
            string frameContent = @"LHC.MQML.5L5.E                                          10.646851 TH  0.070000   $1504239 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                         16.716183 TH  0.000000   $1504240 - 13-FEB-2023 - TSU: 'Control 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm,Off.: dH=0.9cc, dV=0.8cc, dD=0mm' - Was Control according to Tsunami
*FRAME RSTI_103035_LHC.MQML.5L5  -1231.090090  10389.569100   2416.394520 0 0   74.733794 1
*FRAME RST_103035_LHC.MQML.5L5 0 0 0 .7911910531 -.41577637333 0 1
*FRAME RSTRI_103035_LHC.MQML.5L5     -0.001198      0.001174     -0.000592 .00074272462 0 -.00045093994 1 TX TY TZ RX RZ 
*FRAME RSTR_103035_LHC.MQML.5L5 0 0 0 0 0.001336902 0 1 RY
*CALA
BEAM_LHC.MQML.5L5.S                                             -0.000005      4.800002      0.000003   $13135.200 693826 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.BPMR.5L5.E                                             -0.000003     -0.745000      0.000001   $13129.655 683395 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.BPMR.5L5.S                                             -0.000003     -0.745000      0.000001   $13129.655 683396 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MQML.5L5.E                                              0.000000      0.000000      0.000000   $13130.400 693825 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCV.5L5.E                                            -0.000004      4.990007      0.000003   $13135.390 804768 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCV.5L5.S                                            -0.000005      5.894007      0.000003   $13136.294 804769 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCA.5L5.E                                            -0.000004      4.990007      0.000003   $13135.390 686523 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCA.5L5.S                                            -0.000005      5.894007      0.000003   $13136.294 686524 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
% LHC.MQML.5L5.T     -0.360470      4.306890      0.420950 0.1 0.1 0.1   $13134.707 516196 paramètres RST 23-FEB-2023 00:00:00
*OBSXYZ
LHC.MQML.5L5.E      0.168800      0.601580      0.421630 0.1 0.1 0.1   $13131.001 516195 paramètres RST 23-FEB-2023 00:00:00
LHC.MQML.5L5.S      0.167070      4.306810      0.420700 0.1 0.1 0.2   $13134.706 516197 paramètres RST 23-FEB-2023 00:00:00
*INCLY W1015
LHC.MQML.5L5.E  -0.533487369 RF -0.030169411
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME

*FRAME RSTI_103035_LHC.MQML.5L5  -1231.090090  10389.569100   2416.394520 0 0   74.733794 1
*FRAME RST_103035_LHC.MQML.5L5 0 0 0 .7911910531 -.41577637333 0 1
*FRAME RSTRI_103035_LHC.MQML.5L5     -0.001198      0.001174     -0.000592 .00074272462 0 -.00045093994 1 TX TY TZ RX RZ 
*FRAME RSTR_103035_LHC.MQML.5L5 0 0 0 0 0.001336902 0 1 RY
*CALA
BEAM_LHC.MQML.5L5.S                                             -0.000005      4.800002      0.000003   $13135.200 693826 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.BPMR.5L5.E                                             -0.000003     -0.745000      0.000001   $13129.655 683395 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.BPMR.5L5.S                                             -0.000003     -0.745000      0.000001   $13129.655 683396 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MQML.5L5.E                                              0.000000      0.000000      0.000000   $13130.400 693825 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCV.5L5.E                                            -0.000004      4.990007      0.000003   $13135.390 804768 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCV.5L5.S                                            -0.000005      5.894007      0.000003   $13136.294 804769 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCA.5L5.E                                            -0.000004      4.990007      0.000003   $13135.390 686523 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCA.5L5.S                                            -0.000005      5.894007      0.000003   $13136.294 686524 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
% LHC.MQML.5L5.T     -0.360470      4.306890      0.420950 0.1 0.1 0.1   $13134.707 516196 paramètres RST 23-FEB-2023 00:00:00
*OBSXYZ
LHC.MQML.5L5.E      0.168800      0.601580      0.421630 0.1 0.1 0.1   $13131.001 516195 paramètres RST 23-FEB-2023 00:00:00
LHC.MQML.5L5.S      0.167070      4.306810      0.420700 0.1 0.1 0.2   $13134.706 516197 paramètres RST 23-FEB-2023 00:00:00
*INCLY W1015
LHC.MQML.5L5.E  -0.533487369 RF -0.030169411
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
% *FRAME RSTI_103036_LHC.B2.TCL.5L5  -1218.223830  10395.171970   2416.221390 0 0   74.733794 1
% *FRAME RST_103036_LHC.B2.TCL.5L5 0 0 0 .7911910531 -.4197870779 0 1
";
            Frame frame = Frame.FromLGCInputFormat_GetRootFrameInputFormatString(frameContent);
            var a = frame.DeeperFrame;
            Assert.AreEqual("RSTR_103035_LHC.MQML.5L5", frame.SubFrames[0].SubFrames[0].SubFrames[0].SubFrames[0]._Name);
            Assert.AreEqual(0.42070, frame.SubFrames[0].SubFrames[0].SubFrames[0].SubFrames[0].Measurements.ObsXyzs[1].Z.Value);
            Assert.AreEqual(0.0002, frame.SubFrames[0].SubFrames[0].SubFrames[0].SubFrames[0].Measurements.ObsXyzs[1].Z.Sigma);
            Assert.AreEqual(-0.030169411, frame.SubFrames[0].SubFrames[0].SubFrames[0].SubFrames[0].Measurements.Inclinaisons[0].ReferenceAngle.Value/Math.PI*200, 0.000001);
            Assert.AreEqual(-0.533487369, frame.SubFrames[0].SubFrames[0].SubFrames[0].SubFrames[0].Measurements.Inclinaisons[0].Roll.Value / Math.PI * 200, 0.000001);
        }

        [TestMethod()]
        public void CreateLgcInputFrameTest()
        {
            string expectedcontent = @"*FRAME RSTRI_103035_LHC.MQML.5L5   -0.001198   0.001174   -0.000592   0.000743   0.000000   -0.000451   1.000000   TX   TY   TZ   RX   RZ 
*FRAME RSTR_103035_LHC.MQML.5L5   0.000000   0.000000   0.000000   0.000000   0.001336902   0.000000   1   RY
*CALA
BEAM_LHC.MCBCA.5L5.E   -0.000004   4.990007   0.000003   $bla
BEAM_LHC.MCBCA.5L5.S   -0.000005   5.894007   0.000003   $bla
% LHC.MQML.5L5.T   -0.360470   4.306890   0.420950   0.1   0.1   0.1   $bla
*OBSXYZ
LHC.MQML.5L5.E   0.168800   0.601580   0.421630   0.1   0.1   0.1   $bla
LHC.MQML.5L5.S   0.167070   4.306810   0.420700   0.1   0.1   0.2   $bla
*INCLY W1015
LHC.MQML.5L5.E   -0.533487369   RF   -0.030169411
*ENDFRAME
*ENDFRAME
";
            Frame root = new Frame()
            {
                _Name = "RSTRI_103035_LHC.MQML.5L5",
                ProvisoryParameters = new Frame.Parameters()
                {
                    TX = new Frame.Parameters.Parameter() { Value = -0.001198, IsFixed = false },
                    TY = new Frame.Parameters.Parameter() { Value = 0.001174, IsFixed = false },
                    TZ = new Frame.Parameters.Parameter() { Value = -0.000592, IsFixed = false },
                    RX = new Frame.Parameters.Parameter() { Value = .00074272462, IsFixed = false },
                    RY = new Frame.Parameters.Parameter() { Value = 0, IsFixed = true },
                    RZ = new Frame.Parameters.Parameter() { Value = -.000450939948, IsFixed = false },
                    Scale = new Frame.Parameters.Parameter() { Value = 1, IsFixed = true },
                },
                Active = true,
            };
            root.SubFrames.Add(new Frame()
            {
                _Name = "RSTR_103035_LHC.MQML.5L5",
                ProvisoryParameters = new Frame.Parameters()
                {
                    TX = new Frame.Parameters.Parameter() { Value = 0, IsFixed = true },
                    TY = new Frame.Parameters.Parameter() { Value = 0, IsFixed = true },
                    TZ = new Frame.Parameters.Parameter() { Value = 0, IsFixed = true },
                    RX = new Frame.Parameters.Parameter() { Value = 0, IsFixed = true },
                    RY = new Frame.Parameters.Parameter() { Value = 0.001336902, IsFixed = false },
                    RZ = new Frame.Parameters.Parameter() { Value = 0, IsFixed = true },
                    Scale = new Frame.Parameters.Parameter() { Value = 1, IsFixed = true },
                },
                Active = true,
                CalagePoints = new List<ObsXYZ>()
                {
                    new ObsXYZ()
                    {
                        Point = "BEAM_LHC.MCBCA.5L5.E",

                        X = new Observation(){ Value=-0.000004 },
                        Y = new Observation(){Value=4.990007},
                        Z =  new Observation() { Value=0.000003},
                     },
                    new ObsXYZ()
                    {
                        Point = "BEAM_LHC.MCBCA.5L5.S",

                        X =  new Observation(){Value=-0.000005},
                            Y =  new Observation(){Value=5.894007 },
                            Z =  new Observation(){Value=0.000003},
                    }
                 },
                Measurements = new Frame.FrameMeasurement()
                {
                    ObsXyzs = new List<ObsXYZ>()
                    {
                        new ObsXYZ()
                        {
                            Point = "LHC.MQML.5L5.E",
                            X = new Observation() { Value = 0.168800 , Sigma = 0.1},
                            Y = new Observation() { Value = 0.601580 , Sigma = 0.1},
                            Z = new Observation() { Value = 0.421630 , Sigma = 0.1},
                        },
                        new ObsXYZ()
                        {
                            Point = "LHC.MQML.5L5.S",
                            X = new Observation() { Value = 0.167070  , Sigma = 0.1},
                            Y = new Observation() { Value = 4.306810 , Sigma = 0.1},
                            Z = new Observation() { Value = 0.420700 , Sigma = 0.2},
                        }
                    },
                    Inclinaisons = new List<Incly>
                    {
                        new Incly()
                        {
                            Instrument = "W1015",
                            Point = "LHC.MQML.5L5.E",
                            Roll = new Observation () { Value=-0.533487369 },
                            ReferenceAngle = new Observation() { Value = 0.030169411 }
                        }
                    }
                }
            });
            string s = "";
            string createdInput = Frame.ToLGC_CreateInput(root, new List<Elements.Point>(), new List<Elements.Point>(), new List<Elements.Point>(), ref s);

            List<string> expectedlines = expectedcontent.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            List<string> lines = expectedcontent.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).ToList();

            Assert.AreEqual(expectedlines.Count, lines.Count);
        }

        private string GetDatFromGeodeWithFrame()
        {
            return @"LHC       ;MQ.11R1.E                       ;  434.35880; 1800.18479; 2546.42770;2364.85044;364.86846;  .997;  .006246;  .012684;277.3313; 1;A;26-JUN-2023; HCLQTCE001-CR000521;;Socket Circulant - Calculé avec le HCLQTCE001-CR000521  automatiquement avec autom_lhc_socket.fmx - old_slot_id = 241971;
LHC       ;MQ.11R1.T                       ;  438.06261; 1796.89805; 2544.64061;2364.89442;364.91241;99.999;  .006246;  .012684;277.3313; 1;O;26-JUN-2023; HCLQTCE001-CR000521;;Socket Circulant - Calculé avec le HCLQTCE001-CR000521  automatiquement avec autom_lhc_socket.fmx - old_slot_id = 241971;
LHC       ;MQ.11R1.S                       ;  438.06288; 1796.71309; 2545.13724;2364.89770;364.91574; 3.100;  .006246;  .012684;277.3313; 1;O;26-JUN-2023; HCLQTCE001-CR000521;;Socket Circulant - Calculé avec le HCLQTCE001-CR000521  automatiquement avec autom_lhc_socket.fmx - old_slot_id = 241971;
LHC       ;MQ.12R1.E                       ;  489.08741; 1748.76598; 2527.71506;2365.54683;365.56529;  .298;  .008071;  .012775;278.3054; 1;A;26-JUN-2023; HCLQATH001-CR000317;;recalculation bumped coordinates 02-10-2015 - Socket Circulant - Calculé avec le HCLQATH001-CR000317  automatiquement avec autom_lhc_socket.fmx - old_slot_id = 241990;
LHC       ;MQ.12R1.T                       ;  491.93221; 1746.26241; 2526.26386;2365.57891;365.59736;99.999;  .008071;  .012775;278.3054; 1;O;26-JUN-2023; HCLQATH001-CR000317;;recalculation bumped coordinates 02-10-2015 - Socket Circulant - Calculé avec le HCLQATH001-CR000317  automatiquement avec autom_lhc_socket.fmx - old_slot_id = 241990;
LHC       ;MQ.12R1.S                       ;  491.93256; 1746.08509; 2526.76300;2365.58315;365.60164; 3.100;  .008071;  .012775;278.3054; 1;O;26-JUN-2023; HCLQATH001-CR000317;;recalculation bumped coordinates 02-10-2015 - Socket Circulant - Calculé avec le HCLQATH001-CR000317  automatiquement avec autom_lhc_socket.fmx - old_slot_id = 241990;
LHC       ;MQ.13R1.E                       ;  542.53918; 1698.26980; 2510.21939;2366.23201;366.25140;  .298;  .007439;  .012862;279.2795; 1;A;26-JUN-2023; HCLQATU001-CR000340;;recalculation bumped coordinates 02-10-2015 - Socket Circulant - Calculé avec le HCLQATU001-CR000340  automatiquement avec autom_lhc_socket.fmx - old_slot_id = 242006;
LHC       ;MQ.13R1.T                       ;  545.38262; 1695.74514; 2508.80852;2366.26484;366.28425;99.999;  .007439;  .012862;279.2795; 1;O;26-JUN-2023; HCLQATU001-CR000340;;recalculation bumped coordinates 02-10-2015 - Socket Circulant - Calculé avec le HCLQATU001-CR000340  automatiquement avec autom_lhc_socket.fmx - old_slot_id = 242006;
LHC       ;MQ.13R1.S                       ;  545.38331; 1695.57495; 2509.31082;2366.26876;366.28821; 3.100;  .007439;  .012862;279.2795; 1;O;26-JUN-2023; HCLQATU001-CR000340;;recalculation bumped coordinates 02-10-2015 - Socket Circulant - Calculé avec le HCLQATU001-CR000340  automatiquement avec autom_lhc_socket.fmx - old_slot_id = 242006;
LHC       ;MQ.14R1.E                       ;  595.99226; 1647.51021; 2493.49929;2366.92026;366.94107;  .298;  .006524;  .012947;280.2536; 1;A;26-JUN-2023; HCLQATH001-CR000374;;recalculation bumped coordinates 02-10-2015 - Socket Circulant - Calculé avec le HCLQATH001-CR000374  automatiquement avec autom_lhc_socket.fmx - old_slot_id = 242024;
LHC       ;MQ.14R1.T                       ;  598.83554; 1644.96456; 2492.12679;2366.95284;366.97370;99.999;  .006524;  .012947;280.2536; 1;O;26-JUN-2023; HCLQATH001-CR000374;;recalculation bumped coordinates 02-10-2015 - Socket Circulant - Calculé avec le HCLQATH001-CR000374  automatiquement avec autom_lhc_socket.fmx - old_slot_id = 242024;
LHC       ;MQ.14R1.S                       ;  598.83761; 1644.80084; 2492.63093;2366.95629;366.97719; 3.100;  .006524;  .012947;280.2536; 1;O;26-JUN-2023; HCLQATH001-CR000374;;recalculation bumped coordinates 02-10-2015 - Socket Circulant - Calculé avec le HCLQATH001-CR000374  automatiquement avec autom_lhc_socket.fmx - old_slot_id = 242024;
LHC       ;MQ.15R1.E                       ;  649.44308; 1596.50392; 2477.55547;2367.61470;367.63743;  .298;  .006398;  .013028;281.2277; 1;A;26-JUN-2023; HCLQATO001-CR000352;;recalculation bumped coordinates 02-10-2015 - Socket Circulant - Calculé avec le HCLQATO001-CR000352  automatiquement avec autom_lhc_socket.fmx - old_slot_id = 242040;
LHC       ;MQ.15R1.S                       ;  652.28586; 1593.78364; 2476.73074;2367.65124;367.67408; 3.100;  .006398;  .013028;281.2277; 1;O;26-JUN-2023; HCLQATO001-CR000352;;recalculation bumped coordinates 02-10-2015 - Socket Circulant - Calculé avec le HCLQATO001-CR000352  automatiquement avec autom_lhc_socket.fmx - old_slot_id = 242040;
LHC       ;MQ.15R1.T                       ;  652.28739; 1593.93655; 2476.22190;2367.64791;367.67071;99.999;  .006398;  .013028;281.2277; 1;O;26-JUN-2023; HCLQATO001-CR000352;;recalculation bumped coordinates 02-10-2015 - Socket Circulant - Calculé avec le HCLQATO001-CR000352  automatiquement avec autom_lhc_socket.fmx - old_slot_id = 242040;

*FRAME RSTI_102144_LHC.MQ.15R1   1596.73413   2477.25140   2367.19086 0 0  281.22768 1
*FRAME RST_102144_LHC.MQ.15R1 0 0 0 -.82938823944 .33301580293 0 1
*FRAME RSTBI1_102144_LHC.MQ.15R1      0.00000      0.00200      0.00010 0 0 0 1
*FRAME RSTBI2_102144_LHC.MQ.15R1 0 0 0 0 0 0 1
*FRAME RSTB_102144_LHC.MQ.15R1 0 0 0 0 0 0 1
*FRAME RSTRI_102144_LHC.MQ.15R1     -0.00044      0.00041     -0.00159 -.00232058924 0 .00139646078 1 TX TY TZ RX RZ 
*FRAME RSTR_102144_LHC.MQ.15R1 0 0 0 0 0 0 1 RY
*CALA
BEAM_LHC.B2.MQ.15R1.E                                            0.19400     -0.00001      0.00001   $649.286 679921 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.MQ.15R1.S                                            0.19400      3.10000      0.00000   $652.386 679922 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.MQT.15R1.E                                           0.19399     -0.61800      0.00000   $648.668 680917 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.MQT.15R1.S                                           0.19399     -0.29800      0.00000   $648.988 680918 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.MS.15R1.E                                            0.19400      3.26050      0.00000   $652.546 681597 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.MS.15R1.S                                            0.19400      3.62950      0.00000   $652.915 681598 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.BPM.15R1.E                                           0.19400     -1.21201      0.00000   $648.074 669541 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.BPM.15R1.S                                           0.19400     -1.21201      0.00000   $648.074 669542 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.MCBV.15R1.E                                          0.19400      3.71450      0.00000   $653.000 673893 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.MCBV.15R1.S                                          0.19399      4.36150      0.00001   $653.647 673894 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.MSCBA.15R1.E                                         0.19400      3.20600      0.00000   $652.492 824989 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.MSCBA.15R1.S                                         0.19400      3.20600      0.00000   $652.492 824990 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MS.15R1.E                                               0.00000      3.26050      0.00000   $652.566 694743 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MS.15R1.S                                               0.00000      3.62950      0.00001   $652.935 694744 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MQ.15R1.S                                               0.00000      3.10000      0.00000   $652.406 693068 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MQT.15R1.E                                              0.00000     -0.61800      0.00000   $648.688 694063 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MQT.15R1.S                                              0.00000     -0.29801      0.00000   $649.008 694064 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.BPM.15R1.E                                              0.00000     -1.21200      0.00000   $648.094 682687 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MCBH.15R1.E                                             0.00000      3.71450      0.00000   $653.020 686661 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MCBH.15R1.S                                             0.00000      4.36150      0.00000   $653.667 686662 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MQ.15R1.E                                               0.00000      0.00000      0.00000   $649.306 693067 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.BPM.15R1.S                                              0.00000     -1.21200      0.00000   $648.094 682688 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MSCBA.15R1.E                                            0.00000      3.20601      0.00000   $652.512 824237 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MSCBA.15R1.S                                            0.00000      3.20601      0.00000   $652.512 824238 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
*OBSXYZ
LHC.MQ.15R1.E      0.36005      0.13542      0.42011 0.1 0.1 0.1   $649.441 197747 paramètres RST 26-JUN-2023 09:07:58
LHC.MQ.15R1.S      0.36148      2.97820      0.41961 0.1 0.1 0.1   $652.284 198138 paramètres RST 26-JUN-2023 09:07:58
LHC.MQ.15R1.T     -0.16984      2.97973      0.41903 0.1 0.1 0.1   $652.285 198529 paramètres RST 26-JUN-2023 09:07:58
*INCLY Instr_Roll_Theo
LHC.MQ.15R1.E .40712534725 RF .06949468759
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*FRAME RSTI_102140_LHC.MQ.14R1   1647.74624   2493.19739   2366.49679 0 0  280.25358 1
*FRAME RST_102140_LHC.MQ.14R1 0 0 0 -.82423161928 .3456845364 0 1
*FRAME RSTBI1_102140_LHC.MQ.14R1      0.00000      0.00200      0.00000 0 0 0 1
*FRAME RSTBI2_102140_LHC.MQ.14R1 0 0 0 0 0 0 1
*FRAME RSTB_102140_LHC.MQ.14R1 0 0 0 0 0 0 1
*FRAME RSTRI_102140_LHC.MQ.14R1     -0.00013     -0.00009     -0.00103 .00201254642 0 -.0058117412 1 TX TY TZ RX RZ 
*FRAME RSTR_102140_LHC.MQ.14R1 0 0 0 0 0 0 1 RY
*CALA
BEAM_LHC.B2.MQ.14R1.E                                            0.19400      0.00000      0.00000   $595.837 679889 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.MS.14R1.E                                            0.19400      3.26050     -0.00001   $599.098 681565 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.MS.14R1.S                                            0.19400      3.62950      0.00000   $599.467 681566 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.MQT.14R1.E                                           0.19400     -0.61800      0.00000   $595.219 680885 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.MQT.14R1.S                                           0.19400     -0.29800     -0.00001   $595.539 680886 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.BPM.14R1.E                                           0.19399     -1.21200      0.00000   $594.625 669509 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.BPM.14R1.S                                           0.19399     -1.21200      0.00000   $594.625 669510 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.MCBH.14R1.E                                          0.19400      3.71450      0.00000   $599.552 673501 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.MCBH.14R1.S                                          0.19400      4.36150      0.00000   $600.199 673502 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.MQ.14R1.S                                            0.19400      3.10000      0.00000   $598.937 679890 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.MSCBB.14R1.E                                         0.19400      3.20601      0.00000   $599.043 825281 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.B2.MSCBB.14R1.S                                         0.19400      3.20601      0.00000   $599.043 825282 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MS.14R1.E                                               0.00000      3.26050      0.00000   $599.115 694711 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MS.14R1.S                                               0.00000      3.62950      0.00000   $599.484 694712 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MQ.14R1.E                                               0.00000      0.00000      0.00000   $595.854 693035 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MQT.14R1.E                                              0.00000     -0.61800      0.00000   $595.236 694031 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MQT.14R1.S                                              0.00000     -0.29800      0.00000   $595.556 694032 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.BPM.14R1.E                                              0.00000     -1.21200      0.00000   $594.642 682655 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.BPM.14R1.S                                              0.00000     -1.21200      0.00000   $594.642 682656 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MCBV.14R1.E                                             0.00000      3.71450      0.00000   $599.569 687021 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MCBV.14R1.S                                             0.00000      4.36150      0.00000   $600.216 687022 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MQ.14R1.S                                               0.00000      3.10000      0.00000   $598.954 693036 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MSCBB.14R1.E                                            0.00000      3.20600      0.00000   $599.060 824529 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
BEAM_LHC.MSCBB.14R1.S                                            0.00000      3.20600      0.00000   $599.060 824530 coordonnées théoriques dans le système RST au 26-JUN-2023 09:07:58
*OBSXYZ
LHC.MQ.14R1.S      0.36186      2.98144      0.41895 0.1 0.1 0.1   $598.836 198137 paramètres RST 26-JUN-2023 09:07:58
LHC.MQ.14R1.T     -0.16820      2.97937      0.41841 0.1 0.1 0.1   $598.834 198528 paramètres RST 26-JUN-2023 09:07:58
LHC.MQ.14R1.E      0.36182      0.13609      0.41976 0.1 0.1 0.1   $595.990 197746 paramètres RST 26-JUN-2023 09:07:58
*INCLY Instr_Roll_Theo
LHC.MQ.14R1.E .41409251404 RF .06485563931
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
";
        }

        [TestMethod()]
        public void GetRootFrameFromLGCInputFormatStringTest()
        {
            // test the reading from geode
            string datFromGeodeWithFrame = GetDatFromGeodeWithFrame();
            Frame allFramesFromGeode = Frame.FromLGCInputFormat_GetRootFrameInputFormatString(datFromGeodeWithFrame);

            Assert.AreEqual(0.13609, allFramesFromGeode.SubFrames[1].DeeperFrame.Measurements.ObsXyzs[2].Y.Value);
            Assert.AreEqual(0.00201254642, allFramesFromGeode.SubFrames[1].SubFrames[0].SubFrames[0].SubFrames[0].SubFrames[0].SubFrames[0].ProvisoryParameters.RX.Value);

            // test writing back 
            var cala = new List<Elements.Point>();
            var vxy = new List<Elements.Point>();
            var vz = new List<Elements.Point>();
            string proposedName = "";
            string inputCOntent = Frame.ToLGC_CreateInput(allFramesFromGeode.SubFrames[1], cala, vxy, vz, ref proposedName);

            List<string> geodeLines = datFromGeodeWithFrame.Split('\n').ToList();
            List<string> tsunamiLines = inputCOntent.Split('\n').ToList();

            string line37 = tsunamiLines[37];
            Assert.AreEqual("LHC.MQ.14R1.E   0.4140925   RF   0.0648556   $6.5046 mrad\r", line37);

            string line5 = tsunamiLines[5];
            Assert.AreEqual("*FRAME   RSTRI_102140_LHC.MQ.14R1   -0.000130   -0.000090   -0.001030   0.002013   0.000000   -0.005812   1.000000      TX   TY   TZ   RX   RZ\r", line5);
        }
    }
}
