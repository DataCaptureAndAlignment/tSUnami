﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Xml;
using Moq;
using TSU;
using TSU.ENUM;
using TSU.Common.Elements;

using TSU.Tools;
using TsunamiTests;
using O = TSU.Common.Operations;
using I = TSU.Common.Instruments;
using E = TSU.Common.Elements;
using EC = TSU.Common.Elements.Composites;
using M = TSU.Common.Measures;
using Z = TSU.Common.Zones;

namespace TestsTsunami.ThreeD
{
    [TestClass()]
    public class Test
    {
        public List<E.Point> GetPoints()
        {
            //TODO:  TEST_EMPTY

            string content = @" PJ10             1.12480      7.37670     -0.91980
 PJ53            -5.64130      6.91390     -1.29460
 PJ54            -5.64670      6.91290      2.23520
 PMW02            9.80370      3.63070     -1.65910
 TEMP4            4.38040      2.81314     -2.44307
 ST04             4.75707      5.56003     -1.17236
 DC04_DF_JB       4.78618      1.87871     -1.34429
 DC05_DF_JB       5.18873      1.86120     -1.32917
 ST03_DJB         5.55866      2.35091     -1.39754
 SF05_JBJ         6.00823      2.23328     -1.11472
 SF05_JBU         5.95883      2.18413     -1.11474
 BB00_JBJ         5.90439      1.83947     -1.11915
 BB00_JBU         5.83356      1.76903     -1.02532
 DC04_DF_JT       4.78248      1.87207      1.41774
 DC05_DF_JT       5.18593      1.85743      1.41962
 ST03_UJT         5.36314      2.34761      1.40595
 ST03_DJT         5.54292      2.34808      1.40685
 BB00_JTU         5.82509      1.77517      1.02854
 SF05_JTU         5.95963      2.18873      0.74877
 SF05_JTJ         6.00897      2.23781      0.74864
 DC05_RM_UJB      4.92860      1.08287     -1.10640
 DC05_RM_USB      5.04065     -1.07725     -1.10481";

            return TestsTsunami.Mocks.MockPoints(content, TSU.Common.Elements.Coordinates.ReferenceFrames.MLA1985Machine);
        }

        [TestMethod()]
        public void UsePoints()
        {
            //TODO:  TEST_EMPTY
            List<Point> points = GetPoints();
            double x = points[0]._Coordinates.Local.X.Value;
        }
    }
}

