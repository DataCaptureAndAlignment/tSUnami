﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using TSU.Common.Blocks;
using TSU.Common.Elements;

namespace TSU.Tilt.Smart.Tests
{
    [TestClass]
    public class SelectablePoints
    {
        /// <summary>
        /// Ensure all points have been added to the selected list 
        /// </summary>
        [TestMethod()]
        public void TestSelect()
        {
            PrepareSelectablePoints(out List<Point> lstSelected, out List<Selectable<Point>> lst);

            Assert.AreEqual(lst.Count, lstSelected.Count);
        }

        /// <summary>
        /// Check the previous selected point operation
        /// </summary>
        [TestMethod]
        public void TestPrevious()
        {
            PrepareSelectablePoints(out _, out List<Selectable<Point>> lst);

            for (int i = 0; i < lst.Count; i++)
            {
                // we want the list to be circular so expected result is (i - 1) mod lst.Count
                // but C# has no mod operator, only % which is remainder and is the same for positive values only
                // so we add lst.Count to ensure we have a positive value
                int targetIndex = (i - 1 + lst.Count) % lst.Count;
                Assert.AreEqual(lst[targetIndex].Item, ViewModel.PreviousSelectedPointInList(lst, lst[i].Item).Item);
            }
        }

        /// <summary>
        /// Check the next selected point operation
        /// </summary>
        [TestMethod]
        public void TestNext()
        {
            PrepareSelectablePoints(out _, out List<Selectable<Point>> lst);

            for (int i = 0; i < lst.Count; i++)
            {
                // we want the list to be circular so expected result is (i + 1) mod lst.Count
                // but C# has no mod operator, only % which is remainder and is the same for positive values only
                // and it is always positive here
                int targetIndex = (i + 1) % lst.Count;
                Assert.AreEqual(lst[targetIndex].Item, ViewModel.NextSelectedPointInList(lst, lst[i].Item).Item);
            }
        }

        private static void PrepareSelectablePoints(out List<Point> lstSelected, out List<Selectable<Point>> lst)
        {
            //Make a list of 4 points
            lstSelected = new List<Point>();
            lst = new List<Selectable<Point>>()
            {
                new Selectable<Point>(lstSelected, new Point("Point.1")),
                new Selectable<Point>(lstSelected, new Point("Point.2")),
                new Selectable<Point>(lstSelected, new Point("Point.3")),
                new Selectable<Point>(lstSelected, new Point("Point.4"))
            };

            //Select them all
            foreach (var p in lst)
                p.Selected = true;
        }
    }
}
