﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Compute.Compensations;
using D = TSU.Common.Dependencies;

namespace TSU.Common.Dependencies.Tests
{
    [TestClass()]
    public class ApplicationTests
    {
        [TestMethod()]
        public void ApplicationTest()
        {
            D.Application SUSoft = new D.Application("SUSoft Recommendation", "1.00.00", "MAY 2017", @"\\cern.ch\dfsSupport\SurveyingEng\Computing\Software\install\susoft\SUSoftDependencies.xml", @"c:\Program Files\SUSoft\SUSoftDependencies.xml");

            D.Application LGC = new D.Application("LGC", "2.00.07", "Apr 2017", @"{SUSoft}\LGC2Installer-2.00.07-win64.exe", @"{program files}\lgc.exe");

            D.Application gsiPLGC = new D.Application("gsiPLGC", "1.05.01", "?",                        @"{SUSoft}\Setup.exe", @"{program files}\gsiPLGC.exe");

            D.Application GEOFIT = new D.Application("GEOFIT", "1.00.00", "?",                          @"{SUSoft}\GeoFitInstaller.exe", @"{program files}\GeoFit.exe");

            D.Application Shapes = new D.Application("Shapes", "3.00.02", "?",                          @"{SUSoft}\Setup.exe", @"{program files}\SHAPES.xlam");

            D.Application PLGC = new D.Application("PLGC", "4.00.00", "JUNE 2014",                      @"{SUSoft}\Setup.exe", @"{program files}\PLGC.exe");

            D.Application TOPOCALC = new D.Application("TOPOCALC", "2.00.04", "?",            @"{SUSoft}\Setup.exe", @"{program files}\TOPOCALC.xlam");

            D.Application CYLINDER = new D.Application("CYLINDER", "2.00.00",  "?",            @"{SUSoft}\Setup.exe", @"{program files}\CYLINDER.xlam");

            D.Application SUGAR3 = new D.Application("SUGAR3", "1.00.00", "?", @"{SUTest}\Setup.exe", @"{program files}\SUGAR3.exe");

            CYLINDER.Add(GEOFIT);

            D.Application CARNET = new D.Application("CARNET4000", "2.04.44", "?", @"{SUTest}\setup.exe", @"{program files}\CARNET4000.xlam");
            CARNET.Add(new D.Application("LGC", "1.12.03", "?", @"{SUSoft}\LGC++Installer.exe", @"{program files}\lgc.exe"));
            CARNET.Add(new D.Application("PCTOPO32", "3.03.08", "?", @"{SUTest}\setup.exe", @"{program files}\PCTOPO32.exe"));
            CARNET.Add(new D.Application("SHAPES", "3.00.03", "?", @"{SUSoft}\setup.exe", @"{program files}\Shapes.xlam"));
            CARNET.Add(new D.Application("CHABA", "3.01.00", "?", @"{SUTest}\ChaBaInstaller.exe", @"{program files}\ChaBa.exe"));

            D.Application PCTOPO32 = new D.Application("PCTOPO32", "3.03.08", "Mar 2017", @"{SUTest}\setup.exe", @"{program files}\PCTOPO32.exe");
            PCTOPO32.Add(new D.Application("PLGC", "4.01.01", "?", @"{SUSoft}\setup.exe", @"{program files}\PLGC.exe"));
            PCTOPO32.Add(new D.Application("LGC", "1.12.03", "?", @"{SUSoft}\LGC++Installer.exe", @"{program files}\lgc.exe"));
            PCTOPO32.Add(new D.Application("LGC", "1.13.02", "?", @"{SUSoft}\LGC++Installer-1.13.02-win64.exe", @"{program files}\lgc.exe"));
            PCTOPO32.Add(new D.Application("LGC", "2.00.06", "?", @"{SUSoft}\LGC2Installer-2.00.06-win64.exe", @"{program files}\lgc.exe"));
            PCTOPO32.Add(new D.Application("gsiPLGC", "1.05.01", "?", @"{SUSoft}\Setup.exe", @"{program files}\gsiPLGC.exe"));
            PCTOPO32.Add(new D.Application("ADAPT", "1.00.00", "?", @"{SUSoft}\AdaptInstaller.exe", @"{program files}\Adapt.exe"));
            PCTOPO32.Add(new D.Application("CHABA", "3.00.04", "?", @"{SUSoft}\ChaBaInstaller.exe", @"{program files}\ChaBa.exe"));
            PCTOPO32.Add(new D.Application("CHABA", "5.00.00", "?", @"{SUTest}\ChaBa-5.00.00-win64.exe", @"{program files}\ChaBa.exe"));
            PCTOPO32.Add(new D.Application("CSGEO", "5.01.00", "?", @"{SUSoft}\CSGeo-5.01.00-win64.exe", @"{program files}\CSGeo.exe"));
            PCTOPO32.Add(new D.Application("XTRINS", "1.00.00", "?", @"{SUSoft}\XTrinsInstaller.exe", @"{program files}\XTRINS.exe"));
            PCTOPO32.Add(new D.Application("DEFORM", "1.00.00", "?", @"{SUSoft}\Setup.exe", @"{program files}\DEFORM.exe"));
            PCTOPO32.Add(new D.Application("transform", "1.00.00", "?", @"{SUSoft}\setup.exe", @"{program files}\transform.exe"));
            PCTOPO32.Add(new D.Application("CHABA", "4.00.00", "?", @"{SUTest}\ChaBa-4.00.00-win64.exe", @"{program files}\ChaBa.exe"));

            D.Application SFB = new D.Application("SFB", "9.01.04", "?", @"{SUSoft}\Setup.exe", @"{program files}\SFB.exe");
            SFB.Add(new D.Application("LGC", "1.08.03", "?", @"{SUSoft}\Setup.exe", @"{program files}\lgc.exe"));
            SFB.Add(new D.Application("CHABACLI", "3.00.04", "?", @"{SUSoft}\ChaBaCLIInstaller.exe", @"{program files}\ChaBaCli.exe"));
            SFB.Add(new D.Application("PLGC", "4.00.00", "?", @"{SUSoft}\setup.exe",@"{program files}\PLGC.exe"));


            D.Application Tsunami = new D.Application("Tsunami", "1.00.00", "MAY 2017", @"{SUTest}\Setup.exe", @"{program files}\Setup.exe");
            Tsunami.Add(new D.Application("LGC", "2.00.06", "?", @"{SUSoft}\LGC++Installer.exe", @"{program files}\lgc.exe"));
            Tsunami.Add(new D.Application("LGC", "1.12.03", "?", @"{SUSoft}\LGC++Installer.exe", @"{program files}\lgc.exe"));
            Tsunami.Add(new D.Application("CHABACLI", "3.00.04", "?", @"{SUSoft}\ChaBaInstaller.exe", @"{program files}\ChaBaCLI.exe"));
            Tsunami.Add(new D.Application("PLGC", "4.01.01", "?", @"{SUSoft}\setup.exe", @"{program files}\PLGC.exe"));
            Tsunami.Add(new D.Application("CHABA", "3.00.04", "?", @"{SUSoft}\ChaBaInstaller.exe", @"{program files}\ChaBa.exe"));
            Tsunami.Add(new D.Application("SHAPES", "3.00.03", "?", @"{SUSoft}\setup.exe", @"{program files}\Shapes.xlam"));
            Tsunami.Add(new D.Application("PCTOPO32", "3.03.02", "?", @"{SUSoft}\setup.exe", @"{program files}\PCTOPO32.exe"));
            Tsunami.Add(new D.Application("CSGEO", "5.01.00", "?", @"{SUSoft}\CSGeo-5.01.00-win64.exe", @"{program files}\CSGeo.exe"));
            Tsunami.Add(SUGAR3);
            Tsunami.Add(new D.Application("GEOFIT", "1.00.00", "?", @"{SUSoft}\GeoFitInstaller.exe", @"{program files}\GeoFit.exe"));
            Tsunami.Add(new D.Application("CSGEO", "5.01.00", "?", @"{SUSoft}\CSGeo-5.01.00-win64.exe", @"{program files}\CSGEO.exe"));



            SUSoft.Add(gsiPLGC);
            SUSoft.Add(GEOFIT);
            SUSoft.Add(Shapes);
            SUSoft.Add(PLGC);
            SUSoft.Add(TOPOCALC);
            SUSoft.Add(CYLINDER);
            SUSoft.Add(CARNET);
            SUSoft.Add(PCTOPO32);
            SUSoft.Add(LGC);
            SUSoft.Add(SFB);
            SUSoft.Add(Tsunami);

            TSU.Unit_Tests.Serial.TryToSerialize(SUSoft, true);
            object obj2 = new D.Application();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(SUSoft.Version, (obj2 as Application).Version);
        }


        [TestMethod()]
        public void GetRealPathTest()
        {
            Application PCTOPO32 = new Application("PLGC", "4.01.01", "?", @"{SUSoft}\setup.exe", @"{program files}\PLGC.exe");
            string applicationPath = PCTOPO32.CorrectPath(PCTOPO32.StoredApplicationPath);
            string InstallerPath = PCTOPO32.CorrectPath(PCTOPO32.StoredInstallerPath);
            Assert.AreEqual(@"C:\Program Files (x86)\SUSOFT\PLGC\4.01.01\PLGC.EXE", applicationPath);

            // sometime runner do not have dfs acces so it could be in c temp
            string dfs = @"\\cern.ch\dfs\SUPPORT\SURVEYINGENG\COMPUTING\SOFTWARE\INSTALL\SUSOFT\PLGC\4.01.01\SETUP.EXE".ToUpper();
            string temp = @"C:\TEMP\INSTALL\SUSOFT\PLGC\4.01.01\SETUP.EXE";
            string value = InstallerPath.ToUpper();
            bool bDfs = value == dfs;
            bool bTemp = value == temp;

            Assert.IsTrue(bDfs || bTemp);
        }


    }
}
