﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Measures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Common.Elements;
using TSU.Common.Compute;

namespace TSU.Common.Measures.Tests
{
    [TestClass()]
    public class MeasureTheodoliteTests
    {
        [TestMethod()]
        public void GetNiceCloneTest()
        {
            Polar.Measure m1 = new Polar.Measure()
            {
                Angles = new MeasureOfAngles()
                {
                    Corrected = new Angles()
                    {
                        Horizontal = new DoubleValue(100, 0)
                    }
                }
            };

            Polar.Measure m2 = m1.GetNiceClone();

            Survey.TransformToOppositeFace(m2.Angles);

            Assert.AreEqual(100, m1.Angles.Corrected.Horizontal.Value, "Failed");
            Assert.AreEqual(300, m2.Angles.Corrected.Horizontal.Value, "Failed");
        }

        [TestMethod()]
        public void ToStringTest()
        {
            Polar.Measure MeasureTheodolite = new Polar.Measure()
            {
                _Point = new Elements.Point("myname"),
                Angles = new MeasureOfAngles() {  corrected = new Angles() {  Horizontal = new DoubleValue(100,120)} }
            };
            string s = MeasureTheodolite.ToString("AllInLine");
            s = "";
        }
    }
}