﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Measures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using M = TSU.Common.Measures;
using System.Threading.Tasks;

namespace TSU.Common.Measures.Tests
{
    [TestClass()]
    public class TheodoliteRoundTests
    {

        [TestMethod()]
        public void Serialization_Rounds()
        {
            // TODO: ASSERT_FAIL
            // Assertion failed - problem with comparing objects - maybe problem that assert not compare object well

            TheodoliteRound r = new TheodoliteRound(new List<Polar.Measure>() { new Polar.Measure() { _Name = "m1" }, new Polar.Measure() { _Name = "m2" } }) ;
            

            TSU.Unit_Tests.Serial.TryToSerialize(r, true);
                object obj2 = new TheodoliteRound();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(r.Measures.Count, (obj2 as TheodoliteRound).Measures.Count);
            CollectionAssert.AreEqual(r.Measures, (obj2 as TheodoliteRound).Measures);
        }
        [TestMethod()]
        public void Serialization_Roundstates()
        {
            List<M.States.Types> r = new List<M.States.Types>() { M.States.Types.Good, M.States.Types.GoodFromOtherRound, M.States.Types.Replace };

            TSU.Unit_Tests.Serial.TryToSerialize(r, true);
            object obj2 = new List<M.States.Types>();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(States.Types.GoodFromOtherRound, (obj2 as List<M.States.Types>)[1]);
        }
    }
}