﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Instruments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSU.Common.Instruments.Tests
{
    [TestClass()]
    public class ToleranceTests
    {
        [TestMethod()]
        public void ToleranceTest()
        {
            object obj = null;
            object obj2 = null;

            try
            {
                obj   = new List<InstrumentTolerance>()
                {
                    new InstrumentTolerance() {
                        InstrumentType = InstrumentTypes.AT401,
                        Same_Face = new InstrumentTolerance.Type(){ H_CC=8, V_CC=8, D_mm = 0.02 },
                        Opposite_Face = new InstrumentTolerance.Type(){ H_CC=15, V_CC=8, D_mm = 0.1},
                        Closure = new  InstrumentTolerance.Type(){ H_CC= 10, V_CC=8,  D_mm = 0.1 }
                    },
                    new InstrumentTolerance() {
                        InstrumentType = InstrumentTypes.TDA5005,
                        Same_Face = new InstrumentTolerance.Type(){ H_CC=10, V_CC=8, D_mm = 0.2 },
                        Opposite_Face = new InstrumentTolerance.Type(){ H_CC=15, V_CC=8, D_mm = 0.3},
                        Closure = new  InstrumentTolerance.Type(){ H_CC= 10,  V_CC=8, D_mm = 0.2 }
                    }

                };

                obj2 = new List<InstrumentTolerance>();

                TSU.Unit_Tests.Serial.TryToSerialize(obj, true);
                TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);
            }
            catch (Exception ex)
            {
                string a = ex.InnerException.InnerException.InnerException.InnerException.Message;
                Assert.IsTrue(false);
            }
            Assert.AreEqual((obj as List<InstrumentTolerance>)[0].Closure.D_mm, (obj2 as List<InstrumentTolerance>)[0].Closure.D_mm);
        }
    }
}