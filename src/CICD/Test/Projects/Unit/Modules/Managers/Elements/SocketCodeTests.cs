﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Common.Elements;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace TSU.Common.Elements.Tests
{
    [TestClass()]
    public class SocketCodeTests
    {
        [TestMethod()]
        public void SocketCodeTest()
        {
            List<SocketCode> objToSerial = new List<SocketCode>();
            objToSerial.Add(new SocketCode() { Id = "1",  _Name = "Code 1",  Description = "Centre Boule Taylor posée sur coupelle grise", DefaultExtensionForPolarMeasurement=0.0, DefaultExtensionForLevelingMeasurement=0.0});
            objToSerial.Add(new SocketCode() { Id = "2",  _Name = "Code 2",  Description = "Centre Boule Taylor posée sur coupelle rouge (voir croquis)", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "3",  _Name = "Code 3",  Description = "Centre Boule Tayor posée sur petit alésage rouge (voir croquis)", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "4",  _Name = "Code 4",  Description = "Centre petite sphère alésage rouge sur coupelle rouge (Voir croquis)", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "5",  _Name = "Code 5",  Description = "Axe faisceau", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "6",  _Name = "Code 6",  Description = "Repère sur culasse", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "7",  _Name = "Code 7",  Description = "Centre Boule Tayor posée sur alésage spécial", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "8",  _Name = "Code 8",  Description = "Centre Boule Tayor posée sur alésage type ISR", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "9",  _Name = "Code 9",  Description = "Centre Boule Tayor posée sur coupelle \"99\" type PS", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "10", _Name = "Code 10", Description = "Rivet au sol ou marque peinture", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "11", _Name = "Code 11", Description = "Petit talon (39.66mm plus bas que B.T.)", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "12", _Name = "Code 12", Description = "Centre petite boule bleue (cst= 59mm)", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.059 });
            objToSerial.Add(new SocketCode() { Id = "13", _Name = "Code 13", Description = "Point théo. de génie civil", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "14", _Name = "Code 14", Description = "Sommet alésage", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "15", _Name = "Code 15", Description = "Sommet pot HLS", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "16", _Name = "Code 16", Description = "Centre Boule Tayor posée sur une rallonge de 70mm", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.070 });
            objToSerial.Add(new SocketCode() { Id = "17", _Name = "Code 17", Description = "Centre Boule Tayor posée sur un pot HLS", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "18", _Name = "Code 18", Description = "Centre Boule Tayor posée sur coupelle de bloc support de WPS", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "20", _Name = "Code 20", Description = "Repère pour sondage stabilisé sol", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "21", _Name = "Code 21", Description = "Centre Boule Tayor pincée dans support système LHC", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "22", _Name = "Code 22", Description = "Centre prisme CCR 0.5 sur support", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "23", _Name = "Code 23", Description = "Centre cible optique avec offset 20mm dans un alésage de 8H7", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "24", _Name = "Code 24", Description = "Centre prisme CCR 1.5 sur support", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "25", _Name = "Code 25", Description = "Alésage laiton au sol (talon sphérique Boule Taylor (petit diamètre))", DefaultExtensionForPolarMeasurement = 0.0, DefaultExtensionForLevelingMeasurement = 0.0 });
            objToSerial.Add(new SocketCode() { Id = "16", _Name = "Code 16", Description = "Prisme sur fusée haute", DefaultExtensionForPolarMeasurement = 1.3941, DefaultExtensionForLevelingMeasurement = 1.3941 });
            objToSerial.Add(new SocketCode() { Id = "16", _Name = "Code 16", Description = "Prisme sur fusée basse", DefaultExtensionForPolarMeasurement = 1.2668, DefaultExtensionForLevelingMeasurement = 1.2668 });


            TSU.Unit_Tests.Serial.TryToSerialize(objToSerial, true); object objDeserial = new List<SocketCode>();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref objDeserial);

            Assert.AreEqual(objToSerial[0]._Name, ((List<SocketCode>)objDeserial)[0]._Name);
            Assert.AreEqual(objToSerial[0].Id, ((List<SocketCode>)objDeserial)[0].Id);
            Assert.AreEqual(objToSerial[1].Description, ((List<SocketCode>)objDeserial)[1].Description);
            Assert.AreEqual(objToSerial[1].DefaultExtensionForLevelingMeasurement, ((List<SocketCode>)objDeserial)[1].DefaultExtensionForLevelingMeasurement);
        }
    }
}
