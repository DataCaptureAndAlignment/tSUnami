﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Line;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Preferences;


namespace TSU.Line.Tests
{
    [TestClass()]
    public class StationLineModuleTests
    {
        [TestMethod()]
        public void StationLineModuleTest()
        {
            {
                Line.Module parent = new Line.Module(null, "");
                parent._Name = "papa";
                Line.Station.Module obj = new Line.Station.Module(parent);
                obj._Name = "test";

                TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new Line.Station.Module();
                TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

                Assert.AreEqual(obj._Name, (obj2 as Line.Station.Module)._Name);
            }
        }
    }
}
