﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSU.Level.Tests
{
    [TestClass()]
    public class StationLevelModuleTests
    {
        [TestMethod()]
        public void SerializationStationLevelModuleTest()
        {
            Level.Station.Module obj = null;
            object obj2 = null;
            try
            {
                obj = new Level.Station.Module();

                obj2 = new Level.Station.Module();
                (obj as Level.Station.Module).Id = "3";
               // obj._defaultStaff = new TSU.Common.Instruments.LevelingStaff(ENUM.StaffType.G10);

                TSU.Unit_Tests.Serial.TryToSerialize(obj, true);
                TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);
            }
            catch (Exception ex)
            {
                string a = ex.InnerException.InnerException.InnerException.InnerException.Message;
                Assert.IsTrue(false);
            }
            Assert.AreEqual((obj2 as Level.Station.Module).Id, (obj2 as Level.Station.Module).Id);
        }

        //[TestMethod()]
        //public void StationLevelModuleTest1()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void InitializeTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void LoadInstrumentTypesTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void OnNextBasedOnTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void OnNextTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void OnNextBasedOnTest1()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void TheoReadingAndResidualsCalculationTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void CheckIfCalculationPossibleTest()
        //{
        //    Assert.Fail();
        //}
    }
}