﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using T=TSU.AdvancedModules.Theodolite;

using M = TSU.Common.Measures;
using E = TSU.Common.Elements;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSC = TSU.Polar.Compensations;
using TSU.Common.Compute.Compensations;

namespace TSU.AdvancedModules.Theodolite.Tests
{
    [TestClass()]
    public class LocalStrategy2Tests
    {
        [TestMethod()]
        public void LGC_INPUT_COMPUTE_LocalStrategy()
        {
            string fileContent = @"*TITR 
%Input by Tsunami
*OLOC 
*PUNC 
*JSON
*PREC 7
*INSTR 
   *POLAR AT401_390858 RRR0.5_2885 0 0 0 0
      RRR0.5_2885 1 1 0.001 0.001 0 0 0 0 0 0
*CALA 
   ST_UnknownTeam_001 0 0 0 
   Fake 0 1 0 
*POIN
   Point_001 0 9.346 0.692 
   Point_002 0.654 -6.038 -0.286 
   Point_003 -0.801 -2.996 0.972 
*TSTN ST_UnknownTeam_001 AT401_390858 IHFIX  IH 0 IHSE 2
   *V0
      *ANGL
         Point_001 334.18429 
         Fake 334.18429 
         Point_002 127.31244 
         Point_003 150.826 
         Point_001 334.18429 
      *ZEND
         Point_001 96.92029 
         Fake 100 
         Point_002 105.50056 
         Point_003 85.23349 
         Point_001 96.92029 
      *DIST
         Point_001 9.35659713961556 
         Fake 1 
         Point_002 6.09646605247433 
         Point_003 3.18621301339303 
         Point_001 9.35659713961556 
*END 
";
            string filepath = "test-LGC_INPUT_COMPUTE_LocalStrategy";
            System.IO.File.WriteAllText(filepath + ".inp", fileContent);
            TSU.Polar.Compensations.NewFrame_LGC2 str = new TSC.NewFrame_LGC2();
            Lgc2 lgc2 = new Lgc2(null, str);// peut importe ici on simule la creation d'un input
            lgc2._FileName = filepath;
            lgc2.Run(false, false, true, 30);
            //Lgc2.Output o = new Lgc2.Output(filepath + ".res");
            str.ParseOutPutAsTheodoliteResultsAsJSON(lgc2._Output);
            Polar.Station.Parameters.Setup.Values r = lgc2._Output._Result;

            Assert.AreEqual("Point_001", r.VariablePoints[0]._Name);
            Assert.AreEqual(0.6543421, r.VariablePoints[1]._Coordinates.Su.X.Value, 0.00001);
            Assert.AreEqual(0.7324386, r.VariablePoints[2]._Coordinates.Su.Z.Value, 0.00001);
            Assert.AreEqual(65.8157100, r.vZero.Value, 0.00001);
            Assert.AreEqual(0.0, r.InstrumentHeight.Value, 0.00001);
            Assert.AreEqual(334.1842900, r.Measures[0].Angles.Raw.Horizontal.Value, 0.00001);
            Assert.AreEqual(105.5005600, r.Measures[2].Angles.Corrected.Vertical.Value, 0.00001);
            Assert.AreEqual(3.1862130, r.Measures[3].Distance.Corrected.Value, 0.00001);
        }

        [TestMethod()]
        public void LGC_INPUT_COMPUTE_Freestation()
        {
            string fileContent = @"*TITR 
%Input by Tsunami
*OLOC 
*JSON
*PUNC 
*PREC 7
*INSTR 
   *POLAR AT401_390858 RRR0.5_2885 0 0 0 0
      RRR0.5_2885 1 1 0.001 0.001 0 0 0 0 0 0
*CALA 
   Point_001 0 9.346 0.692 
   Point_002 0.654 -6.038 -0.286 
   Point_003 -0.801 -2.996 0.972 
*POIN
   ST_UnknownTeam_001 0 0 0 
*TSTN ST_UnknownTeam_001 AT401_390858 IHFIX  IH 0 IHSE 2
   *V0
      *ANGL
         Point_001 334.18429 
         Point_002 127.31244 
         Point_003 150.826 
         Point_001 334.18429 
      *ZEND
         Point_001 96.92029 
         Point_002 105.50056 
         Point_003 85.23349 
         Point_001 96.92029 
      *DIST
         Point_001 9.35659713961556 
         Point_002 6.09646605247433 
         Point_003 3.18621301339303 
         Point_001 9.35659713961556 
*END 
";
            string filepath = @"test_LGC_INPUT_COMPUTE_Freestation";
            System.IO.File.WriteAllText(filepath + ".inp", fileContent);

            var strategy = new TSU.Polar.Compensations.FreeStation_LGC2();
            Lgc2 lgc2 = new Lgc2(null, strategy);// peut importe ici on simule la creation d'un input
            lgc2._FileName = filepath;
            lgc2.Run(false, false, true, 30);
            strategy.ParseOutPutAsTheodoliteResultsAsJSON(lgc2._Output);
            Polar.Station.Parameters.Setup.Values r = lgc2._Output._Result;
            Assert.AreEqual("ST_UnknownTeam_001", r.VariablePoints[0]._Name);
            Assert.AreEqual(-0.0000005, r.VariablePoints[0]._Coordinates.Su.X.Value, 0.00001);
            Assert.AreEqual(0.0080386, r.VariablePoints[0]._Coordinates.Su.Z.Sigma, 0.00001);
            Assert.AreEqual(65.8158693, r.vZero.Value, 0.00001);
            Assert.AreEqual(0.0, r.InstrumentHeight.Value, 0.00001);
            Assert.AreEqual(334.1842900, r.Measures[0].Angles.Raw.Horizontal.Value, 0.00001);
            Assert.AreEqual(85.5036941, r.Measures[2].Angles.Corrected.Vertical.Value, 0.00001);
            Assert.AreEqual(9.3581234, r.Measures[3].Distance.Corrected.Value, 0.00001);
        }

        [TestMethod()]
        public void LGC_INPUT_COMPUTE_Orientation()
        {
            string fileContent = @"*TITR 
%Input by Tsunami
*OLOC 
*JSON
*PUNC 
*PREC 7
*INSTR 
   *POLAR AT401_390858 RRR0.5_2885 0 0 0 0
      RRR0.5_2885 1 1 0.001 0.001 0 0 0 0 0 0
*CALA 
   Point_001 0 9.346 0.692 
   Point_002 0.654 -6.038 -0.286 
   Point_003 -0.801 -2.996 0.972 
   ST_UnknownTeam_001 0 0 0 
*TSTN ST_UnknownTeam_001 AT401_390858 
   *V0
      *ANGL
         Point_001 334.18429 
         Point_002 127.31244 
         Point_003 150.826 
         Point_001 334.18429 
      *ZEND
         Point_001 96.92029 
         Point_002 105.50056 
         Point_003 85.23349 
         Point_001 96.92029 
      *DIST
         Point_001 9.35659713961556 
         Point_002 6.09646605247433 
         Point_003 3.18621301339303 
         Point_001 9.35659713961556 
*END 
";
            string filepath = @"test_LGC_INPUT_COMPUTE_Orientation";
            System.IO.File.WriteAllText(filepath + ".inp", fileContent);


            var strategy = new TSU.Polar.Compensations.Orientation_LGC2();
            Lgc2 lgc2 = new Lgc2(null, strategy);// peut importe ici on simule la creation d'un input
            lgc2._FileName = filepath;
            lgc2.Run(false, false, true, 30);
            strategy.ParseOutPutAsTheodoliteResultsAsJSON(lgc2._Output);
            Polar.Station.Parameters.Setup.Values r = lgc2._Output._Result;

            Assert.AreEqual(65.8139322, r.vZero.Value, 0.00001);
            Assert.AreEqual(0.0097536, r.vZero.Sigma, 0.00001);

            Assert.AreEqual(0.2407874, r.InstrumentHeight.Value, 0.00001);
            Assert.AreEqual(0.0005581, r.InstrumentHeight.Sigma, 0.00001);

            Assert.AreEqual(334.1842900, r.Measures[0].Angles.Raw.Horizontal.Value, 0.00001);
            Assert.AreEqual(85.2589069, r.Measures[2].Angles.Corrected.Vertical.Value, 0.00001);
            Assert.AreEqual(9.3568856, r.Measures[3].Distance.Corrected.Value, 0.00001);
        }

        [TestMethod()]
        public void LGC_ReadOutput_PointLancer()
        {
            string fileContent = @"*TITR 
%Input by Tsunami
*OLOC 
*PUNC
*JSON
*PREC 7
*INSTR 
   *POLAR AT401_390858 RRR0.5_2885 0 0 0 0
      RRR0.5_2885 1 1 0.001 0.001 0 0 0 0 0 0
*CALA 
   ST_UnknownTeam_001 0 0 0
   ghost 0 1 0
*POIN
   Point_001 0 9.346 0.692 
*TSTN ST_UnknownTeam_001 AT401_390858  IHFIX  IH 0 IHSE 2
   *V0 
      *ANGL 
         ghost -65.8158693 OBSE 97.536
         Point_001 334.18429 
      *ZEND
         ghost 100.0
         Point_001 96.92029 
      *DIST
         ghost 1.0
         Point_001 9.35659713961556 
*END 
";
            string filepath = @"test_LGC_ReadOutput_PointLancer";
            System.IO.File.WriteAllText(filepath + ".inp", fileContent);

            var strategy = new TSU.Polar.Compensations.PointLancéLGC2();
            Lgc2 lgc2 = new Lgc2(null, strategy);
            lgc2._FileName = filepath;
            lgc2.Run(false, false, true, 30);
            strategy.ParseOutPutAsTheodoliteResultsAsJSON(lgc2._Output);
            Polar.Station.Parameters.Setup.Values r = lgc2._Result;


            Assert.AreEqual(65.8158693, r.vZero.Value, 0.00001);

            Assert.AreEqual("Point_001", r.VariablePoints[0]._Name);
            Assert.AreEqual(0.0000234, r.VariablePoints[0]._Coordinates.Su.X.Value, 0.00001);
            Assert.AreEqual(0.0000, r.VariablePoints[0]._Coordinates.Su.Z.Sigma, 0.00001);

        }

        [TestMethod()]
        public void LGC_Read_COMPUTED_OBSERVATION_WITH_CALA()
        {
            string fileContent = @"*TITR 
%Input by Tsunami
*OLOC 
*PUNC 
*JSON
*PREC 7
*INSTR 
   *POLAR AT401_390858 RRR0.5_2885 0 0 0 0
      RRR0.5_2885 1 1 0.001 0.001 0 0 0 0 0 0
*CALA 
    Unknown.Point.001   4470.56147  5006.77756  332.06134
    Unknown.Point.002   4471.13086  5007.52239  332.05729
    Unknown.Point.003   4474.61817  5007.71471  332.02572
    Unknown.Point.004   4473.36268  5004.91878  331.42744
    Unknown.Point.005   4473.06926  5004.63288  331.44603
    Unknown.STL.001     4472.5259913     5005.9732620      331.1818832
*TSTN Unknown.STL.001 AT401_390858  IHFIX  IH 0 IHSE 2
   *V0 
            *ANGL
         Unknown.Point.001 305.8780 
         Unknown.Point.002 334.4634 
         Unknown.Point.003 36.9484 
         Unknown.Point.004 138.4359 
         Unknown.Point.005 156.6259 
         Unknown.Point.001 305.8780 
      *ZEND
         Unknown.Point.001 74.99439 
         Unknown.Point.002 74.68864 
         Unknown.Point.003 80.85860 
         Unknown.Point.004 88.50416 
         Unknown.Point.005 88.49163 
         Unknown.Point.001 74.99417 
      *DIST
         Unknown.Point.001 2.298018 
         Unknown.Point.002 2.261277 
         Unknown.Point.003 2.849746 
         Unknown.Point.004 1.368135 
         Unknown.Point.005 1.470009 
         Unknown.Point.001 2.298018 
*END 
";
            string filepath = @"test_LGC_Read_COMPUTED_OBSERVATION_WITH_CALA";
            System.IO.File.WriteAllText(filepath + ".inp", fileContent);

            var strategy = new TSU.Polar.Compensations.PointLancésLGC2();
            Lgc2 lgc2 = new Lgc2(null, strategy);// peut importe ici on simule la creation d'un input
            lgc2._FileName = filepath;

            lgc2.Run(false, false, true, 30);
            strategy.ParseOutPutAsTheodoliteResultsAsJSON(lgc2._Output);
            Polar.Station.Parameters.Setup.Values r = lgc2._Result;

            Assert.AreEqual(18.861216, r.vZero.Value, 0.00001);

            Assert.AreEqual(305.8774155, r.Measures[0].Angles.Corrected.Horizontal.Value, 0.00001);
            Assert.AreEqual(88.4997141, r.Measures[4].Angles.Corrected.Vertical.Value, 0.00001);
            Assert.AreEqual(2.8498971, r.Measures[2].Distance.Corrected.Value, 0.00001);

        }

        [TestMethod()]
        public void LGC_Read_COMPUTED_OBSERVATION_WITH_ORIESetToV0()
        { 
            string fileContent = @"*TITR 
%Input by Tsunami
*OLOC 
*JSON
*PUNC 
*PREC 7
*INSTR 
   *POLAR AT401_390858 RRR0.5_2885 0 0 0 0
      RRR0.5_2885 1 1 0.001 0.001 0 0 0 0 0 0
*CALA 
    ghost   4472.5259913     6005.9732620      331.1818832
    Unknown.Point.005   4473.06926  5004.63288  331.44603
    Unknown.STL.001     4472.5259913     5005.9732620      331.1818832
*ORIE Unknown.STL.001 AT401_390858 
    ghost 324.7386297 
*TSTN Unknown.STL.001 AT401_390858  IHFIX  IH 0 IHSE 2
   *V0 
     *ANGL
         Unknown.Point.005 156.6259 
      *ZEND
         Unknown.Point.005 88.49163 
      *DIST
         Unknown.Point.005 1.470009 
*END 
";
            string filepath = @"test_LGC_Read_COMPUTED_OBSERVATION_WITH_ORIESetToV0";
            System.IO.File.WriteAllText(filepath + ".inp", fileContent);

            var strategy = new TSU.Polar.Compensations.Orientation_LGC2();
            Lgc2 lgc2 = new Lgc2(null, strategy); // 
            lgc2._FileName = filepath;
            lgc2.Run(false, false, true, 30);
            strategy.ParseOutPutAsTheodoliteResultsAsJSON(lgc2._Output);
            Polar.Station.Parameters.Setup.Values r = lgc2._Result;

            Assert.AreEqual(18.8594905, r.vZero.Value,0.00001);
            Assert.AreEqual(156.6259, r.Measures[0].Angles.Corrected.Horizontal.Value, 0.0001);
            Assert.AreEqual(88.4997141, r.Measures[0].Angles.Corrected.Vertical.Value, 0.0001);
            Assert.AreEqual(1.4702171, r.Measures[0].Distance.Corrected.Value, 0.00001);

        }

        [TestMethod()]
        public void LGC_Read_COMPUTED_OBSERVATION_WITH_ORIE()
        {
            string fileContent = @"*TITR 
%Input by Tsunami
*OLOC 
*JSON
*PUNC 
*PREC 7
*INSTR 
   *POLAR AT401_390858 RRR0.5_2885 0 0 0 0
      RRR0.5_2885 1 1 0.001 0.001 0 0 0 0 0 0
*CALA 
    Unknown.Point.001   4470.56147  5006.77756  332.06134
    Unknown.Point.005   4473.06926  5004.63288  331.44603
    Unknown.STL.001     4472.5259913     5005.9732620      331.1818832
*ORIE Unknown.STL.001 AT401_390858 
    Unknown.Point.001 324.7386297 
*TSTN Unknown.STL.001 AT401_390858  IHFIX  IH 0 IHSE 2
   *V0 
     *ANGL
         Unknown.Point.005 156.6259 
      *ZEND
         Unknown.Point.005 88.49163 
      *DIST
         Unknown.Point.005 1.470009 
*END 
";
            string filepath = @"test-LGC_Read_COMPUTED_OBSERVATION_WITH_ORIE";
            System.IO.File.WriteAllText(filepath + ".inp", fileContent);

            var strategy = new TSU.Polar.Compensations.PointLancésLGC2();
            Lgc2 lgc2 = new Lgc2(null, strategy); // peut importe ici on simule la creation d'un input
            lgc2._FileName = filepath;
            lgc2.Run(false, false, true, 30);
            strategy.ParseOutPutAsTheodoliteResultsAsJSON(lgc2._Output);
            Polar.Station.Parameters.Setup.Values r = lgc2._Result;

            Assert.AreEqual(18.8594905, r.vZero.Value,0.0001);
            Assert.AreEqual(156.6259, r.Measures[0].Angles.Corrected.Horizontal.Value, 0.0001);
            Assert.AreEqual(88.4997141, r.Measures[0].Angles.Corrected.Vertical.Value, 0.0001);
            Assert.AreEqual(1.4702171, r.Measures[0].Distance.Corrected.Value, 0.0001);
        }

        [TestMethod()]
        public void LGC_CreateInput4Goto_WITH_Refs()
        {
            Polar.Station st = new Polar.Station();
            var _StationPoint = new E.Point()
            {
                _Name = "Unknown.STL.001",
                _Coordinates = new E.CoordinatesInAllSystems()
                {
                    Ccs = new E.Coordinates()
                    {
                        X = new DoubleValue(4472.5259913, -999.9),
                        Y = new DoubleValue(5005.9732620, -999.9),
                        Z = new DoubleValue(331.1818832, -999.9)
                    }
                }
            };
            st.Parameters2._Instrument = new TSU.Common.Instruments.Device.AT40x.Instrument() { _Name = "AT401_390858", _Model = "At40x" };
            st.Parameters2.Setups = new Polar.Station.Parameters.Setup();
            st.Parameters2.Setups.InitialValues = new Polar.Station.Parameters.Setup.Values()
            {
                InstrumentHeight = new DoubleValue(0, 0.0001),
                IsInstrumentHeightKnown = Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight.Known,
                StationPoint = _StationPoint
            };
            //st.Parameters2.Setups.FinalValues.vZero.Value = 18.8594905;

            Polar.Measure m1 = new Polar.Measure()
            {
                _Status = new M.States.Good(),
                _Point = new E.Point()
                {
                    _Name = "Unknown.Point.001",
                    _Coordinates = new E.CoordinatesInAllSystems()
                    {
                        Ccs = new E.Coordinates()
                        {
                            X = new DoubleValue(4470.56147, -999.9),
                            Y = new DoubleValue(5006.77756, -999.9),
                            Z = new DoubleValue(332.06134, -999.9)
                        }
                    }
                },
                _OriginalPoint = new E.Point()
                {
                    _Name = "Unknown.Point.001",
                    _Coordinates = new E.CoordinatesInAllSystems()
                    {
                        Ccs = new E.Coordinates()
                        {
                            X = new DoubleValue(4470.56147, -999.9),
                            Y = new DoubleValue(5006.77756, -999.9),
                            Z = new DoubleValue(332.06134, -999.9)
                        }
                    }
                },
                Angles = new M.MeasureOfAngles()
                {
                    Corrected = new E.Angles()
                    {
                        Horizontal = new DoubleValue(305.8780, 0.00001),
                        Vertical = new DoubleValue(74.99439, 0.00001)
                    }
                },
                Distance = new M.MeasureOfDistance()
                {
                    Corrected = new DoubleValue(2.298018, 0.000001)
                }
            };

            Polar.Measure m2 = new Polar.Measure()
            {
                _Status = new M.States.Good(),
                _Point = new E.Point()
                {
                    _Name = "Unknown.Point.002",
                    _Coordinates = new E.CoordinatesInAllSystems()
                    {
                        Ccs = new E.Coordinates()
                        {
                            X = new DoubleValue(4471.13086, -999.9),
                            Y = new DoubleValue(5007.52239, -999.9),
                            Z = new DoubleValue(332.05729, -999.9)
                        }
                    }
                },
                _OriginalPoint = new E.Point()
                {
                    _Name = "Unknown.Point.002",
                    _Coordinates = new E.CoordinatesInAllSystems()
                    {
                        Ccs = new E.Coordinates()
                        {
                            X = new DoubleValue(4471.13086, -999.9),
                            Y = new DoubleValue(5007.52239, -999.9),
                            Z = new DoubleValue(332.05729, -999.9)
                        }
                    }
                },
                Angles = new M.MeasureOfAngles()
                {
                    Corrected = new E.Angles()
                    {
                        Horizontal = new DoubleValue(334.4634, 0.00001),
                        Vertical = new DoubleValue(74.68864, 0.00001)
                    }
                },
                Distance = new M.MeasureOfDistance()
                {
                    Corrected = new DoubleValue(2.261277, 0.000001)
                }
            };

            Polar.Measure m3 = new Polar.Measure()
            {
                _Status = new M.States.Good(),
                _Point = new E.Point()
                {
                    _Name = "Unknown.Point.003",
                    _Coordinates = new E.CoordinatesInAllSystems()
                    {
                        Ccs = new E.Coordinates()
                        {
                            X = new DoubleValue(4473.06926, -999.9),
                            Y = new DoubleValue(5004.63288, -999.9),
                            Z = new DoubleValue(331.44603, -999.9)
                        }
                    }
                },
                _OriginalPoint = new E.Point()
                {
                    _Name = "Unknown.Point.003",
                    _Coordinates = new E.CoordinatesInAllSystems()
                    {
                        Ccs = new E.Coordinates()
                        {
                            X = new DoubleValue(4473.06926, -999.9),
                            Y = new DoubleValue(5004.63288, -999.9),
                            Z = new DoubleValue(331.44603, -999.9)
                        }
                    }
                },
                Angles = new M.MeasureOfAngles()
                {
                    Corrected = new E.Angles()
                    {
                        Horizontal = new DoubleValue(156.6259, 2000000),
                        Vertical = new DoubleValue(88.49163, 2000000)
                    }
                },
                Distance = new M.MeasureOfDistance()
                {
                    Corrected = new DoubleValue(1.470009, 1000000)
                }
            };

            st.MeasuresTaken.Add(m1); m1.GeodeRole = IO.SUSoft.Geode.Roles.Cala; 
            st.MeasuresTaken.Add(m2); m2.GeodeRole = IO.SUSoft.Geode.Roles.Cala; 
            st.MeasuresTaken.Add(m3); m2.GeodeRole = IO.SUSoft.Geode.Roles.Cala;


            var strategy = new TSU.Polar.Compensations.PointLancésLGC2();
            Lgc2 lgc2 = new Lgc2(st, new TSU.Polar.Compensations.FreeStation_LGC2());
            lgc2.Run(false, false, true, 30);
            strategy.ParseOutPutAsTheodoliteResultsAsJSON(lgc2._Output);

            Assert.AreEqual(18.85674, lgc2._Result.vZero.Value, 0.01);

            Assert.AreEqual(334.46341, lgc2._Result.Measures[1].Angles.Corrected.Horizontal.Value, 0.0001);
            Assert.AreEqual(74.6886455, lgc2._Result.Measures[1].Angles.Corrected.Vertical.Value, 0.0001);
        }
    }
}
