﻿using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using E = TSU.Common.Elements;
using TestsTsunami;
using TSU.Views;
using TSU.Common.Elements.Manager;

namespace TSU.Unit_Tests
{
    [TestClass()]
    public class ViewStrategy3DTests
    {
        [TestMethod()]
        public void ViewStrategy3DTest()
        {
            Form form = new Form();

            E.Manager.Module m = new E.Manager.Module(null);
            
            for (int i = 0; i < 10; i++)
			{
			    m.AllElements.Add(new FilledMockPoint().Object);
			}
            ManagerView f = m.View;
            f.TopLevel = false;
            form.Controls.Add(f);

            form.Visible = true;
            form.CreateControl();
            f.currentStrategy = new Common.Strategies.Views.ThreeDimension(f);
            //f._strategy.Show();
            form.Visible =false;
            //form.ShowDialog();
            form.Dispose();
            Assert.IsTrue(true);
        }

    }
}
