﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Xml;
using Moq;
using TSU;
using TSU.ENUM;
using TSU.Common.Elements;
using T = TSU.Tools;
using I = TSU.Common.Instruments;
using M = TSU.Common.Measures;
using TestsTsunami;
using TSU.Common.Instruments.Reflector;
using System.Collections.ObjectModel;
using TSU.Common.Compute.Compensations.Strategies;

namespace TestsTsunami
{

    // Those class create mockup of interface with help of the Moq framework (need to add MOq.dll to the references)
    // And fill the mock up with random values
    public static class Mocks
    {
        public static List<Point> MockPoints(string content, Coordinates.ReferenceFrames referenceFrame)
        {

            List<Point> points = new List<Point>();
            using (System.IO.StringReader reader = new System.IO.StringReader(content))
            {
                string line = string.Empty;
                do
                {
                    line = reader.ReadLine();
                    if (line != null)
                    {
                        Point p = new Point(); ;
                        Coordinates.CoordinateSystemsTsunamiTypes type = Coordinates.GetCoordinateTsunamiType(referenceFrame);
                        Coordinates c = T.Conversions.FileFormat.IdXYZLineToCoordinates(line, referenceFrame, Coordinates.CoordinateSystemsTypes.AutomaticBasedOnReferenceFrame);
                        if (c == null) continue;
                        p._Name = c._Name;
                        switch (type)
                        {
                            case Coordinates.CoordinateSystemsTsunamiTypes.CCS:
                                p._Coordinates.Ccs = c.Clone() as Coordinates;
                                p._Coordinates.Ccs.Z = p._Coordinates.Ccs.Z;
                                break;
                            case Coordinates.CoordinateSystemsTsunamiTypes.MLA:
                                p._Coordinates.Mla = c;
                                break;
                            case Coordinates.CoordinateSystemsTsunamiTypes.SU:
                                p._Coordinates.Su = c;
                                break;
                            case Coordinates.CoordinateSystemsTsunamiTypes.PHYS:
                                p._Coordinates.Physicist = c;
                                break;
                            case Coordinates.CoordinateSystemsTsunamiTypes.UserDefined:
                                p._Coordinates.UserDefined = c;
                                break;
                            case Coordinates.CoordinateSystemsTsunamiTypes.Unknown:
                            default:
                                break;
                        }
                        points.Add(p);
                    }

                } while (line != null);
            }
            return points;
        }

    }
    public class FilledMockInstrument : Mock<I.Sensor> // this use the mock framework to create a object of IInstrument
    {
        public FilledMockInstrument()
        {
            this.CallBase = true;
            this.Setup(m => m._Brand).Returns("Leica"); // here we fill the mock properties with value and returns for methodes
            this.Setup(m => m.Id).Returns("AT402-359056");
            this.Setup(m => m._InstrumentClass).Returns(I.InstrumentClasses.LASER_TRACKER);
            //this.Setup(m => m._IsOn).Returns(true);
            //this.Setup(m => m._LastMeasure).Returns(); cycling
            this.Setup(m => m._Model).Returns("At402");
            this.Setup(m => m._SerialNumber).Returns("359056");
            //this.Setup(m => m._view).Returns();
            //this.Setup(m => m.Management.Measure()).Returns(new Result());
            //this.Setup(m => m.WeatherConditionsStatic).Returns((IWeatherConditions)new FilledMockWeatherConditions().Object);
        }
    }

    public class FilledMockReflector : Mock<Reflector> // this use the mock framework to create a object of IInstrument
    {
        public FilledMockReflector()
        {
            this.CallBase = true;
            this.Setup(m => m._Brand).Returns("Leica"); // here we fill the mock properties with value and returns for methodes
            this.Setup(m => m.Id).Returns("RRR15");
            this.Setup(m => m._Name).Returns("RRR15");
            this.SetupGet(m => m._InstrumentType).Returns(I.InstrumentTypes.PBT);
            this.Setup(m => m._Model).Returns("RRR");
            this.Setup(m => m._SerialNumber).Returns("3");
        }

        public FilledMockReflector(string sn)
        {
            this.CallBase = true;
            this.Setup(m => m._Brand).Returns("Leica"); // here we fill the mock properties with value and returns for methodes
            this.Setup(m => m.Id).Returns("CCR1.5_5589");
            this.Setup(m => m._Name).Returns("CCR1.5");
            this.SetupGet(m => m._InstrumentType).Returns(I.InstrumentTypes.CCR1_5);
            this.Setup(m => m._Model).Returns("RRR");
            this.Setup(m => m._SerialNumber).Returns(sn);
        }
    }

    public class FilledMockPolarInstrument : Mock<I.Theodolite> // this use the mock framework to create a object of IInstrument
    {
        public FilledMockPolarInstrument()
        {
            this.CallBase = true;
            //this.Setup(m => m._Brand).Returns("Leica"); // here we fill the mock properties with value and returns for methodes
            this.Setup(m => m.Id).Returns("AT402-359056");
            this.Setup(m => m._Name).Returns("AT402-359056");
            this.Setup(m => m._InstrumentClass).Returns(I.InstrumentClasses.THEODOLITE);
            //this.Setup(m => m._IsOn).Returns(true);
            //this.Setup(m => m._LastMeasure).Returns(); cycling
            this.Setup(m => m._Model).Returns("At402");
            this.Setup(m => m._SerialNumber).Returns("359056");
            //this.Setup(m => m._view).Returns();
            //this.Setup(m => m.Management.Measure()).Returns(new Result());
            //this.Setup(m => m.WeatherConditionsStatic).Returns((IWeatherConditions)new FilledMockWeatherConditions().Object);
            //this.Setup(m => m.).Returns("At402");
        }
    }

    public class FilledMockTotalStation : Mock<I.TotalStation> // this use the mock framework to create a object of IInstrument
    {
        public FilledMockTotalStation()
        {
            this.CallBase = true;
            this.Setup(m => m._Brand).Returns("Leica"); // here we fill the mock properties with value and returns for methodes
            this.Setup(m => m.Id).Returns("TDA5005  438198");
            this.Setup(m => m._Name).Returns("TDA5005_438198");
            this.Setup(m => m._InstrumentClass).Returns(I.InstrumentClasses.TACHEOMETRE);
            //this.Setup(m => m._IsOn).Returns(true);
            this.Setup(m => m.EtalonnageParameterList).Returns(GetFilledEtalonnageParameter()); // "CCR1.5_5589"

            //this.Setup(m => m._LastMeasure).Returns(); cycling
            this.Setup(m => m._Model).Returns("TDA5005");
            this.Setup(m => m._SerialNumber).Returns("438198");
            //this.Setup(m => m._view).Returns();
            //this.Setup(m => m.Management.Measure()).Returns(new Result());
            //this.Setup(m => m.WeatherConditionsStatic).Returns((WeatherConditions)new FilledMockWeatherConditions().Object);
            //this.Setup(m => m.).Returns("At402");
        }

        public List<I.EtalonnageParameter.Theodolite> GetFilledEtalonnageParameter()
        {
            string s = @"<?xml version=""1.0""?>
<EtalonnageParameterTheodolite>
  <calibrationDate>04-FEB-2010</calibrationDate>
  <fourrierWaveLength>104.75333</fourrierWaveLength>
  <fourrierScale>1.000002363</fourrierScale>
  <fourrierA0Constant>-0.0344697468</fourrierA0Constant>
  <fourrierMaximumDistance>1.10703</fourrierMaximumDistance>
  <fourrierMinimumDistance>105.86036</fourrierMinimumDistance>
  <fourrierCoeff_A>
    <double>2.0876E-06</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
  </fourrierCoeff_A>
  <fourrierCoeff_B>
    <double>7.607E-07</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
    <double>0</double>
  </fourrierCoeff_B>
  <Ecart_axe_edm>0</Ecart_axe_edm>
  <Ecart_axe_chaine>0</Ecart_axe_chaine>
  <PrismeRef _Name=""Unknown"" _Id=""CCR1.5_5589"" _Model=""CCR1.5"" _SerialNumber=""5589"" _InstrumentType=""CCR1_5"" CalibrationDate=""0001-01-01T00:00:00"" Constante=""0"" Diameter=""0"" distCorrectionKnown=""true"" distCorrectionValue=""0"" sigmaDCorr=""0"" sigmaTargetCentering=""0"">
    <_ReflectorType>AGA</_ReflectorType>
  </PrismeRef>
  <Indice_corr_dossier_used>0</Indice_corr_dossier_used>
  <Indice_corr_dossier_used_ATR>0</Indice_corr_dossier_used_ATR>
  <Coll_ATR>false</Coll_ATR>
  <Corr_H_ATR>0</Corr_H_ATR>
  <Corr_V_ATR>0</Corr_V_ATR>
  <Commentaire_C>
    <string d3p1:nil=""true"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
    <string d3p1:nil=""true"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
    <string d3p1:nil=""true"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
  </Commentaire_C>
  <C_H>
    <double>0</double>
    <double>0</double>
    <double>0</double>
  </C_H>
  <C_V>
    <double>0</double>
    <double>0</double>
    <double>0</double>
  </C_V>
  <C_connun>
    <boolean>false</boolean>
    <boolean>false</boolean>
    <boolean>false</boolean>
  </C_connun>
  <D_Collim>
    <string d3p1:nil=""true"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
    <string d3p1:nil=""true"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
    <string d3p1:nil=""true"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
  </D_Collim>
</EtalonnageParameterTheodolite>";
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            System.IO.StreamWriter writer = new System.IO.StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;

            object p2 = new I.EtalonnageParameter.Theodolite();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref p2, stream);
            List<I.EtalonnageParameter.Theodolite> l = new List<I.EtalonnageParameter.Theodolite>() { (I.EtalonnageParameter.Theodolite)p2 };
            return l;
        }
    }


    public class FilledMockDoubleValue : Mock<DoubleValue>
    {
        Random rnd = new Random(Guid.NewGuid().GetHashCode());
        public FilledMockDoubleValue()
        {
            this.Setup(m => m.Value).Returns(rnd.Next(0, 10000000) / 100000.0);
            this.Setup(m => m.Sigma).Returns(rnd.Next(0, 100) / 1000.0);
        }

        public FilledMockDoubleValue(double v, double s)
        {
            this.Setup(m => m.Value).Returns(v);
            this.Setup(m => m.Sigma).Returns(s);
        }
    }

    public class FilledMockMlaCoordinates : Mock<Coordinates>
    {
        public FilledMockMlaCoordinates()
        {
            this.Setup(m => m.X).Returns(new FilledMockDoubleValue().Object);
            this.Setup(m => m.Y).Returns(new FilledMockDoubleValue().Object);
            this.Setup(m => m.Z).Returns(new FilledMockDoubleValue().Object);
        }
        public FilledMockMlaCoordinates(int refpointnumber)
        {
            switch (refpointnumber)
            {

                case 0:
                    Random rnd = new Random(Guid.NewGuid().GetHashCode());
                    this.Setup(m => m.X).Returns(new FilledMockDoubleValue(rnd.Next(-10000, 10000) / 1000, rnd.Next(-10000, 10000) / 1000).Object);
                    this.Setup(m => m.Y).Returns(new FilledMockDoubleValue(rnd.Next(-10000, 10000) / 1000, rnd.Next(-10000, 10000) / 1000).Object);
                    this.Setup(m => m.Z).Returns(new FilledMockDoubleValue(rnd.Next(-10000, 10000) / 1000, rnd.Next(-10000, 10000) / 1000).Object);
                    break;
                case 1:
                    this.Setup(m => m.X).Returns(new FilledMockDoubleValue(-0.468410, 0.00005).Object);
                    this.Setup(m => m.Y).Returns(new FilledMockDoubleValue(-5.671920, 0.00005).Object);
                    this.Setup(m => m.Z).Returns(new FilledMockDoubleValue(-1.043850, 0.00005).Object);
                    break;
                case 2:
                    this.Setup(m => m.X).Returns(new FilledMockDoubleValue(1.290810, 0.00005).Object);
                    this.Setup(m => m.Y).Returns(new FilledMockDoubleValue(-5.677750, 0.00005).Object);
                    this.Setup(m => m.Z).Returns(new FilledMockDoubleValue(-1.091920, 0.00005).Object);
                    break;
                case 3:
                    this.Setup(m => m.X).Returns(new FilledMockDoubleValue(-7.997980, 0.00005).Object);
                    this.Setup(m => m.Y).Returns(new FilledMockDoubleValue(-1.940870, 0.00005).Object);
                    this.Setup(m => m.Z).Returns(new FilledMockDoubleValue(0.059210, 0.00005).Object);
                    break;
                case 4:
                    this.Setup(m => m.X).Returns(new FilledMockDoubleValue(1.404, 0.00005).Object);
                    this.Setup(m => m.Y).Returns(new FilledMockDoubleValue(-3.732, 0.00005).Object);
                    this.Setup(m => m.Z).Returns(new FilledMockDoubleValue(0.225, 0.00005).Object);
                    break;
                default:
                    break;
            }

        }

    }

    public class FilledMockCcsCoordinates : Mock<Coordinates>
    {
        public FilledMockCcsCoordinates()
        {
            this.Setup(m => m.X).Returns(new FilledMockDoubleValue().Object);
            this.Setup(m => m.Y).Returns(new FilledMockDoubleValue().Object);
            this.Setup(m => m.Z).Returns(new FilledMockDoubleValue().Object);
        }
        public FilledMockCcsCoordinates(int refpointnumber)
        {
            switch (refpointnumber)
            {

                case 0:
                    Random rnd = new Random(Guid.NewGuid().GetHashCode());
                    this.Setup(m => m.X).Returns(new FilledMockDoubleValue(rnd.Next(-10000, 10000) / 1000, rnd.Next(-10000, 10000) / 1000).Object);
                    this.Setup(m => m.Y).Returns(new FilledMockDoubleValue(rnd.Next(-10000, 10000) / 1000, rnd.Next(-10000, 10000) / 1000).Object);
                    this.Setup(m => m.Z).Returns(new FilledMockDoubleValue(rnd.Next(-10000, 10000) / 1000, rnd.Next(-10000, 10000) / 1000).Object);
                    break;
                case 1:

                    this.Setup(m => m.X).Returns(new FilledMockDoubleValue(4475.1077099, 0.00005).Object);
                    this.Setup(m => m.Y).Returns(new FilledMockDoubleValue(5011.0441434, 0.00005).Object);
                    this.Setup(m => m.Z).Returns(new FilledMockDoubleValue(330.1378852, 0.00005).Object);
                    break;
                case 2:
                    this.Setup(m => m.X).Returns(new FilledMockDoubleValue(4473.4818009, 0.00005).Object);
                    this.Setup(m => m.Y).Returns(new FilledMockDoubleValue(5011.7159601, 0.00005).Object);
                    this.Setup(m => m.Z).Returns(new FilledMockDoubleValue(330.0898154, 0.00005).Object);
                    break;
                case 3:
                    this.Setup(m => m.X).Returns(new FilledMockDoubleValue(4480.6630608, 0.00005).Object);
                    this.Setup(m => m.Y).Returns(new FilledMockDoubleValue(5004.7392708, 0.00005).Object);
                    this.Setup(m => m.Z).Returns(new FilledMockDoubleValue(331.2409478, 0.00005).Object);
                    break;
                case 4:
                    this.Setup(m => m.X).Returns(new FilledMockDoubleValue(4472.6404349, 0.00005).Object);
                    this.Setup(m => m.Y).Returns(new FilledMockDoubleValue(5009.9587199, 0.00005).Object);
                    this.Setup(m => m.Z).Returns(new FilledMockDoubleValue(331.4067339, 0.00005).Object);
                    break;
                default:
                    break;
            }

        }

    }

    public class FilledMockWeatherConditions : Mock<M.WeatherConditions>
    {
        public FilledMockWeatherConditions()
        {
            this.CallBase = true;
            this.Setup(m => m.dryTemperature).Returns(21);
            this.Setup(m => m.humidity).Returns(65);
            this.Setup(m => m.pressure).Returns(980);
            this.Setup(m => m.WetTemperature).Returns(22);
        }
    }

    public class FilledMockStation : Mock<TSU.Polar.Station>
    {
        public FilledMockStation()
        {
            this.CallBase = true;

            this.Setup(m => m._Name).Returns("aze");
            this.Setup(m => m.Id).Returns("UneStation");
            FilledMockStationParameters s = new FilledMockStationParameters();

        }
    }
    public class FilledMockTheodoliteStationFS : Mock<TSU.Polar.Station>
    {
        public FilledMockTheodoliteStationFS()
        {
            this.CallBase = true;

            this.Setup(m => m.Parameters2).Returns(new FilledMockStationsParameters().Object);
            CloneableList<M.Measure> l = new CloneableList<M.Measure>();
            l.Add(new FilledMockMeasureTheodolite(1, true).Object);
            l.Add(new FilledMockMeasureTheodolite(2, true).Object);
            l.Add(new FilledMockMeasureTheodolite(3, true).Object);
            this.Setup(m => m.MeasuresTaken).Returns(l);
            CloneableList<Point> l2 = new CloneableList<Point>();
            l2.Add(new FilledMockPoint(1).Object);
            l2.Add(new FilledMockPoint(2).Object);
            l2.Add(new FilledMockPoint(3).Object);
            this.Setup(m => m.PointsMeasured).Returns(new ReadOnlyCollection<Point>(l2));
        }
    }
    public class FilledMockTheodoliteStation : Mock<TSU.Polar.Station>
    {
        public FilledMockTheodoliteStation()
        {
            this.CallBase = true;

            this.Setup(m => m.Parameters2).Returns(new FilledMockStationsParameters().Object);
            CloneableList<M.Measure> l = new CloneableList<M.Measure>();
            l.Add(new FilledMockMeasureTheodolite(1).Object);
            l.Add(new FilledMockMeasureTheodolite(2).Object);
            l.Add(new FilledMockMeasureTheodolite(3).Object);
            this.Setup(m => m.MeasuresTaken).Returns(l);
            CloneableList<Point> l2 = new CloneableList<Point>();
            l2.Add(new FilledMockPoint(1).Object);
            l2.Add(new FilledMockPoint(2).Object);
            l2.Add(new FilledMockPoint(3).Object);
            this.Setup(m => m.PointsMeasured).Returns(new ReadOnlyCollection<Point>(l2));
        }
    }
    public class FilledMockTheodoliteStationWithoutCccsCooridnates : Mock<TSU.Polar.Station>
    {
        public FilledMockTheodoliteStationWithoutCccsCooridnates()
        {
            this.CallBase = true;
            this.Setup(m => m.Parameters2).Returns(new FilledMockStationsParameters().Object);
            CloneableList<M.Measure> l = new CloneableList<M.Measure>();
            l.Add(new FilledMockMeasureTheodolite(1).Object);
            l.Add(new FilledMockMeasureTheodolite(2).Object);
            l.Add(new FilledMockMeasureTheodolite(3).Object);
            this.Setup(m => m.MeasuresTaken).Returns(l);
            CloneableList<TSU.Polar.Measure> l3 = new CloneableList<TSU.Polar.Measure>();
            l3.Add(new FilledMockMeasureTheodolite(1).Object);
            l3.Add(new FilledMockMeasureTheodolite(2).Object);
            l3.Add(new FilledMockMeasureTheodolite(3).Object);
            this.Setup(m => m.References).Returns(l3);
            CloneableList<Point> l2 = new CloneableList<Point>();
            l2.Add(new FilledMockPointWithoutCCS(1).Object);
            l2.Add(new FilledMockPointWithoutCCS(2).Object);
            l2.Add(new FilledMockPointWithoutCCS(3).Object);
            this.Setup(m => m.PointsMeasured).Returns(new ReadOnlyCollection<Point>(l2));
        }
    }

    public class FilledMockTheodoliteStationWithoutCccsCooridnatesUnknownHeight : Mock<TSU.Polar.Station>
    {
        public FilledMockTheodoliteStationWithoutCccsCooridnatesUnknownHeight()
        {
            this.CallBase = true;
            this.Setup(m => m.Parameters2).Returns(new FilledMockStationsParameters().Object);
            CloneableList<M.Measure> l = new CloneableList<M.Measure>();
            l.Add(new FilledMockMeasureTheodolite(1).Object);
            l.Add(new FilledMockMeasureTheodolite(2).Object);
            l.Add(new FilledMockMeasureTheodolite(3).Object);
            this.Setup(m => m.MeasuresTaken).Returns(l);
            CloneableList<TSU.Polar.Measure> l3 = new CloneableList<TSU.Polar.Measure>();
            l3.Add(new FilledMockMeasureTheodolite(1).Object);
            l3.Add(new FilledMockMeasureTheodolite(2).Object);
            l3.Add(new FilledMockMeasureTheodolite(3).Object);
            this.Setup(m => m.References).Returns(l3);
            CloneableList<Point> l2 = new CloneableList<Point>();
            l2.Add(new FilledMockPointWithoutCCS(1).Object);
            l2.Add(new FilledMockPointWithoutCCS(2).Object);
            l2.Add(new FilledMockPointWithoutCCS(3).Object);
            this.Setup(m => m.PointsMeasured).Returns(new ReadOnlyCollection<Point>(l2));
        }
    }

    public class FilledMockStationParameters : Mock<TSU.Common.Station.Parameters>
    {
        public FilledMockStationParameters()
        {
            this.CallBase = true;
            this.Setup(m => m._Date).Returns(DateTime.Now);
            this.Setup(m => m._Instrument).Returns((I.Sensor)new FilledMockPolarInstrument().Object);
            this.Setup(m => m._IsSetup).Returns(true);
            this.Setup(m => m._Operation).Returns(new TSU.Common.Operations.Operation(10356, ""));
            this.Setup(m => m._Team).Returns("PS-DEP");
        }
    }

    public class FilledMockStationsParameters : Mock<TSU.Polar.Station.Parameters>
    {
        public FilledMockStationsParameters()
        {
            this.CallBase = true;
            this.Setup(m => m._Date).Returns(DateTime.Now);
            this.Setup(m => m._Instrument).Returns((I.Sensor)new FilledMockPolarInstrument().Object);
            this.Setup(m => m._IsSetup).Returns(true);
            this.Setup(m => m._Operation).Returns(new TSU.Common.Operations.Operation(10356, ""));
            this.Setup(m => m._Team).Returns("PS-DEP");
            Point p = new FilledMockPoint(4).Object;
            var setup = new TSU.Polar.Station.Parameters.Setup.Values()
            {
                InstrumentHeight = new DoubleValue(0, 0),
                IsInstrumentHeightKnown = TSU.Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight.Known,
                StationPoint = p
            };
            var setups = new TSU.Polar.Station.Parameters.Setup();
            setups.InitialValues = setup;

            this.Setup(m => m.Setups).Returns(setups);
        }
    }
    public class FilledMockTotalStationParameters : Mock<TSU.Polar.Station.Parameters>
    {
        public FilledMockTotalStationParameters()
        {
            this.CallBase = true;
            this.Setup(m => m._Date).Returns(DateTime.Now);
            this.Setup(m => m._Instrument).Returns((I.Sensor)new FilledMockTotalStation().Object);
            this.Setup(m => m._IsSetup).Returns(true);
            this.Setup(m => m._Operation).Returns(new TSU.Common.Operations.Operation(10356, ""));
            this.Setup(m => m._Team).Returns("PS-DEP");
            this.Setup(m => m._InstrumentHeight).Returns(new DoubleValue(0, 0));
            Point p = new FilledMockPoint(4).Object;
            this.Setup(m => m._StationPoint).Returns(p);
        }
    }

    public class FilledMockTotalStationStation : Mock<TSU.Polar.Station>
    {
        public FilledMockTotalStationStation()
        {
            this.CallBase = true;
            CloneableList<M.Measure> l = new CloneableList<M.Measure>();
            l.Add(new FilledMockMeasureTheodolite().Object);
            l.Add(new FilledMockMeasureTheodolite().Object);
            l.Add(new FilledMockMeasureTheodolite().Object);
            this.Setup(m => m.MeasuresTaken).Returns(l);
            Point p = new FilledMockPoint().Object;
            this.Setup(m => m.ParametersBasic).Returns(new FilledMockTotalStationParameters().Object);
        }
    }

    public class FilledMockMeasureTheodolite : Mock<TSU.Polar.Measure>
    {
        public FilledMockMeasureTheodolite()
        {
            this.Setup(m => m.Comment).Returns("Blabla");
            this.Setup(m => m._Date).Returns(DateTime.Now);
            //this.Setup(m => m.).Returns((Management.Instrument)new FilledMockInstrument().Object);
            //this.Setup(m => m._Point).Returns((IElement)new FilledMockPoint().Object);
            //this.Setup(m => m._Station).Returns((IStation)new FilledMockStation().Object);
            this.Setup(m => m._Status).Returns(new M.States.Good());
            I.FaceType f = I.FaceType.Face2;
            M.MeasureOfAngles moa = new FilledMockMeasureOfAngles(1, f).Object as M.MeasureOfAngles;
            this.Setup(m => m.Angles).Returns(moa);
            //this.Setup(m => m.IsDoubleFace).Returns(true);

        }
        public FilledMockMeasureTheodolite(int refencePointNumber, bool usedAsRef = false)
        {
            this.Setup(m => m.Comment).Returns("Blabla");
            this.Setup(m => m._Date).Returns(DateTime.Now);
            //this.Setup(m => m._Station._Parameters._Instrument).Returns((Sensor)new FilledMockInstrument().Object);
            this.Setup(m => m._OriginalPoint).Returns(new FilledMockPoint(refencePointNumber).Object);
            this.Setup(m => m._Point).Returns(new FilledMockPoint(refencePointNumber).Object);
            this.Setup(m => m._Status).Returns(new M.States.Good());
            this.Setup(m => m.Extension).Returns(new DoubleValue(0, 0));
            if (usedAsRef)
                this.Setup(m => m.UsedAsReference).Returns(true);
            this.Setup(m => m.TheoreticalOffsetBetweenCoordinatesToStakeOutAndKnownCoordinates).Returns(new DoubleValue(0, 0));
            this.Setup(m => m.Angles).Returns(new FilledMockMeasureOfAngles(refencePointNumber, I.FaceType.DoubleFace).Object);
            this.Setup(m => m.Distance).Returns((M.MeasureOfDistance)new FilledMockMeasureOfDistance(refencePointNumber).Object);
        }
    }

    public class FilledMockAngles : Mock<Angles>
    {
        public FilledMockAngles(int refencePointNumber, I.FaceType face)
        {
            this.CallBase = true;

            DoubleValue h;
            DoubleValue v;
            DoubleValue hm;
            ////DoubleValue vm;

            switch (refencePointNumber)
            {
                case 1:
                    if (face == I.FaceType.Face2)
                    {
                        h = new DoubleValue(96.03283, 0.0003);
                        v = new DoubleValue(267.52213, 0.0003);
                    }
                    else
                    {
                        h = new DoubleValue(296.03281, 0.0003);
                        v = new DoubleValue(132.47785, 0.0003);
                    }
                    hm = new DoubleValue(296.03282, 0.0003);
                    hm = new DoubleValue(132.47786, 0.0003);
                    break;
                case 2:
                    if (face == I.FaceType.Face2)
                    {
                        h = new DoubleValue(50.85947, 0.0003);
                        v = new DoubleValue(857.08639, 0.0003);
                    }
                    else
                    {
                        h = new DoubleValue(250.85947, 0.0003);
                        v = new DoubleValue(142.91361, 0.0003);
                    }
                    break;
                case 3:
                    if (face == I.FaceType.Face2)
                    {
                        h = new DoubleValue(59.13208, 0.0003);
                        v = new DoubleValue(897.29585, 0.0003);
                    }
                    else
                    {
                        h = new DoubleValue(359.13208, 0.0003);
                        v = new DoubleValue(102.70415, 0.0003);
                    }

                    break;
                default:
                    h = new DoubleValue(100, 0.00001);
                    v = new DoubleValue(100, 0.00001);
                    break;
            }
            this.Setup(m => m.Horizontal).Returns(h);
            this.Setup(m => m.Vertical).Returns(v);
        }
    }

    public class FilledMockMeasureOfAngles : Mock<M.MeasureOfAngles>
    {
        public FilledMockMeasureOfAngles(int refencePointNumber, I.FaceType face)
        {
            this.Setup(m => m.Compensator).Returns(new I.Compensator());
            this.Setup(m => m.Raw).Returns(new FilledMockAngles(refencePointNumber, face).Object);
            this.Setup(m => m.Corrected).Returns(this.Object.Raw);
            this.Setup(m => m.CorrectForCollimation()).Returns(new Result());
            Mock<M.MeasureOfAngles> copyMock = this;
            this.Setup(m => m.Clone()).Returns(copyMock.Object);
        }
    }

    public class FilledMockMeasureOfDistance : Mock<M.MeasureOfDistance>
    {
        public FilledMockMeasureOfDistance(int refencePointNumber)
        {
            this.Setup(m => m._Date).Returns(DateTime.Now);
            this.Setup(m => m._InstrumentSN).Returns("123");
            this.Setup(m => m._Point).Returns((Point)new FilledMockPoint().Object);
            this.Setup(m => m._Status).Returns(new M.States.Good());

            //double d1 = 2.102602;
            //double d2 = 3.276595;
            //double d3 = 1.618850;

            switch (refencePointNumber)
            {
                case 1:
                    this.Setup(m => m.Raw).Returns(new FilledMockDoubleValue(3.089418, 0.0005).Object);
                    break;
                case 2:
                    this.Setup(m => m.Raw).Returns(new FilledMockDoubleValue(2.493948, 0.0005).Object);
                    break;
                case 3:
                    this.Setup(m => m.Raw).Returns(new FilledMockDoubleValue(9.580441, 0.0005).Object);
                    break;
                default:
                    break;
            }
            this.Setup(m => m.Corrected).Returns(this.Object.Raw);

            this.Setup(m => m.Reflector).Returns(new FilledMockReflector().Object);
            this.Setup(m => m.WeatherConditions).Returns((M.WeatherConditions)new FilledMockWeatherConditions().Object);
            this.Setup(m => m.MeasurementMode).Returns(DistanceMeasurementMode.WithReflector);
            this.Setup(m => m.IsCorrectedForEtalonnage).Returns(true);
            this.Setup(m => m.IsCorrectedForPrismConstanteValue).Returns(true);
            this.Setup(m => m.IsCorrectedForWeatherConditions).Returns(true);
            Mock<M.MeasureOfDistance> copyMock = this;
            this.Setup(m => m.Clone()).Returns(copyMock.Object);
        }
    }

    public class FilledMockMeasureTheodoliteFace : Mock<TSU.Polar.Measure>
    {
        public FilledMockMeasureTheodoliteFace(int refencePointNumber, I.FaceType face = I.FaceType.Face1)
        {
            //this.Setup(m => m._Comment).Returns("Blabla");
            this.Setup(m => m._Date).Returns(DateTime.Now);
            //this.Setup(m => m._Instrument).Returns((IInstrument)new FilledMockInstrument().Object);
            //this.Setup(m => m._Point).Returns((IElement)new FilledMockPoint().Object);
            //this.Setup(m => m._Station).Returns((IStation)new FilledMockStation().Object);
            //this.Setup(m => m._Status).Returns(new MeasureStateGood());
            this.Setup(m => m.Face).Returns(face);
            //this.Setup(m => m.Angles).Returns(new FilledMockMeasureOfAngles(refencePointNumber, face).Object);
            //this.Setup(m => m.Distance).Returns(new FilledMockMeasureOfDistance(refencePointNumber, face).Object);
            this.Setup(m => m.Extension.Value).Returns(0.07);
            this.Setup(m => m.IsMeasured).Returns(true);
            Mock<TSU.Polar.Measure> copyMock = this;
            this.Setup(m => m.Clone()).Returns(copyMock.Object);
        }
    }

    public class FilledMockPoint : Mock<Point>
    {
        public FilledMockPoint()
        {
            Random rnd = new Random(Guid.NewGuid().GetHashCode());

            this.CallBase = true;
            this.Setup(m => m._Name).Returns("LHC.GGPSO.3R8.E");
            this.Setup(m => m._Accelerator).Returns("LHC");
            this.Setup(m => m._Class).Returns("GGPSO");
            this.Setup(m => m.fileElementType).Returns(ElementType.Point);
            this.Setup(m => m._Numero).Returns("3R8");
            this.Setup(m => m._Point).Returns("Point" + rnd.Next(0, 100));
            this.Setup(m => m._Zone).Returns("LHC");
            this.Setup(m => m._Coordinates.Local).Returns(new FilledMockMlaCoordinates(0).Object);
            this.Setup(m => m._Coordinates.UserDefined).Returns(new FilledMockMlaCoordinates(0).Object);
            this.Setup(m => m._Coordinates.Ccs).Returns(new FilledMockMlaCoordinates(0).Object);
            this.Setup(m => m._Zone).Returns("LHC");
        }
        public FilledMockPoint(int refpointnumber)
        {
            Random rnd = new Random(Guid.NewGuid().GetHashCode());
            this.CallBase = true;
            this.Setup(m => m._Name).Returns("LHC.GGPSO.3R8.E");
            this.Setup(m => m._Accelerator).Returns("LHC");
            this.Setup(m => m._Class).Returns("GGPSO");
            this.Setup(m => m.fileElementType).Returns(ElementType.Point);
            this.Setup(m => m._Numero).Returns("3R8");
            this.Setup(m => m._Zone).Returns("LHC");
            this.Setup(m => m._Coordinates.Local).Returns(new FilledMockMlaCoordinates(refpointnumber).Object);
            this.Setup(m => m._Coordinates.Ccs).Returns(new FilledMockCcsCoordinates(refpointnumber).Object);
            this.Setup(m => m._Coordinates.UserDefined).Returns(new FilledMockMlaCoordinates(refpointnumber).Object);
            this.Setup(m => m._Coordinates.Su).Returns(new FilledMockMlaCoordinates(refpointnumber).Object);
            this.Setup(m => m._Coordinates.Physicist).Returns(new FilledMockMlaCoordinates(refpointnumber).Object);
            this.Setup(m => m._Zone).Returns("LHC");
            switch (refpointnumber)
            {
                case 1:
                    this.Setup(m => m._Name).Returns("PS03");
                    break;
                case 2:
                    this.Setup(m => m._Name).Returns("PS11");
                    break;
                case 3:
                    this.Setup(m => m._Name).Returns("PS30");
                    break;
                case 4:
                    this.Setup(m => m._Name).Returns("ST04");
                    break;
                default:
                    break;
            }
        }
    }
    public class FilledMockPointWithoutCCS : Mock<Point>
    {
        public FilledMockPointWithoutCCS()
        {
            Random rnd = new Random(Guid.NewGuid().GetHashCode());

            this.CallBase = true;
            this.Setup(m => m._Name).Returns("LHC.GGPSO.3R8.E");
            this.Setup(m => m._Accelerator).Returns("LHC");
            this.Setup(m => m._Class).Returns("GGPSO");
            this.Setup(m => m.fileElementType).Returns(ElementType.Point);
            this.Setup(m => m._Numero).Returns("3R8");
            this.Setup(m => m._Point).Returns("Point" + rnd.Next(0, 100));
            this.Setup(m => m._Zone).Returns("LHC");
            this.Setup(m => m._Coordinates.Local).Returns(new FilledMockMlaCoordinates(0).Object);
            this.Setup(m => m._Coordinates.UserDefined).Returns(new FilledMockMlaCoordinates(0).Object);
            this.Setup(m => m._Zone).Returns("LHC");
        }
        public FilledMockPointWithoutCCS(int refpointnumber)
        {
            Random rnd = new Random(Guid.NewGuid().GetHashCode());
            this.CallBase = true;
            this.Setup(m => m._Name).Returns("LHC.GGPSO.3R8.E");
            this.Setup(m => m._Accelerator).Returns("LHC");
            this.Setup(m => m._Class).Returns("GGPSO");
            this.Setup(m => m.fileElementType).Returns(ElementType.Point);
            this.Setup(m => m._Numero).Returns("3R8");
            this.Setup(m => m._Zone).Returns("LHC");
            this.Setup(m => m._Coordinates.Local).Returns(new FilledMockMlaCoordinates(refpointnumber).Object);
            this.Setup(m => m._Coordinates.UserDefined).Returns(new FilledMockMlaCoordinates(refpointnumber).Object);
            this.Setup(m => m._Coordinates.Ccs).Returns(new Coordinates());
            this.Setup(m => m._Zone).Returns("LHC");
            switch (refpointnumber)
            {
                case 1:
                    this.Setup(m => m._Name).Returns("PS03");
                    break;
                case 2:
                    this.Setup(m => m._Name).Returns("PS11");
                    break;
                case 3:
                    this.Setup(m => m._Name).Returns("PS30");
                    break;
                case 4:
                    this.Setup(m => m._Name).Returns("ST04");
                    break;
                default:
                    break;
            }
        }
    }
}
