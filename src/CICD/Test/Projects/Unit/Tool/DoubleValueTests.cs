﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU;
using TSU.ENUM;
using TSU.Common.Elements;

using TSU.Common.Instruments;
using TSU.Common.Measures;
using TSU.Tools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Compute;
using System.Data;

namespace TSU.Unit_Tests
{
    [TestClass()]
    public class DoubleValueTests
    {
        [TestMethod()]
        public void DoubleValue_SerializationOf()
        {
            DoubleValue obj = new DoubleValue() { Value = 10.01, Sigma = 0.02 };
            object obj2 = new DoubleValue();
            TSU.Unit_Tests.Serial.TryToSerialize(obj, true);
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);
            DoubleValue obj3 = obj2 as DoubleValue;
            Assert.AreEqual(obj.Value, obj3.Value);
        }

        [TestMethod()]
        public void ReadXmlTest()
        {
            DoubleValue obj = new DoubleValue(1, 2);

            object obj2 = new DoubleValue();

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true);
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj.Value, ((DoubleValue)obj2).Value);
        }

        [TestMethod()]
        public void EqualsTest()
        {
            DoubleValue d1 = new DoubleValue(1, 2);
            object d2 = new DoubleValue(1, 2);
            DoubleValue d3 = new DoubleValue(2, 3);
            object d4 = new object();
            Assert.IsTrue(d1 == d2);
            Assert.IsFalse(d1 == d3);
            Assert.IsTrue(d1 != d3);
            Assert.IsFalse(d1 != d2);
            Assert.IsFalse(d1 == d4);
            Assert.IsFalse(d4 == d1);
        }

        [TestMethod()]
        [DataRow(10, 0.01, "10.00(1)")]
        [DataRow(10, 0.055, "10.00(6)")]
        [DataRow(10, 0.051, "10.00(5)")]
        [DataRow(10.428, 0.051, "10.43(5)")]
        [DataRow(123.4567, 0.00123, "123.457(1)")]
        [DataRow(123.4567, 0.00456, "123.457(5)")]
        [DataRow(2, 1, "2(1)")]
        [DataRow(3, 1, "3(1)")]
        public void getValueWithSigmaInParanthesisTest(double v, double s, string expected)
        {
            return; // todo
            DoubleValue dv = new DoubleValue(v, s);
            string dvs = DoubleValue.GetValueWithSigmaInParanthesis(dv.Value, dv.Sigma);
            Assert.AreEqual(expected, dvs);
        }
    }
}


namespace TSU.Unit_Tests
{
    [TestClass()]
    public class DoubleValueTests2
    {
        [TestMethod()]
        public void DoubleValueDoAverageTest()
        {
            //TODO: ASSERT_FAIL
            // test.sigma failed

            // arrange

            DoubleValue test = new DoubleValue();
            DoubleValue input1 = new DoubleValue();
            DoubleValue input2 = new DoubleValue();

            input1.Value = 10.5;
            input2.Value = 10.6;

            input1.Sigma = 10;
            input2.Sigma = 10;

            // act
            test = Survey.DoAverage(new List<IDoubleValue> { input1, input2 });

            //assert
            Assert.AreEqual(10.55, test.Value, "Test average of two values failed");
            Assert.AreEqual(7.07, test.Sigma, 0.01, "Test average of two values failed");
            
        }

        [TestMethod()]
        public void DoubleValueCloneTest()
        {
            DoubleValue original = new DoubleValue();
            original.Value = 20;
            original.Sigma = 10;

            DoubleValue copy;

            copy = (DoubleValue)original.Clone();
            copy.Value = 1000;
            copy.Sigma = 2000;
            Assert.AreEqual(20, original.Value, "Failed");
            Assert.AreEqual(10, original.Sigma, "Failed");
            Assert.AreEqual(1000, copy.Value, "Failed");
        }
    }
}
