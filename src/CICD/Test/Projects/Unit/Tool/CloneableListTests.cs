﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using TSU.Common.Elements;
using TSU.Common.Measures;
using TSU.Common.Instruments.Device;

using TSU.Unit_Tests;

namespace TSU.Unit_Tests
{
    [TestClass()]
    public class CloneableListTests
    {
        [TestMethod()]
        public void CloneTestCloneableListTests()
        {
            CloneableList<string> mom = new CloneableList<string>() { "mm", "oo", "mm" };
            CloneableList<string> dad = mom.Clone();
            CloneableList<CloneableList<string>> granny = new CloneableList<CloneableList<string>>() { mom, dad };
            CloneableList<CloneableList<string>> grapa = granny.Clone();
            mom[1] = "mm";
            granny[1][2]="aa";
            Assert.AreNotEqual(mom[1], dad[1]);
            Assert.AreNotEqual(granny[1][2], grapa[1][2]);
        }

        [TestMethod()]
        public void CloneableList_serialization()
        {
            CloneableList<string> l = new CloneableList<string>();
            l.Add("1");
            l.Add("2");
            l.Add("3");
            object l2 = new CloneableList<string>();

            TSU.Unit_Tests.Serial.TryToSerialize(l, true);
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref l2);
            Assert.AreEqual(l[1],(l2 as CloneableList<string>)[1]);
            
        }

        [TestMethod()]
        public void CloneableList_serialization_Of_measure()
        {
            CloneableList<Measure> l = new CloneableList<Measure>();
            Measure m = new Measure();
            m._Name = "test";
            l.Add(m);
            object l2 = new CloneableList<Measure>();

            TSU.Unit_Tests.Serial.TryToSerialize(l, true);
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref l2);
            Assert.AreEqual(l[0]._Name, (l2 as CloneableList<Measure>)[0]._Name);
        
        }

        [TestMethod()]
        public void CloneableList_serialization_Of_Point()
        {
            CloneableList<Point> l = new CloneableList<Point>();
            Point m = new Point("test");
            l.Add(m);
            object l2 = new CloneableList<Point>();

            TSU.Unit_Tests.Serial.TryToSerialize(l, true);
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref l2);
            Assert.AreEqual(l[0]._Name, (l2 as CloneableList<Point>)[0]._Name);
        }

        [TestMethod()]
        public void CloneableList_serialization_Of_measureTheo()
        {
            CloneableList<Measure> l = new CloneableList<Measure>();
            Polar.Measure m = new Polar.Measure();
            m._Name = "test";
            m.Angles.Compensator.inclinationAccuracy = 0;
            l.Add(m as  Measure);
            object l2 = new CloneableList<Measure>();

            TSU.Unit_Tests.Serial.TryToSerialize(l, true);
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref l2);
            Assert.AreEqual(l[0]._Name, (l2 as CloneableList<Measure>)[0]._Name);
            Assert.AreEqual((l[0] as Polar.Measure).Angles.Compensator.inclinationAccuracy,
                ((l2 as CloneableList<Measure>)[0] as Polar.Measure).Angles.Compensator.inclinationAccuracy);
        }
    }
}
