

                          ****               ******              *****           ***********                                     
                          ****            ************        ***********        ***********                                     
                          ****           *****     ****      *****    ****              ****                                     
                          ****          ****        ****    ****       ****             ****                                     
                          ****          ****                ****                        ****                                     
                          ****          ****                ****                 ***********                                     
 ***********************  ****          ****      *******   ****                 ***********    ***********************             
 ***********************  ****          ****      *******   ****                 ****            ***********************             
                          ****          ****       ******   ****       ****      ****                                            
                          ***********    ****     **** **    ****     ****       ****                                            
                          ***********     ***********         ***********        ***********                                     
                          ***********        *****               *****           ***********                                     



*********************************************************************************************************************************** 

LGC2 v2.6.beta_4-OBSXYZ_Upgrade, compiled on Apr 17 2023
Copyright 2022, CERN SU. All rights reserved.
*********************************************************************************************************************************** 
Input by Tsunami

CALCUL DU 17 May 2023 16:17:36
*********************************************************************************************************************************** 

DATA SET -  INFO GENERAL:

	FRAMES :1

	POINTS : 9
	 INCONNUES INTRODUITES POINTS: 8

	PLANS : 1
	 INCONNUES INTRODUITES PLANS: 2

	DISTANCES : 1
	 INCONNUES INTRODUITES DISTANCES: 0

POINTS : 

	LECTURE DES POINTS DE CALAGE : 4
	LECTURE DES POINTS INVARIABLES EN Z : 4

MESURES :

	LECTURE DES DISTANCES SPATIALES (DSPT) : 7
	LECTURE DES ECARTS DANS LE PLAN HORIZONTAL (ECHO) : 8

*** STATISTIQUES ***

	NOMBRE D'OBSERVATIONS =  15
	NOMBRE D'INCONNUES =     10
	NOMBRE D'ITERATIONS =    3

SIGMA ZERO A POSTERIORI =0.5392593, VALEUR CRITIQUE = ( 0.40773,  1.60203)
LES ECARTS-TYPES SONT CALCULES PAR RAPPORT AU SIGMA ZERO A PRIORI (EGAL A 1)



FRAME	ROOT  ID(1)   

POINTS DE CALAGE   (NB. = 5,  REFERENTIEL = OLOC )
SFP               NOM               X                Y                Z         SX         SY         SZ         DX         DY         DZ    
                                   (M)              (M)              (M)       (MM)       (MM)       (MM)       (MM)       (MM)       (MM)   

       LHC.MB.A12R1.E        0.0000000        0.0000000        0.0000000                                                                     
       LHC.MB.A12R1.S       -0.0648510       10.7992530        0.1398530                                                                     
       LHC.MB.C12R1.E       -0.0850230       31.3140240        0.3985720                                                                     
       LHC.MB.C12R1.S       -0.0406190       42.1144990        0.5363130                                                                     
          ECHO_line25       -0.1348196       21.7316721        0.2769254                                                                     

POINTS INVARIABLES EN Z   (NB. = 4,  REFERENTIEL = OLOC )
SFP               NOM               X                Y                Z         SX         SY         SZ         DX         DY         DZ    
                                   (M)              (M)              (M)       (MM)       (MM)       (MM)       (MM)       (MM)       (MM)   

       LHC.MB.B12R1.E       -0.0829698       15.6562575        0.2000200     0.1130     1.0001               -0.0728    -0.0005              
       LHC.MB.B12R1.M       -0.0000208       21.0557749        0.2665510     0.1118     1.0001               88.1432     0.1849              
       LHC.MB.B12R1.T       -0.0000116       26.4638504        0.3356570     0.1130     1.0001              623.7554     7.3524              
       LHC.MB.B12R1.S       -0.0933317       26.4572539        0.3384370     0.1130     1.0001               -0.0957    -0.0011              

SFP = Sub-Frame Point; * = TRUE


*** RESUME DES MESURES ***

ECHO              
	LIGNE              RES_MAX    RES_MIN    RES_MOY   ECART_TYPE   
	                      (MM)       (MM)       (MM)       (MM)   
	ECHO_line25         0.0930    -0.0562     0.0000     0.0456   
RESIDU MOYEN =    0.0000    MM :  LIMITES DE CONFIANCE A 95.0 = (-0.0381, 0.0381)
ECART-TYPE   =    0.0456    MM :  LIMITES DE CONFIANCE A 95.0 = (0.0301, 0.0928)   

DSPT              
	TSTN_POS           RES_MAX    RES_MIN    RES_MOY   ECART_TYPE   
	                      (MM)       (MM)       (MM)       (MM)   
	LHC.MB.A12R1.E      0.0003    -0.0008    -0.0001     0.0003   
RESIDU MOYEN =   -0.0001    MM :  LIMITES DE CONFIANCE A 95.0 = (-0.0003, 0.0003)
ECART-TYPE   =    0.0003    MM :  LIMITES DE CONFIANCE A 95.0 = (0.0002, 0.0007)   


REPARTITION DES ECHO   (1/100 MM)

-7.0000
 +    
-6.0000
 +    **
-5.0000
 +    
-4.0000
 +    
-3.0000
 +    
-2.0000
 +    
-1.0000
 +    ***
0.0000
 +    *
1.0000
 +    *
2.0000
 +    
3.0000
 +    
4.0000
 +    
5.0000
 +    
6.0000
 +    
7.0000
 +    
8.0000
 +    
9.0000
 +    *
10.0000
 +    
11.0000

NOMBRE DE POINTS HORS-HISTOGRAMME   0



REPARTITION DES DSPT   (1/100 MM)

-2.0000
 +    
-1.0000
 +    ******
0.0000
 +    *
1.0000
 +    
2.0000

NOMBRE DE POINTS HORS-HISTOGRAMME   0




*** MESURES ***


	ECHO
	REF POINT         ECHO_line25       X (M)                -0.1348196   Y (M)                21.7316721   Z (M)                 0.2769254   
	PARAMETRE DU FIL                     X (M)                -0.0000196   Y (M)                21.7316719   
	                                    ORIENTATION (GON)       0.0001072   SORIENTATION (CC)          1.9219   SNORMALE (MM)            0.0500   
		ECARTS DANS LE PLAN HORIZONTAL (ECHO)   (NB. = 8 )   
		POSITION                OBSERVE      SIGMA         CALCULE     RESIDU    RES/SIG          SCALE ID       OBSE        PPM       ICSE   
		                            (M)       (MM)             (M)       (MM)                                    (MM)    (MM/KM)       (MM)   
		LHC.MB.A12R1.E        0.0000000     0.1000      -0.0000562    -0.0562    -0.5619        RS_1000_16     0.1000     0.0000     0.0000      
		LHC.MB.A12R1.S        0.0647200     0.1000       0.0648130     0.0930     0.9298        RS_1000_16     0.1000     0.0000     0.0000      
		LHC.MB.B12R1.E        0.0829400     0.1000       0.0829400     0.0000     0.0000        RS_1000_16     0.1000     0.0000     0.0000      
		LHC.MB.B12R1.M        0.0000000     0.1000       0.0000000     0.0000     0.0000        RS_1000_16     0.1000     0.0000     0.0000      
		LHC.MB.B12R1.T        0.0000000     0.1000       0.0000000     0.0000     0.0000        RS_1000_16     0.1000     0.0000     0.0000      
		LHC.MB.B12R1.S        0.0933200     0.1000       0.0933200     0.0000     0.0000        RS_1000_16     0.1000     0.0000     0.0000      
		LHC.MB.C12R1.E        0.0850700     0.1000       0.0850195    -0.0505    -0.5049        RS_1000_16     0.1000     0.0000     0.0000      
		LHC.MB.C12R1.S        0.0406200     0.1000       0.0406337     0.0137     0.1369        RS_1000_16     0.1000     0.0000     0.0000      

	INSTRUMENT EDM: DISTTHEO   
	INST POS          LHC.MB.A12R1.E       HINSTR (M)       0.0000000   

		DISTANCES SPATIALES (DSPT)   (NB. = 7 )   
		POINT                   OBSERVE      SIGMA         CALCULE     RESIDU      SENSI    RES/SIG           CONST     SCONST              TRGT          H_TRGT       OBSE        PPM       TCSE       THSE   
		                            (M)       (MM)             (M)       (MM)    (MM/CM)                        (M)       (MM)                               (M)       (MM)    (MM/KM)       (MM)       (MM)   
		LHC.MB.A12R1.S       10.8003540     1.0000      10.8003532    -0.0008     0.1295    -0.0008       0.0000000      FIXED        TARGETTHEO       0.0000000     1.0000     0.0000     0.0000     0.0000              
		LHC.MB.B12R1.E       15.6577550     1.0000      15.6577550     0.0000     0.1277     0.0000       0.0000000      FIXED        TARGETTHEO       0.0000000     1.0000     0.0000     0.0000     0.0000              
		LHC.MB.B12R1.M       21.0574620     1.0000      21.0574620    -0.0000     0.1266    -0.0000       0.0000000      FIXED        TARGETTHEO       0.0000000     1.0000     0.0000     0.0000     0.0000              
		LHC.MB.B12R1.T       26.4659790     1.0000      26.4659790    -0.0000     0.1268    -0.0000       0.0000000      FIXED        TARGETTHEO       0.0000000     1.0000     0.0000     0.0000     0.0000              
		LHC.MB.B12R1.S       26.4595830     1.0000      26.4595830    -0.0000     0.1279    -0.0000       0.0000000      FIXED        TARGETTHEO       0.0000000     1.0000     0.0000     0.0000     0.0000              
		LHC.MB.C12R1.E       31.3166760     1.0000      31.3166759    -0.0001     0.1273    -0.0001       0.0000000      FIXED        TARGETTHEO       0.0000000     1.0000     0.0000     0.0000     0.0000              
		LHC.MB.C12R1.S       42.1179330     1.0000      42.1179333     0.0003     0.1273     0.0003       0.0000000      FIXED        TARGETTHEO       0.0000000     1.0000     0.0000     0.0000     0.0000              

*** FIN DE FICHIER ***
