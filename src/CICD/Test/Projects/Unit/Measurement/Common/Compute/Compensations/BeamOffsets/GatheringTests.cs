﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TSU.Common.Measures;
using TSU.Common.Measures.States;

namespace TSU.Common.Compute.Compensations.BeamOffsets
{
    [TestClass()]
    public class GatheringTests
    {
        [TestMethod()]
        public void GetDateOfLastStationOfEachTypeTest()
        {
            DateTime testDate = DateTime.Now;
            DateTime d1p = DateTime.Now.AddMinutes(-1);
            DateTime d2t = DateTime.Now.AddMinutes(-2); // good one
            DateTime d3t = DateTime.Now.AddMinutes(-3);
            DateTime d4t = DateTime.Now.AddMinutes(-4);
            DateTime d5p = DateTime.Now.AddMinutes(-5);
            DateTime d6t = DateTime.Now.AddMinutes(-6);
            DateTime d7p = DateTime.Now.AddMinutes(-7);
            DateTime d8p = DateTime.Now.AddMinutes(-8);
            var list = new List<FinalModule>()
                {
                    new Polar.Module()
                    {
                        StationModules = new List<Station.Module>()
                        {
                            new Polar.Station.Module()
                            {
                                StationTheodolite = new Polar.Station()
                                {
                                    ParametersBasic = new Polar.Station.Parameters() { _Date = d7p},
                                }
                            },
                            new Polar.Station.Module()
                            {
                                StationTheodolite = new Polar.Station()
                                {
                                    ParametersBasic = new Polar.Station.Parameters() { _Date = d8p},
                                }
                            }
                        }
                    },
                    new Tilt.Module()
                    {
                        StationModules = new List<Station.Module>()
                        {
                            new Tilt.Station.Module()
                            {
                                _Station = new Tilt.Station()
                                {
                                    ParametersBasic = new Tilt.Station.Parameters() { _Date = d3t, _State = new Station.State.Good()},
                                }
                            },
                            new Tilt.Station.Module()
                            {
                                _Station = new Tilt.Station()
                                {
                                    ParametersBasic = new Tilt.Station.Parameters() { _Date = d4t, _State = new Station.State.Good()},
                                }
                            }
                        }
                    },
                    new Guided.Group.Module()
                    {
                        SubGuidedModules = new List<Guided.Module>()
                        {
                            new Guided.Module()
                            {
                                StationModules = new List<Station.Module>()
                                {
                                    new Polar.Station.Module()
                                    {
                                        StationTheodolite = new Polar.Station()
                                        {
                                            ParametersBasic = new Polar.Station.Parameters() { _Date = d1p, _State = new Station.State.Good()},
                                        }
                                    },
                                    new Tilt.Station.Module()
                                    {
                                        _Station = new Tilt.Station()
                                        {
                                            ParametersBasic = new Tilt.Station.Parameters() { _Date = d6t, _State = new Station.State.Good()},
                                        }
                                    }
                                }
                            }
                        }
                    },
                    new Guided.Module()
                    {
                        StationModules = new List<Station.Module>()
                        {
                            new Polar.Station.Module()
                            {
                                StationTheodolite = new Polar.Station()
                                {
                                    ParametersBasic = new Polar.Station.Parameters() { _Date = d5p, _State = new Station.State.Good() }
                                }
                            },
                            new Tilt.Station.Module()
                            {
                                _Station = new Tilt.Station()
                                {
                                    ParametersBasic = new Tilt.Station.Parameters() { _Date = d2t, _State = new Station.State.Good() },
                                }
                            }
                        }
                    }
                };
             

            var date = Gathering.GetDateOfLastStationOfEachType(list);

            Assert.AreNotEqual(d1p, date);
            Assert.AreNotEqual(d3t, date);
            Assert.AreNotEqual(d4t, date);
            Assert.AreNotEqual(d5p, date);
            Assert.AreNotEqual(d6t, date);
            Assert.AreNotEqual(d7p, date);
            Assert.AreNotEqual(d8p, date);
            Assert.AreEqual(d2t, date);
         }
    }
}

namespace TSU.Common.Compute.Compensations.BeamOffsets.Tests
{
    [TestClass()]
    public class GatheringTests
    {
        [TestMethod()]
        public void GetDataFromOpenedModulesTest()
        {
            //TODO-toRestore: ASSERT_FAIL
            // Assertion failed - Expected:<2>. Actual:<1> in this Assert.AreEqual(2, polarStationsAndMeasures[0].Item2.Count); 
            // Rest of assert is ok!

            DateTime d1 = DateTime.Now.AddMinutes(-1);
            DateTime d2 = DateTime.Now.AddMinutes(-2);
            DateTime d3 = DateTime.Now.AddMinutes(-3);
            DateTime expiration = DateTime.Now.AddMinutes(-4);
            DateTime d4 = DateTime.Now.AddMinutes(-5);
            Gathering.GetDataFrom(
                new List<FinalModule>()
                {
                    new Polar.Module()
                    {
                        StationModules = new List<Station.Module>()
                        {
                            new Polar.Station.Module()
                            {
                                _Station = new Polar.Station()
                                {
                                    MeasuresTaken = new CloneableList<Measures.Measure>()
                                    {
                                        new Polar.Measure(){ _Point = new Elements.Point(){ _Name = "LHC.class.Num1.E" }, _Date = d1 },
                                        new Polar.Measure(){ _Point = new Elements.Point(){ _Name = "LHC.class.Num1.S" }, _Date = d3 },
                                        new Polar.Measure(){ _Point = new Elements.Point(){ _Name = "LHC.class.Num1.E" }, _Date = d2 },
                                        new Polar.Measure(){ _Point = new Elements.Point(){ _Name = "LHC.class.Num1.E" }, _Date = d3 },
                                        new Polar.Measure(){ _Point = new Elements.Point(){ _Name = "LHC.class.Num1.T" }, _Date = d4 },
                                    }
                                }, Guid = Guid.Empty
                            }
                        },idOfActiveStation = Guid.Empty
                    }
                },
                 "LHC.class.Num1" ,
                expiration,
                out GatheredMeasures gm);

            Assert.AreEqual(1, gm.Polar.Count);
            Assert.AreEqual(2, gm.Polar[0].Item2.Count);
            Assert.AreEqual(d1, gm.Polar[0].Item2[0].Measure._Date);
            Assert.AreEqual(d3, gm.Polar[0].Item2[1].Measure._Date);
        }
    }
}