﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Compute.Compensations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using System.IO;
using System.Text.RegularExpressions;

namespace TSU.Common.Compute.Compensations.Tests
{
    [TestClass()]
    public class Ellipse
    {
        private const string TestFilePath = "testFile.txt";

        [TestMethod()]
        public void ReplaceUnknownCoordinatesByLhcCenterTest()
        {
            string[] lines = { "some text", "point_1 -999.9   -999.9   -999.9", "more text" };
            System.IO.File.WriteAllLines(TestFilePath, lines);


            // Act
            Lgc2.ReplaceUnknownCoordinatesByLhcCenter(TestFilePath);

            string[] lines2 = System.IO.File.ReadAllLines(TestFilePath);

            string[] parts = lines2[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            // Assert
            double.TryParse(parts[1], out double x);
            Assert.AreEqual(666, x, 1);

            double.TryParse(parts[2], out double y);
            Assert.AreEqual(6666, y, 1);

            double.TryParse(parts[3], out double z);
            Assert.AreEqual(400, z, 1);

        }

        [TestMethod()]
        public void ReplaceUnknownCoordinatesByLhcCenter_WithOLOC_ReplacesWithFixedCoordinates()
        {
            // Arrange
            string[] lines = { "some text", "*OLOC", "Point2   -999.9   -999.9   -999.9", "more text" };
            File.WriteAllLines(TestFilePath, lines);

            var mockLgc2Input = new Mock<Lgc2.Input>();

            // Act
            Lgc2.ReplaceUnknownCoordinatesByLhcCenter(TestFilePath);

            // Assert
            string[] result = File.ReadAllLines(TestFilePath);
            string expectedReplacement = "6.66   6.66   6.66";
            Assert.AreEqual("some text", result[0]);
            Assert.AreEqual("*OLOC", result[1]);
            Assert.IsTrue(result[2].Contains(expectedReplacement));
            Assert.AreEqual("more text", result[3]);
        }
    }
}