﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Compute.Compensations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Security.Cryptography;
using TSU.Common.Compute.Transformation;
using static TSU.Common.Compute.Compensations.BeamOffsets.Parameters;
using TSU.Common.Elements;
using System.Threading;
using TSU.Tools;

namespace TSU.Common.Compute.Compensations.Tests
{
    [TestClass()]
    public class BeamOffsetsTests
    {
        [TestMethod()]
        public void ComputeTest()
        {
            string frame = Frame.ReadSuAssemblyBlockFromString("LHC.MQML.5L5", @"LHC.MQML.5L5.E                                          10.646851 TH  0.070000   $1504239 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                         16.716183 TH  0.000000   $1504240 - 13-FEB-2023 - TSU: 'Control 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm,Off.: dH=0.9cc, dV=0.8cc, dD=0mm' - Was Control according to Tsunami
*FRAME RSTI_103035_LHC.MQML.5L5  -1231.090090  10389.569100   2416.394520 0 0   74.733794 1
*FRAME RST_103035_LHC.MQML.5L5 0 0 0 .7911910531 -.41577637333 0 1
*FRAME RSTRI_103035_LHC.MQML.5L5     -0.001198      0.001174     -0.000592 .00074272462 0 -.00045093994 1 TX TY TZ RX RZ 
*FRAME RSTR_103035_LHC.MQML.5L5 0 0 0 0 0.001336902 0 1 RY
*CALA
BEAM_LHC.MQML.5L5.S                                             -0.000005      4.800002      0.000003   $13135.200 693826 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.BPMR.5L5.E                                             -0.000003     -0.745000      0.000001   $13129.655 683395 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.BPMR.5L5.S                                             -0.000003     -0.745000      0.000001   $13129.655 683396 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MQML.5L5.E                                              0.000000      0.000000      0.000000   $13130.400 693825 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCV.5L5.E                                            -0.000004      4.990007      0.000003   $13135.390 804768 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCV.5L5.S                                            -0.000005      5.894007      0.000003   $13136.294 804769 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCA.5L5.E                                            -0.000004      4.990007      0.000003   $13135.390 686523 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCA.5L5.S                                            -0.000005      5.894007      0.000003   $13136.294 686524 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
% LHC.MQML.5L5.T     -0.360470      4.306890      0.420950 0.1 0.1 0.1   $13134.707 516196 paramètres RST 23-FEB-2023 00:00:00
*OBSXYZ
LHC.MQML.5L5.E      0.168800      0.601580      0.421630 0.1 0.1 0.1   $13131.001 516195 paramètres RST 23-FEB-2023 00:00:00
LHC.MQML.5L5.S      0.167070      4.306810      0.420700 0.1 0.1 0.1   $13134.706 516197 paramètres RST 23-FEB-2023 00:00:00
*INCLY W1015
LHC.MQML.5L5.E  -0.533487369 RF -0.030169411
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
% *FRAME RSTI_103036_LHC.B2.TCL.5L5  -1218.223830  10395.171970   2416.221390 0 0   74.733794 1
% *FRAME RST_103036_LHC.B2.TCL.5L5 0 0 0 .7911910531 -.4197870779 0 1
");

            string[] lines = frame.Split('\n');

            // Count the number of lines
            int lineCount = lines.Length;

            Assert.AreEqual(24, lineCount);
        }

        [TestMethod()]
        public void ComputeTest_move_ONSXYZ()
        {

            string frameContent = Frame.ReadSuAssemblyBlockFromString("LHC.MQML.5L5", @"
*FRAME RSTR_103035_LHC.MQML.5L5 0 0 0 0 0.001336902 0 1 RY
*CALA
BEAM_LHC.MQML.5L5.S                                             -0.000005      4.800002      0.000003   $13135.200 693826 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.BPMR.5L5.E                                             -0.000003     -0.745000      0.000001   $13129.655 683395 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.BPMR.5L5.S                                             -0.000003     -0.745000      0.000001   $13129.655 683396 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MQML.5L5.E                                              0.000000      0.000000      0.000000   $13130.400 693825 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCV.5L5.E                                            -0.000004      4.990007      0.000003   $13135.390 804768 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCV.5L5.S                                            -0.000005      5.894007      0.000003   $13136.294 804769 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCA.5L5.E                                            -0.000004      4.990007      0.000003   $13135.390 686523 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
% LHC.MQML.5L5.T     -0.360470      4.306890      0.420950 0.1 0.1 0.1   $13134.707 516196 paramètres RST 23-FEB-2023 00:00:00
*OBSXYZ
LHC.MQML.5L5.E      0.168800      0.601580      0.421630 0.1 0.1 0.1   $13131.001 516195 paramètres RST 23-FEB-2023 00:00:00
LHC.MQML.5L5.S      0.167070      4.306810      0.420700 0.1 0.1 0.1   $13134.706 516197 paramètres RST 23-FEB-2023 00:00:00
*INCLY W1015
LHC.MQML.5L5.E  -0.533487369 RF -0.030169411
*ENDFRAME
");
            List<Frame.FrameMeasurement.ObsXYZ> obs = new List<Frame.FrameMeasurement.ObsXYZ>();
            obs.Add(new Frame.FrameMeasurement.ObsXYZ() {
                Point =  "LHC.MQML.5L5.E", 
                X = new Frame.FrameMeasurement.Observation() { Sigma = 0.0001 }, 
                Y = new Frame.FrameMeasurement.Observation() { Sigma = 0.01 }, 
                Z = new Frame.FrameMeasurement.Observation() { Sigma = 0.0001 }
            });
            obs.Add(new Frame.FrameMeasurement.ObsXYZ()
            {
                Point = "LHC.MQML.5L5.S",
                X = new Frame.FrameMeasurement.Observation() { Sigma = 0.0001 },
                Y = new Frame.FrameMeasurement.Observation() { Sigma = 0.01 },
                Z = new Frame.FrameMeasurement.Observation() { Sigma = 0.0001 }
            });
            Frame root = Frame.FromLGCInputFormat_GetRootFrameInputFormatString(frameContent);
            Frame frame = root.DeeperFrame;
            Frame.UpdateObsXyz(frame, obs);

            Assert.AreEqual(0.01, frame.Measurements.GetObsXyzByName("LHC.MQML.5L5.E").Y.Sigma);
            Assert.AreEqual(0.0001, frame.Measurements.GetObsXyzByName("LHC.MQML.5L5.S").X.Sigma);
        }

        private string getLgcInputForAStationMeasuringARefenceAndMagnet()
        {
            return @" * TITR
Fichier d'input créé le 21-MAR-2023
Opération n 28159, 3D
*RS2K
*PUNC   OUT1
*FAUT     .01     .10
*PREC 6
*INSTR
*POLAR AT403.395161 RRR1.5.1 0 0 0 0
RRR1.5.1 3 3 .02 6 0 0 0 0 0 0
*INCL W1015 30 0 0 0 0 
*CALA
LHC.NID.6L5-02.                                           -1236.035780    10387.263670    421.714840   $13124.946    816819   coordonnées au 23-FEB-2023 00:00:00
LHC.NID.6L5-01.                                           -1232.143620    10386.806220    422.282030   $13128.359    816820   coordonnées au 23-FEB-2023 00:00:00
LHC.NID.5L5-05.                                           -1228.076280    10388.522930    423.143080   $13132.774    816821   coordonnées au 23-FEB-2023 00:00:00
LHC.NID.5L5-03.                                           -1219.457340    10395.059090    422.911140   $13143.250    816822   coordonnées au 23-FEB-2023 00:00:00
LHC.NID.5L5-02.                                           -1211.533740    10397.629260    421.385950   $13151.552    816812   coordonnées au 23-FEB-2023 00:00:00
LHC.NID.5L5-01.                                           -1209.602220    10396.516630    423.395550   $13152.904    816813   coordonnées au 23-FEB-2023 00:00:00
LHC.NID.4L5-30.                                           -1203.185430    10398.887610    422.179090   $13159.739    816814   coordonnées au 23-FEB-2023 00:00:00
*POIN
LHC.STL.20230213BDKW01.                                   -1220.196700    10392.463530    422.945020   $-999.900    826501   coordonnées au 23-FEB-2023 00:00:00
LHC.MQML.5L5.E                                            -1230.463560    10389.646990    422.994024   $13131.001    516195   coordonnées réelles au 23-FEB-2023 00:00:00
LHC.MQML.5L5.S                                            -1227.047300    10391.080730    422.947241   $13134.706    516197   coordonnées réelles au 23-FEB-2023 00:00:00
LHC.B2.TCL.5L5.E                                          -1218.129640    10395.427050    422.778475   $13144.917    640301   coordonnées réelles au 23-FEB-2023 00:00:00
LHC.B2.TCL.5L5.S                                          -1217.539460    10395.674480    422.770584   $13145.557    640302   coordonnées réelles au 23-FEB-2023 00:00:00
% LHC.MQML.5L5.T                                            -1227.251700    10391.565710    422.952300   $13134.707    516196   coordonnées théoriques au 23-FEB-2023 00:00:00
*TSTN   LHC.STL.20230213BDKW01.   AT403.395161    IHFIX   IH  0.000000       %station n329490
*V0   
*ANGL   
LHC.NID.6L5-02.                                         76.094579   $1504199 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.6L5-01.                                         68.132229   $1504200 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.5L5-05.                                         66.758588   $1504201 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.5L5-03.                                        213.948228   $1504202 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.5L5-02.                                        262.038593   $1504203 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.5L5-01.                                        273.024865   $1504204 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.4L5-30.                                        273.291549   $1504205 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                         76.094577   $1504206 - 13-FEB-2023 - TSU: 'Control 2F / 2 Standard: => sH=2 cc, sV=2 cc, sD=0.00 mm,Off.: dH=0cc, dV=5.2cc, dD=-0.01mm' - Was Control according to Tsunami
LHC.B2.TCL.5L5.S                                       240.289988   $1504207 - 13-FEB-2023 - TSU: 'Quick measure!, Good F1 / 1 Standard: => sH=0 cc, sV=0 cc, sD=0.00 mm'
LHC.B2.TCL.5L5.E                                       235.052329   $1504208 - 13-FEB-2023 - TSU: 'Quick measure!, Good F1 / 1 Standard: => sH=1 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                         76.094776   $1504209 - 13-FEB-2023 - TSU: 'Quick measure!, Control F1 / 1 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm, - Off.: dH=2cc, dV=6.8cc, dD=0mm' - Was Control according to Tsunami
LHC.MQML.5L5.S                                          83.602564   $1504210 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=1 cc, sD=0.00 mm'
LHC.MQML.5L5.E                                          79.236822   $1504211 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                         76.094672   $1504212 - 13-FEB-2023 - TSU: 'Control 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm,Off.: dH=0.9cc, dV=0.8cc, dD=0mm' - Was Control according to Tsunami
*ZEND   
LHC.NID.6L5-02.                                        104.689045 TH  0.000000   $1504213 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.6L5-01.                                        103.190317 TH  0.000000   $1504214 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.5L5-05.                                         98.568672 TH  0.000000   $1504215 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.5L5-03.                                        100.799551 TH  0.000000   $1504216 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.5L5-02.                                        109.762797 TH  0.000000   $1504217 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.5L5-01.                                         97.472580 TH  0.000000   $1504218 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.4L5-30.                                        102.679813 TH  0.000000   $1504219 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                        104.689560 TH  0.000000   $1504220 - 13-FEB-2023 - TSU: 'Control 2F / 2 Standard: => sH=2 cc, sV=2 cc, sD=0.00 mm,Off.: dH=0cc, dV=5.2cc, dD=-0.01mm' - Was Control according to Tsunami
LHC.B2.TCL.5L5.S                                       101.594899 TH  0.070000   $1504221 - 13-FEB-2023 - TSU: 'Quick measure!, Good F1 / 1 Standard: => sH=0 cc, sV=0 cc, sD=0.00 mm'
LHC.B2.TCL.5L5.E                                       101.700755 TH  0.070000   $1504222 - 13-FEB-2023 - TSU: 'Quick measure!, Good F1 / 1 Standard: => sH=1 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                        104.689727 TH  0.000000   $1504223 - 13-FEB-2023 - TSU: 'Quick measure!, Control F1 / 1 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm, - Off.: dH=2cc, dV=6.8cc, dD=0mm' - Was Control according to Tsunami
LHC.MQML.5L5.S                                          99.342179 TH  0.070000   $1504224 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=1 cc, sD=0.00 mm'
LHC.MQML.5L5.E                                          99.288499 TH  0.070000   $1504225 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                        104.689129 TH  0.000000   $1504226 - 13-FEB-2023 - TSU: 'Control 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm,Off.: dH=0.9cc, dV=0.8cc, dD=0mm' - Was Control according to Tsunami
*DIST   
LHC.NID.6L5-02.                                         16.716185 TH  0.000000   $1504227 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.6L5-01.                                         13.235201 TH  0.000000   $1504228 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.5L5-05.                                          8.812266 TH  0.000000   $1504229 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.5L5-03.                                          2.699071 TH  0.000000   $1504230 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.5L5-02.                                         10.206392 TH  0.000000   $1504231 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.5L5-01.                                         11.352288 TH  0.000000   $1504232 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.4L5-30.                                         18.199988 TH  0.000000   $1504233 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                         16.716176 TH  0.000000   $1504234 - 13-FEB-2023 - TSU: 'Control 2F / 2 Standard: => sH=2 cc, sV=2 cc, sD=0.00 mm,Off.: dH=0cc, dV=5.2cc, dD=-0.01mm' - Was Control according to Tsunami
LHC.B2.TCL.5L5.S                                         4.169290 TH  0.070000   $1504235 - 13-FEB-2023 - TSU: 'Quick measure!, Good F1 / 1 Standard: => sH=0 cc, sV=0 cc, sD=0.00 mm'
LHC.B2.TCL.5L5.E                                         3.614616 TH  0.070000   $1504236 - 13-FEB-2023 - TSU: 'Quick measure!, Good F1 / 1 Standard: => sH=1 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                         16.716186 TH  0.000000   $1504237 - 13-FEB-2023 - TSU: 'Quick measure!, Control F1 / 1 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm, - Off.: dH=2cc, dV=6.8cc, dD=0mm' - Was Control according to Tsunami
LHC.MQML.5L5.S                                           6.989141 TH  0.070000   $1504238 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=1 cc, sD=0.00 mm'
LHC.MQML.5L5.E                                          10.646851 TH  0.070000   $1504239 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                         16.716183 TH  0.000000   $1504240 - 13-FEB-2023 - TSU: 'Control 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm,Off.: dH=0.9cc, dV=0.8cc, dD=0mm' - Was Control according to Tsunami
*END
";
        }
    
        
    }
}