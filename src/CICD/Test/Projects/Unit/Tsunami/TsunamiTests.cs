﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using System.Xml.Serialization;
using TSU.ENUM;
using TSU.Common.Elements;

using TSU.Common.Instruments;
using TSU.Common.Measures;
using TSU.Tools;
using TSU.Unit_Tests;
using TSU.Preferences;

namespace TSU.Unit_Tests
{

   

    [TestClass()]
    public class Serial
    {
        
        public static System.IO.MemoryStream TryToSerialize(object obj, bool toFile)
        {
            try
            {
                if (toFile)
                {
                    var fileName = obj.GetType().ToString();
                    //fileName = fileName.Replace("+", "");
                    System.IO.Directory.CreateDirectory(@"Serialization\");
                    using (System.IO.FileStream fileStream = System.IO.File.Create(@"Serialization\" + fileName + ".xml"))
                    {
                        XmlSerializerNamespaces nameSpace = new XmlSerializerNamespaces();
                        nameSpace.Add("", "");
                        XmlSerializer serializer = new XmlSerializer(obj.GetType());
                        serializer.Serialize(fileStream, obj, nameSpace);
                        fileStream.Position = 0;
                        fileStream.Close();
                    }

                    return null;
                }
                else
                {
                    System.IO.MemoryStream stream = new System.IO.MemoryStream(); 
                    XmlSerializerNamespaces nameSpace = new XmlSerializerNamespaces();
                    nameSpace.Add("", "");
                    XmlSerializer serializer = new XmlSerializer(obj.GetType());
                    serializer.Serialize(stream, obj, nameSpace);

                    return stream;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Serial failed",ex);
            }
        }
        public static void TryToDeSerialize(ref object obj,  string path = "")
        {
            try
            {
                if (path == "") path = string.Format(@"Serialization\{0}.xml", obj.GetType().ToString());
                XmlSerializer serializer = new XmlSerializer(obj.GetType());
                System.IO.StreamReader reader = new System.IO.StreamReader(path);
                obj = serializer.Deserialize(reader);
                reader.Close();
                reader.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.InnerException.InnerException.InnerException.Message);
            }
        }

        public static void TryToDeSerialize(out object obj, Type type, string path = "")
        {
            try
            {
                if (path == "") path = Tsunami2.Preferences.Values.Paths.Temporary + string.Format(@"\serialization\{0}.xml", type.ToString());
                XmlSerializer serializer = new XmlSerializer(type);
                System.IO.StreamReader reader = new System.IO.StreamReader(path);
                obj = serializer.Deserialize(reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.InnerException.InnerException.InnerException.Message);
            }
        }

        public static void TryToDeSerialize(ref object obj, System.IO.Stream s)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(obj.GetType());
                s.Position = 0;
                System.IO.StreamReader reader = new System.IO.StreamReader(s);
                
                obj = serializer.Deserialize(reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.InnerException.InnerException.InnerException.Message);
            }
        }
    }
}
