using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU;
using System.Xml;
using System.Xml.Serialization;
using TSU.Common.Instruments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TsunamiTests;
using P = TSU.Preferences;
using System.Diagnostics;

namespace TSU.Unit_Tests
{
    [TestClass()]
    public class InitTests
    {
        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [AssemblyInitialize]
        //[ClassInitialize]
        public static void CreateTsunamiObject(TestContext testContext)
        {
            Trace.WriteLine("CreateTsunamiObject");
            Tsunami2.InitializePropertiesForUnitTesting();
            Tsunami2.InitializeTsunamiViewForUnitTesting();
            TSU.Preferences.Preferences.Instance.InitializeWithAllXmlParametersFiles();
            var prop = Tsunami2.Properties;
            var pref = Tsunami2.Preferences.Values;
        }

        //[TestInitialize]
        //public void CreateTsunamiObject()
        //{
        //    Trace.WriteLine("CreateTsunamiObject");
        //    Tsunami2.InitializePropertiesForUnitTesting();
        //    Tsunami2.InitializeTsunamiViewForUnitTesting();
        //}

        [AssemblyCleanup]
        public static void AssemblyCleanup()
        {

        }
    }
        [TestClass()]
    public class TSUPreferencesTests
    {
        [TestMethod()]
        public void TSUPreferences_Create_SigmaFile()
        {
            Trace.WriteLine("TSUPreferences_Create_SigmaFile");
            var a = Tsunami2.Properties;
            List<SigmaForAInstrumentCouple> l = new List<SigmaForAInstrumentCouple>();
            SigmaForAInstrumentCouple s1 = new SigmaForAInstrumentCouple();
            s1.Instrument = "at40x";
            s1.constAnglCc = 1;
            s1.ppmDistMm = 1;
            s1.sigmaAnglCc = 1;
            s1.sigmaDistMm = 1;
            s1.sigmaZenDCc = 1;
            l.Add(s1);

            XmlSerializer serializer = new XmlSerializer(l.GetType());
            System.IO.FileStream stream = System.IO.File.Create(@"c:\Data\Tsunami\temp\SigmaForAInstrumentCouple.xml");
            serializer.Serialize(stream, l);
            stream.Close();
            stream.Dispose();

            Assert.IsTrue(true);
        }

        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [TestMethod()]
        public void InitializeGeodeThingsTest()
        {
            
            Assert.AreEqual(true, P.Preferences.Instance.Instruments.Count>0);
            Assert.AreEqual(true, P.Preferences.Instance.InstrumentSlashTargetCoupleSigmas.Count > 0);
        }

        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [TestMethod()]
        public void GetEtalonnageParameterTest()
        {
            //TODO: TEST_BUG 
            // P.Preferences.Instance.GetEtalonnageParameter("438198", "5589"); ---> Index was out of range


            //EtalonnageParameter p = P.Preferences.Instance.GetEtalonnageParameter("438198", "5589");
            //Assert.AreEqual(p.fourrierWaveLength, 104.75333);
            //TSU.Tests.Serial.TryToSerialize(p, true);
            //object p2 = new EtalonnageParameter.Theodolite();
            //TSU.Tests.Serial.TryToDeSerialize(ref p2);
            //Assert.AreEqual(((EtalonnageParameter.Theodolite)p2).fourrierWaveLength , p.fourrierWaveLength);
        }

    }
}
