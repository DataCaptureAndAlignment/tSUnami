﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using System.Xml.Serialization;
using TSU;
using TSU.ENUM;
using TSU.Common.Elements;
using TSU.Common.Elements.Composites;

using TSU.Common.Instruments;
using D = TSU.Common.Instruments.Device;
using M = TSU.Common.Measures;
using TSU.Tools;
using TSU.Tests;
using TestsTsunami;
using TSU.Preferences;
using TSU.Common.Instruments.Reflector;
using System.IO;
using System.CodeDom.Compiler;

namespace TSU.Tests
{
    [TestClass]
    public class TsuSerialization
    {

        [TestMethod]
        public void SerilaizeLevelingStaff()
        {
            object obj = null;
            object obj2 = null;
            try
            {
                obj = new LevelingStaff();
                obj2 = new LevelingStaff();
                (obj as TsuObject).Id = "3";

                TSU.Unit_Tests.Serial.TryToSerialize(obj, true);
                TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);
            }
            catch (Exception ex)
            {
                string a = ex.InnerException.InnerException.InnerException.InnerException.Message;
                Assert.IsTrue(false);
            }
            Assert.AreEqual((obj2 as LevelingStaff).Id, (obj2 as LevelingStaff).Id);
        }


        [TestMethod]
        public void TheodoliteStation_Mock()
        {
            //TODO: TEST_BUG
            // Invalid setup on a non-virtual (overridable in VB) member: m => m._Name for FilledMockStation();

            //var mi = new FilledMockStation();
            //Assert.AreEqual("aze", mi.Object._Name, "nooo");
        }
        [TestMethod]
       
        private Coordinates GetCoordinate()
        {
            DoubleValue x = new DoubleValue() { Value = 10.01, Sigma = 0.01 };
            DoubleValue y = new DoubleValue() { Value = 20.01, Sigma = 0.02 };
            DoubleValue z = new DoubleValue() { Value = 30.01, Sigma = 0.03 };

            return new Coordinates() { X = x, Y = y, Z = z };
        }
        [TestMethod]
        public void Coordinate_SerializationOf()
        {
            //TODO: TEST_EMPTY

            Coordinates obj = GetCoordinate();
            TSU.Unit_Tests.Serial.TryToSerialize(obj, true);
        }

        [TestMethod]
        [DeploymentItem(@"Preferences\", @"Preferences\")]
        public void TheodoliteStation_SerializationOfSensorName()
        {
            //TODO: TEST_BUG
            // Object reference not set to an instance of an object - Polar.Station() is empty

            Polar.Station obj =null;
            object obj2=null;

            //try
            //{
                
            //    obj = new Polar.Station();
            //    obj2 = new Polar.Station();
            //    obj.ParametersBasic._Instrument = new D.AT40x.Instrument() { _Name = "AT401_390858" };

            //    TSU.Tests.Serial.TryToSerialize(obj, true);
            //    TSU.Tests.Serial.TryToDeSerialize(ref obj2);
            //}
            //catch (Exception ex)
            //{
            //    string a = ex.InnerException.InnerException.InnerException.InnerException.Message;
            //    Assert.IsTrue(false);
            //}
            //Assert.AreEqual(obj.ParametersBasic._Instrument._Name, (obj2 as Polar.Station).Parameters2._Instrument._Name);
        }

        [TestMethod]
        public void TheodoliteStation_Serialization()
        {
            //Preferences.Instance.Initialize();
            Polar.Station obj = new Polar.Station();
            object obj2 = new Polar.Station();
            string name = "test";
            try
            {
                
                obj.Parameters2.DefaultMeasure = (new Polar.Measure() { _Name = name });

                TSU.Unit_Tests.Serial.TryToSerialize(obj, true);
                TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);
            }
            catch (Exception ex)
            {
                string a = ex.InnerException.InnerException.InnerException.InnerException.Message;
                Assert.IsTrue(false);
            }
            Assert.AreEqual(name, (obj2 as Polar.Station).Parameters2.DefaultMeasure._Name);
        }
        [TestMethod]
        public void TheodoliteStation_SerializationWithTheoParameter()
        {

            Polar.Station obj = new Polar.Station();
            object obj2 = new Polar.Station();
            try {
                Polar.Station.Parameters param = obj.Parameters2 as Polar.Station.Parameters;

                param._StationPoint = new Point("test");
                param.Setups.FinalValues = new Polar.Station.Parameters.Setup.Values();
                param.Setups.FinalValues.Version = "test";

                TSU.Unit_Tests.Serial.TryToSerialize(obj, true);
                TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);
            }
            catch (Exception ex)
            {
                string a = ex.InnerException.InnerException.InnerException.InnerException.Message;
                Assert.IsTrue(false);
            }
            Assert.AreEqual(
                (obj.Parameters2 as Polar.Station.Parameters).Setups.FinalValues.Version,
                ((obj2 as Polar.Station).Parameters2 as Polar.Station.Parameters).Setups.FinalValues.Version);
            Assert.AreEqual(
                (obj.Parameters2 as Polar.Station.Parameters)._StationPoint._Name,
                ((obj2 as Polar.Station).Parameters2 as Polar.Station.Parameters)._StationPoint._Name);
        }
        [TestMethod]
        public void TheodoliteStation_Serialization_WithSimpleStationParameters()
        {
            //TODO: ASSERT_FAIL
            // obj2 is null


            Polar.Station obj = new Polar.Station();
            object obj2 = new Polar.Station();
            try
            {
                //obj._Parameters = new StationParameters();  // Trick here station parameters instead of theodoliteStationParameter
                
                obj._Name = "test";
                obj.Parameters2._IsSetup = true;
                obj.Parameters2._State = new Common.Station.State.Opening();
                obj.MeasuresToDo.Add(new Polar.Measure() { _Name = "test2", _Point = new Point("test2") });
                obj.MeasuresToDo.Add(new Polar.Measure() { _Name = "test3", _Point = new Point("test3") });
                obj.MeasuresToDo.Add(new Polar.Measure() { _Name = "test4", _Point = new Point("test4") });

                TSU.Unit_Tests.Serial.TryToSerialize(obj, true);
                TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);
            }
            catch (Exception ex)
            {
                string a = ex.InnerException.InnerException.InnerException.InnerException.Message;
                Assert.IsTrue(false);
            }
            var station2 = obj2 as Polar.Station;
            Assert.AreEqual(obj._Name, station2._Name);
            Assert.AreEqual(obj.Parameters2._IsSetup, station2.Parameters2._IsSetup);
            Assert.AreEqual(obj.Parameters2._State.GetType(), station2.Parameters2._State.GetType());
            Assert.AreEqual(obj.MeasuresToDo[0]._Name, station2.MeasuresToDo[0]._Name);
        }

        [TestMethod]
        public void Reflector_SerializationOf()
        {
            //TODO: TEST_EMPTY

            Reflector obj = new Reflector();
            obj._Brand = "test";
            object obj2 = new Reflector();
            TSU.Unit_Tests.Serial.TryToSerialize(obj, true);
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);
        }
        [TestMethod()]
        public void MeasureTheodolite_SerializationOf()
        {
            //TODO: TEST_BUG
            // faced object is snull 

            DoubleValue h = new DoubleValue() { Value = 10.01, Sigma = 0.01 };
            Angles raw = new Angles() { Horizontal = h };
            M.MeasureOfAngles hor = new M.MeasureOfAngles() { Raw = raw };
            Polar.Measure face1 = new Polar.Measure() { Angles = hor, Face = FaceType.Face1};
            Polar.Measure face2 = new Polar.Measure() { Angles = hor, Face = FaceType.Face2 };

            Polar.Measure faced = new Polar.Measure();
            Polar.Measure diff = new Polar.Measure();
            bool tol;
            bool dummy = true;
            string mess;
            //faced = Common.Compute.Survey.DoFaceAverage(face1, face2, 10, 0.1, out diff, out tol,out mess, ref dummy);
            //faced._Status = new M.States.Good();
            //face1._Point._Name = "face1";
            //face1.CommentFromUser = "comface1";
            //face1._Date = DateTime.Now;

            //TSU.Tests.Serial.TryToSerialize(faced, true);
        }
        [TestMethod()]
        public void MeasureOfDistance_Serialization()
        {
            //TODO: TEST_EMPTY

            DoubleValue d = new DoubleValue() { Value = 10.01, Sigma = 0.01 };
            object m = new M.MeasureOfDistance() { Corrected = d };
            TSU.Unit_Tests.Serial.TryToSerialize(m, true);
            object m2 = new M.MeasureOfDistance();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref m2);

        }
    
        private PrimitiveElement GetPrimitiveElement()
        {
            PrimitiveElement e = new PrimitiveElement();
            e._Coordinates.Ccs = GetCoordinate();
            e._Coordinates.Local =  GetCoordinate();
            e._Coordinates.UserDefined = GetCoordinate();
            return e;
        }
        private Point GetPoint(string name)
        {
            Coordinates ccs = GetCoordinate();
            Coordinates phy = GetCoordinate();
            Coordinates lcl = GetCoordinate();

            return new Point(name) { fileElementType = TSU.ENUM.ElementType.Point };
        }
        [TestMethod]
        public void PrimitiveElement_SerializationOf()
        {
            PrimitiveElement obj = GetPrimitiveElement();
            obj._Origin = "OOO";
            TSU.Unit_Tests.Serial.TryToSerialize(obj, true);

            object obj2 = new PrimitiveElement();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj._Origin, ((PrimitiveElement)obj2)._Origin);
        }
        [TestMethod]
        public void Point_SerializationOf()
        {
            Point obj = GetPoint("P1");
            object obj2 = new Point();
            TSU.Unit_Tests.Serial.TryToSerialize(obj, true);
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj._Name, ((Point)obj2)._Name);
        }
        [TestMethod]
        public void CompositeElement_SerializationOf_Test()
        {
            CompositeElement obj = new CompositeElement("Magnet");
            obj.fileElementType = TSU.ENUM.ElementType.Class;
            //obj.elements.Add(GetPoint("P1"));
            //obj.elements.Add(GetPoint("P2"));
            TSU.Unit_Tests.Serial.TryToSerialize(obj, true);
            object obj2 = new CompositeElement();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj._Name, ((CompositeElement)obj2)._Name);
        }

        [TestMethod]
        public void Tolerance_SerializationOf_Test()
        {
            string path = "temp.xml";
            var obj = new Common.Tolerances();

            using (StreamWriter file = File.CreateText(path))
            {
                TSU.IO.Xml.Serialize(obj, file);
            }

            var newObj = TSU.IO.Xml.DeserializeFile(typeof(Common.Tolerances), path) as Common.Tolerances;

            Assert.AreEqual(obj.GetType(), newObj.GetType());
        }
        [TestMethod]
        public void TheodoliteStation_Deserialization()
        {
            //TODO: ASSERT_FAIL
            // obj2 is empty
            // Object reference not set to an instance of an object - Polar.Station()

            //Polar.Station obj = new Polar.Station();
            //obj._Name = "test";
            //TSU.Tests.Serial.TryToSerialize(obj, true);

            //object obj2 = new Polar.Station();
            //TSU.Tests.Serial.TryToDeSerialize(ref obj2);


            //Assert.AreEqual(obj._Name, ((TSU.Common.Station)obj2)._Name);

        }
        
    }
}
