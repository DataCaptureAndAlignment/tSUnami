﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Tools.Transformation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Common.Elements;
using TSU.Common.Compute.Transformation;

namespace TSU.Tools.Transformation.Tests
{
    [TestClass()]
    public class SystemsTests
    {
        [TestMethod()]
        public void SU2PhysIn_LHCb_Test()
        {
            // beam rot = -0.2292824 gon =>  -2.00000             1.01080              -2.99638
            Coordinates c;

            //Back and forth on random point su 2 phys 
            c = new Coordinates("t", 3, 2, 1);
            c = Systems.Rotate3DAroundOrigin(
                        c,
                        300.229285, 0, 100,
                        ENUM.AngularUnit.Gon, false);

            Assert.AreEqual(-2, c.X.Value, 0.00001);
            Assert.AreEqual(1.01080, c.Y.Value, 0.00001);
            Assert.AreEqual(-2.99638, c.Z.Value, 0.00001);

            // phys 2 su
            c = Systems.Rotate3DAroundOrigin(
                        c,
                        300.229285, 0, 100,
                        ENUM.AngularUnit.Gon, true);

            Assert.AreEqual(3, c.X.Value, 0.00001);
            Assert.AreEqual(2, c.Y.Value, 0.00001);
            Assert.AreEqual(1, c.Z.Value, 0.00001);

            // a point on the beam a 10m su 2 phys
            c = new Coordinates("t", -10, 0, 0);
            c = Systems.Rotate3DAroundOrigin(
                        c,
                        300.229285, 0, 100,
                        ENUM.AngularUnit.Gon, false);

            Assert.AreEqual(0, c.X.Value, 0.00001);
            Assert.AreEqual(-0.036015, c.Y.Value, 0.00001);
            Assert.AreEqual(9.9999351, c.Z.Value, 0.00001);
        }

        [TestMethod()]
        public void SU2PhysIn_TestAlice()
        {
            // beam rot = -0.882371111 gon => 2.00000              1.04149              2.98585

            Coordinates c;

            c = new Coordinates("t", 3, 2, 1);
            c = Systems.Rotate3DAroundOrigin(
                        c,
                        -100.882371111, 0, 300,
                        ENUM.AngularUnit.Gon, false);

            Assert.AreEqual(2.00000, c.X.Value, 0.00001); 
            Assert.AreEqual(1.04149, c.Y.Value, 0.00001);
            Assert.AreEqual(2.98585, c.Z.Value, 0.00001);

            c = Systems.Rotate3DAroundOrigin(
                        c,
                        -100.882371111, 0, 300,
                        ENUM.AngularUnit.Gon, true);

            Assert.AreEqual(3, c.X.Value, 0.00001);
            Assert.AreEqual(2, c.Y.Value, 0.00001);
            Assert.AreEqual(1, c.Z.Value, 0.00001);
        }

        [TestMethod()]
        public void SU2PhysIn_TestATLAS()
        {
            // beam rot = -0.78696735 gon =>  2.00000              1.03701              2.98741
            Coordinates c;

            c = new Coordinates("t", 3, 2, 1);
            c = Systems.Rotate3DAroundOrigin(
                        c,
                        -100.78702756, 0, 300,
                        ENUM.AngularUnit.Gon, false);

            Assert.AreEqual(2.00000, c.X.Value, 0.00001);
            Assert.AreEqual(1.03701, c.Y.Value, 0.00001);
            Assert.AreEqual(2.98741, c.Z.Value, 0.00001);

            c = Systems.Rotate3DAroundOrigin(
                        c,
                        -100.78702756, 0, 300,
                        ENUM.AngularUnit.Gon, true);

            Assert.AreEqual(3, c.X.Value, 0.00001);
            Assert.AreEqual(2, c.Y.Value, 0.00001);
            Assert.AreEqual(1, c.Z.Value, 0.00001);
        }

        [TestMethod()]
        public void SU2PhysIn_TestCMS()
        {
            // beam rot = -0.78696735 gon =>  2.00000              0.96292              3.01211
            Coordinates c;

            c = new Coordinates("t", 3, 2, 1);
            c = Systems.Rotate3DAroundOrigin(
                        c,
                        300.785366,0, 300,
                        ENUM.AngularUnit.Gon, false);

            Assert.AreEqual(2.00000, c.X.Value, 0.00001);
            Assert.AreEqual(0.96292, c.Y.Value, 0.00001);
            Assert.AreEqual(3.01211, c.Z.Value, 0.00001);

            c = Systems.Rotate3DAroundOrigin(
                        c,
                        300.785366, 0, 300,
                        ENUM.AngularUnit.Gon, true);

            Assert.AreEqual(3, c.X.Value, 0.00001);
            Assert.AreEqual(2, c.Y.Value, 0.00001);
            Assert.AreEqual(1, c.Z.Value, 0.00001);
        }

        [TestMethod()]
        public void SU2PhysIn_Test_NTof()
        {
            // beam rot = -0.01212  rad gon =>   2.00000              0.96357              3.01190
            Coordinates c;

            // a point on the beam 
            c = new Coordinates("t", 10, 10, 0);
            c = Systems.Rotate3DAroundOrigin(
                        c,
                        -99.24946755, 0, 300,
                        ENUM.AngularUnit.Gon, false);

            Assert.AreEqual(10.00000, c.X.Value, 0.00001);
            Assert.AreEqual(-0.11789, c.Y.Value, 0.00001);
            Assert.AreEqual( 9.99931, c.Z.Value, 0.00001);


            c = new Coordinates("t", 3, 2, 1);
            c = Systems.Rotate3DAroundOrigin(
                        c,
                        -99.24946755, 0, 300,
                        ENUM.AngularUnit.Gon, false);

            Assert.AreEqual(2.00000, c.X.Value, 0.00001);
            Assert.AreEqual(0.96456, c.Y.Value, 0.00001);
            Assert.AreEqual(3.01158, c.Z.Value, 0.00001);

            
            c = Systems.Rotate3DAroundOrigin(
                        c,
                        -99.24946755, 0, 300,
                        ENUM.AngularUnit.Gon, true);

            Assert.AreEqual(3, c.X.Value, 0.00001);
            Assert.AreEqual(2, c.Y.Value, 0.00001);
            Assert.AreEqual(1, c.Z.Value, 0.00001);
        }

            
        [TestMethod()]
        public void SU2PhysIn_TestNa62()
        {
            // beam rot = -0.01212  rad gon =>   0.99995             3.0000              2.00002
            Coordinates c;

            // a point on the beam 
            c = new Coordinates("t", 10, 10, 0);
            c = Systems.Rotate3DAroundOrigin(
                        c,
                        300.001464, 0, 300,
                        ENUM.AngularUnit.Gon, false);

            Assert.AreEqual(10.00000, c.X.Value, 0.00001);
            Assert.AreEqual(-0.00022996, c.Y.Value, 0.00001);
            Assert.AreEqual(9.999999999, c.Z.Value, 0.00001);

            // random point back and forth
            c = new Coordinates("t", 30, 20, 10);
            c = Systems.Rotate3DAroundOrigin(
                        c,
                        300.001464, 0, 300,
                        ENUM.AngularUnit.Gon, false);

            Assert.AreEqual(20, c.X.Value, 0.00001);
            Assert.AreEqual(9.99931, c.Y.Value, 0.00001);
            Assert.AreEqual(30.00023, c.Z.Value, 0.00001);
            
            c = Systems.Rotate3DAroundOrigin(
                        c,
                        300.001464, 0, 300,
                        ENUM.AngularUnit.Gon, true);

            Assert.AreEqual(30, c.X.Value, 0.00001);
            Assert.AreEqual(20, c.Y.Value, 0.00001);
            Assert.AreEqual(10, c.Z.Value, 0.00001);
        }
    }
}