﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using E = TSU.Common.Elements;
using TSU.Common.Compute;
using System.Collections;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ToolTip;

namespace TSU.Tools.Tests
{
    [TestClass()]
    public class LinearRegressionTests
    {
        [TestMethod()]
        public void Get3dLineInAPointTest()
        {
            //TODO-toRestore: TEST_BUG
            //Function LinearRegression.Get3dLine ---> ComputeBearingAndSlope3D(line3D) ---> Can't compute more than 2 points

            E.Composites.FittedShape result;

            //result = LinearRegression.Get3dLine(new List<E.Point>()
            //{
            //    new E.Point() {  _Coordinates = new E.CoordinatesInAllSystems() { Ccs = new  E.Coordinates("c1", 1, 1, 1) } },
            //    new E.Point() {  _Coordinates = new E.CoordinatesInAllSystems() { Ccs = new  E.Coordinates("c2", 2, 2, 2) } },

            //    new E.Point() {  _Coordinates = new E.CoordinatesInAllSystems() { Ccs = new  E.Coordinates("c3", 3, 3, 3) } }
            //}, out string mess,null);

            //Assert.AreEqual(50, (result.parametricShape as E.Line3D).Bearing.Value);

            //double shapesSlope = (100-60.817345) /200* Math.PI;
            //Assert.AreEqual(shapesSlope, (result.parametricShape as E.Line3D).Slope.Value, 0.000001);

            //result = LinearRegression.Get3dLine(new List<E.Point>()
            //{
            //    new E.Point() {  _Coordinates = new E.CoordinatesInAllSystems() { Ccs = new  E.Coordinates("c1", 1.2918008,   13.2916019,  10.6023679) } },
            //    new E.Point() {  _Coordinates = new E.CoordinatesInAllSystems() { Ccs = new  E.Coordinates("c4", 4.5845512,   35.0352314,  18.2172605) } }
            //} , out string message,null);
            
            //Assert.AreEqual(9.567965, (result.parametricShape as E.Line3D).Bearing.Value, 0.000001);// result from shapes
            // shapesSlope = (100-78.7787) / 200 * Math.PI;
            //Assert.AreEqual(shapesSlope, (result.parametricShape as E.Line3D).Slope.Value, 0.000001); // result from shapes
            
        }
        [TestMethod()]
        public void Get3dLinefrom2ointsInAPointTest()
        {
            E.Composites.FittedShape result;

            var p1 = new E.Point() { _Coordinates = new E.CoordinatesInAllSystems() };
            p1._Coordinates.AddOrReplaceCoordinatesInOneSystem("CCS-Z", new E.Coordinates("c1", 4470.63183, 5006.74818, 2329.9745));
            var p2 = new E.Point() { _Coordinates = new E.CoordinatesInAllSystems() };
            p2._Coordinates.AddOrReplaceCoordinatesInOneSystem("CCS-Z", new E.Coordinates("c2", 4471.28138, 5003.90614, 2330.20126));

            result = LinearRegression.Get3dLine(new List<E.Point>() { p1, p2 }, out string mess, null);

            Assert.AreEqual(185.69570, (result.parametricShape as E.Line3D).Bearing.Value, 0.00001);
            double shapesSlope = (100 - 95.0581861078062) / 200 * Math.PI;
            Assert.AreEqual(shapesSlope, (result.parametricShape as E.Line3D).Slope.Value, 0.000001);
        }

        [TestMethod()]
        public void FindLinearLeastSquaresFitTest()
        {
            List<E.Coordinates> coordinates = new List<E.Coordinates>()
            {
                new  E.Coordinates("c1", 5, - 0.15,  0),
                new E.Coordinates("c1", 6, - 0.13, 0),
                new E.Coordinates("c1", 7, - 0.11, 0),
                new E.Coordinates("c1", 8, - 0.09, 0),
                new E.Coordinates("c1", 9 ,- 0.07, 0),
                new E.Coordinates("c1", 10, - 0.05, 0),
                new E.Coordinates("c1", 11, - 0.03, 0),
                new E.Coordinates("c1", 12, - 0.01, 0),
                new E.Coordinates("c1", 13,  0.01, 0),
                new E.Coordinates("c1", 14,  0.03, 0),
            };

            LinearRegression.FindLinearLeastSquaresFit(coordinates, out double m, out double b);

            double result = m * 10 + b;
            Assert.AreEqual(-0.05, result, 0.000001);
        }

        //Measured Points     Projected points        distance dx(mm) dy(mm) dz(mm)
        //1620.39544	2013.03171	434.34361	1620.39898	2013.02186	434.34334	10.47438	3.54357	-9.85301	-0.27199
        //1642.06641	2020.81377	434.34282	1642.06621	2020.81433	434.34332	0.77815	-0.20162	0.56061	0.50058
        //1663.24434	2028.42003	434.34291	1663.24088	2028.42966	434.34330	10.24099	-3.46322	9.62959	0.39354
        //1684.61385	2036.09395	434.34413	1684.60673	2036.11375	434.34329	21.05527	-7.11985	19.79697	-0.84365
        //1706.34736	2043.95217	434.34322	1706.35359	2043.93486	434.34327	18.39629	6.22569	-17.31074	0.04886
        //1729.02306	2052.09579	434.34323	1729.02561	2052.08870	434.34325	7.53244	2.54914	-7.08796	0.02062
        //1752.11986	2060.38958	434.34308	1752.11833	2060.39384	434.34323	4.53449	-1.53371	4.26453	0.15204
        
        [TestMethod()]
        public void Fitting3DLine()
        {
            List<E.Coordinates> xyz = new List<E.Coordinates>()
            {
                new E.Coordinates("P0",1620.39544, 2013.03171, 434.34361),
                new E.Coordinates("P1",1642.06641, 2020.81377, 434.34282),
                new E.Coordinates("P2",1663.24434, 2028.42003, 434.34291),
                new E.Coordinates("P3",1684.61385, 2036.09395, 434.34413),
                new E.Coordinates("P4",1706.34736, 2043.95217, 434.34322),
                new E.Coordinates("P5",1729.02306, 2052.09579, 434.34323),
                new E.Coordinates("P6",1752.11986, 2060.38958, 434.34308)
            };


            LinearRegression.ComputeLineParametersWithNormalEquations(xyz, out double b, out double va, out List<E.Coordinates> proj, out double[] dist, out List<E.Coordinates> res);
            
            Assert.AreEqual(78.021350, b, 0.000001);
            Assert.AreEqual(100.000048, va, 0.000001);
            
            Assert.AreEqual(1620.39898, proj[0].X.Value, 0.00001);
            Assert.AreEqual(2020.81433, proj[1].Y.Value, 0.00001);
            Assert.AreEqual( 434.34330, proj[2].Z.Value, 0.00001);

            Assert.AreEqual(21.05527, dist[3], 0.00001);
            
            Assert.AreEqual(6.22569, res[4].X.Value, 0.00001);
            Assert.AreEqual(-7.08796, res[5].Y.Value, 0.00001);
            Assert.AreEqual(0.15204, res[6].Z.Value, 0.00001);
        }

        [TestMethod()]
        public void Fitting3DLine2points()
        {
            List<E.Coordinates> xyz = new List<E.Coordinates>()
            {
                new E.Coordinates("P0",1620.39544, 2013.03171, 434.34361),
                new E.Coordinates("P6",1752.11986, 2060.38958, 434.34308)
            };

            // shapes:78.02817
            // shapes: 100.00024

            LinearRegression.ComputeLineParametersWithNormalEquations(xyz, out double b, out double va, out List<E.Coordinates> proj, out double[] dist, out List<E.Coordinates> res);

            Assert.AreEqual(78.02817, b, 0.00001);
            Assert.AreEqual(100.00024, va, 0.00001);
        }

        [TestMethod()]
        public void Fitting3DLine2()
        {
            //TODO-toRestore: ASSERT_FAIL
            //Expected value <185.68132> and actual value <184.716175808385>
            //Expected value <94.61195> and actual value <94.6092609710659>.

            List<E.Coordinates> xyz = new List<E.Coordinates>()
            {
                new E.Coordinates("P0",4470.63183,  5006.74818,  2329.9745),
                new E.Coordinates("P2",4470.72149,  5006.34865,  2329.95603),
                new E.Coordinates("P1",4471.28138,  5003.90614,  2330.20126)
            };

            // not same result with shapes ...
            // shapes:185.6813     grades
            // shapes:94.6120  grades

            LinearRegression.ComputeLineParametersWithNormalEquations(xyz, out double b, out double va, out List<E.Coordinates> proj, out double[] dist, out List<E.Coordinates> res);

            Assert.AreEqual(184.71617580838483, b, 0.000001); 
            Assert.AreEqual(94.609260971065851, va, 0.000001);
        }

        [TestMethod()]
        public void Fitting3DLine4()
        {
            // this is not working the result are different from shapes
            //TODO-toRestore: ASSERT_FAIL
            //Expected value <185.68132> and actual value <184.879750514818>. 
            //Expected value <94.61195> and actual value <95.0581858313653>. 

            List<E.Coordinates> xyz = new List<E.Coordinates>()
            {
                new E.Coordinates("P0",4470.63183,  5006.74818,  2329.9745),
                new E.Coordinates("P1",4471.28138,  5003.90614,  2330.20126)
            };

            // shapes:185.6957     grades
            // shapes:95.0582  grades

            LinearRegression.ComputeLineParametersWithNormalEquations(xyz, out double b, out double va, out List<E.Coordinates> proj, out double[] dist, out List<E.Coordinates> res);

            Assert.AreEqual(184.87975051481766, b, 0.000001);
            Assert.AreEqual(95.058185831365336, va, 0.000001);
        }

    }
}