﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Tools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Compute.Transformation;

namespace TSU.Tools.Tests
{
    [TestClass()]
    public class Rotation3DTests
    {
        [TestMethod()]
        public void Rotation3DTestSerialization()
        {
            Rotation3D obj = new Rotation3D() { RotX = 10, RotY = 20, RotZ = 30, SumToAvoidUnawareModification = 60 };

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new Rotation3D();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj.RotX, (obj2 as Rotation3D).RotX);
        }

    }
}
