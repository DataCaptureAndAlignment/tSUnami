﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Elements;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TSU.Common;

namespace TSU.Unit_Tests
{
    [TestClass()]
    public class IterationTests
    {
        [TestMethod()]
        public void GetNextTest()
        {
        
            List<string> nameexisting = new List<string>()
            {
                 "point.005" ,
                 "line.02" ,
                 "point.010" ,
                 "point.006" ,
                 "Point02",
                 "Point[03]",
                 "Point[04]"
            };
            List<string> names;
            List<string> nextnames;

            names = new List<string>() { "point.001", "line.02" };
            nextnames = TSU.Common.Analysis.Element.Iteration.GetNext(names, nameexisting, Common.Analysis.Element.Iteration.Types.All );
            Assert.AreEqual("point.011", nextnames[0]);
            Assert.AreEqual("line.011", nextnames[1]);

            names = new List<string>() { "point.001" };
            nextnames = TSU.Common.Analysis.Element.Iteration.GetNext(names, nameexisting, Common.Analysis.Element.Iteration.Types.All);
            Assert.AreEqual("point.011", nextnames[0]);

            names = new List<string>() { "point[####]" };
            nextnames = TSU.Common.Analysis.Element.Iteration.GetNext(names, nameexisting, Common.Analysis.Element.Iteration.Types.All);
            Assert.AreEqual("point[0001]", nextnames[0]);

            names = new List<string>() { "point.###" };
            nextnames = TSU.Common.Analysis.Element.Iteration.GetNext(names, nameexisting, Common.Analysis.Element.Iteration.Types.All);
            Assert.AreEqual("point.011", nextnames[0]);
        }
    }
}