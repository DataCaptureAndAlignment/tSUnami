﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Tools.Conversions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSU.Tools.Conversions.Tests
{
    [TestClass()]
    public class TolerancesTests
    {
        [TestMethod()]
        public void CheckTest()
        {
            return; // todo
            bool ok = false;
            string message = "";
            ok = Tolerances.Check(0.123456789, 1, out message, "mm");
            Assert.IsTrue(ok);
            Assert.AreEqual("OK, 123.46 < 1000.00 mm", message);

            ok = Tolerances.Check(0.123456789, 0, out message, "m");
            Assert.IsFalse(ok);
            Assert.AreEqual("KO, 0.12346 >= 0.00000 m", message);

            ok = Tolerances.Check(0.123456789, 0, out message, "gon");
            Assert.IsFalse(ok);
            Assert.AreEqual("KO, 0.1235 >= 0.0000 gon", message);

            ok = Tolerances.Check(0.123456789, 0.5, out message, "cc");
            Assert.IsTrue(ok);
            Assert.AreEqual("OK, 1235 < 5000 cc", message);
        }
    }
}