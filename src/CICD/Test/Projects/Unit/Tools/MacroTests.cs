﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSU.Tools.Tests
{
    [TestClass()]
    public class MacroTests
    {
        [TestMethod()]
        public void LoadFromFileTest()
        {
            string path = "temp.xml";
            System.IO.File.WriteAllText(path, GetMacro1Content());

            Macro m = Macro.LoadFromFile(path);

            Assert.AreEqual(263, m.KeyStrokesAndMouseClicks[1].DelayInMillis);
        }

        public static string GetMacro1Content()
        {
            return
@"<?xml version=""1.0"" encoding=""utf-16""?>
<Macro Name=""start"" FunctionKey=""F2"" speedFactor=""2"">
	<KeyStrokesAndMouseClicks>
        <IInputActivity d3p1:type=""KeyStroke____"" CTRL=""false"" SHIFT=""false"" ALT=""false"" Key=""Down"" Target=""Button"" Delay=""470"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
	    <IInputActivity d3p1:type=""KeyStroke____"" CTRL=""false"" SHIFT=""false"" ALT=""false"" Key=""Space"" Target=""BigButton"" Delay=""263"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
	</KeyStrokesAndMouseClicks>
</Macro>";

        }
        public static string GetMacro2Content()
        {
            return @"<?xml version=""1.0"" encoding=""utf-16""?>
<Macro Name=""start"" FunctionKey=""F2"" speedFactor=""2"">
	<KeyStrokesAndMouseClicks>
	    <IInputActivity d3p1:type=""KeyStroke____"" CTRL=""false"" SHIFT=""false"" ALT=""false"" Key=""Down"" Target=""Button"" Delay=""364""    xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
        <IInputActivity d3p1:type=""KeyStroke____"" CTRL=""false"" SHIFT=""false"" ALT=""false"" Key=""Right"" Target=""BigButton"" Delay=""384"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
        <IInputActivity d3p1:type=""KeyStroke____"" CTRL=""false"" SHIFT=""false"" ALT=""false"" Key=""Down"" Target=""BigButton"" Delay=""487"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
        <IInputActivity d3p1:type=""KeyStroke____"" CTRL=""false"" SHIFT=""false"" ALT=""false"" Key=""Down"" Target=""BigButton"" Delay=""159"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
        <IInputActivity d3p1:type=""KeyStroke____"" CTRL=""false"" SHIFT=""false"" ALT=""false"" Key=""Down"" Target=""BigButton"" Delay=""152"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
        <IInputActivity d3p1:type=""KeyStroke____"" CTRL=""false"" SHIFT=""false"" ALT=""false"" Key=""Down"" Target=""BigButton"" Delay=""144"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
        <IInputActivity d3p1:type=""KeyStroke____"" CTRL=""false"" SHIFT=""false"" ALT=""false"" Key=""Down"" Target=""BigButton"" Delay=""159"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
        <IInputActivity d3p1:type=""KeyStroke____"" CTRL=""false"" SHIFT=""false"" ALT=""false"" Key=""Down"" Target=""BigButton"" Delay=""152"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
        <IInputActivity d3p1:type=""KeyStroke____"" CTRL=""false"" SHIFT=""false"" ALT=""false"" Key=""Down"" Target=""BigButton"" Delay=""152"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
        <IInputActivity d3p1:type=""KeyStroke____"" CTRL=""false"" SHIFT=""false"" ALT=""false"" Key=""Down"" Target=""BigButton"" Delay=""144"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
        <IInputActivity d3p1:type=""KeyStroke____"" CTRL=""false"" SHIFT=""false"" ALT=""false"" Key=""Down"" Target=""BigButton"" Delay=""168"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
        <IInputActivity d3p1:type=""KeyStroke____"" CTRL=""false"" SHIFT=""false"" ALT=""false"" Key=""Down"" Target=""BigButton"" Delay=""160"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
        <IInputActivity d3p1:type=""KeyStroke____"" CTRL=""false"" SHIFT=""false"" ALT=""false"" Key=""Down"" Target=""BigButton"" Delay=""183"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
        <IInputActivity d3p1:type=""KeyStroke____"" CTRL=""false"" SHIFT=""false"" ALT=""false"" Key=""Down"" Target=""BigButton"" Delay=""207"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
        <IInputActivity d3p1:type=""KeyStroke____"" CTRL=""false"" SHIFT=""false"" ALT=""false"" Key=""Down"" Target=""BigButton"" Delay=""208"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
        <IInputActivity d3p1:type=""KeyStroke____"" CTRL=""false"" SHIFT=""false"" ALT=""false"" Key=""Right"" Target=""BigButton"" Delay=""384"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
	</KeyStrokesAndMouseClicks>
</Macro>";

        }
    }
}