﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Xml.Serialization;
using X = TSU.IO.Xml;

namespace Unit_Tests.Tools
{
    [TestClass()]
    public class XmlTests
    {
        [TestMethod]
        public void TestXML()
        {
            var obj = new Animal() { SizeInMm = 50 };
            var s = X.ToText(obj);
            Assert.IsNotNull(s);

            System.IO.File.WriteAllText("animal.xml", @"<?xml version=""1.0"" encoding=""utf-16""?>
<Animal xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"">
  <SizeInM>0.5</SizeInM>
</Animal>");
            var obj2 = X.DeserializeFile(typeof(Animal), "animal.xml") as Animal;
            Assert.IsNotNull(obj2);
            Assert.AreEqual(50d, obj2.SizeInMm, 1d);
            System.IO.File.Delete("animal.xml");

            System.IO.File.WriteAllText("animal.xml", @"<?xml version=""1.0"" encoding=""utf-16""?>
<Animal xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"">
  <SizeInMm>50</SizeInMm>
</Animal>");
            var obj3 = X.DeserializeFile(typeof(Animal), "animal.xml") as Animal;
            Assert.IsNotNull(obj3);
            Assert.AreEqual(50d, obj3.SizeInMm, 1d);
            System.IO.File.Delete("animal.xml");
        }
    }

    [XmlInclude(typeof(Animal))]
    public class Animal
    {
        private double size = 0;

        public double SizeInM
        {
            get => size;
            set => size = value;
        }

        [Obsolete]
        public double SizeInMm
        {
            get => size * 100d;
            set => size = value / 100d;
        }
    }
}
