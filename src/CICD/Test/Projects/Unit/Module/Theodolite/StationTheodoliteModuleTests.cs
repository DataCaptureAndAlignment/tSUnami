﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TSU.AdvancedModules.Theodolite;
using E = TSU.Common.Elements;
using M = TSU.Common.Measures;
using TSU.Preferences;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Elements.Manager;
using TSU.Common.Elements;

namespace TSU.AdvancedModules.Theodolite.Tests
{
    [TestClass()]
    public class StationTheodoliteModuleTests
    {
        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [TestMethod()]
        public void StationTheodoliteModuleTest()
        {

            Polar.Module theodoliteModule = new Polar.Module();
            Module em = new TSU.Common.Elements.Manager.Module(theodoliteModule);
            em.View = new E.Manager.View(em);
            theodoliteModule.Add(em);
            Polar.Station.Module obj = new Polar.Station.Module(theodoliteModule);

            obj._Name = "test";

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new Polar.Station.Module();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj._Name, (obj2 as Polar.Station.Module)._Name);
        }

        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [DeploymentItem(@"libs\SuSoft\", @"libs\SuSoft\")]
        [TestMethod()]
        public void FillMissingCoordinatesTest()
        {
            
            Common.FinalModule fm = new Polar.Module();
            Polar.Station.Module stm = new Polar.Station.Module(fm);
            Module em = new E.Manager.Module(fm);
            fm.Add(stm);
            fm.Add(em);


            E.Point theoPoint;
            theoPoint = new E.Point("MBG.411508.E");
            theoPoint._Coordinates.Ccs = new E.Coordinates("MBG.411508.E", 2859.87064, 4221.67338,  393.44110);
            theoPoint._Parameters.GisementFaisceau = 107.7206;
            theoPoint._Parameters.Slope = -.056608;

            E.Point point = new E.Point("MBG.411508.S");
            point._Coordinates.Ccs = new E.Coordinates("MBG.411508.S", 2863.32946, 4221.25184,  393.24395);


            Polar.Station st = new Polar.Station();
            st.Parameters2._StationPoint = theoPoint.Clone() as E.Point;
            Tsunami2.Properties.ReferenceFrameOfTheMeasurement = E.Coordinates.ReferenceFrames.CernXYHg85Machine;

            stm.StationTheodolite = st;

            CoordinatesInAllSystems.FillMissingCoordinates(st.Parameters2._StationPoint, point, theoPoint, silente: true, zone: null, null);


            double admissibleError = 0.00001;

            Assert.AreEqual(2863.32946, point._Coordinates.Ccs.X.Value, admissibleError);
            Assert.AreEqual(4221.25184, point._Coordinates.Ccs.Y.Value, admissibleError);
            Assert.AreEqual(393.24395, point._Coordinates.Ccs.Z.Value, admissibleError);

            Assert.AreEqual(0.000002, point._Coordinates.BeamV.X.Value, admissibleError);
            Assert.AreEqual(3.484431, point._Coordinates.BeamV.Y.Value, admissibleError);
            Assert.AreEqual(-0.19715, point._Coordinates.BeamV.Z.Value, admissibleError);

        }
    }
}
