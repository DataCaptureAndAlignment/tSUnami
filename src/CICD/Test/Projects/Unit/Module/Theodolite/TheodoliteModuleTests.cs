﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.AdvancedModules.Theodolite;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Preferences;
namespace TSU.AdvancedModules.Theodolite.Tests
{
    [TestClass()]
    public class TheodoliteModuleTests
    {
        [TestMethod()]
        public void TheodoliteModuleSerialization_Test()
        {

            Polar.Module obj = new Polar.Module();
            //obj.Initialize();

            obj._Name = "test";

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true);
            object obj2 = new Polar.Module();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj._Name, (obj2 as Polar.Module)._Name);
            Assert.AreEqual(obj.childModules.Count, (obj2 as Polar.Module).childModules.Count);
        }

    }
}
