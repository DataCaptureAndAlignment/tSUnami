﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using D = TSU.Common.Instruments.Device;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using P = TSU.Preferences;
using TSU.Common.Elements;
using TSU.Common.Measures;

namespace TSU.Unit_Tests
{
    [TestClass()]
    public class ParametersTests
    {
        [TestMethod()]
        public void ParametersTest()
        {
            Polar.Station.Parameters obj = new TSU.Polar.Station.Parameters((Polar.Station)null);
            obj.Setups.InitialValues.InstrumentHeight = new DoubleValue(1,1);
            obj.Setups.InitialValues.IsInstrumentHeightKnown = TSU.Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight.Known;
            obj._StationPoint = new Point("test");
            //obj._IsInstrumentHeightKnown = true;
            obj.DefaultMeasure.Face = TSU.Common.Instruments.FaceType.Face2;
            obj.Setups.FinalValues = new Polar.Station.Parameters.Setup.Values() { Version = "test" };
            obj.Setups.FinalValues.VariablePoints = new List<Point>();
            obj.Setups.FinalValues.VariablePoints.Add(new Point() { _Coordinates = new CoordinatesInAllSystems() { Su = new Coordinates("point1", 1, 2, 3) } });
            obj.Setups.FinalValues.VariablePoints.Add(new Point() { _Coordinates = new CoordinatesInAllSystems() { Su = new Coordinates("point2", 2, 3, 4) } });
            obj.Setups.FinalValues.Measures = new List<Polar.Measure>();
            obj.Setups.FinalValues.Measures.Add(new Polar.Measure() { _Name = "point1" });
            obj.Setups.FinalValues.Measures.Add(new Polar.Measure() { _Name = "point2" });

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new TSU.Polar.Station.Parameters((Polar.Station)null);
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj.DefaultMeasure.Face, (obj2 as TSU.Polar.Station.Parameters).DefaultMeasure.Face);
            Assert.AreEqual<Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight>((TSU.Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight)obj.Setups.InitialValues.IsInstrumentHeightKnown, (TSU.Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight)(obj2 as TSU.Polar.Station.Parameters).Setups.InitialValues.IsInstrumentHeightKnown);
            Assert.AreEqual(obj._StationPoint._Name, (obj2 as TSU.Polar.Station.Parameters)._StationPoint._Name);
            Assert.AreEqual(obj._InstrumentHeight.Sigma, (obj2 as TSU.Polar.Station.Parameters)._InstrumentHeight.Sigma);

            Assert.AreEqual(obj.Setups.FinalValues.Version, (obj2 as TSU.Polar.Station.Parameters).Setups.FinalValues.Version);
        }


        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [TestMethod()]
        public void ParametersTestAsStationParameter()
        {
            TSU.Polar.Station.Parameters obj = new TSU.Polar.Station.Parameters((Polar.Station)null);
            obj._Date = DateTime.Now;

            obj._Instrument = new D.AT40x.Instrument() { _Name = "AT401_390858" };
            obj._IsSetup = true;
            obj._Operation = new TSU.Common.Operations.Operation(11,"");
            obj._State = new TSU.Common.Station.State.Closed();
            obj._StationName = "11";
            obj._Team = "test";
            obj.Setups.InitialValues.InstrumentHeight = new DoubleValue(1,1);
            obj.Setups.InitialValues.IsInstrumentHeightKnown = TSU.Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight.Known;
            obj.Setups.FinalValues = new Polar.Station.Parameters.Setup.Values();
            obj.Setups.FinalValues.Version = "test";
            obj._StationName="test";
            obj.DefaultMeasure.Face = TSU.Common.Instruments.FaceType.Face2;

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new TSU.Polar.Station.Parameters((Polar.Station)null);
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj._Instrument._Name, (obj2 as TSU.Polar.Station.Parameters)._Instrument._Name);
            Assert.AreEqual(obj._IsSetup, (obj2 as TSU.Polar.Station.Parameters)._IsSetup);
            Assert.AreEqual(obj._Operation, (obj2 as TSU.Polar.Station.Parameters)._Operation);
            Assert.AreEqual(obj._State.GetType(), (obj2 as TSU.Polar.Station.Parameters)._State.GetType());
            Assert.AreEqual(obj._StationName, (obj2 as TSU.Polar.Station.Parameters)._StationName);
            Assert.AreEqual(obj._InstrumentHeight, (obj2 as TSU.Polar.Station.Parameters)._InstrumentHeight);
            Assert.AreEqual<TSU.Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight>(
                (TSU.Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight)obj.Setups.InitialValues.IsInstrumentHeightKnown, 
                (TSU.Polar.Station.Parameters.Setup.Values.InstrumentTypeHeight)
                (obj2 as TSU.Polar.Station.Parameters).Setups.InitialValues.IsInstrumentHeightKnown);
            Assert.AreEqual(obj.Setups.FinalValues.Version, (obj2 as TSU.Polar.Station.Parameters).Setups.FinalValues.Version);
            Assert.AreEqual(obj._StationName, (obj2 as TSU.Polar.Station.Parameters)._StationName);
            Assert.AreEqual(obj.DefaultMeasure.Face, (obj2 as TSU.Polar.Station.Parameters).DefaultMeasure.Face);
        }

         [TestMethod()]
         public void StationParametersTestAsStationParameter()
         {

            TSU.Common.Station.Parameters obj = new TSU.Common.Station.Parameters(null);
             obj._Date = DateTime.Now;

             obj._Instrument = new D.AT40x.Instrument() { _Name = "AT401_390858" };
             obj._IsSetup = true;
             obj._Operation = P.Preferences.Instance.Operations.Tag as TSU.Common.Operations.Operation;
             obj._State = new TSU.Common.Station.State.Closed();
             obj._StationName = "11";
             obj._Team = "test";

             TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new TSU.Common.Station.Parameters(null);
             TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

             Assert.AreEqual(obj._Instrument._Name, (obj2 as TSU.Common.Station.Parameters)._Instrument._Name);
             Assert.AreEqual(obj._IsSetup, (obj2 as TSU.Common.Station.Parameters)._IsSetup);
             Assert.AreEqual(obj._Operation, (obj2 as TSU.Common.Station.Parameters)._Operation);
             Assert.AreEqual(obj._State.GetType(), (obj2 as TSU.Common.Station.Parameters)._State.GetType());
             Assert.AreEqual(obj._StationName, (obj2 as TSU.Common.Station.Parameters)._StationName);
             Assert.AreEqual(obj._Team, (obj2 as TSU.Common.Station.Parameters)._Team);
         }
    }
}
