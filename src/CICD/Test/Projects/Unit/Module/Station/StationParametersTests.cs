﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Universal.Station.Tests
{
    [TestClass()]
    public class StationParametersTests
    {


        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [TestMethod()]
        public void StationParametersTest()
        {
            TSU.Preferences.Preferences.Instance.InitializeWithAllXmlParametersFiles();
            TSU.Common.Station.Parameters obj = new TSU.Common.Station.Parameters(null);
            obj._Team = "test";

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new TSU.Common.Station.Parameters();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj._Team, ((TSU.Common.Station.Parameters)obj)._Team);
        }


        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [TestMethod()]
        public void StationsTheodoliteParametersTest()
        {
            TSU.Preferences.Preferences.Instance.InitializeWithAllXmlParametersFiles();
            TSU.Polar.Station.Parameters obj = new TSU.Polar.Station.Parameters((TSU.Polar.Station)null);
            obj._Team = "test";
            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new TSU.Polar.Station.Parameters();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual((string)obj._Team, ((TSU.Polar.Station.Parameters)obj)._Team);
        }
    }
}
