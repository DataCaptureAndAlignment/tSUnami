﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Universal.Station.Tests
{
    [TestClass()]
    public class StationModuleTests
    {
        [TestMethod()]
        public void Serial_StationModule_TsunamiTest()
        {

            Module obj = new Module();

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new Module();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj, (obj2 as Module));
        }
    }
}
