﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Preferences;
using TSU;

namespace Universal.Station.Tests
{
    [TestClass()]
    public class StationLevelTests
    {
        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [DeploymentItem(@"libs\SuSoft\", @"libs\SuSoft\")]
        [DeploymentItem(@"libs\Leica\", @"libs\Leica\")]
        //  TSU.Preferences.Preferences.Instance.InitializeWithAllXmlParametersFiles();
        [TestMethod()]
        public void StationLevelTestSerialiazation()
        {
            Preferences.Instance.InitializeWithAllXmlParametersFiles();
            TSU.Level.Station obj = new TSU.Level.Station();
            
            obj._Name = "test";

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new TSU.Level.Station();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj._Name, (obj2 as TSU.Level.Station)._Name);

        }
    }
}
