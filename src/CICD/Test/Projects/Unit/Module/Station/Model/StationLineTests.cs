﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;

using TSU.Common.Measures;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Preferences;


namespace TSU.Common.StationTests
{
    [TestClass()]
    public class StationLineTests
    {
        [TestMethod()]
        public void DeepCopyTestStationLine()
        {
            TSU.Line.Station m = new TSU.Line.Station();
            m._Anchor1._Name = "test";
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, m);
                stream.Position = 0;
                TSU.Line.Station m2 = (TSU.Line.Station)formatter.Deserialize(stream);
                Assert.AreEqual(m._Anchor1._Name, m2._Anchor1._Name);
            }
        }
        [TestMethod()]
        public void DeepCopyTestMeasureOfOffset()
        {
            MeasureOfOffset m = new MeasureOfOffset();
            m._Name = "test";
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, m);
                stream.Position = 0;
                MeasureOfOffset m2 = (MeasureOfOffset)formatter.Deserialize(stream);
                Assert.AreEqual(m._Name, m2._Name);
            }
        }
        [TestMethod()]        
        public void DeepCopyTestCloneableList()
        {
            TSU.CloneableList<MeasureOfOffset> m = new TSU.CloneableList<MeasureOfOffset>();
            m.Add(new MeasureOfOffset(){_Name = "test"});
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, m);
                stream.Position = 0;
                TSU.CloneableList<MeasureOfOffset> m2 = (TSU.CloneableList<MeasureOfOffset>)formatter.Deserialize(stream);
                Assert.AreEqual(m[0]._Name, m2[0]._Name);
            }
        }
        [TestMethod()]
        public void DeepCopyTestMeasureOfOffset2()
        {
            MeasureOfOffset m = new MeasureOfOffset();
            m._Name = "test";
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, m);
                stream.Position = 0;
                MeasureOfOffset m2 = (MeasureOfOffset)formatter.Deserialize(stream);
                Assert.AreEqual(m._Name, m2._Name);
            }
        }
        [TestMethod()]
        public void DeepCopyTestStationTheodolite()
        {
            // TEST_BUG
            // Problem with serialization

            Polar.Station pst = new Polar.Station();
            string name = pst._Name;
            pst.Guid = Guid.NewGuid();

            Common.Station m2;
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, pst);
                stream.Position = 0;
                m2 = (Common.Station)formatter.Deserialize(stream);
                
            }
            Assert.AreEqual(name, m2._Name);
            Assert.AreEqual(pst.Guid, m2.Guid);
        }

        [TestMethod()]
        public void DeepCopyTestStationParameters()
        {
            Common.Station.Parameters m = new Common.Station.Parameters();
            m._Team = "test";
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, m);
                stream.Position = 0;
                Common.Station.Parameters m2 = (Common.Station.Parameters)formatter.Deserialize(stream);
                Assert.AreEqual(m._Team, m2._Team);
            }
        }
    }
}
