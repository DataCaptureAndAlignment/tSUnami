﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;

using TSU.Common.Elements;
using TSU.Common.Measures;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TsunamiTests;
using System.Xml.Serialization;


namespace Universal.Station.Tests
{
    [TestClass()]
    public class CompensationResultsTests
    {
        [TestMethod()]
        public void ReadXmlTest()
        {
            TSU.Polar.Station.Parameters.Setup.Values obj = new TSU.Polar.Station.Parameters.Setup.Values();
            obj.VariablePoints = new  List<Point>();
            obj.VariablePoints.Add(new Point() { _Coordinates = new CoordinatesInAllSystems() { Su = new Coordinates("point1", 1, 2, 3) } });
            obj.VariablePoints.Add(new Point() { _Coordinates = new CoordinatesInAllSystems() { Su = new Coordinates("point2", 2, 3, 4) } });
            obj.Measures = new List<TSU.Polar.Measure>();
            obj.Measures.Add(new TSU.Polar.Measure());
            obj.Measures.Add(new TSU.Polar.Measure());
            object obj2 = new TSU.Polar.Station.Parameters.Setup.Values();
            TSU.Unit_Tests.Serial.TryToSerialize(obj, true);
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);
            Assert.AreEqual(obj.VariablePoints[0]._Coordinates.Su.Z, ((TSU.Polar.Station.Parameters.Setup.Values)obj2).VariablePoints[0]._Coordinates.Su.Z);
            Assert.AreEqual(obj.Measures[1].Distance.Reflector.Constante, ((TSU.Polar.Station.Parameters.Setup.Values)obj2).Measures[1].Distance.Reflector.Constante);
        }

        [TestMethod()]
        public void CompensationResultsSerialisaziton()
        {
            TSU.Polar.Station.Parameters.Setup.Values obj = new TSU.Polar.Station.Parameters.Setup.Values();
            obj.Version = "test";
            obj.Measures = new List<TSU.Polar.Measure>();
            obj.Measures.Add(new TSU.Polar.Measure() { _Point  = new Point("test") });

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new TSU.Polar.Station.Parameters.Setup.Values();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj.Version, ((TSU.Polar.Station.Parameters.Setup.Values)obj2).Version);
            Assert.AreEqual(obj.Measures[0]._PointName, ((TSU.Polar.Station.Parameters.Setup.Values)obj2).Measures[0]._PointName);
        }

        [TestMethod()]
        public void CompensationResultsTest()
        {
            TSU.Polar.Station.Parameters.Setup.Values obj = new TSU.Polar.Station.Parameters.Setup.Values();
            obj.Version = "test";
            obj.VariablePoints = new List<Point>();
            obj.VariablePoints.Add(new Point() { _Coordinates = new CoordinatesInAllSystems() { Su = new Coordinates("point1", 1, 2, 3) } });
            obj.VariablePoints.Add(new Point() { _Coordinates = new CoordinatesInAllSystems() { Su = new Coordinates("point2", 2, 3, 4) } });
            obj.Measures = new List<TSU.Polar.Measure>();
            obj.Measures.Add(new TSU.Polar.Measure() { _Point = new Point("point1")});
            obj.Measures.Add(new TSU.Polar.Measure() { _Point = new Point("point2")});

            object obj2 = new TSU.Polar.Station.Parameters.Setup.Values();
            TSU.Unit_Tests.Serial.TryToSerialize(obj, true);
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj.Version, (obj2 as TSU.Polar.Station.Parameters.Setup.Values).Version);
        }

    }
}
