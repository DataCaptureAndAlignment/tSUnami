﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Unit_Tests;
using TestsTsunami;
using TSU.Preferences;
using TSU;


namespace Universal.Station.Tests
{
    [TestClass()]
    public class StationTests
    {
        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [DeploymentItem(@"libs\SuSoft\", @"libs\SuSoft\")]
        [DeploymentItem(@"libs\Leica\", @"libs\Leica\")]
        [TestMethod()]
        public void StationTheodoliteTest()
        {

            Preferences.Instance.InitializeWithAllXmlParametersFiles();
            TSU.Polar.Station obj = new TSU.Polar.Station();
            obj._Name = "test";
            obj.Id = "test";
            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new TSU.Polar.Station();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj._Name, ((TSU.Polar.Station)obj)._Name);
        }
    }
}
