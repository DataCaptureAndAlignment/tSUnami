﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Tools.Exploration;

namespace TSU.Menu.Tests
{
    [TestClass()]
    public class ExplorationModuleTests
    {
        [TestMethod()]
        public void ExplorationModuleSerialTests()
        {
            Explorator obj = new Explorator(null);

            TSU.Unit_Tests.Serial.TryToSerialize(obj,true); object obj2 = new Explorator();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj, (obj2 as Explorator));
        }
    }
}
