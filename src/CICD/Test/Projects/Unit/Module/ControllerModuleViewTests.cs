﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TSU.Tool;
using TSU.ENUM;
using TSU.Module;
using TSU.Module.Element;
using TSU.Module.Instrument;
using TSU.Module.Measure;
using TSU.Module.Compute;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSUnamiTests;
using System.Xml;

using System.Windows.Forms;

namespace TSU.Tests
{
    [TestClass()]
    public class ControllerModuleViewTests
    {
        [TestMethod()]
        public void CleanNodeTest()
        {
            IElement obj = new CompositeElement();
            obj._Name = "test";
            obj.fileElementType = ENUM.ElementType.File;
            string contents =
                @"TT41      ;QTLD.412100.E                   ;  744.34425; 3003.20310; 4214.49562;2385.11806;385.54483; 2.273; -.01727; -.056608; 98.8033; 1;A;24-AUG-2015;                    ;;
                TT41      ;QTLD.412100.S                   ;  747.14425; 3005.99812; 4214.54816;2384.95965;385.38686; 2.990; -.01727; -.056608; 98.8033; 1;O;24-AUG-2015;                    ;;
                TT41      ;QTLD.412108.E                   ;  747.89725; 3006.74978; 4214.56229;2384.91705;385.34438;  .563; -.01727; -.056608; 98.8033; 1;A;24-AUG-2015;                    ;;
                TT41      ;QTLD.412108.S                   ;  750.69725; 3009.54480; 4214.61484;2384.75863;385.18641; 2.990; -.01727; -.056608; 98.8033; 1;O;24-AUG-2015;                    ;;
                TT41      ;MBG.412115.E                    ;  753.09325; 3011.92347; 4214.67660;2384.39849;384.82666;  .896; -.00033; -.056608; 98.5485; 1;A;24-AUG-2015;                    ;;
                TT41      ;MBG.412115.S                    ;  756.58325; 3015.40697; 4214.75604;2384.20104;384.62977; 6.300; -.00033; -.056608; 98.5485; 1;O;24-AUG-2015;                    ;;
                TT41      ;MBHFD.412133.E                  ;  760.71825; 3019.54045; 4214.85175;2384.07686;384.50626; 2.406; -.01775; -.056608; 98.2561; 1;A;24-AUG-2015;                    ;;
                TT41      ;MBHFD.412133.S                  ;  761.97025; 3020.78998; 4214.88599;2384.00603;384.43564; 1.900; -.01775; -.056608; 98.2561; 1;O;24-AUG-2015;                    ;;
                TT41      ;MBHFD.412141.E                  ;  764.71825; 3023.53251; 4214.96276;2383.85062;384.28068; 2.100; -.01782; -.056588; 98.1807; 1;A;24-AUG-2015;                    ;;
                TT41      ;MBHFD.412141.S                  ;  765.97025; 3024.78199; 4214.99848;2383.77981;384.21008; 1.900; -.01782; -.056588; 98.1807; 1;O;24-AUG-2015;                    ;;
                TCC4      ;NID.8.                          ;99999.00000; 3080.16305; 4213.96310;    .00000;381.57885;      ;        ;         ;        ;24;O;24-AUG-2015;                    ;;
                TCC4      ;NIV.1.                          ;99999.00000; 3082.76349; 4222.18976;    .00000;380.05740;      ;        ;         ;        ; 8;O;24-AUG-2015;                    ;;
                TCC4      ;NID.7.                          ;-9999.00000; 3083.52412; 4214.49502;    .00000;381.42973;      ;        ;         ;        ;24;O;24-AUG-2015;                    ;;
                TCC4      ;NIV.2.                          ;-9999.00000; 3082.80077; 4221.87226;    .00000;380.77771;      ;        ;         ;        ; 8;O;24-AUG-2015;                    ;;
                TCC4      ;NID.6.                          ;-9999.00000; 3089.81978; 4214.66944;    .00000;381.15464;      ;        ;         ;        ;24;O;24-AUG-2015;                    ;;
                TSG40     ;NID.11.                         ; -999.00000; 3047.04773; 4224.58629;    .00000;381.06021;      ;        ;         ;        ;24;O;24-AUG-2015;                    ;;
                TSG40     ;NID.10.                         ; -999.00000; 3047.34201; 4222.02679;    .00000;381.42232;      ;        ;         ;        ;24;O;24-AUG-2015;                    ;;
                TT41      ;GGPSO.227.                      ;  226.63800; 2506.38343; 4335.34018;2400.84128;401.25208;      ;        ;         ;        ;16;O;24-AUG-2015;                    ;;
                TT41      ;GGPSO.258.                      ;  257.93800; 2534.03536; 4320.91719;2400.84154;401.24949;      ;        ;         ;        ;16;O;24-AUG-2015;                    ;;
                TT41      ;GGPSO.289.                      ;  289.23800; 2562.27348; 4307.31452;2400.85553;401.26112;      ;        ;         ;        ;16;O;24-AUG-2015;                    ;;
                TT41      ;GGPSO.321.                      ;  320.53800; 2590.86213; 4294.59315;2400.85575;401.25947;      ;        ;         ;        ;16;O;24-AUG-2015;                    ;;
                TT41      ;NID.745R.                       ;  744.76900; 3003.59911; 4213.51800;2385.13933;385.56583;      ;        ;         ;        ;24;O;24-AUG-2015;                    ;;
                TT41      ;NID.745L.                       ;  744.82000; 3003.57194; 4216.52638;2384.67973;385.10723;      ;        ;         ;        ;24;O;24-AUG-2015;                    ;;";
            string path = TsuPreferences.Instance.FilePaths["Temporary"] + "res.dat";
            System.IO.File.WriteAllText(path, contents);

            obj.AddFromTheoriticalFile(path);

            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(obj.GetType());
            System.Text.StringBuilder sb = new System.Text.StringBuilder();  // StringBuilder to hold the XML
            using (System.Xml.XmlWriter writer = System.Xml.XmlWriter.Create(sb))
            {
                serializer.Serialize(writer, obj); // Serialize the object to XML
                System.Xml.XmlDocument xDoc = new System.Xml.XmlDocument();
                xDoc.LoadXml(sb.ToString());  // Shove it in a XmlDocument
                ModuleView v = new ModuleView(null);
                TreeNode t = v.PopulateNode(xDoc);
                TreeNode clean = v.CleanNode(t);
            }
            Assert.Fail();
        }
    }
}
