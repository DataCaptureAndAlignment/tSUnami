﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Tilt;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Preferences;

namespace TSU.Levelling.Tests
{
    [TestClass()]
    public class StationTiltModuleTests
    {
        [TestMethod()]
        public void StationTiltModuleSerialiazation()
        {
            
            Tilt.Module parent = new Tilt.Module(null, "");
            parent._Name = "papa";
            Tilt.Station.Module obj = new Tilt.Station.Module(parent);
            obj._Name = "test";

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new Tilt.Station.Module();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj._Name, (obj2 as Tilt.Station.Module)._Name);
        }
    }
}
