﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using D = TSU.Common.Instruments.Device;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using P = TSU.Preferences;

namespace TSU.Common.Instruments.Device.Tests
{
    [TestClass()]
    public class At40xTests
    {
        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [TestMethod()]
        public void At40xTest()
        {
            Preferences.Preferences.Instance.InitializeWithAllXmlParametersFiles();
            D.AT40x.Instrument obj = (D.AT40x.Instrument)P.Preferences.Instance.Instruments.Find(x => x.Id == "AT401_390858");
            obj._Name = "test";
            //obj._ToBeMeasuredData = new Management.Measure.MeasureTheodolite();
            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new D.AT40x.Instrument();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj._Name, (obj2 as D.AT40x.Instrument)._Name);
        }
    }
}
