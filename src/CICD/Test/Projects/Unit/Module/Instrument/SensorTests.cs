﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Common.Instruments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace TSU.Common.Instruments.Tests
{
    [TestClass()]
    public class SensorTests
    {
        [TestMethod()]
        public void SensorTest()
        {
            Sensor m = new Sensor();
            m._Name = "test";
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, m);
                stream.Position = 0;
                Sensor m2 = (Sensor)formatter.Deserialize(stream);
                Assert.AreEqual(m._Name, m2._Name);
            } 
        }
    }
}
