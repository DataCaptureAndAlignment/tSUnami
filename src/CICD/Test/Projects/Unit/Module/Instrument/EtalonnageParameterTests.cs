﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Common.Instruments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace TSU.Common.Instruments.Tests
{
    [TestClass()]
    public class EtalonnageParameterTests
    {
       
        [TestMethod()]
        public void SerializeFourrierCoefficients()
        {
            EtalonnageParameter.Theodolite p2 = new EtalonnageParameter.Theodolite();
            p2.Coll_ATR = true;
            System.IO.MemoryStream ms = TSU.Unit_Tests.Serial.TryToSerialize(p2, false);
            object p1 = new EtalonnageParameter.Theodolite();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref p1, ms);
            Assert.AreEqual(p2.Coll_ATR, (p1 as EtalonnageParameter.Theodolite).Coll_ATR);
        }
        
    }
}
