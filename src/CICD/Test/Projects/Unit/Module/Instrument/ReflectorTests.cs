﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Preferences;


namespace TSU.Common.Instruments.Tests
{
    [TestClass()]
    public class ReflectorTests
    {
        [TestMethod()]
        public void ReflectorTest()
        {
            
            Reflector.Reflector obj = new Reflector.Reflector();
            obj._Name = "test";


            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new Reflector.Reflector();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj._Name, (obj2 as Reflector.Reflector)._Name);
        }
    }
}
