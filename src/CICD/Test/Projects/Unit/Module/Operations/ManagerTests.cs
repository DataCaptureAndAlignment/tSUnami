﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Common.Operations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Preferences;

namespace TSU.Common.Operations.Tests
{
    [TestClass()]
    public class ManagerTests
    {
        [TestMethod()]
        public void ManagerTest()
        {
            Manager obj = new Manager();
            
            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new Manager();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj, (obj2 as Manager));
        }
    }
}
