﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Common.Elements;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TSU.Common.Elements.Tests
{
    [TestClass()]
    public class CoordinatesSystemsTests
    {
        [TestMethod()]
        public void CloneTest_CoordinateSystems()
        {
            CoordinatesInAllSystems a = new CoordinatesInAllSystems();
            a.Ccs.Z = new DoubleValue();
            a.Ccs.Z.Sigma = 1;
            CoordinatesInAllSystems b = a.Clone() as CoordinatesInAllSystems;
            Assert.AreEqual(a.Ccs.Z.Sigma, b.Ccs.Z.Sigma);
            b.Ccs.Z.Sigma = 2;
            Assert.AreNotEqual(a.Ccs.Z.Sigma, b.Ccs.Z.Sigma);
        }

    }
}
