﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Common.Elements;
using Microsoft.VisualStudio.TestTools.UnitTesting;
//using shapes;
//using shapesNET;

namespace TSU.Common.Elements.Tests
{
    [TestClass()]
    public class PointTests
    {
        [TestMethod()]
        public void PointTests_CheckIfCoordinatesAreKnownTest()
        {
            Point p = new Point();
            Assert.IsTrue(p._Coordinates.Ccs.AreKnown == false);
            p._Coordinates.Ccs.X.Value = 1;
            Assert.IsTrue(p._Coordinates.Ccs.AreKnown == false);
            p._Coordinates.Ccs.Y.Value = 1;
            p._Coordinates.Ccs.Z.Value = 1;
            Assert.IsTrue(p._Coordinates.Ccs.AreKnown == true);
        }


        [TestMethod()]
        public void CloneTest_Point_name()
        {
            Point p = new Point() { _Name = "test" };
            Point p2 = p.Clone() as Point;
            Assert.AreEqual(p._Name, p2._Name);
        }

        [TestMethod()]
        public void CloneTest_Point_coord()
        {
            Point p = new Point() { _Name = "test" };
            p._Coordinates.Ccs.Z = new DoubleValue();
            p._Coordinates.Ccs.Z.Sigma = 2;
            Point p2 = p.Clone() as Point;
            p2._Coordinates.Ccs.Z.Sigma = 3;

            Assert.AreNotEqual(p._Coordinates.Ccs.Z.Sigma, p2._Coordinates.Ccs.Z.Sigma);
        }

        [TestMethod()]
        public void CloneTest_Point_coordWithDeepCopy()
        {
            Point p = new Point() { _Name = "test" };
            p._Coordinates.Ccs.Z.Sigma = 2;
            Point p2 = p.DeepCopy();
            p2._Coordinates.Ccs.Z.Sigma = 3;
            Assert.AreNotEqual(p._Coordinates.Ccs.Z.Sigma, p2._Coordinates.Ccs.Z.Sigma);
        }

        [TestMethod()]
        public void Point_DeepCopyTest()
        {
            Point p = new Point() { _Name = "test" };
            Point p2 = p.DeepCopy() as Point;
            Assert.AreEqual(p._Name, p2._Name);
        }


        [TestMethod()]
        public void SetCoordinatesTest()
        {
            Point p = new Point() { _Name = "test" };
            p._Coordinates.Ccs = new Coordinates("Unknwon", 1, 2, 3);
            Coordinates c = new Coordinates("Unknwon", 4, 5, 6);

            p.SetCoordinates(c, Coordinates.ReferenceFrames.CernXYHe, Coordinates.CoordinateSystemsTypes._2DPlusH);

            Point p2 = p.DeepCopy() as Point;
            Assert.AreEqual(p._Name, p2._Name);
        }

        [TestMethod()]
        public void shapeNet_1()
        {
            // TEST_EMPTY

            try
            {
                //CHPoint3 P = new CHPoint3(); 
                // P.Set(0, 1);
                //CHPoint3 Q = new CHPoint3(); Q.Set(0, 2);
                //CHPoint3 R = new CHPoint3(); R.Set(0, 3);

                //CLine3 l = new CLine3(); l.Set(0, 4);
                //CLine3 m = new CLine3(); m.Set(0, 4);

                //CHLine3 L = new CHLine3(l);
                //CHLine3 M = new CHLine3(m);

                //CHPlane3 A = new CHPlane3(); A.Set(0, 5);
                //CHPlane3 B = new CHPlane3(); B.Set(0, 5);



                //double d = shapesNET.CBinaries.Distance(P, Q);
            }
            catch (Exception ex)
            {
                throw new Exception("Problem in Shape.NEt " + ex.Message, ex);
            }
        }

        [TestMethod()]
        public void ToStringTest()
        {
            Point p = new Point() { _Name = "LHC.Class.Numero.Point", _Coordinates = new CoordinatesInAllSystems() {  Ccs = new Coordinates("p",1,2,3) , Su = new Coordinates("p", 4, 5, 6) } };
            string result = p.ToString("Ls");
        }
    }
}
