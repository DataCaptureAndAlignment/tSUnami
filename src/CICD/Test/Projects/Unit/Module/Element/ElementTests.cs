﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Common.Elements;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace TSU.Common.Elements.Tests
{
    [TestClass()]
    public class ElementTests
    {
        [TestMethod()]
        public void ElementTest()
        {
            // // TEST_BUG
            // Assertion failed - Class, Zone, Accelerator are different

            Element e = new Point("a.b.c.d.e.f");

            Assert.AreEqual((e as Point)._Name, "a.b.c.d.e.f");
            Assert.AreEqual((e as Point)._Point, "f");
            Assert.AreEqual((e as Point)._Numero, "e");
            Assert.AreEqual((e as Point)._Class, "c.d");
            Assert.AreEqual((e as Point)._Zone, "a");
            Assert.AreEqual((e as Point)._Accelerator, "a.b");
            Assert.AreEqual((e as Point)._Origin, "Unknown");

            e = new Point("d.e.f");

            Assert.AreEqual(e._Name, "d.e.f");
            Assert.AreEqual((e as Point)._Point, "d.e.f");
            Assert.AreEqual((e as Point)._Numero, "");
            Assert.AreEqual((e as Point)._Class, "");
            Assert.AreEqual((e as Point)._Zone, null);
            Assert.AreEqual((e as Point)._Accelerator, "Unknown");
            Assert.AreEqual((e as Point)._Origin, "Unknown");

            e = new Point("f");

            Assert.AreEqual(e._Name, "f");
            Assert.AreEqual((e as Point)._Point, "f");
            Assert.AreEqual((e as Point)._Numero, "Unknown");
            Assert.AreEqual((e as Point)._Class, "Unknown");
            Assert.AreEqual((e as Point)._Zone, null);
            Assert.AreEqual((e as Point)._Accelerator, "Unknown");
            Assert.AreEqual(e._Origin, "Unknown");

        }
    }
}

namespace TsunamiTests.MODULE.Element
{
    class ElementTests
    {
    }
}
