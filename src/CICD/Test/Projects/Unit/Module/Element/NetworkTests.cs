﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using EC = TSU.Common.Elements.Composites;
using TSU.Common.Zones;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Compute.Transformation;
using TSU.Common.Compute;

namespace TSU.Common.Elements.Tests
{
    [TestClass()]
    public class NetworkTests
    {
        [TestMethod()]
        public void NetworkTest()
        {
            EC.TheoreticalElement n = new EC.TheoreticalElement();
            n._Name = "LHCb";
            n.fileElementType = ENUM.ElementType.TheoreticalFile;
            n.networkFilePath=@"c:\";
            n.Definition.InitialCoordinateSystems = Coordinates.CoordinateSystemsTypes._2DPlusH;
            n.Definition.InitialReferenceFrames = Coordinates.ReferenceFrames.CernXYHg85Machine;
            n.Definition.LocalZone = new Zone(
                n._Name, 
                new Coordinates("Unknwon",4472.525917, 5005.972899, 2330.049057), 
                new DoubleValue(224.734577,0.0), 
                new DoubleValue(0.00417001,0.0),
                new Rotation3D(-1.570796327,-0.0036016,1.570796327));
            n.Comments = @"
                    Express yourself here
";
            n.Definition.LocalZone.Su2PhysInGon = new Rotation3D(-1.570796327, -0.0036016, 1.570796327);
            n.Definition.LocalZone.Su2PhysInGon.SumToAvoidUnawareModification = CheckSum.Get(n.Definition.LocalZone.Su2PhysInGon);
            n.PointInIdXYZFormat = @"
                    P1     0.000000     9.346240     0.452600
                    P2     0.654290    -6.037900    -0.526050
                    P3    -0.801430    -2.995800     0.732500
                 ST_01     0.000000     0.000000     0.000000
";

            TSU.Unit_Tests.Serial.TryToSerialize(n, true);

            object o = new EC.TheoreticalElement();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref o);
        }

        
    }
}
