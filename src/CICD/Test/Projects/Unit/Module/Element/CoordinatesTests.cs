﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Common.Elements;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU;
namespace TSU.Common.Elements.Tests
{
    [TestClass()]
    public class CoordinatesTests
    {
      

        [TestMethod()]
        public void ReadXmlTest_coordinates()
        {
            Coordinates obj = new Coordinates("Unknwon", 1, 2, 3);

            object obj2 = new Coordinates();

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true);
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj.X.Value, ((Coordinates)obj2).X.Value);
        }

        [TestMethod()]
        public void EqualsTest_coordinates()
        {
            Coordinates c1 = new Coordinates("Unknwon", 1, 2, 3);
            Coordinates c2 = new Coordinates("Unknwon", 2, 3, 4);
            Coordinates c3 = new Coordinates("Unknwon", 1, 2, 3);

            Assert.AreNotEqual(c1, c2);
            Assert.IsTrue(c1.Equals(c3));
            Assert.AreEqual(c1, c3);
        }

        [TestMethod()]
        public void CloneTest_Coordinates()
        {
            Coordinates c1 = new Coordinates("Unknwon", 1, 2, 3);
            Coordinates c2 = c1.Clone() as Coordinates;
            Assert.AreEqual(c1.X.Value, c2.X.Value);
            c2.X.Value = 2;
            Assert.AreNotEqual(c1.X.Value, c2.X.Value);
            c2.SystemName = "test";
            Assert.AreNotEqual(c1.SystemName, c2.SystemName);
            c2.ReferenceFrame = Coordinates.ReferenceFrames.CernXYHg1985;
            Assert.AreNotEqual(c1.ReferenceFrame, c2.ReferenceFrame);
        }


        [TestMethod()]
        public void Coordinates_PackageOf3Digits()
        {
            // TEST_BUG
            // Assertion failed 

            Coordinates c1 = new Coordinates("Unknwon", 1.123456, 2.123456, 3.123456);
            string s = c1.ToString();
            Assert.AreEqual("             Unknown: X = 1.12346 m, Y = 2.12346 m, Z = 3.12346 m.", s);
        }
    }
}
