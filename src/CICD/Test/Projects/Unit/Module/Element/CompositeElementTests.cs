﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Common.Elements;
using EC = TSU.Common.Elements.Composites;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TSU.Common.Elements.Tests
{
    [TestClass()]
    public class CompositeElementTests
    {
        [TestMethod()]
        public void CompositeElement_serializationnTest()
        {
            EC.CompositeElement obj = new EC.CompositeElement();
            EC.CompositeElement subObj = new EC.CompositeElement("sub");
            subObj.Elements.Add(new Point("test"));
            obj.Add(subObj);
            object obj2 = new EC.CompositeElement();
            TSU.Unit_Tests.Serial.TryToSerialize(obj, true);
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);
            Assert.AreEqual(obj.Elements[0]._Name, (obj2 as EC.CompositeElement).Elements[0]._Name);
        }

        [TestMethod()]
        public void GetElementOfKeyTest()
        {
            EC.CompositeElement obj = new EC.CompositeElement();
            obj.Add(new Point("test"));
            obj.Add(new Point("test"));
            
            Assert.AreEqual(obj.Elements[1]._Name, "test(1)");
        }

    }
}
