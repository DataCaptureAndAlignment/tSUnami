﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using TSU;
using TSU.Common.Compute.Compensations;
using TSU.Tools.Conversions;
using E = TSU.Common.Elements;


namespace TestsTSUnami.Measurement.Common.Compute.Compensations
{
    [TestClass]
    public class JSONParser
    {
        #region Frame
        [TestMethod()]
        public void GetFrameParameterTest()
        {
            return; // todo
            string jsonFileName = @"Samples\lgc2\frame_test.json";
            var json = Lgc2.Output.ParserJSON.ReadJSONFile(jsonFileName);
            string frameName = "RSTI_103035_LHC.MQML.5L5";
            var expected = new List<DoubleValue>();
            expected.AddRange(new[] { new DoubleValue(-1231.090090, Tsunami2.Preferences.Values.na), // in M
                                      new DoubleValue(10389.569100, Tsunami2.Preferences.Values.na), // in M
                                      new DoubleValue(2416.394520, Tsunami2.Preferences.Values.na), // in M
                                      new DoubleValue(0.000000 , Tsunami2.Preferences.Values.na), // in GON
                                      new DoubleValue(0.000000, Tsunami2.Preferences.Values.na), // in GON
                                      new DoubleValue(74.733794, Tsunami2.Preferences.Values.na), // in GON
                                      new DoubleValue(1.000000, Tsunami2.Preferences.Values.na) // no unit
                               });

            var results = Lgc2.Output.ParserJSON.GetFrameParameter(json, frameName);
            Assert.AreEqual(expected[0].Value, results[0].Value, 0.00001);
            Assert.AreEqual(expected[1].Value, results[1].Value, 0.00001);
            Assert.AreEqual(expected[2].Value, results[2].Value, 0.00001);
            Assert.AreEqual(expected[3].Value, results[3].Value, 0.00001);
            Assert.AreEqual(expected[4].Value, results[4].Value, 0.00001);
            Assert.AreEqual(expected[5].Value, results[5].Value, 0.00001);
            Assert.AreEqual(expected[6].Value, results[6].Value, 0.00001);
        }


        [TestMethod()]
        public void GetFrameFixedParameterTest()
        {
            return; // todo
            string jsonFileName = @"samples\lgc2\frame_test.json";
            var json = Lgc2.Output.ParserJSON.ReadJSONFile(jsonFileName);
            string frameName = "RSTI_103035_LHC.MQML.5L5";

            var expected = new List<bool>();
            expected.AddRange(new[] { true,
                                      true,
                                      true,
                                      true,
                                      true,
                                      true,
                                      true
                               });

            var results = Lgc2.Output.ParserJSON.GetFrameFixedParameter(json, frameName);
            CollectionAssert.AreEqual(expected, results);


        }

        [TestMethod()]
        public void GetFrameOBSXYZTest()
        {
            return; // todo
            string jsonFileName = @"Samples\lgc2\frame_test.json";
            var json = Lgc2.Output.ParserJSON.ReadJSONFile(jsonFileName);
            string frameName = "RSTR_103035_LHC.MQML.5L5";

            List<(string, DoubleValue, DoubleValue, DoubleValue)> expected = new List<(string, DoubleValue, DoubleValue, DoubleValue)>();
            expected.AddRange(new[] { ("LHC.MQML.5L5.E", new DoubleValue(-0.000, 100), new DoubleValue(0.000003, 100), new DoubleValue(-0.000, 100)), // in M
                                      ("LHC.MQML.5L5.S", new DoubleValue(0.000, 100), new DoubleValue(-0.000003, 100), new DoubleValue(0.000, 100)), // in M 
                               });

            var results = Lgc2.Output.ParserJSON.GetFrameOBSXYZ(json, frameName);

            for (int i = 0; i < results.Count; i++)
            {
                Assert.AreEqual(Numbers.ToDouble(results[i].ToString()), Numbers.ToDouble(expected[i].ToString()), 0.1);
            }

        }

        [TestMethod()]
        public void GetFrameInclyTest()
        {
            return; // todo
            string jsonFileName = @"Samples\lgc2\frame_test.json";
            var json = Lgc2.Output.ParserJSON.ReadJSONFile(jsonFileName);
            string frameName = "RSTR_103035_LHC.MQML.5L5";

            string expectedName = "LHC.MQML.5L5.E";
            DoubleValue expectedIncly = new DoubleValue(399.466513, 0.00); // In GON

            var results = Lgc2.Output.ParserJSON.GetFrameIncly(json, frameName);

            Assert.AreEqual(expectedName, results[0].Item1);
            Assert.AreEqual(expectedIncly.Value, results[0].Item2.Value, 0.1);
            Assert.AreEqual(expectedIncly.Sigma, results[0].Item2.Sigma, 0.1);
        }




        #endregion
        #region Wire Test
        [TestMethod()]
        public void WireDisplacementTest()
        {
            return; // todo
            string jsonFileName = @"Samples\lgc2\wire_test.json";
            string resFileName = @"Samples\lgc2\wire_test.res";

            var json = Lgc2.Output.ParserJSON.ReadJSONFile(jsonFileName);
            List<string> lines = File.ReadLines(resFileName).ToList();

            var pointsResults = Lgc2.Output.ParserJSON.WireDisplacement(json);
            var sortedResults = new SortedDictionary<string, double>(pointsResults);
            //var pointsExpected = Lgc2.Output.Parser.WireDisplacement(ref lines);
            //ar sortedExpected = new SortedDictionary<string, double>(pointsExpected);
            //CollectionAssert.AreEqual(sortedExpected, sortedResults);


        }

        #endregion
        #region Levelling Test
        [TestMethod()]
        public void LevelResultTest()
        {
            string jsonFileName = @"Samples\lgc2\level_test.json";
            string resFileName = @"Samples\lgc2\level_test.res";

            var json = Lgc2.Output.ParserJSON.ReadJSONFile(jsonFileName);
            List<string> lines = File.ReadLines(resFileName).ToList();

            Dictionary<string, double> pointsResults = Lgc2.Output.ParserJSON.LevelResult(json);
            Dictionary<string, double> pointsExpected = Lgc2.Output.Parser.LevelResults(ref lines);
            var sortedResults = new SortedDictionary<string, double>(pointsResults);
            var sortedExpected = new SortedDictionary<string, double>(pointsExpected);
            CollectionAssert.AreEqual(sortedExpected, sortedResults);
        }
        #endregion

        #region Polar Module Test
        [TestMethod()]
        public void VariablesTest()
        {
            return; // todo

            string jsonFileName = @"Samples\lgc2\polar_test.json";
            string resFileName = @"Samples\lgc2\polar_test.res";

            var json = Lgc2.Output.ParserJSON.ReadJSONFile(jsonFileName);
            List<string> lines = File.ReadLines(resFileName).ToList();
            List<string> temps = null;

            List<E.Point> variablesResults = Lgc2.Output.ParserJSON.VariablePoints(json);
            List<E.Point> variablesExpected = Lgc2.Output.Parser.VariablePoints(ref lines, ref temps, E.Coordinates.CoordinateSystemsTypes.Geodetic);

            for (int i = 0; i < variablesResults.Count; i++)
            {
                Assert.AreEqual(variablesExpected[i]._Name, variablesResults[i]._Name);
                Assert.AreEqual(variablesExpected[i]._Coordinates.Ccs.X.Value, variablesResults[i]._Coordinates.Ccs.X.Value, 0.1);
                Assert.AreEqual(variablesExpected[i]._Coordinates.Ccs.Y.Value, variablesResults[i]._Coordinates.Ccs.Y.Value, 0.1);
                Assert.AreEqual(variablesExpected[i]._Coordinates.Ccs.Z.Value, variablesResults[i]._Coordinates.Ccs.Z.Value, 0.1);
                Assert.AreEqual(variablesExpected[i]._Coordinates.Ccs.X.Sigma, variablesResults[i]._Coordinates.Ccs.X.Sigma, 0.1);
                Assert.AreEqual(variablesExpected[i]._Coordinates.Ccs.Y.Sigma, variablesResults[i]._Coordinates.Ccs.Y.Sigma, 0.1);
                Assert.AreEqual(variablesExpected[i]._Coordinates.Ccs.Z.Sigma, variablesResults[i]._Coordinates.Ccs.Z.Sigma, 0.1);
            }
        }

        public void CreateInputForPolarTest(string path)
        {
            var stm = new TSU.Polar.Station.Module(null);
            var freeStationStragegy = new TSU.Polar.Compensations.FreeStation_LGC2(stm);

            var polarStaton = new TSU.Polar.Station(stm);
            polarStaton.Parameters2._Instrument = new TSU.Common.Instruments.Device.Manual.Theodolite.Instrument() { _Name = "testi" };
            var lgc2 = new TSU.Common.Compute.Compensations.Lgc2(polarStaton, freeStationStragegy, path, "test");
            freeStationStragegy.Create_LGC2_Input(polarStaton, path, lgc2, "test");

        }


        [DeploymentItem(@"Samples\", @"Samples\")]
        [TestMethod()]
        public void AHAVDTest()
        {
            return; // todo
            //CreateInputForPolarTest(@"samples\lgc2\polar_test");

            //Read JSON
            string jsonFileName = @"samples\lgc2\polar_test.json";
            var json = Lgc2.Output.ParserJSON.ReadJSONFile(jsonFileName);

            //Read RES, without blank lines
            string resFileName = @"samples\lgc2\polar_test.res";
            List<string> lines = new Lgc2.Output(resFileName)._TextOutput;
            List<string> temps = null;
            string ANGLES_VERTICAUX = "DISTANCES ZENITHALES";
            string DISTANCES_MEASURED = "DISTANCES MESUREES";

            // Skip the header
            lines = lines.SkipWhile(s => !s.Contains("*** MESURES ***")).ToList();

            // AH tests
            List<TSU.Polar.Measure> anglesResults = Lgc2.Output.ParserJSON.AH(json);
            List<TSU.Polar.Measure> anglesExpected = Lgc2.Output.Parser.AH(ref lines, ref temps, ANGLES_VERTICAUX);

            //Assert.AreEqual(anglesExpected.Count, anglesResults.Count);
            //CollectionAssert.AreEqual(anglesResults, angleExpected);

            for (int i = 0; i < anglesResults.Count; i++)
            {
                Assert.AreEqual(anglesExpected[i]._Name, anglesResults[i]._Name);
                Assert.AreEqual(anglesExpected[i].Angles.raw.Horizontal.Value, anglesResults[i].Angles.raw.Horizontal.Value, 0.01);
                Assert.AreEqual(anglesExpected[i].Angles.corrected.Horizontal.Value, anglesResults[i].Angles.corrected.Horizontal.Value, 0.01);
                Assert.AreEqual(anglesExpected[i].Angles.corrected.Horizontal.Sigma, anglesResults[i].Angles.corrected.Horizontal.Sigma, 0.01);
            }



            // AV tests
            Lgc2.Output.ParserJSON.AV(json, anglesResults);
            Lgc2.Output.Parser.AV(ref lines, ref temps, DISTANCES_MEASURED, ref anglesExpected, false);

            for (int i = 0; i < anglesResults.Count; i++)
            {
                Assert.AreEqual(anglesExpected[i]._Name, anglesResults[i]._Name);
                Assert.AreEqual(anglesExpected[i].Angles.raw.Vertical.Value, anglesResults[i].Angles.raw.Vertical.Value, 0.1);
                Assert.AreEqual(anglesExpected[i].Angles.raw.Vertical.Sigma, anglesResults[i].Angles.raw.Vertical.Sigma, 0.01);
                Assert.AreEqual(anglesExpected[i].Angles.corrected.Vertical.Value, anglesResults[i].Angles.corrected.Vertical.Value, 0.01);
                Assert.AreEqual(anglesExpected[i].Angles.corrected.Vertical.Sigma, anglesResults[i].Angles.corrected.Vertical.Sigma, 0.01);
            }

            // D Tests
            Lgc2.Output.ParserJSON.D(json, anglesResults);
            Lgc2.Output.Parser.D(ref lines, ref temps, ref anglesExpected, false);

            for (int i = 0; i < anglesResults.Count; i++)
            {
                Assert.AreEqual(anglesExpected[i]._Name, anglesResults[i]._Name);
                Assert.AreEqual(anglesExpected[i].Distance.Raw.Value, anglesResults[i].Distance.Raw.Value, 0.1);
                Assert.AreEqual(anglesExpected[i].Distance.Raw.Sigma, anglesResults[i].Distance.Raw.Sigma, 0.01);
                Assert.AreEqual(anglesExpected[i].Distance.Corrected.Value, anglesResults[i].Distance.Corrected.Value, 0.01);
                Assert.AreEqual(anglesExpected[i].Distance.Corrected.Sigma, anglesResults[i].Distance.Corrected.Sigma, 0.01);
                Assert.AreEqual(anglesExpected[i].Distance.Reflector.Constante, anglesResults[i].Distance.Reflector.Constante, 0.01);
                Assert.AreEqual(anglesExpected[i].Distance.Reflector._Name, anglesResults[i].Distance.Reflector._Name);
                Assert.AreEqual(anglesExpected[i].Extension.Value, anglesResults[i].Extension.Value, 0.01);
            }

            Console.WriteLine(anglesResults[0].Distance.Raw.Sigma);
            Console.WriteLine(anglesExpected[0].Distance.Raw.Sigma);
            Console.WriteLine(anglesResults[1].Distance.Raw.Sigma);
            Console.WriteLine(anglesExpected[1].Distance.Raw.Sigma);


        }

        [TestMethod()]
        public void HeaderTest()
        {
            return; // todo
            string jsonFileName = @"Samples\lgc2\polar_test.json";
            string resFileName = @"Samples\lgc2\polar_test.res"; ;

            var json = Lgc2.Output.ParserJSON.ReadJSONFile(jsonFileName);
            List<string> lines = File.ReadLines(resFileName).ToList();
            List<string> temps = null;

            string versionExpected = Lgc2.Output.Parser.Header(ref lines, ref temps);
            string versionResults = Lgc2.Output.ParserJSON.Header(json);

            Assert.AreEqual(versionExpected, versionResults);

        }

        [TestMethod()]
        public void Sigma0Test()
        {
            return; // todo
            string jsonFileName = @"Samples\lgc2\polar_test.json";
            string resFileName = @"Samples\lgc2\polar_test.res";

            var json = Lgc2.Output.ParserJSON.ReadJSONFile(jsonFileName);
            List<string> lines = File.ReadLines(resFileName).ToList();

            double sigmaExpected = Lgc2.Output.Parser.Sigma0(ref lines);
            double sigmaResults = Lgc2.Output.ParserJSON.Sigma0(json);

            Assert.AreEqual(sigmaExpected, sigmaResults, 0.0001);
        }

        [TestMethod()]
        public void InstrumentPropertiesTest()
        {
            return; // todo
            string jsonFileName = @"Samples\lgc2\polar_test.json";
            string resFileName = @"Samples\lgc2\polar_test.res";

            var json = Lgc2.Output.ParserJSON.ReadJSONFile(jsonFileName);
            List<string> lines = File.ReadLines(resFileName).ToList();
            List<string> temps = null;
            string ANGLES_HORIZONTAUX = "ANGLES HORIZONTAUX";


            (double hValueResults, double hSigmaResults) = Lgc2.Output.ParserJSON.InstrumentHeight(json);
            (double v0ValueResults, double v0SigmaResults) = Lgc2.Output.ParserJSON.InstrumentV0(json);

            (double hValueExpected, double hSigmaExpected, double v0ValueExpected, double v0SigmaExpected) =
                                                                                Lgc2.Output.Parser.InstrumentProperties(ref lines, ref temps, ANGLES_HORIZONTAUX);

            Assert.AreEqual(hValueExpected, hValueResults, 0.1);
            Assert.AreEqual(hSigmaExpected, hSigmaResults, 0.1);
            Console.WriteLine(v0SigmaExpected);
            Console.WriteLine(v0SigmaResults);
            Assert.AreEqual(v0ValueExpected, v0ValueResults, 0.1);
            Assert.AreEqual(v0SigmaExpected, v0SigmaResults, 0.1);
        }

        #endregion

        [TestMethod()]
        public void ValidJsonReading()
        {
            return; // todo
            string fileName = @"Samples\lgc2\polar_test.json";
            JsonElement jsonFile = Lgc2.Output.ParserJSON.ReadJSONFile(fileName);
        }

        [TestMethod()]
        public void FileExist()
        {
            return; // todo
            string fileName = @"Samples\lgc2\polar_test.json";
            Assert.IsTrue(
                File.Exists(fileName),
                "Deployment failed: {0} did not get deployed.",
                fileName);
        }
    }
}
