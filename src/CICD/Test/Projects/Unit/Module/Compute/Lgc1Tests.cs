﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Tools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Unit_Tests;
using TSU.Preferences;
using TSU.Common.Elements;
using T= TSU.AdvancedModules.Theodolite;

namespace TSU.Tools.Tests
{
    [TestClass()]
    public class Lgc1Tests
    {
        //[TestMethod()]
        //public void D.Lgc1GetOutputResultsTest()
        //{
        //    StationTheodolite station = new FilledMockTheodoliteStation().Object;

        //    D.Lgc1 D.Lgc1 = new D.Lgc1(station, new Common.Compute.Compensations.Strategies.List.);
        //    D.Lgc1.Run();

        //    Coordinates coord = new Coordinates();
        //    double v0 = TsunamiPreferences.Values.na;
        //    D.Lgc1.GetOutputResults(false, ref coord, ref v0);
        //    Assert.AreEqual(1.40445, coord.X.Value);
        //}
        public void Lgc1_CreateAndRun_Test()
        {
            Polar.Station station = new TestsTsunami.FilledMockTheodoliteStation().Object;
            Polar.Station.Parameters param = station.Parameters2 as Polar.Station.Parameters;
            //LGC.Input.CreateAllAndrun(station);
            Assert.AreEqual(1.40445, param._StationPoint._Coordinates.Local.X.Value);
        }
    }
}
