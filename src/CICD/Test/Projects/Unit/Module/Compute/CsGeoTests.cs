﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TestsTsunami;
using TSU.Common.Elements;
using TSU.Common.Zones;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Compute.Transformation;

namespace TSU.Tools.Tests
{
    [TestClass()]
    public class CsGeoTests
    {
        //[TestMethod()]
        //public void CsGeo_Test_MLA1985_2_XYH()
        //{
        //    string content =
        //@"PJ01               -8.8109000         2.0567200        -1.0946900        
        //PJ02               25.1654400         7.3745100        -2.3511900       
        //PJ03               25.2168500         4.5166700        -1.4226400       
        //PJ04               -5.5777100         6.9295200        -2.1656600       
        //PJ05               38.0604500         3.0437400        -0.9398100";
        //    List<Point> points = Mocks.MockPoints(content, Coordinates.ReferenceFrames.MLA1985Machine);

        //    // "ORIGIN     IP8_2014_SU     4472.525917     5005.972899     2330.049057     324.734623     0.00017239"
        //   Zone z = new Zone("lhsb",
        //        new Coordinates("Unknwon", 4472.525917, 5005.972899, 2330.049057), new DoubleValue(324.734623, 0), new DoubleValue(0.00017239, 0), null);
        //    TSU.IO.SUSoft.CsGeo.XYH2MLAorOpposite(
        //        ref points,
        //        Coordinates.ReferenceFrames.MLA1985Machine, z);

        //    Assert.AreEqual(points[0]._Coordinates.Ccs.X.Value, 4467.28426, 0.00001);
        //    Assert.AreEqual(points[1]._Coordinates.Ccs.Z.Value, 328.83060, 0.00001);

        //}

        //[TestMethod()]
        //public void CsGeo_Test_XYH_2_MLA1985()
        //{
        //    string content =
        //@"PJ01               4467.28426                 4998.59735                  330.08705  
        //  PJ02               4475.23356                 5032.05527                  328.83060  
        //  PJ03               4477.89822                 5031.02064                  329.75914  
        //  PJ04               4463.99907                 5003.43502                  329.01608  
        //  PJ05               4484.12708                 5042.34920                  330.24204  ";
        //    List<Point> points = Mocks.MockPoints(content,Coordinates.ReferenceFrames.CernXYHg85Machine);

        //    // "ORIGIN     IP8_2014_SU     4472.525917     5005.972899     2330.049057     324.734623     0.00017239"
        //    Zone z = new Zone("lhsb",
        //        new Coordinates("Unknwon", 4472.525917, 5005.972899, 2330.049057), new DoubleValue(324.734623, 0), new DoubleValue(0.00017239, 0), null);
        //    TSU.IO.SUSoft.CsGeo.XYH2MLAorOpposite(
        //        ref points,
        //        Coordinates.ReferenceFrames.CernXYHg85Machine, z);

        //    Assert.AreEqual(points[0]._Coordinates.Local.X.Value, -8.8109000, 0.00001);
        //    Assert.AreEqual(points[1]._Coordinates.Local.Z.Value, -2.3511900, 0.00001);
        //}

        //[TestMethod()]
        //public void CsGeo_Test_XYH_2_SU_Direct()
        //{
        //    // 
        //    string content =
        //@"PJ01               4467.28426                 4998.59735                  330.08705  
        //  PJ02               4475.23356                 5032.05527                  328.83060  
        //  PJ03               4477.89822                 5031.02064                  329.75914  
        //  PJ04               4463.99907                 5003.43502                  329.01608  
        //  PJ05               4484.12708                 5042.34920                  330.24204  ";
        //    List<Point> points = Mocks.MockPoints(content, Coordinates.ReferenceFrames.CernXYHg85Machine);

        //    // "ORIGIN     IP8_2014_SU     4472.525917     5005.972899     2330.049057     224.734577    0.00417001"
        //    Zone z = new Zone("lhsb",
        //        new Coordinates("Unknwon", 4472.525917, 5005.972899, 2330.049057), new DoubleValue(224.734577, 0), new DoubleValue(0.00417001, 0), null);
        //    TSU.IO.SUSoft.CsGeo.XYH2MLAorOpposite(
        //        ref points,
        //        Coordinates.ReferenceFrames.CernXYHg85Machine, z);

        //    Assert.AreEqual(points[0]._Coordinates.Local.X.Value, 2.05672, 0.00001);
        //    Assert.AreEqual(points[1]._Coordinates.Local.Z.Value, -2.3511900, 0.00001);
        //}


        //[TestMethod()]
        //public void CsGeo_Test_SU_2_XYH()
        //{
            
        //    string content =
        //@"PJ01                  2.05672                    8.81090                   -1.09469  
        //  PJ02                  7.37451                  -25.16544                   -2.35119  
        //  PJ03                  4.51667                  -25.21685                   -1.42264  
        //  PJ04                  6.92952                    5.57771                   -2.16566  
        //  PJ05                  3.04374                  -38.06045                   -0.93981";
        //    List<Point> points = Mocks.MockPoints(content,Coordinates.ReferenceFrames.MLA1985Machine);

        //    // "ORIGIN     IP8_2014_SU     4472.525917     5005.972899     2330.049057     224.734577    0.00417001"
        //    Zone z = new Zone("lhsb",
        //        new Coordinates("Unknwon", 4472.525917, 5005.972899, 2330.049057), new DoubleValue(224.734577, 0), new DoubleValue(0.00417001, 0), null);
        //    TSU.IO.SUSoft.CsGeo.XYH2MLAorOpposite(
        //        ref points,
        //       Coordinates.ReferenceFrames.MLA1985Machine, z);

        //    Assert.AreEqual(points[0]._Coordinates.Ccs.X.Value, 4467.28426, 0.00001);
        //    Assert.AreEqual(points[1]._Coordinates.Ccs.Z.Value, 328.83060, 0.00001);
        //}


//        [TestMethod()]
//        public void CsGeo_Test_SU_2_XYH_2()
//        {
            
//            string content = @"
//STL.003	0.00094	-0.00676	8e-05
//Point.001	-1e-05	1.44118	0.22744
//Point.002	-0.4405	1.55365	0.26956
//Point.003	-0.79539	1.3488	0.25102";

////STL.003	4472.53252928411	5005.97120809212	331.181812640976
////Point.001	4471.19223435416	5006.51895516396	331.40917279785      XYH
////Point.002	4470.92129262885	5006.15392266663	331.451292833846
////Point.003	4470.97642500157	5005.74787217845	331.432752819747
//            List<Point> points = Mocks.MockPoints(content, Coordinates.ReferenceFrames.Su1985Machine);

//            // "ORIGIN     IP8_2014_SU     4472.525917     5005.972899     2330.049057     224.734577    0.00417001"
//            Zone z = new Zone("lhsb",
//                new Coordinates("Unknwon", 4472.525917, 5005.972899, 2330.049057), new DoubleValue(224.734577, 0), new DoubleValue(0.00417001, 0),
//                new Rotation3D(0,0,100),null);


//            Systems.Su2Mla(points, z.Mla2SuInGon);

//            TSU.IO.SUSoft.CsGeo.XYH2MLAorOpposite(
//                ref points,
//                Coordinates.ReferenceFrames.MLA1985Machine, z);

//            Assert.AreEqual(points[0]._Coordinates.Ccs.X.Value, 4472.53252928411, 0.00001);
//            Assert.AreEqual(points[1]._Coordinates.Ccs.Z.Value, 331.40917279785, 0.00001);
//        }


        [TestMethod()]
        public void CsGeoTestCERNxyz_2_CERNXYH()
        {
            //string content =
//@"a	1000	1000	3000";
            string expectedResult =
@"a	1000	1000	1000.17047254139";

            List<Point> points = Mocks.MockPoints(expectedResult,Coordinates.ReferenceFrames.CernXYHg85Machine);

            for (int i = 0; i < points.Count; i++)
            {
                Assert.AreEqual(points[i]._Coordinates.Ccs.X.Value, points[i]._Coordinates.Ccs.X.Value, 0.00001);
                Assert.AreEqual(points[i]._Coordinates.Ccs.Y.Value, points[i]._Coordinates.Ccs.Y.Value, 0.00001);
                Assert.AreEqual(points[i]._Coordinates.Ccs.Z.Value, points[i]._Coordinates.Ccs.Z.Value, 0.00001);
            }
        }

        [TestMethod()]
        public void CsGeoTestCERNXYH_2_CERNxyz()
        {
//            string content =
//@"a	1000	1000	3000";
            string expectedResult =
@"a	1000	1000	1000.17047254139";


            List<Point> points = Mocks.MockPoints(expectedResult, Coordinates.ReferenceFrames.CernXYHg85Machine);

            for (int i = 0; i < points.Count; i++)
            {
                Assert.AreEqual(points[i]._Coordinates.Ccs.X.Value, points[i]._Coordinates.Ccs.X.Value, 0.00001);
                Assert.AreEqual(points[i]._Coordinates.Ccs.Y.Value, points[i]._Coordinates.Ccs.Y.Value, 0.00001);
                Assert.AreEqual(points[i]._Coordinates.Ccs.Z.Value, points[i]._Coordinates.Ccs.Z.Value, 0.00001);
            }
        }
    }
}
