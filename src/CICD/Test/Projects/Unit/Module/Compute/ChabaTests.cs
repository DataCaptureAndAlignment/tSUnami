﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Tools;
using E = TSU.Common.Elements;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestsTsunami
{
    [TestClass()]
    public class ChabaTests
    {
        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [TestMethod()]
        public void ChabaTest()
        {
            string content =
@"PJ01               -8.8109000         2.0567200        -1.0946900        
PJ02               25.1654400         7.3745100        -2.3511900       
PJ03               25.2168500         4.5166700        -1.4226400       
PJ04               -5.5777100         6.9295200        -2.1656600       
PJ05               38.0604500         3.0437400        -0.9398100";
            string content2 =
@"PJ01   -8.81106   2.05653   -1.09456
PJ02   25.16532   7.37443   -2.35117
PJ03   25.21689   4.51672   -1.42252
PJ04   -5.57775   6.92923   -2.16574";
            List<string> list = new List<string>();
            TSU.IO.SUSoft.Chaba c = new TSU.IO.SUSoft.Chaba(
                TestsTsunami.Mocks.MockPoints(content, E.Coordinates.ReferenceFrames.MLA1985Machine),
                TestsTsunami.Mocks.MockPoints(content2, E.Coordinates.ReferenceFrames.MLA1985Machine), out list,
                true, "chabaTest");
            List<E.Point> l = c.GetOutputPoints();
            Assert.AreEqual(-8.8109503, l[0]._Coordinates.Local.X.Value, 0.00001);
            Assert.AreEqual(38.0603922, l[4]._Coordinates.Local.X.Value, 0.00001);
        }

        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [TestMethod()]
        public void ChabaTesTSU()
        {
            string content =
@"538_1         -0.025000       0.0250000       0.0400000
538_2         0.0250000       0.0250000       0.0400000
538_3         0.0250000       -0.025000       0.0400000
538_4         -0.025000       -0.025000       0.0400000
538_0          0.0000000       0.0000000       0.0000000";
            string content2 =
            @"538_1           -13.10956    -20.93968     -4.95779
538_2           -13.14525    -20.93986     -4.99139
538_3           -13.11087    -20.93977     -5.02758
538_4           -13.07511    -20.93954     -4.99257";

            List<string> list = new List<string>();
            TSU.IO.SUSoft.Chaba c = new TSU.IO.SUSoft.Chaba(
                Mocks.MockPoints(content, E.Coordinates.ReferenceFrames.Su1985Machine),
                Mocks.MockPoints(content2, E.Coordinates.ReferenceFrames.Su1985Machine), out list,
                true, "chabaTest");
            List<E.Point> l = c.GetOutputPoints();
            Assert.AreEqual(-13.10956, l[0]._Coordinates.Local.X.Value, 0.00001);
            Assert.AreEqual(-4.992284, l[4]._Coordinates.Local.Z.Value, 0.00001);

        }
    }
}
