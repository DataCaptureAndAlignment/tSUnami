﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Tools.Transformation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestsTsunami;
using TSU.Common.Elements;
using TSU.Common.Compute.Transformation;

namespace TSU.Tools.Transformation.Tests
{
    [TestClass()]
    public class TsunamiTests
    {

        [TestMethod()]
        public void Csgeo_Transforamtion_H_2_MLA()
        {
            double x=4001, y=4001, z=2001;

            //TSU.IO.SUSoft.SpatialObjDLL_32bit.TransformToMLA(4000, 4000, 2000, ref x, ref y, ref z, "CG2000");
            Common.Compute.Transformation.Systems.Ccs2Mla(4000, 4000, 2000, ref x, ref y, ref z, Coordinates.ReferenceSurfaces.RS2K);
            Assert.AreEqual(x, 0.99969, 0.00001);
            Assert.AreEqual(y, 0.99969, 0.00001);
            Assert.AreEqual(z, 1.00060, 0.00001);

            x = 4001; y = 4001; z = 2001;
        }

        [TestMethod()]
        public void Transforamtion_MLA_2_SU_Rotate3DAroundOriginTest()
        {
            string contentMLA =
       @"PJ01                  2.05672                    8.81090                   -1.09469  
          PJ02                  7.37451                  -25.16544                   -2.35119  
          PJ03                  4.51667                  -25.21685                   -1.42264  
          PJ04                  6.92952                    5.57771                   -2.16566  
          PJ05                  3.04374                  -38.06045                   -0.93981";
            List<Point> points = Mocks.MockPoints(contentMLA, Coordinates.ReferenceFrames.MLA1985Machine);

//            string contenTSU =
//        @"PJ01               -8.8109000         2.0567200        -1.0946900        
//        PJ02               25.1654400         7.3745100        -2.3511900       
//        PJ03               25.2168500         4.5166700        -1.4226400       
//        PJ04               -5.5777100         6.9295200        -2.1656600       
//        PJ05               38.0604500         3.0437400        -0.9398100";

            Coordinates c = TSU.Common.Compute.Transformation.Systems.Rotate3DAroundOrigin(points[0]._Coordinates.Local, 0, 0, 100);

            Assert.AreEqual(c.X.Value, -8.8109000, 0.00001);
            Assert.AreEqual(c.Y.Value, 2.0567200, 0.00001);
            Assert.AreEqual(c.Z.Value, -1.0946900, 0.00001);
        }

        [TestMethod()]
        public void Transforamtion_SU_MLA_Rotate3DAroundOriginTest()
        {
//            string contentMLA =
//       @"PJ01                  2.05672                    8.81090                   -1.09469  
//          PJ02                  7.37451                  -25.16544                   -2.35119  
//          PJ03                  4.51667                  -25.21685                   -1.42264  
//          PJ04                  6.92952                    5.57771                   -2.16566  
//          PJ05                  3.04374                  -38.06045                   -0.93981";
            

            string contenTSU =
        @"PJ01               -8.8109000         2.0567200        -1.0946900        
        PJ02               25.1654400         7.3745100        -2.3511900       
        PJ03               25.2168500         4.5166700        -1.4226400       
        PJ04               -5.5777100         6.9295200        -2.1656600       
        PJ05               38.0604500         3.0437400        -0.9398100";
            List<Point> points = Mocks.MockPoints(contenTSU, Coordinates.ReferenceFrames.MLA1985Machine);

            Coordinates c = TSU.Common.Compute.Transformation.Systems.Rotate3DAroundOrigin(points[0]._Coordinates.Local, 0, 0, -100);

            Assert.AreEqual(c.X.Value, 2.05672, 0.00001);
            Assert.AreEqual(c.Y.Value, 8.81090, 0.00001);
            Assert.AreEqual(c.Z.Value, -1.09469, 0.00001);
        }
    }
}
