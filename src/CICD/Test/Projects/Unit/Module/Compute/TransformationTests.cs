using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TSU.Tools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Elements;
using TSU.Common.Zones;
using TSU.Common.Compute.Transformation;

namespace TSU.Tools.Tests
{
    [TestClass()]
    public class TransformationTests
    {
//   SUlhcb
//PJ00   0    0   0
//PJ01                  2.05672                    8.81090                   -1.09469  
//PJ02                  7.37451                  -25.16544                   -2.35119  
//PJ03                  4.51667                  -25.21685                   -1.42264  
//PJ04                  6.92952                    5.57771                   -2.16566  
//PJ05                  3.04374                  -38.06045                   -0.93981

// PhysLHCb
//PJ00                 -0.00000             0.00000              -0.00000
//PJ01                 -8.81090             -1.08728             -2.06065
//PJ02                 25.16544             -2.32461             -7.38293
//PJ03                 25.21685             -1.40636             -4.52176
//PJ04                 -5.57771             -2.14069             -6.93727
//PJ05                 38.06045             -0.92884             -3.04711
        [TestMethod()]
        public void LHCb_Survey2Physician_Test()
        {
            TSU.Common.Elements.Point p = new TSU.Common.Elements.Point("a");
            p._Coordinates.Su.X.Value = 7.37451;
            p._Coordinates.Su.Y.Value = -25.16544;
            p._Coordinates.Su.Z.Value = -2.35119;
            Rotation3D r1 = new Rotation3D(300, 0, 100); ///0.0036016
            Rotation r2 = new Rotation(0.0036016); ///0.0036016
            Systems.Su2Phys(p, r1, r2);
            Assert.AreEqual(25.16544, p._Coordinates.Physicist.X.Value, 0.00001);
            Assert.AreEqual(-2.32461, p._Coordinates.Physicist.Y.Value, 0.00001);
            Assert.AreEqual(-7.38293, p._Coordinates.Physicist.Z.Value, 0.00001);

            TSU.Common.Compute.Transformation.Systems.Phys2Su(p, r1, r2);
            Assert.AreEqual(7.37451, p._Coordinates.Su.X.Value, 0.000001);
            Assert.AreEqual(-25.16544, p._Coordinates.Su.Y.Value, 0.000001);
            Assert.AreEqual(-2.35119, p._Coordinates.Su.Z.Value, 0.000001);

            r1 = new Rotation3D(300.229285, 0, 100); ///0.0036016
            r2 = new Rotation(0.0); ///0.0036016
            Systems.Su2Phys(p, r1, r2);
            Assert.AreEqual(25.16544, p._Coordinates.Physicist.X.Value, 0.00001);
            Assert.AreEqual(-2.32461, p._Coordinates.Physicist.Y.Value, 0.00001);
            Assert.AreEqual(-7.38293, p._Coordinates.Physicist.Z.Value, 0.00001);

            Systems.Phys2Su(p, r1, r2);
            Assert.AreEqual(7.37451, p._Coordinates.Su.X.Value, 0.000001);
            Assert.AreEqual(-25.16544, p._Coordinates.Su.Y.Value, 0.000001);
            Assert.AreEqual(-2.35119, p._Coordinates.Su.Z.Value, 0.000001);
        }

        [TestMethod()]
        public void nTOF_Survey2Physician_Test()
        {
            TSU.Common.Elements.Point p = new TSU.Common.Elements.Point("a");
            p._Coordinates.Su.X.Value = 7.37451;
            p._Coordinates.Su.Y.Value = -25.16544;
            p._Coordinates.Su.Z.Value = -2.35119;

            Rotation3D r1 = new Rotation3D(-99.22841684, 0, 300); 
            Rotation r2 = new Rotation(0); 
            
            Systems.Su2Phys(p, r1, r2);
            Assert.AreEqual(-25.16544, p._Coordinates.Physicist.X.Value, 0.00001);
            Assert.AreEqual(-2.44039, p._Coordinates.Physicist.Y.Value, 0.00001);
            Assert.AreEqual(7.34547, p._Coordinates.Physicist.Z.Value, 0.00001);

            Systems.Phys2Su(p, r1, r2);
            Assert.AreEqual(7.37451, p._Coordinates.Su.X.Value, 0.000001);
            Assert.AreEqual(-25.16544, p._Coordinates.Su.Y.Value, 0.000001);
            Assert.AreEqual(-2.35119, p._Coordinates.Su.Z.Value, 0.000001);
        }

        [TestMethod()]
        public void Alice_Survey2Physician_Test()
        {
            TSU.Common.Elements.Point p = new TSU.Common.Elements.Point("a");
            p._Coordinates.Su.X.Value = 7.37451;
            p._Coordinates.Su.Y.Value = -25.16544;
            p._Coordinates.Su.Z.Value = -2.35119;
            Rotation3D r1 = new Rotation3D(300, 0, 300); 
            Rotation r2 = new Rotation(0.0138616); // 0.882457 gon
            Systems.Su2Phys(p, r1, r2);
            Assert.AreEqual(-25.16544, p._Coordinates.Physicist.X.Value, 0.00001);
            Assert.AreEqual(-2.24874, p._Coordinates.Physicist.Y.Value, 0.00001);
            Assert.AreEqual(7.40639, p._Coordinates.Physicist.Z.Value, 0.00001);

            Systems.Phys2Su(p, r1, r2);
            Assert.AreEqual(7.37451, p._Coordinates.Su.X.Value, 0.000001);
            Assert.AreEqual(-25.16544, p._Coordinates.Su.Y.Value, 0.000001);
            Assert.AreEqual(-2.35119, p._Coordinates.Su.Z.Value, 0.000001);


            r1 = new Rotation3D(-100.882457, 0, 300);
            r2 = new Rotation(0);
            Systems.Su2Phys(p, r1, r2);
            Assert.AreEqual(-25.16544, p._Coordinates.Physicist.X.Value, 0.00001);
            Assert.AreEqual(-2.24874, p._Coordinates.Physicist.Y.Value, 0.00001);
            Assert.AreEqual(7.40639, p._Coordinates.Physicist.Z.Value, 0.00001);

            Systems.Phys2Su(p, r1, r2);
            Assert.AreEqual(7.37451, p._Coordinates.Su.X.Value, 0.000001);
            Assert.AreEqual(-25.16544, p._Coordinates.Su.Y.Value, 0.000001);
            Assert.AreEqual(-2.35119, p._Coordinates.Su.Z.Value, 0.000001);
        }
        [TestMethod()]
        public void AtlasSurvey2PhysicianTest()
        {
            TSU.Common.Elements.Point p = new TSU.Common.Elements.Point("a");
            p._Coordinates.Su.X.Value = 7.37451;
            p._Coordinates.Su.Y.Value = -25.16544;
            p._Coordinates.Su.Z.Value = -2.35119;
            Rotation3D r1 = new Rotation3D(-100, 0, 300);
            Rotation r2 = new Rotation(0.0123626); // 0.78702755978714611119056096522762
            Systems.Su2Phys(p, r1, r2);
            Assert.AreEqual(-25.16544, p._Coordinates.Physicist.X.Value, 0.00001);
            Assert.AreEqual(-2.25984, p._Coordinates.Physicist.Y.Value, 0.00001);
            Assert.AreEqual(7.40301, p._Coordinates.Physicist.Z.Value, 0.00001);

            Systems.Phys2Su(p, r1, r2);
            Assert.AreEqual(7.37451, p._Coordinates.Su.X.Value, 0.000001);
            Assert.AreEqual(-25.16544, p._Coordinates.Su.Y.Value, 0.000001);
            Assert.AreEqual(-2.35119, p._Coordinates.Su.Z.Value, 0.000001);

            r1 = new Rotation3D(-100.78702756, 0, 300);
            r2 = new Rotation(0); 
            Systems.Su2Phys(p, r1, r2);
            Assert.AreEqual(-25.16544, p._Coordinates.Physicist.X.Value, 0.00001);
            Assert.AreEqual(-2.25984, p._Coordinates.Physicist.Y.Value, 0.00001);
            Assert.AreEqual(7.40301, p._Coordinates.Physicist.Z.Value, 0.00001);

            Systems.Phys2Su(p, r1, r2);
            Assert.AreEqual(-25.16544, p._Coordinates.Physicist.X.Value, 0.00001);
            Assert.AreEqual(-2.25984, p._Coordinates.Physicist.Y.Value, 0.00001);
            Assert.AreEqual(7.40301, p._Coordinates.Physicist.Z.Value, 0.00001);
        }

        [TestMethod()]
        public void CmsSurvey2PhysicianTest()
        {
            TSU.Common.Elements.Point p = new TSU.Common.Elements.Point("a");
            p._Coordinates.Su.X.Value = 7.37451;
            p._Coordinates.Su.Y.Value = -25.16544;
            p._Coordinates.Su.Z.Value = -2.35119;
            Rotation3D r1 = new Rotation3D(300, 0, 300);
            Rotation r2 = new Rotation(-0.0123365); // 0.785366 gon
            Systems.Su2Phys(p, r1, r2);
            Assert.AreEqual(-25.16544, p._Coordinates.Physicist.X.Value, 0.00001);
            Assert.AreEqual(-2.44198, p._Coordinates.Physicist.Y.Value, 0.00001);
            Assert.AreEqual(7.34494, p._Coordinates.Physicist.Z.Value, 0.00001);

            Systems.Phys2Su(p, r1, r2);
            Assert.AreEqual(7.37451, p._Coordinates.Su.X.Value, 0.000001);
            Assert.AreEqual(-25.16544, p._Coordinates.Su.Y.Value, 0.000001);
            Assert.AreEqual(-2.35119, p._Coordinates.Su.Z.Value, 0.000001);

            r1 = new Rotation3D(300.785366, 0, 300);
            r2 = new Rotation(0);
            Systems.Su2Phys(p, r1, r2);
            Assert.AreEqual(-25.16544, p._Coordinates.Physicist.X.Value, 0.00001);
            Assert.AreEqual(-2.44198, p._Coordinates.Physicist.Y.Value, 0.00001);
            Assert.AreEqual(7.34494, p._Coordinates.Physicist.Z.Value, 0.00001);

            Systems.Phys2Su(p, r1, r2);
            Assert.AreEqual(7.37451, p._Coordinates.Su.X.Value, 0.000001);
            Assert.AreEqual(-25.16544, p._Coordinates.Su.Y.Value, 0.000001);
            Assert.AreEqual(-2.35119, p._Coordinates.Su.Z.Value, 0.000001);
        }

        [TestMethod()]
        public void Na62_4Survey2PhysicianTest()
        {
            TSU.Common.Elements.Point p = new TSU.Common.Elements.Point("a");
            p._Coordinates.Su.X.Value = 7.37451;
            p._Coordinates.Su.Y.Value = -25.16544;
            p._Coordinates.Su.Z.Value = -2.35119;

            Translation3D t1 = new Translation3D(0, 0, 230);
            Rotation3D r1 = new Rotation3D(300, 0, 300);
            Rotation r2 = new Rotation(-0.000023); /// -0.001464 gon

            Systems.Su2Phys(p, r1, r2,t1);
            Assert.AreEqual(-25.16544, p._Coordinates.Physicist.X.Value, 0.00001);
            Assert.AreEqual(-2.35136, p._Coordinates.Physicist.Y.Value, 0.00001);
            Assert.AreEqual(237.37451, p._Coordinates.Physicist.Z.Value, 0.00007);    //diff augmented ecasue looks like pctopo and carnet did not take in account the rotation impact along the beam
            
            Systems.Phys2Su(p, r1, r2,t1);
            Assert.AreEqual(7.37451, p._Coordinates.Su.X.Value, 0.000001);
            Assert.AreEqual(-25.16544, p._Coordinates.Su.Y.Value, 0.000001);
            Assert.AreEqual(-2.35119, p._Coordinates.Su.Z.Value, 0.000001);

            t1 = new Translation3D(0, 0, 230);
            r1 = new Rotation3D(300.001464, 0, 300);
            r2 = new Rotation(0);

            Systems.Su2Phys(p, r1, r2, t1);
            Assert.AreEqual(-25.16544, p._Coordinates.Physicist.X.Value, 0.00001);
            Assert.AreEqual(-2.35136, p._Coordinates.Physicist.Y.Value, 0.00001);
            Assert.AreEqual(237.37451, p._Coordinates.Physicist.Z.Value, 0.00007);    //diff augmented ecasue looks like pctopo and carnet did not take in account the rotation impact along the beam

            Systems.Phys2Su(p, r1, r2, t1);
            Assert.AreEqual(7.37451, p._Coordinates.Su.X.Value, 0.000001);
            Assert.AreEqual(-25.16544, p._Coordinates.Su.Y.Value, 0.000001);
            Assert.AreEqual(-2.35119, p._Coordinates.Su.Z.Value, 0.000001);
        }

        [TestMethod()]
        public void Na62_3Survey2PhysicianTest()
        {
            //TODO: ASSERT_FAIL
            //Systems.Su2Phys values are different

            TSU.Common.Elements.Point p = new TSU.Common.Elements.Point("a");
            p._Coordinates.Su.X.Value = 7.37451;
            p._Coordinates.Su.Y.Value = -25.16544;
            p._Coordinates.Su.Z.Value = -2.35119;

            Translation3D t1 = new Translation3D(0, 0, 180);
            Rotation3D r1 = new Rotation3D(-100, -100, 0);
            Rotation r2 = new Rotation(-0.0000152);

            Systems.Su2Phys(p, r1, r2, t1);
            Assert.AreEqual(2.35130, p._Coordinates.Physicist.X.Value, 0.00001);
            Assert.AreEqual(7.374474, p._Coordinates.Physicist.Y.Value, 0.00001);
            Assert.AreEqual(205.16544, p._Coordinates.Physicist.Z.Value, 0.00005);  //diff augmented ecasue looks like pctopo and carnet did not take in account the rotation impact along the beam


            Systems.Phys2Su(p, r1, r2, t1);
            Assert.AreEqual(7.37451, p._Coordinates.Su.X.Value, 0.000001);
            Assert.AreEqual(-25.16544, p._Coordinates.Su.Y.Value, 0.000001);
            Assert.AreEqual(-2.35119, p._Coordinates.Su.Z.Value, 0.000001);
        }


        [TestMethod()]
        public void Na62_1_Survey2PhysicianTest()
        {
            //TODO: ASSERT_FAIL
            //Systems.Su2Phys values are different

            TSU.Common.Elements.Point p = new TSU.Common.Elements.Point("a");
            p._Coordinates.Su.X.Value = 7.37451;
            p._Coordinates.Su.Y.Value = -25.16544;
            p._Coordinates.Su.Z.Value = -2.35119;
            Common.Compute.Transformation.Rotation3D r1 = new Common.Compute.Transformation.Rotation3D(-100, -100, 0);
            Common.Compute.Transformation.Rotation r2 = new Common.Compute.Transformation.Rotation(-0.0000018);
            Common.Compute.Transformation.Translation3D t1 = new Common.Compute.Transformation.Translation3D(0, 0,95);
            
            Common.Compute.Transformation.Systems.Su2Phys(p, r1, r2,t1);
            Assert.AreEqual(2.35120, p._Coordinates.Physicist.X.Value, 0.00001);
            Assert.AreEqual(7.37450, p._Coordinates.Physicist.Y.Value, 0.00001);
            Assert.AreEqual(120.16544, p._Coordinates.Physicist.Z.Value, 0.00001);

            Common.Compute.Transformation.Systems.Phys2Su(p, r1, r2,t1);
            Assert.AreEqual(7.37451, p._Coordinates.Su.X.Value, 0.000001);
            Assert.AreEqual(-25.16544, p._Coordinates.Su.Y.Value, 0.000001);
            Assert.AreEqual(-2.35119, p._Coordinates.Su.Z.Value, 0.000001);
        }

        [TestMethod()]
        public void Csgeo_Cern2Mla1985Test()
        {
            //DOTO: TEST_BUG
            // System.NullReferenceException: Object reference not set to an instance of an object.
            // Values of p are -999.9


            //CERN XYZ : QTLD.412100.E   3003.203100   4214.495620  2385.118060
            //P8 MLA   : QTLD.412100.E  -1469.343358   -791.502315  54.1534834187664
            // with ORIGIN p8    IP8_2014_SU     4472.525917     5005.972899     2330.049057     0.00     0.0

            //Point p = new Point("QTLD.412100.E");
           // p._Coordinates.Ccs = new Coordinates("Unknwon", 3003.203100, 4214.495620, 2385.118060);
            //Coordinates origin = new Coordinates("Unknwon", 4472.525917, 5005.972899, 2330.049057);

            //if (Common.Compute.Transformation.Systems.Cern2La1985(origin, ref p))
            //{
            //    Assert.AreEqual(-1469.343358, p._Coordinates.Mla.X.Value, 0.00001);
            //    Assert.AreEqual( -791.502315, p._Coordinates.Mla.Y.Value, 0.00001);
            //    Assert.AreEqual( 54.15348, p._Coordinates.Mla.Z.Value, 0.00001);
            //}
            //else
            //    Assert.Fail();
        }

        [TestMethod()]
        public void LHCb_SUtoXYh()
        {
            //DOTO: TEST_BUG
            // System.NullReferenceException: Object reference not set to an instance of an object.


            // SU  20A14-5P -19.96201   -14.34947   -5.03459  
            // CCS 20A14-5P 4478.24180  4982.06041  326.14719 
            // MLA 20A14-5P	-14.3494674678714	19.9620127362662	-5.03458993188275


            TSU.Common.Elements.Point p = new TSU.Common.Elements.Point("a");
            p._Coordinates.Su.X.Value = -19.96201;
            p._Coordinates.Su.Y.Value = -14.34947;
            p._Coordinates.Su.Z.Value = -5.03459;
            //Common.Compute.Transformation.Rotation3D r1 = new Common.Compute.Transformation.Rotation3D(-100, -100, 0);
            //Common.Compute.Transformation.Rotation r2 = new Common.Compute.Transformation.Rotation(-0.000023);
            //List<Point> l = new List<Point>();
            //l.Add(p);

            //Zone z = new Zone()
            //{
            //    Mla2SuInGon = new Rotation3D(0, 0, 100),
            //     Ccs2MlaInfo = new Zone.Ccs2Mla()
            //     {
            //         Origin = new Coordinates("Unknown", 4472.525917, 5005.972899, 2330.049057),
            //         Bearing = new DoubleValue(224.734577, 0),
            //         Slope = new DoubleValue(0.00417001, 0)
            //     }
            //};

            //Systems.Su2Xyh(l, Coordinates.ReferenceFrames.Su1985Machine, z);
            //Assert.AreEqual(4478.24180, p._Coordinates.Ccs.X.Value, 0.00001);
            //Assert.AreEqual(4982.06041, p._Coordinates.Ccs.Y.Value, 0.00001);
            //Assert.AreEqual(326.14719, p._Coordinates.Ccs.Z.Value, 0.00001);

            //Systems.Xyh2Su(l, Coordinates.ReferenceFrames.CernXYHg85Machine, z);
            //Assert.AreEqual(-19.96201, p._Coordinates.Su.X.Value, 0.000001);
            //Assert.AreEqual(-14.34947, p._Coordinates.Su.Y.Value, 0.000001);
            //Assert.AreEqual(-5.03459, p._Coordinates.Su.Z.Value, 0.000001);
        }

        [TestMethod()]
        public void LHCb_XYh2Su()
        {
            //DOTO: TEST_BUG
            // System.NullReferenceException: Object reference not set to an instance of an object.

            // SU  20A14-5P -19.96201   -14.34947   -5.03459  
            // CCS 20A14-5P 4478.24180  4982.06041  326.14719 
            // MLA 20A14-5P	-14.3494674678714	19.9620127362662	-5.03458993188275


            TSU.Common.Elements.Point p = new TSU.Common.Elements.Point("a");
            p._Coordinates.Ccs.X.Value = 4478.24180;
            p._Coordinates.Ccs.Y.Value = 4982.06041;
            p._Coordinates.Ccs.Z.Value = 326.14719;
            //Rotation3D r1 = new Rotation3D(-100, -100, 0);
            //Rotation r2 = new Rotation(-0.000023);
            //List<Point> l = new List<Point>();
            //l.Add(p);
            //Zone z = new Zone()
            //{
            //    Mla2SuInGon = new Rotation3D(0, 0, 100),
            //     Ccs2MlaInfo = new Zone.Ccs2Mla()
            //     {
            //         Origin = new Coordinates("Unknwon", 4472.525917, 5005.972899, 2330.049057),
            //         Bearing = new DoubleValue(224.734577, 0),
            //         Slope = new DoubleValue(0.00417001, 0)
            //     }
            //};

            //Systems.Xyh2Su(l, Coordinates.ReferenceFrames.CernXYHg85Machine, z);
            //Assert.AreEqual(-14.349467, p._Coordinates.Mla.X.Value, 0.00001);
            //Assert.AreEqual(19.962012, p._Coordinates.Mla.Y.Value, 0.00001);
            //Assert.AreEqual(-5.034589, p._Coordinates.Mla.Z.Value, 0.00001);

            //Assert.AreEqual(-19.96201, p._Coordinates.Su.X.Value, 0.00001);
            //Assert.AreEqual(-14.34947, p._Coordinates.Su.Y.Value, 0.00001);
            //Assert.AreEqual(-5.03459, p._Coordinates.Su.Z.Value, 0.00001);
        }

        [TestMethod()]
        public void LHCb_SUtoMLA()
        {
            // SU  20A14-5P -19.96201   -14.34947   -5.03459  
            // MLA 20A14-5P	-14.3494674678714	19.9620127362662	-5.03458993188275

            TSU.Common.Elements.Point p = new TSU.Common.Elements.Point("a");
            p._Coordinates.Su.X.Value = -19.96201;
            p._Coordinates.Su.Y.Value = -14.34947;
            p._Coordinates.Su.Z.Value = -5.03459;
            Rotation3D r1 = new Rotation3D(0, 0, 100);
            List<Point> l = new List<Point>();
            l.Add(p);

            Systems.Su2Mla(l, r1);
            Assert.AreEqual(-14.349467, p._Coordinates.Mla.X.Value, 0.00001);
            Assert.AreEqual(19.9620127, p._Coordinates.Mla.Y.Value, 0.00001);
            Assert.AreEqual(-5.03458993, p._Coordinates.Mla.Z.Value, 0.00001);

            Systems.Mla2Su(l, r1);
            Assert.AreEqual(-19.96201, p._Coordinates.Su.X.Value, 0.000001);
            Assert.AreEqual(-14.34947, p._Coordinates.Su.Y.Value, 0.000001);
            Assert.AreEqual(-5.03459, p._Coordinates.Su.Z.Value, 0.000001);
        }

        [TestMethod()]
        public void RotateIn3DTest()
        {
            Coordinates a = Systems.Rotate3DAroundOrigin(new Coordinates("Unknwon", 0, 1, 0), 100, 0, 0);
            Assert.AreEqual(1, a.Z.Value, 0.00001);
            Assert.AreEqual(0, a.X.Value, 0.00001);
            Assert.AreEqual(0, a.Y.Value, 0.00001);
            a = Systems.Rotate3DAroundOrigin(new Coordinates("Unknwon", 0, 1, 0), Math.PI / 2, 0, 0, ENUM.AngularUnit.Rad);
            Assert.AreEqual(1, a.Z.Value, 0.00001);
            Assert.AreEqual(0, a.X.Value, 0.00001);
            Assert.AreEqual(0, a.Y.Value, 0.00001);
            a = Systems.Rotate3DAroundOrigin(new Coordinates("Unknwon", 0, 1, 0), 90, 0, 0, ENUM.AngularUnit.deg);
            Assert.AreEqual(1, a.Z.Value, 0.00001);
            Assert.AreEqual(0, a.X.Value, 0.00001);
            Assert.AreEqual(0, a.Y.Value, 0.00001);
        }

        [TestMethod()]
        public void LHCb_CCS_2_Phys()
        {
            //DOTO: TEST_BUG
            // System.NullReferenceException: Object reference not set to an instance of an object.
            // Different values for all points (maybe change number round for assert)

            // edms 1371198
            // CCS
            //IP8_2014SU 4472.5259174 5005.9728993 2330.0490570
            //IP8_2014SUx 4851.3546592 5931.4395174 2329.4806156
            //IP8_2014SUy 3547.0592007 5384.8017873 2330.2214456
            //IP8_2014SUz 4472.9007994 5006.4336672 3330.0488806

            // MLA
            //IP8_2014SU - 0.0000003 - 0.0000005 0.0000000
            //IP8_2014SUx - 0.0000003 - 1000.0000005 0.0000000
            //IP8_2014SUy 999.9999997 - 0.0000005 0.0000000
            //IP8_2014SUz - 0.0000003 - 0.0000005 1000.0000000

            // SU
            //IP8_2014SU 0.0000005 - 0.0000003 0.0000000
            //IP8_2014SUx 1000.0000005 0.0000034 0.0000000
            //IP8_2014SUy - 0.0000032 999.9999997 0.0000000
            //IP8_2014SUz 0.0000005 - 0.0000003 1000.0000000

            //P ys
            //IP8_2014SU - 0.00000   0.00000 - 0.00000
            //IP8_2014SUx - 0.00000  3.60159 - 999.99351
            //IP8_2014SUy - 1000.0  0.00000 - 0.00000
            //IP8_2014SUz - 0.00000   999.99351   3.60159


            //TSU.Common.Elements.Point p1 = new TSU.Common.Elements.Point("IP8_2014SU");
            //p1._Coordinates.Ccs.X.Value = 4472.5259174;
            //p1._Coordinates.Ccs.Y.Value = 5005.9728993;
            //p1._Coordinates.Ccs.Z.Value = 2330.0490570;
            //TSU.Common.Elements.Point p2 = new TSU.Common.Elements.Point("IP8_2014SUx");
            //p2._Coordinates.Ccs.X.Value = 4851.3546592;
            //p2._Coordinates.Ccs.Y.Value = 5931.4395174;
            //p2._Coordinates.Ccs.Z.Value = 2329.4806156;
            //TSU.Common.Elements.Point p3 = new TSU.Common.Elements.Point("IP8_2014SUy");
            //p3._Coordinates.Ccs.X.Value = 3547.0592007;
            //p3._Coordinates.Ccs.Y.Value = 5384.8017873;
            //p3._Coordinates.Ccs.Z.Value = 2330.2214456;
            //TSU.Common.Elements.Point p4 = new TSU.Common.Elements.Point("IP8_2014SUz");
            //p4._Coordinates.Ccs.X.Value = 4472.9007994;
            //p4._Coordinates.Ccs.Y.Value = 5006.4336672;
            //p4._Coordinates.Ccs.Z.Value = 3330.0488806;

            //List<Point> noCcsPoints = new List<Point>() {};
            //List<Point> noMlaPoints = new List<Point>() { p1, p2, p3, p4 };
            //List<Point> noSuPoints = new List<Point>() { p1, p2, p3, p4 };
            //List<Point> noPhyPoints = new List<Point>() { p1, p2, p3, p4 };

            //TSU.Common.Zones.Zone z = Tsunami2.Preferences.Values.Zones.Find(x => x._Name =="LHCb");
            //Systems.TryToFixMissingCoordinates(
            //    noCcsPoints, noMlaPoints, noSuPoints, noPhyPoints,
            //    Coordinates.ReferenceFrames.CCS, z);

            //Assert.AreEqual(0, p1._Coordinates.Mla.X.Value, 0.000001);
            //Assert.AreEqual(0, p1._Coordinates.Mla.Y.Value, 0.000001);
            //Assert.AreEqual(0, p1._Coordinates.Mla.Z.Value, 0.000001);

            //Assert.AreEqual(1000, p2._Coordinates.Su.X.Value, 0.000001);
            //Assert.AreEqual(0, p2._Coordinates.Su.Y.Value, 0.000001);
            //Assert.AreEqual(0, p2._Coordinates.Su.Z.Value, 0.000001);

            //Assert.AreEqual(100, p3._Coordinates.Mla.X.Value, 0.000001);
            //Assert.AreEqual(0, p3._Coordinates.Mla.Y.Value, 0.000001);
            //Assert.AreEqual(0, p3._Coordinates.Mla.Z.Value, 0.000001);

            //Assert.AreEqual(0, p4._Coordinates.Physicist.X.Value, 0.00001);
            //Assert.AreEqual(999.99351, p4._Coordinates.Physicist.Y.Value, 0.00001);
            //Assert.AreEqual(3.60159, p4._Coordinates.Physicist.Z.Value, 0.00001);
        }
    }
}
