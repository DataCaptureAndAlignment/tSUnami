﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestsTsunami;
using TSU;
using TSU.ENUM;
using TSU.Common.Elements;

using TSU.Common.Instruments;
using TSU.Common.Instruments.Device;
using M = TSU.Common.Measures;
using TSU.Tools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using P = TSU.Preferences;
using TSU.Common.Compute;

namespace TSU.Tools.Tests
{
    [TestClass()]
    public class SurveyComputeTests
    {
        [TestMethod()]
        public void DoubleValue_DoAverageTest()
        {
            DoubleValue one = new DoubleValue(100, 0.5);
            DoubleValue two = new DoubleValue(101, 0.6);
            DoubleValue three = new DoubleValue(102, 0.7);
            DoubleValue average = Survey.DoAverage(new List<IDoubleValue> { one, two, three });
            Assert.AreEqual(101, average.Value);
            Assert.AreEqual(0.349, average.Sigma, 0.001);
        }

        [TestMethod()]
        public void MeasureOfDistance_DoAverageTest()
        {
            M.MeasureOfDistance one = new M.MeasureOfDistance(); one.Corrected.Value = 3;
            M.MeasureOfDistance two = new M.MeasureOfDistance(); two.Corrected.Value = 4;
            M.MeasureOfDistance average = Survey.DoAverage(new List<M.IMeasureOfDistance> { one, two });
            Assert.AreEqual(3.5, average.Corrected.Value);
        }

        [TestMethod()]
        public void DistanceHorizontal()
        {
            Assert.AreEqual(Survey.GetHorizontalDistance(0, 0, 3, 4), 5);
            Assert.AreEqual(Survey.GetHorizontalDistance(4, 5, 1, 1), 5);
        }

        [TestMethod()]
        public void GetVerticalAngleTest1()
        {
            Assert.AreEqual(50, TSU.Common.Compute.Survey.GetVerticalAngle(new Coordinates("Unknwon", 0, 0, 0), new Coordinates("Unknwon", 0, 1, 1)));
            Assert.AreEqual(50, Survey.GetVerticalAngle(new Coordinates("Unknwon", 0, 0, 0), new Coordinates("Unknwon", 1, 0, 1)));
            Assert.AreEqual(0, Survey.GetVerticalAngle(new Coordinates("Unknwon", 0, 0, 0), new Coordinates("Unknwon", 0, 0, 1)));
            Assert.AreEqual(100, Survey.GetVerticalAngle(new Coordinates("Unknwon", 0, 0, 0), new Coordinates("Unknwon", 1, 1, 0)));
        }



        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [TestMethod()]
        public void CorrectForEtalonnageTest1()
        {
            M.MeasureOfOffset m = new M.MeasureOfOffset();
            m._RawReading = 0.75600;
            Line.Station.Average stationLine = new Line.Station.Average();

            TSU.Common.Instruments.Instrument i = P.Preferences.Instance.Instruments.Find(j => j._Model == "RS_1000" && j._SerialNumber == "5");
            OffsetMeter o = i as OffsetMeter;

            stationLine.ParametersBasic._Instrument = o as Sensor;
            Survey.CorrectForEtalonnage(m, (i as OffsetMeter).EtalonnageParameter, stationLine._Parameters);
            Assert.AreNotEqual(0.75600, m._CorrectedReading, 0.00001); // if exactly the same , correction is not applied
            Assert.AreEqual(0.75614887152, m._CorrectedReading, 0.001); // ust be a bit different but within 1mmm
        }

        [TestMethod()]
        public void SurveyCompute_DoReductionTest()
        {

            Point p5 = new Point("LHCb.Unknown.Point.005");
            Point p4 = new Point("LHCb.Unknown.Point.004");
            Point p3 = new Point("LHCb.Unknown.Point.003");

            Polar.Measure m41 = new Polar.Measure() { Face = FaceType.Face1, _Status = new M.States.Good(), _Point = p4, Angles = new M.MeasureOfAngles() { corrected = new Angles() { Horizontal = new DoubleValue(184.8131, 0), Vertical = new DoubleValue(88.48083, 0) } } };
            Polar.Measure m51 = new Polar.Measure() { Face = FaceType.Face1, _Status = new M.States.Good(), _Point = p5, Angles = new M.MeasureOfAngles() { corrected = new Angles() { Horizontal = new DoubleValue(203.0009, 0), Vertical = new DoubleValue(88.47540, 0) } } };
            Polar.Measure m31 = new Polar.Measure() { Face = FaceType.Face1, _Status = new M.States.Good(), _Point = p3, Angles = new M.MeasureOfAngles() { corrected = new Angles() { Horizontal = new DoubleValue(352.3031, 0), Vertical = new DoubleValue(74.96260, 0) } } };
            Polar.Measure m41b = new Polar.Measure() { Face = FaceType.Face1, _Status = new M.States.Temporary(), _Point = p4, Angles = new M.MeasureOfAngles() { corrected = new Angles() { Horizontal = new DoubleValue(184.8128, 0), Vertical = new DoubleValue(88.48077, 0) } } };
            Polar.Measure m32 = new Polar.Measure() { Face = FaceType.Face2, _Status = new M.States.Good(), _Point = p3, Angles = new M.MeasureOfAngles() { corrected = new Angles() { Horizontal = new DoubleValue(152.3031, 0), Vertical = new DoubleValue(325.03781, 0) } } };
            Polar.Measure m52 = new Polar.Measure() { Face = FaceType.Face2, _Status = new M.States.Good(), _Point = p5, Angles = new M.MeasureOfAngles() { corrected = new Angles() { Horizontal = new DoubleValue(2.9966, 0), Vertical = new DoubleValue(311.52631, 0) } } };
            Polar.Measure m42 = new Polar.Measure() { Face = FaceType.Face2, _Status = new M.States.Good(), _Point = p4, Angles = new M.MeasureOfAngles() { corrected = new Angles() { Horizontal = new DoubleValue(384.8127, 0), Vertical = new DoubleValue(311.51953, 0) } } };
            Polar.Measure m41c = new Polar.Measure() { Face = FaceType.Face1, _Status = new M.States.Temporary(), _Point = p4, Angles = new M.MeasureOfAngles() { corrected = new Angles() { Horizontal = new DoubleValue(184.8128, 0), Vertical = new DoubleValue(88.48096, 0) } } };
            
            List<Polar.Measure> l = new List<Polar.Measure>() { m41, m51, m31, m41b, m32, m52, m42, m41c };

            double closure = (m41b.Angles.Corrected.Horizontal.Value - m41.Angles.Corrected.Horizontal.Value)/3;
            List<Polar.Measure> lr = Survey.DoReduction(l);

            
            Assert.AreEqual(-0.0001999, lr[0].Angles.Corrected.Horizontal.Value, 0.001);
            Assert.AreEqual(18.1876000, lr[1].Angles.Corrected.Horizontal.Value, 0.001);
            Assert.AreEqual(167.4898, lr[2].Angles.Corrected.Horizontal.Value, 0.001);
            Assert.AreEqual(167.4898, lr[3].Angles.Corrected.Horizontal.Value, 0.001);
            Assert.AreEqual(18.18330000, lr[4].Angles.Corrected.Horizontal.Value, 0.001);
            Assert.AreEqual(-0.0005999996, lr[5].Angles.Corrected.Horizontal.Value, 0.001);

        }

        [TestMethod()]
        public void Modulo400Test()
        {
            DoubleValue dv = new DoubleValue(801.1, 0.2);
            Survey.Modulo400(dv);
            Assert.AreEqual(1.1, dv.Value, 0.0000001);
            Assert.AreEqual(0.2, dv.Sigma, 0.0000001);

            dv = new DoubleValue(-801.1, 0.2);
            Survey.Modulo400(dv);
            Assert.AreEqual(398.9, dv.Value, 0.0000001);
            Assert.AreEqual(0.2, dv.Sigma, 0.0000001);
        }

        [TestMethod()]
        public void GetSlopeTest()
        {
            Coordinates c1;
            Coordinates c2;

            c1 = new Coordinates("C1", 0, 0, 0);
            c2 = new Coordinates("C2", 0, 1, 0);

            Assert.AreEqual(0, Survey.GetSlope(c1, c2, 0), 0.00001);
            Assert.AreEqual(0, Survey.GetSlope(c1, c2, 1), 0.00001);
            Assert.AreEqual(100, Survey.GetSlope(c1, c2, 2), 0.00001);

            c1 = new Coordinates("C1", 1, 1, 1);
            c2 = new Coordinates("C2", 2, 1, 2);

            Assert.AreEqual(Math.PI / 4, Survey.GetSlope(c1, c2, 0), 0.00001);
            Assert.AreEqual(1000, Survey.GetSlope(c1, c2, 1), 0.00001);
            Assert.AreEqual(50, Survey.GetSlope(c1, c2, 2), 0.00001);

            c1 = new Coordinates("C1", 1, 1, 1);
            c2 = new Coordinates("C2", 2, 1, 1.001);

            Assert.AreEqual(0.000999999666666757, Survey.GetSlope(c1, c2, 0), 0.00001);
            Assert.AreEqual(1, Survey.GetSlope(c1, c2, 1), 0.00001);
            Assert.AreEqual(99.9363380, Survey.GetSlope(c1, c2, 2), 0.00001);
        }

        [DeploymentItem(@"libs\", @"libs\")]
        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [TestMethod()]
        public void ComputeCoordinateInStationCSTest()
        {
            // Local
            // st	1	1	1
            // p	2	2	2
            // f1	5	0	0

            // CCS
            // st	4471.22199642364	5005.42672203737	2331.04979765362   332.181732783692
            // p	4469.91807584688	5004.88054507538	2332.05053830664   333.181733229842
            // f1	4467.89858341606	5007.86704344023	2330.04991894317   331.18173453169

            Point m1;
            Point t1;
            Point sPoint;
            { // local
                //easy points
                t1 = new Point("t1"); t1._Coordinates.Ccs = new Coordinates("t1", 2001, 2001, 301);
                m1 = new Point("m1"); m1._Coordinates.Ccs = new Coordinates("m1", 2001, 2001, 300);
                sPoint = new Point("st"); sPoint._Coordinates.Ccs = new Coordinates("st", 2000, 2000, 300);

                Survey.ComputeCoordinateInStationCSFromCCS(sPoint, theoPoint: t1, measuredPoint: m1, Coordinates.ReferenceFrames.CernXYHg85Machine);

                Assert.AreEqual(0, m1._Coordinates.StationCs.X.Value, 0.00002);
                Assert.AreEqual(0, m1._Coordinates.StationCs.Y.Value, 0.00002);
                Assert.AreEqual(1.0, m1._Coordinates.StationCs.Z.Value, 0.00002);


                Survey.ComputeCoordinateInStationCSFromCCS(sPoint, theoPoint: sPoint, measuredPoint: m1, Coordinates.ReferenceFrames.CernXYHg85Machine);
                Assert.AreEqual(Math.Sqrt(2), m1._Coordinates.StationCs.Y.Value, 0.00002);

            }

            {//real points measured perfect
                m1 = new Point("p1"); m1._Coordinates.Ccs = new Coordinates("p1", 4469.91807584688, 5004.88054507538, 333.181733229842);
                sPoint = new Point("st"); sPoint._Coordinates.Ccs = new Coordinates("st", 4471.22199642364, 5005.42672203737, 332.181732783692);

                Survey.ComputeCoordinateInStationCSFromCCS(sPoint, m1, m1, Coordinates.ReferenceFrames.CernXYHg85Machine);

                Assert.AreEqual(0, m1._Coordinates.StationCs.X.Value, 0.000001);
                Assert.AreEqual(0, m1._Coordinates.StationCs.Y.Value, 0.000001);
                Assert.AreEqual(0.0, m1._Coordinates.StationCs.Z.Value, 0.000001);
            }

            Survey.ComputeCoordinateInStationCSFromCCS(sPoint, sPoint, m1, Coordinates.ReferenceFrames.CernXYHg85Machine);

            Assert.AreEqual(0, m1._Coordinates.StationCs.X.Value, 0.00001);
            Assert.AreEqual(Math.Sqrt(2), m1._Coordinates.StationCs.Y.Value, 0.00001);
            Assert.AreEqual(-1, m1._Coordinates.StationCs.Z.Value, 0.00001);
        }

        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [DeploymentItem(@"libs\SuSoft\", @"libs\SuSoft\")]
        [TestMethod()]
        public void ComputeCoordinateInBeamCSTest()
        {
            //TODO: TEST_BUG
            // Expected value for all assert not working - point coordinates is -999.9
            // System.NullReferenceException: Object reference not set to an instance of an object.


            // TT41      ;MBG.411508.E                    ;  600.49395; 2859.87064; 4221.67338;2393.03229;393.44110;  .657; -.00035; -.056608;107.7206; 1;A;23-MAR-2016;                    ;;
            // TT41      ;MBG.411508.S                    ;  603.98395; 2863.32946; 4221.25184;2392.83483;393.24395; 6.300; -.00035; -.056608;107.7206; 1;O;23-MAR-2016;                    ;;


            //Point theoPoint;
            //theoPoint = new Point("MBG.411508.E");
            //theoPoint._Coordinates.Ccs = new Coordinates("MBG.411508.E", 2859.87064, 4221.67338, 393.44110);//, 393.44110
            //theoPoint._Parameters.GisementFaisceau = 107.7206;
            //theoPoint._Parameters.Slope = -.056608;

            //Point point = new Point("MBG.411508.S");
            //point._Coordinates.Ccs = new Coordinates("MBG.411508.S", 2863.32946, 4221.25184, 393.24395);//, 393.24395

            //Polar.Station station = new Polar.Station();
            //station.Parameters2._StationPoint = point;
            //Survey.ComputeCoordinateInBeamCS(theoPoint, point, Coordinates.ReferenceFrames.CernXYHg85Machine);

            //Assert.AreEqual(0, point._Coordinates.Physicist.X.Value, 0.00001);
            //double dh = Survey.GetSpatialDistance(new Coordinates("1", 2859.87064, 4221.67338, 2393.03229), new Coordinates("2", 2863.32946, 4221.25184, 2392.83483));
            //Assert.AreEqual(dh, point._Coordinates.Physicist.Y.Value, 0.005);

            //Assert.AreEqual(0, point._Coordinates.Physicist.Z.Value, 0.005);
        }

        [TestMethod()]
        public void ComputeNewPointTest()
        {
            // TODO: ASSERT_FAIL
            // All values of new point are -999.9 


            Point point, newPoint;

            var t = new Coordinates("p1", 4000, 4000, 2000);
            point = new Point("p1"); point._Coordinates.AddOrReplaceCoordinatesInOneSystem("CCS-Z",t);

            double dH = 10;
            newPoint = Survey.ComputeNewPoint(point, 0, 0, dH);
            var c1 = newPoint._Coordinates.GetExistingOrCreateOne("CCS-Z", Coordinates.CoordinateSystemsTypes._3DCartesian);
            Assert.AreEqual(t.X.Value, c1.X.Value); 
            Assert.AreEqual(t.Y.Value + dH, c1.Y.Value); 
            Assert.AreEqual(t.Z.Value, c1.Z.Value);

            newPoint = Survey.ComputeNewPoint(point, 100, 0, dH);
            var c2 = newPoint._Coordinates.GetExistingOrCreateOne("CCS-Z", Coordinates.CoordinateSystemsTypes._3DCartesian);
            Assert.AreEqual(t.X.Value + dH, c2.X.Value);
            Assert.AreEqual(t.Y.Value, c2.Y.Value);
            Assert.AreEqual(t.Z.Value, c2.Z.Value);

            newPoint = Survey.ComputeNewPoint(point, 200, 0, dH);
            var c3 = newPoint._Coordinates.GetExistingOrCreateOne("CCS-Z", Coordinates.CoordinateSystemsTypes._3DCartesian);
            Assert.AreEqual(t.X.Value, c3.X.Value);
            Assert.AreEqual(t.Y.Value - dH, c3.Y.Value);
            Assert.AreEqual(t.Z.Value, c3.Z.Value);

            newPoint = Survey.ComputeNewPoint(point, 300, 0, dH);
            var c4 = newPoint._Coordinates.GetExistingOrCreateOne("CCS-Z", Coordinates.CoordinateSystemsTypes._3DCartesian);
            Assert.AreEqual(t.X.Value - dH, c4.X.Value);
            Assert.AreEqual(t.Y.Value, c4.Y.Value);
            Assert.AreEqual(t.Z.Value, c4.Z.Value);

            newPoint = Survey.ComputeNewPoint(point, 0, 0.001, dH); // 1mrad , 1mm/m , pour 10m 10mmm
            var c5 = newPoint._Coordinates.GetExistingOrCreateOne("CCS-Z", Coordinates.CoordinateSystemsTypes._3DCartesian);
            Assert.AreEqual(t.X.Value, c5.X.Value);
            Assert.AreEqual(t.Y.Value + dH, c5.Y.Value);
            Assert.AreEqual(t.Z.Value + 0.01, c5.Z.Value, 0.00001);

            newPoint = Survey.ComputeNewPoint(point, 100, 0.002, dH); // 2mrad , 2mm/m , pour 10m 20mmm
            var c6 = newPoint._Coordinates.GetExistingOrCreateOne("CCS-Z", Coordinates.CoordinateSystemsTypes._3DCartesian);
            Assert.AreEqual(t.X.Value + dH, c6.X.Value);
            Assert.AreEqual(t.Y.Value, c6.Y.Value);
            Assert.AreEqual(t.Z.Value + 0.02, c6.Z.Value, 0.00001);
        }

    }
}

namespace TSU.Unit_Tests
{
    [TestClass()]
    public class SurveyComputeTests
    {
        [TestMethod()]
        public void ApplyCalibrationCorrectionTest()
        {
            M.MeasureOfDistance m = new M.MeasureOfDistance();
            Polar.Station _Station = new FilledMockTotalStationStation().Object;
            m.Reflector = new FilledMockReflector("5589").Object;

            m.Corrected.Value = 100;
            Survey.CorrectForEtalonnage(m, _Station.ParametersBasic._Instrument);

            Assert.AreEqual(100.03470425742955, m.Corrected.Value, 0.00002, "Failed");

        }

        [TestMethod()]
        public void TransformToOppositeFaceTest()
        {
            M.MeasureOfAngles a = new M.MeasureOfAngles();
            a.Raw.Horizontal.Value = 50.5;
            a.Raw.Vertical.Value = 101.1;
            a.Corrected.Horizontal.Value = 250.6;
            a.Corrected.Vertical.Value = 298.9;
            M.MeasureOfAngles b = Survey.TransformToOppositeFace(a);

            Assert.AreEqual(250.5, b.Raw.Horizontal.Value, 0.00002, "Failed");
            Assert.AreEqual(298.9, b.Raw.Vertical.Value, 0.00002, "Failed");
            Assert.AreEqual(50.6, b.Corrected.Horizontal.Value, 0.00002, "Failed");
            Assert.AreEqual(101.1, b.Corrected.Vertical.Value, 0.00002, "Failed");
        }


    }
}
