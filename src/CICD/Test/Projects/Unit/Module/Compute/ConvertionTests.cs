﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TSU;
using TSU.ENUM;
using TSU.Common.Elements;

using TSU.Common.Instruments;
using TSU.Common.Measures;
using T = TSU.Tools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Instruments.Reflector;

namespace TSU.Tools.Tests
{
    [TestClass()]
    public class ConvertionTests
    {
        [TestMethod()]
        public void GonToRadTest()
        {
            double temp = 200;
            temp = T.Conversions.Angles.Gon2Rad(temp);
            Assert.AreEqual(Math.PI, temp, 0.0001);
        }

        [TestMethod()]
        public void ToStringParam()
        {
            return; // todo
            double temp = 200.123456789;
            Assert.AreEqual("200.1235", temp.ToString("F4"));
        }

        [TestMethod()]
        public void ToStringWithOneSigmaTest()
        {
            DoubleValue d = new DoubleValue(12.00127, 0.035);
            string s1 = T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(d, 4);
            string s2 = T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(d, 3);
            string s3 = T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(d, 5);
            Assert.AreEqual(s1, "12.0013(350)");
            Assert.AreEqual(s2, "12.001(35)");
            Assert.AreEqual(s3, "12.00127(3500)");
            d = new DoubleValue(12.00127, 0);
            s1 = T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(d, 4);
            Assert.AreEqual(s1, "12.0013");
            d = new DoubleValue();
            d.Value = 12.001275;
            s1 = T.Conversions.StringManipulation.DoubleValueToStringWithOneSigma(d, 7);
            Assert.AreEqual(s1, "12.0012750");
        }

        [TestMethod()]
        public void ToDoubleTest()
        {
            string point = "1.1";
            string coma = "1,1";

            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-EN");
            Assert.AreEqual(1.1, Conversions.Numbers.ToDouble(point));
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("fr-FR");
            Assert.AreEqual(1.1, Conversions.Numbers.ToDouble(coma, true));

            double d;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-EN");
            bool worked;
            worked = Conversions.Numbers.ToDouble(point, out d, true,8);
            Assert.IsTrue(worked);
            Assert.IsTrue(d==1.1);

            worked = Conversions.Numbers.ToDouble(point, out d, false);
            Assert.IsTrue(worked);
            Assert.IsTrue(d == 1.1);

            worked = Conversions.Numbers.ToDouble(coma, out d, true);
            Assert.IsTrue(worked);
            Assert.IsFalse(d == 1.1);

            worked = Conversions.Numbers.ToDouble(coma, out d, false);
            Assert.IsTrue(worked);
            Assert.IsFalse(d == 1.1);

            worked = Conversions.Numbers.ToDouble("e", out d, true);
            Assert.IsFalse(worked);

            worked = Conversions.Numbers.ToDouble("e", out d, false);
            Assert.IsFalse(worked);

            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("fr-FR");

            worked = Conversions.Numbers.ToDouble(coma, out d, true);
            Assert.IsTrue(worked);
            Assert.IsTrue(d == 1.1);

            worked = Conversions.Numbers.ToDouble(coma, out d, false);
            Assert.IsTrue(worked);
            Assert.IsTrue(d == 1.1);

            worked = Conversions.Numbers.ToDouble(point, out d, true);
            Assert.IsFalse(worked);

            worked = Conversions.Numbers.ToDouble(point, out d, false);
            Assert.IsFalse(worked);

            worked = Conversions.Numbers.ToDouble("e", out d, true);
            Assert.IsFalse(worked);

            worked = Conversions.Numbers.ToDouble("e", out d, false);
            Assert.IsFalse(worked);

        }

    }
}

namespace TSU.Unit_Tests
{
    [TestClass()]
    public class ConvertionTests
    {
        [TestMethod()]
        public void ToNextNameTest()
        {
            Assert.AreEqual("ABC(1)", TsuString.FindNextName("ABC"), false, "Failed");
            Assert.AreEqual("ABC(2)", TsuString.FindNextName("ABC(1)"), false, "Failed");
            Assert.AreEqual("ABC(3)", TsuString.FindNextName("ABC(2)"), false, "Failed");
            Assert.AreEqual("ABC(11)", TsuString.FindNextName("ABC(10)"), false, "Failed");
            Assert.AreEqual("ABC2", TsuString.FindNextName("ABC1"), false, "Failed");
            Assert.AreEqual("ABC10", TsuString.FindNextName("ABC9"), false, "Failed");

        }

        [TestMethod()]
        public void textFormat()
        {
            string GetTabulator = "   ";
            string b = " ";
            string s = "";
            s += "   %POLAR Instrument    DefaultTrgt  IHeight  sIH   sICentering  const_angl \r\n";
            s += "   %POLAR [id]          [id]         [m]      [mm]  [mm]         [gon]      \r\n";
            //   "          AT401_391055  RFI0.5_2884  0.00000  0.00  0.00         0.0000     \r\n";
            s += GetTabulator + "*POLAR" + b;
            s += string.Format("{0,-13}", "AT401_391055") + b;
            s += string.Format("{0,-12}", "RFI0.5_2884") + b;
            s += string.Format("{0,-8:0.00000}", 2) + b;
            s += string.Format("{0,-5:0.00}", 1 ) + b;
            s += string.Format("{0,-12:0.00}", 2) + b;
            s += string.Format("{0,-10:0.0000}", 3) + "\r\n";

            Reflector target = new Reflector();
            SigmaForAInstrumentCouple sigma = new SigmaForAInstrumentCouple();
            DoubleValue targetHt = new DoubleValue();
            s += "      %Target       sAngl  sZend  sDist  ppm   CsteVariable DistCorrection sDC   sCentering  Heigth  sH\r\n";
            s += "      %[id]         [cc]   [cc]   [mm]   [mm]  [1/0]        [m]            [mm]  [mm]        [m]     [mm] \r\n";
            //   "      RFI0.5_2884   0.0    0.0    0.00   0.00  0            0.00000        0.00  0.00  \r\n";
            s += GetTabulator + GetTabulator + string.Format("{0,-13}",target._Name.Replace(' ', '_')) + b;
            s += string.Format("{0,-6:0.0}", sigma.sigmaAnglCc) + b; //cc
            s += string.Format("{0,-6:0.0}", sigma.sigmaZenDCc) + b; //cc
            s += string.Format("{0,-6:0.00}", sigma.sigmaDistMm) + b; //mm
            s += string.Format("{0,-5:0.00}", sigma.ppmDistMm) + b; //mm
            int i = target.distCorrectionKnown ? 0 : 1; // 1 pour mettre la constante comme ajustable, 0 si elle est connue
            s += string.Format("{0,-12:0}", i) + b;
            s += string.Format("{0,-14:0.00000}", target.distCorrectionValue) + b; //m
            s += string.Format("{0,-5:0.00}", target.sigmaDCorr) + b; //mm
            s += string.Format("{0,-7:0.00}", target.sigmaTargetCentering) + b; //mm
            s += string.Format("{0,-11:0.00000}", targetHt.Value) + b; //m
            s += string.Format("{0,-8:0.00}", targetHt.Sigma) + "\r\n"; //mm



            string path = @"c:\Data\Tsunami\temp\lg2.txt";
            //System.IO.File.WriteAllText(path,s );
            //System.Diagnostics.Process.Start(path);
            //Assert.AreEqual("          AT401_391055  RFI0.5_2884  0.00000  0.00  0.00         0.0000    ", s);

        }


       

      
    }
}
