﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;

using TSU;
using TSU.ENUM;
using TSU.Common.Elements;

using TSU.Common.Instruments;
using TSU.Common.Measures;
using TSU.Tools;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TSU.Unit_Tests
{
    [TestClass()]
    public class ResearchTests
    {
        [TestMethod()]
        public void String_FindNext_Test()
        {
            List<Point> l = new List<Point>();
            Point p = new Point() { _Name = "PoInt2" };
            l.Add(p);
            string result = Research.FindNextName("PoinT", l);
            Assert.AreEqual("PoinT3", result, "nooo");
        }

        [TestMethod()]
        public void String_Foramt_test()
        {
            return; // todo
            double d = 1.6;
            string s = string.Format("{0,8:0.00000}", d);
            string t = $"{d,8:0.00000}";
            t = $"{d,8:0.00000}";
            t = $"{d,6:0.0}";
            t = $"{d,6:0.0}";
            t = $"{d,6:0.0}";
            t = $"{d,6}";


            Assert.AreEqual(" 1.60000", s);
        }
        
    }
}
