﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Common.Measures;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace TSU.Common.Measures.Tests
{
    [TestClass()]
    public class MeasureOfOffsetTests
    {
        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [DeploymentItem(@"libs\SuSoft\", @"libs\SuSoft\")]
        [DeploymentItem(@"libs\Leica\", @"libs\Leica\")]
        [TestMethod()]
        public void CloneTestMeasureOfOffsetTests()
        {
            Tsunami2.InitializePropertiesForUnitTesting();
            Preferences.Preferences.Instance.InitializeWithAllXmlParametersFiles();
            MeasureOfOffset m = new MeasureOfOffset();
            m._Name = "test";
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, m);
                stream.Position = 0;
                MeasureOfOffset m2 = ((MeasureOfOffset)formatter.Deserialize(stream));
                Assert.AreEqual(m._Name, m2._Name);
            }
        }
    }
}
