﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU;
using TSU.ENUM;
using TSU.Common.Elements;

using TSU.Common.Instruments;
using TSU.Common.Measures;
using TSU.Tools;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TestsTsunami;

namespace TSU.Unit_Tests
{
    [TestClass()]
    public class AnglesTests
    {
        [TestMethod()]
        public void Angles_Clone_Test()
        {
            Angles original = new Angles();
            original.Horizontal.Value = 1;

            Angles copy = (Angles)original.Clone();
            copy.Horizontal.Value = 2;

            Assert.AreEqual(1, original.Horizontal.Value, "Failed");
            Assert.AreEqual(2, copy.Horizontal.Value, "Failed");
        }

        [TestMethod()]
        public void Angles_DoAverage_Test()
        {
            IAngles un = new FilledMockAngles(1, FaceType.Face1).Object;
            IAngles deux = new FilledMockAngles(1, FaceType.Face2).Object;
            IAngles moyen = Common.Compute.Survey.DoAverage(new List<IAngles> { un, deux });


            Assert.AreEqual(196.03281, moyen.Horizontal.Value,0.0001, "Failed");
        }

    }
}
