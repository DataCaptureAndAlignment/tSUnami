﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Common.Measures;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace TSU.Common.Measures.Tests
{
    [TestClass()]
    public class MeasureTests
    {

        [TestMethod()]
        public void CloneTestMeasureTests()
        {
            Measure m = new Measure();
            m._Name = "test";
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(stream, m);
                stream.Position = 0;
                Measure m2 = ((Measure)formatter.Deserialize(stream));
                Assert.AreEqual(m._Name,m2._Name);
            }
            
        }

    }
}
