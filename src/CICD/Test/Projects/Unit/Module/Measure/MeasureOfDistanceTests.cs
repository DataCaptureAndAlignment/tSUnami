﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Common.Measures;
using TSU.Unit_Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace TSU.Common.Measures.Tests
{
    [TestClass()]
    public class MeasureOfDistanceTests
    {
        [TestMethod()]
        public void MeasureOfDistanceCloneTest()
        {
            MeasureOfDistance two = new MeasureOfDistance();
            two.Corrected.Value = 3;
            MeasureOfDistance cloneTwo = two.Clone() as MeasureOfDistance;
            cloneTwo.Corrected.Value = 4;
            Assert.AreEqual(4, cloneTwo.Corrected.Value);
            Assert.AreEqual(3, two.Corrected.Value);

        }

    }
}
