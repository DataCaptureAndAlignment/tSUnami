﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU;
using TSU.ENUM;
using TSU.Common.Elements;

using TSU.Common.Instruments;
using TSU.Common.Measures;
using TSU.Tools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace TSU.Unit_Tests
{
    [TestClass()]
    public class MeasureOfAnglesTests
    {
        [TestMethod()]
        public void MeasureOfAnglesCloneTest()
        {
            MeasureOfAngles original = new MeasureOfAngles();
            original.Raw.Horizontal.Value = 10;

            MeasureOfAngles copy = (MeasureOfAngles)original.Clone();
            copy.Raw.Horizontal.Value = 11;

            Assert.AreEqual(10, original.Raw.Horizontal.Value, "Failed");
            Assert.AreEqual(11, copy.Raw.Horizontal.Value, "Failed");

            copy = Common.Compute.Survey.TransformToOppositeFace(copy);

            Assert.AreEqual(10, original.Raw.Horizontal.Value, "Failed");
            Assert.AreEqual(211, copy.Raw.Horizontal.Value, "Failed");
        }

    }
}
