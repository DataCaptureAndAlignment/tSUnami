﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Preferences;

namespace TSU.Levelling.Tests
{
    [TestClass()]
    public class StationLevelModuleTests
    {
        [TestMethod()]
        public void StationLevelModuleSerialiazation()
        {
            Level.Module parent = new Level.Module();
            parent._Name = "papa";
            Level.Station.Module obj = new Level.Station.Module(parent);
            obj._Name = "test";

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new Level.Station.Module();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj._Name, (obj2 as Level.Station.Module)._Name);
        }
    }
}
