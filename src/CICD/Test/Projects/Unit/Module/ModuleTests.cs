﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestsTsunami;
using TSU.Preferences;
using E = TSU.Common.Elements;
using TSU.Common.Measures;

using TSU.Unit_Tests;
using Functionalities = TSU.Functionalities;
using TSU.Common;

namespace TSU.Unit_Tests
{
    [TestClass()]
    public class ModuleTests
    {
        [TestMethod()]
        public void Serialization_ModuleEmptyTest()
        {
          
            Module obj = new Module();
            obj._Name = "test";

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new Module();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj._Name, (obj2 as Module)._Name);
        }

        [TestMethod()]
        public void Serialization_CompositeModuleEmptyTest()
        {
            Module obj = new Module(null);
            obj._Name= "test";
            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new Module(null);
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj, (obj2 as Module));
            Assert.AreEqual(obj._Name, (obj2 as Module)._Name);
        }

        [TestMethod()]
        public void Serialization_CompositeModuleTest()
        {
            //Preferences.Instance.Initialize();
            Module obj = new Module(null);

            obj._Name = "test";

            System.IO.MemoryStream  ms = TSU.Unit_Tests.Serial.TryToSerialize(obj, false);
            object obj2 = new Module(null);
            
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2, ms);

            Assert.AreEqual(obj._Name, (obj2 as Module)._Name);
            Assert.AreEqual(obj, (obj2 as Module));
        }

        [TestMethod()]
        public void Serialization_CompositeModuleTheodoliteModuleTest()
        {
            FinalModule obj = new TSU.Polar.Module(null, "");
            obj._Name = "test";

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new TSU.Polar.Module(null,"");
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj._Name, (obj2 as Module)._Name);
            Assert.AreEqual(obj, (obj2 as Module));
        }

        [TestMethod()]
        public void Serialization_TsunamiTest()
        {
            Tsunami obj = new Tsunami(null);
            obj._Name = "test";
            obj.childModules.Add(new Polar.Module(obj, ""));

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new Common.Tsunami(null);
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj, (obj2 as Common.Tsunami));
        }


        [TestMethod()]
        public void Serialization_ClonableList()
        {
            TSU.CloneableList<string>  obj = new CloneableList<string>()  {  "1" ,"2","3"};

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new TSU.CloneableList<string>();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj[0], (obj2 as TSU.CloneableList<string>)[0]);
        }


        [TestMethod()]
        public void Serialization_Theomodule()
        {
            Polar.Module obj = new Polar.Module() { PointsToBeAligned = new CloneableList<E.Point>() { new E.Point("r") } };

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new Polar.Module();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj.PointsToBeAligned[0]._Name, (obj2 as Polar.Module).PointsToBeAligned[0]._Name);
        }


        [TestMethod()]
        public void ModuleTest()
        {
            Module obj = new Module();

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true);
            object obj2 = new Module();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj, (obj2 as Module));
        }

      
    }
}
