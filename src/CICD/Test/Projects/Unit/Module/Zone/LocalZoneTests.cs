﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using Zones=TSU.Common.Zones;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TSU.Preferences;
using TSU.Common.Compute.Transformation;


namespace TSU.Common.Zones.Tests
{
    [TestClass()]
    public class LocalZoneTests
    {
        [TestMethod()]
        public void LocalZoneTest()
        {
            Zone obj = new Zone();

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new Zone();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj, (obj2 as Zone));
        }

        [TestMethod()]
        public void LocalZoneFilledTest()
        {
            Zone zone = new Zone();
            zone.Ccs2MlaInfo = new Zone.Ccs2Mla();
            zone.Ccs2MlaInfo.ReferenceSurface = Elements.Coordinates.ReferenceSurfaces.RS2K;
            zone.Mla2SuInGon = new Rotation3D(1, 2, 3);
            zone.Su2PhysInGon = new Rotation3D(2, 3, 4);
            zone.Su2PhysInM = new Translation3D(3, 4, 5);
            zone.Ccs2MlaInfo.SumToAvoidUnawareModification = 5;

            TSU.Unit_Tests.Serial.TryToSerialize(zone, true); object obj2 = new Zone();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(zone, (obj2 as Zone));
        }

        [TestMethod()]
        public void LocalZoneListTest()
        {
            List<Zone> obj = new List<Zone>();
            obj.Add(new Zone());

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new List<Zone>();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj.Count, (obj2 as List<Zone>).Count);
        }

        [TestMethod()]
        public void LocalZoneManagerTest()
        {
            TsuObject obj = new Zones.Manager();

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new Zones.Manager();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj, (obj2 as Zones.Manager));
        }

        [TestMethod()]
        public void LocalZoneManager_WithOtherManagerTest()
        {
            List<TSU.Manager> obj = new List<TSU.Manager>();
            obj.Add(new Zones.Manager());
            obj.Add(new Operations.Manager());

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new List<TSU.Manager>();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj.Count, (obj2 as List<TSU.Manager>).Count);

        }
    }
}
