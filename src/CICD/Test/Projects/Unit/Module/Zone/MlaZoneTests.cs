﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using TSU.Common.Elements;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Zones;
using TSU;
using System.Xml.Serialization;
using TSU.Common.Compute.Transformation;

namespace TSU.Common.Zones.Tests
{
    [TestClass()]
    public class MlaZoneTests
    {
        [TestMethod()]
        public void serializatinMlaZone_Empty_Test()
        {
            //TSUPreferences.Instance.InitializeGeodeThings();
            Zone obj = new Zone();

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new Zone();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj, (obj2 as Zone));

        }

        [TestMethod()]
        public void serializatinMlaZoneTest()
        {
            //TSUPreferences.Instance.InitializeGeodeThings();
            Zone obj = new Zone("zonetest1", new Coordinates("Unknwon", 1, 2, 3), new DoubleValue(5, 6), new DoubleValue(7, 8), new Rotation3D(1, 2, 3));

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new Zone();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj, (obj2 as Zone));

        }

        [TestMethod()]
        public void SerailizationMlaZonesTest()
        {
            List<Zone> obj = new List<Zone>();
            obj.Add(new Zone("zonetest1", new Coordinates("Unknwon", 1, 2, 3), new DoubleValue(5, 6), new DoubleValue(7, 8), new Rotation3D(1, 2, 3)) { Date = DateTime.Now });
            obj.Add(new Zone("zonetest2", new Coordinates("Unknwon", 1, 2, 3), new DoubleValue(5, 6), new DoubleValue(7, 8), new Rotation3D(1, 2, 3)));

            TSU.Unit_Tests.Serial.TryToSerialize(obj, true); object obj2 = new List<Zone>();
            TSU.Unit_Tests.Serial.TryToDeSerialize(ref obj2);

            Assert.AreEqual(obj[1]._Name, (obj2 as List<Zone>)[1]._Name);
        }
    }
}
