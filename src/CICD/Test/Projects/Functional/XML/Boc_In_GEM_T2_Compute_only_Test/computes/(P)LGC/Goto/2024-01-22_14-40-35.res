

                          ****               ******              *****           ***********                                     
                          ****            ************        ***********        ***********                                     
                          ****           *****     ****      *****    ****              ****                                     
                          ****          ****        ****    ****       ****             ****                                     
                          ****          ****                ****                        ****                                     
                          ****          ****                ****                 ***********                                     
 ***********************  ****          ****      *******   ****                 ***********    ***********************             
 ***********************  ****          ****      *******   ****                 ****            ***********************             
                          ****          ****       ******   ****       ****      ****                                            
                          ***********    ****     **** **    ****     ****       ****                                            
                          ***********     ***********         ***********        ***********                                     
                          ***********        *****               *****           ***********                                     



*********************************************************************************************************************************** 

LGC2 v2.7.beta_5_GaussNewtonArmijoLineSearch, compiled on Oct 16 2023
ATTENTION: EXPERIMENTAL VERSION using a basic linesearch for more robust behavior with respect to provisional values.
Copyright 2022, CERN SU. All rights reserved.
*********************************************************************************************************************************** 
Input by Tsunami 9.99.99 for goto computation

CALCUL DU 2024-01-22 14:40:35 (UTC +0100). PROCESSING ELAPSED SECONDS 0.0067276
*********************************************************************************************************************************** 

DATA SET -  INFO GENERAL:

	FRAMES :1

	POINTS : 3
	 INCONNUES INTRODUITES POINTS: 0

	ANGLES : 1
	 INCONNUES INTRODUITES ANGLES: 1

	DISTANCES : 2
	 INCONNUES INTRODUITES DISTANCES: 0

POINTS : 

	LECTURE DES POINTS DE CALAGE : 3

MESURES :

	LECTURE DES ANGLES HORIZONTAUX (ANGL) : 2
	LECTURE DES DISTANCES ZENITHALES (ZEND) : 1
	LECTURE DES DISTANCES MESUREES (DIST) : 1

*** STATISTIQUES ***

	NOMBRE D'OBSERVATIONS =  4
	NOMBRE D'INCONNUES =     1
	NOMBRE DE CONTRAINTES =  0
	NOMBRE D'ITERATIONS =    1

SIGMA ZERO A POSTERIORI =2.7169023, VALEUR CRITIQUE = ( 0.26820,  1.76526)
LES ECARTS-TYPES SONT CALCULES PAR RAPPORT AU SIGMA ZERO A POSTERIORI



FRAME	ROOT  ID(1)           

POINTS DE CALAGE   (NB. = 3,  REFERENTIEL = RS2K )
SFP                         NOM               X                Y                Z                H         SX         SY         SZ         DX         DY         DZ    
                                             (M)              (M)              (M)              (M)       (MM)       (MM)       (MM)       (MM)       (MM)       (MM)   

                          GHOST     4320.1695600     4739.3887700     2332.1209890      333.0778400                                                                     
                LHC.MQML.10R8.S     4320.1695600     4639.3887700     2332.1618306      333.0778400                                                                     
       FORGOTOOF_LHC.MB.A11R8.E     4318.2681500     4635.1482000     2332.1768703      333.0905100                                                                     

SFP = Sub-Frame Point; * = TRUE


*** RESUME DES MESURES ***

ANGL                        
	TSTN_POS                     RES_MAX    RES_MIN    RES_MOY   ECART_TYPE   
	                                (CC)       (CC)       (CC)       (CC)   
	LHC.MQML.10R8.S             1006309.5392    -0.0000   503154.7696   711568.2992   
RESIDU MOYEN =  503154.7696    CC :  LIMITES DE CONFIANCE A 95.0 = (-6393187.5167, 6393187.5167)
ECART-TYPE   =  711568.2992    CC :  LIMITES DE CONFIANCE A 95.0 = (317465.6167, 22706257.8143)   

ZEND                        
	TSTN_POS                     RES_MAX    RES_MIN    RES_MOY   ECART_TYPE   
	                                (CC)       (CC)       (CC)       (CC)   
	LHC.MQML.10R8.S             1024252.8728   1024252.8728   1024252.8728     0.0000   
RESIDU MOYEN =  1024252.8728    CC :  LIMITES DE CONFIANCE A 95.0 = (0.0000, 0.0000)
ECART-TYPE   =    0.0000    CC :  LIMITES DE CONFIANCE A 95.0 = (0.0000, 0.0000)   

DIST                        
	TSTN_POS                     RES_MAX    RES_MIN    RES_MOY   ECART_TYPE   
	                                (MM)       (MM)       (MM)       (MM)   
	LHC.MQML.10R8.S             4650.7242   4650.7242   4650.7242     0.0000   
RESIDU MOYEN =  4650.7242    MM :  LIMITES DE CONFIANCE A 95.0 = (0.0000, 0.0000)
ECART-TYPE   =    0.0000    MM :  LIMITES DE CONFIANCE A 95.0 = (0.0000, 0.0000)   



*** MESURES ***


	INSTRUMENT POLAIRE: T2_100106   
	POSITION                    LHC.MQML.10R8.S             HI (M)              0.2598000   SHI (MM)     0.0000           ROT3D   FALSE           
	ROM                                                     ACST (GON)          0.0000000   V0 (GON)          126.2032710   SV0 (CC)     19.290   

		ANGLES HORIZONTAUX (ANGL)   (NB. = 2 )   
		POSITION                          OBSERVE      SIGMA         CALCULE     RESIDU      ECART        RES                        TRGT       OBSE       TCSE   
		                                    (GON)       (CC)           (GON)       (CC)       (MM)       /SIG                                   (CC)       (MM)   
		GHOST                         273.7967382      7.100     273.7967382     -0.000    -0.0000      -0.00                       AGA_3      7.100     0.0000      
		FORGOTOOF_LHC.MB.A11R8.E        0.0000000   2000000.000     100.6309539   1006309.539   7346.1271       0.50                       AGA_3   2000000.000     0.0000      

		DISTANCES ZENITHALES (ZEND)   (NB. = 1 )   
		POSITION                          OBSERVE      SIGMA         CALCULE     RESIDU      ECART        RES                        TRGT          H_TRGT       OBSE       TCSE       THSE   
		                                    (GON)       (CC)           (GON)       (CC)       (MM)       /SIG                                         (M)       (CC)       (MM)       (MM)   
		FORGOTOOF_LHC.MB.A11R8.E        0.0000000   2000000.000     102.4252873   1024252.873   7477.1146      0.512                       AGA_3       0.0700000   2000000.000     0.0000     0.0000      

		DISTANCES MESUREES (DIST)   (NB. = 1 )   
		POSITION                          OBSERVE      SIGMA         CALCULE     RESIDU      SENSI        RES           CONST     SCONST                        TRGT          H_TRGT       OBSE        PPM       TCSE       THSE   
		                                      (M)       (MM)             (M)       (MM)    (MM/CM)       /SIG             (M)       (MM)                                         (M)       (MM)    (MM/KM)       (MM)       (MM)   
		FORGOTOOF_LHC.MB.A11R8.E        0.0000000   1000.0000       4.6507242   4650.7242    -0.3758     4.6507       0.0000000      FIXED                       AGA_3       0.0700000   1000.0000     5.0000     0.0000     0.0000      

*** FIN DE FICHIER ***
