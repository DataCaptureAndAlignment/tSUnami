﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using SWA = System.Windows.Automation;
using TSU;
using System.Diagnostics.Eventing.Reader;
using TSU.Tools;
using System.Reflection;
using System.Linq;
using TSU.Views.Message;

namespace Functional_Tests
{
    [TestClass()]
    public class Functional
    {
        [TestMethod()]
        public void L0_START_STEP1_DetectWrongResolution_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity d3p1:type=""MouseActivity"" Pos.=""1"" Delay=""286"" Button=""Left"" Direction=""Down"" Xpos=""325"" Ypos=""124"" Delta=""0"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
    <IInputActivity d3p1:type=""MouseActivity"" Pos.=""2"" Delay=""200"" Button=""Left"" Direction=""Up"" Xpos=""325"" Ypos=""124"" Delta=""0"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
    <IInputActivity d3p1:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: TSUNAMI (beginning)"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
    <IInputActivity d3p1:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: TSUNAMI end"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                //new MessageInput(MessageType.Bug, "ouille!").Show();
            });
        }

        [TestMethod()]
        public void L0_START_STEP2_RunTsunamiInAThread_Test()
        {
            Tools.Artefact.GetArtefactFullPath(nameof(Functional.L0_START_STEP1_DetectWrongResolution_Test) + ".tsut"); // used to fail if resolution is wrong
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity d3p1:type=""MouseActivity"" Pos.=""1"" Delay=""200"" Button=""Left"" Direction=""Down"" Xpos=""325"" Ypos=""124"" Delta=""0"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
    <IInputActivity d3p1:type=""MouseActivity"" Pos.=""2"" Delay=""200"" Button=""Left"" Direction=""Up"" Xpos=""325"" Ypos=""124"" Delta=""0"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
    <IInputActivity d3p1:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: TSUNAMI (beginning)"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
    <IInputActivity d3p1:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: TSUNAMI end"" xmlns:d3p1=""http://www.w3.org/2001/XMLSchema-instance"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L0_START_STEP3_SplashScreen_Errors_Test()
        {
            Tools.Artefact.GetArtefactFullPath(nameof(Functional.L0_START_STEP2_RunTsunamiInAThread_Test) + ".tsut"); // used to fail if the splashscreen test had an error
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""746"" Button=""Left"" Direction=""Down"" Xpos=""313"" Ypos=""130"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""105"" Button=""Left"" Direction=""Up"" Xpos=""313"" Ypos=""130"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: TSUNAMI (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: TSUNAMI end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""194"" Button=""Left"" Direction=""Down"" Xpos=""456"" Ypos=""627"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""145"" Button=""Left"" Direction=""Up"" Xpos=""456"" Ypos=""627"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Small Tools / Links / Gadgets (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Small Tools / Links / Gadgets end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""363"" Button=""Left"" Direction=""Down"" Xpos=""577"" Ypos=""331"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""105"" Button=""Left"" Direction=""Up"" Xpos=""577"" Ypos=""331"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Log (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Log end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""553"" Button=""Left"" Direction=""Down"" Xpos=""978"" Ypos=""682"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""130"" Button=""Left"" Direction=""Up"" Xpos=""978"" Ypos=""682"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""MB: Log: Close"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                string message = "ERRR" + TSU.Logs.Log.ToString();
                Assert.IsTrue(tsunami.View.SplashScreenView.problemFound != true, message);
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

       
        [TestMethod()]
        public void L0_START_STEP4_Select_a_module_with_Mouse_Test()
        {
            Tools.Artefact.GetArtefactFullPath(nameof(Functional.L0_START_STEP3_SplashScreen_Errors_Test) + ".tsut"); // used to fail if the splashscreen test had an error
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""206"" trace=""Advanced modules+"" Button=""Left"" Direction=""Down"" Xpos=""276"" Ypos=""355"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""96"" trace=""Advanced modules+"" Button=""Left"" Direction=""Up"" Xpos=""276"" Ypos=""355"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Advanced modules (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Advanced modules end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""297"" trace=""Polar Module+"" Button=""Left"" Direction=""Down"" Xpos=""413"" Ypos=""381"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""98"" trace=""Polar Module+"" Button=""Left"" Direction=""Up"" Xpos=""413"" Ypos=""381"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Polar Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Polar Module end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var mms = tsunami.MeasurementModules;
                Assert.IsTrue(mms.Count > 0, "No Module Detected, Is there a windows view open on the gitlab runner running the functional test?");
            });
        }

        [TestMethod()]
        public void L0_START_STEP5_Select_a_module_with_keyboard_Test()
        {
            Tools.Artefact.GetArtefactFullPath("L0_START_STEP4_Select_a_module_with_Mouse_Test.tsut"); // used to fail if the splashscreen test had an error
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                MacroXmlContent = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""L2_P_NF_Step1_Administrative_setup_with_T2_Test"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""1"" Delay=""1090"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""+{ESC}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""117"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""+"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""1"" Delay=""149"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{DOWN}"" Target=""Button"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""105"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{DOWN}"" Target=""BigButton"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""105"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{DOWN}"" Target=""BigButton"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""4"" Delay=""125"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{DOWN}"" Target=""BigButton"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""5"" Delay=""101"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{RIGHT}"" Target=""BigButton"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Advanced modules (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Advanced modules end"" />

    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""105"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{RIGHT}"" Target=""BigButton"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Polar Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Polar Module end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var mms = tsunami.MeasurementModules;
                Assert.IsTrue(mms.Count > 0, "No Module Open");
            });
        }
    }
}
