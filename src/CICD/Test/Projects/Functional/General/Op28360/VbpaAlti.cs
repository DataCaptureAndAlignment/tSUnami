﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using TSU.Level;

namespace Functional_Tests.General.Op28360
{
    [TestClass]
    public class VbpaAlti
    {
        private static void CreateGeodeInputDatFile(string name)
        {
            string xmlContent = @"P42       ;QNL.X0430710.E                  ;  707.47500;  796.13876; 5290.75142;2447.75926;448.67225;  .355;  .000000;  .009149;  8.6930; 1;A;22-NOV-2024;  ;;Mise à jour position - ;
P42       ;QNL.X0430710.S                  ;  710.26500;  796.51853; 5293.51534;2447.78478;448.69909; 2.990;  .000000;  .009149;  8.6930; 1;O;22-NOV-2024;  ;;Mise à jour position - ;
P42       ;QNL.X0430714.E                  ;  711.52000;  796.68936; 5294.75860;2447.79627;448.71117; 1.055;  .000000;  .009149;  8.6930; 1;A;22-NOV-2024;  ;;Mise à jour position - ;
P42       ;QNL.X0430714.S                  ;  714.31000;  797.06913; 5297.52252;2447.82179;448.73801; 2.990;  .000000;  .009149;  8.6930; 1;O;22-NOV-2024;  ;;Mise à jour position - ;
P42       ;MBNH.X0430718.E                 ;  716.32999;  797.34437; 5299.52479;2447.71028;448.62745;  .565;  .000041;  .009148;  8.4655; 1;A;22-NOV-2024;  ;;Correction of RST parameters taking out the aperture optimization;
P42       ;MBNH.X0430718.S                 ;  720.12999;  797.84817; 5303.29109;2447.74504;448.66401; 5.000;  .000041;  .009148;  8.4655; 1;O;22-NOV-2024;  ;;Correction of RST parameters taking out the aperture optimization;
BEAM_P42       ;QNL.X0430714.E                  ;  711.42000;  796.67648; 5294.66396;2447.30537;448.22023;;;  .009149;  8.6930;;;22-NOV-2024; ;;coordonnées théoriques dans le système CCS au 22-NOV-2024 14:06:51;
BEAM_P42       ;QNL.X0430714.S                  ;  714.41000;  797.08348; 5297.62601;2447.33273;448.24899;;;  .009149;  8.6930;;;22-NOV-2024; ;;coordonnées théoriques dans le système CCS au 22-NOV-2024 14:06:51;
*FRAME RSTI_P42.QNL.X0430714    796.676482   5294.663962   2447.305372 0 0    8.692953 1
*FRAME RST_P42.QNL.X0430714 0 0 0 -.58244342974 .01626028758 0 1
*FRAME RSTRI_P42.QNL.X0430714     -0.000066      0.000332      0.001184 0 0 .0019801217 1 TX TY TZ RX RZ 
*FRAME RSTR_P42.QNL.X0430714 0 0 0 0 0 0 1 RY
*CALA
BEAM_P42.QNL.X0430714.E                                          0.000000      0.000000      0.000000   $711.420 158408 coordonnées théoriques dans le système RST au 22-NOV-2024 14:06:51
BEAM_P42.QNL.X0430714.S                                          0.000000      2.990000      0.000001   $714.410 158409 coordonnées théoriques dans le système RST au 22-NOV-2024 14:06:51
*OBSXYZ
P42.QNL.X0430714.E      0.000000      0.100000      0.490000 0.1 0.1 0.1   $711.520 183366 paramètres RST 22-NOV-2024 14:06:51
P42.QNL.X0430714.S      0.000000      2.890000      0.490000 0.1 0.1 0.1   $714.310 183383 paramètres RST 22-NOV-2024 14:06:51
*INCLY Instr_Roll_Theo
P42.QNL.X0430714.E 0 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_P42       ;QNL.X0430710.E                  ;  707.37500;  796.12588; 5290.65678;2447.26836;448.18131;;;  .009149;  8.6930;;;22-NOV-2024; ;;coordonnées théoriques dans le système CCS au 22-NOV-2024 14:06:51;
BEAM_P42       ;QNL.X0430710.S                  ;  710.36500;  796.53288; 5293.61883;2447.29572;448.21008;;;  .009149;  8.6930;;;22-NOV-2024; ;;coordonnées théoriques dans le système CCS au 22-NOV-2024 14:06:51;
*FRAME RSTI_P42.QNL.X0430710    796.125881   5290.656782   2447.268363 0 0    8.692953 1
*FRAME RST_P42.QNL.X0430710 0 0 0 -.58244342974 .01626028758 0 1
*FRAME RSTRI_P42.QNL.X0430710     -0.000118     -0.000013      0.001175 .00048970752 0 -.00014904142 1 TX TY TZ RX RZ 
*FRAME RSTR_P42.QNL.X0430710 0 0 0 0 0 0 1 RY
*CALA
BEAM_P42.QNL.X0430710.E                                          0.000000      0.000000      0.000000   $707.375 811527 coordonnées théoriques dans le système RST au 22-NOV-2024 14:06:52
BEAM_P42.QNL.X0430710.S                                          0.000000      2.990000      0.000001   $710.365 811528 coordonnées théoriques dans le système RST au 22-NOV-2024 14:06:52
*OBSXYZ
P42.QNL.X0430710.E      0.000000      0.100000      0.490000 0.1 0.1 0.1   $707.475 811531 paramètres RST 22-NOV-2024 14:06:52
P42.QNL.X0430710.S      0.000000      2.890000      0.490000 0.1 0.1 0.1   $710.265 811532 paramètres RST 22-NOV-2024 14:06:52
*INCLY Instr_Roll_Theo
P42.QNL.X0430710.E 0 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_P42       ;MBNH.X0430718.E                 ;  715.73000;  797.26537; 5298.93336;2447.34481;448.26169;;;  .009148;  8.4655;;;22-NOV-2024; ;;coordonnées bumpées dans le système CCS au 22-NOV-2024 14:06:51;
BEAM_P42       ;MBNH.X0430718.S                 ;  720.73000;  797.92826; 5303.88901;2447.39055;448.30980;;;  .009148;  8.4655;;;22-NOV-2024; ;;coordonnées bumpées dans le système CCS au 22-NOV-2024 14:06:51;
*FRAME RSTI_P42.MBNH.X0430718    797.263154   5298.933659   2447.344804 0 0    8.692953 1
*FRAME RST_P42.MBNH.X0430718 0 0 0 -.58244342974 .01666664198 -.22744832917 1
*FRAME RSTBI1_P42.MBNH.X0430718      0.002233      0.000000      0.000000 0 0 0 1
*FRAME RSTBI2_P42.MBNH.X0430718 0 0 0 0 0 .22744832917 1
*FRAME RSTB_P42.MBNH.X0430718 0 0 0 0 0 -.22744832917 1
*FRAME RSTRI_P42.MBNH.X0430718      0.000144     -0.000086      0.001183 .00128597194 0 -.00036923947 1 TX TY TZ RX RZ 
*FRAME RSTR_P42.MBNH.X0430718 0 0 0 0 0 0 1 RY
*CALA
BEAM_P42.MBNH.X0430718.E                                         0.000000      0.000000      0.000000   $715.730 158412 coordonnées théoriques dans le système RST au 22-NOV-2024 14:06:52
BEAM_P42.MBNH.X0430718.S                                         0.000000      5.000000      0.000001   $720.730 158413 coordonnées théoriques dans le système RST au 22-NOV-2024 14:06:52
*OBSXYZ
P42.MBNH.X0430718.E      0.000000      0.600000      0.360000 0.1 0.1 0.1   $716.330 763365 paramètres RST 22-NOV-2024 14:06:52
P42.MBNH.X0430718.S      0.000000      4.400000      0.360000 0.1 0.1 0.1   $720.130 763366 paramètres RST 22-NOV-2024 14:06:52
*INCLY Instr_Roll_Theo
P42.MBNH.X0430718.E .00259473487 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
";
            using (StreamWriter writer = new StreamWriter($@"C:\data\tsunami\Theoretical Files\{name}.dat"))
            {
                // Write the content of the string variable to the file
                writer.WriteLine(xmlContent);
            }
        }

        private static IEnumerable<FileInfo> EnumerateAllDatFiles(F_Test test)
        {
            return new DirectoryInfo(test.FolderPath).EnumerateFiles("*.dat");
        }

        private static void DeleteAllDatFiles(F_Test test)
        {
            foreach (FileInfo f in EnumerateAllDatFiles(test))
                f.Delete();
        }

        private static string ExpectedDatFileForLevelling(string operation)
        {
            return $@"  001;RE;[0-9]{{1,2}}-[a-zA-Z]{{1,3}}-[0-9]{{1,4}};[a-zA-Z ]{{1,30}};{operation};Tsunami [0-9]{{1,2}}\.[0-9]{{1,2}}\.[0-9]{{1,2}}_{{0,1}}[a-zA-Z ]{{0,30}};
! 002 ----------STLEV_[0-9]{{1,8}}_[0-9]{{1,6}}_A_P42\.QNL\.X0430710\.E Levelling recorded at [0-9]{{1,2}}:[0-9]{{1,2}}:[0-9]{{1,2}}----------
  003;PH;NA2      ;5476556     ;;;;;;;20\.0   ;;;;;;
  004;LE;G40      ;4           ;P42       ;QNL\.X0430710\.E                   ;   15494; 0\.07000;;;;;;A;        ;        ;;A;;[0-9]{{1,2}}:[0-9]{{1,2}}:[0-9]{{1,2}};;          ;                    ;                    ;          
  005;LE;G40      ;4           ;P42       ;QNL\.X0430710\.S                   ;   12798; 0\.07000;;;;;;A;        ;        ;;A;;[0-9]{{1,2}}:[0-9]{{1,2}}:[0-9]{{1,2}};;          ;                    ;                    ;          
  006;LE;G40      ;4           ;P42       ;QNL\.X0430714\.E                   ;   11613; 0\.07000;;;;;;A;        ;        ;;A;;[0-9]{{1,2}}:[0-9]{{1,2}}:[0-9]{{1,2}};;          ;                    ;                    ;          
  007;LE;G40      ;4           ;P42       ;QNL\.X0430714\.S                   ;    8920; 0\.07000;;;;;;A;        ;        ;;A;;[0-9]{{1,2}}:[0-9]{{1,2}}:[0-9]{{1,2}};;          ;                    ;                    ;          
  008;LE;G40      ;4           ;P42       ;MBNH\.X0430718\.E                  ;   20028; 0\.07000;;;;;;A;        ;        ;;R;;[0-9]{{1,2}}:[0-9]{{1,2}}:[0-9]{{1,2}};;          ;                    ;                    ;          
  009;LE;G40      ;4           ;P42       ;MBNH\.X0430718\.S                  ;   16318; 0\.07000;;;;;;A;        ;        ;;A;;[0-9]{{1,2}}:[0-9]{{1,2}}:[0-9]{{1,2}};;          ;                    ;                    ;          
  010;EC;P42       ;QNL\.X0430710\.E                   ;          ;          ;      0\.54;          ;dr dl dh dRoll \[mm\]\|\[mrad\];
  011;EC;P42       ;QNL\.X0430710\.S                   ;          ;          ;      0\.66;          ;dr dl dh dRoll \[mm\]\|\[mrad\];
  012;EC;P42       ;QNL\.X0430714\.E                   ;          ;          ;      0\.43;          ;dr dl dh dRoll \[mm\]\|\[mrad\];
  013;EC;P42       ;QNL\.X0430714\.S                   ;          ;          ;      0\.52;          ;dr dl dh dRoll \[mm\]\|\[mrad\];
  014;EC;P42       ;MBNH\.X0430718\.S                  ;          ;          ;      0\.54;          ;dr dl dh dRoll \[mm\]\|\[mrad\];
! 015 ----------STLEV_[0-9]{{1,8}}_[0-9]{{1,6}}_R_P42\.QNL\.X0430710\.E Levelling recorded at [0-9]{{1,2}}:[0-9]{{1,2}}:[0-9]{{1,2}}----------
  016;PH;NA2      ;5476556     ;;;;;;;20\.0   ;;;;;;
  017;LE;G40      ;4           ;P42       ;QNL\.X0430710\.E                   ;   13420; 0\.07000;;;;;;R;        ;        ;;A;;[0-9]{{1,2}}:[0-9]{{1,2}}:[0-9]{{1,2}};emqA= 2\.2 ;          ;                    ;                    ;          
  018;LE;G40      ;4           ;P42       ;QNL\.X0430710\.S                   ;   10722; 0\.07000;;;;;;R;        ;        ;;A;;[0-9]{{1,2}}:[0-9]{{1,2}}:[0-9]{{1,2}};emqA= 2\.2 ;          ;                    ;                    ;          
  019;LE;G40      ;4           ;P42       ;QNL\.X0430714\.E                   ;    9539; 0\.07000;;;;;;R;        ;        ;;A;;[0-9]{{1,2}}:[0-9]{{1,2}}:[0-9]{{1,2}};emqA= 2\.2 ;          ;                    ;                    ;          
  020;LE;G40      ;4           ;P42       ;QNL\.X0430714\.S                   ;    6843; 0\.07000;;;;;;R;        ;        ;;A;;[0-9]{{1,2}}:[0-9]{{1,2}}:[0-9]{{1,2}};emqA= 2\.2 ;          ;                    ;                    ;          
  021;LE;G40      ;4           ;P42       ;MBNH\.X0430718\.E                  ;   17952; 0\.07000;;;;;;R;        ;        ;;R;;[0-9]{{1,2}}:[0-9]{{1,2}}:[0-9]{{1,2}};emqA= 2\.2 ;          ;                    ;                    ;          
  022;LE;G40      ;4           ;P42       ;MBNH\.X0430718\.S                  ;   14238; 0\.07000;;;;;;R;        ;        ;;A;;[0-9]{{1,2}}:[0-9]{{1,2}}:[0-9]{{1,2}};emqA= 2\.2 ;          ;                    ;                    ;          
  023;EC;P42       ;QNL\.X0430710\.E                   ;          ;          ;      0\.52;          ;dr dl dh dRoll \[mm\]\|\[mrad\];
  024;EC;P42       ;QNL\.X0430710\.S                   ;          ;          ;      0\.66;          ;dr dl dh dRoll \[mm\]\|\[mrad\];
  025;EC;P42       ;QNL\.X0430714\.E                   ;          ;          ;      0\.41;          ;dr dl dh dRoll \[mm\]\|\[mrad\];
  026;EC;P42       ;QNL\.X0430714\.S                   ;          ;          ;      0\.53;          ;dr dl dh dRoll \[mm\]\|\[mrad\];
  027;EC;P42       ;MBNH\.X0430718\.S                  ;          ;          ;      0\.58;          ;dr dl dh dRoll \[mm\]\|\[mrad\];";
        }

        private static string ExpectedDatFileForRoll(string operation)
        {
            return $@"  001;RE;[0-9]{{1,2}}-[a-zA-Z]{{1,3}}-[0-9]{{1,4}};[a-zA-Z ]{{1,30}};{operation};Tsunami [0-9]{{1,2}}\.[0-9]{{1,2}}\.[0-9]{{1,2}}_{{0,1}}[a-zA-Z ]{{0,30}};
! 002 ----------STTilt_Ini_[0-9]{{1,8}}_[0-9]{{1,6}}_P42.QNL.X0430710.E Roll recorded at [0-9]{{1,2}}:[0-9]{{1,2}}:[0-9]{{1,2}}----------
  003;AT;JT       ;14-1  ;P42       ;QNL\.X0430710\.E                   ;  0.000000;;;;;;;;;;;;;[0-9]{{1,2}}:[0-9]{{1,2}}:[0-9]{{1,2}};B1=0\.000 I1=0\.000 mrad ;          ;                    ;                    ;          
  004;EC;P42       ;QNL\.X0430710\.E                   ;          ;          ;          ;     0.000;dr dl dh dRoll \[mm\]\|\[mrad\];";
        }

        private static string ExtractOperationNumber(string fileName)
        {
            int underscorePos = fileName.IndexOf('_');
            Assert.AreNotEqual(-1, underscorePos, "DAT File name should comtain an underscore.");
            return fileName.Substring(0, underscorePos).PadLeft(6);
        }

        [TestMethod]
        public void L1_Op28360_Step1_VbpaRoll()
        {
            CreateGeodeInputDatFile("Op28360Vbpa"); //F-test Op28360.VbpaAlti

            MethodBase testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(Functional.L0_START_STEP3_SplashScreen_Errors_Test) + ".tsuT"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
    <WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />
    <KeyStrokesAndMouseClicks>
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""100"" trace=""Advanced modules+"" Button=""Left"" Direction=""Down"" Xpos=""194"" Ypos=""359"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""10"" trace=""Advanced modules+"" Button=""Left"" Direction=""Up"" Xpos=""194"" Ypos=""359"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Advanced modules (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Advanced modules end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""100"" trace=""Roll module+"" Button=""Left"" Direction=""Down"" Xpos=""364"" Ypos=""488"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""10"" trace=""Roll module+"" Button=""Left"" Direction=""Up"" Xpos=""364"" Ypos=""488"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Roll module (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Roll module end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""100"" trace=""Roll module (Created on: )+"" Button=""Left"" Direction=""Down"" Xpos=""742"" Ypos=""153"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""10"" trace=""Roll module (Created on: )+"" Button=""Left"" Direction=""Up"" Xpos=""742"" Ypos=""153"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Roll module (Created on: 11/11/2024 10:34:46) (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Roll module (Created on: 11/11/2024 10:34:46) end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""100"" trace=""New Station+"" Button=""Left"" Direction=""Down"" Xpos=""769"" Ypos=""199"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""10"" trace=""New Station+"" Button=""Left"" Direction=""Up"" Xpos=""769"" Ypos=""199"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: New Station (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""16"" Delay=""0"" Action=""BB: New Station end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""637"" Ypos=""354"" Delta=""0""/>
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""637"" Ypos=""354"" Delta=""0""/>
        {Tools.GetKeySequence("PWT", "TextBox", 100)}
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""100"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""928"" Ypos=""589"" Delta=""0""/>
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""10"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""928"" Ypos=""589"" Delta=""0""/>
        <IInputActivity xsi:type=""Trace________"" Pos.=""27"" Delay=""0"" Action=""MB: Team: OK""/>
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""665"" Ypos=""396"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""665"" Ypos=""396"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""112"" Ypos=""303"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""112"" Ypos=""303"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""141"" Ypos=""335"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""141"" Ypos=""335"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""276"" Ypos=""370"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""276"" Ypos=""370"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""100"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""861"" Ypos=""922"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""10"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""861"" Ypos=""922"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""27"" Delay=""0"" Action=""MB: Operation Selection: Select"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""706"" Ypos=""424"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""706"" Ypos=""424"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""110"" Ypos=""335"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""110"" Ypos=""335"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""100"" trace=""Options+"" Button=""Left"" Direction=""Down"" Xpos=""1538"" Ypos=""213"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""10"" trace=""Options+"" Button=""Left"" Direction=""Up"" Xpos=""1538"" Ypos=""213"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Options (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""35"" Delay=""0"" Action=""BB: Options end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""100"" trace=""List View+"" Button=""Left"" Direction=""Down"" Xpos=""1625"" Ypos=""338"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""10"" trace=""List View+"" Button=""Left"" Direction=""Up"" Xpos=""1625"" Ypos=""338"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""BB: List View (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""39"" Delay=""0"" Action=""BB: List View end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""441"" Ypos=""266"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""441"" Ypos=""266"" Delta=""0"" />
        {Tools.GetKeySequence("JT&14", "TextBox", 100)}
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""50"" Delay=""1500"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""123"" Ypos=""352"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""51"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""123"" Ypos=""352"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""52"" Delay=""100"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""831"" Ypos=""936"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""53"" Delay=""10"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""831"" Ypos=""936"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""54"" Delay=""0"" Action=""MB: Choose an inclinometer: Select"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""55"" Delay=""100"" trace=""Roll Initial Operation+"" Button=""Left"" Direction=""Down"" Xpos=""608"" Ypos=""212"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""56"" Delay=""10"" trace=""Roll Initial Operation+"" Button=""Left"" Direction=""Up"" Xpos=""608"" Ypos=""212"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""57"" Delay=""0"" Action=""BB: Roll Initial Operation (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""58"" Delay=""0"" Action=""BB: Roll Initial Operation end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""59"" Delay=""100"" trace=""Element selection+"" Button=""Left"" Direction=""Down"" Xpos=""692"" Ypos=""238"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""60"" Delay=""10"" trace=""Element selection+"" Button=""Left"" Direction=""Up"" Xpos=""692"" Ypos=""238"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""61"" Delay=""0"" Action=""BB: Element selection (beginning)"" />
        {Tools.GetKeySequence("Op28360Vbpa.dat", "TextBox", 100)}
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""70"" Delay=""100"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""TextBox"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""71"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""420"" Ypos=""179"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""72"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""420"" Ypos=""179"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""73"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""420"" Ypos=""179"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""74"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""420"" Ypos=""179"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""75"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""64"" Ypos=""331"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""76"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""64"" Ypos=""331"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""77"" Delay=""100"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""870"" Ypos=""949"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""78"" Delay=""10"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""870"" Ypos=""949"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""79"" Delay=""0"" Action=""MB: Magnets: Select"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""80"" Delay=""0"" Action=""BB: Element selection end"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""81"" Delay=""100"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""82"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1165"" Ypos=""569"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""83"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1165"" Ypos=""569"" Delta=""0"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""84"" Delay=""100"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""85"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1211"" Ypos=""576"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""86"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1211"" Ypos=""576"" Delta=""0"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""87"" Delay=""100"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""88"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1685"" Ypos=""576"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""89"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1685"" Ypos=""576"" Delta=""0"" />
    </KeyStrokesAndMouseClicks>
</Macro>"
            };

            DeleteAllDatFiles(test);

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsNotNull(tsunami, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.MeasurementModules.Count > 0, "No measurement modules found!");

                TSU.Tilt.Module mod = tsunami.MeasurementModules[0] as TSU.Tilt.Module;
                Assert.IsNotNull(mod, $"The 1st measurement module is not Advanced Roll !");

                Assert.IsTrue(mod.StationModules.Count > 0, $"No station module in measurement modules {tsunami.MeasurementModules} found!");

                TSU.Tilt.Station.Module module = mod.StationModules[0] as TSU.Tilt.Station.Module;

                var init = module._InitStationTilt;
                string instrumentModel = init.ParametersBasic._Instrument._Model;
                Assert.IsTrue(instrumentModel == "JT", $"Wrong instrument selection for initial station, {instrumentModel} instead of JT");

                FileInfo datFile = EnumerateAllDatFiles(test).FirstOrDefault();
                Assert.IsNotNull(datFile, $"Geode export file not found");

                string operation = ExtractOperationNumber(datFile.Name);
                string[] laExpected = Tools.SplitLines(ExpectedDatFileForRoll(operation));
                string[] laResult = File.ReadAllLines(datFile.FullName);
                Assert.IsTrue(laResult != null && laResult.Length > 0, $"Geode export file empty or not read");
                Tools.AssertLinesArraysMatchLineByLine(laExpected, laResult);

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod]
        public void L1_Op28360_Step2_VbpaLevelling()
        {
            CreateGeodeInputDatFile("Op28360Vbpa"); //F-test Op28360.VbpaAlti

            MethodBase testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L1_Op28360_Step1_VbpaRoll) + ".tsuT"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
    <WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
    <KeyStrokesAndMouseClicks>
        <IInputActivity xsi:type=""MouseActivity"" Delay=""100"" trace=""Advanced modules+"" Button=""Left"" Direction=""Down"" Xpos=""357"" Ypos=""353"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Delay=""10"" trace=""Advanced modules+"" Button=""Left"" Direction=""Up"" Xpos=""357"" Ypos=""353"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Delay=""0"" Action=""BB: Advanced modules (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Delay=""0"" Action=""BB: Advanced modules end"" />
        <IInputActivity xsi:type=""MouseActivity"" Delay=""100"" trace=""Levelling Module+"" Button=""Left"" Direction=""Down"" Xpos=""470"" Ypos=""573"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Delay=""10"" trace=""Levelling Module+"" Button=""Left"" Direction=""Up"" Xpos=""470"" Ypos=""573"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Delay=""0"" Action=""BB: Levelling Module (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Delay=""0"" Action=""BB: Levelling Module end"" />
        <IInputActivity xsi:type=""MouseActivity"" Delay=""100"" trace=""Levelling Module (Created on: )+"" Button=""Left"" Direction=""Down"" Xpos=""720"" Ypos=""96"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""10"" trace=""Levelling Module (Created on: )+"" Button=""Left"" Direction=""Up"" Xpos=""720"" Ypos=""96"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Levelling Module (Created on: 6/11/2024 11:36:48) (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Levelling Module (Created on: 6/11/2024 11:36:48) end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""100"" trace=""New station+"" Button=""Left"" Direction=""Down"" Xpos=""799"" Ypos=""134"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""10"" trace=""New station+"" Button=""Left"" Direction=""Up"" Xpos=""799"" Ypos=""134"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: New station (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""16"" Delay=""0"" Action=""BB: New station end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""649"" Ypos=""389"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""649"" Ypos=""389"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""110"" Ypos=""302"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""110"" Ypos=""302"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""140"" Ypos=""339"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""140"" Ypos=""339"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""243"" Ypos=""361"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""243"" Ypos=""361"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""100"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""845"" Ypos=""944"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""10"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""845"" Ypos=""944"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""27"" Delay=""0"" Action=""MB: Operation Selection: Select"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""684"" Ypos=""422"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""684"" Ypos=""422"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""100"" trace=""Options+"" Button=""Left"" Direction=""Down"" Xpos=""1480"" Ypos=""212"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""10"" trace=""Options+"" Button=""Left"" Direction=""Up"" Xpos=""1480"" Ypos=""212"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""33"" Delay=""0"" Action=""BB: Options (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Options end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""100"" trace=""List View+"" Button=""Left"" Direction=""Down"" Xpos=""1534"" Ypos=""329"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""10"" trace=""List View+"" Button=""Left"" Direction=""Up"" Xpos=""1534"" Ypos=""329"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""BB: List View (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""BB: List View end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""750"" Ypos=""269"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""750"" Ypos=""269"" Delta=""0"" />
        {Tools.GetKeySequence("NA2&5476556", "TextBox", 100)}
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""54"" Delay=""1000"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""117"" Ypos=""323"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""55"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""117"" Ypos=""323"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""56"" Delay=""100"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""877"" Ypos=""956"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""57"" Delay=""10"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""877"" Ypos=""956"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""58"" Delay=""0"" Action=""MB: Choose a level: Select"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""59"" Delay=""100"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{DELETE}}"" Target=""TextBox"" />
        {Tools.GetKeySequence("G40", "TextBox", 100)}
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""69"" Delay=""1000"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""252"" Ypos=""387"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""70"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""252"" Ypos=""387"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""71"" Delay=""100"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""937"" Ypos=""930"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""72"" Delay=""10"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""937"" Ypos=""930"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""73"" Delay=""0"" Action=""MB: Choose a default main staff: Select"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""74"" Delay=""100"" trace=""button2+Cancel"" Button=""Left"" Direction=""Down"" Xpos=""996"" Ypos=""938"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""75"" Delay=""10"" trace=""button2+Cancel"" Button=""Left"" Direction=""Up"" Xpos=""996"" Ypos=""938"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""76"" Delay=""0"" Action=""MB: Choose an optional secondary staff: Cancel"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""47"" Delay=""100"" trace=""Levelling station+"" Button=""Left"" Direction=""Down"" Xpos=""590"" Ypos=""226"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""48"" Delay=""10"" trace=""Levelling station+"" Button=""Left"" Direction=""Up"" Xpos=""590"" Ypos=""226"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""49"" Delay=""0"" Action=""BB: Outward run station  (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""50"" Delay=""0"" Action=""BB: Outward run station  end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""51"" Delay=""100"" trace=""Select element+"" Button=""Left"" Direction=""Down"" Xpos=""667"" Ypos=""264"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""52"" Delay=""10"" trace=""Select element+"" Button=""Left"" Direction=""Up"" Xpos=""667"" Ypos=""264"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""53"" Delay=""0"" Action=""BB: Select element (beginning)"" />
        {Tools.GetKeySequence("Op28360Vbpa.dat", "TextBox", 100)}
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""100"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""Button"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""54"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""462"" Ypos=""161"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""55"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""462"" Ypos=""161"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""56"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1027"" Ypos=""604"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""57"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1027"" Ypos=""604"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""58"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""61"" Ypos=""333"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""59"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""61"" Ypos=""333"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""60"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""63"" Ypos=""358"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""61"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""63"" Ypos=""358"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""62"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""71"" Ypos=""393"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""63"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""71"" Ypos=""393"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""64"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""66"" Ypos=""418"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""65"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""66"" Ypos=""418"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""66"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""66"" Ypos=""438"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""67"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""66"" Ypos=""438"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""68"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""67"" Ypos=""478"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""69"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""67"" Ypos=""478"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""70"" Delay=""100"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""841"" Ypos=""949"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""71"" Delay=""10"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""841"" Ypos=""949"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""72"" Delay=""0"" Action=""MB: Elements Selection: Select"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""73"" Delay=""0"" Action=""BB: Select element end"" />
        {Tools.GetKeySequence("15494", "SplitContainer", 100)}
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""77"" Delay=""100"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{DOWN}}"" Target=""SplitContainer"" />
        {Tools.GetKeySequence("12798", "SplitContainer")}
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""81"" Delay=""100"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{DOWN}}"" Target=""SplitContainer"" />
        {Tools.GetKeySequence("11613", "SplitContainer")}
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""85"" Delay=""100"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{DOWN}}"" Target=""SplitContainer"" />
        {Tools.GetKeySequence("8920", "SplitContainer")}
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""89"" Delay=""100"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{DOWN}}"" Target=""SplitContainer"" />
        {Tools.GetKeySequence("20028", "SplitContainer")}
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""93"" Delay=""100"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{DOWN}}"" Target=""SplitContainer"" />
        {Tools.GetKeySequence("16318", "SplitContainer")}
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""97"" Delay=""100"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""146"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""640"" Ypos=""700"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""147"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""640"" Ypos=""700"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""98"" Delay=""1000"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""705"" Ypos=""191"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""99"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""705"" Ypos=""191"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""100"" Delay=""0"" Action=""BB: Outward run station  (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""101"" Delay=""0"" Action=""BB: Outward run station  end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""102"" Delay=""100"" trace=""Return run station+"" Button=""Left"" Direction=""Down"" Xpos=""880"" Ypos=""931"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""103"" Delay=""10"" trace=""Return run station+"" Button=""Left"" Direction=""Up"" Xpos=""880"" Ypos=""931"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""104"" Delay=""0"" Action=""BB: Return run station (beginning)"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""105"" Delay=""100"" trace=""button2+No"" Button=""Left"" Direction=""Down"" Xpos=""1068"" Ypos=""585"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""106"" Delay=""10"" trace=""button2+No"" Button=""Left"" Direction=""Up"" Xpos=""1068"" Ypos=""585"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""107"" Delay=""0"" Action=""MB: Exporting: No"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""108"" Delay=""0"" Action=""BB: Return run station end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""109"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1096"" Ypos=""580"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""110"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1096"" Ypos=""580"" Delta=""0"" />
        {Tools.GetKeySequence("13420", "SplitContainer", 100)}
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""114"" Delay=""100"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{DOWN}}"" Target=""SplitContainer"" />
        {Tools.GetKeySequence("10722", "SplitContainer")}
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""118"" Delay=""100"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{DOWN}}"" Target=""SplitContainer"" />
        {Tools.GetKeySequence("9539", "SplitContainer")}
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""122"" Delay=""100"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{DOWN}}"" Target=""SplitContainer"" />
        {Tools.GetKeySequence("6843", "SplitContainer")}
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""126"" Delay=""100"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{DOWN}}"" Target=""SplitContainer"" />
        {Tools.GetKeySequence("17952", "SplitContainer")}
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""130"" Delay=""100"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{DOWN}}"" Target=""SplitContainer"" />
        {Tools.GetKeySequence("14238", "SplitContainer")}
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""134"" Delay=""100"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""135"" Delay=""100"" trace=""Levelling Module (Created on: ): Level station 1+"" Button=""Left"" Direction=""Down"" Xpos=""730"" Ypos=""104"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""136"" Delay=""10"" trace=""Levelling Module (Created on: ): Level station 1+"" Button=""Left"" Direction=""Up"" Xpos=""730"" Ypos=""104"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""137"" Delay=""0"" Action=""BB: Levelling Module (Created on: 6/11/2024 11:36:48): Level station 1 (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""138"" Delay=""0"" Action=""BB: Levelling Module (Created on: 6/11/2024 11:36:48): Level station 1 end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""139"" Delay=""100"" trace=""Export all levelling+"" Button=""Left"" Direction=""Down"" Xpos=""850"" Ypos=""438"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""140"" Delay=""10"" trace=""Export all levelling+"" Button=""Left"" Direction=""Up"" Xpos=""850"" Ypos=""438"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""141"" Delay=""0"" Action=""BB: Export all levelling (beginning)"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""142"" Delay=""100"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""899"" Ypos=""576"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""143"" Delay=""10"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""899"" Ypos=""576"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""144"" Delay=""0"" Action=""MB: Successfull export of all levelling: OK"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""145"" Delay=""0"" Action=""BB: Export all levelling end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""148"" Delay=""1000"" trace=""Levelling Module (Created on: ): Level station 1+"" Button=""Left"" Direction=""Down"" Xpos=""765"" Ypos=""150"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""149"" Delay=""10"" trace=""Levelling Module (Created on: ): Level station 1+"" Button=""Left"" Direction=""Up"" Xpos=""765"" Ypos=""150"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""150"" Delay=""0"" Action=""BB: Levelling Module (Created on: 25/11/2024 16:36:44): Level station 1 (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""151"" Delay=""0"" Action=""BB: Levelling Module (Created on: 25/11/2024 16:36:44): Level station 1 end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""152"" Delay=""100"" trace=""Compute Beam Offsets+"" Button=""Left"" Direction=""Down"" Xpos=""940"" Ypos=""344"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""153"" Delay=""10"" trace=""Compute Beam Offsets+"" Button=""Left"" Direction=""Up"" Xpos=""940"" Ypos=""344"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""154"" Delay=""5000"" Action=""BB: Compute Beam Offsets (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""155"" Delay=""0"" Action=""BB: Compute Beam Offsets end"" />
    </KeyStrokesAndMouseClicks>                               
</Macro>"
            };

            DeleteAllDatFiles(test);

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsNotNull(tsunami, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.MeasurementModules.Count > 1, "{tsunami.MeasurementModules.Count} measurement module found instead of 2 !");

                TSU.Level.Module mod = tsunami.MeasurementModules.OfType<TSU.Level.Module>().FirstOrDefault();
                Assert.IsNotNull(mod, $"No Advanced Levelling Module Found !");

                Assert.IsTrue(mod.StationModules.Count > 0, $"No station module in measurement modules {tsunami.MeasurementModules} found!");

                Station.Module module = mod.StationModules[0] as Station.Module;
                string staffModel = module._defaultStaff._Model;
                Assert.IsTrue(staffModel == "G40", $"Wrong staff selection for forward station, {staffModel} instead of G40");

                Station aller = module._AllerStationLevel;
                string instrumentModel = aller.ParametersBasic._Instrument._Model;
                Assert.IsTrue(instrumentModel == "NA2", $"Wrong instrument selection for forward station, {instrumentModel} instead of NA2");

                Station retour = module._RetourStationLevel;
                instrumentModel = retour.ParametersBasic._Instrument._Model;
                Assert.IsTrue(instrumentModel == "NA2", $"Wrong instrument selection for backward station, {instrumentModel} instead of NA2");

                FileInfo datFile = EnumerateAllDatFiles(test).FirstOrDefault();
                Assert.IsNotNull(datFile, $"Geode export file not found");

                string operation = ExtractOperationNumber(datFile.Name);

                string[] laExpected = Tools.SplitLines(ExpectedDatFileForLevelling(operation));
                string[] laResult = File.ReadAllLines(datFile.FullName);
                Assert.IsTrue(laResult != null && laResult.Length > 0, $"Geode export file empty or not read");
                Tools.AssertLinesArraysMatchLineByLine(laExpected, laResult);

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }
    }
}