﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using SWA = System.Windows.Automation;
using TSU;
using System.Diagnostics.Eventing.Reader;
using TSU.Tools;
using System.Reflection;
using System.Linq;

namespace Functional_Tests
{
    [TestClass()]
    public class Functional
    {
        /// <summary>
        /// how to do to create a new one:
        /// 1. Create a folder with the name of the test (i.e. XML/Test1/)
        /// 2. Create an optional xml file if the test starts from a existing .tsu. Name = [test-name].tsu.xml (i.e. XML/Test1/Test1.tsu.xml)
        /// 3. Create a xml file containing the macro. Name = [test-name].macro.xml (i.e. XML/Test1/Test1.macro.xml)
        /// 4. add the 2 files to the solution and set 'copy to out put directory' to 'always' or 'if newer'.
        /// 5. Create a [TestMethod()] in this file, with the test name. (i.e. Test1_Test)
        /// 6. Insert the test assert as parameter of the TestTsunamiWithMacro() method.
        /// 
        /// Most can be skipped of your duplicate the 'Template_Test'.
        /// </summary>
        /// 


        [TestMethod()]
        public void Polar__New_Frame__Run_LGC_Test()
        {
            Tools.TestTsunamiWithMacro( methodName: MethodBase.GetCurrentMethod(),  testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.Points != null, "TSUNAMI.Points  is NULL");
                Assert.IsTrue(tsunami.Points.Count > 3, "TSUNAMI.Points do not have enough Points");
                Assert.IsTrue(tsunami.Points[4]._Coordinates != null, "TSUNAMI.Points[4] do not have coordinates");
                Assert.IsTrue(tsunami.Points[4]._Coordinates.HasSu, "TSUNAMI.Points[4] do not have SU coordinates");
                Assert.AreEqual(10, tsunami.Points[4]._Coordinates.Su.Y.Value, 0.001, $"TSUNAMI.Points[4].SU.Y.Value is {tsunami.Points[4]._Coordinates.Su.Y.Value} instead of 10.0");
            });
        }

        [TestMethod()]
        public void Polar__New_Frame__T2_Test()
        {
            Tools.TestTsunamiWithMacro(methodName: MethodBase.GetCurrentMethod(), testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.Points != null, "TSUNAMI.Points  is NULL");
                Assert.IsTrue(tsunami.Points.Count > 3, "TSUNAMI.Points do not have enough Points");
                Assert.IsTrue(tsunami.Points[4]._Coordinates != null, "TSUNAMI.Points[4] do not have coordinates");
                Assert.IsTrue(tsunami.Points[4]._Coordinates.HasSu, "TSUNAMI.Points[4] do not have SU coordinates");
                Assert.AreEqual(0, tsunami.Points[4]._Coordinates.Su.Y.Value, 0.001, $"TSUNAMI.Points[4].SU.Y.Value is {tsunami.Points[4]._Coordinates.Su.Y.Value} instead of 10.0" );
            });
        }

        [TestMethod()]
        public void Start__RunTsunamiInAThread_Test()
        {
            Tools.TestTsunamiWithMacro(methodName: MethodBase.GetCurrentMethod(), testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
            });
        }

        [TestMethod()]
        public void Start__SplashScreen_Show_Test()
        {
            Tools.TestTsunamiWithMacro(methodName: MethodBase.GetCurrentMethod(), testAction: (tsunami) =>
            {
                string message = "ERRR" + TSU.Logs.Log.ToString();
                Assert.IsTrue(tsunami.View.SplashScreenView.problemFound != true, message);
            });
        }

        [TestMethod()]
        public void BOC__GEM__T2_Test()
        {
            Tools.TestTsunamiWithMacro(methodName: MethodBase.GetCurrentMethod(), testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var boc = tsunami.BOC_Results;
                Assert.IsTrue(boc != null, "TSUNAMI.BOC_Results  is NULL");
                Assert.IsTrue(boc.Count > 0, "TSUNAMI.Points do not have enough BOCr");
                var BEAM_LHC_B2_MB_A11R8_E = boc[0].Points.Find(x => x._Name == "BEAM_LHC.B2.MB.A11R8.E");
                Assert.IsTrue(BEAM_LHC_B2_MB_A11R8_E != null, "BEAM_LHC_B2_MB_A11R8_E is not found n the boc results");
                Assert.AreEqual(0.0, BEAM_LHC_B2_MB_A11R8_E._Coordinates.Beam.X.Value, 0.0001, "BEAM_LHC_B2_MB_A11R8_E  Xsu is not as expected");
            });
        }

        [TestMethod()]
        public void BOC__GEM__T2_Compute_Only_Test()
        {
            Tools.TestTsunamiWithMacro(methodName: MethodBase.GetCurrentMethod(), testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var boc = tsunami.BOC_Results;
                Assert.IsTrue(boc != null, "TSUNAMI.BOC_Results  is NULL");
                Assert.IsTrue(boc.Count > 0, "TSUNAMI.Points do not have enough BOCr");
                var BEAM_LHC_B2_MB_A11R8_E = boc[0].Points.Find(x => x._Name == "BEAM_LHC.B2.MB.A11R8.E");
                Assert.IsTrue(BEAM_LHC_B2_MB_A11R8_E != null, "BEAM_LHC_B2_MB_A11R8_E is not found n the boc results");
                Assert.AreEqual(0.0, BEAM_LHC_B2_MB_A11R8_E._Coordinates.Beam.X.Value, 0.0001, "BEAM_LHC_B2_MB_A11R8_E  Xsu is not as expected");
            });
        }
    }

}
