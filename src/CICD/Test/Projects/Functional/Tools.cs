﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using TSU;
using TSU.Common;
using TSU.Logs;
using TSU.Views.Message;
using static TSU.Tools.Macro;

namespace Functional_Tests
{
    public class F_Test
    {
        public string FolderPath = "";
        public string TsuXmlContent = "";
        public string TsuPath = "";
        public string MacroPath = "";
        public MethodBase methodBase;
        bool readyToAssert = false; // use to check that the tsu and macro are donr before asserting
        public Action FinalAction = null;

        public string FuturTemplatePath
        {
            get
            {
                return GetFullCurrentWantedPath($"{methodBase.Name}.tsut");
            }

        }

        Tools.Logger logger = null;

        public bool JustRunAssertOnExistingTsuT { get; internal set; } = false;
        public bool NewPlacementForMessage { get; set; } = false;
        private string macroXmlContent = "";
        public string MacroXmlContent
        {
            get => FormatMacroXml(macroXmlContent);
            set => macroXmlContent = value;
        }

        private static string FormatMacroXml(string macroXmlContent)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(macroXmlContent);

            XmlNodeList childNodes = doc.ChildNodes;
            int count = 0;
            Recount(childNodes, ref count);

            StringWriter s = new StringWriter();
            XmlTextWriter w = new XmlTextWriter(s);
            w.Formatting = Formatting.Indented;
            doc.Save(w);
            w.Flush();
            s.Flush();
            return s.ToString();
        }

        private static void Recount(XmlNodeList childNodes, ref int count)
        {
            foreach (XmlNode node in childNodes)
            {
                if (node.NodeType == XmlNodeType.Element && node.Name == "IInputActivity")
                {
                    count++;
                    XmlAttribute xmlAttribute = node.Attributes["Pos."];

                    if (xmlAttribute == null)
                    {
                        xmlAttribute = node.OwnerDocument.CreateAttribute("Pos.");
                        node.Attributes.Append(xmlAttribute);
                    }

                    xmlAttribute.Value = count.ToString();
                }

                Recount(node.ChildNodes, ref count);
            }
        }

        public F_Test(MethodBase methodBase)
        {
            this.methodBase = methodBase;
            FolderPath = Tools.GetNamesAndPath(methodBase);
            logger = new Tools.Logger(Path.Combine(FolderPath + "\\log.txt"));
            logger.Log("Logger Created");
        }

        public void Execute(Action<Tsunami> testAction)
        {
            // we want test to run in default UI
            if (File.Exists(@"C:\data\tsunami\Preferences\GuiPreferences_for_v2.xml"))
                File.Delete(@"C:\data\tsunami\Preferences\GuiPreferences_for_v2.xml");

            if (NewPlacementForMessage)
                MessageTsu.PositionOnContainerViewInTest = true;
            else
                MessageTsu.PositionOnContainerViewInTest = false;

            logger.Log("F-test execution started");
            Tsunami tsunami = null;
            Thread tsunamiThread = null;
            readyToAssert = false;
            try
            {
                bool skipTheMacro = JustRunAssertOnExistingTsuT && File.Exists(FuturTemplatePath);
                try
                {
                    if (skipTheMacro)
                        StartTsunamiOpenExistingTsu(out tsunami, out tsunamiThread, FuturTemplatePath);
                    else
                        StartTsunamiOpenTsuRunMacro(out tsunami, out tsunamiThread);
                }
                catch (Exception ex)
                {
                    var message = $"Error during test execution: {ex.Message}";
                    logger.Log(message);
                    throw new Exception(message);
                }

                try
                {
                    // if we want to skip the macro but the tsut was not yet existing
                    if (JustRunAssertOnExistingTsuT && !skipTheMacro)
                        SaveTsunamiProjectAsArtefact(tsunami, $"{methodBase.Name}.tsut");

                    // Do Test
                    logger.Log("Assert started");

                    if (!readyToAssert)
                        throw new Exception("Not ready to assert");
                    testAction(tsunami);
                    logger.Log("Assert done");
                }
                catch (Exception ex)
                {
                    logger.Log(ex.Message);
                    throw;
                }
            }
            catch (AssertFailedException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                logger.Log($"UnKnown execption: {ex}");
                throw;
            }
            finally
            {
                FinalAction?.Invoke();

                TryToCLoseOrAbortThread(tsunami, tsunamiThread);
            }
        }

        private void TryToCLoseOrAbortThread(Tsunami tsunami, Thread tsunamiThread)
        {
            if (tsunami != null)
            {
                // CloseTsunami
                if (tsunamiThread != null && tsunamiThread.IsAlive)
                {
                    logger.Log("Quitting tsunami safely");
                    tsunami.Quit(skipWarning: true, ExitOnlyTheTsunamiThread: true);
                    logger.Log("Tsunami quitted safely");
                }
            }

            // wait for tsunami to close or abort it
            if (tsunamiThread != null && !tsunamiThread.Join(30 * 1000))
            {
                logger.Log("Time out for quitting, aborting tsunami Thread...");
                tsunamiThread.Abort();
            }
        }

        private void StartTsunamiOpenExistingTsu(out Tsunami tsunami, out Thread tsunamiThread, string existingTsu)
        {
            // Set tsunami command line arguments
            string[] args;
            args = new string[] { existingTsu };

            // Reset events
            Tsunami.TsuLoaded.Reset();

            LaunchTsunami(out tsunami, out tsunamiThread, true, args);

            Tsunami2.Preferences.Values.Paths.forcedMeasureFolder = FolderPath;
            Tsunami2.Properties.PathOfTheSavedFile = Path.Combine(FolderPath, "tsu.tsu");
            this.TsuPath = Tsunami2.Properties.PathOfTheSavedFile;


            readyToAssert = true;
        }

        private void StartTsunamiOpenTsuRunMacro(out Tsunami tsunami, out Thread tsunamiThread)
        {
            // check file existence
            ExistOrCreate(ref TsuPath, TsuXmlContent, "tsu.xml");
            ExistOrCreate(ref MacroPath, MacroXmlContent, "macro.xml");

            bool tsuExist = File.Exists(TsuPath);
            bool macroExist = File.Exists(MacroPath);

            if (!macroExist)
                throw new Exception($"Macro file '{MacroPath}' do not exist");

            // Set tsunami command line arguments
            string[] args;
            if (tsuExist)
                args = new string[] { TsuPath, "-m", MacroPath };
            else
                args = new string[] { "-m", MacroPath };

            // Reset events
            Tsunami.TsuLoaded.Reset();

            MacroStarted.Reset();
            MacroFinished2SecondsAgo.Reset();

            // UnLlock windows
            //UnlockScreen(); // returns a denied access

            // Launch
            LaunchTsunami(out tsunami, out tsunamiThread, tsuExist, args);

            // wait for macro to start
            if (macroExist && !MacroStarted.WaitOne(10 * 1000, true))
            {
                logger.Log("CheckingResolution");
                // Check if the macro has the same resolution as the test machine(this)
                var macroForResolutionTest = LoadFromFile(MacroPath);
                if (!Resolution_Check(macroForResolutionTest, out string message))
                {
                    throw new Exception(message);
                }
                throw new Exception("Timeout on MacroStarted, the macro did not start");
            }

            logger.Log("getting tsunami objects");

            Tsunami2.Preferences.Values.Paths.forcedMeasureFolder = FolderPath;
            Tsunami2.Properties.PathOfTheSavedFile = Path.Combine(FolderPath, "tsu.tsu");
            this.TsuPath = Tsunami2.Properties.PathOfTheSavedFile;

            // Wait for the macro to the end
            int maxMacroTimeInSeconds = (MacroBeingPlayed.Duration * 30 + 2); // +2 because we wait 2 seconds after the last macro activity to declare it finished
            logger.Log($"Macro should run in {maxMacroTimeInSeconds} seconds {macroExist}");


            if (macroExist && !MacroFinished2SecondsAgo.WaitOne(maxMacroTimeInSeconds * 1000, exitContext: true))
            {
                var message = $"Timeout after {maxMacroTimeInSeconds} seconds on MacroFinished";
                logger.Log(message);
                throw new Exception(message);
            }
            readyToAssert = true;
            if (macroExist && !MacroFinishedSuccessfully)
            {
                throw new Exception($"Macro did not Finish successfully: {MacroFinishedReason}");
            }
        }

        private void LaunchTsunami(out Tsunami tsunami, out Thread tsunamiThread, bool tsuExist, string[] args)
        {
            Tsunami.TsunamiExists.Reset();


            tsunamiThread = new Thread(() => Program.Main(args));
            tsunamiThread.IsBackground = false;
            tsunamiThread.SetApartmentState(ApartmentState.STA); // Single Thread necessary to launch Forms
            logger.Log("Starting tsunamiThread");
            tsunamiThread.Start();


            Tsunami.TsunamiExists.WaitOne(5 * 1000, true);
            tsunami = Tsunami2.Properties; // is only existing after the splash screen
            tsunami.IsRunningForFunctionalTest = true;

            // Wait for splash screen to finish and the tsu to be loaded and macro to be read
            if (tsuExist)
                if (!Tsunami.TsuLoaded.WaitOne(120 * 1000, true))
                    throw new Exception("Timeout on Tsu file loading");
        }

        private void UnlockScreen()
        {
            bool isLocked = SystemInformation.UserInteractive;
            if (!isLocked)
                return;
            MouseSimulator.SetCursorPosition(960, 550);
            MouseSimulator.Click(960, 550, "Left", "Down");
            MouseSimulator.Click(960, 550, "Left", "Up");

            SendKeys.SendWait("k");
            SendKeys.SendWait("X");
            SendKeys.SendWait("G");
            SendKeys.SendWait("D");
            SendKeys.SendWait("L");
            SendKeys.SendWait("n");
            SendKeys.SendWait("1");
            SendKeys.SendWait("X");
            SendKeys.SendWait("j");
            SendKeys.SendWait("6");
            SendKeys.SendWait("3");
            SendKeys.SendWait("M");
            SendKeys.SendWait("v");
            SendKeys.SendWait("y");
            SendKeys.SendWait("S");
            SendKeys.SendWait("Y");
            SendKeys.SendWait("z");
            SendKeys.SendWait("e");
            SendKeys.SendWait("4");
            SendKeys.SendWait("6");
            SendKeys.SendWait("{ENTER}");
        }

        private void ExistOrCreate(ref string path, string xmlContent, string fileName)
        {
            if (path == "")
            {
                if (xmlContent != "")
                {
                    path = Path.Combine(FolderPath, fileName);
                    CreateFileFromStringContent(xmlContent, path);
                }
            }
        }

        public void CreateArtefact(string artefactName)
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), Tools.Artefact.RelativeArtefactFolderPath, artefactName);

            var fileinfo = new FileInfo(artefactName);

            File.Copy(fileinfo.FullName, path, true);

            // source check
            if (!File.Exists(path))
                throw new Exception($"Artefact creation failed, {path} NOT FOUND !");
        }



        public void SaveTsunamiProjectAsArtefact(Tsunami tsunami, string artefactName)
        {
            string path = GetFullCurrentWantedPath(artefactName);
            TSU.IO.Tsu.ToXmlToTsuAndBackupSync(o: tsunami, filePath: path);

            // source check
            if (!File.Exists(path))
                throw new Exception($"Artefact creation failed, {path} NOT FOUND !");

            this.LookForBugMessagesInTsunamiLog();
        }

        private static string GetFullCurrentWantedPath(string artefactName)
        {
            return Path.Combine(Directory.GetCurrentDirectory(), Tools.Artefact.RelativeArtefactFolderPath, artefactName);
        }

        private static void CreateFileFromStringContent(string content, string filePath)
        {
            // folder check
            var fileInfo = new FileInfo(filePath);
            if (!File.Exists(fileInfo.DirectoryName))
                Directory.CreateDirectory(fileInfo.DirectoryName);

            // Write the string content to the file
            File.WriteAllText(filePath, content);

            Console.WriteLine("String content has been written to the file successfully.");
        }

        public void LookForBugMessagesInTsunamiLog()
        {
            string message = "";
            List<LogEntry> entries = Log.GetAllEntries();
            foreach (LogEntry entry in entries)
            {
                var text = entry.ToString();
                if (text.Length > 11)
                {
                    text = text.Substring(0, 10);
                    if (text.Contains("('Bug"))
                    {
                        if (message == "")
                            message += "Bug message(s) or notification(s) has been detected:\r\n";
                        message += entry.ToString() + "\r\n";
                    }
                }
            }
            Assert.IsTrue(message == "", message);
        }

        internal void LetMeInteractWithTsunamiIfCTempStopTxtExist()
        {
            if (File.Exists(@"c:\temp\stop.txt"))
            {
                if (!Debug.isMouseHooked)
                    Debug.MouseAndKeyboardMonitoring_Subscribe();
                Thread.Sleep(5 * 60 * 1000);
            }
        }

        internal void Wait()
        {
            bool wait = true;
            while (wait)
            {
                Thread.Sleep(3000);
            }
        }

        internal string GetLastDatFileFromCurrentDirectory(out string filePath)
        {
            // Get all .dat files in the current directory
            var dir = FolderPath;
            var datFiles = Directory.GetFiles(dir, "*.dat");

            // Get the latest file based on last write time
            var latestFile = datFiles
                .Select(file => new FileInfo(file))
                .OrderByDescending(file => file.LastWriteTime)
                .FirstOrDefault();

            if (latestFile != null)
            {
                // Read the content of the latest file
                string content = File.ReadAllText(latestFile.FullName);

                // Return or display the content
                Console.WriteLine($"Content of {latestFile.Name}:");
                Console.WriteLine(content);
                filePath = latestFile.FullName;
                return content;
            }
            else
            {
                Console.WriteLine("No .dat files found.");
                filePath = null;
                return "";
            }
        }
    }

    public static class Tools
    {

        public class Logger
        {
            private string logFilePath;

            public Logger(string filePath)
            {
                logFilePath = filePath;
            }

            public void Log(string message)
            {
                try
                {
                    using (StreamWriter writer = new StreamWriter(logFilePath, true))
                    {
                        writer.WriteLine($"{DateTime.Now}: {message}");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error writing to log file: {ex.Message}");
                }
            }
        }


        public static class Artefact
        {

            public static string RelativeArtefactFolderPath = "Functional_Tests\\Artefacts";
            public static void CreateArtefact(string fileContent, string artefactName)
            {
                try
                {
                    string artefactFolderPath = Path.Combine(Directory.GetCurrentDirectory(), RelativeArtefactFolderPath);
                    if (!Directory.Exists(artefactFolderPath))
                        Directory.CreateDirectory(artefactFolderPath);
                    string artefactAbsolutePath = Path.Combine(artefactFolderPath, artefactName);

                    // Open or create the file for writing
                    using (StreamWriter writer = new StreamWriter(artefactAbsolutePath))
                    {
                        // Write the content of the string variable to the file
                        writer.WriteLine(fileContent);
                    }
                    Console.WriteLine("Content has been written to the file successfully.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"An error occurred: {ex.Message}");
                    Assert.Fail($"An error occurred while creating Step1.dat: {ex.Message}");
                }
            }

            /// <summary>
            /// CHeck and return the path of the artefact in the artefact folder, based on its name
            /// </summary>
            /// <param name="artefactName"></param>
            /// <param name="artefactPath"></param>
            /// <returns></returns>
            public static string GetArtefactFullPath(string artefactName, string artefactFolderRelativePath = "")
            {
                if (artefactFolderRelativePath == "")
                    artefactFolderRelativePath = RelativeArtefactFolderPath;
                string artefactAbsolutePath = Path.Combine(Directory.GetCurrentDirectory(), artefactFolderRelativePath, artefactName);

                if (!File.Exists(artefactAbsolutePath))
                    Assert.Fail($"Couldn't start because of missing artefact '{artefactAbsolutePath}' from previous step");

                return artefactAbsolutePath;
            }
        }

        internal static string GetNamesAndPath(MethodBase methodBase)
        {
            string methodName = methodBase.Name;
            string namespaceName = methodBase.DeclaringType.Namespace;
            string cleanFullName = $"{namespaceName}.{methodName}";
            string testPath = Path.Combine(Directory.GetCurrentDirectory(), cleanFullName.Replace(".", @"\") + @"\");
            if (!File.Exists(testPath))
                Directory.CreateDirectory(testPath);
            return testPath;
        }

        public static string GetKeySequence(double input, string target, int initialDelay = 10)
        {
            return GetKeySequence(input.ToString(), target, initialDelay);
        }

        public static string GetKeySequence(string input, string target, int initialDelay = 10)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < input.Length; i++)
            {
                char c = input[i];
                string shift;
                string keyData;
                if (char.IsLetter(c))
                {
                    shift = char.IsUpper(c).ToString().ToLower();
                    keyData = c.ToString();
                }
                else
                {
                    shift = "false";
                    switch (c)
                    {
                        case '"': keyData = "&quot;"; break;
                        case '&': keyData = "&amp;"; break;
                        case '\'': keyData = "&apos;"; break;
                        case '<': keyData = "&lt;"; break;
                        case '>': keyData = "&gt;"; break;
                        default: keyData = c.ToString(); break;
                    }
                }
                string delay = ((i == 0) ? initialDelay : 10).ToString();
                sb.AppendLine($"<IInputActivity xsi:type=\"KeyStroke____\" Pos.=\"0\" Delay=\"{delay}\" CTRL=\"false\" SHIFT=\"{shift}\" ALT=\"false\" KeyData=\"{{{keyData}}}\" Target=\"{target}\" />");
            }

            return sb.ToString();
        }

        internal static object CommentWithinTheMacro(string v)
        {
            return "";
        }

        public static void AssertFilesMatchLineByLine(string filePath1, string filePath2)
        {
            // Read all lines from both files
            string[] file1Lines = File.ReadAllLines(filePath1);
            string[] file2Lines = File.ReadAllLines(filePath2);

            AssertLinesArraysMatchLineByLine(file1Lines, file2Lines);
        }

        public static void AssertStringMatchLineByLine(string expected, string result)
        {
            // Read all lines from both files
            string[] expectedSplitted = SplitLines(expected);
            string[] resultSplitted = SplitLines(result);

            AssertLinesArraysMatchLineByLine(expectedSplitted, resultSplitted);
        }

        private static readonly string[] lineSeparators = { "\r\n", "\r", "\n" };

        public static string[] SplitLines(string expected)
        {
            return expected.Split(lineSeparators, StringSplitOptions.None);
        }

        public static void AssertLinesArraysMatchLineByLine(string[] expected, string[] result)
        {
            // Assert that the number of lines is the same
            Assert.AreEqual(expected.Length, result.Length, "not same number of lines in both feeds");

            // Compare each line
            for (int i = 0; i < expected.Length; i++)
            {
                var m = Regex.Matches(result[i], expected[i]);
                Assert.IsTrue(m.Count == 1 && m[0].Index == 0 && m[0].Length == result[i].Length, $"Line {i} \"{result[i]}\" does not match the expected pattern \"{expected[i]}\"");
            }
        }

        internal static object ClickInDgvWithYOffset(int X, int Y, int datagridOffset, string comment = "")
        {
            return $@"
<IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""785"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""{X}"" Ypos=""{Y + datagridOffset}"" Delta=""0"" />
<IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""{X}"" Ypos=""{Y + datagridOffset}"" Delta=""0"" />";
        }

        
    }
}
