﻿using Functional_Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using TSU.Common;

namespace Functional_Tests.Polar.RS2K
{
    [TestClass]
    public class Orientation
    {

        /// <summary>
        /// network: the zone choosen id LHCb
        /// 926R005.REF.001.          1.658964     1.658964     0.609067
        /// 926R005.REF.002.         -0.058969     1.980180     0.372939
        /// 926R005.REF.003.         -0.564738    -1.399225     0.437875
        /// 926R005.REF.004.          0.497408    -1.569622     0.559243
        /// 926R005.REF.005.          0.678820    -1.597149     0.553405
        //
        
        [TestMethod()]
        public void L4_P_O2000_Step1_Load_OLOC_Existing_Test()
        {
            // this test load an roeintation in OLOC in 926R005, then we select a zone to compute the CCS coordinates, we cancel the existing lgc computation and recompute now in RS2K

            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(Functional_Tests.Polar.OLOC.Intersection.L3_P_OO_Step5_Compute_With_LGC_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""228"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""639"" Ypos=""393"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""50"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""639"" Ypos=""393"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""241"" trace=""Cancel Setup+"" Button=""Left"" Direction=""Down"" Xpos=""752"" Ypos=""438"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""106"" trace=""Cancel Setup+"" Button=""Left"" Direction=""Up"" Xpos=""752"" Ypos=""438"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Cancel Setup (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Cancel Setup end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""262"" trace=""Manager modules+"" Button=""Left"" Direction=""Down"" Xpos=""241"" Ypos=""445"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""97"" trace=""Manager modules+"" Button=""Left"" Direction=""Up"" Xpos=""241"" Ypos=""445"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Manager modules (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Manager modules end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""25"" trace=""Zone Manager+"" Button=""Left"" Direction=""Down"" Xpos=""388"" Ypos=""789"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""82"" trace=""Zone Manager+"" Button=""Left"" Direction=""Up"" Xpos=""388"" Ypos=""789"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""13"" Delay=""0"" Action=""BB: Zone Manager (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""BB: Zone Manager end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""225"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""546"" Ypos=""309"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""546"" Ypos=""309"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""226"" trace=""button1+LHCb"" Button=""Left"" Direction=""Down"" Xpos=""881"" Ypos=""559"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""114"" trace=""button1+LHCb"" Button=""Left"" Direction=""Up"" Xpos=""881"" Ypos=""559"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""19"" Delay=""0"" Action=""MB: New zone!: LHCb"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""225"" trace=""AM_Polar Module+"" Button=""Left"" Direction=""Down"" Xpos=""202"" Ypos=""509"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""114"" trace=""AM_Polar Module+"" Button=""Left"" Direction=""Up"" Xpos=""202"" Ypos=""509"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""22"" Delay=""0"" Action=""BB: AM_Polar Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""23"" Delay=""0"" Action=""BB: AM_Polar Module end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""233"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""612"" Ypos=""387"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""612"" Ypos=""387"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""270"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Down"" Xpos=""814"" Ypos=""506"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""90"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Up"" Xpos=""814"" Ypos=""506"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""28"" Delay=""0"" Action=""BB: Computing (LGC2) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""278"" trace=""button1+Orientation Only"" Button=""Left"" Direction=""Down"" Xpos=""874"" Ypos=""561"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""90"" trace=""button1+Orientation Only"" Button=""Left"" Direction=""Up"" Xpos=""874"" Ypos=""561"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""31"" Delay=""0"" Action=""MB: Choose a type of setup: Orientation Only"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""214"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""986"" Ypos=""737"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""98"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""986"" Ypos=""737"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""MB: Computing results (LGC adjustment): OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""228"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""877"" Ypos=""742"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""98"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""877"" Ypos=""742"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""MB: Compensation: Valid"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""BB: Computing (LGC2) end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
                {
                    Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                    var station = tsunami.MeasurementModules[0]?.StationModules[1]?._Station as TSU.Polar.Station;
                    Assert.IsNotNull(station);

                    Assert.IsTrue(station.MeasuresToDo.Count == 0, "wrong number of measureToDo left");
                    Assert.AreEqual(0, station.Parameters2._StationPoint._Coordinates.Su.X.Value, 0.001);
                    Assert.AreEqual(0, station.Parameters2._StationPoint._Coordinates.Su.Y.Value, 0.001);
                    Assert.AreEqual(0, station.Parameters2._StationPoint._Coordinates.Su.Z.Value, 0.001);
                    test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsuT");
                });
        }
    }
}
