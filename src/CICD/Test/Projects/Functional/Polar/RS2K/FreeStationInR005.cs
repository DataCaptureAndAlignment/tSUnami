﻿using Functional_Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using TSU.Common;

namespace Functional_Tests.Polar.RS2K
{
    [TestClass]
    public class FreeStation
    {

        /// <summary>
        /// network: the zone choosen id LHCb
        /// 926R005.REF.001.          1.658964     1.658964     0.609067
        /// 926R005.REF.002.         -0.058969     1.980180     0.372939
        /// 926R005.REF.003.         -0.564738    -1.399225     0.437875
        /// 926R005.REF.004.          0.497408    -1.569622     0.559243
        /// 926R005.REF.005.          0.678820    -1.597149     0.553405
        //

        [TestMethod()]
        public void L4_P_FS2000_Step1_Load_OLOC_Existing_Test()
        {
            // this test load an freestation in OLOC in 926R005, then we select a zone to compute the CCS coordinates, we cancel the existing lgc computation and recompute now in RS2K

            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(Functional_Tests.Polar.OLOC.FreeStationInR005.L3_P_FS_Step5_Compute_With_LGC_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""267"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""622"" Ypos=""389"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""622"" Ypos=""389"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""289"" trace=""Cancel Setup+"" Button=""Left"" Direction=""Down"" Xpos=""738"" Ypos=""428"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""121"" trace=""Cancel Setup+"" Button=""Left"" Direction=""Up"" Xpos=""738"" Ypos=""428"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Cancel Setup (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Cancel Setup end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""266"" trace=""Manager modules+"" Button=""Left"" Direction=""Down"" Xpos=""253"" Ypos=""424"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""113"" trace=""Manager modules+"" Button=""Left"" Direction=""Up"" Xpos=""252"" Ypos=""423"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Manager modules (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Manager modules end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""278"" trace=""Zone Manager+"" Button=""Left"" Direction=""Down"" Xpos=""424"" Ypos=""770"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""98"" trace=""Zone Manager+"" Button=""Left"" Direction=""Up"" Xpos=""424"" Ypos=""770"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""13"" Delay=""0"" Action=""BB: Zone Manager (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""BB: Zone Manager end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""217"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""564"" Ypos=""313"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""564"" Ypos=""313"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""274"" trace=""button1+LHCb"" Button=""Left"" Direction=""Down"" Xpos=""878"" Ypos=""566"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""113"" trace=""button1+LHCb"" Button=""Left"" Direction=""Up"" Xpos=""878"" Ypos=""566"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""19"" Delay=""0"" Action=""MB: New zone!: LHCb"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""226"" trace=""AM_Polar Module+"" Button=""Left"" Direction=""Down"" Xpos=""125"" Ypos=""484"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""81"" trace=""AM_Polar Module+"" Button=""Left"" Direction=""Up"" Xpos=""125"" Ypos=""484"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""22"" Delay=""0"" Action=""BB: AM_Polar Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""23"" Delay=""0"" Action=""BB: AM_Polar Module end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""277"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""621"" Ypos=""390"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""621"" Ypos=""390"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""229"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Down"" Xpos=""846"" Ypos=""509"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""98"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Up"" Xpos=""846"" Ypos=""509"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""28"" Delay=""0"" Action=""BB: Computing (LGC2) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""245"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""969"" Ypos=""805"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""106"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""969"" Ypos=""805"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""31"" Delay=""0"" Action=""MB: Computing results (LGC adjustment): OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""293"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""885"" Ypos=""746"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""98"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""885"" Ypos=""746"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""MB: Compensation: Valid"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""35"" Delay=""0"" Action=""BB: Computing (LGC2) end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[1]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station);

                Assert.IsTrue(station.MeasuresToDo.Count == 0, "wrong number of measureToDo left");

                Assert.IsTrue(station.Parameters2._StationPoint.SocketCode.Id == "29", "socket code must be 29");
                Assert.AreEqual(0, station.Parameters2._StationPoint._Coordinates.Su.X.Value, 0.001);
                Assert.AreEqual(0, station.Parameters2._StationPoint._Coordinates.Su.Y.Value, 0.001);
                Assert.AreEqual(0, station.Parameters2._StationPoint._Coordinates.Su.Z.Value,0.001);
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsuT");
            });
        }

    }
}
