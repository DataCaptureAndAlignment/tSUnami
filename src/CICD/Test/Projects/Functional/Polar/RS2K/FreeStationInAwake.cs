﻿using Functional_Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using TSU;
using TSU.Common;

namespace Functional_Tests.Polar.RS2K
{
    [TestClass]
    public class FreeStationInAwake
    {

        /// <summary>
        /// network: the zone choosen id LHCb
        /// 926R005.REF.001.          1.658964     1.658964     0.609067
        /// 926R005.REF.002.         -0.058969     1.980180     0.372939
        /// 926R005.REF.003.         -0.564738    -1.399225     0.437875
        /// 926R005.REF.004.          0.497408    -1.569622     0.559243
        /// 926R005.REF.005.          0.678820    -1.597149     0.553405
        //

        public static void CreateTheoFile()
        {
            string content = @"TT41      ;QTGD.411500.E                 ;  595.57395; 2854.99786; 4222.27549;2393.35058;393.75895;  .495; -.00915; -.056608;107.9754; 1;A;23-MAR-2016;                    ;test;
TT41      ;QTGD.411500.S                   ;  597.01395; 2856.42428; 4222.09585;2393.26911;393.67761; 2.200; -.00915; -.056608;107.9754; 1;O;23-MAR-2016;                    ;;
TT41      ;MDGV.411506.E                   ;  598.08295; 2857.42468; 4221.97131;2393.04828;393.45686;  .490; -.00915; -.056608;107.9754; 1;A;23-MAR-2016;                    ;;
TT41      ;MDGV.411506.S                   ;  598.28295; 2857.67232; 4221.94013;2393.03412;393.44273;  .548; -.00915; -.056608;107.9754; 1;O;23-MAR-2016;                    ;;
TT41      ;MBG.411508.E                    ;  600.49395; 2859.87064; 4221.67338;2393.03229;393.44110;  .657; -.00035; -.056608;107.7206; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411508.S                    ;  603.98395; 2863.32946; 4221.25184;2392.83483;393.24395; 6.300; -.00035; -.056608;107.7206; 1;O;23-MAR-2016;                    ;;
TT41      ;MBG.411522.E                    ;  607.36396; 2866.68083; 4220.85712;2392.64360;393.05303;  .570; -.00035; -.056608;107.2111; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411522.S                    ;  610.85396; 2870.14291; 4220.46328;2392.44613;392.85589; 6.300; -.00035; -.056608;107.2111; 1;O;23-MAR-2016;                    ;;
TT41      ;MBG.411536.E                    ;  614.23398; 2873.49733; 4220.09540;2392.25491;392.66499;  .570; -.00035; -.056608;106.7015; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411536.S                    ;  617.72398; 2876.96246; 4219.72928;2392.05745;392.46787; 6.300; -.00035; -.056608;106.7015; 1;O;23-MAR-2016;                    ;;
TT41      ;MBG.411550.E                    ;  621.10400; 2880.31973; 4219.38827;2391.86621;392.27697;  .570; -.00034; -.056608;106.1919; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411550.S                    ;  624.59400; 2883.78767; 4219.04990;2391.66876;392.07987; 6.300; -.00034; -.056608;106.1919; 1;O;23-MAR-2016;                    ;;
TT41      ;QTGF.411600.E                   ;  626.87401; 2886.05524; 4218.82587;2391.57968;391.99102;  .495; -.01095; -.056608;105.9371; 1;A;23-MAR-2016;                    ;;
TT41      ;QTGF.411600.S                   ;  628.31401; 2887.48669; 4218.69199;2391.49821;391.90970; 2.200; -.01095; -.056608;105.9371; 1;O;23-MAR-2016;                    ;;
TT41      ;BPG.411605.E                    ;  628.89401; 2888.06712; 4218.63695;2391.53527;391.94682;  .160; -.01095; -.056608;105.9371; 7;A;23-MAR-2016;                    ;;
TT41      ;BPG.411605.S                    ;  629.06401; 2888.23611; 4218.62114;2391.52566;391.93723;  .250; -.01095; -.056608;105.9371; 7;O;23-MAR-2016;                    ;;
TT41      ;MBG.411608.E                    ;  631.79401; 2890.94486; 4218.38059;2391.26138;391.67324; 1.285; -.00034; -.056608;105.6824; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411608.S                    ;  635.28401; 2894.41540; 4218.06999;2391.06392;391.47615; 6.300; -.00034; -.056608;105.6824; 1;O;23-MAR-2016;                    ;;
TT41      ;MBG.411622.E                    ;  638.66403; 2897.77769; 4217.78275;2390.87270;391.28529;  .570; -.00034; -.056608;105.1728; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411622.S                    ;  642.15403; 2901.25060; 4217.49994;2390.67524;391.08821; 6.300; -.00034; -.056608;105.1728; 1;O;23-MAR-2016;                    ;;
TT41      ;MBG.411636.E                    ;  645.53405; 2904.61508; 4217.23963;2390.48400;390.89735;  .570; -.00034; -.056608;104.6633; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411636.S                    ;  649.02405; 2908.09014; 4216.98462;2390.28655;390.70029; 6.300; -.00034; -.056608;104.6633; 1;O;23-MAR-2016;                    ;;
TT41      ;QTGD.411700.E                   ;  651.30406; 2910.36242; 4216.81470;2390.19747;390.61147;  .495; -.01231; -.056608;104.4085; 1;A;23-MAR-2016;                    ;;
TT41      ;QTGD.411700.S                   ;  652.74406; 2911.79667; 4216.71522;2390.11599;390.53016; 2.200; -.01231; -.056608;104.4085; 1;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.571.                      ;  570.93800; 2830.71683; 4226.50115;2393.49345;393.90011;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.591.                      ;  590.59800; 2850.13398; 4223.71076;2392.38002;392.78824;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.610.                      ;  610.05800; 2869.42602; 4221.34049;2391.27522;391.68518;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.634.                      ;  633.53800; 2892.71484; 4219.02910;2389.94861;390.36092;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.647.                      ;  647.27800; 2906.35278; 4217.93419;2389.15946;389.57328;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.669.                      ;  668.70800; 2927.62769; 4216.53995;2387.96197;388.37830;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.690.                      ;  690.11800; 2948.95121; 4215.64153;2386.74747;387.16655;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.712.                      ;  711.64800; 2970.42012; 4215.20016;2385.53592;385.95799;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.733.                      ;  733.10800; 2991.87414; 4215.16541;2384.32493;384.75020;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;

";
            File.WriteAllText($@"c:\Data\tsunami\theoretical files\awake.dat", content);
        }

        [TestMethod()]
        public void L1_P_FS_AWAKE_Step01_Load_theofile()
        {
            Tools.Artefact.GetArtefactFullPath("L0_START_STEP1_DetectWrongResolution_Test.tsut"); // used to fail if resolution is wrong
            CreateTheoFile();
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""293"" trace=""Advanced modules+"" Button=""Left"" Direction=""Down"" Xpos=""203"" Ypos=""362"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""24"" trace=""Advanced modules+"" Button=""Left"" Direction=""Up"" Xpos=""203"" Ypos=""362"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Advanced modules (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Advanced modules end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""567"" trace=""Polar Module+"" Button=""Left"" Direction=""Down"" Xpos=""353"" Ypos=""396"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""28"" trace=""Polar Module+"" Button=""Left"" Direction=""Up"" Xpos=""353"" Ypos=""396"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Polar Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Polar Module end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""206"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""535"" Ypos=""344"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""45"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""535"" Ypos=""344"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""227"" trace=""New Station+"" Button=""Left"" Direction=""Down"" Xpos=""601"" Ypos=""369"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""24"" trace=""New Station+"" Button=""Left"" Direction=""Up"" Xpos=""601"" Ypos=""369"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""13"" Delay=""0"" Action=""BB: New Station (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""BB: New Station end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""585"" Ypos=""332"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""35"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""585"" Ypos=""332"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""231"" trace=""Import Points from file(s)+"" Button=""Left"" Direction=""Down"" Xpos=""697"" Ypos=""363"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""33"" trace=""Import Points from file(s)+"" Button=""Left"" Direction=""Up"" Xpos=""697"" Ypos=""363"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""19"" Delay=""0"" Action=""BB: Import Points from file(s) (beginning)"" />

    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{A}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{W}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{A}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{K}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{E}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{.}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{D}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{A}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{T}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""275"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""Button"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""269"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""963"" Ypos=""928"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""110"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""963"" Ypos=""928"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""MB: Elements available in this instance of tsunami: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""BB: Import Points from file(s) end"" />

  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[0]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station);

                Assert.IsTrue(station.MeasuresToDo.Count == 0, "wrong number of measureToDo left");

                Assert.IsTrue(tsunami.Points.Count == 33,"problem loading 33 points from awake.dat");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsuT");
            });
        }


        [TestMethod()]
        public void L1_P_FS_AWAKE_Step02_Select_instru_and_station_point()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L1_P_FS_AWAKE_Step01_Load_theofile) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""278"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""644"" Ypos=""362"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""60"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""644"" Ypos=""362"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""208"" trace=""Instrument+"" Button=""Left"" Direction=""Down"" Xpos=""740"" Ypos=""387"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""75"" trace=""Instrument+"" Button=""Left"" Direction=""Up"" Xpos=""740"" Ypos=""387"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Instrument (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""231"" trace=""Options+"" Button=""Left"" Direction=""Down"" Xpos=""1484"" Ypos=""214"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""36"" trace=""Options+"" Button=""Left"" Direction=""Up"" Xpos=""1484"" Ypos=""214"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Options (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Options end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""262"" trace=""List View+"" Button=""Left"" Direction=""Down"" Xpos=""1567"" Ypos=""320"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""41"" trace=""List View+"" Button=""Left"" Direction=""Up"" Xpos=""1567"" Ypos=""320"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: List View (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""13"" Delay=""0"" Action=""BB: List View end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""298"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""666"" Ypos=""266"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""36"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""666"" Ypos=""266"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""16"" Delay=""243"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{T}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""17"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""1292"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""230"" Ypos=""329"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""31"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""230"" Ypos=""329"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""607"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""865"" Ypos=""928"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""28"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""865"" Ypos=""928"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""22"" Delay=""0"" Action=""MB: Select instrument: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""23"" Delay=""0"" Action=""BB: Instrument end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""461"" Ypos=""388"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""31"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""461"" Ypos=""388"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""280"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""597"" Ypos=""518"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""32"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""597"" Ypos=""518"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""207"" trace=""Known position+"" Button=""Left"" Direction=""Down"" Xpos=""706"" Ypos=""542"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""40"" trace=""Known position+"" Button=""Left"" Direction=""Up"" Xpos=""706"" Ypos=""542"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""30"" Delay=""0"" Action=""BB: Known position (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""203"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""256"" Ypos=""360"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""52"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""256"" Ypos=""359"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""212"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""861"" Ypos=""933"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""30"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""861"" Ypos=""933"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""35"" Delay=""0"" Action=""MB: Point stationned: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""36"" Delay=""0"" Action=""BB: Known position end"" />

  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[0]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station);

                Assert.IsTrue(station.MeasuresToDo.Count == 0, "wrong number of measureToDo left");

                Assert.IsTrue(station.Parameters2._Instrument._Model == "T2", "not a T2");
                Assert.IsTrue(station.Parameters2._StationPoint._Name == "TT41.QTGD.411500.S", "not stationned on TT41.QTGD.411500.S");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsuT");
            });
        }

        [TestMethod()]
        public void L1_P_FS_AWAKE_Step03_Select_REFS()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L1_P_FS_AWAKE_Step02_Select_instru_and_station_point) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""372"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""427"" Ypos=""263"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""67"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""427"" Ypos=""263"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""315"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""425"" Ypos=""293"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""88"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""425"" Ypos=""293"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""324"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""531"" Ypos=""325"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""52"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""531"" Ypos=""325"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""318"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Down"" Xpos=""684"" Ypos=""358"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""72"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Up"" Xpos=""684"" Ypos=""358"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Add known Point(s) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""245"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""194"" Ypos=""329"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""77"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""194"" Ypos=""329"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""244"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""244"" Ypos=""443"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""40"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""244"" Ypos=""443"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""264"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""238"" Ypos=""468"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""32"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""238"" Ypos=""468"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""237"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""256"" Ypos=""267"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""80"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""256"" Ypos=""267"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""16"" Delay=""239"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{G}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""17"" Delay=""177"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{G}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""18"" Delay=""106"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{P}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""1494"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""186"" Ypos=""412"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""52"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""186"" Ypos=""412"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""269"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""189"" Ypos=""431"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""49"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""189"" Ypos=""431"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""251"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""887"" Ypos=""936"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""56"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""887"" Ypos=""936"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""25"" Delay=""0"" Action=""MB: Elements Selection: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""26"" Delay=""0"" Action=""BB: Add known Point(s) end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""313"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""428"" Ypos=""326"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""88"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""428"" Ypos=""326"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""364"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""424"" Ypos=""261"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""32"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""424"" Ypos=""261"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""369"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""547"" Ypos=""419"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""57"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""547"" Ypos=""419"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""397"" trace=""Quick measure+"" Button=""Left"" Direction=""Down"" Xpos=""679"" Ypos=""747"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""50"" trace=""Quick measure+"" Button=""Left"" Direction=""Up"" Xpos=""679"" Ypos=""747"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""13"" Delay=""0"" Action=""BB: Quick measure (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""BB: Quick measure end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""212"" trace=""button1+Apply"" Button=""Left"" Direction=""Down"" Xpos=""847"" Ypos=""554"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""39"" trace=""button1+Apply"" Button=""Left"" Direction=""Up"" Xpos=""847"" Ypos=""554"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""MB: New default parameter: Apply"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""298"" trace=""button1+Apply"" Button=""Left"" Direction=""Down"" Xpos=""847"" Ypos=""554"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""50"" trace=""button1+Apply"" Button=""Left"" Direction=""Up"" Xpos=""847"" Ypos=""554"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""MB: New default parameter: Apply"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""BB: Quick measure end"" />

  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[0]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station, "station is null");

                var stparam = station.Parameters2;
                Assert.IsTrue(stparam.Setups.InitialValues.IsPositionKnown, "st pos must be known");
                Assert.IsTrue(stparam._StationPoint._Name == "TT41.QTGD.411500.S", "not stationned on TT41.QTGD.411500.S");

                Assert.IsTrue(station.MeasuresToDo.Count == 5, $"should be ready to measure 5 points not {station.MeasuresToDo.Count}");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsuT");
            });
        }
        
        
        [TestMethod()]
        public void L1_P_FS_AWAKE_Step04_Change_relfector()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L1_P_FS_AWAKE_Step03_Select_REFS) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""2010"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""429"" Ypos=""514"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""35"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""429"" Ypos=""514"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""226"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""428"" Ypos=""456"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""41"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""428"" Ypos=""456"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""229"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""469"" Ypos=""417"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""45"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""469"" Ypos=""417"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""241"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""691"" Ypos=""423"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""43"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""691"" Ypos=""423"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""210"" trace=""Change Reflector+"" Button=""Left"" Direction=""Down"" Xpos=""924"" Ypos=""469"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""57"" trace=""Change Reflector+"" Button=""Left"" Direction=""Up"" Xpos=""924"" Ypos=""469"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Change Reflector (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""2622"" trace=""Options+"" Button=""Left"" Direction=""Down"" Xpos=""1527"" Ypos=""208"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""94"" trace=""Options+"" Button=""Left"" Direction=""Up"" Xpos=""1527"" Ypos=""208"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""BB: Options (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Options end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""1021"" trace=""List View+"" Button=""Left"" Direction=""Down"" Xpos=""1626"" Ypos=""319"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""46"" trace=""List View+"" Button=""Left"" Direction=""Up"" Xpos=""1626"" Ypos=""319"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""BB: List View (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""19"" Delay=""0"" Action=""BB: List View end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""1127"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""334"" Ypos=""251"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""36"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""334"" Ypos=""251"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""22"" Delay=""634"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{C}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""23"" Delay=""241"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{C}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""73"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{R}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""1461"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""145"" Ypos=""329"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""30"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""145"" Ypos=""329"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""688"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""898"" Ypos=""927"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""36"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""898"" Ypos=""927"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""29"" Delay=""0"" Action=""MB: Choose a Reflector: Select"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""1092"" trace=""button1+Apply"" Button=""Left"" Direction=""Down"" Xpos=""896"" Ypos=""578"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""39"" trace=""button1+Apply"" Button=""Left"" Direction=""Up"" Xpos=""896"" Ypos=""578"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""32"" Delay=""0"" Action=""MB: New default parameter: Apply"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""33"" Delay=""0"" Action=""BB: Change Reflector end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[0]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station);

                var stparam = station.Parameters2;
                Assert.IsTrue(stparam.Setups.InitialValues.IsPositionKnown, "st pos must be known");
                Assert.IsTrue(stparam._StationPoint._Name == "TT41.QTGD.411500.S", "not stationned on TT41.QTGD.411500.S");

                Assert.IsTrue(station.MeasuresToDo.Count == 5, $"should be ready to measure 5 points not {station.MeasuresToDo.Count}");
                Assert.IsTrue((station.MeasuresToDo[0] as TSU.Polar.Measure).Reflector._Model == "CCR1.5", $"should be CCR1.5");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsuT");
            });
        }


        [TestMethod()]
        public void L1_P_FS_AWAKE_Step05_acquire()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L1_P_FS_AWAKE_Step04_Change_relfector) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor="".51"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""207"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""435"" Ypos=""261"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""435"" Ypos=""261"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""253"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""426"" Ypos=""288"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""42"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""426"" Ypos=""288"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""280"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""822"" Ypos=""679"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""75"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""822"" Ypos=""679"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""228"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1296"" Ypos=""876"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""52"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1296"" Ypos=""876"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Valid (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""270"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""769"" Ypos=""607"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""49"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""769"" Ypos=""607"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""218"" trace=""button1+Continue"" Button=""Left"" Direction=""Down"" Xpos=""948"" Ypos=""602"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""29"" trace=""button1+Continue"" Button=""Left"" Direction=""Up"" Xpos=""948"" Ypos=""602"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""MB: Big differences in distances (station &lt;-&gt; point): Continue"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""827"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""872"" Ypos=""637"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""32"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""872"" Ypos=""637"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""233"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""1029"" Ypos=""638"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""53"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""1029"" Ypos=""638"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""19"" Delay=""0"" Action=""MB: Point successfully measured: OK!"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""20"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""1731"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""773"" Ypos=""672"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""48"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""773"" Ypos=""672"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""276"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""506"" Ypos=""750"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""80"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""506"" Ypos=""750"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""27"" Delay=""214"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""28"" Delay=""57"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""220"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1257"" Ypos=""876"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""51"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1257"" Ypos=""876"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""31"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""32"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""1789"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""652"" Ypos=""687"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""46"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""652"" Ypos=""687"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""223"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""496"" Ypos=""783"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""52"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""496"" Ypos=""783"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""37"" Delay=""222"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""38"" Delay=""90"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""242"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1260"" Ypos=""885"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""65"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1260"" Ypos=""885"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""41"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""42"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""1407"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""584"" Ypos=""685"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""44"" Delay=""36"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""584"" Ypos=""685"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""45"" Delay=""268"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""496"" Ypos=""750"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""46"" Delay=""59"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""496"" Ypos=""750"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""47"" Delay=""261"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""48"" Delay=""58"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""49"" Delay=""293"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1364"" Ypos=""896"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""50"" Delay=""53"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1364"" Ypos=""896"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""51"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""52"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""55"" Delay=""251"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""544"" Ypos=""681"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""56"" Delay=""59"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""544"" Ypos=""681"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""57"" Delay=""209"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""508"" Ypos=""783"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""58"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""508"" Ypos=""783"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""59"" Delay=""262"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""60"" Delay=""214"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""61"" Delay=""287"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1254"" Ypos=""883"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""62"" Delay=""76"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1254"" Ypos=""883"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""63"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""64"" Delay=""0"" Action=""BB: Valid end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[0]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station);


                var stparam = station.Parameters2;
                Assert.IsTrue(stparam.Setups.InitialValues.IsPositionKnown, "st pos must be known");
                Assert.IsTrue(stparam._StationPoint._Name == "TT41.QTGD.411500.S", "not stationned on TT41.QTGD.411500.S");

                Assert.IsTrue(station.MeasuresToDo.Count == 0, $"the 5 points were not measured");

                var taken = station.MeasuresTaken;
                Assert.IsTrue(taken.Count == 5, $"the 5 points were not measured");

                var measure1 = taken[1] as TSU.Polar.Measure;
                Assert.AreEqual(299.785, measure1.Angles.Corrected.Horizontal.Value, 0.001, $"wrong ellise s1");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsuT");
            });
        }


        [TestMethod()]
        public void L1_P_FS_AWAKE_Step06_ChangeControlPoint()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L1_P_FS_AWAKE_Step05_acquire) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""271"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""428"" Ypos=""263"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""66"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""428"" Ypos=""263"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""299"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""458"" Ypos=""456"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""91"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""458"" Ypos=""456"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""597"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""648"" Ypos=""492"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""85"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""648"" Ypos=""492"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""385"" trace=""Details+"" Button=""Left"" Direction=""Down"" Xpos=""792"" Ypos=""824"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""68"" trace=""Details+"" Button=""Left"" Direction=""Up"" Xpos=""792"" Ypos=""824"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Details (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""205"" trace=""Manager+"" Button=""Left"" Direction=""Down"" Xpos=""458"" Ypos=""209"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""62"" trace=""Manager+"" Button=""Left"" Direction=""Up"" Xpos=""458"" Ypos=""209"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Manager:  (Pm of TT41.QTGD.411500.E) (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""13"" Delay=""0"" Action=""BB: Manager:  (Pm of TT41.QTGD.411500.E) end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""206"" trace=""Change Status+"" Button=""Left"" Direction=""Down"" Xpos=""594"" Ypos=""241"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""69"" trace=""Change Status+"" Button=""Left"" Direction=""Up"" Xpos=""594"" Ypos=""241"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""16"" Delay=""0"" Action=""BB: Change Status (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""BB: Change Status end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""217"" trace=""Bad+"" Button=""Left"" Direction=""Down"" Xpos=""703"" Ypos=""430"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""42"" trace=""Bad+"" Button=""Left"" Direction=""Up"" Xpos=""703"" Ypos=""430"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""20"" Delay=""0"" Action=""BB: Bad (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""21"" Delay=""0"" Action=""BB: Bad end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""241"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""962"" Ypos=""922"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""54"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""962"" Ypos=""922"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""24"" Delay=""0"" Action=""MB: Measures: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""25"" Delay=""0"" Action=""BB: Details end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""221"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""556"" Ypos=""515"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""65"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""556"" Ypos=""515"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""219"" trace=""Control Opening+"" Button=""Left"" Direction=""Down"" Xpos=""723"" Ypos=""774"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""37"" trace=""Control Opening+"" Button=""Left"" Direction=""Up"" Xpos=""723"" Ypos=""774"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""30"" Delay=""0"" Action=""BB: Control Opening (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""31"" Delay=""0"" Action=""BB: Control Opening end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""552"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""432"" Ypos=""297"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""46"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""432"" Ypos=""296"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""257"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""660"" Ypos=""678"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""99"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""660"" Ypos=""678"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""253"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1315"" Ypos=""883"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""41"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1315"" Ypos=""883"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""BB: Valid (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""1075"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""1058"" Ypos=""740"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""98"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""1058"" Ypos=""740"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""MB: Measure comparaison: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""248"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""973"" Ypos=""599"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""60"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""973"" Ypos=""599"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""41"" Delay=""0"" Action=""MB: Closure OK: OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""278"" trace=""button2+No"" Button=""Left"" Direction=""Down"" Xpos=""1142"" Ypos=""559"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""60"" trace=""button2+No"" Button=""Left"" Direction=""Up"" Xpos=""1142"" Ypos=""559"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""44"" Delay=""0"" Action=""MB: Do you want to start the station setup compensation?: No"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""45"" Delay=""0"" Action=""BB: Valid end"" />

  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[0]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station);


                var stparam = station.Parameters2;
                Assert.IsTrue(stparam.Setups.InitialValues.IsPositionKnown, "st pos must be known");
                Assert.IsTrue(stparam._StationPoint._Name == "TT41.QTGD.411500.S", "not stationned on TT41.QTGD.411500.S");
                //test.Wait();
                Assert.IsTrue(station.MeasuresToDo.Count == 0, $"the 5 points were not measured");
                Assert.IsTrue(station.MeasuresTaken.Count == 6, $"6 meaurements should have been done");
                Assert.IsTrue(station.MeasuresTaken[5]._Point._Name == "TT41.MBG.411508.E", $"the gange of control point failed");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsuT");
            });
        }

        [TestMethod()]
        public void L1_P_FS_AWAKE_Step07_compute_as_free_station()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L1_P_FS_AWAKE_Step06_ChangeControlPoint) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""0.8"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""244"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""460"" Ypos=""393"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""69"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""460"" Ypos=""393"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""645"" Ypos=""549"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""47"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""645"" Ypos=""549"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""471"" trace=""H = 0 (FreeStation)+"" Button=""Left"" Direction=""Down"" Xpos=""783"" Ypos=""578"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""45"" trace=""H = 0 (FreeStation)+"" Button=""Left"" Direction=""Up"" Xpos=""783"" Ypos=""578"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: H = 0 (FreeStation) (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: H = 0 (FreeStation) end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""292"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""457"" Ypos=""392"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""39"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""457"" Ypos=""392"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""563"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""598"" Ypos=""392"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""56"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""598"" Ypos=""392"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""264"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Down"" Xpos=""775"" Ypos=""500"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""78"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Up"" Xpos=""775"" Ypos=""500"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Computing (LGC2) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""258"" trace=""button2+Free Station"" Button=""Left"" Direction=""Down"" Xpos=""1029"" Ypos=""554"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""52"" trace=""button2+Free Station"" Button=""Left"" Direction=""Up"" Xpos=""1029"" Ypos=""554"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""MB: Choose a type of setup: Free Station"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""2305"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""951"" Ypos=""762"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""43"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""951"" Ypos=""762"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""21"" Delay=""0"" Action=""MB: Computing results (LGC adjustment): OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""241"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""880"" Ypos=""758"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""30"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""880"" Ypos=""758"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""24"" Delay=""0"" Action=""MB: Compensation: Valid"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""242"" trace=""button1+Yes"" Button=""Left"" Direction=""Down"" Xpos=""872"" Ypos=""574"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""45"" trace=""button1+Yes"" Button=""Left"" Direction=""Up"" Xpos=""872"" Ypos=""574"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""27"" Delay=""0"" Action=""MB: Are you sure?: Yes"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""28"" Delay=""0"" Action=""BB: Computing (LGC2) end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[0]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station);

                Assert.IsTrue(station.Parameters2._IsSetup == true, $"setup not done");

                var ellipses = station.Parameters2.Setups.InitialValues.CompensationStrategy.Points_Accelerator_Ellipses;
                Assert.IsTrue(ellipses.Count == 5, $"not 5 error ellipses");
                var stationEllipses = ellipses.FirstOrDefault(x => x.Key == "TT41.QTGD.411500.S").Value;

                var stationEllipsesInTT41 = stationEllipses.FirstOrDefault(x => x.Key == "TT41").Value;
                Assert.AreEqual(0.32, stationEllipsesInTT41.s1, 0.01, $"wrong ellise s1");
                Assert.AreEqual(2.91, stationEllipsesInTT41.s2, 0.01, $"wrong ellise s2");
                Assert.AreEqual(0.52, stationEllipsesInTT41.s3, 0.01, $"wrong ellise s3");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsuT");
            });
        }
    }
}
