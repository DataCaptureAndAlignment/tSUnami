﻿using Functional_Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using TSU.Common;

namespace Functional_Tests.Polar
{
    [TestClass]
    public class PinTable
    {

        /// <summary>
        /// network: the zone choosen id LHCb
        /// 926R005.REF.001.          1.658964     1.658964     0.609067
        /// 926R005.REF.002.         -0.058969     1.980180     0.372939
        /// 926R005.REF.003.         -0.564738    -1.399225     0.437875
        /// 926R005.REF.004.          0.497408    -1.569622     0.559243
        /// 926R005.REF.005.          0.678820    -1.597149     0.553405
        //

        [TestMethod()]
        public void L5_P_PPT_Step1_ShowTableTest()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(RS2K.Orientation.L4_P_O2000_Step1_Load_OLOC_Existing_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""346"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""625"" Ypos=""452"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""625"" Ypos=""452"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""381"" trace=""Pin all 'Measured' points+"" Button=""Left"" Direction=""Down"" Xpos=""734"" Ypos=""555"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""9"" trace=""Pin all 'Measured' points+"" Button=""Left"" Direction=""Up"" Xpos=""734"" Ypos=""555"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Pin all 'Measured' points (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Pin all 'Measured' points end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""362"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1598"" Ypos=""544"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""11"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1598"" Ypos=""544"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""345"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1598"" Ypos=""544"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""11"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1598"" Ypos=""544"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""306"" trace=""Change visual details+"" Button=""Left"" Direction=""Down"" Xpos=""1614"" Ypos=""896"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""7"" trace=""Change visual details+"" Button=""Left"" Direction=""Up"" Xpos=""1614"" Ypos=""896"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""13"" Delay=""0"" Action=""BB: Change visual details (beginning)"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                //System.Threading.Thread.Sleep(2 * 60 * 1000); // let me interact with tsunami
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[1]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station, "station is null");

                Assert.AreEqual(0, station.Parameters2._StationPoint._Coordinates.Su.X.Value, 0.001, "wrong x for station point");
                Assert.AreEqual(0, station.Parameters2._StationPoint._Coordinates.Su.Y.Value, 0.001, "wrong y for station point");
                Assert.AreEqual(0, station.Parameters2._StationPoint._Coordinates.Su.Z.Value, 0.001, "wrong z for station point");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsuT");
            });
        }

    }
}
