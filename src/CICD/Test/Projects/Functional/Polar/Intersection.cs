﻿using Functional_Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using UnitTestGenerator;

namespace Functional_Tests.Polar
{
    [TestClass]
    public class Intersection
    {

        /// <summary>
        /// network: the zone choosen id LHCb
        /// 926R005.REF.001.          1.658964     1.658964     0.609067
        /// 926R005.REF.002.         -0.058969     1.980180     0.372939
        /// 926R005.REF.003.         -0.564738    -1.399225     0.437875
        /// 926R005.REF.004.          0.497408    -1.569622     0.559243
        /// 926R005.REF.005.          0.678820    -1.597149     0.553405
        /// st2 is on ref1 and open with 100 on r5
        /// st3 is on ref5 and open with 100 on r1
        /// measuring new.point with 150 from st1 and 50 from st2


        [TestMethod()]
        public void L3_P_I_Step1_Create_First_stations()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(OLOC.NewFrameInR005.L2_P_NF_Step3_Compute_With_LGC_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""0.51"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""262"" trace=""Polar Module+"" Button=""Left"" Direction=""Down"" Xpos=""699"" Ypos=""127"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""89"" trace=""Polar Module+"" Button=""Left"" Direction=""Up"" Xpos=""699"" Ypos=""127"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Polar Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Polar Module end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""254"" trace=""New Station+"" Button=""Left"" Direction=""Down"" Xpos=""844"" Ypos=""162"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""114"" trace=""New Station+"" Button=""Left"" Direction=""Up"" Xpos=""844"" Ypos=""162"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: New Station (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""298"" trace=""button1+Clone"" Button=""Left"" Direction=""Down"" Xpos=""907"" Ypos=""645"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""106"" trace=""button1+Clone"" Button=""Left"" Direction=""Up"" Xpos=""907"" Ypos=""645"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""MB: Clone previous station parameters?: Clone"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""257"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""1083"" Ypos=""624"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""97"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""1083"" Ypos=""624"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""13"" Delay=""0"" Action=""MB: Station point name?: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""BB: New Station end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""269"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""464"" Ypos=""388"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""464"" Ypos=""388"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""27"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""683"" Ypos=""512"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""113"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""683"" Ypos=""512"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""24"" trace=""Known position+"" Button=""Left"" Direction=""Down"" Xpos=""838"" Ypos=""542"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""90"" trace=""Known position+"" Button=""Left"" Direction=""Up"" Xpos=""838"" Ypos=""542"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""21"" Delay=""0"" Action=""BB: Known position (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""293"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""238"" Ypos=""356"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""238"" Ypos=""356"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""221"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""864"" Ypos=""907"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""90"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""864"" Ypos=""907"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""26"" Delay=""0"" Action=""MB: Point stationned: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""27"" Delay=""0"" Action=""BB: Known position end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""297"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""421"" Ypos=""266"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""114"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""421"" Ypos=""266"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""217"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""543"" Ypos=""357"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""543"" Ypos=""357"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""214"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Down"" Xpos=""751"" Ypos=""396"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""105"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Up"" Xpos=""751"" Ypos=""396"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Add known Point(s) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""24"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""237"" Ypos=""469"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""237"" Ypos=""469"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""280"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""871"" Ypos=""907"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""122"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""871"" Ypos=""907"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""43"" Delay=""0"" Action=""MB: Elements Selection: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""44"" Delay=""0"" Action=""BB: Add known Point(s) end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""45"" Delay=""227"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""550"" Ypos=""679"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""46"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""550"" Ypos=""679"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""47"" Delay=""234"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1303"" Ypos=""877"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""48"" Delay=""97"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1303"" Ypos=""877"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""49"" Delay=""0"" Action=""BB: Valid (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""50"" Delay=""202"" trace=""button1+Continue"" Button=""Left"" Direction=""Down"" Xpos=""964"" Ypos=""593"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""51"" Delay=""98"" trace=""button1+Continue"" Button=""Left"" Direction=""Up"" Xpos=""964"" Ypos=""593"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""52"" Delay=""0"" Action=""MB: Big differences in distances (station &lt;-&gt; point): Continue"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""53"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""287"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""427"" Ypos=""296"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""53"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""427"" Ypos=""296"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""252"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""558"" Ypos=""329"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""59"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""558"" Ypos=""329"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""210"" trace=""Control Opening+"" Button=""Left"" Direction=""Down"" Xpos=""736"" Ypos=""587"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""37"" trace=""Control Opening+"" Button=""Left"" Direction=""Up"" Xpos=""736"" Ypos=""587"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Control Opening (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Control Opening end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""215"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""724"" Ypos=""681"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""20"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""724"" Ypos=""681"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""218"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1274"" Ypos=""886"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""46"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1274"" Ypos=""886"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Valid (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""208"" trace=""button1+Continue"" Button=""Left"" Direction=""Down"" Xpos=""957"" Ypos=""598"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""95"" trace=""button1+Continue"" Button=""Left"" Direction=""Up"" Xpos=""957"" Ypos=""598"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""MB: Big differences in distances (station &lt;-&gt; point): Continue"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""226"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""957"" Ypos=""598"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""73"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""957"" Ypos=""598"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""21"" Delay=""0"" Action=""MB: Closure OK: OK!"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""22"" Delay=""0"" Action=""BB: Valid end"" />


    <IInputActivity xsi:type=""MouseActivity"" Pos.=""54"" Delay=""281"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1139"" Ypos=""565"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""55"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1139"" Ypos=""565"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""54"" Delay=""281"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""428"" Ypos=""264"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""55"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""428"" Ypos=""264"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""56"" Delay=""218"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""618"" Ypos=""395"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""57"" Delay=""122"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""618"" Ypos=""395"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""58"" Delay=""29"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Down"" Xpos=""762"" Ypos=""503"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""59"" Delay=""66"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Up"" Xpos=""762"" Ypos=""503"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""60"" Delay=""0"" Action=""BB: Computing (LGC2) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""61"" Delay=""230"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""967"" Ypos=""697"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""62"" Delay=""98"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""967"" Ypos=""697"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""63"" Delay=""0"" Action=""MB: Computing results (LGC adjustment): OK!"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""64"" Delay=""293"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""891"" Ypos=""745"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""65"" Delay=""97"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""891"" Ypos=""745"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""66"" Delay=""0"" Action=""MB: Compensation: Valid"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""67"" Delay=""245"" trace=""button1+Yes"" Button=""Left"" Direction=""Down"" Xpos=""895"" Ypos=""560"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""68"" Delay=""178"" trace=""button1+Yes"" Button=""Left"" Direction=""Up"" Xpos=""895"" Ypos=""560"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""69"" Delay=""0"" Action=""MB: Are you sure?: Yes"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""70"" Delay=""0"" Action=""BB: Computing (LGC2) end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (Action<TSU.Common.Tsunami>)((tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var pm = tsunami.MeasurementModules[0] as TSU.Polar.Module;
                Assert.IsNotNull(pm, "no polar module");

                Assert.IsTrue(pm.StationModules.Count == 2, $"{pm.StationModules.Count} stations instead of 3");

                var station = pm.StationModules[1]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station, "no sation ref 1");
                Assert.IsTrue(station.Parameters2._StationPoint._Name == "926R005.REF.001.");
                var goods = station.GoodMeasuresTaken;
                Assert.IsTrue(goods.Count > 0, "no good measures");
                Assert.IsTrue(goods[0]._Point._Name == "926R005.REF.005.");
                Assert.IsTrue(station.Parameters2.Setups.InitialValues.IsPositionKnown = true, "initial value steup is not set as position known");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            }));
        }


        [TestMethod()]
        public void L3_P_I_Step2_Create_second_stations()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_P_I_Step1_Create_First_stations) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""0.51"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""71"" Delay=""282"" trace=""Polar Module+"" Button=""Left"" Direction=""Down"" Xpos=""699"" Ypos=""127"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""72"" Delay=""129"" trace=""Polar Module+"" Button=""Left"" Direction=""Up"" Xpos=""699"" Ypos=""127"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""73"" Delay=""0"" Action=""BB: Polar Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""74"" Delay=""0"" Action=""BB: Polar Module end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""75"" Delay=""262"" trace=""New Station+"" Button=""Left"" Direction=""Down"" Xpos=""852"" Ypos=""162"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""76"" Delay=""106"" trace=""New Station+"" Button=""Left"" Direction=""Up"" Xpos=""852"" Ypos=""162"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""77"" Delay=""0"" Action=""BB: New Station (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""78"" Delay=""201"" trace=""button1+Clone"" Button=""Left"" Direction=""Down"" Xpos=""881"" Ypos=""662"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""79"" Delay=""130"" trace=""button1+Clone"" Button=""Left"" Direction=""Up"" Xpos=""881"" Ypos=""662"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""80"" Delay=""0"" Action=""MB: Clone previous station parameters?: Clone"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""81"" Delay=""0"" Action=""BB: New Station end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""82"" Delay=""190"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""465"" Ypos=""391"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""83"" Delay=""122"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""465"" Ypos=""391"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""84"" Delay=""206"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""639"" Ypos=""517"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""85"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""639"" Ypos=""517"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""86"" Delay=""285"" trace=""Known position+"" Button=""Left"" Direction=""Down"" Xpos=""775"" Ypos=""544"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""87"" Delay=""98"" trace=""Known position+"" Button=""Left"" Direction=""Up"" Xpos=""775"" Ypos=""544"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""88"" Delay=""0"" Action=""BB: Known position (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""89"" Delay=""206"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""213"" Ypos=""464"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""90"" Delay=""95"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""213"" Ypos=""464"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""91"" Delay=""249"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""891"" Ypos=""923"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""92"" Delay=""97"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""891"" Ypos=""923"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""93"" Delay=""0"" Action=""MB: Point stationned: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""94"" Delay=""0"" Action=""BB: Known position end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""95"" Delay=""238"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""428"" Ypos=""262"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""96"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""428"" Ypos=""262"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""97"" Delay=""238"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""578"" Ypos=""353"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""98"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""578"" Ypos=""353"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""99"" Delay=""234"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Down"" Xpos=""728"" Ypos=""392"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""100"" Delay=""138"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Up"" Xpos=""728"" Ypos=""392"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""101"" Delay=""0"" Action=""BB: Add known Point(s) (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""102"" Delay=""285"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""236"" Ypos=""354"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""103"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""236"" Ypos=""354"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""104"" Delay=""282"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""876"" Ypos=""916"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""105"" Delay=""82"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""876"" Ypos=""916"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""106"" Delay=""0"" Action=""MB: Elements Selection: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""107"" Delay=""0"" Action=""BB: Add known Point(s) end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""108"" Delay=""225"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""546"" Ypos=""671"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""109"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""546"" Ypos=""671"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""110"" Delay=""261"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1252"" Ypos=""874"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""111"" Delay=""97"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1252"" Ypos=""874"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""112"" Delay=""0"" Action=""BB: Valid (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""113"" Delay=""237"" trace=""button1+Continue"" Button=""Left"" Direction=""Down"" Xpos=""990"" Ypos=""609"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""114"" Delay=""98"" trace=""button1+Continue"" Button=""Left"" Direction=""Up"" Xpos=""990"" Ypos=""609"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""115"" Delay=""0"" Action=""MB: Big differences in distances (station &lt;-&gt; point): Continue"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""116"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""287"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""427"" Ypos=""296"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""53"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""427"" Ypos=""296"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""252"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""558"" Ypos=""329"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""59"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""558"" Ypos=""329"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""210"" trace=""Control Opening+"" Button=""Left"" Direction=""Down"" Xpos=""736"" Ypos=""587"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""37"" trace=""Control Opening+"" Button=""Left"" Direction=""Up"" Xpos=""736"" Ypos=""587"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Control Opening (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Control Opening end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""215"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""724"" Ypos=""681"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""20"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""724"" Ypos=""681"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""218"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1274"" Ypos=""886"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""46"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1274"" Ypos=""886"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""208"" trace=""button1+Continue"" Button=""Left"" Direction=""Down"" Xpos=""957"" Ypos=""598"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""95"" trace=""button1+Continue"" Button=""Left"" Direction=""Up"" Xpos=""957"" Ypos=""598"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""MB: Big differences in distances (station &lt;-&gt; point): Continue"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""226"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""957"" Ypos=""598"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""73"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""957"" Ypos=""598"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""21"" Delay=""0"" Action=""MB: Closure OK: OK!"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""22"" Delay=""0"" Action=""BB: Valid end"" />


    <IInputActivity xsi:type=""MouseActivity"" Pos.=""54"" Delay=""281"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1139"" Ypos=""565"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""55"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1139"" Ypos=""565"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""117"" Delay=""213"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""422"" Ypos=""258"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""118"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""422"" Ypos=""258"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""119"" Delay=""245"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""594"" Ypos=""390"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""120"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""594"" Ypos=""390"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""121"" Delay=""246"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Down"" Xpos=""755"" Ypos=""495"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""122"" Delay=""106"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Up"" Xpos=""755"" Ypos=""495"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""123"" Delay=""0"" Action=""BB: Computing (LGC2) (beginning)"" />


    <IInputActivity xsi:type=""MouseActivity"" Pos.=""124"" Delay=""294"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""933"" Ypos=""676"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""125"" Delay=""81"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""933"" Ypos=""676"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""126"" Delay=""0"" Action=""MB: Computing results (LGC adjustment): OK!"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""127"" Delay=""221"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""899"" Ypos=""758"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""128"" Delay=""90"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""899"" Ypos=""758"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""129"" Delay=""0"" Action=""MB: Compensation: Valid"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""130"" Delay=""201"" trace=""button1+Yes"" Button=""Left"" Direction=""Down"" Xpos=""881"" Ypos=""570"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""131"" Delay=""122"" trace=""button1+Yes"" Button=""Left"" Direction=""Up"" Xpos=""881"" Ypos=""570"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""132"" Delay=""0"" Action=""MB: Are you sure?: Yes"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""133"" Delay=""0"" Action=""BB: Computing (LGC2) end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (Action<TSU.Common.Tsunami>)((tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var pm = tsunami.MeasurementModules[0] as TSU.Polar.Module;
                Assert.IsNotNull(pm, "no polar module");

                Assert.IsTrue(pm.StationModules.Count == 3, $"{pm.StationModules.Count} stations instead of 3");

                var station = pm.StationModules[1]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station, "no sation ref 1");
                Assert.IsTrue(station.Parameters2._StationPoint._Name == "926R005.REF.001.");
                var goods = station.GoodMeasuresTaken;
                Assert.IsTrue(goods.Count > 0, "no good measures");
                Assert.IsTrue(goods[0]._Point._Name == "926R005.REF.005.");
                Assert.IsTrue(station.Parameters2.Setups.InitialValues.IsPositionKnown = true, "initial value steup is not set as position known");

                station = pm.StationModules[2]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station, "no sation ref 5");
                Assert.IsTrue(station.Parameters2._StationPoint._Name == "926R005.REF.005.");
                Assert.IsTrue(station.GoodMeasuresTaken[0]._Point._Name == "926R005.REF.001.");

                Assert.IsTrue(station.Parameters2.Setups.InitialValues.IsPositionKnown = true, "initial value steup is not set as position known");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            }));
        }


        [TestMethod()]
        public void L3_P_I_Step3_Measure_without_distance()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_P_I_Step2_Create_second_stations) + ".tsut"),
                MacroXmlContent = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""L3_P_I_Step2_Measure_without_distance"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor="".5"" />  
  <KeyStrokesAndMouseClicks>
      <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""241"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""426"" Ypos=""267"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""73"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""426"" Ypos=""267"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""207"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""425"" Ypos=""299"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""425"" Ypos=""299"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""190"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""524"" Ypos=""325"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""66"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""524"" Ypos=""325"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""293"" trace=""Add New point+"" Button=""Left"" Direction=""Down"" Xpos=""674"" Ypos=""432"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""82"" trace=""Add New point+"" Button=""Left"" Direction=""Up"" Xpos=""674"" Ypos=""432"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Add New point (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Add New point end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""297"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""527"" Ypos=""752"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""53"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""527"" Ypos=""752"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""70"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""14"" Delay=""53"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""59"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{TAB}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""83"" Delay=""97"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""84"" Delay=""169"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""90"" Delay=""250"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""91"" Delay=""73"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""91"" Delay=""73"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{6}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""91"" Delay=""73"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""19"" Delay=""56"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{TAB}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""57"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{SUBTRACT}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""21"" Delay=""42"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""22"" Delay=""59"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""23"" Delay=""54"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""55"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""25"" Delay=""58"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""237"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1331"" Ypos=""882"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""74"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1331"" Ypos=""882"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""28"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""29"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""218"" trace=""Polar Module+"" Button=""Left"" Direction=""Down"" Xpos=""793"" Ypos=""134"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""114"" trace=""Polar Module+"" Button=""Left"" Direction=""Up"" Xpos=""793"" Ypos=""134"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""32"" Delay=""0"" Action=""BB: Polar Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""33"" Delay=""0"" Action=""BB: Polar Module end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""281"" trace=""ST 2 (Created on: Replaced_Date_And_Time)+"" Button=""Left"" Direction=""Down"" Xpos=""951"" Ypos=""483"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""105"" trace=""ST 2 (Created on: Replaced_Date_And_Time)+"" Button=""Left"" Direction=""Up"" Xpos=""951"" Ypos=""483"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""36"" Delay=""0"" Action=""BB: ST 2 (Created on: Replaced_Date_And_Time) (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""BB: ST 2 (Created on: Replaced_Date_And_Time) end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""266"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""426"" Ypos=""263"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""426"" Ypos=""263"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""258"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""428"" Ypos=""297"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""428"" Ypos=""297"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""45"" Delay=""298"" trace=""STtheo_240521_135939_926R005_REF_001_+"" Button=""Left"" Direction=""Up"" Xpos=""792"" Ypos=""196"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""46"" Delay=""257"" trace=""STtheo_240521_135939_926R005_REF_001_+"" Button=""Left"" Direction=""Down"" Xpos=""795"" Ypos=""213"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""47"" Delay=""82"" trace=""STtheo_240521_135939_926R005_REF_001_+"" Button=""Left"" Direction=""Up"" Xpos=""795"" Ypos=""213"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""48"" Delay=""0"" Action=""BB: STtheo_240521_135939_926R005_REF_001_ (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""49"" Delay=""0"" Action=""BB: STtheo_240521_135939_926R005_REF_001_ end""  />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""65"" Delay=""297"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""566"" Ypos=""327"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""66"" Delay=""114"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""566"" Ypos=""327"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""67"" Delay=""245"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Down"" Xpos=""742"" Ypos=""364"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""68"" Delay=""122"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Up"" Xpos=""742"" Ypos=""364"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""69"" Delay=""0"" Action=""BB: Add known Point(s) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""70"" Delay=""229"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""226"" Ypos=""498"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""71"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""226"" Ypos=""498"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""72"" Delay=""298"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""925"" Ypos=""944"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""73"" Delay=""74"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""925"" Ypos=""944"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""74"" Delay=""0"" Action=""MB: Elements Selection: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""75"" Delay=""0"" Action=""BB: Add known Point(s) end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""76"" Delay=""221"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""543"" Ypos=""748"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""77"" Delay=""130"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""543"" Ypos=""748"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""78"" Delay=""269"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""79"" Delay=""123"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""80"" Delay=""96"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""81"" Delay=""254"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{TAB}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""16"" Delay=""59"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""17"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""18"" Delay=""55"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""55"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""18"" Delay=""55"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{7}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""18"" Delay=""55"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""18"" Delay=""55"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{7}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""85"" Delay=""227"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{TAB}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""86"" Delay=""336"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{SUBTRACT}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""87"" Delay=""122"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""88"" Delay=""153"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""89"" Delay=""162"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""90"" Delay=""250"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""91"" Delay=""73"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""92"" Delay=""245"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1226"" Ypos=""884"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""93"" Delay=""89"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1226"" Ypos=""884"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""94"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""95"" Delay=""0"" Action=""BB: Valid end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (Action<TSU.Common.Tsunami>)((tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[1]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station, "station is null");

                Assert.IsTrue(station.MeasuresTaken.Count == 3, "Must be 3 measure taken");
                Assert.IsTrue((station.MeasuresTaken[2] as TSU.Polar.Measure).Distance.Corrected.Value == -999.9, "must be no value for the distance obs");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            }));
        }

        [TestMethod()]
        public void L3_P_I_Step4_compute_intersection()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_P_I_Step3_Measure_without_distance) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" FunctionKey=""F1"" speedFactor=""1"">
IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />    
<KeyStrokesAndMouseClicks>
	    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""208"" trace=""STtheo_240521_135952_926R005_REF_005_+"" Button=""Left"" Direction=""Down"" Xpos=""752"" Ypos=""206"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""82"" trace=""STtheo_240521_135952_926R005_REF_005_+"" Button=""Left"" Direction=""Up"" Xpos=""752"" Ypos=""206"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: STtheo_240521_135952_926R005_REF_005_ (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: STtheo_240521_135952_926R005_REF_005_ end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""258"" trace=""Elements manager+"" Button=""Left"" Direction=""Down"" Xpos=""1317"" Ypos=""246"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""81"" trace=""Elements manager+"" Button=""Left"" Direction=""Up"" Xpos=""1318"" Ypos=""246"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Elements manager (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""245"" trace=""Manager+"" Button=""Left"" Direction=""Down"" Xpos=""1337"" Ypos=""410"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""82"" trace=""Manager+"" Button=""Left"" Direction=""Up"" Xpos=""1337"" Ypos=""410"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Manager: 0 item(s) selected (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Manager: 0 item(s) selected end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""253"" trace=""Computation+"" Button=""Left"" Direction=""Down"" Xpos=""1364"" Ypos=""1012"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""106"" trace=""Computation+"" Button=""Left"" Direction=""Up"" Xpos=""1364"" Ypos=""1012"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""BB: Computation (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Computation end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""221"" trace=""Intersections+"" Button=""Left"" Direction=""Down"" Xpos=""1530"" Ypos=""880"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""22"" trace=""Intersections+"" Button=""Left"" Direction=""Up"" Xpos=""1530"" Ypos=""880"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""BB: Intersections (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""533"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1587"" Ypos=""293"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1587"" Ypos=""293"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""245"" trace=""button1+Yes"" Button=""Left"" Direction=""Down"" Xpos=""882"" Ypos=""557"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""106"" trace=""button1+Yes"" Button=""Left"" Direction=""Up"" Xpos=""882"" Ypos=""557"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""MB: Import points?: Yes"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Intersections end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""294"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""967"" Ypos=""748"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""114"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""967"" Ypos=""748"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""MB: Elements available in this instance of tsunami: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Elements manager end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""269"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""967"" Ypos=""756"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""98"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""967"" Ypos=""757"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""19"" Delay=""0"" Action=""MB: Elements available in this instance of tsunami: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""20"" Delay=""0"" Action=""BB: Elements manager end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[1]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station);
                Assert.AreEqual(0.04371, tsunami.Points.FirstOrDefault(x => x._Origin.Contains("Inters") && x._Name.Contains("REF.006."))._Coordinates.Su.Y.Value, 0.001);
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }
    }
}
