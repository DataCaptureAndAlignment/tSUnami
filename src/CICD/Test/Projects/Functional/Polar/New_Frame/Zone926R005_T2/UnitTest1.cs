﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Functional_Tests;

namespace Functional.Polar.New_Frame.Zone926R005_T2
{
    [TestClass]
    public class UnitTest1
    {
        [TestClass()]
        public class Zone926R005
        {
            /// <summary>
            /// network: the zone choosen id LHCb
            /// 926R005.REF.001.          1.658964     1.658964     0.609067
            /// 926R005.REF.002.         -0.058969     1.980180     0.372939
            /// 926R005.REF.003.         -0.564738    -1.399225     0.437875
            /// 926R005.REF.004.          0.497408    -1.569622     0.559243
            /// 926R005.REF.005.          0.678820    -1.597149     0.553405
            /// 
            private List<TSU.Common.Elements.Point> GetNetworkPointsInLHCb()
            {
                string xmlContent = @"<?xml version=""1.0"" encoding=""utf-8""?>
<ArrayOfPoint xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"">
    <Point Name=""926R005.REF.001."" GUID=""b52f6a53-604a-40d5-bec9-c473754a79f7"" State=""Good"" _Origin=""C:\data\tsunami\Theoretical Files\926\926R005.SU.txt.SU.txt"" _Accelerator=""926R005"" _Zone=""926R005"" _ClassAndNumero=""REF.001"" _OriginType=""Unknown"" fileElementType=""point"" HasChild=""false"" Date=""2024-03-20T11:27:33.4278757+01:00"" RefRoll=""false"" _Point="""" SocketType=""32"" SerialNumber="""" LGCFixOption=""POIN"" Type=""Reference"">
      <_Coordinates Approx=""false"">
        <ListOfAllCoordinates>
          <Coordinates Name=""926R005.REF.001."" GUID=""072175d6-03bf-4321-8e03-3a707c96db3c"" Frame=""USERDEFINED"" ReferenceFrame=""Unknown"" CoordinateSystem=""_3DCartesian"" labels=""Xusr Yusr Zusr"">
            <X Value=""-999.9"" Sigma=""-999.9"" />
            <Y Value=""-999.9"" Sigma=""-999.9"" />
            <Z Value=""-999.9"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.001."" GUID=""8bb5a16c-872c-4bbe-b9be-29cb46719dc6"" Frame=""CCS-H"" ReferenceFrame=""Unknown"" CoordinateSystem=""_2DPlusH"" labels=""Xccs Yccs H"">
            <X Value=""4471.6192930063435"" Sigma=""-999.9"" />
            <Y Value=""5008.1369592289921"" Sigma=""-999.9"" />
            <Z Value=""331.79080008462984"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.001."" GUID=""7372043d-72c2-4b1e-a9e8-d6aaa861b398"" Frame=""SU"" ReferenceFrame=""Su1985Machine"" CoordinateSystem=""_3DCartesian"" labels=""Xsu Ysu Zsu"">
            <X Value=""1.658964"" Sigma=""-999.9"" />
            <Y Value=""1.658964"" Sigma=""-999.9"" />
            <Z Value=""0.609067"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.001."" GUID=""1937ad03-6083-425d-8db5-a253096f494f"" Frame=""PHYSICIST"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xphys Yphys Zphys"">
            <X Value=""-1.6589639999999997"" Sigma=""-999.9"" />
            <Y Value=""0.61503796215914019"" Sigma=""-999.9"" />
            <Z Value=""-1.6567596291822584"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.001."" GUID=""cb12f561-bd91-43a4-aa74-419b6191c426"" Frame=""MLA"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xmla Ymla Zmla"">
            <X Value=""1.6589640000000006"" Sigma=""-999.9"" />
            <Y Value=""-1.6589639999999997"" Sigma=""-999.9"" />
            <Z Value=""0.609067"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""Unknown"" GUID=""50490233-e513-4136-bf9c-569416c4d976"" Frame=""CCS-Z"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xccs Yccs ZRS2K"">
            <X Value=""4471.6192930063435"" Sigma=""-999.9"" />
            <Y Value=""5008.1369592289921"" Sigma=""-999.9"" />
            <Z Value=""2330.6574618557129"" Sigma=""-999.9"" />
          </Coordinates>
        </ListOfAllCoordinates>
      </_Coordinates>
      <_Parameters Tilt=""-999.9"" Slope=""-999.9"" GisementFaisceau=""-999.9"" R=""-999.9"" S=""-999.9"" Cumul=""-999.9"" T=""-999.9"" L=""-999.9"" />
    </Point>
    <Point Name=""926R005.REF.002."" GUID=""4ed5e955-fdb7-4f66-b2b8-aaf4c2ab615b"" State=""Good"" _Origin=""C:\data\tsunami\Theoretical Files\926\926R005.SU.txt.SU.txt"" _Accelerator=""926R005"" _Zone=""926R005"" _ClassAndNumero=""REF.002"" _OriginType=""Unknown"" fileElementType=""point"" HasChild=""false"" Date=""2024-03-20T11:27:34.4278757+01:00"" RefRoll=""false"" _Point="""" SocketType=""32"" SerialNumber="""" LGCFixOption=""POIN"" Type=""Reference"">
      <_Coordinates Approx=""false"">
        <ListOfAllCoordinates>
          <Coordinates Name=""926R005.REF.002."" GUID=""eac8907e-3fcd-4c89-9c9e-31d12de7669c"" Frame=""USERDEFINED"" ReferenceFrame=""Unknown"" CoordinateSystem=""_3DCartesian"" labels=""Xusr Yusr Zusr"">
            <X Value=""-999.9"" Sigma=""-999.9"" />
            <Y Value=""-999.9"" Sigma=""-999.9"" />
            <Z Value=""-999.9"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.002."" GUID=""19b31f17-c511-4fcf-85fa-c1ac062218aa"" Frame=""CCS-H"" ReferenceFrame=""Unknown"" CoordinateSystem=""_2DPlusH"" labels=""Xccs Yccs H"">
            <X Value=""4470.6711273726451"" Sigma=""-999.9"" />
            <Y Value=""5006.6686466851152"" Sigma=""-999.9"" />
            <Z Value=""331.55467193677777"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.002."" GUID=""d343abca-31f3-4ed2-a741-a59f4bd72367"" Frame=""SU"" ReferenceFrame=""Su1985Machine"" CoordinateSystem=""_3DCartesian"" labels=""Xsu Ysu Zsu"">
            <X Value=""-0.058969"" Sigma=""-999.9"" />
            <Y Value=""1.98018"" Sigma=""-999.9"" />
            <Z Value=""0.372939"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.002."" GUID=""c9226fa5-a005-4c6c-b472-b22a7b05c387"" Frame=""PHYSICIST"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xphys Yphys Zphys"">
            <X Value=""-1.98018"" Sigma=""-999.9"" />
            <Y Value=""0.37272419889645797"" Sigma=""-999.9"" />
            <Z Value=""0.060311791873510504"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.002."" GUID=""1086eda2-b1ba-49c5-8066-6296000362bb"" Frame=""MLA"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xmla Ymla Zmla"">
            <X Value=""1.98018"" Sigma=""-999.9"" />
            <Y Value=""0.058969000000000438"" Sigma=""-999.9"" />
            <Z Value=""0.372939"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""Unknown"" GUID=""2f5a5908-f605-4cb2-b763-43733a26f4e1"" Frame=""CCS-Z"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xccs Yccs ZRS2K"">
            <X Value=""4470.6711273726451"" Sigma=""-999.9"" />
            <Y Value=""5006.6686466851152"" Sigma=""-999.9"" />
            <Z Value=""2330.4223658147771"" Sigma=""-999.9"" />
          </Coordinates>
        </ListOfAllCoordinates>
      </_Coordinates>
      <_Parameters Tilt=""-999.9"" Slope=""-999.9"" GisementFaisceau=""-999.9"" R=""-999.9"" S=""-999.9"" Cumul=""-999.9"" T=""-999.9"" L=""-999.9"" />
    </Point>
    <Point Name=""926R005.REF.003."" GUID=""64a2b623-95ad-496a-b10d-266fb25bb010"" State=""Good"" _Origin=""C:\data\tsunami\Theoretical Files\926\926R005.SU.txt.SU.txt"" _Accelerator=""926R005"" _Zone=""926R005"" _ClassAndNumero=""REF.003"" _OriginType=""Unknown"" fileElementType=""point"" HasChild=""false"" Date=""2024-03-20T11:27:35.4278757+01:00"" RefRoll=""false"" _Point="""" SocketType=""32"" SerialNumber="""" LGCFixOption=""POIN"" Type=""Reference"">
      <_Coordinates Approx=""false"">
        <ListOfAllCoordinates>
          <Coordinates Name=""926R005.REF.003."" GUID=""2f402851-5db8-4a6d-bda4-06384774acb0"" Frame=""USERDEFINED"" ReferenceFrame=""Unknown"" CoordinateSystem=""_3DCartesian"" labels=""Xusr Yusr Zusr"">
            <X Value=""-999.9"" Sigma=""-999.9"" />
            <Y Value=""-999.9"" Sigma=""-999.9"" />
            <Z Value=""-999.9"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.003."" GUID=""7db5b5fd-38f6-4350-82ae-e5aafa0b2780"" Frame=""CCS-H"" ReferenceFrame=""Unknown"" CoordinateSystem=""_2DPlusH"" labels=""Xccs Yccs H"">
            <X Value=""4473.6070787319568"" Sigma=""-999.9"" />
            <Y Value=""5004.9203880412861"" Sigma=""-999.9"" />
            <Z Value=""331.61960781092631"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.003."" GUID=""d728199b-be88-439a-8ef6-e6cd9d9ac776"" Frame=""SU"" ReferenceFrame=""Su1985Machine"" CoordinateSystem=""_3DCartesian"" labels=""Xsu Ysu Zsu"">
            <X Value=""-0.564738"" Sigma=""-999.9"" />
            <Y Value=""-1.399225"" Sigma=""-999.9"" />
            <Z Value=""0.437875"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.003."" GUID=""a3717ca3-a2e8-4d78-a2c7-56c5d9918578"" Frame=""PHYSICIST"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xphys Yphys Zphys"">
            <X Value=""1.3992249999999997"" Sigma=""-999.9"" />
            <Y Value=""0.43583820386412941"" Sigma=""-999.9"" />
            <Z Value=""0.56631138459463271"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.003."" GUID=""274fa341-c627-481f-acef-27eb56b7cb39"" Frame=""MLA"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xmla Ymla Zmla"">
            <X Value=""-1.3992250000000002"" Sigma=""-999.9"" />
            <Y Value=""0.56473799999999963"" Sigma=""-999.9"" />
            <Z Value=""0.437875"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""Unknown"" GUID=""1d1d0fbb-fef0-415d-9007-8f590fa57f1f"" Frame=""CCS-Z"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xccs Yccs ZRS2K"">
            <X Value=""4473.6070787319568"" Sigma=""-999.9"" />
            <Y Value=""5004.9203880412861"" Sigma=""-999.9"" />
            <Z Value=""2330.4870067333427"" Sigma=""-999.9"" />
          </Coordinates>
        </ListOfAllCoordinates>
      </_Coordinates>
      <_Parameters Tilt=""-999.9"" Slope=""-999.9"" GisementFaisceau=""-999.9"" R=""-999.9"" S=""-999.9"" Cumul=""-999.9"" T=""-999.9"" L=""-999.9"" />
    </Point>
    <Point Name=""926R005.REF.004."" GUID=""40a1d1b6-8554-448e-b8fa-e39080f1c157"" State=""Good"" _Origin=""C:\data\tsunami\Theoretical Files\926\926R005.SU.txt.SU.txt"" _Accelerator=""926R005"" _Zone=""926R005"" _ClassAndNumero=""REF.004"" _OriginType=""Unknown"" fileElementType=""point"" HasChild=""false"" Date=""2024-03-20T11:27:36.4278757+01:00"" RefRoll=""false"" _Point="""" SocketType=""32"" SerialNumber="""" LGCFixOption=""POIN"" Type=""Reference"">
      <_Coordinates Approx=""false"">
        <ListOfAllCoordinates>
          <Coordinates Name=""926R005.REF.004."" GUID=""2ad00226-0973-45f9-a6cb-38ab3b698f26"" Frame=""USERDEFINED"" ReferenceFrame=""Unknown"" CoordinateSystem=""_3DCartesian"" labels=""Xusr Yusr Zusr"">
            <X Value=""-999.9"" Sigma=""-999.9"" />
            <Y Value=""-999.9"" Sigma=""-999.9"" />
            <Z Value=""-999.9"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.004."" GUID=""47d40ed3-3af3-47dc-af03-ca4881e64cc4"" Frame=""CCS-H"" ReferenceFrame=""Unknown"" CoordinateSystem=""_2DPlusH"" labels=""Xccs Yccs H"">
            <X Value=""4474.1671924158709"" Sigma=""-999.9"" />
            <Y Value=""5005.8388733237407"" Sigma=""-999.9"" />
            <Z Value=""331.74097584872823"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.004."" GUID=""fd5f2389-64c5-47fa-a815-cfe089bbdb6e"" Frame=""SU"" ReferenceFrame=""Su1985Machine"" CoordinateSystem=""_3DCartesian"" labels=""Xsu Ysu Zsu"">
            <X Value=""0.497408"" Sigma=""-999.9"" />
            <Y Value=""-1.569622"" Sigma=""-999.9"" />
            <Z Value=""0.559243"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.004."" GUID=""54377433-3347-4edf-93c3-8d9c07b7c55e"" Frame=""PHYSICIST"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xphys Yphys Zphys"">
            <X Value=""1.569622"" Sigma=""-999.9"" />
            <Y Value=""0.56103083384441965"" Sigma=""-999.9"" />
            <Z Value=""-0.495390608498824"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.004."" GUID=""48c1718e-3874-459a-a6a4-3e16dd1b8cce"" Frame=""MLA"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xmla Ymla Zmla"">
            <X Value=""-1.569622"" Sigma=""-999.9"" />
            <Y Value=""-0.49740800000000035"" Sigma=""-999.9"" />
            <Z Value=""0.559243"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""Unknown"" GUID=""ba7c00f1-18a0-4493-ba95-6b8fb26ed546"" Frame=""CCS-Z"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xccs Yccs ZRS2K"">
            <X Value=""4474.1671924158709"" Sigma=""-999.9"" />
            <Y Value=""5005.8388733237407"" Sigma=""-999.9"" />
            <Z Value=""2330.6077415693453"" Sigma=""-999.9"" />
          </Coordinates>
        </ListOfAllCoordinates>
      </_Coordinates>
      <_Parameters Tilt=""-999.9"" Slope=""-999.9"" GisementFaisceau=""-999.9"" R=""-999.9"" S=""-999.9"" Cumul=""-999.9"" T=""-999.9"" L=""-999.9"" />
    </Point>
    <Point Name=""926R005.REF.005."" GUID=""1bd12d56-4716-42e3-8937-728f2b881681"" State=""Good"" _Origin=""C:\data\tsunami\Theoretical Files\926\926R005.SU.txt.SU.txt"" _Accelerator=""926R005"" _Zone=""926R005"" _ClassAndNumero=""REF.005"" _OriginType=""Unknown"" fileElementType=""point"" HasChild=""false"" Date=""2024-03-20T11:27:37.4278757+01:00"" RefRoll=""false"" _Point="""" SocketType=""32"" SerialNumber="""" LGCFixOption=""POIN"" Type=""Reference"">
      <_Coordinates Approx=""false"">
        <ListOfAllCoordinates>
          <Coordinates Name=""926R005.REF.005."" GUID=""a51f6630-4c6a-48ea-b0a8-74e2ec86d0bf"" Frame=""USERDEFINED"" ReferenceFrame=""Unknown"" CoordinateSystem=""_3DCartesian"" labels=""Xusr Yusr Zusr"">
            <X Value=""-999.9"" Sigma=""-999.9"" />
            <Y Value=""-999.9"" Sigma=""-999.9"" />
            <Z Value=""-999.9"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.005."" GUID=""6d81c2ff-309b-4be2-9ecb-94e6f9c8a960"" Frame=""CCS-H"" ReferenceFrame=""Unknown"" CoordinateSystem=""_2DPlusH"" labels=""Xccs Yccs H"">
            <X Value=""4474.2613896730245"" Sigma=""-999.9"" />
            <Y Value=""5005.9963387474236"" Sigma=""-999.9"" />
            <Z Value=""331.73513987360724"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.005."" GUID=""60b593f6-35af-4241-8ed5-67bcef5b3f0e"" Frame=""SU"" ReferenceFrame=""Su1985Machine"" CoordinateSystem=""_3DCartesian"" labels=""Xsu Ysu Zsu"">
            <X Value=""0.678825"" Sigma=""-999.9"" />
            <Y Value=""-1.597147"" Sigma=""-999.9"" />
            <Z Value=""0.553407"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.005."" GUID=""c6f1adae-c9a6-4c07-82b4-64f4675a7dce"" Frame=""PHYSICIST"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xphys Yphys Zphys"">
            <X Value=""1.5971470000000003"" Sigma=""-999.9"" />
            <Y Value=""0.5558482618148366"" Sigma=""-999.9"" />
            <Z Value=""-0.67682745076675532"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.005."" GUID=""f2bf2187-fd78-4809-8564-382f6934eb26"" Frame=""MLA"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xmla Ymla Zmla"">
            <X Value=""-1.5971469999999999"" Sigma=""-999.9"" />
            <Y Value=""-0.67882500000000034"" Sigma=""-999.9"" />
            <Z Value=""0.553407"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""Unknown"" GUID=""6a5ebd94-3b49-4d8a-b930-efd4f89134c1"" Frame=""CCS-Z"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xccs Yccs ZRS2K"">
            <X Value=""4474.2613896730245"" Sigma=""-999.9"" />
            <Y Value=""5005.9963387474236"" Sigma=""-999.9"" />
            <Z Value=""2330.6017977010215"" Sigma=""-999.9"" />
          </Coordinates>
        </ListOfAllCoordinates>
      </_Coordinates>
      <_Parameters Tilt=""-999.9"" Slope=""-999.9"" GisementFaisceau=""-999.9"" R=""-999.9"" S=""-999.9"" Cumul=""-999.9"" T=""-999.9"" L=""-999.9"" />
    </Point>
    <Point Name=""926R005.REF.006."" GUID=""a1a4d24a-7522-47d6-9601-6b0a9f28b369"" State=""Good"" _Origin=""C:\data\tsunami\Theoretical Files\926\926R005.SU.txt.SU.txt"" _Accelerator=""926R005"" _Zone=""926R005"" _ClassAndNumero=""REF.006"" _OriginType=""Unknown"" fileElementType=""point"" HasChild=""false"" Date=""2024-03-20T11:27:38.4278757+01:00"" RefRoll=""false"" _Point="""" SocketType=""32"" SerialNumber="""" LGCFixOption=""POIN"" Type=""Reference"">
      <_Coordinates Approx=""false"">
        <ListOfAllCoordinates>
          <Coordinates Name=""926R005.REF.006."" GUID=""e0ff2f95-ba56-4756-beaf-3f00b0eebe21"" Frame=""USERDEFINED"" ReferenceFrame=""Unknown"" CoordinateSystem=""_3DCartesian"" labels=""Xusr Yusr Zusr"">
            <X Value=""-999.9"" Sigma=""-999.9"" />
            <Y Value=""-999.9"" Sigma=""-999.9"" />
            <Z Value=""-999.9"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.006."" GUID=""f2a0cd4c-473a-4c14-aec6-9f058a1929c7"" Frame=""CCS-H"" ReferenceFrame=""Unknown"" CoordinateSystem=""_2DPlusH"" labels=""Xccs Yccs H"">
            <X Value=""4474.261390889309"" Sigma=""-999.9"" />
            <Y Value=""5005.996328522464"" Sigma=""-999.9"" />
            <Z Value=""331.73513587309492"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.006."" GUID=""4be01b16-02c4-457f-a1ed-80c7bea22fe9"" Frame=""SU"" ReferenceFrame=""Su1985Machine"" CoordinateSystem=""_3DCartesian"" labels=""Xsu Ysu Zsu"">
            <X Value=""0.678816"" Sigma=""-999.9"" />
            <Y Value=""-1.597152"" Sigma=""-999.9"" />
            <Z Value=""0.553403"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.006."" GUID=""6f36e920-9776-4101-a01c-2c049411ee8d"" Frame=""PHYSICIST"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xphys Yphys Zphys"">
            <X Value=""1.5971520000000001"" Sigma=""-999.9"" />
            <Y Value=""0.55584422942644651"" Sigma=""-999.9"" />
            <Z Value=""-0.67681846523149736"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.006."" GUID=""46ef09c9-121a-484b-8e66-896bb08e7b05"" Frame=""MLA"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xmla Ymla Zmla"">
            <X Value=""-1.5971519999999997"" Sigma=""-999.9"" />
            <Y Value=""-0.67881600000000031"" Sigma=""-999.9"" />
            <Z Value=""0.553403"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""Unknown"" GUID=""5c08770f-f703-451e-925e-d8c5ae54a4c0"" Frame=""CCS-Z"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xccs Yccs ZRS2K"">
            <X Value=""4474.261390889309"" Sigma=""-999.9"" />
            <Y Value=""5005.996328522464"" Sigma=""-999.9"" />
            <Z Value=""2330.6017937047636"" Sigma=""-999.9"" />
          </Coordinates>
        </ListOfAllCoordinates>
      </_Coordinates>
      <_Parameters Tilt=""-999.9"" Slope=""-999.9"" GisementFaisceau=""-999.9"" R=""-999.9"" S=""-999.9"" Cumul=""-999.9"" T=""-999.9"" L=""-999.9"" />
    </Point>
  </ArrayOfPoint>
  ";
                var points = new List<TSU.Common.Elements.Point>();
                var type = points.GetType();

                // Specify the file path
                string filePath = "Zone926R005.xml";

                try
                {
                    // Open or create the file for writing
                    using (StreamWriter writer = new StreamWriter(filePath))
                    {
                        // Write the content of the string variable to the file
                        writer.WriteLine(xmlContent);
                    }

                    Console.WriteLine("Content has been written to the file successfully.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"An error occurred: {ex.Message}");
                }

                points = TSU.IO.Xml.DeserializeFile(type, filePath) as List<TSU.Common.Elements.Point>;

                return points;
            }

            [TestMethod()]
            public void ExportPointsListToXml()
            {
                var points = new List<TSU.Common.Elements.Point>();
                points.Add(new TSU.Common.Elements.Point());
                var path = "test.xml";
                TSU.IO.Xml.CreateFile(points, path);
                Process.Start(path);
            }

            [TestMethod()]
            public void Step1_Administrative_setup_with_T2_Test()
            {
                var methodName = MethodBase.GetCurrentMethod();
                Functional_Tests.Tools.TestTsunamiWithMacro(methodName: methodName, testAction: (tsunami) =>
                {
                    Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                    Assert.AreEqual("926R005.REF.###.", tsunami.MeasurementModules[0]?.StationModules[0]?._Station?.ParametersBasic?.DefaultMeasure?._Point._Name);

                    Functional_Tests.Tools.CreateArtefact(tsunami.PathOfTheSavedFile, "Step1.tsu");
                });
            }

            [TestMethod()]
            public void Step2_Acquire_with_T2_Test()
            {
                var points = GetNetworkPointsInLHCb();

                Functional_Tests.Tools.TestTsunamiWithMacro(
                    methodName: MethodBase.GetCurrentMethod(), 
                    tsuPath: Tools.GetArtefact("Step1.tsu"), 
                    testAction: (tsunami) =>
                {
                    Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                    var lastMeasure = tsunami.MeasurementModules[0]?.StationModules[0]?._Station?.MeasuresTaken?[4] as TSU.Polar.Measure;
                    Assert.AreEqual(1.821, lastMeasure.Distance.Corrected.Value, 0.001);
                    Functional_Tests.Tools.CreateArtefact(tsunami.PathOfTheSavedFile, "Step2.tsu");
                });
            }

            [TestMethod()]
            public void Step3_Compute_With_LGC_Test()
            {
                var points = GetNetworkPointsInLHCb();

                Functional_Tests.Tools.TestTsunamiWithMacro(
                    methodName: MethodBase.GetCurrentMethod(),
                    tsuPath: Tools.GetArtefact("Step2.tsu"),
                    testAction: (tsunami) =>
                    {
                        Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                        var station = tsunami.MeasurementModules[0]?.StationModules[0]?._Station as TSU.Polar.Station;

                        Assert.AreEqual(1.609, station.PointsMeasured[4]._Coordinates.Su.X.Value, 0.001);
                        Assert.AreEqual(0, station.Parameters2.Setups.BestValues.SigmaZero, 0.001);
                        Assert.AreEqual(true, station.Parameters2.Setups.BestValues.Verified);
                        Functional_Tests.Tools.CreateArtefact(tsunami.PathOfTheSavedFile, "Step3.tsu");
                    });
            }
        }
    }
}
